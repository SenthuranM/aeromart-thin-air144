package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment.PaymentDetails;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.CustLoyalty;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests.SpecialServiceRequest;
import org.opentravel.ota._2003._05.TPAExtensionsType;
import org.opentravel.ota._2003._05.TravelersFulfillments;
import org.w3c.dom.Element;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.PNRModifyPreValidationStatesTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.dto.ApplicableBufferTimeDTO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.AddModifyGroundSegmentUtil;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.ExtensionsUtil;
import com.isa.thinair.webservices.core.util.ModifyBookingUtil;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.SSRDetailsUtil;
import com.isa.thinair.webservices.core.util.ServiceTaxCalculator;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAdminInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAlterationBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAExternalPayTxType;

/**
 * Reservation modification processes going through Airproxy.
 * 
 * @author Manoj Dhanushka
 */
public class ModifyBookingInterlineUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(ModifyBookingInterlineUtil.class);

	/**
	 * Pay outstanding amount for a reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param airBookModifyRQAAExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void balancePayment(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt airBookModifyRQAAExt, Collection privilegeKeys, UserPrincipal userPrincipal,
			ClientCommonInfoDTO clientInfoDTO, boolean isGroupPNR) throws ModuleException, WebservicesException, Exception {

		ModifyBookingUtil modifyBookingUtil = new ModifyBookingUtil();

		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_, "");
		}

		BigDecimal balanceAmountToPay = ReservationUtil.getTotalBalToPay(reservation);

		if (balanceAmountToPay.doubleValue() == 0) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_PAID, null);
		}

		AAExternalPayTxType wsExternalPayTx = new AAExternalPayTxType();

		if (airBookModifyRQAAExt.getCurrentExtPayTxInfo() != null) {
			wsExternalPayTx = airBookModifyRQAAExt.getCurrentExtPayTxInfo();
		}

		SourceType pos = otaAirBookModifyRQ.getPOS().getSource().get(0);

		String BCT = pos.getBookingChannel().getType();
		boolean isSuccess = false;
		int paxCount = 0;

		// extract existing paxIds
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (Iterator<LCCClientReservationPax> paxIdIt = reservation.getPassengers().iterator(); paxIdIt.hasNext();) {
			pnrPaxIds.add(PaxTypeUtils.getPnrPaxId(paxIdIt.next().getTravelerRefNumber()));
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments() == null) {// full payment expected
			PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();

			PaymentAssembler reservationPayments = new PaymentAssembler();
			OTAUtils.extractReservationPayments(paymentDetails.getPaymentDetail(), reservationPayments,
					userPrincipal.getAgentCurrencyCode(), exchangeRateProxy);

			WSReservationUtil.authorizePayment(privilegeKeys, reservationPayments, userPrincipal.getAgentCode(), pnrPaxIds);

			QuotedChargeDTO serviceCharge = null;
			Object[] firstPaxPnrPaxFareId = null;
			boolean isPmtIntegrationChannel = BCT
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET));

			String pmtClientIdentifier = (String) ThreadLocalData.getCurrentTransaction().getParameter(
					WebservicesContext.CLIENT_IDENTIFIER);
			if (isPmtIntegrationChannel) {
				serviceCharge = (QuotedChargeDTO) ThreadLocalData.getCurrentTnxParam(Transaction.RES_SERVICE_CHARGE);
				firstPaxPnrPaxFareId = (Object[]) ThreadLocalData
						.getCurrentTnxParam(Transaction.RES_SERVICE_CHARGED_PAX_INFO);

				if (serviceCharge != null) {

					if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
						ReservationPax reservationPax;
						for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
							reservationPax = (ReservationPax) itReservationPax.next();

							// Non infant pax count
							if (!ReservationApiUtils.isInfantType(reservationPax)) {
								paxCount = paxCount + 1;
							}
						}
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
								AccelAeroCalculator.parseBigDecimal(
										serviceCharge.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)
												* paxCount));
						// TODO: Uncomment this
						// wsExternalPayTx.setPaxCount(String.valueOf(paxCount));
					} else {
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
								AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
					}
				} else {
					balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
							AccelAeroCalculator.getDefaultBigDecimalZero());
				}
			}

			BigDecimal roundedUpBalAmountToPay = CommonUtil.getBigDecimalWithDefaultPrecision(balanceAmountToPay);

			if (BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))) {
				roundedUpBalAmountToPay = CommonUtil.getRoundedUpValue(balanceAmountToPay, 10);
			}

			IPassenger passenger = new PassengerAssembler(null);

			if (isPmtIntegrationChannel) {
				wsExternalPayTx.setBankChannelCode(BCT);
				passenger.addExternalPaymentTnxInfo(ExtensionsUtil.prepareAAExtPayTxInfo(wsExternalPayTx));
				ThreadLocalData.setWSConextParam(WebservicesContext.BALANCE_QUERY_KEY,
						wsExternalPayTx.getBalanceQueryRPH());
			} else {
				ThreadLocalData.removeWSContextParam(WebservicesContext.BALANCE_QUERY_KEY);
			}

			BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			String version = String.valueOf(reservation.getVersion() + 1);
			if (reservation.getAdminInfo() != null
					&& Integer.parseInt(reservation.getAdminInfo().getOriginChannelId()) == SalesChannelsUtil.SALES_CHANNEL_LCC) {
				version = clientInfoDTO.getCarrierCode() + "-" + version;
			}

			int payablePaxCount = 0;
			Collection<LCCClientReservationPax> passengers = reservation.getPassengers();
			for (LCCClientReservationPax traveler : passengers) {
				if (reservation.isInfantPaymentSeparated() || !ReservationApiUtils.isInfantType(traveler)) {
					payablePaxCount++;
				}
			}

			int segmentCount = 0;
			for (LCCClientReservationSegment reservationSegment : reservation.getSegments()) {
				if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
					segmentCount++;
				}
			}

			String pnr = reservation.getPNR();

			if (reservationPayments.getPayments().size() == 1) {// FIXME support multiple payment options in one request
				Object resPayment = reservationPayments.getPayments().iterator().next();
				if (resPayment instanceof AgentCreditInfo) {

					if (roundedUpBalAmountToPay.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
								"Expected payment of " + roundedUpBalAmountToPay + ", Received "
										+ reservationPayments.getTotalPayAmount());
					}

					Collection<LCCClientReservationPax> reservationPaxes = reservation.getPassengers();
					int adultCount = 1;
					BigDecimal paymentConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
					LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
					lccClientBalancePayment.setGroupPNR(reservation.getPNR());
					lccClientBalancePayment.setOwnerAgent(reservation.getAdminInfo().getOwnerAgentCode());
					lccClientBalancePayment.setForceConfirm(false);
					lccClientBalancePayment.setContactInfo(getContactInfo(reservation.getContactInfo()));
					lccClientBalancePayment.setReservationStatus(reservation.getStatus());

					for (LCCClientReservationPax traveler : reservationPaxes) {
						if (reservation.isInfantPaymentSeparated() || !ReservationApiUtils.isInfantType(traveler)) {
							paxPaymentAmount = traveler.getTotalAvailableBalance().doubleValue() > 0
									? traveler.getTotalAvailableBalance()
									: AccelAeroCalculator.getDefaultBigDecimalZero();
							// FIXME handle scenario with some pax with credits

							// Adding service charge, if present, to the first pax
							if (firstPaxPnrPaxFareId != null && PaxTypeUtils.getPnrPaxId(traveler.getTravelerRefNumber())
									.equals(firstPaxPnrPaxFareId[0])) {
								if (serviceCharge != null) {
									if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
										// Per pax service charge for FAWRY
										paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
												AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(
														QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE) * paxCount));
									} else {
										// Per PNR service charge for others
										paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, AccelAeroCalculator
												.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
									}
								} else {
									paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
											AccelAeroCalculator.subtract(roundedUpBalAmountToPay, balanceAmountToPay));
								}
							}
							if (adultCount == 1 && roundedUpBalAmountToPay.doubleValue() > balanceAmountToPay.doubleValue()) {
								// add additional payment amount resulted from rounding up, to the first adult as credit
								paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
										AccelAeroCalculator.subtract(roundedUpBalAmountToPay, balanceAmountToPay));
							}
							paymentConsumed = AccelAeroCalculator.add(paymentConsumed, paxPaymentAmount);
							IPayment paxPayment = new PaymentAssembler();
							String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
							String wsPaymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
							if (((AgentCreditInfo) resPayment).getPaymentReferenceTO() != null
									&& ((AgentCreditInfo) resPayment).getPaymentReferenceTO().getPaymentRefType() != null
									&& (((AgentCreditInfo) resPayment).getPaymentReferenceTO()
											.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) {
								wsPaymentMethod = Agent.PAYMENT_MODE_BSP;
							}
							paxPayment.addAgentCreditPayment(agentCode, paxPaymentAmount, null,
									WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
									null);

							passenger.addPassengerPayments(PaxTypeUtils.getPnrPaxId(traveler.getTravelerRefNumber()), paxPayment);
							++adultCount;

							// create LCCClientPaymentAssembler for FAWRY payment
							LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
							for (PaymentDetailType paymentDetail : paymentDetails.getPaymentDetail()) {
								String WSPaymentMethod = paymentDetail.getDirectBill().getCompanyName().getCodeContext();
								String paymentMethod = null;
								if (Agent.PAYMENT_MODE_BSP.equals(WSPaymentMethod)) {
									paymentMethod = Agent.PAYMENT_MODE_BSP;
								} else if (WSPaymentMethod != null) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
											"Payment type " + WSPaymentMethod + " not supported");
								}
								paymentAssembler.addAgentCreditPayment(agentCode, paxPaymentAmount,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), new Date(), null, null,
										paymentMethod, null);
							}

							lccClientBalancePayment.addPassengerPayments(traveler.getPaxSequence(), paymentAssembler);
						}
					}

					if (paymentConsumed.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid payment amount");
					}

					if (isPmtIntegrationChannel && serviceCharge != null) {
						// First adjustment for service charge, then do the payment in single transaction.
						if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
							// do the adjustment for service charge and balance payment via AirProxy

							// TODO: Call Airproxy method
							WebServicesModuleUtils.getPassengerBD().adjustCreditManual(reservation.getPNR(),
									(Integer) firstPaxPnrPaxFareId[1], serviceCharge.getChargeRateId(),
									AccelAeroCalculator
											.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT) * paxCount),
									"Booking paid from channel " + SalesChannelsUtil.getSalesChannelName(Integer.parseInt(BCT)),
									Long.parseLong(reservation.getVersion()), null, null, null);

							TrackInfoDTO trackInfoDTO = modifyBookingUtil.getTrackInfo(userPrincipal);
							WebServicesModuleUtils.getAirproxyReservationBD().balancePayment(lccClientBalancePayment, version,
									isGroupPNR, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
									clientInfoDTO, trackInfoDTO, false, false);

						} else {
							// TODO: Expose this method in Airproxy
							WebServicesModuleUtils.getReservationBD().updateResForPaymentWithSeriveCharge(reservation.getPNR(),
									passenger, false,
									BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM)),
									(Integer) firstPaxPnrPaxFareId[1], serviceCharge.getChargeRateId(),
									AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)),
									"Booking paid from channel " + SalesChannelsUtil.getSalesChannelName(Integer.parseInt(BCT)),
									Long.parseLong(reservation.getVersion()), null);
						}
					} else {
						WebServicesModuleUtils.getAirproxyReservationBD().balancePayment(lccClientBalancePayment,
								reservation.getVersion(), isGroupPNR, false, clientInfoDTO,
								modifyBookingUtil.getTrackInfo(userPrincipal), false, false);
					}
					isSuccess = true;
					if (AuthorizationUtil.hasPrivileges(privilegeKeys,
							PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_BALANCE_PAYMENT)) {
						sendIternary(reservation);
					}

				} else if (resPayment instanceof CardPaymentInfo) {

					if (isPmtIntegrationChannel) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Credit card payment is not supported to the channel");
					}

					CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) resPayment;
					int ipgId = cardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId();
					PayCurrencyDTO cardPayCurrencyDTO = cardPaymentInfo.getPayCurrencyDTO();
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId,
							cardPayCurrencyDTO.getPayCurrencyCode());

					ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(balanceAmountToPay, payablePaxCount,
							segmentCount, ChargeRateOperationType.MAKE_ONLY);
					BigDecimal ccFee = ccCharge.getAmount();

					if (log.isDebugEnabled()) {
						log.debug("##Balance to pay without credit card payments " + balanceAmountToPay + ", PNR=" + pnr);
						log.debug("## Credit Card Payment " + ccFee + ", PNR=" + pnr);
					}
					balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay, ccFee);
					if (log.isDebugEnabled()) {
						log.debug("##Balance to pay with credit card payments " + balanceAmountToPay + ", PNR=" + pnr);
					}
					if (balanceAmountToPay.doubleValue() != reservationPayments.getTotalPayAmount().doubleValue()) {
						log.error("Expected [" + balanceAmountToPay + "] received ["
								+ reservationPayments.getTotalPayAmount().doubleValue() + "]),PNR=" + pnr);
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
								"Expected [" + balanceAmountToPay + "] received ["
										+ reservationPayments.getTotalPayAmount().doubleValue() + "]");
					}

					Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
					extChgMap.put(ccCharge.getExternalChargesEnum(), ccCharge);
					ExternalChargesMediator creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, true, true);
					LinkedList perPaxCreditExternalCharges = creditExtChargesMediator
							.getExternalChargesForABalancePayment(payablePaxCount, 0);

					LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
					lccClientBalancePayment.setGroupPNR(reservation.getPNR());
					lccClientBalancePayment.setOwnerAgent(reservation.getAdminInfo().getOwnerAgentCode());
					lccClientBalancePayment.setForceConfirm(false);
					lccClientBalancePayment.setContactInfo(getContactInfo(reservation.getContactInfo()));
					lccClientBalancePayment.setReservationStatus(reservation.getStatus());
					lccClientBalancePayment.setExternalChargesMap(extChgMap);

					BigDecimal paymentConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
					Collection<LCCClientReservationPax> reservationPaxes = reservation.getPassengers();
					for (LCCClientReservationPax traveler : reservationPaxes) {
						if (reservation.isInfantPaymentSeparated() || !ReservationApiUtils.isInfantType(traveler)) {
							paxPaymentAmount = traveler.getTotalAvailableBalance().doubleValue() > 0
									? traveler.getTotalAvailableBalance()
									: AccelAeroCalculator.getDefaultBigDecimalZero();
							paymentConsumed = AccelAeroCalculator.add(paymentConsumed, paxPaymentAmount);

							LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

							Object extCharges = null;
							if (perPaxCreditExternalCharges.size() > 0) {
								extCharges = perPaxCreditExternalCharges.pop();
							}
							if (extCharges != null) {
								Collection<ExternalChgDTO> perPax = (Collection<ExternalChgDTO>) extCharges;
								for (ExternalChgDTO extCharge : perPax) {
									paymentConsumed = AccelAeroCalculator.add(paymentConsumed, extCharge.getAmount());
								}
								paymentAssembler.addExternalCharges(perPax);
							}

							paymentAssembler.addInternalCardPayment(cardPaymentInfo.getType(), cardPaymentInfo.getEDate().trim(),
									cardPaymentInfo.getNo().trim(), cardPaymentInfo.getName().trim(),
									cardPaymentInfo.getAddress().trim(), cardPaymentInfo.getSecurityCode().trim(),
									paxPaymentAmount, AppIndicatorEnum.APP_WS, TnxModeEnum.MAIL_TP_ORDER,
									ipgIdentificationParamsDTO, cardPayCurrencyDTO, null, null, null, null, null, null);

							lccClientBalancePayment.addPassengerPayments(traveler.getPaxSequence(), paymentAssembler);
						}
					}

					if (paymentConsumed.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
						log.error("Pax Consumed [" + paymentConsumed.doubleValue() + "] received ["
								+ reservationPayments.getTotalPayAmount().doubleValue() + "]),PNR=" + pnr);
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid payment amount");
					}

					WebServicesModuleUtils.getAirproxyReservationBD().balancePayment(lccClientBalancePayment,
							reservation.getVersion(), isGroupPNR, false, clientInfoDTO,
							modifyBookingUtil.getTrackInfo(userPrincipal), false, false);

					isSuccess = true;
					if (AuthorizationUtil.hasPrivileges(privilegeKeys,
							PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_BALANCE_PAYMENT)) {
						sendIternary(reservation);
					}

				} else {
					// TODO - implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Only agent onaccount payment supported");
				}
			} else {
				// TODO - implement for acceptiong multiple payment
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
						"Only single payment type in one request supported");
			}

			if (isSuccess) {
				ThreadLocalData.removeWSContextParam(WebservicesContext.BALANCE_QUERY_KEY);
			}

		} else {// partial payment or payment by multiple sources
			Map<Integer, IPayment> paxPayments = getPaxPaymentsForBalancePay(reservation,
					otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment(),
					otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments());
			boolean isForceConfirm = false;
			if (paxPayments.size() != reservation.getPassengers().size()) {
				isForceConfirm = true;
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_PARTIAL_PAYMNETS);
			}

			IPassenger passenger = new PassengerAssembler(null);
			for (Iterator<Integer> paxIdIt = paxPayments.keySet().iterator(); paxIdIt.hasNext();) {
				Integer pnrPaxId = paxIdIt.next();
				// authorize payments
				Collection<Integer> pnrPaxIdCollection = new ArrayList<Integer>();
				pnrPaxIdCollection.add(pnrPaxId);

				WSReservationUtil.authorizePayment(privilegeKeys, paxPayments.get(pnrPaxId), userPrincipal.getAgentCode(),
						pnrPaxIdCollection);

				passenger.addPassengerPayments(pnrPaxId, paxPayments.get(pnrPaxId));
			}
			// TODO: Call Airproxy method
			WebServicesModuleUtils.getReservationBD().updateReservationForPayment(reservation.getPNR(), passenger, isForceConfirm,
					false, Long.parseLong(reservation.getVersion()), null, false, false, true, false, true, false, null, false,
					null);
		}
	}

	/**
	 * Method to add user notes to reservation.
	 * 
	 * @param lccClientReservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void addUserNotes(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, ClientCommonInfoDTO clientInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {
		LCCClientReservation oldLccClientReservation = populateOldLccReservation(lccClientReservation);
		setUserNotes(lccClientReservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		TrackInfoDTO trackInfoDTO = null;
		if (!isGroupPNR) {
			trackInfoDTO = getTrackInforDTO(otaAirBookModifyRQ);
		}
		WebServicesModuleUtils.getAirproxyReservationBD().modifyPassengerInfo(lccClientReservation, oldLccClientReservation,
				isGroupPNR, clientInfoDTO, AppIndicatorEnum.APP_WS.toString(), false, false, null, trackInfoDTO);
	}

	/**
	 * Method to change the passenger name details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void nameChange(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, ClientCommonInfoDTO clientInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {

		LCCClientReservation oldLccClientReservation = populateOldLccReservation(lccClientReservation);
		setNameChangeDetails(lccClientReservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		TrackInfoDTO trackInfoDTO = null;
		if (!isGroupPNR) {
			trackInfoDTO = getTrackInforDTO(otaAirBookModifyRQ);
		}
		setFFIDChangeDetails(lccClientReservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		WebServicesModuleUtils.getAirproxyReservationBD().modifyPassengerInfo(lccClientReservation, oldLccClientReservation,
				isGroupPNR, clientInfoDTO, AppIndicatorEnum.APP_WS.toString(), false, false, null, trackInfoDTO);
	}

	/**
	 * 
	 * @param otaAirBookModifyRQ
	 * @return
	 */
	private TrackInfoDTO getTrackInforDTO(OTAAirBookModifyRQ otaAirBookModifyRQ) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		String directBillId = CommonServicesUtil
				.getDirectBillId((otaAirBookModifyRQ != null) ? otaAirBookModifyRQ.getAirBookModifyRQ() : null);
		trackInfoDTO.setDirectBillId(directBillId);
		return trackInfoDTO;
	}

	/**
	 * Method to update contact details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void updateContactDetails(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, TrackInfoDTO trackInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {

		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getContactInfo() != null)) {
			lccClientReservationContactInfo = ExtensionsUtil.getAAContactInfoFromWSRequest(aaAirBookModifyRQExt.getContactInfo());
		}

		WebServicesModuleUtils.getAirproxyReservationBD().modifyContactInfo(reservation.getPNR(), reservation.getVersion(),
				lccClientReservationContactInfo, reservation.getContactInfo(), isGroupPNR, AppIndicatorEnum.APP_WS.toString(),
				trackInfoDTO);
	}

	/**
	 * Cancel reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void cancelReservation(LCCClientReservation res, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfoDTO,
			boolean isGroupPNR) throws ModuleException, WebservicesException {
		String pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();

		if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(res.getStatus())) {
			LCCClientResAlterModesTO lccClientResAlterModesTO = LCCClientResAlterModesTO.composeCancelReservationRequest(pnr,
					null, null, null, res.getVersion(), null);
			lccClientResAlterModesTO.setGroupPnr(isGroupPNR);
			WebServicesModuleUtils.getAirproxyReservationBD().cancelReservation(lccClientResAlterModesTO, clientInfoDTO,
					trackInfoDTO, false, false);
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_, "");
		}
	}

	/**
	 * Method to remove passengers from the reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void removePassenger(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, TrackInfoDTO trackInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {

		List<AirTravelerType> listAirTraveller = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();
		Collection<Integer> pnrPaxIds = getPaxIds(listAirTraveller);

		if ((pnrPaxIds.size() > 0) && (listAirTraveller.size() == pnrPaxIds.size())) {
			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			List<LCCClientReservationPax> filteredLCCClientReservationPaxs = new ArrayList<LCCClientReservationPax>();
			for (LCCClientReservationPax lccClientReservationPax : reservation.getPassengers()) {
				for (Integer pnrPaxId : pnrPaxIds) {
					if (getPnrPaxId(lccClientReservationPax.getTravelerRefNumber()).equals(pnrPaxId)) {
						filteredLCCClientReservationPaxs.add(lccClientReservationPax);
						// TODO: Add CNX charges
					}
				}
			}
			WebServicesModuleUtils.getAirproxyPassengerBD().removePassenger(reservation.getPNR(), reservation.getVersion(),
					filteredLCCClientReservationPaxs, isGroupPNR, customChargesTO, trackInfoDTO, null, null);
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
					"Pax Ids extracted are not equal to the traveller count");
		}
	}

	/**
	 * Add Infant process
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws Exception
	 */
	public void addInfant(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, TrackInfoDTO trackInfoDTO, boolean isGroupPNR) throws Exception {

		Collection<AirTravelerType> travellers = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();
		// // for onhold or confirmed boooking, leave the same.
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

		// iterate the Ota to-moodify pax and add infant to the assembled passenger interface
		for (AirTravelerType airTravelerType : travellers) {
			if (airTravelerType.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				int[] infSeq_ParentSeq_ParentPnrPaxID = OTAUtils.getInfSeqParentSeqAndParentPnrPaxIDForAddInfant(airTravelerType);
				int infSeq = infSeq_ParentSeq_ParentPnrPaxID[0];
				int parentSeq = infSeq_ParentSeq_ParentPnrPaxID[1];
				int pnrPaxID = infSeq_ParentSeq_ParentPnrPaxID[2];

				LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();
				LCCClientReservationPax parent = null;
				Integer maxPaxSeq = 0;
				List<String> adultSeqId = new ArrayList<String>();

				for (LCCClientReservationPax adultPax : lccClientReservation.getPassengers()) {
					Integer adultPaxSeq = PaxTypeUtils.getPaxSeq(adultPax.getTravelerRefNumber());
					if (adultPaxSeq.compareTo(parentSeq) == 0) {
						parent = adultPax;
						adultSeqId.add(adultPaxSeq.toString());
					}
					// fix to get max sequence to add to infant
					if (adultPaxSeq.compareTo(Integer.valueOf(maxPaxSeq)) > 0) {
						maxPaxSeq = adultPaxSeq;
					}
				}

				if (parent != null) {
					lccClientReservationPax.setAttachedPaxId(parent.getPaxSequence());
					lccClientReservationPax
							.setFirstName(CommonUtil.getValueFromNullSafeList(airTravelerType.getPersonName().getGivenName(), 0));
					lccClientReservationPax.setLastName(airTravelerType.getPersonName().getSurname());
					lccClientReservationPax.setNationalityCode(OTAUtils.getNationalityCode(airTravelerType));
					lccClientReservationPax.setDateOfBirth(airTravelerType.getBirthDate() != null
							? airTravelerType.getBirthDate().toGregorianCalendar().getTime()
							: null);
					lccClientReservationPax.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
					int totalPaxCount = maxPaxSeq;
					lccClientReservationPax.setPaxSequence(totalPaxCount + 1);
					lccClientReservationPax.setParent(parent);
					parent.getInfants().add(lccClientReservationPax);
					lccClientReservation.addPassenger(lccClientReservationPax);
				}
			}

		}

		Map<Integer, LCCClientPaymentAssembler> payMap = new HashMap<Integer, LCCClientPaymentAssembler>();

		for (LCCClientReservationPax adultPax : lccClientReservation.getPassengers()) {
			if (!adultPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
				payMap.put(adultPax.getPaxSequence(), lccClientPaymentAssembler);
			}
		}

		WebServicesModuleUtils.getAirproxyPassengerBD().addInfant(lccClientReservation, intPaymentMode, isGroupPNR, payMap,
				AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT), false, false, null, trackInfoDTO,
				true, null);
	}

	/**
	 * Passenger Refund
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void passengerRefund(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, TrackInfoDTO trackInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {

		IPassenger iPassenger = new PassengerAssembler(null);
		Integer pnrPaxId = null;
		String userNotes = null;
		BigDecimal refundAmount = null;
		LCCClientReservationPax reservationPax = null;
		String carrierVisePayments = null;
		PaymentDetailType paymentDetail = null;

		PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();
		for (PaymentDetailType paymentDetailType : paymentDetails.getPaymentDetail()) {
			pnrPaxId = getPnrPaxId(paymentDetailType.getRPH());
			paymentDetail = paymentDetailType;
			refundAmount = PaymentsInterlineUtil.validateAndGetPaymentInfoForRefund(paymentDetailType);
			if ((pnrPaxId == null) || (refundAmount == null)) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Payment details required");
			}
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				if (pax.getTravelerRefNumber().equals(paymentDetailType.getRPH())) {
					reservationPax = pax;
				}
			}
		}

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getUserNotes() != null)) {
			userNotes = ExtensionsUtil.transformToAAUserNote(aaAirBookModifyRQExt.getUserNotes());
		}

		// TODO: Set correct payment date
		Date paymentdate = new Date();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(paymentdate);
		CommonReservationAssembler reservationAssembler = PaymentsInterlineUtil.getReservationAssembler(reservation.getVersion(),
				reservationPax, refundAmount, carrierVisePayments, exchangeRateProxy, paymentDetail);
		reservationAssembler.getLccreservation().setVersion(reservation.getVersion());

		// FIXME: Refund flow is not completed for web services
		WebServicesModuleUtils.getAirproxyPassengerBD().refundPassenger(reservationAssembler, userNotes, isGroupPNR, trackInfoDTO,
				null, null, false, false);
	}

	/**
	 * Cancel OND.
	 * 
	 * @param res
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void cancelOND(LCCClientReservation res, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO, String groupPNR)
			throws ModuleException, WebservicesException {

		List<Integer> pnrSegIdsForCNX = extractPNRSegIds(res,
				otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions());

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		authorizeModification(privilegeKeys, res, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX,
				pnrSegIdsForCNX);

		if (!checkIfSegsAlreadyCancelled(res, pnrSegIdsForCNX)) {
			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
			Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForCNX.size());
			for (Integer segId : pnrSegIdsForCNX)
				removedSegmentIds.add(segId.toString());
			requoteModifyRQ.setRemovedSegmentIds(removedSegmentIds);
			requoteModifyRQ.setPnr(res.getPNR());
			requoteModifyRQ.setCustomCharges(customChargesTO);
			requoteModifyRQ.setGroupPnr(groupPNR);
			requoteModifyRQ.setVersion(res.getVersion());
			OTAPaxCountTO otaPaxCountTO = getPaxCounts(res);

			PriceInfoTO priceInfoTO = (PriceInfoTO) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_INFO);

			if (priceInfoTO == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
						"Quoted price no longer available,Please search again");
			}

			FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
			String bookingType = BookingClass.BookingClassType.NORMAL;
			flightPriceRQ.getTravelPreferences().setBookingType(bookingType);
			TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

			PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
			adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
			adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

			PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
			childQuantity.setPassengerType(PaxTypeTO.CHILD);
			childQuantity.setQuantity(otaPaxCountTO.getChildCount());

			PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
			infantQuantity.setPassengerType(PaxTypeTO.INFANT);
			infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
			requoteModifyRQ.setFareInfo(fareInfo);

			requoteModifyRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
			requoteModifyRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());
			requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);
			AnalyticsLogger.logRequoteSearch(AnalyticSource.WS_CNX_REQUOTE, flightPriceRQ, ThreadLocalData
					.getCurrentUserPrincipal(), groupPNR, null);
			WebServicesModuleUtils.getAirproxyReservationBD().requoteModifySegmentsWithAutoRefund(requoteModifyRQ, trackInfoDTO);

		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SEGMENT_IS_ALREADY_CANCELLED_BY_ANOTHER_USER_,
					"");
		}
	}

	/**
	 * Adds OND to the reservation.
	 * 
	 * @param lccClientReservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 * 
	 */
	public void modifyOND(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO, String groupPNR)
			throws ModuleException, WebservicesException {

		AirReservationType airBookModRQ = otaAirBookModifyRQ.getAirBookModifyRQ();

		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "New OriginDestinationOptions required");
		}

		AirItineraryType.OriginDestinationOptions modifiedOND = otaAirBookModifyRQ.getAirReservation().getAirItinerary()
				.getOriginDestinationOptions();
		if (modifiedOND == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Modified OriginDestinationOptions required");
		}

		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
		PriceInfoTO priceInfoTO = (PriceInfoTO) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_INFO);

		if (quotedPriceInfoTO == null || priceInfoTO == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Quoted price no longer available,Please search again");
		}

		List<Integer> pnrSegIdsForMod = extractPNRSegIds(lccClientReservation, modifiedOND);

		Collection<String> privilegeKeys = ThreadLocalData
				.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		authorizeModification(privilegeKeys, lccClientReservation,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForMod);

		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;

		// checking insurance is selected and then do the validations and modify the segment
		Object insuranceObject = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED);
		if (insuranceObject != null) {
			Integer insuranceRefNo = Integer.parseInt((String) insuranceObject);
			// TODO: populate Insurance

		}

		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {
			pnrPaxIds.add(getPnrPaxId(reservationPax.getTravelerRefNumber()));
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
		Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForMod.size());
		for (Integer segId : pnrSegIdsForMod)
			removedSegmentIds.add(segId.toString());
		requoteModifyRQ.setRemovedSegmentIds(removedSegmentIds);
		requoteModifyRQ.setPnr(lccClientReservation.getPNR());
		requoteModifyRQ.setCustomCharges(customChargesTO);
		requoteModifyRQ.setGroupPnr(groupPNR);
		requoteModifyRQ.setVersion(lccClientReservation.getVersion());

		OTAPaxCountTO otaPaxCountTO = getPaxCounts(lccClientReservation);

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		flightPriceRQ.getTravelPreferences().setBookingType(quotedPriceInfoTO.getBookingType());
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(otaPaxCountTO.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

		@SuppressWarnings("unchecked")
		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		ReservationBalanceTO reservationBalanceTO = (ReservationBalanceTO) ThreadLocalData.getCurrentTnxParam(
				Transaction.REQUOTE_BALANCE_TO);

		// TODO check reservation balance segments with current actual modifying segments

		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = WSReservationUtil.getPassengerExtChgMap(quotedExternalCharges,
				lccClientReservation.getPassengers());

		// Extract payment information
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();
		if (airBookModRQ.getFulfillment() == null || airBookModRQ.getFulfillment().getPaymentDetails() == null) {

			// Add segment without payment
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
				// Force confirm
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
			} else {// OHD or CNX
				// Retain the OHD status
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);

				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

				// TODO: calculate and set new release timestamp

			}
		} else {

			PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();

			PaymentAssembler reservationPayments = new PaymentAssembler();
			OTAUtils.extractReservationPayments(paymentDetails.getPaymentDetail(), reservationPayments,
					userPrincipal.getAgentCurrencyCode(), exchangeRateProxy);

			WSReservationUtil.authorizePayment(privilegeKeys, reservationPayments, userPrincipal.getAgentCode(), pnrPaxIds);

			requoteModifyRQ.setLccPassengerPayments(
					WSReservationUtil.getPassengerPayments(reservationBalanceTO, lccClientReservation, reservationPayments,
							newOnds.getOriginDestinationOption().size(), paxExtChgMap, userPrincipal.getAgentCode()));
			requoteModifyRQ.setActualPayment(true);
		}

		requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);
		if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
			requoteModifyRQ.setExternalChargesMap(WSReservationUtil.getExternalChgMap(paxExtChgMap));
		}

		QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
		requoteModifyRQ.setFareInfo(fareInfo);
		requoteModifyRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
		requoteModifyRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());
		requoteModifyRQ.setPaymentType(intPaymentMode);

		WebServicesModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);

	}

	/**
	 * Adds OND to the reservation.
	 * 
	 * @param res
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	@SuppressWarnings("unchecked")
	public void addOND(LCCClientReservation res, OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO, String groupPNR)
			throws ModuleException, WebservicesException {

		AirReservationType airBookModRQ = otaAirBookModifyRQ.getAirBookModifyRQ();
		List<OriginDestinationOptionType> newPnrSegments = airBookModRQ.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption();

		boolean isGroundSegment = AddModifyGroundSegmentUtil.isSegContainGroundSeg(newPnrSegments);
		ISegment addedResSegs = null;
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;

		// checking insurance is selected and then do the validations and modify the segment
		Object insuranceObject = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED);
		if (insuranceObject != null) {
			Integer insuranceRefNo = Integer.parseInt((String) insuranceObject);
			// TODO: populate Insurance
			// InsuranceServiceUtil.validateInsuranceOnModification(res, fareSeatsAndSegDTOCol, insuranceRefNo);
			// InsuranceServiceUtil.populateInsurance(res, null, addedResSegs, insuranceRefNo, true);
		}

		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
		requoteModifyRQ.setPnr(res.getPNR());
		requoteModifyRQ.setCustomCharges(customChargesTO);
		requoteModifyRQ.setGroupPnr(groupPNR);
		requoteModifyRQ.setVersion(res.getVersion());

		OTAPaxCountTO otaPaxCountTO = getPaxCounts(res);

		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData.getCurrentTnxParam(
				Transaction.QUOTED_PRICE_INFO);
		PriceInfoTO priceInfoTO = (PriceInfoTO) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_INFO);

		if (quotedPriceInfoTO == null || priceInfoTO == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Quoted price no longer available,Please search again");
		}

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		flightPriceRQ.getTravelPreferences().setBookingType(quotedPriceInfoTO.getBookingType());
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(otaPaxCountTO.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (LCCClientReservationPax reservationPax : res.getPassengers()) {
			pnrPaxIds.add(getPnrPaxId(reservationPax.getTravelerRefNumber()));
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		ReservationBalanceTO reservationBalanceTO = (ReservationBalanceTO) ThreadLocalData.getCurrentTnxParam(
				Transaction.REQUOTE_BALANCE_TO);

		// TODO check reservation balance segments with current actual modifying segments

		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = WSReservationUtil.getPassengerExtChgMap(quotedExternalCharges,
				res.getPassengers());

		// Extract payment information
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();
		if (airBookModRQ.getFulfillment() == null || airBookModRQ.getFulfillment().getPaymentDetails() == null) {

			// Add segment without payment
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(res.getStatus())) {
				// Force confirm
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
			} else {// OHD or CNX
				// Retain the OHD status
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

				// TODO: calculate and set new release timestamp

			}
		} else {

			PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();

			PaymentAssembler reservationPayments = new PaymentAssembler();
			OTAUtils.extractReservationPayments(paymentDetails.getPaymentDetail(), reservationPayments,
					userPrincipal.getAgentCurrencyCode(), exchangeRateProxy);

			WSReservationUtil.authorizePayment(privilegeKeys, reservationPayments, userPrincipal.getAgentCode(), pnrPaxIds);
			for (PaymentDetailType paymentDetail : paymentDetails.getPaymentDetail()) {
				String WSPaymentMethod = paymentDetail.getDirectBill().getCompanyName().getCodeContext();
				String paymentMethod = null;
				if (Agent.PAYMENT_MODE_BSP.equals(WSPaymentMethod)) {
					paymentMethod = Agent.PAYMENT_MODE_BSP;
				} else if (WSPaymentMethod != null) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Payment type " + WSPaymentMethod + " not supported");
				} else {
					paymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
				}
				requoteModifyRQ.setLccPassengerPayments(WSReservationUtil.getPassengerPayments(reservationBalanceTO, res,
						reservationPayments, newPnrSegments.size(), paxExtChgMap, userPrincipal.getAgentCode()));
			}

		}

		requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);
		requoteModifyRQ.setExternalChargesMap(WSReservationUtil.getExternalChgMap(paxExtChgMap));

		QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
		requoteModifyRQ.setFareInfo(fareInfo);

		requoteModifyRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
		requoteModifyRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());
		requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);

		WebServicesModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);

	}

	/**
	 * Method to authorize segment modification according to the segment modification privileges and fare rule settings
	 * 
	 * @param newONDs
	 * @param modifiedONDs
	 * @param reservation
	 * @throws modCnxPnrSegIds
	 * @throws privilegeKeys
	 */
	public static void authorizeSegmentWiseModification(Collection<OndFareDTO> newONDFareDTOs,
			AirItineraryType.OriginDestinationOptions modifiedONDs, LCCClientReservation reservation, Collection modCnxPnrSegIds,
			Collection privilegeKeys) throws ModuleException, WebservicesException {
		// boolean isReturnTrip = false;
		List<FlightSegmentDTO> outBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		List<FlightSegmentDTO> inBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		for (OndFareDTO ondFareDTO : newONDFareDTOs) {
			if (ondFareDTO.isInBoundOnd()) {
				inBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			} else {
				outBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			}
		}

		Collections.sort(outBoundFlightSegments);
		Collections.sort(inBoundFlightSegments);

		Date journeyStartDate = outBoundFlightSegments.get(0).getDepartureDateTime();
		Date journeyEndDate = null;
		String journeyStartAirpport = outBoundFlightSegments.get(0).getFromAirport();
		String journeyEndAirport = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getToAirport();
		if (inBoundFlightSegments.size() > 0) {
			journeyEndDate = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getArrivalDateTime();
			// isReturnTrip = true;
			if (!journeyEndAirport.equals(inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport())) {
				journeyEndAirport = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport();
			}
		} else {
			journeyEndDate = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getArrivalDateTime();
		}

		String modifiedArrivalAirport = "";
		String modifedDepartureAirport = "";
		Date modifiedArrivalTime = null;
		Date modifiedDepartureTime = null;

		// getting modified onds and times
		for (OriginDestinationOptionType originDestinationOptionType : modifiedONDs.getOriginDestinationOption()) {
			for (BookFlightSegmentType bookFlightSegmentType : originDestinationOptionType.getFlightSegment()) {
				modifiedArrivalAirport = bookFlightSegmentType.getArrivalAirport().getLocationCode();
				modifedDepartureAirport = bookFlightSegmentType.getDepartureAirport().getLocationCode();
				modifiedArrivalTime = bookFlightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime();
				modifiedDepartureTime = bookFlightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime();
			}
		}

		// check route modification or date modification
		boolean isRouteModification = !((modifiedArrivalAirport + modifedDepartureAirport)
				.equals(journeyStartAirpport + journeyEndAirport));
		boolean isDateModification = false;
		if (journeyStartDate != null && modifiedDepartureTime != null) {
			if (!(journeyStartDate.equals(modifiedDepartureTime))) {
				isDateModification = true;
			}
		}
		/*
		 * //TODO: Uncomment this // getting fare rule segment modification statuses PNRModifyPreValidationStatesTO
		 * statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation, modCnxPnrSegIds);
		 * 
		 * // authorize segment modification according to the fare rule if (isRouteModification) { if
		 * (AuthorizationUtil.hasPrivilege(privilegeKeys,
		 * PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE)) { if (!statesTO.isAllowModifySegRoute())
		 * { // throw error throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
		 * "Segment OND cannot be modified according to the fare rule"); } } else { throw new
		 * WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
		 * "Unauthorized for OND modification in segments"); } } if (isDateModification) { if
		 * (AuthorizationUtil.hasPrivilege(privilegeKeys,
		 * PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE)) { if (!statesTO.isAllowModifySegDate()) {
		 * // throw error throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
		 * "Segment Dates cannot be modified according to the fare rule"); } } else { throw new
		 * WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
		 * "Unauthorized for date modification in segments"); } }
		 */
	}

	/**
	 * @return int [] {adultsCount, infantsCount, children}
	 * @throws WebservicesException
	 */
	private OTAPaxCountTO getPaxCounts(LCCClientReservation res) throws WebservicesException {
		int adults = 0;
		int children = 0;
		int infants = 0;

		LCCClientReservationPax reservationPax = null;
		for (Iterator iterPaxs = res.getPassengers().iterator(); iterPaxs.hasNext();) {
			reservationPax = (LCCClientReservationPax) iterPaxs.next();
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
				adults++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				children++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				infants++;
			}
		}
		return new OTAPaxCountTO(adults, children, infants);
	}

	/**
	 * Extracts pnrSegIds for cancellation/modification from the request.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	private static List<Integer> extractPNRSegIds(LCCClientReservation reservation,
			AirItineraryType.OriginDestinationOptions onds) throws WebservicesException {

		if (onds.getOriginDestinationOption() == null || onds.getOriginDestinationOption().size() != 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Only one OND at a time allowed to cancel/modify");
		}

		OriginDestinationOptionType ond = onds.getOriginDestinationOption().get(0);
		int ondGroupCode = -1;
		List<Integer> pnrSegIdsForCnx = new ArrayList<Integer>();
		for (BookFlightSegmentType wsFlightSeg : ond.getFlightSegment()) {
			if (reservation.getSegments() != null) {
				for (LCCClientReservationSegment resSegment : reservation.getSegments()) {
					Integer pnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(wsFlightSeg.getRPH());
					if (resSegment.getPnrSegID().equals(pnrSegId)
							&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSegment.getStatus())) {
						pnrSegIdsForCnx.add(resSegment.getPnrSegID());
						if (ondGroupCode == -1) {
							ondGroupCode = resSegment.getJourneySequence();
						} else if (ondGroupCode != resSegment.getJourneySequence()) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Only one OND at a time allowed to cancel");
						}
						break;
					}
				}
			}
		}
		return pnrSegIdsForCnx;
	}

	/**
	 * Returns true if at least one of the provided pnrSegId is already cancelled.
	 * 
	 * @param res
	 * @param pnrSegIds
	 * @return
	 */
	private boolean checkIfSegsAlreadyCancelled(LCCClientReservation res, List<Integer> pnrSegIds) {
		boolean alreadyCnx = false;
		for (Iterator<LCCClientReservationSegment> resSegIt = res.getSegments().iterator(); resSegIt.hasNext();) {
			LCCClientReservationSegment resSeg = resSegIt.next();
			if (pnrSegIds.contains(resSeg.getPnrSegID())
					&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
				alreadyCnx = true;
				break;
			}
		}
		return alreadyCnx;
	}

	private LCCClientReservation populateOldLccReservation(LCCClientReservation lccClientReservation)
			throws ModuleException, WebservicesException {
		LCCClientReservation oldLccClientReservation = new LCCClientReservation();
		oldLccClientReservation.setPNR(lccClientReservation.getPNR());
		oldLccClientReservation.setLastUserNote(lccClientReservation.getLastUserNote());
		oldLccClientReservation.setVersion(lccClientReservation.getVersion());
		oldLccClientReservation.setItineraryFareMask(lccClientReservation.getItineraryFareMask());
		Collection<LCCClientReservationPax> colpaxs = lccClientReservation.getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : colpaxs) {
			oldLccClientReservation.addPassenger(lccClientReservationPax);
		}
		return oldLccClientReservation;
	}

	/**
	 * Throws WebserviceExcetion if the user is not authorized to perform requested operation based on the time to
	 * departure/already flown constraints.
	 * 
	 * @param reservation
	 * @param bufferModPrivilege
	 * @return
	 * @throws WebservicesException
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static void authorizeModification(Collection privilegeKeys, LCCClientReservation reservation,
			String bufferModPrivilege, Collection modCnxPnrSegIds) throws WebservicesException, ModuleException {

		PNRModifyPreValidationStatesTO statesTO = getBasicReservationOrSegStatuses(reservation, modCnxPnrSegIds);
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		// // Interline bookings modifiable or not
		// if (!statesTO.isInterlinedModifiable()) {
		// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
		// "Interlined Reservations cannot be modified once transfered");
		// }

		// Reservation / Segment cancelled or not
		if (statesTO.isCancelled()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Already cancelled");
		}

		// Confirmed Reservation / Segment having authorization to cancel
		if (!statesTO.isCancelled()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Cancelled Reservations are not authorized to modify");
		}

		// Restricted Segment
		if (statesTO.isRestricted()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Restricted Segments are not authorized to modify");
		}

		// Reservation / Segment allow modifying
		if (statesTO.isFlown()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Reservations having flown segment(s) are not authorized to modify");
		}

		if (bufferModPrivilege != null && !"".equals(bufferModPrivilege)) {
			if (statesTO.isInBufferTime() && !AuthorizationUtil.hasPrivilege(privilegeKeys, bufferModPrivilege)) {
				log.warn(
						"Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + bufferModPrivilege + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
						"Not allowed within modification buffer duration");
			}
		}
	}

	/**
	 * Returns the basic pre validation states
	 * 
	 * @param reservation
	 * @param modCnxPnrSegIds
	 * @param statesTO
	 * @return
	 * @throws ModuleException
	 */
	private static PNRModifyPreValidationStatesTO getBasicReservationOrSegStatuses(LCCClientReservation reservation,
			Collection<Integer> modCnxPnrSegIds) throws ModuleException {

		Collection<LCCClientReservationSegment> resSegs = reservation.getSegments();
		PNRModifyPreValidationStatesTO statesTO = new PNRModifyPreValidationStatesTO();

		boolean isInBufferTime = false;
		boolean hasFlownSegs = false;
		boolean isCancelledRes = ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus());
		boolean hasCancelledSegs = isCancelledRes;
		boolean isSegStatusesCheck = (modCnxPnrSegIds != null);
		boolean isRestrictedSegs = false;
		boolean hasFareRuleBuffer = false;
		boolean hasNOSHOWPax = false;
		boolean isSpecialFare = false;
		boolean allowModifyDate = false;
		boolean allowModifyRoute = false;
		if (!isCancelledRes) {
			Date depDateZulu = null;
			Date currentDateZulu = CalendarUtil.getCurrentSystemTimeInZulu();
			for (LCCClientReservationSegment resSeg : resSegs) {
				if ((isSegStatusesCheck && modCnxPnrSegIds.contains(resSeg.getPnrSegID())) || !isSegStatusesCheck) {
					if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
						depDateZulu = resSeg.getDepartureDateZulu();
						Calendar flightClosureTimeZulu = new GregorianCalendar();
						flightClosureTimeZulu.setTime(depDateZulu);
						flightClosureTimeZulu.add(Calendar.MINUTE, -AppSysParamsUtil.getFlightClosureGapInMins());

						if (flightClosureTimeZulu.getTime().before(currentDateZulu)) {
							hasFlownSegs = true;
							hasNOSHOWPax = resSeg.isAllPaxNoShow();
							// if (resSeg.getModifyTillBufferDateTime()) > 0) {
							// isSpecialFare = true;
							// }
						} else {
							long timeToDeparture = depDateZulu.getTime() - currentDateZulu.getTime();

							// ApplicableBufferTimeDTO applicableBufferDTO =
							// FlightUtil.getApplicableModifyBufferTime(resSeg
							// .getFareTO().getInternationalModifyBufferTimeInMillis(), resSeg.getFareTO()
							// .getDomesticModifyBufferTimeInMillis(), resSeg.getFlightSegmentRefNumber());

							ApplicableBufferTimeDTO applicableBufferDTO = FlightUtil.getApplicableModifyBufferTime(0, 0, null);
							long modifyBufferTime = applicableBufferDTO.getApplicableBufferTime();
							hasFareRuleBuffer = applicableBufferDTO.isFareRuleBufferUsed();

							// long flexiModifyCutOverBufferInMillis = ((Long)
							// ReservationApiUtils.getFlexiModifyCutOverBuffer(
							// reservation, modCnxPnrSegIds)[0]).longValue();
							// if (flexiModifyCutOverBufferInMillis != -1 && flexiModifyCutOverBufferInMillis <
							// modifyBufferTime) {
							// modifyBufferTime = flexiModifyCutOverBufferInMillis;
							// }

							if (timeToDeparture <= modifyBufferTime) {
								isInBufferTime = true;
							}
						}
					} else {
						if (isSegStatusesCheck && modCnxPnrSegIds.contains(resSeg.getPnrSegID())) {
							hasCancelledSegs = true;
						}
					}

					// if (resSeg.getFareTO().isFareRestricted()) {
					// isRestrictedSegs = true;
					// }

					// if (resSeg.getFareTO().getIsAllowModifyDate()) {
					// allowModifyDate = true;
					// }
					// if (resSeg.getFareTO().getIsAllowModifyRoute()) {
					// allowModifyRoute = true;
					// }
					if (resSeg.isModifible()) {
						allowModifyDate = true;
						allowModifyRoute = true;
					}
				}
			}
		}

		statesTO.setCancelled((isCancelledRes || hasCancelledSegs));
		statesTO.setRestricted(isRestrictedSegs);
		statesTO.setFlown(hasFlownSegs);
		statesTO.setInBufferTime(isInBufferTime);
		statesTO.setHasFareRuleBuffer(hasFareRuleBuffer);
		statesTO.setHasNOSHOWPax(hasNOSHOWPax);
		statesTO.setSpecialFare(isSpecialFare);
		statesTO.setAllowModifySegDate(allowModifyDate);
		statesTO.setAllowModifySegRoute(allowModifyRoute);
		return statesTO;
	}

	/**
	 * Method to set the user notes to reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setUserNotes(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {
		String userNote = "";

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getUserNotes() != null)) {
			userNote = ExtensionsUtil.transformToAAUserNote(aaAirBookModifyRQExt.getUserNotes());
		}

		if (!userNote.trim().equals("")) {
			reservation.setLastUserNote(userNote);
		}
	}

	private CommonReservationContactInfo getContactInfo(CommonReservationContactInfo contactInfo) {

		CommonReservationContactInfo ccontactInfo = new CommonReservationContactInfo();

		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setFirstName(contactInfo.getFirstName());
		ccontactInfo.setLastName(contactInfo.getLastName());
		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setCity(contactInfo.getCity());
		ccontactInfo.setCountryCode(contactInfo.getCountryCode());
		ccontactInfo.setCustomerId(contactInfo.getCustomerId());
		ccontactInfo.setEmail(contactInfo.getEmail());
		ccontactInfo.setState(contactInfo.getState());
		ccontactInfo.setFax(contactInfo.getFax());
		ccontactInfo.setMobileNo(contactInfo.getMobileNo());
		ccontactInfo.setPhoneNo(contactInfo.getPhoneNo());
		if (contactInfo.getNationalityCode() != null) {
			ccontactInfo.setNationalityCode(contactInfo.getNationalityCode());
		}
		ccontactInfo.setStreetAddress1(contactInfo.getStreetAddress1());
		ccontactInfo.setStreetAddress2(contactInfo.getStreetAddress2());
		ccontactInfo.setPreferredLanguage(contactInfo.getPreferredLanguage());
		ccontactInfo.setZipCode(contactInfo.getZipCode());

		ccontactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		ccontactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		ccontactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		ccontactInfo.setEmgnPhoneNo(contactInfo.getEmgnPhoneNo());
		ccontactInfo.setEmgnEmail(contactInfo.getEmgnEmail());

		return ccontactInfo;
	}

	/**
	 * Extracts pax wise payments.
	 * 
	 * TODO - validate passenger credit eligibility.
	 * 
	 * @param reservation
	 * @param fulfillment
	 * @param travelersFulfillments
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 * @throws NumberFormatException
	 */
	private Map<Integer, IPayment> getPaxPaymentsForBalancePay(LCCClientReservation reservation,
			AirReservationType.Fulfillment fulfillment, TravelersFulfillments travelersFulfillments)
			throws WebservicesException, NumberFormatException, ModuleException {
		Map<Integer, IPayment> aaPaxPaymentMap = new HashMap<Integer, IPayment>();

		// Putting passengers into a map to optimize looking up
		Map<Integer, ReservationPax> aaPaxMap = new HashMap<Integer, ReservationPax>();
		for (Iterator it = reservation.getPassengers().iterator(); it.hasNext();) {
			ReservationPax pax = (ReservationPax) it.next();
			aaPaxMap.put(pax.getPnrPaxId(), pax);
		}

		BigDecimal expectedPaxPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPaxPayments = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<Integer, BigDecimal[]> expectedPaxesPayments = new HashMap<Integer, BigDecimal[]>();
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		for (TravelersFulfillments.TravelerFulfillments travelerPayments : travelersFulfillments.getTravelerFulfillments()) {
			Integer pnrPaxId = Integer.parseInt(OTAUtils.getPNRPaxIdFromTravellerRPH(travelerPayments.getTravelerRefNumberRPH()));
			ReservationPax aaPax = aaPaxMap.get(pnrPaxId);
			expectedPaxPayment = aaPax.getTotalAvailableBalance().doubleValue() > 0
					? aaPax.getTotalAvailableBalance()
					: AccelAeroCalculator.getDefaultBigDecimalZero();

			PaymentAssembler paxPayment = null;
			if (ReservationApiUtils.isAdultType(aaPax) || ReservationApiUtils.isChildType(aaPax)) {
				if (aaPaxPaymentMap.containsKey(pnrPaxId)) {
					paxPayment = (PaymentAssembler) aaPaxPaymentMap.get(pnrPaxId);
					expectedPaxesPayments.remove(pnrPaxId);
				} else {
					paxPayment = new PaymentAssembler();
				}
				OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
						principal.getAgentCurrencyCode(), exchangeRateProxy);

				// Infant payment is provided separately in WS payment request
				if (aaPax.getInfants() != null && aaPax.getInfants().size() > 0) {
					ReservationPax infantPax = aaPax.getInfants().iterator().next();

					for (TravelersFulfillments.TravelerFulfillments innerTravelerPayments : travelersFulfillments
							.getTravelerFulfillments()) {
						Integer innerPnrPaxId = Integer
								.parseInt(OTAUtils.getPNRPaxIdFromTravellerRPH(innerTravelerPayments.getTravelerRefNumberRPH()));
						if (innerPnrPaxId.equals(infantPax.getPnrPaxId())) {
							OTAUtils.extractReservationPayments(innerTravelerPayments.getPaymentDetail(), paxPayment,
									principal.getAgentCurrencyCode(), exchangeRateProxy);
							break;
						}
					}
				}

				ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

				expectedPaxesPayments.put(pnrPaxId, new BigDecimal[] { expectedPaxPayment, paxPayment.getTotalPayAmount() });

				aaPaxPaymentMap.put(pnrPaxId, paxPayment);
			}
		}

		// Validate individual pax payments
		for (Iterator<Integer> paxIdIt = expectedPaxesPayments.keySet().iterator(); paxIdIt.hasNext();) {
			BigDecimal[] expectedPayments = expectedPaxesPayments.get(paxIdIt.next());
			if (log.isDebugEnabled()) {
				log.debug(expectedPayments[0] + " " + expectedPayments[1]);
			}

			if (expectedPayments[0].compareTo(expectedPayments[1]) != 0) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
						"Partial payment for a traveler not allowed");
			}
			totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, expectedPayments[1]);
		}

		// Check if total payment equals sum of individual pax payments
		PaymentAssembler reservationPayments = new PaymentAssembler();
		OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), reservationPayments,
				principal.getAgentCurrencyCode(), exchangeRateProxy);

		if (reservationPayments.getTotalPayAmount().doubleValue() != totalPaxPayments.doubleValue()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Traveler wise payment does not match total payment");
		}

		return aaPaxPaymentMap;
	}

	/**
	 * Method to update the passenger ssr details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void updateSSR(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		setSSRDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		// TODO : Complete
		// WebServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false);
	}

	/**
	 * Method to set the updated passenger ssr details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private void setSSRDetails(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		Set setPassenger = null;
		Iterator iterPassenger = null;
		LCCClientReservationPax resPax = null;
		Integer pnrPaxId = null;
		String ssrCode = null;
		String ssrRemarks = null;
		boolean isSSRValid = false;

		List<SpecialReqDetailsType> specialRequestDetailType = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo()
				.getSpecialReqDetails();

		for (SpecialReqDetailsType specialReqType : specialRequestDetailType) {
			for (SpecialServiceRequest specialReq : specialReqType.getSpecialServiceRequests().getSpecialServiceRequest()) {

				isSSRValid = false;

				ssrCode = specialReq.getSSRCode();
				if ((ssrCode != null) && (!ssrCode.trim().equals(""))) {
					isSSRValid = WebServicesModuleUtils.getWebServiceDAO().isValidSSR(ssrCode);
				}

				if (!isSSRValid) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"SSR code " + ssrCode + " is invalid");
				}

				Integer ssrId = SSRUtil.getSSRId(ssrCode);

				if (SSRDetailsUtil.isChargableSSR(ssrId)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"SSR code [" + ssrCode + "] is a chargable SSR & not supported as non-chargable SSR.");
				}

				ssrRemarks = specialReq.getText();
				for (String travellerRPH : specialReq.getTravelerRefNumberRPHList()) {
					// pnrPaxId = new Integer(OTAUtils.getPNRPaxIdFromTravellerRPH(travellerRPH));

					setPassenger = reservation.getPassengers();
					if (setPassenger != null) {
						iterPassenger = setPassenger.iterator();
						resPax = null;

						while (iterPassenger.hasNext()) {
							resPax = (LCCClientReservationPax) iterPassenger.next();
							if (resPax.getTravelerRefNumber().equals(travellerRPH)) {
								SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();

								segmentSSRs.addPaxSegmentSSR(null, SSRUtil.getSSRId(ssrCode), ssrRemarks);
								// TODO: Write this method for LCC
								// ReservationSSRUtil.updatePaxSegmentSSRs(resPax, segmentSSRs, false);
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Method to update the reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void updateReservation(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, ClientCommonInfoDTO clientInfoDTO, boolean isGroupPNR)
			throws ModuleException, WebservicesException {

		LCCClientReservation oldLccClientReservation = populateOldLccReservation(reservation);

		setSSRDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setNameChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setFFIDChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setUserNotes(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		TrackInfoDTO trackInfoDTO = null;
		if (!isGroupPNR) {
			trackInfoDTO = getTrackInforDTO(otaAirBookModifyRQ);
		}

		WebServicesModuleUtils.getAirproxyReservationBD().modifyPassengerInfo(reservation, oldLccClientReservation, isGroupPNR,
				clientInfoDTO, AppIndicatorEnum.APP_WS.toString(), false, false, null, trackInfoDTO);
	}

	/**
	 * Extend onhold reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void extendOnholdReservation(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfoDTO,
			boolean isGroupPNR) throws ModuleException, WebservicesException {

		Date newReleaseTimeStamp = null;

		if ((!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus()))
				|| (otaAirBookModifyRQ.getAirBookModifyRQ().getTicketing().get(0).getTicketTimeLimit() == null)) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_MODIFICATION_DATA_IS_INVALID_,
					"Reservation is not onhold or the extend onhold timestamp is invalid");
		} else {

			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(
					CommonUtil.getTime(otaAirBookModifyRQ.getAirBookModifyRQ().getTicketing().get(0).getTicketTimeLimit()));
			newReleaseTimeStamp = cal.getTime();

			if (newReleaseTimeStamp.before(CalendarUtil.getCurrentSystemTimeInZulu())) {// Release time cannot be a past
																						// time
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_NOT_ELIGIBLE_FOR_HOLD,
						"Reservation can not extend onhold for expired segment(s)");
			}

			WebServicesModuleUtils.getAirproxyReservationBD().extendOnHold(reservation.getPNR(), newReleaseTimeStamp,
					reservation.getVersion(), isGroupPNR, clientInfoDTO, trackInfoDTO);
		}
	}

	/**
	 * Method to split a set of passengers from a reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public Object splitReservation(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			TrackInfoDTO trackInfoDTO, boolean isGroupPNR) throws ModuleException, WebservicesException {

		List<AirTravelerType> listAirTraveller = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();

		Collection<Integer> pnrPaxIds = getPaxIds(listAirTraveller);
		Set<LCCClientReservationPax> paxs = new HashSet<LCCClientReservationPax>();
		for (LCCClientReservationPax reservationPax : reservation.getPassengers()) {
			for (Integer pnrPaxId : pnrPaxIds) {
				if (pnrPaxId.equals(getPnrPaxId(reservationPax.getTravelerRefNumber()))) {
					paxs.add(reservationPax);
				}
			}
		}
		reservation.getPassengers().clear();
		reservation.getPassengers().addAll(paxs);

		if ((pnrPaxIds.size() > 0) && (listAirTraveller.size() == pnrPaxIds.size())) {
			LCCClientReservation clientReservation = WebServicesModuleUtils.getAirproxyReservationBD()
					.splitReservation(reservation, isGroupPNR, trackInfoDTO);
			return clientReservation.getPNR();
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
					"Pax Ids extracted are not equal to the traveller count");
		}
	}

	private Integer getPnrPaxId(String lccTravelerReferenceNumber) {
		int pnrPaxIdStartIndex = lccTravelerReferenceNumber.lastIndexOf("$");
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = lccTravelerReferenceNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

	/**
	 * Method to get the paxIds from the list of AirTraveller
	 * 
	 * @param listAirTraveller
	 * @return
	 */
	private Collection<Integer> getPaxIds(List<AirTravelerType> listAirTraveller) {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();

		String pnrPaxId = null;
		for (AirTravelerType airTraveller : listAirTraveller) {
			pnrPaxId = OTAUtils.getPNRPaxIdFromTravellerRPH(airTraveller.getTravelerRefNumber().getRPH());
			if ((pnrPaxId != null) && (!pnrPaxId.trim().equals(""))) {
				pnrPaxIds.add(new Integer(pnrPaxId));
			} else {
				break;
			}
		}

		return pnrPaxIds;
	}

	/**
	 * Method to set the updated passenger name details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private void setNameChangeDetails(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		LCCClientReservationPax resPax = null;
		Nationality nationality = null;
		Set setPassenger = null;
		Iterator iterPassenger = null;
		Integer pnrPaxId = null;
		Integer nationalityCode = null;
		String paxTypeCode = null;
		String title = null;
		String firstName = null;
		String lastName = null;
		String foidPaxTypeCode = null;
		String foidNumber = null;
		PaxCategoryFoidTO paxCategoryFOIDTO = null;

		String bookingCategory = reservation.getBookingCategory();

		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		boolean skipValidation = AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());

		for (AirTravelerType traveler : otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler()) {

			// pnrPaxId = new Integer(OTAUtils.getPNRPaxIdFromTravellerRPH(traveler.getTravelerRefNumber().getRPH()));

			setPassenger = reservation.getPassengers();

			if (setPassenger != null) {
				iterPassenger = setPassenger.iterator();
				resPax = null;
				nationality = null;
				String employeeId = "";
				Date dateOfJoin = null;
				String idCategory = "";

				while (iterPassenger.hasNext()) {
					resPax = (LCCClientReservationPax) iterPassenger.next();

					if (resPax.getTravelerRefNumber().equals(traveler.getTravelerRefNumber().getRPH())) {
						paxTypeCode = traveler.getPassengerTypeCode();

						foidNumber = null;
						if (traveler.getDocument().size() > 0) {
							for (Document document : traveler.getDocument()) {

								if (CommonsConstants.SSRCode.PSPT.toString().equals(document.getDocType())) {
									if (document.getDocHolderNationality() != null) {
										nationality = WebServicesModuleUtils.getCommonMasterBD()
												.getNationality(document.getDocHolderNationality());
									}

									if (document.getDocID() != null && document.getDocType() != null) {
										foidNumber = document.getDocID();
									}

								} else if (CommonsConstants.SSRCode.ID.toString().equals(document.getDocType())
										&& BookingCategory.HR.getCode().equals(bookingCategory)) {
									if (document.getDocID() != null && document.getDocType() != null) {
										employeeId = document.getDocID();
										dateOfJoin = document.getEffectiveDate() != null
												? document.getEffectiveDate().toGregorianCalendar().getTime()
												: null;
										idCategory = document.getDocIssueAuthority();
									}
								}

								if (nationality == null) {
									if (document.getDocHolderNationality() != null) {
										nationality = WebServicesModuleUtils.getCommonMasterBD()
												.getNationality(document.getDocHolderNationality());
									}
								}

							}
						}

						if (nationality != null) {
							nationalityCode = nationality.getNationalityCode();
						}

						boolean isDomesticSegmentExist = false;
						if (quotedPriceInfoTO != null) {
							isDomesticSegmentExist = OTAUtils.isDomesticSegmentExists(quotedPriceInfoTO.getQuotedSegments());
						}
						String paxFfid = null;
						String oldPaxFfid = null;
						// validate loyalty information
						if (AppSysParamsUtil.isLMSEnabled() && traveler.getCustLoyalty() != null
								&& !traveler.getCustLoyalty().isEmpty() && traveler.getCustLoyalty().get(0) != null) {
							paxFfid = traveler.getCustLoyalty().get(0).getMembershipID();
						}

						if (resPax.getLccClientAdditionPax() != null) {
							oldPaxFfid = resPax.getLccClientAdditionPax().getFfid();
						}

						if (oldPaxFfid != null) {
							if (!oldPaxFfid.equals(paxFfid)) {
								Collection<String> privilegeKeys = ThreadLocalData
										.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
								WSReservationUtil.authorize(privilegeKeys, null,
										PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_EDIT_FFID);
							}
						}
						// Pax validation
						if (paxTypeCode.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {

							OTAUtils.validatePaxDetails(
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.ADULT, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, null);

						} else if (paxTypeCode
								.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

							OTAUtils.validatePaxDetails(
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.CHILD, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, null);

						} else if (paxTypeCode
								.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {

							OTAUtils.validatePaxDetails(null,
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.INFANT, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, null);
						} else {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Pax Type Code");
						}

						resPax.setTitle(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0));
						resPax.setFirstName(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0));
						resPax.setLastName(traveler.getPersonName().getSurname());
						resPax.setNationalityCode(nationalityCode);

						LCCClientReservationAdditionalPax addnInfo = null;
						if (resPax.getLccClientAdditionPax() == null) {
							addnInfo = new LCCClientReservationAdditionalPax();
						} else {
							addnInfo = resPax.getLccClientAdditionPax();
						}

						addnInfo.setPassportNo(foidNumber);
						resPax.setLccClientAdditionPax(addnInfo);
						break;
					}
				}
			}
		}
	}

	/**
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setFFIDChangeDetails(LCCClientReservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		LCCClientReservationPax resPax = null;
		Set<LCCClientReservationPax> setPassenger = null;
		Iterator<LCCClientReservationPax> iterPassenger = null;
		String paxTypeCode = null;

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		for (AirTravelerType traveler : otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler()) {

			setPassenger = reservation.getPassengers();

			if (setPassenger != null) {
				iterPassenger = setPassenger.iterator();
				resPax = null;
				while (iterPassenger.hasNext()) {
					resPax = iterPassenger.next();

					if (resPax.getTravelerRefNumber().equals(traveler.getTravelerRefNumber().getRPH())) {
						paxTypeCode = traveler.getPassengerTypeCode();
						String paxFfid = null;
						String oldPaxFfid = null;
						// validate loyalty information
						if (AppSysParamsUtil.isLMSEnabled()) {
							if (resPax.getLccClientAdditionPax() != null) {
								oldPaxFfid = resPax.getLccClientAdditionPax().getFfid();
							}

							if (!traveler.getCustLoyalty().isEmpty()) {
								List<CustLoyalty> custLoyalty = traveler.getCustLoyalty();
								paxFfid = custLoyalty.iterator().next().getMembershipID();
							}

							if (oldPaxFfid != null) {
								if (!oldPaxFfid.equals(paxFfid)) {
									WSReservationUtil.authorize(privilegeKeys, null,
											PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_EDIT_FFID);
								}
							}
							// Pax validation
							if (paxFfid != null) {
								if (paxTypeCode.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
										|| paxTypeCode.equals(
												CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

									OTAUtils.validateLoyaltyInformation(paxFfid,
											CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
											traveler.getPersonName().getSurname());

								} else if (paxTypeCode
										.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
									paxFfid = null;
								}
							}

							LCCClientReservationAdditionalPax addnInfo = null;
							if (resPax.getLccClientAdditionPax() == null) {
								addnInfo = new LCCClientReservationAdditionalPax();
							} else {
								addnInfo = resPax.getLccClientAdditionPax();
							}

							addnInfo.setFfid(paxFfid);
							resPax.setLccClientAdditionPax(addnInfo);

							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Get balances for cancelling specified OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param privilegeKeys
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public OTAAirBookRS getBalancesForCancelOND(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			TrackInfoDTO trackInfoDTO, String groupPNR, Collection privilegeKeys) throws WebservicesException, ModuleException {
		if (otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions() == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "OriginDestinationOptions required");
		}

		validateCancelSegmentRQ(otaAirBookModifyRQ, lccClientReservation);

		// get the pnrSegIds after checking the validity
		List<Integer> pnrSegIdsForCnx = extractPNRSegIds(lccClientReservation,
				otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions());

		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
		Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForCnx.size());
		for (Integer segId : pnrSegIdsForCnx)
			removedSegmentIds.add(segId.toString());
		balanceQueryRQ.setRemovedSegmentIds(removedSegmentIds);
		balanceQueryRQ.setPnr(lccClientReservation.getPNR());
		balanceQueryRQ.setCustomCharges(customChargesTO);
		balanceQueryRQ.setGroupPnr(groupPNR);
		balanceQueryRQ.setVersion(lccClientReservation.getVersion());

		int[] paxCounts = { lccClientReservation.getTotalPaxAdultCount(), lccClientReservation.getTotalPaxChildCount(),
				lccClientReservation.getTotalPaxInfantCount() };

		WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT);

		ModifyBookingInterlineUtil.authorizeModification(privilegeKeys, lccClientReservation,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForCnx);

		Map<Integer, List<LCCClientReservationSegment>> allConfirmedSegmentsMap = getExistingConfirmedSegments(
				lccClientReservation);

		validatePresenceOfOnlyGroundSegments(allConfirmedSegmentsMap, pnrSegIdsForCnx);

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		createFlightPriceRQ(flightPriceRQ, allConfirmedSegmentsMap, pnrSegIdsForCnx, paxCounts);

		FlightPriceRS priceResponse = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ, null);

		if (priceResponse == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
					"Valid fare not available for the requested cancellation. Please contact airline to cancel");
		}

		OTAPaxCountTO otaPaxCountTO = getPaxCounts(lccClientReservation);

		PriceInfoTO priceInfoTO = priceResponse.getSelectedPriceFlightInfo();

		ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_INFO, priceInfoTO);

		@SuppressWarnings("unchecked")
		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = WSReservationUtil.getPassengerExtChgMap(quotedExternalCharges,
				lccClientReservation.getPassengers());

		balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);

		QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
		balanceQueryRQ.setFareInfo(fareInfo);
		balanceQueryRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
		balanceQueryRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());

		ReservationBalanceTO reservationBalanceTO = WebServicesModuleUtils.getAirproxyReservationBD()
				.getRequoteBalanceSummary(balanceQueryRQ, trackInfoDTO);

		ExtensionsInterlineUtil.clearRequoteBalanceTO();
		AAAlterationBalancesType aaResCnxBalances = ExtensionsInterlineUtil.prepareWSResAlterationBalances(
				lccClientReservation.getPassengers(), reservationBalanceTO, ReservationConstants.AlterationType.ALT_CANCEL_OND,
				0);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	private void validatePresenceOfOnlyGroundSegments(Map<Integer, List<LCCClientReservationSegment>> allConfirmedSegmentsMap,
			List<Integer> pnrSegIdsForCnx) throws WebservicesException, ModuleException {

		if (allConfirmedSegmentsMap != null && !allConfirmedSegmentsMap.isEmpty()) {
			FlightSearchDTO searchDTO = null;
			List<FlightSearchDTO> searchDTOs = new ArrayList<FlightSearchDTO>();
			for (Integer ondSequence : allConfirmedSegmentsMap.keySet()) {
				List<LCCClientReservationSegment> confirmedSegments = allConfirmedSegmentsMap.get(ondSequence);
				for (LCCClientReservationSegment confirmedSegment : confirmedSegments) {
					Integer existingPNRSegId = confirmedSegment.getPnrSegID();
					if (!pnrSegIdsForCnx.contains(existingPNRSegId)) {
						String segmentCode = confirmedSegment.getSegmentCode();
						if (segmentCode != null) {
							String segArray[] = segmentCode.split("/");
							searchDTO = new FlightSearchDTO();
							searchDTO.setFromAirport(segArray[0]);
							searchDTO.setToAirport(segArray[segArray.length - 1]);
							searchDTOs.add(searchDTO);
						}
					}
				}
			}

			boolean onlyGroundSegments = AddModifySurfaceSegmentValidations.isOnlyGroundSegments(searchDTOs);
			if (onlyGroundSegments) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Cannot leave the reservation only with bus segments");
			}

		}

	}

	private void createFlightPriceRQ(FlightPriceRQ flightPriceRQ,
			Map<Integer, List<LCCClientReservationSegment>> allConfirmedSegmentsMap, List<Integer> pnrSegIdsForCNX,
			int[] paxCounts) {

		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		for (Integer ondSequence : allConfirmedSegmentsMap.keySet()) {
			List<LCCClientReservationSegment> ondSegList = allConfirmedSegmentsMap.get(ondSequence);
			Collections.sort(ondSegList);
			if (ondSegList.size() == 1) {
				LCCClientReservationSegment lccClientReservationSegment = ondSegList.get(0);
				if (!pnrSegIdsForCNX.contains(lccClientReservationSegment.getPnrSegID())) {
					depatureOnD.setOrigin(lccClientReservationSegment.getDepartureAirportName());
					depatureOnD.setDestination(lccClientReservationSegment.getArrivalAirportName());
					depatureOnD.setPreferredClassOfService(lccClientReservationSegment.getCabinClassCode());
					depatureOnD.setPreferredLogicalCabin(lccClientReservationSegment.getLogicalCabinClass());

					depatureOnD.setPreferredDate(lccClientReservationSegment.getDepartureDate());

					Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(lccClientReservationSegment.getDepartureDate());
					Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(lccClientReservationSegment.getDepartureDate());

					depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
					depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);

					OriginDestinationOptionTO originDestinationOptionTO = new OriginDestinationOptionTO();

					FlightSegmentTO flightSegTO = new FlightSegmentTO();
					flightSegTO.setFlightRefNumber(lccClientReservationSegment.getFlightSegmentRefNumber());
					flightSegTO.setFlightSegId(FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(lccClientReservationSegment.getFlightSegmentRefNumber()));
					flightSegTO.setOndSequence(ondSequence);
					originDestinationOptionTO.getFlightSegmentList().add(flightSegTO);

					depatureOnD.getOrignDestinationOptions().add(originDestinationOptionTO);
				}
			} else {
				OriginDestinationOptionTO originDestinationOptionTO = new OriginDestinationOptionTO();
				for (LCCClientReservationSegment lccClientReservationSegment : ondSegList) {
					if (!pnrSegIdsForCNX.contains(lccClientReservationSegment.getPnrSegID())) {
						FlightSegmentTO flightSegTO = new FlightSegmentTO();
						flightSegTO.setFlightRefNumber(lccClientReservationSegment.getFlightSegmentRefNumber());
						flightSegTO.setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(lccClientReservationSegment.getFlightSegmentRefNumber()));
						flightSegTO.setOndSequence(ondSequence);
						originDestinationOptionTO.getFlightSegmentList().add(flightSegTO);
					}
				}

				if (originDestinationOptionTO.getFlightSegmentList() != null
						&& originDestinationOptionTO.getFlightSegmentList().size() > 0) {
					LCCClientReservationSegment firstSeg = ondSegList.get(0);
					LCCClientReservationSegment lastSeg = ondSegList.get(ondSegList.size() - 1);

					Date depDate = firstSeg.getDepartureDate();
					Date arrivalDate = lastSeg.getArrivalDate();

					depatureOnD.setOrigin(firstSeg.getSegmentCode().split("/")[0]);
					depatureOnD
							.setDestination(lastSeg.getSegmentCode().split("/")[lastSeg.getSegmentCode().split("/").length - 1]);
					depatureOnD.setPreferredClassOfService(firstSeg.getCabinClassCode());
					depatureOnD.setPreferredLogicalCabin(firstSeg.getLogicalCabinClass());
					depatureOnD.setPreferredBookingType(firstSeg.getBookingType());
					depatureOnD.setPreferredDate(depDate);

					Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depDate);
					Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(arrivalDate);

					depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
					depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);

					List<Integer> flightSegmentIds = new ArrayList<Integer>();
					List<String> pnrSegRefIds = new ArrayList<String>();
					for (LCCClientReservationSegment lccClientReservationSegment : ondSegList) {
						if (!pnrSegIdsForCNX.contains(lccClientReservationSegment.getPnrSegID())) {
							String flightRefNumber = lccClientReservationSegment.getFlightSegmentRefNumber();
							if (flightRefNumber.indexOf("#") != -1) {
								String arr[] = flightRefNumber.split("#");
								flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
							} else {
								flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
							}
							pnrSegRefIds.add(this.extractPnrSegRph(flightRefNumber, lccClientReservationSegment.getPnrSegID()));
						}
					}
					depatureOnD.getExistingFlightSegIds().addAll(flightSegmentIds);
					depatureOnD.getExistingPnrSegRPHs().addAll(pnrSegRefIds);

					depatureOnD.getOrignDestinationOptions().add(originDestinationOptionTO);
				}
			}

		}

		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		String bookingType = BookingClass.BookingClassType.NORMAL;

		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(bookingType);

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(paxCounts[0]);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(paxCounts[1]);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(paxCounts[2]);

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availPref.setTravelAgentCode(principal.getAgentCode());
		availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
		availPref.setQuoteOndFlexi(OndSequence.OUT_BOUND, false);
		availPref.setQuoteOndFlexi(OndSequence.IN_BOUND, false);
		availPref.setQuoteFares(true);
		availPref.setSearchSystem(SYSTEM.AA);
		availPref.setAppIndicator(ApplicationEngine.XBE);

	}

	private Map<Integer, List<LCCClientReservationSegment>>
			getExistingConfirmedSegments(LCCClientReservation lccClientReservation) {
		Set<LCCClientReservationSegment> segments = lccClientReservation.getSegments();
		Map<Integer, List<LCCClientReservationSegment>> confirmedSegments = new HashMap<Integer, List<LCCClientReservationSegment>>();

		for (LCCClientReservationSegment segment : segments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {
				Integer ondSequence = segment.getFareTO().getOndSequence();
				if (confirmedSegments.get(ondSequence) != null) {
					confirmedSegments.get(ondSequence).add(segment);
				} else {
					List<LCCClientReservationSegment> segList = new ArrayList<LCCClientReservationSegment>();
					segList.add(segment);
					confirmedSegments.put(ondSequence, segList);
				}
			}
		}

		return confirmedSegments;
	}

	private List<Integer> extractPNRSegIds(LCCClientReservation lccClientReservation,
			List<OriginDestinationOptionType> originDestinationOption) {
		List<String> rphs = new ArrayList<String>();
		for (OriginDestinationOptionType originDestOption : originDestinationOption) {

			for (BookFlightSegmentType flightSegment : originDestOption.getFlightSegment()) {
				rphs.add(flightSegment.getRPH());
			}

		}
		Collection<LCCClientReservationSegment> segments = lccClientReservation.getSegments();

		List<Integer> pnrSegIds = new ArrayList<Integer>();

		for (String rph : rphs) {
			for (LCCClientReservationSegment segment : segments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus()) && rph.equals(
						FlightRefNumberUtil.getPnrSegIdFromPnrSegRPH(segment.getBookingFlightSegmentRefNumber()).toString())) {
					pnrSegIds.add(segment.getPnrSegID());
				}
			}
		}

		return pnrSegIds;
	}

	/**
	 * Returns balances for modify OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public OTAAirBookRS getBalancesForModifyOND(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			TrackInfoDTO trackInfoDTO, String groupPNR) throws WebservicesException, ModuleException {
		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "New OriginDestinationOptions required");
		}

		AirItineraryType.OriginDestinationOptions modifiedOND = otaAirBookModifyRQ.getAirReservation().getAirItinerary()
				.getOriginDestinationOptions();
		if (modifiedOND == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Modified OriginDestinationOptions required");
		}

		validateBusOnlyBookings(lccClientReservation, newOnds, modifiedOND);

		List<Integer> pnrSegIdsForMod = extractPNRSegIds(lccClientReservation, modifiedOND);

		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
		Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForMod.size());
		for (Integer segId : pnrSegIdsForMod)
			removedSegmentIds.add(segId.toString());
		balanceQueryRQ.setRemovedSegmentIds(removedSegmentIds);
		balanceQueryRQ.setPnr(lccClientReservation.getPNR());
		balanceQueryRQ.setCustomCharges(customChargesTO);
		balanceQueryRQ.setGroupPnr(groupPNR);
		balanceQueryRQ.setVersion(lccClientReservation.getVersion());

		OTAPaxCountTO otaPaxCountTO = getPaxCounts(lccClientReservation);

		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
		PriceInfoTO priceInfoTO = (PriceInfoTO) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_INFO);

		if (quotedPriceInfoTO == null || priceInfoTO == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Quoted price no longer available,Please search again");
		}

		@SuppressWarnings("unchecked")
		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = WSReservationUtil.getPassengerExtChgMap(quotedExternalCharges,
				lccClientReservation.getPassengers());

		balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		flightPriceRQ.getTravelPreferences().setBookingType(quotedPriceInfoTO.getBookingType());
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(otaPaxCountTO.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

		QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
		balanceQueryRQ.setFareInfo(fareInfo);
		balanceQueryRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
		balanceQueryRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());

		ReservationBalanceTO reservationBalanceTO = WebServicesModuleUtils.getAirproxyReservationBD()
				.getRequoteBalanceSummary(balanceQueryRQ, trackInfoDTO);

		ExtensionsInterlineUtil.clearRequoteBalanceTO();
		AAAlterationBalancesType aaResCnxBalances = ExtensionsInterlineUtil.prepareWSResAlterationBalances(
				lccClientReservation.getPassengers(), reservationBalanceTO, ReservationConstants.AlterationType.ALT_MODIFY_OND,
				pnrSegIdsForMod.size());

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	private void validateBusOnlyBookings(LCCClientReservation lccClientReservation, OriginDestinationOptions newOnds,
			OriginDestinationOptions modifiedOND) throws ModuleException, WebservicesException {

		List<FlightSearchDTO> addingSectors = new ArrayList<FlightSearchDTO>();
		FlightSearchDTO addingSector = null;
		for (OriginDestinationOptionType originDestinationOption : newOnds.getOriginDestinationOption()) {
			List<BookFlightSegmentType> bookingSegments = originDestinationOption.getFlightSegment();
			if (bookingSegments != null) {
				for (BookFlightSegmentType bookingSegment : bookingSegments) {
					String fromAirport = bookingSegment.getDepartureAirport().getLocationCode();
					String toAirport = bookingSegment.getArrivalAirport().getLocationCode();
					if (fromAirport != null && toAirport != null) {
						addingSector = new FlightSearchDTO();
						addingSector.setFromAirport(fromAirport);
						addingSector.setToAirport(toAirport);
						addingSectors.add(addingSector);
					}

				}
			}
		}

		boolean onlyAddingBus = AddModifySurfaceSegmentValidations.isOnlyGroundSegments(addingSectors);

		if (onlyAddingBus) {
			List<FlightSearchDTO> candidateConfirmedSectors = new ArrayList<FlightSearchDTO>();
			FlightSearchDTO candidateConfirmedSector = null;

			// system will only allow to modify one sector at given time.
			OriginDestinationOptionType modifyingOnd = modifiedOND.getOriginDestinationOption().get(0);

			for (BookFlightSegmentType modifyingSegment : modifyingOnd.getFlightSegment()) {
				if (lccClientReservation.getSegments() != null) {
					for (LCCClientReservationSegment resSegment : lccClientReservation.getSegments()) {
						Integer modifiedFltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(modifyingSegment.getRPH());
						Integer resSegFltSegId = FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(resSegment.getFlightSegmentRefNumber());

						if (resSegFltSegId != null && !resSegFltSegId.equals(modifiedFltSegId)
								&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSegment.getStatus())) {
							String segmentCode = resSegment.getSegmentCode();
							String segArray[] = segmentCode.split("/");
							candidateConfirmedSector = new FlightSearchDTO();
							candidateConfirmedSector.setFromAirport(segArray[0]);
							candidateConfirmedSector.setToAirport(segArray[segArray.length - 1]);
							candidateConfirmedSectors.add(candidateConfirmedSector);
						}
					}
				}
			}

			boolean jourenyHasOnlyBuses = candidateConfirmedSectors.isEmpty()
					|| AddModifySurfaceSegmentValidations.isOnlyGroundSegments(candidateConfirmedSectors);
			if (jourenyHasOnlyBuses) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Cannot leave the reservation only with bus segments");
			}
		}

	}

	/**
	 * Returns balances for add OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public OTAAirBookRS getBalancesForAddOND(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			TrackInfoDTO trackInfoDTO, String groupPNR) throws WebservicesException, ModuleException {
		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "OriginDestinationOptions required");
		}

		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
		balanceQueryRQ.setPnr(lccClientReservation.getPNR());
		balanceQueryRQ.setCustomCharges(customChargesTO);
		balanceQueryRQ.setGroupPnr(groupPNR);
		balanceQueryRQ.setVersion(lccClientReservation.getVersion());
		OTAPaxCountTO otaPaxCountTO = getPaxCounts(lccClientReservation);
		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
		PriceInfoTO priceInfoTO = (PriceInfoTO) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_INFO);

		if (quotedPriceInfoTO == null || priceInfoTO == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Quoted price no longer available,Please search again");
		}

		@SuppressWarnings("unchecked")
		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = WSReservationUtil.getPassengerExtChgMap(quotedExternalCharges,
				lccClientReservation.getPassengers());

		balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		flightPriceRQ.getTravelPreferences().setBookingType(quotedPriceInfoTO.getBookingType());
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(otaPaxCountTO.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

		QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(), flightPriceRQ, null);
		balanceQueryRQ.setFareInfo(fareInfo);
		balanceQueryRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
		balanceQueryRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());

		ReservationBalanceTO reservationBalanceTO = WebServicesModuleUtils.getAirproxyReservationBD()
				.getRequoteBalanceSummary(balanceQueryRQ, trackInfoDTO);

		// TODO check effectingSegsCount = 0
		ExtensionsInterlineUtil.clearRequoteBalanceTO();
		AAAlterationBalancesType aaResCnxBalances = ExtensionsInterlineUtil.prepareWSResAlterationBalances(
				lccClientReservation.getPassengers(), reservationBalanceTO, ReservationConstants.AlterationType.ALT_ADD_OND, 0);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Get balances for cancelling reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public OTAAirBookRS getBalancesForCancelRes(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			TrackInfoDTO trackInfoDTO, boolean isGroupPNR) throws ModuleException, WebservicesException {

		LCCClientResAlterQueryModesTO resAlterQueryModesTO = LCCClientResAlterModesTO.composeCancelReservationQueryRequest(
				lccClientReservation.getPNR(), lccClientReservation.getVersion(), isGroupPNR);

		ReservationBalanceTO reservationBalanceTO = WebServicesModuleUtils.getAirproxyReservationBD()
				.getResAlterationBalanceSummary(resAlterQueryModesTO, trackInfoDTO);

		ExtensionsInterlineUtil.clearRequoteBalanceTO();
		AAAlterationBalancesType aaResCnxBalances = ExtensionsInterlineUtil.prepareWSResAlterationBalances(
				lccClientReservation.getPassengers(), reservationBalanceTO, ReservationConstants.AlterationType.ALT_CANCEL_RES,
				0);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Transferring ownership of a reservation.
	 * 
	 * @param lccClientReservation
	 * @param otaAirBookModifyRQ
	 * @param trackInfoDTO
	 * @param isGroupPNR
	 * @throws WebservicesException
	 * 
	 */
	public void transferBooking(LCCClientReservation lccClientReservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt, boolean isGroupPNR, TrackInfoDTO trackInfoDTO)
			throws WebservicesException {
		AAAdminInfoType adminInfoType = aaAirBookModifyRQExt.getAdminInfo();
		if ((adminInfoType == null) || (adminInfoType.getOwnerAgentCode() == null)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Agent Code required");
		}
		String ownerAgentCode = adminInfoType.getOwnerAgentCode();
		String groupPNR = lccClientReservation.getPNR();

		try {
			WebServicesModuleUtils.getAirproxyReservationBD().transferOwnership(groupPNR, ownerAgentCode,
					lccClientReservation.getVersion(), isGroupPNR, trackInfoDTO);
		} catch (ModuleException e) {
			e.printStackTrace();
		}

	}

	private String extractPnrSegRph(String flightSegRph, Integer pnrSegID) {
		String delimiter = "\\$";
		String[] splittedString;
		String pnrSegRph = "";

		if (flightSegRph.contains("$")) {
			splittedString = flightSegRph.split(delimiter);
			if (splittedString != null && splittedString.length == 5) {
				splittedString[2] = pnrSegID.toString();
			}

			for (int i = 0; i < splittedString.length; i++) {
				pnrSegRph += splittedString[i];
				if (i != splittedString.length) {
					pnrSegRph += "$";
				}
			}
		} else {
			pnrSegRph = pnrSegID.toString();
		}

		return pnrSegRph;
	}

	private void validateCancelSegmentRQ(OTAAirBookModifyRQ otaAirBookModifyRQ, LCCClientReservation lccClientReservation)
			throws WebservicesException {
		LCCClientReservationSegment firstSegment = ReservationUtil.getFirstFlightSeg(lccClientReservation.getSegments());

		if (!isServiceTaxAppliedForJouney(firstSegment) && otaAirBookModifyRQ.getAirReservation() != null
				&& otaAirBookModifyRQ.getAirReservation().getAirItinerary() != null
				&& otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions() != null
				&& otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
						.getOriginDestinationOption() != null
				&& !otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
						.getOriginDestinationOption().isEmpty()
				&& otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
						.getOriginDestinationOption().get(0).getFlightSegment() != null
				&& !otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
						.getOriginDestinationOption().get(0).getFlightSegment().isEmpty()) {

			String originCode = otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
					.getOriginDestinationOption().get(0).getFlightSegment().get(0).getDepartureAirport().getLocationCode();

			String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			String newOriginCountryCode = (String) WebServicesModuleUtils.getGlobalConfig().retrieveAirportInfo(originCode)[3];
			for (String countryCode : taxRegNoEnabledCountries) {
				if (newOriginCountryCode.equals(countryCode)) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SEGMENT_CANCELLATION,
							"Selected segment cancellation is not allowed. Please cancel the reseravtion and do a fresh booking.");
				}
			}
		}
	}

	private boolean isServiceTaxAppliedForJouney(LCCClientReservationSegment firstSegment) {
		boolean isServiceTaxAppliedForJouney = false;

		String originCode = "";
		if (firstSegment != null && firstSegment.getSegmentCode() != null && firstSegment.getSegmentCode().length() > 2) {
			originCode = firstSegment.getSegmentCode().split("/")[0];
		}

		String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
		String newOriginCountryCode = (String) WebServicesModuleUtils.getGlobalConfig().retrieveAirportInfo(originCode)[3];
		for (String countryCode : taxRegNoEnabledCountries) {
			if (newOriginCountryCode.equals(countryCode)) {
				isServiceTaxAppliedForJouney = true;
			}
		}

		return isServiceTaxAppliedForJouney;
	}

	private void sendIternary(LCCClientReservation reservation) throws ModuleException {

		ReservationBD reservationDelegate = WebServicesModuleUtils.getReservationBD();

		CommonReservationContactInfo reservationContactInfo = reservation.getContactInfo();
		if (reservationContactInfo != null && reservationContactInfo.getEmail() != null
				&& !"".equals(reservationContactInfo.getEmail())) {
			// Email itinerary
			ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
			itineraryLayoutModesDTO.setPnr(reservation.getPNR());

			String emailLang = (reservationContactInfo.getPreferredLanguage() == null)
					? Locale.ENGLISH.toString()
					: reservationContactInfo.getPreferredLanguage();

			itineraryLayoutModesDTO.setLocale(new Locale(emailLang));
			itineraryLayoutModesDTO.setIncludePassengerPrices(true);

			reservationDelegate.emailItinerary(itineraryLayoutModesDTO, null, null, false);
		}

	}
}
