/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAAgentAvailCreditType;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRS;
import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRS;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRS;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRQ;
import org.opentravel.ota._2003._05.AAOTAEBIManualDailyReconRptSendRQ;
import org.opentravel.ota._2003._05.AAOTAEBIPnrTxnsReconRQ;
import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditReadRQ;
import org.opentravel.ota._2003._05.AAOTAResAuditRS;
import org.opentravel.ota._2003._05.AAOTAResAuditReadRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRS;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRQ;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRS;
import org.opentravel.ota._2003._05.AAPNRAuditMessagesType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirFlifoRQ;
import org.opentravel.ota._2003._05.OTAAirFlifoRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirScheduleRQ;
import org.opentravel.ota._2003._05.OTAAirScheduleRS;
import org.opentravel.ota._2003._05.OTAAirSeatMapRQ;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRQ;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAResRetrieveRS;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.UniqueIDType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.ReservationQueryInterlineUtil;
import com.isa.thinair.webservices.core.interlineUtil.TransformInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.AgentSalesDataUtil;
import com.isa.thinair.webservices.core.util.AvailabilitySearchUtil;
import com.isa.thinair.webservices.core.util.BaggageDetailsUtil;
import com.isa.thinair.webservices.core.util.BookUtil;
import com.isa.thinair.webservices.core.util.ConnectionFlightLoadUtil;
import com.isa.thinair.webservices.core.util.CreditAvailabilityUtil;
import com.isa.thinair.webservices.core.util.ETicketUpdateUtil;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.ExternalPaymentsUtil;
import com.isa.thinair.webservices.core.util.FlightLegLoadReportUtil;
import com.isa.thinair.webservices.core.util.FlightLoadInfoUtil;
import com.isa.thinair.webservices.core.util.FlightScheduleSearchUtil;
import com.isa.thinair.webservices.core.util.FlightSearchUtil;
import com.isa.thinair.webservices.core.util.InsuranceServiceUtil;
import com.isa.thinair.webservices.core.util.MealDetailsUtil;
import com.isa.thinair.webservices.core.util.ModifyBookingUtil;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.PFSXmlUtil;
import com.isa.thinair.webservices.core.util.PassengerCreditSearchUtil;
import com.isa.thinair.webservices.core.util.PriceQuoteUtil;
import com.isa.thinair.webservices.core.util.RMUtil;
import com.isa.thinair.webservices.core.util.ReceiptUpdateUtil;
import com.isa.thinair.webservices.core.util.ReservationQueryUtil;
import com.isa.thinair.webservices.core.util.SSRDetailsUtil;
import com.isa.thinair.webservices.core.util.SeatServiceUtil;
import com.isa.thinair.webservices.core.util.TransformUtil;
import com.isa.thinair.webservices.core.util.TypeBResMsgProcessUtill;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvBatchUpdateRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventryBatchUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAStatusErrorsAndWarnings;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRS;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRQ;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAIOBoundPaxCountInfoRS;
import com.isaaviation.thinair.webservices.api.commons.AAResultType;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRQ;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRS;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRQ;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCreditRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCreditRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCreditType;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileSimpleType;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileStatusRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileStatusRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileType;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.FailedData;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.FailedField;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRQ;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRS;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRQ;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRS;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRQ;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRS;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

/**
 * @author Mohamed Nasly
 * @author Nilindra
 * @author Mehdi
 * @author Byorn
 * 
 * @since 2.0
 */
public class BaseWebServicesImpl {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Returns Availability
	 */
	public OTAAirAvailRS getAvailability(OTAAirAvailRQ oTAAirAvailRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getAvailability(OTAAirAvailRQ)");
		}

		OTAAirAvailRS otaAirAvailRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			otaAirAvailRS = new AvailabilitySearchUtil().getAvailability(oTAAirAvailRQ);
		} catch (Exception ex) {
			log.error("getAvailability(OTAAirAvailRQ) failed.", ex);
			if (log.isInfoEnabled()) {
				OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AVAILABILITY_RESPONSE_START, null,
						null, null);
			}
			setTriggerPostProcessError(ex);
			if (otaAirAvailRS == null)
				otaAirAvailRS = new OTAAirAvailRS();
			if (otaAirAvailRS.getErrors() == null)
				otaAirAvailRS.setErrors(new ErrorsType());
			if (otaAirAvailRS.getWarnings() == null)
				otaAirAvailRS.setWarnings(new WarningsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirAvailRS.getErrors().getError(),
					otaAirAvailRS.getWarnings().getWarning(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getAvailability(OTAAirAvailRQ)");
		return otaAirAvailRS;
	}

	/**
	 * Returns Fare Quote
	 */
	public OTAAirPriceRS getPrice(OTAAirPriceRQ otaAirPriceRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getPrice(OTAAirPriceRQ)");
		}

		OTAAirPriceRS otaAirPriceRS = null;
		try {
			triggerPreProcessErrorIfPresent();

			otaAirPriceRS = new PriceQuoteUtil().getPrice(otaAirPriceRQ);
		} catch (Exception ex) {
			log.error("getPrice(OTAAirPriceRQ) failed", ex);
			if (log.isInfoEnabled()) {
				OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.PRICE_RESPONSE, null, null, null);
			}
			setTriggerPostProcessError(ex);
			if (otaAirPriceRS == null)
				otaAirPriceRS = new OTAAirPriceRS();
			if (otaAirPriceRS.getErrors() == null)
				otaAirPriceRS.setErrors(new ErrorsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirPriceRS.getErrors().getError(),
					otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getPrice(OTAAirPriceRQ)");
		return otaAirPriceRS;
	}

	/**
	 * Make booking
	 */
	@SuppressWarnings("unchecked")
	public OTAAirBookRS book(OTAAirBookRQ otaAirBookRQ, AAAirBookRQExt aaAirBookRQExt) {
		OTAAirBookRS airBookRS = null;
		BookUtil bookUtil = new BookUtil();

		try {
			triggerPreProcessErrorIfPresent();

			airBookRS = bookUtil.book(otaAirBookRQ, aaAirBookRQExt);

		} catch (Exception ex) {
			log.error("book(OTAAirBookRQ) failed", ex);
			if (log.isInfoEnabled()) {
				OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.BOOK_RESPONSE, null, null, null);
			}
			setTriggerPostProcessError(ex);
			if (airBookRS == null)
				airBookRS = new OTAAirBookRS();
			if (airBookRS.getErrors() == null)
				airBookRS.setErrors(new ErrorsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(airBookRS.getErrors().getError(),
					airBookRS.getSuccessAndWarningsAndAirReservation(), ex);
		}
		return airBookRS;
	}

	/**
	 * Get reservation by PNR - Uses OTAReadRQ.ReadRequests.ReadRequest.UniqueID for search
	 * 
	 * @param otaReadRQ
	 * @return
	 */
	public OTAAirBookRS getReservationbyPNR(OTAReadRQ otaReadRQ, AAReadRQExt aaReadRQExt) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getReservationbyPNR(OTAReadRQ)");
		}
		OTAAirBookRS airBookRS = null;
		AALoadDataOptionsType aaLoadDataOptions = null;

		try {
			triggerPreProcessErrorIfPresent();

			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS,
					new Boolean(true));

			if (aaReadRQExt != null && aaReadRQExt.getAALoadDataOptions() != null) {
				aaLoadDataOptions = aaReadRQExt.getAALoadDataOptions();
			} else {
				// TODO prepare load data options
			}

			TransformUtil.ReadRequest readRequestTransfomer = new TransformUtil.ReadRequest(otaReadRQ, aaLoadDataOptions,
					TransformUtil.ReadRequest.TransformTO.AirBookRS);

			Object[] transformedData = readRequestTransfomer.transform(false);

			airBookRS = (OTAAirBookRS) transformedData[0];

			if (airBookRS == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (Exception ex) {
			log.error("getReservationbyPNR(OTAReadRQ) failed", ex);
			setTriggerPostProcessError(ex);
			if (airBookRS == null) {
				airBookRS = new OTAAirBookRS();
			}
			if (airBookRS.getErrors() == null)
				airBookRS.setErrors(new ErrorsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(airBookRS.getErrors().getError(),
					airBookRS.getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END getReservationbyPNR(OTAReadRQ)");
		}

		return airBookRS;
	}

	/**
	 * Get a list of reservations matching search parameters. Uses OTAReadRQ.ReadRequests.ReadRequest.AirReadRequest for
	 * search
	 * 
	 * @param otaReadRQ
	 * @return
	 */
	public OTAResRetrieveRS getReservationsList(OTAReadRQ otaReadRQ, AAReadRQExt aaReadRQExt) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getReservationsList(OTAReadRQ)");
		}
		OTAResRetrieveRS otaResRetrieveRS = null;
		AALoadDataOptionsType aaLoadDataOptions = null;

		try {
			triggerPreProcessErrorIfPresent();

			TransformUtil.ReadRequest readReqTransformer = new TransformUtil.ReadRequest(otaReadRQ, aaLoadDataOptions,
					TransformUtil.ReadRequest.TransformTO.ResRetrieveRS);

			otaResRetrieveRS = (OTAResRetrieveRS) readReqTransformer.transform(false)[0];

			if (otaResRetrieveRS == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (Exception ex) {
			log.error("getReservationsList(OTAReadRQ) failed", ex);
			setTriggerPostProcessError(ex);
			if (otaResRetrieveRS == null)
				otaResRetrieveRS = new OTAResRetrieveRS();
			if (otaResRetrieveRS.getErrors() == null)
				otaResRetrieveRS.setErrors(new ErrorsType());
			if (otaResRetrieveRS.getWarnings() == null)
				otaResRetrieveRS.setWarnings(new WarningsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(otaResRetrieveRS.getErrors().getError(), otaResRetrieveRS.getWarnings()
					.getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getReservationsList(OTAReadRQ)");
		}
		return otaResRetrieveRS;
	}

	/**
	 * Modify Reservation Functionalities
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @return
	 * 
	 */
	@SuppressWarnings("unchecked")
	public OTAAirBookRS modifyReservation(OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt) {

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
		Reservation reservation = null;
		Collection<String> privilegeKeys = null;
		String modificationType = null;
		String pnr = null;

		try {
			triggerPreProcessErrorIfPresent();

			modificationType = otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType();
			if (modificationType == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
						"Modification type is invalid");
			}
			privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadLastUserNote(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setRecordAudit(true);

			ModifyBookingUtil modifyBookingUtil = new ModifyBookingUtil();

			// Balance Payment
			if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_BALANCE_PAYMENT))) {
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				modifyBookingUtil.balancePayment(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt, privilegeKeys);

				// SSR Update
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_SSR_UPDATE))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_SSR_CHANGE);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SSR, null);
				ModifyBookingUtil.updateSSR(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Name Change
			} else if (modificationType.equals(CommonUtil
					.getOTACodeValue(IOTACodeTables.ModificationType_MOD_NAME_CHANGE___CORRECTION))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_NAME_CHANGE);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME, null);
				ModifyBookingUtil.nameChange(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Add Notes to the reservation.
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_NOTES))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_UPDATE);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeLCCReservation(reservation);
				modifyBookingUtil.addUserNotes(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Split reservation - Splitting selected passengers into a new reservation.
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_SPLIT_ONLY))) {
				reservation = getReservation(pnrModesDTO, null);
				if (ReservationUtil.isBulkTicketReservation(reservation)) {
					WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_SPLIT);
				} else {
					WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_SPLIT);
				}
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
				pnr = (String) modifyBookingUtil.splitReservation(reservation, otaAirBookModifyRQ);

				// Update passenger contact details.
			} else if (modificationType.equals(CommonUtil
					.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CONTACT_DETAILS_UPDATE))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.CHANGE_CONTACT_DETAILS);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeLCCReservation(reservation);
				modifyBookingUtil.updateContactDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Remove passenger
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCEL_PARTLY))) {
				reservation = getReservation(pnrModesDTO, null);
				if (ReservationUtil.isBulkTicketReservation(reservation)) {
					WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX);
				} else {
					WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_REMOVE_PAX);
				}
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
				modifyBookingUtil.removePassenger(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Cancel reservation
			} else if (modificationType.equals(CommonUtil
					.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCEL_ENTIRE_BOOKING_FILE))) {
				// otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID() --> res being cancelled
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.CANCEL_RES);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
				modifyBookingUtil.cancelReservation(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Cancel OND
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCEL_OND))) {
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> OND being cancelled
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeLCCReservation(reservation);
				modifyBookingUtil.cancelOND(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Modify OND
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_MODIFY_OND))) {
				// TODO complete the implementation
				// otaAirBookModifyRQ.getAirReservation().getAirItinerary() --> OND being modified
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> new OND
				// AATransactionManager.getInstance().getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_COLLECTION)
				// --> OndFareDTO collection for new OND
				// WSReservationUtil.authorize(privilegeKeys,
				// null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT);
				WSReservationUtil.auhtorizeModifySegment(privilegeKeys,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeLCCReservation(reservation);
				modifyBookingUtil.modifyOND(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Add OND
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_OND))) {
				// TODO test
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> new OND
				// AATransactionManager.getInstance().getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_COLLECTION)
				// --> OndFareDTO collection for added OND
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeLCCReservation(reservation);
				modifyBookingUtil.addOND(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Add Infant
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_INFANT))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_ADD_INFANT);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
				modifyBookingUtil.addInfant(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Extend Onhold
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_EXTEND_ONHOLD))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.RES_EXTEND);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation, null, null);
				modifyBookingUtil.extendOnholdReservation(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Transfer Booking
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_TRANSFER_BOOKING))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.MakeResPrivilegesKeys.RES_TRANSFER);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation, null, null);
				modifyBookingUtil.transferBooking(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Passenger Refund
			} else if (modificationType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_REFUND_PASSENGER))) {
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.PAX_REFUND);
				reservation = getReservation(pnrModesDTO, null);
				WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
						WSConstants.ALLOW_MODIFY_RESERVATION);
				ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_CHARGES, null);
				modifyBookingUtil.passengerRefund(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

				// Update Reservation
			} else if (modificationType
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_RESERVATION_UPDATE))) {

				if (AuthorizationUtil.hasPrivileges(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.RES_UPDATE,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_SSR_CHANGE,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_NAME_CHANGE)) {
					reservation = getReservation(pnrModesDTO, null);
					WSReservationUtil.authorizeBookingCategory(privilegeKeys, reservation.getBookingCategory(),
							WSConstants.ALLOW_MODIFY_RESERVATION);
					ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME, null);
					ModifyBookingUtil.updateReservation(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
				}
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE, "Modification type ["
						+ modificationType + "] not supported");
			}

			// Set context params for retrieveReservation
			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS,
					new Boolean(false));

			OTAReadRQ readRq = new OTAReadRQ();
			readRq.setUniqueID(new UniqueIDType());
			readRq.getUniqueID().setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
			readRq.getUniqueID().setID(pnr);
			readRq.setPOS(otaAirBookModifyRQ.getPOS());

			ReservationQueryUtil resQUtil = new ReservationQueryUtil();

			AALoadDataOptionsType aaLoadDataOptions = null;

			if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getAALoadDataOptions() != null)) {
				aaLoadDataOptions = aaAirBookModifyRQExt.getAALoadDataOptions();
			} else {
				// TODO prepare load data options
			}

			otaAirBookRS = resQUtil.getReservation(readRq, aaLoadDataOptions, true);

		} catch (Exception ex) {
			log.error("modifyReservation(OTAAirBookModifyRQ, LoadDataOptions) failed", ex);
			setTriggerPostProcessError(ex);
			if (otaAirBookRS == null)
				otaAirBookRS = new OTAAirBookRS();
			if (otaAirBookRS.getErrors() == null)
				otaAirBookRS.setErrors(new ErrorsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirBookRS.getErrors().getError(),
					otaAirBookRS.getSuccessAndWarningsAndAirReservation(), ex);
		}
		return otaAirBookRS;
	}

	/**
	 * Booking modification queries.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * 
	 */
	public OTAAirBookRS modifyResQuery(OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt) {
		String pnr = null;
		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
		try {
			triggerPreProcessErrorIfPresent();

			pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);

			pnrModesDTO.setLoadLastUserNote(true);
			pnrModesDTO.setRecordAudit(true);

			Reservation reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
			if (reservation == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}

			// Avoid modification of LCC reservations
			ModifyBookingUtil.authorizeLCCReservation(reservation);

			// Balances for cancel reservation
			if (otaAirBookModifyRQ
					.getAirBookModifyRQ()
					.getModificationType()
					.equals(CommonUtil
							.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCELLATION_BALANCES_FOR_CANCEL_RESERVATION))) {
				otaAirBookRS = ModifyBookingUtil.getBalancesForCancelRes(reservation, otaAirBookModifyRQ);

				// Balances for cancel OND
			} else if (otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCELLATION_BALANCES_FOR_CANCEL_OND))) {
				otaAirBookRS = ModifyBookingUtil.getBalancesForCancelOND(reservation, otaAirBookModifyRQ);

				// Balances for modify OND
			} else if (otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_MODIFICATION_BALANCES_FOR_MODIFY_OND))) {
				otaAirBookRS = ModifyBookingUtil.getBalancesForModifyOND(reservation, otaAirBookModifyRQ);
				// otaAirBookModifyRQ.getAirReservation().getAirItinerary() --> OND being modified
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> new OND
				// AATransactionManager.getInstance().getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_COLLECTION)
				// --> OndFareDTO collection

				// Balances for add OND
			} else if (otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_BALANCES_FOR_ADD_OND))) {
				otaAirBookRS = ModifyBookingUtil.getBalancesForAddOND(reservation, otaAirBookModifyRQ);
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> added OND
				// AATransactionManager.getInstance().getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_COLLECTION)
				// --> OndFareDTO collection for added OND

			} // Balances for add INF
			else if (otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_INFANT))) {

				otaAirBookRS = ModifyBookingUtil.getBalancesForAddInfant(reservation, otaAirBookModifyRQ);
				// otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary() --> added OND
				// AATransactionManager.getInstance().getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_COLLECTION)
				// --> OndFareDTO collection for added OND

			} else {

				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
						"Modification query type [" + otaAirBookModifyRQ.getAirBookModifyRQ().getModificationType()
								+ "] not supported");
			}

		} catch (Exception ex) {
			log.error("modifyResQuery(OTAAirBookModifyRQ, LoadDataOptions) failed", ex);
			setTriggerPostProcessError(ex);
			if (otaAirBookRS == null)
				otaAirBookRS = new OTAAirBookRS();
			if (otaAirBookRS.getErrors() == null)
				otaAirBookRS.setErrors(new ErrorsType());

			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirBookRS.getErrors().getError(),
					otaAirBookRS.getSuccessAndWarningsAndAirReservation(), ex);
		}
		return otaAirBookRS;
	}

	/**
	 * Returns flight schedule information.
	 * 
	 * @param otaAirScheduleRQ
	 * @return {@link OTAAirScheduleRS}
	 */
	public OTAAirScheduleRS getFlightSchedule(OTAAirScheduleRQ otaAirScheduleRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getFlightSchedule(OTAAirScheduleRQ)");
		}
		OTAAirScheduleRS otaAirScheduleRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADMIN_SCHEDULE_ALLOW_VIEW_DETAILS);

			otaAirScheduleRS = FlightScheduleSearchUtil.searchFlightSchedule(otaAirScheduleRQ);
		} catch (Exception ex) {
			log.error("getFlightSchedule(OTAAirAvailRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (otaAirScheduleRS == null)
				otaAirScheduleRS = new OTAAirScheduleRS();
			if (otaAirScheduleRS.getErrors() == null)
				otaAirScheduleRS.setErrors(new ErrorsType());
			if (otaAirScheduleRS.getWarnings() == null)
				otaAirScheduleRS.setWarnings(new WarningsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirScheduleRS.getErrors().getError(), otaAirScheduleRS.getWarnings()
					.getWarning(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getFlightSchedule(OTAAirScheduleRQ)");
		return otaAirScheduleRS;
	}

	/**
	 * Returns flight information.
	 */
	public OTAAirFlifoRS getFlightInfo(OTAAirFlifoRQ otaAirFlifoRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getFlightInfo(OTAAirFlifoRQ)");
		}
		OTAAirFlifoRS otaAirFlifoRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADMIN_FLT_ALLOW_VIEW_DETAILS);

			otaAirFlifoRS = FlightSearchUtil.getFlightInfo(otaAirFlifoRQ);
		} catch (Exception ex) {
			log.error("getFlightInfo(OTAAirFlifoRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (otaAirFlifoRS == null) {
				otaAirFlifoRS = new OTAAirFlifoRS();
			}
			if (otaAirFlifoRS.getErrors() == null) {
				otaAirFlifoRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirFlifoRS.getErrors().getError(),
					otaAirFlifoRS.getWarnings().getWarning(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getFlightInfo(OTAAirFlifoRQ)");
		return otaAirFlifoRS;
	}

	public AAOTAAgentAvailCreditRS getAgentAvailableCredit(AAOTAAgentAvailCreditRQ availableCreditRQ) {
		AAOTAAgentAvailCreditRS response = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADMIN_ALLOW_VIEW_AGENT_CREDIT_AVAILABLE);

			CreditAvailabilityUtil.authorize(availableCreditRQ);
			response = new AAOTAAgentAvailCreditRS();
			AAAgentAvailCreditType agentAvailCredit = new AAAgentAvailCreditType();
			String agentCode = availableCreditRQ.getAgentID();
			if (agentCode != null && agentCode.length() > 0) {
				agentCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + agentCode;
			}
			agentAvailCredit.setAgentID(agentCode);
			agentAvailCredit.setAvailableCredit(new AAAgentAvailCreditType.AvailableCredit());
			BigDecimal creditLimit = WebServicesModuleUtils.getTravelAgentBD().getAgent(agentCode).getAgentSummary()
					.getAvailableCredit();
			OTAUtils.setAmountAndDefaultCurrency(agentAvailCredit.getAvailableCredit(), creditLimit);
			response.getSuccessAndWarningsAndAgentAvailCredit().add(agentAvailCredit);
			response.getSuccessAndWarningsAndAgentAvailCredit().add(new SuccessType());
		} catch (Exception ex) {
			log.error(ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAOTAAgentAvailCreditRS();
			response.setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(response.getErrors().getError(), null, ex);
		}
		return response;
	}

	/**
	 * Method to get the passenger credit details.
	 * 
	 * @param aaOTAPaxCreditReadRQ
	 * @return
	 */
	public AAOTAPaxCreditRS getPassengerCredit(AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getPassengerCredit(AAOTAPaxCreditReadRQ)");
		}
		AAOTAPaxCreditRS aaOTAPaxCreditRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS);

			aaOTAPaxCreditRS = new PassengerCreditSearchUtil().getPassengerCredit(aaOTAPaxCreditReadRQ);
		} catch (Exception ex) {
			log.error("getPassengerCredit(AAOTAPaxCreditRS) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOTAPaxCreditRS == null) {
				aaOTAPaxCreditRS = new AAOTAPaxCreditRS();
			}
			if (aaOTAPaxCreditRS.getErrors() == null) {
				aaOTAPaxCreditRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOTAPaxCreditRS.getErrors().getError(),
					aaOTAPaxCreditRS.getSuccessAndWarningsAndPaxCredits(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getPassengerCredit(AAOTAPaxCreditReadRQ)");
		return aaOTAPaxCreditRS;
	}

	/**
	 * Reconciles AA transactions with Bank transactions.
	 */
	public AAOTATransactionsReconRS reconPaymentTransactions(AAOTATransactionsReconRQ transactionsReconRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN reconPaymentTransactions(AAOTATransactionsReconRQ)");
		}
		AAOTATransactionsReconRS reconRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.ALLOW_WS_EBI_DAILY_RECON);

			reconRS = ExternalPaymentsUtil.reconExternalPayTransactions(transactionsReconRQ);
		} catch (Exception ex) {
			log.error("reconPaymentTransactions(AAOTATransactionsReconRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (reconRS == null) {
				reconRS = new AAOTATransactionsReconRS();
			}
			if (reconRS.getErrors() == null) {
				reconRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(reconRS.getErrors().getError(), reconRS.getSuccessAndWarnings(), ex);

		}
		if (log.isDebugEnabled()) {
			log.debug("END reconPaymentTransactions(AAOTATransactionsReconRQ)");
		}
		return reconRS;
	}

	/**
	 * Returns audits of all the activities took place on booking. TODO - Stucture the output removing formating.
	 * 
	 * @param aaOTAuditReadRQ
	 * @returns {@link AAOTAResAuditRS}
	 */
	public AAOTAResAuditRS getResAuditHistory(AAOTAResAuditReadRQ aaOTAuditReadRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getResAuditHistory(AAOTAResAuditReadRQ)");
		}
		AAOTAResAuditRS resAuditRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_RES_HISTORY);

			resAuditRS = new AAOTAResAuditRS();
			String pnr = aaOTAuditReadRQ.getBookingReferenceID().getID();
			Collection resAudits = WebServicesModuleUtils.getReservationQueryBD().getPnrHistory(pnr);
			AAPNRAuditMessagesType aaPNRAuditMessages = OTAUtils.prepareWSResAuditHistory(resAudits, pnr);
			resAuditRS.getSuccessAndWarningsAndPNRAuditMessages().add(aaPNRAuditMessages);
			resAuditRS.getSuccessAndWarningsAndPNRAuditMessages().add(new SuccessType());
		} catch (Exception ex) {
			log.error("getResAuditHistory(AAOTAResAuditReadRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (resAuditRS == null) {
				resAuditRS = new AAOTAResAuditRS();
			}
			if (resAuditRS.getErrors() == null) {
				resAuditRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(resAuditRS.getErrors().getError(),
					resAuditRS.getSuccessAndWarningsAndPNRAuditMessages(), ex);

		}
		if (log.isDebugEnabled()) {
			log.debug("END getResAuditHistory(AAOTAResAuditReadRQ)");
		}
		return resAuditRS;
	}

	/**
	 * Returns terms & conditions applicable for bookings. TODO - structure the output removing formattings.
	 * 
	 * @param aaOTATermsNConditionsRQ
	 * @returns {@link AAOTATermsNConditionsRS}
	 */
	public AAOTATermsNConditionsRS getTermsNConditions(AAOTATermsNConditionsRQ aaOTATermsNConditionsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getTermsNConditions(AAOTATermsNConditionsRQ)");
		}
		AAOTATermsNConditionsRS termsNConditionsRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_ITINERARY_TERMS);

			termsNConditionsRS = new AAOTATermsNConditionsRS();
			Locale locale = new Locale("en");
			String termsNConditions = WebServicesModuleUtils.getReservationQueryBD().getTermsNConditions(locale);
			termsNConditionsRS.getSuccessAndWarningsAndTermsNConditionsText().add(termsNConditions);
			termsNConditionsRS.getSuccessAndWarningsAndTermsNConditionsText().add(new SuccessType());
		} catch (Exception ex) {
			log.error("getTermsNConditions(AAOTATermsNConditionsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (termsNConditionsRS == null) {
				termsNConditionsRS = new AAOTATermsNConditionsRS();
			}
			if (termsNConditionsRS.getErrors() == null) {
				termsNConditionsRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(termsNConditionsRS.getErrors().getError(),
					termsNConditionsRS.getSuccessAndWarningsAndTermsNConditionsText(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END getTermsNConditions(AAOTATermsNConditionsRQ)");
		}
		return termsNConditionsRS;
	}

	/**
	 * Returns Itinerary for bookings. (With attachment)
	 * 
	 * @param otaReadRQ
	 * @returns {@link AAOTAItineraryRS}
	 */
	public AAOTAItineraryRS getItinerary(OTAReadRQ otaReadRQ) {
		return getItinerary(otaReadRQ, true);
	}

	/**
	 * Returns Itinerary for bookings. (Without attachment)
	 * 
	 * @param otaReadRQ
	 * @returns {@link AAOTAItineraryRS}
	 */
	public AAOTAItineraryRS getItineraryForPrint(OTAReadRQ otaReadRQ) {
		return getItinerary(otaReadRQ, false);
	}

	/**
	 * Returns Itinerary for bookings
	 * 
	 * @param otaReadRQ
	 * @returns {@link AAOTAItineraryRS}
	 */
	private AAOTAItineraryRS getItinerary(OTAReadRQ otaReadRQ, boolean includeAttachment) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getItinerary(OTAReadRQ)");
		}

		AAOTAItineraryRS itineraryRS = null;
		try {
			String PNR = otaReadRQ.getReadRequests().getReadRequest().get(0).getUniqueID().getID();
			if (!ReservationApiUtils.isPNRValid(PNR)) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_INVALID_, "Booking Reference ["
						+ PNR + "] is invalid");
			}

			triggerPreProcessErrorIfPresent();

			/*
			 * Collection privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
			 * AAUserSession.USER_PRIVILEGES_KEYS); WSReservationUtil.authorize(privilegeKeys,
			 * PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_ITINERARY_TERMS);
			 */
			itineraryRS = new AAOTAItineraryRS();
			ReservationQueryInterlineUtil resQUtil = new ReservationQueryInterlineUtil();
			itineraryRS = resQUtil.getItinerary(otaReadRQ, includeAttachment);
			;
		} catch (Exception ex) {
			log.error("getItinerary(OTAReadRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (itineraryRS == null) {
				itineraryRS = new AAOTAItineraryRS();
			}
			if (itineraryRS.getErrors() == null) {
				itineraryRS.setErrors(new ErrorsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(itineraryRS.getErrors().getError(),
					itineraryRS.getSuccessAndWarningsAndItineraryContentText(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END getItinerary(OTAReadRQ)");
		}
		return itineraryRS;
	}

	/**
	 * OTA ping functionality
	 */
	public OTAPingRS ping(OTAPingRQ otaPingRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN ping(OTAPingRQ)");
		}

		OTAPingRS otaPingRS = null;
		try {
			triggerPreProcessErrorIfPresent();

			otaPingRS = new OTAPingRS();
			otaPingRS.getSuccessAndWarningsAndEchoData().add(new SuccessType());
			otaPingRS.getSuccessAndWarningsAndEchoData().add("Received = " + otaPingRQ.getEchoData());
		} catch (Exception ex) {
			log.error("ping(OTAPingRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (otaPingRS == null)
				otaPingRS = new OTAPingRS();
			if (otaPingRS.getErrors() == null)
				otaPingRS.setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(otaPingRS.getErrors().getError(),
					otaPingRS.getSuccessAndWarningsAndEchoData(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END ping(OTAPingRQ)");
		}
		return otaPingRS;
	}

	public OTAAirSeatMapRS getSeatMap(OTAAirSeatMapRQ otaAirSeatMapRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getSeatMap(OTAAirSeatMapRQ)");
		}

		OTAAirSeatMapRS otaAirSeatMapRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			otaAirSeatMapRS = new SeatServiceUtil().getSeatMap(otaAirSeatMapRQ);
		} catch (Exception ex) {
			log.error("getSeatMap(OTAAirSeatMapRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (otaAirSeatMapRS == null) {
				otaAirSeatMapRS = new OTAAirSeatMapRS();
			}
			if (otaAirSeatMapRS.getErrors() == null) {
				otaAirSeatMapRS.setErrors(new ErrorsType());
			}
			if (otaAirSeatMapRS.getWarnings() == null) {
				otaAirSeatMapRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirSeatMapRS.getErrors().getError(), otaAirSeatMapRS.getWarnings()
					.getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getSeatMap(OTAAirSeatMapRQ)");
		}

		return otaAirSeatMapRS;
	}

	public OTAInsuranceQuoteRS getInsuranceQuote(OTAInsuranceQuoteRQ otaInsuranceQuoteRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getInsuranceQuote(OTAInsuranceQuoteRQ)");
		}

		OTAInsuranceQuoteRS otaInsuranceQuoteRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			InsuranceServiceUtil insuranceServiceUtil = new InsuranceServiceUtil();
			otaInsuranceQuoteRS = insuranceServiceUtil.getInsuranceQuote(otaInsuranceQuoteRQ);
		} catch (Exception ex) {
			log.error("getInsuranceQuote(OTAInsuranceQuoteRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (otaInsuranceQuoteRS == null) {
				otaInsuranceQuoteRS = new OTAInsuranceQuoteRS();
			}
			if (otaInsuranceQuoteRS.getErrors() == null) {
				otaInsuranceQuoteRS.setErrors(new ErrorsType());
			}
			if (otaInsuranceQuoteRS.getWarnings() == null) {
				otaInsuranceQuoteRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(otaInsuranceQuoteRS.getErrors().getError(), otaInsuranceQuoteRS.getWarnings()
					.getWarning(), ex);
		}

		return otaInsuranceQuoteRS;
	}

	/**
	 * Internal WS method for checking PNR transactions status at EBI and to sync Air Arabia transactions accordinly.
	 */
	public void syncPnrTxnsWithEBI(AAOTAEBIPnrTxnsReconRQ aaOTAPnrTxnsReconRQ) {
		try {
			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.ALLOW_WS_EBI_STATUS_CHECK);

			ReservationUtil.syncPNRExtPayTransactions(aaOTAPnrTxnsReconRQ.getBookingReferenceID().getID());
		} catch (Exception e) {
			log.error("PNR Transactions recon failed", e);
			setTriggerPostProcessError(e);
		}
	}

	/**
	 * Internal method for initiating daily reconciliation report sending process.
	 * 
	 */
	public void manualSendEBIDailyReconRpt(AAOTAEBIManualDailyReconRptSendRQ aaOTAEBIManualDailyReconRptSendRQ) {
		try {
			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.WSFuncPrivilegesKeys.ALLOW_WS_EBI_DAILY_RECON);

			Calendar startTimestamp = aaOTAEBIManualDailyReconRptSendRQ.getReconRptCriteria().getStartTimestamp()
					.toGregorianCalendar();
			Calendar endTimestmap = aaOTAEBIManualDailyReconRptSendRQ.getReconRptCriteria().getEndTimestamp()
					.toGregorianCalendar();
			log.info("External Payment Transaction Reconcilation Task " + "[startTimestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(startTimestamp.getTime()) + ",endTimestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(endTimestmap.getTime()) + "]");
			WSClientBD wsClientBD = WebServicesModuleUtils.getWSClientBD();
			// TODO - FIXME serviceProvider code
			wsClientBD.requestDailyBankTnxReconcilation(startTimestamp.getTime(), endTimestmap.getTime(), null);

			log.info("Completed External Payment Transaction Reconcilation Task " + "[Current Timestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
		} catch (Exception e) {
			log.error(e);
			setTriggerPostProcessError(e);
			log.error("Preparing/Sending transactions report failed.", e);
		}
	}

	/**
	 * Update Inventory for RM optimization recommendations.
	 */
	public AAUpdateOptimizedInventoryRS updateOptimizedInventory(AAUpdateOptimizedInventoryRQ aaUpdateOptimizedInventoryRQ) {
		AAUpdateOptimizedInventoryRS response = new AAUpdateOptimizedInventoryRS();
		AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
		try {
			response.setMessageId(aaUpdateOptimizedInventoryRQ.getMessageId());

			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.AdminFuncPrivilegesKeys.ALLOW_UPDATE_RM_INV_OPT_RECOMENDATIONS);

			RMUtil.updateOptimizedInventory(aaUpdateOptimizedInventoryRQ);

			status.setSuccess(true);

		} catch (Exception ex) {
			log.error(ex);
			setTriggerPostProcessError(ex);
			status = ExceptionUtil.addAAErrrorsAndWarnings(ex);

		} finally {

			response.setStatusErrorsAndWarnings(status);

		}
		return response;
	}

	/**
	 * Publish Invenry for RM
	 */
	public AAAgentSeatMovemantRS getAgentSeatSellingReport(AAAgentSeatMovemantRQ agentSeatMovemantRQ) {

		AAAgentSeatMovemantRS response = null;
		AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
		try {
			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AdminFuncPrivilegesKeys.ALLOW_RM_INV_BATCH_UPDATE);

			response = RMUtil.getAgentSeatSellingReport(agentSeatMovemantRQ);
			status.setSuccess(true);
		} catch (Exception ex) {
			log.error(ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAAgentSeatMovemantRS();

			status = ExceptionUtil.addAAErrrorsAndWarnings(ex);

		} finally {
			response.setStatusErrorsAndWarnings(status);
		}

		return response;

	}

	/**
	 * Publish Invenry for RM
	 */
	public AAFlightInvBatchUpdateRS batchUpdateFlightInventory(AAFlightInventryBatchUpdateRQ wsAAFlightInventoryBatchRQ) {

		AAFlightInvBatchUpdateRS response = null;
		AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
		try {
			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AdminFuncPrivilegesKeys.ALLOW_RM_INV_BATCH_UPDATE);

			response = RMUtil.updateFltInventoryBatchUpdate(wsAAFlightInventoryBatchRQ);
			status.setSuccess(true);
		} catch (Exception ex) {
			log.error(ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAFlightInvBatchUpdateRS();

			status = ExceptionUtil.addAAErrrorsAndWarnings(ex);

		} finally {
			response.setStatusErrorsAndWarnings(status);
		}
		return response;
	}

	/**
	 * Retrieve Flight Inventory Segment wise allocations
	 */
	public AAFlightInventoryDataRS getFlightInventoryAllocations(AAFlightInventorySearchRQ searchRQ) {
		AAFlightInventoryDataRS response = null;
		AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
		try {
			triggerPreProcessErrorIfPresent();

			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AdminFuncPrivilegesKeys.ALLOW_RM_INV_BATCH_UPDATE);

			response = RMUtil.getFlightInventoryAllocations(searchRQ);
			status.setSuccess(true);
		} catch (Exception ex) {
			log.error(ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAFlightInventoryDataRS();

			status = ExceptionUtil.addAAErrrorsAndWarnings(ex);

		} finally {
			response.setStatusErrorsAndWarnings(status);
		}
		return response;
	}

	/**
	 * Method to get the reservation.
	 * 
	 * @param pnrModesDTO
	 * @param trackingInfoDTO
	 * @return
	 * @throws Exception
	 */
	private Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackingInfoDTO) throws Exception {
		Reservation reservation = null;
		try {
			reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackingInfoDTO);
			if (reservation == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (Exception ex) {
			throw ex;
		}
		return reservation;
	}

	/**
	 * Flight leg load report.
	 */
	public AAFligthtLegPaxCountInfoRS getPaxTypesForFlightLeg(AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ) {
		log.debug("inside webservice method getPaxTypesForFlightLeg");
		AAFligthtLegPaxCountInfoRS response = null;
		AAResultType status = new AAResultType();
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_FLIGHT_LEG_LOAD);
			response = FlightLegLoadReportUtil.getPaxTypesForFlightLeg(flightLetPaxCountRQ);
			status.setSuccess(true);
			return response;
		} catch (Exception ex) {
			log.error("Error occured", ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAFligthtLegPaxCountInfoRS();

			AAStatusErrorsAndWarnings andWarnings = ExceptionUtil.addAAErrrorsAndWarnings(ex);
			status.setErrorCode(andWarnings.getErrorCode());
			status.setErrorText(andWarnings.getErrorText());
			status.setSuccess(andWarnings.isSuccess());
			status.setWarningMessage(andWarnings.getWarningMessage());

		} finally {
			response.setSuccessOrError(status);
		}
		log.debug("finished getPaxTypesForFlightLeg ");
		return response;
	}

	/**
	 * 
	 * 
	 * @param t
	 *            The exception occured
	 */
	public AALoyaltyCustomerProfileRS addCustomerProfile(AALoyaltyCustomerProfileRQ aaLoyaltyCustomerProfile) {
		Collection privilegeKeys = null;
		Hashtable<String, Hashtable<String, Integer>> rs = null;
		ArrayList<LoyaltyCustomerProfile> customerList = new ArrayList<LoyaltyCustomerProfile>();
		short resCode = 0;
		AALoyaltyCustomerProfileRS aaLoyaltyCustomerProfileRS = new AALoyaltyCustomerProfileRS();
		try {
			for (int i = 0; i < aaLoyaltyCustomerProfile.getProfileList().size(); i++) {
				AALoyaltyCustomerProfileType p = aaLoyaltyCustomerProfile.getProfileList().get(i);
				LoyaltyCustomerProfile customer = new LoyaltyCustomerProfile();
				String accountNo = p.getAccountNumber();
				try {
					customer.setLoyaltyAccountNo(accountNo);
				} catch (Exception ne) {
					// do nothing
				}
				customer.setCity(p.getCity());
				customer.setCountryCode(p.getCountryCode());
				try {
					customer.setDateOfBirth(p.getDateOfBirth().toGregorianCalendar().getTime());
				} catch (Exception ne) {
					// do nothing
				}
				customer.setEmail(p.getEmail());
				customer.setMobile(p.getMobileNo());
				customer.setNationalityCode(p.getNationalityCode());
				customer.setCardType(p.getCardType());
				customer.setStatus(p.getStatus());
				customer.setCreatedDate(new Date());

				customerList.add(customer);
			}
			// privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
			// AAUserSession.USER_PRIVILEGES_KEYS);
			// WSReservationUtil.authorize(privilegeKeys,
			// PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADD_LOYALTY_PROFILE);
			rs = WebServicesModuleUtils.getLoyaltyCustomerBD().saveAll(customerList);
			if (rs != null && rs.size() > 0) {
				resCode = 1;
				aaLoyaltyCustomerProfileRS.setResponseCode(Short.valueOf(resCode));
				Enumeration<String> keys = rs.keys();
				while (keys.hasMoreElements()) {
					String key = keys.nextElement();
					Hashtable<String, Integer> value = rs.get(key);
					FailedData fp = new FailedData();
					Enumeration<String> FFkeys = value.keys();
					while (FFkeys.hasMoreElements()) {
						FailedField ff = new FailedField();
						String FFkey = FFkeys.nextElement();
						Integer FFValue = value.get(FFkey);
						ff.setFieldName(FFkey);
						ff.setErrorCode(FFValue);
						fp.getFailedFieldList().add(ff);
					}

					fp.setAccountNumber(key);
					aaLoyaltyCustomerProfileRS.getFailedProfileList().add(fp);
				}
			} else
				resCode = 0;

		} catch (Exception e) {
			log.error(e);
			resCode = 2;
		}
		aaLoyaltyCustomerProfileRS.setResponseCode(resCode);
		return aaLoyaltyCustomerProfileRS;
	}

	public AALoyaltyCreditRS addCustomerCredit(AALoyaltyCreditRQ aaLoyaltyCredit) {
		short resCode = 0; // success
		AALoyaltyCreditRS aaLoyaltyCreditRS = new AALoyaltyCreditRS();
		if (!AppSysParamsUtil.isIntegrateMashreqWithLMS()) {
			Hashtable<String, Hashtable<String, Integer>> rs = null;
			ArrayList<LoyaltyCredit> creditList = new ArrayList<LoyaltyCredit>();
			try {
				for (int i = 0; i < aaLoyaltyCredit.getCreditList().size(); i++) {
					AALoyaltyCreditType p = aaLoyaltyCredit.getCreditList().get(i);
					LoyaltyCredit credit = new LoyaltyCredit();
					String accountNo = p.getAccountNumber();
					try {
						credit.setLoyaltyAccountNo(accountNo);
						credit.setCreditEarned(new BigDecimal(p.getCredit()));
						credit.setCreditBalance(new BigDecimal(p.getCredit()));
					} catch (Exception ne) {
						// do nothing
					}
					try {
						credit.setDateEarn(p.getDateEarn().toGregorianCalendar().getTime());
						GregorianCalendar gc = p.getDateEarn().toGregorianCalendar();
						gc.add(Calendar.YEAR, 2);
						credit.setDateExp(gc.getTime());
					} catch (Exception e) {
						// do no thing
					}
					creditList.add(credit);
				}
				// privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
				// AAUserSession.USER_PRIVILEGES_KEYS);
				// WSReservationUtil.authorize(privilegeKeys,
				// PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANY_CHANNEL);
				rs = WebServicesModuleUtils.getLoyaltyCreditBD().saveAll(creditList);
				if (rs != null && rs.size() > 0) {
					resCode = 1; // has partial errors
					aaLoyaltyCreditRS.setResponseCode(Short.valueOf(resCode));
					Enumeration<String> keys = rs.keys();
					while (keys.hasMoreElements()) {
						String key = keys.nextElement();
						Hashtable<String, Integer> value = rs.get(key);
						FailedData fp = new FailedData();
						Enumeration<String> FFkeys = value.keys();
						while (FFkeys.hasMoreElements()) {
							FailedField ff = new FailedField();
							String FFkey = FFkeys.nextElement();
							Integer FFValue = value.get(FFkey);
							ff.setFieldName(FFkey);
							ff.setErrorCode(FFValue);
							fp.getFailedFieldList().add(ff);
						}

						fp.setAccountNumber(key);
						aaLoyaltyCreditRS.getFailedCreditList().add(fp);
					}
				} else {
					resCode = 0;
				}
			} catch (Exception e) {
				log.error(e);
				resCode = 2;
			}
		}
		aaLoyaltyCreditRS.setResponseCode(resCode);
		return aaLoyaltyCreditRS;
	}

	/**
	 * 
	 * 
	 * @param t
	 *            The exception occured
	 */
	public AALoyaltyCustomerProfileRS setCustomerProfileStatus(AALoyaltyCustomerProfileRQ aaLoyaltyCustomerProfile) {
		Collection privilegeKeys = null;
		Hashtable<String, Hashtable<String, Integer>> rs = null;
		ArrayList<LoyaltyCustomerProfile> customerList = new ArrayList<LoyaltyCustomerProfile>();
		Hashtable<String, Hashtable<String, Integer>> rs2 = new Hashtable<String, Hashtable<String, Integer>>();
		short resCode = 0;
		Hashtable<String, Integer> failedField = new Hashtable<String, Integer>();
		AALoyaltyCustomerProfileRS aaLoyaltyCustomerProfileRS = new AALoyaltyCustomerProfileRS();
		try {
			for (int i = 0; i < aaLoyaltyCustomerProfile.getProfileList().size(); i++) {
				AALoyaltyCustomerProfileType p = aaLoyaltyCustomerProfile.getProfileList().get(i);
				String accountNo = p.getAccountNumber();
				LoyaltyCustomerProfile customer = WebServicesModuleUtils.getLoyaltyCustomerBD().getLoyaltyCustomer(accountNo);
				if (customer != null) {
					customer.setStatus(p.getStatus());
					customerList.add(customer);
				} else {
					failedField.put("accountNo", new Integer(5));
					rs2.put(accountNo, failedField);
				}

			}
			// privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
			// AAUserSession.USER_PRIVILEGES_KEYS);
			// WSReservationUtil.authorize(privilegeKeys,
			// PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADD_LOYALTY_PROFILE);
			rs = WebServicesModuleUtils.getLoyaltyCustomerBD().saveAll(customerList);
			if (rs != null && rs.size() > 0 || rs2.size() > 0) {
				resCode = 1;
				aaLoyaltyCustomerProfileRS.setResponseCode(Short.valueOf(resCode));
				if (rs2.size() > 0) {
					resCode = 1;
					aaLoyaltyCustomerProfileRS.setResponseCode(Short.valueOf(resCode));
					Enumeration<String> keys = rs2.keys();
					while (keys.hasMoreElements()) {
						String key = keys.nextElement();
						Hashtable<String, Integer> value = rs2.get(key);
						FailedData fp = new FailedData();
						Enumeration<String> FFkeys = value.keys();
						while (FFkeys.hasMoreElements()) {
							FailedField ff = new FailedField();
							String FFkey = FFkeys.nextElement();
							Integer FFValue = value.get(FFkey);
							ff.setFieldName(FFkey);
							ff.setErrorCode(FFValue);
							fp.getFailedFieldList().add(ff);
						}

						fp.setAccountNumber(key);
						aaLoyaltyCustomerProfileRS.getFailedProfileList().add(fp);
					}
				}
				if (rs != null && rs.size() > 0) {
					resCode = 1;
					aaLoyaltyCustomerProfileRS.setResponseCode(Short.valueOf(resCode));
					Enumeration<String> keys = rs.keys();
					while (keys.hasMoreElements()) {
						String key = keys.nextElement();
						Hashtable<String, Integer> value = rs.get(key);
						FailedData fp = new FailedData();
						Enumeration<String> FFkeys = value.keys();
						while (FFkeys.hasMoreElements()) {
							FailedField ff = new FailedField();
							String FFkey = FFkeys.nextElement();
							Integer FFValue = value.get(FFkey);
							ff.setFieldName(FFkey);
							ff.setErrorCode(FFValue);
							fp.getFailedFieldList().add(ff);
						}

						fp.setAccountNumber(key);
						aaLoyaltyCustomerProfileRS.getFailedProfileList().add(fp);
					}
				}
			} else
				resCode = 0;

		} catch (Exception e) {
			log.error(e);
			resCode = 2;
		}
		aaLoyaltyCustomerProfileRS.setResponseCode(resCode);
		return aaLoyaltyCustomerProfileRS;
	}

	/**
	 * 
	 * 
	 * @param t
	 *            The exception occured
	 */
	public AALoyaltyCustomerProfileStatusRS getCustomerProfileStatus(
			AALoyaltyCustomerProfileStatusRQ aaLoyaltyCustomerProfileStatus) {

		AALoyaltyCustomerProfileStatusRS aaLoyaltyCustomerProfileStatusRS = new AALoyaltyCustomerProfileStatusRS();
		try {
			for (int i = 0; i < aaLoyaltyCustomerProfileStatus.getAccountList().size(); i++) {
				AALoyaltyCustomerProfileSimpleType p = aaLoyaltyCustomerProfileStatus.getAccountList().get(i);
				String accountNo = p.getAccountNumber();
				LoyaltyCustomerProfile customer = WebServicesModuleUtils.getLoyaltyCustomerBD().getLoyaltyCustomer(accountNo);
				AALoyaltyCustomerProfileSimpleType statusRS = new AALoyaltyCustomerProfileSimpleType();
				if (customer != null) {
					statusRS.setAccountNumber(accountNo);
					statusRS.setStatus(customer.getStatus());
				} else {
					statusRS.setAccountNumber(accountNo);
					statusRS.setStatus(null);
				}
				aaLoyaltyCustomerProfileStatusRS.getAccountList().add(statusRS);

			}
			// privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
			// AAUserSession.USER_PRIVILEGES_KEYS);
			// WSReservationUtil.authorize(privilegeKeys,
			// PrivilegesKeys.WSFuncPrivilegesKeys.WS_ADD_LOYALTY_PROFILE);
		} catch (Exception e) {
			log.error(e);
		}
		return aaLoyaltyCustomerProfileStatusRS;
	}

	/**
	 * Sends agent sales data.
	 * 
	 * @param aapnrPaxTicketingDataRQ
	 * @return
	 */
	public AAPNRPaxTicketingDataRS getAgentSalesData(AAPNRPaxTicketingDataRQ aapnrPaxTicketingDataRQ) {
		AAPNRPaxTicketingDataRS res = null;
		try {
			res = AgentSalesDataUtil.getAgentSalesData(aapnrPaxTicketingDataRQ);
		} catch (Exception ex) {
			log.error(ex);
		}
		return res;
	}

	public AADynBestOffersRS getDynBestOffers(AADynBestOffersRQ aaDynBestOffersRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getDynBestOffers(aaDynBestOffersRQ)");
		}

		AADynBestOffersRS aaDynBestOffersRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			aaDynBestOffersRS = new AvailabilitySearchUtil().getDynamicBestOffers(aaDynBestOffersRQ);
		} catch (Exception ex) {
			log.error("getDynBestOffers(aaDynBestOffersRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaDynBestOffersRS == null)
				aaDynBestOffersRS = new AADynBestOffersRS();
			if (aaDynBestOffersRS.getStatusErrorsAndWarnings() == null)
				aaDynBestOffersRS
						.setStatusErrorsAndWarnings(new com.isaaviation.thinair.webservices.api.dynbestoffers.AAStatusErrorsAndWarnings());
			ExceptionUtil.addAAErrrorsAndWarnings(ex);
		}
		if (log.isDebugEnabled())
			log.debug("END getDynBestOffers(aaDynBestOffersRQ)");
		return aaDynBestOffersRS;

	}

	public AAReceiptUpdateRS updateReceipt(AAReceiptUpdateRQ aaReceiptUpdateRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN updateReceipt(aaReceiptUpdateRQ)");
		}

		AAReceiptUpdateRS aaReceiptUpdateRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			aaReceiptUpdateRS = new ReceiptUpdateUtil().updateReceipt(aaReceiptUpdateRQ);
		} catch (Exception ex) {
			log.error("updateReceipt(aaReceiptUpdateRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaReceiptUpdateRS == null) {
				aaReceiptUpdateRS = new AAReceiptUpdateRS();
			}
			if (aaReceiptUpdateRS.getStatusErrorsAndWarnings() == null) {
				aaReceiptUpdateRS
						.setStatusErrorsAndWarnings(new com.isaaviation.thinair.webservices.api.receiptupdate.AAStatusErrorsAndWarnings());
			}
			aaReceiptUpdateRS.setStatusErrorsAndWarnings(ExceptionUtil.addAAErrrorsAndWarningsForReceiptUpdate(ex));
		}

		if (log.isDebugEnabled())
			log.debug("END updateReceipt(aaReceiptUpdateRQ)");
		return aaReceiptUpdateRS;
	}

	public AAOTAAirMealDetailsRS getMealDetails(AAOTAAirMealDetailsRQ aaOtaAirMealDetailsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getMealDetails(AAOTAAirMealDetailsRQ)");
		}

		AAOTAAirMealDetailsRS aaOtaAirMealDetailsRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			aaOtaAirMealDetailsRS = new MealDetailsUtil().getMealDetails(aaOtaAirMealDetailsRQ);
		} catch (Exception ex) {
			log.error("getMealDetails(AAOTAAirMealDetailsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOtaAirMealDetailsRS == null) {
				aaOtaAirMealDetailsRS = new AAOTAAirMealDetailsRS();
			}
			if (aaOtaAirMealDetailsRS.getErrors() == null) {
				aaOtaAirMealDetailsRS.setErrors(new ErrorsType());
			}
			if (aaOtaAirMealDetailsRS.getWarnings() == null) {
				aaOtaAirMealDetailsRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOtaAirMealDetailsRS.getErrors().getError(), aaOtaAirMealDetailsRS
					.getWarnings().getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getMealDetails(AAOTAAirMealDetailsRQ)");
		}

		return aaOtaAirMealDetailsRS;
	}

	public AAOTAAirMultiMealDetailsRS getMultiMealDetails(AAOTAAirMultiMealDetailsRQ aaOtaAirMultiMealDetailsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getMealDetails(AAOTAAirMealDetailsRQ)");
		}

		AAOTAAirMultiMealDetailsRS aaOtaAirMultiMealDetailsRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			aaOtaAirMultiMealDetailsRS = new MealDetailsUtil().getMultiMealDetails(aaOtaAirMultiMealDetailsRQ);
		} catch (Exception ex) {
			log.error("getMealDetails(AAOTAAirMealDetailsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOtaAirMultiMealDetailsRS == null) {
				aaOtaAirMultiMealDetailsRS = new AAOTAAirMultiMealDetailsRS();
			}
			if (aaOtaAirMultiMealDetailsRS.getErrors() == null) {
				aaOtaAirMultiMealDetailsRS.setErrors(new ErrorsType());
			}
			if (aaOtaAirMultiMealDetailsRS.getWarnings() == null) {
				aaOtaAirMultiMealDetailsRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOtaAirMultiMealDetailsRS.getErrors().getError(),
					aaOtaAirMultiMealDetailsRS.getWarnings().getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getMealDetails(AAOTAAirMealDetailsRQ)");
		}

		return aaOtaAirMultiMealDetailsRS;
	}

	/**
	 * Triggers error if there any preprocessing error occured. All the service methods should call this method as the
	 * first step.
	 * 
	 * @throws WebservicesException
	 */
	private void triggerPreProcessErrorIfPresent() throws WebservicesException {
		Boolean triggerError = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_PREPROCESS_ERROR);
		if (triggerError != null && triggerError.booleanValue()) {
			throw (WebservicesException) ThreadLocalData.getWSContextParam(
					WebservicesContext.PREPROCESS_ERROR_OBJECT);
		}

	}

	/**
	 * Specifies whether or not an exception occured during processing.
	 * 
	 * @param t
	 *            The exception occured
	 */
	private void setTriggerPostProcessError(Throwable t) {
		ThreadLocalData.setTriggerPostProcessError(t);
	}

	public AAOTAAirSSRDetailsRS getSSRDetails(AAOTAAirSSRDetailsRQ aaOTAAirSSRDetailsRQ) {

		AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			aaOtaAirSSRDetailsRS = new SSRDetailsUtil().getSSRDetails(aaOTAAirSSRDetailsRQ);
		} catch (Exception ex) {
			log.error("getSSRDetails(AAOTAAirSSRDetailsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOtaAirSSRDetailsRS == null) {
				aaOtaAirSSRDetailsRS = new AAOTAAirSSRDetailsRS();
			}
			if (aaOtaAirSSRDetailsRS.getErrors() == null) {
				aaOtaAirSSRDetailsRS.setErrors(new ErrorsType());
			}
			if (aaOtaAirSSRDetailsRS.getWarnings() == null) {
				aaOtaAirSSRDetailsRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOtaAirSSRDetailsRS.getErrors().getError(), aaOtaAirSSRDetailsRS
					.getWarnings().getWarning(), ex);
		}

		return aaOtaAirSSRDetailsRS;
	}

	public AAOTAAirBaggageDetailsRS getBaggageDetails(AAOTAAirBaggageDetailsRQ aaOtaAirBaggageDetailsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getBaggageDetails(AAOTAAirBaggageDetailsRQ)");
		}

		AAOTAAirBaggageDetailsRS aaOtaAirBaggageDetailsRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			aaOtaAirBaggageDetailsRS = new BaggageDetailsUtil().getBaggageDetails(aaOtaAirBaggageDetailsRQ, privilegeKeys);
		} catch (Exception ex) {
			log.error("getBaggageDetails(AAOTAAirBaggageDetailsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOtaAirBaggageDetailsRS == null) {
				aaOtaAirBaggageDetailsRS = new AAOTAAirBaggageDetailsRS();
			}
			if (aaOtaAirBaggageDetailsRS.getErrors() == null) {
				aaOtaAirBaggageDetailsRS.setErrors(new ErrorsType());
			}
			if (aaOtaAirBaggageDetailsRS.getWarnings() == null) {
				aaOtaAirBaggageDetailsRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOtaAirBaggageDetailsRS.getErrors().getError(), aaOtaAirBaggageDetailsRS
					.getWarnings().getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getBaggageDetails(AAOTAAirBaggageDetailsRQ)");
		}

		return aaOtaAirBaggageDetailsRS;
	}

	public AAOTAETicketStatusUpdateRS updateEticketStatus(AAOTAETicketStatusUpdateRQ eTicketStatusUpdateRQ) {
		return ETicketUpdateUtil.updateEticketStatus(eTicketStatusUpdateRQ);
	}

	/**
	 * Accelero DCS will use this WS to update PFS with ther reservation system.
	 * 
	 * @param aaOtaPfsUpdateRQ
	 * @return
	 */
	public AAOTAPFSUpdateRS updatePFS(AAOTAPFSUpdateRQ aaOtaPfsUpdateRQ) {
		return PFSXmlUtil.updatePfs(aaOtaPfsUpdateRQ);
	}

	/**
	 * Connection Flight Load
	 */
	public AAIOBoundPaxCountInfoRS getIOBoundPaxCountInfo(AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ) {
		log.debug("inside webservice method getIOBoundPaxCountInfo");
		AAIOBoundPaxCountInfoRS response = null;
		AAResultType status = new AAResultType();
		try {
			triggerPreProcessErrorIfPresent();
			Collection<String> privilegeKeys = ThreadLocalData
					.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			// TODO implement a different privilege for in bound out bound count
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_FLIGHT_LEG_LOAD);
			response = ConnectionFlightLoadUtil.getInboundOutboundPaxCount(flightLetPaxCountRQ);
			status.setSuccess(true);
			return response;
		} catch (Exception ex) {
			log.error("Error occured", ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAIOBoundPaxCountInfoRS();

			AAStatusErrorsAndWarnings andWarnings = ExceptionUtil.addAAErrrorsAndWarnings(ex);
			status.setErrorCode(andWarnings.getErrorCode());
			status.setErrorText(andWarnings.getErrorText());
			status.setSuccess(andWarnings.isSuccess());
			status.setWarningMessage(andWarnings.getWarningMessage());

		} finally {
			response.setSuccessOrError(status);
		}
		log.debug("finished getIOBoundPaxCountInfo ");
		return response;
	}
	
	public AATypeBReservationRS processTypeBResMessage(AATypeBReservationRQ typeBReservationRQ) {
		return TypeBResMsgProcessUtill.processTypeBResMessage(typeBReservationRQ);
	}
	
	/**
	 * getTaxInvoicebyPNR - Uses aaOTAAirTaxInvoiceGetRQ.UniqueID for search
	 * 
	 * @param aaOTAAirTaxInvoiceGetRQ
	 * @return
	 */
	public AAOTAAirTaxInvoiceGetRS getTaxInvoicebyPNR(AAOTAAirTaxInvoiceGetRQ aaOTAAirTaxInvoiceGetRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getTaxInvoicebyPNR(aaOTAAirTaxInvoiceGetRQ)");
		}
		AAOTAAirTaxInvoiceGetRS aaOTAAirTaxInvoiceGetRS = new AAOTAAirTaxInvoiceGetRS();

		try {
			triggerPreProcessErrorIfPresent();
			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS,
					new Boolean(true));

			TransformInterlineUtil transformInterlineUtil = new TransformInterlineUtil();			
			transformInterlineUtil.createTaxInvoiceByPnrResponse(aaOTAAirTaxInvoiceGetRS, aaOTAAirTaxInvoiceGetRQ);
		} catch (Exception ex) {
			log.error("getReservationbyPNR(OTAReadRQ) failed", ex);
			setTriggerPostProcessError(ex);

//			ExceptionUtil.addOTAErrrorsAndWarnings(aaOTAAirTaxInvoiceGetRS.getErrors().getError(),
//					aaOTAAirTaxInvoiceGetRS.getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END getTaxInvoicebyPNR(aaOTAAirTaxInvoiceGetRQ)");
		}

		return aaOTAAirTaxInvoiceGetRS;
	}
	
	/**
	 * Returns Availability
	 */
	public AAOTAAirAllPriceAvailRS getAllPriceAvailability(OTAAirAvailRQ oTAAirAvailRQ) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getAllPriceAvailability(OTAAirAvailRQ)");
		}

		AAOTAAirAllPriceAvailRS otaAirAvailRS = null;
		try {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Not Implemented.No support for non LCC flow");

		} catch (Exception ex) {
			log.error("getAllPriceAvailability(OTAAirAvailRQ) failed.", ex);
			if (log.isInfoEnabled()) {
				OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AVAILABILITY_RESPONSE_START, null,
						null, null);
			}
			setTriggerPostProcessError(ex);
			if (otaAirAvailRS == null)
				otaAirAvailRS = new AAOTAAirAllPriceAvailRS();
			if (otaAirAvailRS.getErrors() == null)
				otaAirAvailRS.setErrors(new ErrorsType());
			if (otaAirAvailRS.getWarnings() == null)
				otaAirAvailRS.setWarnings(new WarningsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirAvailRS.getErrors().getError(),
					otaAirAvailRS.getWarnings().getWarning(), ex);
		}
		if (log.isDebugEnabled()) {
			log.debug("END getAllPriceAvailability(OTAAirAvailRQ)");
		}

		return otaAirAvailRS;
	}

	public AAFlightLoadInfoRS getFlightLoadInfo(AAFlightLoadInfoRQ flightLoadInfoRQ) {
		log.debug("inside webservice method getFlightLoadInfo");
		AAFlightLoadInfoRS response = null;
		AAResultType status = new AAResultType();
		Collection<String> privilegeKeys = null;
		try {
			triggerPreProcessErrorIfPresent();
			privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.WSFuncPrivilegesKeys.WS_RES_ALLOW_VIEW_FLIGHT_LOAD_INFO);
			response = FlightLoadInfoUtil.getFlightLoadInfo(flightLoadInfoRQ);
			status.setSuccess(true);
			return response;
		} catch (Exception ex) {
			log.error("Error occured", ex);
			setTriggerPostProcessError(ex);
			if (response == null)
				response = new AAFlightLoadInfoRS();

			AAStatusErrorsAndWarnings andWarnings = ExceptionUtil.addAAErrrorsAndWarnings(ex);
			status.setErrorCode(andWarnings.getErrorCode());
			status.setErrorText(andWarnings.getErrorText());
			status.setSuccess(andWarnings.isSuccess());
			status.setWarningMessage(andWarnings.getWarningMessage());

		} finally {
			response.setSuccessOrError(status);
		}
		log.debug("finished getFlightLoadInfo ");
		return response;
	}

	/**
	 * Load SSRs that the user can define the amount (Changed is done for GOQUO)
	 */
	public AAOTAAirSSRDetailsRS getUserDefinedSSRDetails(AAOTAAirSSRDetailsRQ aaotaAirSSRDetailsRQ) {

		AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS = null;

		try {
			triggerPreProcessErrorIfPresent();
			aaOtaAirSSRDetailsRS = new SSRDetailsUtil().getUserDefinedSSRDetails(aaotaAirSSRDetailsRQ);
		} catch (Exception ex) {
			log.error("getUserDefinedSSRDetails(AAOTAAirSSRDetailsRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaOtaAirSSRDetailsRS == null) {
				aaOtaAirSSRDetailsRS = new AAOTAAirSSRDetailsRS();
			}
			if (aaOtaAirSSRDetailsRS.getErrors() == null) {
				aaOtaAirSSRDetailsRS.setErrors(new ErrorsType());
			}
			if (aaOtaAirSSRDetailsRS.getWarnings() == null) {
				aaOtaAirSSRDetailsRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaOtaAirSSRDetailsRS.getErrors().getError(),
					aaOtaAirSSRDetailsRS.getWarnings().getWarning(), ex);
		}

		return aaOtaAirSSRDetailsRS;

	}
	
	/**
	 * Set Amount of a user defined SSR
	 */
	public AAOTAAirSSRSetAmountRS setUserDefinedSSRAmount(AAOTAAirSSRSetAmountRQ aaOTAAirSSRSetAmountRQ){
		
		AAOTAAirSSRSetAmountRS aaotaAirSSRSetAmountRS = null;
		
		try{
			triggerPreProcessErrorIfPresent();
			aaotaAirSSRSetAmountRS = new SSRDetailsUtil().setSSRPrice(aaOTAAirSSRSetAmountRQ);
			
		}catch(Exception ex){
			log.error("setUserDefinedSSRAmount(aaOTAAirSSRSetAmountRQ) failed.", ex);
			setTriggerPostProcessError(ex);
			if (aaotaAirSSRSetAmountRS == null) {
				aaotaAirSSRSetAmountRS = new AAOTAAirSSRSetAmountRS();
			}
			if (aaotaAirSSRSetAmountRS.getErrors() == null) {
				aaotaAirSSRSetAmountRS.setErrors(new ErrorsType());
			}
			if (aaotaAirSSRSetAmountRS.getWarnings() == null) {
				aaotaAirSSRSetAmountRS.setWarnings(new WarningsType());
			}
			ExceptionUtil.addOTAErrrorsAndWarnings(aaotaAirSSRSetAmountRS.getErrors().getError(),
					aaotaAirSSRSetAmountRS.getWarnings().getWarning(), ex);
			aaotaAirSSRSetAmountRS.setSSRSetAmountSucsses(false);
		}
		
		return aaotaAirSSRSetAmountRS;
		
	}
}