/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.interceptor;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBAccessException;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.transport.http.XFireServletController;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;

import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isa.thinair.webservices.core.session.SessionGateway;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.transaction.TransactionGateway;
import com.isa.thinair.webservices.core.util.ThruReflection;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Service Interceptor. Handles Transaction injection, perform common request/response attributes manipulation and calls
 * cleaups at the end of the service call.
 * 
 * @author Nasly
 */
public class ServiceInterceptor implements MethodInterceptor {
	
	final private static String WS_INTERCEPTOR = "ws-interceptor";
	private static Log logger = LogFactory.getLog(WS_INTERCEPTOR);
	
	private final Log log = LogFactory.getLog(getClass());


	public Object invoke(MethodInvocation mi) throws Throwable {

		if (log.isDebugEnabled()) {
			log.debug("ServiceInterceptor [" + mi.getMethod().getDeclaringClass().getName() + "::" + mi.getMethod().getName()
					+ "]");
		}

		long startTime = System.currentTimeMillis();

		Map extractedReqParams = null;
		Object response = null;

		try {
			extractedReqParams = beforeServiceCall(mi);
		} catch (Throwable t) {
			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_PREPROCESS_ERROR, new Boolean(true));
			WebservicesException wse = null;
			if (t != null && t instanceof WebservicesException) {
				wse = (WebservicesException) t;
			}else if (t != null && t instanceof EJBAccessException) {
				log.error("Authenticated username/password does not match with supplied credentials " + "[userId=" + USER_ID
						+ "]");
				throw new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION,
						"Authenticated username/password does not match with supplied credentials " + "[userId=" + USER_ID
								+ "]");
			}else if (t != null && t instanceof Exception) {
				wse = new WebservicesException((Exception) t);
			} else {
				String msg = "Error in Invoking service [" + mi.getMethod().getName() + "]";
				wse = new WebservicesException(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED, t != null ? msg
						+ t.getMessage() : msg);
			}
			ThreadLocalData.setWSConextParam(WebservicesContext.PREPROCESS_ERROR_OBJECT, wse);
		}

		boolean serviceCallSucces = false;
		try {
			// call the service
			response = mi.proceed();
			serviceCallSucces = true;
			return response;
		} catch (Throwable t) {
			String msg = "Invoking service [" + mi.getMethod().getName() + "] failed. ";
			log.error(msg, t);
			throw new UndeclaredThrowableException(t, t != null ? msg + t.getMessage() : msg);
		} finally {
			
			String sessionId = 	(String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
						
			String transactionId = null;
			ITransaction currTnx = ThreadLocalData.getCurrentTransaction();
			if (currTnx != null) {
				transactionId = currTnx.getId();
			}
			
			String forwardingIP = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP);
			
			String userId = null;
			UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
			if (userPrincipal != null) {
				userId = userPrincipal.getUserId();
			}
			
			if (logger.isDebugEnabled()) {
				logger.debug("WS_OPERATION=<" + mi.getMethod().getName() + ">,BS_SUCCESS=<" + serviceCallSucces
						+ ">,RESPONSE_TIME=<" + (System.currentTimeMillis() - startTime) + ">,SESSION_ID=<" + sessionId
						+ ">,TRANSACTION_ID=<" + transactionId + ">,IP=<" + forwardingIP + ">,USER_ID=<" + userId + ">");
			}
	    		
			if (!AAUtils.isAAMethod(mi)) {
				afterServiceCall(mi, extractedReqParams, response);
			}
			
		}
	}

	/**
	 * Things to do before service call.
	 * 
	 * @param mi
	 * @throws Exception
	 */
	private Map beforeServiceCall(MethodInvocation mi) throws Exception {
		Boolean triggerError = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_PREPROCESS_ERROR);
		boolean triggerErrorBool = triggerError != null && triggerError.booleanValue();
		Map requestParams = null;
		String defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();

		String sessionId = null;
		HttpSession httpSession = null;
		try {
			httpSession = XFireServletController.getRequest().getSession();
			if (httpSession != null) {
				/*
				 * access session so that web container initiate the session ?
				 */
				sessionId = httpSession.getId();
				log.debug("HTTP sessionID=" + sessionId);
			} else {
				log.debug("HTTP sessionID is null");
			}
		} catch (Exception e) {
			log.error("HTTP session initialization failed", e);
		}

		// To maintain a seperate set of request parameters for Fawry WebService
		// functionalities
		if (AAUtils.isFAWRYMethod(mi)) {

			String loginName = null, loginPass = null, defaultSalesChannelId = null, companyCode = null;

			Map userCredentials = AppSysParamsUtil.getFAWRYCredentials();

			if (userCredentials != null) {
				loginName = (String) userCredentials.get(WebservicesConstants.FAWRYConstants.LOGIN_NAME);
				if (loginName != null && loginName.length() > 0) {
					loginName = defaultAirlineCode + loginName;
				}
				loginPass = (String) userCredentials.get(WebservicesConstants.FAWRYConstants.LOGIN_PASS);
				defaultSalesChannelId = (String) userCredentials
						.get(WebservicesConstants.FAWRYConstants.Default_SALES_CHANNEL_ID);
				companyCode = (String) userCredentials.get(WebservicesConstants.FAWRYConstants.Company_Code);
			} else {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
						"FAWRY credential is not configure in Accelero. Please report!!");
			}

			if (log.isDebugEnabled()) {
				log.debug("ServiceInterceptor [ Operation = " + mi.getMethod().getName() + ", UserId = " + loginName
						+ ", SessionId = " + sessionId + " ]");
			}

			// Bind the current WebservicesContext to the current thread
			ThreadLocalData.setCurrentWSContext(new WebservicesContext());

			ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_NAME, loginName);

			ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_PASS, loginPass);

			ThreadLocalData.getCurrentWSContext()
					.setParameter(WebservicesConstants.FAWRYConstants.Company_Code, companyCode);

			try {
				// Propagate the request ip to be used for Fraud check
				String forwardingIP = getHTTPHeaderValue(XFireServletController.getRequest(), "X-Forwarded-For");
				
				// If there are two ip addresses, take the first one
				if (forwardingIP != null && forwardingIP.length() != 0) {
					if (forwardingIP.contains(",")) {
						String[] ips = forwardingIP.split(",");
						if (ips.length > 0) {
							forwardingIP = ips[0].trim();
						}
					}
				}

				if (forwardingIP != null) {

					if (log.isDebugEnabled()) {
						log.debug("Forwarding IP for Fawry request is : " + forwardingIP);
					}
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP, forwardingIP);
				} else {
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP,
							XFireServletController.getRequest().getRemoteAddr());
				}

			} catch (Exception e) {
				log.error("HTTP header.host not found", e);
			}

			// Fawry Access Contorl List Validation

			String requestIp = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP);

			if (requestIp == null || requestIp.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Sender_is_not_authorized,
						"Sender IP Address is Blank.");

			List<String> fawryAccessControlList = AppSysParamsUtil.getFAWRYAccessControlList();

			if (fawryAccessControlList != null) {

				if (!(fawryAccessControlList.contains(requestIp))) {
					log.error("Un Authorize IP request for Fawry IP is : " + requestIp);
					throw new WebservicesException(
							WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Authentication_Error,
							"Sender IP Address is not allowed.");
				}
			}

			if (httpSession != null) {
				/*
				 * Propagate the web session to be send to CC Fraud check
				 */
				ThreadLocalData.setWSConextParam(WebservicesContext.WEB_SESSION_ID, sessionId);

			} else {
				log.debug("HTTP sessionID is null");
			}

			// temporary set sales channel to default to pass ejb authentication
			// process
			ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID, defaultSalesChannelId);

			handleAuthentication(mi);
			return requestParams;
		}

		if (AAUtils.isAmadeusMethod(mi)) {
			String loginName = null, loginPass = null, defaultSalesChannelId = null, companyCode = null;

			Map userCredentials = AppSysParamsUtil.getAmadeusCredentials();

			if (userCredentials != null) {
				loginName = (String) userCredentials.get(WebservicesConstants.AmadeusConstants.LOGIN_NAME);
				if (loginName != null && loginName.length() > 0) {
					loginName = defaultAirlineCode + loginName;
				}
				loginPass = (String) userCredentials.get(WebservicesConstants.AmadeusConstants.LOGIN_PASS);
				defaultSalesChannelId = (String) userCredentials
						.get(WebservicesConstants.AmadeusConstants.DEFAULT_SALES_CHANNEL_ID);
			} else {
				// FiXME the error code
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
						"Amadeus credentials not configured in Accelero. Please report!!");
			}

			if (log.isDebugEnabled()) {
				log.debug("ServiceInterceptor [ Operation = " + mi.getMethod().getName() + ", UserId = " + loginName
						+ ", SessionId = " + sessionId + " ]");
			}

			// Bind the current WebservicesContext to the current thread
			ThreadLocalData.setCurrentWSContext(new WebservicesContext());

			ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_NAME, loginName);

			ThreadLocalData.getCurrentWSContext().setParameter(WebservicesContext.LOGIN_PASS, loginPass);

			try {
				// Propagate the request ip to be used for Fraud check
				String forwardingIP = getHTTPHeaderValue(XFireServletController.getRequest(), "X-Forwarded-For");
				// If there are two ip addresses, take the first one
				if (forwardingIP != null && forwardingIP.length() != 0) {
					if (forwardingIP.contains(",")) {
						String[] ips = forwardingIP.split(",");
						if (ips.length > 0) {
							forwardingIP = ips[0].trim();
						}
					}
				}
				if (forwardingIP != null)
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP, forwardingIP);
				else
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP,
							XFireServletController.getRequest().getRemoteAddr());

			} catch (Exception e) {
				log.error("HTTP header.host not found", e);
			}

			// Fawry Access Contorl List Validation

			String requestIp = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP);

			if (requestIp == null || requestIp.equals(""))
				// FIXME the error code
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Sender_is_not_authorized,
						"Sender IP Address is Blank.");

			List<String> amadeusACL = AppSysParamsUtil.getAmadeusAccessControlList();

			if (amadeusACL != null) {
				if (!(amadeusACL.contains(requestIp)))
					// FIXME the error code
					throw new WebservicesException(
							WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Authentication_Error,
							"Sender IP Address is not allowed.");
			}

			if (httpSession != null) {
				/*
				 * Propagate the web session to be send to CC Fraud check
				 */
				ThreadLocalData.setWSConextParam(WebservicesContext.WEB_SESSION_ID, sessionId);
			} else {
				log.debug("HTTP sessionID is null");
			}

			// temporary set sales channel to default to pass ejb authentication
			// process
			ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID, defaultSalesChannelId);

			handleAuthentication(mi);
			return requestParams;
		}

		// To maintain a seperate set of request parameters for AccelAero Admin
		// functionalities
		if (AAUtils.isAAMethod(mi)) {
			requestParams = extractRequestParamsForAA(mi.getArguments());
			String loginId = null;
			loginId = (String) requestParams.get(USER_ID);
			if (loginId != null && loginId.length() > 0) {
				loginId = defaultAirlineCode + loginId;
			}
			
			if (log.isDebugEnabled()) {
				log.debug("ServiceInterceptor [ Operation = " + mi.getMethod().getName() + ", UserId = " + loginId + ", SessionId = "
						+ sessionId + " ]");
			}

			ThreadLocalData.setWSConextParam(WebservicesContext.LOGIN_NAME, loginId);
			ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID,
					(String) requestParams.get(SALES_CHANNEL));
			handleAuthentication(mi);
			return requestParams;
		}

		if (AAUtils.isLoginApiMethod(mi) || AAUtils.isMealServicesMethod(mi)) {
			String userName = (String) ThreadLocalData.getCurrentWSContext()
					.getParameter(WebservicesContext.LOGIN_NAME);

			if (userName != null && userName.length() > 0) {
				userName = defaultAirlineCode + userName;
			}


			ThreadLocalData.setWSConextParam(WebservicesContext.LOGIN_NAME, userName );

			if (AAUtils.isLoginApiMethod(mi)) {
				ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID,
						String.valueOf(SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES));
			} else if (AAUtils.isMealServicesMethod(mi)) {
				ThreadLocalData.setWSConextParam(WebservicesContext.SALES_CHANNEL_ID,
						String.valueOf(SalesChannelsUtil.SALES_CHANNEL_CREW_MGMT));
			}
			handleAuthentication(mi);
			return requestParams;
		}

		// from here onwards defualt behaviour for OTA
		requestParams = extractRequestParams(mi.getArguments());

		if (triggerErrorBool)
			return requestParams;
		String userId = null;
		if (WebServicesModuleUtils.getWebServicesConfig().getNoPOSRequiredMethods() == null
				|| !WebServicesModuleUtils.getWebServicesConfig().getNoPOSRequiredMethods().contains(mi.getMethod().getName())) {
			// Extract POS information
			SourceType source = extractPOS(mi.getArguments());
			ThreadLocalData.setWSConextParam(WebservicesContext.REQUEST_POS, source);
			userId = (String) ThreadLocalData.getCurrentWSContext().getParameter(WebservicesContext.LOGIN_NAME);
			if (userId != null && userId.length() > 0) {
				userId = defaultAirlineCode + userId;
			}
			ThreadLocalData.setWSConextParam(WebservicesContext.LOGIN_NAME, userId);
		}

		ITransaction existingTnx = null;
		Date lastAccessedTime = null;
		if ((String) requestParams.get(TRANSACTION_ID) != null && !"".equals((String) requestParams.get(TRANSACTION_ID))) {
			existingTnx = WebServicesModuleUtils.getTrasnsactionStore().getTransaction(
					(String) requestParams.get(TRANSACTION_ID));
		}
		if (existingTnx != null) {
			lastAccessedTime = new Date(existingTnx.getLastAccessedTimestamp());
		}

		if (log.isDebugEnabled()) {
			log.debug("ServiceInterceptor [ Operation = " + mi.getMethod().getName() + ", UserId = " + userId + ", SessionId = "
					+ sessionId + ", TransactionId = " + (String) requestParams.get(TRANSACTION_ID) + ", LastAccessedTime = "
					+ lastAccessedTime + " ]");
		}

		if (WebServicesModuleUtils.getWebServicesConfig().isTransactionValidityCheck()) {
			if (existingTnx != null && !existingTnx.getSessionId().equals(sessionId)) {
				log.error("Requested transaction does not match with requested session [requestedTnxId="
						+ (String) requestParams.get(TRANSACTION_ID) + ", SessionId=" + sessionId + ", ActualSessionIdOfTnx="
						+ existingTnx.getSessionId() + ", UserId=" + userId + "]");
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Requested transaction does not match with requested session [requestedTnxId="
								+ (String) requestParams.get(TRANSACTION_ID) + ", SessionId=" + sessionId
								+ ", ActualSessionIdOfTnx=" + existingTnx.getSessionId() + ", UserId=" + userId + "]");
			}
		}

		WebServiceTransaction aaTnxAnnotation = mi.getMethod().getAnnotation(WebServiceTransaction.class);

		TransactionGateway.TransactionInHandler.handleTransactionIn((String) requestParams.get(TRANSACTION_ID), aaTnxAnnotation);

		if (WebServicesModuleUtils.getWebServicesConfig().getNoPOSRequiredMethods() == null
				|| !WebServicesModuleUtils.getWebServicesConfig().getNoPOSRequiredMethods().contains(mi.getMethod().getName())) {

			// Handles the authentication mechanism
			handleAuthentication(mi);
		}

		return requestParams;
	}

	/**
	 * In order to handle authentication for OTA Related - default
	 */
	private void handleAuthentication(MethodInvocation mi) {
		String userName = (String) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.LOGIN_NAME);
		String password = (String) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.LOGIN_PASS);
		try {
			Integer salesChannelId = null;
			if (AAUtils.isAAMethod(mi) || AAUtils.isFAWRYMethod(mi) || AAUtils.isAmadeusMethod(mi) ||
					AAUtils.isLoginApiMethod(mi) || AAUtils.isMealServicesMethod(mi)) {
				salesChannelId = Integer.parseInt((String) ThreadLocalData.getCurrentWSContext()
						.getParameter(WebservicesContext.SALES_CHANNEL_ID));
			} else {
				SourceType sourceType = (SourceType) ThreadLocalData.getCurrentWSContext()
						.getParameter(WebservicesContext.REQUEST_POS);
				salesChannelId = Integer.parseInt(sourceType.getBookingChannel().getType());

				// Validate the POS userId against the userId to be authenticated
				String posUserId = sourceType.getRequestorID().getID();
				if (posUserId != null && posUserId.length() > 0) {
					posUserId = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + posUserId;
				}
				if (userName == null || "".equals(userName) || !userName.equals(posUserId)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"RequestorID in POS information is incorrect " + "[ID received in POS = " + posUserId + "]");
				}
			}

			// Invoke client login module
			ForceLoginInvoker.webserviceLogin(userName, password, salesChannelId);
			// Initialize user session, if not already exists
			SessionGateway.SessionInHandler.handleSessionIn(userName, password, salesChannelId);
			log.debug("SecurityHandler:: Successfully authenticated user [userId=" + userName + "]");
		} catch (LoginException le) {
			String msg = "Authentication failed for [userId=" + userName + "]";
			ThreadLocalData.setTriggerPreProcessError(
					new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION, msg));
			log.error(msg, le);
		}catch (EJBAccessException eje) {
			String msg = "Authentication failed for [userId=" + userName + "]";
			ThreadLocalData.setTriggerPreProcessError(
					new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION, msg));
			log.error(msg, eje);
		}
		catch (WebservicesException we) {
			ThreadLocalData.setTriggerPreProcessError(we);
			log.error("User session creation failed [userId=" + userName + "]", we);
		}
	}

	/**
	 * Extracts POS information from request.
	 * 
	 * @param arguments
	 * @return
	 * @throws Exception
	 */
	private SourceType extractPOS(Object[] arguments) throws Exception {

		SourceType source = null;
		POSType posType = (POSType) ThruReflection.getAttributeValue(arguments[0], "POS");

		if (posType == null || posType.getSource().size() == 0) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "POS information must be supplied.");
		}

		source = (SourceType) posType.getSource().get(0);

		if (source.getRequestorID() == null || source.getRequestorID().getID() == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "RequesterID is POS must be supplied");
		}

		if (source.getBookingChannel() == null || source.getBookingChannel().getType() == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "BookingChannel is POS must be supplied");
		}
		return source;
	}

	/**
	 * Things to do after service call.
	 * 
	 * @param mi
	 */
	private void afterServiceCall(MethodInvocation mi, Map extractedReqParams, Object response) {
		try {

			if (AAUtils.isFAWRYMethod(mi) || AAUtils.isAmadeusMethod(mi)) {

				log.debug("In After Service Call for Fawery operation: Handling post process transaction managment");
				// 1. Handle Prostprocess Error
				ITransaction currTnx = ThreadLocalData.getCurrentTransaction();
				Boolean isNewTxnCreated = (Boolean) ThreadLocalData.getWSContextParam(
						WebservicesContext.TRIGGER_NEW_TXN_CREATED);
				if (isNewTxnCreated != null && isNewTxnCreated.booleanValue()) {
					Boolean postProcessExTriggered = (Boolean) ThreadLocalData.getWSContextParam(
							WebservicesContext.TRIGGER_POSTPROCESS_ERROR);
					if (postProcessExTriggered != null && postProcessExTriggered.booleanValue()) {
						WebServicesModuleUtils.getTrasnsactionStore().forceExpireAndRemoveTransaction(currTnx.getId());
					}
				}

			} else if (WebServicesModuleUtils.getWebServicesConfig().getOnewayMethods() == null
					|| !WebServicesModuleUtils.getWebServicesConfig().getOnewayMethods().contains(mi.getMethod().getName())) {

				if (response != null) {

					// Set common reponse parameters
					Map<String, Object> resParams = new HashMap<String, Object>();
					resParams.put(VERSION, new BigDecimal("2006.01"));
					resParams.put(PRIMARY_LAGN_ID, WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
					if (extractedReqParams != null) {
						resParams.put(ECHO_TOKEN, extractedReqParams.get(ECHO_TOKEN));
						resParams.put(SEQUENCE_NUMBER, extractedReqParams.get(SEQUENCE_NUMBER));
					}

					ITransaction currTnx = ThreadLocalData.getCurrentTransaction();
					Boolean isNewTxnCreated = (Boolean) ThreadLocalData.getWSContextParam(
							WebservicesContext.TRIGGER_NEW_TXN_CREATED);
					
					if (isNewTxnCreated != null && isNewTxnCreated.booleanValue()) {
						Boolean postProcessExTriggered = (Boolean) ThreadLocalData.getWSContextParam(
								WebservicesContext.TRIGGER_POSTPROCESS_ERROR);
						if (postProcessExTriggered != null && postProcessExTriggered.booleanValue() && currTnx != null) {
							WebServicesModuleUtils.getTrasnsactionStore().forceExpireAndRemoveTransaction(currTnx.getId());
							currTnx = null;
						}
					}

					if (currTnx != null) {
						resParams.put(TRANSACTION_ID, currTnx.getId());
					}
					setResponseParams(response, resParams);

				}
			}
		} catch (Throwable ex) {
			log.error("Setting Common Response Parameters failed.", ex);
		}

		try {
			WebServiceTransaction aaTnxAnnotation = mi.getMethod().getAnnotation(WebServiceTransaction.class);
			TransactionGateway.TransactionOutHandler.handleTransactionOut(aaTnxAnnotation);
		} catch (Throwable ex) {
			log.error("Transaction out handler failed", ex);
		}

		try {
			SessionGateway.SessionOutHandler.handleSessionOut();
		} catch (Throwable ex) {
			log.error("Sessioin out handler failed", ex);
		}
	}

	private Map<String, Object> extractRequestParamsForAA(Object[] objects) throws Exception {
		Map<String, Object> reqParams = null;
		if (objects != null && objects.length > 0) {
			reqParams = ThruReflection.getAttributeValues(objects[0], new String[] { SALES_CHANNEL, USER_ID });
		}
		return reqParams;
	}

	/**
	 * Extract TransactionIndetifier from the request.
	 * 
	 * @param objects
	 *            [] - first element must be OTA Request Object
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> extractRequestParams(Object[] objects) throws Exception {
		Map<String, Object> reqParams = null;
		if (objects != null && objects.length > 0) {
			reqParams = ThruReflection.getAttributeValues(objects[0],
					new String[] { TRANSACTION_ID, ECHO_TOKEN, SEQUENCE_NUMBER });
		}
		return reqParams;
	}

	/**
	 * Set specified attributes in response.
	 * 
	 * @param response
	 * @param responseParams
	 * @throws Exception
	 */
	private void setResponseParams(Object response, Map<String, Object> responseParams) throws Exception {
		if (response != null && responseParams != null && responseParams.size() > 0) {
			String[] resParamNames = new String[responseParams.size()];
			Object[] resParamValues = new Object[responseParams.size()];

			Iterator<String> resParamKeysIt = responseParams.keySet().iterator();
			String key;
			for (int i = 0; resParamKeysIt.hasNext(); ++i) {
				key = resParamKeysIt.next();
				resParamNames[i] = key;
				resParamValues[i] = responseParams.get(key);
			}

			ThruReflection.setAttributeValues(response, resParamNames, resParamValues);
		}
	}

	/**
	 * Get specified attributes in header.
	 * 
	 * @param request
	 * @param headerName
	 * @return String
	 */
	private String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = request.getHeader(headerName);
		} catch (Exception e) {
			headerParamValue = null;
		}
		return headerParamValue;
	}

	private static final String TRANSACTION_ID = "TransactionIdentifier";
	private static final String ECHO_TOKEN = "EchoToken";
	private static final String SEQUENCE_NUMBER = "SequenceNmbr";
	private static final String VERSION = "Version";
	private static final String PRIMARY_LAGN_ID = "PrimaryLangID";
	private static final String SALES_CHANNEL = "ChannelId";
	private static final String USER_ID = "UserId";
}
