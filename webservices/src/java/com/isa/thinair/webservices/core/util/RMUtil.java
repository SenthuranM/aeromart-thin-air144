package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateStatusRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.InventorySeatMovementDTO;
import com.isa.thinair.airinventory.api.dto.rm.UpdateOptimizedInventoryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.dto.rm.RMFlightSummaryDTO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.SeatMvSummaryDTO;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovementType;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovementsType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryAdd;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryDelete;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryUpdate;
import com.isaaviation.thinair.webservices.api.airinventory.AAFareType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvBatchUpdateRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvRSType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventryBatchUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAInventorySeatMovementType;
import com.isaaviation.thinair.webservices.api.airinventory.AAInventorySeatMovementsType;
import com.isaaviation.thinair.webservices.api.airinventory.AASeatBCAllocationsType;
import com.isaaviation.thinair.webservices.api.airinventory.AASegmentSeatInventoriesType;
import com.isaaviation.thinair.webservices.api.airinventory.AAStatusErrorsAndWarnings;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRQ;

/**
 * @author Byorn
 */
public class RMUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(RMUtil.class);

	public static void updateOptimizedInventory(AAUpdateOptimizedInventoryRQ aaUpdateOptimizedInventoryRQ) throws ModuleException {

		UpdateOptimizedInventoryDTO updateOptimizedInventoryDTO = new UpdateOptimizedInventoryDTO();
		updateOptimizedInventoryDTO.setCabinClass(aaUpdateOptimizedInventoryRQ.getCabinClass());
		updateOptimizedInventoryDTO.setDepatureDate(aaUpdateOptimizedInventoryRQ.getDepatureDate().toGregorianCalendar()
				.getTime());
		updateOptimizedInventoryDTO.setSegmentCode(aaUpdateOptimizedInventoryRQ.getSegmentCode());
		updateOptimizedInventoryDTO.setFlightNumber(aaUpdateOptimizedInventoryRQ.getFlightNumber());
		// is not used.
		// updateOptimizedInventoryDTO.setProcessingStatus(aaUpdateOptimizedInventoryRQ.getProcessingStatus());
		Collection<InventorySeatMovementDTO> inventorySeatMovemantDTOCol = new ArrayList<InventorySeatMovementDTO>();
		AAInventorySeatMovementsType inventorySeatMovementsType = aaUpdateOptimizedInventoryRQ.getInventorySeatMovements();

		Collection<AAInventorySeatMovementType> inventorySeatMovemants = inventorySeatMovementsType.getInventorySeatMovement();
		for (AAInventorySeatMovementType inventorySeatMovemantType : inventorySeatMovemants) {
			InventorySeatMovementDTO inventorySeatMovementDTO = new InventorySeatMovementDTO();
			inventorySeatMovementDTO.setFromBookingClass(inventorySeatMovemantType.getFromBookingClass());
			inventorySeatMovementDTO.setToBookingClass(inventorySeatMovemantType.getToBookingClass());
			inventorySeatMovementDTO.setNoOfSeats(inventorySeatMovemantType.getNoOfSeats());
			inventorySeatMovementDTO.setMessageType(RMConstant.fromValue(inventorySeatMovemantType.getMessageType()).name());
			inventorySeatMovemantDTOCol.add(inventorySeatMovementDTO);
		}
		updateOptimizedInventoryDTO.setInventorySeatMovements(inventorySeatMovemantDTOCol);

		updateOptimizedInventoryDTO = WebServicesModuleUtils.getFlightInventoryBD().updateOptimizedInventory(
				updateOptimizedInventoryDTO);

	}

	public static AAFlightInvBatchUpdateRS updateFltInventoryBatchUpdate(
			AAFlightInventryBatchUpdateRQ wsAAFlightInventoryBatchRQ) throws ModuleException, WebservicesException {
		/** Holds the Response to be populated with results and to return **/
		AAFlightInvBatchUpdateRS wsAAflightInventoryBatchRS = new AAFlightInvBatchUpdateRS();

		wsAAflightInventoryBatchRS.setChannelId(wsAAFlightInventoryBatchRQ.getChannelId());
		wsAAflightInventoryBatchRS.setUserId(wsAAFlightInventoryBatchRQ.getUserId());
		wsAAflightInventoryBatchRS.setMessageId(wsAAFlightInventoryBatchRQ.getMessageId());

		String warningMSG = "";
		boolean haveWarnings = false;
		HashMap<Long, String> warningMsgMap = new HashMap<Long, String>();
		String operationType;

		/** Holds the Collection to pass to inventory module **/
		Collection<FCCSegInventoryUpdateRMDTO> fccSegInventoryUpdateDTOs = new ArrayList<FCCSegInventoryUpdateRMDTO>();

		/** The Request sent by rm **/
		List<AAFlightInvUpdateRQ> flightInventory = wsAAFlightInventoryBatchRQ.getFlightInvUpdate().getFlightInvUpdateDetail();

		/**
		 * Iterate the request 'flight level inventory details' and prepare the dto that needs to be added to the
		 * collection to be passed.
		 **/
		for (AAFlightInvUpdateRQ wsaaFlightInventoryRQ : flightInventory) {

			warningMSG = "";
			haveWarnings = false;

			FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTO = new FCCSegInventoryUpdateRMDTO();

			// TODO : Logical CC Change - Got the default Logical CC
			String defaultLogicalCC = WebServicesModuleUtils.getLogicalCabinClassBD().getDefaultLogicalCabinClass(
					wsaaFlightInventoryRQ.getCabinClass());
			fccSegInventoryUpdateDTO.setLogicalCCCode(defaultLogicalCC);
			fccSegInventoryUpdateDTO.setSegmentCode(wsaaFlightInventoryRQ.getSegmentCode());
			fccSegInventoryUpdateDTO.setVersion(0);
			fccSegInventoryUpdateDTO.setDepDateZulu(wsaaFlightInventoryRQ.getDepatureDate().toGregorianCalendar().getTime());
			fccSegInventoryUpdateDTO.setFlightNumber(wsaaFlightInventoryRQ.getFlightNumber());

			// for now we make it false.....
			fccSegInventoryUpdateDTO.setFCCSegInvUpdated(false);
			if (wsaaFlightInventoryRQ.getOversellCount() != null) {
				fccSegInventoryUpdateDTO.setOversellSeats(wsaaFlightInventoryRQ.getOversellCount());
				fccSegInventoryUpdateDTO.setFCCSegInvUpdated(true);
				fccSegInventoryUpdateDTO.setOversellUpdate(true);
			}
			if (wsaaFlightInventoryRQ.getCurtailCount() != null) {
				fccSegInventoryUpdateDTO.setCurtailedSeats(wsaaFlightInventoryRQ.getCurtailCount());
				fccSegInventoryUpdateDTO.setFCCSegInvUpdated(true);
				fccSegInventoryUpdateDTO.setCurtailUpdate(true);
			}
			operationType = wsaaFlightInventoryRQ.getOperationType();

			if (operationType != null && operationType.equals(WebservicesConstants.OPERATION_TYPE_PUBLISH)) {
				fccSegInventoryUpdateDTO.setPublish(true);
			}

			// populate col to add
			Set<FCCSegBCInventory> addFccSegBCInventories = new HashSet<FCCSegBCInventory>();
			if (wsaaFlightInventoryRQ.getFccSegBCInventoriesToAdd() != null) {
				Collection<AAFCCSegBCInventoryAdd> wsaaBCAddCol = wsaaFlightInventoryRQ.getFccSegBCInventoriesToAdd()
						.getFccSegBCInventory();
				if (wsaaBCAddCol != null) {
					HashMap<Integer, Double> conBucketAdd = new HashMap<Integer, Double>();
					HashMap<Double, String> conBucketAddSorted = new HashMap<Double, String>();

					Integer conBucketAddIndex = 0;
					for (AAFCCSegBCInventoryAdd wsaasegBCInventoryADDTmp : wsaaBCAddCol) {
						if (wsaasegBCInventoryADDTmp.getFareType() != null
								&& wsaasegBCInventoryADDTmp.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							conBucketAdd.put(conBucketAddIndex, wsaasegBCInventoryADDTmp.getAdultFareAmount());
						}
						conBucketAddIndex++;
					}

					if (!conBucketAdd.isEmpty()) {
						conBucketAddSorted = getSortedConBucketList(conBucketAdd, wsaaFlightInventoryRQ);
					}

					Integer addBucketIndex = 0;

					for (AAFCCSegBCInventoryAdd wsaasegBCInventoryADD : wsaaBCAddCol) {

						// get the booking code from the adult fare if booking code not define in request
						String bookingCode = wsaasegBCInventoryADD.getBookingCode();
						List<String> bookingClasses = null;

						if (bookingCode == null
								&& !wsaasegBCInventoryADD.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
							bookingClasses = flightBD.getBookingClassForFareAmount(wsaaFlightInventoryRQ.getSegmentCode(),
									wsaaFlightInventoryRQ.getDepatureDate().toGregorianCalendar().getTime(),
									wsaasegBCInventoryADD.getAdultFareAmount(), wsaasegBCInventoryADD.getFareType());
							if (!bookingClasses.isEmpty()) {
								bookingCode = bookingClasses.get(bookingClasses.size() - 1);// if not CON(RET
																							// or SEG) get
																							// the
								// last matching value
							}
						}

						FCCSegBCInventory fccSegBCinventoryADD = new FCCSegBCInventory();
						// TODO : Logical CC Change - Got the default Logical CC
						fccSegBCinventoryADD.setLogicalCCCode(defaultLogicalCC);
						fccSegBCinventoryADD.setSegmentCode(wsaaFlightInventoryRQ.getSegmentCode());
						fccSegBCinventoryADD.setSeatsAllocated(wsaasegBCInventoryADD.getAllocatedSeats());
						fccSegBCinventoryADD.setPriorityFlag(wsaasegBCInventoryADD.isPriorityFlag());
						fccSegBCinventoryADD.setStatus(FCCSegBCInventory.Status.OPEN);
						fccSegBCinventoryADD.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);
						if (bookingCode != null) {
							fccSegBCinventoryADD.setBookingCode(bookingCode);
							addFccSegBCInventories.add(fccSegBCinventoryADD);
						} else if (wsaasegBCInventoryADD.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							if (conBucketAdd.get(addBucketIndex) == wsaasegBCInventoryADD.getAdultFareAmount()
									&& !conBucketAddSorted.isEmpty()) {
								fccSegBCinventoryADD.setBookingCode(conBucketAddSorted.get(wsaasegBCInventoryADD
										.getAdultFareAmount()));
								addFccSegBCInventories.add(fccSegBCinventoryADD);
							}
						}

						addBucketIndex++;
					}
				}
			}
			fccSegInventoryUpdateDTO.setAddedFCCSegBCInventories(addFccSegBCInventories);

			// populate col to del
			Set<FCCSegBCInventory> delFccSegBCInventories = new HashSet<FCCSegBCInventory>();
			if (wsaaFlightInventoryRQ.getFccSegBCInventoriesToDelete() != null) {
				Collection<AAFCCSegBCInventoryDelete> wsaaBCDelCol = wsaaFlightInventoryRQ.getFccSegBCInventoriesToDelete()
						.getFccSegBCInventory();
				if (wsaaBCDelCol != null) {
					HashMap<Integer, Double> conBucketDel = new HashMap<Integer, Double>();
					HashMap<Double, String> conBucketDelSorted = new HashMap<Double, String>();

					Integer conBucketDelIndex = 0;
					for (AAFCCSegBCInventoryDelete wsaasegBCInventoryDELTmp : wsaaBCDelCol) {
						if (wsaasegBCInventoryDELTmp.getFareType() != null
								&& wsaasegBCInventoryDELTmp.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							conBucketDel.put(conBucketDelIndex, wsaasegBCInventoryDELTmp.getAdultFareAmount());
						}
						conBucketDelIndex++;
					}
					if (!conBucketDel.isEmpty()) {
						conBucketDelSorted = getSortedConBucketList(conBucketDel, wsaaFlightInventoryRQ);
					}

					Integer delBucketIndex = 0;

					for (AAFCCSegBCInventoryDelete wsaasegBCInventoryDEL : wsaaBCDelCol) {

						// get the booking code from the adult fare if booking code not define in request
						String bookingCode = wsaasegBCInventoryDEL.getBookingCode();
						List<String> bookingClasses = null;

						if (bookingCode == null
								&& !wsaasegBCInventoryDEL.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
							bookingClasses = flightBD.getBookingClassForFareAmount(wsaaFlightInventoryRQ.getSegmentCode(),
									wsaaFlightInventoryRQ.getDepatureDate().toGregorianCalendar().getTime(),
									wsaasegBCInventoryDEL.getAdultFareAmount(), wsaasegBCInventoryDEL.getFareType());
							if (!bookingClasses.isEmpty()) {
								bookingCode = bookingClasses.get(bookingClasses.size() - 1);// if not CON(RET
																							// or SEG) get
																							// the
								// last matching value
							}
						}

						FCCSegBCInventory fccSegBCinventoryDEL = new FCCSegBCInventory();
						if (bookingCode != null) {
							fccSegBCinventoryDEL.setBookingCode(bookingCode);
							delFccSegBCInventories.add(fccSegBCinventoryDEL);
						} else if (wsaasegBCInventoryDEL.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							if (conBucketDel.get(delBucketIndex) == wsaasegBCInventoryDEL.getAdultFareAmount()
									&& !conBucketDelSorted.isEmpty()) {
								fccSegBCinventoryDEL.setBookingCode(conBucketDelSorted.get(wsaasegBCInventoryDEL
										.getAdultFareAmount()));
								delFccSegBCInventories.add(fccSegBCinventoryDEL);
							}
						}

						delBucketIndex++;

					}
				}
			}
			fccSegInventoryUpdateDTO.setDeletedFCCSegBCInventories(delFccSegBCInventories);

			// populate col to update
			Set<FCCSegBCInventory> updateFccSegBCInventories = new HashSet<FCCSegBCInventory>();
			if (wsaaFlightInventoryRQ.getFccSegBCInventoriesToUpdate() != null) {
				Collection<AAFCCSegBCInventoryUpdate> wsaaBCUpdateCol = wsaaFlightInventoryRQ.getFccSegBCInventoriesToUpdate()
						.getFccSegBCInventory();
				if (wsaaBCUpdateCol != null) {
					HashMap<Integer, Double> conBucketUpdate = new HashMap<Integer, Double>();
					HashMap<Double, String> conBucketUpdateSorted = new HashMap<Double, String>();

					Integer conBucketUpdateIndex = 0;
					for (AAFCCSegBCInventoryUpdate wsaasegBCInventoryUPDATETmp : wsaaBCUpdateCol) {
						if (wsaasegBCInventoryUPDATETmp.getFareType() != null
								&& wsaasegBCInventoryUPDATETmp.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							conBucketUpdate.put(conBucketUpdateIndex, wsaasegBCInventoryUPDATETmp.getAdultFareAmount());
						}
						conBucketUpdateIndex++;
					}

					if (!conBucketUpdate.isEmpty()) {
						if (conBucketUpdate.size() > WebservicesConstants.MAX_CON_BUCKET_SIZE) {
							haveWarnings = true;
							warningMSG = "[Flight No:" + wsaaFlightInventoryRQ.getFlightNumber()
									+ "] has exceed the maximum no of connection fares for batch inventory update";
						}
						conBucketUpdateSorted = getSortedConBucketList(conBucketUpdate, wsaaFlightInventoryRQ);
					}

					Integer updateBucketIndex = 0;

					for (AAFCCSegBCInventoryUpdate wsaasegBCInventoryUPDATE : wsaaBCUpdateCol) {

						// get the booking code from the adult fare if booking code not define in request
						String bookingCode = wsaasegBCInventoryUPDATE.getBookingCode();
						List<String> bookingClasses = null;

						if (bookingCode == null
								&& !wsaasegBCInventoryUPDATE.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
							bookingClasses = flightBD.getBookingClassForFareAmount(wsaaFlightInventoryRQ.getSegmentCode(),
									wsaaFlightInventoryRQ.getDepatureDate().toGregorianCalendar().getTime(),
									wsaasegBCInventoryUPDATE.getAdultFareAmount(), wsaasegBCInventoryUPDATE.getFareType());
							if (!bookingClasses.isEmpty()) {
								bookingCode = bookingClasses.get(bookingClasses.size() - 1);// if not CON(RET
																							// or SEG) get
																							// the
								// last matching value
							}
						}

						FCCSegBCInventory fccSegBCinventoryUPDATE = new FCCSegBCInventory();
						// get the available seat count and set as allocated seat count.
						// If already have sold seats, make the allocation = available + cnx + sold - acquired
						fccSegBCinventoryUPDATE.setSeatsAvailable(wsaasegBCInventoryUPDATE.getAvailableSeats());
						if (wsaasegBCInventoryUPDATE.getAllocatedSeats() >= 0) {
							fccSegBCinventoryUPDATE.setSeatsAllocated(wsaasegBCInventoryUPDATE.getAllocatedSeats());
						} else { // if allocation count not received available count set as allocation
							fccSegBCinventoryUPDATE.setSeatsAllocated(wsaasegBCInventoryUPDATE.getAvailableSeats());
						}
						fccSegBCinventoryUPDATE.setStatus(wsaasegBCInventoryUPDATE.getStatus());

						if (wsaasegBCInventoryUPDATE.getPriorityFlag() != null) {
							fccSegBCinventoryUPDATE.setPriorityFlag(wsaasegBCInventoryUPDATE.getPriorityFlag());
						}
						//For RM API this should be Manual
						fccSegBCinventoryUPDATE.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
						// TODO : Logical CC Change - Got the default Logical CC
						fccSegBCinventoryUPDATE.setLogicalCCCode(defaultLogicalCC);
						fccSegBCinventoryUPDATE.setSegmentCode(wsaaFlightInventoryRQ.getSegmentCode());

						if (bookingCode != null) {
							fccSegBCinventoryUPDATE.setBookingCode(bookingCode);
							updateFccSegBCInventories.add(fccSegBCinventoryUPDATE);
						} else if (wsaasegBCInventoryUPDATE.getFareType().equals(BookingClass.AllocationType.CONNECTION)) {
							if (conBucketUpdate.get(updateBucketIndex) == wsaasegBCInventoryUPDATE.getAdultFareAmount()
									&& !conBucketUpdateSorted.isEmpty()) {
								// get All the connection booking class for given flight.Then sort them according to
								// nest rank.
								// Requested fares also sorted by fare amount.
								// Highest fare will allocate 3rd highest nest ranked booking class. second highest to
								// 2nd, 3rd highest to 1st connection
								fccSegBCinventoryUPDATE.setBookingCode(conBucketUpdateSorted.get(wsaasegBCInventoryUPDATE
										.getAdultFareAmount()));
								updateFccSegBCInventories.add(fccSegBCinventoryUPDATE);
							} else {
								if (haveWarnings)
									warningMSG += "\n ";
								haveWarnings = true;
								warningMSG += " Unable to found valid booking class for the [Flight No: "
										+ wsaaFlightInventoryRQ.getFlightNumber() + "], [Fare Amount: "
										+ wsaasegBCInventoryUPDATE.getAdultFareAmount() + "], [Fare Type: "
										+ wsaasegBCInventoryUPDATE.getFareType() + "] ";

							}
						} else {
							if (haveWarnings)
								warningMSG += "\n ";
							haveWarnings = true;
							warningMSG += " Unable to found valid booking class for the [Flight No: "
									+ wsaaFlightInventoryRQ.getFlightNumber() + "], [Fare Amount: "
									+ wsaasegBCInventoryUPDATE.getAdultFareAmount() + "], [Fare Type: "
									+ wsaasegBCInventoryUPDATE.getFareType() + "] ";

						}

						updateBucketIndex++;
					}
				}
			}
			fccSegInventoryUpdateDTO.setUpdatedFCCSegBCInventories(updateFccSegBCInventories);

			/** add to the collection that will be sent to inventory **/
			fccSegInventoryUpdateDTOs.add(fccSegInventoryUpdateDTO);

			if (haveWarnings) {
				warningMsgMap.put(fccSegInventoryUpdateDTO.getDepDateZulu().getTime(), warningMSG);
			}

		}

		/** Call the inventory method **/
		Collection<FCCSegInventoryUpdateStatusRMDTO> fccSegInventoryUpdateStatusDTOs = new ArrayList<FCCSegInventoryUpdateStatusRMDTO>();

		fccSegInventoryUpdateStatusDTOs = WebServicesModuleUtils.getFlightInventoryBD().batchUpdateFCCSegInventory(
				fccSegInventoryUpdateDTOs);

		/** response type holds the details per flight segment with result...inclusive in the main response **/
		AAFlightInvRSType wsaaflightInventoryRSType = new AAFlightInvRSType();

		for (FCCSegInventoryUpdateStatusRMDTO inventoryUpdateStatusDTO : fccSegInventoryUpdateStatusDTOs) {

			AAFlightInvRS wsAAflightInventoryRS = new AAFlightInvRS();
			wsAAflightInventoryRS.setCabinClass(inventoryUpdateStatusDTO.getCabinClass());

			wsAAflightInventoryRS.setDepatureDate(CommonUtil.parse(inventoryUpdateStatusDTO.getDepZuluDate()));

			AAStatusErrorsAndWarnings status = new AAStatusErrorsAndWarnings();
			boolean operationsuccess = true;
			if (inventoryUpdateStatusDTO.getStatus() == OperationStatusDTO.OPERATION_FAILURE) {
				operationsuccess = false;
				status.setErrorText("Save/Update operation is failed.");
				status.setWarningMessage(inventoryUpdateStatusDTO.getMsg());
			} else if (inventoryUpdateStatusDTO.getStatus() == OperationStatusDTO.OPERATION_COMPLETED
					&& !inventoryUpdateStatusDTO.getMsg().equals("")) {
				operationsuccess = false;
				status.setErrorText("Save/Update operation is partially failed.");
				if (warningMsgMap.get(inventoryUpdateStatusDTO.getDepZuluDate().getTime()) != null) {
					status.setWarningMessage(inventoryUpdateStatusDTO.getMsg() + "\n "
							+ (String) warningMsgMap.get(inventoryUpdateStatusDTO.getDepZuluDate().getTime()));
				} else {
					status.setWarningMessage(inventoryUpdateStatusDTO.getMsg());
				}
			} else if (warningMsgMap.get(inventoryUpdateStatusDTO.getDepZuluDate().getTime()) != null) {
				operationsuccess = false;
				status.setErrorText("Save/Update operation is partially failed.");
				status.setWarningMessage((String) warningMsgMap.get(inventoryUpdateStatusDTO.getDepZuluDate().getTime()));
			}

			status.setSuccess(operationsuccess);

			wsAAflightInventoryRS.setFlightNumber(inventoryUpdateStatusDTO.getFlightNumber());
			wsAAflightInventoryRS.setSegmentCode(inventoryUpdateStatusDTO.getSegmentCode());

			wsAAflightInventoryRS.setStatusErrorsAndWarnings(status);

			wsaaflightInventoryRSType.getFlightInvStatusDetail().add(wsAAflightInventoryRS);

		}

		// add the response type collection to the main response
		wsAAflightInventoryBatchRS.setFlightInvUpdateStatus(wsaaflightInventoryRSType);

		return wsAAflightInventoryBatchRS;
	}

	private static HashMap<Double, String> getSortedConBucketList(HashMap<Integer, Double> conBucket,
			AAFlightInvUpdateRQ wsaaFlightInventoryRQ) throws ModuleException, WebservicesException {

		HashMap<Double, String> conBucketSorted = new HashMap<Double, String>();

		List<Double> fareList = new ArrayList<Double>(conBucket.values());
		Collections.sort(fareList);
		Collections.reverse(fareList);

		List<String> bookingClasses = WebServicesModuleUtils.getFlightBD().getBookingClassForFareAmount(
				wsaaFlightInventoryRQ.getSegmentCode(), wsaaFlightInventoryRQ.getDepatureDate().toGregorianCalendar().getTime(),
				new Double("0"), BookingClass.AllocationType.CONNECTION);

		if (!bookingClasses.isEmpty()) {
			int size = Math.min(fareList.size(), WebservicesConstants.MAX_CON_BUCKET_SIZE);
			int bcCount = Math.min(bookingClasses.size(), WebservicesConstants.MAX_CON_BUCKET_SIZE);
			for (int i = 0; i < size; i++) {
				conBucketSorted.put((Double) fareList.get(i), (String) bookingClasses.get(bcCount - i - 1));
			}
		}

		return conBucketSorted;

	}

	/**
	 * 
	 * @author isa11
	 * 
	 */
	public static AAAgentSeatMovemantRS getAgentSeatSellingReport(AAAgentSeatMovemantRQ agentSeatMovemantRQ)
			throws ModuleException, WebservicesException {
		/** Holds the Response to be poplulated with results and to return * */
		AAAgentSeatMovemantRS seatMovemantRS = new AAAgentSeatMovemantRS();
		seatMovemantRS.setChannelId(agentSeatMovemantRQ.getChannelId());
		seatMovemantRS.setUserId(agentSeatMovemantRQ.getUserId());
		seatMovemantRS.setMessageId(agentSeatMovemantRQ.getMessageId());
		String ccCode = agentSeatMovemantRQ.getCabinClass();

		/** Get the BDs **/
		FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
		DataExtractionBD dxBD = WebServicesModuleUtils.getDataExtractionBD();

		/** Get the Flight Details from AA **/
		RMFlightSummaryDTO summaryDTO = flightBD.getFlightSummary(agentSeatMovemantRQ.getFlightNumber(), agentSeatMovemantRQ
				.getDepatureDate().toGregorianCalendar().getTime());

		// if flight or sengmet details not found throw exception
		if (summaryDTO == null || summaryDTO.getConnectedSegments().size() == 0) {
			throw new WebservicesException("RM1", "Flight or Segments were not found");
		}
		/** Take out the segments from the summary DTO got from AA **/
		Collection<FlightSegmentDTO> connectedSegments = summaryDTO.getConnectedSegments();

		/** Get the Agent details passing the flight ID, got from AA **/
		FCCAgentsSeatMvDTO fccSeatMvDTO = dxBD.getFCCSeatMovementSummary(summaryDTO.getFlightId(),
				agentSeatMovemantRQ.getCabinClass(), true);

		if (fccSeatMvDTO == null) {
			throw new WebservicesException("RM10", "FCCAgentsSeatMvDTO was null");
		}

		/** Iterate all the segment IDs' and pass each segment ID to the Agent DTO to get segment vise seat summaries **/
		for (FlightSegmentDTO segmentDTO : connectedSegments) {

			// WSDTO hold the flight segment information
			AAAgentSeatMovementsType seatMovementsType = new AAAgentSeatMovementsType();
			seatMovementsType.setCabinClass(ccCode);
			seatMovementsType.setDepatureDate(CommonUtil.parse(segmentDTO.getEstTimeDepatureLocal()));
			seatMovementsType.setSegmentCode(segmentDTO.getSegmentCode());
			seatMovementsType.setFlightNumber(summaryDTO.getFlightNumber());

			Collection<SeatMvSummaryDTO> dtos = fccSeatMvDTO.getSegAgentsSeatMvWithFCCAgentsSeatMv(segmentDTO.getFlightSegId());

			for (SeatMvSummaryDTO seatMvSummaryDTO : dtos) {
				// WSDTO holds the seat movemant type information
				AAAgentSeatMovementType agentSeatMovementType = new AAAgentSeatMovementType();
				agentSeatMovementType.setAgentCode(seatMvSummaryDTO.getAgentCode());
				agentSeatMovementType.setAgentName(seatMvSummaryDTO.getAgentName());
				agentSeatMovementType.setAgentTypeCode(seatMvSummaryDTO.getAgentTypeCode());

				agentSeatMovementType.setStationCode(seatMvSummaryDTO.getStationCode());

				agentSeatMovementType.setCancelledChildSeats(seatMvSummaryDTO.getCancelledChildSeats());
				agentSeatMovementType.setCancelledChildSeatsSum(seatMvSummaryDTO.getCancelledChildSeatsSum());
				agentSeatMovementType.setCancelledInfantSeats(seatMvSummaryDTO.getCancelledInfantSeats());
				agentSeatMovementType.setCancelledInfantSeatsSum(seatMvSummaryDTO.getCancelledChildSeatsSum());
				agentSeatMovementType.setCancelledSeats(seatMvSummaryDTO.getCancelledSeats());
				agentSeatMovementType.setCancelledSeatsSum(seatMvSummaryDTO.getCancelledChildSeatsSum());
				agentSeatMovementType.setOnHoldChildSeats(seatMvSummaryDTO.getOnHoldChildSeats());
				agentSeatMovementType.setOnHoldChildSeatsSum(seatMvSummaryDTO.getOnHoldChildSeatsSum());
				agentSeatMovementType.setOnholdInfantSeats(seatMvSummaryDTO.getOnholdInfantSeats());
				agentSeatMovementType.setOnholdInfantSeatsSum(seatMvSummaryDTO.getOnholdInfantSeatsSum());
				agentSeatMovementType.setOnholdSeats(seatMvSummaryDTO.getOnholdSeats());
				agentSeatMovementType.setOnholdSeatsSum(seatMvSummaryDTO.getOnholdSeatsSum());
				agentSeatMovementType.setSoldChildSeats(seatMvSummaryDTO.getSoldChildSeats());
				agentSeatMovementType.setSoldChildSeatsSum(seatMvSummaryDTO.getSoldChildSeatsSum());
				agentSeatMovementType.setSoldInfantSeats(seatMvSummaryDTO.getSoldInfantSeats());
				agentSeatMovementType.setSoldInfantSeatsSum(seatMvSummaryDTO.getSoldInfantSeatsSum());
				agentSeatMovementType.setSoldSeatsSum(seatMvSummaryDTO.getSoldSeatsSum());
				agentSeatMovementType.setSoldSeats(seatMvSummaryDTO.getSoldSeats());

				agentSeatMovementType.getConfirmedPnrList().addAll(seatMvSummaryDTO.getConfirmedPNRs());
				agentSeatMovementType.getCancelledPnrList().addAll(seatMvSummaryDTO.getCancelledPNRs());
				agentSeatMovementType.getOnholdPnrList().addAll(seatMvSummaryDTO.getOnHoldPNRs());

				// Add WS-Agent-details to WS-Flight-detail
				seatMovementsType.getInventorySeatMovement().add(agentSeatMovementType);

			}
			// ws final response will have the flight segment detail wise agent seat movemants
			seatMovemantRS.getInventorySeatMovements().add(seatMovementsType);

		}

		return seatMovemantRS;

	}

	// private Collection<AAPn> getConfirmedPNRs()

	/**
	 * Search Inventory allocations
	 * 
	 * @param searchRQ
	 * @return
	 * @throws ModuleException
	 *             , WebservicesException
	 */
	public static AAFlightInventoryDataRS getFlightInventoryAllocations(AAFlightInventorySearchRQ searchRQ)
			throws ModuleException, WebservicesException {
		/** Get the BDs **/
		FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
		FlightInventoryBD inventoryBD = WebServicesModuleUtils.getFlightInventoryBD();

		// RM WS final res to send
		AAFlightInventoryDataRS dataRS = new AAFlightInventoryDataRS();

		RMFlightSummaryDTO flightSummaryDTO = flightBD.getFlightSummary(searchRQ.getFlightNumber(), searchRQ.getDepatureDate()
				.toGregorianCalendar().getTime());

		// flight not found in db
		if (flightSummaryDTO == null) {
			throw new WebservicesException("RM1", "Flight or Segments were not found");

		}

		Integer overlapFltId = null;
		int overlapFlightID = flightSummaryDTO.getOverlappling_flightId();
		if (overlapFlightID != 0) {
			overlapFltId = new Integer(overlapFlightID);
		}

		Collection<FCCInventoryDTO> inventoryDetails = inventoryBD.getFCCInventory(flightSummaryDTO.getFlightId(),
				searchRQ.getCabinClass(), overlapFltId,false);

		if (inventoryDetails == null || inventoryDetails.size() < 1) {
			throw new WebservicesException("RM9", "inventoryBD.getFCCInventory - did not return");
		}

		for (FCCInventoryDTO inventory : inventoryDetails) {
			Collection<FCCSegInventoryDTO> segInventoryDTOs = inventory.getFccSegInventoryDTOs();

			if (segInventoryDTOs == null || segInventoryDTOs.size() < 1) {
				throw new WebservicesException("RM9", "segInventoryDTOs - did not return");
			}

			// iterate segment wise inventory
			for (FCCSegInventoryDTO inventoryDTO : segInventoryDTOs) {
				FCCSegInventory fccSegInventory = inventoryDTO.getFccSegInventory();
				if (fccSegInventory == null) {
					throw new WebservicesException("RM9", "FCCSegInventoryDTO - did not return");
				}

				AASegmentSeatInventoriesType segInv = new AASegmentSeatInventoriesType();
				segInv.setAllocatedSeats(fccSegInventory.getAllocatedSeats());
				segInv.setAvailableInfantSeats(fccSegInventory.getAvailableInfantSeats());
				segInv.setAvailableSeats(fccSegInventory.getAvailableSeats());

				// TODO : Logical CC Change - Got the Cabin Class of Logical CC
				segInv.setCabinClass(WebServicesModuleUtils.getLogicalCabinClassBD().getCabinClass(
						fccSegInventory.getLogicalCCCode()));
				segInv.setCurtailedSeats(fccSegInventory.getCurtailedSeats());
				// segInv.setDepatureDate(inventoryDTO.g.get)
				segInv.setFixedSeats(fccSegInventory.getFixedSeats());
				segInv.setFlightNumber(inventoryDTO.getFlightNumber());
				segInv.setInfantAllocation(fccSegInventory.getInfantAllocation());
				segInv.setOnholdInfantSeats(fccSegInventory.getOnholdInfantSeats());
				segInv.setOnHoldSeats(fccSegInventory.getOnHoldSeats());
				segInv.setOversellSeats(fccSegInventory.getOversellSeats());
				segInv.setSeatsSold(fccSegInventory.getSeatsSold());
				segInv.setSegmentCode(fccSegInventory.getSegmentCode());
				segInv.setSoldInfantSeats(fccSegInventory.getSoldInfantSeats());
				Collection<ExtendedFCCSegBCInventory> bcInventories = inventoryDTO.getFccSegBCInventories();

				if (bcInventories == null || bcInventories.size() < 1) {
					throw new WebservicesException("RM5", "bcInventories - did not return");
				}

				for (ExtendedFCCSegBCInventory extendedFCCSegBCInventory : bcInventories) {

					FCCSegBCInventory segBCInventory = extendedFCCSegBCInventory.getFccSegBCInventory();
					if (segBCInventory == null) {
						throw new WebservicesException("RM5", "FCCSegBCInventory - did not return");
					}

					AASeatBCAllocationsType seatBCAllocationsType = new AASeatBCAllocationsType();
					seatBCAllocationsType.setBookingClass(segBCInventory.getBookingCode());
					seatBCAllocationsType.setOnHoldSeats(segBCInventory.getActualSeatsOnHold());
					seatBCAllocationsType.setPriorityFlag(segBCInventory.getPriorityFlag());
					seatBCAllocationsType.setSeatsAcquired(segBCInventory.getSeatsAcquired());
					seatBCAllocationsType.setSeatsAllocated(segBCInventory.getSeatsAllocated());
					seatBCAllocationsType.setSeatsAvailable(segBCInventory.getSeatsAvailable());
					seatBCAllocationsType.setSeatsCancelled(segBCInventory.getSeatsCancelled());
					seatBCAllocationsType.setSeatsSold(segBCInventory.getActualSeatsSold());
					seatBCAllocationsType.setStatus(segBCInventory.getStatus());
					seatBCAllocationsType.setIsStandardCode(extendedFCCSegBCInventory.isStandardCode());
					seatBCAllocationsType.setIsFixedFlag(extendedFCCSegBCInventory.isFixedFlag());
					seatBCAllocationsType.setBcType(extendedFCCSegBCInventory.getBcType());
					seatBCAllocationsType.setAllocationType(extendedFCCSegBCInventory.getAllocationType());
					seatBCAllocationsType.setSeatsSoldNestedIn(segBCInventory.getSeatsSoldAquiredByNesting());
					seatBCAllocationsType.setSeatsSoldNestedOut(segBCInventory.getSeatsSoldNested());
					seatBCAllocationsType.setSeatsOnHoldNestedIn(segBCInventory.getSeatsOnHoldAquiredByNesting());
					seatBCAllocationsType.setSeatsOnHoldNestedOut(segBCInventory.getSeatsOnHoldNested());
//					seatBCAllocationsType.setSurchargeApplicability(segBCInventory.isSurchargeApplicability());
					if (extendedFCCSegBCInventory.getNestRank() != null) {
						seatBCAllocationsType.setNestRank(extendedFCCSegBCInventory.getNestRank());
					}
					seatBCAllocationsType.setChangeActionStatus(segBCInventory.getStatusChangeAction());
					int agentSize = extendedFCCSegBCInventory.getLinkedEffectiveAgents() == null ? 0 : extendedFCCSegBCInventory
							.getLinkedEffectiveAgents().size();
					seatBCAllocationsType.setNumOfAgents(agentSize);

					segInv.getSeatAllocations().add(seatBCAllocationsType);

					Collection<FareSummaryLightDTO> fareSummaryDTOs = extendedFCCSegBCInventory.getLinkedEffectiveFares();
					for (FareSummaryLightDTO fareSummaryLightDTO : fareSummaryDTOs) {

						AAFareType fareType = new AAFareType();
						fareType.setAdultFareAmount(fareSummaryLightDTO.getFareAmount());
						fareType.setChildFareAmount(fareSummaryLightDTO.getChildFareAmount());
						fareType.setInfantFareAmount(fareSummaryLightDTO.getInfantFareAmount());
						seatBCAllocationsType.getFareDetails().add(fareType);
					}

				}

				dataRS.getSegmentSeatInventories().add(segInv);
			}
		}

		return dataRS;

	}

	public static enum RMConstant {

		// RMThresholdType
		E("ABOVE"), D("BELOW");

		private final String value;

		RMConstant(String v) {
			value = v;
		}

		public String value() {
			return value;
		}

		public static RMConstant fromValue(String v) {
			for (RMConstant c : RMConstant.values()) {

				if (c.name().equals(v)) {
					return c;
				}
			}
			throw new IllegalArgumentException(v.toString());
		}
	}

}
