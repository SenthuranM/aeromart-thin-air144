package com.isa.thinair.webservices.core.cache.store;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import com.isa.thinair.webservices.core.cache.util.CacheCommonUtil;

public abstract class AbstractStore {

	private String cacheOperationCode;
	private CacheManager cacheManager;

	public String getCacheOperationCode() {
		return cacheOperationCode;
	}

	public void setCacheOperationCode(String cacheOperationCode) {
		this.cacheOperationCode = cacheOperationCode;
	}

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public String getCacheName() {
		return CacheCommonUtil.getFullQualifiedCacheName(this.cacheOperationCode);
	}

	public Cache getCache() {
		String cacheName = getCacheName();
		Cache cache = getCacheManager().getCache(cacheName);
		return cache;
	}

}
