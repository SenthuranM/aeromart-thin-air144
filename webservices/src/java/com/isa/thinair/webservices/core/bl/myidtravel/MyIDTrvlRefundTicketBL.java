package com.isa.thinair.webservices.core.bl.myidtravel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.SuccessType;

import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIdTravelRefundTicketRS;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIdTravelRefundTicketType;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlRefundTicketBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlRefundTicketBL.class);

	public static MyIDTravelResAdapterRefundTicketResponse execute(MyIDTravelResAdapterRefundTicketRequest myIDRefTktReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN refundTicket(MyIDTravelResAdapterRefundTicketRequest)");
		}
		MyIDTravelResAdapterRefundTicketResponse myIDRefTktRes = null;
		try {
			myIDRefTktRes = refundTicket(myIDRefTktReq);
		} catch (Exception ex) {
			log.error("refundTicket(MyIDTravelResAdapterRefundTicketRequest) failed.", ex);
			if (myIDRefTktRes == null) {
				myIDRefTktRes = new MyIDTravelResAdapterRefundTicketResponse();
				MyIdTravelRefundTicketRS refundResponse = new MyIdTravelRefundTicketRS();
				refundResponse.setEchoToken(myIDRefTktReq.getMyIdTravelRefundTicketRQ().getEchoToken());
				myIDRefTktRes.setMyIdTravelRefundTicketRS(refundResponse);
			}
			if (myIDRefTktRes.getMyIdTravelRefundTicketRS().getErrors() == null)
				myIDRefTktRes.getMyIdTravelRefundTicketRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDRefTktRes.getMyIdTravelRefundTicketRS().getErrors().getError(), null, ex);
		}
		if (log.isDebugEnabled())
			log.debug("END refundTicket(MyIDTravelResAdapterRefundTicketRequest)");
		return myIDRefTktRes;

	}

	private static MyIDTravelResAdapterRefundTicketResponse refundTicket(MyIDTravelResAdapterRefundTicketRequest myIDRefTktReq)
			throws WebservicesException {

		MyIDTravelResAdapterRefundTicketResponse myIDRefTktRes = new MyIDTravelResAdapterRefundTicketResponse();
		myIDRefTktRes.setEmployeeData(myIDRefTktReq.getEmployeeData());
		MyIdTravelRefundTicketType refundReq = myIDRefTktReq.getMyIdTravelRefundTicketRQ().getRefundTicketRQ();

		MyIdTravelRefundTicketRS refundResponse = new MyIdTravelRefundTicketRS();
		refundResponse.setEchoToken(myIDRefTktReq.getMyIdTravelRefundTicketRQ().getEchoToken());

		MyIdTravelRefundTicketType refundRes = new MyIdTravelRefundTicketType();
		refundRes.setAirline(refundReq.getAirline());
		refundRes.setName(refundReq.getName());
		refundRes.setTicketNumber(refundReq.getTicketNumber());
		refundResponse.setRefundTicketRS(refundRes);
		myIDRefTktRes.setMyIdTravelRefundTicketRS(refundResponse);

		String retEticket = refundReq.getTicketNumber().getETicketNumber();

		retEticket = retEticket.replace("-", "");
		ReservationSearchDTO searchDTO = new ReservationSearchDTO();
		searchDTO.setETicket(retEticket);
		Collection<ReservationDTO> reservations;
		try {
			reservations = WebServicesModuleUtils.getReservationQueryBD().getReservations(searchDTO);

			if (reservations == null || reservations.isEmpty()) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_TICKET_NOT_FOUND,
						"No matching ticket found: Eticket - " + retEticket);
			}

			ReservationDTO firstReservation = PlatformUtiltiies.getFirstElement(reservations);

			Reservation reservation = MyIDTrvlCommonUtil.getReservation(firstReservation.getPnr());

			Set<ReservationPax> passengers = reservation.getPassengers();

			Integer selectedPnrPaxId = null;
			BigDecimal availableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

			ReservationPax selectedPassenger = null;

			for (ReservationPax passenger : passengers) {
				Collection<EticketTO> etickets = passenger.geteTickets();
				if (etickets != null && !etickets.isEmpty()) {
					for (EticketTO eticket : etickets) {

						if (eticket.getEticketNumber().equals(retEticket)) {
							availableBalance = passenger.getTotalAvailableBalance();
							selectedPnrPaxId = passenger.getPnrPaxId();
							selectedPassenger = passenger;
							break;
						}
					}
				}
			}

			if (availableBalance.compareTo(BigDecimal.ZERO) == 0) {
				// Already refunded
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_TICKET_REFUNDED,
						"Already refunded: Eticket - " + retEticket);
			} else if (availableBalance.compareTo(BigDecimal.ZERO) < 0) {
				// Refundable

				try {
					// Extract credit card payments
					Collection<ReservationTnx> transactions = selectedPassenger.getPaxPaymentTnxView();

					// There aren't multiple payment scenarios in MyIDTravel,
					// hence whatever the payment done
					// it is once and we are taking first payment

					List<Integer> payemntTransactionIds = new ArrayList<Integer>();
					ReservationTnx paymentTransaction = null;
					for (ReservationTnx transaction : transactions) {
						if (transaction.getNominalCode().equals(
								new Integer(ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode()))
								|| transaction.getNominalCode().equals(
										new Integer(ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode()))) {
							payemntTransactionIds.add(transaction.getTnxId());
							paymentTransaction = transaction;
							break;
						}
					}

					Map<Integer, CardPaymentInfo> paymentInfoMap = WebServicesModuleUtils.getReservationBD().getCreditCardInfo(
							payemntTransactionIds, true);

					String paymentGatewayName = WebServicesModuleUtils.getPaymentBrokerBD()
							.getPaymentGatewayNameForCCTransaction(paymentTransaction.getTnxId(), true);
					IPGIdentificationParamsDTO ipgIdentifiactionParams = new IPGIdentificationParamsDTO(paymentGatewayName);

					CardPaymentInfo paymentInfo = paymentInfoMap.get(paymentTransaction.getTnxId());
					IPassenger iPassenger = new PassengerAssembler(null);
					String userNotes = "MyIDTravel Refund";
					IPayment payment = new PaymentAssembler();
					payment.addCardPayment(paymentInfo.getType(), paymentInfo.getEDate(), paymentInfo.getNo(),
							paymentInfo.getName(), paymentInfo.getAddress(), paymentInfo.getSecurityCode(),
							availableBalance.negate(), AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER,
							paymentInfo.getCcdId(), ipgIdentifiactionParams, AppSysParamsUtil.getDefaultCarrierCode(),
							paymentInfo.getPayCurrencyDTO(), null, paymentInfo.getAuthorizationId(),
							paymentInfo.getPaymentBrokerRefNo(), paymentTransaction.getTnxId());
					iPassenger.addPassengerPayments(selectedPnrPaxId, payment);

					WebServicesModuleUtils.getPassengerBD().passengerRefund(reservation.getPnr(), iPassenger,
							userNotes, true, reservation.getVersion(), null, false, null, null, false);
					refundResponse.setSuccess(new SuccessType());
				} catch (Exception ex) {
					log.error(ex);
					throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_ERROR_DURING_REFUND,
							"Error ocurred in refund: Eticket - " + retEticket);
				}

			} else {
				// no refund
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_SYSTEM_NOT_AVAILABLE,
						"Ticket has a balance cannot refund - " + retEticket);
			}

		} catch (ModuleException e) {
			log.error(e);
			throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_SYSTEM_NOT_AVAILABLE,
					"System error try again later");
		}

		return myIDRefTktRes;
	}

}
