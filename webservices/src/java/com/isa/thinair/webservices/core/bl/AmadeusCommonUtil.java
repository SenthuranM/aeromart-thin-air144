package com.isa.thinair.webservices.core.bl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.XMLGregorianCalendar;

import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.TLABookingClassDataType;
import com.amadeus.ama._2008._03.TLACurrencyAmountType;
import com.amadeus.ama._2008._03.TLAFareReferenceType;
import com.amadeus.ama._2008._03.TLAOriginDestinationInformationType;
import com.amadeus.ama._2008._03.TLAUniqueIDType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusCommonUtil {

	/**
	 * Compose BookingClassData structure based on fare, tax & surcharges, booking class code, etc
	 * 
	 * @param inputFare
	 * @param inputChargesAndTax
	 * @param bookingClassCode
	 * @param fareBasisCode
	 * @param currencyExgRate
	 * @return
	 */
	public static TLABookingClassDataType composeBookingClassData(BigDecimal inputFare, BigDecimal inputChargesAndTax,
			String bookingClassCode, String fareBasisCode, CurrencyExchangeRate currencyExgRate) {

		BigDecimal minFare = new BigDecimal(inputFare.toString());
		BigDecimal totalTaxNCharges = new BigDecimal(inputChargesAndTax.toString());
		if (currencyExgRate != null) {
			minFare = AccelAeroCalculator.multiply(minFare, currencyExgRate.getMultiplyingExchangeRate());
			totalTaxNCharges = AccelAeroCalculator.multiply(totalTaxNCharges, currencyExgRate.getMultiplyingExchangeRate());
		}

		minFare = AccelAeroCalculator.parseBigDecimal(minFare.doubleValue());
		totalTaxNCharges = AccelAeroCalculator.parseBigDecimal(totalTaxNCharges.doubleValue());

		TLABookingClassDataType bookingClassData = new TLABookingClassDataType();
		TLAFareReferenceType fareBasis = new TLAFareReferenceType();
		fareBasis.setResBookDesigCode(bookingClassCode);
		fareBasis.setValue(composeAmadeusFareBasisCode(bookingClassCode, fareBasisCode));
		TLACurrencyAmountType taxAmount = new TLACurrencyAmountType();
		taxAmount.setAmount(totalTaxNCharges);
		TLACurrencyAmountType totalAmount = new TLACurrencyAmountType();
		totalAmount.setAmount(AccelAeroCalculator.add(totalTaxNCharges, minFare));

		bookingClassData.setFareBasis(fareBasis);
		bookingClassData.setInformativeTaxAmount(taxAmount);
		bookingClassData.setTotalAmount(totalAmount);
		return bookingClassData;
	}

	/**
	 * Combine single character booking Class and fare basis Code to give amadeus compatible Fare basis code
	 * 
	 * @param bookingClass
	 * @param fareBasisCode
	 * @return
	 */
	public static String composeAmadeusFareBasisCode(String bookingClass, String fareBasisCode) {
		return bookingClass + fareBasisCode;
	}

	/**
	 * Given Amadeus Fare basis code retrieve single charater booking class which is the first character
	 * 
	 * @param amadeusFareBasisCode
	 * @return
	 */
	public static String getBookingClassFromAmadeusFBC(String amadeusFareBasisCode) {
		return amadeusFareBasisCode.substring(0, 1);
	}

	/**
	 * Given Amadeus Fare basis code retrieve the original fare basis code by removing the first character belonging to
	 * the booking class
	 * 
	 * @param amadeusFareBasisCode
	 * @return
	 */
	public static String getFareBasisFromAmadeusFBC(String amadeusFareBasisCode) {
		return amadeusFareBasisCode.substring(1);
	}

	/**
	 * Get a Credit Card External Chg DTO
	 * 
	 * @param totalTicketPriceExcludingCC
	 * @param noOfPax
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ExternalChgDTO calculateCCCharges(BigDecimal totalTicketPriceExcludingCC, int noOfPayablePax, int noOfSegments,
			int chargeRatetype) throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, chargeRatetype);
		ExternalChgDTO externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		if (externalChgDTO.isRatioValueInPercentage()) {
			externalChgDTO.calculateAmount(totalTicketPriceExcludingCC);
		} else {
			externalChgDTO.calculateAmount(noOfPayablePax, noOfSegments);
		}
		BigDecimal perPaxCCCharge = AccelAeroCalculator.divide(externalChgDTO.getAmount(), new BigDecimal(noOfPayablePax));
		externalChgDTO.setAmount(AccelAeroCalculator.multiply(perPaxCCCharge, new BigDecimal(noOfPayablePax)));
		return externalChgDTO;
	}

	/**
	 * Format and return a two decimal number suitable for response
	 * 
	 * @param baseValue
	 * @param exgRate
	 * @return
	 */
	public static BigDecimal getDispValue(BigDecimal baseValue, CurrencyExchangeRate exgRate) {
		if (exgRate != null) {
			return AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(baseValue,
					exgRate.getMultiplyingExchangeRate()).doubleValue());
		} else {
			return AccelAeroCalculator.parseBigDecimal(baseValue.doubleValue());
		}
	}

	/**
	 * Compose Availability Search based on Amadeus Request Parameters
	 * 
	 * @param ondInfo
	 * @param adultCount
	 * @param childCount
	 * @param infantCount
	 * @param currencyCode
	 * @param principal
	 * @param airportDelegate
	 * @return
	 * @throws ModuleException
	 */
	public static AvailableFlightSearchDTO composeAvailSearchDTO(TLAOriginDestinationInformationType ondInfo, int adultCount,
			int childCount, int infantCount, String currencyCode, UserPrincipal principal,
			ZuluLocalTimeConversionHelper zuluHelper, List<Integer> segIds) throws ModuleException {
		String bookingType = BookingClass.BookingClassType.NORMAL;
		String cabinClass = null;
		
		if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
			cabinClass = AppSysParamsUtil.getDefaultCOS();
		} else {
			cabinClass = "Y";
		}
		
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		
		OriginDestinationInfoDTO originDestinationOption = new OriginDestinationInfoDTO();
		
		String fromAirport = ondInfo.getOriginLocation().getLocationCode();
		String toAirport = ondInfo.getDestinationLocation().getLocationCode();
		AmadeusValidationUtil.validateAirports(fromAirport, toAirport);
		
		originDestinationOption.setOrigin(ondInfo.getOriginLocation().getCodeContext());
		originDestinationOption.setDestination(ondInfo.getDestinationLocation().getCodeContext());
		
		Date departureDate = CommonUtil.parseDateTime(ondInfo.getDepartureDateTime());
		
		AmadeusValidationUtil.validateDepDate(zuluHelper.getZuluDateTime(fromAirport, departureDate));
		
		originDestinationOption.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(departureDate));
		originDestinationOption.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(departureDate));
		originDestinationOption.setFlightSegmentIds(segIds);
		originDestinationOption.setPreferredBookingType(bookingType);
		originDestinationOption.setPreferredClassOfService(cabinClass);
		
		availableFlightSearchDTO.addOriginDestination(originDestinationOption);
		
		
		availableFlightSearchDTO.setAdultCount(adultCount);
		availableFlightSearchDTO.setChildCount(childCount);
		availableFlightSearchDTO.setInfantCount(infantCount);
		
		if (currencyCode != null && !currencyCode.equals("")) {
			availableFlightSearchDTO.setPreferredCurrencyCode(currencyCode);
		} else {
			availableFlightSearchDTO.setPreferredCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		}
		
		availableFlightSearchDTO.setQuoteFares(true);
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());
		availableFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY);
		availableFlightSearchDTO.setAppIndicator(ApplicationEngine.WS.toString());
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		

		return availableFlightSearchDTO;
	}


	/**
	 * TODO move this to a string util class Join list of strings to a single string using a specified joiner
	 * 
	 * @param list
	 * @param joinBy
	 * @return
	 */
	public static String join(List<String> list, String joinBy) {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for (String s : list) {
			if (i > 0)
				sb.append(joinBy);
			sb.append(s);
			i++;
		}
		return sb.toString();
	}

	/**
	 * Create a temporary flight reference number
	 * 
	 * @param aaFlightSegmentDTO
	 * @return
	 */
	static String getTmpSegRefNo(FlightSegmentDTO aaFlightSegmentDTO) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		String origin;
		String destination;

		/**
		 * To support multi-leg flights
		 */
		if (aaFlightSegmentDTO.getSegmentCode() != null) {

			String[] originDest = aaFlightSegmentDTO.getSegmentCode().split("/");
			origin = originDest[0];
			destination = originDest[originDest.length - 1];

		} else {
			origin = aaFlightSegmentDTO.getFromAirport();
			destination = aaFlightSegmentDTO.getToAirport();
		}

		return aaFlightSegmentDTO.getFlightNumber() + origin + destination
				+ sdf.format(aaFlightSegmentDTO.getDepartureDateTime()) + sdf.format(aaFlightSegmentDTO.getArrivalDateTime());

	}

	/**
	 * Compose Amadeus Record Locator by combining the ID and date
	 * 
	 * @param recordLocator
	 * @param creationDate
	 * @return
	 */
	public static String getAmadeusRLoc(TLAUniqueIDType recordLocator, XMLGregorianCalendar creationDate) {
		String rLoc = recordLocator.getID() + creationDate.toXMLFormat();
		return rLoc;
	}

	/**
	 * Return the payment gateway compatible currency code
	 * 
	 * @param currencyCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getPGWCurrencyCode(String currencyCode, String originISOCountryCode) throws ModuleException {
		// TODO make this configurable on T_COUNTRY adding new column for identify whether origin country need be
		// considered
		String pricingCurrency = "EUR";
		if ("3O".equals(AppSysParamsUtil.getDefaultCarrierCode()) && "MA".equals(originISOCountryCode)) {
			pricingCurrency = "MAD";
		}
		return pricingCurrency;

		//
		// if (currencyCode == null)
		// return AppSysParamsUtil.getBaseCurrency();
		// IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		// ipgPaymentOptionDTO.setSelCurrency(currencyCode);
		// ipgPaymentOptionDTO.setModuleCode("XBE");
		// List<IPGPaymentOptionDTO> pgwList = WebServicesModuleUtils.getPaymentBrokerBD().getPaymentGateways(
		// ipgPaymentOptionDTO);
		// if (pgwList == null || pgwList.size() == 0) {
		// throw new ModuleException("No payment gateways found");
		// }
		// String ipgCurrencyCode = pgwList.get(0).getSelCurrency();
		// return ipgCurrencyCode;
	}

	/**
	 * Get the currency exchange rate for this instance
	 * 
	 * @param currencyCode
	 * @return
	 * @throws ModuleException
	 */
	public static CurrencyExchangeRate getCurrencyExchangeRate(String currencyCode) throws ModuleException {
		return getCurrencyExchangeRate(currencyCode, new Date());
	}

	/**
	 * Return the effective currency exchange rate for given date
	 * 
	 * @param currencyCode
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static CurrencyExchangeRate getCurrencyExchangeRate(String currencyCode, Date date) throws ModuleException {
		CurrencyExchangeRate currencyExgRate = null;
		if (!AppSysParamsUtil.getBaseCurrency().equals(currencyCode)) {
			try {
				currencyExgRate = new ExchangeRateProxy(date).getCurrencyExchangeRate(currencyCode);
			} catch (ModuleException me) {
				throw new ModuleException("Exchange rate not found for " + currencyCode, me);
			}
		}
		return currencyExgRate;
	}

	/**
	 * Get origin IP address - Removing dependency from airreservation
	 * 
	 * @param ip
	 * @return
	 */
	public static long getReservationOriginIPAddress(String ip) {
		ip = BeanUtils.nullHandler(ip);
		if (ip.length() > 0) {
			String[] splitIP = ip.split("\\.");

			try {
				return (Long.parseLong(splitIP[0]) * 16777216) + (Long.parseLong(splitIP[1]) * 65536)
						+ (Long.parseLong(splitIP[2]) * 256) + (Long.parseLong(splitIP[3]));
			} catch (Exception e) {
				return 0;
			}
		}
		return 0;
	}

	/**
	 * Returns mapped or default booking class for the given FareSummaryDTO
	 * 
	 * @param fareSummaryDTO
	 * @return
	 */

	public static String getBookingClassCode(FareSummaryDTO fareSummaryDTO) {
		String bookingClassCode = null;

		if (fareSummaryDTO != null) {

			// Sending out default booking class irrespective of the booking class returned.
			Map<String, String> compatMap = AppSysParamsUtil.getAmadeusCompatibilityMap();
			return compatMap.get(AmadeusConstants.DEFAULT_BOOKING_CLASS);

			// FIXME: Booking classs mapping implementation
			// Check default booking class mapping enabled if it does get the default booking class
			// Map<BookingClassCharMappingTO, String> bookingClassCharMappingMap =
			// WebServicesModuleUtils.getGlobalConfig().getBookingClassCharMappingMap();
			// if(WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()){
			// bookingClassCode = bookingClassCharMappingMap.get(new
			// BookingClassCharMappingTO(fareSummaryDTO.getBookingClassCode(), AmadeusConstants.GDS_ID));
			// } else {
			// // If resultant booking class is in the allowed booking classes then send the default booking class
			// boolean allowedBookingClass = false;
			// for (Entry<BookingClassCharMappingTO, String> entry : bookingClassCharMappingMap.entrySet()) {
			// if(entry.getKey().getBookingCode().equals(fareSummaryDTO.getBookingClassCode())){
			// allowedBookingClass = true;
			// break;
			// }
			// }
			//
			// if(allowedBookingClass){
			// bookingClassCode = WebServicesModuleUtils.getAmadeusConfig().getDefaultBookingClass();
			// }
			//
			// }

		}

		return bookingClassCode;
	}

	/**
	 * Returns mapped of default farebasis for the given FareSummaryDTO
	 * 
	 * @param fareSummaryDTO
	 * @return
	 * @throws AmadeusWSException
	 */
	public static String getFareCode(FareSummaryDTO fareSummaryDTO) throws AmadeusWSException {

		String fareCode = null;

		if (fareSummaryDTO != null) {
			// Check default booking class mapping enabled if it does get the default booking class
			if (WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()) {
				fareCode = fareSummaryDTO.getFareBasisCode();
			} else {

				try {
					FareDTO fareDTO = WebServicesModuleUtils.getFareBD().getFare(fareSummaryDTO.getFareId());

					if (AmadeusConstants.YES.equals(fareDTO.getFareRule().getReturnFlag())) {
						fareCode = WebServicesModuleUtils.getAmadeusConfig().getDefaultReturnFareCode();
					} else {
						fareCode = WebServicesModuleUtils.getAmadeusConfig().getDefaultOneWayFareCode();
					}

				} catch (ModuleException e) {

					throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
							"Fares not available");
				}
			}

		}

		return fareCode;
	}

	/**
	 * Validate email address for sending itinerary
	 */

	public static boolean isValidEmailAddress(String email) {

		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern;
		Matcher matcher;

		pattern = Pattern.compile(EMAIL_PATTERN);

		if (email != null && email.length() > 0) {
			matcher = pattern.matcher(email);
			return matcher.matches();
		} else {
			return false;
		}
	}

}
