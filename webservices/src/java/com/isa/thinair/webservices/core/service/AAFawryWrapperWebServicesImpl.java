/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.TransactionAttributeType;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.ErrorType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmountInPayCur;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SourceType.RequestorID;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.fawry_eg.ebpp.ifxmessages.BillInfoType;
import com.fawry_eg.ebpp.ifxmessages.BillInqRqType;
import com.fawry_eg.ebpp.ifxmessages.BillInqRsType;
import com.fawry_eg.ebpp.ifxmessages.BillRecType;
import com.fawry_eg.ebpp.ifxmessages.BillSummAmtType;
import com.fawry_eg.ebpp.ifxmessages.CurAmtType;
import com.fawry_eg.ebpp.ifxmessages.CustIdType;
import com.fawry_eg.ebpp.ifxmessages.CustomPropertiesType;
import com.fawry_eg.ebpp.ifxmessages.CustomPropertyType;
import com.fawry_eg.ebpp.ifxmessages.DeliveryMethodEnum;
import com.fawry_eg.ebpp.ifxmessages.FAWRYType;
import com.fawry_eg.ebpp.ifxmessages.MsgRqHdrType;
import com.fawry_eg.ebpp.ifxmessages.PaySvcRqType;
import com.fawry_eg.ebpp.ifxmessages.PaySvcRsType;
import com.fawry_eg.ebpp.ifxmessages.PmtInfoType;
import com.fawry_eg.ebpp.ifxmessages.PmtNotifyRsType;
import com.fawry_eg.ebpp.ifxmessages.PmtRecType;
import com.fawry_eg.ebpp.ifxmessages.PmtStatusCodeEnum;
import com.fawry_eg.ebpp.ifxmessages.PmtStatusRecType;
import com.fawry_eg.ebpp.ifxmessages.PmtStatusType;
import com.fawry_eg.ebpp.ifxmessages.PmtTransIdType;
import com.fawry_eg.ebpp.ifxmessages.PmtldTypeEnum;
import com.fawry_eg.ebpp.ifxmessages.PresSvcRqType;
import com.fawry_eg.ebpp.ifxmessages.PresSvcRsType;
import com.fawry_eg.ebpp.ifxmessages.RecCtrlOutType;
import com.fawry_eg.ebpp.ifxmessages.RequestType;
import com.fawry_eg.ebpp.ifxmessages.ResponseType;
import com.fawry_eg.ebpp.ifxmessages.SeverityEnum;
import com.fawry_eg.ebpp.ifxmessages.SignonProfileType;
import com.fawry_eg.ebpp.ifxmessages.SignonRqType;
import com.fawry_eg.ebpp.ifxmessages.SignonRsType;
import com.fawry_eg.ebpp.ifxmessages.StatusType;
import com.fawryis.ebpp.fawryswitch.bussinessfacade.ProcessRequest;
import com.fawryis.ebpp.fawryswitch.bussinessfacade.ProcessRequestResponse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.service.AAFawryWrapperWebServices;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction.TransactionBoundry;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.util.ReservationQueryUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAExternalPayTxType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * Implementation of FAWRY EBPP integration for payments
 * 
 * @author Farooq Sheikh
 * 
 */
@WebService(serviceName = "EBPPSwitchBeanService", targetNamespace = "http://bussinessfacade.fawryswitch.ebpp.fawry.com/", endpointInterface = "com.isa.thinair.webservices.api.service.AAFawryWrapperWebServices")
public class AAFawryWrapperWebServicesImpl extends BaseWebServicesImpl implements AAFawryWrapperWebServices {

	private final Log log = LogFactory.getLog(getClass());

	/*
	 * Facade method for processing FAWRY requests
	 */
	public ProcessRequestResponse processRequest(ProcessRequest processRequest) {

		log.debug("Begin AAFawryWrapperWebServicesImpl.processRequest()");

		ProcessRequestResponse fawryProcessRequestResponse = null;
		String msgCode = null;
		String sender = null;
		String receiver = null;
		String reqUID = null;

		try {
			if (processRequest == null) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Request cannot be empty or null");
			}

			RequestType request = processRequest.getArg0().getRequest();
			SignonRqType signonRq = request.getSignonRq();
			msgCode = signonRq.getSignonProfile().getMsgCode();
			triggerPreProcessErrorIfPresent();
			sender = signonRq.getSignonProfile().getSender().trim();

			if (!(sender != null && sender.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.Sender))) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Sender_is_not_authorized,
						"Sender[" + sender + "] is not authorized");
			}

			receiver = signonRq.getSignonProfile().getReceiver().trim();
			if (!(receiver != null && receiver.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID))) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Receiver, "Receiver["
						+ receiver + "] is not invalid");
			}

			if (msgCode == null) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_type_not_exists,
						"Message Code is missing");
			} else if (msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.BillInRq_MsgCode)) {
				reqUID = request.getPresSvcRq().getAsyncRqUID();
				handleTransactionIn(null, TransactionAttributeType.REQUIRES_NEW, processRequest);
				
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.CLIENT_IDENTIFIER, WebservicesConstants.ClientIdentifiers.FAWRY_EBPP);
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.EXTERNAL_REQ_ID, reqUID);
				
				fawryProcessRequestResponse = processBillInQueryRequest(processRequest);
			} else if (msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.PmtNotifyRq_MsgCode)) {
				String requestedTnxId = null;
				try {
					// get Transaction ID from BillingNumber
					requestedTnxId = request.getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0).getPmtInfo().getBillNumber()
							.trim();
				} catch (NullPointerException e) {// FIXME - revise
					throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
							"Billing Number is missing");
				}
				// for isRetry functionality
				try {
					handleTransactionIn(requestedTnxId, TransactionAttributeType.MANDATORY, processRequest);
				} catch (WebservicesException wse) {
					// 1. check if isRetry is not set; throw same exception and follow normal behavior
					if (!(processRequest.getArg0().getRequest().getIsRetry())) {
						throw wse;
					}
					// 2. if Transaction is Invalid; there is a possibility; this transaction may already being
					// processed
					if (wse.getOtaErrorCodes() != null
							&& wse.getOtaErrorCodes().get(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED) != null) {
						return processRetryPmtNotifyRq(processRequest);
					}
				}
				
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.CLIENT_IDENTIFIER,
						WebservicesConstants.ClientIdentifiers.FAWRY_EBPP);
				fawryProcessRequestResponse = processPmtNotifyRequest(processRequest);
			} else {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Unsupported_Message,
						"Requested Message not supported");
			}
		} catch (Exception ex) {
			log.error("Processing FAWRY request failed", ex);
			ThreadLocalData.setTriggerPostProcessError(ex);
			if (fawryProcessRequestResponse == null)
				fawryProcessRequestResponse = new ProcessRequestResponse();

			if (msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.BillInRq_MsgCode)) {
				if (ex != null && ex instanceof WebservicesException) {
					WebservicesException wse = (WebservicesException) ex;
					if (wse.getOtaErrorCodes() != null && wse.getOtaErrorCodes().size() > 0) {
						String error = (String) wse.getOtaErrorCodes().keySet().toArray()[0];
						if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_)) {
							composeBillInResponseForError(fawryProcessRequestResponse, processRequest, new WebservicesException(
									WebservicesConstants.FAWRYConstants.IFawryErrorCode.Req_Cancelled_Booking,
									"Biller Request Customer To Contact The Biller Offices"));
						} else if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT)) {
							composeBillInResponseForError(fawryProcessRequestResponse, processRequest, new WebservicesException(
									WebservicesConstants.FAWRYConstants.IFawryErrorCode.Payment_Amount_validation_Error,
									"Payment Amount validation Error"));
						} else if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_PAID)) {
							composeBillInResponseForError(fawryProcessRequestResponse, processRequest, new WebservicesException(
									WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
									"Balance Payment Record for Billing Acoount Number Not Found"));
						} else {
							composeBillInResponseForError(fawryProcessRequestResponse, processRequest, ex);
						}
					} else {
						composeBillInResponseForError(fawryProcessRequestResponse, processRequest, ex);
					}
				} else if (msgCode != null && msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.PmtNotifyRq_MsgCode)) {
					composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
				} else if (ex != null && ex instanceof ModuleException) {
					ModuleException mxe = (ModuleException) ex;
					if (mxe.getExceptionCode().equals("airreservations.arg.noLongerExist")) {
						WebservicesException wse = new WebservicesException(
								WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_billing_account_number,
								"Invalid Billing Acoount Number");
						composeBillInResponseForError(fawryProcessRequestResponse, processRequest, wse);
					} else {
						composeBillInResponseForError(fawryProcessRequestResponse, processRequest, ex);
					}
				} else {
					composeBillInResponseForError(fawryProcessRequestResponse, processRequest, ex);
				}
			} else {
				if (ex != null && ex instanceof WebservicesException) {
					WebservicesException wse = (WebservicesException) ex;
					if (wse.getOtaErrorCodes() != null && wse.getOtaErrorCodes().size() > 0) {
						String error = (String) wse.getOtaErrorCodes().keySet().toArray()[0];
						if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_)) {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest,
									new WebservicesException(
											WebservicesConstants.FAWRYConstants.IFawryErrorCode.Req_Cancelled_Booking,
											"Biller Request Customer To Contact The Biller Offices"));
						} else if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT)) {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest,
									new WebservicesException(
											WebservicesConstants.FAWRYConstants.IFawryErrorCode.Payment_Amount_validation_Error,
											"Payment Amount validation Error"));
						} else if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_PAID)) {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest,
									new WebservicesException(
											WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
											"Balance Payment Record for Billing Acoount Number Not Found"));
						} else if (error.equals(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED)) {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest,
									new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
											(String) wse.getOtaErrorCodes().get(error)));
						} else if (error.equals(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error)) {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest,
									new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
											"No existing transactional context or transaction specified in request is invalid"));
						} else {
							composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
						}
					} else {
						composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
					}
				} else if (msgCode != null && msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.PmtNotifyRq_MsgCode)) {
					composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
				} else if (ex != null && ex instanceof ModuleException) {
					ModuleException mxe = (ModuleException) ex;
					if (mxe.getExceptionCode().equals("airreservations.arg.noLongerExist")) {
						WebservicesException wse = new WebservicesException(
								WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_billing_account_number,
								"Invalid Billing Acoount Number");
						composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, wse);
					} else {
						composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
					}
				} else {
					composePmtNotifyResponseForError(fawryProcessRequestResponse, processRequest, ex);
				}
			}
		} finally {
			if (msgCode != null && msgCode.equalsIgnoreCase(WebservicesConstants.FAWRYConstants.PmtNotifyRq_MsgCode)) {
				handleTransactionOut(WebServiceTransaction.TransactionBoundry.FINISH);
			} else {
				handleTransactionOut(WebServiceTransaction.TransactionBoundry.START);
			}
		}

		return fawryProcessRequestResponse;
	}

	/**
	 * Method to Handle Retry PmtNotifyRq
	 * 
	 * @throws WebservicesException
	 * @throws ModuleException
	 * 
	 */
	private ProcessRequestResponse processRetryPmtNotifyRq(ProcessRequest processRequest) throws WebservicesException,
			ModuleException {

		String PNR = null;
		String deliveryMethod = null;

		try {

			PNR = processRequest.getArg0().getRequest().getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0).getPmtInfo()
					.getBillingAcct().trim();

			if (!ReservationApiUtils.isPNRValid(PNR)) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
						"Booking Reference [" + PNR + "] is invalid");
			}

			deliveryMethod = processRequest.getArg0().getRequest().getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0).getPmtInfo()
					.getDeliveryMethod().trim();

			if (deliveryMethod == null || deliveryMethod.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Delievery Method not exists");

		} catch (WebservicesException we) {
			throw we;
		} catch (Exception e) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
					"Booking Reference/Delievery Method not Exists");
		}

		ExternalPaymentTnx successExternalPaymentTnx = null;
		ExtPayTxCriteriaDTO successExtPayTxCriteriaDTO = new ExtPayTxCriteriaDTO();
		successExtPayTxCriteriaDTO.setPnr(PNR);
		successExtPayTxCriteriaDTO.addStatus("S");
		Map pnrTnxMap = WebServicesModuleUtils.getReservationQueryBD().getExtPayTransactions(successExtPayTxCriteriaDTO);

		if (pnrTnxMap != null && pnrTnxMap.containsKey(PNR)) {
			PNRExtTransactionsTO pnrExtTransactionsTO = (PNRExtTransactionsTO) pnrTnxMap.get(PNR);
			if (pnrExtTransactionsTO != null) {
				successExternalPaymentTnx = (ExternalPaymentTnx) pnrExtTransactionsTO.getExtPayTransactions().iterator().next();
			}
		}

		if (successExternalPaymentTnx == null)
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Transaction_State,
					"No existing transactional context or transaction specified in request is invalid");

		return transformAirBookRSToPmtNotifyRs(null, processRequest, successExternalPaymentTnx.getBalanceQueryKey(),
				CommonUtil.parse(new Date(successExternalPaymentTnx.getInternalTnxStartTimestamp().getTime())));

	}

	/**
	 * Method to Handle Transaction for outgoing response
	 * 
	 */
	private void handleTransactionOut(TransactionBoundry transactionBoundry) {
		log.debug("AAFawryWrapperWebServicesImpl::handleTransactionOut called");

		ITransaction currTnx = ThreadLocalData.getCurrentTransaction();

		if (currTnx != null && transactionBoundry != null) {
			if (transactionBoundry == WebServiceTransaction.TransactionBoundry.FINISH) {
				try {
					WebServicesModuleUtils.getTrasnsactionStore().removeTransaction(currTnx.getId());
				} catch (WebservicesException e) {
					log.error("AAFawry Transaction removel issue for the transaction id :" + currTnx.getId(), e);
				}
			}
		}

	}

	/**
	 * Method to Handle Transaction for incoming request
	 * 
	 */
	private void handleTransactionIn(String requestedTnxId, TransactionAttributeType transType, ProcessRequest processRequest)
			throws WebservicesException, ModuleException {

		log.debug("AAFawryWrapperWebServicesImpl::handleTransactionIn called");
		ITransaction existingTnx = null;

		if (requestedTnxId != null && !"".equals(requestedTnxId)) {
			existingTnx = WebServicesModuleUtils.getTrasnsactionStore().getTransaction(requestedTnxId);
		}

		ITransaction requestedTnx = null;

		switch (transType) {

		case MANDATORY:
			if (requestedTnxId == null || "".equals(requestedTnxId)) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Requested Service should be called with Transaction specified");
			} else {
				Map pnrTnxMap = WebServicesModuleUtils.getReservationQueryBD().getExtPayTransactionsForReqUID(
						processRequest.getArg0().getRequest().getPaySvcRq().getAsyncRqUID());
				if (pnrTnxMap.isEmpty()) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
							"No existing transactional context or transaction specified in request is invalid");
				} else if (existingTnx == null) {// Checked the reUID is in T_EXTERNAL_PAYMENT_TX.If there are entries
													// for given reqUID it will create new transaction.
					
					
					existingTnx = WebServicesModuleUtils.getTrasnsactionStore().createNewTransaction();
					existingTnx.setParameter(WebservicesContext.CLIENT_IDENTIFIER,
							WebservicesConstants.ClientIdentifiers.FAWRY_EBPP);
					createOldTransactionDependencies(processRequest, existingTnx, pnrTnxMap);
				}

			}
			requestedTnx = existingTnx;
			break;
		case REQUIRES_NEW:
			if (requestedTnxId != null && !"".equals(requestedTnxId)) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Requested Service should be called without Transaction specified");
			}
			requestedTnx = WebServicesModuleUtils.getTrasnsactionStore().createNewTransaction();
			break;
		default:
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Unsupported Transaction Attribute Found");
		}

		// Bind the transaction to the current thread
		ThreadLocalData.setCurrentTransaction(requestedTnx);
	}

	/**
	 * Method to Prepare PmtNotifyResponse in case of Exception Occurs
	 * 
	 */
	private void composePmtNotifyResponseForError(ProcessRequestResponse fawryProcessRequestResponse,
			ProcessRequest processRequest, Exception exception) {

		composePmtNotifyRsDraft(fawryProcessRequestResponse, processRequest);

		StatusType status = new StatusType();

		if (exception != null && exception instanceof WebservicesException) {

			WebservicesException wse = (WebservicesException) exception;

			if (wse.getOtaErrorCodes() != null && wse.getOtaErrorCodes().size() > 0) {

				for (String fawryErrorCode : wse.getOtaErrorCodes().keySet()) {
					try {
						status.setStatusCode(fawryErrorCode);
						status.setStatusDesc(wse.getOtaErrorCodes().get(fawryErrorCode));
						status.setSeverity(SeverityEnum.ERROR);
					} catch (Exception e) {// Some OTA Validation Exception
						status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
						status.setStatusDesc(wse.getOtaErrorCodes().get(fawryErrorCode));
						status.setSeverity(SeverityEnum.ERROR);
					}
				}
			} else {
				status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
				status.setStatusDesc("AccelAero Error Please Report[" + exception.getMessage() + "]");
				status.setSeverity(SeverityEnum.ERROR);
			}

		} else {

			status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
			status.setStatusDesc("AccelAero Error Please Report[" + exception.getMessage() + "]");
			status.setSeverity(SeverityEnum.ERROR);
		}

		fawryProcessRequestResponse.getReturn().getResponse().getPaySvcRs().setStatus(status);

	}

	/**
	 * Triggers error if there any preprocessing error occured. All the service methods should call this method as the
	 * first step.
	 * 
	 * @throws WebservicesException
	 */
	private void triggerPreProcessErrorIfPresent() throws WebservicesException {
		Boolean triggerError = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_PREPROCESS_ERROR);
		if (triggerError != null && triggerError.booleanValue()) {
			throw (WebservicesException) ThreadLocalData.getWSContextParam(
					WebservicesContext.PREPROCESS_ERROR_OBJECT);
		}

	}

	/**
	 * Method to prepare/generate PmtNotifyRs intial Response
	 */
	private void composePmtNotifyRsDraft(ProcessRequestResponse response, ProcessRequest processRequest) {

		FAWRYType frwType = new FAWRYType();
		ResponseType res = new ResponseType();
		PaySvcRsType paySvcRs = new PaySvcRsType();
		SignonRsType singonRs = new SignonRsType();

		SignonRqType signonRq = processRequest.getArg0().getRequest().getSignonRq();

		Date currentZuluDate = new Date(System.currentTimeMillis());
		try {
			singonRs.setServerDt(CommonUtil.parse(currentZuluDate));
		} catch (ModuleException e) {
			log.error(e);
		}

		// ECHO as per fawry Documentation
		singonRs.setClientDt(signonRq.getClientDt());

		// Echo CustLangPref if exist
		if (signonRq.getCustLangPref() != null)
			singonRs.setCustLangPref(signonRq.getCustLangPref());

		singonRs.setLanguage(WebservicesConstants.FAWRYConstants.PRIMARY_LANG_CODE);

		SignonProfileType signonProfileRs = new SignonProfileType();

		SignonProfileType signonProfileRq = processRequest.getArg0().getRequest().getSignonRq().getSignonProfile();

		signonProfileRs.setMsgCode(WebservicesConstants.FAWRYConstants.PmtNotifyRs_MsgCode);

		signonProfileRs.setReceiver(signonProfileRq.getSender());

		// set with FAWRY assigned Biller ID Code for AirArabia
		// signonProfileRs.setSender(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID);
		signonProfileRs.setSender(signonProfileRq.getReceiver());

		signonProfileRs.setVersion(WebservicesConstants.FAWRYConstants.VERSION_PmtNotifyRs);

		PaySvcRqType paySvcRq = processRequest.getArg0().getRequest().getPaySvcRq();

		// Echo as per fawry Documentation
		paySvcRs.setRqUID(paySvcRq.getRqUID());

		// Echo if present
		if (paySvcRq.getAsyncRqUID() != null)
			paySvcRs.setAsyncRqUID(paySvcRq.getAsyncRqUID());

		// paySvcRs.setMsgRqHdr(paySvcRq.getMsgRqHdr());

		PmtNotifyRsType pmtNotifyRs = new PmtNotifyRsType();

		// Echo CustId if exists
		CustIdType custIDRq = paySvcRq.getPmtNotifyRq().getCustId();
		if (custIDRq != null) {

			pmtNotifyRs.setCustId(custIDRq);
		}

		// Update PmtTransId list from Request
		PmtRecType pmtRec = paySvcRq.getPmtNotifyRq().getPmtRec().get(0);

		PmtStatusRecType pmtStatusRec = new PmtStatusRecType();

		for (Iterator<PmtTransIdType> pmtTransIt = pmtRec.getPmtTransId().iterator(); pmtTransIt.hasNext();) {
			PmtTransIdType pmtTransId = pmtTransIt.next();
			pmtStatusRec.getPmtTransId().add(pmtTransId);
		}

		pmtNotifyRs.getPmtStatusRec().add(pmtStatusRec);

		paySvcRs.setPmtNotifyRs(pmtNotifyRs);
		singonRs.setSignonProfile(signonProfileRs);
		res.setSignonRs(singonRs);
		frwType.setResponse(res);
		res.setPaySvcRs(paySvcRs);

		response.setReturn(frwType);

	}

	/**
	 * Method to prepare/generate BillInRq intial Response
	 */
	private void composeBillInRsDraft(ProcessRequestResponse response, ProcessRequest processRequest) {

		FAWRYType frwType = new FAWRYType();
		ResponseType res = new ResponseType();
		PresSvcRsType preSvcRs = new PresSvcRsType();
		SignonRsType singonRs = new SignonRsType();

		SignonRqType signonRq = processRequest.getArg0().getRequest().getSignonRq();

		Date currentZuluDate = new Date(System.currentTimeMillis());
		try {
			singonRs.setServerDt(CommonUtil.parse(currentZuluDate));
		} catch (ModuleException e) {
			log.error(e);
		}

		// ECHO as per fawry Documentation
		singonRs.setClientDt(signonRq.getClientDt());

		// Echo CustLangPref if exist in request
		if (signonRq.getCustLangPref() != null)
			singonRs.setCustLangPref(signonRq.getCustLangPref());

		singonRs.setLanguage(WebservicesConstants.FAWRYConstants.PRIMARY_LANG_CODE);

		SignonProfileType signonProfileRs = new SignonProfileType();

		SignonProfileType signonProfileRq = processRequest.getArg0().getRequest().getSignonRq().getSignonProfile();

		signonProfileRs.setMsgCode(WebservicesConstants.FAWRYConstants.BillInRs_MsgCode);
		// set the Receiver and Sender
		signonProfileRs.setReceiver(signonProfileRq.getSender());

		// //set with FAWRY assigned Biller ID Code for AirArabia
		// signonProfileRs.setSender(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID);
		signonProfileRs.setSender(signonProfileRq.getReceiver());

		signonProfileRs.setVersion(WebservicesConstants.FAWRYConstants.VERSION_BillInRs);

		PresSvcRqType preSvcRq = processRequest.getArg0().getRequest().getPresSvcRq();

		preSvcRs.setRqUID(preSvcRq.getRqUID());

		// Echo Header If exist in request
		if (preSvcRq.getMsgRqHdr() != null)
			preSvcRs.setMsgRqHdr(preSvcRq.getMsgRqHdr());

		BillInqRsType billInRs = new BillInqRsType();

		BillInqRqType billInRq = preSvcRq.getBillInqRq();

		// Echo CustId if exists in request
		if (billInRq.getCustId() != null)
			billInRs.setCustId(billInRq.getCustId());

		BillRecType billRec = new BillRecType();

		billRec.setBillingAcct(billInRq.getBillingAcct());

		if (billInRq.getBillTypeCode() != null)
			billRec.setBillTypeCode(billInRq.getBillTypeCode());

		if (billInRq.getBillerId() != null)
			billRec.setBillerId(billInRq.getBillerId());

		BillInfoType billInfo = new BillInfoType();

		BillSummAmtType billSummary = new BillSummAmtType();
		CurAmtType amt = new CurAmtType();
		amt.setAmt(new BigDecimal(0));
		billSummary.setCurAmt(amt);
		billInfo.getBillSummAmt().add(billSummary);
		billRec.setBillInfo(billInfo);
		billInRs.getBillRec().add(billRec);
		preSvcRs.setBillInqRs(billInRs);
		singonRs.setSignonProfile(signonProfileRs);
		res.setSignonRs(singonRs);
		frwType.setResponse(res);
		res.setPresSvcRs(preSvcRs);

		response.setReturn(frwType);

	}

	/**
	 * Method to Prepare BillIn Response in case of Exception Occurs
	 * 
	 */
	private void composeBillInResponseForError(ProcessRequestResponse fawryProcessRequestResponse, ProcessRequest processRequest,
			Exception exception) {

		composeBillInRsDraft(fawryProcessRequestResponse, processRequest);

		StatusType status = new StatusType();

		if (exception != null && exception instanceof WebservicesException) {

			WebservicesException wse = (WebservicesException) exception;

			if (wse.getOtaErrorCodes() != null && wse.getOtaErrorCodes().size() > 0) {

				for (String fawryErrorCode : wse.getOtaErrorCodes().keySet()) {
					try {
						status.setStatusCode(fawryErrorCode);
						status.setStatusDesc(wse.getOtaErrorCodes().get(fawryErrorCode));
						status.setSeverity(SeverityEnum.ERROR);
					} catch (Exception e) {// Some OTA Validation Exception
						status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
						status.setStatusDesc(wse.getOtaErrorCodes().get(fawryErrorCode));
						status.setSeverity(SeverityEnum.ERROR);
					}
				}
			} else {
				status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
				status.setStatusDesc("AccelAero Error Please Report[" + exception.getMessage() + "]");
				status.setSeverity(SeverityEnum.ERROR);
			}

		} else {
			status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error);
			status.setStatusDesc("AccelAero Error Please Report[" + exception.getMessage() + "]");
			status.setSeverity(SeverityEnum.ERROR);
		}

		fawryProcessRequestResponse.getReturn().getResponse().getPresSvcRs().setStatus(status);

	}

	/**
	 * Method to process PmntNotify Request
	 * 
	 * @return ProcessRequestResponse
	 */
	private ProcessRequestResponse processPmtNotifyRequest(ProcessRequest processRequest) throws ModuleException,
			WebservicesException {

		log.debug("In processPmtNotifyRequest ");

		Object[] transformedData = transformPmtNotifyRqToAirBookModifyRS(processRequest);

		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) transformedData[0];

		ProcessRequestResponse response = transformAirBookRSToPmtNotifyRs(otaAirBookRS, processRequest,
				(String) transformedData[2], (XMLGregorianCalendar) transformedData[3]);

		log.debug("!!!!!!!!!!!!!!!!!!processPmtNotifyRequest compeleted!!!!!!!!!!!!!!!!");

		return response;

	}

	private ProcessRequestResponse transformAirBookRSToPmtNotifyRs(OTAAirBookRS otaAirBookRS, ProcessRequest processRequest,
			String balanceQueryKey, XMLGregorianCalendar clientDate) {

		ProcessRequestResponse response = new ProcessRequestResponse();

		composePmtNotifyRsDraft(response, processRequest);

		StatusType status = new StatusType();

		status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Success);
		status.setStatusDesc("SUCCESS");
		status.setSeverity(SeverityEnum.INFO);

		PaySvcRsType paySvcRs = response.getReturn().getResponse().getPaySvcRs();

		paySvcRs.setStatus(status);

		PmtTransIdType pmtTransId = new PmtTransIdType();
		pmtTransId.setPmtIdType(PmtldTypeEnum.BLRPTN);
		pmtTransId.setPmtId(balanceQueryKey);
		pmtTransId.setCreatedDt(clientDate);

		paySvcRs.getPmtNotifyRs().getPmtStatusRec().get(0).getPmtTransId().add(pmtTransId);

		return response;
	}

	private Object[] transformPmtNotifyRqToAirBookModifyRS(ProcessRequest processRequest) throws ModuleException,
			WebservicesException {

		String PNR = null;
		String BalanceQueryKey = null;
		String deliveryMethod = null;
		String currencyCode = null;
		BigDecimal balanceAmountToPay = null;
		XMLGregorianCalendar externalTxTimestamp = null;
		String companyCode = null;
		String externalSystemTxRPH = null;
		String internalTxTimestamp = null;
		String billerId = null;
		Long billTypeCode = null;
		BigDecimal totalEquivAmt = null;
		BigDecimal totalBaseAmt = null;

		try {
			PmtInfoType pmtInfo = processRequest.getArg0().getRequest().getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0)
					.getPmtInfo();

			PNR = pmtInfo.getBillingAcct().trim();

			if (!ReservationApiUtils.isPNRValid(PNR)) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
						"Booking Reference [" + PNR + "] is invalid");
			}

			// Bind Balance Query Key from Current Transaction
			ThreadLocalData.getCurrentTnxParam(WebservicesContext.BALANCE_QUERY_KEY);
			BalanceQueryKey = (String) ThreadLocalData.getCurrentTnxParam(WebservicesContext.BALANCE_QUERY_KEY);
			totalEquivAmt = (BigDecimal) ThreadLocalData.getCurrentTnxParam(WebservicesContext.TOTAL_BALANCE_AMT);
			totalBaseAmt = (BigDecimal) ThreadLocalData.getCurrentTnxParam(WebservicesContext.TOTAL_BALANCE_AMT_BASE);

			if (BalanceQueryKey == null || BalanceQueryKey.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Balance Query Key Not found in Transaction");

			// Bind internalTxTimestamp from Current Transaction
			internalTxTimestamp = (String) ThreadLocalData.getCurrentTnxParam(WebservicesContext.TX_START_TIMESTAMP);

			deliveryMethod = pmtInfo.getDeliveryMethod().trim();

			if (deliveryMethod == null || deliveryMethod.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Delievery Method not exists");

			balanceAmountToPay = pmtInfo.getCurAmt().getAmt();
			// Jira 4741
			if (!AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency())) {
				if (totalEquivAmt.compareTo(balanceAmountToPay) != 0)
					throw new WebservicesException(
							WebservicesConstants.FAWRYConstants.IFawryErrorCode.Payment_Amount_validation_Error,
							"Amount Mismatch Error");
			}

			if (balanceAmountToPay == null || balanceAmountToPay.doubleValue() <= 0)
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"balanceAmountToPay can not be zero or less");

			currencyCode = pmtInfo.getCurAmt().getCurCode();

			if (currencyCode == null || currencyCode.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Currency Code does not exists");

			// Jira 4741
			if (!(currencyCode.equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency())))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Currency [" + currencyCode + "] can not match with FAWRY Currency["
								+ AppSysParamsUtil.getFAWRYCurrency() + "]");

			billerId = pmtInfo.getBillerId();

			if (billerId != null && !(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID.equalsIgnoreCase(billerId)))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_BillerId,
						"Invalid Biller Id[" + billerId + "] ");

			billTypeCode = pmtInfo.getBillTypeCode();

			if (billTypeCode != null && billTypeCode.longValue() != WebservicesConstants.FAWRYConstants.FAWRY_BILL_TYPE_CODE)
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Bill_Type_Code,
						"Invalid Biller Type Code Specified [" + billTypeCode + "] ");

			PmtRecType pmtRec = processRequest.getArg0().getRequest().getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0);

			PmtStatusType pmtStatus = pmtRec.getPmtStatus();

			if (pmtStatus.getPmtStatusCode() == null || !(pmtStatus.getPmtStatusCode().equals(PmtStatusCodeEnum.PMT_NEW)))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Can not find New Payment Transaction Element in Message");

			// Check FAWRY Transaction ID and TimeStamp from Request

			for (Iterator<PmtTransIdType> pmtTransIt = pmtRec.getPmtTransId().iterator(); pmtTransIt.hasNext();) {
				PmtTransIdType pmtTransId = pmtTransIt.next();
				if (pmtTransId.getPmtIdType().equals(PmtldTypeEnum.FPTN)) {
					externalSystemTxRPH = pmtTransId.getPmtId().trim();
					externalTxTimestamp = pmtTransId.getCreatedDt();
					break;
				}

			}

			if (externalSystemTxRPH == null)
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Can not find FAWRY PmtTransId element");

			companyCode = (String) ThreadLocalData.getCurrentWSContext()
					.getParameter(WebservicesConstants.FAWRYConstants.Company_Code);

			if (companyCode == null || companyCode.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
						"Can not find company Code in Current Session. Please Report to Accelero");

		} catch (WebservicesException we) {
			throw we;
		} catch (Exception e) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
					"Required Fields are missing or null");
		}

		OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();

		AirReservationType.Fulfillment fulfillment = null;
		PaymentDetailType paymentDetailType = null;

		otaAirBookModifyRQ.setPOS(new POSType());
		SourceType pos = composeFawryPOS(deliveryMethod);
		otaAirBookModifyRQ.getPOS().getSource().add(pos);

		otaAirBookModifyRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirBookModifyRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_RetrieveRes);
		otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirBookModifyRQ.setEchoToken(UID.generate());
		otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirBookModifyRQ.setTransactionIdentifier(null);

		AirBookModifyRQ otaBookModifyRQ = new AirBookModifyRQ();

		otaBookModifyRQ.setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_BALANCE_PAYMENT));

		// Set the PNR in AirBookModifyRequest
		UniqueIDType uniqueIDType = new UniqueIDType();
		uniqueIDType.setID(PNR);
		otaBookModifyRQ.getBookingReferenceID().add(uniqueIDType);

		otaBookModifyRQ.setFulfillment(fulfillment = new AirReservationType.Fulfillment());
		fulfillment.setPaymentDetails(new AirReservationType.Fulfillment.PaymentDetails());
		fulfillment.getPaymentDetails().getPaymentDetail().add(paymentDetailType = new PaymentDetailType());
		PaymentAmount amount = new PaymentAmount();
		// Jira 4741
		PaymentAmountInPayCur currAmount = new PaymentAmountInPayCur();

		if (!AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency())) {
			amount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(totalBaseAmt));
			amount.setCurrencyCode(currencyCode);
			amount.setDecimalPlaces(WebservicesConstants.OTAConstants.DECIMALS_AED);

			currAmount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(balanceAmountToPay));
			currAmount.setCurrencyCode(AppSysParamsUtil.getFAWRYCurrency());
			currAmount.setDecimalPlaces(WebservicesConstants.OTAConstants.DECIMALS_AED);
			paymentDetailType.setPaymentAmountInPayCur(currAmount);
		} else {
			amount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(balanceAmountToPay));
			amount.setCurrencyCode(currencyCode);
			amount.setDecimalPlaces(WebservicesConstants.OTAConstants.DECIMALS_AED);
		}

		paymentDetailType.setPaymentAmount(amount);
		paymentDetailType.setDirectBill(new DirectBillType());
		paymentDetailType.getDirectBill().setCompanyName(new CompanyName());
		paymentDetailType.getDirectBill().getCompanyName().setValue(pos.getRequestorID().getID());

		paymentDetailType.getDirectBill().getCompanyName().setCode(companyCode);

		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();

		AAExternalPayTxType wsExternalPayTx = new AAExternalPayTxType();

		wsExternalPayTx.setBookingReferenceID(PNR);
		wsExternalPayTx.setBalanceQueryRPH(BalanceQueryKey);
		wsExternalPayTx.setAmount(amount.getAmount());

		wsExternalPayTx.setExternalTxStatus(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS));
		wsExternalPayTx.setExternalTxTimestamp(CommonUtil.getFormattedDate(new Date(CommonUtil.getTime(externalTxTimestamp))));
		wsExternalPayTx.setExternalSystemTxRPH(externalSystemTxRPH);

		wsExternalPayTx.setAATxEndTimestamp(internalTxTimestamp);

		airBookModifyRQAAExt.setCurrentExtPayTxInfo(wsExternalPayTx);

		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);

		airBookModifyRQAAExt.setAALoadDataOptions(options);

		otaAirBookModifyRQ.setAirBookModifyRQ(otaBookModifyRQ);

		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS, new Boolean(true));

		BaseWebServicesImpl baseWebServices = new BaseWebServicesImpl();

		OTAAirBookRS otaAirBookRS = baseWebServices.modifyReservation(otaAirBookModifyRQ, airBookModifyRQAAExt);

		/* Verify the Payment detailed from OTABookRS */

		if (otaAirBookRS == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		ErrorsType errors = otaAirBookRS.getErrors();

		if (errors != null) {
			ErrorType error = errors.getError().get(0);
			if (error.getCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_)))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
						error.getShortText());

			else if (error.getCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT)))
				throw new WebservicesException(
						WebservicesConstants.FAWRYConstants.IFawryErrorCode.Payment_Amount_validation_Error, error.getShortText());

			else if (error.getCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_PAID)))
				throw new WebservicesException(
						WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment, error.getShortText());
			else
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.General_Error,
						error.getShortText());
		}

		AirReservationType airReservation = null;

		Iterator it = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();
		while (it.hasNext()) {
			Object o = it.next();
			if (o instanceof AirReservationType) {
				airReservation = (AirReservationType) o;
				break;
			}
		}

		if (airReservation == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		return new Object[] { otaAirBookRS, PNR, BalanceQueryKey,
				CommonUtil.parse(CommonUtil.parseDate(internalTxTimestamp).getTime()), balanceAmountToPay, currencyCode };
	}

	private ProcessRequestResponse processBillInQueryRequest(ProcessRequest processRequest) throws Exception {

		log.debug("In processBillInQueryRequest ");

		Object[] transformedData = transformBillInRqToAirBookRS(processRequest);

		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) transformedData[0];
		String pnr = (String) transformedData[1];

		ProcessRequestResponse response = transformAirBookRSToBillInRs(otaAirBookRS, processRequest, pnr);

		log.debug("!!!!!!!!!!!!!!!!!!processBillInQueryRequest compeleted!!!!!!!!!!!!!!!!");

		return response;
	}

	private ProcessRequestResponse transformAirBookRSToBillInRs(OTAAirBookRS otaAirBookRS, ProcessRequest processRequest,
			String pnr) throws WebservicesException, ModuleException {

		if (otaAirBookRS == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		AirReservationType airReservation = null;

		Iterator it = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();
		while (it.hasNext()) {
			Object o = it.next();
			if (o instanceof AirReservationType) {
				airReservation = (AirReservationType) o;
				break;
			}
		}

		if (airReservation == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		ValidateAirReservation(airReservation);

		String balanceQuery = "";
		BigDecimal totalBalanceAmount;
		BigDecimal totalBalanceAmountBase;

		List<Object> aaAirReservationExtList = TPAExtensionUtil.getInstance().unmarshall(
				airReservation.getTPAExtensions().getAny(), AAAirReservationExt.class);

		if (aaAirReservationExtList != null && aaAirReservationExtList.size() > 0) {

			AAAirReservationExt airReservationExtensions = (AAAirReservationExt) aaAirReservationExtList.get(0);

			if (airReservationExtensions.getCurrentExtPayTxInfo() != null) {

				balanceQuery = airReservationExtensions.getCurrentExtPayTxInfo().getBalanceQueryRPH();

				// Jira 4741
				if (!AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency())) {
					totalBalanceAmount = airReservation.getPriceInfo().getItinTotalFare().getTotalEquivFare().getAmount();
					totalBalanceAmountBase = airReservationExtensions.getCurrentExtPayTxInfo().getAmount();
					// Now Set Base amount in Current Transaction
					ThreadLocalData.setCurrentTnxParam(WebservicesContext.TOTAL_BALANCE_AMT_BASE, totalBalanceAmountBase);
				} else
					totalBalanceAmount = airReservationExtensions.getCurrentExtPayTxInfo().getAmount();

				// Now Set Balance Query Key & totalBalAmount in Current Transaction
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.BALANCE_QUERY_KEY, balanceQuery);
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.TOTAL_BALANCE_AMT, totalBalanceAmount);


				String tnStartTimestamp = airReservationExtensions.getCurrentExtPayTxInfo().getAATxStartTimestamp();
				// Now Set TxStartTimestamp in Current Transaction for Reconciliation
				ThreadLocalData.setCurrentTnxParam(WebservicesContext.TX_START_TIMESTAMP, tnStartTimestamp);

			} else {
				throw new WebservicesException(
						WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
						"Balance Payment Record for Booking Reference [" + pnr + "] Not Found");
			}

		} else {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
					"Balance Payment Record for Booking Reference [" + pnr + "] Not Found");
		}

		ProcessRequestResponse response = new ProcessRequestResponse();

		composeBillInRsDraft(response, processRequest);

		addItineraryInfo(response, airReservation);

		StatusType status = new StatusType();

		status.setStatusCode(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Success);
		status.setStatusDesc("SUCCESS");
		status.setSeverity(SeverityEnum.INFO);

		BillInqRsType billInRs = new BillInqRsType();

		PresSvcRqType preSvcRq = processRequest.getArg0().getRequest().getPresSvcRq();

		BillInqRqType billInRq = preSvcRq.getBillInqRq();

		// set the status to success
		response.getReturn().getResponse().getPresSvcRs().setStatus(status);

		// Echo CustId if exists in request
		if (billInRq.getCustId() != null)
			billInRs.setCustId(billInRq.getCustId());

		billInRs.setDeliveryMethod(billInRq.getDeliveryMethod());

		// ECHO if exists in request
		if (billInRq.getServiceType() != null)
			billInRs.setServiceType(billInRq.getServiceType());

		// ECHO if exists in request
		if (billInRq.getPmtType() != null)
			billInRs.setPmtType(billInRq.getPmtType());

		// This flag indicate if the inquiry request is against the total amount of all bills for the requested billing
		// account
		billInRs.setIncOpenAmt(true);

		RecCtrlOutType recCtrlOut = new RecCtrlOutType();
		recCtrlOut.setCursor(0L);
		recCtrlOut.setMatchedRec(1);
		recCtrlOut.setSentRec(1);

		BillRecType billRec = new BillRecType();

		billRec.setBillingAcct(pnr);
		billRec.setBillerId(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID);

		// bill type code is the unique code identified by FAWRY to uniqley identify the different bill types.
		billRec.setBillTypeCode(WebservicesConstants.FAWRYConstants.FAWRY_BILL_TYPE_CODE);

		BillInfoType billInfo = new BillInfoType();

		BillSummAmtType billSummary = new BillSummAmtType();
		CurAmtType amt = new CurAmtType();

		amt.setAmt(totalBalanceAmount);

		// Jira 4741
		if (!AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency()))
			amt.setCurCode(AppSysParamsUtil.getFAWRYCurrency());
		else
			amt.setCurCode(AppSysParamsUtil.getBaseCurrency());

		billSummary.setCurAmt(amt);

		billInfo.getBillSummAmt().add(billSummary);
		billRec.setBillInfo(billInfo);
				
		ITransaction currTnx = ThreadLocalData.getCurrentTransaction();
		billRec.setBillNumber(currTnx.getId());
		billInRs.getBillRec().add(billRec);
		billInRs.setRecCtrlOut(recCtrlOut);

		response.getReturn().getResponse().getPresSvcRs().setBillInqRs(billInRs);

		return response;
	}

	private void ValidateAirReservation(AirReservationType airReservation) throws WebservicesException {

		// 1. Throw Exception for expire booking

		Date currentZuluDate = new Date(System.currentTimeMillis());
		boolean allFlightsFlown = true;

		for (Iterator<OriginDestinationOptionType> ODOptionIt = airReservation.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption().iterator(); ODOptionIt.hasNext();) {
			OriginDestinationOptionType OriginDestObj = ODOptionIt.next();
			for (Iterator<BookFlightSegmentType> flightSegIt = OriginDestObj.getFlightSegment().iterator(); flightSegIt.hasNext();) {
				BookFlightSegmentType flightSeg = flightSegIt.next();
				Date flightDepDateTime = new Date(CommonUtil.getTime(flightSeg.getDepartureDateTime()));
				if (flightDepDateTime.compareTo(currentZuluDate) > 0) {
					allFlightsFlown = false;
				}

			}
		}

		if (allFlightsFlown)
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
					"RESERVATION IS ALREADY EXPIRED");

	}

	private void addItineraryInfo(ProcessRequestResponse response, AirReservationType airReservation) {

		MsgRqHdrType msgHdr = response.getReturn().getResponse().getPresSvcRs().getMsgRqHdr();

		if (msgHdr == null) {
			msgHdr = new MsgRqHdrType();
			response.getReturn().getResponse().getPresSvcRs().setMsgRqHdr(msgHdr);
		}

		CustomPropertiesType custmProperties = msgHdr.getCustomProperties();

		if (custmProperties == null) {
			custmProperties = new CustomPropertiesType();
			msgHdr.setCustomProperties(custmProperties);
		}

		List<CustomPropertyType> custmProperty = msgHdr.getCustomProperties().getCustomProperty();

		CustomPropertyType flightInfo = new CustomPropertyType();

		StringBuilder sb = new StringBuilder();

		for (Iterator<OriginDestinationOptionType> ODOptionIt = airReservation.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption().iterator(); ODOptionIt.hasNext();) {
			OriginDestinationOptionType OriginDestObj = ODOptionIt.next();
			for (Iterator<BookFlightSegmentType> flightSegIt = OriginDestObj.getFlightSegment().iterator(); flightSegIt.hasNext();) {
				BookFlightSegmentType flightSeg = flightSegIt.next();
				sb.append(flightSeg.getDepartureAirport().getLocationCode() + "/"
						+ flightSeg.getArrivalAirport().getLocationCode() + "  ");
				sb.append(flightSeg.getFlightNumber() + " ");
				sb.append("DEP " + flightSeg.getDepartureDateTime().toString() + " ");
				sb.append("ARR " + flightSeg.getArrivalDateTime().toString() + " ;");
			}
		}

		flightInfo.setKey("FlightInfo");
		flightInfo.setValue(sb.toString());

		custmProperty.add(flightInfo);

		CustomPropertyType passengerInfo = new CustomPropertyType();

		StringBuilder paxValue = new StringBuilder();

		for (Iterator<AirTravelerType> travelerInfoIt = airReservation.getTravelerInfo().getAirTraveler().iterator(); travelerInfoIt
				.hasNext();) {

			AirTravelerType travelerInfo = travelerInfoIt.next();

			paxValue.append(travelerInfo.getPassengerTypeCode() + " ");

			if (travelerInfo.getPersonName().getNameTitle() != null && travelerInfo.getPersonName().getNameTitle().size() > 0)
				paxValue.append(travelerInfo.getPersonName().getNameTitle().get(0) + " ");

			if (travelerInfo.getPersonName().getGivenName() != null && travelerInfo.getPersonName().getGivenName().size() > 0)
				paxValue.append(travelerInfo.getPersonName().getGivenName().get(0) + " ");

			paxValue.append(travelerInfo.getPersonName().getSurname() + " ;");

		}

		passengerInfo.setKey("PassengerInfo");
		passengerInfo.setValue(paxValue.toString());

		custmProperty.add(passengerInfo);
	}

	private SourceType composeFawryPOS(String deliveryMethod) {
		SourceType sourceType = new SourceType();

		sourceType.setTerminalID("FawryWraperTestUser/FawryWraper Test Runner");
		RequestorID requestorID = new RequestorID();
		sourceType.setRequestorID(requestorID);

		// Agent ID
		requestorID.setID((String) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.LOGIN_NAME));
		// Set Type to Cooperate
		requestorID.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_COMPANY));
		// Set the Booking Channel
		SourceType.BookingChannel bookingChannel = new SourceType.BookingChannel();
		sourceType.setBookingChannel(bookingChannel);

		if (AppSysParamsUtil.isFawryCDMRoundingEnabled()) {

			if (deliveryMethod.equalsIgnoreCase(DeliveryMethodEnum.ATM.name()))
				bookingChannel.setType(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM));

			else if (deliveryMethod.equalsIgnoreCase(DeliveryMethodEnum.CAM.name()))
				bookingChannel.setType(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM));
			else
				bookingChannel.setType(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET));

		} else {
			// Temporary Set Channel to Default Channel as set in Database as per Client Request; Bcz they don't want
			// rounding feature in beginning
			bookingChannel.setType((String) ThreadLocalData.getCurrentWSContext()
					.getParameter(WebservicesContext.SALES_CHANNEL_ID));
		}

		ThreadLocalData.setWSConextParam(WebservicesContext.REQUEST_POS, sourceType);

		return sourceType;
	}

	private Object[] transformBillInRqToAirBookRS(ProcessRequest processRequest) throws ModuleException, WebservicesException {

		String PNR = null;
		String deliveryMethod = null;
		String billerId = null;
		Long billTypeCode = null;
		try {

			PNR = processRequest.getArg0().getRequest().getPresSvcRq().getBillInqRq().getBillingAcct();

			if (!ReservationApiUtils.isPNRValid(PNR)) {
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
						"Booking Reference [" + PNR + "] is invalid");
			}

			deliveryMethod = processRequest.getArg0().getRequest().getPresSvcRq().getBillInqRq().getDeliveryMethod().trim();

			if (deliveryMethod == null || deliveryMethod.equals(""))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
						"Delievery Method not exists");

			billerId = processRequest.getArg0().getRequest().getPresSvcRq().getBillInqRq().getBillerId();

			if (billerId != null && !(WebservicesConstants.FAWRYConstants.FAWRY_BILLER_ID.equalsIgnoreCase(billerId)))
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_BillerId,
						"Invalid Biller Id[" + billerId + "] ");

			billTypeCode = processRequest.getArg0().getRequest().getPresSvcRq().getBillInqRq().getBillTypeCode();

			if (billTypeCode != null && billTypeCode.longValue() != WebservicesConstants.FAWRYConstants.FAWRY_BILL_TYPE_CODE)
				throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Bill_Type_Code,
						"Invalid Biller Type Code Specified [" + billTypeCode + "] ");

		} catch (WebservicesException we) {
			throw we;
		} catch (Exception e) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Message_Not_Valid,
					"Booking Reference/Delievery Method not Exists");
		}

		AALoadDataOptionsType loadDataOptions = new AALoadDataOptionsType();

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(PNR);

		// Setting the Load Information
		loadDataOptions.setLoadAccSummary(true);
		loadDataOptions.setLoadAirItinery(true);
		loadDataOptions.setLoadPriceInfoTotals(true);
		loadDataOptions.setLoadTravelerInfo(true);
		loadDataOptions.setLoadPTCFullFilment(true);
		loadDataOptions.setLoadPTCAccSummary(true);

		loadDataOptions.setLoadFullFilment(true);
		loadDataOptions.setLoadPTCPriceInfo(true);

		OTAReadRQ request = new OTAReadRQ();

		request.setPOS(new POSType());
		SourceType pos = composeFawryPOS(deliveryMethod);
		request.getPOS().getSource().add(pos);

		request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_RetrieveRes);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(null);

		ReadRequest readRequest = null;
		request.setReadRequests(new ReadRequests());
		request.getReadRequests().getReadRequest().add(readRequest = new ReadRequest());
		readRequest.setUniqueID(new UniqueIDType());
		readRequest.getUniqueID().setID(PNR);

		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS, new Boolean(true));

		// skip Departure Date validation checks
		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_IGNORE_DEPARTURE_DATE_VALIDATEION,
				new Boolean(true));

		ReservationQueryUtil resQUtil = new ReservationQueryUtil();
		OTAAirBookRS otaAirBookRS = resQUtil.getReservation(request, loadDataOptions, true);

		return new Object[] { otaAirBookRS, PNR };
	}

	private void createOldTransactionDependencies(ProcessRequest processRequest, ITransaction existingTnx, Map pnrTnxMap)
			throws ModuleException, WebservicesException {

		ThreadLocalData.setCurrentTransaction(existingTnx);

		PmtInfoType pmtInfo = processRequest.getArg0().getRequest().getPaySvcRq().getPmtNotifyRq().getPmtRec().get(0)
				.getPmtInfo();

		String PNR = pmtInfo.getBillingAcct().trim();
		String deliveryMethod = null;

		PNRExtTransactionsTO pnrPayTnxs = (PNRExtTransactionsTO) pnrTnxMap.get(PNR);
		ExternalPaymentTnx currentExternalPaymentTnx = (ExternalPaymentTnx) pnrPayTnxs.getExtPayTransactions().iterator().next();
		Integer salesChannel = Integer.parseInt(currentExternalPaymentTnx.getChannel());
		if (salesChannel == SalesChannelsUtil.SALES_CHANNEL_EBI_ATM) {
			deliveryMethod = DeliveryMethodEnum.ATM.name();
		} else if (salesChannel == SalesChannelsUtil.SALES_CHANNEL_EBI_CDM) {
			deliveryMethod = DeliveryMethodEnum.CAM.name();
		}

		AALoadDataOptionsType loadDataOptions = new AALoadDataOptionsType();
		// Setting the Load Information
		loadDataOptions.setLoadAccSummary(true);
		loadDataOptions.setLoadAirItinery(true);
		loadDataOptions.setLoadPriceInfoTotals(true);
		loadDataOptions.setLoadTravelerInfo(true);
		loadDataOptions.setLoadPTCFullFilment(true);
		loadDataOptions.setLoadPTCAccSummary(true);

		loadDataOptions.setLoadFullFilment(true);
		loadDataOptions.setLoadPTCPriceInfo(true);

		OTAReadRQ request = new OTAReadRQ();

		request.setPOS(new POSType());
		SourceType pos = composeFawryPOS(deliveryMethod);
		request.getPOS().getSource().add(pos);

		request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_RetrieveRes);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(null);

		ReadRequest readRequest = null;
		request.setReadRequests(new ReadRequests());
		request.getReadRequests().getReadRequest().add(readRequest = new ReadRequest());
		readRequest.setUniqueID(new UniqueIDType());
		readRequest.getUniqueID().setID(PNR);

		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS, new Boolean(true));

		// skip Departure Date validation checks
		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_IGNORE_DEPARTURE_DATE_VALIDATEION,
				new Boolean(true));

		ReservationQueryUtil resQUtil = new ReservationQueryUtil();
		OTAAirBookRS otaAirBookRS = resQUtil.getReservation(request, loadDataOptions, true);

		if (otaAirBookRS == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		AirReservationType airReservation = null;

		Iterator it = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();
		while (it.hasNext()) {
			Object o = it.next();
			if (o instanceof AirReservationType) {
				airReservation = (AirReservationType) o;
				break;
			}
		}

		if (airReservation == null) {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Invalid_Billing_Account,
					"No matching booking found");
		}

		ValidateAirReservation(airReservation);

		String BalanceQuery = "";
		BigDecimal totalBalanceAmount;
		BigDecimal totalBalanceAmountBase;

		List<Object> aaAirReservationExtList = TPAExtensionUtil.getInstance().unmarshall(
				airReservation.getTPAExtensions().getAny(), AAAirReservationExt.class);

		if (aaAirReservationExtList != null && aaAirReservationExtList.size() > 0) {

			AAAirReservationExt airReservationExtensions = (AAAirReservationExt) aaAirReservationExtList.get(0);

			if (airReservationExtensions.getCurrentExtPayTxInfo() != null) {

				BalanceQuery = airReservationExtensions.getCurrentExtPayTxInfo().getBalanceQueryRPH();

				if (!AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(AppSysParamsUtil.getFAWRYCurrency())) {
					totalBalanceAmount = airReservation.getPriceInfo().getItinTotalFare().getTotalEquivFare().getAmount();
					totalBalanceAmountBase = airReservationExtensions.getCurrentExtPayTxInfo().getAmount();
					// Now Set Base amount in Current Transaction
					existingTnx.setParameter(WebservicesContext.TOTAL_BALANCE_AMT_BASE, totalBalanceAmountBase);
				} else
					totalBalanceAmount = airReservationExtensions.getCurrentExtPayTxInfo().getAmount();

				// Now Set Balance Query Key & totalBalAmount in Current Transaction
				existingTnx.setParameter(WebservicesContext.BALANCE_QUERY_KEY, BalanceQuery);
				existingTnx.setParameter(WebservicesContext.TOTAL_BALANCE_AMT, totalBalanceAmount);

				String tnStartTimestamp = airReservationExtensions.getCurrentExtPayTxInfo().getAATxStartTimestamp();
				// Now Set TxStartTimestamp in Current Transaction for Reconciliation
				existingTnx.setParameter(WebservicesContext.TX_START_TIMESTAMP, tnStartTimestamp);

			} else {
				throw new WebservicesException(
						WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
						"Balance Payment Record for Booking Reference [" + PNR + "] Not Found");
			}

		} else {
			throw new WebservicesException(WebservicesConstants.FAWRYConstants.IFawryErrorCode.Bill_not_available_for_payment,
					"Balance Payment Record for Booking Reference [" + PNR + "] Not Found");
		}

	}

}
