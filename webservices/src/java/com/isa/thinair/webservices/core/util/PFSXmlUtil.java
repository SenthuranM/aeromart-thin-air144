package com.isa.thinair.webservices.core.util;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pfs.PfsEntryDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PfsXmlDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRQ;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRS;
import com.isaaviation.thinair.webservices.api.updatepfs.AAStatusErrorsAndWarnings;
import com.isaaviation.thinair.webservices.api.updatepfs.EntryStatus;
import com.isaaviation.thinair.webservices.api.updatepfs.PFSFlight;
import com.isaaviation.thinair.webservices.api.updatepfs.PFSPax;

/**
 * AccelAero DCS PFS procesing is done through this util. If modification happenes to this class Please notify the AA
 * DCS team
 * 
 * @author malaka
 * 
 */
public class PFSXmlUtil {

	private final static Log log = LogFactory.getLog(PFSXmlUtil.class);

	public static AAOTAPFSUpdateRS updatePfs(AAOTAPFSUpdateRQ aaOtaPfsUpdateRQ) {
		AAOTAPFSUpdateRS rs = new AAOTAPFSUpdateRS();
		AAStatusErrorsAndWarnings errorsAndWarnings = new AAStatusErrorsAndWarnings();

		PfsXmlDTO pfsXmlDTO = getPfsXmlDTO(aaOtaPfsUpdateRQ);

		try {
			Integer pfsId = WebServicesModuleUtils.getReservationBD().updatePfs(pfsXmlDTO);
			WebServicesModuleUtils.getReservationBD().reconcileReservations(pfsId, false, true, null,
					AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
			errorsAndWarnings.setSuccess(true);
		} catch (Exception e) {
			errorsAndWarnings.setSuccess(false);
			if (e instanceof ModuleException) {
				errorsAndWarnings.setErrorText(((ModuleException) e).getMessageString());
			} else {
				errorsAndWarnings.setErrorText(e.getMessage());
			}
			log.error(e);
		}

		rs.setStatusErrorsAndWarnings(errorsAndWarnings);

		return rs;
	}

	private static PfsXmlDTO getPfsXmlDTO(AAOTAPFSUpdateRQ aaOtaPfsUpdateRQ) {
		PfsXmlDTO pfsXmlDTO = new PfsXmlDTO();

		PFSFlight pfsFlight = aaOtaPfsUpdateRQ.getFlight();
		pfsXmlDTO.setDateDownloaded(new Date());
		pfsXmlDTO.setDepartureDate(pfsFlight.getDepartureDate().toGregorianCalendar().getTime());
		pfsXmlDTO.setFlightNumber(pfsFlight.getFlightNo());
		pfsXmlDTO.setFromAddress("DCS_WS");
		pfsXmlDTO.setFromAirport(pfsFlight.getFromAirport());

		for (PFSPax pfsPax : aaOtaPfsUpdateRQ.getPfsPaxs()) {
			PfsEntryDTO entryDTO = new PfsEntryDTO();
			entryDTO.setArrivalAirport(pfsPax.getArrivalAirport());
			entryDTO.setCabinClassCode(pfsPax.getCabinClassCode());
			entryDTO.setDepartureAirport(pfsFlight.getFromAirport());
			String paxStatus = "";
			if (pfsPax.getEntryStatus() == EntryStatus.GO_SHORE) {
				paxStatus = ReservationInternalConstants.PfsPaxStatus.GO_SHORE;
			} else if (pfsPax.getEntryStatus() == EntryStatus.NO_REC) {
				paxStatus = ReservationInternalConstants.PfsPaxStatus.NO_REC;
			} else if (pfsPax.getEntryStatus() == EntryStatus.NO_SHORE) {
				paxStatus = ReservationInternalConstants.PfsPaxStatus.NO_SHORE;
			} else if (pfsPax.getEntryStatus() == EntryStatus.OFF_LD) {
				paxStatus = ReservationInternalConstants.PfsPaxStatus.OFF_LD;
			} else {
				log.error(" UnSupported PfsPaxStatus, please improve...");
			}
			entryDTO.setEntryStatus(paxStatus);

			entryDTO.seteTicketNo(pfsPax.getETicketNo());
			entryDTO.setFirstName(pfsPax.getFirstName());
			entryDTO.setFlightDate(pfsFlight.getDepartureDate().toGregorianCalendar().getTime());
			entryDTO.setFlightNumber(pfsFlight.getFlightNo());

			entryDTO.setInfant(getInfant(pfsPax));

			entryDTO.setLastName(pfsPax.getLastname());
			entryDTO.setPaxType(pfsPax.getPaxType());
			entryDTO.setPnr(pfsPax.getPnr());
			entryDTO.setReceivedDate(new Date());
			entryDTO.setTitle(pfsPax.getTitle());

			pfsXmlDTO.getEntryDTOs().add(entryDTO);

		}

		return pfsXmlDTO;
	}

	private static PfsEntryDTO getInfant(PFSPax pfsPax) {
		PfsEntryDTO infant = new PfsEntryDTO();

		PFSPax pfsInfant = pfsPax.getInfant();
		if (pfsInfant != null) {
			infant.setTitle(pfsInfant.getTitle());
			infant.setFirstName(pfsInfant.getFirstName());
			infant.setLastName(pfsInfant.getLastname());
		}

		return infant;
	}
}
