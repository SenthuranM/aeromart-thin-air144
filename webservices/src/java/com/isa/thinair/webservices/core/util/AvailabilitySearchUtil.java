/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirSearchPrefsType.CabinPref;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS.AAAirAvailRSExt;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IAvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRQ;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRS;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AAOnDDynBestOfferType;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AAOnDDynBestOffersType;

/**
 * Manipulates OTA_AirAvailRQ/RS
 * 
 * @author Mohamed Nasly
 */
public class AvailabilitySearchUtil extends BaseUtil {

	private final Log log = LogFactory.getLog(getClass());

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public AvailabilitySearchUtil() {
		super();
	}

	public OTAAirAvailRS getAvailability(OTAAirAvailRQ oTAAirAvailRQ) throws ModuleException, WebservicesException {
		OTAAirAvailRS otaAirAvailRS = new OTAAirAvailRS();
		otaAirAvailRS.setErrors(new ErrorsType());
		otaAirAvailRS.setWarnings(new WarningsType());

		// As the user is initiating a new availability search we have to clear the FareSegChargeTO from the
		// transaction.
		PriceQuoteUtil.clearFareSegmentChargeTO();

		AvailableFlightSearchDTO availableFlightSearchDTO = extractAvailabilitySearchReqParams(oTAAirAvailRQ, otaAirAvailRS);
		availableFlightSearchDTO.setQuoteFares(true);
		if (availableFlightSearchDTO != null) {
			AvailableFlightDTO availableFlightDTO = WebServicesModuleUtils.getReservationQueryBD()
					.getAvailableFlightsWithAllFares(availableFlightSearchDTO, null);

			// For the selected flight save the FareSegChargeTO in the user tnx.
			if (availableFlightDTO.getSelectedFlight() != null) {
				PriceQuoteUtil.addFareSegmentChargeTOToTransaction(availableFlightDTO.getSelectedFlight(),
						availableFlightSearchDTO);
				ServiceTaxContainer applicableServiceTax = AirproxyModuleUtils.getReservationBD().getApplicableServiceTax(
						availableFlightDTO.getSelectedFlight().getSelectedOndFlight(OndSequence.OUT_BOUND),
						EXTERNAL_CHARGES.JN_OTHER);
				if (applicableServiceTax != null) {
					List<ServiceTaxContainer> applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
					applicableServiceTaxes.add(applicableServiceTax);
					ThreadLocalData.setCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES, applicableServiceTaxes);
				}
			}

			prepareAvailabilitySearchResponse(oTAAirAvailRQ, otaAirAvailRS, availableFlightDTO, availableFlightSearchDTO);
		}
		return otaAirAvailRS;
	}

	/**
	 * OTA params expected version, transactionIdentifier, sequenceNumber, echoToken, primaryLang Origin Destination
	 * Departure datetime Adults/children/infants counts POS - airportCode, agentCode isDirectFligtsOnly
	 * 
	 * @param otaAirAvailRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private AvailableFlightSearchDTO extractAvailabilitySearchReqParams(OTAAirAvailRQ otaAirAvailRQ, OTAAirAvailRS otaAirAvailRS)
			throws ModuleException, WebservicesException {
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo();

		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AVAILABILITY_REQUEST, principal, trackInfo,
				null);

		Collection<String> obAirports = new HashSet<String>();
		Collection<String> inAirports = new HashSet<String>();
		Collection<String> cabinClasses = new ArrayList<String>();
		Integer departureVariance = null;
		Integer returnVariance = null;

		Date returnDate = null;
		boolean isReturn = false;

		OriginDestinationInfoDTO outboundOndInfo = new OriginDestinationInfoDTO();

		for (OriginDestinationInformationType originDestinationInformation : otaAirAvailRQ.getOriginDestinationInformation()) {

			addOndLog(originDestinationInformation, principal);

			/**
			 * departure date of the very first segment is taken as departure date
			 */
			if (outboundOndInfo.getDepartureDateTimeStart() == null) {
				outboundOndInfo.setDepartureDateTimeStart(CommonUtil.parseDate(
						originDestinationInformation.getDepartureDateTime().getValue()).getTime());
			}

			/** very first airport is taken as origin */
			if (obAirports.size() == 0) {
				outboundOndInfo.setOrigin(originDestinationInformation.getOriginLocation().getLocationCode());
			}

			if (!isReturn
					&& CommonUtil.isReturnSegment(obAirports, originDestinationInformation.getOriginLocation().getLocationCode(),
							originDestinationInformation.getDestinationLocation().getLocationCode())) {
				isReturn = true;
			}

			/** departure/return variance */
			if (originDestinationInformation.getDepartureDateTime().getWindowBefore() != null
					|| originDestinationInformation.getDepartureDateTime().getWindowAfter() != null) {

				int windowBefore = originDestinationInformation.getDepartureDateTime().getWindowBefore() != null
						? originDestinationInformation.getDepartureDateTime().getWindowBefore().getDays()
						: 0;
				int windowAfter = originDestinationInformation.getDepartureDateTime().getWindowAfter() != null
						? originDestinationInformation.getDepartureDateTime().getWindowAfter().getDays()
						: 0;

				if (!isReturn) {
					if (departureVariance == null || departureVariance.intValue() < windowBefore
							|| departureVariance.intValue() < windowAfter) {
						departureVariance = Math.max(windowBefore, windowAfter);
					}
				} else {
					if (returnVariance == null || returnVariance.intValue() < windowBefore
							|| returnVariance.intValue() < windowAfter) {
						returnVariance = Math.max(windowBefore, windowAfter);
					}
				}
			}

			/**
			 * last station of the outbound journey is taken as the destination airport
			 */
			if (!isReturn) {
				outboundOndInfo.setDestination(originDestinationInformation.getDestinationLocation().getLocationCode());
			}

			/**
			 * departure date of the very first return segment is taken as the arrival date
			 */
			if (isReturn && returnDate == null) {

				returnDate = CommonUtil.parseDate(originDestinationInformation.getDepartureDateTime().getValue()).getTime();
			}

			if (!isReturn) {
				obAirports.add(originDestinationInformation.getOriginLocation().getLocationCode());
				obAirports.add(originDestinationInformation.getDestinationLocation().getLocationCode());
			} else {
				inAirports.add(originDestinationInformation.getOriginLocation().getLocationCode());
				inAirports.add(originDestinationInformation.getDestinationLocation().getLocationCode());
			}

			/** Adding the Booking classes for validation */
			if (originDestinationInformation instanceof org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) {
				org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation odInfo = (org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) originDestinationInformation;
				if (odInfo.getTravelPreferences() != null && odInfo.getTravelPreferences().getCabinPref() != null) {
					CabinPref cabinPref = BeanUtils.getFirstElement(odInfo.getTravelPreferences().getCabinPref());
					if (cabinPref.getCabin() != null) {
						cabinClasses.add(cabinPref.getCabin().value());
					}
				}
			}

			if (originDestinationInformation.getGroundSegmentAvailability() != null
					&& (originDestinationInformation.getGroundSegmentAvailability())) {
				availableFlightSearchDTO.setGroundSegmentAvailability(true);
			}
		}

		boolean areAirportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(
				outboundOndInfo.getOrigin(), outboundOndInfo.getDestination());

		if (!areAirportsValid) {
			log.warn("Invalid flight search request found in extractAvailabilitySearchReqParams(OTAAirAvailRQ). "
					+ "From/To aiports validation failed. " + "[From airport=" + outboundOndInfo.getOrigin() + ","
					+ "To airport=" + outboundOndInfo.getDestination() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OND, "Invalid From and/or To Airport(s)");
		}

		/**
		 * Journey having exact reverse segments of outbound segments is considered as a Return journey
		 */
		if (isReturn) {
			if (!CommonUtil.validateReturnJouney(obAirports, inAirports)) {
				log.error("ERROR occured in extractAvailabilitySearchReqParams(OTAAirAvailRQ). "
						+ "Return journey verification failed. " + "[outbound airports="
						+ CommonUtil.getCollectionElementsStr(obAirports) + "," + "inbound airports="
						+ CommonUtil.getCollectionElementsStr(inAirports) + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Flight Segment(s)");
			}
		}

		String bookingClass = null;
		String bookingType = BookingClass.BookingClassType.NORMAL;
		if (otaAirAvailRQ.getSpecificFlightInfo() != null && otaAirAvailRQ.getSpecificFlightInfo().getBookingClassPref() != null) {
			bookingClass = otaAirAvailRQ.getSpecificFlightInfo().getBookingClassPref().getResBookDesigCode();
			if (bookingClass != null) {
				Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
				if (standByBCList.contains(bookingClass)) {
					bookingType = BookingClass.BookingClassType.STANDBY;
					bookingClass = null;
				}
			}
		}

		String cabinClass = getCabinClass(cabinClasses, otaAirAvailRQ);

		Date depatureDate = outboundOndInfo.getDepartureDateTimeStart();
		Date preferredDepatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
		Date depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(preferredDepatureDateTimeStart, departureVariance != null
				? Math.min(3, departureVariance.intValue()) * -1
				: 0);

		Date preferredDepatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
		Date depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(preferredDepatureDateTimeEnd,
				departureVariance != null ? Math.min(3, departureVariance.intValue()) : 0);

		outboundOndInfo.setPreferredDateTimeStart(preferredDepatureDateTimeStart);
		outboundOndInfo.setPreferredDateTimeEnd(preferredDepatureDateTimeEnd);
		outboundOndInfo.setDepartureDateTimeStart(depatureDateTimeStart);
		outboundOndInfo.setDepartureDateTimeEnd(depatureDateTimeEnd);
		outboundOndInfo.setPreferredBookingType(bookingType);
		outboundOndInfo.setPreferredClassOfService(cabinClass);
		outboundOndInfo.setPreferredBookingClass(bookingClass);
		if (otaAirAvailRQ.getFlexiQuote() != null && otaAirAvailRQ.getFlexiQuote()) {
			outboundOndInfo.setQuoteFlexi(true);
		}
		availableFlightSearchDTO.addOriginDestination(outboundOndInfo);

		if (returnDate != null) {
			OriginDestinationInfoDTO inboundOndInfo = new OriginDestinationInfoDTO();
			Date preferredReturnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
			Date returnDateTimeStart = CalendarUtil.getOfssetAddedTime(preferredReturnDateTimeStart, returnVariance != null
					? Math.min(3, returnVariance.intValue()) * -1
					: 0);

			Date preferredReturnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			Date returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(preferredReturnDateTimeEnd,
					returnVariance != null ? Math.min(3, returnVariance.intValue()) : 0);

			inboundOndInfo.setPreferredDateTimeStart(preferredReturnDateTimeStart);
			inboundOndInfo.setPreferredDateTimeEnd(preferredReturnDateTimeEnd);
			inboundOndInfo.setDepartureDateTimeStart(returnDateTimeStart);
			inboundOndInfo.setDepartureDateTimeEnd(returnDateTimeEnd);
			inboundOndInfo.setOrigin(outboundOndInfo.getDestination());
			inboundOndInfo.setDestination(outboundOndInfo.getOrigin());
			inboundOndInfo.setPreferredBookingType(bookingType);
			inboundOndInfo.setPreferredClassOfService(cabinClass);
			inboundOndInfo.setPreferredBookingClass(bookingClass);
			if (otaAirAvailRQ.getFlexiQuote() != null && otaAirAvailRQ.getFlexiQuote()) {
				inboundOndInfo.setQuoteFlexi(true);
			}
			availableFlightSearchDTO.addOriginDestination(inboundOndInfo);
		}

		// pax counts
		TravelerInfoSummaryType travelerInfoSummaryType = otaAirAvailRQ.getTravelerInfoSummary();
		int adults = 0;
		int children = 0;
		int infants = 0;
		for (TravelerInformationType travelerInformationType : travelerInfoSummaryType.getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adults += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					children += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}

		String preferredCurrencyCode = null;
		if (travelerInfoSummaryType.getPriceRequestInformation() != null) {
			preferredCurrencyCode = travelerInfoSummaryType.getPriceRequestInformation().getCurrencyCode();
		}
		availableFlightSearchDTO.setPreferredCurrencyCode(preferredCurrencyCode);

		if (adults == 0 && children == 0) {
			log.error("ERROR occured in extractAvailabilitySearchReqParams(OTAAirAvailRQ). "
					+ "Invalid adults/children pax count. " + "[adults pax=" + adults + "]" + "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult pax count cannot be zero");
		}

		if (infants > adults) {
			log.error("ERROR occured in extractAvailabilitySearchReqParams(OTAAirAvailRQ). "
					+ "Infants cannot be more than adults. " + "[adults pax=" + adults + ", infant pax =" + infants + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_NUMBER_OF_ADULTS,
					"Infants cannot be more than adults");
		}

		availableFlightSearchDTO.setAdultCount(adults);
		availableFlightSearchDTO.setChildCount(children);
		availableFlightSearchDTO.setInfantCount(infants);

		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());

		availableFlightSearchDTO.setAppIndicator(ApplicationEngine.WS.toString());

		// FIXME make followings configurable
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

		if (otaAirAvailRQ.isDirectFlightsOnly()) {
			availableFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY);
		} else {
			availableFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		}

		Map<Integer, Boolean> ondFlexiQuote = new HashMap<Integer, Boolean>();
		if (otaAirAvailRQ.getFlexiQuote() != null) {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, otaAirAvailRQ.getFlexiQuote());
			ondFlexiQuote.put(OndSequence.IN_BOUND, otaAirAvailRQ.getFlexiQuote());
		} else {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, false);
			ondFlexiQuote.put(OndSequence.IN_BOUND, false);
		}

		availableFlightSearchDTO.setOndQuoteFlexi(ondFlexiQuote);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		int availabilityRestriction = (AuthorizationUtil.hasPrivileges(privilegeKeys, PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY
				.equals(bookingType))
				? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
				: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

		availableFlightSearchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);

		boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
		if (isReturn && !availableFlightSearchDTO.isOpenReturnSearch()) {
			availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
		} else if (otaAirAvailRQ.getModifiedSegmentInfo() != null) {
			String pnr = otaAirAvailRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();

			if (pnr != null) { // Availability request for Modify OND
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				Reservation reservation = getReservation(pnrModesDTO, null);

				AirItineraryType.OriginDestinationOptions modifiedOND = otaAirAvailRQ.getModifiedSegmentInfo().getAirItinerary()
						.getOriginDestinationOptions();
				// get the pnrSegIds after checking the validity
				List<Integer> pnrSegIdsForMod = extractPNRSegIds(reservation, modifiedOND);
				ReservationUtil.setExistingSegInfo(reservation, availableFlightSearchDTO, pnrSegIdsForMod);
			}
		}

		return availableFlightSearchDTO;
	}

	private String getCabinClass(Collection<String> cabinClassSet, OTAAirAvailRQ oTAAirAvailRQ) throws WebservicesException {

		Collection<String> ccSet = new HashSet<String>();
		if (!cabinClassSet.isEmpty()) { // we have user specified cabin classes
			if (cabinClassSet.size() != oTAAirAvailRQ.getOriginDestinationInformation().size()) {
				log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ cabinClassSet.size() + "] [expected = " + oTAAirAvailRQ.getOriginDestinationInformation().size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Cabin class missing from OND");
			} else {
				ccSet.addAll(cabinClassSet);
			}
		}

		if (!ccSet.isEmpty() && ccSet.size() > 1) { // we only support booking class per one journey
			log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). "
					+ "Invalid no of booking class. Booking class shoud be same for all ONDs" + "[booking class given="
					+ cabinClassSet.size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
					"Too many cabin classes specifyed .Only one is supported per availability search");
		}
		String cabinClass = BeanUtils.getFirstElement(ccSet);
		// defaulting to defaultCOS if no CC is specified.
		if (cabinClass == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}

		return cabinClass;
	}

	public static String getLogicalCabinClass(String cabinClass) {
		String defaultLogicalCCCode = null;
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (cabinClass != null && cabinClass.equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCCode = defaultLogicalCCDetailDTO.getDefaultLogicalCCCode();
					break;
				}
			}

		} else {
			defaultLogicalCCCode = cabinClass;
		}
		return defaultLogicalCCCode;
	}

	/**
	 * OTA params set version, transactionIdentifier, sequenceNumber, echoToken, primaryLang
	 * OriginDestinationInformation - origin,destination,departure datetime
	 * OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment
	 * 
	 * @param otaAirAvailRQ
	 * @param availableFlights
	 * @return
	 * @throws WebservicesException
	 */
	private void prepareAvailabilitySearchResponse(OTAAirAvailRQ otaAirAvailRQ, OTAAirAvailRS otaAirAvailRS,
			AvailableFlightDTO availableFlights, AvailableFlightSearchDTO availableFlightSearchDTO) throws WebservicesException,
			ModuleException {
		boolean flightsFound = false;
		boolean selectedInboundFlightsExists = false;
		boolean selectedOutboundFlightsExists = false;
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo();
		SelectedFlightDTO selectedFlightDTO = availableFlights.getSelectedFlight();
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AVAILABILITY_RESPONSE_START, userPrincipal,
				trackInfo, otaAirAvailRQ.getTransactionIdentifier());
		// FIXME set all the necessary params from the request
		otaAirAvailRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		otaAirAvailRS.setTransactionIdentifier(otaAirAvailRQ.getTransactionIdentifier());
		otaAirAvailRS.setSequenceNmbr(otaAirAvailRQ.getSequenceNmbr());
		otaAirAvailRS.setEchoToken(otaAirAvailRQ.getEchoToken());
		otaAirAvailRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		Collection<AvailableIBOBFlightSegment> outboundFlights = availableFlights.getAvailableOndFlights(OndSequence.OUT_BOUND);
		Collection<AvailableIBOBFlightSegment> inboundFlights = null;
		if (availableFlights.getAvailableOndFlights(OndSequence.IN_BOUND).size() > 0) {
			inboundFlights = availableFlights.getAvailableOndFlights(OndSequence.IN_BOUND);
		}

		// TODO charith need to pass this to bucket filtering
		boolean isFixedFareOnly = false;
		if (CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA).equals(
				Integer.toString(userPrincipal.getSalesChannel()))) {
			isFixedFareOnly = true;
		}

		if (selectedFlightDTO != null) {
			AvailableIBOBFlightSegment selectedOutboundFlight = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);
			if (selectedOutboundFlight != null && selectedOutboundFlight.getFlightSegmentDTOs().size() > 0) {
				selectedOutboundFlightsExists = true;
			}
		}

		if (selectedFlightDTO != null) {
			AvailableIBOBFlightSegment selectedInboundFlight = selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND);
			if (selectedInboundFlight != null && selectedInboundFlight.getFlightSegmentDTOs().size() > 0) {
				selectedInboundFlightsExists = true;
			}
		}

		for (AvailableIBOBFlightSegment availableFlightSegment : outboundFlights) {
			Collection<IAvailableFlightSegment> matchingInboundFlights = null;
			Iterator<IAvailableFlightSegment> matchingInboundFlightsIt = null;
			if (inboundFlights != null) {
				matchingInboundFlights = getMatchingInboundFlights(availableFlightSegment, inboundFlights);
				if (matchingInboundFlights != null) {
					matchingInboundFlightsIt = matchingInboundFlights.iterator();
				}
			}

			boolean hasMore = true;
			if ((!selectedInboundFlightsExists && matchingInboundFlights == null && selectedOutboundFlightsExists)
					|| (selectedInboundFlightsExists && matchingInboundFlights != null && matchingInboundFlightsIt.hasNext())) {
				while (hasMore) {
					flightsFound = true;
					OTAAirAvailRS.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRS.OriginDestinationInformation();
					otaAirAvailRS.getOriginDestinationInformation().add(originDestinationInformation);

					OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions();
					originDestinationInformation.setOriginDestinationOptions(originDestinationOptions);

					Collection<FlightSegmentDTO> flightSegmentDTOs = availableFlightSegment.getFlightSegmentDTOs();

					if (matchingInboundFlights != null) {
						Collection<FlightSegmentDTO> allFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
						allFlightSegmentDTOs.addAll(flightSegmentDTOs);
						allFlightSegmentDTOs.addAll(matchingInboundFlightsIt.next().getFlightSegmentDTOs());

						flightSegmentDTOs = allFlightSegmentDTOs;
					}

					Iterator<FlightSegmentDTO> flightSegmentDTOsIt = flightSegmentDTOs.iterator();
					for (int i = 0; flightSegmentDTOsIt.hasNext(); ++i) {// FIXME each segment is assumed to belong
																			// to seperate flights
						FlightSegmentDTO flightSegmentDTO = flightSegmentDTOsIt.next();

						String[] segmentCodeList = flightSegmentDTO.getSegmentCode().split("/");
						OriginDestinationInformationType.OriginLocation originLocation = new OriginDestinationInformationType.OriginLocation();
						originLocation.setLocationCode(segmentCodeList[0]);
						originLocation.setValue((String) globalConfig.getAirportInfo(originLocation.getLocationCode(), true)[0]);

						OriginDestinationInformationType.DestinationLocation destinationLocation = new OriginDestinationInformationType.DestinationLocation();
						destinationLocation.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
						destinationLocation.setValue((String) globalConfig.getAirportInfo(destinationLocation.getLocationCode(),
								true)[0]);

						TimeInstantType departureDateTime = new TimeInstantType();
						departureDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentDTO.getDepartureDateTime()));

						TimeInstantType arrivalDateTime = new TimeInstantType();
						arrivalDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentDTO.getArrivalDateTime()));

						if (i == 0) {
							originDestinationInformation.setOriginLocation(originLocation);
							originDestinationInformation.setDepartureDateTime(departureDateTime);
						}

						if (i == flightSegmentDTOs.size() - 1) {
							originDestinationInformation.setDestinationLocation(destinationLocation);
							originDestinationInformation.setArrivalDateTime(arrivalDateTime);
						}

						OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption();
						originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);

						OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment otaFlightSegment = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment();
						originDestinationOption.getFlightSegment().add(otaFlightSegment);

						otaFlightSegment.setRPH(flightSegmentDTO.getSegmentId().toString());
						if (segmentCodeList.length > 2) {
							otaFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
						}

						GregorianCalendar departureDateTimeCal = new GregorianCalendar();
						departureDateTimeCal.setTime(flightSegmentDTO.getDepartureDateTime());
						otaFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));
						GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
						arrivalDateTimeCal.setTime(flightSegmentDTO.getArrivalDateTime());
						otaFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

						otaFlightSegment.setJourneyDuration(CommonUtil.getDuration(flightSegmentDTO.getArrivalDateTimeZulu()
								.getTime() - flightSegmentDTO.getDepartureDateTimeZulu().getTime()));
						otaFlightSegment.setFlightNumber(flightSegmentDTO.getFlightNumber());

						FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
						departureAirport.setLocationCode(segmentCodeList[0]);
						departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(),
								true)[2]);

						FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
						arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
						arrivalAirport
								.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);

						otaFlightSegment.setDepartureAirport(departureAirport);
						otaFlightSegment.setArrivalAirport(arrivalAirport);
					}

					if (matchingInboundFlights != null && matchingInboundFlightsIt.hasNext()) {
						hasMore = true;
					} else {
						hasMore = false;
					}
				}
			}
		}

		if (flightsFound) {
			// one or more matching flight(s) found
			otaAirAvailRS.setSuccess(new SuccessType());
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No matching flight found");
		}

		if (selectedFlightDTO != null && selectedFlightDTO.getFareType() != FareTypes.NO_FARE) {
			AAAirAvailRSExt aaOTAAirAvailRSExtensions = new AAAirAvailRSExt();
			PricedItinerariesType pricedItineraries = new PricedItinerariesType();
			aaOTAAirAvailRSExtensions.setPricedItineraries(pricedItineraries);
			PricedItineraryType pricedItinerary = new PricedItineraryType();
			pricedItineraries.getPricedItinerary().add(pricedItinerary);
			pricedItinerary.setSequenceNumber(new BigInteger("1"));
			AirItineraryType airItinerary = new AirItineraryType();
			pricedItinerary.setAirItinerary(airItinerary);

			AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
			airItinerary.setOriginDestinationOptions(originDestinationOptions);

			String pricingKey = PriceQuoteUtil.generatePricingKey(new String[] {
					availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getOrigin(),
					availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getDestination() });

			PricedItineraryUtil.preparePricedItinerary(pricedItinerary, selectedFlightDTO, availableFlightSearchDTO, true,
					pricingKey, new FlexiFareSelectionCriteria());
			// store the total price in the transaction
			PriceQuoteUtil.storeTotalPrice(pricedItinerary);

			// store available OnD bundled fares in the transaction
			AvailabilitySearchUtil.setAvailableOndBundledFares(pricedItinerary);

			otaAirAvailRS.setAAAirAvailRSExt(aaOTAAirAvailRSExtensions);
		}

		if (log.isDebugEnabled()) {
			log.debug(getAvailabilityResultsSummary(otaAirAvailRS).toString());
		}
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AVAILABILITY_RESPONSE_END, userPrincipal,
				trackInfo, otaAirAvailRQ.getTransactionIdentifier());
	}

	public static Collection<IAvailableFlightSegment> getMatchingInboundFlights(
			AvailableIBOBFlightSegment availableFlightSegment, Collection<AvailableIBOBFlightSegment> inboundFlights) {
		Collection<IAvailableFlightSegment> matchingInboundFlights = null;
		Collection<FlightSegmentDTO> obFlightSegments = availableFlightSegment.getFlightSegmentDTOs();
		Iterator<FlightSegmentDTO> obFlightSegmentsIt = obFlightSegments.iterator();
		Calendar obArrivalDate = new GregorianCalendar();
		while (obFlightSegmentsIt.hasNext()) {// capture last segment arrival date
			obArrivalDate.setTime(obFlightSegmentsIt.next().getArrivalDateTime());
		}

		Date obArrivalDateWithTranTime = getDateWithTranTimeAdded(obArrivalDate).getTime();

		Date ibDepartureDate = null;

		Iterator<AvailableIBOBFlightSegment> ibFlightsIt = inboundFlights.iterator();
		while (ibFlightsIt.hasNext()) {
			AvailableIBOBFlightSegment availFlightSegment = ibFlightsIt.next();

			ibDepartureDate = (availFlightSegment.getFlightSegmentDTOs().iterator().next()).getDepartureDateTime();
			if (ibDepartureDate.after(obArrivalDateWithTranTime)) {
				if (matchingInboundFlights == null) {
					matchingInboundFlights = new LinkedList<IAvailableFlightSegment>();
				}
				matchingInboundFlights.add(availFlightSegment);
			}

		}
		return matchingInboundFlights;
	}

	/**
	 * Get date with Return Transition Duration added
	 * 
	 * @param date
	 * @return
	 */
	private static Calendar getDateWithTranTimeAdded(Calendar date) {
		String minRetTransitionTimeStr = WebServicesModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
		String hours = minRetTransitionTimeStr.substring(0, minRetTransitionTimeStr.indexOf(":"));
		String mins = minRetTransitionTimeStr.substring(minRetTransitionTimeStr.indexOf(":") + 1);

		Calendar dateWithTranTime = new GregorianCalendar();
		dateWithTranTime.setTime(date.getTime());
		dateWithTranTime.add(Calendar.HOUR, Integer.parseInt(hours));
		dateWithTranTime.add(Calendar.MINUTE, Integer.parseInt(mins));

		return dateWithTranTime;
	}

	public static StringBuffer getAvailabilityResultsSummary(OTAAirAvailRS otaAirAvailRS) throws WebservicesException,
			ModuleException {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r \t";
		summary.append("Number of search results found = " + otaAirAvailRS.getOriginDestinationInformation().size());
		for (OriginDestinationInformation originDestinationInformationRS : otaAirAvailRS.getOriginDestinationInformation()) {
			summary.append(nl + "Flight Details [" + "Origin= "
					+ originDestinationInformationRS.getOriginLocation().getLocationCode() + ",Destination= "
					+ originDestinationInformationRS.getDestinationLocation().getLocationCode() + ",DepDateTime= "
					+ originDestinationInformationRS.getDepartureDateTime().getValue() + ",ArrDateTime="
					+ originDestinationInformationRS.getArrivalDateTime().getValue() + "]");
			for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption
						.getFlightSegment()) {
					summary.append(nl + "Segment Info [fltNumber=" + flightSegment.getFlightNumber() + ",Origin="
							+ flightSegment.getDepartureAirport().getLocationCode() + ",Destination="
							+ flightSegment.getArrivalAirport().getLocationCode() + ",DepDateTime="
							+ CommonUtil.getFormattedDate(flightSegment.getDepartureDateTime().toGregorianCalendar().getTime())
							+ ",ArrDateTime="
							+ CommonUtil.getFormattedDate(flightSegment.getArrivalDateTime().toGregorianCalendar().getTime())
							+ "]");
				}
			}
		}
		return summary;
	}

	/**
	 * Extracts pnrSegIds for cancellation/modification from the request.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	private static List<Integer> extractPNRSegIds(Reservation reservation, AirItineraryType.OriginDestinationOptions onds)
			throws WebservicesException {

		if (onds.getOriginDestinationOption() == null || onds.getOriginDestinationOption().size() != 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Only one OND at a time allowed to cancel/modify");
		}

		OriginDestinationOptionType ond = onds.getOriginDestinationOption().get(0);
		int ondGroupCode = -1;
		List<Integer> pnrSegIdsForCnx = new ArrayList<Integer>();
		for (Object element : ond.getFlightSegment()) {
			BookFlightSegmentType wsFlightSeg = (BookFlightSegmentType) element;
			for (Object element2 : reservation.getSegments()) {
				ReservationSegment resSegment = (ReservationSegment) element2;
				if (resSegment.getPnrSegId() == Integer.parseInt(wsFlightSeg.getRPH())) {
					pnrSegIdsForCnx.add(Integer.parseInt(wsFlightSeg.getRPH()));
					if (ondGroupCode == -1) {
						ondGroupCode = resSegment.getOndGroupId();
					} else if (ondGroupCode != resSegment.getOndGroupId()) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Only one OND at a time allowed to cancel");
					}
					break;
				}
			}
		}
		return pnrSegIdsForCnx;
	}

	/**
	 * Method to get the reservation.
	 * 
	 * @param pnrModesDTO
	 * @param trackingInfoDTO
	 * @return
	 * @throws Exception
	 */
	private Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackingInfoDTO) throws ModuleException,
			WebservicesException {
		Reservation reservation = null;
		try {
			reservation = WPModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackingInfoDTO);
			if (reservation == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (ModuleException ex) {
			throw ex;
		}
		return reservation;
	}

	public AADynBestOffersRS getDynamicBestOffers(AADynBestOffersRQ aaDynBestOffersRQ) throws ModuleException,
			WebservicesException {
		AADynBestOffersRS aaDynBestOffersRS = new AADynBestOffersRS();
		com.isaaviation.thinair.webservices.api.dynbestoffers.AAStatusErrorsAndWarnings aaStatusErrorsAndWarnings = new com.isaaviation.thinair.webservices.api.dynbestoffers.AAStatusErrorsAndWarnings();
		aaDynBestOffersRS.setStatusErrorsAndWarnings(aaStatusErrorsAndWarnings);

		BestOffersSearchDTO bestOffersSearchDTO = extractBestOffersSearchReqParams(aaDynBestOffersRQ);
		if (bestOffersSearchDTO != null) {
			log.info("Best offers called Origin : " + bestOffersSearchDTO.getOriginAirport() + " Destination : "
					+ bestOffersSearchDTO.getDestinationAirport() + " Month : " + bestOffersSearchDTO.getMonthOfTravel()
					+ " Country code : " + bestOffersSearchDTO.getCountryCode() + " Summary Flag : "
					+ bestOffersSearchDTO.isSummaryFlag() + " Total Fare quote :" + bestOffersSearchDTO.isTotalQuoteFlag());
			Collection colBestOffers = WebServicesModuleUtils.getFlightInventoryResBD().getDynamicBestOffers(bestOffersSearchDTO);
			prepareBestOffersSearchResponse(aaDynBestOffersRQ, aaDynBestOffersRS, colBestOffers, bestOffersSearchDTO);
		}
		return aaDynBestOffersRS;
	}

	public static void setAvailableOndBundledFares(PricedItineraryType pricedItinerary) {
		ThreadLocalData.setCurrentTnxParam(Transaction.AVAILABLE_OND_BUNDLED_FARES, pricedItinerary.getAirItinerary()
				.getOriginDestinationOptions().getAABundledServiceExt());
	}

	private BestOffersSearchDTO extractBestOffersSearchReqParams(AADynBestOffersRQ aaDynBestOffersRQ) throws ModuleException,
			WebservicesException {
		BestOffersSearchDTO bestOffersSearchDTO = new BestOffersSearchDTO();
		bestOffersSearchDTO.setCountryCode(aaDynBestOffersRQ.getCountryOfOrigin());
		bestOffersSearchDTO.setSummaryFlag(aaDynBestOffersRQ.isForSummaryResults());
		bestOffersSearchDTO.setTotalQuoteFlag(aaDynBestOffersRQ.isIncludeTaxSurcharge());
		bestOffersSearchDTO.setOriginAirport("".equals(aaDynBestOffersRQ.getOriginAirportCode()) ? null : aaDynBestOffersRQ
				.getOriginAirportCode());
		bestOffersSearchDTO.setDestinationAirport("".equals(aaDynBestOffersRQ.getDestinationAirportCode())
				? null
				: aaDynBestOffersRQ.getDestinationAirportCode());
		if (aaDynBestOffersRQ.getMonthOfTravel() != null && !"".equals(aaDynBestOffersRQ.getMonthOfTravel())) {
			bestOffersSearchDTO.setMonthOfTravel(Integer.valueOf(aaDynBestOffersRQ.getMonthOfTravel()));
		}
		
		bestOffersSearchDTO.setChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB+"");
		return bestOffersSearchDTO;
	}

	private void prepareBestOffersSearchResponse(AADynBestOffersRQ aaDynBestOffersRQ, AADynBestOffersRS aaDynBestOffersRS,
			Collection colBestOffers, BestOffersSearchDTO bestOffersSearchDTO) throws WebservicesException, ModuleException {
		if (colBestOffers != null && !colBestOffers.isEmpty()) {
			Iterator itBestOffers = colBestOffers.iterator();

			String origin = null;
			String destination = null;
			String selectedCurrency = WebServicesModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);

			LinkedHashMap hmBestOffers = new LinkedHashMap();
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			// Jira 5397 - Enable currency support
			if (aaDynBestOffersRQ.getSelectedCurrency() != null) {
				if (OTAUtils.isPreferredCurrencyCodeDefined(aaDynBestOffersRQ.getSelectedCurrency())) {
					selectedCurrency = aaDynBestOffersRQ.getSelectedCurrency();
				}
			}

			while (itBestOffers.hasNext()) {
				BestOffersDTO bestOffersDTO = (BestOffersDTO) itBestOffers.next();
				try {
					if (bestOffersDTO != null) {

						origin = bestOffersDTO.getOriginAirport();
						destination = bestOffersDTO.getDestinationAirport();
						String uniqueKey = origin + "/" + destination;

						if (!hmBestOffers.containsKey(uniqueKey)) {
							AAOnDDynBestOffersType aaOnDDynBestOffersType = new AAOnDDynBestOffersType();
							aaDynBestOffersRS.getOnDDynBestOffer().add(aaOnDDynBestOffersType);
							aaOnDDynBestOffersType.setOriginAirportCode(origin);
							aaOnDDynBestOffersType.setDestinationAirportCode(destination);
							aaOnDDynBestOffersType.setOriginAirportName(bestOffersDTO.getOriginAirportName());
							aaOnDDynBestOffersType.setDestinationAirportName(bestOffersDTO.getDestinationAirportName());
							hmBestOffers.put(uniqueKey, aaOnDDynBestOffersType);
						}
						AAOnDDynBestOffersType aaOnDDynBestOffersType = (AAOnDDynBestOffersType) hmBestOffers.get(uniqueKey);
						AAOnDDynBestOfferType aaOnDDynBestOffer = new AAOnDDynBestOfferType();
						aaOnDDynBestOffersType.getDynBestOffer().add(aaOnDDynBestOffer);

						aaOnDDynBestOffer.setCurrencyCode(selectedCurrency);
						if (selectedCurrency.equalsIgnoreCase(WebServicesModuleUtils.getGlobalConfig().getBizParam(
								SystemParamKeys.BASE_CURRENCY))) {
							aaOnDDynBestOffer.setFare(bestOffersDTO.getFareAmount());
							if (aaDynBestOffersRQ.isIncludeTaxSurcharge()) {
								aaOnDDynBestOffer.setSurcharge(bestOffersDTO.getTotalSurchargesAmount());
								aaOnDDynBestOffer.setTax(bestOffersDTO.getTotalTaxAmount());
								aaOnDDynBestOffer.setTotal(bestOffersDTO.getTotalAmount());
							}
						} else {// Convert to selected currency
							aaOnDDynBestOffer.setFare(WSReservationUtil.getAmountInSpecifiedCurrency(
									bestOffersDTO.getFareAmount(), selectedCurrency, exchangeRateProxy));
							if (aaDynBestOffersRQ.isIncludeTaxSurcharge()) {
								aaOnDDynBestOffer.setSurcharge(WSReservationUtil.getAmountInSpecifiedCurrency(
										bestOffersDTO.getTotalSurchargesAmount(), selectedCurrency, exchangeRateProxy));
								aaOnDDynBestOffer.setTax(WSReservationUtil.getAmountInSpecifiedCurrency(
										bestOffersDTO.getTotalTaxAmount(), selectedCurrency, exchangeRateProxy));
								aaOnDDynBestOffer.setTotal(WSReservationUtil.getAmountInSpecifiedCurrency(
										bestOffersDTO.getTotalAmount(), selectedCurrency, exchangeRateProxy));
							}
						}
						aaOnDDynBestOffer.setRank(bestOffersDTO.getRank());
						aaOnDDynBestOffer.setDepartureDateTimeLocal(CommonUtil.parse(bestOffersDTO.getDepartureDateTimeLocal()));

						String flightNos = StringUtils.join(bestOffersDTO.getFlightNos().iterator(), ",");

						aaOnDDynBestOffer.setFlightNumbers(flightNos);
					}

				} catch (Exception e) {
					aaDynBestOffersRS.getStatusErrorsAndWarnings().setSuccess(false);
				}
				aaDynBestOffersRS.getStatusErrorsAndWarnings().setSuccess(true);
			}
		}
	}

	private void addOndLog(OriginDestinationInformationType originDestinationInformation, UserPrincipal principal) {
		StringBuilder logs = new StringBuilder();
		logs.append("USER ID :" + principal.getUserId());
		logs.append(" ONDS : " + originDestinationInformation.getOriginLocation().getLocationCode() + "/"
				+ originDestinationInformation.getDestinationLocation().getLocationCode());
		log.info(logs.toString());
	}
}
