package com.isa.thinair.webservices.core.handler.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Mehdi Raza
 * 
 */
public class PasswordHandler implements CallbackHandler {

	private final Log log = LogFactory.getLog(getClass());

	public PasswordHandler() {
	}

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
	}
}
