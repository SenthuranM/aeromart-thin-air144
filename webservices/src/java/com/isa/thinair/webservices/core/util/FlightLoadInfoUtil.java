package com.isa.thinair.webservices.core.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airreservation.api.dto.flightLoad.CabinInventoryDTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightLoadInfoDTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightSegmentInventoryDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isaaviation.thinair.webservices.api.airschedules.AACabinInventoryType;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfo;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AASegmentInventoryType;

public class FlightLoadInfoUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(FlightLoadInfoUtil.class);

	public static AAFlightLoadInfoRS getFlightLoadInfo(AAFlightLoadInfoRQ flightLoadInfoRQ) throws Exception {

		final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

		AAFlightLoadInfoRS LoadInfoRS = new AAFlightLoadInfoRS();

		Date currentDate = CalendarUtil.getStartTimeOfDate(new Date());

		Date departureDateFromZulu = flightLoadInfoRQ.getDepartureDateFrom().toGregorianCalendar().getTime();
		Date departureDateToZulu = flightLoadInfoRQ.getDepartureDateTo().toGregorianCalendar().getTime();
		
		if (currentDate.after(departureDateFromZulu)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_DURATION_PERIOD_OR_DATES_INCORRECT_,
					" Param departureDateFrom should be any future date. ");
		}
		if (departureDateFromZulu.after(departureDateToZulu)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_DURATION_PERIOD_OR_DATES_INCORRECT_,
					"Invalied date for the param departureDateToZulu");
		}
		if(CalendarUtil.getTimeDifferenceInDays(departureDateFromZulu, departureDateToZulu)  > 31  ){
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_DURATION_PERIOD_OR_DATES_INCORRECT_,
					"Date range should be less than 31 days");
		}
		Collection<FlightLoadInfoDTO> records = ReservationModuleUtils.getReservationAuxilliaryBD().getFlightLoadInfo(
				departureDateFromZulu, departureDateToZulu);

		LoadInfoRS.setChannelId(flightLoadInfoRQ.getChannelId());
		LoadInfoRS.setEchoToken(flightLoadInfoRQ.getEchoToken());
		LoadInfoRS.setUserId(flightLoadInfoRQ.getUserId());
		LoadInfoRS.setTimeStamp(CalendarUtil.asXMLGregorianCalendar(new Date()));

		for (FlightLoadInfoDTO flightLoadInfoDTO : records) {

			AAFlightLoadInfo aAFlightLoadInfo = new AAFlightLoadInfo();

			aAFlightLoadInfo.setAircraftModel(flightLoadInfoDTO.getAircraftModel());
			aAFlightLoadInfo.setDepartureDate(CalendarUtil.asXMLGregorianCalendar(flightLoadInfoDTO.getDepartureTimeZulu()));
			aAFlightLoadInfo.setFlightNumber(flightLoadInfoDTO.getFlightNumber());

			for (CabinInventoryDTO cabinInventoryDTO : flightLoadInfoDTO.getCabinInventoryMap().values()) {
				AACabinInventoryType aACabinInventoryType = new AACabinInventoryType();
				aACabinInventoryType.setCabinClass(cabinInventoryDTO.getCabinClassCode());
				for (FlightSegmentInventoryDTO flightSegmentInventoryDTO : cabinInventoryDTO.getFlightSegmentInventoryList()) {
					
					AASegmentInventoryType aASegmentInventory = new AASegmentInventoryType();
					
					aASegmentInventory.setSegmentCode(flightSegmentInventoryDTO.getSegmentCode());
					aASegmentInventory.setAdultCount(flightSegmentInventoryDTO.getAdultCount());
					aASegmentInventory.setChildCount(flightSegmentInventoryDTO.getChildCount());
					aASegmentInventory.setInfantCount(flightSegmentInventoryDTO.getInfantCount());
					aASegmentInventory.setTotalBoookings(flightSegmentInventoryDTO.getTotalBookings());

					aACabinInventoryType.getSegmentInventory().add(aASegmentInventory);
				}

				aAFlightLoadInfo.getCabinInventory().add(aACabinInventoryType);
			}

			LoadInfoRS.getFlightLoadInfo().add(aAFlightLoadInfo);
		}

		return LoadInfoRS;
	}

}
