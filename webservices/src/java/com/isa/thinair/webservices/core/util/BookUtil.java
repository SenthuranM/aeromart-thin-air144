package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.BookInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.TransformUtil.BookingRequest;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AABookingCategoryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * Create booking process.
 * 
 * @author Nasly
 */
public class BookUtil extends BaseUtil {
	public OTAAirBookRS book(OTAAirBookRQ otaAirBookRQ, AAAirBookRQExt aaAirBookRQExt) throws ModuleException,
			WebservicesException {
		OTAAirBookRS airBookRS = new OTAAirBookRS();
		airBookRS.setErrors(new ErrorsType());

		Object[] transformedBookRQInfo = null;
		IReservation iReservation = null;
		Collection collBlockIDs = null;
		ServiceResponce serviceResponse = null;
		boolean splitReservation = false;
		boolean onholdReservation = false;

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo();
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.BOOK_REQUEST, userPrincipal, trackInfo,
				otaAirBookRQ.getTransactionIdentifier());

		AABookingCategoryType bookingCategoryType = aaAirBookRQExt.getBookingCategoryType();

		String bookingCategoryCode;
		if (bookingCategoryType != null) {
			bookingCategoryCode = aaAirBookRQExt.getBookingCategoryType().getTypeCode();
		} else {
			bookingCategoryCode = BookingCategory.STANDARD.getCode();
			bookingCategoryType = new AABookingCategoryType();
			bookingCategoryType.setTypeCode(bookingCategoryCode);
			aaAirBookRQExt.setBookingCategoryType(bookingCategoryType);

		}

		WSReservationUtil.authorizeBookingCategory(privilegeKeys, bookingCategoryCode, WSConstants.ALLOW_MAKE_RESERVATION);

		transformedBookRQInfo = new BookingRequest(otaAirBookRQ, aaAirBookRQExt).transform(false);

		iReservation = (IReservation) transformedBookRQInfo[0];
		onholdReservation = (Boolean) transformedBookRQInfo[2];
		splitReservation = (Boolean) transformedBookRQInfo[3];

		if (!onholdReservation) {
			for (ReservationPax pax : ((ReservationAssembler) iReservation).getDummyReservation().getPassengers()) {
				Collection<Integer> pnrPaxIdCol = new ArrayList<Integer>();
				pnrPaxIdCol.add(pax.getPnrPaxId());

				WSReservationUtil.authorizePayment(privilegeKeys, pax.getPayment(), userPrincipal.getAgentCode(), pnrPaxIdCol);
			}
		}

		ReservationBD reservationDelegate = WebServicesModuleUtils.getReservationBD();

		Collection<OndFareDTO> ONDFareDTOCollection = (Collection<OndFareDTO>) transformedBookRQInfo[5];

		if (onholdReservation
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
			if (ONDFareDTOCollection != null) {
				if (!WSReservationUtil.isHoldAllowed(ONDFareDTOCollection)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Hold not allowed. Need to be ticketed immediately");
				}
			}
		}

		iReservation.addOndFareDTOs(ONDFareDTOCollection);

		extractExtSegsAndAmmendForReservation(iReservation, otaAirBookRQ);

		Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(ONDFareDTOCollection, 1, 1, 1);

		Collection outBoundSegments = (Collection) inOutboundSegments[0];
		Collection inBoundSegments = (Collection) inOutboundSegments[1];

		Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
		Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();

		try {
			AddModifyGroundSegmentUtil.mapStationsBySegment(otaAirBookRQ.getAirItinerary().getOriginDestinationOptions()
					.getOriginDestinationOption(), groundStationBySegmentMap, groundSementFlightByAirFlightMap);
		} catch (NumberFormatException e) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Flight Reference Number format is invalid");
		}

		if ((groundStationBySegmentMap != null && groundStationBySegmentMap.size() > 0)
				&& (groundSementFlightByAirFlightMap == null || groundSementFlightByAirFlightMap.size() == 0)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_SEGMENT_DATA_IN_ITINERARY,
					"Ground segment must be connect to a Flight segment");
		}

		for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
			ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();
			iReservation.addOutgoingSegment(segmentInfo.getSegmentSeq(), segmentInfo.getFlightSegId(),
					segmentInfo.getOndGroupId(), segmentInfo.getReturnOndGroupId(), null, segmentInfo.getSelectedBundledFarePeriodId(),
					null, null);
			iReservation.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
		}

		if (inBoundSegments != null) {
			for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
				ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
				iReservation.addReturnSegment(segmentInfo.getSegmentSeq(), segmentInfo.getFlightSegId(),
						segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(), null,
						segmentInfo.getSelectedBundledFarePeriodId(), null, null);
				iReservation.addGroundStationDataToPNRSegment(groundStationBySegmentMap, groundSementFlightByAirFlightMap);
			}
		}
		collBlockIDs = reservationDelegate.blockSeats(ONDFareDTOCollection, null);

		String pnr = null;
		String splitOnHoldPnr = null;
		ReservationContactInfo reservationContactInfo = (ReservationContactInfo) transformedBookRQInfo[6];
		if (onholdReservation) {
			serviceResponse = reservationDelegate.createOnHoldReservation(iReservation, collBlockIDs, false, trackInfo, true);
			pnr = (String) serviceResponse.getResponseParam(CommandParamNames.PNR);
			// Email Itinerary for OHD Bookings
			if (AuthorizationUtil.hasPrivileges(privilegeKeys, PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_OHDBOOKING)) {

				LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(pnr, false, null, false, null, null, null);
				CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
				contactInfo.setEmail(reservationContactInfo.getEmail());
				contactInfo.setPreferredLanguage(reservationContactInfo.getPreferredLanguage());
				BookInterlineUtil.sendEmailItenary(pnrModesDTO, contactInfo, userPrincipal, false);

			}
		} else if (!splitReservation) {
			boolean isFraudEnabled = AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT);
			if (isFraudEnabled) {
				isFraudEnabled = AppSysParamsUtil.isFraudEnabledAgent(userPrincipal.getAgentCode());
			}

			trackInfo.setPaymentAgent(userPrincipal.getAgentCode());
			serviceResponse = reservationDelegate
					.createCCReservation(iReservation, collBlockIDs, trackInfo, isFraudEnabled, true);
			// FIXME put the flag in CCpayment info
			pnr = (String) serviceResponse.getResponseParam(CommandParamNames.PNR);

			// Email Itinerary for CNF Bookings
			if (AuthorizationUtil.hasPrivileges(privilegeKeys, PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_CNFBOOKING)) {
				LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(pnr, false, null, false, null, null, null);
				CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
				contactInfo.setEmail(reservationContactInfo.getEmail());
				contactInfo.setPreferredLanguage(reservationContactInfo.getPreferredLanguage());
				BookInterlineUtil.sendEmailItenary(pnrModesDTO, contactInfo, userPrincipal, false);
			}
		} else {
			serviceResponse = reservationDelegate.createSplitReservation(iReservation, collBlockIDs, trackInfo);
			pnr = (String) serviceResponse.getResponseParam(CommandParamNames.CONFIRM_PNR);
			splitOnHoldPnr = (String) serviceResponse.getResponseParam(CommandParamNames.ON_HOLD_PNR);
		}

		// Remove total price store in the transaction.
		PriceQuoteUtil.clearTotalPrice();

		OTAReadRQ readRQ = new OTAReadRQ();
		readRQ.setPOS(new POSType());
		readRQ.getPOS().getSource().add(otaAirBookRQ.getPOS().getSource().get(0));

		readRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		readRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		readRQ.setSequenceNmbr(otaAirBookRQ.getSequenceNmbr());
		readRQ.setEchoToken(otaAirBookRQ.getEchoToken());
		readRQ.setTimeStamp(CommonUtil.parse(new Date()));
		readRQ.setTransactionIdentifier(otaAirBookRQ.getTransactionIdentifier());

		AALoadDataOptionsType loadDataOptions = new AALoadDataOptionsType();
		loadDataOptions.setLoadAirItinery(true);
		loadDataOptions.setLoadPriceInfoTotals(true);
		loadDataOptions.setLoadPTCPriceInfo(true);
		loadDataOptions.setLoadFullFilment(true);
		loadDataOptions.setLoadTravelerInfo(true);

		ReadRequest resReadRQ = null;
		readRQ.setReadRequests(new ReadRequests());
		readRQ.getReadRequests().getReadRequest().add(resReadRQ = new ReadRequest());

		resReadRQ.setUniqueID(new UniqueIDType());
		resReadRQ.getUniqueID().setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		resReadRQ.getUniqueID().setID(pnr);

		TransformUtil.ReadRequest readRequest = new TransformUtil.ReadRequest(readRQ, loadDataOptions,
				TransformUtil.ReadRequest.TransformTO.AirBookRS);

		Object[] transformedData = readRequest.transform(true);

		PriceQuoteUtil.clearPriceQuoteRelatedTransactionParams();
		
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.BOOK_RESPONSE, userPrincipal, trackInfo,
				otaAirBookRQ.getTransactionIdentifier());

		return (OTAAirBookRS) transformedData[0];
	}

	/**
	 * Extract the external segments and ammend to the reservation
	 * 
	 * @param iReservation
	 * @param otaAirBookRQ
	 * @return
	 * @throws ModuleException
	 */
	private void extractExtSegsAndAmmendForReservation(IReservation iReservation, OTAAirBookRQ otaAirBookRQ)
			throws ModuleException {
		SourceType pos = otaAirBookRQ.getPOS().getSource().get(0);
		String BCT = pos.getBookingChannel().getType();

		if ((BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA))
				&& otaAirBookRQ.getAirItinerary() != null && otaAirBookRQ.getAirItinerary().getOriginDestinationOptions() != null
				&& otaAirBookRQ.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption() != null && otaAirBookRQ
				.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption().size() > 0)) {

			List lstResExtSegment = new ArrayList();
			ExternalPnrSegment reservationExternalSegment;

			for (Object element : otaAirBookRQ.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()) {
				OriginDestinationOptionType originDestinationOptionType = (OriginDestinationOptionType) element;

				for (Object element2 : originDestinationOptionType.getFlightSegment()) {
					BookFlightSegmentType bookFlightSegmentType = (BookFlightSegmentType) element2;

					reservationExternalSegment = new ExternalPnrSegment();

					ExternalFlightSegment externalFlightSegment = new ExternalFlightSegment();
					externalFlightSegment.setFlightNumber(bookFlightSegmentType.getFlightNumber());
					externalFlightSegment.setEstTimeDepatureLocal(new Date(CommonUtil.getTime(bookFlightSegmentType
							.getDepartureDateTime())));
					externalFlightSegment.setEstTimeArrivalLocal(new Date(CommonUtil.getTime(bookFlightSegmentType
							.getArrivalDateTime())));
					externalFlightSegment.setSegmentCode(bookFlightSegmentType.getDepartureAirport().getLocationCode() + "/"
							+ bookFlightSegmentType.getArrivalAirport().getLocationCode());

					reservationExternalSegment.setExternalFlightSegment(externalFlightSegment);
					reservationExternalSegment.setStatus(BeanUtils.nullHandler(bookFlightSegmentType.getStatus()));

					// FIXME to be set from travel preferences; only Economy cabin class is supported
					reservationExternalSegment.setCabinClassCode("Y");

					lstResExtSegment.add(reservationExternalSegment);
				}
			}

			// Sorts the flight segments by earliest departure date
			Collections.sort(lstResExtSegment);

			int i = 1;
			StringBuilder ioInfo = new StringBuilder();
			for (Iterator itr = lstResExtSegment.iterator(); itr.hasNext();) {
				reservationExternalSegment = (ExternalPnrSegment) itr.next();

				if (!AppSysParamsUtil.getDefaultCarrierCode().equals(
						AppSysParamsUtil.extractCarrierCode(reservationExternalSegment.getExternalFlightSegment()
								.getFlightNumber()))) {

					if (ioInfo.length() == 0) {
						ioInfo.append(reservationExternalSegment.toString());
					} else {
						ioInfo.append(", <br> " + reservationExternalSegment.toString());
					}

					ExternalFlightSegment externalFlightSegment = reservationExternalSegment.getExternalFlightSegment();
					String externalFlightNumber = externalFlightSegment.getFlightNumber();
					String externalCarrierCode = AppSysParamsUtil.extractCarrierCode(externalFlightNumber);
					iReservation.addExternalPnrSegment(i, externalCarrierCode, externalFlightNumber,
							externalFlightSegment.getSegmentCode(), reservationExternalSegment.getCabinClassCode(),
							externalFlightSegment.getEstTimeDepatureLocal(), externalFlightSegment.getEstTimeArrivalLocal());
					i++;
				}
			}

			if (ioInfo.length() > 0) {
				iReservation.ammendUserNotes("<br> <br> <br> [ Inbound/Outbound Info {" + ioInfo.toString() + "} ] ");
			}
		}
	}

	/**
	 * @param currency
	 *            - Selected currency
	 * @return The total amount of the external charges in the current transaction context.
	 */
	public static BigDecimal getTotalAmountOfSelectedExternalCharges(String currency, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		if (quotedExternalCharges != null) {
			for (List<ExternalChgDTO> externalChgDTOs : quotedExternalCharges.values()) {
				for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
					total = AccelAeroCalculator.add(total, WSReservationUtil.getAmountInSpecifiedCurrency(
							externalChgDTO.getAmount(), currency, exchangeRateProxy));
				}
			}
		}

		return total;
	}

	/**
	 * Adds the selected external charges in the current transaction context to the given payment assembler of the given
	 * traveler ref number.
	 * 
	 * @param iPayment
	 *            : The payment assembler.
	 * @param travelerRefNo
	 *            : The traveler reference number.
	 */
	public static void addSelectedExternalCharges(IPayment iPayment, String travelerRefNo) {
		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		if (quotedExternalCharges != null) {
			for (String travelerRef : quotedExternalCharges.keySet()) {
				if (travelerRef.equals(travelerRefNo)) {
					List<ExternalChgDTO> externalChgDTOs = quotedExternalCharges.get(travelerRef);
					if (externalChgDTOs != null && externalChgDTOs.size() > 0) {
						iPayment.addExternalCharges(externalChgDTOs);
					}
				}
			}
		}
	}

	/**
	 * Retrieve paxwise amount map of quoted external charges of the current transaction
	 * 
	 * @return
	 */
	public static Map<String, BigDecimal> getQuotedExternalChargesAmountMap() {
		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		Map<String, BigDecimal> externalChargesAmountsMap = new HashMap<String, BigDecimal>();

		if (quotedExternalCharges != null) {
			for (String travelerRef : quotedExternalCharges.keySet()) {
				BigDecimal perPaxQuotedExternalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
				List<ExternalChgDTO> externalChgDTOs = quotedExternalCharges.get(travelerRef);
				if (externalChgDTOs != null && externalChgDTOs.size() > 0) {
					for (ExternalChgDTO extCharge : externalChgDTOs) {
						perPaxQuotedExternalCharge = AccelAeroCalculator.add(perPaxQuotedExternalCharge, extCharge.getAmount());
					}
				}

				externalChargesAmountsMap.put(travelerRef, perPaxQuotedExternalCharge);
			}
		}
		return externalChargesAmountsMap;
	}
}
