package com.isa.thinair.webservices.core.bl;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.AMATLAGetFareRulesRQ;
import com.amadeus.ama._2008._03.AMATLAGetFareRulesRS;
import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.SuccessType;
import com.amadeus.ama._2008._03.TLAErrorType;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusFareRuleBL {

	private final static Log log = LogFactory.getLog(AmadeusFareRuleBL.class);

	public static AMATLAGetFareRulesRS execute(AMATLAGetFareRulesRQ amaTLAGetFareRulesRQ) {
		AMATLAGetFareRulesRS fareRS = new AMATLAGetFareRulesRS();
		fareRS.setRelease(WebservicesConstants.AmadeusConstants.ReleaseFareRuleRS);
		fareRS.setVersion(WebservicesConstants.AmadeusConstants.VersionFareRuleRS);
		try {
			Map<String, String> compatMap = AppSysParamsUtil.getAmadeusCompatibilityMap();
			if (compatMap.get(AmadeusConstants.FARE_RULE).equals(AmadeusConstants.YES)) {
				directFareRuleFetch(fareRS, amaTLAGetFareRulesRQ);
			} else {
				mappedFareRuleFetch(fareRS, amaTLAGetFareRulesRQ);
			}
			fareRS.setSuccess(new SuccessType());
		} catch (ModuleException e) {
			log.error("Error occured", e);
			TLAErrorType error = new TLAErrorType();
			if (e instanceof AmadeusWSException) {
				AmadeusWSException ae = (AmadeusWSException) e;
				error.setCode(ae.getErrorCode());
				error.setType(ae.getErrorWarningType());
				// TODO fetch the descriptive message from resource bundle
				if (ae.getDescription() != null && !"".equals(ae.getDescription())) {
					error.setValue(ae.getDescription());
				}
			} else {
				error.setCode(Integer.valueOf(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_RETRY.getErrorCode()));
				error.setValue(e.getExceptionCode());
				error.setType(ErrorWarningType.UNKNOWN);
			}
			fareRS.setError(error);
		}

		return fareRS;
	}

	public static void directFareRuleFetch(AMATLAGetFareRulesRS fareRS, AMATLAGetFareRulesRQ amaTLAGetFareRulesRQ)
			throws ModuleException {
		// singleChar booking class validation
		Map<Integer, Map<String, String>> gdsBookingClassMap = WebServicesModuleUtils.getGlobalConfig()
				.getGdsBookingClassMap();
		String singleCharBC = AmadeusCommonUtil.getBookingClassFromAmadeusFBC(amaTLAGetFareRulesRQ.getFareCode());
		boolean bcExists = false;
		if(!gdsBookingClassMap.get(AmadeusConstants.GDS_ID).isEmpty()){
			for(Entry<String, String> entry : gdsBookingClassMap.get(AmadeusConstants.GDS_ID).entrySet()) {
				if (entry.getValue().equals(singleCharBC)) {
					bcExists = true;
					break;
				}
			}
		}
		
		if (!bcExists) {
			throw new AmadeusWSException(AmadeusErrorCodes.FARE_RULE_DOES_NOT_EXISTS, ErrorWarningType.BIZ_RULE,
					"Fare rule is not supported");
		}

		Collection<FareRule> fareRules = WebServicesModuleUtils.getFareBD().getFareRules(
				AmadeusCommonUtil.getFareBasisFromAmadeusFBC(amaTLAGetFareRulesRQ.getFareCode()));
		if (fareRules == null || fareRules.size() != 1) {
			throw new AmadeusWSException(AmadeusErrorCodes.FARE_RULE_DOES_NOT_EXISTS, ErrorWarningType.BIZ_RULE,
					"Fare rule is not supported");
		} else {
			FareRule fr = fareRules.iterator().next();
			if (fr.getRulesComments() != null) {
				fareRS.setFareRule(fr.getRulesComments());
			} else {
				fareRS.setFareRule("");
			}
		}
	}

	public static void mappedFareRuleFetch(AMATLAGetFareRulesRS fareRS, AMATLAGetFareRulesRQ amaTLAGetFareRulesRQ) {
		fareRS.setFareRule("Please refer airline webiste for general terms & conditions"); // TODO do a mapping to xml
																							// config or db
	}
}
