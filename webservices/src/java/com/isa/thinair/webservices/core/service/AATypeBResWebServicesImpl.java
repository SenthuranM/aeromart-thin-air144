package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import com.isa.thinair.webservices.api.service.AATypeBResWebServices;

@WebService(name = "AATypeBResWebServices", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AATypeBResWebServices")
public class AATypeBResWebServicesImpl extends BaseWebServicesImpl implements AATypeBResWebServices {
}
