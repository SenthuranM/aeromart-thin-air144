/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.transaction;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.util.UID;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Abstracts a webservices transaction. Holds transactional data during transaction session.
 * 
 * @author Mehdi Raza
 * @auther Nasly
 */
public class Transaction implements ITransaction {

	private Log log = LogFactory.getLog(getClass());

	/** Transaction data keys */
	public static final String QUOTED_SEGMENTS_FARES_SEATS_MAP = "segsFaresSeatsColMap";
	public static final String AVAIlABLE_FLIGHT_CRITERIA = "availableFlightSearchDTO";
	public static final String RES_SERVICE_CHARGE = "serviceChargeDTO";
	public static final String RES_SERVICE_CHARGED_PAX_INFO = "serviceChargePaxId";
	public static final String RES_SERVICE_TOTAL_PRICE = "totalPrice";
	public static final String QUOTED_EXTERNAL_CHARGES = "externalCharges";
	public static final String AVAILABLE_OND_BUNDLED_FARES = "availableOndBundledFare";
	public static final String APPLICABLE_SERVICE_TAXES = "applicableServiceTaxes";

	/** Boolean flag to indicate in bound flexi was selected in the price quote. */
	public static final String RES_INBOUND_FLEXI_SELECTED = "inBoundFlexiSelected";

	/** Boolean flag to indicate out bound flexi was selected in the price quote. */
	public static final String RES_OUTBOUND_FLEXI_SELECTED = "outBoundFlexiSelected";

	public static final String FLIGHT_MULTI_MEAL_SELECTION = "flightMultiMealSelection";

	/** The light weight {@link FareSegChargeTO} object to be used in recreating the {@link OndFareDTO} collection. */
	public static final String FARE_SEGMENT_CHARGE_TO = "fareSegmentChargeTO";
	public static final String RES_INSURANCE_SELECTED = "selectedInsuranceRef";
	public static final String QUOTED_INSURANCE = "quotedInsurance";
	public static final String PRICE_QUOTED_INSURANCE = "priceQuotedInsurance";
	public static final String QUOTED_PRICE_INFO = "quotedPriceInfo";
	public static final String PRICE_INFO = "priceInfo";
	public static final String RETURN_POINT_AIRPORT = "returnPointAirport";
	public static final String REQUOTE_BALANCE_TO = "requoteBalanceTO";
	public static final String SERVICE_TAX_BASE_CRITERIA_TO = "serviceTaxBaseCriteriaDTO";
	public static final String PRICE_QUOTED_BUNDLED = "priceQuotedBundled";
	public static final String USER_DEFINED_SSR_PRICE = "userDefinedSSRPrice";


	private String _transactionId;

	private Map<String, Object> _transactionData = new HashMap<String, Object>();

	private boolean _isnew = true;

	private boolean _isCommitted = false;

	private long _lastAccessed;

	private long _creationTime;

	private int _status = 0;

	private String _sessionId;

	public Transaction() {
		String uid = UID.generate();
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String sessionIdSuffix = sessionId;
		if (sessionIdSuffix != null && sessionIdSuffix.length() >= 9) {
			sessionIdSuffix = sessionIdSuffix.substring(sessionIdSuffix.length() - 9);
			uid = uid.substring(0, uid.length() - 9) + sessionIdSuffix;
		}

		_transactionId = new String("TID$").concat(uid);
		this.setCreationTime(System.currentTimeMillis());
		updateLastAccessed();
		setStatus(STATUS_ACTIVE);
		if (log.isInfoEnabled()) {
			log.info("New Transaction Created [TransactionId =" + _transactionId + ", SessionId =" + sessionId
					+ ", Creation Timestamp=" + CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(this.getCreationTime()))
					+ "]");
		}
	}

	public String getId() {
		return _transactionId;
	}

	public void setParameter(String key, Object value) {
		_transactionData.put(key, value);
		updateLastAccessed();
	}

	public Object getParameter(String key) {
		updateLastAccessed();
		return _transactionData.get(key);
	}

	public Object removeParameter(String key) {
		updateLastAccessed();
		return _transactionData.remove(key);
	}
	
	@Override
	public Map<String, Object> getAllParameters() {
		return _transactionData;
	}
	
	@Override
	public void setAllParameters(Map<String, Object> parameters) {
		_transactionData.putAll(parameters);
	}

	public boolean containsParameter(String key) {
		updateLastAccessed();
		return _transactionData.containsKey(key);
	}

	public int getParamsCount() {
		updateLastAccessed();
		return _transactionData.size();
	}

	public boolean isNew() {
		updateLastAccessed();
		return _isnew;
	}

	public void commit() {
		_isCommitted = true;
	}

	public boolean isCommitted() {
		return _isCommitted;
	}

	private void updateLastAccessed() {
		if (log.isDebugEnabled()) {
			log.debug("Before Updating Transaction Last Accessed Timestamp [TransactionId =" + _transactionId
					+ ", Current Timestamp="
					+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis())) + ", Status ="
					+ (getStatus() == STATUS_ACTIVE ? "ACTIVE" : "EXPIRED") + "]");
		}
		if (getStatus() == STATUS_EXPIRED) {
			throw new RuntimeException("Transaction Expired");
		}
		_lastAccessed = System.currentTimeMillis();
	}

	public long getLastAccessedTimestamp() {
		return _lastAccessed;
	}

	public int getStatus() {
		return _status;
	}

	private void setStatus(int status) {
		this._status = status;
	}

	/**
	 * @return the creationTime
	 */
	public long getCreationTime() {
		return _creationTime;
	}

	/**
	 * @param time
	 *            the creationTime to set
	 */
	private void setCreationTime(long time) {
		_creationTime = time;
	}

	/**
	 * Times out transaction.
	 * 
	 * @param tnxTimeoutDuration
	 *            Txn timeout duration.
	 */
	public boolean qualifyForExpiration(long tnxTimeoutDuration) {
		boolean isExpired = false;
		if (getStatus() == STATUS_EXPIRED || (System.currentTimeMillis() - getLastAccessedTimestamp() > tnxTimeoutDuration)) {
			if (log.isInfoEnabled())
				log.info("Qualifying transaction for expiration. [Trasaction Id=" + getId() + ", Creation Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(getCreationTime())) + ", Last Accessed Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(getLastAccessedTimestamp()))
						+ ", Active Duration (seconds)=" + (System.currentTimeMillis() - getLastAccessedTimestamp()) / 1000
						+ ", Current Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis())) + "]");
			setStatus(STATUS_EXPIRED);
			_transactionData.clear();
			isExpired = true;
		}
		return isExpired;
	}

	/**
	 * Force expire transaction.
	 */
	public void forceExpiration() {
		if (getStatus() != STATUS_EXPIRED) {
			if (log.isInfoEnabled())
				log.info("Forcing transaction expiration. [Trasaction Id=" + getId() + ",Creation Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(getCreationTime())) + ",Last Accessed Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(getLastAccessedTimestamp()))
						+ ",Active Duration (seconds)=" + (System.currentTimeMillis() - getLastAccessedTimestamp()) / 1000
						+ ",Current Time="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis())) + "]");
			setStatus(STATUS_EXPIRED);
			_transactionData.clear();
		}
	}

	@Override
	public String toString() {
		StringBuffer string = new StringBuffer("Keys:");
		for (Iterator i = _transactionData.keySet().iterator(); i.hasNext();) {
			string.append(i.next());
			if (i.hasNext())
				string.append(",");
		}

		return string.toString();
	}

	public String getSessionId() {
		return _sessionId;
	}

	public void setSessionId(String sessionId) {
		_sessionId = sessionId;
	}

}
