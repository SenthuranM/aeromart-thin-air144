package com.isa.thinair.webservices.core.cache.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class MasterDataCacheUtil {

	private static Log log = LogFactory.getLog(MasterDataCacheUtil.class);
	
	private boolean enableSpecificAgentsCache;

	private long specificAgentsCacheTimeOutInMinutes;

	private List<String> specificAgents = new ArrayList<>();

	private Date cacheTimeOutDateTimeOfSpecificAgents = new Date();

	private void refreshSpecificAgentsCache() {
		if (isCacheTimeoutSpecificAgents()) {
			synchronized (MasterDataCacheUtil.class) {
				if (isCacheTimeoutSpecificAgents()) {
					try {
						specificAgents = WebServicesModuleUtils.getTravelAgentBD()
								.getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents();
						cacheTimeOutDateTimeOfSpecificAgents = new Date(
								System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(specificAgentsCacheTimeOutInMinutes));
					} catch (Exception exception) {
						log.error("getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents fail:", exception);
					}
				}
			}
		}
	}

	private boolean isCacheTimeoutSpecificAgents() {
		return cacheTimeOutDateTimeOfSpecificAgents.getTime() <= System.currentTimeMillis();
	}

	public List<String> getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents() {
		refreshSpecificAgentsCache();
		return specificAgents;
	}


	public long getSpecificAgentsCacheTimeOutInMinutes() {
		return specificAgentsCacheTimeOutInMinutes;
	}

	public void setSpecificAgentsCacheTimeOutInMinutes(long specificAgentsCacheTimeOutInMinutes) {
		this.specificAgentsCacheTimeOutInMinutes = specificAgentsCacheTimeOutInMinutes;
	}

	public boolean isEnableSpecificAgentsCache() {
		return enableSpecificAgentsCache;
	}

	public void setEnableSpecificAgentsCache(boolean enableSpecificAgentsCache) {
		this.enableSpecificAgentsCache = enableSpecificAgentsCache;
	}

}
