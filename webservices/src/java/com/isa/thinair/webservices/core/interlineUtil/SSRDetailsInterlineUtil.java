package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.TravelerInfoType;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCUserDefinedSSRDetails;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class SSRDetailsInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(SSRDetailsInterlineUtil.class);
	private static final String ADULT_REF_PREFIX = "A";
	private static final String CHILD_REF_PREFIX = "C";
	private static final String INFANT_REF_PREFIX = "I";

	/**
	 * 
	 * @param travelerFlightDetailsWithSSRs
	 * @param paxCountAssembler
	 * @param system
	 * @param flightPriceRS
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<String, List<LCCAirportServiceDTO>>> getSelectedAirportServiceDetails(
			Map<String, Map<String, List<HashMap<String, String>>>> travelerFlightDetailsWithSSRs,
			PaxCountAssembler paxCountAssembler, SYSTEM system, FlightPriceRS flightPriceRS) throws ModuleException,
			WebservicesException {

		Map<String, Map<String, List<LCCAirportServiceDTO>>> segmentWithSSRs = new HashMap<String, Map<String, List<LCCAirportServiceDTO>>>();

		// Map<Integer, List<String>> airportFltSegIdMap = new HashMap<Integer, List<String>>();

		Map<String, LCCAirportServiceDTO> ssrApplByReserMap = new HashMap<String, LCCAirportServiceDTO>();

		// for (String travelerRef : travelerFlightDetailsWithSSRs.keySet()) {
		// Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs =
		// travelerFlightDetailsWithSSRs.get(travelerRef);
		//
		// for (Integer fltSegId : flightDetailsWithSSRs.keySet()) {
		// List<HashMap<String, String>> serviceListByAptList = flightDetailsWithSSRs.get(fltSegId);
		// Iterator<HashMap<String, String>> itr = serviceListByAptList.iterator();
		// while (itr.hasNext()) {
		// HashMap<String, String> serviceByAptMap = itr.next();
		//
		// String aptCode = serviceByAptMap.get("AIRPORT_CODE");
		//
		// if (airportFltSegIdMap.containsKey(fltSegId) && (!airportFltSegIdMap.containsValue(aptCode))) {
		// airportFltSegIdMap.get(fltSegId).add(aptCode);
		// } else if (airportFltSegIdMap.get(fltSegId) == null) {
		// airportFltSegIdMap.put(fltSegId, new ArrayList<String>());
		// airportFltSegIdMap.get(fltSegId).add(aptCode);
		// }
		//
		// }
		// }
		//
		// }

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());

		}

		List<BundledFareDTO> bundledFareDTOs = null;
		if (flightPriceRS != null && flightPriceRS.getSelectedPriceFlightInfo() != null
				&& flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
			bundledFareDTOs = flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO().getOndBundledFareDTOs();
		}

		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability = WebServicesModuleUtils
				.getAirproxyAncillaryBD().getAvailableAiportServices(flightSegmentTOs, AppIndicatorEnum.APP_WS,
						SSRCategory.ID_AIRPORT_SERVICE, system, Locale.ENGLISH.getLanguage(), null, null, null, false,
						bundledFareDTOs, null);

		for (String travelerRef : travelerFlightDetailsWithSSRs.keySet()) {
			Map<String, List<HashMap<String, String>>> flightDetailsWithSSRs = travelerFlightDetailsWithSSRs.get(travelerRef);

			for (String flightRPH : flightDetailsWithSSRs.keySet()) {

				List<HashMap<String, String>> serviceListByAptList = flightDetailsWithSSRs.get(flightRPH);

				Iterator<HashMap<String, String>> itr = serviceListByAptList.iterator();
				while (itr.hasNext()) {
					HashMap<String, String> serviceByAptMap = itr.next();

					String airportCode = serviceByAptMap.get("AIRPORT_CODE");
					String airportType = serviceByAptMap.get("AIRPORT_TYPE");
					String ssrCode = serviceByAptMap.get("SSR_CODE");

					LCCAirportServiceDTO airportServiceDTO = getLCCAirportServiceDTO(flightRPH, airportCode, ssrCode,
							aiportWiseAvailability);

					if (airportServiceDTO == null) {
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_AIRPORT_,
								" Requested Airport Services not available for this flight segment");
					}

					if (airportServiceDTO != null) {
						if (segmentWithSSRs.get(travelerRef) == null) {
							segmentWithSSRs.put(travelerRef, new HashMap<String, List<LCCAirportServiceDTO>>());
							if (segmentWithSSRs.get(travelerRef).get(flightRPH) == null) {
								segmentWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<LCCAirportServiceDTO>());
							}
						} else {
							if (segmentWithSSRs.get(travelerRef).get(flightRPH) == null) {
								segmentWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<LCCAirportServiceDTO>());
							}
						}

						if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(airportServiceDTO.getApplicabilityType())) {
							// airport services applies by reservation
							// applicable by reservation will be added @ last
							String key = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH) + AirportServiceDTO.SEPERATOR
									+ airportCode + AirportServiceDTO.SEPERATOR + flightRPH;
							ssrApplByReserMap.put(key, airportServiceDTO);
						} else {
							segmentWithSSRs.get(travelerRef).get(flightRPH).add(airportServiceDTO);
						}

					}

				}

			}
		}

		if (ssrApplByReserMap != null && ssrApplByReserMap.size() > 0) {
			return includeLCCAirportServicesByReservation(paxCountAssembler, segmentWithSSRs, ssrApplByReserMap);
		} else {
			return segmentWithSSRs;
		}
	}

	/**
	 * 
	 * @param flightRPH
	 * @param airportCode
	 * @param ssrCode
	 * @param selectedAPSMap
	 * @return
	 */
	private static LCCAirportServiceDTO getLCCAirportServiceDTO(String flightRPH, String airportCode, String ssrCode,
			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> selectedAPSMap) {

		if (selectedAPSMap != null && !selectedAPSMap.isEmpty()) {
			for (AirportServiceKeyTO airportServiceKeyTO : selectedAPSMap.keySet()) {

				if (airportServiceKeyTO.getAirport().equals(airportCode)) {

					LCCFlightSegmentAirportServiceDTO segmentServiceDTO = selectedAPSMap.get(airportServiceKeyTO);
					FlightSegmentTO flightSegmentTO = segmentServiceDTO.getFlightSegmentTO();

					if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
						for (LCCAirportServiceDTO lccAirportServiceDTO : segmentServiceDTO.getAirportServices()) {
							if (ssrCode.equals(lccAirportServiceDTO.getSsrCode())) {
								return lccAirportServiceDTO;
							}
						}
					}
				}
			}
		}

		return null;
	}

	private static Map<String, Map<String, List<LCCAirportServiceDTO>>> includeLCCAirportServicesByReservation(
			PaxCountAssembler paxCountAssembler, Map<String, Map<String, List<LCCAirportServiceDTO>>> segmentWithSSRsNew,
			Map<String, LCCAirportServiceDTO> ssrByReserMap) throws ModuleException {

		Map<String, Map<String, List<LCCAirportServiceDTO>>> segmentWithSSRsMap = new HashMap<String, Map<String, List<LCCAirportServiceDTO>>>();

		try {

			Collection<String> paxTypes = new ArrayList<String>();
			if (paxCountAssembler.getAdultCount() > 0)
				paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
			if (paxCountAssembler.getChildCount() > 0)
				paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));

			int paxSeq = 0;

			for (String paxType : paxTypes) {
				int paxQuantity = getPaxQuantity(paxType, paxCountAssembler);
				for (int i = 0; i < paxQuantity; i++) {
					String travelerRPH = null;
					if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						travelerRPH = "A" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						travelerRPH = "C" + (++paxSeq);
					}

					segmentWithSSRsMap.put(travelerRPH, new HashMap<String, List<LCCAirportServiceDTO>>());
				}
			}

			for (String travelerRPH : segmentWithSSRsMap.keySet()) {
				if (segmentWithSSRsNew.get(travelerRPH) != null) {
					segmentWithSSRsMap.get(travelerRPH).putAll(segmentWithSSRsNew.get(travelerRPH));
				}

				if (ssrByReserMap != null && ssrByReserMap.size() > 0) {

					for (String fltSegRefKey : ssrByReserMap.keySet()) {

						LCCAirportServiceDTO availableSSRDTO = ssrByReserMap.get(fltSegRefKey);

						String[] fltSegRefKeyArr = fltSegRefKey.split("\\" + AirportServiceDTO.SEPERATOR);

						String flightRPH = fltSegRefKeyArr[2];

						if (segmentWithSSRsMap.get(travelerRPH) == null) {
							segmentWithSSRsMap.put(travelerRPH, new HashMap<String, List<LCCAirportServiceDTO>>());
							if (segmentWithSSRsMap.get(travelerRPH).get(flightRPH) == null) {
								segmentWithSSRsMap.get(travelerRPH).put(flightRPH, new ArrayList<LCCAirportServiceDTO>());
							}
						} else {
							if (segmentWithSSRsMap.get(travelerRPH).get(flightRPH) == null) {
								segmentWithSSRsMap.get(travelerRPH).put(flightRPH, new ArrayList<LCCAirportServiceDTO>());
							}
						}

						segmentWithSSRsMap.get(travelerRPH).get(flightRPH).add(availableSSRDTO);

					}

				}

			}

		} catch (Exception e) {
			throw new ModuleException("Error while adding Airport services applies by reservation");
		}

		return segmentWithSSRsMap;
	}

	/**
	 * 
	 * @param paxType
	 * @param paxCountAssembler
	 * @return
	 */
	private static int getPaxQuantity(String paxType, PaxCountAssembler paxCountAssembler) {

		int paxQuantity = 0;

		if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
			paxQuantity = paxCountAssembler.getAdultCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
			paxQuantity = paxCountAssembler.getChildCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
			paxQuantity = paxCountAssembler.getInfantCount();
		}

		return paxQuantity;
	}

	/**
	 * 
	 * @param ssrDetailsMap
	 * @param paxCountAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForLCCAirportServices(
			Map<String, Map<String, List<LCCAirportServiceDTO>>> ssrDetailsMap, PaxCountAssembler paxCountAssembler)
			throws ModuleException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();

		int paxCount = paxCountAssembler.getAdultCount() + paxCountAssembler.getChildCount();

		int i = 1;

		for (String travelerRef : ssrDetailsMap.keySet()) {

			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();

			Map<String, List<LCCAirportServiceDTO>> ssrsMap = ssrDetailsMap.get(travelerRef);

			for (String flightRPH : ssrsMap.keySet()) {
				List<LCCAirportServiceDTO> airportServiceDTOs = ssrsMap.get(flightRPH);
				for (LCCAirportServiceDTO airportServiceDTO : airportServiceDTOs) {
					LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

					BigDecimal amount = BigDecimal.ZERO;

					if (LCCAirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(airportServiceDTO.getApplicabilityType())) {
						// PAX wise
						if (travelerRef.startsWith(ADULT_REF_PREFIX)) {
							// adult
							amount = airportServiceDTO.getAdultAmount();
						} else if (travelerRef.startsWith(CHILD_REF_PREFIX)) {
							// child
							amount = airportServiceDTO.getChildAmount();
						} else if (travelerRef.startsWith(INFANT_REF_PREFIX)) {
							// infant
							amount = airportServiceDTO.getInfantAmount();
						}

					} else {
						// Reservation
						// if the service is per reservation then split for all PAX(AD+CH)
						amount = airportServiceDTO.getReservationAmount();
						BigDecimal[] roundUpValues = AccelAeroCalculator.roundAndSplit(amount, paxCount);
						if (i != paxCount) {
							amount = roundUpValues[0];
						} else {
							amount = roundUpValues[paxCount - 1];
						}

					}

					if ((!travelerRef.startsWith(INFANT_REF_PREFIX))) {

						externalChargeTO.setAmount(amount);
						externalChargeTO.setCode(airportServiceDTO.getSsrCode());
						externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
						externalChargeTO.setAirportCode(airportServiceDTO.getAirportCode());
						externalChargeTO.setApplyOn(airportServiceDTO.getApplyOn());
						externalChargeTO.setFlightRefNumber(flightRPH);

						chargesList.add(externalChargeTO);
					}

				}
			}
			externalCharges.put(travelerRef, chargesList);

			if (travelerRef.startsWith(ADULT_REF_PREFIX) || travelerRef.startsWith(CHILD_REF_PREFIX)) {
				i++;
			}

		}
		return externalCharges;
	}

	/**
	 * 
	 * @param travelerFlightDetailsWithSSRs
	 * @param flightPriceRS
	 * @param system
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<String, List<LCCSpecialServiceRequestDTO>>> getSelectedInflightServiceDetails(
			Map<String, Map<String, List<HashMap<String, String>>>> travelerFlightDetailsWithSSRs, FlightPriceRS flightPriceRS,
			SYSTEM system) throws ModuleException, WebservicesException {

		Map<String, Map<String, List<LCCSpecialServiceRequestDTO>>> segmentWithSSRs = new HashMap<String, Map<String, List<LCCSpecialServiceRequestDTO>>>();

		Map<String, String> fltSegMap = new HashMap<String, String>();

		for (Entry<String, Map<String, List<HashMap<String, String>>>> entry : travelerFlightDetailsWithSSRs.entrySet()) {
			Map<String, List<HashMap<String, String>>> flightDetailsWithSSRs = entry.getValue();
			for (Entry<String, List<HashMap<String, String>>> innerEnrty : flightDetailsWithSSRs.entrySet()) {
				List<HashMap<String, String>> serviceList = innerEnrty.getValue();
				String flightRPH = innerEnrty.getKey();

				for (HashMap<String, String> hashMap : serviceList) {
					String segCode = hashMap.get("SEGMENT_CODE");
					fltSegMap.put(flightRPH, segCode);
				}
			}
		}

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());

		}

		LCCUserDefinedSSRDetails userDefinedSSRDetails = getSSRDetailsWithUserDefinedSSR(travelerFlightDetailsWithSSRs);
		List<LCCFlightSegmentSSRDTO> inflightServicesList = new ArrayList<LCCFlightSegmentSSRDTO>();
		if (userDefinedSSRDetails.getSsrCodes().size() > 0) {
			inflightServicesList = WebServicesModuleUtils.getAirproxyAncillaryBD().getSpecialServiceRequests(flightSegmentTOs,
					null, system, Locale.ENGLISH.getLanguage(), null, null, false, false, false, false);
		}

		if (userDefinedSSRDetails.isUserDefinedSSRExist()) {
			inflightServicesList = includeUserDefinedSSRWithInFlightServices(inflightServicesList, flightSegmentTOs, system);
		}

		Map<String, Map<String, Integer>> fltWiseCount = getFlightWiseCount(travelerFlightDetailsWithSSRs);

		for (Entry<String, Map<String, List<HashMap<String, String>>>> entry : travelerFlightDetailsWithSSRs.entrySet()) {
			String travelerRef = entry.getKey();
			Map<String, List<HashMap<String, String>>> flightDetailsWithSSRs = entry.getValue();
			for (Entry<String, List<HashMap<String, String>>> innerEnrty : flightDetailsWithSSRs.entrySet()) {
				String flightRPH = innerEnrty.getKey();
				List<HashMap<String, String>> serviceList = innerEnrty.getValue();

				for (HashMap<String, String> hashMap : serviceList) {
					String ssrCode = hashMap.get("SSR_CODE");
					boolean isUserDefined = isUserDefinedSSR(hashMap);					
					for (LCCFlightSegmentSSRDTO lccFlightSegmentSSRDTO : inflightServicesList) {

						FlightSegmentTO flightSegmentTO = lccFlightSegmentSSRDTO.getFlightSegmentTO();

						if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {

							LCCSpecialServiceRequestDTO ssrDTO = getLCCInflightServiceDTO(ssrCode,
									lccFlightSegmentSSRDTO.getSpecialServiceRequest());

							if (ssrDTO == null) {
								throw new WebservicesException(
										IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_FLIGHT_SEGMENT_,
										" Requested In-flight Services not available for this flight segment");
							}

							int requestedSSRCount = fltWiseCount.get(flightRPH).get(ssrCode);

							if (!isUserDefined && requestedSSRCount > ssrDTO.getAvailableQty()) {
								throw new WebservicesException(
										IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_FLIGHT_SEGMENT_,
										" Not enough In-flight Service [" + ssrCode + "] available for requested flight segment");
							}

							if (segmentWithSSRs.get(travelerRef) == null) {
								segmentWithSSRs.put(travelerRef, new HashMap<String, List<LCCSpecialServiceRequestDTO>>());
								if (segmentWithSSRs.get(travelerRef).get(flightRPH) == null) {
									segmentWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<LCCSpecialServiceRequestDTO>());
								}
							} else {
								if (segmentWithSSRs.get(travelerRef).get(flightRPH) == null) {
									segmentWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<LCCSpecialServiceRequestDTO>());
								}
							}

							segmentWithSSRs.get(travelerRef).get(flightRPH).add(ssrDTO);
						}
					}
				}
			}
		}

		return segmentWithSSRs;
	}

	public static Map<String, Map<String, Integer>> getFlightWiseCount(
			Map<String, Map<String, List<HashMap<String, String>>>> paxWiseMap) {

		Map<String, Map<String, Integer>> fltWiseCount = new HashMap<String, Map<String, Integer>>();

		for (Entry<String, Map<String, List<HashMap<String, String>>>> paxEntry : paxWiseMap.entrySet()) {
			Map<String, List<HashMap<String, String>>> flightDetailsWithSSRs = paxEntry.getValue();
			for (Entry<String, List<HashMap<String, String>>> flightEntry : flightDetailsWithSSRs.entrySet()) {
				String flightRPH = flightEntry.getKey();
				if (fltWiseCount.get(flightRPH) == null || !fltWiseCount.containsKey(flightRPH)) {
					fltWiseCount.put(flightRPH, new HashMap<String, Integer>());
				}

				Map<String, Integer> ssrCountMap = fltWiseCount.get(flightRPH);

				List<HashMap<String, String>> serviceList = flightEntry.getValue();
				for (HashMap<String, String> hashMap : serviceList) {
					String ssrCode = hashMap.get("SSR_CODE");
					if (ssrCountMap.get(ssrCode) == null || !ssrCountMap.containsKey(ssrCode)) {
						ssrCountMap.put(ssrCode, 1);
					} else {
						ssrCountMap.put(ssrCode, ssrCountMap.get(ssrCode) + 1);
					}

				}

			}
		}

		return fltWiseCount;
	}

	private static LCCSpecialServiceRequestDTO
			getLCCInflightServiceDTO(String ssrCode, List<LCCSpecialServiceRequestDTO> ssrLisr) {
		for (LCCSpecialServiceRequestDTO specialServiceRequestDTO : ssrLisr) {
			if (specialServiceRequestDTO.getSsrCode().equals(ssrCode)) {
				return specialServiceRequestDTO;
			}
		}

		return null;
	}

	/**
	 * 
	 * @param ssrDetailsMap
	 * @param paxCountAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForLCCInflightService(
			Map<String, Map<String, List<LCCSpecialServiceRequestDTO>>> ssrDetailsMap, PaxCountAssembler paxCountAssembler)
			throws ModuleException,WebservicesException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();

		for (Entry<String, Map<String, List<LCCSpecialServiceRequestDTO>>> paxEntry : ssrDetailsMap.entrySet()) {

			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();

			Map<String, List<LCCSpecialServiceRequestDTO>> fltSSRMap = paxEntry.getValue();
			String travelerRef = paxEntry.getKey();

			for (Entry<String, List<LCCSpecialServiceRequestDTO>> fltEntry : fltSSRMap.entrySet()) {
				String flightRPH = fltEntry.getKey();
				List<LCCSpecialServiceRequestDTO> ssrList = fltEntry.getValue();

				for (LCCSpecialServiceRequestDTO specialServiceRequestDTO : ssrList) {

					LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

					BigDecimal amount = specialServiceRequestDTO.getCharge();
					
					if (specialServiceRequestDTO.isUserDefinedCharge()) {
						amount = getUserDefinedSSRChargeAmountFromTnx(specialServiceRequestDTO.getSsrCode(), false);
					}

					if ((!travelerRef.startsWith(INFANT_REF_PREFIX))) {

						externalChargeTO.setAmount(amount);
						externalChargeTO.setCode(specialServiceRequestDTO.getSsrCode());
						externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
						externalChargeTO.setFlightRefNumber(flightRPH);
						externalChargeTO.setApplyOn(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT);

						chargesList.add(externalChargeTO);
					}
				}

			}

			externalCharges.put(travelerRef, chargesList);

		}

		return externalCharges;

	}

	/**
	 * 
	 * @param travelerInfoType
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, List<LCCSelectedSegmentAncillaryDTO>> processPaxWiseSpecialServices(
			TravelerInfoType travelerInfoType) throws ModuleException, WebservicesException {
		Map<String, List<LCCSelectedSegmentAncillaryDTO>> paxWiseServiceMap = new HashMap<String, List<LCCSelectedSegmentAncillaryDTO>>();

		List<SpecialReqDetailsType> specialReqList = travelerInfoType.getSpecialReqDetails();

		if (specialReqList == null) {
			return paxWiseServiceMap;
		}

		LCCSpecialServiceRequestDTO specialServiceReqDTO = null;
		LCCAirportServiceDTO airportServiceDTO = null;

		for (SpecialReqDetailsType specialReqDetailsType : specialReqList) {
			if (specialReqDetailsType.getSSRRequests() != null && specialReqDetailsType.getSSRRequests().getSSRRequest() != null) {
				for (SSRRequest ssrRequest : specialReqDetailsType.getSSRRequests().getSSRRequest()) {
					LCCSelectedSegmentAncillaryDTO ancillaryDTO = new LCCSelectedSegmentAncillaryDTO();
					boolean isAirportService = false;
					String errorMessage = "";

					/*
					 * If service has an airport code then service is an Airport Service otherwise is an In-flight
					 * Service
					 */
					if (ssrRequest.getAirportCode() != null && !"".equals(ssrRequest.getAirportCode())) {
						isAirportService = true;
						errorMessage = "Airport Service(Hala)";
						airportServiceDTO = new LCCAirportServiceDTO();
					} else {
						isAirportService = false;
						errorMessage = "In-flight Service";
						specialServiceReqDTO = new LCCSpecialServiceRequestDTO();
					}
					// TODO does this also need to go through airproxy ?
					if (ssrRequest != null
							&& (ssrRequest.getSsrCode().length() > 4 || !WebServicesModuleUtils.getWebServiceDAO().isValidSSR(
									ssrRequest.getSsrCode()))) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, errorMessage + " code ["
								+ ssrRequest.getSsrCode() + "] is not supported.");
					}

					int ssrId = SSRUtil.getSSRId(ssrRequest.getSsrCode());

					// TODO Nafly, have to write an api in LCC and call it through Airproxy to retrive the
					// SSR using the ID,
					SSRCharge ssrCharge = WebServicesModuleUtils.getChargeBD().getSSRChargesBySSRId(ssrId);

					if (ssrCharge == null) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Service charge for "
								+ errorMessage + " code [" + ssrRequest.getSsrCode() + "] is not available.");
					}

					if (isAirportService) {
						airportServiceDTO.setSsrCode(ssrRequest.getSsrCode());
						airportServiceDTO.setAirportCode(ssrRequest.getAirportCode());
						// ssrCustomDTO.setAirportType(ssrRequest.getAirportType());
					} else {
						specialServiceReqDTO.setSsrCode(ssrRequest.getSsrCode());
						specialServiceReqDTO.setText(ssrRequest.getText());
						// ssrCustomDTO.setAirportType(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT);
					}

					FlightSegmentTO flightSegmentTO = FlightRefNumberUtil.getFlightSegmentFromRPH(ssrRequest
							.getFlightRefNumberRPHList().get(0));
					flightSegmentTO.setFlightNumber(ssrRequest.getFlightNumber());
					flightSegmentTO.setDepartureDateTime(CommonUtil.parseXMLGregorianCalendar(ssrRequest.getDepartureDate()));

					ancillaryDTO.setFlightSegmentTO(flightSegmentTO);

					List<String> paxRphList = null;

					if (isAirportService) {
						if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(ssrCharge.getApplicablityType())) {
							paxRphList = new ArrayList<String>();
							for (AirTravelerType airTraveler : travelerInfoType.getAirTraveler()) {
								if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
										airTraveler.getPassengerTypeCode())) {
									paxRphList.add(airTraveler.getTravelerRefNumber().getRPH());
								}
							}
						} else if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(ssrCharge.getApplicablityType())) {
							paxRphList = ssrRequest.getTravelerRefNumberRPHList();
						}
					} else {
						paxRphList = ssrRequest.getTravelerRefNumberRPHList();
					}

					int count = 0;
					for (String paxRph : paxRphList) {
						AirTravelerType airTravelerType = getAirTravelerTypeObj(paxRph, travelerInfoType);

						if (isAirportService) {
							getApplicableSSRCharge(
									ssrCharge,
									airTravelerType.getPassengerTypeCode(),
									(airTravelerType.isAccompaniedByInfant() ? airTravelerType
											.isAccompaniedByInfant() : false), paxCountWithoutInf(travelerInfoType.getAirTraveler()),
									count, airportServiceDTO);

							if (ancillaryDTO.getAirportServiceDTOs() == null) {
								ancillaryDTO.setAirportServiceDTOs(new ArrayList<LCCAirportServiceDTO>());
							}

							ancillaryDTO.getAirportServiceDTOs().add(airportServiceDTO);

						} else {
							boolean isUserDefinedSSR = WebServicesModuleUtils.getSsrServiceBD().isUserDefinedSSR(ssrRequest.getSsrCode());
							BigDecimal chargeAmount = ssrCharge.getAdultAmount();
							if(isUserDefinedSSR){
								chargeAmount = getUserDefinedSSRChargeAmountFromTnx(ssrRequest.getSsrCode(), false);
							}							
							specialServiceReqDTO.setCharge(chargeAmount);

							if (ancillaryDTO.getSpecialServiceRequestDTOs() == null) {
								ancillaryDTO.setSpecialServiceRequestDTOs(new ArrayList<LCCSpecialServiceRequestDTO>());
							}

							ancillaryDTO.getSpecialServiceRequestDTOs().add(specialServiceReqDTO);
						}

						ancillaryDTO.setTravelerRefNumber(paxRph);

						List<LCCSelectedSegmentAncillaryDTO> ssrDtoList = null;
						if (paxWiseServiceMap.get(paxRph) == null) {
							paxWiseServiceMap.put(paxRph, new ArrayList<LCCSelectedSegmentAncillaryDTO>());
						}

						ssrDtoList = paxWiseServiceMap.get(paxRph);
						if (!isServiceExists(ssrDtoList, specialServiceReqDTO, airportServiceDTO, isAirportService, paxRph, flightSegmentTO.getFlightRefNumber())) {
							ssrDtoList.add(ancillaryDTO);
						}

						count++;
					}
				}
			}
		}

		return paxWiseServiceMap;

	}

	/**
	 * 
	 * @param travelerRph
	 * @param travelerInfoType
	 * @return
	 */
	private static AirTravelerType getAirTravelerTypeObj(String travelerRph, TravelerInfoType travelerInfoType) {
		AirTravelerType airTravelerType = null;
		for (AirTravelerType airTraveler : travelerInfoType.getAirTraveler()) {
			if (airTraveler.getTravelerRefNumber().getRPH().equals(travelerRph)) {
				airTravelerType = airTraveler;
				break;
			}
		}

		return airTravelerType;
	}

	/**
	 * 
	 * @param airTravelers
	 * @return
	 */
	private static int paxCountWithoutInf(List<AirTravelerType> airTravelers) {
		int count = 0;

		if (airTravelers != null) {
			for (AirTravelerType airTravelerType : airTravelers) {
				if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
						airTravelerType.getPassengerTypeCode())) {
					count++;
				}
			}
		}

		return count;
	}

	private static void getApplicableSSRCharge(SSRCharge ssrCharge, String paxType, boolean isParent, int paxCount, int paxSeq,
			LCCAirportServiceDTO airportServiceDTO) {

		if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(ssrCharge.getApplicablityType())) {
			airportServiceDTO
					.setServiceCharge(AccelAeroCalculator.roundAndSplit(ssrCharge.getReservationAmount(), paxCount)[paxSeq]);

		} else if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(ssrCharge.getApplicablityType())) {
			if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(paxType) && isParent) {
				airportServiceDTO
						.setInfantAmount(AccelAeroCalculator.add(ssrCharge.getAdultAmount(), ssrCharge.getInfantAmount()));

			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(paxType) && !isParent) {
				airportServiceDTO.setAdultAmount(ssrCharge.getAdultAmount());

			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(paxType)) {
				airportServiceDTO.setChildAmount(ssrCharge.getChildAmount());
			}
		}

	}

	/**
	 * 
	 * @param paxServiceList
	 * @param specialReqservice
	 * @param airportService
	 * @param isAirportService
	 * @param paxRPH
	 * @param flightRefNumberRPH 
	 * @return
	 */
	private static boolean isServiceExists(List<LCCSelectedSegmentAncillaryDTO> paxServiceList,
			LCCSpecialServiceRequestDTO specialReqservice, LCCAirportServiceDTO airportService, boolean isAirportService,
			String paxRPH, String flightRefNumberRPH) {
		boolean exists = false;

		for (LCCSelectedSegmentAncillaryDTO paxService : paxServiceList) {

			if (paxService.getTravelerRefNumber().equals(paxRPH)) {
				if (isAirportService) {
					if (isAirPortServiceExists(paxService.getAirportServiceDTOs(), airportService)) {
						exists = true;
						break;
					}
				} else if (isSameFlightSegment(paxService.getFlightSegmentTO(), flightRefNumberRPH)) {
					if (isSSRServiceExists(paxService.getSpecialServiceRequestDTOs(), specialReqservice)) {
						exists = true;
						break;
					}
				}
			}
		}

		return exists;
	}

	/**
	 * 
	 * @param airportServiceDTOs
	 * @param airportService
	 * @return
	 */
	private static boolean isAirPortServiceExists(List<LCCAirportServiceDTO> airportServiceDTOs,
			LCCAirportServiceDTO airportService) {
		boolean exists = false;

		for (LCCAirportServiceDTO airportServiceDTO : airportServiceDTOs) {
			if (airportServiceDTO.getSsrCode().equals(airportService.getSsrCode())
					&& airportServiceDTO.getAirportCode().equals(airportService.getAirportCode())) {
				exists = true;
				break;
			}
		}
		return exists;
	}

	/**
	 * 
	 * @param serviceRequestDTOs
	 * @param serviceRequestDTO
	 * @return
	 */
	private static boolean isSSRServiceExists(List<LCCSpecialServiceRequestDTO> serviceRequestDTOs,
			LCCSpecialServiceRequestDTO serviceRequestDTO) {
		boolean exists = false;

		for (LCCSpecialServiceRequestDTO speRequestDTO : serviceRequestDTOs) {
			if (speRequestDTO.getSsrCode().equals(serviceRequestDTO.getSsrCode())) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	private static LCCUserDefinedSSRDetails getSSRDetailsWithUserDefinedSSR(
			Map<String, Map<String, List<HashMap<String, String>>>> travelerFlightDetailsWithSSRs) {
		LCCUserDefinedSSRDetails userDefinedSSRDetails = new LCCUserDefinedSSRDetails();
		if (travelerFlightDetailsWithSSRs != null && !travelerFlightDetailsWithSSRs.isEmpty()) {
			for (Entry<String, Map<String, List<HashMap<String, String>>>> entry : travelerFlightDetailsWithSSRs.entrySet()) {
				Map<String, List<HashMap<String, String>>> flightDetailsWithSSRs = entry.getValue();
				for (Entry<String, List<HashMap<String, String>>> innerEnrty : flightDetailsWithSSRs.entrySet()) {
					List<HashMap<String, String>> serviceList = innerEnrty.getValue();
					for (HashMap<String, String> hashMap : serviceList) {
						String ssrCode = hashMap.get("SSR_CODE");
						if (!StringUtil.isNullOrEmpty(ssrCode)) {
							if (isUserDefinedSSR(hashMap)) {
								userDefinedSSRDetails.addUserDefinedSsrCode(ssrCode);
							} else {
								userDefinedSSRDetails.addSsrCode(ssrCode);
							}
						}

					}
				}
			}
		}

		return userDefinedSSRDetails;
	}
	
	private static boolean isUserDefinedSSR(HashMap<String, String> hashMap) {
		if (hashMap != null && !hashMap.isEmpty()) {
			return "Y".equalsIgnoreCase(hashMap.get("USER_DEFINED_SSR")) ? true : false;
		}
		return false;
	}
	
	private static List<LCCFlightSegmentSSRDTO> includeUserDefinedSSRWithInFlightServices(
			List<LCCFlightSegmentSSRDTO> inflightServicesList, List<FlightSegmentTO> flightSegmentTOs, SYSTEM system)
			throws ModuleException {

		List<LCCFlightSegmentSSRDTO> inflightUserDefinedServicesList = WebServicesModuleUtils.getAirproxyAncillaryBD()
				.getSpecialServiceRequests(flightSegmentTOs, null, system, Locale.ENGLISH.getLanguage(), null, null, false, false,
						false, true);
		if (inflightUserDefinedServicesList != null && !inflightUserDefinedServicesList.isEmpty()) {
			if (inflightServicesList == null) {
				inflightServicesList = new ArrayList<>();
			}

			for (LCCFlightSegmentSSRDTO userDefinedFlightSegmentDTO : inflightUserDefinedServicesList) {
				FlightSegmentTO fltSegmentTemp = userDefinedFlightSegmentDTO.getFlightSegmentTO();
				boolean userDefinedSSRIncluded = false;
				for (LCCFlightSegmentSSRDTO lccFlightSegmentSSRDTO : inflightServicesList) {
					FlightSegmentTO flightSegmentTO = lccFlightSegmentSSRDTO.getFlightSegmentTO();

					if (fltSegmentTemp.getFlightRefNumber().equals(flightSegmentTO.getFlightRefNumber())) {
						lccFlightSegmentSSRDTO.getSpecialServiceRequest()
								.addAll(userDefinedFlightSegmentDTO.getSpecialServiceRequest());
						userDefinedSSRIncluded = true;
						break;
					}
				}

				if (!userDefinedSSRIncluded) {
					inflightServicesList.add(userDefinedFlightSegmentDTO);
				}
			}

		}
		return inflightServicesList;
	}
	
	private static boolean isSameFlightSegment(FlightSegmentTO flightSegmentTO, String flightRefNumberRPH) {
		if (flightSegmentTO != null && !StringUtil.isNullOrEmpty(flightSegmentTO.getFlightRefNumber())
				&& flightSegmentTO.getFlightRefNumber().equals(flightRefNumberRPH)) {
			return true;
		}
		return false;
	}

}
