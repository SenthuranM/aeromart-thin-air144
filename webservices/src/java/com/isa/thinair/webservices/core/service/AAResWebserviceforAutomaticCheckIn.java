package com.isa.thinair.webservices.core.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.automaticcheckin.AutomaticCheckinBL;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.commons.api.dto.ets.AutomaticCheckInReq;
import com.isa.thinair.commons.api.dto.ets.AutomaticCheckInRes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.service.AAResWebserviceforAutomaticCheckInService;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * This class includes services to Listen the Automatic check in Scheduler
 * 
 * @author Panchatcharam.s
 */

@Controller
@RequestMapping(value = "aeromart/automaticcheckin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AAResWebserviceforAutomaticCheckIn implements AAResWebserviceforAutomaticCheckInService {

	private Log log = LogFactory.getLog(AAResWebserviceforAutomaticCheckIn.class);
	private final String REQ_SUCCESS = "SUCCESS";
	private final String REQ_FAIL = "FAIL";
	private final String RETRIED_CHECKIN_SEAT_SELECTION = "RC";
	private final String RETRIED_SEAT_SELECTION = "RS";
	private final String CHECKIN_SEAT_SELECTION_SUCCESS = "CS";
	private final String SEAT_SELECTION_FAILED = "CF";
	private final String CHECKIN_SEAT_SELECTION_FAILED = "FF";
	private final int INIT_ATTEMPT = 1;
	private final int LAST_ATTEMPT = 2;
	private final String PNR_NOT_FOUND = "PNR not found";
	private final String CHECKIN_NOT_ALLOWED = "Check-in not allowed for this connection";

	@Override
	@ResponseBody
	@RequestMapping(value = "/update")
	public AutomaticCheckInRes display(@RequestBody AutomaticCheckInReq request) {
		AutomaticCheckInRes response = null;
		try {

			response = callAutocheckin(request);

		} catch (ModuleException e) {
			response = new AutomaticCheckInRes();
			response.setSuccess(false);
			response.setErrorMsg("Module Exception Error");

		} catch (ParseException e) {
			response = new AutomaticCheckInRes();
			response.setSuccess(false);
			response.setErrorMsg("Parse Exception Error");
		} catch (Exception e) {
			response = new AutomaticCheckInRes();
			response.setSuccess(false);
			response.setErrorMsg("Something went wrong becs : " + e.getMessage());
		}

		return response;
	}

	private AutomaticCheckInRes callAutocheckin(AutomaticCheckInReq request) throws ModuleException, ParseException {
		if (log.isDebugEnabled()) {
			log.debug("Entered into callAutocheckin ");
		}
		AutomaticCheckInRes response = new AutomaticCheckInRes();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date departureDate = sdf.parse(request.getDepartureDate());

		String seatSelectionStatus = WebServicesModuleUtils.getReservationAuxilliaryBD().getAutoCheckinSeatSelectionStatus(
				departureDate, request.getFlightNumber(), request.getTitle(), request.getFirstName(), request.getLastName(),
				request.getPnr(), request.getCheckinSeat());
		if (log.isDebugEnabled()) {
			log.debug("seatSelectionStatus : " + seatSelectionStatus);
		}
		PaxAutomaticCheckinTO paxAutomaticCheckinTO = WebServicesModuleUtils.getReservationAuxilliaryBD()
				.getAutoCheckinPassengerDetails(departureDate, request.getFlightNumber(), request.getTitle(),
						request.getFirstName(), request.getLastName(), request.getPnr(), request.getCheckinSeat(),
						seatSelectionStatus);
		String dcsCheckinStatus = "";
		Integer flightId = 0;
		AutomaticCheckinBL automaticCheckinBL = new AutomaticCheckinBL();
		flightId = AirSchedulesUtil.getFlightBD().getFlightId(request.getFlightNumber(), departureDate);
		switch (paxAutomaticCheckinTO.getNoOfAttempts()) {
		case INIT_ATTEMPT:
			// No Retrying for Check-in not allowed for this connection
			if (request.getCheckinStatus().equalsIgnoreCase(CHECKIN_NOT_ALLOWED)) {
				dcsCheckinStatus = CHECKIN_SEAT_SELECTION_FAILED;
				// Retried for both checkin and Seat Selection
			} else if (request.getCheckinStatus().equalsIgnoreCase(PNR_NOT_FOUND)) {
				dcsCheckinStatus = RETRIED_CHECKIN_SEAT_SELECTION;
				// Retried for Seat Selection. Checkin succeeded in first
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_FAIL)) {
				dcsCheckinStatus = RETRIED_CHECKIN_SEAT_SELECTION;
				// Retried for both checkin and Seat Selection
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_FAIL) && seatSelectionStatus.equalsIgnoreCase(REQ_FAIL)) {
				dcsCheckinStatus = RETRIED_CHECKIN_SEAT_SELECTION;
				// Retried for Seat Selection. Checkin succeeded in first
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_FAIL) || seatSelectionStatus.equalsIgnoreCase(REQ_FAIL)) {
				dcsCheckinStatus = RETRIED_SEAT_SELECTION;
				// Both checkin and Seat Selection success
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_SUCCESS)
					&& seatSelectionStatus.equalsIgnoreCase(REQ_SUCCESS)) {
				dcsCheckinStatus = CHECKIN_SEAT_SELECTION_SUCCESS;
			}

			if (log.isDebugEnabled()) {
				log.debug("No of attempts : " + paxAutomaticCheckinTO.getNoOfAttempts() + "dcsCheckinStatus for passenger : "
						+ paxAutomaticCheckinTO.getPnrPaxId() + " : " + dcsCheckinStatus);
			}
			if (dcsCheckinStatus.equalsIgnoreCase(CHECKIN_SEAT_SELECTION_SUCCESS)) {
				ReservationModuleUtils.getReservationAuxilliaryBD().updateAutomaticCheckin(
						new ArrayList<Integer>(Arrays.asList(paxAutomaticCheckinTO.getPnrPaxId())), flightId,
						paxAutomaticCheckinTO.getNoOfAttempts(), dcsCheckinStatus, request.getCheckinStatus());
			} else if (!dcsCheckinStatus.equalsIgnoreCase(CHECKIN_SEAT_SELECTION_SUCCESS)) {
				try {
					if (dcsCheckinStatus.equalsIgnoreCase(CHECKIN_SEAT_SELECTION_FAILED)) {
						ReservationModuleUtils.getReservationAuxilliaryBD().updateAutomaticCheckin(
								new ArrayList<Integer>(Arrays.asList(paxAutomaticCheckinTO.getPnrPaxId())), flightId,
								paxAutomaticCheckinTO.getNoOfAttempts(), dcsCheckinStatus, request.getCheckinStatus());
						automaticCheckinBL.emailAutoCheckin(request, paxAutomaticCheckinTO.getEmail(),
								ReservationInternalConstants.PnrTemplateNames.AUTOMATIC_CHECKIN_FAILURE);
					} else {
						automaticCheckinBL.sendAutomaticCheckinDetails(paxAutomaticCheckinTO.getPnrPaxId(), flightId,
								paxAutomaticCheckinTO.getNoOfAttempts() + 1, dcsCheckinStatus, request.getCheckinStatus());
					}

				} catch (Exception me) {
					throw new ModuleException("error.automaticcheckin.details");
				}
			}

			break;
		case LAST_ATTEMPT:
			// PNR not found OR Check-in not allowed for this connection
			if (request.getCheckinStatus().equalsIgnoreCase(PNR_NOT_FOUND)
					|| request.getCheckinStatus().equalsIgnoreCase(CHECKIN_NOT_ALLOWED)) {
				dcsCheckinStatus = CHECKIN_SEAT_SELECTION_FAILED;
				// Checkin success, but Seat Selection Failed
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_SUCCESS) && seatSelectionStatus.equalsIgnoreCase(REQ_FAIL)) {
				dcsCheckinStatus = SEAT_SELECTION_FAILED;
				// Both checkin and Seat Selection Failed
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_FAIL) && seatSelectionStatus.equalsIgnoreCase(REQ_FAIL)) {
				dcsCheckinStatus = CHECKIN_SEAT_SELECTION_FAILED;
				// Both checkin and Seat Selection success
			} else if (request.getCheckinStatus().equalsIgnoreCase(REQ_SUCCESS)
					&& seatSelectionStatus.equalsIgnoreCase(REQ_SUCCESS)) {
				dcsCheckinStatus = CHECKIN_SEAT_SELECTION_SUCCESS;
			}
			if (log.isDebugEnabled()) {
				log.debug("No of attempts : " + paxAutomaticCheckinTO.getNoOfAttempts() + "dcsCheckinStatus for passenger : "
						+ paxAutomaticCheckinTO.getPnrPaxId() + " : " + dcsCheckinStatus);
			}
			ReservationModuleUtils.getReservationAuxilliaryBD().updateAutomaticCheckin(
					new ArrayList<Integer>(Arrays.asList(paxAutomaticCheckinTO.getPnrPaxId())), flightId,
					paxAutomaticCheckinTO.getNoOfAttempts(), dcsCheckinStatus, request.getCheckinStatus());
			if (dcsCheckinStatus.equalsIgnoreCase(SEAT_SELECTION_FAILED)) {
				automaticCheckinBL.emailAutoCheckin(request, paxAutomaticCheckinTO.getEmail(),
						ReservationInternalConstants.PnrTemplateNames.AUTOMATIC_CHECKIN_DIFFERENT_SEAT);
			} else if (dcsCheckinStatus.equalsIgnoreCase(CHECKIN_SEAT_SELECTION_FAILED)) {
				automaticCheckinBL.emailAutoCheckin(request, paxAutomaticCheckinTO.getEmail(),
						ReservationInternalConstants.PnrTemplateNames.AUTOMATIC_CHECKIN_FAILURE);
			}
			break;
		default:
		}
		response.setRawResponse("The result yet to obtain");
		response.setSuccess(true);
		return response;
	}

}
