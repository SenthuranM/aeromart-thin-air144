package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRQ;
import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRS;
import ae.isa.service.meal.FlightRQ;
import ae.isa.service.meal.FlightRS;
import ae.isa.service.meal.FlightSegment;
import ae.isa.service.meal.Meal;
import ae.isa.service.meal.MealRecord;

import com.isa.thinair.airinventory.api.dto.FlightSegmentTo;
import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.airinventory.api.dto.PassengerTo;
import com.isa.thinair.airinventory.api.dto.meal.MealTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebServicesModuleException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

public class MealServicesUtils {
	private static Log log = LogFactory.getLog(MealServicesUtils.class);

	private MealServicesUtils() {
	}

	public static List<FlightTo> toFlightTos(AAMealServicesMealRecordsByFltSegRQ mealRecordsRQ) throws WebServicesModuleException {
		List<FlightTo> flights = new ArrayList<FlightTo>();
		FlightTo flightTo;

		for (FlightRQ flightRQ : mealRecordsRQ.getFlights()) {
			if (flightRQ.getFlightNumber() == null || flightRQ.getDepartureDate() == null) {
				throw new WebServicesModuleException(WebservicesConstants.WebServicesErrorCodes.MANDATORY_FIELD_ABSENT);
			}

			flightTo = new FlightTo();
			flightTo.setFlightNumber(flightRQ.getFlightNumber());
			flightTo.setDepartureDate(flightRQ.getDepartureDate().toGregorianCalendar().getTime());
			flights.add(flightTo);
		}

		return flights;
	}

	public static AAMealServicesMealRecordsByFltSegRS toMealRecordsRS(List<FlightTo> mealRecords) {
		AAMealServicesMealRecordsByFltSegRS mealRecordsRS = new AAMealServicesMealRecordsByFltSegRS();
		FlightRS flightRS;
		FlightSegment flightSegment;
		MealRecord mealRecord;
		Meal meal;
		Calendar calendar = new GregorianCalendar();

		for (FlightTo flightTo : mealRecords) {
			flightRS = new FlightRS();
			flightRS.setFlightNumber(flightTo.getFlightNumber());
			calendar.setTime(flightTo.getDepartureDate());
			try {
				flightRS.setDepartureDate(CommonUtil.getXMLGregorianCalendar(calendar));
			} catch (ModuleException e) {
				log.error(e.getMessage(), e);
			}

			for (FlightSegmentTo flightSegmentTo : flightTo.getFlightSegments()) {
				flightSegment = new FlightSegment();
				flightSegment.setSegmentCode(flightSegmentTo.getSegmentCode());

				for (PassengerTo passengerTo : flightSegmentTo.getPassengers()) {
					mealRecord = new MealRecord();
					mealRecord.setPnr(passengerTo.getPnr());
					mealRecord.setTitle(passengerTo.getTitle());
					mealRecord.setFirstName(passengerTo.getFirstName());
					mealRecord.setLastName(passengerTo.getLastName());
					mealRecord.setSeatCode(passengerTo.getSeatCode());

					for (MealTo mealTo : passengerTo.getMeals()) {
						meal = new Meal();
						meal.setMealCode(mealTo.getMealCode());
						meal.setMealName(mealTo.getMealName());
						meal.setQuantity(mealTo.getQuantity());

						mealRecord.getMeal().add(meal);
					}

					flightSegment.getMealRecord().add(mealRecord);
				}

				flightRS.getFlightSegments().add(flightSegment);
			}

			mealRecordsRS.getFlights().add(flightRS);
		}


		return mealRecordsRS;
	}
}
