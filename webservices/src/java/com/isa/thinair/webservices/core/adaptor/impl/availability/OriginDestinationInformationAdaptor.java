package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;

import com.isa.thinair.webservices.core.adaptor.Adaptor;

public class OriginDestinationInformationAdaptor implements
		Adaptor<OTAAirAvailRS.OriginDestinationInformation, AAOTAAirAllPriceAvailRS.OriginDestinationInformation> {

	@Override
	public org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS.OriginDestinationInformation adapt(
			OriginDestinationInformation source) {

		AAOTAAirAllPriceAvailRS.OriginDestinationInformation target = new AAOTAAirAllPriceAvailRS.OriginDestinationInformation();

		target.setDepartureDateTime(source.getDepartureDateTime());
		target.setArrivalDateTime(source.getArrivalDateTime());
		target.setOriginLocation(source.getOriginLocation());
		target.setDestinationLocation(source.getDestinationLocation());
		target.setConnectionLocations(source.getConnectionLocations());
		target.setGroundSegmentAvailability(source.getGroundSegmentAvailability());
		target.setRPH(source.getRPH());
		target.setSameAirportInd(source.isSameAirportInd());

		OriginDestinationOptionsAdptor adaptor = new OriginDestinationOptionsAdptor();
		target.setOriginDestinationOptions(adaptor.adapt(source.getOriginDestinationOptions()));

		return target;
	}
}
