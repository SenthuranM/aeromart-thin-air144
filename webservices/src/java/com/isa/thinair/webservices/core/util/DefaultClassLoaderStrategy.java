package com.isa.thinair.webservices.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Picks the child most ClassLoader from among Caller Class ClassLoader, ContextClassLoader and SystemClassLoader
 */
public class DefaultClassLoaderStrategy {

	private static final Log log = LogFactory.getLog(DefaultClassLoaderStrategy.class);

	public ClassLoader getClassLoader(final Class callerClass) {
		if (callerClass == null)
			throw new IllegalArgumentException("Caller Class is null.");

		final ClassLoader callerLoader = callerClass.getClassLoader();
		final ClassLoader contextLoader = Thread.currentThread().getContextClassLoader();

		ClassLoader result;

		/**
		 * if 'callerLoader' and 'contextLoader' are in a parent-child relationship, always choose the child:
		 */
		if (isChild(contextLoader, callerLoader)) {
			log.debug("Caller class classloader is the child");
			result = callerLoader;
		} else if (isChild(callerLoader, contextLoader)) {
			log.debug("Context classloader is the child");
			result = contextLoader;
		} else {
			// ambiguous case
			log.debug("Could not determine the child classloader; using Context ClassLoader");
			result = contextLoader;
		}

		final ClassLoader systemLoader = ClassLoader.getSystemClassLoader();

		// precaution for when deployed as a bootstrap or extension class:
		if (isChild(result, systemLoader)) {
			log.debug("SystemClassLoader is the child");
			result = systemLoader;
		}

		return result;
	}

	/**
	 * Returns 'true' if 'loader2' is a delegation child of 'loader1' or if 'loader1'=='loader2'.
	 */
	private static boolean isChild(final ClassLoader loader1, ClassLoader loader2) {
		if (loader1 == loader2)
			return true;
		// 'null' means primordial loader - parent of all the classloaders
		if (loader2 == null)
			return false;
		if (loader1 == null)
			return true;

		for (; loader2 != null; loader2 = loader2.getParent()) {
			if (loader2 == loader1)
				return true;
		}

		return false;
	}
}
