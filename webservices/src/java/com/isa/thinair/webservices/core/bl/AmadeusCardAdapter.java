package com.isa.thinair.webservices.core.bl;

import java.util.Map;

import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.TLAPaymentCardTypeRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusCardAdapter {
	private int cardType;
	private String cardHolderName;
	private String billingAddress;
	private String expiryDate;
	private String securityCode;
	private String cardNumber;

	public AmadeusCardAdapter(TLAPaymentCardTypeRS payment) throws ModuleException {
		payment.getCardCode();
		this.cardNumber = payment.getCardNumber();
		this.cardHolderName = payment.getCardHolderName();
		this.expiryDate = payment.getExpireDate().substring(2, 4) + payment.getExpireDate().substring(0, 2);
		this.securityCode = payment.getSeriesCode();
		String cardCode = payment.getCardCode();

		Map<String, Integer> cardAlphaMap = SelectListGenerator.creditCardAlphaCodeMap();
		Integer cardId = null;
		if (cardCode != null) {
			cardId = cardAlphaMap.get(cardCode);
		}
		if (cardId == null) {
			cardId = cardAlphaMap.get("*");
		}
		if (cardId == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.INVALID_CARD_CODE, ErrorWarningType.REQUIRED_FIELD_MISSING,
					"Invalid credit card code specified.");
		}
		cardType = cardId;

		if (payment.getBillingAddress() != null) {
			StringBuilder sb = new StringBuilder();
			if (payment.getBillingAddress().getPostalCode() != null)
				sb.append(payment.getBillingAddress().getPostalCode());
			sb.append(", " + AmadeusCommonUtil.join(payment.getBillingAddress().getAddressLine(), ","));
			if (payment.getBillingAddress().getCityName() != null)
				sb.append(", " + payment.getBillingAddress().getCityName());
			if (payment.getBillingAddress().getCountryName() != null)
				sb.append(", " + payment.getBillingAddress().getCountryName().getValue());
			billingAddress = sb.toString();

			if (this.cardHolderName == null) {
				if (payment.getBillingAddress().getGivenName() != null && payment.getBillingAddress().getSurname() != null) {
					this.cardHolderName = payment.getBillingAddress().getGivenName() + " "
							+ payment.getBillingAddress().getSurname();
				}

			}
		}

	}

	public String getName() {
		return cardHolderName;
	}

	public String getCardNo() {
		return cardNumber;
	}

	public String getAddress() {
		return billingAddress;
	}

	public int getType() {
		return cardType;
	}

	public String getExpDate() {
		return expiryDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

}
