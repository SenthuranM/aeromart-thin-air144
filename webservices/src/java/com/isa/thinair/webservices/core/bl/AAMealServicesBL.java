package com.isa.thinair.webservices.core.bl;

import java.util.Collection;
import java.util.List;

import javax.ejb.EJBAccessException;

import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRQ;
import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRS;
import ae.isa.service.meal.Error;
import ae.isa.service.meal.ErrorType;
import ae.isa.service.meal.ResponseStatus;

import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webservices.api.exception.WebServicesModuleException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.MealServicesUtils;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class AAMealServicesBL {
	
	public static AAMealServicesMealRecordsByFltSegRS getMealRecords(AAMealServicesMealRecordsByFltSegRQ mealRecordsRQ) {
		Collection<String> privilegesKeys;
		List<FlightTo> requestedFlights;
		List<FlightTo> mealRecords;
		AAMealServicesMealRecordsByFltSegRS recordsRS = null;
		boolean accessGranted = false;
		WebservicesConstants.WebServicesErrorCodes errorCode = null;

		privilegesKeys=ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		if (privilegesKeys == null) {
			errorCode = WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED;
		} else if (!AuthorizationUtil.hasPrivileges(privilegesKeys,
				PrivilegesKeys.WSFuncPrivilegesKeys.WS_CREW_MGMT_ACCESS)) {
			errorCode = WebservicesConstants.WebServicesErrorCodes.WS_AUTH_USER_INSUFFICIENT_PRIV;
		} else if (!AuthorizationUtil.hasPrivileges(privilegesKeys,
				PrivilegesKeys.WSFuncPrivilegesKeys.WS_CREW_MGMT_VIEW_MEAL_RECS)) {
			errorCode = WebservicesConstants.WebServicesErrorCodes.WS_AUTH_USER_INSUFFICIENT_PRIV;
		} else {
			accessGranted = true;
		}

		if (accessGranted) {
			try {
				requestedFlights = MealServicesUtils.toFlightTos(mealRecordsRQ);
				mealRecords = WebServicesModuleUtils.getMealBD().getMealRecords(requestedFlights);
				recordsRS = MealServicesUtils.toMealRecordsRS(mealRecords);
				recordsRS.setStatus(ResponseStatus.SUCCESS);
			} catch (EJBAccessException e){
				errorCode = WebservicesConstants.WebServicesErrorCodes.WS_AUTHENTICATION_FAILED;
			} catch (WebServicesModuleException e) {
				errorCode = e.getErrorCode();
			} catch (Exception e) {
				errorCode = WebservicesConstants.WebServicesErrorCodes.SERVER_ERROR;
			}
		}

		if (errorCode != null) {
			ErrorType errorType = new ErrorType();
			ae.isa.service.meal.Error error = new Error();
			error.setErrorCode(errorCode.getErrorCode());
			error.setDescription(errorCode.getErrorDescription());
			errorType.getError().add(error);

			recordsRS = new AAMealServicesMealRecordsByFltSegRS();
			recordsRS.setError(errorType);
			recordsRS.setStatus(ResponseStatus.FAIL);
		}

		return recordsRS;
	}
	
}
