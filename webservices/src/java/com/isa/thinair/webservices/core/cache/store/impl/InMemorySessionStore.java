package com.isa.thinair.webservices.core.cache.store.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.store.SessionStore;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class InMemorySessionStore implements SessionStore {

	private Map<String, Object> userSessions = Collections.synchronizedMap(new HashMap<String, Object>());

	private static final Log log = LogFactory.getLog(InMemorySessionStore.class);

	/**
	 * Create and initialize a new user session
	 * 
	 * @param userID
	 * @param password
	 * @return
	 * @throws WebservicesException
	 */
	@Override
	public AAUserSession createNewUserSession(String userID, String password, int salesChannelId) throws WebservicesException {
		AAUserSession aaUserSession = new AAUserSession();
		Collection privilegeKeys = new HashSet();
		try {
			/** Initialize User Data */
			User user = WebServicesModuleUtils.getSecurityBD().getUser(userID);
			Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(user.getAgentCode());

			// Calling from login module
			Collection<UserDST> colUserDST = DatabaseUtil.getAgentLocalTime(agent.getStationCode());
			Collection userCarriers = DatabaseUtil.getCarrierCodes(userID);
			// Setting salesChannel agents and loginType to -1
			UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity(userID, salesChannelId,
					user.getAgentCode(), agent.getStationCode(), null, userID, colUserDST, password, userCarriers,
					user.getDefaultCarrierCode(), user.getAirlineCode(), agent.getAgentTypeCode(), agent.getCurrencyCode(), null,
					null, null, null);

			// Storing data in user session
			aaUserSession.addParameter(AAUserSession.USER_PRINCIPAL, userPrincipal);
			aaUserSession.addParameter(AAUserSession.USER, user);
			aaUserSession.addParameter(AAUserSession.USER_AGENT, agent);
			if (user != null) {
				privilegeKeys = user.getPrivitedgeIDs();
			}
			aaUserSession.addParameter(AAUserSession.USER_PRIVILEGES_KEYS, privilegeKeys);

			userSessions.put(userID, aaUserSession);
		} catch (ModuleException me) {
			log.error("User session initialization failed", me);
			throw new WebservicesException(me);
		}
		log.debug("AASessionManager:: created new user session [userId=" + userID + "]");
		return aaUserSession;
	}

	/**
	 * Get user session; null if no session exists for user yet
	 * 
	 */
	@Override
	public AAUserSession getUserSession(String userID) {
		return (AAUserSession) userSessions.get(userID);
	}

	/**
	 * Removes expired user sessions TODO - test
	 */
	@Override
	public void removeExpiredUserSessions() {
		if (log.isDebugEnabled()) {
			log.debug("Begin user sessions expiration [Timestamp=" + new Date() + "]");
		}
		Collection<String> expiredSessionIDs = new ArrayList<String>();
		AAUserSession session = null;
		String sessionID = null;
		for (Iterator sessionKeysIt = userSessions.keySet().iterator(); sessionKeysIt.hasNext();) {
			sessionID = (String) sessionKeysIt.next();
			session = (AAUserSession) userSessions.get(sessionID);
			if (session.qualifyForExpiration(WebServicesModuleUtils.getWebServicesConfig().getUserSessTimeoutInMills())) {
				expiredSessionIDs.add(sessionID);
			}
		}

		if (expiredSessionIDs.size() > 0) {
			for (String id : expiredSessionIDs) {
				if (log.isDebugEnabled()) {
					log.debug("Removing expired session [SessionID=" + id + "]");
				}

				userSessions.remove(id);
			}
		}

		log.debug("End user sessions expiration [Timestamp=" + new Date() + "]");
	}

	@Override
	public void flushUserSession(AAUserSession aaUserSession) {
		// Not required for In-Memory store
	}

}
