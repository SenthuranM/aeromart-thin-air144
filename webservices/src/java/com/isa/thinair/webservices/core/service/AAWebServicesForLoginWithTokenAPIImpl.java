package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ae.isa.api.loginwithtoken.AAAPILoginWithTokenRQ;
import ae.isa.api.loginwithtoken.AAAPILoginWithTokenRS;
import com.isa.thinair.webservices.api.service.AAWebServicesForLoginWithTokenAPI;
import com.isa.thinair.webservices.core.bl.AACustomerInfoByTokenAPIBL;

@WebService(name = "AAWebServicesForLoginWithTokenAPIImpl", targetNamespace = "http://www.isa.ae/api/loginWithToken",
		endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForLoginWithTokenAPI")
public class AAWebServicesForLoginWithTokenAPIImpl implements AAWebServicesForLoginWithTokenAPI {

	private final Log log = LogFactory.getLog(AAWebServicesForLoginWithTokenAPIImpl.class);

	public AAAPILoginWithTokenRS loginWithToken(AAAPILoginWithTokenRQ userToken) {
		AAAPILoginWithTokenRS customerRS;
		if (log.isDebugEnabled()) {
			log.debug("BEGIN ---- loginWithToken(AAAPILoginWithTokenRQ)");
		}

		customerRS = AACustomerInfoByTokenAPIBL.getCustomerData(userToken.getUserToken());
		if (log.isDebugEnabled()) {
			log.debug("END ---- loginWithToken(AAAPILoginWithTokenRQ)");
		}
		return customerRS;
	}
}
