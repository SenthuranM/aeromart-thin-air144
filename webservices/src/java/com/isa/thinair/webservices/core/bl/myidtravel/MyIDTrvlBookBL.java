package com.isa.thinair.webservices.core.bl.myidtravel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ.Fulfillment;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialRemarks.SpecialRemark;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.BookInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIdTravelBookingContactDetails;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlBookBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlBookBL.class);

	public static MyIDTravelResAdapterBookingResponse execute(MyIDTravelResAdapterBookingRequest myIDBookReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN book(MyIDTravelResAdapterBookingRequest)");
		}

		MyIDTravelResAdapterBookingResponse myIDBookRes = null;
		try {
			myIDBookRes = book(myIDBookReq);
		} catch (Exception ex) {
			log.error("book(MyIDTravelResAdapterBookingRequest) failed.", ex);
			if (myIDBookRes == null) {
				myIDBookRes = new MyIDTravelResAdapterBookingResponse();
				OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
				otaAirBookRS.setEchoToken(myIDBookReq.getOTAAirBookRQ().getEchoToken());
				myIDBookRes.setOTAAirBookRS(otaAirBookRS);
			}
			if (myIDBookRes.getOTAAirBookRS().getErrors() == null)
				myIDBookRes.getOTAAirBookRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDBookRes.getOTAAirBookRS().getErrors().getError(), myIDBookRes
					.getOTAAirBookRS().getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END book(MyIDTravelResAdapterBookingRequest)");
		return myIDBookRes;
	}

	private static MyIDTravelResAdapterBookingResponse book(MyIDTravelResAdapterBookingRequest myIDBookReq)
			throws ModuleException, WebservicesException {

		MyIDTravelResAdapterBookingResponse myIDBookRes = new MyIDTravelResAdapterBookingResponse();
		myIDBookRes.setEmployeeData(myIDBookReq.getEmployeeData());
		OTAAirBookRQ otaAirBookRQ = myIDBookReq.getOTAAirBookRQ();

		// Get onhold PNR and validate request parameters

		if (otaAirBookRQ.getBookingReferenceID() != null) {

			String pnr = otaAirBookRQ.getBookingReferenceID().getID();
			Reservation reservation = MyIDTrvlCommonUtil.getReservation(pnr);

			// We are not supporting listing for existing tickets without a
			// payment
			if (otaAirBookRQ.getFulfillment() == null) {
				CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
				WebServicesModuleUtils.getReservationBD().cancelReservation(pnr, customChargesTO, reservation.getVersion(),
						false, false, false, null, false, null, GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), null);
				log.error("Listing for existing ticket not allowed - Hence cancelling onhold PNR - " + pnr);
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Listing for existing tickets not allowed");
			}

			if (!validateBookReqWithSegSell(reservation, otaAirBookRQ)) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_PNR_NOT_FOUND,
						"Segment sell, booking request information not matching");
			}

			TrackInfoDTO trackInfo = MyIDTrvlCommonUtil.getTrackInfo();

			// Update contact info received
			updateContactInfo(reservation, myIDBookReq.getBookingContactDetails(), trackInfo);

			reservation = MyIDTrvlCommonUtil.getReservation(pnr);

			if (otaAirBookRQ.getTravelerInfo().getSpecialReqDetails() != null
					&& otaAirBookRQ.getTravelerInfo().getSpecialReqDetails().size() == 1) {
				updateRemarks(reservation, otaAirBookRQ.getTravelerInfo().getSpecialReqDetails().get(0).getSpecialRemarks()
						.getSpecialRemark(), trackInfo);
			}

			// Load PNR again for payment
			reservation = MyIDTrvlCommonUtil.getReservation(pnr);
			doPayment(reservation, otaAirBookRQ.getFulfillment(), trackInfo);

			// Send itinerary
			sendItinerary(reservation);

			OTAReadRQ readRQ = MyIDTrvlCommonUtil.generateReadRQ(pnr, otaAirBookRQ.getPOS(), otaAirBookRQ.getSequenceNmbr(),
					otaAirBookRQ.getEchoToken());

			OTAAirBookRS otaAirBookRS = MyIDTrvlCommonUtil.generateOTAAirBookRS(readRQ, false, false, false);
			myIDBookRes.setOTAAirBookRS(otaAirBookRS);

		} else {
			throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_PNR_NOT_FOUND,
					"Segment sell booking reference not set");
		}

		return myIDBookRes;

	}

	private static void updateContactInfo(Reservation reservation, MyIdTravelBookingContactDetails contactInfo,
			TrackInfoDTO trackInfo) throws ModuleException {

		// Phone number formats are expected to be in xxxx-xxx-xxxxxx
		ReservationContactInfo resContactInfo = reservation.getContactInfo();

		resContactInfo.setEmail(contactInfo.getEmailAddress());
		resContactInfo.setMobileNo(contactInfo.getLeaveNumber());
		resContactInfo.setPhoneNo(contactInfo.getOfficeNumber());
		resContactInfo.setEmgnPhoneNo(contactInfo.getPrivateNumber());

		WebServicesModuleUtils.getReservationBD().updateContactInfo(reservation.getPnr(), reservation.getVersion(),
				resContactInfo, trackInfo);
	}

	private static void updateRemarks(Reservation reservation, List<SpecialRemark> specialRemarks, TrackInfoDTO trackInfo)
			throws ModuleException {

		String reservationRemark = null;
		Map<String, String> paxRemarkMap = new HashMap<String, String>();

		for (SpecialRemark specialRemark : specialRemarks) {
			if (WebservicesConstants.MyIDTravelConstants.REMARK_TYPE.equals(specialRemark.getRemarkType())) {
				if (specialRemark.getTravelerRefNumber() == null || specialRemark.getTravelerRefNumber().size() == 0) {
					reservationRemark = specialRemark.getText();
				} else {
					paxRemarkMap.put(specialRemark.getTravelerRefNumber().get(0).getRPH(), specialRemark.getText());
				}
			}
		}

		reservation.setUserNote(reservationRemark);

		Set<ReservationPax> passegers = reservation.getPassengers();
		for (ReservationPax passenger : passegers) {
			ReservationPaxAdditionalInfo addInfo = passenger.getPaxAdditionalInfo();
			addInfo.setIdCategory(paxRemarkMap.get(passenger.getPaxSequence().toString()));
			passenger.setPaxAdditionalInfo(addInfo);
		}

		WebServicesModuleUtils.getReservationBD().updateReservation(reservation, trackInfo, false, false, null,null);

	}

	private static void doPayment(Reservation reservation, Fulfillment fullfillment, TrackInfoDTO trackInfo)
			throws NumberFormatException, ModuleException, WebservicesException {

		// Balance payment with CC
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		PaymentAssembler specifiedPayment = new PaymentAssembler();

		Map<String, Integer> cardAlphaMap = SelectListGenerator.creditCardAlphaCodeMap();
		List<PaymentDetailType> paymentDetails = fullfillment.getPaymentDetails().getPaymentDetail();

		if (paymentDetails != null && paymentDetails.size() > 0) {

			for (PaymentDetailType paymentDetail : paymentDetails) {

				String cardCode = paymentDetail.getPaymentCard().getCardCode();
				Integer cardId = null;
				if (cardCode != null) {
					cardId = cardAlphaMap.get(cardCode);
				}
				if (cardId == null) {
					throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_INVALID_CREDIT_CARD,
							"Invalid credit card type:" + cardCode);
				}

				paymentDetail.getPaymentCard().setCardType(cardId.toString());

			}
		}

		OTAUtils.extractReservationPayments(fullfillment.getPaymentDetails().getPaymentDetail(), specifiedPayment,
				AppSysParamsUtil.getBaseCurrency(), exchangeRateProxy);

		IPassenger passenger = new PassengerAssembler(null);

		BigDecimal calculatedPaymentAmount = ReservationUtil.getTotalBalToPay(reservation);

		ReservationPax infantPax = null;
		if (specifiedPayment.getPayments().size() == 1) {
			Object resPayment = specifiedPayment.getPayments().iterator().next();
			if (resPayment instanceof CardPaymentInfo) {

				int totalAdultChildCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
				
				ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(calculatedPaymentAmount, totalAdultChildCount,
						reservation.getSegmentsView().size(), ChargeRateOperationType.MAKE_ONLY);
				
				ExternalChgDTO jnOtherCC = MyIDTrvlCommonUtil.getJNOtherTax(reservation, ccCharge);
				
				boolean jnOtherCCAvailable = jnOtherCC != null && AccelAeroCalculator.isGreaterThan(jnOtherCC.getAmount(), AccelAeroCalculator.getDefaultBigDecimalZero());
			
				BigDecimal ccFee = ccCharge.getAmount();
				BigDecimal jnOtherCCFee = jnOtherCCAvailable?jnOtherCC.getAmount():AccelAeroCalculator.getDefaultBigDecimalZero();
						
				calculatedPaymentAmount = AccelAeroCalculator.add(calculatedPaymentAmount, ccFee,jnOtherCCFee);
				if (calculatedPaymentAmount.doubleValue() != specifiedPayment.getTotalPayAmount().doubleValue()) {
					// Specified payment does not match expected
					// payments
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT, "Expected ["
							+ calculatedPaymentAmount + "] received [" + specifiedPayment.getTotalPayAmount() + "]");
				}

				Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
				extChgMap.put(ccCharge.getExternalChargesEnum(), ccCharge);
				
				if(jnOtherCCAvailable) {
					extChgMap.put(jnOtherCC.getExternalChargesEnum(), jnOtherCC);
				}

				ExternalChargesMediator chgMediator = new ExternalChargesMediator(null, extChgMap, true, true);
				chgMediator.setCalculateJNTaxForCCCharge(true);
				LinkedList perPaxChgs = chgMediator.getExternalChargesForABalancePayment(totalAdultChildCount, 0);

				CardPaymentInfo resCardPayment = (CardPaymentInfo) resPayment;

				Collection<ReservationPax> paxes = reservation.getPassengers();

				for (ReservationPax pax : paxes) {

					BigDecimal expectedPaxPaymentAmount = pax.getTotalAvailableBalance();
					// If it's an adult
					if (ReservationApiUtils.isAdultType(pax)) {
						if (!reservation.isInfantPaymentRecordedWithInfant() && pax.getInfants() != null
								&& !pax.getInfants().isEmpty()) {// Parent pax
							// one and only one infant per parent exists
							infantPax = (ReservationPax) pax.getInfants().iterator().next();
							// Add infant charges
							expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
									infantPax.getTotalAvailableBalance());
						}

						IPayment paxPayment = new PaymentAssembler();
						Object extCharges = perPaxChgs.pop();
						if (extCharges != null) {
							paxPayment.addExternalCharges((Collection<ExternalChgDTO>) extCharges);
						}
						paxPayment.addCardPayment(resCardPayment.getType(), resCardPayment.getEDate(), resCardPayment.getNo(),
								resCardPayment.getName(), resCardPayment.getAddress(), resCardPayment.getSecurityCode(),
								expectedPaxPaymentAmount, resCardPayment.getAppIndicator(), resCardPayment.getTnxMode(), null,
								resCardPayment.getIpgIdentificationParamsDTO(), null, resCardPayment.getPayCurrencyDTO(), null,
								null, null, null);
						passenger.addPassengerPayments(pax.getPnrPaxId(), paxPayment);
						// If it's a child
					} else if (ReservationApiUtils.isChildType(pax)) {
						IPayment paxPayment = new PaymentAssembler();
						Object extCharges = perPaxChgs.pop();
						if (extCharges != null) {
							paxPayment.addExternalCharges((Collection<ExternalChgDTO>) extCharges);
						}
						paxPayment.addCardPayment(resCardPayment.getType(), resCardPayment.getEDate(), resCardPayment.getNo(),
								resCardPayment.getName(), resCardPayment.getAddress(), resCardPayment.getSecurityCode(),
								expectedPaxPaymentAmount, resCardPayment.getAppIndicator(), resCardPayment.getTnxMode(), null,
								resCardPayment.getIpgIdentificationParamsDTO(), null, resCardPayment.getPayCurrencyDTO(), null,
								null, null, null);

						passenger.addPassengerPayments(pax.getPnrPaxId(), paxPayment);
					} else if (reservation.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(pax)) {
						IPayment paxPayment = new PaymentAssembler();
//						Object extCharges = perPaxChgs.pop();
//						if (extCharges != null) {
//							paxPayment.addExternalCharges((Collection<ExternalChgDTO>) extCharges);
//						}
						paxPayment.addCardPayment(resCardPayment.getType(), resCardPayment.getEDate(), resCardPayment.getNo(),
								resCardPayment.getName(), resCardPayment.getAddress(), resCardPayment.getSecurityCode(),
								expectedPaxPaymentAmount, resCardPayment.getAppIndicator(), resCardPayment.getTnxMode(), null,
								resCardPayment.getIpgIdentificationParamsDTO(), null, resCardPayment.getPayCurrencyDTO(), null,
								null, null, null);

						passenger.addPassengerPayments(pax.getPnrPaxId(), paxPayment);
					}
				}
			} else {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
						"Only cc payment supported");
			}
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
					"Only single payment type in one request supported");
		}

		WebServicesModuleUtils.getReservationBD().updateReservationForPayment(reservation.getPnr(), passenger, false, false,
				reservation.getVersion(), trackInfo, false, false, true, false, true, false, null, false, null);
	}

	@SuppressWarnings("unchecked")
	private static void sendItinerary(Reservation reservation) throws ModuleException {

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
	
		if (AuthorizationUtil.hasPrivileges(privilegeKeys, PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_CNFBOOKING)) {
			LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(reservation.getPnr(), false, null, false, null,
					null, null);
			CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
			contactInfo.setEmail(reservation.getContactInfo().getEmail());
			contactInfo.setPreferredLanguage(reservation.getContactInfo().getPreferredLanguage());
			BookInterlineUtil.sendEmailItenary(pnrModesDTO, contactInfo,
					ThreadLocalData.getCurrentUserPrincipal(), false);
		}
	}

	private static boolean validateBookReqWithSegSell(Reservation res, OTAAirBookRQ otaAirBookRQ) throws ModuleException {

		boolean validRequest = true;

		Collection<ReservationSegmentDTO> segSellSegments = res.getSegmentsView();

		// Validate Segments
		List<OriginDestinationOptionType> ondOptions = otaAirBookRQ.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption();

		Collection<FlightSegmentDTO> reqSegments = new ArrayList<FlightSegmentDTO>();
		for (OriginDestinationOptionType ondOption : ondOptions) {
			for (BookFlightSegmentType bookFlightSegment : ondOption.getFlightSegment()) {
				FlightSegmentDTO flightSeg = new FlightSegmentDTO();
				flightSeg.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				flightSeg.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				flightSeg.setFlightNumber(bookFlightSegment.getOperatingAirline().getCode()
						+ bookFlightSegment.getOperatingAirline().getFlightNumber());
				flightSeg.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				flightSeg.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());
				reqSegments.add(flightSeg);
			}
		}

		Collection<FlightSegmentDTO> validSegments = WebServicesModuleUtils.getFlightBD().getFilledFlightSegmentDTOs(reqSegments);

		if (segSellSegments.size() != validSegments.size()) {
			// segments count different
			validRequest = false;
		} else {
			for (ReservationSegmentDTO segSellSeg : segSellSegments) {
				boolean segMatched = false;
				for (FlightSegmentDTO validSegment : validSegments) {
					if (segSellSeg.getFlightSegId().equals(validSegment.getSegmentId())) {
						segMatched = true;
						break;
					}

				}

				if (!segMatched) {
					// segments not matching
					validRequest = false;
					break;
				}
			}
		}

		return validRequest;
	}

}
