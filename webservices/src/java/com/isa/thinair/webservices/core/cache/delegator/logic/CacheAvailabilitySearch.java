package com.isa.thinair.webservices.core.cache.delegator.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.delegator.AbstractCacheTemplate;
import com.isa.thinair.webservices.core.cache.delegator.CacheResponse;
import com.isa.thinair.webservices.core.cache.delegator.auditor.impl.AvailabilitySearchAuditor;
import com.isa.thinair.webservices.core.cache.delegator.keygen.AvailabilityRequestKey;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.Operation;
import com.isa.thinair.webservices.core.interlineUtil.AvailabilitySearchInterlineUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.util.ExceptionUtil;

public class CacheAvailabilitySearch extends AbstractCacheTemplate<OTAAirAvailRS, OTAAirAvailRQ> {

	private static final List<String> CACHEABLE_ERROR_CODES = setCacheableErrorCodes();

	@Override
	public OTAAirAvailRS emptyResponse() {
		OTAAirAvailRS response = new OTAAirAvailRS();
		response.setErrors(new ErrorsType());
		response.setWarnings(new WarningsType());
		return response;
	}

	@Override
	public OTAAirAvailRS execute(OTAAirAvailRQ request) throws ModuleException, WebservicesException {

		OTAAirAvailRS response = null;
		try {
			response = new AvailabilitySearchInterlineUtil().getAvailability(request);

		} catch (WebservicesException we) {

			boolean cacheable = isCacheableException(we);
			if (cacheable) {
				response = emptyResponse();
				ExceptionUtil.addOTAErrrorsAndWarnings(response.getErrors().getError(), null, we);
				return response;
			}

			throw we;
		}
		return response;
	}

	@Override
	public String generateCacheStoreKey(OTAAirAvailRQ request) throws ModuleException, WebservicesException {
		return new AvailabilityRequestKey(request, getCacheOperationCode()).generateKey();
	}

	@Override
	public String getAudit(OTAAirAvailRQ request) {
		AvailabilitySearchAuditor auditor = new AvailabilitySearchAuditor(request);
		return auditor.getAudit(Operation.AVAILABILITY_SEARCH);
	}

	@Override
	public OTAAirAvailRS cachePostProcess(CacheResponse<OTAAirAvailRS> cacheResponse, OTAAirAvailRQ request)
			throws ModuleException, WebservicesException {
		ITransaction currentTransaction = ThreadLocalData.getCurrentTransaction();
		currentTransaction.setAllParameters(new HashMap<>(cacheResponse.getCacheTransactionData()));
		return cacheResponse.getOtaCacheResponse();
	}

	private boolean isCacheableException(WebservicesException we) {
		HashMap<String, String> otaErrorCodes = we.getOtaErrorCodes();
		if (otaErrorCodes != null && !otaErrorCodes.isEmpty()) {
			for (String key : otaErrorCodes.keySet()) {
				if (CACHEABLE_ERROR_CODES.contains(key)) {
					return true;
				}
			}
		}
		return false;
	}

	private static List<String> setCacheableErrorCodes() {
		List<String> errorCodes = new ArrayList<String>();
		errorCodes.add(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY);
		errorCodes.add(IOTACodeTables.ErrorCodes_ERR_INVALID_OND);
		return errorCodes;
	}

}
