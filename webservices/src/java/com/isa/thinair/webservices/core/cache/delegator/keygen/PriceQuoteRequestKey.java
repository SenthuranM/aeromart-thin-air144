package com.isa.thinair.webservices.core.cache.delegator.keygen;

import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRQ.FlexiFareSelectionOptions;
import org.opentravel.ota._2003._05.OTAAirPriceRQ.ServiceTaxCriteriaOptions;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests.BaggageRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests.MealRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests.SeatRequest;

import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.CacheKeyParamName;

public class PriceQuoteRequestKey extends OTARequestKeyUtil {

	private OTAAirPriceRQ request;
	private String operationCode;

	public PriceQuoteRequestKey(OTAAirPriceRQ request, String operationCode) {
		this.request = request;
		this.operationCode = operationCode;;
	}

	public String generateKey() throws  WebservicesException {

		validate();		
		StringBuilder key = new StringBuilder();
		
		key.append(getKeyHeader(this.operationCode)).append(moduleSeparator);
		key.append(getPointOfSalesKeyInfo(request.getPOS()));
		key.append(getSpecificAgentKeyInfo());
		key.append(getTravelerQuantityInfo(request.getTravelerInfoSummary()));
		key.append(getServiceTaxKeyInfo());
		key.append(getOriginDestinationInfo());
		key.append(getBundledServiceSelectionKeyInfo());
		key.append(getFlexiSelectionKeyInfo());
		key.append(getSeatSelectionKeyInfo());
		key.append(getMealSelectionKeyInfo());
		key.append(getSSRSelectionKeyInfo());
		key.append(getBaggageSelectionKeyInfo());
		
		return key.toString();
	}

	private String getServiceTaxKeyInfo() {
		
		StringBuilder serviceTaxKey = new StringBuilder();
		
		ServiceTaxCriteriaOptions serviceTaxOptions = request.getServiceTaxCriteriaOptions();
		
		if (serviceTaxOptions != null) {
			appendWithSeparator(serviceTaxKey, serviceTaxOptions.getCountryCode());
			appendWithSeparator(serviceTaxKey, serviceTaxOptions.getStateCode());		
		}
		
		return serviceTaxKey.toString();
		
	}

	private Object getFlexiSelectionKeyInfo() {

		StringBuilder flexiSelectionKey = new StringBuilder();

		if (request.getFlexiFareSelectionOptions() != null) {
			FlexiFareSelectionOptions flexiOptions = request.getFlexiFareSelectionOptions();
			appendKeyValueWithSeparator(flexiSelectionKey, CacheKeyParamName.FLEXI_QUOTE, flexiOptions.getInBoundFlexiSelected());
			appendKeyValueWithSeparator(flexiSelectionKey, CacheKeyParamName.FLEXI_QUOTE, flexiOptions.getOutBoundFlexiSelected());
		}

		return flexiSelectionKey.toString();
	}

	private Object getBundledServiceSelectionKeyInfo() {

		StringBuilder bundledSlectionKey = new StringBuilder();
		OTAAirPriceRQ.BundledServiceSelectionOptions bundledServiceSelection = request.getBundledServiceSelectionOptions();

		if (bundledServiceSelection != null) {
			appendKeyValueWithSeparator(bundledSlectionKey, CacheKeyParamName.BUNDLED,
					bundledServiceSelection.getOutBoundBunldedServiceId());
			appendKeyValueWithSeparator(bundledSlectionKey, CacheKeyParamName.BUNDLED,
					bundledServiceSelection.getInBoundBunldedServiceId());
		}

		return bundledSlectionKey.toString();
	}

	private Object getOriginDestinationInfo() {

		StringBuilder originDestinationKey = new StringBuilder();

		for (OriginDestinationOptionType originDestinationOption : request.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption()) {

			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {

				appendKeyValueWithSeparator(originDestinationKey, CacheKeyParamName.CABIN_CLASS,
						bookFlightSegment.getResCabinClass());
				appendKeyValueWithSeparator(originDestinationKey, CacheKeyParamName.BOOKING_CLASS,
						bookFlightSegment.getResBookDesigCode());

				appendWithSeparator(originDestinationKey, bookFlightSegment.getDepartureAirport().getLocationCode());
				appendWithSeparator(originDestinationKey, bookFlightSegment.getArrivalAirport().getLocationCode());

				appendWithSeparator(originDestinationKey, CalendarUtil
						.formatDateYYYYMMDD(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime()));

				appendWithSeparator(originDestinationKey, bookFlightSegment.getRPH());
			}
		}

		return originDestinationKey.toString();

	}

	private String getSeatSelectionKeyInfo() throws WebservicesException {

		StringBuilder seatSelectionKey = new StringBuilder();

		for (SpecialReqDetailsType specialReqDetailsType : request.getTravelerInfoSummary().getSpecialReqDetails()) {

			SeatRequests seatRequests = specialReqDetailsType.getSeatRequests();

			if (seatRequests != null) {

				for (SeatRequest seatRequest : seatRequests.getSeatRequest()) {	
					String segmentCode = getSegmentCode(seatRequest.getFlightRefNumberRPHList().get(0));
					appendKeyValueWithSeparator(seatSelectionKey, CacheKeyParamName.SEAT, segmentCode + seatRequest.getSeatNumber());
				}
			}
		}

		return seatSelectionKey.toString();
	}

	private String getSegmentCode(String rphNumber) {
		return 	FlightRefNumberUtil.getSegmentCodeFromFlightRPH(rphNumber);
	}

	private String getMealSelectionKeyInfo() {

		StringBuilder mealSelectionKey = new StringBuilder();

		for (SpecialReqDetailsType specialReqDetailsType : request.getTravelerInfoSummary().getSpecialReqDetails()) {

			MealRequests mealRequests = specialReqDetailsType.getMealRequests();

			if (mealRequests != null) {

				for (MealRequest mealRequest : mealRequests.getMealRequest()) {

					int mealQuantity = 1;

					if (mealRequest.getMealQuantity() > 1) {
						mealQuantity = mealRequest.getMealQuantity();
					}
					String segmentCode = getSegmentCode(mealRequest.getFlightRefNumberRPHList().get(0));
					appendKeyValueWithSeparator(mealSelectionKey, CacheKeyParamName.MEAL, segmentCode+ mealRequest.getMealCode() + mealQuantity);

				}
			}

		}

		return mealSelectionKey.toString();
	}

	private String getSSRSelectionKeyInfo() throws WebservicesException {

		StringBuilder ssrSelectionKey = new StringBuilder();

		for (SpecialReqDetailsType specialReqDetailsType : request.getTravelerInfoSummary().getSpecialReqDetails()) {

			SSRRequests ssrRequests = specialReqDetailsType.getSSRRequests();

			if (ssrRequests != null) {

				for (SSRRequest ssrRequest : ssrRequests.getSSRRequest()) {	
					String segmentCode = getSegmentCode(ssrRequest.getFlightRefNumberRPHList().get(0));
					appendKeyValueWithSeparator(ssrSelectionKey, CacheKeyParamName.SSR, segmentCode + ssrRequest.getSsrCode());
				}
			}
		}

		return ssrSelectionKey.toString();
	}

	private String getBaggageSelectionKeyInfo() {

		StringBuilder baggageSelectionKey = new StringBuilder();

		for (SpecialReqDetailsType specialReqDetailsType : request.getTravelerInfoSummary().getSpecialReqDetails()) {

			BaggageRequests baggageRequests = specialReqDetailsType.getBaggageRequests();

			if (baggageRequests != null) {

				for (BaggageRequest baggageRequest : baggageRequests.getBaggageRequest()) {		
					String segmentCode = getSegmentCode(baggageRequest.getFlightRefNumberRPHList().get(0));
					appendKeyValueWithSeparator(baggageSelectionKey, CacheKeyParamName.BAGGAGE, segmentCode + baggageRequest.getBaggageCode());

				}
			}
		}

		return baggageSelectionKey.toString();
	}

	private void validate() throws WebservicesException {

		if (request == null || request.getModifiedSegmentInfo() != null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION, "Cache doesn't support");
		}

		if (request.getTravelerInfoSummary().getSpecialReqDetails() != null) {

			for (SpecialReqDetailsType specialReqDetailsType : request.getTravelerInfoSummary().getSpecialReqDetails()) {

				if (specialReqDetailsType != null && specialReqDetailsType.getInsuranceRequests() != null
						&& specialReqDetailsType.getInsuranceRequests().getInsuranceRequest() != null) {

					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
							"Cache doesn't support");
				}
			}
		}

	}

}
