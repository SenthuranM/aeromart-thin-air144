package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS;

import com.isa.thinair.webservices.core.adaptor.Adaptor;

public class BookingClassAvailAdaptor
		implements
		Adaptor<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail, AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail> {

	@Override
	public
			AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail
			adapt(OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail source) {

		AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail target = new AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail();

		target.setResBookDesigCode(source.getResBookDesigCode());
		target.setResBookDesigQuantity(source.getResBookDesigQuantity());
		target.setResBookDesigStatusCode(source.getResBookDesigStatusCode());
		target.setRPH(source.getRPH());

		return target;
	}

}
