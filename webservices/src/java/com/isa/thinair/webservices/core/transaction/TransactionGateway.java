package com.isa.thinair.webservices.core.transaction;

import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Gatekeepers to the Transactions
 * 
 * Handles Transasction In and Out Operations
 *
 */
public class TransactionGateway {

	private static final Log log = LogFactory.getLog(TransactionGateway.class);

	/**
	 * Handles transaction before service call
	 * 
	 */
	public static class TransactionInHandler {

		public static ITransaction handleTransactionIn(String requestedTnxId, WebServiceTransaction tnxAnnotation)
				throws WebservicesException {
			log.debug("TransactionInHandler::handleTransactionIn called");
			ITransaction existingTnx = null;

			if (requestedTnxId != null && !"".equals(requestedTnxId)) {
				existingTnx = WebServicesModuleUtils.getTrasnsactionStore().getTransaction(requestedTnxId);
			}

			ITransaction requestedTnx = null;
			if (tnxAnnotation != null) {
				TransactionAttributeType transType = tnxAnnotation.value();
				switch (transType) {
				case REQUIRED:
					requestedTnx = handleRequired(existingTnx, requestedTnxId);
					break;
				case MANDATORY:
					requestedTnx = handleMandatory(existingTnx, requestedTnxId);
					break;
				case REQUIRES_NEW:
					requestedTnx = handleRequiresNew(requestedTnxId);
					break;
				default:
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Unsupported Transaction Attribute Found");
				}
			}
			// Bind the transaction to the current thread
			ThreadLocalData.setCurrentTransaction(requestedTnx);
			return requestedTnx;
		}

		/**
		 * Initialize a new transation.
		 * 
		 * @param requestedTnxId
		 * @return
		 * @throws Exception
		 */
		private static ITransaction handleRequiresNew(String requestedTnxId) throws WebservicesException {
			String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
			String userId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.LOGIN_NAME);
			if (log.isDebugEnabled()) {
				try {
					log.debug("Before handleRequiresNew [" + ", Session Id=" + (sessionId != null ? sessionId : "") + ", UserId="
							+ userId + "]");
				} catch (Exception ex) {
					log.error(ex);
				}
			}
			if (requestedTnxId != null && !"".equals(requestedTnxId)) {
				log.error("A service call is made with transaction specified where it should run in new transaction [requestedTnxId="
						+ requestedTnxId + "]");
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Requested Service should be called without Transaction specified [requestedTnxId=" + requestedTnxId
								+ ", SessionId=" + (sessionId != null ? sessionId : "") + ", UserId=" + userId + "]");
			}
			ITransaction txn = WebServicesModuleUtils.getTrasnsactionStore().createNewTransaction();
			txn.setSessionId(sessionId);
			if (log.isDebugEnabled()) {
				try {
					log.debug("After handleRequiresNew [" + ", Session Id=" + (sessionId != null ? sessionId : "") + ", UserId="
							+ userId + ", TransactionId=" + txn.getId() + "]");
				} catch (Exception ex) {
					log.error(ex);
				}
			}
			return txn;
		}

		/**
		 * Use existing transaction matching the requested. Requested transaction must already exist.
		 * 
		 * @param existingTnx
		 * @param requestedTnxId
		 * @return
		 * @throws Exception
		 */
		private static ITransaction handleMandatory(ITransaction existingTnx, String requestedTnxId) throws WebservicesException {
			String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
			String userId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.LOGIN_NAME);

			if (requestedTnxId == null || "".equals(requestedTnxId)) {
				log.error("A service call is made without transaction specified where it should run in existing transaction [requestedTnxId="
						+ requestedTnxId + ", SessionId=" + (sessionId != null ? sessionId : "") + ", UserId=" + userId + "]");
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Requested Service should be called with Transaction specified [requestedTnxId" + requestedTnxId
								+ ", SessionId=" + (sessionId != null ? sessionId : "") + ", UserId=" + userId + "]");
			} else if (existingTnx == null) {
				log.error("Requested transaction does not exists [requestedTnxId=" + requestedTnxId + ", SessionId="
						+ (sessionId != null ? sessionId : "") + ", UserId=" + userId + "]");
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_TRANSACTION_INVALID_OR_EXPIRED,
						"Transaction specified in request is invalid or already expired [requestedTnxId" + requestedTnxId
								+ ", SessionId=" + (sessionId != null ? sessionId : "") + ", UserId=" + userId + "]");
			}
			return existingTnx;
		}

		/**
		 * If requested transaction exists, use it, otherwise intialize a new transaction.
		 * 
		 * @param existingTnx
		 * @param requestedTnxId
		 * @return
		 * @throws Exception
		 */
		private static ITransaction handleRequired(ITransaction existingTnx, String requestedTnxId) throws WebservicesException {
			if (existingTnx == null) {
				return handleRequiresNew(requestedTnxId);
			}
			return existingTnx;
		}
	}

	/**
	 * Handle Transaction after service call.
	 * 
	 * @author Nasly
	 * 
	 */
	public static class TransactionOutHandler {

		public static void handleTransactionOut(WebServiceTransaction aaTnxAnnontation) {
			log.debug("TransactionOutHandler::handleTransactionOut called");
			try {
				ITransaction currTnx = ThreadLocalData.getCurrentTransaction();

				try {
					WebServicesModuleUtils.getTrasnsactionStore().flushTransaction(currTnx);
				} catch (Exception ex) {
					log.error("Error in flusing transaction " + ex);
				}

				if (currTnx != null && aaTnxAnnontation != null) {
					if (aaTnxAnnontation.boundry() == WebServiceTransaction.TransactionBoundry.FINISH) {
						try {
							WebServicesModuleUtils.getTrasnsactionStore().removeTransaction(currTnx.getId());
						} catch (Exception e) {
							log.error("Error in removing transaction at TransactionOutHandler  " + e);
						}
					}
				}

			} finally {
				ThreadLocalData.unsetCurrentTransaction();
			}
		}
	}

}
