package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxBaseCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.dto.PAXSummeryDTO;
import com.isa.thinair.webplatform.api.dto.ResBalancesSummaryDTO;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAddressType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAdminInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirTravelerType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAlterationBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACnxModAddONDBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAContactInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACountryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACurrencyAmountType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAEmergencyContactType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAExternalPayTxType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPersonNameType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAResAccSummaryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATelephoneType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelerAccSummaryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelerCnxModAddResBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelerInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelersAccSummaryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATravelersCnxModAddResBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNoteType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNotesType;

/**
 * For instantiating OTA Extensions/For extracting info from OTA Extensions
 */
public class ExtensionsUtil {

	/**
	 * Transform AA {@link ReservationContactInfo} to WS {@link ContactInfoType}
	 * 
	 * @param AA
	 *            {@link ReservationContactInfo}
	 * @return {@link ContactInfoType}
	 * @throws ModuleException
	 */
	public static AAContactInfoType transformToWSContactInfo(ReservationContactInfo aaContactInfo) throws ModuleException {
		AAContactInfoType wsContactInfo = new AAContactInfoType();

		if (aaContactInfo.getCustomerId() != null) {
			wsContactInfo.setProfileRef(aaContactInfo.getCustomerId().toString());
		}

		AAPersonNameType name = new AAPersonNameType();
		wsContactInfo.setPersonName(name);
		if (!CommonUtil.isEmpty(aaContactInfo.getTitle())) {
			name.setTitle(aaContactInfo.getTitle());
		}

		name.setFirstName(aaContactInfo.getFirstName());
		name.setLastName(aaContactInfo.getLastName());

		if (!CommonUtil.isEmpty(aaContactInfo.getPhoneNo()) && !"--".equals(aaContactInfo.getPhoneNo())) {
			AATelephoneType telephone = new AATelephoneType();
			wsContactInfo.setTelephone(telephone);
			String[] phoneNoArr = StringUtils.split(aaContactInfo.getPhoneNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			telephone.setPhoneNumber(phoneNoArr.length == 3 ? phoneNoArr[2] : (phoneNoArr.length == 2
					? phoneNoArr[1]
					: phoneNoArr[0]));
			telephone.setAreaCode(phoneNoArr.length == 3 ? phoneNoArr[1] : (phoneNoArr.length == 2 ? phoneNoArr[0] : null));
			telephone.setCountryCode(phoneNoArr.length == 3 ? phoneNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getMobileNo()) && !"--".equals(aaContactInfo.getMobileNo())) {
			AATelephoneType mobile = new AATelephoneType();
			wsContactInfo.setMobile(mobile);
			String[] mobileNoArr = StringUtils.split(aaContactInfo.getMobileNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			mobile.setPhoneNumber(mobileNoArr.length == 3 ? mobileNoArr[2] : (mobileNoArr.length == 2
					? mobileNoArr[1]
					: mobileNoArr[0]));
			mobile.setAreaCode(mobileNoArr.length == 3 ? mobileNoArr[1] : (mobileNoArr.length == 2 ? mobileNoArr[0] : null));
			mobile.setCountryCode(mobileNoArr.length == 3 ? mobileNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getFax()) && !"--".equals(aaContactInfo.getFax())) {
			AATelephoneType fax = new AATelephoneType();
			wsContactInfo.setFax(fax);
			String[] faxNoArr = StringUtils.split(aaContactInfo.getFax(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			fax.setPhoneNumber(faxNoArr.length == 3 ? faxNoArr[2] : (faxNoArr.length == 2 ? faxNoArr[1] : faxNoArr[0]));
			fax.setAreaCode(faxNoArr.length == 3 ? faxNoArr[1] : (faxNoArr.length == 2 ? faxNoArr[0] : null));
			fax.setCountryCode(faxNoArr.length == 3 ? faxNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getEmail())) {
			wsContactInfo.setEmail(aaContactInfo.getEmail());
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getCountryCode()) || !CommonUtil.isEmpty(aaContactInfo.getStreetAddress1())
				|| !CommonUtil.isEmpty(aaContactInfo.getStreetAddress2()) || !CommonUtil.isEmpty(aaContactInfo.getCity())
				|| !CommonUtil.isEmpty(aaContactInfo.getState())) {

			AAAddressType address = new AAAddressType();
			wsContactInfo.setAddress(address);

			if (aaContactInfo.getStreetAddress1() != null) {
				address.setAddressLine1(aaContactInfo.getStreetAddress1());
			}

			if (aaContactInfo.getStreetAddress2() != null) {
				address.setAddressLine2(aaContactInfo.getStreetAddress2());
			}

			if (aaContactInfo.getCity() != null) {
				address.setCityName(aaContactInfo.getCity());
			}

			if (aaContactInfo.getState() != null) {
				address.setStateProvinceName(aaContactInfo.getState());
			}

			if (aaContactInfo.getCountryCode() != null) {
				AACountryType country = new AACountryType();
				address.setCountryName(country);
				country.setCountryName(WebServicesModuleUtils.getCommonMasterBD()
						.getCountryByName(aaContactInfo.getCountryCode()).getCountryName());
				country.setCountryCode(aaContactInfo.getCountryCode());
			}

			address.setZipCode(aaContactInfo.getZipCode());
		}

		String prefLang = (aaContactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : aaContactInfo
				.getPreferredLanguage();
		wsContactInfo.setPreferredLanguage(prefLang);

		// Set Emergency Contact Information
		if (!CommonUtil.isEmpty(aaContactInfo.getEmgnFirstName()) || !CommonUtil.isEmpty(aaContactInfo.getEmgnLastName())
				|| !CommonUtil.isEmpty(aaContactInfo.getEmgnPhoneNo())) {
			AAEmergencyContactType emergencyContact = new AAEmergencyContactType();

			AAPersonNameType personeName = new AAPersonNameType();
			personeName.setTitle(aaContactInfo.getEmgnTitle());
			personeName.setFirstName(aaContactInfo.getEmgnFirstName());
			personeName.setLastName(aaContactInfo.getEmgnLastName());

			emergencyContact.setPersonName(personeName);

			if (!CommonUtil.isEmpty(aaContactInfo.getEmgnPhoneNo()) && !"--".equals(aaContactInfo.getEmgnPhoneNo())) {
				AATelephoneType tel = new AATelephoneType();
				String[] telArr = StringUtils.split(aaContactInfo.getEmgnPhoneNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
				tel.setPhoneNumber(telArr.length == 3 ? telArr[2] : (telArr.length == 2 ? telArr[1] : telArr[0]));
				tel.setAreaCode(telArr.length == 3 ? telArr[1] : (telArr.length == 2 ? telArr[0] : null));
				tel.setCountryCode(telArr.length == 3 ? telArr[0] : null);

				emergencyContact.setTelephone(tel);
			}

			emergencyContact.setEmail(aaContactInfo.getEmgnEmail());

			wsContactInfo.setEmergencyContact(emergencyContact);
		}

		return wsContactInfo;
	}

	/**
	 * Transform AA {@link CommonReservationContactInfo} to WS {@link ContactInfoType}
	 * 
	 * @param AA
	 *            {@link CommonReservationContactInfo}
	 * @return {@link ContactInfoType}
	 * @throws ModuleException
	 */
	public static AAContactInfoType transformToWSContactInfo(CommonReservationContactInfo aaContactInfo) throws ModuleException {
		AAContactInfoType wsContactInfo = new AAContactInfoType();

		if (aaContactInfo.getCustomerId() != null) {
			wsContactInfo.setProfileRef(aaContactInfo.getCustomerId().toString());
		}

		AAPersonNameType name = new AAPersonNameType();
		wsContactInfo.setPersonName(name);
		if (!CommonUtil.isEmpty(aaContactInfo.getTitle())) {
			name.setTitle(aaContactInfo.getTitle());
		}

		name.setFirstName(aaContactInfo.getFirstName());
		name.setLastName(aaContactInfo.getLastName());

		if (!CommonUtil.isEmpty(aaContactInfo.getPhoneNo()) && !"--".equals(aaContactInfo.getPhoneNo())) {
			AATelephoneType telephone = new AATelephoneType();
			wsContactInfo.setTelephone(telephone);
			String[] phoneNoArr = StringUtils.split(aaContactInfo.getPhoneNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			telephone.setPhoneNumber(phoneNoArr.length == 3 ? phoneNoArr[2] : (phoneNoArr.length == 2
					? phoneNoArr[1]
					: phoneNoArr[0]));
			telephone.setAreaCode(phoneNoArr.length == 3 ? phoneNoArr[1] : (phoneNoArr.length == 2 ? phoneNoArr[0] : null));
			telephone.setCountryCode(phoneNoArr.length == 3 ? phoneNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getMobileNo()) && !"--".equals(aaContactInfo.getMobileNo())) {
			AATelephoneType mobile = new AATelephoneType();
			wsContactInfo.setMobile(mobile);
			String[] mobileNoArr = StringUtils.split(aaContactInfo.getMobileNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			mobile.setPhoneNumber(mobileNoArr.length == 3 ? mobileNoArr[2] : (mobileNoArr.length == 2
					? mobileNoArr[1]
					: mobileNoArr[0]));
			mobile.setAreaCode(mobileNoArr.length == 3 ? mobileNoArr[1] : (mobileNoArr.length == 2 ? mobileNoArr[0] : null));
			mobile.setCountryCode(mobileNoArr.length == 3 ? mobileNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getFax()) && !"--".equals(aaContactInfo.getFax())) {
			AATelephoneType fax = new AATelephoneType();
			wsContactInfo.setFax(fax);
			String[] faxNoArr = StringUtils.split(aaContactInfo.getFax(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
			fax.setPhoneNumber(faxNoArr.length == 3 ? faxNoArr[2] : (faxNoArr.length == 2 ? faxNoArr[1] : faxNoArr[0]));
			fax.setAreaCode(faxNoArr.length == 3 ? faxNoArr[1] : (faxNoArr.length == 2 ? faxNoArr[0] : null));
			fax.setCountryCode(faxNoArr.length == 3 ? faxNoArr[0] : null);
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getEmail())) {
			wsContactInfo.setEmail(aaContactInfo.getEmail());
		}

		if (!CommonUtil.isEmpty(aaContactInfo.getCountryCode()) || !CommonUtil.isEmpty(aaContactInfo.getStreetAddress1())
				|| !CommonUtil.isEmpty(aaContactInfo.getStreetAddress2()) || !CommonUtil.isEmpty(aaContactInfo.getCity())
				|| !CommonUtil.isEmpty(aaContactInfo.getState())) {

			AAAddressType address = new AAAddressType();
			wsContactInfo.setAddress(address);

			if (aaContactInfo.getStreetAddress1() != null) {
				address.setAddressLine1(aaContactInfo.getStreetAddress1());
			}

			if (aaContactInfo.getStreetAddress2() != null) {
				address.setAddressLine2(aaContactInfo.getStreetAddress2());
			}

			if (aaContactInfo.getCity() != null) {
				address.setCityName(aaContactInfo.getCity());
			}

			if (aaContactInfo.getState() != null) {
				address.setStateProvinceName(WebServicesModuleUtils.getCommonMasterBD().getState(aaContactInfo.getState())
						.getStateName());
			}

			if (aaContactInfo.getCountryCode() != null) {
				AACountryType country = new AACountryType();
				address.setCountryName(country);
				country.setCountryName(aaContactInfo.getCountryCode());
				country.setCountryCode(aaContactInfo.getCountryCode());
			}
			address.setStateCode(aaContactInfo.getState());
			address.setZipCode(aaContactInfo.getZipCode());
		}

		String prefLang = (aaContactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : aaContactInfo
				.getPreferredLanguage();
		wsContactInfo.setPreferredLanguage(prefLang);

		wsContactInfo.setTaxRegNo(aaContactInfo.getTaxRegNo());

		// Set Emergency Contact Information
		if (!CommonUtil.isEmpty(aaContactInfo.getEmgnFirstName()) || !CommonUtil.isEmpty(aaContactInfo.getEmgnLastName())
				|| !CommonUtil.isEmpty(aaContactInfo.getEmgnPhoneNo())) {
			AAEmergencyContactType emergencyContact = new AAEmergencyContactType();

			AAPersonNameType personeName = new AAPersonNameType();
			personeName.setTitle(aaContactInfo.getEmgnTitle());
			personeName.setFirstName(aaContactInfo.getEmgnFirstName());
			personeName.setLastName(aaContactInfo.getEmgnLastName());

			emergencyContact.setPersonName(personeName);

			if (!CommonUtil.isEmpty(aaContactInfo.getEmgnPhoneNo()) && !"--".equals(aaContactInfo.getEmgnPhoneNo())) {
				AATelephoneType tel = new AATelephoneType();
				String[] telArr = StringUtils.split(aaContactInfo.getEmgnPhoneNo(), WebservicesConstants.PHONE_NUMBER_SEPERATOR);
				tel.setPhoneNumber(telArr.length == 3 ? telArr[2] : (telArr.length == 2 ? telArr[1] : telArr[0]));
				tel.setAreaCode(telArr.length == 3 ? telArr[1] : (telArr.length == 2 ? telArr[0] : null));
				tel.setCountryCode(telArr.length == 3 ? telArr[0] : null);

				emergencyContact.setTelephone(tel);
			}

			emergencyContact.setEmail(aaContactInfo.getEmgnEmail());

			wsContactInfo.setEmergencyContact(emergencyContact);
		}

		return wsContactInfo;
	}

	/**
	 * Transforms WS {@link ContactInfoType} to AA {@link ReservationContactInfo}
	 * 
	 * @return AA {@link ReservationContactInfo}
	 * @throws WebservicesException
	 */
	public static ReservationContactInfo transformToAAContactInfo(AAContactInfoType wsContactInfo) throws WebservicesException {
		ReservationContactInfo aaContactInfo = new ReservationContactInfo();

		if (wsContactInfo.getPersonName() != null) {
			aaContactInfo.setTitle(wsContactInfo.getPersonName().getTitle());
			aaContactInfo.setFirstName(wsContactInfo.getPersonName().getFirstName());
			aaContactInfo.setLastName(wsContactInfo.getPersonName().getLastName());
		}

		AAAddressType address = wsContactInfo.getAddress();
		if (address != null) {
			aaContactInfo.setCountryCode((address.getCountryName() != null) ? address.getCountryName().getCountryCode() : "");
			aaContactInfo.setCity(address.getCityName());
			aaContactInfo.setStreetAddress1(address.getAddressLine1());
			aaContactInfo.setStreetAddress2(address.getAddressLine2());
			aaContactInfo.setZipCode(address.getZipCode());
		}

		String telephoneCountryCode = null, mobileCountryCode = null, faxCountryCode = null;
		String telephoneAreaCode = null, mobileAreaCode = null, faxAreaCode = null;
		if (wsContactInfo.getEmail() != null) {
			aaContactInfo.setEmail(wsContactInfo.getEmail());
		}

		if (wsContactInfo.getTelephone() != null && !CommonUtil.isEmpty(wsContactInfo.getTelephone().getPhoneNumber())) {
			telephoneCountryCode = wsContactInfo.getTelephone().getCountryCode();
			telephoneAreaCode = wsContactInfo.getTelephone().getAreaCode();
			String phoneNumber = telephoneCountryCode != null ? telephoneCountryCode
					+ WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
			if (wsContactInfo.getTelephone().getAreaCode() != null) {
				phoneNumber += telephoneAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
			}
			phoneNumber += wsContactInfo.getTelephone().getPhoneNumber();
			// validateCountryAccessCode(telephoneCountryCode,telephoneAreaCode,
			// wsContactInfo.getTelephone().getPhoneNumber(), aaContactInfo.getCountryCode());
			if (!OTAUtils.validateNumberFormat(telephoneCountryCode) || !OTAUtils.validateNumberFormat(telephoneAreaCode)
					|| !OTAUtils.validateNumberFormat(wsContactInfo.getTelephone().getPhoneNumber())) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_PHONE_NUMBER, "Invalid Format");
			}
			aaContactInfo.setPhoneNo(phoneNumber);
		}

		if (wsContactInfo.getMobile() != null && !CommonUtil.isEmpty(wsContactInfo.getMobile().getPhoneNumber())) {
			mobileCountryCode = wsContactInfo.getMobile().getCountryCode();
			mobileAreaCode = wsContactInfo.getMobile().getAreaCode();
			String phoneNumber = mobileCountryCode != null ? mobileCountryCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
			if (wsContactInfo.getMobile().getAreaCode() != null) {
				phoneNumber += mobileAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
			}
			phoneNumber += wsContactInfo.getMobile().getPhoneNumber();
			// validateCountryAccessCode(mobileCountryCode,mobileAreaCode, wsContactInfo.getMobile().getPhoneNumber(),
			// aaContactInfo.getCountryCode());
			if (!OTAUtils.validateNumberFormat(mobileCountryCode) || !OTAUtils.validateNumberFormat(mobileAreaCode)
					|| !OTAUtils.validateNumberFormat(wsContactInfo.getMobile().getPhoneNumber())) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_PHONE_NUMBER, "Invalid Mobile Number Format");
			}
			aaContactInfo.setMobileNo(phoneNumber);
		}

		if (wsContactInfo.getFax() != null && !CommonUtil.isEmpty(wsContactInfo.getFax().getPhoneNumber())) {
			faxCountryCode = wsContactInfo.getFax().getCountryCode();
			faxAreaCode = wsContactInfo.getFax().getAreaCode();
			String faxNumber = faxCountryCode != null ? faxCountryCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
			if (wsContactInfo.getFax().getAreaCode() != null) {
				faxNumber += faxAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
			}
			faxNumber += wsContactInfo.getFax().getPhoneNumber();
			// validateCountryAccessCode(faxCountryCode,faxAreaCode, wsContactInfo.getFax().getPhoneNumber(),
			// aaContactInfo.getCountryCode());
			if (!OTAUtils.validateNumberFormat(faxCountryCode) || !OTAUtils.validateNumberFormat(faxAreaCode)
					|| !OTAUtils.validateNumberFormat(wsContactInfo.getFax().getPhoneNumber())) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_FAX_NUMBER, "Invalid Fax Number Format");
			}
			aaContactInfo.setFax(faxNumber);
		}

		String prefLang = (wsContactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : wsContactInfo
				.getPreferredLanguage();
		aaContactInfo.setPreferredLanguage(prefLang);

		// Set Emergency Information
		AAEmergencyContactType emergencyContact = wsContactInfo.getEmergencyContact();
		if (emergencyContact != null) {
			AAPersonNameType emgnPersonName = emergencyContact.getPersonName();
			if (emgnPersonName != null) {
				aaContactInfo.setTitle(emgnPersonName.getTitle());
				aaContactInfo.setEmgnFirstName(emgnPersonName.getFirstName());
				aaContactInfo.setEmgnLastName(emgnPersonName.getLastName());
			}

			AATelephoneType emgnTel = emergencyContact.getTelephone();
			String emgnTelStr = "";

			if (emgnTel != null && (emgnTel.getPhoneNumber() != null || !emgnTel.getPhoneNumber().equals(""))) {
				emgnTelStr = emgnTel.getCountryCode() + WebservicesConstants.PHONE_NUMBER_SEPERATOR + emgnTel.getAreaCode()
						+ WebservicesConstants.PHONE_NUMBER_SEPERATOR + emgnTel.getPhoneNumber();
			}

			aaContactInfo.setEmgnPhoneNo(emgnTelStr);
			aaContactInfo.setEmgnEmail(emergencyContact.getEmail());
		}

		OTAUtils.validateContactInfo(aaContactInfo);

		return aaContactInfo;
	}

	/**
	 * 
	 * @param wsContactInfo
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static CommonReservationContactInfo getAAContactInfoFromWSRequest(AAContactInfoType wsContactInfo)
			throws WebservicesException, ModuleException {

		CommonReservationContactInfo aaContactInfo = new CommonReservationContactInfo();

		if (wsContactInfo.getPersonName() != null) {
			aaContactInfo.setTitle(CommonUtil.removeNonLetterCharactersFromString(wsContactInfo.getPersonName().getTitle()));
			aaContactInfo.setFirstName(CommonUtil.removeNonLetterCharactersFromString(wsContactInfo.getPersonName()
					.getFirstName()));
			aaContactInfo
					.setLastName(CommonUtil.removeNonLetterCharactersFromString(wsContactInfo.getPersonName().getLastName()));
		}

		AAAddressType address = wsContactInfo.getAddress();
		String stateCode = null;
		String taxRegNo = wsContactInfo.getTaxRegNo();
		if (address != null) {
			aaContactInfo.setCountryCode((address.getCountryName() != null) ? address.getCountryName().getCountryCode() : "");
			aaContactInfo.setCity(CommonUtil.removeNonLetterCharactersFromString(address.getCityName()));
			aaContactInfo.setStreetAddress1(CommonUtil.removeNonLetterCharactersFromString(address.getAddressLine1()));
			aaContactInfo.setStreetAddress2(CommonUtil.removeNonLetterCharactersFromString(address.getAddressLine2()));
			aaContactInfo.setZipCode(address.getZipCode());
			stateCode = address.getStateCode();
		}

		String telephoneCountryCode = null, mobileCountryCode = null, faxCountryCode = null;
		String telephoneAreaCode = null, mobileAreaCode = null, faxAreaCode = null;
		if (wsContactInfo.getEmail() != null) {
			aaContactInfo.setEmail(CommonUtil.removeInvalidCharactersFromEmailAddress(wsContactInfo.getEmail()));
		}

		if (wsContactInfo.getTelephone() != null && !CommonUtil.isEmpty(wsContactInfo.getTelephone().getPhoneNumber())) {
			telephoneCountryCode = wsContactInfo.getTelephone().getCountryCode();
			telephoneAreaCode = wsContactInfo.getTelephone().getAreaCode();
			String phoneNumber = prepareWSContactNumber(telephoneCountryCode, telephoneAreaCode, wsContactInfo.getTelephone()
					.getPhoneNumber());

			aaContactInfo.setPhoneNo(phoneNumber);
		}

		if (wsContactInfo.getMobile() != null && !CommonUtil.isEmpty(wsContactInfo.getMobile().getPhoneNumber())) {
			mobileCountryCode = wsContactInfo.getMobile().getCountryCode();
			mobileAreaCode = wsContactInfo.getMobile().getAreaCode();
			String phoneNumber = prepareWSContactNumber(mobileCountryCode, mobileAreaCode, wsContactInfo.getMobile()
					.getPhoneNumber());

			aaContactInfo.setMobileNo(phoneNumber);
		}

		if (wsContactInfo.getFax() != null && !CommonUtil.isEmpty(wsContactInfo.getFax().getPhoneNumber())) {
			faxCountryCode = wsContactInfo.getFax().getCountryCode();
			faxAreaCode = wsContactInfo.getFax().getAreaCode();
			String faxNumber = faxCountryCode != null ? faxCountryCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
			if (wsContactInfo.getFax().getAreaCode() != null) {
				faxNumber += faxAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
			}
			faxNumber += wsContactInfo.getFax().getPhoneNumber();

			aaContactInfo.setFax(faxNumber);
		}

		String prefLang = (wsContactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : wsContactInfo
				.getPreferredLanguage();
		aaContactInfo.setPreferredLanguage(prefLang);

		String[] taxRegNoEnabledCoutryCodes = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");

		boolean validateServiceTaxRegNumber = true;
		for (String countryCode : taxRegNoEnabledCoutryCodes) {
			if (aaContactInfo.getCountryCode().equals(countryCode)) {
				if (stateCode == null || stateCode.isEmpty()) {
					// FIXME this is adding to skip validation state when booking req state is empty
					if ("IN".equals(countryCode)) {
						ServiceTaxBaseCriteriaDTO taxQuoteBaseDTO = (ServiceTaxBaseCriteriaDTO) ThreadLocalData
								.getCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO);
						FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
								.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);
						String pointOfEmbarkationAirport = getPointOfIndianEmbarkationAirport(fareSegChargeTO);
						// [0] - country, [1] - sta
						String[] pointOfEmbarkationInfo = getPointOfEmbarkationInfo(pointOfEmbarkationAirport);
						if (taxQuoteBaseDTO.getPaxState() != null && !taxQuoteBaseDTO.getPaxState().isEmpty()) {
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_STATE_CODE,
									"State code should be provided for the given country");
						} else {
							if (countryCode.equals(pointOfEmbarkationInfo[0])) {
								stateCode = pointOfEmbarkationInfo[1];
								validateServiceTaxRegNumber = false;
							}
						}
					} else {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_STATE_CODE,
								"State code should be provided for the given country");
					}
				} else {
					List<State> states = WebServicesModuleUtils.getCommonMasterBD().getStates(countryCode);
					boolean isValidStateCode = false;
					for (State state : states) {
						if (state.getStateCode().trim().equals(stateCode)) {
							isValidStateCode = true;
							break;
						}
					}

					if (!isValidStateCode) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_STATE_CODE,
								"Given state code is not a state in the given country");
					}
				}
			}
		}

		aaContactInfo.setState(stateCode);

		if (validateServiceTaxRegNumber) {
			if (taxRegNo != null && !taxRegNo.isEmpty() && !stateCode.equals(taxRegNo.substring(0, 2))) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTRATION_NUMBER,
						"Given tax registration number and the state code does not match");
			}

			if (taxRegNo != null && !taxRegNo.isEmpty() && taxRegNo.length() != 15) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_TAX_REGISTRATION_NUMBER,
						"Tax registration number should be a 15 digit number");
			}

			ServiceTaxUtil.validatePriceQuoteServiceTaxCriteriaWithContactDetails(aaContactInfo.getCountryCode(), stateCode,
					taxRegNo);
		}

		aaContactInfo.setTaxRegNo(taxRegNo);

		// Set Emergency Information
		AAEmergencyContactType emergencyContact = wsContactInfo.getEmergencyContact();
		if (emergencyContact != null) {
			AAPersonNameType emgnPersonName = emergencyContact.getPersonName();
			if (emgnPersonName != null) {
				aaContactInfo.setTitle(CommonUtil.removeNonLetterCharactersFromString(emgnPersonName.getTitle()));
				aaContactInfo.setEmgnFirstName(CommonUtil.removeNonLetterCharactersFromString(emgnPersonName.getFirstName()));
				aaContactInfo.setEmgnLastName(CommonUtil.removeNonLetterCharactersFromString(emgnPersonName.getLastName()));
			}

			AATelephoneType emgnTel = emergencyContact.getTelephone();
			String emgnTelStr = "";

			if (emgnTel != null && (emgnTel.getPhoneNumber() != null || !emgnTel.getPhoneNumber().equals(""))) {
				emgnTelStr = emgnTel.getCountryCode() + WebservicesConstants.PHONE_NUMBER_SEPERATOR + emgnTel.getAreaCode()
						+ WebservicesConstants.PHONE_NUMBER_SEPERATOR + emgnTel.getPhoneNumber();
			}

			aaContactInfo.setEmgnPhoneNo(emgnTelStr);
			aaContactInfo.setEmgnEmail(CommonUtil.removeInvalidCharactersFromEmailAddress(emergencyContact.getEmail()));
		}

		OTAUtils.validateContactInfo(aaContactInfo);

		return aaContactInfo;
	}

	/**
	 * This method validate the phone countryAccessCode against the country code FIXME this validation is disable temp
	 * untill we add a new app param.
	 * 
	 * @param countryAccessCode
	 * @param countryCode
	 * @return
	 * @throws WebservicesException
	 */
	public static void
			validateCountryAccessCode(String countryAccessCode, String areaCode, String phoneNumber, String countryCode)
					throws WebservicesException {

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		if (!AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode())) {
			Collection<String[]> result = SelectListGenerator.getTelephoneCodes(countryCode, null);
			if (result != null && result.size() == 1) {
				String[] record = new ArrayList<String[]>(result).get(0);

				// countryAccessCode validation
				if (!countryAccessCode.equals(record[0])) {
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_DATA_FOUND_IN_PROCESSING_THE_RESERVATION_,
							"invalid countryAccessCode present");
				}

				// validating the area code
				Collection<String[]> areaCodes = SelectListGenerator.getAreaCodes(countryCode);
				if (areaCodes != null && areaCodes.size() > 0) { // validate only for the area cods present in the db.
					Set<String> codeSet = new HashSet<String>();
					for (String[] acode : areaCodes) {
						codeSet.add(acode[4]);
					}

					if (!codeSet.contains(areaCode)) { // invalid data
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_DATA_FOUND_IN_PROCESSING_THE_RESERVATION_,
								"invalid areaCode present");
					}
				}

				// phone number length validation
				int minLength = Integer.parseInt(record[3]);
				int maxLength = Integer.parseInt(record[4]);
				if (phoneNumber.length() < minLength || phoneNumber.length() > maxLength) {
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_DATA_FOUND_IN_PROCESSING_THE_RESERVATION_,
							"invalid phone number length present");
				}
			} // skipping the validation for countries which are not having a country Code in the db.
		}
	}

	public static AAUserNotesType transformToWSUserNotes(String userNote) {
		AAUserNotesType otaUserNotes = new AAUserNotesType();

		AAUserNoteType aaUserNote = new AAUserNoteType();
		aaUserNote.setNoteText(userNote);
		otaUserNotes.getUserNote().add(aaUserNote);

		return otaUserNotes;
	}

	public static String transformToAAUserNote(AAUserNotesType wsUserNotes) {
		String userNote = "";
		for (AAUserNoteType wsUserNote : wsUserNotes.getUserNote()) {
			if (!userNote.equals("")) {
				userNote += ",";
			}
			userNote += wsUserNote.getNoteText();
		}
		return userNote;
	}

	public static AAResAccSummaryType prepareWSAccountSummary(Reservation reservation, boolean setPaxWiseTotals)
			throws WebservicesException {
		AAResAccSummaryType wsAccSummary = new AAResAccSummaryType();

		wsAccSummary.setTotalPrice(new AACurrencyAmountType());
		wsAccSummary.setTotalPaidAmount(new AACurrencyAmountType());
		wsAccSummary.setTotalBalanceDue(new AACurrencyAmountType());
		wsAccSummary.setTotalCreditAvailable(new AACurrencyAmountType());

		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalPrice(), reservation.getTotalChargeAmount());
		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalPaidAmount(), reservation.getTotalPaidAmount());
		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalBalanceDue(), reservation.getTotalAvailableBalance()
				.doubleValue() > 0 ? reservation.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero());
		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalCreditAvailable(), reservation.getTotalAvailableBalance()
				.doubleValue() < 0 ? reservation.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero());

		if (setPaxWiseTotals) {
			wsAccSummary.setTravelersAccSummary(new AATravelersAccSummaryType());
			Collection<ReservationPax> reservationPaxes = reservation.getPassengers();

			BigDecimal[] paidAndBalanceAmounts = null;
			BigDecimal totalPaxChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPaxPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal totalPaxBalanceDue = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPaxCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (ReservationPax reservationPax : reservationPaxes) {
				AATravelerAccSummaryType travelerAccSummary = new AATravelerAccSummaryType();
				wsAccSummary.getTravelersAccSummary().getTravelerAccSummary().add(travelerAccSummary);

				travelerAccSummary.setTotalPrice(new AACurrencyAmountType());
				travelerAccSummary.setTotalPaidAmount(new AACurrencyAmountType());
				travelerAccSummary.setTotalBalanceDue(new AACurrencyAmountType());
				travelerAccSummary.setTotalCreditAvailable(new AACurrencyAmountType());

				Integer parentPaxSequence = null;
				if (ReservationApiUtils.isInfantType(reservationPax)) {// infant

					if (reservationPax.getParent() != null) {
						parentPaxSequence = reservationPax.getParent().getPaxSequence();

						paidAndBalanceAmounts = getIdetifiedInfantPaymentAndBalance(reservationPax, reservationPax.getParent());
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = paidAndBalanceAmounts[0];
						totalPaxBalanceDue = paidAndBalanceAmounts[1];
						totalPaxCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						;// infant pax credit is attached to the parent
					} else {
						// FIXME is this case possible?
						throw new WebservicesException("", "");
					}
				} else {
					Collection<ReservationPax> infants = reservationPax.getInfants();

					if (infants != null && infants.size() > 0) {// parent
						paidAndBalanceAmounts = getIdetifiedParentPaymentAndBalance(reservationPax, infants.iterator().next());
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = paidAndBalanceAmounts[0];
						totalPaxBalanceDue = paidAndBalanceAmounts[1];
						totalPaxCreditAmount = reservationPax.getTotalAvailableBalance().doubleValue() < 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {// adult or child
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = reservationPax.getTotalPaidAmount();
						totalPaxBalanceDue = reservationPax.getTotalAvailableBalance().doubleValue() > 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
						totalPaxCreditAmount = reservationPax.getTotalAvailableBalance().doubleValue() < 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
					}
				}

				travelerAccSummary.setTravelerRefNumberRPH(OTAUtils.prepareTravelerRPH(reservationPax.getPaxType(),
						reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), parentPaxSequence));

				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalPrice(), totalPaxChargeAmount);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalPaidAmount(), totalPaxPaidAmount);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalBalanceDue(), totalPaxBalanceDue);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalCreditAvailable(), totalPaxCreditAmount);
			}
		}
		return wsAccSummary;
	}

	public static AAResAccSummaryType
			prepareWSAccountSummary(LCCClientReservation lccClientReservation, boolean setPaxWiseTotals)
					throws WebservicesException {
		AAResAccSummaryType wsAccSummary = new AAResAccSummaryType();

		wsAccSummary.setTotalPrice(new AACurrencyAmountType());
		wsAccSummary.setTotalPaidAmount(new AACurrencyAmountType());
		wsAccSummary.setTotalBalanceDue(new AACurrencyAmountType());
		wsAccSummary.setTotalCreditAvailable(new AACurrencyAmountType());

		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalPrice(), lccClientReservation.getTotalChargeAmount());
		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalPaidAmount(), lccClientReservation.getTotalPaidAmount());
		OTAUtils.setAmountAndDefaultCurrency(
				wsAccSummary.getTotalBalanceDue(),
				lccClientReservation.getTotalAvailableBalance().doubleValue() > 0 ? lccClientReservation
						.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero());
		OTAUtils.setAmountAndDefaultCurrency(wsAccSummary.getTotalCreditAvailable(), lccClientReservation
				.getTotalAvailableBalance().doubleValue() < 0
				? lccClientReservation.getTotalAvailableBalance()
				: AccelAeroCalculator.getDefaultBigDecimalZero());

		if (setPaxWiseTotals) {
			wsAccSummary.setTravelersAccSummary(new AATravelersAccSummaryType());

			BigDecimal[] paidAndBalanceAmounts = null;
			BigDecimal totalPaxChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPaxPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal totalPaxBalanceDue = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPaxCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {
				AATravelerAccSummaryType travelerAccSummary = new AATravelerAccSummaryType();
				wsAccSummary.getTravelersAccSummary().getTravelerAccSummary().add(travelerAccSummary);

				travelerAccSummary.setTotalPrice(new AACurrencyAmountType());
				travelerAccSummary.setTotalPaidAmount(new AACurrencyAmountType());
				travelerAccSummary.setTotalBalanceDue(new AACurrencyAmountType());
				travelerAccSummary.setTotalCreditAvailable(new AACurrencyAmountType());

				Integer parentPaxSequence = null;
				if (ReservationApiUtils.isInfantType(reservationPax)) {// infant

					if (reservationPax.getParent() != null) {
						parentPaxSequence = reservationPax.getParent().getPaxSequence();

						paidAndBalanceAmounts = getIdetifiedInfantPaymentAndBalance(reservationPax, reservationPax.getParent());
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = paidAndBalanceAmounts[0];
						totalPaxBalanceDue = paidAndBalanceAmounts[1];
						totalPaxCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						;// infant pax credit is attached to the parent
					} else {
						// FIXME is this case possible?
						throw new WebservicesException("", "");
					}
				} else {
					Collection<LCCClientReservationPax> infants = reservationPax.getInfants();

					if (infants != null && !infants.isEmpty()) {// parent
						paidAndBalanceAmounts = getIdetifiedParentPaymentAndBalance(reservationPax,
								BeanUtils.getFirstElement(infants));
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = paidAndBalanceAmounts[0];
						totalPaxBalanceDue = paidAndBalanceAmounts[1];
						totalPaxCreditAmount = reservationPax.getTotalAvailableBalance().doubleValue() < 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
					} else {// adult or child
						totalPaxChargeAmount = ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts());
						totalPaxPaidAmount = reservationPax.getTotalPaidAmount();
						totalPaxBalanceDue = reservationPax.getTotalAvailableBalance().doubleValue() > 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
						totalPaxCreditAmount = reservationPax.getTotalAvailableBalance().doubleValue() < 0 ? reservationPax
								.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
					}
				}

				travelerAccSummary.setTravelerRefNumberRPH(reservationPax.getTravelerRefNumber());

				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalPrice(), totalPaxChargeAmount);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalPaidAmount(), totalPaxPaidAmount);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalBalanceDue(), totalPaxBalanceDue);
				OTAUtils.setAmountAndDefaultCurrency(travelerAccSummary.getTotalCreditAvailable(), totalPaxCreditAmount);
			}
		}
		return wsAccSummary;
	}

	private static BigDecimal[]
			getIdetifiedInfantPaymentAndBalance(LCCClientReservationPax infant, LCCClientReservationPax parent) {
		BigDecimal[] infantPaidAndBalanceAmount = new BigDecimal[] { AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero() };
		if (parent.getTotalAvailableBalance().doubleValue() <= 0) {
			infantPaidAndBalanceAmount[0] = infant.getTotalChargeAmount();
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else if (parent.getTotalPaidAmount().doubleValue() == 0) {
			infantPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			infantPaidAndBalanceAmount[1] = infant.getTotalChargeAmount();
		} else if (parent.getTotalPaidAmount().doubleValue() >= infant.getTotalChargeAmount().doubleValue()) {
			infantPaidAndBalanceAmount[0] = infant.getTotalChargeAmount();
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			infantPaidAndBalanceAmount[0] = AccelAeroCalculator.subtract(infant.getTotalChargeAmount(),
					parent.getTotalPaidAmount());
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.subtract(infant.getTotalChargeAmount(),
					infantPaidAndBalanceAmount[0]);
		}
		return infantPaidAndBalanceAmount;
	}

	private static BigDecimal[]
			getIdetifiedParentPaymentAndBalance(LCCClientReservationPax parent, LCCClientReservationPax infant) {
		BigDecimal[] parentPaidAndBalanceAmount = new BigDecimal[] { AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero() };
		if (parent.getTotalAvailableBalance().doubleValue() <= 0) {
			parentPaidAndBalanceAmount[0] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
			parentPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else if (parent.getTotalPaidAmount().doubleValue() == 0) {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			parentPaidAndBalanceAmount[1] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
		} else if (parent.getTotalPaidAmount().doubleValue() >= infant.getTotalChargeAmount().doubleValue()) {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.subtract(parent.getTotalPaidAmount(),
					infant.getTotalChargeAmount());
			parentPaidAndBalanceAmount[1] = AccelAeroCalculator.subtract(
					(AccelAeroCalculator.add(parent.getTotalChargeAmounts())), parentPaidAndBalanceAmount[0]);
		} else {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			parentPaidAndBalanceAmount[1] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
		}
		return parentPaidAndBalanceAmount;
	}

	private static BigDecimal[] getIdetifiedInfantPaymentAndBalance(ReservationPax infant, ReservationPax parent) {
		BigDecimal[] infantPaidAndBalanceAmount = new BigDecimal[] { AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero() };
		if (parent.getTotalAvailableBalance().doubleValue() <= 0) {
			infantPaidAndBalanceAmount[0] = infant.getTotalChargeAmount();
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else if (parent.getTotalPaidAmount().doubleValue() == 0) {
			infantPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			infantPaidAndBalanceAmount[1] = infant.getTotalChargeAmount();
		} else if (parent.getTotalPaidAmount().doubleValue() >= infant.getTotalChargeAmount().doubleValue()) {
			infantPaidAndBalanceAmount[0] = infant.getTotalChargeAmount();
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			infantPaidAndBalanceAmount[0] = AccelAeroCalculator.subtract(infant.getTotalChargeAmount(),
					parent.getTotalPaidAmount());
			infantPaidAndBalanceAmount[1] = AccelAeroCalculator.subtract(infant.getTotalChargeAmount(),
					infantPaidAndBalanceAmount[0]);
		}
		return infantPaidAndBalanceAmount;
	}

	private static BigDecimal[] getIdetifiedParentPaymentAndBalance(ReservationPax parent, ReservationPax infant) {
		BigDecimal[] parentPaidAndBalanceAmount = new BigDecimal[] { AccelAeroCalculator.getDefaultBigDecimalZero(),
				AccelAeroCalculator.getDefaultBigDecimalZero() };
		if (parent.getTotalAvailableBalance().doubleValue() <= 0) {
			parentPaidAndBalanceAmount[0] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
			parentPaidAndBalanceAmount[1] = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else if (parent.getTotalPaidAmount().doubleValue() == 0) {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			parentPaidAndBalanceAmount[1] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
		} else if (parent.getTotalPaidAmount().doubleValue() >= infant.getTotalChargeAmount().doubleValue()) {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.subtract(parent.getTotalPaidAmount(),
					infant.getTotalChargeAmount());
			parentPaidAndBalanceAmount[1] = AccelAeroCalculator.subtract(
					(AccelAeroCalculator.add(parent.getTotalChargeAmounts())), parentPaidAndBalanceAmount[0]);
		} else {
			parentPaidAndBalanceAmount[0] = AccelAeroCalculator.getDefaultBigDecimalZero();
			parentPaidAndBalanceAmount[1] = ReservationApiUtils.getTotalChargeAmount(parent.getTotalChargeAmounts());
		}
		return parentPaidAndBalanceAmount;
	}

	/**
	 * Prepare WS external transaction info from AA data
	 * 
	 * @param aaExternalPaymentTnx
	 * @return
	 * @throws WebservicesException
	 */
	public static AAExternalPayTxType prepareWSExtPayTxInfo(ExternalPaymentTnx aaExternalPaymentTnx) throws WebservicesException,
			ModuleException {
		AAExternalPayTxType wsExternalPayTxType = new AAExternalPayTxType();

		wsExternalPayTxType.setBalanceQueryRPH(aaExternalPaymentTnx.getBalanceQueryKey());
		wsExternalPayTxType.setAATxStatus(OTAUtils.getWSExtPayTxStatus(aaExternalPaymentTnx.getStatus()));
		wsExternalPayTxType.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(aaExternalPaymentTnx.getAmount()));
		if (aaExternalPaymentTnx.getPaxCount() != null)
			wsExternalPayTxType.setPaxCount(aaExternalPaymentTnx.getPaxCount().toString());
		wsExternalPayTxType.setBookingReferenceID(aaExternalPaymentTnx.getPnr());
		wsExternalPayTxType
				.setAATxStartTimestamp(CommonUtil.getFormattedDate(aaExternalPaymentTnx.getInternalTnxStartTimestamp()));
		if (aaExternalPaymentTnx.getInternalTnxStartTimestamp() != null) {
			wsExternalPayTxType.setAATxEndTimestamp(CommonUtil.getFormattedDate(aaExternalPaymentTnx
					.getInternalTnxStartTimestamp()));
		}
		if (aaExternalPaymentTnx.getExternalPayStatus() != null) {
			wsExternalPayTxType.setExternalTxStatus(OTAUtils.getWSExtPayTxStatus(aaExternalPaymentTnx.getExternalPayStatus()));
		}
		wsExternalPayTxType.setBankChannelCode(aaExternalPaymentTnx.getChannel());
		return wsExternalPayTxType;
	}

	/**
	 * Prepare AA external transaction info from WS data.
	 * 
	 * @param wsExternalPayTx
	 * @return
	 * @throws WebservicesException
	 */
	public static ExternalPaymentTnx prepareAAExtPayTxInfo(AAExternalPayTxType wsExternalPayTx) throws WebservicesException,
			ModuleException {
		ExternalPaymentTnx aaExternalPaymentTnx = new ExternalPaymentTnx();
		aaExternalPaymentTnx.setBalanceQueryKey(wsExternalPayTx.getBalanceQueryRPH());
		aaExternalPaymentTnx.setExternalPayId(wsExternalPayTx.getExternalSystemTxRPH());
		aaExternalPaymentTnx.setPnr(wsExternalPayTx.getBookingReferenceID());
		aaExternalPaymentTnx.setAmount(wsExternalPayTx.getAmount());
		if (wsExternalPayTx.getPaxCount() != null)
			aaExternalPaymentTnx.setPaxCount(new Integer(wsExternalPayTx.getPaxCount()));
		aaExternalPaymentTnx.setExternalPayStatus(OTAUtils.getAAExtPayTnxStatus(wsExternalPayTx.getExternalTxStatus()));
		if (wsExternalPayTx.getExternalTxTimestamp() != null) {// GMT time is expected
			aaExternalPaymentTnx.setExternalTnxEndTimestamp(CommonUtil.parseDate(wsExternalPayTx.getExternalTxTimestamp())
					.getTime());
		}
		aaExternalPaymentTnx.setChannel(wsExternalPayTx.getBankChannelCode());
		aaExternalPaymentTnx.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED);

		return aaExternalPaymentTnx;
	}

	/**
	 * Prepares WS cancellation/modification/addition balances.
	 * 
	 * @param resSummaryDTO
	 * @param effectingSegsCount
	 *            TODO
	 * 
	 * @return AACnxModBalancesType
	 * @throws ModuleException
	 */
	public static AAAlterationBalancesType prepareWSResAlterationBalances(Collection<ReservationPax> resPaxes,
			ResBalancesSummaryDTO resSummaryDTO, String alterationType, int effectingSegsCount) throws ModuleException {
		AAAlterationBalancesType balances = new AAAlterationBalancesType();

		AACnxModAddONDBalancesType updatingONDBalances = null;
		AACnxModAddONDBalancesType newONDBalances = null;

		int adultCount = 0;
		int childCount = 0;

		for (ReservationPax pax : resPaxes) {
			if (PaxTypeTO.ADULT.equals(pax.getPaxType())) {
				adultCount++;
			} else if (PaxTypeTO.CHILD.equals(pax.getPaxType())) {
				childCount++;
			}
		}

		Map<Integer, ReservationPax> resPaxMap = new HashMap<Integer, ReservationPax>();
		for (ReservationPax resPax : resPaxes) {
			resPaxMap.put(resPax.getPnrPaxId(), resPax);
		}

		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)) {
			ModifyBookingUtil.updateResSummaryModificationWithExtCharges(resSummaryDTO, resPaxMap);
		}

		// Existing OND data
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			updatingONDBalances = new AACnxModAddONDBalancesType();
			updatingONDBalances.setFare(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getCurrentFare()));
			updatingONDBalances.setTaxAndSurcharge(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getCurrentCharge()));
			updatingONDBalances.setAdjustments(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getCurrentAdjustments()));
			updatingONDBalances.setOtherCharges(getAACurrenyAmount(AccelAeroCalculator.add(resSummaryDTO.getSegmentSummeryDTO()
					.getCurrentCnxCharge(), resSummaryDTO.getSegmentSummeryDTO().getCurrentModCharge())));
			updatingONDBalances
					.setTotalCharges(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getCurrentTotalCharges()));
			updatingONDBalances.setNonRefundableAmount(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO()
					.getCurrentNonRefunds()));
			updatingONDBalances.setRefundableAmount(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getCurrentRefunds()));
		}

		// New OND data
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			newONDBalances = new AACnxModAddONDBalancesType();

			newONDBalances.setFare(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewFare()));
			newONDBalances.setTotalCharges(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewTotalCharges()));
			newONDBalances.setTaxAndSurcharge(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewCharge()));
			newONDBalances.setAdjustments(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewAdjustments()));
			newONDBalances.setOtherCharges(getAACurrenyAmount(AccelAeroCalculator.add(resSummaryDTO.getSegmentSummeryDTO()
					.getNewCnxCharge(), resSummaryDTO.getSegmentSummeryDTO().getNewModCharge())));
			newONDBalances.setNonRefundableAmount(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewNonRefunds()));
			newONDBalances.setRefundableAmount(getAACurrenyAmount(resSummaryDTO.getSegmentSummeryDTO().getNewRefunds()));
		}

		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)) {
			balances.setAACnxONDBalances(updatingONDBalances);
		}

		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(alterationType)) {
			balances.setAAModONDBalances(new AAAlterationBalancesType.AAModONDBalances());
			balances.getAAModONDBalances().setUpdatingONDCharges(updatingONDBalances);
			balances.getAAModONDBalances().setNewONDCharges(newONDBalances);
		}

		if (ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			balances.setAAAddONDBalances(newONDBalances);
		}

		// Set pax balances
		AATravelersCnxModAddResBalancesType travelersBalances = new AATravelersCnxModAddResBalancesType();
		Collection<PAXSummeryDTO> paxSummaries = resSummaryDTO.getPaxSummaryDTOCol();

		for (PAXSummeryDTO paxSummary : paxSummaries) {
			AATravelerCnxModAddResBalancesType travelerBalances = new AATravelerCnxModAddResBalancesType();
			travelerBalances.setTravelerRefNumberRPH(CommonServicesUtil.getTravelerRPH(resPaxMap.get(paxSummary.getPNRPaxId()),
					true));
			travelerBalances.setCurrentTotalCharges(getAACurrenyAmount(paxSummary.getCurrentCharges()));
			travelerBalances.setNewTotalCharges(getAACurrenyAmount(paxSummary.getNewTotalCharges()));
			travelerBalances.setCurrentTotalPayments(getAACurrenyAmount(paxSummary.getCurrentPayments()));
			travelerBalances.setBalance(getAACurrenyAmount(paxSummary.getNewBalance()));

			travelersBalances.getTravelerCnxModAddResBalances().add(travelerBalances);
		}

		balances.setTravelersCnxModAddResBalances(travelersBalances);
		balances.setTotalAmountDue(getAACurrenyAmount(resSummaryDTO.getTotalAmountDue()));
		balances.setTotalCnxChargeForCurrentOperation(getAACurrenyAmount(resSummaryDTO.getTotalCnxChargeForCurrentAlt()));
		balances.setTotalModChargeForCurrentOperation(getAACurrenyAmount(resSummaryDTO.getTotalModChargeForCurrentAlt()));
		balances.setTotalPrice(getAACurrenyAmount(resSummaryDTO.getTotalPrice()));

		if (BigDecimal.ZERO.compareTo(resSummaryDTO.getTotalAmountDue()) == -1) {
			ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(resSummaryDTO.getTotalAmountDue(), adultCount
					+ childCount, effectingSegsCount, ChargeRateOperationType.ANY);

			BigDecimal ccFee = ccCharge.getAmount();
			ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

			balances.setTotalAmountDueCC(getAACurrenyAmount(AccelAeroCalculator.add(resSummaryDTO.getTotalAmountDue(), ccFee)));
			balances.setTotalPriceCC(getAACurrenyAmount(AccelAeroCalculator.add(resSummaryDTO.getTotalPrice(), ccFee)));
		} else {
			balances.setTotalAmountDueCC(getAACurrenyAmount(resSummaryDTO.getTotalAmountDue()));
			balances.setTotalPriceCC(getAACurrenyAmount(resSummaryDTO.getTotalPrice()));
		}

		return balances;
	}

	/**
	 * Returns amount in AACurrencyAmountType.
	 * 
	 * @param amount
	 * @return
	 */
	public static AACurrencyAmountType getAACurrenyAmount(BigDecimal amount) {
		AACurrencyAmountType currencyAmount = new AACurrencyAmountType();
		currencyAmount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(amount));

		return currencyAmount;
	}

	/**
	 * 
	 * @param aaAdminInfo
	 * @return
	 */
	public static AAAdminInfoType transformToWSAdminInfo(ReservationAdminInfo aaAdminInfo) {
		AAAdminInfoType wsAdminInfo = new AAAdminInfoType();

		if (aaAdminInfo.getOriginChannelId() != null) {
			wsAdminInfo.setOriginChannelID(aaAdminInfo.getOriginChannelId().toString());
		}

		if (!CommonUtil.isEmpty(aaAdminInfo.getOriginAgentCode())) {
			wsAdminInfo.setOriginAgentCode(aaAdminInfo.getOriginAgentCode());
		}

		if (aaAdminInfo.getOwnerChannelId() != null) {
			wsAdminInfo.setOwnerChannelID(aaAdminInfo.getOwnerChannelId().toString());
		}

		if (!CommonUtil.isEmpty(aaAdminInfo.getOwnerAgentCode())) {
			wsAdminInfo.setOwnerAgentCode(aaAdminInfo.getOwnerAgentCode());
		}

		return wsAdminInfo;
	}

	/**
	 * 
	 * @param wsAdminInfo
	 * @return
	 */
	public static ReservationAdminInfo transformToAAAdminInfo(AAAdminInfoType wsAdminInfo) {
		ReservationAdminInfo aaAdminInfo = new ReservationAdminInfo();

		if (CommonUtil.isEmpty(wsAdminInfo.getOriginChannelID())) {
			aaAdminInfo.setOriginChannelId(new Integer(wsAdminInfo.getOriginChannelID()));
		}

		if (CommonUtil.isEmpty(wsAdminInfo.getOriginAgentCode())) {
			aaAdminInfo.setOriginAgentCode(wsAdminInfo.getOriginAgentCode());
		}

		if (CommonUtil.isEmpty(wsAdminInfo.getOwnerChannelID())) {
			aaAdminInfo.setOwnerChannelId(new Integer(wsAdminInfo.getOwnerChannelID()));
		}

		if (CommonUtil.isEmpty(wsAdminInfo.getOwnerAgentCode())) {
			aaAdminInfo.setOwnerAgentCode(wsAdminInfo.getOwnerAgentCode());
		}

		return aaAdminInfo;
	}

	/**
	 * Method to get the additional traveler informations.
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	public static AATravelerInfoType prepareWSTravelerInfoType(Reservation reservation) throws WebservicesException {
		AATravelerInfoType travelerInfoType = new AATravelerInfoType();
		Collection<ReservationPax> reservationPaxes = reservation.getPassengers();
		Integer parentPaxSequence = null;
		for (ReservationPax reservationPax : reservationPaxes) {
			AAAirTravelerType airTravelerType = new AAAirTravelerType();
			travelerInfoType.getAAAirTravelersType().add(airTravelerType);
			airTravelerType.setFOIDNumber((reservationPax.getPaxAdditionalInfo() != null) ? reservationPax.getPaxAdditionalInfo()
					.getPassportNo() : null);

			parentPaxSequence = null;
			if (ReservationApiUtils.isInfantType(reservationPax)) {
				if (reservationPax.getParent() != null) {
					parentPaxSequence = reservationPax.getParent().getPaxSequence();
				} else {
					// FIXME is this case possible?
					throw new WebservicesException("", "");
				}
			}
			airTravelerType.setTravelerRefNumberRPH(OTAUtils.prepareTravelerRPH(reservationPax.getPaxType(),
					reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), parentPaxSequence));
		}
		return travelerInfoType;
	}

	private static String prepareWSContactNumber(String countryCode, String areaCode, String phoneNumber) {
		String formattedPhoneNumber = "";
		if (CommonUtil.isEmpty(countryCode) || CommonUtil.isEmpty(areaCode) || CommonUtil.isEmpty(phoneNumber)) {

			String tempPhoneNumber = BeanUtils.nullHandler(countryCode) + BeanUtils.nullHandler(areaCode)
					+ BeanUtils.nullHandler(phoneNumber);
			tempPhoneNumber = CommonUtil.removeNonNumericCharactersFromNumericString(tempPhoneNumber);
			if (!CommonUtil.isEmpty(tempPhoneNumber) && tempPhoneNumber.length() > 4) {
				formattedPhoneNumber = tempPhoneNumber.substring(0, 2) + WebservicesConstants.PHONE_NUMBER_SEPERATOR
						+ tempPhoneNumber.substring(2, 4) + WebservicesConstants.PHONE_NUMBER_SEPERATOR
						+ tempPhoneNumber.substring(4);
			} else {
				formattedPhoneNumber = "" + WebservicesConstants.PHONE_NUMBER_SEPERATOR + ""
						+ WebservicesConstants.PHONE_NUMBER_SEPERATOR + tempPhoneNumber;
			}
		} else {
			formattedPhoneNumber = CommonUtil.removeNonNumericCharactersFromNumericString(countryCode)
					+ WebservicesConstants.PHONE_NUMBER_SEPERATOR
					+ CommonUtil.removeNonNumericCharactersFromNumericString(areaCode)
					+ WebservicesConstants.PHONE_NUMBER_SEPERATOR
					+ CommonUtil.removeNonNumericCharactersFromNumericString(phoneNumber);
		}

		return formattedPhoneNumber;
	}

	private static boolean isPointOfEmbarkationIndian(String ond) {
		boolean isPointOfEmbarkationIndian = false;

		Object[] pointOfEmbarkationInfo = CommonsServices.getGlobalConfig().retrieveAirportInfo(ond);

		if (pointOfEmbarkationInfo[3] != null && "IN".equals(pointOfEmbarkationInfo[3].toString())) {
			isPointOfEmbarkationIndian = true;
		}

		return isPointOfEmbarkationIndian;
	}

	private static String[] getPointOfEmbarkationInfo(String pointOfEmbarkationAirport) {
		String pointOfEmbarkationInfoStr[] = new String[2];
		Object[] pointOfEmbarkationInfo = CommonsServices.getGlobalConfig().retrieveAirportInfo(pointOfEmbarkationAirport);
		if (pointOfEmbarkationInfo[3] != null) {
			pointOfEmbarkationInfoStr[0] = pointOfEmbarkationInfo[3].toString();
		}
		if (pointOfEmbarkationInfo[5] != null) {
			pointOfEmbarkationInfoStr[1] = pointOfEmbarkationInfo[5].toString();
		}
		return pointOfEmbarkationInfoStr;
	}

	// FIXME currently getting indian airports
	private static String getPointOfIndianEmbarkationAirport(FareSegChargeTO fareSegChargeTO) {
		String ondCode = "";
		String pointOfEmbarkation = "";
		if (fareSegChargeTO != null) {
			Collection<OndFareSegChargeTO> ondFareSegChgTo = fareSegChargeTO.getOndFareSegChargeTOs();
			for (OndFareSegChargeTO ondFareSegChg : ondFareSegChgTo) {
				ondCode = ondFareSegChg.getOndCode();
				if (!ondCode.isEmpty()) {
					String[] ondCodes = ondCode.split("/");
					for (String ond : ondCodes) {
						if (isPointOfEmbarkationIndian(ond)) {
							return ond;
						}
					}
				}
			}
		}
		return "DEL";
	}
}
