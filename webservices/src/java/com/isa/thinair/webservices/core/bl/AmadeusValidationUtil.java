package com.isa.thinair.webservices.core.bl;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDataTypePRRQ;
import com.amadeus.ama._2008._03.TLAPaymentCardTypeRS;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusValidationUtil {
	private final static Log log = LogFactory.getLog(AmadeusValidationUtil.class);

	/**
	 * Validate the pax count
	 * 
	 * @param adultCount
	 * @param childCount
	 * @param infantCount
	 * @throws AmadeusWSException
	 */
	public static void validatePaxCounts(int adultCount, int childCount, int infantCount) throws AmadeusWSException {
		if (adultCount + childCount <= 0) {
			log.error("Invalid adults/children pax count. " + "[adults =" + adultCount + ", children =" + childCount + "]");
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Adult pax count cannot be zero");
		}

		if (adultCount < infantCount) {
			log.error("Infants count exceeded adult count " + "[adults =" + adultCount + ",[infants =" + infantCount + "]");
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Infants count cannot exceed adult count");
		}
	}

	/**
	 * Validate SSR
	 * 
	 * @param ssr
	 * @throws AmadeusWSException
	 */
	public static void validateSSR(TLAPassengerMonetaryDataTypePRRQ paxData) throws AmadeusWSException {
		if (paxData.getQuantity() != null && paxData.getQuantity().intValue() < 0) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"SSR quantity cannot be negative");
		}
	}

	/**
	 * Validate CreditCardDetails
	 * 
	 * @param creditCardDetails
	 * @throws AmadeusWSException
	 */
	public static void validateCreditCardDetails(TLAPaymentCardTypeRS paymentCard) throws AmadeusWSException {
		if (paymentCard.getCardCode().length() != 2) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Card code should be 2 characters.");
		}
		if (paymentCard.getExpireDate() == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Expiry date of Credit card must be specified");
		}
		if (paymentCard.getSeriesCode() == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.SECURITY_CODE_REQUIRED, ErrorWarningType.BIZ_RULE,
					"Security code must be specified");
		}
		if (paymentCard.getCardNumber() == null || "".equals(paymentCard.getCardNumber())) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Invalid Card Number");
		}
	}

	/**
	 * Validate airport codes against the data base
	 * 
	 * @param fromAirport
	 * @param toAirport
	 * @throws AmadeusWSException
	 */
	public static void validateAirports(String fromAirport, String toAirport) throws AmadeusWSException {
		boolean airportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(fromAirport, toAirport);

		if (!airportsValid) {
			log.error("From/To aiports validation failed. " + "[From airport=" + fromAirport + "," + "To airport=" + toAirport
					+ "]");
			throw new AmadeusWSException(AmadeusErrorCodes.INVALID_ORIGIN_DESTINATION, ErrorWarningType.BIZ_RULE,
					"Invalid origin/destination");
		}
	}

	public static void validateDepDate(Date zuluDate) throws AmadeusWSException {
		Date cdate = CalendarUtil.getCurrentSystemTimeInZulu();
		if (zuluDate.before(cdate)) {
			if (!CalendarUtil.isSameDay(zuluDate, cdate)) {
				throw new AmadeusWSException(AmadeusErrorCodes.INVALID_DEPARTURE_DATE_FOR_SEGMENT, ErrorWarningType.BIZ_RULE,
						"Invalid departure date");
			}
		}
	}

	public static void validateCurrency(String currencyCode) throws AmadeusWSException {
		CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
		Currency currency;
		try {
			currency = commonMasterBD.getCurrency(currencyCode);
		} catch (ModuleException e) {
			throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_CURRENCY_NOT_SUPPORTED, ErrorWarningType.BIZ_RULE,
					"Currency is not supported, cannot continue with authorization");
		}
		if (currency == null || Currency.STATUS_INACTIVE.equals(currency.getStatus())) {
			throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_CURRENCY_NOT_SUPPORTED, ErrorWarningType.BIZ_RULE,
					"Currency is not supported, cannot continue with authorization");
		}
	}
}
