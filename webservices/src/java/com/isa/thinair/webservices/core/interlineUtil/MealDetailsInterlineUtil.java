/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.MealDetailsUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * @author nafly
 * 
 */
public class MealDetailsInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(MealDetailsInterlineUtil.class);

	/**
	 * 
	 * @param travelerFlightDetailsWithMeals
	 * @param flightPriceRS
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<String, List<LCCMealDTO>>> getSelectedMealDetails(
			Map<String, Map<String, List<String>>> travelerFlightDetailsWithMeals, FlightPriceRS flightPriceRS, SYSTEM system)
			throws ModuleException, WebservicesException {

		Map<String, Map<String, List<LCCMealDTO>>> segmentWithMeals = new HashMap<String, Map<String, List<LCCMealDTO>>>();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());

		}

		List<BundledFareDTO> bundledFareDTOs = null;
		if (flightPriceRS != null && flightPriceRS.getSelectedPriceFlightInfo() != null
				&& flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
			bundledFareDTOs = flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO().getOndBundledFareDTOs();
		}

		List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, null, null);

		LCCMealResponceDTO mealResponceDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getAvailableMeals(
				lccMealRequestDTOs, null, null, bundledFareDTOs, system, null, ApplicationEngine.WS, null);

		if (mealResponceDTO == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_MEALS_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
					"");
		}
		for (String travelerRef : travelerFlightDetailsWithMeals.keySet()) {
			// FlightRPH -> Meal(s)
			Map<String, List<String>> flightDetailsWithMeals = travelerFlightDetailsWithMeals.get(travelerRef);
			for (String flightRPH : flightDetailsWithMeals.keySet()) {
				List<LCCMealDTO> lccMealDTOs = new ArrayList<LCCMealDTO>();
				List<String> mealDetails = flightDetailsWithMeals.get(flightRPH);
				if (mealDetails != null) {
					for (String selectedMeal : mealDetails) {
						Integer mealQuantity = Collections.frequency(mealDetails, selectedMeal);
						boolean isCatRestriction = false;
						for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
							if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
								List<LCCMealDTO> lCCMealDTOs = getLCCMealDTOsByRPH(flightRPH, mealResponceDTO);
								boolean isMealFound = false;
								for (LCCMealDTO lccMealDTO : lCCMealDTOs) {
									if (lccMealDTO.getMealCode().equals(selectedMeal)
											&& lccMealDTO.getAvailableMeals() > mealQuantity) {
										isMealFound = true;
										lccMealDTOs.add(lccMealDTO);
									}
									if(lccMealDTO.isCategoryRestricted()){
										isCatRestriction = true;
									}
								}
								if (segmentWithMeals.get(travelerRef) == null) {
									segmentWithMeals.put(travelerRef, new HashMap<String, List<LCCMealDTO>>());
								}
								segmentWithMeals.get(travelerRef).put(flightSegmentTO.getFlightRefNumber(), lccMealDTOs);
								if (!isMealFound) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_MEALS_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
											selectedMeal);
								}

								if ((!MealDetailsUtil.isMultiMealEnabled(flightSegmentTO.getFlightSegId()) || isCatRestriction)
										&& (mealQuantity > 1 || lccMealDTOs.size() > 1)) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_MULTI_MEAL_NOT_ENABLED_IN_THE_SYSTEM, "");
								}
							}

						}
					}
				}
			}
		}

		return segmentWithMeals;
	}

	/**
	 * Compose the meal request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCMealRequestDTO>
	 */
	private static List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				mrd.setCabinClass(cabinClass);
			} else {
				mrd.setCabinClass(fst.getCabinClassCode());
			}
			mrd.getFlightSegment().add(fst);
			mL.add(mrd);
		}
		return mL;
	}

	/**
	 * 
	 * @param flightRPH
	 * @param mealResponceDTO
	 * @return
	 */
	private static List<LCCMealDTO> getLCCMealDTOsByRPH(String flightRPH, LCCMealResponceDTO mealResponceDTO) {
		List<LCCMealDTO> lccMealDTOs = new ArrayList<LCCMealDTO>();

		if (mealResponceDTO != null) {

			for (LCCFlightSegmentMealsDTO flightSegmentMealsDTO : mealResponceDTO.getFlightSegmentMeals()) {
				FlightSegmentTO flightSegmentTO = flightSegmentMealsDTO.getFlightSegmentTO();

				if (flightSegmentTO.getFlightRefNumber().equals(flightRPH)) {

					lccMealDTOs.addAll(flightSegmentMealsDTO.getMeals());
					break;
				}

			}
		}

		return lccMealDTOs;
	}

	/**
	 * @param mealDetailsMap
	 * @return externalCharegs
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForLCCMeals(
			Map<String, Map<String, List<LCCMealDTO>>> mealDetailsMap) throws ModuleException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();

		for (String travelerRef : mealDetailsMap.keySet()) {
			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();
			Map<String, List<LCCMealDTO>> mealsMap = mealDetailsMap.get(travelerRef);
			for (String flightRPH : mealsMap.keySet()) {
				// If multi meal enabled and multiple meals selected following list contains more than 1 elements
				List<LCCMealDTO> mealDTOs = mealsMap.get(flightRPH);
				for (LCCMealDTO mealDTO : mealDTOs) {
					LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

					externalChargeTO.setAmount(mealDTO.getMealCharge());
					externalChargeTO.setCode(mealDTO.getMealCode());
					externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.MEAL);
					externalChargeTO.setFlightRefNumber(flightRPH);

					chargesList.add(externalChargeTO);
				}
			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}
}
