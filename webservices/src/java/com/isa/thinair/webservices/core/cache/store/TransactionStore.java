package com.isa.thinair.webservices.core.cache.store;

import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.transaction.ITransaction;

public interface TransactionStore {

	ITransaction createNewTransaction() throws WebservicesException;

	void removeTransaction(String transactionId) throws WebservicesException;

	ITransaction getTransaction(String transactionId) throws WebservicesException;

	void removeExpiredTransactions();

	void forceExpireAndRemoveTransaction(String transactionId) throws WebservicesException;

	void flushTransaction(ITransaction transaction) throws WebservicesException;

}
