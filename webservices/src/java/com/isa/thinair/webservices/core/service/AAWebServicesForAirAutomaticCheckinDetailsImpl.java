package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRS;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.service.AAWebServicesForAirAutomaticCheckinDetails;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.AutomaticChekinDetailsUtil;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.WebservicesContext;

@WebService(serviceName = "AAWebServicesForAirAutomaticCheckinDetails", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForAirAutomaticCheckinDetails")
public class AAWebServicesForAirAutomaticCheckinDetailsImpl implements AAWebServicesForAirAutomaticCheckinDetails {

	private final Log log = LogFactory.getLog(AAWebServicesForLoginWithTokenAPIImpl.class);

	@Override
	public AAOTAAirAutomaticCheckinDetailsRS getAutomaticCheckinDetails(
			AAOTAAirAutomaticCheckinDetailsRQ getAutomaticCheckinDetails) {
		if (log.isDebugEnabled()) {
			log.debug("Begin web service method getAutomaticCheckinDetails(AAOTAAirAutomaticCheckinDetailsRQ)");
		}

		AAOTAAirAutomaticCheckinDetailsRS otaAirAutomaticCheckinDetailsRS = null;
		try {
			triggerPreProcessErrorIfPresent();
			otaAirAutomaticCheckinDetailsRS = new AutomaticChekinDetailsUtil()
					.getAutomaticCheckinDetails(getAutomaticCheckinDetails);
		} catch (Exception ex) {
			setTriggerPostProcessError(ex);
			if (otaAirAutomaticCheckinDetailsRS == null)
				otaAirAutomaticCheckinDetailsRS = new AAOTAAirAutomaticCheckinDetailsRS();
			if (otaAirAutomaticCheckinDetailsRS.getErrors() == null)
				otaAirAutomaticCheckinDetailsRS.setErrors(new ErrorsType());
			if (otaAirAutomaticCheckinDetailsRS.getWarnings() == null)
				otaAirAutomaticCheckinDetailsRS.setWarnings(new WarningsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(otaAirAutomaticCheckinDetailsRS.getErrors().getError(),
					otaAirAutomaticCheckinDetailsRS.getWarnings().getWarning(), ex);
			log.error("Error occured while executing web service method getAutomaticCheckinDetails(AAOTAAirAutomaticCheckinDetailsRQ)", ex);

		}
		if (log.isDebugEnabled())
			log.debug("End web service method getAutomaticCheckinDetails(AAOTAAirAutomaticCheckinDetailsRQ)");
		return otaAirAutomaticCheckinDetailsRS;
	}

	private void triggerPreProcessErrorIfPresent() throws WebservicesException {
		Boolean triggerError = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_PREPROCESS_ERROR);
		if (triggerError != null && triggerError.booleanValue()) {
			throw (WebservicesException) ThreadLocalData.getWSContextParam(
					WebservicesContext.PREPROCESS_ERROR_OBJECT);
		}

	}

	private void setTriggerPostProcessError(Throwable t) {
		ThreadLocalData.setTriggerPostProcessError(t);
	}

}