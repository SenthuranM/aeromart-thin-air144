package com.isa.thinair.webservices.core.web;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.isa.thinair.webservices.core.session.SessionCleaningQuartzJob;
import com.isa.thinair.webservices.core.transaction.TxnCleaningQuartzJob;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class LocalQuartzScheduler {

	private static Log log = LogFactory.getLog(LocalQuartzScheduler.class);

	private Scheduler sched;

	public LocalQuartzScheduler() throws Exception {
		log.info("------- Initializing -------------------");

		SchedulerFactory sf = new StdSchedulerFactory();
		sched = sf.getScheduler();

		log.info("------- Initialization Complete --------");
	}

	public void scheduleAndStartScheduler() throws Exception {
		log.info("------- Scheduling Jobs ----------------");

		long sessCleaningIntervalInMins = WebServicesModuleUtils.getWebServicesConfig().getUserSessionCleaningGapInMillis()
				/ (1000 * 60);

		JobDetailImpl job1 = new JobDetailImpl("jobSessionCleaning", "groupLocal", SessionCleaningQuartzJob.class);
		CronTriggerImpl trigger1 = new CronTriggerImpl("triggerSessionCleaning", "groupLocal", "jobSessionCleaning", "groupLocal", "0 0/"
				+ sessCleaningIntervalInMins + " * * * ?");		
		
		Date ft1 = sched.scheduleJob(job1, trigger1);
		
		
		log.info(job1.getKey().getName() + " has been scheduled to run at: " + ft1 + " and repeat based on expression: "
				+ trigger1.getCronExpression());

		long txnCleaningIntervalInMins = WebServicesModuleUtils.getWebServicesConfig().getTxnCleaningGapInMillis() / (1000 * 60);

		JobDetailImpl job2 = new JobDetailImpl("jobTxnCleaning", "groupLocal", TxnCleaningQuartzJob.class);
		CronTriggerImpl trigger2 = new CronTriggerImpl("triggerTxnCleaning", "groupLocal", "jobTxnCleaning", "groupLocal", "0 0/"
				+ txnCleaningIntervalInMins + " * * * ?");
		Date ft2 = sched.scheduleJob(job2, trigger2);
		log.info(job2.getKey().getName() + " has been scheduled to run at: " + ft2 + " and repeat based on expression: "
				+ trigger2.getCronExpression());

		log.info("------- Starting Scheduler ----------------");
		sched.start();
		log.info("------- Started Scheduler -----------------");
	}

	public void stopScheduler() throws Exception {
		sched.shutdown();
	}
}
