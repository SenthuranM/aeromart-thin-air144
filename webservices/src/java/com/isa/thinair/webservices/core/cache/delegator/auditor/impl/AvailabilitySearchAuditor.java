package com.isa.thinair.webservices.core.cache.delegator.auditor.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.core.cache.delegator.auditor.BaseAuditor;
import com.isa.thinair.webservices.core.cache.delegator.dto.JourenyInfo;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.Operation;

/**
 * 
 * Below audits will be act as a source for the External visualization
 *
 */
public class AvailabilitySearchAuditor extends BaseAuditor<OTAAirAvailRQ> {

	private static final Log log = LogFactory.getLog(AvailabilitySearchAuditor.class);

	public AvailabilitySearchAuditor(OTAAirAvailRQ request) {
		super(request);
	}

	public String getAudit(Operation operation) {
		StringBuilder audit = new StringBuilder();

		try {
			List<JourenyInfo> jourenyList = getRouteRequests();
			Gson gson = new GsonBuilder().create();
			String routeInfo = gson.toJson(jourenyList);

			Map<String, Integer> travelerQuantity = getTravelerQuantityInfo();
			String quantity = gson.toJson(travelerQuantity);

			audit.append(CacheConstant.Audit.OPERATION.getCode());
			audit.append(operation.getCode());
			audit.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

			audit.append(CacheConstant.Audit.JOURNEY_INFO.getCode());
			audit.append(routeInfo);
			audit.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

			audit.append(CacheConstant.Audit.QUANTITY.getCode());
			audit.append(quantity);
			audit.append(CacheConstant.Delimeters.END.getCode());
		} catch (Exception e) {
			log.error("Error in AvailabilitySearchAuditor", e);
		}

		return audit.toString();

	}

	private List<JourenyInfo> getRouteRequests() throws ModuleException {

		List<JourenyInfo> journeyList = new ArrayList<JourenyInfo>();

		List<OriginDestinationInformation> originDestinationInformations = this.request.getOriginDestinationInformation();
		if (originDestinationInformations != null) {
			JourenyInfo journey = new JourenyInfo();
			for (OriginDestinationInformation originDestinationInformation : originDestinationInformations) {

				String origin = originDestinationInformation.getOriginLocation().getLocationCode();
				String destination = originDestinationInformation.getDestinationLocation().getLocationCode();

				journey.setOrigin(origin);
				journey.setDetination(destination);

				TimeInstantType timeInstantType = originDestinationInformation.getDepartureDateTime();

				if (timeInstantType != null) {
					Date departureDate = CommonUtil.parseDate(timeInstantType.getValue()).getTime();
					String formattedDate = CalendarUtil.formatDateYYYYMMDD(departureDate);
					journey.setDepartureDate(formattedDate);

					if (timeInstantType.getWindowBefore() != null) {
						int windowBefore = timeInstantType.getWindowBefore().getDays();
						journey.setWindowBefore(windowBefore);
					}

					if (timeInstantType.getWindowAfter() != null) {
						int windowAfter = timeInstantType.getWindowAfter().getDays();
						journey.setWindowAfter(windowAfter);
					}

				}

				journeyList.add(journey);

			}

		}
		return journeyList;
	}

	private Map<String, Integer> getTravelerQuantityInfo() {

		Map<String, Integer> quantityMap = new HashMap<String, Integer>();

		TravelerInfoSummaryType travelerInfoSummaryType = this.request.getTravelerInfoSummary();
		if (travelerInfoSummaryType != null && travelerInfoSummaryType.getAirTravelerAvail() != null) {
			List<TravelerInformationType> airTravelerAvails = travelerInfoSummaryType.getAirTravelerAvail();

			if (airTravelerAvails != null) {
				for (TravelerInformationType travelerInformationType : airTravelerAvails) {
					if (travelerInformationType.getPassengerTypeQuantity() != null) {
						for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType
								.getPassengerTypeQuantity()) {

							quantityMap.put(passengerTypeQuantityType.getCode(), passengerTypeQuantityType.getQuantity());
						}
					}
				}
			}
		}

		return quantityMap;
	}

}
