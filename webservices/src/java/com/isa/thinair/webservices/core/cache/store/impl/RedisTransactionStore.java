package com.isa.thinair.webservices.core.cache.store.impl;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.springframework.cache.Cache;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.store.AbstractStore;
import com.isa.thinair.webservices.core.cache.store.TransactionStore;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * 
 * Redis Transaction Store Implementation
 *
 */
public class RedisTransactionStore extends AbstractStore implements TransactionStore {

	private static final Log log = LogFactory.getLog(RedisTransactionStore.class);

	@Override
	public ITransaction createNewTransaction() throws WebservicesException {
		ITransaction transaction = new Transaction();
		String transactionId = transaction.getId();

		try {
			Cache cache = getCache();
			cache.put(transactionId, transaction);

			if (log.isDebugEnabled()) {
				log.debug("REDIS Success on createNewTransaction. Transaction Id=" + transaction.getId() + ",Creation Timestamp="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(transaction.getCreationTime())));
			}

		} catch (Exception e) {
			log.error("REDIS Error on createNewTransaction for transaction Id:" + transactionId, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE,
					CacheConstant.CACHE_SERVICE_ERROR);
		}

		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_NEW_TXN_CREATED, new Boolean(true));

		return transaction;
	}

	@Override
	public void removeTransaction(String transactionId) throws WebservicesException {

		try {
			Cache cache = getCache();
			cache.evict(transactionId);

			if (log.isDebugEnabled()) {
				log.debug("REDIS Success on removeTransaction. Transaction Id=" + transactionId);
			}

		} catch (Exception e) {
			log.error("REDIS Error on removeTransaction. Transaction Id=" + transactionId, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE,
					CacheConstant.CACHE_SERVICE_ERROR);
		}

	}

	@Override
	public ITransaction getTransaction(String transactionId) throws WebservicesException {
		ITransaction transaction = null;

		try {
			Cache cache = getCache();
			if (cache.get(transactionId) != null) {
				transaction = (ITransaction) cache.get(transactionId).get();
			}

			if (log.isDebugEnabled()) {
				log.debug("REDIS Success on getTransaction. Transaction Id=" + transactionId);
			}

		} catch (Exception e) {
			log.error("REDIS Error on getTransaction. Transaction Id=" + transactionId, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE,
					CacheConstant.CACHE_SERVICE_ERROR);
		}

		return transaction;
	}

	@Override
	public void forceExpireAndRemoveTransaction(String transactionId) throws WebservicesException {
		ITransaction transaction = getTransaction(transactionId);
		if (transaction != null) {
			transaction.forceExpiration();
			removeTransaction(transactionId);
		}

	}

	@Override
	public void removeExpiredTransactions() {
		// Currently this is done explicitly with Redis TTL
	}

	/**
	 * Used to flush the local transaction to remote store
	 * 
	 * Must flush the current thread local transaction to remote to keep it sync
	 * 
	 * This Should done before unsetting the threadlocal transaction
	 */
	@Override
	public void flushTransaction(ITransaction transaction) throws WebservicesException {
		String transactionId = null;
		try {
			if (transaction != null) {
				transactionId = transaction.getId();
				Cache cache = getCache();
				cache.put(transactionId, transaction);
			}

			if (log.isDebugEnabled()) {
				log.debug("REDIS Success on flushTransaction. Transaction Id=" + transactionId);
			}

		} catch (Exception e) {
			log.error("REDIS Error on flushTransaction. Transaction Id=" + transactionId, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE,
					CacheConstant.CACHE_SERVICE_ERROR);
		}

	}
}
