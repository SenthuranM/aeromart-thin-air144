package com.isa.thinair.webservices.core.cache.store.impl;

import com.isa.thinair.webservices.core.cache.store.AbstractStoreFactory;
import com.isa.thinair.webservices.core.cache.store.SessionStore;

/**
 * 
 * Factory to get the the SessionStore based on the configuration
 *
 */
public class SessionStoreFactory extends AbstractStoreFactory<SessionStore> {

}