package com.isa.thinair.webservices.core.bl.myidtravel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.isa.thinair.webservices.core.util.ServiceTaxCalculator;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlSegmentSellBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlSegmentSellBL.class);

	public static MyIDTravelResAdapterSegmentSellResponse execute(MyIDTravelResAdapterSegmentSellRequest myIDSegSellReq) {

		if (log.isDebugEnabled()) {
			log.debug("BEGIN doSegmentSell(MyIDTravelResAdapterSegmentSellRequest)");
		}

		MyIDTravelResAdapterSegmentSellResponse myIDSegSellRes = null;
		try {
			myIDSegSellRes = doSegmentSell(myIDSegSellReq);
		} catch (Exception ex) {
			log.error("doSegmentSell(MyIDTravelResAdapterSegmentSellRequest) failed.", ex);
			if (myIDSegSellRes == null) {
				myIDSegSellRes = new MyIDTravelResAdapterSegmentSellResponse();
				OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
				otaAirBookRS.setEchoToken(myIDSegSellReq.getOTAAirBookRQ().getEchoToken());
				myIDSegSellRes.setOTAAirBookRS(otaAirBookRS);
			}
			if (myIDSegSellRes.getOTAAirBookRS().getErrors() == null)
				myIDSegSellRes.getOTAAirBookRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDSegSellRes.getOTAAirBookRS().getErrors().getError(), myIDSegSellRes
					.getOTAAirBookRS().getSuccessAndWarningsAndAirReservation(), ex);
		}
		if (log.isDebugEnabled())
			log.debug("END doSegmentSell(MyIDTravelResAdapterSegmentSellRequest)");
		return myIDSegSellRes;

	}

	private static MyIDTravelResAdapterSegmentSellResponse doSegmentSell(MyIDTravelResAdapterSegmentSellRequest myIDSegSellReq)
			throws ModuleException, WebservicesException {

		MyIDTravelResAdapterSegmentSellResponse myIDSegSellRes = new MyIDTravelResAdapterSegmentSellResponse();
		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
		myIDSegSellRes.setOTAAirBookRS(otaAirBookRS);
		myIDSegSellRes.setEmployeeData(myIDSegSellReq.getEmployeeData());

		OTAAirBookRQ otaAirBookRQ = myIDSegSellReq.getOTAAirBookRQ();
		Map<Integer, String> refSegIdRPHMap = new HashMap<Integer, String>();

		// do an onhold booking for the given booking class data
		AvailableFlightSearchDTO availableFlightSearchDTO = extractPriceQuoteReqParamsForSegSell(otaAirBookRQ.getAirItinerary(),
				otaAirBookRQ.getTravelerInfo(), refSegIdRPHMap);
		if (availableFlightSearchDTO != null) {
			SelectedFlightDTO selectedFlightDTO = WebServicesModuleUtils.getReservationQueryBD().getFareQuote(
					availableFlightSearchDTO, null);

			// Pricing available now do an onhold booking
			Collection<OndFareDTO> selectedOndCollection = selectedFlightDTO.getOndFareDTOs(false);

			if (selectedOndCollection == null || selectedOndCollection.isEmpty()) {

				log.error("Segment sell request failed:Booking class -"
						+ availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getPreferredBookingClass()
						+ "might not be available in all flights");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Booking class - "
						+ availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getPreferredBookingClass()
						+ "might not be available in all flights, Please contact operating airline");
			}

			ServiceTaxCalculator serviceTaxCalculator = MyIDTrvlCommonUtil.getServiceTaxCalculator(selectedFlightDTO);
			IReservation iReservation = composeReservationAssembler(selectedOndCollection, otaAirBookRQ, myIDSegSellReq
					.getEmployeeData().getTravellerId(), serviceTaxCalculator);

			Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(selectedOndCollection, 1, 1, 1);

			Collection outBoundSegments = (Collection) inOutboundSegments[0];
			Collection inBoundSegments = (Collection) inOutboundSegments[1];

			for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
				ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();
				iReservation.addOutgoingSegment(segmentInfo.getSegmentSeq(), segmentInfo.getFlightSegId(),
						segmentInfo.getOndGroupId(), segmentInfo.getReturnOndGroupId(),
						refSegIdRPHMap.get(segmentInfo.getFlightSegId()), segmentInfo.getSelectedBundledFarePeriodId(), null, null);
			}

			if (inBoundSegments != null) {
				for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
					ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
					iReservation.addReturnSegment(segmentInfo.getSegmentSeq(), segmentInfo.getFlightSegId(),
							segmentInfo.getOndGroupId(), null, segmentInfo.getReturnOndGroupId(),
							refSegIdRPHMap.get(segmentInfo.getFlightSegId()), segmentInfo.getSelectedBundledFarePeriodId(), null, null);
				}
			}

			// Handle exception
			ServiceResponce serviceResponce = null;
			Collection collBlockID = null;
			String pnr = null;
			collBlockID = WebServicesModuleUtils.getReservationBD().blockSeats(selectedOndCollection, null);

			TrackInfoDTO trackInfoDTO = MyIDTrvlCommonUtil.getTrackInfo();
			serviceResponce = WebServicesModuleUtils.getReservationBD().createOnHoldReservation(iReservation, collBlockID, false,
					trackInfoDTO, true);
			pnr = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);

			otaAirBookRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
			otaAirBookRS.setEchoToken(otaAirBookRQ.getEchoToken());
			otaAirBookRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

			AirReservation airReservation = new AirReservation();

			String segmentStatus = null;

			if (BookingClass.BookingClassType.STANDBY.equals(availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0)
					.getPreferredBookingType())) {
				segmentStatus = CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_LIST_SPACE_AVAILABLE);
			} else {
				segmentStatus = CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_OK);
			}

			// All the segments are bookable
			for (OriginDestinationOptionType originDestinationOption : otaAirBookRQ.getAirItinerary()
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
					bookFlightSegment.setStatus(segmentStatus);
				}

			}

			airReservation.setAirItinerary(otaAirBookRQ.getAirItinerary());
			airReservation.setTravelerInfo(otaAirBookRQ.getTravelerInfo());
			otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(new SuccessType());
			otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);
			UniqueIDType uniqueIDType = new UniqueIDType();
			uniqueIDType.setID(pnr);
			uniqueIDType.setType(WebservicesConstants.MyIDTravelConstants.SEGMENT_SELL);
			airReservation.getBookingReferenceID().add(uniqueIDType);
		}

		return myIDSegSellRes;

	}

	private static AvailableFlightSearchDTO extractPriceQuoteReqParamsForSegSell(AirItineraryType otaItinerary,
			TravelerInfoType travellers, Map<Integer, String> refSegIdRPHMap) throws ModuleException, WebservicesException {

		int adults = 0;
		int children = 0;
		int infants = 0;

		TravelerInfoType travelerInfo = (TravelerInfoType) travellers;
		List<AirTravelerType> airTravelers = travelerInfo.getAirTraveler();

		for (AirTravelerType singleTraveller : airTravelers) {

			if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(
					singleTraveller.getPassengerTypeCode())) {
				adults++;

			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(
					singleTraveller.getPassengerTypeCode())) {
				children++;

			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
					singleTraveller.getPassengerTypeCode())) {
				infants++;
			}

		}

		if (adults == 0 && children == 0) {
			log.error("Invalid segment sell request: " + "Invalid adult/children pax count. " + "[adults pax=" + adults + "]"
					+ "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult and Children pax count cannot be zero");
		}

		if (adults < infants) {
			log.error("Invalid segment sell request: " + "Invalid adult/infant pax count. " + "[adults pax=" + adults + "]"
					+ "[infant pax=" + infants + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Adult count should be greater than or equal to infant count");
		}

		int[] paxCounts = { adults, children, infants };
		return MyIDTrvlCommonUtil.extractPriceQuoteReqParams(otaItinerary, paxCounts, refSegIdRPHMap);

	}
	

	private static IReservation composeReservationAssembler(Collection<OndFareDTO> ondFares, OTAAirBookRQ otaAirBookRQ,
			String employeeRPH, ServiceTaxCalculator serviceTaxCalculator) throws WebservicesException, ModuleException {

		IReservation iReservation = null;
		iReservation = new ReservationAssembler(ondFares, null);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		Date holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(ReleaseTimeUtil.getFlightDepartureInfo(ondFares),
				privilegeKeys, true, null, AppIndicatorEnum.APP_MYID.toString(),
				ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(ondFares));

		List<Integer> parentList = new ArrayList<Integer>();
		List<Integer> infantList = new ArrayList<Integer>();

		Map<String, List<ExternalChgDTO>> selectedBaggageCharges = null;

		if (WebServicesModuleUtils.getMyIDTravelConfig().getAutoBaggageSelectionEnabled()) {
			selectedBaggageCharges = MyIDTrvlCommonUtil.getPaxwiseBaggageExternalCharges(ondFares,
					MyIDTrvlCommonUtil.getAdultChildRPHs(otaAirBookRQ.getTravelerInfo().getAirTraveler()));
			serviceTaxCalculator.setQuotedExternalCharges(selectedBaggageCharges);
			serviceTaxCalculator.calculate();
		}
		
				
		String contactFirstName = null;
		String contactLastName = null;
		String contactTitle = null;

		boolean contactNameInfoSelected = false;
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

		// What'll happen if booking cannot be made onhold
		if (holdReleaseTimestamp != null) {
			iReservation.setPnrZuluReleaseTimeStamp(holdReleaseTimestamp);

			for (AirTravelerType traveler : otaAirBookRQ.getTravelerInfo().getAirTraveler()) {

				Integer paxSequence = new Integer(traveler.getTravelerRefNumber().getRPH());
				if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					if (traveler.isAccompaniedByInfant()) {
						parentList.add(paxSequence);
					}

				} else if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infantList.add(paxSequence);
				}

				if (!contactNameInfoSelected && traveler.getTravelerRefNumber().getRPH().equals(employeeRPH)) {
					contactNameInfoSelected = true;
				}

			}

			for (AirTravelerType traveler : otaAirBookRQ.getTravelerInfo().getAirTraveler()) {

				Integer paxSequence = new Integer(traveler.getTravelerRefNumber().getRPH());

				// SSRs to be handled later
				SegmentSSRAssembler ssrAssembler = new SegmentSSRAssembler();

				Date dateOfBirth = traveler.getBirthDate() != null
						? traveler.getBirthDate().toGregorianCalendar().getTime()
						: null; // birth date

				String firstName = CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0);
				String lastName = traveler.getPersonName().getSurname();
				String title = CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0);
				Integer nationalityCode = null;

				IPayment paymentAsm = null;

//				if (!traveler.getPassengerTypeCode().equals(
//						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					paymentAsm = new PaymentAssembler();

					if (selectedBaggageCharges != null && !selectedBaggageCharges.isEmpty()) {
						paymentAsm.addExternalCharges(selectedBaggageCharges.get(traveler.getTravelerRefNumber().getRPH()));
					}

					if (contactNameInfoSelected && traveler.getTravelerRefNumber().getRPH().equals(employeeRPH)
							|| !contactNameInfoSelected) {
						contactFirstName = firstName;
						contactLastName = lastName;
						contactTitle = title;
						contactNameInfoSelected = true;
					}
//				}

				if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					if (traveler.isAccompaniedByInfant()) {

						int infantSeqId = infantList.get(parentList.indexOf(paxSequence));
						iReservation.addParent(firstName, lastName, title, dateOfBirth,
								null, // nationalityCode
								paxSequence, infantSeqId, paxAdditionalInfoDTO, null, paymentAsm, ssrAssembler, null, null, null,
								null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
					} else {
						iReservation.addSingle(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence,
								paxAdditionalInfoDTO, null, paymentAsm, ssrAssembler, null, null, null, null,
								CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
					}
				} else if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					iReservation.addChild(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence,
							paxAdditionalInfoDTO, null, paymentAsm, ssrAssembler, null, null, null, null,
							CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				} else if (traveler.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					int parentSequence = parentList.get(infantList.indexOf(paxSequence));
					iReservation.addInfant(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence, parentSequence,
							paxAdditionalInfoDTO, null, paymentAsm, new SegmentSSRAssembler(), null, null, null,
							null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				}
			}

			ReservationAdminInfoTO reservationAdminInfoTO = new ReservationAdminInfoTO();
			reservationAdminInfoTO.setLastSalesTerminal(AppIndicatorEnum.APP_MYID.toString());
			reservationAdminInfoTO.setOriginSalesTerminal(AppIndicatorEnum.APP_MYID.toString());
			iReservation.setReservationAdminInfoTO(reservationAdminInfoTO);

			ReservationContactInfo contactInfo = new ReservationContactInfo();
			contactInfo.setFirstName(contactFirstName);
			contactInfo.setLastName(contactLastName);
			contactInfo.setTitle(contactTitle);
			contactInfo.setPreferredLanguage(WebservicesConstants.MyIDTravelConstants.DEFAULT_LANG);
			iReservation.addContactInfo("", contactInfo, null);
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Segment sell cannot perform");
		}

		return iReservation;
	}
}
