package com.isa.thinair.webservices.core.util;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.WarningType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isaaviation.thinair.webservices.api.airinventory.AAStatusErrorsAndWarnings;

/**
 * Utility for preparing Error/Warning objects
 * 
 * @author Mohamed Nasly, Byorn
 */
public class ExceptionUtil {

	private static final Log log = LogFactory.getLog(ExceptionUtil.class);

	/**
	 * Prepares a colleciton of ErrorType from WebservicesException
	 * 
	 * @param wse
	 *            WebservicesException
	 * @return ErrorType collection
	 */
	public static AAStatusErrorsAndWarnings extractAAErrorsFromWSException(WebservicesException wse) {

		/**
		 * @todo : Presently we are sending one AAStatusErrorsAndWarnings at a time and not a collection.
		 */
		AAStatusErrorsAndWarnings statusErrorsAndWarnings = new AAStatusErrorsAndWarnings();

		if (wse.getCause() != null && wse.getCause() instanceof ModuleException) {
			ModuleException me = (ModuleException) wse.getCause();
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			return statusErrorsAndWarnings = ExceptionUtil.getMappedAAError(me, details);

		}

		for (String otaErrorCode : wse.getOtaErrorCodes().keySet()) {
			return statusErrorsAndWarnings = ExceptionUtil.getAAError(otaErrorCode, wse.getOtaErrorCodes().get(otaErrorCode));
		}

		return statusErrorsAndWarnings;
	}

	/**
	 * Prepares a colleciton of ErrorType from WebservicesException
	 * 
	 * @param wse
	 *            WebservicesException
	 * @return ErrorType collection
	 */
	public static Collection<ErrorType> extractOTAErrorsFromWSException(WebservicesException wse) {
		Collection<ErrorType> errors = new ArrayList<ErrorType>();

		for (ErrorType error : wse.getOtaErrors()) {
			errors.add(error);
		}

		if (wse.getCause() != null && wse.getCause() instanceof ModuleException) {
			ModuleException me = (ModuleException) wse.getCause();
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			errors.add(ExceptionUtil.getMappedOTAError(me, details));
		}

		for (String otaErrorCode : wse.getOtaErrorCodes().keySet()) {
			errors.add(ExceptionUtil.getOTAError(otaErrorCode, wse.getOtaErrorCodes().get(otaErrorCode)));
		}

		return errors;
	}

	/**
	 * Add ErrorType and/or Warning Type from Exception to the collections
	 * 
	 * @param errorsCollection
	 *            ErrorType objects are added to this collection
	 * @param warningsCollection
	 *            WarningType objects are added to this collection
	 * @param exception
	 *            Errors/Warnings are extracted from this exception
	 */
	@SuppressWarnings("unchecked")
	public static void addOTAErrrorsAndWarnings(Collection errorsCollection, Collection warningsCollection, Exception exception) {
		if (exception != null && exception instanceof WebservicesException) {
			if (errorsCollection != null) {
				Collection<ErrorType> errors = extractOTAErrorsFromWSException((WebservicesException) exception);
				for (ErrorType error : errors) {
					errorsCollection.add(error);
				}

				if (errorsCollection.isEmpty()) {
					errorsCollection.add(getOTAError(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED,
							exception != null ? "Error StackTrace = " + getStackTrace(exception, 1) : "no exception details"));
				}
			}
			if (warningsCollection != null) {
				Collection<WarningType> warnigs = ((WebservicesException) exception).getOTAWarnings();
				for (WarningType warning : warnigs) {
					warningsCollection.add(warning);
				}
			}

			if (errorsCollection == null && warningsCollection == null) {
				errorsCollection.add(getOTAError(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED,
						exception != null ? "Error StackTrace = " + getStackTrace(exception, 1) : "no exception details"));
			}
		} else if (exception != null && exception instanceof ModuleException) {
			ModuleException me = (ModuleException) exception;
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			errorsCollection.add(ExceptionUtil.getMappedOTAError(me, details));
		} else {
			errorsCollection.add(getOTAError(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED,
					exception != null ? "Error StackTrace = " + getStackTrace(exception, 1) : "no exception details"));
		}
	}

	/**
	 * Add ErrorType and/or Warning Type from Exception to the collections
	 * 
	 * @param errorsCollection
	 *            ErrorType objects are added to this collection
	 * @param warningsCollection
	 *            WarningType objects are added to this collection
	 * @param exception
	 *            Errors/Warnings are extracted from this exception
	 */
	@SuppressWarnings("unchecked")
	public static AAStatusErrorsAndWarnings addAAErrrorsAndWarnings(Exception exception) {

		if (exception != null && exception instanceof WebservicesException) {
			return extractAAErrorsFromWSException((WebservicesException) exception);
		}

		if (exception != null && exception instanceof ModuleException) {
			ModuleException me = (ModuleException) exception;
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			return ExceptionUtil.getMappedAAError(me, details);
		} else {
			return getAAError(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED,
					exception != null ? "Error StackTrace = " + getStackTrace(exception, 1) : "no exception details");
		}
	}

	/**
	 * Add ErrorType and/or Warning Type from Exception to the collections
	 * 
	 * @param errorsCollection
	 *            ErrorType objects are added to this collection
	 * @param warningsCollection
	 *            WarningType objects are added to this collection
	 * @param exception
	 *            Errors/Warnings are extracted from this exception
	 */
	@SuppressWarnings("unchecked")
	public static com.isaaviation.thinair.webservices.api.receiptupdate.AAStatusErrorsAndWarnings
			addAAErrrorsAndWarningsForReceiptUpdate(Exception exception) {
		com.isaaviation.thinair.webservices.api.receiptupdate.AAStatusErrorsAndWarnings statusErrorsAndWarnings = new com.isaaviation.thinair.webservices.api.receiptupdate.AAStatusErrorsAndWarnings();
		AAStatusErrorsAndWarnings aaStatusErrorsAndWarnings = addAAErrrorsAndWarnings(exception);
		statusErrorsAndWarnings.setErrorCode(aaStatusErrorsAndWarnings.getErrorCode());
		statusErrorsAndWarnings.setErrorText(aaStatusErrorsAndWarnings.getErrorText());
		statusErrorsAndWarnings.setSuccess(false);
		statusErrorsAndWarnings.setWarningMessage(aaStatusErrorsAndWarnings.getWarningMessage());
		return statusErrorsAndWarnings;
	}

	/**
	 * Get the OTA Error mapped to ModuleException If no mapping found, OTA "General failure occurred" error is
	 * returned. Mapping are defined in <project-src-dir>/resources/ota_codes/AAOTAErrorCodesMapping_en.properties
	 * property file
	 * 
	 * @param moduleException
	 *            ModuleException for which mapped OTA code is needed
	 * @param contextInfo
	 *            Any value at the context of error that helps figuring out the exact errorneous condition
	 * @return ErrorType OTA Error mapped to the ModuleException
	 */
	public static ErrorType getMappedOTAError(ModuleException moduleException, String contextInfo) {
		String otaCode = "";
		try {
			otaCode = getAAOTACodesMapping().getString(moduleException.getExceptionCode()).trim();
		} catch (MissingResourceException mre) {
			otaCode = IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED;
			if (contextInfo == null) {
				contextInfo = "Internal ErrorCode = " + moduleException.getExceptionCode() + WebservicesConstants.NEWLINE
						+ " Error StackTrace = " + getStackTrace(moduleException, 1);
			} else {
				contextInfo = "Internal ErrorCode = " + moduleException.getExceptionCode() + WebservicesConstants.NEWLINE
						+ contextInfo + WebservicesConstants.NEWLINE + " Error StackTrace = " + getStackTrace(moduleException, 1);
			}
			log.warn("No OTA codes is mapped for [" + moduleException.getExceptionCode() + "], using " + otaCode);
		}
		return getOTAError(otaCode, contextInfo);
	}

	/**
	 * Get the AA Error mapped to ModuleException If no mapping found, AA "General failure occurred" error is returned.
	 * Mapping are defined in <project-src-dir>/resources/ota_codes/AAErrorCodesMapping_en.properties property file
	 * 
	 * @param moduleException
	 *            ModuleException for which mapped AA code is needed
	 * @param contextInfo
	 *            Any value at the context of error that helps figuring out the exact errorneous condition
	 * @return ErrorType OTA Error mapped to the ModuleException
	 */
	public static AAStatusErrorsAndWarnings getMappedAAError(ModuleException moduleException, String contextInfo) {
		String aaCode = "";
		try {
			aaCode = getAACodesMapping().getString(moduleException.getExceptionCode()).trim();
		} catch (MissingResourceException mre) {
			aaCode = IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED;
			if (contextInfo == null) {
				contextInfo = "Internal ErrorCode = " + moduleException.getExceptionCode() + WebservicesConstants.NEWLINE
						+ " Error StackTrace = " + getStackTrace(moduleException, 1);
			} else {
				contextInfo = "Internal ErrorCode = " + moduleException.getExceptionCode() + WebservicesConstants.NEWLINE
						+ contextInfo + WebservicesConstants.NEWLINE + " Error StackTrace = " + getStackTrace(moduleException, 1);
			}
			log.warn("No AA codes is mapped for [" + moduleException.getExceptionCode() + "], using " + aaCode);
		}
		return getAAError(aaCode, contextInfo);
	}

	/**
	 * Get the ErrorType for AA code AA codes are defined in the
	 * <project-src-dir>/resources/aa_codes/AA_CodeTable20060606.xml file
	 * 
	 * @param aaErrorCode
	 *            AA code for which details are needed
	 * @param contextInfo
	 *            Any value at the context of error that helps figuring out the exact errorneous condition
	 * @return ErrorType AA Error object for AA code
	 */
	public static AAStatusErrorsAndWarnings getAAError(String aaErrorCode, String contextInfo) {
		String errorData = getAACodesMap().getString(aaErrorCode);
		if (errorData == null) {
			errorData = getAACodesMap().getString(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED);
			log.warn("Undefined AA error code requested [" + aaErrorCode + "], using "
					+ IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED);

			if (errorData == null) {
				throw new RuntimeException(
						"AA Code table entry for " + IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED + "is not found");
			}
		}
		AAStatusErrorsAndWarnings statusErrorsAndWarnings = new AAStatusErrorsAndWarnings();
		statusErrorsAndWarnings.setErrorCode(aaErrorCode);
		statusErrorsAndWarnings.setErrorText(errorData);
		statusErrorsAndWarnings.setWarningMessage(errorData);
		// errorType.setType(errorData[0]);

		if (contextInfo != null && contextInfo.length() > 0) {
			statusErrorsAndWarnings.setErrorText(errorData + " [" + contextInfo + "]");
		}

		return statusErrorsAndWarnings;
	}

	/**
	 * Get the ErrorType for OTA code OTA codes are defined in the
	 * <project-src-dir>/resources/ota_codes/OTA_CodeTable20060606.xml file
	 * 
	 * @param otaErrorCode
	 *            OTA code for which details are needed
	 * @param contextInfo
	 *            Any value at the context of error that helps figuring out the exact errorneous condition
	 * @return ErrorType OTA Error object for OTA code
	 */
	public static ErrorType getOTAError(String otaErrorCode, String contextInfo) {
		String[] errorData = getOTACodesMap().get(otaErrorCode);
		if (errorData == null) {
			errorData = getOTACodesMap().get(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED);
			log.warn("Undefined OTA error code requested [" + otaErrorCode + "], using "
					+ IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED);

			if (errorData == null) {
				throw new RuntimeException(
						"OTA Code table entry for " + IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED + "is not found");
			}
		}

		ErrorType errorType = null;
		errorType = new ErrorType();
		errorType.setType(errorData[0]);
		errorType.setCode(errorData[1]);
		errorType.setShortText(errorData[2]);

		if (contextInfo != null && contextInfo.length() > 0) {
			errorType.setShortText(errorData[2] + " [" + contextInfo + "]");
		}

		return errorType;
	}

	/**
	 * Get the exception stack trace as a String
	 * 
	 * @param ex
	 * @return Stack Trace
	 */
	private static String getStackTrace(Exception ex, int lines) {
		StackTraceElement[] stackTrace = ex.getStackTrace();
		StringBuffer stackBuf = new StringBuffer();
		if (stackTrace != null) {
			for (int i = 0; i < lines; i++) {
				stackBuf.append(stackTrace[i].toString()).append(WebservicesConstants.NEWLINE);
			}
		}
		return stackBuf.toString();
	}

	private static ResourceBundle getAAOTACodesMapping() {
		if (aaOTACodesMappings == null) {
			initializeData();
		}
		return aaOTACodesMappings;
	}

	private static ResourceBundle getAACodesMapping() {
		if (aaCodesMappings == null) {
			initializeAAData();
		}
		return aaCodesMappings;
	}

	private static HashMap<String, String[]> getOTACodesMap() {
		if (otaCodesMap == null) {
			initializeData();
		}
		return otaCodesMap;
	}

	private static ResourceBundle getAACodesMap() {
		if (aaCodesMap == null) {
			initializeAAData();
		}
		return aaCodesMap;
	}

	/**
	 * Initializes AA codes tables and AA-ModuleException mappings
	 */
	public static void initializeAAData() {
		synchronized (lock) {
			if (aaCodesMap == null) {
				Locale locale = new Locale("en");
				aaCodesMap = ResourceBundle.getBundle(AA_CODETABLES_FILE, locale);
				// aaCodesMap = new HashMap<String, String[]>();
				// initializeAACodesMap(aaCodesMap);
			}

			if (aaCodesMappings == null) {
				Locale locale = new Locale("en");
				aaCodesMappings = ResourceBundle.getBundle(AAERRORS_AACODE_MAPPINGS_RESOURCE_BUNDLE, locale);
			}
		}
	}

	/**
	 * Initializes OTA codes tables and OTA-ModuleException mappings
	 */
	public static void initializeData() {
		synchronized (lock) {
			if (otaCodesMap == null) {
				otaCodesMap = new HashMap<String, String[]>();
				initializeOTACodesMap(otaCodesMap);
			}

			if (aaOTACodesMappings == null) {
				Locale locale = new Locale("en");
				aaOTACodesMappings = ResourceBundle.getBundle(AAERRORS_OTACODE_MAPPINGS_RESOURCE_BUNDLE, locale);
			}
		}
	}

	private static void initializeOTACodesMap(HashMap<String, String[]> otaCodesMap) {
		try {
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			OTACodeTablesHandler h = new OTACodeTablesHandler(otaCodesMap);
			log.debug("Going to initialize OTA code tables [file=" + OTA_CODETABLES_FILE + "]");
			ClassLoader loader = new DefaultClassLoaderStrategy().getClassLoader(ExceptionUtil.class);
			InputStream is = new BufferedInputStream(loader.getResourceAsStream(OTA_CODETABLES_FILE));
			parser.parse(is, h);
			log.debug("Completed OTA code tables initialization.");
		} catch (Exception ex) {
			throw new RuntimeException("Parsing OTA code tables failed.", ex);
		}
	}

	private static Object lock = new Object();
	private static HashMap<String, String[]> otaCodesMap = null;
	private static ResourceBundle aaCodesMap = null;
	private static ResourceBundle aaOTACodesMappings = null;
	private static ResourceBundle aaCodesMappings = null;
	private static String OTA_CODETABLES_FILE = "resources/ota_codes/OTA_CodeTable20060606.xml";
	private static String AA_CODETABLES_FILE = "resources/aa_codes/AAErrorCodes";
	private static String AAERRORS_OTACODE_MAPPINGS_RESOURCE_BUNDLE = "resources/ota_codes/AAOTAErrorCodesMapping";
	private static String AAERRORS_AACODE_MAPPINGS_RESOURCE_BUNDLE = "resources/aa_codes/AAErrorCodesMappings";

	/**
	 * Parses OTA Code Tables
	 * 
	 * @auther Mohamed Nasly
	 */
	public static class OTACodeTablesHandler extends DefaultHandler {
		HashMap<String, String[]> otaCodesMap;

		private String QNAME_OTA_CodeTable = "OTA_CodeTable";
		private String QNAME_Code = "Code";
		private String QNAME_CodeContent = "CodeContent";
		private int count = 0;

		private String codeTableNameCode = "";
		private String codeValue = "";
		private String codeName = "";

		public OTACodeTablesHandler(HashMap<String, String[]> otaCodesMap) {
			this.otaCodesMap = otaCodesMap;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals(QNAME_OTA_CodeTable)) {
				codeTableNameCode = attributes.getValue("NameCode");
			} else if (qName.equals(QNAME_Code)) {
				codeValue = attributes.getValue("Value");
			} else if (qName.equals(QNAME_CodeContent)) {
				codeName = attributes.getValue("Name");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equals(QNAME_Code)) {
				otaCodesMap.put(codeTableNameCode + "." + codeValue, new String[] { codeTableNameCode, codeValue, codeName });
				++count;
				// if (log.isDebugEnabled()) {
				// log.debug("added [" + codeTableNameCode + "_" + codeValue + "_" + codeName + "]");
				// }
			}
		}

		@Override
		public void endDocument() throws SAXException {
			super.endDocument();
			if (log.isDebugEnabled()) {
				log.debug("Total of " + count + " codes loaded.");
			}
		}

	}

	/**
	 * Parses AA Code Tables
	 * 
	 * @auther Byorn
	 */
	public static class AACodeTablesHandler extends DefaultHandler {
		HashMap<String, String[]> aaCodesMap;

		private String QNAME_AA_CodeTable = "AA_CodeTable";
		private String QNAME_Code = "Code";
		private String QNAME_CodeContent = "CodeContent";
		private int count = 0;

		private String codeTableNameCode = "";
		private String codeValue = "";
		private String codeName = "";

		public AACodeTablesHandler(HashMap<String, String[]> aaCodesMap) {
			this.aaCodesMap = aaCodesMap;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals(QNAME_AA_CodeTable)) {
				codeTableNameCode = attributes.getValue("NameCode");
			} else if (qName.equals(QNAME_Code)) {
				codeValue = attributes.getValue("Value");
			} else if (qName.equals(QNAME_CodeContent)) {
				codeName = attributes.getValue("Name");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equals(QNAME_Code)) {
				otaCodesMap.put(codeTableNameCode + "." + codeValue, new String[] { codeTableNameCode, codeValue, codeName });
				++count;
				// if (log.isDebugEnabled()) {
				// log.debug("added [" + codeTableNameCode + "_" + codeValue + "_" + codeName + "]");
				// }
			}
		}

		@Override
		public void endDocument() throws SAXException {
			super.endDocument();
			if (log.isDebugEnabled()) {
				log.debug("Total of " + count + " codes loaded.");
			}
		}

	}
	
	public static void triggerPreProcessErrorIfPresent() throws WebservicesException {
		Boolean triggerError = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_PREPROCESS_ERROR);
		if (triggerError != null && triggerError.booleanValue()) {
			throw (WebservicesException) ThreadLocalData.getWSContextParam(
					WebservicesContext.PREPROCESS_ERROR_OBJECT);
		}

	}
	
	public static void setTriggerPostProcessError(Throwable t) {
		ThreadLocalData.setTriggerPostProcessError(t);
	}

}
