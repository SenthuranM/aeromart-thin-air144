package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.opentravel.ota._2003._05.AABundledServiceExt;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BundledService;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.PricingSourceType;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * Prepare PricedItinerary from SelectedFlightDTO
 * 
 * @author Mohamed Nasly
 */
public class PricedItineraryUtil {

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public static void preparePricedItinerary(PricedItineraryType pricedItinerary, SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean isFixedPriceQuote, String pricingKey,
			FlexiFareSelectionCriteria flexiSelectionCriteria) throws WebservicesException, ModuleException {
		// set flight segments info
		int fareType = selectedFlightDTO.getFareType();

		boolean isOutboundHasONDFare = false;
		boolean isInboudHasONDFare = false;
		if (fareType == FareTypes.OND_FARE || fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {
			isOutboundHasONDFare = true;
		}
		boolean showBookingClass = (availableFlightSearchDTO.getBookingClassCode() != null ? true : false);

		addOriginDestinationOptions(pricedItinerary, selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND),
				isOutboundHasONDFare, pricingKey, showBookingClass);

		if (selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND) != null) {
			if (fareType == FareTypes.OND_FARE || fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE) {
				isInboudHasONDFare = true;
			}
			addOriginDestinationOptions(pricedItinerary, selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND),
					isInboudHasONDFare, pricingKey, showBookingClass);
		}

		// set individual passenger fare/taxes breakdowns and total fare/taxes
		setItineraryPricingInfo(pricedItinerary, selectedFlightDTO, availableFlightSearchDTO, isFixedPriceQuote, pricingKey,
				flexiSelectionCriteria);
	}

	/**
	 * Sets indivdual fare breakdowns and total price for itinerary
	 * 
	 * @param pricedItinerary
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 *            : The flexibility selection criteria.
	 * 
	 * @throws WebservicesException
	 */
	private static void setItineraryPricingInfo(PricedItineraryType pricedItinerary, SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean isFixedFare, String pricingKey,
			FlexiFareSelectionCriteria flexiSelectionCriteria) throws WebservicesException, ModuleException {

		// fares and tax for single adult pax is set
		AirItineraryPricingInfoType airItineraryPricingInfo = new AirItineraryPricingInfoType();
		pricedItinerary.setAirItineraryPricingInfo(airItineraryPricingInfo);
		airItineraryPricingInfo.setPricingSource(PricingSourceType.PUBLISHED);

		// Calculating the Handling fare for the agent
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		@SuppressWarnings("unchecked")
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MAKE_ONLY);

		// calculate the handling fee
		if (externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE) != null) {
			calculateHandlingFee(selectedFlightDTO, availableFlightSearchDTO,
					externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE), isFixedFare);
		}

		// set Passenger Type, fare and taxes/surcharges breakdown
		setPassengerFareBreakdowns(airItineraryPricingInfo, selectedFlightDTO, availableFlightSearchDTO, externalChargesMap,
				isFixedFare, pricingKey, flexiSelectionCriteria);
	}

	/**
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param externalChgDTO
	 * @param isFixedFare
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void calculateHandlingFee(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, ExternalChgDTO externalChgDTO, boolean isFixedFare)
			throws WebservicesException, ModuleException {

		PaxSet paxSet = new PaxSet(availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getChildCount(),
				availableFlightSearchDTO.getInfantCount());

		ExternalChargeUtil.calculateExternalChargeAmount(selectedFlightDTO.getOndFareDTOs(false), paxSet, externalChgDTO,
				availableFlightSearchDTO.isReturnFlag());

	}

	/**
	 * set Bundled Services data
	 * 
	 * @param pricedItinerary
	 * @param availableIBOBFlightSegment
	 * @param isSingleFare
	 * @param pricingKey
	 * @param showBookingClass
	 */
	private static void addBundledServices(AirItineraryType.OriginDestinationOptions originDestinationOptions,
			AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		String selectedLogicalCC = availableIBOBFlightSegment.getSelectedLogicalCabinClass();
		Map<String, List<BundledFareLiteDTO>> logicalCabinBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		if (logicalCabinBundledFares != null && logicalCabinBundledFares.containsKey(selectedLogicalCC)) {
			List<BundledFareLiteDTO> availableBundledFares = logicalCabinBundledFares.get(selectedLogicalCC);
			if (availableBundledFares != null && !availableBundledFares.isEmpty()) {

				AABundledServiceExt aaBundledServiceExt = new AABundledServiceExt();
				aaBundledServiceExt.setApplicableOnd(availableIBOBFlightSegment.getOndCode());
				aaBundledServiceExt.setApplicableOndSequence(availableIBOBFlightSegment.getOndSequence());
				originDestinationOptions.getAABundledServiceExt().add(aaBundledServiceExt);

				for (BundledFareLiteDTO bundledFareLiteDTO : availableBundledFares) {
					if (bundledFareLiteDTO != null) {
						BundledService bundledService = new BundledService();
						bundledService.setBunldedServiceId(bundledFareLiteDTO.getBundleFarePeriodId());
						bundledService.setBundledServiceName(bundledFareLiteDTO.getBundledFareName());
						bundledService.setDescription(bundledFareLiteDTO.getDescription());
						if (bundledFareLiteDTO.getBookingClasses() != null) {
							bundledService.getBookingClasses().addAll(bundledFareLiteDTO.getBookingClasses());
						}
						bundledService.setPerPaxBundledFee(bundledFareLiteDTO.getPerPaxBundledFee());
						bundledService.getIncludedServies().addAll(bundledFareLiteDTO.getFreeServices());

						aaBundledServiceExt.getBundledService().add(bundledService);
					}
				}
			}
		}
	}

	/**
	 * Set flight segments information
	 * 
	 * @param originDestinationOptions
	 * @param aaFlightSegmentDTOs
	 * @param isSingleFare
	 * @param pricingKey
	 * @param bookingClass
	 * @throws ModuleException
	 */
	private static void addFlightSegments(AirItineraryType.OriginDestinationOptions originDestinationOptions,
			Collection<FlightSegmentDTO> aaFlightSegmentDTOs, boolean isSingleFare, String pricingKey, String bookingClass)
			throws ModuleException {

		OriginDestinationOptionType originDestinationOption = null;
		for (FlightSegmentDTO aaFlightSegmentDTO : aaFlightSegmentDTOs) {
			if (!isSingleFare || (isSingleFare && originDestinationOption == null)) {
				originDestinationOption = new OriginDestinationOptionType();
				originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
			}

			BookFlightSegmentType bookFlightSegment = new BookFlightSegmentType();
			originDestinationOption.getFlightSegment().add(bookFlightSegment);

			bookFlightSegment.setRPH(aaFlightSegmentDTO.getSegmentId().toString() + "-" + pricingKey);

			GregorianCalendar departureDateTimeCal = new GregorianCalendar();
			departureDateTimeCal.setTime(aaFlightSegmentDTO.getDepartureDateTime());
			bookFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));

			GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
			arrivalDateTimeCal.setTime(aaFlightSegmentDTO.getArrivalDateTime());
			bookFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

			bookFlightSegment.setFlightNumber(aaFlightSegmentDTO.getFlightNumber());

			String[] segmentCodeList = aaFlightSegmentDTO.getSegmentCode().split("/");

			FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
			departureAirport.setLocationCode(segmentCodeList[0]);
			departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);

			FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
			arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
			arrivalAirport.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);

			bookFlightSegment.setDepartureAirport(departureAirport);
			bookFlightSegment.setArrivalAirport(arrivalAirport);
			if (bookingClass != null) {
				bookFlightSegment.setResBookDesigCode(bookingClass);
			}
			if (segmentCodeList.length > 2) {
				bookFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}
		}
	}

	/**
	 * Sets Origin Destination Options information
	 * 
	 * @param pricedItinerary
	 * @param showBookingClass
	 * @param aaFlightSegmentDTOs
	 * @throws WebservicesException
	 */
	private static void addOriginDestinationOptions(PricedItineraryType pricedItinerary,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, boolean isSingleFare, String pricingKey,
			boolean showBookingClass) throws WebservicesException, ModuleException {
		Collection<FlightSegmentDTO> aaFlightSegmentDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();
		String bookingClass = null;
		if (showBookingClass && availableIBOBFlightSegment.getSelectedLogicalCabinClass() != null) {
			bookingClass = availableIBOBFlightSegment.getFare(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
					.getBookingClassCode();
		}
		AirItineraryType.OriginDestinationOptions originDestinationOptions = pricedItinerary.getAirItinerary()
				.getOriginDestinationOptions();

		addFlightSegments(originDestinationOptions, aaFlightSegmentDTOs, isSingleFare, pricingKey, bookingClass);
		addBundledServices(originDestinationOptions, availableIBOBFlightSegment);
	}

	/**
	 * Sets total fare, total taxes and total surcharges(fees)
	 * 
	 * @param airItineraryPricingInfo
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 *            TODO
	 * @throws WebservicesException
	 */
	private static void setTotalFaresForItinerary(AirItineraryPricingInfoType airItineraryPricingInfo,
			SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, boolean isFixedFare, String pricingKey,
			AirItineraryPricingInfoType.PTCFareBreakdowns ptcFareBreakdowns, BigDecimal totalInSelectedCurByFareBreakdown,
			FlexiFareSelectionCriteria flexiSelectionCriteria, BigDecimal totalSelectedExternalCharges,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException, WebservicesException {
		FareType itinearyFareInfo = new FareType();
		airItineraryPricingInfo.setItinTotalFare(itinearyFareInfo);

		// CC charge as a fixed value -
		int totalPayablePaxCount = availableFlightSearchDTO.getAdultCount() + availableFlightSearchDTO.getChildCount();
		int totalSegmentCount = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND).getFlightSegmentDTOs().size()
				+ (selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND) != null ? selectedFlightDTO
						.getSelectedOndFlight(OndSequence.IN_BOUND).getFlightSegmentDTOs().size() : 0);

		// set total base fare
		FareType.BaseFare baseFare = new FareType.BaseFare();
		BigDecimal totalBaseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		itinearyFareInfo.setBaseFare(baseFare);

		FareType.TotalFare totalFare = new FareType.TotalFare();
		itinearyFareInfo.setTotalFare(totalFare);
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();
		String currencyCode = null;
		String equiCurrencyCode = null;

		if (showPriceInselectedCurrency) {
			if (availableFlightSearchDTO.getPreferredCurrencyCode() != null
					&& OTAUtils.isPreferredCurrencyCodeSupportedByPC(availableFlightSearchDTO.getPreferredCurrencyCode())) {
				currencyCode = availableFlightSearchDTO.getPreferredCurrencyCode();
				equiCurrencyCode = principal.getAgentCurrencyCode();
			} else {
				currencyCode = principal.getAgentCurrencyCode();
			}

			// Adult Fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(WSReservationUtil
						.getAmountInSpecifiedCurrency(
								AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT)),
								currencyCode, exchangeRateProxy), new BigDecimal(availableFlightSearchDTO.getAdultCount())));
			}

			// Child Fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(WSReservationUtil
						.getAmountInSpecifiedCurrency(
								AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD)),
								currencyCode, exchangeRateProxy), new BigDecimal(availableFlightSearchDTO.getChildCount())));
			}

			// Infant fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(WSReservationUtil
						.getAmountInSpecifiedCurrency(
								AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT)),
								currencyCode, exchangeRateProxy), new BigDecimal(availableFlightSearchDTO.getInfantCount())));
			}

			// Set total BaseFare
			OTAUtils.setAmountAndCurrency(baseFare, totalBaseFare, currencyCode);

			BigDecimal[] totals = ReservationUtil.getQuotedTotalPrices(selectedFlightDTO.getOndFareDTOs(false),
					availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getChildCount(),
					availableFlightSearchDTO.getInfantCount());

			// Adding Handling fare to the total price. For now only external charge is the handling fee
			BigDecimal handleCharge = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount();
			handleCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handleCharge);

			// BigDecimal jnTaxFor

			BigDecimal totalPrice = AccelAeroCalculator.add(totals[0], handleCharge);
			totalPrice = AccelAeroCalculator.add(totalPrice, totalSelectedExternalCharges);

			BigDecimal handlingChargeInSelCur = WSReservationUtil.getAmountInSpecifiedCurrency(handleCharge, currencyCode,
					exchangeRateProxy);
			totalInSelectedCurByFareBreakdown = AccelAeroCalculator
					.add(totalInSelectedCurByFareBreakdown, handlingChargeInSelCur);

			// adding the selected flexi charges for the price quote
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				BigDecimal totalFlexiChargesForPriceQuote = WSFlexiFareUtil.calculateTotalFlexiFareForPriceQuote(
						selectedFlightDTO, availableFlightSearchDTO, flexiSelectionCriteria, isFixedFare);

				totalPrice = AccelAeroCalculator.add(totalPrice, totalFlexiChargesForPriceQuote);
			}

			// set total price
			OTAUtils.setAmountAndCurrency(totalFare, totalInSelectedCurByFareBreakdown, currencyCode);

			// set total Equiv price
			if (equiCurrencyCode != null) {
				BigDecimal totalPriceInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalPrice,
						equiCurrencyCode, exchangeRateProxy);
				if (totalPriceInEquivCurrency != null) {
					FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
					itinearyFareInfo.setTotalEquivFare(totalEquivFare);
					OTAUtils.setAmountAndCurrency(totalEquivFare, totalPriceInEquivCurrency, equiCurrencyCode);
				}
			} else {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				itinearyFareInfo.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndDefaultCurrency(totalEquivFare, totalPrice);
			}

			// Set total price with card payment transaction fee
			if (AppSysParamsUtil.applyCreditCardCharge()) {
				// Amount in base currency
				FareType.TotalFareWithCCFee totalFareWithCCFee = new FareType.TotalFareWithCCFee();
				itinearyFareInfo.setTotalFareWithCCFee(totalFareWithCCFee);

				// ExternalChgDTO externalChgDTO = ReservationUtil.getCCCharge(totalPrice);
				ExternalChgDTO externalChgDTO = ReservationUtil.getCCCharge(totalPrice, totalPayablePaxCount, totalSegmentCount,
						ChargeRateOperationType.MAKE_ONLY);

				BigDecimal ccFee = externalChgDTO.getAmount();
				ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

				BigDecimal totalWithCCFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, ccFee);

				BigDecimal CCFeeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(ccFee, currencyCode,
						exchangeRateProxy);
				BigDecimal totalWithCCFeeInPreferredCurrency = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
						CCFeeInPreferredCurrency);

				// set total price With CCFee in preferred currency
				if (totalWithCCFeeInPreferredCurrency != null) {
					OTAUtils.setAmountAndCurrency(totalFareWithCCFee, totalWithCCFeeInPreferredCurrency, currencyCode);
				}

				FareType.TotalEquivFareWithCCFee totalEquivFareWithCCFee = new FareType.TotalEquivFareWithCCFee();
				itinearyFareInfo.setTotalEquivFareWithCCFee(totalEquivFareWithCCFee);
				// set total With CCFee Equiv price
				if (equiCurrencyCode != null) {
					BigDecimal toalWithCCFeeInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							totalWithCCFeeInBaseCurrency, equiCurrencyCode, exchangeRateProxy);
					OTAUtils.setAmountAndCurrency(totalEquivFareWithCCFee, toalWithCCFeeInEquivCurrency, equiCurrencyCode);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(totalEquivFareWithCCFee, totalWithCCFeeInBaseCurrency);
				}
			}
		} else {
			// Adult Fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(AccelAeroCalculator
						.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT)), new BigDecimal(
						availableFlightSearchDTO.getAdultCount())));
			}

			// Child Fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(AccelAeroCalculator
						.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD)), new BigDecimal(
						availableFlightSearchDTO.getChildCount())));
			}

			// Infant fare
			if (ReservationApiUtils.isFareExist(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT))) {
				totalBaseFare = AccelAeroCalculator.add(totalBaseFare, AccelAeroCalculator.multiply(AccelAeroCalculator
						.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT)), new BigDecimal(
						availableFlightSearchDTO.getInfantCount())));
			}

			OTAUtils.setAmountAndDefaultCurrency(baseFare, totalBaseFare);

			BigDecimal[] totals = ReservationUtil.getQuotedTotalPrices(selectedFlightDTO.getOndFareDTOs(false),
					availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getChildCount(),
					availableFlightSearchDTO.getInfantCount());

			BigDecimal totalPrice = totals[0];

			// Adding Handling fare to the total price. For now only external charge is the handling fee
			BigDecimal handleCharge = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount();
			handleCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handleCharge);

			totalPrice = AccelAeroCalculator.add(totalPrice, handleCharge);

			// adding selected external charges for the selected services
			totalPrice = AccelAeroCalculator.add(totalPrice, totalSelectedExternalCharges);

			// adding the selected flexi charges for the price quote
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				BigDecimal totalFlexiChargesForPriceQuote = WSFlexiFareUtil.calculateTotalFlexiFareForPriceQuote(
						selectedFlightDTO, availableFlightSearchDTO, flexiSelectionCriteria, isFixedFare);
				totalPrice = AccelAeroCalculator.add(totalPrice, totalFlexiChargesForPriceQuote);
			}

			OTAUtils.setAmountAndDefaultCurrency(totalFare, totalPrice);

			// Set total price in agent currency
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalPrice,
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				itinearyFareInfo.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			}

			// Set total price with card payment transaction fee
			if (AppSysParamsUtil.applyCreditCardCharge()) {
				// Amount in base currency
				FareType.TotalFareWithCCFee totalFareWithCCFee = new FareType.TotalFareWithCCFee();
				itinearyFareInfo.setTotalFareWithCCFee(totalFareWithCCFee);

				ExternalChgDTO externalChgDTO = ReservationUtil.getCCCharge(totalPrice, totalPayablePaxCount, totalSegmentCount,
						ChargeRateOperationType.MAKE_ONLY); // ReservationUtil.getCCCharge(totalPrice);

				BigDecimal ccFee = externalChgDTO.getAmount();
				ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

				BigDecimal totalWithCCFeeInBaseCurrency = AccelAeroCalculator.add(totalPrice, ccFee);

				OTAUtils.setAmountAndDefaultCurrency(totalFareWithCCFee, totalWithCCFeeInBaseCurrency);

				// Amount in agent currency
				FareType.TotalEquivFareWithCCFee totalEquivFareWithCCFee = new FareType.TotalEquivFareWithCCFee();
				itinearyFareInfo.setTotalEquivFareWithCCFee(totalEquivFareWithCCFee);

				BigDecimal toalWithCCFeeInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalWithCCFeeInBaseCurrency, principal.getAgentCurrencyCode(), exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(totalEquivFareWithCCFee, toalWithCCFeeInAgentCurrency,
						principal.getAgentCurrencyCode());
			}
		}

		// Sets each passenger type wise fare, tax break down and surcharges(fees) breakdown
		airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);
	}

	/**
	 * Sets each passenger type wise fare, tax break down and surcharges(fees) breakdown
	 * 
	 * @param airItineraryPricingInfo
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 *            : The selected flexibilites for the price quote.
	 * 
	 * @throws WebservicesException
	 */
	private static void setPassengerFareBreakdowns(AirItineraryPricingInfoType airItineraryPricingInfo,
			SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, boolean isFixedFare, String pricingKey,
			FlexiFareSelectionCriteria flexiSelectionCriteria) throws WebservicesException, ModuleException {

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		AirItineraryPricingInfoType.PTCFareBreakdowns ptcFareBreakdowns = new AirItineraryPricingInfoType.PTCFareBreakdowns();
		// airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);
		BigDecimal totalInSelectedCurByFareBreakdown = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalChargesInSelCur = AccelAeroCalculator.getDefaultBigDecimalZero();

		Collection<String> paxTypes = new ArrayList<String>();
		if (availableFlightSearchDTO.getAdultCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		}
		if (availableFlightSearchDTO.getChildCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		}
		if (availableFlightSearchDTO.getInfantCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		}

		// calculating the per pax handling charge
		int handlingFeePaxCount = availableFlightSearchDTO.getAdultCount() + availableFlightSearchDTO.getChildCount();
		ExternalChgDTO externalChgDTO = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);

		BigDecimal handlingFeeAmount = externalChgDTO.getAmount();
		handlingFeeAmount = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFeeAmount);
		BigDecimal[] perPaxHandlingCharges = AccelAeroCalculator.roundAndSplit(handlingFeeAmount, handlingFeePaxCount);

		int paxSeq = 0;
		int parentSeq = 0;
		int count = 0; // to get the per pax handling fare from the array.

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		boolean isHoldAllowed = true;
		if (!AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
			isHoldAllowed = WSReservationUtil.isHoldAllowed(selectedFlightDTO.getOndFareDTOs(false));
		}

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();
		String currencyCode = null;
		if (showPriceInselectedCurrency) {
			if (availableFlightSearchDTO.getPreferredCurrencyCode() != null
					&& OTAUtils.isPreferredCurrencyCodeSupportedByPC(availableFlightSearchDTO.getPreferredCurrencyCode())) {
				currencyCode = availableFlightSearchDTO.getPreferredCurrencyCode();
			} else {
				currencyCode = principal.getAgentCurrencyCode();
			}
		}

		// Creates the flexi fare related objects. We can have two cases to display flexi
		// 1. User requests to display the flexi in the availability search
		// 2. The request contains to include flexibilities in the price quote.
		// in both these cases we need to display the available flexi flares.

		if (availableFlightSearchDTO.isFlexiQuote() || WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			WSFlexiFareUtil.prepareFlexiFareDisplayObjects(airItineraryPricingInfo, selectedFlightDTO, availableFlightSearchDTO,
					currencyCode, exchangeRateProxy);
		}

		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		for (String paxType : paxTypes) {

			List<PTCFareBreakdownType> fareBreakdownTypesWithExternalCharges = new ArrayList<PTCFareBreakdownType>();

			int paxQuantity;
			BigDecimal fareAmount;
			BigDecimal totalCharges;
			String chargeQuotePaxType;
			Collection<String> taxChargeGroupCodes = new ArrayList<String>();
			taxChargeGroupCodes.add(ChargeGroups.TAX);
			BigDecimal totalInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();

			Collection<String> surchargeChargeGroupCodes = new ArrayList<String>();
			surchargeChargeGroupCodes.add(ChargeGroups.SURCHARGE);

			if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
				paxQuantity = availableFlightSearchDTO.getAdultCount();
				fareAmount = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT));
				totalCharges = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getTotalCharges().get(0));
				chargeQuotePaxType = PaxTypeTO.ADULT;

			} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				paxQuantity = availableFlightSearchDTO.getChildCount();
				fareAmount = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD));
				totalCharges = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getTotalCharges().get(2));
				chargeQuotePaxType = PaxTypeTO.CHILD;

			} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				paxQuantity = availableFlightSearchDTO.getInfantCount();
				fareAmount = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT));
				totalCharges = AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getTotalCharges().get(1));
				surchargeChargeGroupCodes.add(ChargeGroups.INFANT_SURCHARGE);
				chargeQuotePaxType = PaxTypeTO.INFANT;
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
			// airItineraryPricingInfo.getPTCFareBreakdowns().getPTCFareBreakdown().add(ptcFareBreakdown);
			// ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);

			ptcFareBreakdown.setPricingSource(PricingSourceType.PUBLISHED);

			// set pax quantity
			PassengerTypeQuantityType passengerTypeQuantity = new PassengerTypeQuantityType();
			ptcFareBreakdown.setPassengerTypeQuantity(passengerTypeQuantity);
			passengerTypeQuantity.setCode(paxType);
			passengerTypeQuantity.setQuantity(paxQuantity);

			// set fare basis code
			PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
			ptcFareBreakdown.setFareBasisCodes(fareBasisCodes);
			// FIXME fare basis code to be returned in the price quote
			String dummyFareBasisCode = "P";
			fareBasisCodes.getFareBasisCode().add(dummyFareBasisCode);

			if (!isHoldAllowed) {
				String noHoldAllowedFareBasisCode = "NH"; // FIXME - Remove hard coding
				fareBasisCodes.getFareBasisCode().add(noHoldAllowedFareBasisCode);
			}

			FareType passengerFareInfo = new FareType();
			ptcFareBreakdown.setPassengerFare(passengerFareInfo);

			// base fare for single pax
			FareType.BaseFare baseFare = new FareType.BaseFare();
			passengerFareInfo.setBaseFare(baseFare);
			if (showPriceInselectedCurrency && currencyCode != null) {
				// fareAmount in preferred currency
				BigDecimal fareAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(fareAmount,
						currencyCode, exchangeRateProxy);
				totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, fareAmountInPreferredCurrency);
				OTAUtils.setAmountAndCurrency(baseFare, fareAmountInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(baseFare, fareAmount);
			}

			// taxes for single pax
			FareType.Taxes ptcTaxes = new FareType.Taxes();
			passengerFareInfo.setTaxes(ptcTaxes);

			Collection<String> chargeQuotePaxTypes = new ArrayList<String>();
			chargeQuotePaxTypes.add(chargeQuotePaxType);

			Collection<QuotedChargeDTO> aaTaxes = selectedFlightDTO.getUnifiedCharges(taxChargeGroupCodes, chargeQuotePaxTypes);

			if (aaTaxes != null && aaTaxes.size() > 0) {
				for (QuotedChargeDTO tax : aaTaxes) {
					AirTaxType taxType = new AirTaxType();
					ptcTaxes.getTax().add(taxType);
					taxType.setTaxCode(tax.getChargeCode());
					taxType.setTaxName(tax.getChargeDescription());
					BigDecimal taxAmount = AccelAeroCalculator.parseBigDecimal(tax.getEffectiveChargeAmount(chargeQuotePaxType));
					if (showPriceInselectedCurrency && currencyCode != null) {
						// taxType in preferred currency
						BigDecimal taxAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(taxAmount,
								currencyCode, exchangeRateProxy);
						totalInSelectedCurPerPax = AccelAeroCalculator
								.add(totalInSelectedCurPerPax, taxAmountInPreferredCurrency);
						OTAUtils.setAmountAndCurrency(taxType, taxAmountInPreferredCurrency, currencyCode);
					} else {
						OTAUtils.setAmountAndDefaultCurrency(taxType, taxAmount);
					}
				}
			}

			// surcharges(fees) for single pax
			FareType.Fees fees = new FareType.Fees();
			passengerFareInfo.setFees(fees);

			Collection<QuotedChargeDTO> aaSurcharges = selectedFlightDTO.getUnifiedCharges(surchargeChargeGroupCodes,
					chargeQuotePaxTypes);
			if (aaSurcharges != null && aaSurcharges.size() > 0) {
				for (QuotedChargeDTO surcharge : aaSurcharges) {
					AirFeeType feeType = new AirFeeType();
					fees.getFee().add(feeType);
					BigDecimal feeAmount = AccelAeroCalculator.parseBigDecimal(surcharge
							.getEffectiveChargeAmount(chargeQuotePaxType));
					if (showPriceInselectedCurrency && currencyCode != null) {
						// feeAmount in preferred currency
						BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(feeAmount,
								currencyCode, exchangeRateProxy);
						totalInSelectedCurPerPax = AccelAeroCalculator
								.add(totalInSelectedCurPerPax, feeAmountInPreferredCurrency);
						OTAUtils.setAmountAndCurrency(feeType, feeAmountInPreferredCurrency, currencyCode);
					} else {
						OTAUtils.setAmountAndDefaultCurrency(feeType, feeAmount);
					}
					feeType.setFeeCode(surcharge.getChargeCode() + "/" + surcharge.getChargeDescription());
				}
			}

			// add the handling fee only if the pax type is not a infant
			BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (!paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) { // ADULT
																												// or
																												// CHILD
				if (AccelAeroCalculator.isGreaterThan(perPaxHandlingCharges[count],
						(AccelAeroCalculator.getDefaultBigDecimalZero()))) {
					AirFeeType feeType = new AirFeeType();
					if (showPriceInselectedCurrency && currencyCode != null) {
						// feeAmount in preferred currency
						BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
								perPaxHandlingCharges[count], currencyCode, exchangeRateProxy);
						totalInSelectedCurPerPax = AccelAeroCalculator
								.add(totalInSelectedCurPerPax, feeAmountInPreferredCurrency);
						OTAUtils.setAmountAndCurrency(feeType, feeAmountInPreferredCurrency, currencyCode);
					} else {
						OTAUtils.setAmountAndDefaultCurrency(feeType, perPaxHandlingCharges[count]);
					}
					feeType.setFeeCode(externalChgDTO.getChargeCode() + "/" + externalChgDTO.getChargeDescription());
					fees.getFee().add(feeType);
				}
				// note: adult does not carry the infant charges
				totalPrice = AccelAeroCalculator.add(fareAmount, totalCharges, perPaxHandlingCharges[count]);
				count += paxQuantity;
			} else { // INFANT
				totalPrice = AccelAeroCalculator.add(fareAmount, totalCharges);
			}

			// ------------Start add flexi-charges------------------
			if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
				AirFeeType flexiFee = WSFlexiFareUtil.getPerPaxAirFeeForFlexiCharges(selectedFlightDTO, paxType, currencyCode,
						flexiSelectionCriteria, isFixedFare);
				fees.getFee().add(flexiFee);

				BigDecimal flexiAmountPerPaxType = flexiFee.getAmount();

				if (showPriceInselectedCurrency && currencyCode != null) {
					// No need to convert as the converstion has already happened at the object creating time.
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, flexiAmountPerPaxType);
				} else {
					totalPrice = AccelAeroCalculator.add(totalPrice, flexiAmountPerPaxType);
				}
			}
			// ------------ end add flexi charges------------------

			FareType.TotalFare totalFare = new FareType.TotalFare();
			passengerFareInfo.setTotalFare(totalFare);
			if (showPriceInselectedCurrency && currencyCode != null) {
				BigDecimal totalPriceInPreferredCurrency = totalInSelectedCurPerPax;
				OTAUtils.setAmountAndCurrency(totalFare, totalPriceInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(totalFare, totalPrice);
			}

			// set traveller reference numbers and introduce new breakdowns if any special services added
			for (int i = 0; i < paxQuantity; i++) {

				PTCFareBreakdownType.TravelerRefNumber travelerRefNumber = new PTCFareBreakdownType.TravelerRefNumber();
				if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					travelerRefNumber.setRPH("A" + (++paxSeq));
				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					travelerRefNumber.setRPH("C" + (++paxSeq));
				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					travelerRefNumber.setRPH("I" + (++paxSeq) + "/A" + (++parentSeq));
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				if (quotedExternalCharges != null && quotedExternalCharges.keySet().contains(travelerRefNumber.getRPH())) {
					// this is calling to split the breakdowns if special services selected
					BigDecimal[] returnVals = splitFareBreakdownsWithSelectedServices(quotedExternalCharges, travelerRefNumber,
							totalSelecteExternalCharges, showPriceInselectedCurrency, currencyCode,
							totalSelecteExternalChargesInSelCur, fareBreakdownTypesWithExternalCharges, ptcFareBreakdown,
							exchangeRateProxy);
					totalSelecteExternalCharges = returnVals[0];
					totalSelecteExternalChargesInSelCur = returnVals[1];
				} else {
					ptcFareBreakdown.getTravelerRefNumber().add(travelerRefNumber);
					ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
				}
			}

			if (paxQuantity == fareBreakdownTypesWithExternalCharges.size()) {
				ptcFareBreakdowns.getPTCFareBreakdown().addAll(fareBreakdownTypesWithExternalCharges);
			} else {
				ptcFareBreakdowns.getPTCFareBreakdown().addAll(fareBreakdownTypesWithExternalCharges);
				ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
				ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
			}

			totalInSelectedCurByFareBreakdown = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
					AccelAeroCalculator.multiply(totalInSelectedCurPerPax, paxQuantity));
		}

		if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			WSFlexiFareUtil.addFlexiChargesToExternalChargesMap(selectedFlightDTO, availableFlightSearchDTO,
					flexiSelectionCriteria, paxTypes);
		}

		totalInSelectedCurByFareBreakdown = AccelAeroCalculator.add(totalInSelectedCurByFareBreakdown,
				totalSelecteExternalChargesInSelCur);
		// set total fare and total taxes/surcharges for itinerary
		setTotalFaresForItinerary(airItineraryPricingInfo, selectedFlightDTO, availableFlightSearchDTO, externalChargesMap,
				isFixedFare, pricingKey, ptcFareBreakdowns, totalInSelectedCurByFareBreakdown, flexiSelectionCriteria,
				totalSelecteExternalCharges, exchangeRateProxy);
	}

	private static BigDecimal[] splitFareBreakdownsWithSelectedServices(Map<String, List<ExternalChgDTO>> quotedExternalCharges,
			PTCFareBreakdownType.TravelerRefNumber travelerRefNumber, BigDecimal totalSelecteExternalCharges,
			boolean showPriceInselectedCurrency, String currencyCode, BigDecimal totalSelecteExternalChargesInSelCur,
			List<PTCFareBreakdownType> fareBreakdownTypesWithExternalCharges, PTCFareBreakdownType ptcFareBreakdown,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		boolean isSpecialServicesSelected = false;
		boolean isSeatSelected, isMealSelected, isBaggageSelected, isHalaSelected, isInflightServiceSelected, isInsSelected;
		isSeatSelected = isMealSelected = isBaggageSelected = isHalaSelected = isInflightServiceSelected = isInsSelected = false;

		String mealChargesDescription = null;
		String seatChargesDescription = null;
		String halaChargesDescription = null;
		String inflightChargesDescription = null;
		String insuranceChargesDescription = null;
		String baggageChargesDescription = null;
		String serviceTaxDescription = null;
		List<ExternalChgDTO> externalCharges = quotedExternalCharges.get(travelerRefNumber.getRPH());

		// getting seat charges total
		BigDecimal seatChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal halaChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal inflightChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal insuranceChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal baggageChargesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal serviceTaxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ExternalChgDTO externalChargeTO : externalCharges) {
			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.SEAT_MAP)) {
				seatChargesTotal = AccelAeroCalculator.add(seatChargesTotal, externalChargeTO.getAmount());
				isSpecialServicesSelected = true;
				isSeatSelected = true;
				seatChargesDescription = externalChargeTO.getChargeDescription();
			}
			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.MEAL)) {
				mealChargesTotal = AccelAeroCalculator.add(mealChargesTotal, externalChargeTO.getAmount());
				isSpecialServicesSelected = true;
				isMealSelected = true;
				mealChargesDescription = externalChargeTO.getChargeDescription();
			}

			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.AIRPORT_SERVICE)) {
				halaChargesTotal = AccelAeroCalculator.add(halaChargesTotal, externalChargeTO.getAmount());
				isSpecialServicesSelected = true;
				isHalaSelected = true;
				halaChargesDescription = externalChargeTO.getChargeDescription();
			}

			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.INFLIGHT_SERVICES)) {
				inflightChargesTotal = AccelAeroCalculator.add(inflightChargesTotal, externalChargeTO.getAmount());
				isSpecialServicesSelected = true;
				isInflightServiceSelected = true;
				inflightChargesDescription = externalChargeTO.getChargeDescription();
			}

			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.INSURANCE)) {
				insuranceChargesTotal = AccelAeroCalculator.add(insuranceChargesTotal, externalChargeTO.getAmount());
				isInsSelected = true;
				insuranceChargesDescription = externalChargeTO.getChargeDescription();
			}

			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.BAGGAGE)) {
				baggageChargesTotal = AccelAeroCalculator.add(baggageChargesTotal, externalChargeTO.getAmount());
				isSpecialServicesSelected = true;
				isBaggageSelected = true;
				baggageChargesDescription = externalChargeTO.getChargeDescription();
			}

			if (externalChargeTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.JN_ANCI)) {
				isSpecialServicesSelected = true;
				serviceTaxTotal = AccelAeroCalculator.add(serviceTaxTotal, externalChargeTO.getAmount());
				serviceTaxDescription = externalChargeTO.getChargeDescription();
			}
		}

		if (isSpecialServicesSelected) {
			PTCFareBreakdownType newPtcFareBreakdownType = OTAUtils.clonePtcFareBreakdownType(ptcFareBreakdown);

			BigDecimal totalExternalChargesPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
			// for the seat selection
			if (isSeatSelected) {
				AirFeeType seatFee = new AirFeeType();
				// fees.getFee().add(seatFee);
				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, seatChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(seatChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(seatFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(seatFee, seatChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, seatChargesTotal);
				}
				seatFee.setFeeCode(seatChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(seatFee);
			}
			// This is for the meal selection
			if (isMealSelected) {
				AirFeeType mealFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, mealChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(mealChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(mealFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(mealFee, mealChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, mealChargesTotal);
				}
				mealFee.setFeeCode(mealChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(mealFee);
			}
			// for hala services
			if (isHalaSelected) {
				AirFeeType halaFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, halaChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(halaChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(halaFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(halaFee, halaChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, halaChargesTotal);
				}
				halaFee.setFeeCode(halaChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(halaFee);
			}

			// For In-flight Services
			if (isInflightServiceSelected) {
				AirFeeType inflightFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, inflightChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// Fee amount in preferred language
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							inflightChargesTotal, currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(inflightFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(inflightFee, inflightChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, inflightChargesTotal);
				}
				inflightFee.setFeeCode(inflightChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(inflightFee);
			}

			// for the insurance selection
			if (isInsSelected) {
				AirFeeType insuranceFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, insuranceChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
							insuranceChargesTotal, currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(insuranceFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(insuranceFee, insuranceChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, insuranceChargesTotal);
				}
				insuranceFee.setFeeCode(insuranceChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(insuranceFee);
			}
			// This is for the baggage selection
			if (isBaggageSelected) {
				AirFeeType baggageFee = new AirFeeType();

				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, baggageChargesTotal);
				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal feeAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(baggageChargesTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							feeAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(baggageFee, feeAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, feeAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(baggageFee, baggageChargesTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, baggageChargesTotal);
				}
				baggageFee.setFeeCode(baggageChargesDescription);
				newPtcFareBreakdownType.getPassengerFare().getFees().getFee().add(baggageFee);
			}

			if (!serviceTaxTotal.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
				AirTaxType serviceTax = new AirTaxType();
				totalSelecteExternalCharges = AccelAeroCalculator.add(totalSelecteExternalCharges, serviceTaxTotal);

				if (showPriceInselectedCurrency && currencyCode != null) {
					// feeAmount in preferred currency
					BigDecimal taxAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(serviceTaxTotal,
							currencyCode, exchangeRateProxy);
					totalSelecteExternalChargesInSelCur = AccelAeroCalculator.add(totalSelecteExternalChargesInSelCur,
							taxAmountInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(serviceTax, taxAmountInPreferredCurrency, currencyCode);
					totalExternalChargesPerPax = AccelAeroCalculator
							.add(totalExternalChargesPerPax, taxAmountInPreferredCurrency);
				} else {
					OTAUtils.setAmountAndDefaultCurrency(serviceTax, serviceTaxTotal);
					totalExternalChargesPerPax = AccelAeroCalculator.add(totalExternalChargesPerPax, serviceTaxTotal);
				}
				serviceTax.setTaxCode(serviceTaxDescription);
				newPtcFareBreakdownType.getPassengerFare().getTaxes().getTax().add(serviceTax);
			}

			newPtcFareBreakdownType
					.getPassengerFare()
					.getTotalFare()
					.setAmount(
							AccelAeroCalculator.add(newPtcFareBreakdownType.getPassengerFare().getTotalFare().getAmount(),
									totalExternalChargesPerPax));

			newPtcFareBreakdownType.getTravelerRefNumber().add(travelerRefNumber);
			newPtcFareBreakdownType.getPassengerTypeQuantity().setQuantity(newPtcFareBreakdownType.getTravelerRefNumber().size());

			fareBreakdownTypesWithExternalCharges.add(newPtcFareBreakdownType);

		} else {
			ptcFareBreakdown.getTravelerRefNumber().add(travelerRefNumber);
			ptcFareBreakdown.getPassengerTypeQuantity().setQuantity(ptcFareBreakdown.getTravelerRefNumber().size());
		}

		// BigDecimal[0]- total selected external charges,
		// BigDecimal[1]- total selected external charges in selected currency
		return new BigDecimal[] { totalSelecteExternalCharges, totalSelecteExternalChargesInSelCur };
	}
}
