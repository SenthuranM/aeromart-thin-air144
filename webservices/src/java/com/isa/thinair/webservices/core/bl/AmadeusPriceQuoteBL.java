package com.isa.thinair.webservices.core.bl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.AMATLAPricingRQ;
import com.amadeus.ama._2008._03.AMATLAPricingRS;
import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.SourceType;
import com.amadeus.ama._2008._03.StateType;
import com.amadeus.ama._2008._03.SuccessType;
import com.amadeus.ama._2008._03.TLAAssociationType;
import com.amadeus.ama._2008._03.TLABookingClassDataType;
import com.amadeus.ama._2008._03.TLACurrencyAmountType;
import com.amadeus.ama._2008._03.TLACurrencyAmountsType;
import com.amadeus.ama._2008._03.TLAErrorType;
import com.amadeus.ama._2008._03.TLAFareCategoryCode;
import com.amadeus.ama._2008._03.TLAFareCategoryType;
import com.amadeus.ama._2008._03.TLAFareReferenceType;
import com.amadeus.ama._2008._03.TLAFareType;
import com.amadeus.ama._2008._03.TLAFareType.PassengerRPHs;
import com.amadeus.ama._2008._03.TLAFareTypeRQ;
import com.amadeus.ama._2008._03.TLAJourneySingleFareTypePRS;
import com.amadeus.ama._2008._03.TLAOriginDestinationInformationType;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDataTypePRRQ;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDataTypeWS;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDatasType;
import com.amadeus.ama._2008._03.TLAPassengerRPHType;
import com.amadeus.ama._2008._03.TLAPassengerTypeP;
import com.amadeus.ama._2008._03.TLAPassengersTypeP;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypePRQ;
import com.amadeus.ama._2008._03.TLASegmentTypeP;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationTypePRQ;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationTypePRS;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationsTypePRS;
import com.amadeus.ama._2008._03.TLATypedCurrencyAmountType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IAvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.dtos.AmadeusTotalPriceDTO;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusPriceQuoteBL {
	private final static Log log = LogFactory.getLog(AmadeusAvailabilitySearchBL.class);

	@SuppressWarnings({})
	public static AMATLAPricingRS execute(AMATLAPricingRQ pricingRQ) {
		AMATLAPricingRS pricingRS = new AMATLAPricingRS();
		String bookingType = BookingClass.BookingClassType.NORMAL;
		pricingRS.setRelease(WebservicesConstants.AmadeusConstants.ReleasePriceRS);
		pricingRS.setVersion(WebservicesConstants.AmadeusConstants.VersionPriceRS);
		try {
			int adultCount = 0;
			int childCount = 0;
			int infantCount = 0;
			Map<String, TLAPassengerTypeP> paxMap = new HashMap<String, TLAPassengerTypeP>();
			for (TLAPassengerTypeP pax : pricingRQ.getPassengerList().getPassenger()) {
				paxMap.put(pax.getTravelerRefNumber().getRPH(), pax);
				if (pax.getPassengerTypeCode().equals(AmadeusConstants.ADULT)) {
					adultCount++;
				} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.CHILD)) {
					childCount++;
				} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
					infantCount++;
				}
			}
			AmadeusValidationUtil.validatePaxCounts(adultCount, childCount, infantCount);

			if (pricingRQ.getSSRGroup() != null) {
				SsrBD ssrBD = WebServicesModuleUtils.getSsrServiceBD();

				for (TLAPassengerMonetaryDataTypePRRQ paxData : pricingRQ.getSSRGroup().getPassengerSSRData()) {
					AmadeusValidationUtil.validateSSR(paxData);
					paxData.getPassengerRPHs().getPassengerRPH().iterator().next().getValue();
					paxData.getFlightSegmentRPHs().getFlightSegmentRPH().iterator().next().getValue();
					if (paxData.getType() == TLAFareCategoryType.SSR) {
						List<SSR> ssrCodeList = ssrBD.getSSR(paxData.getCode());
						if (ssrCodeList == null || ssrCodeList.size() == 0) {
							throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_SERVICE_NOT_AVAILABLE,
									ErrorWarningType.BIZ_RULE, "Requested SSR Code: " + paxData.getCode() + " is not available");
						} else {
							boolean status = false;
							for (SSR ssr : ssrCodeList) {
								if (ssr.getStatus().equals(SSR.STATUS_ACTIVE)) {
									status = true;
									break;
								}
							}
							if (!status) {
								throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_SERVICE_NOT_AVAILABLE,
										ErrorWarningType.BIZ_RULE, "Requested SSR Code: " + paxData.getCode()
												+ " is not available");
							}
						}

					}

				}
			}

			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

			String originISOCountryCode = null;
			if (pricingRQ.getPOS() != null) {
				if (pricingRQ.getPOS().getSource() != null && pricingRQ.getPOS().getSource().size() > 0) {
					SourceType pos = pricingRQ.getPOS().getSource().get(0);
					originISOCountryCode = pos.getISOCountry();
				}
			}
			String currencyCode = AmadeusCommonUtil.getPGWCurrencyCode(pricingRQ.getDisplayCurrencyCode(), originISOCountryCode);
			CurrencyExchangeRate currencyExgRate = AmadeusCommonUtil.getCurrencyExchangeRate(currencyCode);

			FlightBD flightDelegate = WebServicesModuleUtils.getFlightBD();
			ZuluLocalTimeConversionHelper zuluHelper = new ZuluLocalTimeConversionHelper(WebServicesModuleUtils.getAirportBD());

			AmadeusTotalPriceDTO totalPriceDTO = new AmadeusTotalPriceDTO();
			TLASingleOriginDestinationsTypePRS itinerary = new TLASingleOriginDestinationsTypePRS();
			int invalidBookingCodes = 0;

			// cc charge as a fixed value;
			int totalSegmentCount = 0;

			for (TLASingleOriginDestinationTypePRQ singleOndRQ : pricingRQ.getItinerary().getSingleOriginDestination()) {
				TLAOriginDestinationInformationType ondInfo = singleOndRQ.getOriginDestinationInfo();

				Map<String, String> refSegRPHMap = new HashMap<String, String>();
				Collection<FlightSegmentDTO> obFlightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
				for (TLASegmentTypeP segment : singleOndRQ.getJourney().getSegment()) {

					FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
					aaFlightSegmentDTO.setFromAirport(segment.getDepartureAirport().getLocationCode());
					aaFlightSegmentDTO.setToAirport(segment.getArrivalAirport().getLocationCode());
					aaFlightSegmentDTO.setFlightNumber(segment.getOperatingAirline().getCode()
							+ segment.getOperatingAirline().getFlightNumber());
					aaFlightSegmentDTO.setDepartureDateTime(CommonUtil
							.parseDateTime(segment.getDepartureDateTime().toXMLFormat()));
					aaFlightSegmentDTO.setArrivalDateTime(CommonUtil.parseDateTime(segment.getArrivalDateTime().toXMLFormat()));
					refSegRPHMap.put(AmadeusCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO), segment.getRPH());

					obFlightSegmentDTOs.add(aaFlightSegmentDTO);
				}
				totalSegmentCount += obFlightSegmentDTOs.size();

				Collection<FlightSegmentDTO> filledFltSegDTOs = flightDelegate.getFilledFlightSegmentDTOs(obFlightSegmentDTOs);
				if (filledFltSegDTOs == null || filledFltSegDTOs.size() == 0) {
					throw new AmadeusWSException(AmadeusErrorCodes.FLIGHT_IS_NOT_AVAILABLE, ErrorWarningType.BIZ_RULE,
							"Flights are not available");
				}

				List<Integer> segIds = new ArrayList<Integer>();
				for (FlightSegmentDTO fltSegDTO : filledFltSegDTOs) {
					segIds.add(fltSegDTO.getSegmentId());
				}

				// To do fare quotes for all amadeus exposed booking class and gets lowest priced one

				// Assuming a single booking class for ond
				TLAFareReferenceType fareType = singleOndRQ.getJourney().getFaresForSegment().iterator().next()
						.getBookingClassData().getFareBasis();
				fareType.setResBookDesigCode("Y"); // FIXME
				/*
				 * String bookingClassCode = null;
				 * 
				 * Map<BookingClassCharMappingTO, String> bookingClassCharMappingMap =
				 * WebServicesModuleUtils.getGlobalConfig().getBookingClassCharMappingMap();
				 * Set<BookingClassCharMappingTO> keys = new HashSet<BookingClassCharMappingTO>(); String singleCharBC =
				 * fareType.getResBookDesigCode(); if (singleCharBC == null){ singleCharBC =
				 * AmadeusCommonUtil.getBookingClassFromAmadeusFBC(fareType.getValue()); }
				 */

				SelectedFlightDTO selectedFlightDTO = null;
				AvailableFlightSearchDTO searchDTO = AmadeusCommonUtil.composeAvailSearchDTO(ondInfo, adultCount, childCount,
						infantCount, currencyCode, principal, zuluHelper, segIds);
				
				Collection<String> privilegeKeys = ThreadLocalData
						.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
	
				int availabilityRestriction = (AuthorizationUtil.hasPrivileges(privilegeKeys,
						PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY.equals(bookingType)) ? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
						: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

				searchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);

				/*
				 * if(WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()){
				 * 
				 * for (Entry<BookingClassCharMappingTO, String> entry : bookingClassCharMappingMap.entrySet()) { if
				 * (entry.getValue().equals(singleCharBC)) { if(entry.getKey().getGdsId() == AmadeusConstants.GDS_ID) {
				 * bookingClassCode = entry.getKey().getBookingCode(); fareType.setResBookDesigCode(bookingClassCode);
				 * break; } } }
				 * 
				 * if (bookingClassCode == null) { invalidBookingCodes++; continue; }
				 */

				// searchDTO.setBookingClassCode(bookingClassCode);
				searchDTO.setExcludeConnectionFares(true);
				searchDTO.setExcludeReturnFares(true);
				selectedFlightDTO = WebServicesModuleUtils.getReservationQueryBD().getFareQuote(searchDTO, null);

				/*
				 * } else { // Do fare quotes for all the Amadeus exposed booking classes and get lowest fare
				 * 
				 * BigDecimal minFareAmount = null;
				 * 
				 * for (Entry<BookingClassCharMappingTO, String> entry : bookingClassCharMappingMap.entrySet()) {
				 * 
				 * bookingClassCode = entry.getKey().getBookingCode(); searchDTO.setBookingClassCode(bookingClassCode);
				 * SelectedFlightDTO tempSelectedFlightDTO =
				 * WebServicesModuleUtils.getReservationQueryBD().getFareQuote( searchDTO);
				 * 
				 * if (tempSelectedFlightDTO != null) {
				 * 
				 * AmadeusTotalPriceDTO totalPrice = getTotalFare(tempSelectedFlightDTO.getOutboundFlight(),
				 * singleOndRQ, refSegRPHMap, new int[] { adultCount,childCount, infantCount }, currencyExgRate); if
				 * (minFareAmount == null) { if(totalPrice != null){ minFareAmount = totalPrice.getTotal();
				 * selectedFlightDTO = tempSelectedFlightDTO; } } else {
				 * 
				 * if(totalPrice != null && minFareAmount.compareTo(totalPrice.getTotal()) == 1){ minFareAmount =
				 * totalPrice.getTotal(); selectedFlightDTO = tempSelectedFlightDTO; }
				 * 
				 * } }
				 * 
				 * }
				 * 
				 * }
				 */

				if (selectedFlightDTO == null) {
					throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
							"Fares not available");
				}

				TLASingleOriginDestinationTypePRS ondTypeRS = transform(selectedFlightDTO, singleOndRQ, refSegRPHMap, paxMap,
						currencyExgRate, totalPriceDTO, new int[] { adultCount, childCount, infantCount });

				itinerary.getSingleOriginDestination().add(ondTypeRS);
			}

			if (invalidBookingCodes == pricingRQ.getItinerary().getSingleOriginDestination().size()) {
				throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
						"Fares not available");
			}

			TLACurrencyAmountsType totalAmtList = new TLACurrencyAmountsType();

			BigDecimal recalConvertedCCCharge = BigDecimal.ZERO;

			if (pricingRQ.getFormOfPayment() != null) {
				if (pricingRQ.getFormOfPayment().getPayment().size() > 0) {
					if (pricingRQ.getFormOfPayment().getPayment().size() > 1) {
						throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND,
								ErrorWarningType.BIZ_RULE, "Multiple payments methods not supported");
					}
					TLAPaymentDetailTypePRQ paymentTypeRQ = pricingRQ.getFormOfPayment().getPayment().get(0);
					if (paymentTypeRQ.getAgencyPayment() != null) {
						throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_DO_NOT_RETRY,
								ErrorWarningType.NO_IMPLEMENTATION, "Agent payments are not supported yet");
					} else if (paymentTypeRQ.getPaymentCard() != null) {
						if (paymentTypeRQ.getPaymentCard().getCardCode().length() != 2) {
							throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND,
									ErrorWarningType.BIZ_RULE, "Card code should be 2 characters.");
						}
						ExternalChgDTO ccExtChg = AmadeusCommonUtil.calculateCCCharges(totalPriceDTO.getTotalWithoutFee(),
								adultCount + childCount, totalSegmentCount, ChargeRateOperationType.MAKE_ONLY);
						/*
						 * setting the base amt for totalprice dto
						 */
						totalPriceDTO.setTotalFee(ccExtChg.getAmount());

						BigDecimal perPaxConvertedCCCharge = AmadeusCommonUtil.getDispValue(
								AccelAeroCalculator.divide(ccExtChg.getAmount(), new BigDecimal(adultCount + childCount)),
								currencyExgRate);

						recalConvertedCCCharge = AccelAeroCalculator.multiply(perPaxConvertedCCCharge, new BigDecimal(adultCount
								+ childCount));

						TLACurrencyAmountType tlaAmount = new TLACurrencyAmountType();
						tlaAmount.setAmount(perPaxConvertedCCCharge);
						TLAPassengerRPHType paxRPHs = new TLAPassengerRPHType();
						for (String paxRPH : paxMap.keySet()) {
							if (!paxMap.get(paxRPH).getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
								TLAAssociationType assocType = new TLAAssociationType();
								assocType.setTargetState(StateType.NEW);
								assocType.setValue(paxRPH);
								paxRPHs.getPassengerRPH().add(assocType);
							}
						}
						TLAPassengerMonetaryDataTypeWS ccMonetaryData = new TLAPassengerMonetaryDataTypeWS();
						ccMonetaryData.setChargeCode(TLAFareCategoryCode.CC);
						ccMonetaryData.setType(TLAFareCategoryType.FEE);
						ccMonetaryData.setAmount(tlaAmount);
						ccMonetaryData.setQuantity(BigInteger.ONE);
						ccMonetaryData.setPassengerRPHs(paxRPHs);

						/* add total fee to total list */
						TLATypedCurrencyAmountType totalFeeAmt = new TLATypedCurrencyAmountType();
						totalFeeAmt.setAmount(recalConvertedCCCharge);
						totalFeeAmt.setType(TLAFareCategoryType.FEE);
						totalAmtList.getTotalAmount().add(totalFeeAmt);

						TLAPassengerMonetaryDatasType paxMonetaryData = new TLAPassengerMonetaryDatasType();
						paxMonetaryData.getPassengerMonetaryData().add(ccMonetaryData);

						pricingRS.setFeesAndChargesGroup(paxMonetaryData);
					}

				} else {
					log.warn("Form of payment element contain zero entries");
				}
			}

			// Calculate Total price

			BigDecimal totalFare = BigDecimal.ZERO;
			BigDecimal totalTaxes = BigDecimal.ZERO;

			for (TLASingleOriginDestinationTypePRS singleOriDest : itinerary.getSingleOriginDestination()) {

				for (TLAFareType fareType : singleOriDest.getJourney().getFaresForSegment()) {

					BigDecimal totalSegmentFare = BigDecimal.ZERO;
					BigDecimal totalSegmentTaxes = BigDecimal.ZERO;
					BigDecimal perPaxSegmentFare = BigDecimal.ZERO;
					BigDecimal perPaxSegmentTax = BigDecimal.ZERO;

					perPaxSegmentTax = fareType.getBookingClassData().getInformativeTaxAmount().getAmount();
					perPaxSegmentFare = fareType.getBookingClassData().getTotalAmount().getAmount().subtract(perPaxSegmentTax);

					totalSegmentFare = perPaxSegmentFare.multiply(new BigDecimal(fareType.getPassengerRPHs().getPassengerRPH()
							.size()));
					totalSegmentTaxes = perPaxSegmentTax.multiply(new BigDecimal(fareType.getPassengerRPHs().getPassengerRPH()
							.size()));

					totalFare = AccelAeroCalculator.add(totalSegmentFare, totalFare);
					totalTaxes = AccelAeroCalculator.add(totalSegmentTaxes, totalTaxes);

				}
			}

			/* add base fare amount */
			TLATypedCurrencyAmountType totalFareAmt = new TLATypedCurrencyAmountType();
			totalFareAmt.setAmount(totalFare);
			totalFareAmt.setType(TLAFareCategoryType.BASE_FARE);
			totalAmtList.getTotalAmount().add(totalFareAmt);

			/* add total tax */
			TLATypedCurrencyAmountType totalTaxAmt = new TLATypedCurrencyAmountType();
			totalTaxAmt.setAmount(totalTaxes);
			totalTaxAmt.setType(TLAFareCategoryType.TOTAL_TAX);
			totalAmtList.getTotalAmount().add(totalTaxAmt);

			TLACurrencyAmountType totalAmt = new TLACurrencyAmountType();

			// No chargable SSRs yet adding for completion
			BigDecimal totalSSRInSelCur = AmadeusCommonUtil.getDispValue(totalPriceDTO.getTotalSSR(), currencyExgRate);
			BigDecimal totalAmount = AccelAeroCalculator.add(totalFareAmt.getAmount(), totalTaxAmt.getAmount(), totalSSRInSelCur,
					recalConvertedCCCharge);

			totalAmt.setAmount(totalAmount);

			pricingRS.setDisplayCurrencyCode(currencyCode);
			TLAPassengersTypeP passengerList = new TLAPassengersTypeP();
			passengerList.getPassenger().addAll(paxMap.values());
			pricingRS.setItinerary(itinerary);
			pricingRS.setPassengerList(passengerList);
			pricingRS.setTotalAmountList(totalAmtList);
			pricingRS.setTotalAmount(totalAmt);
			pricingRS.setSuccess(new SuccessType());
		} catch (ModuleException e) {
			log.error("Error occured", e);
			TLAErrorType error = new TLAErrorType();
			if (e instanceof AmadeusWSException) {
				AmadeusWSException ae = (AmadeusWSException) e;
				error.setCode(ae.getErrorCode());
				error.setType(ae.getErrorWarningType());
				// TODO fetch the descriptive message from resource bundle
				if (ae.getDescription() != null && !"".equals(ae.getDescription())) {
					error.setValue(ae.getDescription());
				}
			} else {
				error.setCode(Integer.valueOf(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_RETRY.getErrorCode()));
				error.setValue(e.getExceptionCode());
				error.setType(ErrorWarningType.UNKNOWN);
			}
			pricingRS.setError(error);
		}
		return pricingRS;
	}

	/**
	 * Tranform the selected flight DTO to amadeus SingleOriginDestination Type response
	 * 
	 * @param selectedFlightDTO
	 * @param singleOndRQ
	 * @param refSegRPHMap
	 * @param paxMap
	 * @param currencyExgRate
	 * @param totalPriceDTO
	 * @param paxCount
	 * @return
	 * @throws ModuleException
	 */
	private static TLASingleOriginDestinationTypePRS transform(SelectedFlightDTO selectedFlightDTO,
			TLASingleOriginDestinationTypePRQ singleOndRQ, Map<String, String> refSegRPHMap,
			Map<String, TLAPassengerTypeP> paxMap, CurrencyExchangeRate currencyExgRate, AmadeusTotalPriceDTO totalPriceDTO,
			int[] paxCount) throws ModuleException {

		IAvailableFlightSegment onewayFltSeg = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);
		if (onewayFltSeg == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.NO_FLIGHTS_FOUND, ErrorWarningType.BIZ_RULE, "No flights found");
		}

		// Map<String, TLAFareReferenceType> rphBookingClassMap = new HashMap<String, TLAFareReferenceType>();
		// Map<String, String> segRefRPHMap = new HashMap<String, String>();
		// for (TLAFareTypeRQ fareSegs : singleOndRQ.getJourney().getFaresForSegment()) {
		// rphBookingClassMap.put(fareSegs.getFlightSegmentRPH(), fareSegs.getBookingClassData().getFareBasis());
		// }

		Map<String, Map<String, TLABookingClassDataType>> segmentFareMap = new HashMap<String, Map<String, TLABookingClassDataType>>();

		if (onewayFltSeg.isDirectFlight()) {
			AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) onewayFltSeg;

			String segmentRPH = refSegRPHMap.get(AmadeusCommonUtil.getTmpSegRefNo(availableFlightSegment.getFlightSegmentDTO()));
			segmentFareMap.put(segmentRPH, new HashMap<String, TLABookingClassDataType>());
			String selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
			if (selectedLogicalCabinClass != null) {
				// if (isMatchingBC(rphBookingClassMap.get(segmentRPH),
				// availableFlightSegment.getFare(LogicalCabinClassTypes.NON_FIXED_FARE))) {
				segmentFareMap.get(segmentRPH).putAll(
						getPaxTypeFareMap(availableFlightSegment.getFare(selectedLogicalCabinClass),
								availableFlightSegment.getTotalCharges(selectedLogicalCabinClass), currencyExgRate));

				updateTotalPriceDTO(totalPriceDTO, availableFlightSegment.getFare(selectedLogicalCabinClass),
						availableFlightSegment.getTotalCharges(selectedLogicalCabinClass), paxCount);
				// }
			} else {
				throw new AmadeusWSException(AmadeusErrorCodes.FLIGHT_IS_NOT_AVAILABLE, ErrorWarningType.BIZ_RULE,
						"Some Flights not available");
			}

		} else {
			AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) onewayFltSeg;

			// Non-fixed fare amount
			String selectedLogicalCabinClass = availableConnectedFlight.getSelectedLogicalCabinClass();
			if (selectedLogicalCabinClass != null && availableConnectedFlight.getFare(selectedLogicalCabinClass) != null) {
				log.error("FIXME - CONNECTION FARES ARE NOT HANDLED YET!");
				throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
						"Fares not available");
			} else {
				for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
					String segmentRPH = refSegRPHMap.get(AmadeusCommonUtil.getTmpSegRefNo(availableFlightSegment
							.getFlightSegmentDTO()));
					selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
					if (availableFlightSegment.getFare(selectedLogicalCabinClass) != null) {
						// if (isMatchingBC(rphBookingClassMap.get(segmentRPH),
						// availableFlightSegment.getFare(LogicalCabinClassTypes.NON_FIXED_FARE))) {
						segmentFareMap.put(segmentRPH, new HashMap<String, TLABookingClassDataType>());
						segmentFareMap.get(segmentRPH).putAll(
								getPaxTypeFareMap(availableFlightSegment.getFare(selectedLogicalCabinClass),
										availableFlightSegment.getTotalCharges(selectedLogicalCabinClass), currencyExgRate));
						updateTotalPriceDTO(totalPriceDTO, availableFlightSegment.getFare(selectedLogicalCabinClass),
								availableFlightSegment.getTotalCharges(selectedLogicalCabinClass), paxCount);
						// }
					} else {
						throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
								"Fares not available");
					}
				}
			}
		}

		TLASingleOriginDestinationTypePRS singleOndRS = new TLASingleOriginDestinationTypePRS();
		TLAJourneySingleFareTypePRS journey = new TLAJourneySingleFareTypePRS();
		singleOndRS.setJourney(journey);
		singleOndRS.setOriginDestinationInfo(singleOndRQ.getOriginDestinationInfo());

		for (TLAFareTypeRQ fareForSeg : singleOndRQ.getJourney().getFaresForSegment()) {
			Set<String> adultRPHs = new HashSet<String>();
			Set<String> infantRPHs = new HashSet<String>();
			Set<String> childRPHs = new HashSet<String>();

			for (String paxRPH : fareForSeg.getPassengerRPHs().getPassengerRPH()) {
				if (paxMap.get(paxRPH).getPassengerTypeCode().equals(AmadeusConstants.ADULT)) {
					adultRPHs.add(paxRPH);
				} else if (paxMap.get(paxRPH).getPassengerTypeCode().equals(AmadeusConstants.CHILD)) {
					childRPHs.add(paxRPH);
				} else if (paxMap.get(paxRPH).getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
					infantRPHs.add(paxRPH);
				}
			}

			if (adultRPHs.size() != paxCount[0] || childRPHs.size() != paxCount[1] || infantRPHs.size() != paxCount[2]) {
				throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
						"Segments RPH:" + fareForSeg.getFlightSegmentRPH() + " contain fewer passengers");
			}
			if (adultRPHs.size() > 0) {
				PassengerRPHs paxRPH = new PassengerRPHs();
				paxRPH.getPassengerRPH().addAll(adultRPHs);

				TLAFareType fareType = new TLAFareType();
				fareType.setFlightSegmentRPH(fareForSeg.getFlightSegmentRPH());
				fareType.setPassengerRPHs(paxRPH);
				TLABookingClassDataType bc = segmentFareMap.get(fareForSeg.getFlightSegmentRPH()).get(AmadeusConstants.ADULT);
				if (bc == null) {
					throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
							"Fares not available");
				}
				fareType.setBookingClassData(bc);
				journey.getFaresForSegment().add(fareType);
			}
			if (childRPHs.size() > 0) {
				PassengerRPHs paxRPH = new PassengerRPHs();
				paxRPH.getPassengerRPH().addAll(childRPHs);

				TLAFareType fareType = new TLAFareType();
				fareType.setFlightSegmentRPH(fareForSeg.getFlightSegmentRPH());
				fareType.setPassengerRPHs(paxRPH);
				TLABookingClassDataType bc = segmentFareMap.get(fareForSeg.getFlightSegmentRPH()).get(AmadeusConstants.CHILD);
				if (bc == null) {
					throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
							"Fares not available");
				}
				fareType.setBookingClassData(bc);
				journey.getFaresForSegment().add(fareType);
			}
			if (infantRPHs.size() > 0) {
				PassengerRPHs paxRPH = new PassengerRPHs();
				paxRPH.getPassengerRPH().addAll(infantRPHs);

				TLAFareType fareType = new TLAFareType();
				fareType.setFlightSegmentRPH(fareForSeg.getFlightSegmentRPH());
				fareType.setPassengerRPHs(paxRPH);
				TLABookingClassDataType bc = segmentFareMap.get(fareForSeg.getFlightSegmentRPH()).get(AmadeusConstants.INFANT);
				if (bc == null) {
					throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
							"Fares not available");
				}
				fareType.setBookingClassData(bc);
				journey.getFaresForSegment().add(fareType);
			}
		}
		journey.getSegment().addAll(singleOndRQ.getJourney().getSegment());

		return singleOndRS;
	}

	/**
	 * Update the total price based on the total price DTO
	 * 
	 * @param totalPriceDTO
	 * @param fare
	 * @param totalCharges
	 * @param paxCount
	 */
	private static void updateTotalPriceDTO(AmadeusTotalPriceDTO totalPriceDTO, FareSummaryDTO fare, List<Double> totalCharges,
			int[] paxCount) {
		BigDecimal totalFare = BigDecimal.ZERO;
		BigDecimal totalTax = BigDecimal.ZERO;
		if (paxCount[0] > 0) {
			totalFare = AccelAeroCalculator.add(
					AccelAeroCalculator.multiply(new BigDecimal(fare.getFareAmount(PaxTypeTO.ADULT)), paxCount[0]), totalFare);
			totalTax = AccelAeroCalculator.add(AccelAeroCalculator.multiply(new BigDecimal(totalCharges.get(0)), paxCount[0]),
					totalTax);
		}
		if (paxCount[1] > 0) {
			totalFare = AccelAeroCalculator.add(
					AccelAeroCalculator.multiply(new BigDecimal(fare.getFareAmount(PaxTypeTO.CHILD)), paxCount[1]), totalFare);
			totalTax = AccelAeroCalculator.add(AccelAeroCalculator.multiply(new BigDecimal(totalCharges.get(2)), paxCount[1]),
					totalTax);
		}
		if (paxCount[2] > 0) {
			totalFare = AccelAeroCalculator.add(
					AccelAeroCalculator.multiply(new BigDecimal(fare.getFareAmount(PaxTypeTO.INFANT)), paxCount[2]), totalFare);
			totalTax = AccelAeroCalculator.add(AccelAeroCalculator.multiply(new BigDecimal(totalCharges.get(1)), paxCount[2]),
					totalTax);
		}
		totalPriceDTO.setTotalFare(AccelAeroCalculator.add(totalPriceDTO.getTotalFare(), totalFare));
		totalPriceDTO.setTotalTax(AccelAeroCalculator.add(totalPriceDTO.getTotalTax(), totalTax));
	}

	/**
	 * Check for matching booking class
	 * 
	 * @param tlaFareReferenceType
	 * @param fare
	 * @return
	 * @throws AmadeusWSException
	 */
	public static boolean isMatchingBC(TLAFareReferenceType tlaFareReferenceType, FareSummaryDTO fare) throws AmadeusWSException {
		String fb = AmadeusCommonUtil.getFareBasisFromAmadeusFBC(tlaFareReferenceType.getValue());
		String bc = tlaFareReferenceType.getResBookDesigCode();
		if (bc == null) {
			bc = AmadeusCommonUtil.getBookingClassFromAmadeusFBC(tlaFareReferenceType.getValue());
		}

		String mappedBc = AmadeusCommonUtil.getBookingClassCode(fare);
		String mappedFareCode = AmadeusCommonUtil.getFareCode(fare);
		if (bc.equals(mappedBc) && fb.equals(mappedFareCode)) {
			return true;
		}
		return false;
	}

	/**
	 * Compose pax wise fare map based on the fare summary dto
	 * 
	 * @param fareSummaryDTO
	 * @param chargesArr
	 * @param currencyExgRate
	 * @return
	 * @throws AmadeusWSException
	 */
	private static Map<String, TLABookingClassDataType> getPaxTypeFareMap(FareSummaryDTO fareSummaryDTO, List<Double> chargesArr,
			CurrencyExchangeRate currencyExgRate) throws AmadeusWSException {
		Map<String, TLABookingClassDataType> paxTypeFaresMap = new HashMap<String, TLABookingClassDataType>();
		String bookingClassCode = AmadeusCommonUtil.getBookingClassCode(fareSummaryDTO);
		String fareCode = AmadeusCommonUtil.getFareCode(fareSummaryDTO);
		TLABookingClassDataType adultBC = AmadeusCommonUtil.composeBookingClassData(
				new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)), new BigDecimal(chargesArr.get(0)),
				bookingClassCode, fareCode, currencyExgRate);
		paxTypeFaresMap.put(AmadeusConstants.ADULT, adultBC);

		TLABookingClassDataType child = AmadeusCommonUtil.composeBookingClassData(
				new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)), new BigDecimal(chargesArr.get(2)),
				bookingClassCode, fareCode, currencyExgRate);
		paxTypeFaresMap.put(AmadeusConstants.CHILD, child);

		TLABookingClassDataType infant = AmadeusCommonUtil.composeBookingClassData(
				new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)), new BigDecimal(chargesArr.get(1)),
				bookingClassCode, fareCode, currencyExgRate);
		paxTypeFaresMap.put(AmadeusConstants.INFANT, infant);

		return paxTypeFaresMap;
	}
}
