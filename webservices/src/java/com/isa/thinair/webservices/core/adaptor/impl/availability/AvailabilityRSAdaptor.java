package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.AAPricedItinerariesType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.webservices.core.adaptor.Adaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;

public class AvailabilityRSAdaptor implements Adaptor<OTAAirAvailRS, AAOTAAirAllPriceAvailRS> {

	@Override
	public AAOTAAirAllPriceAvailRS adapt(OTAAirAvailRS source) {

		AAOTAAirAllPriceAvailRS target = new AAOTAAirAllPriceAvailRS();

		target.setErrors(source.getErrors());
		target.setWarnings(source.getWarnings());
		target.setSuccess(source.getSuccess());

		target.setVersion(source.getVersion());
		target.setTransactionIdentifier(source.getTransactionIdentifier()); // review
		target.setSequenceNmbr(source.getSequenceNmbr());
		target.setEchoToken(source.getEchoToken());
		target.setPrimaryLangID(source.getPrimaryLangID());
		target.setAltLangID(source.getAltLangID());

		target.setComment(source.getComment());
		target.setRetransmissionIndicator(source.isRetransmissionIndicator());
		target.setTimeStamp(source.getTimeStamp());
		target.setTransactionStatusCode(source.getTransactionStatusCode());
		// target.setTarget(source.getTarget());

		OriginDestinationInformationAdaptor infoAdaptor = new OriginDestinationInformationAdaptor();
		AdaptorUtil.adaptCollection(source.getOriginDestinationInformation(), target.getOriginDestinationInformation(),
				infoAdaptor);

		if (source.getAAAirAvailRSExt() != null) {
			PricedItinerariesType sourcePricedIternary = source.getAAAirAvailRSExt().getPricedItineraries();
			PricedItinerariesTypeAdaptor pricingAdaptor = new PricedItinerariesTypeAdaptor();
			AAPricedItinerariesType targetPricedItinerary = pricingAdaptor.adapt(sourcePricedIternary);
			targetPricedItinerary.setSelectedPriceQuote(true);

			AAOTAAirAllPriceAvailRS.AAAirAvailRSExt targetAAAirAvailRSExt = new AAOTAAirAllPriceAvailRS.AAAirAvailRSExt();
			targetAAAirAvailRSExt.getPricedItineraries().add(targetPricedItinerary);
			target.setAAAirAvailRSExt(targetAAAirAvailRSExt);
		}

		return target;
	}

}
