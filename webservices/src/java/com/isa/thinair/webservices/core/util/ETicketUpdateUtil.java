package com.isa.thinair.webservices.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateResponseDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRQ;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRS;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAStatusErrorsAndWarnings;
import com.isaaviation.thinair.webservices.api.eticketupdate.ETicketStatusUpdateType;
import com.isaaviation.thinair.webservices.api.eticketupdate.EticketUserbilityStatusType;

public class ETicketUpdateUtil extends BaseUtil {
	private static final Log log = LogFactory.getLog(ETicketUpdateUtil.class);

	public static AAOTAETicketStatusUpdateRS updateEticketStatus(AAOTAETicketStatusUpdateRQ eTicketStatusUpdateRQ) {
		AAStatusErrorsAndWarnings statusErrorsAndWarnings = new AAStatusErrorsAndWarnings();
		AAOTAETicketStatusUpdateRS response = new AAOTAETicketStatusUpdateRS();
		ETicketUpdateResponseDTO result = null;

		response.setChannelId(eTicketStatusUpdateRQ.getChannelId());
		response.setEchoToken(eTicketStatusUpdateRQ.getEchoToken());
		response.setMessageId(eTicketStatusUpdateRQ.getMessageId());
		response.setPrimaryLangID(eTicketStatusUpdateRQ.getPrimaryLangID());
		response.setSequenceNmbr(eTicketStatusUpdateRQ.getSequenceNmbr());
		response.setTimeStamp(eTicketStatusUpdateRQ.getTimeStamp());
		response.setTransactionIdentifier(eTicketStatusUpdateRQ.getTransactionIdentifier());
		response.setUserId(eTicketStatusUpdateRQ.getUserId());
		response.setVersion(eTicketStatusUpdateRQ.getVersion());

		try {
			ETicketUpdateRequestDTO eTicketUpdateRequestDTO = new ETicketUpdateRequestDTO();
			eTicketUpdateRequestDTO.setFlightNo(eTicketStatusUpdateRQ.getFlightNo());
			eTicketUpdateRequestDTO.setDepartureDate(CommonUtil.parseXMLGregorianCalendar(eTicketStatusUpdateRQ
					.getFlightDepartureDate()));
			eTicketUpdateRequestDTO.setSegmentCode(eTicketStatusUpdateRQ.getSegmentCode());
			eTicketUpdateRequestDTO.seteTicketNo(eTicketStatusUpdateRQ.getETicketNo());
			eTicketUpdateRequestDTO.setStatus(convertStatus(eTicketStatusUpdateRQ.getStatus()));
			eTicketUpdateRequestDTO.setUnaccompaniedETicketNo(eTicketStatusUpdateRQ.getUnaccompaniedETicketNo());
			eTicketUpdateRequestDTO.setUnaccompaniedStatus(convertStatus(eTicketStatusUpdateRQ.getUnaccompaniedStatus()));
			eTicketUpdateRequestDTO.setFirstName(eTicketStatusUpdateRQ.getFirstName());
			eTicketUpdateRequestDTO.setLastName(eTicketStatusUpdateRQ.getLastName());
			eTicketUpdateRequestDTO.setInfantFirstName(eTicketStatusUpdateRQ.getInfantFirstName());
			eTicketUpdateRequestDTO.setInfantLsatName(eTicketStatusUpdateRQ.getInfantLastName());
			eTicketUpdateRequestDTO.setCouponNumber(eTicketStatusUpdateRQ.getCoupenNo());
			eTicketUpdateRequestDTO.setUnaccompaniedCouponNo(eTicketStatusUpdateRQ.getUnaccompaniedCoupenNo());
			eTicketUpdateRequestDTO.setTitle(eTicketStatusUpdateRQ.getTitle());
			eTicketUpdateRequestDTO.setCabinClassCode(eTicketStatusUpdateRQ.getCabinClassCode());

			result = WebServicesModuleUtils.getReservationBD().updateETicketStatus(eTicketUpdateRequestDTO);
			if (result.isNOREC()) {
				statusErrorsAndWarnings.setWarningMessage("E tikcet update done as a NOREC passenger.");
			}
			statusErrorsAndWarnings.setSuccess(true);
		} catch (Exception e) {
			statusErrorsAndWarnings = convertStatusErrorsAndWarningsType(ExceptionUtil.addAAErrrorsAndWarnings(e));
			statusErrorsAndWarnings.setSuccess(false);
			result = new ETicketUpdateResponseDTO();
			result.setUsabilityStatus(EticketUserbilityStatusType.NO_CIN.toString());
			log.error(e);
		}

		response.setStatusErrorsAndWarnings(statusErrorsAndWarnings);
		response.setUsabilityStatus(convertUserbilityStatus(result.getUsabilityStatus()));
		return response;

	}

	private static EticketUserbilityStatusType convertUserbilityStatus(String availablityStatus) {

		if ("OK_CIN".equals(availablityStatus)) {
			return EticketUserbilityStatusType.OK_CIN;
		} else if ("NO_CIN".equals(availablityStatus)) {
			return EticketUserbilityStatusType.NO_CIN;
		} else if ("OK_BDD".equals(availablityStatus)) {
			return EticketUserbilityStatusType.OK_BDD;
		} else if ("NO_BDD".equals(availablityStatus)) {
			return EticketUserbilityStatusType.NO_BDD;
		} else {
			return null;
		}

	}

	private static String convertStatus(ETicketStatusUpdateType status) {
		if (ETicketStatusUpdateType.BOARDED.equals(status)) {
			return EticketStatus.BOARDED.code();
		} else if (ETicketStatusUpdateType.CHECKEDIN.equals(status)) {
			return EticketStatus.CHECKEDIN.code();
		} else if (ETicketStatusUpdateType.OPEN.equals(status)) {
			return EticketStatus.OPEN.code();
		} else {
			return null;
		}

	}

	private static AAStatusErrorsAndWarnings convertStatusErrorsAndWarningsType(
			com.isaaviation.thinair.webservices.api.airinventory.AAStatusErrorsAndWarnings errorsAndWarnings) {
		AAStatusErrorsAndWarnings statusErrorsAndWarnings = new AAStatusErrorsAndWarnings();
		statusErrorsAndWarnings.setErrorCode(errorsAndWarnings.getErrorCode());
		statusErrorsAndWarnings.setErrorText(errorsAndWarnings.getErrorText());
		statusErrorsAndWarnings.setWarningMessage(errorsAndWarnings.getWarningMessage());
		return statusErrorsAndWarnings;
	}

}
