package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRQ.BaggageDetailsRequests.BaggageDetailsRequest;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS.BaggageDetailsResponses;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS.BaggageDetailsResponses.BaggageDetailsResponse;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS.BaggageDetailsResponses.BaggageDetailsResponse.FlightSegmentInfo;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS.BaggageDetailsResponses.OnDBaggageDetailsResponse;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS.BaggageDetailsResponses.OnDBaggageDetailsResponse.OnDFlightSegmentInfo;
import org.opentravel.ota._2003._05.Baggage;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class BaggageDetailsUtil extends BaseUtil {
	private static final Log log = LogFactory.getLog(BaggageDetailsUtil.class);

	public AAOTAAirBaggageDetailsRS getBaggageDetails(AAOTAAirBaggageDetailsRQ aaOtaAirBaggageDetailsRQ,
			Collection<String> privilegesKeys) throws WebservicesException {

		AAOTAAirBaggageDetailsRS aaOtaAirBaggageDetailsRS = null;
		LCCReservationBaggageSummaryTo reservationBaggageSummaryTo = null;
		try {
			List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

			for (BaggageDetailsRequest baggageDetailsRequest : aaOtaAirBaggageDetailsRQ.getBaggageDetailsRequests()
					.getBaggageDetailsRequest()) {
				FlightSegmentTO flightSegmentTo = getFlightSegmentTO(baggageDetailsRequest);
				if (flightSegmentTo.getFlightRefNumber() == null || "".equals(flightSegmentTo.getFlightRefNumber())) {
					// If RPH value is not getting from client side, retrieve flight
					// segment id
					if (flightSegmentTo.getFlightSegId() == null) {
						Collection<Integer> segIds = SegmentUtil.getFlightSegmentIds(flightSegmentTo);
						if (segIds.size() > 0) {
							for (Integer segId : segIds) {
								flightSegmentTo.setFlightSegId(segId);
								flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
								flightSegmentTOs.add(flightSegmentTo);
							}
						}
					} else {
						flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
						flightSegmentTOs.add(flightSegmentTo);
					}
				} else {
					flightSegmentTOs.add(flightSegmentTo);
				}
			}
			// inject other segment information
			injectSegmentDetails(flightSegmentTOs);

			String requestedCurrencyCode = AppSysParamsUtil.getBaseCurrency();
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

			if (showPriceInselectedCurrency) {
				if (aaOtaAirBaggageDetailsRQ.getPriceRequestCurrencyCode() != null && OTAUtils
						.isPreferredCurrencyCodeSupportedByPC(aaOtaAirBaggageDetailsRQ.getPriceRequestCurrencyCode())) {
					requestedCurrencyCode = aaOtaAirBaggageDetailsRQ.getPriceRequestCurrencyCode();
				} else {
					requestedCurrencyCode = principal.getAgentCurrencyCode();
				}
			}

			boolean isAllowTillFinalCutOver = WSReservationUtil.hasPrivilege(privilegesKeys,
					PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER);
			reservationBaggageSummaryTo = getReservationBaggageSummaryTo(flightSegmentTOs);
			reservationBaggageSummaryTo.setAllowTillFinalCutOver(isAllowTillFinalCutOver);

			if (WSAncillaryUtil.isAncillaryAvailable(flightSegmentTOs, LCCAncillaryType.BAGGAGE, reservationBaggageSummaryTo)) {

				List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs, null,
						aaOtaAirBaggageDetailsRQ.getTransactionIdentifier());
				LCCBaggageResponseDTO baggageResponseDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getAvailableBaggages(
						lccBaggageRequestDTOs, aaOtaAirBaggageDetailsRQ.getTransactionIdentifier(), SYSTEM.AA, null, false,
						isAllowTillFinalCutOver, false, ApplicationEngine.WS, reservationBaggageSummaryTo,
						WSAncillaryUtil.getOndSelectedBundledFares(), null, null);

				aaOtaAirBaggageDetailsRS = generateBaggageResponse(baggageResponseDTO, requestedCurrencyCode);
			} else {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_BAGGAGE_DETAILS_NOT_AVAILABLE_FOR_REQUESTED_CARRIER_, "");
			}
		} catch (ModuleException e) {
			log.error(e);
			if (e.getExceptionCode().equals("airinventory.flight.notfound")) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_FLIGHT_DOES_NOT_OPERATE_ON_DATE_REQUESTED, "");
			}
			throw new WebservicesException(e);
		}

		return aaOtaAirBaggageDetailsRS;
	}

	private static FlightSegmentTO getFlightSegmentTO(BaggageDetailsRequest baggageDetailsRequest) {
		FlightSegmentType flightSegmentType = baggageDetailsRequest.getFlightSegmentInfo();
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(flightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setArrivalTerminalName(flightSegmentType.getArrivalAirport().getTerminal());
		flightSegmentTO.setDepartureDateTime(flightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setFlightNumber(flightSegmentType.getFlightNumber());
		flightSegmentTO.setOperatingAirline(flightSegmentType.getOperatingAirline().getCode());
		if (flightSegmentType.isReturnFlag()) {
			flightSegmentTO.setReturnFlag(flightSegmentType.isReturnFlag());
		}
		if (flightSegmentType.getSegmentCode() != null) {
			flightSegmentTO.setSegmentCode(flightSegmentType.getSegmentCode());
		} else {
			flightSegmentTO.setSegmentCode(flightSegmentType.getDepartureAirport().getLocationCode() + "/"
					+ flightSegmentType.getArrivalAirport().getLocationCode());
		}
		if (flightSegmentType.getRPH() != null && !flightSegmentType.getRPH().equals("")) {
			if (flightSegmentType.getRPH().indexOf("$") == -1) {
				flightSegmentTO.setFlightSegId(new Integer(flightSegmentType.getRPH().split("-")[0].trim()));
			} else {
				flightSegmentTO.setFlightSegId(new Integer(flightSegmentType.getRPH().split("\\$")[2].trim()));
			}
		}
		String classOfService = getCabinClassCode(baggageDetailsRequest);
		flightSegmentTO.setCabinClassCode(classOfService);
		flightSegmentTO.setLogicalCabinClassCode(classOfService);
		flightSegmentTO.setFlightRefNumber(flightSegmentType.getRPH());
		CommonServicesUtil.setOndSequence(flightSegmentTO);
		return flightSegmentTO;
	}

	private static void injectSegmentDetails(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		FlightBD flightBD = WebServicesModuleUtils.getFlightBD();
		for (FlightSegmentTO fltSegTO : flightSegmentTOs) {
			FlightSegement fltSeg = flightBD.getFlightSegment(fltSegTO.getFlightSegId());
			fltSegTO.setDepartureDateTimeZulu(fltSeg.getEstTimeDepatureZulu());
			fltSegTO.setArrivalDateTimeZulu(fltSeg.getEstTimeArrivalZulu());
		}

	}

	/**
	 * Compose the baggage request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCBaggageRequestDTO>
	 */
	private static List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String cabinClass,
			String txnId) {
		List<LCCBaggageRequestDTO> bg = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCBaggageRequestDTO bgd = new LCCBaggageRequestDTO();
			bgd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				bgd.setCabinClass(cabinClass);
			} else {
				bgd.setCabinClass(fst.getCabinClassCode());
			}
			bgd.setFlightSegment(fst);
			bg.add(bgd);
		}
		return bg;
	}

	/**
	 * generate WS baggage response
	 * 
	 * @param baggageDetailsDTO
	 * @param currency
	 *            TODO
	 * @return aaOtaAirBaggageDetailsRS
	 * @throws ModuleException
	 */
	public static AAOTAAirBaggageDetailsRS generateBaggageResponse(LCCBaggageResponseDTO baggageDetailsDTO, String currency)
			throws ModuleException {

		AAOTAAirBaggageDetailsRS aaOtaAirBaggageDetailsRS = new AAOTAAirBaggageDetailsRS();

		aaOtaAirBaggageDetailsRS.setErrors(new ErrorsType());
		aaOtaAirBaggageDetailsRS.setWarnings(new WarningsType());

		BaggageDetailsResponses baggageDetailsResponses = new BaggageDetailsResponses();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {

			List<LCCFlightSegmentBaggagesDTO> segmentBaggages = new ArrayList<LCCFlightSegmentBaggagesDTO>();
			String baggageOnDGroupId = null;
			Map<String, List<OnDFlightSegmentInfo>> baggageGroups = new HashMap<String, List<OnDFlightSegmentInfo>>();
			Map<String, LCCFlightSegmentBaggagesDTO> flightSegmentBaggages = new HashMap<String, LCCFlightSegmentBaggagesDTO>();
			for (LCCFlightSegmentBaggagesDTO flightSegmentBaggagesDto : baggageDetailsDTO.getFlightSegmentBaggages()) {
				FlightSegmentTO flightSegmentTO = flightSegmentBaggagesDto.getFlightSegmentTO();

				OnDFlightSegmentInfo flightSegmentInfo = getOnDFlightSegmentInfo(flightSegmentTO);

				if (flightSegmentTO.getBaggageONDGroupId() == null) {
					segmentBaggages.add(flightSegmentBaggagesDto);
				} else {

					baggageOnDGroupId = flightSegmentTO.getBaggageONDGroupId();
					flightSegmentBaggages.put(baggageOnDGroupId, flightSegmentBaggagesDto);
					if (baggageGroups.get(baggageOnDGroupId) == null) {
						List<OnDFlightSegmentInfo> ondSegments = new ArrayList<OnDFlightSegmentInfo>();
						baggageGroups.put(baggageOnDGroupId, ondSegments);
						baggageGroups.get(baggageOnDGroupId).add(flightSegmentInfo);
					} else {
						baggageGroups.get(baggageOnDGroupId).add(flightSegmentInfo);
					}

				}
			}
			for (String groupId : flightSegmentBaggages.keySet()) {
				OnDBaggageDetailsResponse baggageDetailsResponse = new OnDBaggageDetailsResponse();
				baggageDetailsResponse.getOnDFlightSegmentInfo().addAll(baggageGroups.get(groupId));
				for (LCCBaggageDTO baggage : flightSegmentBaggages.get(groupId).getBaggages()) {
					Baggage wsBaggage = new Baggage();
					wsBaggage.setBaggageCode(baggage.getBaggageName());
					wsBaggage.setBaggageDescription(baggage.getBaggageDescription());
					wsBaggage.setBaggageCharge(WSReservationUtil.getAmountInSpecifiedCurrency(baggage.getBaggageCharge(),
							currency, exchangeRateProxy));
					wsBaggage.setCurrencyCode(currency);
					baggageDetailsResponse.getBaggage().add(wsBaggage);
				}
				baggageDetailsResponses.getOnDBaggageDetailsResponse().add(baggageDetailsResponse);
			}
			// This means segment baggages are exists with OnD baggages
			if (segmentBaggages.size() > 0) {
				for (LCCFlightSegmentBaggagesDTO flightSegmentBaggagesDto : segmentBaggages) {
					BaggageDetailsResponse segmentBaggageDetailsResponse = new BaggageDetailsResponse();
					FlightSegmentTO flightSegmentTO = flightSegmentBaggagesDto.getFlightSegmentTO();
					FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

					segmentBaggageDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

					for (LCCBaggageDTO baggage : flightSegmentBaggagesDto.getBaggages()) {
						Baggage wsBaggage = new Baggage();
						wsBaggage.setBaggageCode(baggage.getBaggageName());
						wsBaggage.setBaggageDescription(baggage.getBaggageDescription());
						wsBaggage.setBaggageCharge(WSReservationUtil.getAmountInSpecifiedCurrency(baggage.getBaggageCharge(),
								currency, exchangeRateProxy));
						wsBaggage.setCurrencyCode(currency);
						segmentBaggageDetailsResponse.getBaggage().add(wsBaggage);
					}
					baggageDetailsResponses.getBaggageDetailsResponse().add(segmentBaggageDetailsResponse);

				}
			}

		} else {
			// This is completely for segment baggages
			for (LCCFlightSegmentBaggagesDTO flightSegmentBaggagesDto : baggageDetailsDTO.getFlightSegmentBaggages()) {
				BaggageDetailsResponse baggageDetailsResponse = new BaggageDetailsResponse();
				FlightSegmentTO flightSegmentTO = flightSegmentBaggagesDto.getFlightSegmentTO();
				FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

				baggageDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

				for (LCCBaggageDTO baggage : flightSegmentBaggagesDto.getBaggages()) {
					Baggage wsBaggage = new Baggage();
					wsBaggage.setBaggageCode(baggage.getBaggageName());
					wsBaggage.setBaggageDescription(baggage.getBaggageDescription());
					wsBaggage.setBaggageCharge(WSReservationUtil.getAmountInSpecifiedCurrency(baggage.getBaggageCharge(),
							currency, exchangeRateProxy));
					wsBaggage.setCurrencyCode(currency);
					baggageDetailsResponse.getBaggage().add(wsBaggage);
				}
				baggageDetailsResponses.getBaggageDetailsResponse().add(baggageDetailsResponse);

			}
		}

		aaOtaAirBaggageDetailsRS.setBaggageDetailsResponses(baggageDetailsResponses);
		aaOtaAirBaggageDetailsRS.setSuccess(new SuccessType());
		aaOtaAirBaggageDetailsRS.setOnDBaggagesEnabled(AppSysParamsUtil.isONDBaggaeEnabled());
		return aaOtaAirBaggageDetailsRS;
	}

	/**
	 * Build flight segment Info
	 * 
	 * @param flightSegmentTO
	 * @return flightSegmentInfo
	 * @throws ModuleException
	 */
	public static FlightSegmentInfo getFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		FlightSegmentInfo flightSegmentInfo = new FlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfo.setRPH(flightSegmentTO.getFlightSegId().toString());
		return flightSegmentInfo;
	}

	/**
	 * Build flight segment Info
	 * 
	 * @param flightSegmentTO
	 * @return flightSegmentInfo
	 * @throws ModuleException
	 */
	public static OnDFlightSegmentInfo getOnDFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		OnDFlightSegmentInfo flightSegmentInfo = new OnDFlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfo.setRPH(flightSegmentTO.getFlightRefNumber());
		return flightSegmentInfo;
	}

	private static Map<Integer, FlightSegmentTO> getFltSegIdWiseFlightSegmentDTOs(SelectedFlightDTO selectedFlightDTO) {
		Map<Integer, FlightSegmentTO> fltSegIdWiseFlightSegmentDTOs = new HashMap<Integer, FlightSegmentTO>();

		AvailableIBOBFlightSegment availableIBOBFlightSegment = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);

		if (availableIBOBFlightSegment != null) {
			Collection<FlightSegmentDTO> aaFlightSegmentDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();

			if (aaFlightSegmentDTOs != null && aaFlightSegmentDTOs.size() > 0) {
				for (FlightSegmentDTO flightSegmentDTO : aaFlightSegmentDTOs) {
					FlightSegmentTO flightSegTO = new FlightSegmentTO();
					flightSegTO.setFlightSegId(flightSegmentDTO.getSegmentId());
					flightSegTO.setSegmentCode(flightSegmentDTO.getSegmentCode());
					flightSegTO.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
					flightSegTO.setDepartureDateTime(flightSegmentDTO.getDepartureDateTime());
					flightSegTO.setArrivalDateTimeZulu(flightSegmentDTO.getArrivalDateTimeZulu());
					flightSegTO.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
					flightSegTO.setCabinClassCode(availableIBOBFlightSegment.getCabinClassCode());
					flightSegTO.setReturnFlag(false);

					fltSegIdWiseFlightSegmentDTOs.put(flightSegTO.getFlightSegId(), flightSegTO);
				}
			}
		}

		availableIBOBFlightSegment = selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND);

		if (availableIBOBFlightSegment != null) {
			Collection<FlightSegmentDTO> aaFlightSegmentDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();

			if (aaFlightSegmentDTOs != null && aaFlightSegmentDTOs.size() > 0) {
				for (FlightSegmentDTO flightSegmentDTO : aaFlightSegmentDTOs) {
					FlightSegmentTO flightSegTO = new FlightSegmentTO();
					flightSegTO.setFlightSegId(flightSegmentDTO.getSegmentId());
					flightSegTO.setSegmentCode(flightSegmentDTO.getSegmentCode());
					flightSegTO.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
					flightSegTO.setDepartureDateTime(flightSegmentDTO.getDepartureDateTime());
					flightSegTO.setArrivalDateTimeZulu(flightSegmentDTO.getArrivalDateTimeZulu());
					flightSegTO.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
					flightSegTO.setCabinClassCode(availableIBOBFlightSegment.getCabinClassCode());
					flightSegTO.setReturnFlag(true);

					fltSegIdWiseFlightSegmentDTOs.put(flightSegTO.getFlightSegId(), flightSegTO);
				}
			}
		}

		return fltSegIdWiseFlightSegmentDTOs;
	}

	public static Map<String, Map<Integer, LCCBaggageDTO>> getSelectedBaggageDetails(
			Map<String, Map<Integer, String>> travelerFlightDetailsWithBaggages, SelectedFlightDTO selectedFlightDTO,
			Map<String, Integer> baggageOndGrpIdWiseCount) throws ModuleException, WebservicesException {

		Map<String, Map<Integer, LCCBaggageDTO>> segmentWithBaggages = new HashMap<String, Map<Integer, LCCBaggageDTO>>();

		Map<Integer, FlightSegmentTO> fltSegIdWiseFlightSegmentDTOs = getFltSegIdWiseFlightSegmentDTOs(selectedFlightDTO);

		Map<Integer, List<FlightBaggageDTO>> baggages = null;

		List<FlightSegmentTO> flightSegs = new ArrayList<FlightSegmentTO>(fltSegIdWiseFlightSegmentDTOs.values());
		LCCReservationBaggageSummaryTo reservationBaggageSummaryTo;
		BaggageRq baggageRq;

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			reservationBaggageSummaryTo = getReservationBaggageSummaryTo(flightSegs);
			baggageRq = new BaggageRq();
			baggageRq.setOwnReservation(true);
			baggageRq.setRequote(false);
			baggageRq.setCheckCutoverTime(true);
			baggageRq.setBookingClasses(reservationBaggageSummaryTo.getBookingClasses());
			baggageRq.setClassesOfService(reservationBaggageSummaryTo.getClassesOfService());
			baggageRq.setLogicalCC(reservationBaggageSummaryTo.getLogicalCC());
			baggageRq.setFlightSegmentTOs(flightSegs);
			baggageRq.setAgent(ThreadLocalData.getCurrentUserPrincipal().getAgentCode());
			baggageRq.setSalesChannel(ThreadLocalData.getCurrentUserPrincipal().getSalesChannel());

			baggages = WebServicesModuleUtils.getBaggageBD().getBaggage(baggageRq);

			if (baggages != null && baggages.size() > 0) {
				for (Entry<Integer, List<FlightBaggageDTO>> entry : baggages.entrySet()) {
					String ondGroupId = BeanUtils.getFirstElement(entry.getValue()).getOndGroupId();
					Integer count = baggageOndGrpIdWiseCount.get(ondGroupId);

					if (count == null) {
						baggageOndGrpIdWiseCount.put(ondGroupId, new Integer(1));
					} else {
						baggageOndGrpIdWiseCount.put(ondGroupId, ++count);
					}
				}
			}
		} else {
			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			for (Integer fltSegId : fltSegIdWiseFlightSegmentDTOs.keySet()) {
				flightSegIdWiseCos.put(fltSegId, null);
			}
			baggages = WebServicesModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos, false, false);
		}

		if (baggages.isEmpty()) {
			throw new WebservicesException(
					IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_BAGGAGES_ARE_NOT_AVAILABLE_AT_THE_MOMENT, "");
		}

		// override baggages if bundled fare is selected
		setBaggageOffers(baggages, selectedFlightDTO);

		for (String travelerRef : travelerFlightDetailsWithBaggages.keySet()) {
			// FlightSegmentId -> Baggage
			Map<Integer, String> flightDetailsWithBaggages = travelerFlightDetailsWithBaggages.get(travelerRef);
			for (Integer flightSegId : flightDetailsWithBaggages.keySet()) {
				String selectedBaggage = flightDetailsWithBaggages.get(flightSegId);
				if (selectedBaggage != null) {
					for (FlightSegmentTO flightSegmentTO : fltSegIdWiseFlightSegmentDTOs.values()) {
						if (flightSegId.equals(flightSegmentTO.getFlightSegId())) {
							Collection<FlightBaggageDTO> flightBaggageDTOs = baggages.get(flightSegId);
							boolean isBaggageFound = false;
							for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
								if (flightBaggageDTO.getBaggageName().equals(selectedBaggage)) {
									isBaggageFound = true;
									LCCBaggageDTO lccBaggageDTO = new LCCBaggageDTO();
									lccBaggageDTO.setBaggageCharge(flightBaggageDTO.getAmount());
									lccBaggageDTO.setBaggageName(flightBaggageDTO.getBaggageName());
									lccBaggageDTO.setOndBaggageChargeId(BeanUtils.nullHandler(flightBaggageDTO.getChargeId()));
									lccBaggageDTO.setOndBaggageGroupId(BeanUtils.nullHandler(flightBaggageDTO.getOndGroupId()));

									if (segmentWithBaggages.get(travelerRef) == null) {
										segmentWithBaggages.put(travelerRef, new HashMap<Integer, LCCBaggageDTO>());
									}
									segmentWithBaggages.get(travelerRef).put(flightSegId, lccBaggageDTO);

								}
							}

							if (!isBaggageFound) {
								throw new WebservicesException(
										IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_BAGGAGES_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
										selectedBaggage);
							}
						}

					}

				}
			}
		}

		return segmentWithBaggages;
	}

	public static void setBaggageOffers(Map<Integer, List<FlightBaggageDTO>> baggages, SelectedFlightDTO selectedFlightDTO)
			throws ModuleException {
		Map<Integer, Integer> fltSegWiseBaggageTemplates = WSAncillaryUtil
				.getFlightSegmentWiseBundledServiceTemplate(selectedFlightDTO, EXTERNAL_CHARGES.BAGGAGE.toString());

		if (fltSegWiseBaggageTemplates != null && !fltSegWiseBaggageTemplates.isEmpty()) {
			for (Entry<Integer, List<FlightBaggageDTO>> seatEntry : baggages.entrySet()) {
				Integer fltSegId = seatEntry.getKey();

				String cabinClassCode = seatEntry.getValue() != null
						? seatEntry.getValue().iterator().next().getCabinClassCode()
						: null;

				if (fltSegWiseBaggageTemplates.containsKey(fltSegId) && fltSegWiseBaggageTemplates.get(fltSegId) != null) {
					Integer baggageTemplateId = fltSegWiseBaggageTemplates.get(fltSegId);

					List<FlightBaggageDTO> bundledFareBaggages = AirproxyModuleUtils.getBaggageBusinessDelegate()
							.getBaggageFromTemplate(baggageTemplateId, null, cabinClassCode);

					AncillaryUtil.injectBaggageONDGroupID(bundledFareBaggages, getBaggageONDGroupID(baggages.get(fltSegId)));

					baggages.put(fltSegId, bundledFareBaggages);
				}
			}
		}
	}

	/**
	 * @param baggageDetailsMap
	 * @return externalCharegs
	 * @throws ModuleException
	 */
	public static Map<String, List<ExternalChgDTO>> getExternalChargesMapForBaggages(
			Map<String, Map<Integer, LCCBaggageDTO>> baggageDetailsMap, SelectedFlightDTO selectedFlightDTO,
			Map<String, Integer> baggageOndGrpIdWiseCount) throws ModuleException {

		Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
		exChargestypes.add(EXTERNAL_CHARGES.BAGGAGE);
		@SuppressWarnings("unchecked")
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD()
				.getQuotedExternalCharges(exChargestypes, null, null);

		BaggageExternalChgDTO quotedChgDTO = (BaggageExternalChgDTO) quotedMap.get(EXTERNAL_CHARGES.BAGGAGE);

		if (quotedChgDTO == null) {
			throw new ModuleException("Can not locate the Baggage external charges details");
		}

		Map<String, List<ExternalChgDTO>> externalCharges = new HashMap<String, List<ExternalChgDTO>>();

		for (String travelerRef : baggageDetailsMap.keySet()) {
			List<ExternalChgDTO> chargesList = new ArrayList<ExternalChgDTO>();
			Map<Integer, LCCBaggageDTO> baggagesMap = baggageDetailsMap.get(travelerRef);
			for (Integer flightSegmentId : baggagesMap.keySet()) {
				LCCBaggageDTO baggageDTO = baggagesMap.get(flightSegmentId);

				BaggageExternalChgDTO externalChargeTO = new BaggageExternalChgDTO();

				externalChargeTO.setChargeCode(baggageDTO.getBaggageName());
				externalChargeTO.setChargeDescription(quotedChgDTO.getChargeCode() + "/" + quotedChgDTO.getChargeDescription());
				externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
				externalChargeTO.setChgRateId(quotedChgDTO.getChgRateId());
				externalChargeTO.setExternalChargesEnum(EXTERNAL_CHARGES.BAGGAGE);
				externalChargeTO.setOndBaggageGroupId(baggageDTO.getOndBaggageGroupId());
				externalChargeTO.setOndBaggageChargeGroupId(baggageDTO.getOndBaggageChargeId());

				if (AppSysParamsUtil.isONDBaggaeEnabled()) {
					BigDecimal baggageCharge = AccelAeroCalculator.divide(baggageDTO.getBaggageCharge(),
							baggageOndGrpIdWiseCount.get(baggageDTO.getOndBaggageGroupId()));

					externalChargeTO.setAmount(baggageCharge);
					externalChargeTO.setBaggageCharge(baggageCharge);
				} else {
					externalChargeTO.setAmount(baggageDTO.getBaggageCharge());
					externalChargeTO.setBaggageCharge(baggageDTO.getBaggageCharge());
				}

				externalChargeTO.setBaggageCode(baggageDTO.getBaggageName());
				externalChargeTO.setFlightSegId(flightSegmentId);

				chargesList.add(externalChargeTO);

			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}

	public static LCCReservationBaggageSummaryTo getReservationBaggageSummaryTo(List<FlightSegmentTO> flightSegmentTOs) {
		LCCReservationBaggageSummaryTo baggageSummaryTo = new LCCReservationBaggageSummaryTo();
		Map<String, String> bookingClasses = new HashMap<String, String>();
		Map<String, String> classesOfService = new HashMap<String, String>();

		FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);

		if (fareSegChargeTO != null) {
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				for (OndFareSegChargeTO ondFareSegChargeTO : fareSegChargeTO.getOndFareSegChargeTOs()) {
					if (ondFareSegChargeTO.getFsegBCAlloc().containsKey(flightSegmentTO.getFlightSegId())) {
						for (BookingClassAlloc bookingClassAlloc : ondFareSegChargeTO.getFsegBCAlloc()
								.get(flightSegmentTO.getFlightSegId())) {
							bookingClasses.put(flightSegmentTO.getSegmentCode(), bookingClassAlloc.getBookingClassCode());
						}

					}
				}

				classesOfService.put(flightSegmentTO.getFlightRefNumber(), flightSegmentTO.getCabinClassCode());
			}
		}

		baggageSummaryTo.setBookingClasses(bookingClasses);
		baggageSummaryTo.setClassesOfService(classesOfService);

		return baggageSummaryTo;
	}

	private static String getBaggageONDGroupID(Collection<FlightBaggageDTO> flightBaggageDTOs) {
		String groupID = null;

		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			groupID = flightBaggageDTO.getOndGroupId();
			break;
		}

		return groupID;
	}

	private static String getCabinClassCode(BaggageDetailsRequest baggageDetailsRequest) {
		String cabinClassCode = null;
		if (baggageDetailsRequest.getBaggageDetails() != null && baggageDetailsRequest.getBaggageDetails().getCabinClass() != null
				&& !baggageDetailsRequest.getBaggageDetails().getCabinClass().isEmpty()) {
			cabinClassCode = baggageDetailsRequest.getBaggageDetails().getCabinClass().get(0).getCabinType().value();
		}

		if (cabinClassCode == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClassCode = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClassCode = "Y";
			}
		}
		return cabinClassCode;
	}
}