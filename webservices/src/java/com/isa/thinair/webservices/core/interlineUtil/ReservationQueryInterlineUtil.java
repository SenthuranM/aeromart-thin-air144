/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.activation.DataHandler;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRQ;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRS;
import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType.PTCFareBreakdowns;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment.PaymentDetails;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.AirTravelerType.ETicketInfo;
import org.opentravel.ota._2003._05.AirTravelerType.Telephone;
import org.opentravel.ota._2003._05.AirTravelerType.TravelerRefNumber;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BookingPriceInfoType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.ETicketStatusType;
import org.opentravel.ota._2003._05.ETicketUsedStatusType;
import org.opentravel.ota._2003._05.EticketInfoType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.FareType.Fees;
import org.opentravel.ota._2003._05.FareType.Taxes;
import org.opentravel.ota._2003._05.FareType.TotalFare;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.AirReadRequest;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.GlobalReservationReadRequest;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PaymentCardType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmountInPayCur;
import org.opentravel.ota._2003._05.PaymentFormType.Cash;
import org.opentravel.ota._2003._05.PaymentFormType.LoyaltyRedemption;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.SSRType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests.BaggageRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.InsuranceRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.InsuranceRequests.InsuranceRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests.MealRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests.SeatRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests.SpecialServiceRequest;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TPAExtensionsType;
import org.opentravel.ota._2003._05.TaxInvoice;
import org.opentravel.ota._2003._05.TaxInvoiceAirlineDetails;
import org.opentravel.ota._2003._05.TaxInvoiceOtherDetails;
import org.opentravel.ota._2003._05.TaxInvoiceRecipientDetails;
import org.opentravel.ota._2003._05.TicketType;
import org.opentravel.ota._2003._05.TicketingInfoType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.ExtensionsUtil;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.WSFlexiFareUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAdminInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPTCCountType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPTCCountsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAResSummaryType;

/**
 * @author nafly
 * 
 */
public class ReservationQueryInterlineUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(ReservationQueryInterlineUtil.class);

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	private static final String ITINERARY_FILE_EXTN = ".html";

	/**
	 * 
	 * @param oTAReadRQ
	 * @param loadDataOptions
	 * @param userPrincipal
	 * @param pnrModesDTO
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 * @throws ParseException
	 */
	public void getReservation(OTAAirBookRS airBookRS, OTAReadRQ oTAReadRQ, AALoadDataOptionsType loadDataOptions,
			UserPrincipal userPrincipal, boolean isNewBooking) throws ModuleException, WebservicesException, ParseException {

		ReservationSearchDTO reservationSearchDTO = null;
		LCCClientReservation lccClientReservation = null;
		ReservationListTO reservationListTO = null;

		if (oTAReadRQ.getUniqueID() == null) {
			log.info("###WSLOGS### UNIQUE ID IS NULL");
			reservationSearchDTO = extractReservationSearchReqParams(oTAReadRQ);
		} else if (oTAReadRQ.getUniqueID().getType() != null
				&& oTAReadRQ.getUniqueID().getType()
						.equals(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION))) {
			log.info("###WSLOGS### UNIQUE ID IS NOT NULL : " + oTAReadRQ.getUniqueID().getID());
			reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setPnr(oTAReadRQ.getUniqueID().getID());
		}

		// If an interline booking does not have any reference in own carrier DB, we do not load that
		// Eg : if a 3O booking is done by a 3O agent, and if G9 agents try to load it, it will not show it unless
		// some one else has loaded it via xbe ir ibe.
		// But if a 3O booking was done by a G9 agent, this can be loaded by G9 agents
		reservationSearchDTO.setSearchAll(false);
		if (!isNewBooking) {
			reservationSearchDTO = includeReporingAgentSearchParams(reservationSearchDTO);
		}
		log.info("###WSLOGS### LOADING RESERVATION STARTED*** PNR : " + reservationSearchDTO.getPnr() + " ***PAGE NO: "
				+ reservationSearchDTO.getPageNo() + " ***SEARCH ALL: " + reservationSearchDTO.isSearchAll());
		Collection<ReservationListTO> reservationListTOs = WebServicesModuleUtils.getAirproxyReservationBD().searchReservations(
				reservationSearchDTO, userPrincipal, CommonServicesUtil.getTrackInfo(userPrincipal));
		log.info("###WSLOGS### LOADING RESERVATION END***" + reservationSearchDTO.getPnr() + " RESERVATION LIST IS EMPTY: "
				+ reservationListTOs.isEmpty());

		if (isNewBooking) {
			reservationListTO = new ReservationListTO();
			reservationListTO.setPnrNo(reservationSearchDTO.getPnr());

			lccClientReservation = getReservation(reservationListTO, loadDataOptions, userPrincipal);

			if (lccClientReservation != null) {
				log.info("###WSLOGS### GENERATING BOOKRS STARTED***");
				generateOTAAirBookRS(airBookRS, oTAReadRQ, lccClientReservation, loadDataOptions);
				log.info("###WSLOGS### GENERATING BOOKRS END***");
			}
		} else if (reservationListTOs != null && !reservationListTOs.isEmpty()) {

			reservationListTO = BeanUtils.getFirstElement(reservationListTOs);
			reservationSearchDTO.setPnr(reservationListTO.getPnrNo());

			// load full reservation
			lccClientReservation = getReservation(reservationListTO, loadDataOptions, userPrincipal);

			validateReservation(oTAReadRQ, lccClientReservation);

			authorizeReservationAccess(lccClientReservation, false);

			if (lccClientReservation != null) {
				log.info("###WSLOGS### GENERATING BOOKRS STARTED***");
				generateOTAAirBookRS(airBookRS, oTAReadRQ, lccClientReservation, loadDataOptions);
				log.info("###WSLOGS### GENERATING BOOKRS END***");
			}
		} else {
			if (isNewBooking) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_MATCHING_BOOKINGS_FOUND_,
						"Please try Loading the booking again. PNR : " + reservationSearchDTO.getPnr());
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_MATCHING_BOOKINGS_FOUND_,
						"No Matching Booking Found for PNR : " + reservationSearchDTO.getPnr());
			}
		}
	}

	public AAOTAItineraryRS getItinerary(OTAReadRQ oTAReadRQ, boolean includeAttachment) throws ModuleException,
			WebservicesException, ParseException {
		AAOTAItineraryRS aaOTAItineraryRS = new AAOTAItineraryRS();
		;
		String itinerary = null;
		DataHandler dataHandler = null;

		String pnr = oTAReadRQ.getReadRequests().getReadRequest().get(0).getUniqueID().getID();

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnr);
		reservationSearchDTO.setSearchAll(false);
		reservationSearchDTO = includeReporingAgentSearchParams(reservationSearchDTO);

		Collection<ReservationListTO> reservationListTOs = WebServicesModuleUtils.getAirproxyReservationBD().searchReservations(
				reservationSearchDTO, userPrincipal, CommonServicesUtil.getTrackInfo(userPrincipal));

		if (reservationListTOs != null && !reservationListTOs.isEmpty()) {
			ReservationListTO reservationListTO = BeanUtils.getFirstElement(reservationListTOs);
			reservationSearchDTO.setPnr(reservationListTO.getPnrNo());

			boolean isInterlineBooking = (reservationListTO.getOriginatorPnr() != null && reservationListTO.getOriginatorPnr()
					.length() > 0) ? true : false;

			// TODO: Revise and check whether we should load pnr to get
			// preferred language
			// and show itinerary in that. Keeping en as default for the time
			// being as per
			// earlier implementation

			LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(pnr, isInterlineBooking,
					Locale.ENGLISH.toString(), false, null, null, null);

			itinerary = BookInterlineUtil.getItineraryContent(pnrModesDTO, userPrincipal, isInterlineBooking);

			if (includeAttachment) {
				if (itinerary != null) {
					try {
						String fileName = PlatformConstants.getAbsAttachmentsPath() + File.separatorChar + "Itinerary" + pnr
								+ ITINERARY_FILE_EXTN;
						FileOutputStream fos = new FileOutputStream(fileName);
						fos.write(itinerary.getBytes());
						fos.close();
						FileInputStream fis = new FileInputStream(fileName);
						byte[] istContentArr = new byte[itinerary.getBytes().length];
						fis.read(istContentArr);
						dataHandler = new DataHandler(new String(istContentArr), "text/html");

					} catch (Exception e) {
						log.error("error writing itinerary to html file ", e);
					}
				}
				aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(dataHandler);
			}
			aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(itinerary);
			aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(new SuccessType());
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
					"No matching booking found");
		}

		return aaOTAItineraryRS;
	}

	public AAOTAAirTaxInvoiceGetRS getTaxInvoice(AAOTAAirTaxInvoiceGetRS aaOTAAirTaxInvoiceGetRS, AAOTAAirTaxInvoiceGetRQ aaOTAAirTaxInvoiceGetRQ) throws ModuleException,
			WebservicesException, ParseException {
		
		LCCClientReservation lccClientReservation = null;
		String pnr = aaOTAAirTaxInvoiceGetRQ.getTaxInvoiceByPnr().getUniqueID().getID();
		
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
		reservationSearchDTO.setPnr(pnr);
		reservationSearchDTO.setSearchAll(false);
		reservationSearchDTO = includeReporingAgentSearchParams(reservationSearchDTO);

		Collection<ReservationListTO> reservationListTOs = WebServicesModuleUtils.getAirproxyReservationBD().searchReservations(
				reservationSearchDTO, userPrincipal, CommonServicesUtil.getTrackInfo(userPrincipal));

		if (reservationListTOs != null && !reservationListTOs.isEmpty()) {
			ReservationListTO reservationListTO = BeanUtils.getFirstElement(reservationListTOs);
			reservationSearchDTO.setPnr(reservationListTO.getPnrNo());

			boolean isInterlineBooking = (reservationListTO.getOriginatorPnr() != null && reservationListTO.getOriginatorPnr()
					.length() > 0) ? true : false;


			LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(pnr, isInterlineBooking,
					Locale.ENGLISH.toString(), false, null, null, null);
			
			ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
			modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
			modificationParamRQInfo.setIsRegisteredUser(false);
			
			lccClientReservation = getReservation(pnrModesDTO, modificationParamRQInfo, userPrincipal);

			for (com.isa.thinair.airreservation.api.model.TaxInvoice taxInvoiceRes: lccClientReservation.getTaxInvoicesList()){
				TaxInvoice taxInvoiceWs = new TaxInvoice();
				if (taxInvoiceRes.getCurrencyCode() != null){
					taxInvoiceWs.setCurrencyCode(taxInvoiceRes.getCurrencyCode());
				} if (taxInvoiceRes.getInvoiceType() != null){
					taxInvoiceWs.setInvoiceType(taxInvoiceRes.getInvoiceType());
				} /* if (taxInvoiceRes.getNonTaxableAmount() != null){
					taxInvoiceWs.setNonTaxableAmount(taxInvoiceRes.getNonTaxableAmount());
				} */ if (taxInvoiceRes.getOriginalPnr() != null){
					taxInvoiceWs.setOriginalPnr(taxInvoiceRes.getOriginalPnr());
				} if (taxInvoiceRes.getOriginalTaxInvoiceId() != null){
					taxInvoiceWs.setOriginalTaxInvoiceId(taxInvoiceRes.getOriginalTaxInvoiceId());
				} if (taxInvoiceRes.getStateCode() != null){
					taxInvoiceWs.setStateCode(taxInvoiceRes.getStateCode());
				} /* if (taxInvoiceRes.getTaxableAmount() != null){
					taxInvoiceWs.setTaxableAmount(taxInvoiceRes.getTaxableAmount());
				} */ if (taxInvoiceRes.getTaxInvoiceId() != null){
					taxInvoiceWs.setTaxInvoiceId(taxInvoiceRes.getTaxInvoiceId());
				} if (taxInvoiceRes.getTaxRate1Amount() != null){
					taxInvoiceWs.setTaxRate1Amount(taxInvoiceRes.getTaxRate1Amount());
				} if (taxInvoiceRes.getTaxRate1Id() != null){
					taxInvoiceWs.setTaxRate1Id(taxInvoiceRes.getTaxRate1Id());
				} if (taxInvoiceRes.getTaxRate2Amount() != null){
					taxInvoiceWs.setTaxRate2Amount(taxInvoiceRes.getTaxRate2Amount());
				} if (taxInvoiceRes.getTaxRate2Id() != null){
					taxInvoiceWs.setTaxRate2Id(taxInvoiceRes.getTaxRate2Id());
				} if (taxInvoiceRes.getTaxRate3Amount() != null){
					taxInvoiceWs.setTaxRate3Amount(taxInvoiceRes.getTaxRate3Amount());
				} if (taxInvoiceRes.getTaxRate3Id() != null){
					taxInvoiceWs.setTaxRate3Id(taxInvoiceRes.getTaxRate3Id());
				} if (taxInvoiceRes.getTaxRegistrationNumber() != null){
					taxInvoiceWs.setTaxRegistrationNumber(taxInvoiceRes.getTaxRegistrationNumber());
				} /* if (taxInvoiceRes.getTnxSeq() != null){
					taxInvoiceWs.setTnxSeq(taxInvoiceRes.getTnxSeq());
				} if (taxInvoiceRes.getTotalDiscount() != null){
					taxInvoiceWs.setTotalDiscount(taxInvoiceRes.getTotalDiscount());
				} */ if (taxInvoiceRes.getDateOfIssue() != null){
					taxInvoiceWs.setDateOfIssue(taxInvoiceRes.getDateOfIssue().toString());
				} if (taxInvoiceRes.getDateOfIssueOriginalTaxInvoice() != null){
					taxInvoiceWs.setDateOfIssueOriginalTaxInvoice(taxInvoiceRes.getDateOfIssueOriginalTaxInvoice().toString());
				}
				
				// add airline details
				TaxInvoiceAirlineDetails taxInvoiceAirlineDetails = new TaxInvoiceAirlineDetails();
				taxInvoiceAirlineDetails.setNameOfTheAirline(taxInvoiceRes.getNameOfTheAirline());
				taxInvoiceAirlineDetails.setAirlineOfficeSTAddress1(taxInvoiceRes.getAirlineOfficeSTAddress1());
				taxInvoiceAirlineDetails.setAirlineOfficeSTAddress2(taxInvoiceRes.getAirlineOfficeSTAddress2());
				taxInvoiceAirlineDetails.setAirlineOfficeCity(taxInvoiceRes.getAirlineOfficeCity());
				taxInvoiceAirlineDetails.setAirlineOfficeCountryCode(taxInvoiceRes.getAirlineOfficeCountryCode());
					
				// add TaxInvoiceRecipientDetails
				TaxInvoiceRecipientDetails taxInvoiceRecipientDetails = new TaxInvoiceRecipientDetails();
				taxInvoiceRecipientDetails.setNameOfTheRecipient(taxInvoiceRes.getNameOfTheRecipient());
				taxInvoiceRecipientDetails.setGstinOfTheRecipient(taxInvoiceRes.getGstinOfTheRecipient());
				taxInvoiceRecipientDetails.setStateOfTheRecipient(taxInvoiceRes.getStateCodeOfTheRecipient());
				taxInvoiceRecipientDetails.setStateCodeOfTheRecipient(taxInvoiceRes.getStateCodeOfTheRecipient());
				taxInvoiceRecipientDetails.setRecipientStreetAddress1(taxInvoiceRes.getRecipientStreetAddress1());
				taxInvoiceRecipientDetails.setRecipientStreetAddress2(taxInvoiceRes.getRecipientStreetAddress2());
				taxInvoiceRecipientDetails.setRecipientCity(taxInvoiceRes.getRecipientCity());
				taxInvoiceRecipientDetails.setRecipientCountryCode(taxInvoiceRes.getRecipientCountryCode());

				// add TaxInvoiceOtherDetails
				TaxInvoiceOtherDetails taxInvoiceOtherDetails = new TaxInvoiceOtherDetails();
				taxInvoiceOtherDetails.setGstinForInvoiceState(taxInvoiceRes.getGstinForInvoiceState());
				taxInvoiceOtherDetails.setDigitalSignForInvoiceState(taxInvoiceRes.getDigitalSignForInvoiceState());
				taxInvoiceOtherDetails.setAccountCodeOfService(taxInvoiceRes.getAccountCodeOfService());
				taxInvoiceOtherDetails.setDescriptionOfService(taxInvoiceRes.getDescriptionOfService());
				taxInvoiceOtherDetails.setPlaceOfSupply(taxInvoiceRes.getPlaceOfSupply());
				taxInvoiceOtherDetails.setPlaceOfSupplyState(taxInvoiceRes.getPlaceOfSupplyState());
				
				taxInvoiceWs.setTaxRate1Percentage(new BigDecimal(taxInvoiceRes.getTaxRate1Percentage()));
				taxInvoiceWs.setTaxRate2Percentage(new BigDecimal(taxInvoiceRes.getTaxRate2Percentage()));
				taxInvoiceWs.setTaxRate3Percentage(new BigDecimal(taxInvoiceRes.getTaxRate3Percentage()));

				taxInvoiceWs.setTotalValueOfService(AccelAeroCalculator.add(taxInvoiceRes.getTaxableAmount(),
						taxInvoiceRes.getNonTaxableAmount()));
				taxInvoiceWs.setTotalTaxAmount(AccelAeroCalculator.add(taxInvoiceRes.getTaxRate1Amount(),
						taxInvoiceRes.getTaxRate2Amount(), taxInvoiceRes.getTaxRate3Amount()));

				taxInvoiceWs.getTaxInvoiceAirlineDetails().add(taxInvoiceAirlineDetails);
				taxInvoiceWs.getTaxInvoiceRecipientDetails().add(taxInvoiceRecipientDetails);
				taxInvoiceWs.getTaxInvoiceOtherDetails().add(taxInvoiceOtherDetails);
				
				aaOTAAirTaxInvoiceGetRS.getTaxInvoice().add(taxInvoiceWs);
			}
			
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
					"No matching booking found");
		}

		return aaOTAAirTaxInvoiceGetRS;
	}

	/**
	 * 
	 * @param readRQ
	 * @param lccClientReservation
	 * @param options
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private void generateOTAAirBookRS(OTAAirBookRS airBookRS, OTAReadRQ readRQ, LCCClientReservation lccClientReservation,
			AALoadDataOptionsType options) throws ModuleException, WebservicesException {
		AirReservation airReservation = new AirReservation();
		airBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		SourceType pos = (SourceType) ThreadLocalData.getWSContextParam(WebservicesContext.REQUEST_POS);
		String BCT = pos.getBookingChannel().getType();

		// Setting the booking reference
		UniqueIDType uniqueIDType = new UniqueIDType();
		uniqueIDType.setID(lccClientReservation.getPNR());
		uniqueIDType.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		airReservation.getBookingReferenceID().add(uniqueIDType);

		BigDecimal balanceToPay = ReservationUtil.getTotalBalToPay(lccClientReservation);

		Agent agent = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_AGENT);
		
		boolean isPmtIntegrationChannel = BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
				|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))
				|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET));

		if (balanceToPay.doubleValue() > 0 && isPmtIntegrationChannel) {

			/**
			 * If there is a balance to pay, and the booking is loaded by one of the above channels to do the payment.
			 * we need to add a service charge to it. This charge is added as an adjustment to the payment before
			 * capturing it. This is required in the Modify booking flow/balance payment scenario. This needs to be done
			 * to support dry and interline when implementing modify booking flow. See
			 * ReservationQueryUtil.generateOTAAirBookRS method
			 */
		}

		// check if agency has enough credit left
		BigDecimal availableCredit = WebServicesModuleUtils.getTravelAgentFinanceBD().getAgentAvailableCredit(
				agent.getAgentCode());

		if (balanceToPay.doubleValue() > 0 && balanceToPay.compareTo(availableCredit) == 1) {
			log.warn("Agency available credit is insufficient " + "[agencyCode=" + agent.getAgentCode() + ",agentName="
					+ agent.getAgentDisplayName() + ",availableCredit=" + availableCredit + ",balanceToPay=" + balanceToPay + "]");
			if (isPmtIntegrationChannel) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INSUFFICIENT_AGENT_AVAILABLE_CREDIT_,
						"[Insufficient agency available credit. Please contact finance team.]");
			}
		}

		if (options != null) {

			if (options.getLoadAirItinery() != null && options.getLoadAirItinery()) {// Setting the Air Itinerary
				// Information
				boolean excludeCanceledSegments = false;
				if (isPmtIntegrationChannel) {
					excludeCanceledSegments = true;
				}
				airReservation.setAirItinerary(getAirItineraryType(lccClientReservation, excludeCanceledSegments));
			}

			if (options.getLoadTravelerInfo() != null && options.getLoadTravelerInfo() != null) {// Setting the Traveler
				// Information
				airReservation.setTravelerInfo(getTravelerInfoType(lccClientReservation));
			}

			String baseCurrencyCode = null;
			String preferredCurrencyCode = null;

			if ((options.getLoadFullFilment() != null && options.getLoadFullFilment())
					|| (options.getLoadPTCFullFilment() != null && options.getLoadPTCFullFilment())) {// Setting the
																										// Fullfilment
																										// Information
				if (lccClientReservation.getTotalPaidAmount().doubleValue() > 0) {// TODO test this for reservation
																					// having
					// REFUNDS
					// ----TODO set unified payment details for overall reservation; set Pax wise payment details
					Fulfillment fulfillment = getPaymentInfo(lccClientReservation);
					airReservation.setFulfillment(fulfillment);
					if (OTAUtils.isShowPriceInselectedCurrency()) {
						List<PaymentDetailType> paymentDetails = fulfillment.getPaymentDetails().getPaymentDetail();
						if (paymentDetails != null && paymentDetails.size() > 0) {
							for (PaymentDetailType paymentDetail : paymentDetails) {
								baseCurrencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
								preferredCurrencyCode = paymentDetail.getPaymentAmountInPayCur().getCurrencyCode();
							}
						}
					}
				}
			}

			if ((options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals())
					|| (options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo())) {// Setting the Pricing
																									// Information
				// TODO add service charge for FARY,ATM..etc in modify booking flow.for now this will be sent null
				if (OTAUtils.isShowPriceInselectedCurrency() && baseCurrencyCode != null && preferredCurrencyCode != null
						&& !baseCurrencyCode.equals(preferredCurrencyCode)) {
					airReservation.setPriceInfo(getBookingPriceInfoType(lccClientReservation, null, null,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo(), preferredCurrencyCode));
				} else {
					airReservation.setPriceInfo(getBookingPriceInfoType(lccClientReservation, null, null,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo()));
				}
			}
		}

		// set ticketing status
		airReservation.getTicketing().add(getTicketingInfo(lccClientReservation));

		// setting the extensions
		AAAirReservationExt airReservationExtensions = new AAAirReservationExt();
		if (options.getLoadAccSummary() != null && options.getLoadAccSummary()) {
			airReservationExtensions.setResAccountSummary(ExtensionsUtil.prepareWSAccountSummary(lccClientReservation,
					(options != null && options.getLoadPTCAccSummary())));
		}

		airReservationExtensions.setContactInfo(ExtensionsUtil.transformToWSContactInfo(lccClientReservation.getContactInfo()));
		if (lccClientReservation.getLastUserNote() != null && !"".equals(lccClientReservation.getLastUserNote())) {
			airReservationExtensions.setUserNotes(ExtensionsUtil.transformToWSUserNotes(lccClientReservation.getLastUserNote()));
		}

		/**
		 * TODO, Nafly Transactions required to be updated for ATM,CDM..etc when a balance payment is initialized.
		 * Balance query keys are added to the transaction in the modify booking flow for these bookings. Change this to
		 * go through Airproxy when doing modify booking flow.
		 * 
		 * @See ReservationQueryUtil.generateOTAAirBookRS method
		 */

		// set reservation summary information
		AAResSummaryType aaResSummary = new AAResSummaryType();
		aaResSummary.setPTCCounts(new AAPTCCountsType());

		AAPTCCountType adultsCounts = new AAPTCCountType();
		adultsCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		adultsCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxAdultCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(adultsCounts);

		AAPTCCountType childCounts = new AAPTCCountType();
		childCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		childCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxChildCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(childCounts);

		AAPTCCountType infantCount = new AAPTCCountType();
		infantCount.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		infantCount.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxInfantCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(infantCount);

		AAAdminInfoType aaAdminInfo = new AAAdminInfoType();
		aaAdminInfo.setOriginAgentCode(lccClientReservation.getAdminInfo().getOriginAgentCode());
		aaAdminInfo.setOriginSalesTerminal(lccClientReservation.getAdminInfo().getOriginSalesTerminal());
		airReservationExtensions.setAdminInfo(aaAdminInfo);

		airReservationExtensions.setResSummary(aaResSummary);

		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(TPAExtensionUtil.getInstance().marshall(airReservationExtensions));

		airBookRS.getSuccessAndWarningsAndAirReservation().add(new SuccessType());
		log.info("###WSLOGS### BOOKING REFERENCE NUMBER IS: " + lccClientReservation.getPNR());

	}

	public AirReservationType
			generateAirReservationType(LCCClientReservation lccClientReservation, AALoadDataOptionsType options)
					throws ModuleException, WebservicesException {
		AirReservationType airReservation = new AirReservationType();

		// Setting the booking reference
		UniqueIDType uniqueIDType = new UniqueIDType();
		uniqueIDType.setID(lccClientReservation.getPNR());
		uniqueIDType.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		airReservation.getBookingReferenceID().add(uniqueIDType);

		if (options != null) {

			if (options.getLoadAirItinery() != null && options.getLoadAirItinery()) {// Setting the Air Itinerary

				boolean excludeCanceledSegments = true;

				airReservation.setAirItinerary(getAirItineraryType(lccClientReservation, excludeCanceledSegments));
			}

			if (options.getLoadTravelerInfo() != null && options.getLoadTravelerInfo() != null) {// Setting the Traveler
				// Information
				airReservation.setTravelerInfo(getTravelerInfoType(lccClientReservation));
			}

			String baseCurrencyCode = null;
			String preferredCurrencyCode = null;

			if ((options.getLoadFullFilment() != null && options.getLoadFullFilment())
					|| (options.getLoadPTCFullFilment() != null && options.getLoadPTCFullFilment())) {// Setting the
																										// Fullfilment
																										// Information
				if (lccClientReservation.getTotalPaidAmount().doubleValue() > 0) {// TODO test this for reservation
																					// having
					// REFUNDS
					// ----TODO set unified payment details for overall reservation; set Pax wise payment details
					Fulfillment fulfillment = getPaymentInfo(lccClientReservation);
					airReservation.setFulfillment(fulfillment);
					if (OTAUtils.isShowPriceInselectedCurrency()) {
						List<PaymentDetailType> paymentDetails = fulfillment.getPaymentDetails().getPaymentDetail();
						if (paymentDetails != null && paymentDetails.size() > 0) {
							for (PaymentDetailType paymentDetail : paymentDetails) {
								baseCurrencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
								preferredCurrencyCode = paymentDetail.getPaymentAmountInPayCur().getCurrencyCode();
							}
						}
					}
				}
			}

			if ((options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals())
					|| (options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo())) {// Setting the Pricing
																									// Information
				// TODO add service charge for FARY,ATM..etc in modify booking flow.for now this will be sent null
				if (OTAUtils.isShowPriceInselectedCurrency() && baseCurrencyCode != null && preferredCurrencyCode != null
						&& !baseCurrencyCode.equals(preferredCurrencyCode)) {
					airReservation.setPriceInfo(getBookingPriceInfoType(lccClientReservation, null, null,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo(), preferredCurrencyCode));
				} else {
					airReservation.setPriceInfo(getBookingPriceInfoType(lccClientReservation, null, null,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo()));
				}
			}
		}

		// set ticketing status
		airReservation.getTicketing().add(getTicketingInfo(lccClientReservation));

		// setting the extensions
		AAAirReservationExt airReservationExtensions = new AAAirReservationExt();
		if (options.getLoadAccSummary() != null && options.getLoadAccSummary()) {
			airReservationExtensions.setResAccountSummary(ExtensionsUtil.prepareWSAccountSummary(lccClientReservation,
					(options != null && options.getLoadPTCAccSummary())));
		}

		airReservationExtensions.setContactInfo(ExtensionsUtil.transformToWSContactInfo(lccClientReservation.getContactInfo()));
		if (lccClientReservation.getLastUserNote() != null && !"".equals(lccClientReservation.getLastUserNote())) {
			airReservationExtensions.setUserNotes(ExtensionsUtil.transformToWSUserNotes(lccClientReservation.getLastUserNote()));
		}

		/**
		 * TODO, Nafly Transactions required to be updated for ATM,CDM..etc when a balance payment is initialized.
		 * Balance query keys are added to the transaction in the modify booking flow for these bookings. Change this to
		 * go through Airproxy when doing modify booking flow.
		 * 
		 * @See ReservationQueryUtil.generateOTAAirBookRS method
		 */

		// set reservation summary information
		AAResSummaryType aaResSummary = new AAResSummaryType();
		aaResSummary.setPTCCounts(new AAPTCCountsType());

		AAPTCCountType adultsCounts = new AAPTCCountType();
		adultsCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		adultsCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxAdultCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(adultsCounts);

		AAPTCCountType childCounts = new AAPTCCountType();
		childCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		childCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxChildCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(childCounts);

		AAPTCCountType infantCount = new AAPTCCountType();
		infantCount.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		infantCount.setPassengerTypeQuantity(new BigInteger(Integer.toString(lccClientReservation.getTotalPaxInfantCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(infantCount);

		AAAdminInfoType aaAdminInfo = new AAAdminInfoType();
		aaAdminInfo.setOriginAgentCode(lccClientReservation.getAdminInfo().getOriginAgentCode());
		aaAdminInfo.setOriginSalesTerminal(lccClientReservation.getAdminInfo().getOriginSalesTerminal());
		airReservationExtensions.setAdminInfo(aaAdminInfo);

		airReservationExtensions.setResSummary(aaResSummary);

		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(TPAExtensionUtil.getInstance().marshall(airReservationExtensions));

		return airReservation;

	}

	/**
	 * Prepares the reservation status
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	private static TicketingInfoType getTicketingInfo(LCCClientReservation lccClientReservation) throws WebservicesException,
			ModuleException {
		TicketingInfoType ticketingInfo = new TicketingInfoType();

		ticketingInfo.setTicketType(TicketType.E_TICKET);

		String otaTicketingStatus = null;
		String advisotryText = null;

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
			if (lccClientReservation.getTotalAvailableBalance().doubleValue() > 0) {
				otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_PARTIAL_TICKETED);
				advisotryText = "Reservation is partially confirmed. Has " + lccClientReservation.getTotalAvailableBalance()
						+ " balance amount to pay";
			} else {
				otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_TICKETED);
				advisotryText = "Reservation is fully paid and confirmed.";
			}
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(lccClientReservation.getStatus())) {
			otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_TICKET_TIME_LIMIT_TTL);
			ticketingInfo.setTicketTimeLimit(CommonUtil.parse(lccClientReservation.getZuluReleaseTimeStamp()));
			advisotryText = "Reservation is onhold. To avoid cancellation, pay before "
					+ CommonUtil.getFormattedDate(lccClientReservation.getZuluReleaseTimeStamp()) + " GMT";
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccClientReservation.getStatus())) {
			otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_CANCELLED);
			advisotryText = "Reservation is cancelled.";
		}

		ticketingInfo.setTicketingStatus(otaTicketingStatus);
		ticketingInfo.getTicketAdvisory().add(CommonUtil.getFreeTextType(advisotryText != null ? advisotryText : ""));

		return ticketingInfo;
	}

	/**
	 * Return Price Information
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 *             TODO QuotedChargeDTO is added to first pax if its a FAWRY,ATM...etc channel booking this is to be
	 *             done in the modify booking flow, hence this charge will not be shown even if the booking is loaded
	 *             through ATM..etc This two methods are overdidden, check if this can be merged
	 */
	private static BookingPriceInfoType getBookingPriceInfoType(LCCClientReservation lccClientReservation,
			QuotedChargeDTO serviceChargeDTO, Object[] firstPaxPnrPaxFareId, boolean setPricingInfoTotals,
			boolean setPTCPricingInfo) throws ModuleException, WebservicesException {

		BookingPriceInfoType bookingPriceInfoType = new BookingPriceInfoType();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (setPricingInfoTotals) {// set the total amounts
			FareType fareType = new FareType();
			bookingPriceInfoType.setItinTotalFare(fareType);
			// FIXME Fare Per Pax Type
			BigDecimal dblServiceCharge;
			// For Fawry apply the charge per pax
			if (serviceChargeDTO != null) {
				if (serviceChargeDTO.getChargeBasis() != null
						&& serviceChargeDTO.getChargeBasis().equalsIgnoreCase("CHARGEPERPAX")) {
					int paxCount = 0;
					for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {

						// Which means Parent or normal adult or child
						if (!ReservationApiUtils.isInfantType(reservationPax)) {
							paxCount = paxCount + 1;
						}
					}
					dblServiceCharge = AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE) * paxCount);
				} else
					dblServiceCharge = AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE));
			} else
				dblServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			// dblServiceCharge = (serviceChargeDTO!=null?
			// AccelAeroCalculator.parseBigDecimal(serviceChargeDTO.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)):
			// AccelAeroCalculator.getDefaultBigDecimalZero());

			// Setting the base fare
			BaseFare baseFare = new BaseFare();
			fareType.setBaseFare(baseFare);
			OTAUtils.setAmountAndDefaultCurrency(baseFare, lccClientReservation.getTotalTicketFare());

			// Setting the taxes
			Taxes taxes = new Taxes();
			fareType.setTaxes(taxes);
			AirTaxType airTaxType = new AirTaxType();
			taxes.getTax().add(airTaxType);
			airTaxType.setTaxCode("TOTALTAX");// FIXME - set individual taxes breakdonw for overall reservation
			OTAUtils.setAmountAndDefaultCurrency(airTaxType, lccClientReservation.getTotalTicketTaxCharge());

			// Setting the total fees (which excludes fare and tax)
			AirFeeType airFeeType = new AirFeeType();
			fareType.setFees(new Fees());
			fareType.getFees().getFee().add(airFeeType);
			airFeeType.setFeeCode("TOTALFEE");// FIXME - set individual fees breakdown for overall reservation
			
			BigDecimal totalFees = AccelAeroCalculator.add(lccClientReservation.getTotalTicketSurCharge(),
					lccClientReservation.getTotalTicketCancelCharge(), lccClientReservation.getTotalTicketModificationCharge(),
					lccClientReservation.getTotalTicketAdjustmentCharge(), dblServiceCharge);
			OTAUtils.setAmountAndDefaultCurrency(airFeeType, totalFees);

			// Setting the total charge amount
			TotalFare totalFare = new TotalFare();
			fareType.setTotalFare(totalFare);
			
			BigDecimal computedTotalFare = (AccelAeroCalculator.add(
					AccelAeroCalculator.add(lccClientReservation.getTotalChargeAmounts()), dblServiceCharge));
			OTAUtils.setAmountAndDefaultCurrency(totalFare, computedTotalFare);
			
			// Set total price in agent currency
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalFare.getAmount(),
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			}
			
			boolean qualifiedForCCPayment = false;
			BigDecimal balanceToPay = ReservationUtil.getTotalBalToPay(lccClientReservation);
			qualifiedForCCPayment = AccelAeroCalculator.isGreaterThan(balanceToPay, BigDecimal.ZERO)
					&& AccelAeroCalculator.isEqual(balanceToPay, computedTotalFare)
					&& lccClientReservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD);

			if (qualifiedForCCPayment) {
				int payablePaxCount = 0;
				Collection<LCCClientReservationPax> passengers = lccClientReservation.getPassengers();
				for (LCCClientReservationPax traveler : passengers) {
					if (lccClientReservation.isInfantPaymentSeparated() || !ReservationApiUtils.isInfantType(traveler)) {
						payablePaxCount++;
					}
				}

				int segmentCount = 0;
				for (LCCClientReservationSegment reservationSegment : lccClientReservation.getSegments()) {
					if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
						segmentCount++;
					}
				}

				ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(computedTotalFare, payablePaxCount, segmentCount,
						ChargeRateOperationType.MAKE_ONLY);
				computedTotalFare = AccelAeroCalculator.add(computedTotalFare, ccCharge.getAmount());

				UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRINCIPAL);

				String equiCurrencyCode = userPrincipal.getAgentCurrencyCode();

				if (equiCurrencyCode != null) {
					FareType.TotalEquivFareWithCCFee totalEquivFareWithCCFee = new FareType.TotalEquivFareWithCCFee();
					fareType.setTotalEquivFareWithCCFee(totalEquivFareWithCCFee);
					BigDecimal toalWithCCFeeInEquivCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(computedTotalFare,
							equiCurrencyCode, exchangeRateProxy);
					OTAUtils.setAmountAndCurrency(totalEquivFareWithCCFee, toalWithCCFeeInEquivCurrency, equiCurrencyCode);
				}
				FareType.TotalFareWithCCFee totalFareWithCCFee = new FareType.TotalFareWithCCFee();
				fareType.setTotalFareWithCCFee(totalFareWithCCFee);
				OTAUtils.setAmountAndDefaultCurrency(totalFareWithCCFee, computedTotalFare);
			}
		}

		if (setPTCPricingInfo) {
			PTCFareBreakdowns ptcBreakdowns = new PTCFareBreakdowns();
			bookingPriceInfoType.setPTCFareBreakdowns(ptcBreakdowns);
			PTCFareBreakdownType ptcBreakdown;
			FareType fare;
			TotalFare totalFare;
			AirFeeType fee;
			AirTaxType tax;
			PTCFareBreakdownType.TravelerRefNumber travelerRPH;

			for (LCCClientReservationPax pax : lccClientReservation.getPassengers()) {
				ptcBreakdown = new PTCFareBreakdownType();
				ptcBreakdowns.getPTCFareBreakdown().add(ptcBreakdown);
				ptcBreakdown.setPassengerTypeQuantity(new PassengerTypeQuantityType());

				// Identifying the passenger type - ADULT
				if (ReservationApiUtils.isAdultType(pax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(pax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - CHILD
				} else if (ReservationApiUtils.isChildType(pax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(pax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - INFANT
				} else if (ReservationApiUtils.isInfantType(pax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(pax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				ptcBreakdown.getPassengerTypeQuantity().setQuantity(Integer.valueOf(1));
				fare = new FareType();
				ptcBreakdown.setPassengerFare(fare);

				// -----BEGIN FIXME fare basis code
				PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
				fareBasisCodes.getFareBasisCode().add("P");
				ptcBreakdown.setFareBasisCodes(fareBasisCodes);
				// -----END

				// Setting the base fare
				fare.setBaseFare(new BaseFare());
				OTAUtils.setAmountAndDefaultCurrency(fare.getBaseFare(), pax.getTotalFare());

				// Setting the total ticket amount
				totalFare = new TotalFare();
				fare.setTotalFare(totalFare);
				OTAUtils.setAmountAndDefaultCurrency(totalFare,
						ReservationApiUtils.getTotalChargeAmount(pax.getTotalChargeAmounts()));

				fare.setTaxes(new Taxes());
				fare.setFees(new Fees());

				for (TaxTO taxTO : pax.getTaxes()) {

					tax = new AirTaxType();
					fare.getTaxes().getTax().add(tax);
					tax.setTaxName(taxTO.getTaxName());
					tax.setTaxCode(taxTO.getTaxCode());
					OTAUtils.setAmountAndDefaultCurrency(tax, taxTO.getAmount());
				}

				for (FeeTO feeTO : pax.getFees()) {// Fees
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(feeTO.getFeeName());
					fee.setFeeCode(feeTO.getFeeCode());
					OTAUtils.setAmountAndDefaultCurrency(fee, feeTO.getAmount());
				}

				for (SurchargeTO surchargeTO : pax.getSurcharges()) {// Surcharges
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(surchargeTO.getSurchargeName());
					fee.setFeeCode(surchargeTO.getSurchargeCode());
					OTAUtils.setAmountAndDefaultCurrency(fee, surchargeTO.getAmount());
				}

				for (FareTO fareTO : pax.getFares()) {
					fareBasisCodes.getFareBasisCode().add(fareTO.getFareBasisCode());
				}

				// Set the service charge to the first adult pax
				if (serviceChargeDTO != null && pax.getAttachedPaxId().equals(firstPaxPnrPaxFareId[0])) {
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(serviceChargeDTO.getChargeDescription());
					fee.setFeeCode(serviceChargeDTO.getChargeCode());
					// FIXME Fare Per Pax Type
					OTAUtils.setAmountAndDefaultCurrency(fee, AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)));
				}
			}
		}

		return bookingPriceInfoType;
	}

	/**
	 * Return Price Information
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 *             TODO QuotedChargeDTO is added to first pax if its a FAWRY,ATM...etc channel booking this is to be
	 *             done in the modify booking flow, hence this charge will not be shown even if the booking is loaded
	 *             through ATM..etc
	 */
	private static BookingPriceInfoType getBookingPriceInfoType(LCCClientReservation lccClientReservation,
			QuotedChargeDTO serviceChargeDTO, Object[] firstPaxPnrPaxFareId, boolean setPricingInfoTotals,
			boolean setPTCPricingInfo, String currencyCode) throws ModuleException, WebservicesException {
		BookingPriceInfoType bookingPriceInfoType = new BookingPriceInfoType();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(lccClientReservation.getZuluBookingTimestamp());
		PTCFareBreakdowns ptcBreakdowns = new PTCFareBreakdowns();
		BigDecimal totalFareInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalFeeInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalTaxInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (setPTCPricingInfo) {
			PTCFareBreakdownType ptcBreakdown;
			FareType fare;
			TotalFare totalFare;
			AirFeeType fee;
			AirTaxType tax;
			PTCFareBreakdownType.TravelerRefNumber travelerRPH;

			for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {

				ptcBreakdown = new PTCFareBreakdownType();
				ptcBreakdowns.getPTCFareBreakdown().add(ptcBreakdown);
				ptcBreakdown.setPassengerTypeQuantity(new PassengerTypeQuantityType());
				BigDecimal totalInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalFeeInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalTaxInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();

				// Identifying the passenger type - ADULT
				if (ReservationApiUtils.isAdultType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(reservationPax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - CHILD
				} else if (ReservationApiUtils.isChildType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(reservationPax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - INFANT
				} else if (ReservationApiUtils.isInfantType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(reservationPax.getTravelerRefNumber());
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				ptcBreakdown.getPassengerTypeQuantity().setQuantity(Integer.valueOf(1));
				fare = new FareType();
				ptcBreakdown.setPassengerFare(fare);

				// -----BEGIN FIXME fare basis code
				PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
				fareBasisCodes.getFareBasisCode().add("P");
				ptcBreakdown.setFareBasisCodes(fareBasisCodes);
				// -----END

				// Setting the base fare
				fare.setBaseFare(new BaseFare());
				BigDecimal totalBaseFareInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						reservationPax.getTotalFare(), currencyCode, exchangeRateProxy);
				totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, totalBaseFareInPreferredCurrency);
				OTAUtils.setAmountAndCurrency(fare.getBaseFare(), totalBaseFareInPreferredCurrency, currencyCode);

				fare.setTaxes(new Taxes());
				fare.setFees(new Fees());

				for (TaxTO taxTO : reservationPax.getTaxes()) {
					tax = new AirTaxType();
					fare.getTaxes().getTax().add(tax);
					tax.setTaxName(taxTO.getTaxName());
					tax.setTaxCode(taxTO.getChargeCode());

					BigDecimal taxInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							taxTO.getAmount(), currencyCode, exchangeRateProxy);
					totalTaxInSelectedCurPerPax = AccelAeroCalculator.add(totalTaxInSelectedCurPerPax, taxInPreferredCurrency);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, taxInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(tax, taxInPreferredCurrency, currencyCode);
				}

				for (FeeTO feeTO : reservationPax.getFees()) {
					// Fees
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(feeTO.getFeeName());
					fee.setFeeCode(feeTO.getChargeCode());

					BigDecimal feeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							feeTO.getAmount(), currencyCode, exchangeRateProxy);

					totalFeeInSelectedCurPerPax = AccelAeroCalculator.add(totalFeeInSelectedCurPerPax, feeInPreferredCurrency);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(fee, feeInPreferredCurrency, currencyCode);
				}

				for (SurchargeTO surchargeTO : reservationPax.getSurcharges()) {
					// Surcharges
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(surchargeTO.getSurchargeName());
					fee.setFeeCode(surchargeTO.getChargeCode());

					BigDecimal feeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							surchargeTO.getAmount(), currencyCode, exchangeRateProxy);

					totalFeeInSelectedCurPerPax = AccelAeroCalculator.add(totalFeeInSelectedCurPerPax, feeInPreferredCurrency);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(fee, feeInPreferredCurrency, currencyCode);
				}

				for (FareTO fareTO : reservationPax.getFares()) {
					fareBasisCodes.getFareBasisCode().add(fareTO.getFareBasisCode());
				}

				// Set the service charge to the first adult pax
				if (serviceChargeDTO != null && reservationPax.getAttachedPaxId().equals(firstPaxPnrPaxFareId[0])) {
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(serviceChargeDTO.getChargeDescription());
					fee.setFeeCode(serviceChargeDTO.getChargeCode());
					// FIXME Fare Per Pax Type
					BigDecimal feeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
									.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)), currencyCode,
							exchangeRateProxy);
					totalFeeInSelectedCurPerPax = AccelAeroCalculator.add(totalFeeInSelectedCurPerPax, feeInPreferredCurrency);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(fee, feeInPreferredCurrency, currencyCode);
				}

				// Setting the total ticket amount
				totalFare = new TotalFare();
				fare.setTotalFare(totalFare);
				BigDecimal totalFareInPreferredCurrencyPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (totalInSelectedCurPerPax != null) {
					totalFareInPreferredCurrencyPerPax = totalInSelectedCurPerPax;
				} else {
					WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts()), currencyCode,
							exchangeRateProxy);
				}
				OTAUtils.setAmountAndCurrency(totalFare, totalFareInPreferredCurrencyPerPax, currencyCode);

				totalFareInPreferredCurrency = AccelAeroCalculator.add(totalFareInPreferredCurrency,
						totalFareInPreferredCurrencyPerPax);
				totalFeeInPreferredCurrency = AccelAeroCalculator.add(totalFeeInPreferredCurrency, totalFeeInSelectedCurPerPax);
				totalTaxInPreferredCurrency = AccelAeroCalculator.add(totalTaxInPreferredCurrency, totalTaxInSelectedCurPerPax);
			}
		}

		if (setPricingInfoTotals) {// set the total amounts
			FareType fareType = new FareType();
			bookingPriceInfoType.setItinTotalFare(fareType);
			// FIXME Fare Per Pax Type
			BigDecimal dblServiceCharge = (serviceChargeDTO != null ? AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
					.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)) : AccelAeroCalculator
					.getDefaultBigDecimalZero());

			// Setting the base fare
			BaseFare baseFare = new BaseFare();
			fareType.setBaseFare(baseFare);
			BigDecimal totalTicketFareInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
					lccClientReservation.getTotalTicketFare(), currencyCode, exchangeRateProxy);
			OTAUtils.setAmountAndCurrency(baseFare, totalTicketFareInPreferredCurrency, currencyCode);

			// Setting the taxes
			Taxes taxes = new Taxes();
			fareType.setTaxes(taxes);
			AirTaxType airTaxType = new AirTaxType();
			taxes.getTax().add(airTaxType);
			airTaxType.setTaxCode("TOTALTAX");// FIXME - set individual taxes breakdonw for overall reservation
			BigDecimal totalTicketTaxChargeInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalTaxInPreferredCurrency != null
					&& !totalTaxInPreferredCurrency.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
				totalTicketTaxChargeInPreferredCurrency = totalTaxInPreferredCurrency;
			} else {
				totalTicketTaxChargeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						lccClientReservation.getTotalTicketTaxCharge(), currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(airTaxType, totalTicketTaxChargeInPreferredCurrency, currencyCode);

			// Setting the total fees (which excludes fare and tax)
			AirFeeType airFeeType = new AirFeeType();
			fareType.setFees(new Fees());
			fareType.getFees().getFee().add(airFeeType);
			airFeeType.setFeeCode("TOTALFEE");// FIXME - set individual fees breakdown for overall reservation
			BigDecimal totalFees = AccelAeroCalculator.add(lccClientReservation.getTotalTicketSurCharge(),
					lccClientReservation.getTotalTicketCancelCharge(), lccClientReservation.getTotalTicketModificationCharge(),
					lccClientReservation.getTotalTicketAdjustmentCharge(), dblServiceCharge);
			BigDecimal totalFeesInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalFeeInPreferredCurrency != null
					&& !totalFeeInPreferredCurrency.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
				totalFeesInPreferredCurrency = totalFeeInPreferredCurrency;
			} else {
				totalFeesInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(totalFees,
						currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(airFeeType, totalFeesInPreferredCurrency, currencyCode);

			// Setting the total charge amount
			TotalFare totalFare = new TotalFare();
			fareType.setTotalFare(totalFare);
			BigDecimal totalFareAmountInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalFareInPreferredCurrency != null
					&& !totalFareInPreferredCurrency.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
				totalFareAmountInPreferredCurrency = totalFareInPreferredCurrency;
			} else {
				totalFareAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						AccelAeroCalculator.add(AccelAeroCalculator.add(lccClientReservation.getTotalChargeAmounts()),
								dblServiceCharge), currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(totalFare, totalFareAmountInPreferredCurrency, currencyCode);

			// Set total price in agent currency
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
					AccelAeroCalculator.add(AccelAeroCalculator.add(lccClientReservation.getTotalChargeAmounts()),
							dblServiceCharge), principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null && !currencyCode.equals(principal.getAgentCurrencyCode())) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			} else if (currencyCode.equals(principal.getAgentCurrencyCode())) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndDefaultCurrency(totalEquivFare, (AccelAeroCalculator.add(
						AccelAeroCalculator.add(lccClientReservation.getTotalChargeAmounts()), dblServiceCharge)));
			}

		}
		if (setPTCPricingInfo) {
			bookingPriceInfoType.setPTCFareBreakdowns(ptcBreakdowns);
		}
		return bookingPriceInfoType;
	}

	/**
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static Fulfillment getPaymentInfo(LCCClientReservation lccClientReservation) throws ModuleException {
		Fulfillment fulfillment = new Fulfillment();
		PaymentDetails paymentDetails = new PaymentDetails();
		fulfillment.setPaymentDetails(paymentDetails);

		PaymentDetailType paymentDetailType = null;
		PaymentAmount paymentAmount = null;
		PaymentAmountInPayCur paymentAmountInPayCur = null;
		Cash cash;
		CompanyName companyName;
		DirectBillType directBillType;
		LoyaltyRedemption loyaltyRedemption;
		PaymentCardType paymentCardType;

		for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {
			LCCClientPaymentHolder lccClientPaymentHolder = reservationPax.getLccClientPaymentHolder();

			for (LCCClientPaymentInfo lccClientPaymentInfo : lccClientPaymentHolder.getPayments()) {

				paymentDetailType = new PaymentDetailType();
				paymentDetails.getPaymentDetail().add(paymentDetailType);

				paymentAmount = new PaymentAmount();
				paymentDetailType.setPaymentAmount(paymentAmount);

				paymentAmount.setAmount(lccClientPaymentInfo.getTotalAmount());
				paymentAmount.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
				paymentAmount.setDecimalPlaces(new BigInteger(String.valueOf(AppSysParamsUtil.getNumberOfDecimalPoints())));

				if (lccClientPaymentInfo.getPayCurrencyDTO() != null
						&& lccClientPaymentInfo.getPayCurrencyDTO().getPayCurrencyCode() != null) {

					paymentAmountInPayCur = new PaymentAmountInPayCur();
					paymentDetailType.setPaymentAmountInPayCur(paymentAmountInPayCur);

					paymentAmountInPayCur.setAmount(lccClientPaymentInfo.getPayCurrencyAmount());
					paymentAmountInPayCur.setCurrencyCode(lccClientPaymentInfo.getPayCurrencyDTO().getPayCurrencyCode());
					paymentAmountInPayCur.setDecimalPlaces(new BigInteger(String.valueOf(AppSysParamsUtil
							.getNumberOfDecimalPoints())));
				}

				if (lccClientPaymentInfo instanceof LCCClientCashPaymentInfo) {
					cash = new Cash();
					cash.setCashIndicator(new Boolean(true));
					paymentDetailType.setCash(cash);

				} else if (lccClientPaymentInfo instanceof LCCClientOnAccountPaymentInfo) {

					directBillType = new DirectBillType();
					companyName = new CompanyName();
					companyName.setCode(((LCCClientOnAccountPaymentInfo) lccClientPaymentInfo).getAgentCode());
					companyName.setContactName(((LCCClientOnAccountPaymentInfo) lccClientPaymentInfo).getAgentName());
					companyName.setValue(companyName.getContactName());

					directBillType.setCompanyName(companyName);
					paymentDetailType.setDirectBill(directBillType);

				} else if (lccClientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo) {

					loyaltyRedemption = new LoyaltyRedemption();
					loyaltyRedemption.setProgramName(((LCCClientPaxCreditPaymentInfo) lccClientPaymentInfo).getDescription());
					paymentDetailType.setLoyaltyRedemption(loyaltyRedemption);

				} else if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {

					paymentCardType = new PaymentCardType();
					paymentDetailType.setPaymentCard(paymentCardType);
					paymentCardType.setCardType(PaymentType
							.getPaymentTypeDesc(((CommonCreditCardPaymentInfo) lccClientPaymentInfo).getType()));
					paymentCardType.setCardHolderName(((CommonCreditCardPaymentInfo) lccClientPaymentInfo).getName());
					paymentCardType.setCardNumber("xxxxxxxxxxxx"
							+ ((CommonCreditCardPaymentInfo) lccClientPaymentInfo).getNoLastDigits());
				}
			}

		}

		return fulfillment;
	}

	/**
	 * Return the travellers information
	 * 
	 * @param lccClientReservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static TravelerInfoType getTravelerInfoType(LCCClientReservation lccClientReservation) throws ModuleException,
			WebservicesException {
		TravelerInfoType travelerInfoType = new TravelerInfoType();

		AirTravelerType airTravelerType;
		PersonNameType personNameType;
		Telephone telephone;
		SpecialReqDetailsType specialReqDetailsType;
		SpecialServiceRequests ssrs = new SpecialServiceRequests();
		SpecialServiceRequest ssr;
		String paxTitle = null;
		Document doc;
		List<SeatRequest> seatRequests = new ArrayList<SeatRequest>();
		List<MealRequest> mealRequests = new ArrayList<MealRequest>();
		List<SSRRequest> ssrRequests = new ArrayList<SSRRequest>();
		List<BaggageRequest> baggageRequests = new ArrayList<BaggageRequest>();

		for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {

			airTravelerType = new AirTravelerType();
			airTravelerType.setTravelerRefNumber(new TravelerRefNumber());
			paxTitle = null;
			doc = new Document();

			// Adult
			if (ReservationInternalConstants.PassengerType.ADULT.equals(lccClientReservationPax.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
				airTravelerType.getTravelerRefNumber().setRPH(lccClientReservationPax.getTravelerRefNumber());

				paxTitle = CommonUtil.getWSPaxTitle(lccClientReservationPax.getTitle());

				// Child
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(lccClientReservationPax.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
				airTravelerType.getTravelerRefNumber().setRPH(lccClientReservationPax.getTravelerRefNumber());

				paxTitle = CommonUtil.getWSPaxTitle(lccClientReservationPax.getTitle());

				// Infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(lccClientReservationPax.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
				airTravelerType.getTravelerRefNumber().setRPH(lccClientReservationPax.getTravelerRefNumber());
				paxTitle = CommonUtil.getWSPaxTitle(lccClientReservationPax.getTitle());

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			personNameType = new PersonNameType();
			if (paxTitle != null) {
				personNameType.getNameTitle().add(paxTitle);
			}
			personNameType.setSurname(lccClientReservationPax.getLastName());
			personNameType.getGivenName().add(lccClientReservationPax.getFirstName());
			airTravelerType.setPersonName(personNameType);

			telephone = new Telephone();
			telephone.setPhoneNumber(lccClientReservation.getContactInfo().getPhoneNo());
			airTravelerType.getTelephone().add(telephone);

			LCCClientReservationAdditionalPax addInfo = lccClientReservationPax.getLccClientAdditionPax();

			if (lccClientReservationPax.getNationalityCode() != null) {
				Nationality nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
						lccClientReservationPax.getNationalityCode());
				if ((nationality != null) && (nationality.getIsoCode() != null)) {
					doc.setDocHolderNationality(nationality.getIsoCode());
					airTravelerType.getDocument().add(doc);
					if (addInfo != null) {
						if (addInfo.getPassportNo() != null) {
							doc.setDocID(addInfo.getPassportNo());
							doc.setDocType(CommonsConstants.SSRCode.PSPT.toString());
							if (addInfo.getPassportIssuedCntry() != null) {
								doc.setDocIssueCountry(addInfo.getPassportIssuedCntry());
							}
							if (addInfo.getPassportExpiry() != null) {
								doc.setExpireDate(CommonUtil.parse(addInfo.getPassportExpiry()));
							}
						}
					}
				}
			}

			if (addInfo != null) {
				if (addInfo.getEmployeeId() != null) {
					doc = new Document();
					doc.setDocID(addInfo.getEmployeeId());
					doc.setDocType(CommonsConstants.SSRCode.ID.toString());
					if (addInfo.getDateOfJoin() != null) {
						doc.setEffectiveDate(CommonUtil.parse(addInfo.getDateOfJoin()));
					}
					if (addInfo.getIdCategory() != null) {
						doc.setDocIssueAuthority(addInfo.getIdCategory());
					}
					airTravelerType.getDocument().add(doc);
				}

				if (addInfo.getFfid() != null && !"".equals(addInfo.getFfid())) {
					AirTravelerType.CustLoyalty custLoyalty = new AirTravelerType.CustLoyalty();
					custLoyalty.setMembershipID(addInfo.getFfid());
					airTravelerType.getCustLoyalty().add(custLoyalty);
				}
			}

			travelerInfoType.getAirTraveler().add(airTravelerType);

			/*
			 * Add eticket details.
			 */
			Collection<LccClientPassengerEticketInfoTO> eTicketList = lccClientReservationPax.geteTickets();
			airTravelerType.setETicketInfo(new ETicketInfo());
			if (eTicketList != null) {
				for (LccClientPassengerEticketInfoTO eTicket : eTicketList) {
					EticketInfoType eTicketInfo = new EticketInfoType();
					eTicketInfo.setCouponNo(eTicket.getCouponNo());
					eTicketInfo.setETicketNo(eTicket.getPaxETicketNo());
					eTicketInfo.setFlightSegmentRPH(String.valueOf(eTicket.getPnrSegId()));
					eTicketInfo.setFlightSegmentCode(eTicket.getSegmentCode());
					eTicketInfo.setStatus(ETicketStatusType.fromValue(eTicket.getPaxETicketStatus()));

					// If "O" then unused. Else used...
					eTicketInfo.setUsedStatus(EticketStatus.OPEN.code().equalsIgnoreCase(eTicket.getPaxETicketStatus())
							? ETicketUsedStatusType.UNUSED
							: ETicketUsedStatusType.USED);

					airTravelerType.getETicketInfo().getETicketInfomation().add(eTicketInfo);
				}
			}

			List<String[]> ssrDetails = ReservationSSRUtil.getPaxSSR(lccClientReservationPax);
			List<SpecialServiceRequest> specialServiceRequests = composeSpecialServiceRequests(ssrDetails,
					airTravelerType.getTravelerRefNumber().getRPH());
			if (specialServiceRequests != null && !specialServiceRequests.isEmpty()) {
				ssrs.getSpecialServiceRequest().addAll(specialServiceRequests);
			}

			seatRequests.addAll(getSeatRequests(lccClientReservationPax, airTravelerType.getTravelerRefNumber().getRPH()));
			mealRequests.addAll(getMealRequests(lccClientReservationPax, airTravelerType.getTravelerRefNumber().getRPH()));
			ssrRequests.addAll(getPaxAirportServices(lccClientReservationPax, airTravelerType.getTravelerRefNumber().getRPH()));
			baggageRequests.addAll(getBaggageRequests(lccClientReservationPax, airTravelerType.getTravelerRefNumber().getRPH()));
		}

		if ((ssrs.getSpecialServiceRequest() != null) && (!ssrs.getSpecialServiceRequest().isEmpty())
				|| (!seatRequests.isEmpty()) || !mealRequests.isEmpty() || !ssrRequests.isEmpty() || !baggageRequests.isEmpty()
				|| (lccClientReservation.getReservationInsurances() != null)
				&& !lccClientReservation.getReservationInsurances().isEmpty()) {

			specialReqDetailsType = new SpecialReqDetailsType();
			if ((ssrs.getSpecialServiceRequest() != null) && (ssrs.getSpecialServiceRequest().size() > 0)) {
				specialReqDetailsType.setSpecialServiceRequests(ssrs);
			}
			if (seatRequests.size() > 0) {
				SeatRequests otaSeatRequests = new SeatRequests();
				for (SeatRequest seatRequest : seatRequests) {
					otaSeatRequests.getSeatRequest().add(seatRequest);
				}
				specialReqDetailsType.setSeatRequests(otaSeatRequests);
			}
			if (mealRequests.size() > 0) {
				MealRequests otaMealRequests = new MealRequests();
				for (MealRequest mealRequest : mealRequests) {
					otaMealRequests.getMealRequest().add(mealRequest);
				}
				specialReqDetailsType.setMealRequests(otaMealRequests);
			}
			if (ssrRequests.size() > 0) {
				SSRRequests otaSsrRequests = new SSRRequests();
				for (SSRRequest ssrRequest : ssrRequests) {
					otaSsrRequests.getSSRRequest().add(ssrRequest);
				}
				specialReqDetailsType.setSSRRequests(otaSsrRequests);
			}
			if (baggageRequests.size() > 0) {
				BaggageRequests otaBaggageRequests = new BaggageRequests();
				for (BaggageRequest baggagRequest : baggageRequests) {
					otaBaggageRequests.getBaggageRequest().add(baggagRequest);
				}
				specialReqDetailsType.setBaggageRequests(otaBaggageRequests);
			}

			if (lccClientReservation.getReservationInsurances() != null
					&& !lccClientReservation.getReservationInsurances().isEmpty()) {
				LCCClientReservationInsurance resInsurance = lccClientReservation.getReservationInsurances().get(0);
				InsuranceRequests insuranceRequests = new InsuranceRequests();
				InsuranceRequest insuranceRequest = new InsuranceRequest();
				insuranceRequest.setDepartureDate(CommonUtil.parse(resInsurance.getDateOfTravel()));
				insuranceRequest.setArrivalDate(CommonUtil.parse(resInsurance.getDateOfReturn()));
				insuranceRequest.setDestination(resInsurance.getDestination());
				insuranceRequest.setOrigin(resInsurance.getOrigin());
				insuranceRequest.setRPH(resInsurance.getInsuranceQuoteRefNumber());

				if (resInsurance != null) {
					if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.SC.toString())) {
						insuranceRequest.setState("Success");
						insuranceRequest.setPolicyCode(resInsurance.getPolicyCode());
					} else if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.OH.toString())) {
						insuranceRequest.setState("On Hold");
						insuranceRequest.setPolicyCode(resInsurance.getPolicyCode());
					} else if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.FL.toString())) {
						insuranceRequest.setState("Failed");
					} else {
						insuranceRequest.setState("Processing");
					}
				}
				insuranceRequests.setInsuranceRequest(insuranceRequest);
				specialReqDetailsType.setInsuranceRequests(insuranceRequests);
			}

			travelerInfoType.getSpecialReqDetails().add(specialReqDetailsType);
		}

		return travelerInfoType;
	}

	/**
	 * 
	 * @param pax
	 * @param travelereRPH
	 * @return
	 * @throws ModuleException
	 */
	private static List<BaggageRequest> getBaggageRequests(LCCClientReservationPax pax, String travelereRPH)
			throws ModuleException {
		List<BaggageRequest> baggageRequests = new ArrayList<BaggageRequest>();

		for (LCCSelectedSegmentAncillaryDTO aniclary : pax.getSelectedAncillaries()) {

			FlightSegmentTO flightSegmentTO = aniclary.getFlightSegmentTO();

			for (LCCBaggageDTO baggageDTO : aniclary.getBaggageDTOs()) {

				BaggageRequest baggageRequest = new BaggageRequest();
				baggageRequest.setBaggageCode(baggageDTO.getBaggageName());
				baggageRequest.getTravelerRefNumberRPHList().add(travelereRPH);
				baggageRequest.setDepartureDate(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
				baggageRequest.setFlightNumber(flightSegmentTO.getFlightNumber());
				baggageRequest.getFlightRefNumberRPHList().add(flightSegmentTO.getFlightRefNumber());

				baggageRequests.add(baggageRequest);
			}

		}

		return baggageRequests;
	}

	/**
	 * 
	 * @param pax
	 * @param travelereRPH
	 * @return
	 * @throws ModuleException
	 */
	private static List<SSRRequest> getPaxAirportServices(LCCClientReservationPax pax, String travelereRPH)
			throws ModuleException {
		List<SSRRequest> ssrRequests = new ArrayList<SSRRequest>();

		for (LCCSelectedSegmentAncillaryDTO aniclary : pax.getSelectedAncillaries()) {
			FlightSegmentTO flightSegmentTO = aniclary.getFlightSegmentTO();

			for (LCCAirportServiceDTO airportServiceDTO : aniclary.getAirportServiceDTOs()) {
				// This will add airport services only
				if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(airportServiceDTO.getStatus())
						&& !BeanUtils.nullHandler(airportServiceDTO.getAirportCode()).equals("")) {

					SSRRequest ssrRequest = new SSRRequest();
					ssrRequest.setSsrCode(airportServiceDTO.getSsrCode());
					ssrRequest.setAirportCode(airportServiceDTO.getAirportCode());
					ssrRequest.setAirportType(airportServiceDTO.getApplyOn());
					ssrRequest.getTravelerRefNumberRPHList().add(travelereRPH);
					ssrRequest.setDepartureDate(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
					ssrRequest.setFlightNumber(flightSegmentTO.getFlightNumber());
					ssrRequest.getFlightRefNumberRPHList().add(flightSegmentTO.getFlightRefNumber());
					ssrRequest.setServiceType(SSRType.AIRPORT);

					ssrRequests.add(ssrRequest);
				}
			}
		}
		return ssrRequests;
	}

	/**
	 * 
	 * @param pax
	 * @param travelereRPH
	 * @return
	 * @throws ModuleException
	 */
	private static List<SeatRequest> getSeatRequests(LCCClientReservationPax pax, String travelereRPH) throws ModuleException {
		List<SeatRequest> seatRequests = new ArrayList<SeatRequest>();

		for (LCCSelectedSegmentAncillaryDTO ancilary : pax.getSelectedAncillaries()) {
			FlightSegmentTO flightSegmentTO = ancilary.getFlightSegmentTO();

			LCCAirSeatDTO seatDTO = ancilary.getAirSeatDTO();

			SeatRequest seatRequest = new SeatRequest();
			if (seatDTO != null) {
				seatRequest.setSeatNumber(seatDTO.getSeatNumber());
			}
			seatRequest.getTravelerRefNumberRPHList().add(travelereRPH);
			seatRequest.setDepartureDate(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
			seatRequest.setFlightNumber(flightSegmentTO.getFlightNumber());
			seatRequest.getFlightRefNumberRPHList().add(flightSegmentTO.getFlightRefNumber());

			seatRequests.add(seatRequest);

		}

		return seatRequests;
	}

	/**
	 * 
	 * @param pax
	 * @param travelereRPH
	 * @return
	 * @throws ModuleException
	 */
	private static List<MealRequest> getMealRequests(LCCClientReservationPax pax, String travelereRPH) throws ModuleException {

		List<MealRequest> mealRequests = new ArrayList<MealRequest>();

		for (LCCSelectedSegmentAncillaryDTO ancilary : pax.getSelectedAncillaries()) {
			FlightSegmentTO flightSegmentTO = ancilary.getFlightSegmentTO();

			for (LCCMealDTO meal : ancilary.getMealDTOs()) {

				MealRequest mealRequest = new MealRequest();

				mealRequest.setMealCode(meal.getMealCode());
				mealRequest.setMealQuantity(meal.getAllocatedMeals());
				mealRequest.getTravelerRefNumberRPHList().add(travelereRPH);
				mealRequest.setDepartureDate(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
				mealRequest.setFlightNumber(flightSegmentTO.getFlightNumber());
				mealRequest.getFlightRefNumberRPHList().add(flightSegmentTO.getFlightRefNumber());

				mealRequests.add(mealRequest);

			}

		}

		return mealRequests;
	}

	/**
	 * Return the Air Itinerary Information
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	private static AirItineraryType getAirItineraryType(LCCClientReservation lccClientReservation,
			boolean excludeCancelledSegments) throws WebservicesException, ModuleException {
		AirItineraryType airItineraryType = new AirItineraryType();
		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOptionType originDestinationOptionType;
		BookFlightSegmentType bookFlightSegmentType;
		ArrivalAirport arrivalAirport;
		DepartureAirport departureAirport;

		Map<String, OriginDestinationOptionType> tmpOndOptionMap = new HashMap<String, OriginDestinationOptionType>();

		for (LCCClientReservationSegment lccReservationSegment : lccClientReservation.getSegments()) {

			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(lccReservationSegment.getStatus())
					&& excludeCancelledSegments) {
				// Exclude canceled segments
				continue;
			}

			if (lccReservationSegment.getSubStatus() != null
					&& !("").equals(lccReservationSegment.getSubStatus())
					&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(lccReservationSegment
							.getSubStatus())) {
				continue;
			}

			departureAirport = new DepartureAirport();
			arrivalAirport = new ArrivalAirport();

			departureAirport.setLocationCode(getDepartureOrArrivalSegment(lccReservationSegment.getSegmentCode(), true));
			departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);
			// departureAirport.setCodeContext((String) mapAirportCodeIsGcc.get(getDepartureOrArrivalSegment(
			// reservationSegmentDTO.getSegmentCode(), true))); // Is this required ?

			arrivalAirport.setLocationCode(getDepartureOrArrivalSegment(lccReservationSegment.getSegmentCode(), false));
			arrivalAirport.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);
			// arrivalAirport.setCodeContext((String) mapAirportCodeIsGcc.get(getDepartureOrArrivalSegment(
			// reservationSegmentDTO.getSegmentCode(), false)));// Is this required ?

			bookFlightSegmentType = new BookFlightSegmentType();
			bookFlightSegmentType.setDepartureAirport(departureAirport);
			bookFlightSegmentType.setArrivalAirport(arrivalAirport);

			// Short names for airports are referred by ATM and CDM
			FreeTextType airportAbbreviatedNames = new FreeTextType();
			airportAbbreviatedNames.setValue("airport_short_names:" + departureAirport.getLocationCode() + "="
					+ globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[1] + ","
					+ arrivalAirport.getLocationCode() + "="
					+ globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[1]);
			bookFlightSegmentType.getComment().add(airportAbbreviatedNames);

			bookFlightSegmentType.setFlightNumber(lccReservationSegment.getFlightNo());
			bookFlightSegmentType.setStatus(OTAUtils.getWSResSegStatus(lccReservationSegment.getStatus()));

			bookFlightSegmentType.setDepartureDateTime(CommonUtil.parse(lccReservationSegment.getDepartureDate()));
			bookFlightSegmentType.setArrivalDateTime(CommonUtil.parse(lccReservationSegment.getArrivalDate()));
			bookFlightSegmentType.setRPH(FlightRefNumberUtil.composePnrSegRPHFromFlightRPH(
					lccReservationSegment.getFlightSegmentRefNumber(), lccReservationSegment.getPnrSegID().toString()));
			bookFlightSegmentType.setResCabinClass((lccReservationSegment.getCabinClassCode() != null) ? lccReservationSegment
					.getCabinClassCode() : "");

			WSFlexiFareUtil.setSegmentFlexiFareAvailabilities(bookFlightSegmentType, lccReservationSegment, lccClientReservation);

			String[] segmentCodeList = lccReservationSegment.getSegmentCode().split("/");

			if (segmentCodeList.length > 2) {
				bookFlightSegmentType.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}

			String fareGroupId = lccReservationSegment.getInterlineGroupKey();

			if (tmpOndOptionMap.containsKey(fareGroupId)) {
				originDestinationOptionType = tmpOndOptionMap.get(fareGroupId);
			} else {
				originDestinationOptionType = new OriginDestinationOptionType();
				originDestinationOptions.getOriginDestinationOption().add(originDestinationOptionType);
				tmpOndOptionMap.put(fareGroupId, originDestinationOptionType);
			}
			originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
		}

		airItineraryType.setOriginDestinationOptions(originDestinationOptions);
		tmpOndOptionMap.clear();
		return airItineraryType;
	}

	/**
	 * Returns the departure segment or arrival segment
	 * 
	 * @param segmentCode
	 * @param isDepartureSegment
	 * @return
	 */
	private static String getDepartureOrArrivalSegment(String segmentCode, boolean isDepartureSegment) {
		if (isDepartureSegment) {
			return segmentCode.substring(0, 3);
		} else {
			return segmentCode.substring(segmentCode.length() - 3);
		}
	}

	/**
	 * Returns Meta OTAAirBookRS
	 * 
	 * @param readRQ
	 * @return
	 */
	private static OTAAirBookRS getMetaOTAAirBookRS(OTAReadRQ readRQ) {
		OTAAirBookRS oTAAirBookRS = new OTAAirBookRS();

		oTAAirBookRS.setTransactionIdentifier(readRQ.getTransactionIdentifier());
		oTAAirBookRS.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		oTAAirBookRS.setSequenceNmbr(readRQ.getSequenceNmbr());
		oTAAirBookRS.setEchoToken(readRQ.getEchoToken());
		oTAAirBookRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		return oTAAirBookRS;
	}

	private void authorizeReservationAccess(LCCClientReservation lccClientReservation, boolean isInternalCall)
			throws WebservicesException, ModuleException {

		Collection<String> privilegesKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		// TODO: authorization processes for internal method invocations should be refactored
		// Authorizing booking category
		if (!isInternalCall) {

			if (!userPrincipal.getAgentCode().equals(lccClientReservation.getAdminInfo().getOwnerAgentCode())) {
				if (!AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ANY_PNR_SEARCH)
						&& !AuthorizationUtil
						.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALL_REPORTING_AGENT_PNR_SEARCH)
						&& !AuthorizationUtil
						.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.REPORTING_AGENT_PNR_SEARCH)) {

					WSReservationUtil.authorizeBookingCategory(privilegesKeys, lccClientReservation.getBookingCategory(),
							WSConstants.ALLOW_FIND_RESERVATION);

					// authorizing based on the owner channel
					if (!AuthorizationUtil.hasPrivilege(privilegesKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANY_CHANNEL)) {
						int ownerChannel = new Integer(lccClientReservation.getAdminInfo().getOriginChannelId());

						switch (ownerChannel) {
						case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_DNATA_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_HOLIDAY_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_AGENT:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_XBE_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_WEB:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_IBE_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_IOS:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_IOS_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANDROID_CHANNEL);
							break;
						}
						
					}
				}

			}

		}

	}

	/**
	 * Validates retrieved reservation's eligibility.
	 * 
	 * @param oTAReadRQ
	 * @param lccClientReservation
	 * @throws WebservicesException
	 */
	private void validateReservation(OTAReadRQ oTAReadRQ, LCCClientReservation lccClientReservation) throws WebservicesException {

		Boolean triggerValidations = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS);

		if (triggerValidations != null && triggerValidations.booleanValue()) {

			SourceType pos = oTAReadRQ.getPOS().getSource().get(0);
			String BCT = pos.getBookingChannel().getType();

			// ATM/CDM specific checkings
			if ((BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM)) || BCT
						.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET)))) {

				// Do not load cancelled reservations for ATM/CDM
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccClientReservation.getStatus())) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_,
							"RESERVATION IS ALREADY CANCELLED");
				}

				// Ignore Departure Date Validation for Fawry Webservices
				Boolean triggerIgnorDeptDateValidation = (Boolean) ThreadLocalData.getWSContextParam(
						WebservicesContext.TRIGGER_IGNORE_DEPARTURE_DATE_VALIDATEION);
				if (triggerIgnorDeptDateValidation != null && triggerIgnorDeptDateValidation.booleanValue()) {
					return;
				}

				// Check if first or next segment departure date matches
				if (oTAReadRQ.getReadRequests() != null && !oTAReadRQ.getReadRequests().getAirReadRequest().isEmpty()) {
					AirReadRequest airReadRequest = BeanUtils.getFirstElement(oTAReadRQ.getReadRequests().getAirReadRequest());
					Calendar specifiedFirstDepDate = airReadRequest.getDepartureDate().toGregorianCalendar();

					Calendar[] firstAndNextDep = getFirstAndNextSegDepDateLocal(lccClientReservation.getSegments());

					boolean isMatching = false;

					// Check if specified date matches first flight departure
					if (firstAndNextDep[0] != null) {
						if (firstAndNextDep[0].get(Calendar.MONTH) == specifiedFirstDepDate.get(Calendar.MONTH)
								&& firstAndNextDep[0].get(Calendar.DAY_OF_MONTH) == specifiedFirstDepDate
										.get(Calendar.DAY_OF_MONTH)) {
							isMatching = true;
						}
					}

					// Check if specified date matches next flight departure
					if (!isMatching && firstAndNextDep[1] != null) {
						if (firstAndNextDep[1].get(Calendar.MONTH) == specifiedFirstDepDate.get(Calendar.MONTH)
								&& firstAndNextDep[1].get(Calendar.DAY_OF_MONTH) == specifiedFirstDepDate
										.get(Calendar.DAY_OF_MONTH)) {
							isMatching = true;
						}
					}

					if (!isMatching) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INCORRECT_FIRST_DEPARTURE_DATE, "");
					}
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"First flight departure date not specified.");
				}
			}// end ATM/CDM specific processing
		}
	}

	/**
	 * Returns first and next fight segment's departure date(Local).
	 * 
	 * @param segmentsView
	 * @return Calendar [] {firstSegDepDateLocal, nextSegDepDateLocal}
	 */
	private Calendar[] getFirstAndNextSegDepDateLocal(Set<LCCClientReservationSegment> segments) {
		Date firstSegDepDate = null;
		Date firstSegDepDataZulu = null;

		Date nextSegDepDate = null;
		Date nextSegDepDateZulu = null;

		Date currentZuluDate = CalendarUtil.getCurrentSystemTimeInZulu();
		for (LCCClientReservationSegment resSeg : segments) {

			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSeg.getStatus())) {
				if ((firstSegDepDataZulu == null || firstSegDepDataZulu.after(resSeg.getDepartureDateZulu()))) {
					firstSegDepDate = resSeg.getDepartureDate();
					firstSegDepDataZulu = resSeg.getDepartureDateZulu();
				}

				if (resSeg.getDepartureDateZulu().after(currentZuluDate)
						&& (nextSegDepDateZulu == null || nextSegDepDateZulu.after(resSeg.getDepartureDateZulu()))) {
					nextSegDepDate = resSeg.getDepartureDate();
					nextSegDepDateZulu = resSeg.getDepartureDateZulu();
				}
			}
		}

		Calendar calFirstSeg = null;
		Calendar calNextSeg = null;

		if (firstSegDepDataZulu != null) {
			calFirstSeg = Calendar.getInstance();
			calFirstSeg.setTime(firstSegDepDate);
		}

		if (nextSegDepDateZulu != null) {
			calNextSeg = Calendar.getInstance();
			calNextSeg.setTime(nextSegDepDate);
		}

		return new Calendar[] { calFirstSeg, calNextSeg };
	}

	/**
	 * 
	 * @param reservationListTO
	 * @param loadDataOptions
	 * @param userPrincipal
	 * @return
	 * @throws ModuleException
	 */
	public LCCClientReservation getReservation(ReservationListTO reservationListTO, AALoadDataOptionsType loadDataOptions,
			UserPrincipal userPrincipal) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		pnrModesDTO.setPnr(reservationListTO.getPnrNo());

		if (StringUtils.isNotEmpty(pnrModesDTO.getMarketingAirlineCode()) && StringUtils.isNotEmpty(pnrModesDTO.getAirlineCode())) {
			pnrModesDTO.setMarketingAirlineCode(pnrModesDTO.getMarketingAirlineCode());
			pnrModesDTO.setAirlineCode(pnrModesDTO.getAirlineCode());
			pnrModesDTO.setGroupPNR(reservationListTO.getPnrNo());
		}

		// temperary by defult if loadDataOptions is null everything will be true
		// due to a method called by mehedi's functions
		if (loadDataOptions == null) {
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
		} else {
			if ((loadDataOptions.getLoadPriceInfoTotals() != null && loadDataOptions.getLoadPriceInfoTotals())
					|| (loadDataOptions.getLoadPTCPriceInfo() != null && loadDataOptions.getLoadPTCPriceInfo())) {
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadOndChargesView(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
			}
			if (loadDataOptions.getLoadAirItinery() != null && loadDataOptions.getLoadAirItinery()) {
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadSegView(true);
			}
			if ((loadDataOptions.getLoadFullFilment() != null && loadDataOptions.getLoadFullFilment())
					|| (loadDataOptions.getLoadPTCFullFilment() != null && loadDataOptions.getLoadPTCFullFilment())) {
				pnrModesDTO.setLoadPaxAvaBalance(true);
			}
		}

		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setRecordAudit(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadExternalReference(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadAirportTransferInfo(true);

		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		modificationParamRQInfo.setIsRegisteredUser(false);

		return WebServicesModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				CommonServicesUtil.getTrackInfo(userPrincipal));
	}
	
	/**
	 * @param pnrModesDTO
	 * @param modificationParamRQInfo
	 * @param userPrincipal
	 * @return LCCClientReservation
	 * @throws ModuleException
	 */
	public LCCClientReservation getReservation(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo modificationParamRQInfo,
			UserPrincipal userPrincipal) throws ModuleException {
		return WebServicesModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
				CommonServicesUtil.getTrackInfo(userPrincipal));
	}

	public ServiceResponce isExternalReferenceExist(String externalReference, String status, char productType)
			throws ModuleException {
		return WebServicesModuleUtils.getAirproxyReservationBD().isPaymentReferenceAvailable(externalReference, status,
				productType);
	}

	/**
	 * Extract Read Request Parameters
	 * 
	 * @param oTAReadRQ
	 * @return
	 * @throws ModuleException
	 */
	private ReservationSearchDTO extractReservationSearchReqParams(OTAReadRQ oTAReadRQ) throws ModuleException {
		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();

		processReadRequest(reservationSearchDTO, oTAReadRQ.getReadRequests().getReadRequest());
		processGlobalReadRequest(reservationSearchDTO, oTAReadRQ.getReadRequests().getGlobalReservationReadRequest());
		processAirReadRequest(reservationSearchDTO, oTAReadRQ.getReadRequests().getAirReadRequest());

		return reservationSearchDTO;
	}

	/**
	 * Process Read Request
	 * 
	 * @param reservationPaymentDTO
	 * @param listReadRequest
	 */
	private void processReadRequest(ReservationSearchDTO reservationSearchDTO, List<ReadRequest> listReadRequest) {
		if (listReadRequest != null) {
			String pnr;
			String telephoneNo;
			String emailAddress;
			String givenName;
			String surname;
			String maskedCreditCardNumber;
			String creditCardExpiryDate;

			for (OTAReadRQ.ReadRequests.ReadRequest readRequest : listReadRequest) {

				// Capturing the pnr
				if (readRequest.getUniqueID() != null) {
					pnr = BeanUtils.nullHandler(readRequest.getUniqueID().getID());
					if (!pnr.equals("")) {
						reservationSearchDTO.setPnr(pnr);
					}
				}

				// Verification is optional to validate the earlier record
				if (readRequest.getVerification() != null) {
					// Capturing the telephone number
					if (readRequest.getVerification().getTelephoneInfo() != null) {
						telephoneNo = BeanUtils.nullHandler(readRequest.getVerification().getTelephoneInfo().getPhoneNumber());
						if (!telephoneNo.equals("")) {
							reservationSearchDTO.setTelephoneNo(telephoneNo);
						}
					}

					// Capturing the email address
					if (readRequest.getVerification().getEmail() != null) {
						emailAddress = BeanUtils.nullHandler(readRequest.getVerification().getEmail().getValue());
						if (!emailAddress.equals("")) {
							reservationSearchDTO.setEmail(emailAddress);
						}
					}

					// Capturing the person name
					if (readRequest.getVerification().getPersonName() != null) {
						// Capturing the given name
						givenName = BeanUtils.nullHandler(readRequest.getVerification().getPersonName().getGivenName().get(0));
						if (!givenName.equals("")) {
							reservationSearchDTO.setFirstName(givenName);
						}

						// Capturing the surname
						surname = BeanUtils.nullHandler(readRequest.getVerification().getPersonName().getSurname());
						if (!surname.equals("")) {
							reservationSearchDTO.setLastName(surname);
						}
					}

					// // Capturing the credit card information
					// if its a reservationPaymentDTO, its searching by card name..etc
					// if (readRequest.getVerification().getPaymentCard() != null) {
					// // Capturing the masked credit card number
					// maskedCreditCardNumber = BeanUtils.nullHandler(readRequest.getVerification().getPaymentCard()
					// .getMaskedCardNumber());
					// if (!maskedCreditCardNumber.equals("")) {
					// if (maskedCreditCardNumber.length() == 16) {
					// reservationPaymentDTO.setCreditCardNo(maskedCreditCardNumber.substring(maskedCreditCardNumber
					// .length() - 4));
					// }
					// }
					//
					// // Capturing the credit card expiry date
					// creditCardExpiryDate = BeanUtils.nullHandler(readRequest.getVerification().getPaymentCard()
					// .getExpireDate());
					// if (!creditCardExpiryDate.equals("")) {
					// reservationPaymentDTO.setEDate(creditCardExpiryDate);
					// }
					// }
				}
			}
		}
	}

	/**
	 * Process a global read request
	 * 
	 * @param reservationPaymentDTO
	 * @param globalReservationReadRequest
	 */
	private void processGlobalReadRequest(ReservationSearchDTO reservationSearchDTO,
			List<GlobalReservationReadRequest> globalReservationReadRequest) {
		if (globalReservationReadRequest != null) {
			String givenName;
			String surname;
			PersonNameType personNameType;

			for (OTAReadRQ.ReadRequests.GlobalReservationReadRequest readRequest : globalReservationReadRequest) {
				personNameType = readRequest.getTravelerName();

				if (personNameType != null) {
					// Capturing the given name
					givenName = BeanUtils.nullHandler(personNameType.getGivenName().get(0));
					if (!givenName.equals("")) {
						reservationSearchDTO.setFirstName(givenName);
					}

					// Capturing the surname
					surname = BeanUtils.nullHandler(personNameType.getSurname());
					if (!surname.equals("")) {
						reservationSearchDTO.setLastName(surname);
					}
				}
			}
		}
	}

	/**
	 * Process Air Read Request
	 * 
	 * @param reservationPaymentDTO
	 * @param listAirReadRequest
	 */
	private void processAirReadRequest(ReservationSearchDTO reservationSearchDTO, List<AirReadRequest> listAirReadRequest) {
		if (listAirReadRequest != null) {
			String flightNumber;
			String departureStation;
			String telephoneNo;
			String surname;
			String givenName;
			String maskedCreditCardNumber;
			String creditCardExpiryDate;
			Date departureDate;

			for (OTAReadRQ.ReadRequests.AirReadRequest airReadRequest : listAirReadRequest) {

				// If flight number specified
				if (airReadRequest.getFlightNumber() != null) {
					flightNumber = BeanUtils.nullHandler(airReadRequest.getFlightNumber());
					if (!flightNumber.equals("")) {
						reservationSearchDTO.setFlightNo(flightNumber);
					}
				}

				// If departure airport is not null
				if (airReadRequest.getDepartureAirport() != null) {
					departureStation = BeanUtils.nullHandler(airReadRequest.getDepartureAirport().getLocationCode());
					if (!departureStation.equals("")) {
						reservationSearchDTO.setFromAirport(departureStation);
					}
				}

				// If departure date is not null
				if (airReadRequest.getDepartureDate() != null) {
					departureDate = airReadRequest.getDepartureDate().toGregorianCalendar().getTime();
					if (departureDate != null) {
						reservationSearchDTO.setDepartureDate(departureDate);
					}
				}

				// If person name exist
				if (airReadRequest.getName() != null) {
					// Capturing the given name
					givenName = BeanUtils.nullHandler(airReadRequest.getName().getGivenName().get(0));
					if (!givenName.equals("")) {
						reservationSearchDTO.setFirstName(givenName);
					}

					// Capturing the surname
					surname = BeanUtils.nullHandler(airReadRequest.getName().getSurname());
					if (!surname.equals("")) {
						reservationSearchDTO.setLastName(surname);
					}
				}

				// If telephone number exist
				if (airReadRequest.getTelephone() != null) {
					telephoneNo = BeanUtils.nullHandler(airReadRequest.getTelephone().getPhoneNumber());
					if (!telephoneNo.equals("")) {
						reservationSearchDTO.setTelephoneNo(telephoneNo);
					}
				}

				// // If the credit card information exist
				// if its reservationPaymentDTO,its looking for cc no
				// if (airReadRequest.getCreditCardInfo() != null) {
				// // Capturing the masked credit card number
				// maskedCreditCardNumber =
				// BeanUtils.nullHandler(airReadRequest.getCreditCardInfo().getMaskedCardNumber());
				// if (!maskedCreditCardNumber.equals("")) {
				// if (maskedCreditCardNumber.length() == 16) {
				// reservationPaymentDTO.setCreditCardNo(maskedCreditCardNumber.substring(maskedCreditCardNumber
				// .length() - 4));
				// }
				// }
				//
				// // Capturing the credit card expiry date
				// creditCardExpiryDate = BeanUtils.nullHandler(airReadRequest.getCreditCardInfo().getExpireDate());
				// if (!creditCardExpiryDate.equals("")) {
				// reservationPaymentDTO.setEDate(creditCardExpiryDate);
				// }
				// }
			}
		}
	}
	
	private static List<SpecialServiceRequest> composeSpecialServiceRequests(List<String[]> ssrDetails, String travelerRefRPH) {
		List<SpecialServiceRequest> ssrList = new ArrayList<SpecialServiceRequest>();
		if (ssrDetails != null && !ssrDetails.isEmpty()) { 

			for (String[] ssrData : ssrDetails) {
				if (ssrData != null && !StringUtil.isNullOrEmpty(ssrData[0]) && !StringUtil.isNullOrEmpty(ssrData[1])) {
					SpecialServiceRequest ssr = new SpecialServiceRequest();
					ssr.setSSRCode(ssrData[0]);
					ssr.setText(ssrData[1]);
					ssr.getTravelerRefNumberRPHList().clear();
					ssr.getTravelerRefNumberRPHList().add(travelerRefRPH);

					ssrList.add(ssr);
				}

			}

		}
		
		return ssrList;
	}

	private ReservationSearchDTO includeReporingAgentSearchParams(ReservationSearchDTO reservationSearchDTO) {
		
		Collection<String> privilegesKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);	
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		
		if (!AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ANY_PNR_SEARCH)) {
			reservationSearchDTO.setOwnerAgentCode(userPrincipal.getAgentCode());
			if (AuthorizationUtil
					.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALL_REPORTING_AGENT_PNR_SEARCH)) {
				reservationSearchDTO.setSearchAllReportingAgentBookings(true);
				reservationSearchDTO.setOwnerAgentCode(userPrincipal.getAgentCode());
			} else if (AuthorizationUtil
					.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.REPORTING_AGENT_PNR_SEARCH)) {
				reservationSearchDTO.setSearchReportingAgentBookings(true);
				reservationSearchDTO.setOwnerAgentCode(userPrincipal.getAgentCode());
			}
		}
		return reservationSearchDTO;
	}
}
