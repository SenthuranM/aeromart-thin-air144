package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRQ;
import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRS;

import com.isa.thinair.webservices.api.service.AAWebServicesForMealServices;
import com.isa.thinair.webservices.core.bl.AAMealServicesBL;

@WebService(name = "AAWebServicesForMealServices", targetNamespace = "http://www.isa.ae/system/crew",
		endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForMealServices")
public class AAWebServicesForMealServicesImpl implements AAWebServicesForMealServices {

	private final Log log = LogFactory.getLog(AAWebServicesForMealServicesImpl.class);

	public AAMealServicesMealRecordsByFltSegRS mealRecordsByFlightSegment (AAMealServicesMealRecordsByFltSegRQ mealRecordsByFltSegRQ) {
		AAMealServicesMealRecordsByFltSegRS mealRecordsRS = null;

		if(log.isDebugEnabled()) {
			log.debug("BEGIN ---- AAMealServicesMealRecordsByFltSegRS:mealRecordsByFlightSegment(AAMealServicesMealRecordsByFltSegRQ)");
		}

		mealRecordsRS = AAMealServicesBL.getMealRecords(mealRecordsByFltSegRQ);

		if(log.isDebugEnabled()) {
			log.debug("END ---- AAMealServicesMealRecordsByFltSegRS:mealRecordsByFlightSegment(AAMealServicesMealRecordsByFltSegRQ)");
		}

		return mealRecordsRS;
	}
}
