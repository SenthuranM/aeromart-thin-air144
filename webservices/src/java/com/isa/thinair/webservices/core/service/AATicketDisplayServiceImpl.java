package com.isa.thinair.webservices.core.service;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.thinair.commons.api.dto.ets.TicketDisplayReq;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayRes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.service.AATicketDisplayService;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

@Controller
@RequestMapping(value = "aeromart/ticket", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AATicketDisplayServiceImpl implements AATicketDisplayService {

	@Override
	@ResponseBody
	@RequestMapping(value = "/display")
	public TicketDisplayRes invokeTicketDisplay(@RequestBody TicketDisplayReq request) {

		TicketDisplayRes response = null;
		try {

			response = callDisplayTicket(request);

		} catch (ModuleException e) {
			response = new TicketDisplayRes();
			response.setSuccess(false);

		}
		return response;
	}
	
	private TicketDisplayRes callDisplayTicket(TicketDisplayReq request) throws ModuleException{
		
		
		TicketDisplayRes response =  new TicketDisplayRes();
		String rawResponse = WebServicesModuleUtils.getGdsServicesBD().processEdifactMesssage(request.getRawRequest(), true);
		
		response.setRawResponse(rawResponse);
		
		return response;
	}

}
