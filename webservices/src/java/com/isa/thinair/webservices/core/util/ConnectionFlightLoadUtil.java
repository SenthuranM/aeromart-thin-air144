package com.isa.thinair.webservices.core.util;


import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isaaviation.thinair.webservices.api.airschedules.AAAirportSummaryType;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAIOBoundPaxCountInfoRS;

/**
 * @author Byorn
 */
public class ConnectionFlightLoadUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(ConnectionFlightLoadUtil.class);

	public static AAIOBoundPaxCountInfoRS getInboundOutboundPaxCount(AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ) {

		AAIOBoundPaxCountInfoRS iBInfoRS = new AAIOBoundPaxCountInfoRS();		

		try {

			Date departureDateZulu = flightLetPaxCountRQ.getDepartureZulu().toGregorianCalendar().getTime();
			
			Collection<Airport>   airports = WebServicesModuleUtils.getAirportBD().getActiveAirports();
			StringBuffer sb = new StringBuffer();
			for(Airport  airport : airports){
				sb.append(airport.getAirportCode());
				sb.append(",");
			}			
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			
			search.setFlightNumber(flightLetPaxCountRQ.getFlightNumber().toUpperCase());
			search.setCountryOfResidence(sb.toString());
			search.setReportOption("I");
			String strMaxCon = WebServicesModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.MAX_TRANSIT_TIME);
			String[] arrMaxCon = strMaxCon.split(":");			
			search.setConTime(arrMaxCon[0]);
			search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			
			search.setDateFrom(toDateType(departureDateZulu,true));
			search.setDateTo(toDateType(departureDateZulu,false));
			
			ResultSet resultSet = WebServicesModuleUtils.getDataExtractionBD().getInbDatas(search);
			
			while(resultSet != null && resultSet.next()){
				AAAirportSummaryType airportSummaryType = new AAAirportSummaryType();
				airportSummaryType.setAirportCode(resultSet.getString("ORIGIN_DESTINATION_CODE"));
				airportSummaryType.setJourneyType("O");
				airportSummaryType.setPaxCount(resultSet.getInt("NO_OF_PAX"));
				
				iBInfoRS.getAirportPaxTypeCounts().add(airportSummaryType);		
			}
			
			search.setReportOption("O");
			ResultSet rs = WebServicesModuleUtils.getDataExtractionBD().getInbDatas(search);
			
			while(rs != null && rs.next()){
				AAAirportSummaryType airportSummaryType = new AAAirportSummaryType();
				airportSummaryType.setAirportCode(rs.getString("ORIGIN_DESTINATION_CODE"));
				airportSummaryType.setJourneyType("I");
				airportSummaryType.setPaxCount(rs.getInt("NO_OF_PAX"));
				
				iBInfoRS.getAirportPaxTypeCounts().add(airportSummaryType);		
			}			

		} catch (ModuleException e) {
			log.error(e);
		} catch (Throwable e) {
			log.error(e);
		}
		return iBInfoRS;

	}
	
	private static Timestamp toDateType(Date date, boolean fromDate) {
		Timestamp dateTime = null;

		Calendar cal = new GregorianCalendar();
		cal.setTime(date);

		if (fromDate) {
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		} else {
			cal.set(Calendar.HOUR, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
		}

		dateTime = new Timestamp(cal.getTimeInMillis());

		return dateTime;
	}

}
