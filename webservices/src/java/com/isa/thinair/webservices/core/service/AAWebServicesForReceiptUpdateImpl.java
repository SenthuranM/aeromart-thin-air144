package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import com.isa.thinair.webservices.api.service.AAWebServicesForReceiptUpdate;


/**
 * @author Manoj Dhanushka
 */
@WebService(serviceName = "AAWebServicesForReceiptUpdate", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForReceiptUpdate")
public class AAWebServicesForReceiptUpdateImpl extends BaseWebServicesImpl implements AAWebServicesForReceiptUpdate {
}
