package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRS;
import org.opentravel.ota._2003._05.AutomaticCheckinDetailsRequestType;
import org.opentravel.ota._2003._05.AutomaticCheckinDetailsResponseType;
import org.opentravel.ota._2003._05.AutomaticCheckinDetailsResponsesType;
import org.opentravel.ota._2003._05.AutomaticCheckinInfoType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinRequest;
import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinResponse;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public class AutomaticChekinDetailsUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(AutomaticChekinDetailsUtil.class);
	
	public AutomaticChekinDetailsUtil() {
		super();
	}

	public AAOTAAirAutomaticCheckinDetailsRS getAutomaticCheckinDetails(AAOTAAirAutomaticCheckinDetailsRQ otaAutomaticCheckinRQ)
			throws ModuleException, WebservicesException {
		AAOTAAirAutomaticCheckinDetailsRS otaAirAutomaticCheckinDetailsRS = new AAOTAAirAutomaticCheckinDetailsRS();
		otaAirAutomaticCheckinDetailsRS.setErrors(new ErrorsType());
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo();
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AUTOCHECKIN_REQUEST, principal, trackInfo,
				otaAutomaticCheckinRQ.getTransactionIdentifier());

		prepareFlightSegmentResParams(otaAutomaticCheckinRQ, otaAirAutomaticCheckinDetailsRS);

		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.AUTOCHECKIN_RESPONSE, principal, trackInfo,
				otaAutomaticCheckinRQ.getTransactionIdentifier());
		return otaAirAutomaticCheckinDetailsRS;
	}

	private void prepareFlightSegmentResParams(AAOTAAirAutomaticCheckinDetailsRQ otaAirPriceRQ,
			AAOTAAirAutomaticCheckinDetailsRS otaAirPriceRS) throws WebservicesException, ModuleException {

		otaAirPriceRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
		otaAirPriceRS.setTransactionIdentifier(otaAirPriceRQ.getTransactionIdentifier());
		otaAirPriceRS.setSequenceNmbr(otaAirPriceRQ.getSequenceNmbr());
		otaAirPriceRS.setEchoToken(otaAirPriceRQ.getEchoToken());
		otaAirPriceRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		AutomaticCheckinDetailsResponsesType autoCheckinResponses = new AutomaticCheckinDetailsResponsesType();
		otaAirPriceRS.setAutomaticCheckinDetailsResponses(autoCheckinResponses);
		AutomaticCheckinRequest automaticCheckinRQ = new AutomaticCheckinRequest();

		List<AutomaticCheckinDetailsRequestType> automaticCheckinDetailsRequest = otaAirPriceRQ.getAutomaticCheckinDetails()
				.getAutomaticCheckinDetailsRequests().getAutomaticCheckinDetailsRequest();
		for (AutomaticCheckinDetailsRequestType autoCheckinRequestType : automaticCheckinDetailsRequest) {
			List<BookFlightSegmentType> flightSegmentList = autoCheckinRequestType.getFlightSegmentInfo();
			for (BookFlightSegmentType flightSegment : flightSegmentList) {
				AutomaticCheckinDetailsResponseType autoCheckinResponse = new AutomaticCheckinDetailsResponseType();
				autoCheckinResponses.getAutomaticCheckinDetailsResponse().add(autoCheckinResponse);

				BookFlightSegmentType otaFlightSegment = new BookFlightSegmentType();
				autoCheckinResponse.getFlightSegmentInfo().add(otaFlightSegment);

				otaFlightSegment.setRPH(flightSegment.getRPH());
				otaFlightSegment.setArrivalDateTime(flightSegment.getArrivalDateTime());
				otaFlightSegment.setDepartureDateTime(flightSegment.getDepartureDateTime());
				otaFlightSegment.setFlightNumber(flightSegment.getFlightNumber());

				FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
				departureAirport.setLocationCode(flightSegment.getDepartureAirport().getLocationCode());
				departureAirport.setTerminal(flightSegment.getDepartureAirport().getTerminal());

				otaFlightSegment.setDepartureAirport(departureAirport);

				automaticCheckinRQ.setDepartAirPortCode(flightSegment.getDepartureAirport().getLocationCode());

				FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
				arrivalAirport.setLocationCode(flightSegment.getArrivalAirport().getLocationCode());
				arrivalAirport.setTerminal(flightSegment.getArrivalAirport().getTerminal());

				otaFlightSegment.setArrivalAirport(arrivalAirport);
				
				boolean areAirportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(
						otaFlightSegment.getDepartureAirport().getLocationCode(),
						otaFlightSegment.getArrivalAirport().getLocationCode());

				if (!areAirportsValid) {
					log.warn("Invalid flight search request found in createFlightAvailRQ(OTAAirAvailRQ). "
							+ "From/To aiports validation failed. " + "[From airport="
							+ otaFlightSegment.getDepartureAirport().getLocationCode() + "," + "To airport="
							+ otaFlightSegment.getArrivalAirport().getLocationCode() + "]");

					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Invalid From and/or To Airport(s)");
				}

				AutomaticCheckinInfoType automaticCheckin = new AutomaticCheckinInfoType();
				autoCheckinResponse.setAutomaticCheckin(automaticCheckin);

				prepareAutomaticCheckinResponse(automaticCheckinRQ, automaticCheckin);
			}
		}

	}

	private void prepareAutomaticCheckinResponse(AutomaticCheckinRequest automaticCheckinRQ,
			AutomaticCheckinInfoType automaticCheckin) throws ModuleException, WebservicesException {
		AutomaticCheckinResponse automaticCheckinResponse = WebServicesModuleUtils.getAutomaticCheckinBD()
				.getAutomaticCheckinResponse(automaticCheckinRQ);
		if (automaticCheckinResponse.getAutomaticCheckinDTO() != null) {
			List<AutomaticCheckinDTO> automaticCheckinDTOs = automaticCheckinResponse.getAutomaticCheckinDTO();
			for (AutomaticCheckinDTO automaticCheckinDTO : automaticCheckinDTOs) {
				automaticCheckin.setAdultAmount(automaticCheckinDTO.getAmount());
				automaticCheckin.setChildAmount(automaticCheckinDTO.getAmount());
				automaticCheckin.setInfantAmount(new BigDecimal(BigInteger.ZERO));
				automaticCheckin.setApplicability(WebservicesConstants.RequestResponseTypes.AUTOCHECKIN_APPLICABILITY_YES);
			}

			if (automaticCheckinDTOs.size() == 0) {
				automaticCheckin.setApplicability(WebservicesConstants.RequestResponseTypes.AUTOCHECKIN_APPLICABILITY_NO);
				automaticCheckin.setAdultAmount(new BigDecimal(BigInteger.ZERO));
				automaticCheckin.setChildAmount(new BigDecimal(BigInteger.ZERO));
				automaticCheckin.setInfantAmount(new BigDecimal(BigInteger.ZERO));
			}
		}
	}

}