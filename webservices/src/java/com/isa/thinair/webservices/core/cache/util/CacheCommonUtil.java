package com.isa.thinair.webservices.core.cache.util;

import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.CacheModuleName;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class CacheCommonUtil {
	
	private static final String MASTER_DATA_CACHE_UTIL = "materDataCacheUtilBean";

	public static String getCacheEnvironmentCode() {
		return AppSysParamsUtil.getEnvironmentIdentificationCode();
	}

	public static String getFullQualifiedModuleName() {
		return getCacheEnvironmentCode() + CacheConstant.cacheDataSeparator + CacheModuleName.WS.name();
	}

	public static String getFullQualifiedCacheName(String operationCode) {
		return getFullQualifiedModuleName() + CacheConstant.cacheDataSeparator + operationCode;
	}
	
	public static MasterDataCacheUtil getMasterDataCacheUtil() {
		return  WebServicesModuleUtils.getBeanByName(MASTER_DATA_CACHE_UTIL);		
	}
	
	public static boolean isSpecificCacheAgent(String agentCode) {	
		boolean specificCacheAgent = false;
		MasterDataCacheUtil masterDataCacheUtil = getMasterDataCacheUtil(); 
		
		if (masterDataCacheUtil.isEnableSpecificAgentsCache()) {
			List<String> agents = masterDataCacheUtil.getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents();
			if (agents.contains(agentCode)) {
				specificCacheAgent = true;
			}			
		}
		return specificCacheAgent;		
	}

}
