package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class BaggageDetailsInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(BaggageDetailsInterlineUtil.class);

	/**
	 * 
	 * @param travelerFlightDetailsWithBaggages
	 * @param flightPriceRS
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<String, LCCBaggageDTO>> getSelectedBaggageDetails(
			Map<String, Map<String, String>> travelerFlightDetailsWithBaggages, FlightPriceRS flightPriceRS, SYSTEM system,
			Map<String, Integer> baggageOndGrpIdWiseCount) throws ModuleException, WebservicesException {

		Map<String, Map<String, LCCBaggageDTO>> segmentWithBaggages = new HashMap<String, Map<String, LCCBaggageDTO>>();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());
		}

		List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs, null, null);

		List<BundledFareDTO> bundledFareDTOs = null;
		if (flightPriceRS != null && flightPriceRS.getSelectedPriceFlightInfo() != null
				&& flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
			bundledFareDTOs = flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO().getOndBundledFareDTOs();
		}

		LCCBaggageResponseDTO baggageResponseDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getAvailableBaggages(
				lccBaggageRequestDTOs, null, system, null, false, false, false, null,
				getReservationBaggageSummaryTo(flightPriceRS), bundledFareDTOs, null, null);

		if (baggageResponseDTO == null) {
			throw new WebservicesException(
					IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_BAGGAGES_ARE_NOT_AVAILABLE_AT_THE_MOMENT, "");
		}

		if (AppSysParamsUtil.isONDBaggaeEnabled() && baggageResponseDTO.getFlightSegmentBaggages().size() > 0) {
			checkBaggageBookedForAllOnds(baggageResponseDTO,travelerFlightDetailsWithBaggages);
			for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : baggageResponseDTO.getFlightSegmentBaggages()) {
				String ondGroupId = lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getBaggageONDGroupId();
				Integer count = baggageOndGrpIdWiseCount.get(ondGroupId);
				if (count == null) {
					baggageOndGrpIdWiseCount.put(ondGroupId, new Integer(1));
				} else {
					baggageOndGrpIdWiseCount.put(ondGroupId, ++count);
				}
			}
		}

		for (String travelerRef : travelerFlightDetailsWithBaggages.keySet()) {
			// FlightRPH -> Baggage
			Map<String, String> flightDetailsWithBaggages = travelerFlightDetailsWithBaggages.get(travelerRef);
			for (String flightRPH : flightDetailsWithBaggages.keySet()) {
				String selectedBaggage = flightDetailsWithBaggages.get(flightRPH);
				if (selectedBaggage != null) {
					for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
						if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
							Collection<LCCBaggageDTO> flightBaggageDTOs = getLCCBaggageDTOByRPH(flightRPH,
									baggageResponseDTO.getFlightSegmentBaggages());
							boolean isBaggageFound = false;
							for (LCCBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
								if (flightBaggageDTO.getBaggageName().equals(selectedBaggage)) {
									isBaggageFound = true;

									if (segmentWithBaggages.get(travelerRef) == null) {
										segmentWithBaggages.put(travelerRef, new HashMap<String, LCCBaggageDTO>());
									}
									segmentWithBaggages.get(travelerRef).put(flightRPH, flightBaggageDTO);

								}
							}

							if (!isBaggageFound) {
								throw new WebservicesException(
										IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_BAGGAGES_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
										selectedBaggage);
							}
						}

					}

				}
			}
		}

		return segmentWithBaggages;
	}

	/**
	 * 
	 * @param flightRPH
	 * @param baggagesDTOs
	 * @return
	 */
	private static List<LCCBaggageDTO> getLCCBaggageDTOByRPH(String flightRPH, List<LCCFlightSegmentBaggagesDTO> baggagesDTOs) {
		List<LCCBaggageDTO> lccBaggageDTOs = new ArrayList<LCCBaggageDTO>();

		if (baggagesDTOs != null) {
			for (LCCFlightSegmentBaggagesDTO baggagesDTO : baggagesDTOs) {
				FlightSegmentTO flightSegmentTO = baggagesDTO.getFlightSegmentTO();

				if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
					lccBaggageDTOs.addAll(baggagesDTO.getBaggages());

					break;
				}
			}
		}
		return lccBaggageDTOs;

	}

	/**
	 * Compose the baggage request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCBaggageRequestDTO>
	 */
	private static List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String cabinClass,
			String txnId) {
		List<LCCBaggageRequestDTO> bg = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCBaggageRequestDTO bgd = new LCCBaggageRequestDTO();
			bgd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				bgd.setCabinClass(cabinClass);
			} else {
				bgd.setCabinClass(fst.getCabinClassCode());
			}
			bgd.setFlightSegment(fst);
			bg.add(bgd);
		}
		return bg;
	}

	/**
	 * @param baggageDetailsMap
	 * @param baggageOndGrpIdWiseCount
	 * @return externalCharegs
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForBaggages(
			Map<String, Map<String, LCCBaggageDTO>> baggageDetailsMap, Map<String, Integer> baggageOndGrpIdWiseCount)
			throws ModuleException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();

		for (String travelerRef : baggageDetailsMap.keySet()) {
			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();
			Map<String, LCCBaggageDTO> baggagesMap = baggageDetailsMap.get(travelerRef);
			for (String flightRPH : baggagesMap.keySet()) {
				LCCBaggageDTO baggageDTO = baggagesMap.get(flightRPH);

				LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

				if (AppSysParamsUtil.isONDBaggaeEnabled()) {
					BigDecimal baggageCharge = AccelAeroCalculator.divide(baggageDTO.getBaggageCharge(),
							baggageOndGrpIdWiseCount.get(baggageDTO.getOndBaggageGroupId()));

					externalChargeTO.setAmount(baggageCharge);
				} else {
					externalChargeTO.setAmount(baggageDTO.getBaggageCharge());
				}

				externalChargeTO.setCode(baggageDTO.getBaggageName());
				externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
				externalChargeTO.setFlightRefNumber(flightRPH);
				externalChargeTO.setOndBaggageChargeGroupId(baggageDTO.getOndBaggageChargeId());
				externalChargeTO.setOndBaggageGroupId(baggageDTO.getOndBaggageGroupId());

				chargesList.add(externalChargeTO);

			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}

	private static LCCReservationBaggageSummaryTo getReservationBaggageSummaryTo(FlightPriceRS flightPriceRS) {
		LCCReservationBaggageSummaryTo baggageSummaryTo = AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightPriceRS);
		return baggageSummaryTo;
	}
	
	private static void checkBaggageBookedForAllOnds(LCCBaggageResponseDTO baggageResponseDTO,
			Map<String, Map<String, String>> travelerFlightDetailsWithBaggages) throws WebservicesException {
		Map<String, Set<String>> bagAvailableBagOndGroupWiseFlightRphSet = new HashMap<String, Set<String>>();

		if (baggageResponseDTO.getFlightSegmentBaggages().size() > 0) {
			for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : baggageResponseDTO.getFlightSegmentBaggages()) {
				if (lccFlightSegmentBaggagesDTO.getBaggages().size() > 0) {
					String bagOndGroupId = lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getBaggageONDGroupId();
					if (bagAvailableBagOndGroupWiseFlightRphSet.containsKey(bagOndGroupId)) {
						bagAvailableBagOndGroupWiseFlightRphSet.get(bagOndGroupId).add(
								lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getFlightRefNumber());
					} else {
						Set<String> tmpRphSet = new HashSet<String>();
						tmpRphSet.add(lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getFlightRefNumber());
						bagAvailableBagOndGroupWiseFlightRphSet.put(bagOndGroupId, tmpRphSet);
					}
				}

			}
		}

		for (String key : bagAvailableBagOndGroupWiseFlightRphSet.keySet()) {
			Set<String> flightRphSet = bagAvailableBagOndGroupWiseFlightRphSet.get(key);
			for (String travelerRef : travelerFlightDetailsWithBaggages.keySet()) {
				int count = flightRphSet.size();
				int bagSelectedFlightCount = 0;
				Map<String, String> flightDetailsWithBaggages = travelerFlightDetailsWithBaggages.get(travelerRef);
				for (String rphValue : flightRphSet) {
					if (flightDetailsWithBaggages != null && flightDetailsWithBaggages.containsKey(rphValue)) {
						bagSelectedFlightCount++;
					}
				}
				if (bagSelectedFlightCount != 0 && bagSelectedFlightCount != count) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Invalid Baggage Selection for: " + travelerRef);
				}
			}
		}
	}
}