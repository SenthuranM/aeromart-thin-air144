package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ClassOfServicePreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.interlineUtil.PriceQuoteInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * To extract the AvailableFlightSearchDTO for the booking request which will be used to re-price.
 * 
 * @author mekanayake
 * 
 */
public class PriceQuoteReqParamsExtractUtill {

	private final Log log = LogFactory.getLog(getClass());

	// holds the SelectedFlightDTOs calculated from the
	// AvailableFlightSearchDTOs. This will be used to recalculate the
	// total amount from price breakdowns.
	private final Collection<SelectedFlightDTO> selectedFlightDTOs = new ArrayList<SelectedFlightDTO>();

	private final Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();

	private final boolean isFixedFare;

	public PriceQuoteReqParamsExtractUtill(UserPrincipal userPrincipal) {
		super();
		// check normal fare or fixed fare to choose
		this.isFixedFare = CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA).equals(
				Integer.toString(userPrincipal.getSalesChannel()));
	}

	public Collection<SelectedFlightDTO> getSelectedFlightDTOs() {
		return selectedFlightDTOs;
	}

	public Collection<OndFareDTO> getOnDFareDTOs() {
		return ondFareDTOs;
	}

	/**
	 * Extract the AvailableFlightSearchDTO from the booking request data
	 * 
	 * @param itineraryType
	 * @param paxCount
	 *            {adult, infant, children}
	 * @param posSourceSize
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public Collection<AvailableFlightSearchDTO> extractPriceQuoteReqParams(AirItineraryType itineraryType, int[] paxCount)
			throws WebservicesException, ModuleException {

		Collection<AvailableFlightSearchDTO> availableFlightSearchDTOs = new ArrayList<AvailableFlightSearchDTO>();

		Map<String, Collection<BookFlightSegmentType>> priceKeysFSMap = getPriceKeyFSMap(itineraryType);
		for (String priceKey : priceKeysFSMap.keySet()) { // This loop create a
															// AvailableFlightSearchDTO
															// for every price
			// key in the request. This will be used for re-price
			AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();

			// used to find the journey is returned or not
			boolean returnSegments = false;
			Date returnDate = null;
			Collection<String> obAirports = new ArrayList<String>();
			Collection<String> bookingClassSet = new HashSet<String>();
			int bookingClassCount = 0;

			OriginDestinationInfoDTO outboundOndInfo = new OriginDestinationInfoDTO();
			// inbound and outbound flightSegment lists
			Collection<FlightSegmentDTO> obFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
			Collection<FlightSegmentDTO> ibFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
			for (BookFlightSegmentType bookFlightSegment : priceKeysFSMap.get(priceKey)) {

				/**
				 * departure date of the first segment of the journey is taken as departure date
				 */
				if (outboundOndInfo.getDepartureDateTimeStart() == null) {
					outboundOndInfo.setDepartureDateTimeStart(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
							.getTime());
				}

				/** first airport is taken as origin */
				if (obAirports.size() == 0) {
					outboundOndInfo.setOrigin(bookFlightSegment.getDepartureAirport().getLocationCode());
				}

				if (!returnSegments
						&& CommonUtil.isReturnSegment(obAirports, bookFlightSegment.getDepartureAirport().getLocationCode(),
								bookFlightSegment.getArrivalAirport().getLocationCode())) {
					returnSegments = true;
				}

				/**
				 * last station of the outbound journey is taken as the destination airport
				 */
				if (!returnSegments) {
					outboundOndInfo.setDestination(bookFlightSegment.getArrivalAirport().getLocationCode());
				}

				/**
				 * departure date of the first return segment is taken as the return date
				 */
				if (returnSegments && returnDate == null) {
					returnDate = bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime();
				}

				if (!returnSegments) {
					obAirports.add(bookFlightSegment.getDepartureAirport().getLocationCode());
					obAirports.add(bookFlightSegment.getArrivalAirport().getLocationCode());
				}

				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());

				if (!returnSegments) {
					obFlightSegmentDTOs.add(aaFlightSegmentDTO);
				} else {
					if (ibFlightSegmentDTOs == null) {
						log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). "
								+ "Invalid directionIndicator. " + "[directionInd=" + itineraryType.getDirectionInd() + "]");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"[Direction Indictor is invalid]");
					}
					ibFlightSegmentDTOs.add(aaFlightSegmentDTO);
				}

				/** Adding the Booking classes */
				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
					bookingClassCount++;
				}
			}

			boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
			if (returnSegments && !availableFlightSearchDTO.isOpenReturnSearch())
				availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);

			// retrieve the additional required information on flight segments
			Collection<FlightSegmentDTO> filledObFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
					.getFilledFlightSegmentDTOs(obFlightSegmentDTOs);

			if (filledObFlightSegmentDTOs.size() != obFlightSegmentDTOs.size()) {
				log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Outbound Segment(s). ");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Outbound Segment(s)]");
			}

			// set outbound flight segment IDs
			List<Integer> obFlightSegmentIds = new ArrayList<Integer>();
			for (FlightSegmentDTO flightSegmentDTO : filledObFlightSegmentDTOs) {
				if (flightSegmentDTO.getSegmentId() != null) {
					obFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
				} else {
					log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Outbound Segment. "
							+ "[" + flightSegmentDTO + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Outbound Segment. "
							+ flightSegmentDTO + "]");
				}
			}

			outboundOndInfo.setFlightSegmentIds(obFlightSegmentIds);

			if (bookingClassCount != 0 && bookingClassCount != priceKeysFSMap.get(priceKey).size()) {
				log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ bookingClassCount + "] [expected = " + priceKeysFSMap.get(priceKey).size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Cabin class missing from FlightSegment");
			}

			if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) {
				log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class shoud be same for all Flight Segments Per Price Quote"
						+ "[booking class given=" + bookingClassSet.size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
						"Too many cabin classes specifyed .Only one is supported per availability search");
			}

			String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
			String bookingType = BookingClass.BookingClassType.NORMAL;
			String cabinClass = null;

			if (bookingClass != null) {
				Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
				if (standByBCList.contains(bookingClass)) {
					bookingType = BookingClass.BookingClassType.STANDBY;
					bookingClass = null;
				} else {
					cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);
				}
			}

			if (bookingClass == null) {
				if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
					cabinClass = AppSysParamsUtil.getDefaultCOS();
				} else {
					cabinClass = "Y";
				}
			}

			outboundOndInfo.setPreferredBookingClass(bookingClass);
			outboundOndInfo.setPreferredClassOfService(cabinClass);
			outboundOndInfo.setPreferredBookingType(bookingType);
			availableFlightSearchDTO.addOriginDestination(outboundOndInfo);

			// set inbound flight segment IDs
			if (returnSegments) {
				Collection<FlightSegmentDTO> filledIbFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
						.getFilledFlightSegmentDTOs(ibFlightSegmentDTOs);

				if (filledIbFlightSegmentDTOs.size() != ibFlightSegmentDTOs.size()) {
					log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Inbound Segment(s). ");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment(s)]");
				}

				List<Integer> ibFlightSegmentIds = new ArrayList<Integer>();
				for (FlightSegmentDTO flightSegmentDTO : filledIbFlightSegmentDTOs) {
					if (flightSegmentDTO.getSegmentId() != null) {
						ibFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
					} else {
						log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Inbound Segment. "
								+ "[" + flightSegmentDTO + "]");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
								+ flightSegmentDTO + "]");
					}
				}

				OriginDestinationInfoDTO inboundOndInfo = new OriginDestinationInfoDTO();
				inboundOndInfo.setOrigin(outboundOndInfo.getDestination());
				inboundOndInfo.setDestination(outboundOndInfo.getOrigin());
				inboundOndInfo.setDepartureDateTimeStart(returnDate);
				inboundOndInfo.setFlightSegmentIds(ibFlightSegmentIds);
				inboundOndInfo.setPreferredBookingClass(bookingClass);
				inboundOndInfo.setPreferredClassOfService(cabinClass);
				inboundOndInfo.setPreferredBookingType(bookingType);
				availableFlightSearchDTO.addOriginDestination(inboundOndInfo);
			}

			// pax counts paxCount{adult, infant, children}
			if (paxCount[0] == 0 && paxCount[2] == 0) {
				log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid adult/children pax count. "
						+ "[adults pax=" + paxCount[0] + "]" + "[children pax=" + paxCount[2] + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
						"Adult and Children pax count cannot be zero");
			}
			availableFlightSearchDTO.setAdultCount(paxCount[0]);
			availableFlightSearchDTO.setInfantCount(paxCount[1]);
			availableFlightSearchDTO.setChildCount(paxCount[2]);

			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
			availableFlightSearchDTO.setAgentCode(principal.getAgentCode());

			// FIXME make followings configurable
			availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

			Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
	
			int availabilityRestriction = (AuthorizationUtil
					.hasPrivileges(privilegeKeys, PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY
					.equals(bookingType)) ? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
					: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

			availableFlightSearchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);
			availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
			availableFlightSearchDTOs.add(availableFlightSearchDTO);

		}// end of main loop

		return availableFlightSearchDTOs;
	}

	/**
	 * 
	 * @param airItineraryType
	 * @param otaPaxCountTO
	 * @param defaultBaggageOndMap
	 *            TODO
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public FlightPriceRQ extractPriceQuoteReq(AirItineraryType airItineraryType, OTAPaxCountTO otaPaxCountTO,
			Collection<String> obRPHList, Collection<String> ibRPHList, CommonReservationAssembler lcclientReservationAssembler,
			QuotedPriceInfoTO quotedPriceInfoTO, Map<Integer, String> defaultBaggageOndMap) throws WebservicesException,
			ModuleException {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		Collection<Integer> flightSegIds = new ArrayList<Integer>();
		Collection<String> obAirports = new ArrayList<String>();
		Date returnDate = null;
		Collection<String> bookingClassSet = new HashSet<String>();

		if (airItineraryType.getOriginDestinationOptions() != null) {
			int segemntSequnce = 1;
			int ondSequence = 0;
			OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
			for (OriginDestinationOptionType ond : airItineraryType.getOriginDestinationOptions().getOriginDestinationOption()) {

				ClassOfServicePreferencesTO preferencesTO = flightPriceRQ.getClassOfServicePrefTO();
				PriceQuoteInterlineUtil.setClassOfServicePreference(preferencesTO, null);

				boolean returnSegments = false;
				for (BookFlightSegmentType flightSegment : ond.getFlightSegment()) {

					if (!flightSegIds.contains(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getRPH()))) {
						flightSegIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getRPH()));

						/** departure date of the first segment of the journey is taken as departure date */
						if (depatureOnD.getDepartureDateTimeStart() == null) {
							depatureOnD.setDepartureDateTimeStart(flightSegment.getDepartureDateTime().toGregorianCalendar()
									.getTime());
						}

						/** first airport is taken as origin */
						if (!obAirports.isEmpty()) {
							depatureOnD.setOrigin(flightSegment.getDepartureAirport().getLocationCode());
						}

						if (airItineraryType.getDirectionInd() == AirTripType.RETURN
								&& !returnSegments
								&& CommonUtil.isReturnSegment(obAirports, flightSegment.getDepartureAirport().getLocationCode(),
										flightSegment.getArrivalAirport().getLocationCode())) {
							returnSegments = true;
						}

						/** last station of the outbound journey is taken as the destination airport */
						if (!returnSegments) {
							depatureOnD.setDestination(flightSegment.getArrivalAirport().getLocationCode());
							obRPHList.add(flightSegment.getRPH());
						} else {
							ibRPHList.add(flightSegment.getRPH());
						}

						/** departure date of the first return segment is taken as the return date */
						if (returnSegments && returnDate == null) {
							returnDate = flightSegment.getDepartureDateTime().toGregorianCalendar().getTime();
						}

						if (!returnSegments) {
							obAirports.add(flightSegment.getDepartureAirport().getLocationCode());
							obAirports.add(flightSegment.getArrivalAirport().getLocationCode());
						}

						/** Adding the Booking classes */
						if (flightSegment.getResBookDesigCode() != null) {
							bookingClassSet.add(flightSegment.getResBookDesigCode());
						}

						String segmentCode = flightSegment.getDepartureAirport().getLocationCode() + "/"
								+ flightSegment.getArrivalAirport().getLocationCode();
						Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getRPH());

						if (!isFlightSegmentFoundInQuotedList(quotedPriceInfoTO.getQuotedSegments(),
								flightSegment.getFlightNumber(), flightSegId)) {
							log.error("ERROR occured in extractPriceQuoteReq. " + "Invalid Inbound/Outbound Segment. " + "["
									+ segmentCode + "]");
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"[Invalid Inbound/Outbound Segment. " + segmentCode + "]");
						}
						// add segment details
						if (!returnSegments) {
							lcclientReservationAssembler.addOutgoingSegment(segemntSequnce++, flightSegId.toString(),
									flightSegment.getRPH(), flightSegment.getFlightNumber(), segmentCode,
									BeanUtils.getFirstElement(preferencesTO.getCabinClassSelection().keySet()),
									BeanUtils.getFirstElement(preferencesTO.getLogicalCabinClassSelection().keySet()),
									flightSegment.getDepartureDateTime().toGregorianCalendar().getTime(), flightSegment
											.getArrivalDateTime().toGregorianCalendar().getTime(), null, flightSegment
											.getDepartureDateTime().toGregorianCalendar().getTime(), flightSegment
											.getArrivalDateTime().toGregorianCalendar().getTime(),
									defaultBaggageOndMap.get(flightSegId), false, null);
						} else {
							lcclientReservationAssembler.addReturnSegment(segemntSequnce++, flightSegId.toString(),
									flightSegment.getRPH(), flightSegment.getFlightNumber(), segmentCode,
									BeanUtils.getFirstElement(preferencesTO.getCabinClassSelection().keySet()),
									BeanUtils.getFirstElement(preferencesTO.getLogicalCabinClassSelection().keySet()),
									flightSegment.getDepartureDateTime().toGregorianCalendar().getTime(), flightSegment
											.getArrivalDateTime().toGregorianCalendar().getTime(), null, flightSegment
											.getDepartureDateTime().toGregorianCalendar().getTime(), flightSegment
											.getArrivalDateTime().toGregorianCalendar().getTime(),
									defaultBaggageOndMap.get(flightSegId), false, null);
						}

					} else {
						// Inconsistent data
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Inconsistent pricing information found");
					}

				}

				Date depatureDate = depatureOnD.getDepartureDateTimeStart();
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);

				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);

				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);

				depatureOnD.getOrignDestinationOptions().add(PriceQuoteInterlineUtil.setFlightSegmentTo(obRPHList, ondSequence));

				if (returnDate != null) {

					// setting the return journey information
					OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
					Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);

					Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
					returnOnD.setPreferredDate(returnDate);
					returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
					returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);

					returnOnD.setOrigin(depatureOnD.getDestination());
					returnOnD.setDestination(depatureOnD.getOrigin());
					returnOnD.setReturnFlag(true);

					returnOnD.getOrignDestinationOptions()
							.add(PriceQuoteInterlineUtil.setFlightSegmentTo(ibRPHList, ondSequence));
				}

				if (airItineraryType.getDirectionInd() == AirTripType.RETURN) {
					flightPriceRQ.getAvailPreferences().setHalfReturnFareQuote(
							AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
				}

				SegmentCarrierDTO segmentCarrierDTO = new SegmentCarrierDTO();

				// Uncomment Below code when WS supports for Interline

				// if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
				// segmentCarrierDTO = CarrierCacheUtil.getInstance().getCarrierDetails(depatureOnD.getOrigin(),
				// depatureOnD.getDestination());
				// } else {
				segmentCarrierDTO.setSystem(SYSTEM.AA);
				// }

				if (segmentCarrierDTO != null) {
					flightPriceRQ.getAvailPreferences().setSearchSystem(segmentCarrierDTO.getSystem());
				}

				if (otaPaxCountTO.getAdultCount() == 0 && otaPaxCountTO.getChildCount() == 0) {
					log.error("ERROR detected in extractPriceQuoteReq. " + "Invalid adult/children pax count. " + "[adults pax="
							+ otaPaxCountTO.getAdultCount() + "]" + "[children pax=" + otaPaxCountTO.getChildCount() + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
							"Adult and Children pax count cannot be zero");
				}

				TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

				PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
				adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
				adultsQuantity.setQuantity(otaPaxCountTO.getAdultCount());

				PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
				childQuantity.setPassengerType(PaxTypeTO.CHILD);
				childQuantity.setQuantity(otaPaxCountTO.getChildCount());

				PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
				infantQuantity.setPassengerType(PaxTypeTO.INFANT);
				infantQuantity.setQuantity(otaPaxCountTO.getInfantCount());

				AvailPreferencesTO availPreferencesTO = flightPriceRQ.getAvailPreferences();
				UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
				availPreferencesTO.setTravelAgentCode(principal.getAgentCode());
				availPreferencesTO.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

				if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) {
					log.error("ERROR occured in extractPriceQuoteReq. "
							+ "Invalid no of booking class. Booking class shoud be same for all Flight Segments Per Price Quote"
							+ "[booking class given=" + bookingClassSet.size() + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
							"Too many cabin classes specifyed .Only one is supported per availability search");
				}

				TravelPreferencesTO travelPreferencesTO = flightPriceRQ.getTravelPreferences();
				PriceQuoteInterlineUtil.setTravelPreference(bookingClassSet, travelPreferencesTO);

			}
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_ITINERARY_ELEMENT_DOES_NOT_EXIST_,
					"AirItinerary information not found");
		}
		return flightPriceRQ;
	}

	/**
	 * 
	 * @param quotedFlightSegments
	 * @param flightNumber
	 * @param flightSegId
	 * @return
	 */
	private boolean isFlightSegmentFoundInQuotedList(List<FlightSegmentTO> quotedFlightSegments, String flightNumber,
			Integer flightSegId) {

		if (quotedFlightSegments != null && !quotedFlightSegments.isEmpty()) {
			for (FlightSegmentTO quoSegmentTO : quotedFlightSegments) {
				if (quoSegmentTO.getFlightNumber().equals(flightNumber) && quoSegmentTO.getFlightSegId().equals(flightSegId)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Retrieves the {@link OndFareDTO} collection related to the price quote associated with the reservation.
	 * 
	 * @param availableFlightSearchDTOs
	 *            : Availability search information
	 * @return : Re-created {@link OndFareDTO} collection.
	 * 
	 * @throws ModuleException
	 *             : If an error occurs in the {@link OndFareDTO} re-creation process.
	 * @throws WebservicesException
	 *             : If the {@link FareSegChargeTO} is not available in the current Tnx context.
	 */
	public Collection<OndFareDTO> getSelectedSegsFaresSeatsColForBookingFromFareSegChargeTO(
			Collection<AvailableFlightSearchDTO> availableFlightSearchDTOs) throws ModuleException, WebservicesException {

		Collection<OndFareDTO> ondFareDTOCollection = new ArrayList<OndFareDTO>();

		for (AvailableFlightSearchDTO availableFlightSearchDTO : availableFlightSearchDTOs) {
			FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
					.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);

			if (fareSegChargeTO == null) {
				// The user has to do a price quote before a booking stage.
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_CANNOT_TICKET_UNTIL_BOOKING_PRICED_,
						"Please perform a price quote before trying to do a reservation stage operation");
			}

			Collection<OndFareSegChargeTO> fares = fareSegChargeTO.getOndFareSegChargeTOs();

			Collection<Integer> allSegments = new HashSet<Integer>();

			for (OriginDestinationInfoDTO ond : availableFlightSearchDTO.getOriginDestinationInfoDTOs()) {
				allSegments.addAll(ond.getFlightSegmentIds());
			}

			Collection<Integer> priceQuotedSegments = new HashSet<Integer>();
			for (OndFareSegChargeTO ondFareSegChargeTO : fares) {
				priceQuotedSegments.addAll(ondFareSegChargeTO.getFsegBCAlloc().keySet());
			}

			// Should confirm with newly introduced modification scenarios-
			if (allSegments.size() != priceQuotedSegments.size()) {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_PRICE_QUOTE_AND_BOOKING_SEGMENTS_ARE_NOT_MATCHING_,
						"Selected flight information is different from the last performed price quote. Please use "
								+ "last performed price quote for booking");
			} else {
				for (Integer segId : allSegments) {
					if (!priceQuotedSegments.contains(segId)) {
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_PRICE_QUOTE_AND_BOOKING_SEGMENTS_ARE_NOT_MATCHING_,
								"Selected flight information is different from the last performed price quote. Please use "
										+ "last performed price quote for booking");
					}
				}
			}

			boolean isInboudFlexiSelected = false;
			boolean isOutBoudFlexiSelected = false;

			Object inboundFlexi = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INBOUND_FLEXI_SELECTED);
			if (inboundFlexi != null) {
				isInboudFlexiSelected = (Boolean) inboundFlexi;
			}

			Object outboundFlexi = ThreadLocalData.getCurrentTnxParam(Transaction.RES_OUTBOUND_FLEXI_SELECTED);
			if (outboundFlexi != null) {
				isOutBoudFlexiSelected = (Boolean) outboundFlexi;
			}

			IPaxCountAssembler paxAssm = PriceQuoteUtil.getPaxCountAssembler(availableFlightSearchDTO);

			OndRebuildCriteria rebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(paxAssm, fareSegChargeTO,
					availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getPreferredBookingType(), false);

			Collection<OndFareDTO> collFares = WebServicesModuleUtils.getReservationQueryBD().recreateFareSegCharges(
					rebuildCriteria);
			ondFareDTOCollection.addAll(collFares);
			ondFareDTOs.addAll(collFares);

		}

		return ondFareDTOCollection;
	}

	/**
	 * 
	 * @param flightPriceRQ
	 * @param commonReservationAssembler
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void getFareSegChargeFromQuotedIno(FlightPriceRQ flightPriceRQ, CommonReservationAssembler commonReservationAssembler)
			throws ModuleException, WebservicesException {

		FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);

		if (fareSegChargeTO == null) {
			// The user has to do a price quote before a booking stage.
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_CANNOT_TICKET_UNTIL_BOOKING_PRICED_,
					"Please perform a price quote before trying to do a reservation stage operation");
		}

		commonReservationAssembler.setSelectedFareSegChargeTO(fareSegChargeTO);

		Collection<Integer> allSegmentIds = new HashSet<Integer>();

		List<OriginDestinationInformationTO> ondInfoList = flightPriceRQ.getOriginDestinationInformationList();

		for (OriginDestinationInformationTO informationTO : ondInfoList) {
			for (OriginDestinationOptionTO optionTO : informationTO.getOrignDestinationOptions()) {
				for (FlightSegmentTO segmentTO : optionTO.getFlightSegmentList()) {
					allSegmentIds.add(segmentTO.getFlightSegId());
				}
			}
		}

		Collection<OndFareSegChargeTO> fares = fareSegChargeTO.getOndFareSegChargeTOs();

		Collection<Integer> priceQuotedSegments = new HashSet<Integer>();
		for (OndFareSegChargeTO ondFareSegChargeTO : fares) {
			priceQuotedSegments.addAll(ondFareSegChargeTO.getFsegBCAlloc().keySet());
		}

		// Should confirm with newly introduced modification scenarios-
		if (allSegmentIds.size() != priceQuotedSegments.size()) {
			throw new WebservicesException(
					IOTACodeTables.AccelaeroErrorCodes_AAE_PRICE_QUOTE_AND_BOOKING_SEGMENTS_ARE_NOT_MATCHING_,
					"Selected flight information is different from the last performed price quote. Please use "
							+ "last performed price quote for booking");
		} else {
			for (Integer segId : allSegmentIds) {
				if (!priceQuotedSegments.contains(segId)) {
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_PRICE_QUOTE_AND_BOOKING_SEGMENTS_ARE_NOT_MATCHING_,
							"Selected flight information is different from the last performed price quote. Please use "
									+ "last performed price quote for booking");
				}
			}
		}

	}

	/**
	 * Returns the total fair by calculating the price breakdown
	 * 
	 * @param pax
	 *            {adult,infant,children}
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getTotalAmountFromPriceBreakdown(int[] pax, String currencyCode, Collection<OndFareDTO> ondFareDTOs,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargeMap, ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		Collection<String> paxTypes = new ArrayList<String>();
		if (pax[0] > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		if (pax[2] > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		if (pax[1] > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));

		BigDecimal totalFair = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (String paxType : paxTypes) {
			BigDecimal totalFarePerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal baseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
			int paxCount = 0;
			for (OndFareDTO ondFareDTO : ondFareDTOs) {

				if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					baseFare = AccelAeroCalculator.add(
							baseFare,
							WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getFareSummaryDTO().getFareAmount(
											PaxTypeTO.ADULT)), currencyCode, exchangeRateProxy));

					totalTax = populatePaxTaxSurcharge(ChargeGroups.TAX, PaxTypeTO.ADULT, ondFareDTO, totalTax, currencyCode,
							exchangeRateProxy);
					totalCharges = populatePaxTaxSurcharge(ChargeGroups.SURCHARGE, PaxTypeTO.ADULT, ondFareDTO, totalCharges,
							currencyCode, exchangeRateProxy);
					paxCount = pax[0];

				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					baseFare = AccelAeroCalculator.add(
							baseFare,
							WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getFareSummaryDTO().getFareAmount(
											PaxTypeTO.CHILD)), currencyCode, exchangeRateProxy));

					totalTax = populatePaxTaxSurcharge(ChargeGroups.TAX, PaxTypeTO.CHILD, ondFareDTO, totalTax, currencyCode,
							exchangeRateProxy);
					totalCharges = populatePaxTaxSurcharge(ChargeGroups.SURCHARGE, PaxTypeTO.CHILD, ondFareDTO, totalCharges,
							currencyCode, exchangeRateProxy);
					paxCount = pax[2];

				} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					baseFare = AccelAeroCalculator.add(
							baseFare,
							WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(ondFareDTO.getFareSummaryDTO().getFareAmount(
											PaxTypeTO.INFANT)), currencyCode, exchangeRateProxy));

					totalTax = populatePaxTaxSurcharge(ChargeGroups.TAX, PaxTypeTO.INFANT, ondFareDTO, totalTax, currencyCode,
							exchangeRateProxy);
					totalCharges = populatePaxTaxSurcharge(ChargeGroups.SURCHARGE, PaxTypeTO.INFANT, ondFareDTO, totalCharges,
							currencyCode, exchangeRateProxy);
					BigDecimal infantSurcharge = AccelAeroCalculator.parseBigDecimal(ondFareDTO
							.getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1]);
					totalCharges = AccelAeroCalculator.add(totalCharges,
							WSReservationUtil.getAmountInSpecifiedCurrency(infantSurcharge, currencyCode, exchangeRateProxy));

					paxCount = pax[1];
				}

				// Total per pax from converted amount.
				totalFarePerPax = AccelAeroCalculator.add(baseFare, totalTax, totalCharges);
			}

			totalFair = AccelAeroCalculator.add(totalFair, AccelAeroCalculator.multiply(totalFarePerPax, paxCount));
		}
		// adding handler fee to the total cost.
		BigDecimal handlingFee = (externalChargeMap.containsKey(EXTERNAL_CHARGES.HANDLING_CHARGE) ? externalChargeMap.get(
				EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount() : BigDecimal.ZERO);
		handlingFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFee);

		totalFair = AccelAeroCalculator.add(totalFair,
				WSReservationUtil.getAmountInSpecifiedCurrency(handlingFee, currencyCode, exchangeRateProxy));
		return totalFair;
	}

	/**
	 * 
	 * @param pax
	 * @param currencyCode
	 * @param quotedPriceInfoTO
	 * @param externalChargeMap
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getTotalAmountFromPriceBreakdown(OTAPaxCountTO otaPaxCountTO, String currencyCode,
			QuotedPriceInfoTO quotedPriceInfoTO, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargeMap,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		Collection<String> paxTypes = new ArrayList<String>();
		if (otaPaxCountTO.getAdultCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		}

		if (otaPaxCountTO.getChildCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		}

		if (otaPaxCountTO.getInfantCount() > 0) {
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		}

		BigDecimal totalFair = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (String paxType : paxTypes) {
			BigDecimal totalFarePerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal baseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
			int paxCount = 0;
			PerPaxChargesTO paxChargesTO = null;

			paxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(paxType);

			if (paxChargesTO != null) {

				baseFare = AccelAeroCalculator.add(baseFare, WSReservationUtil.getAmountInSpecifiedCurrency(
						paxChargesTO.getTotalBaseFare(), currencyCode, exchangeRateProxy));
				if (OTAUtils.isShowPriceInselectedCurrency()) {
					totalCharges = paxChargesTO.getTotalChargesInSelectedCur();
				} else {
					totalCharges = WSReservationUtil.getAmountInSpecifiedCurrency(paxChargesTO.getTotalCharges(), currencyCode,
							exchangeRateProxy);
				}
				paxCount = otaPaxCountTO.getPaxCountByOTAPaxType(paxType);

				// //IF INFANT DO THE FOLLWOING (CHECK IF SURCHARGES DOES NOT INCLUDE INFANT CHARGE)
				// BigDecimal infantSurcharge = AccelAeroCalculator.parseBigDecimal(ondFareDTO
				// .getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1]);
				// totalCharges = AccelAeroCalculator.add(totalCharges,
				// WSReservationUtil.getAmountInSpecifiedCurrency(infantSurcharge, currencyCode));
			}

			// Total per pax from converted amount.
			totalFarePerPax = AccelAeroCalculator.add(baseFare, totalCharges);

			totalFair = AccelAeroCalculator.add(totalFair, AccelAeroCalculator.multiply(totalFarePerPax, paxCount));
		}
		// adding handler fee to the total cost.
		BigDecimal handlingFee = (externalChargeMap.containsKey(EXTERNAL_CHARGES.HANDLING_CHARGE) ? externalChargeMap.get(
				EXTERNAL_CHARGES.HANDLING_CHARGE).getAmount() : BigDecimal.ZERO);
		handlingFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFee);

		totalFair = AccelAeroCalculator.add(totalFair,
				WSReservationUtil.getAmountInSpecifiedCurrency(handlingFee, currencyCode, exchangeRateProxy));
		return totalFair;
	}

	private BigDecimal populatePaxTaxSurcharge(String ChargeGroup, String paxType, OndFareDTO ondFareDTO,
			BigDecimal currentTotal, String currencyCode, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		BigDecimal total = currentTotal;
		if (ondFareDTO.getAllCharges() != null) {
			for (QuotedChargeDTO quotedChargeDTO : ondFareDTO.getAllCharges()) {
				if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroup) == 0) {
					if (paxType.compareTo(PaxTypeTO.ADULT) == 0) {
						if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
							total = AccelAeroCalculator.add(total, WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType)),
									currencyCode, exchangeRateProxy));
						}
					} else if (paxType.compareTo(PaxTypeTO.CHILD) == 0) {
						if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
							total = AccelAeroCalculator.add(total, WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType)),
									currencyCode, exchangeRateProxy));
						}
					} else if (paxType.compareTo(PaxTypeTO.INFANT) == 0) {
						if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
							total = AccelAeroCalculator.add(total, WSReservationUtil.getAmountInSpecifiedCurrency(
									AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType)),
									currencyCode, exchangeRateProxy));
						}
					}
				}
			}
		}
		return total;
	}

	// /**
	// * Returns the total fair by calculating the price breakdown
	// *
	// * @param pax
	// * {adult,infant,children}
	// * @return
	// * @throws ModuleException
	// */
	// public BigDecimal getTotalAmountFromPriceBreakdown(int[] pax, String
	// currencyCode) throws ModuleException {
	//
	// Collection<String> paxTypes = new ArrayList<String>();
	// if (pax[0] > 0)
	// paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
	// if (pax[2] > 0)
	// paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
	// if (pax[1] > 0)
	// paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
	//
	// BigDecimal totalFair = AccelAeroCalculator.getDefaultBigDecimalZero();
	// for (String paxType : paxTypes) {
	// BigDecimal totalFairPerPax =
	// AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal baseFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
	// BigDecimal totalChargers =
	// AccelAeroCalculator.getDefaultBigDecimalZero();
	// int paxCount = 0;
	// for (SelectedFlightDTO selectedFlightDTO : getSelectedFlightDTOs()) {
	// String chargeQuotePaxType = null;
	//
	// Collection<String> taxChargeGroupCodes = new ArrayList<String>();
	// taxChargeGroupCodes.add(ChargeGroups.TAX);
	//
	// Collection<String> surchargeChargeGroupCodes = new ArrayList<String>();
	// surchargeChargeGroupCodes.add(ChargeGroups.SURCHARGE);
	//
	// if
	// (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT)))
	// {
	// baseFare = AccelAeroCalculator.add(baseFare,
	// AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.ADULT,
	// isFixedFare)));
	// chargeQuotePaxType = PaxTypeTO.ADULT;
	// paxCount = pax[0];
	//
	// } else if
	// (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD)))
	// {
	// baseFare = AccelAeroCalculator.add(baseFare,
	// AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.CHILD,
	// isFixedFare)));
	// chargeQuotePaxType = PaxTypeTO.CHILD;
	// paxCount = pax[2];
	//
	// } else if
	// (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT)))
	// {
	// baseFare = AccelAeroCalculator.add(baseFare,
	// AccelAeroCalculator.parseBigDecimal(selectedFlightDTO.getFareAmount(PaxTypeTO.INFANT,
	// isFixedFare)));
	// surchargeChargeGroupCodes.add(ChargeGroups.INFANT_SURCHARGE);
	// chargeQuotePaxType = PaxTypeTO.INFANT;
	// paxCount = pax[1];
	// }
	// baseFare = WSReservationUtil.getAmountInSpecifiedCurrency(baseFare,
	// currencyCode);
	//
	// Collection<String> chargeQuotePaxTypes = new ArrayList<String>();
	// chargeQuotePaxTypes.add(chargeQuotePaxType);
	//
	// Collection<QuotedChargeDTO> aaTaxes =
	// selectedFlightDTO.getUnifiedCharges(taxChargeGroupCodes,
	// chargeQuotePaxTypes, isFixedFare);
	// if (aaTaxes != null && aaTaxes.size() > 0) {
	// for (QuotedChargeDTO tax : aaTaxes) {
	// totalTax = AccelAeroCalculator.add(totalTax,
	// WSReservationUtil.getAmountInSpecifiedCurrency(
	// AccelAeroCalculator.parseBigDecimal(tax.getEffectiveChargeAmount(chargeQuotePaxType)),
	// currencyCode));
	// }
	// }
	//
	// Collection<QuotedChargeDTO> aaSurcharges =
	// selectedFlightDTO.getUnifiedCharges(surchargeChargeGroupCodes,
	// chargeQuotePaxTypes, isFixedFare);
	//
	// if (aaSurcharges != null && aaSurcharges.size() > 0) {
	// for (QuotedChargeDTO surcharge : aaSurcharges) {
	// totalChargers = AccelAeroCalculator.add(totalChargers,
	// WSReservationUtil.getAmountInSpecifiedCurrency(
	// AccelAeroCalculator.parseBigDecimal(surcharge.getEffectiveChargeAmount(chargeQuotePaxType)),
	// currencyCode));
	// }
	// }
	// totalFairPerPax = AccelAeroCalculator.add(baseFare, totalTax,
	// totalChargers);
	// }
	//
	// totalFair = AccelAeroCalculator.add(totalFair,
	// AccelAeroCalculator.multiply(totalFairPerPax, paxCount));
	// }
	// // adding handler fee to the total cost.
	// BigDecimal handleFare = ReservationUtil.getHangleCharges().getAmount();
	// totalFair = AccelAeroCalculator.add(totalFair,
	// WSReservationUtil.getAmountInSpecifiedCurrency(handleFare,
	// currencyCode));
	// return totalFair;
	// }

	/**
	 * THis traversal the AirItinerary and create a map of <pricekey,bookFligthSegments>
	 * 
	 * @param airItinerary
	 * @return the map
	 * @throws WebservicesException
	 */
	private Map<String, Collection<BookFlightSegmentType>> getPriceKeyFSMap(AirItineraryType airItinerary)
			throws WebservicesException {

		Map<String, Collection<BookFlightSegmentType>> priceMap = new HashMap<String, Collection<BookFlightSegmentType>>();

		String currentOnDKey = null;
		String previousOnDKey = null;
		boolean isFirstSegOfOnd = true;

		if (airItinerary.getOriginDestinationOptions() != null) {
			for (OriginDestinationOptionType ond : airItinerary.getOriginDestinationOptions().getOriginDestinationOption()) {
				isFirstSegOfOnd = true;
				for (BookFlightSegmentType flightSegment : ond.getFlightSegment()) {

					Collection<String> carrierCodes = WebServicesModuleUtils.getReservationBD().getCarrierCodesByAirline(
							AppSysParamsUtil.getDefaultAirlineIdentifierCode());

					if (!AppSysParamsUtil.getDefaultCarrierCode().equals(
							AppSysParamsUtil.extractCarrierCode(flightSegment.getFlightNumber()))
							&& (!carrierCodes.contains(AppSysParamsUtil.extractCarrierCode(flightSegment.getFlightNumber())))) {
						continue; // External segment from interline bookings
									// transfer
					}

					currentOnDKey = flightSegment.getRPH().substring(flightSegment.getRPH().indexOf("-") + 1);
					if (isFirstSegOfOnd) {
						if (!priceMap.keySet().contains(currentOnDKey)) {
							previousOnDKey = currentOnDKey;
							// adding a new key and a value to the map
							Collection<BookFlightSegmentType> bookFlightSegmentTypeList = new ArrayList<BookFlightSegmentType>();
							bookFlightSegmentTypeList.add(flightSegment);
							priceMap.put(currentOnDKey, bookFlightSegmentTypeList);
						} else {
							if (previousOnDKey != null && !previousOnDKey.equals(currentOnDKey)) {
								// Inconsistent data
								throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
										"Inconsistent pricing information found");
							} else {
								// adding a BookFlightSegmentType to existing
								// priceKey
								Collection<BookFlightSegmentType> bookFlightSegmentTypeList = priceMap.get(currentOnDKey);
								bookFlightSegmentTypeList.add(flightSegment);
							}
						}
						isFirstSegOfOnd = false;
					} else {
						if (!priceMap.keySet().contains(currentOnDKey)) {
							// Inconsistent data
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Inconsistent pricing information found");
						} else {
							// adding a BookFlightSegmentType to existing
							// priceKey
							Collection<BookFlightSegmentType> bookFlightSegmentTypeList = priceMap.get(currentOnDKey);
							bookFlightSegmentTypeList.add(flightSegment);
						}
					}
				}
			}
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_ITINERARY_ELEMENT_DOES_NOT_EXIST_,
					"AirItinerary information not found");
		}
		return priceMap;
	}

}
