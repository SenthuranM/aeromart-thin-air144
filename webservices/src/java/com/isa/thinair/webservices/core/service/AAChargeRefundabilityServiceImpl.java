package com.isa.thinair.webservices.core.service;

import java.util.List;
import java.util.Map;

import javax.interceptor.AroundInvoke;
import javax.security.auth.login.LoginException;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.thinair.commons.api.dto.ets.AeroMartChargeRefundabilityRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartChargeRefundabilityRs;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.ServiceError;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.login.Constants;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.webservices.api.service.AAChargeRefundabilityService;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

@Controller
@RequestMapping(value = "aeromart/ticket", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class AAChargeRefundabilityServiceImpl implements AAChargeRefundabilityService {

	@Override
	@ResponseBody
	@RequestMapping(value = "/chargeRefundability")
	public AeroMartChargeRefundabilityRs getChargeWiseRefundability(@RequestBody AeroMartChargeRefundabilityRq chargeRefundabilityRq) {

		AeroMartChargeRefundabilityRs chargeRefundabilityRs = null;
		List<String> chargeCodes = chargeRefundabilityRq.getChargeCodeList();
		
		if (chargeCodes != null && !chargeCodes.isEmpty()) {

			Map<String, Boolean> chargecodeRefundabiltyMap;
			try {
				
				ForceLoginInvoker.webserviceLogin(getWebServicesConfig().getEjbAccessUsername(),
						getWebServicesConfig().getEjbAccessPassword(), SalesChannelsUtil.SALES_CHANNEL_GDS);
				
				chargecodeRefundabiltyMap = getRefundableCharges(chargeCodes);
				chargeRefundabilityRs = prepareResponse(chargecodeRefundabiltyMap);

			} catch (ModuleException e) {
				
				chargeRefundabilityRs = new AeroMartChargeRefundabilityRs();
				chargeRefundabilityRs.setSuccess(false);
				ServiceError serviceError = new ServiceError();
				serviceError.setCode(e.getExceptionCode());
				serviceError.setMessage(e.getMessage());
				chargeRefundabilityRs.setServiceError(serviceError);
				
			} catch (LoginException e) {
				
				chargeRefundabilityRs = new AeroMartChargeRefundabilityRs();
				chargeRefundabilityRs.setSuccess(false);
				ServiceError serviceError = new ServiceError();
				serviceError.setCode("Authorization error");
				serviceError.setMessage("Authorization error");
				chargeRefundabilityRs.setServiceError(serviceError);
			}

		} else {

			chargeRefundabilityRs = new AeroMartChargeRefundabilityRs();
			chargeRefundabilityRs.setSuccess(true);
			ServiceError serviceError = new ServiceError();
			serviceError.setCode("null or empty request");
			serviceError.setMessage("null or empty request");
			chargeRefundabilityRs.setServiceError(serviceError);

		}

		return chargeRefundabilityRs;
	}

	private Map<String, Boolean> getRefundableCharges(List<String> chargeCodes) throws ModuleException {

		return WebServicesModuleUtils.getChargeBD().getChargeCodeWiseRefundability(chargeCodes);
		
	}

	private AeroMartChargeRefundabilityRs prepareResponse(Map<String, Boolean> chargecodeRefundabiltyMap) {

		AeroMartChargeRefundabilityRs chargeRefundabilityRs = new AeroMartChargeRefundabilityRs();
		chargeRefundabilityRs.setSuccess(true);
		chargeRefundabilityRs.setServiceError(null);
		chargeRefundabilityRs.setChargecodeRefundabiltyMap(chargecodeRefundabiltyMap);
		return chargeRefundabilityRs;
	}

	public static WebservicesConfig getWebServicesConfig() {
		return (WebservicesConfig) LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/webservices?id=webservicesModuleConfig");
	}
	
}
