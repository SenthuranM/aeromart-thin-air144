package com.isa.thinair.webservices.core.cache.store.impl;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.springframework.cache.Cache;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.store.AbstractStore;
import com.isa.thinair.webservices.core.cache.store.SessionStore;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class RedisSessionStore extends AbstractStore implements SessionStore {

	private static final Log log = LogFactory.getLog(RedisSessionStore.class);

	@Override
	public AAUserSession createNewUserSession(String userID, String password, int salesChannelId) throws WebservicesException {
		AAUserSession aaUserSession = new AAUserSession();
		Collection privilegeKeys = new HashSet();
		try {
			/** Initialize User Data */
			User user = WebServicesModuleUtils.getSecurityBD().getUser(userID);
			Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(user.getAgentCode());

			// Calling from login module
			Collection<UserDST> colUserDST = DatabaseUtil.getAgentLocalTime(agent.getStationCode());
			Collection userCarriers = DatabaseUtil.getCarrierCodes(userID);
			// Setting salesChannel agents and loginType to -1
			UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity(userID, salesChannelId,
					user.getAgentCode(), agent.getStationCode(), null, userID, colUserDST, password, userCarriers,
					user.getDefaultCarrierCode(), user.getAirlineCode(), agent.getAgentTypeCode(), agent.getCurrencyCode(), null,
					null, null, null);

			// Storing data in user session
			aaUserSession.addParameter(AAUserSession.USER_PRINCIPAL, userPrincipal);
			aaUserSession.addParameter(AAUserSession.USER, user);
			aaUserSession.addParameter(AAUserSession.USER_AGENT, agent);
			if (user != null) {
				privilegeKeys = user.getPrivitedgeIDs();
			}
			aaUserSession.addParameter(AAUserSession.USER_PRIVILEGES_KEYS, privilegeKeys);

			try {
				Cache cache = getCache();
				cache.put(userID, aaUserSession);
			} catch (Exception e) {
				log.error("REDIS Error on createNewUserSession for the user Id:" + userID, e);
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE, CacheConstant.CACHE_SERVICE_ERROR);
			}

		} catch (ModuleException me) {
			log.error("User session initialization failed", me);
			throw new WebservicesException(me);
		}
		log.debug("AASessionManager:: created new user session [userId=" + userID + "]");
		return aaUserSession;
	}

	@Override
	public AAUserSession getUserSession(String userID) throws WebservicesException {
		AAUserSession userSession = null;

		try {
			Cache cache = getCache();
			if (cache.get(userID) != null) {
				userSession = (AAUserSession) cache.get(userID).get();
			}
		} catch (Exception e) {
			log.error("REDIS Error on getUserSession for the user Id:" + userID, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE, CacheConstant.CACHE_SERVICE_ERROR);
		}
		return userSession;
	}

	@Override
	public void removeExpiredUserSessions() {
		// This is delegated to Redis TTL
	}

	/**
	 * Used to flush the local AAUserSession to remote store
	 * 
	 * Must flush the current thread local AAUserSession to remote to keep it sync
	 * 
	 * This Should done before unsetting the threadlocal AAUserSession
	 * 
	 * @throws WebservicesException
	 */
	@Override
	public void flushUserSession(AAUserSession aaUserSession) throws WebservicesException {
		String userId = null;

		if (aaUserSession != null) {
			User user = (User) aaUserSession.getParameter(AAUserSession.USER);
			if (user != null) {
				userId = user.getUserId();

				try {
					Cache cache = getCache();
					cache.put(userId, aaUserSession);
				} catch (Exception e) {
					log.error("REDIS Error on flushUserSession for the user Id:" + userId, e);
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PROCESSING_ISSUE,
							CacheConstant.CACHE_SERVICE_ERROR);
				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("REDIS Session is synced to the Redis store. user ID" + userId);
		}

	}

}
