package com.isa.thinair.webservices.core.cache.delegator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.WarningsType;
import org.springframework.cache.Cache;

import com.isa.thinair.webservices.core.cache.delegator.auditor.impl.WSAnalyticsAuditor;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.ThruReflection;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class CacheTemplateInvoker<T, V> {

	private final static Log log = LogFactory.getLog(CacheTemplateInvoker.class);

	public static <T, V> T invoke(V source, String beanName, CacheConstant.ReflectionMethodName reflectionMethodName) {

		long startTime = System.currentTimeMillis();
		boolean servedFromCache = false;
		boolean success = false;

		StatelessCacheTemplate<T, V> statelessCachedTemplate = WebServicesModuleUtils.getBeanByName(beanName);
		String sourceAuditStr = statelessCachedTemplate.getAudit(source);
		T otaResponse = null;
		Exception exception = null;

		try {

			ExceptionUtil.triggerPreProcessErrorIfPresent();

			String cacheKey = generateCacheStoreKey(source, statelessCachedTemplate);
			String cacheName = statelessCachedTemplate.getCacheName();
			Cache cache = statelessCachedTemplate.getCacheManager().getCache(cacheName);
			boolean isCacheEnabled = statelessCachedTemplate.isEnabledCache();
			CacheResponse<T> cacheStoreResponse = null;

			if (cacheKey != null && isCacheEnabled) {
				cacheStoreResponse = getCacheResponseFromCacheStore(cacheKey, cache);
			}

			if (cacheStoreResponse == null) {
				if (statelessCachedTemplate.getThrottleManager().isAllowed()) {
					otaResponse = (T) statelessCachedTemplate.execute(source);
					storeDataInCacheSever(otaResponse, cacheKey, cache, statelessCachedTemplate);
				}

			} else {
				servedFromCache = true;
				otaResponse = (T) statelessCachedTemplate.cachePostProcess(cacheStoreResponse, source);
			}

			success = true;

		} catch (Exception e) {
			exception = e;
			otaResponse = (T) statelessCachedTemplate.emptyResponse();
			otaResponse = composeException(otaResponse, e, reflectionMethodName);
		} finally {

			WSAnalyticsAuditor.audit(success, startTime, servedFromCache, sourceAuditStr, exception);
		}

		return otaResponse;
	}

	private static <T, V> void storeDataInCacheSever(T otaResponse, String cacheKey, Cache cache,
			StatelessCacheTemplate<T, V> statelessCachedTemplate) {

		if (cacheKey != null && statelessCachedTemplate.isEnabledCache()) {
			try {
				T cacheStoreResponse = statelessCachedTemplate.composeCacheStoreResponse(otaResponse);
				cache.put(cacheKey, cacheStoreResponse);
			} catch (Exception exception) {
				log.error("Error in updateCache process : ", exception);
			}
		}
	}

	private static <T, V> CacheResponse<T> getCacheResponseFromCacheStore(String cacheKey, Cache cache) {
		CacheResponse<T> response = null;
		try {
			response = cache.get(cacheKey) == null ? null : (CacheResponse<T>) cache.get(cacheKey).get();
		} catch (Exception ex) {
			log.error("Error in cache provider : ", ex);
		}
		return response;
	}

	private static <T, V> String generateCacheStoreKey(V source, StatelessCacheTemplate<T, V> statelessCachedTemplate) {
		String cacheKey = null;
		if (statelessCachedTemplate.isEnabledCache()) {
			try {
				cacheKey = statelessCachedTemplate.generateCacheStoreKey(source);
			} catch (Exception exception) {
				log.info("Cannot generate key:", exception);
			}
		}
		return cacheKey;
	}

	@SuppressWarnings("rawtypes")
	private static <T, V> T composeException(T response, Exception exception, CacheConstant.ReflectionMethodName reflectionMethodName) {

		log.error("Processing error", exception);
		ExceptionUtil.setTriggerPostProcessError(exception);

		ErrorsType errorsType = new ErrorsType();
		List successAndWarnings = new ArrayList<>();

		try {

			errorsType = (ErrorsType) ThruReflection.getAttributeValue(response, CacheConstant.ReflectionMethodName.ERRORS.getName());

			if (reflectionMethodName == CacheConstant.ReflectionMethodName.WARNINGS) {
				WarningsType warningsType = (WarningsType) ThruReflection.getAttributeValue(response,
						reflectionMethodName.getName());
				successAndWarnings = warningsType.getWarning();
			} else {
				successAndWarnings = (List) ThruReflection.getAttributeValue(response, reflectionMethodName.getName());
			}

		} catch (Exception e) {
			log.error("Reflection failed.", e);
		}

		ExceptionUtil.addOTAErrrorsAndWarnings(errorsType.getError(), successAndWarnings, exception);
		return response;
	}

}
