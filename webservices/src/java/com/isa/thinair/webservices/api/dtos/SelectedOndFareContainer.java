package com.isa.thinair.webservices.api.dtos;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class SelectedOndFareContainer {
	private Collection<SelectedOndFareDTO> selectedOndFareDTOs = new ArrayList<SelectedOndFareDTO>();
	private Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();

	public void addSelectedOndFareDTO(SelectedOndFareDTO selectedOndFareDTO) {
		ondFareDTOs.addAll(selectedOndFareDTO.getFareDTOs());
		selectedOndFareDTOs.add(selectedOndFareDTO);
	}

	public Collection<SelectedOndFareDTO> getSelectedOndFareDTOs() {
		return selectedOndFareDTOs;
	}

	public Collection<OndFareDTO> getOndFareDTOs() {
		return ondFareDTOs;
	}

}
