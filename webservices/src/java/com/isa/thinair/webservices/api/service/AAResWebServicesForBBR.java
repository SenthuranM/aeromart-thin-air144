/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRS;
import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRS;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.opentravel.ota._2003._05.OTAReadRQ;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

/**
 * @author Mohamed Nasly
 * 
 *         Web Service(s) for core reservation services
 */

@WebService(name = "AAResWebServicesForBBR", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForBBR")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAResWebServicesForBBR {

	/**
	 * OTA Ping service. Clients can use this service for health check.
	 * 
	 * @param oTAPingRQ
	 * @return OTAPingRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "ping", action = "ping")
	@WebResult(name = "OTA_PingRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAPingRS ping(
			@WebParam(name = "OTA_PingRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAPingRQ oTAPingRQ);

	/**
	 * Flight availability for a given airport pair, for a specific date, for a given number and type of passengers.
	 * 
	 * Clients should call the service with empty transaction when availability search is done for a new booking and
	 * should call with previous transaction when doing availability search for add/modify segment.
	 * 
	 * @param oTAAirAvailRQ
	 * @return OTAAirAvailRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.STARTORMIDDLE)
	@WebMethod(operationName = "getAvailability", action = "getAvailability")
	@WebResult(name = "OTA_AirAvailRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirAvailRS
			getAvailability(
					@WebParam(name = "OTA_AirAvailRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirAvailRQ oTAAirAvailRQ);

	/**
	 * Requests pricing information for specific flights on certain dates for a specific number and type of passengers.
	 * Clients should call the service specifying the previous transaction.
	 * 
	 * @param otaAirPriceRQ
	 * @return OTAAirPriceRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "getPrice", action = "getPrice")
	@WebResult(name = "OTA_AirPriceRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirPriceRS
			getPrice(
					@WebParam(name = "OTA_AirPriceRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirPriceRQ otaAirPriceRQ);

	/**
	 * Book a specific itinerary on specified booking class (and fare) for one or more passengers with or without
	 * payment specified. Client must call this service with the transaction initiated by availability search/price
	 * quote.
	 * 
	 * @param otaAirBookRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "book", action = "book")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			book(@WebParam(name = "OTA_AirBookRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookRQ otaAirBookRQ,
					@WebParam(name = "AAAirBookRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookRQExt aaAirBookRQExt);

	/**
	 * Returns detailed reservation information. Clients should always call the service with empty transaction
	 * specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationbyPNR", action = "getReservationbyPNR")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			getReservationbyPNR(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);

	/**
	 * Returns Terms & Conditions.
	 * 
	 * @param aaOTATermsNConditionsRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getTermsNConditions", action = "getTermsNConditions")
	@WebResult(name = "AA_OTA_TermsNConditionsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTATermsNConditionsRS
			getTermsNConditions(
					@WebParam(name = "AA_OTA_TermsNConditionsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTATermsNConditionsRQ aaOTATermsNConditionsRQ);

	/**
	 * Returns Itinerary
	 * 
	 * @param otaReadRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getItineraryForPrint", action = "getItineraryForPrint")
	@WebResult(name = "AA_OTA_ItineraryRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public AAOTAItineraryRS getItineraryForPrint(
			@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ otaReadRQ);

	/**
	 * Returns agent available credit amount.
	 * 
	 * @param agentAvailCreditRQ
	 * @return AAOTAAgentAvailCreditRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getAgentAvailableCredit", action = "getAgentAvailableCredit")
	@WebResult(name = "AA_OTA_AgentAvailCreditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAgentAvailCreditRS
			getAgentAvailableCredit(
					@WebParam(name = "AA_OTA_AgentAvailCreditRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAgentAvailCreditRQ agentAvailCreditRQ);

}