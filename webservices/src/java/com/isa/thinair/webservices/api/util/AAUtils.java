package com.isa.thinair.webservices.api.util;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;
import org.opentravel.ota._2003._05.SourceType;

import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Common utilities for processing OTA messages
 * 
 * @author Mehdi Raza
 * @author Webservices Team
 * 
 */
public class AAUtils {

	public static enum RMMethods {
		UPDATE_OPTIMIZED_INVENTORY, BATCH_UPDATE_FLT_INVENTORY
	}

	/**
	 * Returns true if the method source is AA, otherwise false
	 * 
	 * @param mi
	 * @return
	 */
	public static boolean isAAMethod(MethodInvocation mi) {
		return isAAMethod(mi.getMethod());
	}

	public static boolean isAAMethod(Method m) {
		WebServiceSource webServiceSource = m.getAnnotation(WebServiceSource.class);
		if (webServiceSource != null) {
			if (webServiceSource.source().equals(WebServiceSource.MethodSource.AA)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isLoginApiMethod(MethodInvocation mi) {
		return isLoginApiMethod(mi.getMethod());
	}

	public static boolean isLoginApiMethod(Method m) {
		WebServiceSource webServiceSource = m.getAnnotation(WebServiceSource.class);
		if (webServiceSource != null) {
			if (webServiceSource.source().equals(WebServiceSource.MethodSource.LOGIN_API)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isMealServicesMethod(MethodInvocation mi) {
		Method m = mi.getMethod();
		WebServiceSource webServiceSource = m.getAnnotation(WebServiceSource.class);
		if (webServiceSource != null) {
			if (webServiceSource.source().equals(WebServiceSource.MethodSource.MEAL_SERVICES)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if the method source is FAWRY, otherwise false
	 * 
	 * @param mi
	 * @return
	 */
	public static boolean isFAWRYMethod(MethodInvocation mi) {
		return isFAWRYMethod(mi.getMethod());
	}

	public static boolean isFAWRYMethod(Method m) {
		WebServiceSource webServiceSource = m.getAnnotation(WebServiceSource.class);
		if (webServiceSource != null) {
			if (webServiceSource.source().equals(WebServiceSource.MethodSource.FAWRY)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isAmadeusMethod(MethodInvocation mi) {
		return isAmadeusMethod(mi.getMethod());
	}

	public static boolean isAmadeusMethod(Method m) {
		WebServiceSource webServiceSource = m.getAnnotation(WebServiceSource.class);
		if (webServiceSource != null) {
			if (webServiceSource.source().equals(WebServiceSource.MethodSource.AMADEUS)) {
				return true;
			}
		}
		return false;
	}

	public static TrackInfoDTO getTrackInfo() {
		SourceType pos = (SourceType) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.REQUEST_POS);
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);

		String ipAddress = BeanUtils.nullHandler(pos.getIPAddress());
		if (ipAddress.length() == 0) {
			ipAddress = BeanUtils
					.nullHandler(ThreadLocalData.getWSContextParam(WebservicesContext.WEB_REQUEST_IP));
		}

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfo.setSessionId(sessionId);
		trackInfo.setIpAddress(ipAddress);

		return trackInfo;
	}

}
