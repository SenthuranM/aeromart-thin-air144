package com.isa.thinair.webservices.api.exception;

import com.amadeus.ama._2008._03.ErrorWarningType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;

public class AmadeusWSException extends ModuleException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private AmadeusErrorCodes errorCode;
	private ErrorWarningType errorWarningType;
	private String description = "";

	public AmadeusWSException(AmadeusErrorCodes exceptionCode, ErrorWarningType errorWarningType) {
		super(exceptionCode.getErrorCode());
		this.errorCode = exceptionCode;
		this.errorWarningType = errorWarningType;
	}

	public AmadeusWSException(AmadeusErrorCodes exceptionCode, ErrorWarningType errorWarningType, String description) {
		super(exceptionCode.getErrorCode());
		this.errorCode = exceptionCode;
		this.description = description;
		this.errorWarningType = errorWarningType;
	}

	public Integer getErrorCode() {
		return Integer.valueOf(errorCode.getErrorCode());
	}

	public String getDescription() {
		return description;
	}

	public ErrorWarningType getErrorWarningType() {
		return errorWarningType;
	}

}
