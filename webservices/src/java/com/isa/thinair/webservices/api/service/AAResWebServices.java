/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.ejb.TransactionAttributeType;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRS;
import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRS;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRQ;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirBaggageDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRS;
import org.opentravel.ota._2003._05.AAOTAEBIManualDailyReconRptSendRQ;
import org.opentravel.ota._2003._05.AAOTAEBIPnrTxnsReconRQ;
import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditReadRQ;
import org.opentravel.ota._2003._05.AAOTAResAuditRS;
import org.opentravel.ota._2003._05.AAOTAResAuditReadRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRQ;
import org.opentravel.ota._2003._05.AAOTATermsNConditionsRS;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRQ;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRS;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirFlifoRQ;
import org.opentravel.ota._2003._05.OTAAirFlifoRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirScheduleRQ;
import org.opentravel.ota._2003._05.OTAAirScheduleRS;
import org.opentravel.ota._2003._05.OTAAirSeatMapRQ;
import org.opentravel.ota._2003._05.OTAAirSeatMapRS;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRQ;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAResRetrieveRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvBatchUpdateRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventryBatchUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRS;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRQ;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFlightLoadInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAIOBoundPaxCountInfoRS;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRQ;
import com.isaaviation.thinair.webservices.api.eticketupdate.AAOTAETicketStatusUpdateRS;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRQ;
import com.isaaviation.thinair.webservices.api.updatepfs.AAOTAPFSUpdateRS;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

/**
 * Web Service(s) for core reservation services
 * 
 * @author Mohamed Nasly
 * @author Nilindra
 * @author Byorn
 * @author Mehdi
 * 
 * @since 1.0
 */

@WebService(name = "AAResWebServices", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAResWebServices {

	/**
	 * OTA Ping service. Clients can use this service for health check.
	 * 
	 * @param oTAPingRQ
	 * @return OTAPingRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "ping", action = "ping")
	@WebResult(name = "OTA_PingRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAPingRS ping(
			@WebParam(name = "OTA_PingRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAPingRQ oTAPingRQ);

	/**
	 * Flight availability for a given airport pair, for a specific date, for a given number and type of passengers.
	 * 
	 * Clients should call the service with empty transaction when availability search is done for a new booking and
	 * should call with previous transaction when doing availability search for add/modify segment.
	 * 
	 * @param oTAAirAvailRQ
	 * @return OTAAirAvailRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.STARTORMIDDLE)
	@WebMethod(operationName = "getAvailability", action = "getAvailability")
	@WebResult(name = "OTA_AirAvailRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirAvailRS
			getAvailability(
					@WebParam(name = "OTA_AirAvailRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirAvailRQ oTAAirAvailRQ);

	/**
	 * Requests pricing information for specific flights on certain dates for a specific number and type of passengers.
	 * Clients should call the service specifying the previous transaction.
	 * 
	 * @param otaAirPriceRQ
	 * @return OTAAirPriceRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "getPrice", action = "getPrice")
	@WebResult(name = "OTA_AirPriceRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirPriceRS
			getPrice(
					@WebParam(name = "OTA_AirPriceRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirPriceRQ otaAirPriceRQ);

	/**
	 * Return the reservations list. Clients should always call the service with empty transaction specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAResRetrieveRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationsList", action = "getReservationsList")
	@WebResult(name = "OTA_ResRetrieveRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAResRetrieveRS
			getReservationsList(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);

	
	/**
	 * Returns detailed reservation information. Clients should always call the service with empty transaction
	 * specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationbyPNR", action = "getReservationbyPNR")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			getReservationbyPNR(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);
	
	
	/**
	 * Book a specific itinerary on specified booking class (and fare) for one or more passengers with or without
	 * payment specified. Client must call this service with the transaction initiated by availability search/price
	 * quote.
	 * 
	 * @param otaAirBookRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "book", action = "book")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			book(@WebParam(name = "OTA_AirBookRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookRQ otaAirBookRQ,
					@WebParam(name = "AAAirBookRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookRQExt aaAirBookRQExt);

	/**
	 * Returns flight Schedules information.
	 * 
	 * @param OTAAirScheduleRQ
	 * @return OTAAirScheduleRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getFlightSchedule", action = "getFlightSchedule")
	@WebResult(name = "OTA_AirScheduleRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirScheduleRS
			getFlightSchedule(
					@WebParam(name = "OTA_AirScheduleRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirScheduleRQ otaAirScheduleRQ);

	/**
	 * Returns flight information.
	 * 
	 * @param OTAAirFlifoRQ
	 * @return OTAAirFlifoRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getFlightInfo", action = "getFlightInfo")
	@WebResult(name = "OTA_AirFlifoRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirFlifoRS
			getFlightInfo(
					@WebParam(name = "OTA_AirFlifoRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirFlifoRQ otaAirFlifoRQ);

	/**
	 * Modify Reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "modifyReservation", action = "modifyReservation")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			modifyReservation(
					@WebParam(name = "OTA_AirBookModifyRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookModifyRQ otaAirBookModifyRQ,
					@WebParam(name = "AAAirBookModifyRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookModifyRQExt aaAirModifyRQExt);

	/**
	 * Modify reservation queries.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirModifyRQExt
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "modifyResQuery", action = "modifyResQuery")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			modifyResQuery(
					@WebParam(name = "OTA_AirBookModifyRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookModifyRQ otaAirBookModifyRQ,
					@WebParam(name = "AAAirBookModifyRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookModifyRQExt aaAirModifyRQExt);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getSeatMap", action = "getSeatMap")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "OTA_AirSeatMapRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAAirSeatMapRS getSeatMap(OTAAirSeatMapRQ otaAirSeatMapRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "getInsuranceQuote", action = "getInsuranceQuote")
	@WebResult(name = "OTA_InsuranceQuoteRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAInsuranceQuoteRS getInsuranceQuote(OTAInsuranceQuoteRQ otaInsuranceQuoteRQ);

	/**
	 * Returns agent available credit amount.
	 * 
	 * @param agentAvailCreditRQ
	 * @return AAOTAAgentAvailCreditRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getAgentAvailableCredit", action = "getAgentAvailableCredit")
	@WebResult(name = "AA_OTA_AgentAvailCreditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAgentAvailCreditRS
			getAgentAvailableCredit(
					@WebParam(name = "AA_OTA_AgentAvailCreditRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAgentAvailCreditRQ agentAvailCreditRQ);

	/**
	 * Retrieve Passenger Credit.
	 * 
	 * @param passengerCreditRQ
	 * @return AAOTAPaxCreditRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getPassengerCredit", action = "getPassengerCredit")
	@WebResult(name = "AA_OTA_PaxCreditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAPaxCreditRS
			getPassengerCredit(
					@WebParam(name = "AA_OTA_PaxCreditReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ);

	/**
	 * Reconciles AA transactions with Bank transactions.
	 * 
	 * @param transactionsReconRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "reconPaymentTransactions", action = "reconPaymentTransactions")
	@WebResult(name = "AA_OTA_TransactionsReconRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTATransactionsReconRS
			reconPaymentTransactions(
					@WebParam(name = "AA_OTA_TransactionsReconRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTATransactionsReconRQ aaOTATransactionsReconRQ);

	/**
	 * Returns Terms & Conditions.
	 * 
	 * @param aaOTATermsNConditionsRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getTermsNConditions", action = "getTermsNConditions")
	@WebResult(name = "AA_OTA_TermsNConditionsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTATermsNConditionsRS
			getTermsNConditions(
					@WebParam(name = "AA_OTA_TermsNConditionsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTATermsNConditionsRQ aaOTATermsNConditionsRQ);

	/**
	 * Returns audit history.
	 * 
	 * @param aaOTAuditReadRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getResAuditHistory", action = "getResAuditHistory")
	@WebResult(name = "AA_OTA_ResAuditRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAResAuditRS
			getResAuditHistory(
					@WebParam(name = "AA_OTA_ResAuditReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAResAuditReadRQ aaOTAuditReadRQ);

	// ---------------------------------------------------------------------------------
	// Inventory operations for RM integration
	// ---------------------------------------------------------------------------------

	/**
	 * Update Inventory for RM optimization recommendations.
	 * 
	 * @param AAUpdateOptimizedInventoryRQ
	 * @return AAUpdateOptimizedInventoryRS
	 * 
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "updateOptimizedInventory", action = "updateOptimizedInventory")
	@WebResult(name = "AAUpdateOptimizedInventoryRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAUpdateOptimizedInventoryRS
			updateOptimizedInventory(
					@WebParam(name = "AAUpdateOptimizedInventoryRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAUpdateOptimizedInventoryRQ aaUpdateOptimizedInventoryRQ);

	/**
	 * For batch update of the inventories.
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "batchUpdateFlightInventory", action = "batchUpdateFlightInventory")
	@WebResult(name = "AAFlightInvBatchUpdateRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAFlightInvBatchUpdateRS
			batchUpdateFlightInventory(
					@WebParam(name = "AAFlightInvBatchUpdateRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAFlightInventryBatchUpdateRQ aaAAFlightInventoryBatchRQ);

	/**
	 * For geting segment wise - agents seat selilng report for a flight
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getAgentSeatSellingReport", action = "getAgentSeatSellingReport")
	@WebResult(name = "AAAgentSeatMovemantRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAAgentSeatMovemantRS
			getAgentSeatSellingReport(
					@WebParam(name = "AAAgentSeatMovemantRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAAgentSeatMovemantRQ aaSeatMovemantRQ);

	/**
	 * For Retrieving Flight Inventory Allocations
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getFlightInventoryAllocations", action = "getFlightInventoryAllocations")
	@WebResult(name = "AAFlightInventoryDataRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAFlightInventoryDataRS
			getFlightInventoryAllocations(
					@WebParam(name = "AAFlightInventorySearchRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAFlightInventorySearchRQ searchRQ);

	/**
	 * TEMPORARY WS METHODS FOR MANUAL STATUS CHECK AND MANUAL RECONCILIATION OF AIR ARABIA - EMIRATES BANK TRANSACTIONS
	 * 
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "syncPnrTxnsWithEBI", action = "syncPnrTxnsWithEBI")
	@Oneway
	public
			void
			syncPnrTxnsWithEBI(
					@WebParam(name = "AA_OTA_EBIPnrTxnsReconRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAEBIPnrTxnsReconRQ aaOTAPnrTxnsReconRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "manualSendEBIDailyReconRpt", action = "manualSendEBIDailyReconRpt")
	@Oneway
	public
			void
			manualSendEBIDailyReconRpt(
					@WebParam(name = "AA_OTA_EBIManualDailyReconRptSendRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAEBIManualDailyReconRptSendRQ aaOTAEBIManualDailyReconRptSendRQ);

	/**
	 * For Ceaser
	 * 
	 * @param aaAAFlightInventoryBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getPaxTypesForFlightLeg", action = "getPaxTypesForFlightLeg")
	@WebResult(name = "AAFligthtLegPaxCountInfoRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules")
	public
			AAFligthtLegPaxCountInfoRS
			getPaxTypesForFlightLeg(
					@WebParam(name = "AAFligthtLegPaxCountInfoRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules") AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ);

	/**
	 * Returns Itinerary
	 * 
	 * @param otaReadRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getItinerary", action = "getItinerary")
	@WebResult(name = "AA_OTA_ItineraryRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public AAOTAItineraryRS getItinerary(
			@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ otaReadRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getAgentSalesData", action = "getAgentSalesData")
	@WebResult(name = "AAPNRPaxTicketingDataRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airreservation")
	public
			AAPNRPaxTicketingDataRS
			getAgentSalesData(
					@WebParam(name = "AAPNRPaxTicketingDataRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airreservation") AAPNRPaxTicketingDataRQ aapnrPaxTicketingDataRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getMealDetails", action = "getMealDetails")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirMealDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAirMealDetailsRS
			getMealDetails(
					@WebParam(name = "AA_OTA_AirMealDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirMealDetailsRQ aaOtaAirMealDetailsRQ);
	
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getMultiMealDetails", action = "getMultiMealDetails")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirMultiMealDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public AAOTAAirMultiMealDetailsRS
		   getMultiMealDetails(@WebParam(name = "AA_OTA_AirMultiMealDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirMultiMealDetailsRQ aaOtaAirmultiMealDetailsRQ);
	
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getSSRDetails", action = "getSSRDetails")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirSSRDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAirSSRDetailsRS
			getSSRDetails(
					@WebParam(name = "AA_OTA_AirSSRDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirSSRDetailsRQ aaOTAAirSSRDetailsRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getUserDefinedSSRDetails", action = "getUserDefinedSSRDetails")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirUserDefinedSSRDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
	AAOTAAirSSRDetailsRS
			getUserDefinedSSRDetails(
					@WebParam(name = "AA_OTA_AirSSRDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirSSRDetailsRQ aaOTAAirSSRDetailsRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "setUserDefinedSSRAmount", action = "setUserDefinedSSRAmount")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirSSRSetAmountRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
		AAOTAAirSSRSetAmountRS
			setUserDefinedSSRAmount(
					@WebParam(name = "AA_OTA_AirSSRSetAmountRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirSSRSetAmountRQ aaOTAAirSSRSetAmountRQ);

	
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getBaggageDetails", action = "getBaggageDetails")
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebResult(name = "AA_OTA_AirBaggageDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAirBaggageDetailsRS
			getBaggageDetails(
					@WebParam(name = "AA_OTA_AirBaggageDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirBaggageDetailsRQ aaOtaAirBaggageDetailsRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "updateEticketStatus", action = "updateEticketStatus")
	@WebResult(name = "AA_OTA_ETicketStatusUpdateRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/eticketupdate")
	public
			AAOTAETicketStatusUpdateRS
			updateEticketStatus(
					@WebParam(name = "AA_OTA_ETicketStatusUpdateRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/eticketupdate") AAOTAETicketStatusUpdateRQ AA_OTA_ETicketStatusUpdateRQ);

	/**
	 * PFS update method for AA DCS
	 * 
	 * @param AA_OTA_PFSUpdateRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "updatePFS", action = "updatePFS")
	@WebResult(name = "AA_OTA_PFSUpdateRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/updatepfs")
	public
			AAOTAPFSUpdateRS
			updatePFS(
					@WebParam(name = "AA_OTA_PFSUpdateRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/updatepfs") AAOTAPFSUpdateRQ AA_OTA_PFSUpdateRQ);

	/**
	 * For SPS
	 * 
	 * @param AAFligthtLegPaxCountInfoRQ
	 * @return AAIOBoundPaxCountInfoRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getIOBoundPaxCountInfo", action = "getIOBoundPaxCountInfo")
	@WebResult(name = "AAIOBoundPaxCountInfoRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules")
	public
			AAIOBoundPaxCountInfoRS
			getIOBoundPaxCountInfo(
					@WebParam(name = "AAFligthtLegPaxCountInfoRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules") AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ);
	
	
	/**
	 * Returns tax invoice information. Clients should always call the service with empty transaction
	 * specified.
	 * 
	 * @param AAOTAAirTaxInvoiceGetRQ
	 * @return AAOTAAirTaxInvoiceGetRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getTaxInvoicebyPNR", action = "getTaxInvoicebyPNR")
	@WebResult(name = "AA_OTA_AirTaxInvoiceGetRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAirTaxInvoiceGetRS
			getTaxInvoicebyPNR(
					@WebParam(name = "AA_OTA_AirTaxInvoiceGetRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirTaxInvoiceGetRQ aaOTAAirTaxInvoiceGetRS);
	
	/**
	 * Flight price availability for a given airport pair, for a specific date, for a given number and type of passengers.
	 * For all available combinations,prices will be returned
	 * 
	 * Clients should call the service with empty transaction when availability search is done for a new booking and
	 * should call with previous transaction when doing availability search for add/modify segment.
	 * 
	 * @param oTAAirAvailRQ
	 * @return OTAAirAvailRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.STARTORMIDDLE)
	@WebMethod(operationName = "getAllPriceAvailability", action = "getAllPriceAvailability")
	@WebResult(name = "AA_OTA_AirAllPriceAvailRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
		AAOTAAirAllPriceAvailRS
			getAllPriceAvailability(
					@WebParam(name = "OTA_AirAvailRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirAvailRQ oTAAirAvailRQ);


	/**
	 * Mahan Air Fleet watch to provide the booked Pax load
	 * 
	 * @param AAFlightLoadInfoRQ
	 * @return AAFlightLoadInfoRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getFlightLoadInfo", action = "getFlightLoadInfo")
	@WebResult(name = "AAFlightLoadInfoRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules")
	public
	AAFlightLoadInfoRS
			getFlightLoadInfo(
					@WebParam(name = "AAFlightLoadInfoRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airschedules") AAFlightLoadInfoRQ flightLetPaxCountRQ);

}