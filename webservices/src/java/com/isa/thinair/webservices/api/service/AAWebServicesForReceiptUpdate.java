package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRQ;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRS;

/**
 * @author Manoj Dhanushka
 */
@WebService(name = "AAWebServicesForReceiptUpdate", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForReceiptUpdate")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAWebServicesForReceiptUpdate {

	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "updateReceipt", action = "updateReceipt")
	@WebResult(name = "AAReceiptUpdateRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/receiptupdate")
	public
		AAReceiptUpdateRS
			updateReceipt(
					@WebParam(name = "AAReceiptUpdateRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/receiptupdate") AAReceiptUpdateRQ aaReceiptUpdateRQ);
}
