/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Constants shared across the webservices module
 */
public interface WebservicesConstants {

	public static final String NEWLINE = System.getProperty("line.separator");

	public static final String PHONE_NUMBER_SEPERATOR = "-";

	public static SimpleDateFormat DATE_FORMAT_FOR_LOGGING = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public static final String DEFAULT_PAX_CATEGORY = "A";

	public static final Integer MAX_CON_BUCKET_SIZE = 3;
	
	public static final String OPERATION_TYPE_PUBLISH = "PUBLISH";
	public static final String OPERATION_TYPE_UPDATE = "UPDATE";
	

	public interface FAWRYConstants {

		public static final String LOGIN_NAME = "loginName";
		public static final String LOGIN_PASS = "loginPass";
		public static final String Default_SALES_CHANNEL_ID = "defaultSalesChannelId";
		public static final String Company_Code = "companyCode";

		/** Default Langugae */
		public static final String PRIMARY_LANG_CODE = "en-us";

		public static final String FAWRY_BILLER_ID = "6";

		public static final long FAWRY_BILL_TYPE_CODE = 236;

		public static final String Sender = "FAWRY";

		public static final String VERSION_BillInRs = "V1.0";
		public static final String VERSION_BillInRq = "V1.0";
		public static final String VERSION_PmtNotifyRs = "V1.0";
		public static final String VERSION_PmtNotifyRq = "V1.0";
		public static final String BillInRs_MsgCode = "BillInqRs";
		public static final String BillInRq_MsgCode = "BillInqRq";
		public static final String PmtNotifyRq_MsgCode = "PmtNotifyRq";
		public static final String PmtNotifyRs_MsgCode = "PmtNotifyRs";

		public interface IFawryErrorCode {

			public static final String Success = "00200";

			public static final String Message_Authentication_Error = "00031";

			public static final String Sender_is_not_authorized = "00006";

			public static final String Invalid_Billing_Account = "21013";

			public static final String Bill_not_available_for_payment = "21012";

			public static final String Payment_Amount_validation_Error = "21004";

			public static final String Message_Not_Valid = "00007";

			public static final String General_Error = "00026";

			public static final String Unsupported_Message = "00010";

			public static final String Message_type_not_exists = "00015";

			public static final String Invalid_Transaction_State = "00040";

			public static final String Invalid_BillerId = "00027";

			public static final String Invalid_Receiver = "00001";

			public static final String Invalid_Bill_Type_Code = "00039";

			public static final String Invalid_billing_account_number = "12011";

			public static final String Req_Cancelled_Booking = "12007";

		}
	}

	public interface OTAConstants {

		public static interface CardTypes {
			public static final String CREDIT = "1";
			public static final String DEBIT = "2";
			public static final String CENTRAL_BILL = "3";
		}

		/** Default Langugae */
		public static final String PRIMARY_LANG_CODE = "en-us";

		/** Decimals in currency values */
		public static final BigInteger DECIMALS_AED = new BigInteger("2");

		/** Default decimals truncation for BigDecimal input */
		public DecimalFormat REFAULT_DECIMAL_FORMAT = new DecimalFormat(".####");

		/** Versions */
		public static final BigDecimal VERSION_Ping = new BigDecimal("20061.00"); // format
																					// :
																					// ota
																					// spec
																					// version.internal
																					// version
		public static final BigDecimal VERSION_AirAvail = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirPrice = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_RetrieveRes = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_RetrieveResList = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_ResSearch = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_Book = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirBookModify = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AgentAvailCredit = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_PaxCreditReq = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirScheudle = new BigDecimal("20061.00");
		public static final BigDecimal VERSION_AirFltInfo = new BigDecimal("20061.00");
		
		public static final String CODE_CONTEXT_IATA = "IATA";

	}

	/**
	 * Refer to OTA_CodeTable for Ticketing Status (TST)
	 * 
	 * @author Mehdi Raza
	 * 
	 */
	public interface TicketStatus {

		public static final String CONFIRMED = "1";
		public static final String PARTIAL = "4";
		public static final String ON_HOLD = "1";
	}

	public interface PriviledgeConstants {

	}

	public interface UniqueIDTypes {
		public static final String RESERVATION = "14";
		public static final String HOTEL = "10";
	}
	
	public interface RequestResponseTypes {
		public static final String AVAILABILITY_REQUEST = "AVAIL_REQ";
		public static final String AVAILABILITY_RESPONSE_START = "AVAIL_RES_START";
		public static final String AVAILABILITY_RESPONSE_END = "AVAIL_RES_END";
		public static final String PRICE_REQUEST = "PRICE_REQ";
		public static final String PRICE_RESPONSE = "PRICE_RES";
		public static final String BOOK_REQUEST = "BOOK_REQ";
		public static final String BOOK_RESPONSE = "BOOK_RES";
		public static final String AUTOCHECKIN_REQUEST = "AUTOCHECKIN_REQ";
		public static final String AUTOCHECKIN_RESPONSE = "AUTOCHECKIN_RES";
		public static final String AUTOCHECKIN_APPLICABILITY_YES = "Y";
		public static final String AUTOCHECKIN_APPLICABILITY_NO = "N";
	}

	public interface UserTokenKeys {
		public static final String PRINCIPAL = "userPrincipal";
		public static final String PLAIN_TEXT_PASSWORD = "userPlainTextPassword";
		public static final String HASHED_PASSWORD = "userHashedPassword";
	}

	// holds the payment type information
	public interface PaymentTypes {
		public static final String CASH = "cash";
		public static final String CREDIT_CARD = "creditcard";
		public static final String ON_ACCOUNT = "onaccount";
	}

	public interface GCC {
		public static final String GCC = "GCC";
		public static final String NON_GCC = "NONGCC";
	}

	public interface ClientIdentifiers {
		public static final String FAWRY_EBPP = "FAWRY_EBPP";
	}

	public interface AmadeusConstants {

		public static final String LOGIN_NAME = "loginName";
		public static final String LOGIN_PASS = "loginPass";
		public static final String DEFAULT_SALES_CHANNEL_ID = "defaultSalesChannelId";
		public static final String COMPANY_CODE = "1A";

		public static final String BOOKING_CLASS = "bookingClass";
		public static final String FARE_RULE = "fareRule";
		public static final String DEFAULT_BOOKING_CLASS_ENABLED = "defaultBCEnabled";
		public static final String DEFAULT_BOOKING_CLASS = "defaultBC";
		public static final String DEFAULT_FARE_RULE = "defaultFareRule";

		public static final String YES = "Y";
		public static final String NO = "N";

		public static final int GDS_ID = 1;

		/* releases */
		public static final String ReleaseAvailPriceRS = "2008A";
		public static final String ReleaseFareRuleRS = "2008A";
		public static final String ReleasePriceRS = "2008A";
		public static final String ReleasePayAndBookRS = "2008A";
		public static final String ReleaseRetrieveBooking = "2008A";

		/* versions */
		public static final BigDecimal VersionAvailPriceRS = new BigDecimal("1.001");
		public static final BigDecimal VersionFareRuleRS = new BigDecimal("1.001");
		public static final BigDecimal VersionPriceRS = new BigDecimal("1.001");
		public static final BigDecimal VersionPayAndBookRS = new BigDecimal("1.001");
		public static final BigDecimal VersionRetrieveBooking = new BigDecimal("1.001");

		public static final String ADULT = "ADT";
		public static final String CHILD = "CHD";
		public static final String INFANT = "INF";

		public enum AmadeusErrorCodes {
			/* Generic errors */
			GENERIC_PERMISSION_DENIED("100001"), GENERIC_INVALID_ACCOUNT("100002"), GENERIC_PASSWORD_INCORRECT("100003"),

			GENERIC_INTERNAL_ERROR_RETRY("100010"), GENERIC_INTERNAL_ERROR_DO_NOT_RETRY("100011"), GENERIC_QUERY_UNABLE_TO_PARSE(
					"100012"), GENERIC_QUERY_INCOHERENT_DATA_FOUND("100013"),

			/* errors */

			FARES_RETRIEVAL_FAILED("100020"),

			NO_FLIGHTS_FOUND("100030"), INVALID_ORIGIN_DESTINATION("100031"),

			INVALID_DEPARTURE_DATE_FOR_SEGMENT("100040"), FLIGHT_IS_NOT_AVAILABLE("100041"),

			PNR_DESYNCHRONIZED("100050"), REQUESTED_FARE_NOT_AVAILABLE("100051"),

			REQUESTED_SERVICE_NOT_MATCHING("100060"), REQUESTED_SERVICE_NOT_AVAILABLE("100061"), REQUESTED_TOO_MANY_SEGMENTS(
					"100063"), REQUESTED_INFANTS_NOT_SUPPORTED("100064"),

			SECURITY_CODE_REQUIRED("100120"), REQUESTED_CURRENCY_NOT_SUPPORTED("100150"),

			INCORRECT_PAYMENTS("100220"),

			DUPLICATE_RESERVATION_ID("100280"),

			TELEPHONE_NUMBER_REQUIRED("100300"), REQUESTED_PNR_NOT_FOUND("100310"), INVALID_CARD_CODE("100320"), FARE_RULE_DOES_NOT_EXISTS(
					"100330"),

			PAYMENT_NOT_APPROVED_RETRY_LATER("100110"), YOUR_CREDIT_CARD_COULD_NOT_BE_PROCESSED_PLEASE_CHECK("100140"), INVALID_EXPIRATION_DATE(
					"100080"), INVALID_OR_MISSING_CREDI_CARD_SECURITY_CODE("100121");

			private String errorCode = null;

			AmadeusErrorCodes(String code) {
				this.errorCode = code;
			}

			public String getErrorCode() {
				return errorCode;
			}

		}
	}
	
	public interface MyIDTravelConstants {

		public static final String SEGMENT_SELL = "SS";
		// Security config
		public static final String KEYSTORE_FILE_REL_PATH = "keystorePath";
		public static final String KEYSTORE_TYPE = "keystoreType";
		public static final String KEYSTORE_PASSWORD = "keystorePassword";
		public static final String PRIVATE_KEY_ALIAS = "privateKeyAlias";
		public static final String PUBLIC_CERT_ALIAS = "publicCertAlias";
		public static final String AA_CHANNEL = "aAChannel";
		public static final String SECURITY_ENABLED = "securityEnabled";

		public static final String DEFAULT_LANG = "en";
		public static final String REMARK_TYPE = "RC";

		public static final String TICKET_STATUS_OPEN = "101";
		public static final String TICKET_STATUS_USED = "102";
		public static final String TICKET_STATUS_REFUNDED = "103";
		public static final String TICKET_STATUS_LOCKED = "104";

		public static final String MOD_TYPE_CANCEL_SEGMENT = "10";
		public static final String MOD_TYPE_REBOOK_SEGMENT = "20";
		public static final String MOD_TYPE_REBOOK_ALL = "30";

	}


	public enum WebServicesErrorCodes {
		CREDENTIALS_INVALID("1001", "User Name / Password does not match."),
		SERVER_ERROR("2001", "Cannot process the request."),
		WS_AUTHENTICATION_FAILED("2002", "Web-Service Authentication Failed."),
		WS_AUTH_USER_INSUFFICIENT_PRIV("2003", "Insufficient Privileges of Web-Service Invoker."),
		MANDATORY_FIELD_ABSENT("3001", "Mandatory Field Missing"),
		USER_NOT_FOUND("1002", "User does not exist."),
		INVALID_TOKEN("1003", "Invalid Token.");

		private String errorCode;
		private String errorDescription;

		private WebServicesErrorCodes(String errorCode, String errorDescription) {
			this.errorCode = errorCode;
			this.errorDescription = errorDescription;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public String getErrorDescription() {
			return errorDescription;
		}
	}
}
