package com.isa.thinair.webservices.api.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.isa.thinair.webservices.api.exception.WebservicesException;

public class TPAExtensionUtil {

	private Map<String, String> nsMappings = new HashMap<String, String>();

	private static TPAExtensionUtil instance = new TPAExtensionUtil();

	private DocumentBuilderFactory factory = null;

	DocumentBuilder builder = null;

	public static interface Namespaces {

		public static final String OPENTRAVEL_NAMESPACE = "http://www.opentravel.org/OTA/2003/05";

		public static final String ISAAVIATION_NAMESPACE = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05";
	}

	public static interface RootTags {

		public static final String AA_READRQ_EXT = "AAReadRQExt";

		public static final String AA_AIRBOOKRQ_EXT = "AAAirBookRQExt";

		public static final String AA_AIRBOOKMODIFYRQ_EXT = "AAAirBookModifyRQExt";
	}

	private TPAExtensionUtil() {
		nsMappings.put(Namespaces.ISAAVIATION_NAMESPACE, "com.isaaviation.thinair.webservices.ota.extensions._2003._05");
		nsMappings.put(Namespaces.OPENTRAVEL_NAMESPACE, "org.opentravel.ota._2003._05");
		factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

	public static TPAExtensionUtil getInstance() {
		return instance;
	}

	/**
	 * Unmarshals into objects of specified type
	 * 
	 * @param listExtensions
	 * @param clazz
	 * @return
	 * @throws WebservicesException
	 * 
	 *             TODO use Streaming API
	 */
	public List<Object> unmarshall(List<Element> listExtensions, Class clazz) throws WebservicesException {
		try {
			JAXBContext context = JAXBContext.newInstance(clazz.getPackage().getName());
			Unmarshaller unmarshaller = context.createUnmarshaller();
			List<Object> list = new ArrayList<Object>();
			for (Element element : listExtensions) {
				String fullyQualifiedClass = new StringBuffer(nsMappings.get(element.getNamespaceURI()).toString()).append(".")
						.append(getClassName(element.getLocalName())).toString();
				if (clazz == Class.forName(fullyQualifiedClass)) {
					list.add(unmarshaller.unmarshal(element, clazz).getValue());
				}
			}
			return list;
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		} catch (ClassNotFoundException e) {
			throw new WebservicesException(e);
		}
	}

	public Object unmarshalFromFile(List<String> pkgs, File file) throws JAXBException {
		boolean first = true;
		StringBuffer pckgs = new StringBuffer();
		for (String p : pkgs) {
			if (!first)
				pckgs.append(":");
			else
				first = false;

			pckgs.append(p);
		}
		String pckg = pckgs.toString();

		JAXBContext jc = JAXBContext.newInstance(pckg);
		Unmarshaller u = jc.createUnmarshaller();
		return u.unmarshal(file);
	}

	public Element marshall(Object o) throws WebservicesException {
		try {
			JAXBContext context = JAXBContext
					.newInstance("org.opentravel.ota._2003._05:com.isaaviation.thinair.webservices.ota.extensions._2003._05");
			Marshaller marshaller = context.createMarshaller();
			Document doc = getNewDocument();
			marshaller.marshal(o, doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}

	/**
	 * Marshals adding root tag
	 * 
	 * @param objectToMarshal
	 * @param clazz
	 * @param rootTag
	 * @return
	 * @throws WebservicesException
	 * 
	 *             TODO use Streaming API
	 */
	public Element marshalNonRootElement(Object objectToMarshal, Class clazz, String namespaceURI, String rootTag)
			throws WebservicesException {
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			Document doc = getNewDocument();
			marshaller.marshal(new JAXBElement(new QName(namespaceURI, rootTag), clazz, objectToMarshal), doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}

	public void marshallToFile(Object o, String absFilePath) throws WebservicesException, FileNotFoundException {
		try {
			JAXBContext context = JAXBContext.newInstance(o.getClass().getPackage().getName());
			Marshaller marshaller = context.createMarshaller();
			OutputStream os = new FileOutputStream(absFilePath);
			marshaller.marshal(o, os);
			os.close();
		} catch (IOException e) {
			throw new WebservicesException(e);
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}

	public void marshallToSysOut(Object o) throws WebservicesException {
		try {
			JAXBContext context = JAXBContext
					.newInstance("org.opentravel.ota._2003._05:com.isaaviation.thinair.webservices.ota.extensions._2003._05");
			Marshaller marshaller = context.createMarshaller();
			marshaller.marshal(o, System.out);
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}

	private Document getNewDocument() {
		return builder.newDocument();
	}

	private String getClassName(String localName) {
		return localName.replace("_", "");
	}
}
