package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRQ;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRS;

/**
 * @author Manoj Dhanushka 
 * Web Service(s) for type B reservation message processing
 */
@WebService(name = "AATypeBResWebServices", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AATypeBResWebServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AATypeBResWebServices {

	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "processTypeBResMessage", action = "processTypeBResMessage")
	@WebResult(name = "AATypeBReservationRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/typebreservation")
	public
			AATypeBReservationRS
			processTypeBResMessage(
					@WebParam(name = "AATypeBReservationRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/typebreservation") AATypeBReservationRQ aaTypeBReservationRQ);

}
