package com.isa.thinair.webservices.api.service;

import com.isa.thinair.commons.api.dto.ets.AeroMartChargeRefundabilityRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartChargeRefundabilityRs;

public interface AAChargeRefundabilityService {

	AeroMartChargeRefundabilityRs getChargeWiseRefundability(AeroMartChargeRefundabilityRq chargeRefundabilityRq);

}
