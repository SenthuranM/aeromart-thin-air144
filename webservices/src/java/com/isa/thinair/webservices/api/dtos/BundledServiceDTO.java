package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DTO to keep bundled service details required for price quoting
 * 
 * @author rumesh
 * 
 */
public class BundledServiceDTO implements Serializable {

	private static final long serialVersionUID = 8300619936424946220L;

	private Map<Integer, Set<String>> ondBookingClasses;

	private Map<Integer, Map<String, String>> ondSegBookingClasses;

	private Map<Integer, Boolean> ondFlexiSelection;

	public Map<Integer, Set<String>> getOndBookingClasses() {
		if (ondBookingClasses == null) {
			ondBookingClasses = new HashMap<Integer, Set<String>>();
		}
		return ondBookingClasses;
	}

	public void setOndBookingClass(Map<Integer, Set<String>> ondBookingClasses) {
		this.ondBookingClasses = ondBookingClasses;
	}

	public Map<Integer, Map<String, String>> getOndSegBookingClasses() {
		if (this.ondSegBookingClasses == null) {
			this.ondSegBookingClasses = new HashMap<Integer, Map<String, String>>();
		}
		return ondSegBookingClasses;
	}

	public void setOndSegBookingClasses(Map<Integer, Map<String, String>> ondSegBookingClasses) {
		this.ondSegBookingClasses = ondSegBookingClasses;
	}

	public Map<Integer, Boolean> getOndFlexiSelection() {
		if (ondFlexiSelection == null) {
			ondFlexiSelection = new HashMap<Integer, Boolean>();
		}
		return ondFlexiSelection;
	}

	public void setOndFlexiSelection(Map<Integer, Boolean> ondFlexiSelection) {
		this.ondFlexiSelection = ondFlexiSelection;
	}

	public void addBookingClass(int ondSequence, Set<String> bookingClasses) {
		getOndBookingClasses().put(ondSequence, bookingClasses);
	}

	public void addFlexiSelection(int ondSequence, boolean flexiIncluded) {
		getOndFlexiSelection().put(ondSequence, flexiIncluded);
	}
}
