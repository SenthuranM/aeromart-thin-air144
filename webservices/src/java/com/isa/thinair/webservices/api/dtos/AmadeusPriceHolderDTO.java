package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.amadeus.ama._2008._03.TLASingleOriginDestinationTypeRRQ;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AmadeusPriceHolderDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5461991796424243653L;

	private Map<String, SelectedOndFareDTO> originDestinationList = new HashMap<String, SelectedOndFareDTO>();

	private AmadeusTotalPriceDTO priceHolder = new AmadeusTotalPriceDTO();

	private int noOfAdults;

	private int noOfChilds;

	private int noOfInfants;

	private CurrencyExchangeRate exchangeRate;

	public AmadeusPriceHolderDTO(int adults, int childs, int infants, CurrencyExchangeRate exRate) {
		this.noOfAdults = adults;
		this.noOfChilds = childs;
		this.noOfInfants = infants;
		this.exchangeRate = exRate;
	}

	public void addEntry(TLASingleOriginDestinationTypeRRQ singleOndRQ, String bookingClass, SelectedOndFareDTO fares) {

		String key = singleOndRQ.getOriginDestinationInfo().getOriginLocation().getLocationCode() + "/"
				+ singleOndRQ.getOriginDestinationInfo().getDestinationLocation().getLocationCode() + "/" + bookingClass;

		if (!originDestinationList.containsKey(key)) {
			originDestinationList.put(key, fares);

		}

	}

	public AmadeusTotalPriceDTO getCalculatedTotalPrice() {

		for (String key : this.originDestinationList.keySet()) {

			SelectedOndFareDTO selectedOndFare = this.originDestinationList.get(key);

			for (OndFareDTO ondFare : selectedOndFare.getFareDTOs()) {
				BigDecimal baseFare = BigDecimal.ZERO;
				BigDecimal taxNCharges = BigDecimal.ZERO;
				BigDecimal baseFareInSelCur = BigDecimal.ZERO;
				BigDecimal taxesInSelCur = BigDecimal.ZERO;
				if (this.noOfAdults > 0) {
					baseFare = AccelAeroCalculator.add(baseFare, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare()), this.noOfAdults));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[0]), this.noOfAdults));

					// convert ond fare to exchange rate -

					if (this.exchangeRate != null) {

						BigDecimal adultFareInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare()),
										this.exchangeRate.getMultiplyingExchangeRate())));

						BigDecimal adultChargesInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[0]),
										this.exchangeRate.getMultiplyingExchangeRate())));

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(adultFareInSelectedCur, new BigDecimal(this.noOfAdults)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(adultChargesInSelectedCur, new BigDecimal(this.noOfAdults)),
								taxesInSelCur);

					}

				}

				if (this.noOfChilds > 0) {
					baseFare = AccelAeroCalculator.add(baseFare, AccelAeroCalculator.multiply(

					AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare()), this.noOfChilds));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[2]), this.noOfChilds));

					if (this.exchangeRate != null) {

						BigDecimal childFareInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare()),
										this.exchangeRate.getMultiplyingExchangeRate())));

						BigDecimal childChargesInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[2]),
										this.exchangeRate.getMultiplyingExchangeRate())));

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(childFareInSelectedCur, new BigDecimal(this.noOfChilds)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(childChargesInSelectedCur, new BigDecimal(this.noOfChilds)),
								taxesInSelCur);

					}

				}
				if (this.noOfInfants > 0) {
					baseFare = AccelAeroCalculator.add(baseFare, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare()), this.noOfInfants));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[1]), this.noOfInfants));

					if (this.exchangeRate != null) {

						BigDecimal infantFareInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare()),
										this.exchangeRate.getMultiplyingExchangeRate())));

						BigDecimal infantChargesInSelectedCur = new BigDecimal(
								AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
										AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[1]),
										this.exchangeRate.getMultiplyingExchangeRate())));

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(infantFareInSelectedCur, new BigDecimal(this.noOfInfants)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(infantChargesInSelectedCur, new BigDecimal(this.noOfInfants)),
								taxesInSelCur);

					}
				}

				priceHolder.setTotalFare(AccelAeroCalculator.add(priceHolder.getTotalFare(), baseFare));
				priceHolder.setTotalTax(AccelAeroCalculator.add(priceHolder.getTotalTax(), taxNCharges));

				if (exchangeRate != null) {
					priceHolder.setTotalInSelectedCurrency(AccelAeroCalculator.add(priceHolder.getTotalInSelectedCurrency(),
							AccelAeroCalculator.add(baseFareInSelCur, taxesInSelCur)));
				} else {
					priceHolder.setTotalInSelectedCurrency(AccelAeroCalculator.add(priceHolder.getTotalInSelectedCurrency(),
							AccelAeroCalculator.add(baseFare, taxNCharges)));
				}
			}
		}

		return this.priceHolder;
	}

	public Map<String, SelectedOndFareDTO> getOriginDestinationList() {
		return originDestinationList;
	}

	public void setOriginDestinationList(Map<String, SelectedOndFareDTO> originDestinationList) {
		this.originDestinationList = originDestinationList;
	}

	public AmadeusPriceHolderDTO copy() {

		AmadeusPriceHolderDTO copyPriceHolder = new AmadeusPriceHolderDTO(this.noOfAdults, this.noOfChilds, this.noOfInfants,
				this.exchangeRate);

		for (String key : this.originDestinationList.keySet()) {
			copyPriceHolder.getOriginDestinationList().put(key, this.originDestinationList.get(key));
		}

		return copyPriceHolder;
	}

	public CurrencyExchangeRate getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(CurrencyExchangeRate exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

}
