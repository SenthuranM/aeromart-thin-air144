package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaymentSummaryTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4960309702116657344L;

	private Integer tnxId;

	BigDecimal amount;

	BigDecimal amountInPayCurrency;

	String payCurrencyCode;

	String agencyCode;

	/**
	 * @return the tnxId
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            the tnxId to set
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the amoutInPayCurrency
	 */
	public BigDecimal getAmountInPayCurrency() {
		return amountInPayCurrency;
	}

	/**
	 * @param amountInPayCurrency
	 *            the amoutInPayCurrency to set
	 */
	public void setAmountInPayCurrency(BigDecimal amountInPayCurrency) {
		this.amountInPayCurrency = amountInPayCurrency;
	}

	/**
	 * @return the payCurrencyCode
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the agencyCode
	 */
	public String getAgencyCode() {
		return agencyCode;
	}

	/**
	 * @param agencyCode
	 *            the agencyCode to set
	 */
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}
}
