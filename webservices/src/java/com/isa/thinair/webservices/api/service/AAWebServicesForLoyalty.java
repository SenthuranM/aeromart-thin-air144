/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCreditRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCreditRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileRS;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileStatusRQ;
import com.isaaviation.thinair.webservices.api.masherqbankloyalty.AALoyaltyCustomerProfileStatusRS;

/**
 * @author Haider Sahib 08Jul09
 * 
 *         Web Service(s) for Loyalty Program services
 */

@WebService(name = "AAWebServicesForLoyalty", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForLoyalty")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAWebServicesForLoyalty {

	/**
	 * OTA Ping service. Clients can use this service for health check.
	 * 
	 * @param oTAPingRQ
	 * @return OTAPingRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "ping", action = "ping")
	@WebResult(name = "OTA_PingRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAPingRS ping(
			@WebParam(name = "OTA_PingRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAPingRQ oTAPingRQ);

	/**
	 * For adding new customers profile.
	 * 
	 * @param addCustomerProfile
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "addCustomerProfile", action = "addCustomerProfile")
	@WebResult(name = "AALoyaltyCustomerProfileRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty")
	public
			AALoyaltyCustomerProfileRS
			addCustomerProfile(
					@WebParam(name = "AALoyaltyCustomerProfileRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty") AALoyaltyCustomerProfileRQ aaLoyaltyCustomerProfile);

	/**
	 * For adding new customers cerdit.
	 * 
	 * @param addCustomerCredit
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "addCustomerCredit", action = "addCustomerCredit")
	@WebResult(name = "AALoyaltyCreditRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty")
	public
			AALoyaltyCreditRS
			addCustomerCredit(
					@WebParam(name = "AALoyaltyCreditRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty") AALoyaltyCreditRQ aaLoyaltyCerdit);

	/**
	 * For set customers profile status.
	 * 
	 * @param setCustomerProfileStatus
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "setCustomerProfileStatus", action = "setCustomerProfileStatus")
	@WebResult(name = "AALoyaltyCustomerProfileRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty")
	public
			AALoyaltyCustomerProfileRS
			setCustomerProfileStatus(
					@WebParam(name = "AALoyaltyCustomerProfileRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty") AALoyaltyCustomerProfileRQ aaLoyaltyCustomerProfile);

	/**
	 * For set customers profile status.
	 * 
	 * @param getCustomerProfileStatus
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getCustomerProfileStatus", action = "getCustomerProfileStatus")
	@WebResult(name = "AALoyaltyCustomerProfileStatusRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty")
	public
			AALoyaltyCustomerProfileStatusRS
			getCustomerProfileStatus(
					@WebParam(name = "AALoyaltyCustomerProfileStatusRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/masherqbankloyalty") AALoyaltyCustomerProfileStatusRQ aaLoyaltyCustomerProfileStatus);

}
