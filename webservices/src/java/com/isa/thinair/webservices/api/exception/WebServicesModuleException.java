package com.isa.thinair.webservices.api.exception;

import com.isa.thinair.webservices.api.util.WebservicesConstants;

public class WebServicesModuleException extends Exception {

	private WebservicesConstants.WebServicesErrorCodes errorCode;
	private String additionInfo;

	public WebServicesModuleException(WebservicesConstants.WebServicesErrorCodes errorCode) {
		this.errorCode = errorCode;
	}

	public WebServicesModuleException(WebservicesConstants.WebServicesErrorCodes errorCode, String additionInfo) {
		this.errorCode = errorCode;
		this.additionInfo = additionInfo;
	}

	public WebservicesConstants.WebServicesErrorCodes getErrorCode() {
		return errorCode;
	}

	public String getAdditionInfo() {
		return additionInfo;
	}

	public String getErrorMessage() {
		return errorCode.getErrorDescription() + (additionInfo != null ? " : " + additionInfo : "");
	}
}
