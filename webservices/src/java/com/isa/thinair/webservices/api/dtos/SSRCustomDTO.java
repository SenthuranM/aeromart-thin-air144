package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.math.BigDecimal;

import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;

/**
 * 
 * @author rumesh
 * 
 */

public class SSRCustomDTO extends SSRRequest implements Serializable {
	private static final long serialVersionUID = -4960309702116657456L;

	private BigDecimal serviceCharge;
	private int segmentSequence;

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}
}
