package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.PaymentDetailType;

import com.isa.thinair.webservices.core.util.CommonServicesUtil;

public class ExternalReferenceValidationDTO implements Serializable {
	
	private static final long serialVersionUID = 8965352314578956423L;
	
	private String directBillId;
	
	private BigDecimal totalAmount = new BigDecimal(0);
	
	private boolean isPaymentExist;
	
	private List<PaymentDetailType> paymentDetails;
	
	public ExternalReferenceValidationDTO(OTAAirBookRQ otaAirBookRQ) {
		this.directBillId = CommonServicesUtil.getDirectBillId((otaAirBookRQ != null) ? otaAirBookRQ.getFulfillment() : null);
		this.totalAmount = CommonServicesUtil.getAmount((otaAirBookRQ != null) ? otaAirBookRQ.getFulfillment() : null);
		if (otaAirBookRQ != null && otaAirBookRQ.getFulfillment() != null) {
			this.isPaymentExist = true;
			this.paymentDetails = otaAirBookRQ.getFulfillment().getPaymentDetails().getPaymentDetail();
		}
	}
	
	public ExternalReferenceValidationDTO(OTAAirBookModifyRQ otaAirBookModifyRQ) {
		this.directBillId = CommonServicesUtil.getDirectBillId((otaAirBookModifyRQ != null) ? otaAirBookModifyRQ
				.getAirBookModifyRQ() : null);
		this.totalAmount = CommonServicesUtil.getAmount((otaAirBookModifyRQ != null)
				? otaAirBookModifyRQ.getAirBookModifyRQ()
				: null);
		if (otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment() != null) {
			this.isPaymentExist = true;
			this.paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails().getPaymentDetail();
		}
	}

	public String getDirectBillId() {
		return directBillId;
	}

	public void setDirectBillId(String directBillId) {
		this.directBillId = directBillId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public boolean isPaymentExist() {
		return isPaymentExist;
	}

	public void setPaymentExist(boolean isPaymentExist) {
		this.isPaymentExist = isPaymentExist;
	}

	public List<PaymentDetailType> getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(List<PaymentDetailType> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}   
}
