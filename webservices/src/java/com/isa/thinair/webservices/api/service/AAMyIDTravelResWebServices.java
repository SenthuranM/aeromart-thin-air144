package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Action;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellResponse;

/**
 * @author Manoj Dhanushka
 */
@WebService(name = "AAMyIDTravelResWebServices", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAMyIDTravelResWebServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AAMyIDTravelResWebServices {

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "getAvailability", action = "urn:getAvailability")
	@Action(input = "urn:getAvailability", output = "urn:getAvailability")
	@WebResult(name = "availabilityResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterAvailabilityResponse
			getAvailability(
					@WebParam(name = "availabilityRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterAvailabilityRequest myIDTravelResAdapterAvailabilityRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "doSegmentSell", action = "urn:doSegmentSell")
	@Action(input = "urn:doSegmentSell", output = "urn:doSegmentSell")
	@WebResult(name = "segmentSellResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterSegmentSellResponse
			doSegmentSell(
					@WebParam(name = "segmentSellRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterSegmentSellRequest myIDTravelResAdapterSegmentSellRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "cancelSegmentSell", action = "urn:cancelSegmentSell")
	@Action(input = "urn:cancelSegmentSell", output = "urn:cancelSegmentSell")
	@WebResult(name = "cancelSegmentSellResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterCancelSegmentSellResponse
			cancelSegmentSell(
					@WebParam(name = "cancelSegmentSellRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterCancelSegmentSellRequest myIDTravelResAdapterCancelSegmentSellRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "pricing", action = "urn:pricing")
	@Action(input = "urn:pricing", output = "urn:pricing")
	@WebResult(name = "pricingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterPricingResponse
			pricing(@WebParam(name = "pricingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterPricingRequest myIDTravelResAdapterPricingRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "book", action = "urn:book")
	@Action(input = "urn:book", output = "urn:book")
	@WebResult(name = "bookingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterBookingResponse
			book(@WebParam(name = "bookingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterBookingRequest myIDTravelResAdapterBookingRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "retrieveBooking", action = "urn:retrieveBooking")
	@Action(input = "urn:retrieveBooking", output = "urn:retrieveBooking")
	@WebResult(name = "retrieveBookingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterRetrieveBookingResponse
			retrieveBooking(
					@WebParam(name = "retrieveBookingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterRetrieveBookingRequest myIDTravelResAdapterRetrieveBookingRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "retrieveTicket", action = "urn:retrieveTicket")
	@Action(input = "urn:retrieveTicket", output = "urn:retrieveTicket")
	@WebResult(name = "retrieveTicketResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterRetrieveTicketResponse
			retrieveTicket(
					@WebParam(name = "retrieveTicketRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterRetrieveTicketRequest myIDTravelResAdapterRetrieveTicketRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "cancelBooking", action = "urn:cancelBooking")
	@Action(input = "urn:cancelBooking", output = "urn:cancelBooking")
	@WebResult(name = "cancelBookingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterCancelBookingResponse
			cancelBooking(
					@WebParam(name = "cancelBookingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterCancelBookingRequest myIDTravelResAdapterCancelBookingRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "modifyBooking", action = "urn:modifyBooking")
	@Action(input = "urn:modifyBooking", output = "urn:modifyBooking")
	@WebResult(name = "modifyBookingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterBookingModifyResponse
			modifyBooking(
					@WebParam(name = "modifyBookingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterBookingModifyRequest myIDTravelResAdapterBookingModifyRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "refundTicket", action = "urn:refundTicket")
	@Action(input = "urn:refundTicket", output = "urn:refundTicket")
	@WebResult(name = "refundTicketResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterRefundTicketResponse
			refundTicket(
					@WebParam(name = "refundTicketRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterRefundTicketRequest myIDTravelResAdapterRefundTicketRequest);

	@WebServiceSource(source = WebServiceSource.MethodSource.MYIDTRAVEL)
	@WebMethod(operationName = "ping", action = "urn:ping")
	@Action(input = "urn:ping", output = "urn:ping")
	@WebResult(name = "pingResponse", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com")
	public
			MyIDTravelResAdapterPingResponse
			ping(@WebParam(name = "pingRequest", targetNamespace = "http://service.resadapter.myidtravel.lhsystems.com") MyIDTravelResAdapterPingRequest myIDTravelResAdapterPingRequest);

}