package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRQ;
import ae.isa.service.meal.AAMealServicesMealRecordsByFltSegRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;

@WebService(name = "AAWebServicesForMealServices", targetNamespace = "http://www.isa.ae/service/meal",
		endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForMealServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL,
		parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AAWebServicesForMealServices {

	@WebServiceSource(source = WebServiceSource.MethodSource.MEAL_SERVICES)
	@WebMethod(operationName = "mealRecordsByFlightSegment", action = "mealRecordsByFlightSegment")
	@WebResult(name = "AA_MealServices_MealRecordsByFltSegRS", targetNamespace = "http://www.isa.ae/service/meal")
	public AAMealServicesMealRecordsByFltSegRS mealRecordsByFlightSegment (
			@WebParam(name = "AA_MealServices_MealRecordsByFltSegRQ", targetNamespace = "http://www.isa.ae/service/meal")
			AAMealServicesMealRecordsByFltSegRQ mealRecordsByFltSegRQ);

}