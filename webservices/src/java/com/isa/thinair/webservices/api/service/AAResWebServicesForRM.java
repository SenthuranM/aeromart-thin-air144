package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInvBatchUpdateRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventryBatchUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRS;

/**
 * 
 * @author Manoj Dhanushka
 *
 */
@WebService(name = "AAResWebServicesForRM", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForRM")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAResWebServicesForRM {
	
	/**
	 * For batch update of the inventories.
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "batchUpdateFlightInventory", action = "batchUpdateFlightInventory")
	@WebResult(name = "AAFlightInvBatchUpdateRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAFlightInvBatchUpdateRS
			batchUpdateFlightInventory(
					@WebParam(name = "AAFlightInvBatchUpdateRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAFlightInventryBatchUpdateRQ aaAAFlightInventoryBatchRQ);
	
	/**
	 * Update Inventory for RM optimization recommendations.
	 * 
	 * @param AAUpdateOptimizedInventoryRQ
	 * @return AAUpdateOptimizedInventoryRS
	 * 
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "updateOptimizedInventory", action = "updateOptimizedInventory")
	@WebResult(name = "AAUpdateOptimizedInventoryRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAUpdateOptimizedInventoryRS
			updateOptimizedInventory(
					@WebParam(name = "AAUpdateOptimizedInventoryRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAUpdateOptimizedInventoryRQ aaUpdateOptimizedInventoryRQ);

	/**
	 * For geting segment wise - agents seat selilng report for a flight
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getAgentSeatSellingReport", action = "getAgentSeatSellingReport")
	@WebResult(name = "AAAgentSeatMovemantRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAAgentSeatMovemantRS
			getAgentSeatSellingReport(
					@WebParam(name = "AAAgentSeatMovemantRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAAgentSeatMovemantRQ aaSeatMovemantRQ);

	/**
	 * For Retrieving Flight Inventory Allocations
	 * 
	 * @param aaAAFlightInvBatchRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getFlightInventoryAllocations", action = "getFlightInventoryAllocations")
	@WebResult(name = "AAFlightInventoryDataRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory")
	public
			AAFlightInventoryDataRS
			getFlightInventoryAllocations(
					@WebParam(name = "AAFlightInventorySearchRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/airinventory") AAFlightInventorySearchRQ searchRQ);


}
