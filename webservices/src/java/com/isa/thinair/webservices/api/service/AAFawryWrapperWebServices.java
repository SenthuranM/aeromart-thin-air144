/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.fawryis.ebpp.fawryswitch.bussinessfacade.ProcessRequest;
import com.fawryis.ebpp.fawryswitch.bussinessfacade.ProcessRequestResponse;
import com.isa.thinair.webservices.core.annotations.WebServiceSource;

/**
 * Web Service(s) to support Fawry EBPP services
 * 
 * @author Farooq Ali Sheikh
 * 
 * 
 */

@WebService(name = "EBPPSwitchBeanService", targetNamespace = "http://bussinessfacade.fawryswitch.ebpp.fawryis.com/", endpointInterface = "com.isa.thinair.webservices.api.service.AAFawryWrapperWebServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAFawryWrapperWebServices {

	/**
	 * Fawry Process Request service. Fawry can use this service to invoke process request.
	 * 
	 * @param processRequest
	 * @return processRequestResponse
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.FAWRY)
	@WebMethod(operationName = "processRequest", action = "processRequest")
	@WebResult(name = "processRequestResponse", targetNamespace = "http://bussinessfacade.fawryswitch.ebpp.fawryis.com/")
	public
			ProcessRequestResponse
			processRequest(
					@WebParam(name = "processRequest", targetNamespace = "http://bussinessfacade.fawryswitch.ebpp.fawryis.com/") ProcessRequest processRequest);

}