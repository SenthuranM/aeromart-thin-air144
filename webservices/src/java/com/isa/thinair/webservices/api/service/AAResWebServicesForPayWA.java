/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRQ;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRS;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.opentravel.ota._2003._05.OTAReadRQ;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

/**
 * @author Mohamed Nasly
 * 
 *         Web Service(s) for core reservation services
 */

@WebService(name = "AAResWebServicesForPayWA", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForPayWA")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAResWebServicesForPayWA {

	/**
	 * OTA Ping service. Clients can use this service for health check.
	 * 
	 * @param oTAPingRQ
	 * @return OTAPingRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "ping", action = "ping")
	@WebResult(name = "OTA_PingRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public OTAPingRS ping(
			@WebParam(name = "OTA_PingRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAPingRQ oTAPingRQ);

	/**
	 * Returns detailed reservation information. Clients should always call the service with empty transaction
	 * specified.
	 * 
	 * @param oTAReadRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRES_NEW, boundry = WebServiceTransaction.TransactionBoundry.START)
	@WebMethod(operationName = "getReservationbyPNR", action = "getReservationbyPNR")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			getReservationbyPNR(
					@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ oTAReadRQ,
					@WebParam(name = "AAReadRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAReadRQExt aaReadRQExt);

	/**
	 * Modify Reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @return OTAAirBookRS
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.MANDATORY, boundry = WebServiceTransaction.TransactionBoundry.MIDDLE)
	@WebMethod(operationName = "modifyReservation", action = "modifyReservation")
	@WebResult(name = "OTA_AirBookRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			OTAAirBookRS
			modifyReservation(
					@WebParam(name = "OTA_AirBookModifyRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAAirBookModifyRQ otaAirBookModifyRQ,
					@WebParam(name = "AAAirBookModifyRQExt", targetNamespace = "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05") AAAirBookModifyRQExt aaAirModifyRQExt);

	/**
	 * Reconciles AA transactions with Bank transactions.
	 * 
	 * @param transactionsReconRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "reconPaymentTransactions", action = "reconPaymentTransactions")
	@WebResult(name = "AA_OTA_TransactionsReconRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTATransactionsReconRS
			reconPaymentTransactions(
					@WebParam(name = "AA_OTA_TransactionsReconRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTATransactionsReconRQ aaOTATransactionsReconRQ);

	/**
	 * Returns Itinerary
	 * 
	 * @param otaReadRQ
	 * @return
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebMethod(operationName = "getItinerary", action = "getItinerary")
	@WebResult(name = "AA_OTA_ItineraryRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public AAOTAItineraryRS getItinerary(
			@WebParam(name = "OTA_ReadRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") OTAReadRQ otaReadRQ);
}