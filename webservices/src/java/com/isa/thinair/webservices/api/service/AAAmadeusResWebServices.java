package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRQ;
import com.amadeus.ama._2008._03.AMATLAGetAvailabilityAndPriceRS;
import com.amadeus.ama._2008._03.AMATLAGetBookingRQ;
import com.amadeus.ama._2008._03.AMATLAGetBookingRS;
import com.amadeus.ama._2008._03.AMATLAGetFareRulesRQ;
import com.amadeus.ama._2008._03.AMATLAGetFareRulesRS;
import com.amadeus.ama._2008._03.AMATLAPayAndBookRQ;
import com.amadeus.ama._2008._03.AMATLAPayAndBookRS;
import com.amadeus.ama._2008._03.AMATLAPricingRQ;
import com.amadeus.ama._2008._03.AMATLAPricingRS;
import com.amadeus.ama._2008._03.AMATLARepricingRQ;
import com.amadeus.ama._2008._03.AMATLARepricingRS;
import com.isa.thinair.webservices.core.annotations.WebServiceSource;

@WebService(name = "AAAmadeusResWebServices", targetNamespace = "http://www.amadeus.com/AMA/2008/03", endpointInterface = "com.isa.thinair.webservices.api.service.AAAmadeusResWebServices")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AAAmadeusResWebServices {

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "FareRules", action = "")
	@WebResult(name = "AMA_TLA_GetFareRulesRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLAGetFareRulesRS
			fareRules(
					@WebParam(name = "AMA_TLA_GetFareRulesRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLAGetFareRulesRQ AMA_TLA_GetFareRulesRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "PayAndBook", action = "")
	@WebResult(name = "AMA_TLA_PayAndBookRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLAPayAndBookRS
			payAndBook(
					@WebParam(name = "AMA_TLA_PayAndBookRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLAPayAndBookRQ AMA_TLA_PayAndBookRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "Pricing", action = "")
	@WebResult(name = "AMA_TLA_PricingRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLAPricingRS
			pricing(@WebParam(name = "AMA_TLA_PricingRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLAPricingRQ AMA_TLA_PricingRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "Repricing", action = "")
	@WebResult(name = "AMA_TLA_RepricingRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLARepricingRS
			repricing(
					@WebParam(name = "AMA_TLA_RepricingRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLARepricingRQ AMA_TLA_RepricingRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "Booking", action = "")
	@WebResult(name = "AMA_TLA_GetBookingRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLAGetBookingRS
			booking(@WebParam(name = "AMA_TLA_GetBookingRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLAGetBookingRQ AMA_TLA_GetBookingRQ);

	@WebServiceSource(source = WebServiceSource.MethodSource.AMADEUS)
	@WebMethod(operationName = "AvailabilityAndPrice", action = "")
	@WebResult(name = "AMA_TLA_GetAvailabilityAndPriceRS", targetNamespace = "http://www.amadeus.com/AMA/2008/03")
	public
			AMATLAGetAvailabilityAndPriceRS
			availabilityAndPrice(
					@WebParam(name = "AMA_TLA_GetAvailabilityAndPriceRQ", targetNamespace = "http://www.amadeus.com/AMA/2008/03") AMATLAGetAvailabilityAndPriceRQ AMA_TLA_GetAvailabilityAndPriceRQ);
}
