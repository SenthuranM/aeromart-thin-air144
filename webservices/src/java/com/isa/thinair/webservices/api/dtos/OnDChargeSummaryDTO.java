package com.isa.thinair.webservices.api.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class OnDChargeSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8315155224839849112L;

	private String ondCode;

	BigDecimal fareAmount;

	Map<String, BigDecimal> taxes;

	Map<String, BigDecimal> surcharges;

	/**
	 * @return the ondCode
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return the taxes
	 */
	public Map<String, BigDecimal> getTaxes() {
		return taxes;
	}

	/**
	 * @param taxes
	 *            the taxes to set
	 */
	public void addTax(String chargeCode, BigDecimal chargeAmount) {
		if (this.taxes == null) {
			this.taxes = new HashMap<String, BigDecimal>();
		}

		if (this.taxes.containsKey(chargeCode)) {
			this.taxes.put(chargeCode, AccelAeroCalculator.add(this.taxes.get(chargeCode), chargeAmount));
		} else {
			this.taxes.put(chargeCode, chargeAmount);
		}
	}

	/**
	 * @return the surcharges
	 */
	public Map<String, BigDecimal> getSurcharges() {
		return surcharges;
	}

	/**
	 * @param surcharges
	 *            the surcharges to set
	 */
	public void addSurcharge(String chargeCode, BigDecimal chargeAmount) {
		if (this.surcharges == null) {
			this.surcharges = new HashMap<String, BigDecimal>();
		}

		if (this.surcharges.containsKey(chargeCode)) {
			this.surcharges.put(chargeCode, AccelAeroCalculator.add(this.surcharges.get(chargeCode), chargeAmount));
		} else {
			this.surcharges.put(chargeCode, chargeAmount);
		}
	}

}
