package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import ae.isa.api.loginwithtoken.AAAPILoginWithTokenRQ;
import ae.isa.api.loginwithtoken.AAAPILoginWithTokenRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;

@WebService(name = "AAWebServicesForLoginWithTokenAPI", targetNamespace = "http://www.isa.ae/api/loginWithToken", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForLoginWithTokenAPI")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface AAWebServicesForLoginWithTokenAPI {

	@WebServiceSource(source = WebServiceSource.MethodSource.LOGIN_API)
	@WebMethod(operationName = "loginWithToken", action = "loginWithToken")
	@WebResult(name = "AA_API_LoginWithTokenRS", targetNamespace = "http://www.isa.ae/api/loginWithToken")
	public
			AAAPILoginWithTokenRS
			loginWithToken(
					@WebParam(name = "AA_API_LoginWithTokenRQ", targetNamespace = "http://www.isa.ae/api/loginWithToken") AAAPILoginWithTokenRQ userToken);
}
