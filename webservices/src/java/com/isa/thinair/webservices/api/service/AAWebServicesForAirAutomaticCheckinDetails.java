package com.isa.thinair.webservices.api.service;

import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirAutomaticCheckinDetailsRS;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isa.thinair.webservices.core.annotations.WebServiceTransaction;

@WebService(name = "AAWebServicesForAirAutomaticCheckinDetails", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForAirAutomaticCheckinDetails")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAWebServicesForAirAutomaticCheckinDetails {

	@WebServiceSource(source = WebServiceSource.MethodSource.OTA)
	@WebServiceTransaction(value = TransactionAttributeType.REQUIRED, boundry = WebServiceTransaction.TransactionBoundry.STARTORMIDDLE)
	@WebMethod(operationName = "getAutomaticCheckinDetails", action = "getAutomaticCheckinDetails")
	@WebResult(name = "AA_OTA_AirAutomaticCheckinDetailsRS", targetNamespace = "http://www.opentravel.org/OTA/2003/05")
	public
			AAOTAAirAutomaticCheckinDetailsRS
			getAutomaticCheckinDetails(
					@WebParam(name = "AA_OTA_AirAutomaticCheckinDetailsRQ", targetNamespace = "http://www.opentravel.org/OTA/2003/05") AAOTAAirAutomaticCheckinDetailsRQ automaticCheckinDetailsRQ);

}