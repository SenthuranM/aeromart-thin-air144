/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.api.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.isa.thinair.webservices.core.annotations.WebServiceSource;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRQ;
import com.isaaviation.thinair.webservices.api.dynbestoffers.AADynBestOffersRS;

/**
 * Web Service(s) for Dynamic Best OFfers
 */

@WebService(name = "AAWebServicesForDynBestOffers", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAWebServicesForDynBestOffers")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AAWebServicesForDynBestOffers {

	/**
	 * For return best offers
	 */
	@WebServiceSource(source = WebServiceSource.MethodSource.AA)
	@WebMethod(operationName = "getDynBestOffers", action = "getDynBestOffers")
	@WebResult(name = "AADynBestOffersRS", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/dynbestoffers")
	public
			AADynBestOffersRS
			getDynBestOffers(
					@WebParam(name = "AADynBestOffersRQ", targetNamespace = "http://www.isaaviation.com/thinair/webservices/api/dynbestoffers") AADynBestOffersRQ aaDynBestOffersRQ);
}
