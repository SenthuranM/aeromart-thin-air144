package com.isa.thinair.webservices.helpers;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Mehdi Raza
 *
 */
public abstract class AbstractHelper extends TestCase implements IRequestObject {

	protected Log log=LogFactory.getLog(getClass());

	public boolean requiresAuthentication() {
		return true;
	}
	
	public String getPrincipal() {		
		return Constants.TEST_AGENT_TEST_USER;
	}
	
	public void assertResponse(Object response) {
		System.out.println("response not asserted: "+response);
	}
}
