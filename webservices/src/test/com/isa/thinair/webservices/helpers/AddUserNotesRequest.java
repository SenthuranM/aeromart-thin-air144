/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNoteType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNotesType;


public class AddUserNotesRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, ModuleException {
		AirReservationType reservationType = (AirReservationType) helpers[0];
		OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();
		otaAirBookModifyRQ.setPOS(new POSType());
    	otaAirBookModifyRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	otaAirBookModifyRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirBookModifyRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirBookModifyRQ.setEchoToken(UID.generate());
		otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirBookModifyRQ.setTransactionIdentifier(((OTAAirBookRS) helpers[2]).getTransactionIdentifier());
		
		otaAirBookModifyRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		otaAirBookModifyRQ.getAirBookModifyRQ().setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_NOTES));
        
        otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().add(reservationType.getBookingReferenceID().get(0));
        
        AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
        airBookModifyRQAAExt.setUserNotes(transformToWSUserNotes("dasdasdasdas"));
        
        airBookModifyRQAAExt.setAALoadDataOptions((AALoadDataOptionsType)helpers[1]);
        
//        otaAirBookModifyRQ.getAirBookModifyRQ().setTPAExtensions(new TPAExtensionsType());
//        otaAirBookModifyRQ.getAirBookModifyRQ().getTPAExtensions().getAny().add(TPAExtensionUtil.getInstance().marshall(airBookModifyRQAAExt));
        

		return new Object[]{otaAirBookModifyRQ, airBookModifyRQAAExt};
	}
    

    private AAUserNotesType transformToWSUserNotes(String userNote){
        AAUserNotesType otaUserNotes = new AAUserNotesType();

        AAUserNoteType aaUserNote = new AAUserNoteType();
        aaUserNote.setNoteText(userNote);
        otaUserNotes.getUserNote().add(aaUserNote);
        
        return otaUserNotes;
    }
}
