package com.isa.thinair.webservices.helpers.cesar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.Constants;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRQ;


/**
 * @author Byorn
 */
public class PaxTypeSummaryRequest extends AbstractHelper {

    public String getOperation(){       
        return "getPaxTypesForFlightLeg";
    }
    
	public Object[] getRequestObject(Object[] helpers) throws Exception {
	       
			AAFligthtLegPaxCountInfoRQ rq = new AAFligthtLegPaxCountInfoRQ();
	    
			
			GregorianCalendar gc = new GregorianCalendar(2009, Calendar.MARCH,1);
		
			
			
			rq.setUserId(Constants.TEST_AGENT_TEST_USER);
			rq.setChannelId(Constants.BOOKING_CHANNEL_TYPE);
			rq.setFlightNumber("G9505");
			rq.setOrigin("SHJ");
			rq.setDestination("CMB");
			rq.setDepartureZulu(CommonUtil.parse(gc.getTime()));
	    
			return new Object[]{rq}; 
    }
}
