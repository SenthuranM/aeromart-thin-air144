package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRQ;
import org.opentravel.ota._2003._05.AAPNRPaymentTransactionType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.POSType;


import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

public class ExtPayReconciliationRequest extends AbstractHelper {

	public void assertResponse(Object response) {
		// TODO Auto-generated method stub
	}

	public String getOperation() {
		return "reconPaymentTransactions";
	}


	public Object[] getRequestObject(Object[] helpers) throws Exception {
		AAOTATransactionsReconRQ reconRQ = new AAOTATransactionsReconRQ();
		
		reconRQ.setPOS(new POSType());
		reconRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	
		reconRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		reconRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		reconRQ.setSequenceNmbr(new BigInteger("1"));
		reconRQ.setEchoToken(UID.generate());
		reconRQ.setTimeStamp(CommonUtil.parse(new Date()));
		reconRQ.setTransactionIdentifier(null);
		
		AAPNRPaymentTransactionType pnrPayTnx = new AAPNRPaymentTransactionType();
		pnrPayTnx.setPNRNo("11678834");
		
		AAPNRPaymentTransactionType.PaymentTransaction payTnx = new AAPNRPaymentTransactionType.PaymentTransaction();
		payTnx.setAARefNo("11678834GG04140540812");
		payTnx.setAmount(270);
		payTnx.setBankChannel(Constants.BOOKING_CHANNEL_TYPE);
		payTnx.setBankRefNo("2");
		payTnx.setBankStatus(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_BANK_EXCESS));
		
		pnrPayTnx.getPaymentTransaction().add(payTnx);
		
		AAPNRPaymentTransactionType.PaymentTransaction payTnx1 = new AAPNRPaymentTransactionType.PaymentTransaction();
		payTnx1.setAARefNo("11678834GG04140540812");
		payTnx1.setAmount(270);
		payTnx1.setBankChannel(Constants.BOOKING_CHANNEL_TYPE);
		payTnx1.setBankRefNo("2");
		payTnx1.setBankStatus(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_BANK_EXCESS));
		
		pnrPayTnx.getPaymentTransaction().add(payTnx1);
		
		reconRQ.getPNRPaymentTransactions().add(pnrPayTnx);
		
		AAPNRPaymentTransactionType pnrPayTnx1 = new AAPNRPaymentTransactionType();
		pnrPayTnx1.setPNRNo("11678834");
		
		AAPNRPaymentTransactionType.PaymentTransaction payTnx2 = new AAPNRPaymentTransactionType.PaymentTransaction();
		payTnx2.setAARefNo("11678834GG04140540812");
		payTnx2.setAmount(270);
		payTnx2.setBankChannel(Constants.BOOKING_CHANNEL_TYPE);
		payTnx2.setBankRefNo("2");
		payTnx2.setBankStatus(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_BANK_EXCESS));
		
		pnrPayTnx1.getPaymentTransaction().add(payTnx);
		
		reconRQ.getPNRPaymentTransactions().add(pnrPayTnx1);
		
		return new Object [] {reconRQ};
	}

}
