package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * 
 * @author Mehdi Raza
 * @author Mohamed Nasly
 *
 */
public class AvailabilityRequest extends AbstractHelper {

	public String getOperation() {		
		return "getAvailability";
	}

	public Object[] getRequestObject(Object[] helpers) throws WebservicesException, ModuleException {
		OTAAirAvailRQ otaAirAvailRQ = new OTAAirAvailRQ();
        otaAirAvailRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        otaAirAvailRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
        otaAirAvailRQ.setTimeStamp(CommonUtil.parse(new Date()));
        otaAirAvailRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirAvailRQ.setEchoToken(UID.generate());
        otaAirAvailRQ.setTarget(Constants.TARGET_NAME);
        if (helpers[1] != null){
        	OTAAirBookRS airbookRS = (OTAAirBookRS) helpers[1];
        	otaAirAvailRQ.setTransactionIdentifier(airbookRS.getTransactionIdentifier());
        } else {
        	otaAirAvailRQ.setTransactionIdentifier(null);
        }
        
        POSType pos = new POSType();
        otaAirAvailRQ.setPOS(pos);
        pos.getSource().add(CommonTestUtils.preparePOS());
       
        if (helpers[2] == null) { //fixed segment
			if (Constants.JourneyType.RETURN.equals((String) helpers[0])) {
				setReturnSegments(otaAirAvailRQ);
			} else {
				setOnewaySegment(otaAirAvailRQ);
			}
		}else { 
			otaAirAvailRQ.getOriginDestinationInformation().add((OTAAirAvailRQ.OriginDestinationInformation)helpers[2]);
		}
		TravelerInfoSummaryType travellerInfoSummary = new TravelerInfoSummaryType();
        otaAirAvailRQ.setTravelerInfoSummary(travellerInfoSummary);
        
        TravelerInformationType travelerInfo = new TravelerInformationType();
        travellerInfoSummary.getAirTravelerAvail().add(travelerInfo);
        
        PassengerTypeQuantityType paxQuantity = new PassengerTypeQuantityType();
        paxQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
        paxQuantity.setQuantity(1);
        travelerInfo.getPassengerTypeQuantity().add(paxQuantity);
        
//        PassengerTypeQuantityType paxQuantityInf = new PassengerTypeQuantityType();
//        paxQuantityInf.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
//        paxQuantityInf.setQuantity(1);
//        travelerInfo.getPassengerTypeQuantity().add(paxQuantityInf);
        
        return new Object[]{otaAirAvailRQ};
	}

	private void setOnewaySegment(OTAAirAvailRQ otaAirAvailRQ) throws WebservicesException, ModuleException {
		Calendar deparuteDateCal = new GregorianCalendar();
        deparuteDateCal.set(deparuteDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 30, 0, 0, 0);
        
        Calendar arrivalDateCal = new GregorianCalendar();
        arrivalDateCal.set(arrivalDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 30, 0, 0, 0);
        
		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation("SHJ", "CMB", deparuteDateCal, arrivalDateCal));
	}
	
	private void setReturnSegments(OTAAirAvailRQ otaAirAvailRQ) throws WebservicesException, ModuleException {
		Calendar obDeparuteDateCal = new GregorianCalendar();
        obDeparuteDateCal.set(obDeparuteDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 25, 0, 0, 0);
        
        Calendar obArrivalDateCal = new GregorianCalendar();
        obArrivalDateCal.set(obArrivalDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 25, 0, 0, 0);
        
		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation("SHJ", "CMB", obDeparuteDateCal, obArrivalDateCal));
		
		Calendar ibDeparuteDateCal = new GregorianCalendar();
        ibDeparuteDateCal.set(ibDeparuteDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 30, 0, 0, 0);
        
        Calendar ibArrivalDateCal = new GregorianCalendar();
        ibArrivalDateCal.set(ibArrivalDateCal.get(Calendar.YEAR), Calendar.SEPTEMBER, 30, 0, 0, 0);
        
		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation("CMB", "SHJ", ibDeparuteDateCal, ibArrivalDateCal));
	}
	
	public static OTAAirAvailRQ.OriginDestinationInformation getOriginDestinationInformation(
			String originAirport, String destAirport, 
			Calendar depDate, Calendar arrDate) throws WebservicesException, ModuleException{
		OTAAirAvailRQ.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRQ.OriginDestinationInformation();
        
        
        OriginDestinationInformationType.OriginLocation origin = new OriginDestinationInformationType.OriginLocation();
        origin.setLocationCode(originAirport);
        OriginDestinationInformationType.DestinationLocation destination = new OriginDestinationInformationType.DestinationLocation();
        destination.setLocationCode(destAirport);
        originDestinationInformation.setOriginLocation(origin);
        originDestinationInformation.setDestinationLocation(destination);

        TimeInstantType departureDate = new TimeInstantType();
        
        departureDate.setValue(CommonUtil.getFormattedDate(depDate.getTime()));
        
        TimeInstantType arrivalDate = new TimeInstantType();
        
        arrivalDate.setValue(CommonUtil.getFormattedDate(arrDate.getTime()));
              
        originDestinationInformation.setDepartureDateTime(departureDate);
        originDestinationInformation.setArrivalDateTime(arrivalDate);
        
        return originDestinationInformation;
	}
}
