package com.isa.thinair.webservices.helpers;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.xfire.util.DOMUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class UpdatePersonTest 
	extends AbstractHelper {
	
	public void assertResponse(Object response) {
		assertTrue(true);
	}

	public String getOperation() {
		return "updatePerson";
	}

	public Object[] getRequestObject(Object[] helpers) throws Exception, JAXBException {
		
//		PersonX personX = new PersonX();
//		personX.setName("Nasly");
//		personX.setAge(27);
//		BriefDescriptonX briefDescriptonX = new BriefDescriptonX();
//		briefDescriptonX.setBriefDescripton("Programmer");
//		briefDescriptonX.setVersion("ver1");
//		
////		BriefDescriptonTypeX briefDescripton = new BriefDescriptonTypeX();
////		CountryTypeX country = new CountryTypeX();
////		country.setCountryName("Sri Lanka");
////		country.setContinent("x");
////		briefDescripton.setCountry(country);
////		
////		briefDescriptonX.setBriefDescripton("Im a programmer");
////		briefDescriptonX.setVersion("ver1");
////		
////		EmployerTypeX employerType = new EmployerTypeX();
////		employerType.setEmployerName("ISA");
////		
////		briefDescripton.setEmployer(employerType);
//		
////		Element e = marshall(briefDescriptonX, BriefDescriptonX.class);
//		
//		marshalSysOut(briefDescriptonX, BriefDescriptonX.class);
//		
////		Element e = marshalNonRootElement(briefDescripton, BriefDescriptonTypeX.class, "BriefDescriptonX");
//		
//		Element e = marshall(briefDescriptonX, BriefDescriptonX.class);
//		
//		personX.setDescriptonsX(new DescriptonsTypeX());
//		personX.getDescriptonsX().getAny().add(e);
//		
////		List<String> pkgs = new ArrayList<String>();
//////		pkgs.add("com.isaaviation.thinair.webservices.ota.extensions._2003._05");
////		pkgs.add("org.opentravel.ota._2003._05");
////		
////		PersonX p  = (PersonX) unmarshalFromFile(pkgs, new File("D://Projects//ThinAir//samplemessages//payload.xml"));
////		
//		marshalSysOut(personX, PersonX.class);
		
		return new Object[] { null };

	}
	
	public Object unmarshalFromFile(List<String> pkgs, File file) throws JAXBException{
		boolean first = true;
		StringBuffer pckgs = new StringBuffer();
		for (String p : pkgs)
        {
            if (!first) pckgs.append(":");
            else first = false;
            
            pckgs.append(p);
        }
        String pckg = pckgs.toString();
        
		JAXBContext jc = JAXBContext.newInstance( pckg );
		Unmarshaller u = jc.createUnmarshaller();
		return u.unmarshal( file );
	}
	
	public Element marshalNonRootElement(Object o, Class c, String rootTag) throws Exception{
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Document doc= DOMUtils.createDocument();
			marshaller.marshal( new JAXBElement(
					  new QName("http://www.opentravel.org/OTA/2003/05", rootTag), c, o), doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new Exception(e);
		}
	}
	
	public void marshalSysOut(Object o, Class c, String rootTag) throws Exception{
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			marshaller.marshal( new JAXBElement(
					  new QName("", rootTag), c, o), System.out);
		} catch (JAXBException e) {
			throw new Exception(e);
		}
	}
	
	public void marshalSysOut(Object o, Class c) throws Exception{
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			marshaller.marshal( o, System.out);
		} catch (JAXBException e) {
			throw new Exception(e);
		}
	}
	
	public Element marshall(Object o, Class c) throws Exception {
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Document doc=getNewDocument();
			marshaller.marshal(o, doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new Exception(e);
		}
	}
	
	private Document getNewDocument() {
		factory=DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			builder=factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return builder.newDocument();
	}

	private DocumentBuilderFactory factory=null;
	DocumentBuilder builder=null;
}
