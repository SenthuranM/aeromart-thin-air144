package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.UniqueIDType;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAReadRQExt;

public class RetreiveResRequest extends AbstractHelper {
	public String getOperation() {
		return "getReservationbyPNR";
	}
	
	public Object[] getRequestObject(Object[] helpers) throws Exception {		
		String pnr=(String)helpers[0];
		OTAReadRQ request=new OTAReadRQ();
		
		request.setPOS(new POSType());
		SourceType pos = CommonTestUtils.preparePOS();
    	request.getPOS().getSource().add(pos);
    	
    	request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(null);
		
		ReadRequest readRequest=null;
		request.setReadRequests(new ReadRequests());
		request.getReadRequests().getReadRequest().add(readRequest=new ReadRequest());
		readRequest.setUniqueID(new UniqueIDType());
		readRequest.getUniqueID().setID(pnr);
		readRequest.getUniqueID().setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		
//		if (pos.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_EBI_ATM)) ||
//				pos.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_EBI_CDM)) ){
//		
		if (pos.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA)) ||
				pos.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA)) ){
			
			OTAReadRQ.ReadRequests.AirReadRequest airReadRequest = new OTAReadRQ.ReadRequests.AirReadRequest();
			request.getReadRequests().getAirReadRequest().add(airReadRequest);
			
			Calendar cal = new GregorianCalendar();
			cal.set(Calendar.YEAR, 2007);
			cal.set(Calendar.MONTH, Calendar.OCTOBER);
			cal.set(Calendar.DAY_OF_MONTH, 22);
			
			airReadRequest.setDepartureDate(CommonUtil.getXMLGregorianCalendar(cal));
		}
		
		AAReadRQExt aaReadRQExt = new AAReadRQExt();
		aaReadRQExt.setAALoadDataOptions((AALoadDataOptionsType)helpers[1]);
		
//		Element readRQExtElement = marshalNonRootElement(aaReadRQExt, AAReadRQExt.class, TPAExtensionUtil.RootTags.AA_READRQ_EXT);
//		
//		request.setTPAExtensions(new TPAExtensionsType());
//		request.getTPAExtensions().getAny().add(readRQExtElement);
		
		return new Object[]{request, aaReadRQExt};
	}
	
	@Override
	public void assertResponse(Object response) {		
		log.debug("asserting response..."+response);
      OTAAirBookRS airBookRS = (OTAAirBookRS) response;
      assertTrue(airBookRS.getSuccessAndWarningsAndAirReservation().size()>0);
     System.out.println(airBookRS.getSuccessAndWarningsAndAirReservation().size());
	}
	
	
	public Element marshalNonRootElement(Object o, Class c, String rootTag) throws WebservicesException{
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			Document doc=getNewDocument();
			marshaller.marshal( new JAXBElement(
					  new QName("http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05", rootTag), c, o), doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}
	
	public Element marshall(Object o, Class c) throws WebservicesException {
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Document doc=getNewDocument();
			marshaller.marshal(o, doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}
	
	private Document getNewDocument() {
		factory=DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			builder=factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return builder.newDocument();
	}

	private DocumentBuilderFactory factory=null;
	DocumentBuilder builder=null;
}
