/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AAOTAAgentAvailCreditRQ;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

import com.isa.thinair.webservices.api.util.WebservicesConstants;


public class AgentCreditLimitRequest extends AbstractHelper {

	public String getOperation() {		
		return "getAgentAvailableCredit";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) 
            throws DatatypeConfigurationException, WebservicesException, ModuleException {
        
        AAOTAAgentAvailCreditRQ request=new AAOTAAgentAvailCreditRQ();
        
        POSType pos = new POSType();
		request.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);
        
        request.setAgentID("AMM005");
        request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        request.setVersion(WebservicesConstants.OTAConstants.VERSION_AgentAvailCredit);
        request.setSequenceNmbr(new BigInteger("1"));
        request.setEchoToken(UID.generate());
        request.setTimeStamp(CommonUtil.parse(new Date()));
        request.setTransactionIdentifier(null);
		return new Object[]{request};
	}
}
