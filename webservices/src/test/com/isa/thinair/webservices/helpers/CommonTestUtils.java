package com.isa.thinair.webservices.helpers;

import java.util.List;

import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SourceType.RequestorID;

import com.isa.thinair.webplatform.core.util.CommonUtil;



public class CommonTestUtils {

	public static SourceType preparePOS(){
		SourceType sourceType = new SourceType();
		sourceType.setTerminalID("TestUser/Test Runner");
		
		RequestorID requestorID = new RequestorID();
		sourceType.setRequestorID(requestorID);
		
		requestorID.setID(Constants.TEST_AGENT_TEST_USER);
		requestorID.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_COMPANY));
		
		SourceType.BookingChannel bookingChannel = new SourceType.BookingChannel();
		sourceType.setBookingChannel(bookingChannel);
		
		//@todo: check
		sourceType.setAirportCode("SHJ");
		
		bookingChannel.setType(Constants.BOOKING_CHANNEL_TYPE);
		
		return sourceType;
	}
	
	public static OriginDestinationOptionType getFirstConfirmedOND(List<OriginDestinationOptionType> originDestinationOption) {
		OriginDestinationOptionType firstConfirmedOND = null;
		for (OriginDestinationOptionType ond : originDestinationOption){
			List<BookFlightSegmentType> fltSegs = ond.getFlightSegment();
			for (BookFlightSegmentType fltSeg : fltSegs){
				if (CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_ACTIVE).equals(fltSeg.getStatus())){
					firstConfirmedOND = ond;
					break;
				}
			}
		}
		return firstConfirmedOND;
	}
}
