package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.CompanyNameType;
import org.opentravel.ota._2003._05.OTAAirFlifoRQ;
import org.opentravel.ota._2003._05.POSType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * @author Mohamed Nasly
 */
public class FlightInfoRequest extends AbstractHelper {

	public String getOperation() {		
		return "getFlightInfo";
	}

	public Object[] getRequestObject(Object[] helpers) throws WebservicesException, ModuleException {
		OTAAirFlifoRQ otaAirFlifoRQ = new OTAAirFlifoRQ();
		otaAirFlifoRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirFlifoRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirFltInfo);
		otaAirFlifoRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirFlifoRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirFlifoRQ.setEchoToken(UID.generate());
        otaAirFlifoRQ.setTarget(Constants.TARGET_NAME);
        
        POSType pos = new POSType();
        otaAirFlifoRQ.setPOS(pos);
        pos.getSource().add(CommonTestUtils.preparePOS());
        
        otaAirFlifoRQ.setFlightNumber("G9505");
        CompanyNameType companyName = new CompanyNameType();
        companyName.setValue("Air Arabia");
        companyName.setCode("G9");
        otaAirFlifoRQ.setAirline(companyName);
        
        Calendar startDateCal = new GregorianCalendar();
        startDateCal.set(2007, Calendar.JULY, 25, 0, 0, 0);
        otaAirFlifoRQ.setDepartureDate(CommonUtil.getXMLGregorianCalendar(startDateCal));
        
        
        
        return new Object[]{otaAirFlifoRQ};
	}
}
