package com.isa.thinair.webservices.helpers;

import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.webplatform.core.util.CommonUtil;




public interface Constants {
	
//	 public static final String TEST_AGENT_CODE = "DXB221";
//	
//	public static final String TEST_AGENT_NAME = "DNATA WS Test Agency";
//	
//	public static final String TEST_AGENT_TEST_USER = "TESTWSDNATA";
//	
//	public static final String TARGET_NAME = "TEST";
//	
//	public static final String BOOKING_CHANNEL_TYPE = CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_DNATA_AGENCIES);
	 
	
//	public static final String TEST_AGENT_CODE = "DXB258";
//	
//	public static final String TEST_AGENT_NAME = "EBI Test Agency";
//	
//	public static final String TEST_AGENT_TEST_USER = "WSEBIATMUSER";
//	
//	public static final String TARGET_NAME = "TEST";
//	
//	public static final String BOOKING_CHANNEL_TYPE = CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_EBI_ATM);
	
//	public static final String TEST_AGENT_CODE = "SHJ98";
//	
//	public static final String TEST_AGENT_NAME = "WS Test Agency";
//	
//	public static final String TEST_AGENT_TEST_USER = "SYSTEM";
//	
//	public static final String TARGET_NAME = "TEST";
//	
//	public static final String BOOKING_CHANNEL_TYPE = CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_EBI_ATM);
//	
//	public static final String TEST_AGENT_CODE = "SHJ001";
//	
//	public static final String TEST_AGENT_NAME = "AAHOL Test Agency";
//	
//	public static final String TEST_AGENT_TEST_USER = "SYSTEM";
//	
//	public static final String TARGET_NAME = "TEST";
//	
//	public static final String BOOKING_CHANNEL_TYPE = CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA);
	
	
//	public static final String TEST_AGENT_CODE = "SHJ001";
//	
//	public static final String TEST_AGENT_NAME = "Air Arabia Head office";
//	
//	public static final String TEST_AGENT_TEST_USER = "SYSTEM";
//	
//	public static final String TARGET_NAME = "TEST";
//	
//	public static final String BOOKING_CHANNEL_TYPE = "12";
	
	
	public static final String TEST_AGENT_CODE = "CMN251";
	
	public static final String TEST_AGENT_NAME = "USDMalaka";
	
	public static final String TEST_AGENT_TEST_USER = "MALAKA";
	
	public static final String TARGET_NAME = "TEST";
	
	public static final String BOOKING_CHANNEL_TYPE = "12";

	

	
	public static interface JourneyType {
		public static final String ONEWAY = "1";
		public static final String RETURN = "2";
	}

}
