/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAddressType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAContactInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACountryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPersonNameType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATelephoneType;


public class UpdateContactDetailsRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) 
            throws DatatypeConfigurationException, WebservicesException, ModuleException {
        
        AirReservationType airReservationType = 
            getAirReservation((OTAAirBookRS) helpers[0]);
        
		OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();
		otaAirBookModifyRQ.setPOS(new POSType());
    	otaAirBookModifyRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	otaAirBookModifyRQ.setPrimaryLangID(
                WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirBookModifyRQ.setVersion(
                WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirBookModifyRQ.setEchoToken(UID.generate());
		otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirBookModifyRQ.setTransactionIdentifier(((OTAAirBookRS) helpers[0]).getTransactionIdentifier());
		
		otaAirBookModifyRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		otaAirBookModifyRQ.getAirBookModifyRQ().setModificationType(
                CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CONTACT_DETAILS_UPDATE));
//        otaAirBookModifyRQ.setAirReservation(airReservationType);

        otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().add(
                airReservationType.getBookingReferenceID().get(0));
        
        AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
        AAContactInfoType contactInfo = new AAContactInfoType();
        contactInfo.setAddress(new AAAddressType());
        contactInfo.getAddress().setAddressLine1("148");
        contactInfo.getAddress().setAddressLine2("Vauxhall Street.");
        contactInfo.getAddress().setAddressLine3("Colombo - 02.");
        contactInfo.getAddress().setCityName("Colombo");
        contactInfo.getAddress().setStateProvinceName("Western");
        contactInfo.getAddress().setCountryName(new AACountryType());
        contactInfo.getAddress().getCountryName().setCountryCode("AE");
        contactInfo.getAddress().getCountryName().setCountryName("AE");
        
        contactInfo.setPersonName(new AAPersonNameType());
        contactInfo.getPersonName().setTitle(
                CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
        contactInfo.getPersonName().setFirstName("Test");
        contactInfo.getPersonName().setLastName("Contact");
        
        contactInfo.setEmail("test@test.lk");
        
        contactInfo.setMobile(new AATelephoneType());
        contactInfo.getMobile().setAreaCode("112");
        contactInfo.getMobile().setCountryCode("0094");
        contactInfo.getMobile().setPhoneNumber("300770");
        
        airBookModifyRQAAExt.setContactInfo(contactInfo);
        airBookModifyRQAAExt.setAALoadDataOptions((AALoadDataOptionsType) helpers[1]);
               
		return new Object[]{otaAirBookModifyRQ, airBookModifyRQAAExt};
	}
    
    /**
     * Retrive the AirReservationType Object from the OTAAirBookRS
     * @param bookResponse
     * @return
     */
    private AirReservationType getAirReservation(OTAAirBookRS bookResponse){
        for (Iterator i=bookResponse.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
            Object o=i.next();
            
            if (o instanceof AirReservation) {
//                return (AirReservationType) o;
                
                AirReservation convert = (AirReservation) o;
                AirReservationType reservation=new AirReservationType();
                reservation.setAirItinerary(convert.getAirItinerary());
                reservation.setFulfillment(convert.getFulfillment());
                reservation.setLastModified(convert.getLastModified());
                reservation.setPriceInfo(convert.getPriceInfo());
                reservation.setQueues(convert.getQueues());
                reservation.setTicketing(convert.getTicketing());
                //reservation.setTPAExtensions(convert.getTPAExtensions());
                reservation.setTravelerInfo(convert.getTravelerInfo());
                reservation.getBookingReferenceID().addAll(convert.getBookingReferenceID());
                return reservation;
            }
        }
       return null;
        
    }
}
