package com.isa.thinair.webservices.helpers.rm;



import java.util.Calendar;
import java.util.GregorianCalendar;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.Constants;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAAgentSeatMovemantRS;


/**
 * @author Byorn
 */
public class AgentSeatSellingReportRequest extends AbstractHelper {

  
    
	public String getOperation(){		
        return "getAgentSeatSellingReport";
	}

	public Object[] getRequestObject(Object[] helpers) throws Exception { 
      AAAgentSeatMovemantRQ agentSeatMovemantRQ = new AAAgentSeatMovemantRQ();
      agentSeatMovemantRQ.setCabinClass("Y");
      agentSeatMovemantRQ.setFlightNumber("G9505");

      Calendar calendar = new GregorianCalendar(2008, Calendar.MARCH, 27);
      agentSeatMovemantRQ.setDepatureDate(CommonUtil.parse(calendar.getTime()));
      agentSeatMovemantRQ.setUserId(Constants.TEST_AGENT_TEST_USER);
      agentSeatMovemantRQ.setChannelId(Constants.BOOKING_CHANNEL_TYPE);
      agentSeatMovemantRQ.setMessageId("123");
      //agentSeatMovemantRQ.set
       return new Object[]{agentSeatMovemantRQ};     
	}
    
     public void assertResponse(Object response) {
         if(response instanceof AAAgentSeatMovemantRS){
             AAAgentSeatMovemantRS rs = (AAAgentSeatMovemantRS)response;
             System.out.println("Is Success ?" + rs.getStatusErrorsAndWarnings().isSuccess());
         }
         System.out.println("response asserted: "+response);
     }
   
}
