/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * @author Mohamed Nasly
 * @author Mehdi Raza
 */
public class PriceQuoteRequest extends AbstractHelper {
	
	public String getOperation() {		
		return "getPrice";
	}

	public Object[] getRequestObject(Object[] helpers) throws WebservicesException, ModuleException {
		
		OTAAirAvailRS otaAirAvailRS=(OTAAirAvailRS)helpers[0];
		//OTAAirPriceRS otaAirPriceRS = null;
		OTAAirPriceRQ otaAirPriceRQ=null;
		
		if (otaAirAvailRS.getSuccess() != null){
			log.info("getAvailability SUCCESS [found " + otaAirAvailRS.getOriginDestinationInformation().size() + " Results]");
        	for (OriginDestinationInformation originDestinationInformationRS : otaAirAvailRS.getOriginDestinationInformation()){
        		log.info("Fligt Details :: [" +
        				"Origin Airport = " + originDestinationInformationRS.getOriginLocation().getLocationCode() +
        				",Destination Airport = " + originDestinationInformationRS.getDestinationLocation().getLocationCode() + 
        				",Departure datetime = " + originDestinationInformationRS.getDepartureDateTime().getValue() + "]");
        		
        		//try {
            		/** price quote */
        			otaAirPriceRQ = new OTAAirPriceRQ();
        			otaAirPriceRQ.setPOS(new POSType());
        			otaAirPriceRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
        	    	
        			otaAirPriceRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        			otaAirPriceRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
        			otaAirPriceRQ.setSequenceNmbr(new BigInteger("1"));
        			otaAirPriceRQ.setEchoToken(UID.generate());
        			otaAirPriceRQ.setTimeStamp(CommonUtil.parse(new Date()));
        			otaAirPriceRQ.setTransactionIdentifier(otaAirAvailRS.getTransactionIdentifier());
                    
                    AirItineraryType airItineraryType = new AirItineraryType();
                    otaAirPriceRQ.setAirItinerary(airItineraryType);
                    airItineraryType.setDirectionInd(AirTripType.ONE_WAY);//FIXME
                    
                    AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
                    airItineraryType.setOriginDestinationOptions(originDestinationOptions);
                    
            		for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption()){
            			OriginDestinationOptionType originDestinationOptionType = new OriginDestinationOptionType();
            			airItineraryType.getOriginDestinationOptions().getOriginDestinationOption().add(originDestinationOptionType);
            			for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption.getFlightSegment()){
            				BookFlightSegmentType bookFlightSegmentType = new BookFlightSegmentType();
            				originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
            				bookFlightSegmentType.setDepartureAirport(flightSegment.getDepartureAirport());
            				bookFlightSegmentType.setArrivalAirport(flightSegment.getArrivalAirport());
            				bookFlightSegmentType.setDepartureDateTime(flightSegment.getDepartureDateTime());
            				bookFlightSegmentType.setArrivalDateTime(flightSegment.getArrivalDateTime());
            				bookFlightSegmentType.setFlightNumber(flightSegment.getFlightNumber());
            			}
            		}
            		
            		TravelerInfoSummaryType travellerInfoSummary1 = new TravelerInfoSummaryType();
                    otaAirPriceRQ.setTravelerInfoSummary(travellerInfoSummary1);
                    
                    TravelerInformationType travelerInfo1 = new TravelerInformationType();
                    travellerInfoSummary1.getAirTravelerAvail().add(travelerInfo1);
                    
                    PassengerTypeQuantityType paxQuantity1 = new PassengerTypeQuantityType();
                    paxQuantity1.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
                    paxQuantity1.setQuantity(2);
                    travelerInfo1.getPassengerTypeQuantity().add(paxQuantity1);
                    
                    PassengerTypeQuantityType paxQuantityInf = new PassengerTypeQuantityType();
                    paxQuantityInf.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
                    paxQuantityInf.setQuantity(1);
                    travelerInfo1.getPassengerTypeQuantity().add(paxQuantityInf);
                    
//                    otaAirPriceRS = aaResWebServices.getPrice(otaAirPriceRQ);
//                    
//                    List<Object> priceRS = otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries();
//                    Iterator<Object> priceRSIt = priceRS.iterator();
//                    while (priceRSIt.hasNext()){
//                    	Object obj= priceRSIt.next();
//                    	if (obj instanceof SuccessType){
//                    		log.info("getPrice successfully proced");
//                    	} else if (obj instanceof WarningsType){
//                    		WarningsType warnings = (WarningsType)obj;
//                    		Iterator<WarningType> warningsIt = warnings.getWarning().iterator();
//                    		while (warningsIt.hasNext()){
//                    			log.info("getPrice WARNING :: " + warningsIt.next().getShortText());
//                    		}
//                    	} else if (obj instanceof PricedItinerariesType){
//                    		PricedItinerariesType pricedItinerariesType = (PricedItinerariesType)obj;
//                    		log.info("getPrice SUCCESS [found " + pricedItinerariesType.getPricedItinerary().size() + " price quotes ]");
//                    		for (PricedItineraryType pricedItineraryType : pricedItinerariesType.getPricedItinerary()){
//                    			AirItineraryPricingInfoType airItineraryPricingInfoType = pricedItineraryType.getAirItineraryPricingInfo();
//                    			log.info("total fare=" + airItineraryPricingInfoType.getItinTotalFare().getTotalFare().getAmount() + " " + 
//                    										airItineraryPricingInfoType.getItinTotalFare().getTotalFare().getCurrencyCode());
//                    			if (airItineraryPricingInfoType.getItinTotalFare().getTaxes() != null){
//	                    			for ( AirTaxType airTaxType : airItineraryPricingInfoType.getItinTotalFare().getTaxes().getTax()){
//	                    				log.info("TAX amount=" + airTaxType.getAmount() + " " + airTaxType.getCurrencyCode() + ",code=" + airTaxType.getTaxCode() +",description=" + airTaxType.getTaxName());
//	                    			}
//                    			}
//                    			
//                    			if (airItineraryPricingInfoType.getItinTotalFare().getFees() != null){
//	                    			for (AirFeeType airFeeType : airItineraryPricingInfoType.getItinTotalFare().getFees().getFee()){
//	                    				log.info("SURCHARGE amount=" + airFeeType.getAmount() + " " + airFeeType.getCurrencyCode() + ",code=" + airFeeType.getFeeCode());
//	                    			}
//                    			}
//                    		}
//                    	}
//                    }
//        		} catch (Exception ex){
//        			log.error("WsClient.callWebService(): EXCEPTION: getPrice :: " + ex.toString());
//        			throw ex;
//        		}
        	}	
        	
		}
		return new Object[]{otaAirPriceRQ};
	}

}
