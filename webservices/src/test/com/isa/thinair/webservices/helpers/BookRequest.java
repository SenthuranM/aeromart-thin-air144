/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.CountryNameType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType.PTCFareBreakdowns;
import org.opentravel.ota._2003._05.AirTravelerType.Address;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAddressType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAContactInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACountryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPersonNameType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATelephoneType;

/**
 * @author Mehdi Raza
 */
public class BookRequest extends AbstractHelper {

	public String getOperation() {		
		return "book";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, ModuleException {

		OTAAirBookRQ request=new OTAAirBookRQ();
    	CountryNameType country=null;
    	PaymentDetailType paymentDetail=null;
    	DirectBillType directBill=null;
    	CompanyName directBillCompanyName=null;
    	PaymentAmount paymentAmount=null;
    	Address address=null;
    	AirTravelerType tr=null;
    	OTAAirPriceRS otaAirPriceRS=(OTAAirPriceRS)helpers[0];
    	
    	request.setPOS(new POSType());
    	request.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	
    	request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_Book);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(otaAirPriceRS.getTransactionIdentifier());
    	
    	directBill=new DirectBillType();	
    	directBill.setCompanyName(directBillCompanyName=new CompanyName());
    	directBillCompanyName.setCode(Constants.TEST_AGENT_CODE);
    	
    	TravelerInfoType trInfo=new TravelerInfoType();
    	request.setTravelerInfo(trInfo);
  	
//    	AirBookRQAAExt airBookRQAAExt = new AirBookRQAAExt();
//    	airBookRQAAExt.setTravelersFulfillments(new TravelersFulfillmentsType());
    	
    	// POPULATING PRICE BREAKDOWNS FROM OTA_AirPriceRS
    	// Assume that price breakdowns quoted by system should be returned back
    	// At the same time for each TravelRefNumber push the traveller info
    	int adultIndex=0;
    	double totalPrice=0;
    	request.setPriceInfo(new PriceInfo());
    	request.getPriceInfo().setPTCFareBreakdowns(new PTCFareBreakdowns());
    	for (Object o : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {    		
    		if (o instanceof PricedItinerariesType) {    			
    			for (PricedItineraryType itinsType : ((PricedItinerariesType)o).getPricedItinerary()) {
    				request.setAirItinerary(itinsType.getAirItinerary());
    				request.getPriceInfo().setItinTotalFare(itinsType.getAirItineraryPricingInfo().getItinTotalFare());
    				totalPrice += itinsType.getAirItineraryPricingInfo().getItinTotalFare().getTotalFare().getAmount().doubleValue();
    				request.getPriceInfo().setFareInfos(itinsType.getAirItineraryPricingInfo().getFareInfos());
    				request.getPriceInfo().setPTCFareBreakdowns(itinsType.getAirItineraryPricingInfo().getPTCFareBreakdowns());
    				for (PTCFareBreakdownType each : itinsType.getAirItineraryPricingInfo().getPTCFareBreakdowns().getPTCFareBreakdown()) {
//    					request.getPriceInfo().getPTCFareBreakdowns().getPTCFareBreakdown().add(each);
//    					intPaymentAmount+=each.getPassengerFare().getBaseFare().getAmount().intValue();
//    					for (AirTaxType tax : each.getPassengerFare().getTaxes().getTax()) {
//    						intPaymentAmount+=tax.getAmount().intValue();
//    					}
//    					for (AirFeeType fee : each.getPassengerFare().getFees().getFee()) {
//    						intPaymentAmount+=fee.getAmount().intValue();
//    					}
    					if (each.getPassengerTypeQuantity().getCode().equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
    						for (int x=0;x<each.getPassengerTypeQuantity().getQuantity().intValue();x++) {
//    							TravelerFulfillmentsType travelerFulfillments  = new TravelerFulfillmentsType();
//    							airBookRQAAExt.getTravelersFulfillments().getTravelerFulfillments().add(travelerFulfillments);
//    							travelerFulfillments.getPaymentDetail().add(paymentDetail=new PaymentDetailType());
//    					    	paymentDetail.setDirectBill(directBill);    	
//    					    	paymentDetail.setPaymentAmount(paymentAmount=new PaymentAmount());
//    							paymentAmount.setAmount(each.getPassengerFare().getTotalFare().getAmount());
//    							paymentDetail.setRPH(each.getTravelerRefNumber().get(x).getRPH());
//    							totalResPaymentAmount += each.getPassengerFare().getTotalFare().getAmount().doubleValue();
    							if (adultIndex==0) {
    								
        							// ################ ADULT 1
        							tr=new AirTravelerType();
        					    	tr.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
        					    	
        					    	tr.setPersonName(new PersonNameType()); 
        					    	tr.getPersonName().getNameTitle().add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
        					    	tr.getPersonName().getGivenName().add("Ros");
        					    	tr.getPersonName().setSurname("Taylor");
        					    	tr.setBirthDate(CommonUtil.getXMLGregorianCalendar(CommonUtil.parseDate("1976-08-12T00:00:00")));
        					    	
        					    	AirTravelerType.Telephone telephone = new AirTravelerType.Telephone();
        					
        					    	telephone.setCountryAccessCode("971");
        					    	telephone.setAreaCityCode("6");
        					    	telephone.setPhoneNumber("5088952");
        					    	
        					    	tr.getTelephone().add(telephone);
        					    	
        					    	address=new Address();    					    	
        					    	address.setCountryName(country=new CountryNameType());
        					    		country.setCode("AU");
        					    		
        					    	tr.getAddress().add(address);
        					    	tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
        					    	tr.getTravelerRefNumber().setRPH(each.getTravelerRefNumber().get(x).getRPH());
        					    	
        					    	trInfo.getAirTraveler().add(tr);
        							adultIndex++;
        						} else {
        							// ################ ADULT 2
        							tr=new AirTravelerType();
        					    	tr.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
        					    	tr.setPersonName(new PersonNameType()); 
        					    	tr.getPersonName().getNameTitle().add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
        					    	tr.getPersonName().getGivenName().add("Jennie");
        					    	tr.getPersonName().setSurname("Taylor");
        					    	tr.setBirthDate(CommonUtil.getXMLGregorianCalendar(CommonUtil.parseDate("1940-08-12T00:00:00")));
        					    	
        					    	address=new Address();
        					    	
        					    	address.setCountryName(country=new CountryNameType());
        					    		country.setCode("AU");
        					    	tr.getAddress().add(address);
        					    	tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
       					    		tr.getTravelerRefNumber().setRPH(each.getTravelerRefNumber().get(x).getRPH());
       					    		trInfo.getAirTraveler().add(tr);
        						}
    						}    						
    					} else if(each.getPassengerTypeQuantity().getCode().equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
    						// ################ INFANT 1
    						for (int y=0;y<each.getPassengerTypeQuantity().getQuantity().intValue();y++) {
    							
	    						//request.getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetail=new PaymentDetailType());
//    							TravelerFulfillmentsType travelerFulfillments  = new TravelerFulfillmentsType();
//    							airBookRQAAExt.getTravelersFulfillments().getTravelerFulfillments().add(travelerFulfillments);
//    							travelerFulfillments.getPaymentDetail().add(paymentDetail=new PaymentDetailType());
//    							
//						    	paymentDetail.setDirectBill(directBill);
//						    	paymentDetail.setPaymentAmount(paymentAmount=new PaymentAmount());
//								paymentAmount.setAmount(each.getPassengerFare().getTotalFare().getAmount());
//								//System.out.println("##############################--->"+each.getPassengerFare().getTotalFare().getAmount().intValue());
//								paymentDetail.setRPH(each.getTravelerRefNumber().get(y).getRPH());
//								totalResPaymentAmount += each.getPassengerFare().getTotalFare().getAmount().doubleValue();
								
	    				    	tr=new AirTravelerType();
	    				    	tr.setPassengerTypeCode("INF");
	    				    	tr.setPersonName(new PersonNameType()); 
	    				    	tr.getPersonName().getGivenName().add("Jeromy");
	    				    	tr.getPersonName().setSurname("Taylor");
	    				    	tr.setBirthDate(CommonUtil.getXMLGregorianCalendar(CommonUtil.parseDate("2006-08-12T00:00:00")));
	    				    	
	    				    	address=new Address();
	    				    	address.setCountryName(country=new CountryNameType());
	    				    		country.setCode("AU");
	    				    	tr.getAddress().add(address);
	    				    	tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
	    				    	
	    				    	tr.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
						    	tr.getTravelerRefNumber().setRPH(each.getTravelerRefNumber().get(y).getRPH());
						    	
	    				    	// no fare breakdown for infants
	    				    	
	    				    	trInfo.getAirTraveler().add(tr);
    						}
    					}
    				}
    			}
    		}
    	}
    	// ################ PAYMENT DETAILS (DIRECT BILL)
    	request.setFulfillment(new OTAAirBookRQ.Fulfillment());
    	request.getFulfillment().setPaymentDetails(new OTAAirBookRQ.Fulfillment.PaymentDetails());
    	
    	//total reservation payments
    	request.getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetail=new PaymentDetailType());
    	paymentDetail.setDirectBill(directBill);
    	paymentDetail.setPaymentAmount(paymentAmount=new PaymentAmount());
		paymentAmount.setAmount(new BigDecimal(totalPrice));
		
    	//set contact information
		AAContactInfoType wsContactInfo = new AAContactInfoType();
		AAPersonNameType name = new AAPersonNameType();
		wsContactInfo.setPersonName(name);
		name.setTitle(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR));
		name.setFirstName("Adam");
		name.setLastName("Bawa");
		
		AATelephoneType telephone = new AATelephoneType();
		wsContactInfo.setTelephone(telephone);
		telephone.setPhoneNumber("5088952");
		telephone.setAreaCode("6");
		telephone.setCountryCode("971");
		
		wsContactInfo.setEmail("support@isaaviation.ae");
		
		AAAddressType caddress = new AAAddressType();
		wsContactInfo.setAddress(caddress);
		
		AACountryType ccountry = new AACountryType();
		caddress.setCountryName(ccountry);
		ccountry.setCountryCode("AU");
		ccountry.setCountryName("AU");
		
		AAAirBookRQExt aaAirBookRQExt = new AAAirBookRQExt();
		aaAirBookRQExt.setContactInfo(wsContactInfo);
		
//		Element bookRQExtElement = TPAExtensionUtil.getInstance().marshalNonRootElement(aaAirBookRQExt, AAAirBookRQExt.class, TPAExtensionUtil.RootTags.AA_AIRBOOKRQ_EXT);
//		
//		request.setTPAExtensions(new TPAExtensionsType());
//		request.getTPAExtensions().getAny().add(bookRQExtElement);
    	
    	return new Object[]{request, aaAirBookRQExt};
    }
}