/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAExternalPayTxType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * @author Mehdi Raza
 */
public class BalancePaymentRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
		
		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];
		BigDecimal balanceAmount = (BigDecimal) helpers[1];
		
		

		AirReservationType.Fulfillment fulfillment = null;
		PaymentDetailType paymentDetailType = null;

		OTAAirBookModifyRQ request = new OTAAirBookModifyRQ();
		
		request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
		
		POSType pos = new POSType();
		request.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);
        
        //if (source.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_EBI_CDM)) ){
        if (source.getBookingChannel().getType().equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA)) ){
			balanceAmount = CommonUtil.getRoundedUpValue(balanceAmount, 10);
		}
		
		request.setAirBookModifyRQ(new AirBookModifyRQ());
		request.getAirBookModifyRQ().setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_BALANCE_PAYMENT));
		
		AirReservationType airReservationType = null;
		for (Iterator i=otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
			Object o=i.next();
			if (o instanceof AirReservation) {
				airReservationType = (AirReservationType) o;
			}
		}
		
		request.getAirBookModifyRQ().getBookingReferenceID().add(airReservationType.getBookingReferenceID().get(0));	
		
		request.getAirBookModifyRQ().setFulfillment(fulfillment=new AirReservationType.Fulfillment());
		fulfillment.setPaymentDetails(new AirReservationType.Fulfillment.PaymentDetails());
		fulfillment.getPaymentDetails().getPaymentDetail().add(paymentDetailType=new PaymentDetailType());
		PaymentAmount amount=new PaymentAmount();
		amount.setAmount(CommonUtil.getBigDecimalWithDefaultPrecision(balanceAmount));
		amount.setCurrencyCode("AED");
		amount.setDecimalPlaces(WebservicesConstants.OTAConstants.DECIMALS_AED);
		
		paymentDetailType.setPaymentAmount(amount);
		paymentDetailType.setDirectBill(new DirectBillType());						
		paymentDetailType.getDirectBill().setCompanyName(new CompanyName());
		paymentDetailType.getDirectBill().getCompanyName().setValue(Constants.TEST_AGENT_NAME);
		paymentDetailType.getDirectBill().getCompanyName().setCode(Constants.TEST_AGENT_CODE);
		
		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
		
		List l = TPAExtensionUtil.getInstance().unmarshall(airReservationType.getTPAExtensions().getAny(), AAAirReservationExt.class);
		
//		AAAirReservationExt airReservationAAExt = (AAAirReservationExt) airReservationType.getTPAExtensions().getAny();
		if (l !=  null && l.size()>0){
			AAAirReservationExt airReservationAAExt =  (AAAirReservationExt) l.get(0);
//		if (airReservationAAExt != null) {
			AAExternalPayTxType wsExternalPayTx = airReservationAAExt.getCurrentExtPayTxInfo();
			
			if (wsExternalPayTx != null){
			
				wsExternalPayTx.setExternalSystemTxRPH(UID.generate());
				wsExternalPayTx.setAmount(amount.getAmount());
				wsExternalPayTx.setExternalTxStatus(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS));
				wsExternalPayTx.setExternalTxTimestamp(CommonUtil.getFormattedDate(new Date()));
				
//				wsExternalPayTx.setProperties(new AAPropertiesType());
//				AAPropertyType p = new AAPropertyType();
//				p.setValue("x");
//				p.setKey("y");
//				wsExternalPayTx.getProperties().getProperty().add(p);
				
				wsExternalPayTx.setExternalSystemTxRPH(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS));
				
				airBookModifyRQAAExt.setCurrentExtPayTxInfo(wsExternalPayTx);
			}
		}
//		}
		
//		PaymentDetailType pd = new PaymentDetailType();
//		DirectBillType db = new DirectBillType();
//		pd.setDirectBill(db);
//		DirectBillType.CompanyName cn = new DirectBillType.CompanyName();
//		cn.setValue("SHJ53");
//		cn.setCode("SHJ53");
//		db.setCompanyName(cn);
//		PaymentDetailType.PaymentAmount pa = new PaymentDetailType.PaymentAmount();
//		pa.setAmount(new BigDecimal(100));
//		pa.setCurrencyCode("AED");
//		pd.setPaymentAmount(pa);
		
//		OTAPingRQ otaPingRQ = new OTAPingRQ();
//		otaPingRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
//		otaPingRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_Ping);
//		otaPingRQ.setSequenceNmbr(new BigInteger("1"));
//		otaPingRQ.setEchoToken(UID.generate());
//		otaPingRQ.setTimeStamp(CommonUtil.parse(new Date()));
//		otaPingRQ.setTransactionIdentifier(null);
//		
//		try {
//			otaPingRQ.setEchoData("Ping Request From "
//					+ InetAddress.getLocalHost().getHostAddress() + " At "
//					+ new Date(System.currentTimeMillis()));
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//    	Element elementExtension = marshall(otaPingRQ, OTAPingRQ.class);
//    	request.getAirBookModifyRQ().setTPAExtensions(new TPAExtensionsType());
//    	request.getAirBookModifyRQ().getTPAExtensions().getAny().add(elementExtension);
		
		
//		marshallToSysOut(request);
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		
		airBookModifyRQAAExt.setAALoadDataOptions(options);
		
//		Element airBookModRQExtElement = TPAExtensionUtil.getInstance().marshalNonRootElement(airBookModifyRQAAExt, AAAirBookModifyRQExt.class, TPAExtensionUtil.RootTags.AA_AIRBOOKMODIFYRQ_EXT);
//		
//		request.getAirBookModifyRQ().setTPAExtensions(new TPAExtensionsType());
//		request.getAirBookModifyRQ().getTPAExtensions().getAny().add(airBookModRQExtElement);
		
//		List<String> pkgs = new ArrayList<String>();
//		pkgs.add("org.opentravel.ota._2003._05");
		//pkgs.add("com.isaaviation.thinair.webservices.ota.extensions._2003._05");
//		
//		OTAAirBookModifyRQ airBookModifyRQ = (OTAAirBookModifyRQ) TPAExtensionUtil.getInstance().unmarshalFromFile(pkgs, new File("D://Projects//ThinAir//samplemessages//pay.xml"));
		
//		airBookModifyRQAAExt.setAALoadDataOptions(options);
		
//		OTAAirBookModifyRQ tmp = (OTAAirBookModifyRQ) TPAExtensionUtil.getInstance().unmarshalFromFile(pkgs, new File("D://Projects//ThinAir//samplemessages//pay.xml"));
		return new Object[] { request, airBookModifyRQAAExt };
	}
	
	public void marshallToSysOut(Object o){
		try {
		JAXBContext context=JAXBContext.newInstance(o.getClass().getPackage().getName());
		Marshaller marshaller=context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(o, System.out);
		} catch (JAXBException e) {
			log.error(e);
		}
	}
	
	public Element marshall(Object o, Class c) throws WebservicesException {
		try {
			JAXBContext context=JAXBContext.newInstance(c);
			Marshaller marshaller=context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Document doc=getNewDocument();
			marshaller.marshal(o, doc);
			return doc.getDocumentElement();
		} catch (JAXBException e) {
			throw new WebservicesException(e);
		}
	}
	
	private Document getNewDocument() {
		_mapping.put("http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05", "com.isaaviation.thinair.webservices.ota.extensions._2003._05");
		_mapping.put("http://www.opentravel.org/OTA/2003/05", "org.opentravel.ota._2003._05");
		factory=DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			builder=factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("TPA Extension Util initialization failed.");
		}
		return builder.newDocument();
	}
	
	private Map _mapping=new HashMap();
	private DocumentBuilderFactory factory=null;
	DocumentBuilder builder=null;
}
