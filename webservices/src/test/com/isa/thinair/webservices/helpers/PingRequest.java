/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.OTAPingRQ;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * @author Mohamed Nasly
 * @author Mehdi Raza
 */
public class PingRequest extends AbstractHelper {

	public void assertResponse(Object response) {
		assertNotNull(response);
	}

	public String getOperation() {
		return "ping";
	}

	public Object[] getRequestObject(Object[] helpers)throws  ModuleException {
		/** Ping Request */
		OTAPingRQ otaPingRQ = new OTAPingRQ();
    	
		otaPingRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaPingRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_Ping);
		otaPingRQ.setSequenceNmbr(new BigInteger("1"));
		otaPingRQ.setEchoToken(UID.generate());
		otaPingRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaPingRQ.setTransactionIdentifier(null);

		try {
			otaPingRQ.setEchoData("Ping Request From "
					+ InetAddress.getLocalHost().getHostAddress() + " At "
					+ new Date(System.currentTimeMillis()));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Object[] { otaPingRQ };

		// OTAPingRS otPingRS = getService().ping(otaPingRQ);
		// List<Object> res = otPingRS.getSuccessAndWarningsAndEchoData();
		// Iterator<Object> resIt = res.iterator();
		// while (resIt.hasNext()){
		// Object obj= resIt.next();
		// if (obj instanceof SuccessType){
		// log.info("PING SUCCESS");
		// } else if (obj instanceof WarningsType){
		// WarningsType warnings = (WarningsType)obj;
		// Iterator<WarningType> warningsIt = warnings.getWarning().iterator();
		// while (warningsIt.hasNext()){
		// log.info("PING WARNING :: " + warningsIt.next().getShortText());
		// }
		// } else {
		// log.info(obj.toString());
		// }
		// }
	}
}
