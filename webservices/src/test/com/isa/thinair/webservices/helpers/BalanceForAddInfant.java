/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.AirTravelerType.TravelerRefNumber;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;


public class BalanceForAddInfant extends AbstractHelper {
	public String getOperation() {		
		return "modifyResQuery";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
        OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];
        OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();
        otaAirBookModifyRQ.setPOS(new POSType());
        otaAirBookModifyRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
        otaAirBookModifyRQ.setPrimaryLangID(
                WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        otaAirBookModifyRQ.setVersion(
                WebservicesConstants.OTAConstants.VERSION_AirBookModify);
        otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirBookModifyRQ.setEchoToken(UID.generate());
        otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
        otaAirBookModifyRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
        
        AirReservationType airReservation = null;
        for (Iterator i = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator(); 
                i.hasNext();) {
            Object o = i.next();
            if (o instanceof AirReservation) {
                airReservation = (AirReservationType) o;
            }
        }
        
        int infantSeq = airReservation.getTravelerInfo().getAirTraveler().size() + 1;
        
        
        otaAirBookModifyRQ.setAirBookModifyRQ(new AirBookModifyRQ());
        otaAirBookModifyRQ.getAirBookModifyRQ().setModificationType(
                CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_INFANT));
        otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().add(
                airReservation.getBookingReferenceID().get(0));
       
    	
        
        TravelerInfoType infoType = new TravelerInfoType();
        AirTravelerType airTravelerType = new AirTravelerType();
        PassengerTypeQuantityType passengerTypeQuantityType = new PassengerTypeQuantityType();
        passengerTypeQuantityType.setQuantity(1);
        airTravelerType.setPassengerTypeQuantity(passengerTypeQuantityType);
        airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
        airTravelerType.setPersonName(new PersonNameType()); 
        airTravelerType.getPersonName().getNameTitle().add("MR");
        airTravelerType.getPersonName().getGivenName().add("Jeromy");
        airTravelerType.getPersonName().setSurname("Taylor");
       
        
        TravelerRefNumber refNumber = new TravelerRefNumber();
        refNumber.setRPH("I"+ infantSeq + "/" + airReservation.getTravelerInfo().getAirTraveler().get(0).getTravelerRefNumber().getRPH());
        //airReservation.get
        airTravelerType.setTravelerRefNumber(refNumber);
        
        infoType.getAirTraveler().add(airTravelerType);
        otaAirBookModifyRQ.getAirBookModifyRQ().setTravelerInfo(infoType);
        
        DirectBillType directBill = new DirectBillType();	
    	directBill.setCompanyName(new CompanyName());
    	directBill.getCompanyName().setCode(Constants.TEST_AGENT_CODE);
    	directBill.getCompanyName().setValue(Constants.TEST_AGENT_CODE);
    	
    	PaymentDetailType paymentDetail = new PaymentDetailType();
    	paymentDetail.setDirectBill(directBill);
    	paymentDetail.setPaymentAmount(new PaymentAmount());
    	paymentDetail.getPaymentAmount().setAmount(new BigDecimal(0));
    	
    	//Fulfillment fulfillment = new Fulfillment();
    	//PaymentDetails p = new PaymentDetails();
    	//fulfillment.setPaymentDetails(p);
    	//otaAirBookModifyRQ.getAirBookModifyRQ().setFulfillment(fulfillment);
    //	otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetail = new PaymentDetailType());
    	
        
       
        AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
        airBookModifyRQAAExt.setAALoadDataOptions((AALoadDataOptionsType) helpers[1]);
               
		return new Object[]{otaAirBookModifyRQ, airBookModifyRQAAExt};
	}
}
