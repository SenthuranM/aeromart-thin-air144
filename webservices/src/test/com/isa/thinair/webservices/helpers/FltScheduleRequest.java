package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.OTAAirScheduleRQ;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SpecificFlightInfoType;
import org.opentravel.ota._2003._05.TimeInstantType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * @author Mohamed Nasly
 */
public class FltScheduleRequest extends AbstractHelper {

	public String getOperation() {		
		return "getFlightSchedule";
	}

	public Object[] getRequestObject(Object[] helpers) throws WebservicesException, ModuleException {
		OTAAirScheduleRQ otaAirScheudleRQ = new OTAAirScheduleRQ();
        otaAirScheudleRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        otaAirScheudleRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirScheudle);
        otaAirScheudleRQ.setTimeStamp(CommonUtil.parse(new Date()));
        otaAirScheudleRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirScheudleRQ.setEchoToken(UID.generate());
        otaAirScheudleRQ.setTarget(Constants.TARGET_NAME);
        
        POSType pos = new POSType();
        otaAirScheudleRQ.setPOS(pos);
        pos.getSource().add(CommonTestUtils.preparePOS());
        
        SpecificFlightInfoType flightInfo = new SpecificFlightInfoType();
        flightInfo.setFlightNumber("G9505");
        otaAirScheudleRQ.setFlightInfo(flightInfo);
        
        OriginDestinationInformationType originDestinationInformation = new OriginDestinationInformationType();
        OriginDestinationInformationType.OriginLocation origin = new OriginDestinationInformationType.OriginLocation();
        origin.setLocationCode("SHJ");
        OriginDestinationInformationType.DestinationLocation destination = new OriginDestinationInformationType.DestinationLocation();
        destination.setLocationCode("CMB");
        originDestinationInformation.setOriginLocation(origin);
        originDestinationInformation.setDestinationLocation(destination);
       
        Calendar startDateCal = new GregorianCalendar();
        startDateCal.set(2007, Calendar.JULY, 25, 0, 0, 0);
        
        Calendar endDateCal = new GregorianCalendar();
        endDateCal.set(2007, Calendar.AUGUST, 25, 0, 0, 0);
        
        TimeInstantType startDate = new TimeInstantType();
        startDate.setValue(CommonUtil.getFormattedDate(startDateCal.getTime()));
        originDestinationInformation.setDepartureDateTime(startDate);
        
        TimeInstantType stopDate = new TimeInstantType();
        stopDate.setValue(CommonUtil.getFormattedDate(endDateCal.getTime()));
        originDestinationInformation.setArrivalDateTime(stopDate);
        
        otaAirScheudleRQ.setOriginDestinationInformation(originDestinationInformation);
        
        return new Object[]{otaAirScheudleRQ};
	}
}
