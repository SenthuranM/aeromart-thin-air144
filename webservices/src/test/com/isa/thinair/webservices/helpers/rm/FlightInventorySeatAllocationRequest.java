package com.isa.thinair.webservices.helpers.rm;



import java.util.Calendar;
import java.util.GregorianCalendar;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.Constants;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;


/**
 * @author Byorn
 */
public class FlightInventorySeatAllocationRequest extends AbstractHelper {

  
    
	public String getOperation(){		
        return "getFlightInventoryAllocations";
	}

	public Object[] getRequestObject(Object[] helpers) throws Exception { 
      AAFlightInventorySearchRQ rq = new AAFlightInventorySearchRQ();
      rq.setCabinClass("Y");
      rq.setFlightNumber("G9505");

      Calendar calendar = new GregorianCalendar(2008, Calendar.MARCH, 31);
      rq.setDepatureDate(CommonUtil.parse(calendar.getTime()));
      rq.setUserId(Constants.TEST_AGENT_TEST_USER);
      rq.setChannelId(Constants.BOOKING_CHANNEL_TYPE);
      rq.setMessageId("123");
    
      
      
       return new Object[]{rq};     
	}
    
     public void assertResponse(Object response) {
         if(response instanceof AAFlightInventoryDataRS){
             AAFlightInventoryDataRS rs = (AAFlightInventoryDataRS)response;
             System.out.println("Is Success ?" + rs.getStatusErrorsAndWarnings().isSuccess());
         }
         System.out.println("response asserted: "+response);
     }
   
}
