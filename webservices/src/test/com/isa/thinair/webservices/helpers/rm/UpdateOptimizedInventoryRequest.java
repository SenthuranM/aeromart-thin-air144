package com.isa.thinair.webservices.helpers.rm;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isaaviation.thinair.webservices.api.airinventory.AAInventorySeatMovementType;
import com.isaaviation.thinair.webservices.api.airinventory.AAInventorySeatMovementsType;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAUpdateOptimizedInventoryRS;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.Constants;


/**
 * @author Byorn
 */
public class UpdateOptimizedInventoryRequest extends AbstractHelper {

  
    
	public String getOperation(){		
        return "updateOptimizedInventory";
	}

	public Object[] getRequestObject(Object[] helpers) throws Exception { 
       AAUpdateOptimizedInventoryRQ updateOptimizedInventoryRQ = new AAUpdateOptimizedInventoryRQ();
       updateOptimizedInventoryRQ.setCabinClass("Y");
       updateOptimizedInventoryRQ.setFlightNumber("G9505");
       updateOptimizedInventoryRQ.setMessageId("123");
       updateOptimizedInventoryRQ.setProcessingStatus("");
       updateOptimizedInventoryRQ.setSegmentCode("SHJ/CMB");
       updateOptimizedInventoryRQ.setChannelId(Constants.BOOKING_CHANNEL_TYPE);
       updateOptimizedInventoryRQ.setUserId(Constants.TEST_AGENT_TEST_USER);
       
       AAInventorySeatMovementsType inventorySeatMovementsType = new AAInventorySeatMovementsType();
       AAInventorySeatMovementType inventorySeatMovementType = new AAInventorySeatMovementType();
       inventorySeatMovementType.setFromBookingClass("N2");
       inventorySeatMovementType.setToBookingClass("N3");
       inventorySeatMovementType.setNoOfSeats(20);
//       inventorySeatMovementType.setMessageType(RMThresholdType.ABOVE.value());// E - Above Threshold, D - Below Threshold
       inventorySeatMovementsType.getInventorySeatMovement().add(inventorySeatMovementType);
       updateOptimizedInventoryRQ.setInventorySeatMovements(inventorySeatMovementsType);

       Calendar calendar = new GregorianCalendar(2008, Calendar.MARCH, 15);
       updateOptimizedInventoryRQ.setDepatureDate(CommonUtil.parse(calendar.getTime()));
       
       return new Object[]{updateOptimizedInventoryRQ};     
	}
    
     public void assertResponse(Object response) {
         if(response instanceof AAUpdateOptimizedInventoryRS){
             AAUpdateOptimizedInventoryRS inventoryRS = (AAUpdateOptimizedInventoryRS)response;
             System.out.println("Is Success ?" + inventoryRS.getStatusErrorsAndWarnings().isSuccess());
         }
         System.out.println("response asserted: "+response);
        }
   
}
