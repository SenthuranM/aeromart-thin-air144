/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import javax.xml.datatype.DatatypeConfigurationException;

import org.opentravel.ota._2003._05.OTAAirScheduleRQ;

import com.isa.thinair.webservices.api.exception.WebservicesException;

/**
 * @author Byorn
 * REFUND FOR ALL THE PASSENGERS IN THE RESERVATION
 */
public class RetrieveFlightScheduleRequest extends AbstractHelper {

	
    /** Holds the operation name defined in AAResWebService Interface **/
    public String getOperation() {		
		return "refundPassenger";
	}
	
	
    /** Peform the Assert Operatins **/
    public void assertResponse(Object response) {
      

    }
	

    /** Populate OTAAirBookModifyRQ and return it *
     * @throws WebservicesException */
    public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException {
        OTAAirScheduleRQ airScheduleRQ = new OTAAirScheduleRQ();
      
        
        
        return helpers;
        
        
    }
    
    
    
    
    
    
    
}
