package com.isa.thinair.webservices.helpers.rm;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryAdd;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryAddType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryDelete;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryDeleteType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryUpdate;
import com.isaaviation.thinair.webservices.api.airinventory.AAFCCSegBCInventoryUpdateType;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryBatchUpdateRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryRQType;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.Constants;


/**
 * @author Byorn
 */
public class PublishInventoryRequest extends AbstractHelper {

    public String getOperation(){       
        return "batchUpdateFlightInventory";
    }
    
	public Object[] getRequestObject(Object[] helpers) throws Exception {
	        AAFlightInventoryBatchUpdateRQ  batchRQ = new AAFlightInventoryBatchUpdateRQ();
        
  
	        batchRQ.setUserId(Constants.TEST_AGENT_TEST_USER);
	        batchRQ.setChannelId(Constants.BOOKING_CHANNEL_TYPE);
  
	        AAFlightInventoryRQType  flightInventoryRQType = new AAFlightInventoryRQType();
	       
            
            AAFlightInventoryRQ  flightInventoryRQ = new AAFlightInventoryRQ();
	        flightInventoryRQ.setCabinClass("Y");
   
	        Calendar calendar = new GregorianCalendar(2008, Calendar.MARCH, 20);
	        flightInventoryRQ.setDepatureDate(CommonUtil.parse(calendar.getTime()));    
	        
	        flightInventoryRQ.setFlightNumber("G9505"); 
	        flightInventoryRQ.setSegmentCode("SHJ/CMB");
	      
            AAFCCSegBCInventoryAddType inventoryADDType = new AAFCCSegBCInventoryAddType();
            AAFCCSegBCInventoryAdd inventoryADD = new AAFCCSegBCInventoryAdd();
            inventoryADD.setAllocatedSeats(30);
            inventoryADD.setBookingCode("N7");
            inventoryADD.setPriorityFlag(true);
            inventoryADDType.getFccSegBCInventory().add(inventoryADD);
            flightInventoryRQ.setFccSegBCInventoriesToAdd(inventoryADDType);
            
            AAFCCSegBCInventoryUpdateType inventoryUDPATEType = new AAFCCSegBCInventoryUpdateType();
            AAFCCSegBCInventoryUpdate inventoryUPDATE = new AAFCCSegBCInventoryUpdate();
            inventoryUPDATE.setBookingCode("N2");
            inventoryUPDATE.setPriorityFlag(true);
            inventoryUPDATE.setStatus("OPN");// valid options are OPN or CLS
            inventoryUPDATE.setAcquiredSeats(0);
            inventoryUPDATE.setAllocatedSeats(20);
           
            inventoryUDPATEType.getFccSegBCInventory().add(inventoryUPDATE);
            flightInventoryRQ.setFccSegBCInventoriesToUpdate(inventoryUDPATEType);

            AAFCCSegBCInventoryDeleteType inventoryDELETEType = new AAFCCSegBCInventoryDeleteType();
            AAFCCSegBCInventoryDelete inventoryDELETE = new AAFCCSegBCInventoryDelete();
         
            inventoryDELETE.setBookingCode("N8");
            inventoryDELETEType.getFccSegBCInventory().add(inventoryDELETE);
            flightInventoryRQ.setFccSegBCInventoriesToDelete(inventoryDELETEType);
            
            flightInventoryRQType.getFlightInventoryDetail().add(flightInventoryRQ);
            
            batchRQ.setFlightInventory(flightInventoryRQType);
            return new Object[]{batchRQ}; 
    }
}
