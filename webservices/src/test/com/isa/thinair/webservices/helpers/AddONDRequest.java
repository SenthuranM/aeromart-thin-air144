/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * @author Nasly
 */
public class AddONDRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
		
		OTAAirBookRS otaAirBookRS = (OTAAirBookRS) helpers[0];

		OTAAirBookModifyRQ airBookModRQ = new OTAAirBookModifyRQ();
		
		airBookModRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		airBookModRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		airBookModRQ.setSequenceNmbr(new BigInteger("2"));
		airBookModRQ.setEchoToken(UID.generate());
		airBookModRQ.setTimeStamp(CommonUtil.parse(new Date()));
		airBookModRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());
		
		POSType pos = new POSType();
		airBookModRQ.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);

		
		airBookModRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		airBookModRQ.getAirBookModifyRQ().setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_OND));
		
		AirReservationType currentOTAAirRes = null;
		for (Iterator i=otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
			Object o=i.next();
			if (o instanceof AirReservation) {
				currentOTAAirRes = (AirReservationType) o;
			}
		}
		
		airBookModRQ.getAirBookModifyRQ().getBookingReferenceID().add(currentOTAAirRes.getBookingReferenceID().get(0));	
		
		
		double totalPrice=0;
		OTAAirPriceRS otaAirPriceRS=(OTAAirPriceRS)helpers[1];
    	for (Object o : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {    		
    		if (o instanceof PricedItinerariesType) {    			
    			for (PricedItineraryType itinsType : ((PricedItinerariesType)o).getPricedItinerary()) {
    				airBookModRQ.getAirBookModifyRQ().setAirItinerary(itinsType.getAirItinerary());
    				totalPrice += itinsType.getAirItineraryPricingInfo().getItinTotalFare().getTotalFare().getAmount().doubleValue();
    			}
    		}
    	}
    	
    	// Total outstanding payment
    	airBookModRQ.getAirBookModifyRQ().setFulfillment(new AirReservationType.Fulfillment());
    	airBookModRQ.getAirBookModifyRQ().getFulfillment().setPaymentDetails(new AirReservationType.Fulfillment.PaymentDetails());

    	DirectBillType directBill = new DirectBillType();	
    	directBill.setCompanyName(new CompanyName());
    	directBill.getCompanyName().setCode(Constants.TEST_AGENT_CODE);
    	directBill.getCompanyName().setValue(Constants.TEST_AGENT_CODE);
    	
    	PaymentDetailType paymentDetail = null;
    	airBookModRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetail = new PaymentDetailType());
    	paymentDetail.setDirectBill(directBill);
    	paymentDetail.setPaymentAmount(new PaymentAmount());
    	paymentDetail.getPaymentAmount().setAmount(new BigDecimal(totalPrice));
		
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		
		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
		airBookModifyRQAAExt.setAALoadDataOptions(options);
		
		return new Object[] { airBookModRQ, airBookModifyRQAAExt };
	}

	private OriginDestinationOptionType getFirstConfirmedOND(List<OriginDestinationOptionType> originDestinationOption) {
		OriginDestinationOptionType firstConfirmedOND = null;
		for (OriginDestinationOptionType ond : originDestinationOption){
			List<BookFlightSegmentType> fltSegs = ond.getFlightSegment();
			for (BookFlightSegmentType fltSeg : fltSegs){
				if (CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_ACTIVE).equals(fltSeg.getStatus())){
					firstConfirmedOND = ond;
					break;
				}
			}
		}
		return firstConfirmedOND;
	}
}
