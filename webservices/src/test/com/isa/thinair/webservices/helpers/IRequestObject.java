package com.isa.thinair.webservices.helpers;

public interface IRequestObject {
	
	public Object[] getRequestObject(Object[] helpers) throws Exception;
	public boolean requiresAuthentication();
	public void assertResponse(Object response);
	public String getPrincipal();
	public String getOperation();
}
