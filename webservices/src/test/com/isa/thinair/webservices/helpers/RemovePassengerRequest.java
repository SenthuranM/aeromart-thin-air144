/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

public class RemovePassengerRequest extends AbstractHelper {

	public String getOperation() {		
		return "modifyReservation";
	}
	
	public void assertResponse(Object response) {
		System.out.println("assert response called..."+response);
	}
	
	public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, FileNotFoundException, JAXBException, ModuleException {
		
        AirReservationType airReservationType = getAirReservation((OTAAirBookRS)helpers[0]);
        
		OTAAirBookModifyRQ request = new OTAAirBookModifyRQ();
		
		request.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		request.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		request.setSequenceNmbr(new BigInteger("1"));
		request.setEchoToken(UID.generate());
		request.setTimeStamp(CommonUtil.parse(new Date()));
		request.setTransactionIdentifier(((OTAAirBookRS)helpers[0]).getTransactionIdentifier());
		
		request.setPOS(new POSType());
        request.getPOS().getSource().add(CommonTestUtils.preparePOS());
		
		request.setAirBookModifyRQ(new AirBookModifyRQ());
		request.getAirBookModifyRQ().setModificationType(
                CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_CANCEL_PARTLY));
        
        request.getAirBookModifyRQ().getBookingReferenceID().add(
                airReservationType.getBookingReferenceID().get(0));
        request.getAirBookModifyRQ().setTravelerInfo(new TravelerInfoType());
        
        List<AirTravelerType> listTravellers = airReservationType.getTravelerInfo().getAirTraveler();
        
        Collection<String> paxId = new ArrayList<String>();
        paxId.add("A1");
        
        for (AirTravelerType airTraveller: listTravellers) {
            if ((airTraveller.getTravelerRefNumber() != null) 
                    && (airTraveller.getTravelerRefNumber().getRPH() != null) 
                    && (airTraveller.getTravelerRefNumber().getRPH().indexOf("$") != -1)) {
            	String curPaxId = airTraveller.getTravelerRefNumber().getRPH().substring(0, airTraveller.getTravelerRefNumber().getRPH().indexOf("$"));
                //airTraveller.getTravelerRefNumber().setRPH("");
                if (paxId.contains(curPaxId)) {
                    request.getAirBookModifyRQ().getTravelerInfo().getAirTraveler().add(airTraveller);                    
                }
            }
        }
        
        AAAirBookModifyRQExt aAirBookModifyRQExt = new AAAirBookModifyRQExt();
        aAirBookModifyRQExt.setAALoadDataOptions((AALoadDataOptionsType) helpers[1]);
        
		return new Object[] {request, aAirBookModifyRQExt};
	}
    
    
    /**
     * Retrive the AirReservationType Object from the OTAAirBookRS
     * @param bookResponse
     * @return
     */
    private AirReservationType getAirReservation(OTAAirBookRS bookResponse){
        for (Iterator i=bookResponse.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
            Object o=i.next();
            
            if (o instanceof AirReservation) {
                return (AirReservationType) o;
            }
        }
       return null;
        
    }
}
