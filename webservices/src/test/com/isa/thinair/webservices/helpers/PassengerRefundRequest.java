/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import javax.xml.datatype.DatatypeConfigurationException;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.ErrorType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;

/**
 * @author Byorn
 * REFUND FOR ALL THE PASSENGERS IN THE RESERVATION
 */
public class PassengerRefundRequest extends AbstractHelper {

	
    /** Holds the operation name defined in AAResWebService Interface **/
    public String getOperation() {		
		return "refundPassenger";
	}
	
	
    /** Peform the Assert Operatins **/
    public void assertResponse(Object response) {
        OTAAirBookRS airBookRS = (OTAAirBookRS) response;
        System.out.println("assert response called..." + response);
        if (airBookRS != null) {
            for (ErrorType errorType : airBookRS.getErrors().getError()) {
                System.out.println(errorType.getCode());
                System.out.println(errorType.getShortText());
            }
        } else {
            System.out.println("airBookRS was null");
        }
        System.out.println(airBookRS.getErrors().getError().get(0).toString());

    }
	

    /** Populate OTAAirBookModifyRQ and return it *
     * @throws WebservicesException 
     * @throws ModuleException */
    public Object[] getRequestObject(Object[] helpers) throws DatatypeConfigurationException, WebservicesException, ModuleException {
        
        
        OTAAirBookModifyRQ otaAirBookModifyRQ = new OTAAirBookModifyRQ();
        otaAirBookModifyRQ.setPOS(new POSType());
        otaAirBookModifyRQ.getPOS().getSource().add(CommonTestUtils.preparePOS());
    	
        otaAirBookModifyRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
        otaAirBookModifyRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
        otaAirBookModifyRQ.setSequenceNmbr(new BigInteger("1"));
        otaAirBookModifyRQ.setEchoToken(UID.generate());
        otaAirBookModifyRQ.setTimeStamp(CommonUtil.parse(new Date()));
        otaAirBookModifyRQ.setTransactionIdentifier(null);
        
        //Current Reservaiton details
        //Retrive the AirReservationType Object from the hleper and set it to otaAirBookModifiyRQ
        OTAAirBookRS bookReponse= (OTAAirBookRS) helpers[0];
        AirReservationType airReservationType = getAirReservation(bookReponse);
        //AirReservation airReservation= airReservationType.getA
        otaAirBookModifyRQ.setAirReservation(airReservationType);
      
        
        //Details for the refund
        //Access the AirBookModifyRQ from OTAAirBookModifyRQ 
        AirBookModifyRQ airBookModifyRQ =  new AirBookModifyRQ();
        
        
        airBookModifyRQ.setModificationType(CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_REFUND_PASSENGER));
        //pnr details  
        airBookModifyRQ.getBookingReferenceID().addAll(airReservationType.getBookingReferenceID());
    	//payment details
        airBookModifyRQ.setFulfillment(airReservationType.getFulfillment());
        //traveller details
        airBookModifyRQ.setTravelerInfo(airReservationType.getTravelerInfo());
                
        otaAirBookModifyRQ.setAirBookModifyRQ(airBookModifyRQ);
               
    	return new Object[]{otaAirBookModifyRQ};
    }
    
    
    
    /**
     * Retrive the AirReservationType Object from the OTAAirBookRS
     * @param bookResponse
     * @return
     */
    private AirReservationType getAirReservation(OTAAirBookRS bookResponse){
       
        
        for (Iterator i=bookResponse.getSuccessAndWarningsAndAirReservation().iterator();i.hasNext();) {
            Object o=i.next();
            
            if (o instanceof AirReservation) {
                AirReservation airReservation = (AirReservation) o;
                AirReservationType airReservationType = new AirReservationType();
                airReservationType.setAirItinerary(airReservation.getAirItinerary());
                airReservationType.setFulfillment(airReservation.getFulfillment());
                airReservationType.setTravelerInfo(airReservation.getTravelerInfo());
                airReservationType.getBookingReferenceID().addAll(airReservation.getBookingReferenceID());
                return airReservationType;
                
                //return (AirReservationType) o;
                             
            }
        }
       return null;
        
    }
    
    
    
}
