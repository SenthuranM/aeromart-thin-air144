/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.helpers;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AAOTAPaxCreditReadRQ;
import org.opentravel.ota._2003._05.AAPaxCreditReadRequestType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.SourceType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;


public class PassengerCreditRequest extends AbstractHelper {
	
	public String getOperation() {		
		return "getPassengerCredit";
	}

	public Object[] getRequestObject(Object[] helpers) throws WebservicesException, ModuleException {

        AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ = new AAOTAPaxCreditReadRQ();
        
        POSType pos = new POSType();
        aaOTAPaxCreditReadRQ.setPOS(pos);
		SourceType source = CommonTestUtils.preparePOS();
        pos.getSource().add(source);

        aaOTAPaxCreditReadRQ.setPrimaryLangID(
                WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		aaOTAPaxCreditReadRQ.setVersion(
                WebservicesConstants.OTAConstants.VERSION_PaxCreditReq);
		aaOTAPaxCreditReadRQ.setSequenceNmbr(new BigInteger("1"));
		aaOTAPaxCreditReadRQ.setEchoToken(UID.generate());
		aaOTAPaxCreditReadRQ.setTimeStamp(CommonUtil.parse(new Date()));
		aaOTAPaxCreditReadRQ.setTransactionIdentifier(null);
                
        aaOTAPaxCreditReadRQ.setPaxCreditReadRequest(new AAPaxCreditReadRequestType());
        aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setBookingReferenceID("20150729");
        aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setFirstName("Adam");
        aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setLastName("Bawa");
        //aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setCreditCardNumber("4444");
        //aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setExpiryDate("01/07");
        aaOTAPaxCreditReadRQ.getPaxCreditReadRequest().setExcludeZeroBalanceRecords(false);
        
		return new Object[] {aaOTAPaxCreditReadRQ};
	}

}
