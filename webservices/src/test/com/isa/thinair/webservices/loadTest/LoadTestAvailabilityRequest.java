package com.isa.thinair.webservices.loadTest;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.helpers.AbstractHelper;
import com.isa.thinair.webservices.helpers.CommonTestUtils;
import com.isa.thinair.webservices.helpers.Constants;

public class LoadTestAvailabilityRequest extends AbstractHelper {

	@Override
	public String getOperation() {
		return "getAvailability";
	}

	@Override
	public Object[] getRequestObject(Object[] helpers) throws Exception {

		OTAAirAvailRQ otaAirAvailRQ = new OTAAirAvailRQ();
		otaAirAvailRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirAvailRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		otaAirAvailRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirAvailRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirAvailRQ.setEchoToken(UID.generate());
		otaAirAvailRQ.setTarget(Constants.TARGET_NAME);

		otaAirAvailRQ.setTransactionIdentifier(null);

		POSType pos = new POSType();
		otaAirAvailRQ.setPOS(pos);
		pos.getSource().add(CommonTestUtils.preparePOS());

		otaAirAvailRQ.getOriginDestinationInformation().add(getOriginDestinationInformation((String)helpers[0], (String)helpers[1], (Calendar)helpers[2]));
		TravelerInfoSummaryType travellerInfoSummary = new TravelerInfoSummaryType();
		otaAirAvailRQ.setTravelerInfoSummary(travellerInfoSummary);

		TravelerInformationType travelerInfo = new TravelerInformationType();
		travellerInfoSummary.getAirTravelerAvail().add(travelerInfo);

		PassengerTypeQuantityType paxQuantity = new PassengerTypeQuantityType();
		paxQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		paxQuantity.setQuantity(1);
		travelerInfo.getPassengerTypeQuantity().add(paxQuantity);

		return new Object[]{otaAirAvailRQ};
	}

	private void setOnewaySegment(OTAAirAvailRQ otaAirAvailRQ) throws WebservicesException, ModuleException {
		Calendar deparuteDateCal = new GregorianCalendar();
		deparuteDateCal.set(2010, 10 - 1, 20, 0, 0, 0);
		otaAirAvailRQ.getOriginDestinationInformation().add(getOriginDestinationInformation("CMN", "CDG", deparuteDateCal));
	}

	public static OTAAirAvailRQ.OriginDestinationInformation getOriginDestinationInformation(String originAirport, String destAirport,
			Calendar depDate) throws WebservicesException, ModuleException {
		OTAAirAvailRQ.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRQ.OriginDestinationInformation();

		OriginDestinationInformationType.OriginLocation origin = new OriginDestinationInformationType.OriginLocation();
		origin.setLocationCode(originAirport);
		OriginDestinationInformationType.DestinationLocation destination = new OriginDestinationInformationType.DestinationLocation();
		destination.setLocationCode(destAirport);
		originDestinationInformation.setOriginLocation(origin);
		originDestinationInformation.setDestinationLocation(destination);

		TimeInstantType departureDate = new TimeInstantType();
		departureDate.setValue(CommonUtil.getFormattedDate(depDate.getTime()));

		originDestinationInformation.setDepartureDateTime(departureDate);
		return originDestinationInformation;
	}
}
