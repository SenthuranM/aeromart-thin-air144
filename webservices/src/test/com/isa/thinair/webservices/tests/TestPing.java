package com.isa.thinair.webservices.tests;

import com.isa.thinair.webservices.helpers.PingRequest;

public class TestPing extends BaseTestUtil {
	
	public TestPing(){}
	
	public void testPing(){
		runTest(new PingRequest(),null);
	}

}
