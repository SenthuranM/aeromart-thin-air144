package com.isa.thinair.webservices.tests;

import com.isa.thinair.webservices.helpers.rm.FlightInventorySeatAllocationRequest;


/**
 * @author Byorn
 */
public class TestGetFlightInventoryAndAllocations extends BaseTestUtil {

	/**
     * Test Update Optimized Inventory for RM
     *
	 */
	public void testGetFlightInventoryAndAllocations() {
			runTest(new FlightInventorySeatAllocationRequest(),null);
	}	
    
   
}
