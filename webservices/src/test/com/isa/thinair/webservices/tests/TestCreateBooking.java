package com.isa.thinair.webservices.tests;


import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.webservices.helpers.AvailabilityRequest;
import com.isa.thinair.webservices.helpers.BookRequest;
import com.isa.thinair.webservices.helpers.CancelReservationRequest;
import com.isa.thinair.webservices.helpers.Constants;
import com.isa.thinair.webservices.helpers.PingRequest;
import com.isa.thinair.webservices.helpers.PriceQuoteRequest;
import com.isa.thinair.webservices.helpers.RetreiveResRequest;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
/**
 * 
 * @author WS Team
 *
 */
public class TestCreateBooking extends BaseTestUtil {

	public TestCreateBooking(){
		
	}
	
	private static OTAAirPriceRS priceRS;
	private static OTAAirAvailRS availableRS;
	private static OTAAirBookRS bookRS;
	private static String pnr = "";
	
	public void testPing() {
		runTest(new PingRequest(), null);
	}
	
	public void testAvailability() {
		availableRS = (OTAAirAvailRS) runTest(new AvailabilityRequest(), new Object[]{Constants.JourneyType.ONEWAY, bookRS,null});
		assertNotNull(availableRS);
		assertTrue((availableRS.getOriginDestinationInformation().size() > 0)); //we got flights available
	}	
	
	public void testPriceQuote() {
		priceRS = (OTAAirPriceRS)runTest(new PriceQuoteRequest(), new Object[]{availableRS});
		assertNotNull(priceRS);
	}
	
	
	public void testBook() throws Exception {
		//TODO check if the agent SHJ001 has credits available in the account to do the payment
		bookRS = (OTAAirBookRS)runTest(new BookRequest(), new Object[]{priceRS});
		assertNotNull(bookRS);
	}

	
	public void testRetriveReservation() {
		
		
			for (Object o : bookRS.getSuccessAndWarningsAndAirReservation()) {
				if (o instanceof AirReservation) {
					AirReservation reservation = (AirReservation) o;
					assertEquals(1, reservation.getBookingReferenceID().size());
					pnr = reservation.getBookingReferenceID().get(0).getID();
					break;
				}
			}
			
		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);
		bookRS = (OTAAirBookRS)runTest(new RetreiveResRequest(),new Object[]{pnr,options});
		
		bookRS = (OTAAirBookRS)runTest(new CancelReservationRequest(),new Object[]{bookRS});
	}
}
