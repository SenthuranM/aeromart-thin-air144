/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.tests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.handler.Handler;
import org.codehaus.xfire.security.wss4j.AbstractWSS4JHandler;
import org.codehaus.xfire.security.wss4j.WSS4JOutHandler;
import org.codehaus.xfire.util.dom.DOMOutHandler;
import org.opentravel.ota._2003._05.ErrorsType;


import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.helpers.IRequestObject;
import com.isa.thinair.wsclient.api.acceaero.AAResWebServices;
import com.isa.thinair.wsclient.api.acceaero.AAResWebServicesClient;


/**
 * @author Mohamed Nasly
 * @author Mehdi Raza
 */
public abstract class BaseTestUtil extends TestCase {
	
	protected static AAResWebServices service;

	protected final Log log = LogFactory.getLog(getClass());

	protected static boolean setup = false;

	protected static boolean authenticationAdded = false;
	
	private Object lock = new Object();
	
	protected void setUp() throws Exception {
		if (setup) return;
		super.setUp();

		/*//Create a metadata of the service      
		Properties props = new Properties();
		   props.put(ObjectServiceFactory.STYLE, SoapConstants.STYLE_DOCUMENT);
		   props.put(ObjectServiceFactory.USE, SoapConstants.USE_LITERAL);
		   
		Service serviceModel = new JaxbServiceFactory().create(AAResWebServicesImpl.class,props);
   
        // Create a proxy for the deployed service
        XFire xfire = XFireFactory.newInstance().getXFire();
        XFireProxyFactory factory = new XFireProxyFactory(xfire);      
    
        String serviceUrl = "http://localhost:8080/webservices/services/AAResWebServices";
       
        service=(AAResWebServices)factory.create(serviceModel, serviceUrl);*/
		
		//String serviceUrl = "http://airarabia3o.isaaviations.com/webservices/services/AAResWebServices";
		String serviceUrl = "http://10.200.2.221:8280/webservices/services/AAResWebServices";
		//instantiate client stubs
		service = new AAResWebServicesClient().getAAResWebServicesHttpPort(serviceUrl);
        setup=true;
	}
	
	protected void addAuthenticationInfo(String userName) {
    	Client clientx = ((XFireProxy) Proxy.getInvocationHandler(service)).getClient();
    	if(!authenticationAdded){
            synchronized (lock) {
            	if (!authenticationAdded) {
					clientx.addOutHandler(new DOMOutHandler());
					Properties properties = new Properties();
					properties.setProperty(WSHandlerConstants.ACTION,
							WSHandlerConstants.USERNAME_TOKEN);
					properties.setProperty(WSHandlerConstants.PASSWORD_TYPE,
							WSConstants.PW_TEXT);
					properties.setProperty(WSHandlerConstants.USER, userName);
					properties.setProperty(
							WSHandlerConstants.PW_CALLBACK_CLASS,
							PasswordHandler.class.getName());
					clientx.addOutHandler(new WSS4JOutHandler(properties));
					authenticationAdded = true;
				}else{
		    		for (Iterator i=clientx.getOutHandlers().iterator();i.hasNext();) {
		    			Handler handler=(Handler)i.next();
		    			if (handler instanceof AbstractWSS4JHandler) {
		    				((AbstractWSS4JHandler)handler).setProperty(WSHandlerConstants.USER, userName);
		    				break;
		    			}    			
		    		}
		    		return;
				}
    		}
    	}else{
    		for (Iterator i=clientx.getOutHandlers().iterator();i.hasNext();) {
    			Handler handler=(Handler)i.next();
    			if (handler instanceof AbstractWSS4JHandler) {
    				((AbstractWSS4JHandler)handler).setProperty(WSHandlerConstants.USER, userName);
    				break;
    			}    			
    		}
    		return;
    	}
    }
	
	public static class PasswordHandler implements CallbackHandler {
		private final Log log = LogFactory.getLog(getClass());
		
		private Map<String, String> passwords = new HashMap<String, String>();

		public PasswordHandler() {
			
			passwords.put("serveralias", "aliaspass");
	        passwords.put("client-344-839","client344Password");
	        passwords.put("TESTUSER", "password123");
	        passwords.put("TESTWSDNATA", "password123");
	        passwords.put("TESTWSEBIATM", "password123");
	        passwords.put("TESTWSEBICDM", "password123");
	        passwords.put("TESTWSUSER", "password123");
	        passwords.put("WSEBIATMUSER", "password");
	        passwords.put("WSAAHOLUSER", "password");
            passwords.put("SYSTEM", "3O1saps23#");
            passwords.put("WSRMUSER", "password123");
            passwords.put("WSDNATAUSER", "password123");
            passwords.put("MALAKA", "malaka123");
            
		}
		
		public void handle(Callback[] callbacks) throws IOException,
															UnsupportedCallbackException {
			if (log.isDebugEnabled()) log.debug("BEGIN handle(Callback[])");
			WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
			String id = pc.getIdentifer();
			pc.setPassword(passwords.get(id));
			if (log.isDebugEnabled()) log.debug("END handle(Callback[])");
		}
	}
	
	/**
	 * 
	 * @param requestObject An implementation of IRequestObject interface
	 * @param helpers Any helper objects which needed to be passed to IRequestObject 
	 * implementation for preparing the request. 
	 * For e.g. the price quote request needs to have the availability response object 
	 * @return The result Object from the service
	 * 
	 */
	protected Object runTest(IRequestObject requestObject, Object[] helpers) {
		
		Object response = null;
		
		if (requestObject.requiresAuthentication()) {
			addAuthenticationInfo(requestObject.getPrincipal());
		}
		
		try {
			Object request[] = requestObject.getRequestObject(helpers);
			Class params[] = new Class[request.length];
			for (int x=0; x<request.length; x++) {
				params[x] = request[x].getClass();
			}
		
			System.out.println(String.format("calling method:%s",requestObject.getOperation()));
            Method m = service.getClass().getMethod(requestObject.getOperation(), params);            
			response = m.invoke(service,request);
	        if(AAUtils.isAAMethod(m)){
                
            }else{
               Method methodErrors = response.getClass().getDeclaredMethod("getErrors");
               if (methodErrors != null) {
            	   ErrorsType errors = (ErrorsType)methodErrors.invoke(response);
            	   if (errors != null && errors.getError().size() > 0) throw new Exception(errors.getError().get(0).getShortText());
               }
	        }
	        requestObject.assertResponse(response);
		} catch (Exception e) {
			log.error(e,e);
			fail();
		}
		return response;
	}
}
