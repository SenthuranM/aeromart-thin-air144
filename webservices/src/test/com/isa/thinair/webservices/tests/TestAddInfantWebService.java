package com.isa.thinair.webservices.tests;

import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.webservices.helpers.AddInfant;
import com.isa.thinair.webservices.helpers.AvailabilityRequest;
import com.isa.thinair.webservices.helpers.BalanceForAddInfant;
import com.isa.thinair.webservices.helpers.BookRequest;
import com.isa.thinair.webservices.helpers.Constants;
import com.isa.thinair.webservices.helpers.PriceQuoteRequest;
import com.isa.thinair.webservices.helpers.RetreiveResRequest;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
/**
 * 
 * @author WS Team
 *
 */
public class TestAddInfantWebService extends BaseTestUtil {
	
	private static OTAAirPriceRS priceRS;
	private static OTAAirAvailRS availableRS;
	private static OTAAirBookRS bookRS;
	private static String pnr ;
  
    public void testBlancesForAddInfant() {
    	
    	//create a booking first
    	availableRS = (OTAAirAvailRS) runTest(new AvailabilityRequest(), new Object[]{Constants.JourneyType.ONEWAY, bookRS,null});
    	priceRS = (OTAAirPriceRS)runTest(new PriceQuoteRequest(), new Object[]{availableRS});
    	bookRS = (OTAAirBookRS)runTest(new BookRequest(), new Object[]{priceRS});
		for (Object o : bookRS.getSuccessAndWarningsAndAirReservation()) {
			if (o instanceof AirReservation) {
				AirReservation reservation=(AirReservation)o;
				assertEquals(1, reservation.getBookingReferenceID().size());
				pnr=reservation.getBookingReferenceID().get(0).getID();
				break;				
			}
		}

        AALoadDataOptionsType options = new AALoadDataOptionsType();
        options.setLoadAirItinery(true);
        options.setLoadPriceInfoTotals(true);
        options.setLoadFullFilment(true);
        options.setLoadTravelerInfo(true);
        
		bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[]{pnr, options});

       // AddInfant addInfant = new AddInfant();
        BalanceForAddInfant baladdInfant = new BalanceForAddInfant();
        Object[] helpers = { bookRS, options };
        bookRS = (OTAAirBookRS) runTest(baladdInfant, helpers);
    }

    public void testAddInfant() {

        AALoadDataOptionsType options = new AALoadDataOptionsType();
        options.setLoadAirItinery(true);
        options.setLoadPriceInfoTotals(true);
        options.setLoadFullFilment(true);
        options.setLoadTravelerInfo(true);
        
        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(), new Object[] {
                pnr, options });

       // AddInfant addInfant = new AddInfant();
        AddInfant addInfant = new AddInfant();
        Object[] helpers = { bookRS, options };
        bookRS = (OTAAirBookRS) runTest(addInfant, helpers);
    }
}
