package com.isa.thinair.webservices.tests;

import com.isa.thinair.webservices.helpers.rm.UpdateOptimizedInventoryRequest;
import com.isa.thinair.webservices.tests.BaseTestUtil;


/**
 * @author Byorn
 */
public class TestUpdateOptimizedInventoryRM extends BaseTestUtil {

	/**
     * Test Update Optimized Inventory for RM
     *
	 */
	public void testUpdateOptimizedInventory() {
			runTest(new UpdateOptimizedInventoryRequest(),null);
	}	
    
   
}
