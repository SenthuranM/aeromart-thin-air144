package com.isa.thinair.webservices.tests;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.OTAAirBookRS;

import com.isa.thinair.webservices.helpers.PassengerRefundRequest;
import com.isa.thinair.webservices.helpers.PingRequest;
import com.isa.thinair.webservices.helpers.RetreiveResRequest;
import com.isa.thinair.webservices.tests.BaseTestUtil;
/**
 * 
 * @author Byorn de Silva
 * Test Class used for testing All Scenarios of the passenger Refund
 *
 */
public class TestPassengerRefund extends BaseTestUtil {

	private final Log log = LogFactory.getLog(getClass());
    private static OTAAirBookRS bookRS;
      
    
    
   /* public void testPing() {
        runTest(new PingRequest(),null);
    }*/
    
    //Test Retrival of Reservation By PNR
    public void testRetreiveReservation() {
        String pnr="10000023";
        bookRS = (OTAAirBookRS) runTest(new RetreiveResRequest(),new Object[]{pnr});
    }
    
  
    //refund  the passenger in reservation
    public void testRefundForAllPassengers() {
        PassengerRefundRequest passengerRefundRequest = new PassengerRefundRequest();
        Object[] helpers = {bookRS};
        bookRS=(OTAAirBookRS)runTest(passengerRefundRequest,helpers);
    }
    
    
}
