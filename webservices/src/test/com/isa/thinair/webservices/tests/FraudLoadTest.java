package com.isa.thinair.webservices.tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import net.sourceforge.groboutils.junit.v1.MultiThreadedTestRunner;
import net.sourceforge.groboutils.junit.v1.TestRunnable;

import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.webplatform.core.util.CommonUtil;
import com.isa.thinair.webservices.loadTest.LoadTestAvailabilityRequest;
import com.isa.thinair.webservices.loadTest.LoadTestBookRequest;
import com.isa.thinair.webservices.loadTest.LoadTestPriceQuoteRequest;

public class FraudLoadTest extends BaseTestUtil {
	private static final String FROM_AIRPORT = "CMN";
	private static final String TO_AIRPORT = "CDG";
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	private static final ConcurrentMap<String, String> map = new ConcurrentHashMap<String, String>();
	private static final int nThreads = 20;
	private static final int perThread = 25;

	private class BookingCreator extends TestRunnable {
		
		public BookingCreator() {
			super();
		}

		public void runTest() throws Throwable {
			//foo();
//			long l = Math.round(2 + Math.random() * 5);
//			Thread.sleep(l*1000);
			createBooking();
		}
	}
	
	private void foo(){
		assertTrue(true);
	}
	
	private void createBooking(){
		
		for(int i = 0 ; i < perThread; i++){
			OTAAirPriceRS priceRS;
			OTAAirAvailRS availableRS = null;
			OTAAirBookRS bookRS;
			String tnxID = null;
			try {
				Calendar departureDate = CommonUtil.parseDate("2010-10-10T00:00:00");
				do {
					availableRS = (OTAAirAvailRS) runTest(new LoadTestAvailabilityRequest(),new Object[] { FROM_AIRPORT, TO_AIRPORT,departureDate });
					assertNotNull(availableRS);
					departureDate.add(Calendar.DATE, 1);
				} while ((availableRS.getOriginDestinationInformation().size() == 0));
				
				tnxID = availableRS.getTransactionIdentifier();
				map.put(tnxID, "STARTED");
				System.out.println(String.format("[BEGIN] [TnxID = %s] [CMN] [CDG] [Date = %s]", tnxID, dateFormat.format(departureDate
						.getTime())));
				priceRS = (OTAAirPriceRS) runTest(new LoadTestPriceQuoteRequest(), new Object[] { availableRS });
				assertNotNull(priceRS);
				long startTime = System.currentTimeMillis();
				bookRS = (OTAAirBookRS) runTest(new LoadTestBookRequest(), new Object[] { priceRS });
				long time = (System.currentTimeMillis() - startTime);
				assertNotNull(bookRS);
				for (Object o : bookRS.getSuccessAndWarningsAndAirReservation()) {
					if (o instanceof AirReservation) {
						AirReservation airReservation = (AirReservation) o;
						String pnr = airReservation.getBookingReferenceID().get(0).getID();
						System.out.println(String.format("[END] [TnxID = %s] [pnr = %s]", bookRS.getTransactionIdentifier(), pnr));
						map.put(bookRS.getTransactionIdentifier(), String.format("[pnr = %s] [time = %d]",pnr ,time));
					}
				}
			} catch (Exception e) {
				System.out.println(String.format("[FAIL] [TnxID = %s]", tnxID));
			}
		}
	}

	public void testExampleThread(){

		// instantiate the TestRunnable classes
		TestRunnable[] trs = new TestRunnable[nThreads];
		for(int i = 0 ; i < nThreads ;i++){
			trs[i] = new BookingCreator();
		}
		// pass that instance to the MTTR
		MultiThreadedTestRunner mttr = new MultiThreadedTestRunner(trs);
		try {
			mttr.runTestRunnables();
		} catch (Throwable e) {
			e.printStackTrace();
		}finally{
			for(String key : map.keySet()){
				System.out.println(String.format("[%35s] %s", key,map.get(key)));
			}
		}
	}
}
