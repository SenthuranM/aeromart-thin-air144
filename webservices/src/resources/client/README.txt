Do followings to run the junit tests:

1. Set the WSDL URL in build.properties
2. Run 'gen-client-from-wsdl' ant target for generating client artifacts
3. Run 'run-junit-tests' ant target for running the junit tests