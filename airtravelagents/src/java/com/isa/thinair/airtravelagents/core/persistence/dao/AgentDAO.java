/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.criteria.AdminFeeAgentSearchCriteria;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentTokenTo;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentAlertConfig;
import com.isa.thinair.airtravelagents.api.model.AgentApplicableOND;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.airtravelagents.api.model.AgentTicketStock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * TravelAgentDAO interface.
 * 
 * @author Chamindap
 */
public interface AgentDAO {

	/**
	 * 
	 * @param agnetId
	 * @return
	 */
	public Agent getAgent(String agnetId);

	/**
	 * 
	 * @param agnetId
	 * @return
	 */
	public boolean getShowPriceInSelectedCur(String agentId);

	/**
	 * 
	 * @param agnetId
	 * @return
	 */
	public Agent getAgent(String agnetId, String channel[]);

	/**
	 * 
	 * @param agentIATANumber
	 * @return
	 */
	public Agent getAgentByIATANumber(String agentIATANumber);

	/**
	 * Return Agent's Country Code
	 * 
	 * @param stationCodes
	 * @return
	 */
	public Map<String, String> getAgentCountryCodes(Collection<String> stationCodes);

	/**
	 * Return Agent Ids for country code
	 * 
	 * @param countryCode
	 * @return
	 */
	public Map<String, Collection<String>> getAgentIds(Collection<String> countryCodes);

	/**
	 * 
	 * @param agent
	 */
	public void saveAgent(Agent agent, String userId);

	/**
	 * Removes the agent
	 */
	public void removeAgent(String agentCode, long version) throws CommonsDataAccessException;

	/**
	 * Method to get all agents for given set of agentCodes.
	 * 
	 * @param agentCodes
	 * @return the agents Collection
	 */
	public Collection<Agent> getAgents(Collection<String> agentCodes);

	/**
	 * Method to get all agents for given set of agentCodes.
	 * 
	 * @param agentCodes
	 * @return the agents Map
	 */
	public Map<String, Agent> getAgentsMap(Collection<String> agentCodes);

	/**
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public Page getAgents(int startIndex, int pageSize);
	
	/**
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public Page getActiveAgents(AdminFeeAgentSearchCriteria criteria, int rownum, int pageSize);
	
	public void saveAdminFeeAgent(List agentCodesAddList, List agentCodesDeleteList) throws ModuleException;

	/**
	 * 
	 * @param gsaId
	 * @return
	 */
	public Collection<Agent> getAgentsForGSA(String gsaId);

	/**
	 * 
	 * @param gsaId
	 * @return
	 */
	public Collection<Agent> getReportingAgentsForGSA(String gsaId);

	/**
	 * 
	 * @param territoryId
	 * @return
	 */
	public Collection<Agent> getReportingAgentsForTerritory(String territoryId);

	/**
	 * 
	 * 
	 * @param territoryId
	 * @return
	 */
	public Collection<Agent> getAgentForTerritory(String territoryId);

	/**
	 * 
	 * @param userId
	 * @return
	 */
	// public Agent getAgentForUser(String userId);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public Page searchAgents(List<ModuleCriterion> criteria, int startIndex, int pageSize, List<String> orderByFieldList);

	/**
	 * 
	 * @param agentCode
	 * @return
	 */
	public Collection<AgentCreditHistory> getCreditHistoryForAgent(String agentCode);

	/**
	 * 
	 * @param code
	 * @param locType
	 * @return
	 */
	public Collection<Agent> getAgentsForStation(String code, int locType);

	/**
	 * 
	 * @return
	 */
	public Collection<AgentType> getAgentTypes(List<ModuleCriterion> criteria, List<String> orderByFieldList);

	/**
	 * 
	 * @param agentCode
	 * @param newCreditLimit
	 */
	public void updateAgentCreditLimit(String agentCode, BigDecimal newCreditLimit, BigDecimal newCreditLimitLocal);

	/**
	 * 
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 */
	public void updateAgentAvailableCredit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit);

	/**
	 * 
	 * @param territories
	 * @return
	 */
	public Collection<Agent> getGSAsForTerritories();

	/**
	 * Method to get agent for given territory.
	 * 
	 * @param territoryCode
	 * @return the agent
	 */
	public Agent getGSAForTerritory(String territoryCode);

	/**
	 * Method to get agent for given territory.
	 * 
	 * @param territoryCode
	 * @return the agent
	 */
	public Agent getGSAForTerritory(String territoryCode, String channel);

	/**
	 * Method to get all active agent for given GSA Code.
	 * 
	 * @param gSACode
	 * @return the agent
	 */
	public Collection<Agent> getActiveAgentsForGSA(String gSACode);

	/**
	 * Returns the agent type details of a particular agent type code
	 * 
	 * @param agentTypeCode
	 * @return
	 */
	public AgentType getAgentType(String agentTypeCode);

	public String getAgentTypeByAgentId(String agentId);

	public Collection<Agent> getAgents(String agentTypeCode);

	public BigDecimal getAgentAvailableCredit(String agentCode) throws ModuleException;

	/**
	 * Return All Agents
	 * 
	 * @return
	 */
	public Collection<String> getAllAgents();

	/**
	 * Return All Own Airline Agents
	 * 
	 * @return
	 */
	public Collection<String> getAllOwnAirlineAgents();

	/**
	 * 
	 * @param agentCode
	 * @param newCreditLimit
	 * @param remarks
	 * @param loginId
	 */
	public void addAgentCreditHistory(String agentCode, BigDecimal newCreditLimit, BigDecimal newCreditLimitLocal,
			String specifiedCur, String remarks, String loginId);

	public void addAgentCreditHistory(Collection<AgentCreditHistory> colAgentHistory);

	public List<AgentSummary> getAgentSummary(String agentCode);

	public void saveOrUpdate(Agent agent);

	public void saveOrUpdate(Collection<Agent> agents);

	public void saveOrUpdateAgentSummary(Collection<AgentSummary> agentSummary);

	public Collection<Agent> getAgentswithLocalCurrency(String currency);

	public Collection<Agent> getAgentsCol(Collection<String> colAgentCodes);

	public Page getAgentUsers(String agentCode, int startIndex, int pageSize);

	public Collection<AgentTO> getAllAgentTOs();

	public Collection<User> getUnassignUsers();

	/**
	 * Retrieves Agent details for given location & type
	 * 
	 * @param country
	 * @param city
	 * @param type
	 * @return
	 * @author lalanthi JIRA : AARESAA - 2650
	 */
	public Collection<AgentInfoDTO> getAgentsForLocationAndType(String country, String city, String type);

	public HashMap<String, List<AgentInfoDTO>> getAgentsForCountries(List<String> countryCodes);

	public BigDecimal getUninvocedTotal(String agentCode, Date startDate, Date endDate);

	public List<AgentTicketStock> getAgentSTock(String agentCode);

	public void saveOrUpdateAgentStock(AgentTicketStock agentSTock);

	public void createAgentTicketSequnceRange(String sequenceName, long lowerBound, long upperBound, boolean cycle)
			throws ModuleException;

	public void dropAgentTicketSequnceRange(String sequenceNames) throws ModuleException;

	boolean isValidAgentTicketSequnceRange(String agentCode, long lowerBound, long upperBound, long tktSequenceId)
			throws ModuleException;

	public Page getAgentTicketStocks(String agentCode, int startIndex, int pageSize) throws ModuleException;

	public Long saveAgentTicketStock(AgentTicketSequnce agentTicketSequnce) throws ModuleException;

	public Collection<AgentTicketSequnce> getAgentTicketStocks(String agentCode) throws ModuleException;

	public AgentTicketSequnce getAgentTicketStock(long agentTicketSequnceId) throws ModuleException;

	public void updateAgentTicketStockUpperBound(String sequenceName, long upperBound) throws ModuleException;

	public Map<String, String> getAgentTransationNominalCodes() throws ModuleException;

	public List<String> getGSAAirports(String territoryCode) throws ModuleException;

	public void deactivateTravelAgents();

	public List<Agent> getLCCUnpublishedAgents() throws ModuleException;

	public void updateAgentVersion(String agentCode, long version);

	public void updateAgentPublishStatus(String agentCode, String lccPublished);

	public AgentAlertConfig getAgentAlertConfig(String paymentMethod) throws ModuleException;
	
	public void setAgentToken(AgentToken agentToken) throws ModuleException;

	public AgentToken getAgentToken(String token) throws ModuleException;
	
	public Serializable saveAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException;
	
	public void updateAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException;
	
	public AgentCreditBlock getAgentCreditBlock(Integer blockId, String status) throws ModuleException;

	public void expireAgentToken(int tokenID) throws ModuleException;

	AgentToken getAgentTokenById(String token) throws ModuleException;
	
	public String getCurrencyCodeByStationCode(String station) throws ModuleException;

	public List<AgentTokenTo> getExpiredAgentTokens() throws ModuleException;

	public void removeAgentTokens(List<Integer> agentTokenIds) throws ModuleException;
	
	public void removeAgentToken(int agentTokenId) throws ModuleException;

	public List<String> getSubAgentTypesByAgentCode(String agentCode) throws ModuleException;

	public void deleteAgentApplicableOND(Set<AgentApplicableOND> applicableOND);
	
	public List<String> getAgentWiseApplicableRoutes (String agentCode) throws ModuleException;
	
	public void deleteAgentHandlingFeeModList(Set<AgentHandlingFeeModification> agentHandlingFeeModList);

	public Collection<AgentHandlingFeeModification> getAgentActiveHandlingFeeDefinedForMOD(String agentCode);
	
	public List<String> getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents();
}
