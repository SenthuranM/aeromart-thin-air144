/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.dto.SaveIncentiveDTO;
import com.isa.thinair.airtravelagents.api.dto.SaveIncentiveSlabDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentIncDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author harsha
 * 
 */
public class TravelAgentIncentiveBL {

	private static final Log log = LogFactory.getLog(TravelAgentIncentiveBL.class);

	private static AgentDAO agentDAO = AirTravelagentsModuleUtils.getAgentDAO();

	private static AgentIncDAO agentIncentiveDAO = AirTravelagentsModuleUtils.getAgentIncentiveDAO();

	private final static char FIX_FARE = 'F'; // For
												// Fare
												// only
	private final static char TOTAL_FARE = 'T'; // For
												// Fare
												// +
												// SUR
												// CHG
	private final static char BASIS_ALL_ISSUED = 'S'; // For
														// All
														// Issued
	private final static char BASIS_FLOWN = 'F'; // For
													// Flown
	private final static char YES = 'Y';
	private final static char NO = 'N';
	private final static String ACT = "ACT";
	public static String DEFAULT_DATE_INPUT_FORMAT = "dd/MM/yyyy";

	public static void saveIncentive(SaveIncentiveDTO incentiveSave, UserPrincipal principal) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Executes saveAgentIncentive in TravelAgentIncentiveBL");
		}
		AgentIncentive existIncentive = null;

		if (incentiveSave != null) {
			String agentsID = null;
			if (isNotNullorEmpty(incentiveSave.getAgents())) {
				agentsID = incentiveSave.getAgents();
			}
			if (PlatformUtiltiies.isNotEmptyString(incentiveSave.getAgentIncSchemeID())) {
				existIncentive = agentIncentiveDAO.getAgentIncentiveByID(Long.valueOf(incentiveSave.getAgentIncSchemeID()));
			}
			if (existIncentive == null) {
				existIncentive = new AgentIncentive();
			}
			// validating for existing name
			AgentIncentive existingSameName = agentIncentiveDAO.getAgentIncentive(incentiveSave.getSchemeName());

			if (PlatformUtiltiies.isNotEmptyString(incentiveSave.getVersion())
					&& Long.valueOf(incentiveSave.getVersion()).longValue() < 0) {
				// if a new scheme cannot save new record with same name
				if (existingSameName != null) {
					throw new ModuleException("um.incentive.duplicate");
				}
			} else {
				// if edit--> Should be a different name only if the existing is the current
				if (incentiveSave.getAgentIncSchemeID() != null && existingSameName != null
						&& !existingSameName.getAgentIncSchemeID().equals(Integer.valueOf(incentiveSave.getAgentIncSchemeID()))) {
					throw new ModuleException("um.incentive.duplicate");
				}
			}

			// adds dto data to exsitIncentive object
			existIncentive = convertDTOtoModel(incentiveSave, principal, existIncentive);
			existIncentive.setUserDetails(principal);

			Set<AgentIncentiveSlab> slabSet = existIncentive.getAgentIncentiveSlabs();
			if (slabSet == null || slabSet.isEmpty()) {
				throw new ModuleException("um.incentive.scheme.least.slab");
			}
			agentIncentiveDAO.saveIncentive(existIncentive);
			updateAgentswithIncentive(agentsID, existIncentive.getAgentIncSchemeID());
		}

	}

	private static void updateAgentswithIncentive(String agentsID, Integer incentiveID) {
		AgentIncentive incentiveClnt = agentIncentiveDAO.getAgentIncentive(incentiveID.toString());
		boolean overlap = false;
		Collection<String> lstAgents = new ArrayList<String>();
		if (isNotNullorEmpty(agentsID)) {
			String[] arrAgents = agentsID.split(",");
			lstAgents = Arrays.asList(arrAgents);
			Collection<Agent> colAgents = agentDAO.getAgents(lstAgents);
			if (colAgents != null && !colAgents.isEmpty()) {
				for (Agent agent : colAgents) {
					Set<Integer> setScheme = agent.getSchemeIDs();
					Collection<AgentIncentive> colSchem = agentIncentiveDAO.getAgentIncentive(setScheme);
					for (AgentIncentive incen : colSchem) {
						overlap = checkOverlap(incen, incentiveClnt);
					}
					if (!overlap) {
						setScheme.add(incentiveID);
					}
					agent.setSchemeIDs(setScheme);
				}
				agentDAO.saveOrUpdate(colAgents);
			}
		}

	}

	private static boolean checkOverlap(AgentIncentive incen, AgentIncentive incentiveClnt) {
		Date clntFrm, clntTo, existFrm, existTo;
		clntFrm = incentiveClnt.getEffectiveFrom();
		clntTo = incentiveClnt.getEffectiveTo();
		existFrm = incen.getEffectiveFrom();
		existTo = incen.getEffectiveTo();
		if (clntFrm != null && clntTo != null && existFrm != null && existTo != null) {
			if (existFrm.after(clntFrm) && existFrm.before(clntTo)) {
				return true;
			}
			if (existTo.after(clntFrm) && existTo.before(clntTo)) {
				return true;
			}
		}
		return false;
	}

	private static AgentIncentive convertDTOtoModel(SaveIncentiveDTO incentive, UserPrincipal principal,
			AgentIncentive currentIncentive) throws ModuleException {
		AgentIncentive incentiveSave = currentIncentive;

		Integer incentiveId = null;
		if (isNotNullorEmpty(incentive.getAgentIncSchemeID())) {
			incentiveId = new Integer(incentive.getAgentIncSchemeID());
			incentiveSave.setAgentIncSchemeID(new Integer(incentive.getAgentIncSchemeID()));
		} else {
			incentiveSave.setAgentIncSchemeID(null);
		}
		if (isNotNullorEmpty(incentive.getSchemeName()))
			incentiveSave.setSchemeName(incentive.getSchemeName());
		try {
			if (isNotNullorEmpty(incentive.getSchemePrdFrom()))
				incentiveSave
						.setEffectiveFrom(CalendarUtil.getParsedTime(incentive.getSchemePrdFrom(), DEFAULT_DATE_INPUT_FORMAT));
			if (isNotNullorEmpty(incentive.getSchemePrdTo()))
				incentiveSave.setEffectiveTo(CalendarUtil.getParsedTime(incentive.getSchemePrdTo(), DEFAULT_DATE_INPUT_FORMAT));
		} catch (Exception e) {
			throw new ModuleException("cc_common");
		}
		if (isNotNullorEmpty(incentive.getComments()))
			incentiveSave.setComments(incentive.getComments());
		if (incentive.getIncToalFare() != null
				&& (incentive.getIncToalFare().equals("on") || incentive.getIncToalFare().equals("T") || incentive
						.getIncToalFare().equals("true"))) {
			incentiveSave.setTicketPriceComponent(TOTAL_FARE);
		} else {
			incentiveSave.setTicketPriceComponent(FIX_FARE);
		}
		if (incentive.getIncPromoBookings() != null
				&& (incentive.getIncPromoBookings().equals("on") || incentive.getIncPromoBookings().equals("true") || incentive
						.getIncPromoBookings().equals("Y"))) {
			incentiveSave.setIncPromoBookings(YES);
		} else {
			incentiveSave.setIncPromoBookings(NO);
		}
		if (incentive.getIncOwnTRFBookings() != null
				&& (incentive.getIncOwnTRFBookings().equals("on") || incentive.getIncOwnTRFBookings().equals("true") || incentive
						.getIncOwnTRFBookings().equals("Y"))) {
			incentiveSave.setIncOwnTRFBookings(YES);
		} else {
			incentiveSave.setIncOwnTRFBookings(NO);
		}
		if (incentive.getIncRepAgentSales() != null
				&& (incentive.getIncRepAgentSales().equals("on") || incentive.getIncRepAgentSales().equals("true") || incentive
						.getIncRepAgentSales().equals("Y"))) {
			incentiveSave.setIncRepAgentSales(YES);
		} else {
			incentiveSave.setIncRepAgentSales(NO);
		}
		if (incentive.getIncentiveBasisA() != null
				&& (incentive.getIncentiveBasisA().equals("on") || incentive.getIncentiveBasisA().equals("true") || incentive
						.getIncentiveBasisA().equals("Y"))) {
			incentiveSave.setIncentiveBasis(BASIS_ALL_ISSUED);
		} else {
			incentiveSave.setIncentiveBasis(BASIS_FLOWN);
		}
		if (incentive.getSlabLst() != null) {
			List<SaveIncentiveSlabDTO> slbList = incentive.getSlabLst();
			Iterator<SaveIncentiveSlabDTO> iter = slbList.iterator();
			Set<AgentIncentiveSlab> slabSet = new HashSet<AgentIncentiveSlab>();
			while (iter.hasNext()) {
				SaveIncentiveSlabDTO slabDTO = iter.next();
				AgentIncentiveSlab saveSlab = new AgentIncentiveSlab();
				//Integer shcemeId = null;
				if (slabDTO.getAgentIncSlabID() != null) {
					if (isNotNullorEmpty(slabDTO.getAgentIncSlabID())) {
						saveSlab.setAgentIncSlabID(new Integer(slabDTO.getAgentIncSlabID()));
					} else {
						saveSlab.setAgentIncSlabID(null);
					}
					saveSlab.setAgentIncSchemeID(incentiveId);
					saveSlab.setRateStartValue(new Integer(slabDTO.getRateStartValue()));
					saveSlab.setRateEndValue(new Integer(slabDTO.getRateEndValue()));
					saveSlab.setPersentage(new Integer(slabDTO.getPersentage()));
					if (slabDTO.getVersion() > -1) {
						saveSlab.setVersion(slabDTO.getVersion());
					}
				} else {
					saveSlab.setAgentIncSlabID(null);
					saveSlab.setAgentIncSchemeID(incentiveId);
					saveSlab.setRateStartValue(new Integer(slabDTO.getRateStartValue()));
					saveSlab.setRateEndValue(new Integer(slabDTO.getRateEndValue()));
					saveSlab.setPersentage(new Integer(slabDTO.getPersentage()));
					saveSlab.setVersion(new Integer(0).longValue());
				}
				saveSlab.setUserDetails(principal);
				slabSet.add(saveSlab);
			}
			incentiveSave.setAgentIncentiveSlabs(slabSet);
		}
		if (isNotNullorEmpty(incentive.getVersion())) {
			incentiveSave.setVersion(Long.valueOf(incentive.getVersion()).longValue());
		}

		incentiveSave.setStatus(ACT);

		return incentiveSave;
	}

	private static boolean isNotNullorEmpty(String strVar) {
		if (strVar != null && !"".equals(strVar)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the DateObj.
	 */
	public static Date getStrToDate(String strDate) {

		Calendar cal = Calendar.getInstance();
		if (strDate != null && !"".equals(strDate))
			cal.set(new Integer(strDate.substring(6)), new Integer(strDate.substring(3, 5)), new Integer(strDate.substring(0, 2)));

		Date date = cal.getTime();

		return date;
	}

	public static boolean saveAgentIncentive(Collection<Integer> setIncenID, String agentCode) {
		boolean isAllAgentsSaved = true;
		Agent agent = agentDAO.getAgent(agentCode);
		List<Integer> lstAddedSchemes = new ArrayList<Integer>();
		if (agent != null && setIncenID != null && !setIncenID.isEmpty()) {
			Set<Integer> existSchemes = agent.getSchemeIDs();
			if (existSchemes == null) {
				existSchemes = new HashSet<Integer>();
			}
			for (Integer schemeId : setIncenID) {
				if (!existSchemes.contains(schemeId)) {
					lstAddedSchemes.add(schemeId);
				}
			}
			List<AgentIncentive> lstIncentive = (List<AgentIncentive>) agentIncentiveDAO.getAgentIncentive(lstAddedSchemes);

			if (lstIncentive != null && !lstIncentive.isEmpty()) {
				Date cuuDate = new Date();
				for (AgentIncentive incentive : lstIncentive) {
					Date effecFrom = incentive.getEffectiveFrom();
					Date effecTo = incentive.getEffectiveTo();
					if (cuuDate.before(effecFrom) && cuuDate.before(effecTo)) {
						existSchemes.add(incentive.getAgentIncSchemeID());
					} else {
						isAllAgentsSaved = false;
					}
				}

			}
			agent.setSchemeIDs(existSchemes);
			agentDAO.saveOrUpdate(agent);
		}
		return isAllAgentsSaved;
	}
}