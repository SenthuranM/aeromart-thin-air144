/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.RemoteBindings;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.agentnotify.AgentNotifierBL;
import com.isa.thinair.airtravelagents.api.dto.AgentNotificationDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.dto.ExternalAgentRQDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionsSearchTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTAgentPayTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTPaxPayTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentAlertConfig;
import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.airtravelagents.core.audit.AuditAirTravelAgent;
import com.isa.thinair.airtravelagents.core.bl.TravelAgentBL;
import com.isa.thinair.airtravelagents.core.bl.AdjustAgentCreditBO;
import com.isa.thinair.airtravelagents.core.bl.TravelAgentFinanceBL;
import com.isa.thinair.airtravelagents.core.bl.TravelAgentTransactionBL;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.TravelAgentJdbcDAO;
import com.isa.thinair.airtravelagents.core.service.bd.TravelAgentFinanceDelegateImpl;
import com.isa.thinair.airtravelagents.core.service.bd.TravelAgentFinanceLocalDelegateImpl;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * @author Chamindap / Byorn
 */
@Stateless
@RemoteBindings({ @RemoteBinding(jndiBinding = "TravelAgentFinanceService.remote"),
		@RemoteBinding(jndiBinding = "ejb/TravelAgentFinanceService") })
@LocalBinding(jndiBinding = "TravelAgentFinanceService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TravelAgentFinanceServiceBean extends PlatformBaseSessionBean
		implements TravelAgentFinanceDelegateImpl, TravelAgentFinanceLocalDelegateImpl {

	public static String AGENT_COLLECTIONDAO = "agentCollectionDAO";

	public static String INVOICEDAO = "invoiceDAO";

	private final Log log = LogFactory.getLog(getClass());

	/***
	 * @param agentCode
	 * @param creditLimit
	 * @param availableCredit
	 * @param agentcollection
	 * @throws ModuleException
	 */
	public void postAgentCollection(String agentCode, BigDecimal creditLimit, BigDecimal availableCredit,
			AgentCollection agentcollection, boolean forceCancel) throws ModuleException {
		TravelAgentFinanceBL travelAgentFinanceBL = null;
		String userId = null;
		Integer salesChannelCode = null;

		travelAgentFinanceBL = new TravelAgentFinanceBL();
		// userId = "TEST_USER";

		if (getUserPrincipal() != null) {
			userId = getUserPrincipal().getUserId();
			salesChannelCode = getUserPrincipal().getSalesChannel();
		}

		try {

			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
			String creditSharedAgentCode = null;
			if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
				creditSharedAgentCode = creditSharedAgent.getAgentCode();
			}

			travelAgentFinanceBL.postAgentCollection(agentCode, creditLimit, availableCredit, agentcollection, forceCancel,
					userId, salesChannelCode, creditSharedAgentCode);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("postAgentCollection  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("postAgentCollection", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	public void agentTopupEmailNotification(String amount,String agentCode)  throws ModuleException  {
		AgentNotifierBL.sendTopupEmailNotification(amount,agentCode);
	}

	/**
	 * @param agentCode
	 * @param amount
	 * @throws ModuleException
	 */
	public void recordDebit(String agentCode, BigDecimal amount, boolean force, String pnr) throws ModuleException {
		TravelAgentFinanceBL travelAgentFinanceBL = null;
		String userId = null;

		travelAgentFinanceBL = new TravelAgentFinanceBL();
		int nominalCode = AgentTransactionNominalCode.ACC_SALE.getCode();
		Integer salesChannelCode = null;

		if (getUserPrincipal() != null) {
			userId = getUserPrincipal().getUserId();
			salesChannelCode = getUserPrincipal().getSalesChannel();
		}

		try {

			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
			String creditSharedAgentCode = null;
			if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
				creditSharedAgentCode = creditSharedAgent.getAgentCode();
			}

			travelAgentFinanceBL
					.recordDebit(agentCode, amount, nominalCode, userId, salesChannelCode, pnr, creditSharedAgentCode);
			getAgentCollectionDAO().recordDebit(creditSharedAgent.getAgentCode(), amount, force);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}

	}

	/**
	 * @param agentCode
	 * @param amount
	 * @throws ModuleException
	 */
	public void recordCredit(String agentCode, BigDecimal amount, String pnr) throws ModuleException {
		TravelAgentFinanceBL travelAgentFinanceBL = null;
		String userId = null;

		int nominalCode = AgentTransactionNominalCode.ADJ.getCode();
		travelAgentFinanceBL = new TravelAgentFinanceBL();
		Integer salesChannelCode = null;

		if (getUserPrincipal() != null) {
			userId = getUserPrincipal().getUserId();
			salesChannelCode = getUserPrincipal().getSalesChannel();
		}

		try {

			Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
			String creditSharedAgentCode = null;
			if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
				creditSharedAgentCode = creditSharedAgent.getAgentCode();
			}

			travelAgentFinanceBL.recordCredit(agentCode, amount, nominalCode, userId, salesChannelCode, pnr,
					null, creditSharedAgentCode);
			getAgentCollectionDAO().recordCredit(creditSharedAgent.getAccountCode(), amount);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordCredit failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	/**
	 * private method to execute the command
	 * 
	 * @param command
	 * @return response
	 * @throws ModuleException
	 */
	public ServiceResponce execute(Command command) throws ModuleException {

		try {
			return command.execute();
		} catch (ModuleException ex) {
			this.sessionContext.setRollbackOnly();
			throw ex;
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	/**
	 * Gets AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	private AgentCollectionDAO getAgentCollectionDAO() {
		return (AgentCollectionDAO) AirTravelagentsModuleUtils.getDAO(AGENT_COLLECTIONDAO);
	}

	public Integer recordSale(String agentCode, BigDecimal amount, String remarks, String pnr, PayCurrencyDTO payCurrencyDTO)
			throws ModuleException {
		AgentTransaction agentTransaction = new TravelAgentTransactionBL().recordSale(getUserPrincipal(), agentCode, amount,
				remarks, pnr, payCurrencyDTO, null, null);
		return agentTransaction.getTnxId();
	}

	public AgentTransaction recordSale(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType, String orderId)
			throws ModuleException {
		return new TravelAgentTransactionBL().recordSale(getUserPrincipal(), agentCode, amount, remarks, pnr, payCurrencyDTO,
				paymentReferenceTO, productType);
	}
	
	public AgentTransaction recordExternalProductSale(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType, String orderId)
			throws ModuleException {
		return new TravelAgentTransactionBL().recordExternalProductSale(getUserPrincipal(), agentCode, amount, remarks, pnr, payCurrencyDTO,
				paymentReferenceTO, productType);
	}

	public AgentTotalSaledDTO recordRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO) throws ModuleException {
		return new TravelAgentTransactionBL().recordRefund(getUserPrincipal(), agentCode, amount, remarks, pnr, payCurrencyDTO,
				null, null);
	}

	public AgentTotalSaledDTO recordRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType) throws ModuleException {
		return new TravelAgentTransactionBL().recordRefund(getUserPrincipal(), agentCode, amount, remarks, pnr, payCurrencyDTO,
				paymentReferenceTO, productType);
	}

	public AgentTransaction recordAgentCommission(String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn) throws ModuleException {
		return new TravelAgentTransactionBL().recordAgentCommission(getUserPrincipal(), agentCode, amount, remarks, pnr,
				amountSpecifiedIn);
	}

	public AgentTransaction removeAgentCommission(String agentCode, String userId, Integer salesChannelCode, BigDecimal amount,
			String remarks, String pnr, String amountSpecifiedIn) throws ModuleException {
		return new TravelAgentTransactionBL().removeAgentCommission(agentCode, userId, salesChannelCode, amount, remarks, pnr,
				amountSpecifiedIn);
	}

	@Override
	public BigDecimal getPendingAgentCommissions(String agentCode, String pnr) throws ModuleException {
		return getTravelAgentJdbcDAO().getPendingAgentCommissions(agentCode, pnr);
	}

	@Override
	public BigDecimal getTotalOfferedAgentCommissions(String agentCode, Date periodStartDate, Date periodEndDate)
			throws ModuleException {
		return getTravelAgentJdbcDAO().getTotalOfferedAgentCommissions(agentCode, periodStartDate, periodEndDate);
	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param paymentType
	 * @throws ModuleException
	 */
	public void recordPayment(String agentCode, BigDecimal amount, String remarks, CollectionTypeEnum collectionTypeEnum,
			String amountSpecifiedIn, String strCarrier, String strReceiptNumber) throws ModuleException {
		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		int txnId = transactionBL.recordPayment(getUserPrincipal(), agentCode, amount, remarks, collectionTypeEnum,
				amountSpecifiedIn, strCarrier, strReceiptNumber);

		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("amount", String.valueOf(amount));
		contents.put("txnID", String.valueOf(txnId));
		contents.put("txnType", collectionTypeEnum.getCode());
		contents.put("remarks", remarks.trim());
		contents.put("curSpecIn", amountSpecifiedIn);
		contents.put("strCarrier", strCarrier);
		AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordDebit(String agentCode, BigDecimal amount, String remarks, String pnr, String curSpecIn, String strCarrier,
			String strReceiptNumber) throws ModuleException {
		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		int txnId = transactionBL.recordDebit(getUserPrincipal(), agentCode, amount, remarks, pnr, curSpecIn, strCarrier,
				strReceiptNumber);

		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("amount", String.valueOf(amount));
		contents.put("txnID", String.valueOf(txnId));
		contents.put("txnType", CollectionTypeEnum.DEBIT.getCode());
		contents.put("remarks", remarks.trim());
		contents.put("curSpecIn", curSpecIn);
		contents.put("strCarrier", strCarrier);
		AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String curSpecIn, String strCarrier,
			String strReceiptNumber) throws ModuleException {
		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		int txnId = transactionBL.recordCredit(getUserPrincipal(), agentCode, amount, remarks, pnr, curSpecIn, strCarrier,
				strReceiptNumber);

		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("amount", String.valueOf(amount));
		contents.put("txnID", String.valueOf(txnId));
		contents.put("txnType", CollectionTypeEnum.CREDIT.getCode());
		contents.put("remarks", remarks.trim());
		contents.put("curSpecIn", curSpecIn);
		contents.put("strCarrier", strCarrier);
		AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

	}

	public Collection<AgentTransaction> getAgentTransactions(AgentTransactionsDTO agentTransactionsDTO) throws ModuleException {
		try {
			AgentTransactionDAO agentTransactionDAO = AirTravelagentsModuleUtils.getAgentTransactionDAO();
			return agentTransactionDAO.getAgentTransactions(agentTransactionsDTO);
		} catch (CommonsDataAccessException commonsDataAccessException) {
			log.error("Agent Transaction DAO threw exception :" + commonsDataAccessException);
			throw new ModuleException(commonsDataAccessException, commonsDataAccessException.getExceptionCode(),
					"airtravelagent.desc");

		}
	}

	public Collection<AgentTransactionTO> getAgentTransactions(AgentTransactionsSearchTO agentTransactionsSearchTO)
			throws ModuleException {
		try {
			AgentTransactionDAO agentTransactionDAO = AirTravelagentsModuleUtils.getAgentTransactionDAO();
			Collection<AgentTransaction> colAgentTransaction = agentTransactionDAO
					.getAgentTransactions(new AgentTransactionsDTO(agentTransactionsSearchTO));
			Collection<AgentTransactionTO> colAgentTransactionTO = new ArrayList<AgentTransactionTO>();
			if (colAgentTransaction != null && colAgentTransaction.size() > 0) {
				for (AgentTransaction agentTransaction : colAgentTransaction) {
					colAgentTransactionTO.add(agentTransaction.toAgentTransactionTO());
				}
			}

			return colAgentTransactionTO;
		} catch (CommonsDataAccessException commonsDataAccessException) {
			log.error("Agent Transaction DAO threw exception :" + commonsDataAccessException);
			throw new ModuleException(commonsDataAccessException, commonsDataAccessException.getExceptionCode(),
					"airtravelagent.desc");
		}
	}

	/**
	 * @param receiptNumber
	 */
	public AgentTransaction getAgentTransaction(String receiptNumber) throws ModuleException {
		AgentTransactionDAO agentTransactionDAO = AirTravelagentsModuleUtils.getAgentTransactionDAO();
		try {
			return agentTransactionDAO.getAgentTransaction(receiptNumber);
		} catch (CommonsDataAccessException commonsDataAccessException) {
			log.error("Agent Transaction DAO threw exception :" + commonsDataAccessException);
			throw new ModuleException(commonsDataAccessException, commonsDataAccessException.getExceptionCode(),
					"airtravelagent.desc");
		}
	}

	/**
	 * @param fromDate
	 * @param toDate
	 */
	public void recordPaymentReverse(String agentCode, BigDecimal amount, String remarks, String curSpecIn, String strCarrier,
			String strReceiptNumber) throws ModuleException {

		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		int txnId = transactionBL.recordPaymentReverse(getUserPrincipal(), agentCode, amount, remarks, curSpecIn, strCarrier,
				strReceiptNumber);

		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("amount", String.valueOf(amount));
		contents.put("txnID", String.valueOf(txnId));
		contents.put("txnType", CollectionTypeEnum.REVERSE.getCode());
		contents.put("remarks", remarks.trim());
		contents.put("curSpecIn", curSpecIn);
		contents.put("strCarrier", strCarrier);
		AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

	}

	/**
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getAgentAvailableCredit(String agentCode) throws ModuleException {
		return AirTravelagentsModuleUtils.getAgentDAO().getAgentAvailableCredit(agentCode);
	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordExternalAccountPay(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount, String remarks,
			String pnr) throws ModuleException {
		new TravelAgentTransactionBL().recordExternalAccountPay(getUserPrincipal(), colPaxPays, agentCode, amount, remarks, pnr,
				getUserPrincipal().getUserId());
	}

	/**
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @throws ModuleException
	 */
	public void recordExternalAccountRefund(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount,
			String remarks, String pnr) throws ModuleException {
		new TravelAgentTransactionBL().recordExternalAccountRefund(getUserPrincipal(), colPaxPays, agentCode, amount, remarks,
				pnr, getUserPrincipal().getUserId());
	}

	/**
	 * 
	 */
	public void recordExternalAccountPay(EXTAgentPayTO extAgentTo) throws ModuleException {
		new TravelAgentTransactionBL().recordExternalAccountPay(getUserPrincipal(), extAgentTo.getColPaxPayTos(),
				extAgentTo.getAgentCode(), extAgentTo.getAmount(), extAgentTo.getRemarks(), extAgentTo.getPnr(),
				extAgentTo.getUserId());
	}

	/**
	 * 
	 */
	public void recordExternalAccountRefund(EXTAgentPayTO extAgentTo) throws ModuleException {
		new TravelAgentTransactionBL().recordExternalAccountRefund(getUserPrincipal(), extAgentTo.getColPaxPayTos(),
				extAgentTo.getAgentCode(), extAgentTo.getAmount(), extAgentTo.getRemarks(), extAgentTo.getPnr(),
				extAgentTo.getUserId());
	}

	public void updateAgentSummarySettle(String agentCode, BigDecimal amount) throws ModuleException {
		getAgentCollectionDAO().updateAgentSummarySettle(agentCode, amount);
	}

	public void recordBSPCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String curSpecIn,
			String strCarrier, String strReceiptNumber) throws ModuleException {
		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		int txnId = transactionBL.recordBSPCredit(getUserPrincipal(), agentCode, amount, remarks, pnr, curSpecIn, strCarrier,
				strReceiptNumber);

		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("amount", String.valueOf(amount));
		contents.put("txnID", String.valueOf(txnId));
		contents.put("txnType", CollectionTypeEnum.BSP.getCode());
		contents.put("remarks", remarks.trim());
		contents.put("curSpecIn", curSpecIn);
		contents.put("strCarrier", strCarrier);
		AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

	}

	/**
	 * Send an email to the agent and airline accounts department when bank guarantee expire date reaches given values
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendBankGuaranteeNotificationEmail() throws ModuleException {

		if (log.isInfoEnabled()) {
			log.info("##################### SENDING BANK GUARANTEE NOTIFICATION ################################");
		}

		try {
			List<String> notificationIntervals = AppSysParamsUtil.getBankGuaranteeExpiryNotificationIntervals();

			List<AgentNotificationDTO> traAgentNotificationDTOs = getTravelAgentJdbcDAO()
					.getAgentsWithinExpiryNotificationPeriod(notificationIntervals);

			if (!traAgentNotificationDTOs.isEmpty()) {

				new AgentNotifierBL().sendAgentNotificationEmails(traAgentNotificationDTOs, getUserPrincipal(),
						ReservationInternalConstants.BankGuranteeNotifyTemplates.BANK_GUARANTEE_EXPIRY_NOTIFICATION, 0,
						AppSysParamsUtil.getBankGuaranteeNotifyingEmailAddresses());

			} else {
				if (log.isInfoEnabled()) {
					log.info("No Travel Agents to send Bank Guarantee Expiry Notifications");
				}
			}

			if (log.isInfoEnabled()) {
				log.info("##################### BANK GUARANTEE NOTIFICATION SENT ################################");
			}
		} catch (CommonsDataAccessException e) {
			if (log.isErrorEnabled()) {
				log.error("Error in sending bank guarantee email notification");
			}

			throw new ModuleException(e.getExceptionCode(), e);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendCreditLimitUtilizationEmail() throws ModuleException {

		if (log.isInfoEnabled()) {
			log.info("##################### SENDING CREDIT LIMIT UTILIZATION NOTIFICATION ################################");
		}

		// Get agent alert configuration
		AgentAlertConfig agentAlertConfig = AirTravelagentsModuleUtils.getAgentDAO()
				.getAgentAlertConfig(Agent.PAYMENT_MODE_ONACCOUNT);

		if (agentAlertConfig == null) {
			if (log.isErrorEnabled()) {
				log.error(
						"Error in sending agent credit limit email notification. No configuration for On Account(OA) payment type.");
			}

			throw new ModuleException(
					"Error in sending agent credit limit email notification. No configuration for On Account(OA) payment type.",
					new Throwable());
		}

		try {
			List<Pair<Double, Double>> limitPairs = AppSysParamsUtil
					.getDoublePairOfValuesFromString(agentAlertConfig.getNotifyingLimits(), AgentAlertConfig.ENTRY_SEPARATOR);

			Set<AgentNotificationDTO> notificationDTOs = getTravelAgentJdbcDAO().getAgentsWithCreditLimitNotification(limitPairs,
					agentAlertConfig.getNotifyIntervalInDays(), Agent.PAYMENT_MODE_ONACCOUNT);

			if (!notificationDTOs.isEmpty()) {
				// Add all to a List
				List<AgentNotificationDTO> agentNotificationDTOs = new ArrayList<AgentNotificationDTO>();
				agentNotificationDTOs.addAll(notificationDTOs);

				new AgentNotifierBL().sendAgentNotificationEmails(agentNotificationDTOs, getUserPrincipal(),
						ReservationInternalConstants.BankGuranteeNotifyTemplates.CREDIT_LIMIT_UTILIZATION_NOTIFICATION,
						agentAlertConfig.getNotifyIntervalInDays(), agentAlertConfig.getNotifyingEmails());
			} else {
				if (log.isInfoEnabled()) {
					log.info("No Travel Agents to send Credit Limit Utilization Notifications");
				}
			}

			if (log.isInfoEnabled()) {
				log.info("##################### CREDIT LIMIT UTILIZATION NOTIFICATION SENT ################################");
			}

		} catch (CommonsDataAccessException e) {
			if (log.isErrorEnabled()) {
				log.error("Error in sending credit limit email notification");
			}

			throw new ModuleException(e.getExceptionCode(), e);
		}

	}

	public List<AgentTransaction> isDuplicateRecordExistForAgent(String agentCode, BigDecimal amount, int nominalCode)
			throws ModuleException {
		TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
		return transactionBL.isDuplicateRecordExistForAgent(agentCode, amount, nominalCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendBSPCreditLimitUtilizationEmail() throws ModuleException {
		if (log.isInfoEnabled()) {
			log.info("##################### SENDING BSP CREDIT LIMIT UTILIZATION NOTIFICATION ################################");
		}

		// Get agent alert configuration
		AgentAlertConfig agentAlertConfig = AirTravelagentsModuleUtils.getAgentDAO().getAgentAlertConfig(Agent.PAYMENT_MODE_BSP);

		if (agentAlertConfig == null) {
			if (log.isErrorEnabled()) {
				log.error("Error in sending BSP credit limit email notification. No configuration for BSP payment type.");
			}

			throw new ModuleException(
					"Error in sending BSP credit limit email notification. No configuration for BSP payment type.",
					new Throwable());
		}

		try {
			List<Pair<Double, Double>> limitPairs = AppSysParamsUtil
					.getDoublePairOfValuesFromString(agentAlertConfig.getNotifyingLimits(), AgentAlertConfig.ENTRY_SEPARATOR);

			Set<AgentNotificationDTO> notificationDTOs = getTravelAgentJdbcDAO().getAgentsWithCreditLimitNotification(limitPairs,
					agentAlertConfig.getNotifyIntervalInDays(), Agent.PAYMENT_MODE_BSP);

			if (!notificationDTOs.isEmpty()) {
				List<AgentNotificationDTO> agentNotificationDTOs = new ArrayList<AgentNotificationDTO>(notificationDTOs);

				new AgentNotifierBL().sendAgentNotificationEmails(agentNotificationDTOs, getUserPrincipal(),
						ReservationInternalConstants.BankGuranteeNotifyTemplates.BSP_CREDIT_LIMIT_UTILIZATION_NOTIFICATION,
						agentAlertConfig.getNotifyIntervalInDays(), agentAlertConfig.getNotifyingEmails());
			} else {
				if (log.isInfoEnabled()) {
					log.info("No Travel Agents to send BSP Credit Limit Utilization Notifications");
				}
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Error in sending BSP credit limit email notification");
			}

			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			}

			throw new ModuleException("Error in sending BSP credit limit email notification ", e);
		}

		if (log.isInfoEnabled()) {
			log.info("##################### CREDIT BSP LIMIT UTILIZATION NOTIFICATION SENT ################################");
		}
	}

	public void recordCredit(String agentCode, BigDecimal amount, String pnr, int nominalCode, Character productType) throws ModuleException {
		TravelAgentFinanceBL travelAgentFinanceBL = null;
		String userId = null;

		travelAgentFinanceBL = new TravelAgentFinanceBL();
		Integer salesChannelCode = null;

		if (getUserPrincipal() != null) {
			userId = getUserPrincipal().getUserId();
			salesChannelCode = getUserPrincipal().getSalesChannel();
		}

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}

		// TODO : MANoji , Please remove this after proper fix
		if ((userId == null || userId.isEmpty()) && (nominalCode == AgentTransactionNominalCode.BLOCK_CREDIT.getCode()
				|| nominalCode == AgentTransactionNominalCode.RELEASE_BLOCKED_CREDIT.getCode())) {
			userId = "ABYSYSTEM";
		}

		try {
			travelAgentFinanceBL.recordCredit(agentCode, amount, nominalCode, userId, salesChannelCode, pnr, null,
					creditSharedAgentCode);
			getAgentCollectionDAO().recordCredit(agentCode, amount);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordCredit failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	public void recordDebit(String agentCode, BigDecimal amount, boolean force, String pnr, int nominalCode)
			throws ModuleException {
		TravelAgentFinanceBL travelAgentFinanceBL = null;
		String userId = null;

		travelAgentFinanceBL = new TravelAgentFinanceBL();
		Integer salesChannelCode = null;

		if (getUserPrincipal() != null) {
			userId = getUserPrincipal().getUserId();
			salesChannelCode = getUserPrincipal().getSalesChannel();
		}

		Agent creditSharedAgent = TravelAgentBL.getCreditSharedAgent(agentCode);
		String creditSharedAgentCode = null;
		if (!agentCode.equals(creditSharedAgent.getAgentCode())) {
			creditSharedAgentCode = creditSharedAgent.getAgentCode();
		}
		// TODO : MANoji , Please remove this after proper fix
		if ((userId == null || userId.isEmpty()) && (nominalCode == AgentTransactionNominalCode.BLOCK_CREDIT.getCode()
				|| nominalCode == AgentTransactionNominalCode.RELEASE_BLOCKED_CREDIT.getCode())) {
			userId = "ABYSYSTEM";
		}

		try {
			travelAgentFinanceBL.recordDebit(agentCode, amount, nominalCode, userId, salesChannelCode, pnr, creditSharedAgentCode);
			getAgentCollectionDAO().recordDebit(agentCode, amount, force);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}

	}

	public Integer blockAgentCredit(AgentCreditBlock agentCreditBlock) throws ModuleException {
		Integer blockId = null;
		try {
			blockId = (Integer) AirTravelagentsModuleUtils.getAgentDAO().saveAgentCreditBlock(agentCreditBlock);
			recordDebit(agentCreditBlock.getAgentCode(), agentCreditBlock.getLocalAmount(), false, "",
					AgentTransactionNominalCode.BLOCK_CREDIT.getCode());

		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
		return blockId;
	}

	public AgentTransaction confirmBlockedCredit(AgentToken agentToken, ExternalAgentRQDTO confirmRQ,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException {
		AgentTransaction agentTransaction = null;
		try {
			AgentCreditBlock agentCreditBlock = null;
			if (isExistingExternalReference(confirmRQ.getOrderId())) {
				throw new ModuleException("external.agent.orderid.already.exists");
			} else {
				agentTransaction = recordSale(agentToken.getAgentCode(), new BigDecimal(confirmRQ.getAmount()), "", "",
						payCurrencyDTO, paymentReferenceTO, PromotionCriteriaConstants.ProductType.GOQUO, confirmRQ.getOrderId());
				if (!confirmRQ.getBlockId().isEmpty()) {
					agentCreditBlock = getAgentCreditBlock(new Integer(confirmRQ.getBlockId()), AgentCreditBlock.BLOCKED);
					if (agentCreditBlock == null || !agentCreditBlock.getStatus().equals(AgentCreditBlock.BLOCKED)) {
						throw new ModuleException("external.agent.invalid.blockId");
					} else {
						if (agentCreditBlock.getStatus().equals(AgentCreditBlock.REVERSED)) {
							throw new ModuleException("external.agent.inactive.blockId");
						} else if (agentCreditBlock.getStatus().equals(AgentCreditBlock.CONFRIMED)) {
							throw new ModuleException("external.agent.invalid.blockId");
						}
					}

					if (agentCreditBlock.getLocalAmount().compareTo(new BigDecimal(confirmRQ.getAmount())) != 0) {
						throw new ModuleException("external.agent.invalid.amount.for.block");
					}
					agentCreditBlock.setStatus(AgentCreditBlock.CONFRIMED);
					agentCreditBlock.setConfirmTxnId(agentTransaction.getTnxId());
					updateAgentCreditBlock(agentCreditBlock);
					recordCredit(agentToken.getAgentCode(), new BigDecimal(confirmRQ.getAmount()), "",
							AgentTransactionNominalCode.RELEASE_BLOCKED_CREDIT.getCode(), null);
				}
			}
			return agentTransaction;
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	public AgentTransaction externalPayment(AgentToken agentToken, ExternalAgentRQDTO confirmRQ, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO) throws ModuleException {
		AgentTransaction agentTransaction = null;
		try {
			agentTransaction = recordExternalProductSale(agentToken.getAgentCode(), new BigDecimal(confirmRQ.getAmount()), "",
					"", payCurrencyDTO, paymentReferenceTO, PromotionCriteriaConstants.ProductType.GOQUO, confirmRQ.getOrderId());
			return agentTransaction;
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	public void updateAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException {
		AirTravelagentsModuleUtils.getAgentDAO().updateAgentCreditBlock(agentCreditBlock);
	}

	public AgentCreditBlock getAgentCreditBlock(Integer blockId, String status) throws ModuleException {
		return AirTravelagentsModuleUtils.getAgentDAO().getAgentCreditBlock(blockId, status);
	}
	
	public boolean isExistingExternalReference(String extRef) {
		return (AirTravelagentsModuleUtils.getAgentTransactionDAO().getAgentTransactionByExtRef(extRef) != null);
	}
	
	public AgentTotalSaledDTO recordExternalProductRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType) throws ModuleException {
		return new TravelAgentTransactionBL().recordExternalProductRefund(getUserPrincipal(), agentCode, amount, remarks, pnr,
				payCurrencyDTO, paymentReferenceTO, productType);
	}

	public void recordSubAgentOperationDebit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currency,
			String userId, Integer salesChannelCode, String subAgentOperation) throws ModuleException {
		(new TravelAgentFinanceBL()).recordSubAgentOperationDebit(agentCode, amount, amountInLocal, currency, userId,
				salesChannelCode, subAgentOperation);
	}

	public void recordSubAgentOperationCredit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currency,
			String userId, Integer salesChannelCode, String subAgentOperation) throws ModuleException {
		(new TravelAgentFinanceBL()).recordSubAgentOperationCredit(agentCode, amount, amountInLocal, currency, userId,
				salesChannelCode, subAgentOperation, null);
	}

	/**
	 * get the travel agent JDBC DAO
	 * 
	 * @return
	 */
	private static TravelAgentJdbcDAO getTravelAgentJdbcDAO() {
		return AirTravelagentsModuleUtils.getAgentJdbcDAO();
	}
	
	public void reverseCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber, String originalTnxId) throws ModuleException {

		try {

			TravelAgentTransactionBL transactionBL = new TravelAgentTransactionBL();
			int txnId = transactionBL.reverseCredit(getUserPrincipal(), agentCode, amount, remarks, pnr, amountSpecifieidIn,
					strCarrier, strReceiptNumber, originalTnxId);

			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			contents.put("amount", String.valueOf(amount));
			contents.put("txnID", String.valueOf(txnId));
			contents.put("txnType", CollectionTypeEnum.REVERSE_CREDIT.getCode());
			contents.put("remarks", remarks.trim());
			contents.put("curSpecIn", amountSpecifieidIn);
			contents.put("strCarrier", strCarrier);
			AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.RECORD_PAYMENT, getUserPrincipal().getUserId(), contents);

		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("reverseCredit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (ModuleException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("reverseCredit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("reverseCredit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}

	}

	@Override
	public Integer makeTemporyPaymentEntry(ReservationContactInfo reservationContactInfo, PaymentInfo paymentInfo, TrackInfoDTO trackInfoDTO, boolean isCredit, boolean isOfflinePayment) throws ModuleException{
		Integer tempTxnId = null;
		CardPaymentInfo cardPaymentInfo = null;
		if (paymentInfo instanceof CardPaymentInfo) {
			cardPaymentInfo = (CardPaymentInfo) paymentInfo;
		}
		tempTxnId = AdjustAgentCreditBO.recordTemporyPaymentEntry(reservationContactInfo, cardPaymentInfo, trackInfoDTO, isCredit, isOfflinePayment);
		return tempTxnId;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean recordselfTopup(String agentCode, BigDecimal amount, String remarks, CollectionTypeEnum collectionTypeEnum,
			String amountSpecifiedIn, String strCarrier, String strReceiptNumber) throws ModuleException {
		boolean returnVal = true;
		try{
		recordPayment(agentCode, amount, remarks, collectionTypeEnum, amountSpecifiedIn, strCarrier, strReceiptNumber);
		} catch (ModuleException ex){
			returnVal = false;
			log.error("Error in saving topup data...", ex);
			throw new ModuleException(ex, AirTravelAgentConstants.MODULE_NAME);
		}
		return returnVal;

	}
}
