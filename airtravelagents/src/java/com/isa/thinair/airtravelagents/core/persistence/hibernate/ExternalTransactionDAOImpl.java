/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.util.Collection;

import com.isa.thinair.airtravelagents.api.model.PaxExtTransaction;
import com.isa.thinair.airtravelagents.core.persistence.dao.ExternalTransactionDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Thushara
 * @isa.module.dao-impl dao-name="externalTransactionDAO"
 */
public class ExternalTransactionDAOImpl extends PlatformHibernateDaoSupport implements ExternalTransactionDAO {

	/**
	 * The constructor.
	 */
	public ExternalTransactionDAOImpl() {
		super();
	}

	/**
	 * Save or update external transaction data.
	 * 
	 * @param paxExtTransaction
	 *            the paxExtTransaction.
	 * 
	 */
	public void saveOrUpdate(PaxExtTransaction paxExtTransaction) {
		try {
			super.hibernateSaveOrUpdate(paxExtTransaction);
		} catch (RuntimeException e) {

		}
	}

	/**
	 * Remove given PaxExtTransaction.
	 * 
	 * @param paxExtTransactionId
	 *            .
	 * 
	 */
	public void removePaxExtTransaction(int paxExtTransactionId) {
		Integer tnsId = new Integer(paxExtTransactionId);

		Object externalTransaction = load(PaxExtTransaction.class, tnsId);
		delete(externalTransaction);
	}

	/**
	 * 
	 * Get a given AgentTransaction.
	 * 
	 * @param agentTransactionId
	 *            agentTransactionId.
	 * @return the agentTransactionId.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao .AgentTransactionDAO#getAgentTransaction(int)
	 */
	public PaxExtTransaction getPaxExtTransaction(int paxExtTransactionId) {
		Integer tnsId = new Integer(paxExtTransactionId);

		return (PaxExtTransaction) get(PaxExtTransaction.class, tnsId);
	}

	/**
	 * Save the external transactions.
	 * 
	 * @param colExtTrans
	 *            the collection of external transactions.
	 */
	public void saveExtTransactions(Collection<PaxExtTransaction> colExtTrans) {

		hibernateSaveOrUpdateAll(colExtTrans);

	}

}