/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import java.math.BigDecimal;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.airtravelagents.core.util.AgentTransactionFactory;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap,ISuru
 * 
 */
public class TravelAgentFinanceBL {

	public static String AGENT_COLLECTIONDAO = "agentCollectionDAO";

	public static String RESERVATION_TNX_DAO = "ReservationTnxDAO";

	public static String AGENT_TNX_DAO = "agentTransactionDAO";

	private AgentCollectionDAO agentCollectionDAO = null;

	// private Log log = LogFactory.getLog(TravelAgentFinanceBL.class);

	/**
	 * The constructor. -
	 */
	public TravelAgentFinanceBL() {
		super();
	}

	/**
	 * 
	 * @param agentCode
	 * @param creditLimit
	 * @param availableCredit
	 * @param creditSharedAgent
	 * @param agentcollection
	 * @throws ModuleException
	 */
	@SuppressWarnings("deprecation")
	public void postAgentCollection(String agentCode, BigDecimal creditLimit, BigDecimal availableCredit,
			AgentCollection agentCollection, boolean forceCancel, String userId, Integer salesChannelCode,
			String creditSharedAgent) throws ModuleException {

		boolean creditCheck = true; // boolean creditCheck = false; changed as
		String cp = null;
		String cmCode = null;
		BigDecimal amount;
		int nominalCode = 0;

		cp = agentCollection.getCpCode();
		cmCode = agentCollection.getCmCode();
		amount = agentCollection.getPaymentAmount();

		if (agentCollectionDAO == null) {
			agentCollectionDAO = getAgentCollectionDAO();
		}

		if (creditCheck) {
			agentCollection.setStatus("P"); /* Status 'P' added as requested */
			agentCollectionDAO.addAgentCollection(agentCollection);
			agentCollectionDAO.updateAgentSummaryPost(agentCode, amount); // availableCredit

			if (cp.equals("INV")) {
				if (cmCode.equals("CSH")) {
					nominalCode = AgentTransactionNominalCode.PAY_INV_CASH.getCode();
				} else if (cmCode.equals("CHQ")) {
					nominalCode = AgentTransactionNominalCode.PAY_INV_CHQ.getCode();
				}
			} else if (cp.equals("ADV")) {
				if (cmCode.equals("CSH")) {
					nominalCode = AgentTransactionNominalCode.PAY_ADV_CASH.getCode();
				} else if (cmCode.equals("CHQ")) {
					nominalCode = AgentTransactionNominalCode.PAY_ADV_CHQ.getCode();
				}
			}
			if (amount.compareTo(BigDecimal.ZERO) != -1) { // negative amount is taken as a debit payment
				recordCredit(agentCode, amount, nominalCode, userId, salesChannelCode, "", null, creditSharedAgent);
			} else {
				recordDebit(agentCode, amount.negate(), nominalCode, userId, salesChannelCode, "", creditSharedAgent);
			}
		}
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param nominalCode
	 * @param creditSharedAgent
	 * @param force
	 */
	public void recordDebit(String agentCode, BigDecimal amount, int nominalCode, String userId, Integer salesChannelCode,
			String pnr, String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = null;
		String notes = "xx";
		int invoiced = 0;

		agentTransaction = AgentTransactionFactory.getDebitInstance(amount, nominalCode, invoiced, notes, agentCode, userId,
				salesChannelCode, pnr, null, null, null, creditSharedAgent);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param subAgentCode
	 *            TODO
	 * @param subAgentOperation
	 *            TODO
	 * @param nominalCode
	 * @param creditSharedAgent
	 * @param force
	 */
	public void recordSubAgentOperationDebit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currencyCode,
			String userId, Integer salesChannelCode, String subAgentOperation) throws ModuleException {
		AgentTransaction agentTransaction = null;
		int invoiced = 0;
		int nominalCode = AgentTransactionNominalCode.SUB_AGENT_OPERATIONS.getCode();
		agentTransaction = AgentTransactionFactory.getDebitInstance(amount, amountInLocal, nominalCode, invoiced,
				subAgentOperation, agentCode, userId, salesChannelCode, "", currencyCode, null, null, null, agentCode);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param subAgentCode
	 *            TODO
	 * @param subAgentOperation
	 *            TODO
	 * @param nominalCode
	 * @param creditSharedAgent
	 * @param force
	 */
	public void recordSubAgentOperationCredit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currencyCode,
			String userId, Integer salesChannelCode, String subAgentOperation, Character productType) throws ModuleException {
		AgentTransaction agentTransaction = null;
		int invoiced = 0;
		int nominalCode = AgentTransactionNominalCode.SUB_AGENT_OPERATIONS.getCode();
		agentTransaction = AgentTransactionFactory.getCreditInstance(amount, amountInLocal, nominalCode, invoiced,
				subAgentOperation, agentCode, userId, salesChannelCode, "", currencyCode, null, null, agentCode, productType);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
	}

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param nominalCode
	 * @param creditSharedAgent
	 */
	public void recordCredit(String agentCode, BigDecimal amount, int nominalCode, String userId, Integer salesChannelCode,
			String pnr, Character productType, String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = null;
		String notes = "xx";
		int invoiced = 1;

		agentTransaction = AgentTransactionFactory.getCreditInstance(amount, nominalCode, invoiced, notes, agentCode, userId,
				salesChannelCode, pnr, null, productType, creditSharedAgent);

		getAgentTransactionDAO().saveOrUpdate(agentTransaction);
	}

	/**
	 * Private to get AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	private AgentCollectionDAO getAgentCollectionDAO() {
		return (AgentCollectionDAO) AirTravelagentsModuleUtils.getDAO(AGENT_COLLECTIONDAO);
	}

	/**
	 * Private to get AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	private AgentTransactionDAO getAgentTransactionDAO() {
		return (AgentTransactionDAO) AirTravelagentsModuleUtils.getDAO(AGENT_TNX_DAO);
	}

}