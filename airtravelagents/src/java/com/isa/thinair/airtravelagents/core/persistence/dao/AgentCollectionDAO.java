/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * AgentCollectionDAO interface.
 * 
 * @author Chamindap
 * 
 */
public interface AgentCollectionDAO {

	/**
	 * @param agentcollection
	 */
	public void addAgentCollection(AgentCollection agentcollection);

	/**
	 * @param receiptNumber
	 * @return
	 */
	public AgentCollection getCollectionDetails(int receiptNumber);

	/**
	 * @param agentCode
	 */
	public void removeAgentCollection(int receiptNumber);

	/**
	 * 
	 * @param receiptNumber
	 */
	public void updateAgentCollection(int receiptNumber);

	/**
	 * Update AgentSummary
	 * 
	 * @param agentSummary
	 */
	public void updateAgentSummary(AgentSummary agentSummary);

	// /**
	// *
	// * @param agentCode
	// * @param amount
	// * @param cp
	// * @param isPost
	// */
	// public void updateAgentSummary(String agentCode, double amount, String cp,
	// boolean isPost);

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 */
	public void updateAgentSummarySettle(String agentCode, BigDecimal amount);

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 */
	public void updateAgentSummaryPost(String agentCode, BigDecimal amount);

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 */
	public void updateAgentSummary(String agentCode, BigDecimal amount);

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 * @param cpCode
	 */
	public void updateAgentSummary(String agentCode, BigDecimal amount, String cpCode);

	/**
	 * @param agentCode
	 * @return
	 */
	public Collection<AgentCollection> getAgentCollections(String agentCode);

	/**
	 * 
	 * @param agentCode
	 * @return
	 */
	public AgentSummary getAgentSummary(String agentCode);

	/**
	 * Method to record sales on agent summary.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @param amount
	 *            record sales amount.
	 * @param force
	 */
	public void recordDebit(String agentCode, BigDecimal amount, boolean force) throws CommonsDataAccessException;

	/**
	 * Method to record refund sales on agent summary.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @param amount
	 *            refund record sales amount.
	 */
	public void recordCredit(String agentCode, BigDecimal amount) throws CommonsDataAccessException;
}
