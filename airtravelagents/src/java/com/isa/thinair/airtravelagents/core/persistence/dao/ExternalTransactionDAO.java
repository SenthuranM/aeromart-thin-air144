/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airtravelagents.api.model.PaxExtTransaction;

/**
 * @author Thushara
 * 
 */
public interface ExternalTransactionDAO {

	/**
	 * Save or update pax external transaction data.
	 * 
	 * @param paxExtTransaction
	 *            the PaxExtTransaction
	 */
	public void saveOrUpdate(PaxExtTransaction paxExtTransaction);

	/**
	 * Remove given PaxExtTransaction.
	 * 
	 * @param paxExtTransactionId
	 */
	public void removePaxExtTransaction(int paxExtTransactionId);

	/**
	 * Get a given PaxExtTransaction.
	 * 
	 * @param paxExtTransactionId
	 *            paxExtTransactionId.
	 * @return the PaxExtTransaction
	 */
	public PaxExtTransaction getPaxExtTransaction(int paxExtTransactionId);

	/**
	 * saves the collection of external transactions
	 * 
	 * @param colExtTrans
	 */
	public void saveExtTransactions(Collection<PaxExtTransaction> colExtTrans);

}