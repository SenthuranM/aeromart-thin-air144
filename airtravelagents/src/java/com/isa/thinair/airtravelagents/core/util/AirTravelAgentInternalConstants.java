/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.core.util;

/**
 * To keep track of AirTravelAgent Internal Constants
 * 
 * @author Byorn John
 * @since 1.0
 */
public class AirTravelAgentInternalConstants {

	/** Denotes Transfer Status's to Interface Tables */
	public static interface InvoiceStatus {
		/** Denotes credit transaction type */
		public static final String INVOICE_SETTLED = "SETTLED";

		/** Denotes debit transaction type */
		public static final String INVOICE_UNSETTLED = "UNSETTLED";
	}

	/** Denotes Transfer Status's to Interface Tables */
	public static interface AgentPaymentMethods {

		public static final String ON_ACCOUNT = "OA";

		public static final String CASH = "CA";

		public static final String CREDIT_CARD = "CC";
	}

}
