package com.isa.thinair.airtravelagents.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * @author Nilindra Fernando
 * @since August 30, 2011
 */
public class AdjustAgentCreditBO {

	private Collection<CurrencyExchangeRate> currExchangeList;

	private static Log log = LogFactory.getLog(AdjustAgentCreditBO.class);

	private String loginName;

	public AdjustAgentCreditBO(Collection<CurrencyExchangeRate> currExchangeList, String loginName) {
		this.setCurrExchangeList(currExchangeList);
		this.setLoginName(loginName);
	}

	public void execute() throws ModuleException {
		if (this.getCurrExchangeList() != null && !this.getCurrExchangeList().isEmpty()) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			Iterator<CurrencyExchangeRate> itrCurrencyList = this.getCurrExchangeList().iterator();
			while (itrCurrencyList.hasNext()) {
				CurrencyExchangeRate currExRate = (CurrencyExchangeRate) itrCurrencyList.next();
				adjustForCurrAgentSet(currExRate.getCurrency().getCurrencyCode(), exchangeRateProxy);
			}
		}
	}

	/**
	 * Adjust Credit Limit of agents that uses local currency when the currency exchange rate is updated <br>
	 * author : dumindag
	 * 
	 * @param currencySpecifiedIn
	 * @param login
	 * @throws ModuleException
	 */
	private void adjustForCurrAgentSet(String currencySpecifiedIn, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		AgentDAO agentDAO = AirTravelagentsModuleUtils.getAgentDAO();
		Collection<Agent> colAgents = agentDAO.getAgentswithLocalCurrency(currencySpecifiedIn);
		Collection<AgentCreditHistory> colAgentHistory = new ArrayList<AgentCreditHistory>();
		Collection<Agent> colAgentCreditLimitToUpd = new ArrayList<Agent>();
		Collection<AgentSummary> colAgentSummaryToUpd = new ArrayList<AgentSummary>();

		for (Agent updateAgent : colAgents) {
			if (BeanUtils.nullHandler(updateAgent.getCurrencySpecifiedIn()).equals("L")) {
				processPerAgentSpecifiedInLocalCurrency(updateAgent, colAgentHistory, colAgentCreditLimitToUpd,
						colAgentSummaryToUpd, exchangeRateProxy);
			} else {
				processPerAgentSpecifiedInBaseCurrency(updateAgent, colAgentHistory, colAgentCreditLimitToUpd,
						colAgentSummaryToUpd, exchangeRateProxy);
			}
		}
		if (!colAgentHistory.isEmpty()) {
			agentDAO.addAgentCreditHistory(colAgentHistory);
		}
		if (!colAgentSummaryToUpd.isEmpty()) {
			agentDAO.saveOrUpdateAgentSummary(colAgentSummaryToUpd);
		}
		if (!colAgentCreditLimitToUpd.isEmpty()) {
			agentDAO.saveOrUpdate(colAgentCreditLimitToUpd);
		}
	}

	private void processPerAgentSpecifiedInLocalCurrency(Agent updateAgent, Collection<AgentCreditHistory> colAgentHistory,
			Collection<Agent> colAgentCreditLimitToUpd, Collection<AgentSummary> colAgentSummaryToUpd,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		AgentSummary agentSummary = updateAgent.getAgentSummary();

		BigDecimal newCreditLimitLocal = updateAgent.getCreditLimitLocal();
		BigDecimal newCreditLimitBase = BLUtil.getAmountInBase(newCreditLimitLocal, updateAgent.getCurrencyCode(),
				exchangeRateProxy);
		String remark = "";
		AgentCreditHistory agentCreditHistory = new AgentCreditHistory();

		if (newCreditLimitBase.compareTo(AccelAeroCalculator.subtract(updateAgent.getCreditLimit(),
				agentSummary.getAvailableCredit())) == 1) {
			// Update Agent Summary
			AgentSummary agentSummaryToUpdate = BLUtil.updateAgentAvailableCreditEXRateChange(updateAgent.getAgentCode(),
					updateAgent.getCreditLimit(), newCreditLimitBase, agentSummary);

			if (agentSummaryToUpdate != null) {
				colAgentSummaryToUpd.add(agentSummaryToUpdate);
			}

			// Update Agent Credit Limit
			updateAgent.setCreditLimit(newCreditLimitBase);
			updateAgent.setCreditLimitLocal(newCreditLimitLocal);
			// colAgentCreditLimitToUpd.add(updateAgent);

			// Update Agent History
			agentCreditHistory.setCreditLimitamount(newCreditLimitBase);
			remark = " Credit Updated by " + getLoginName();
		} else {
			remark = " Credit Update Failed for credit limit [" + newCreditLimitBase + "]";
			agentCreditHistory.setCreditLimitamount(updateAgent.getCreditLimit());
		}

		/**
		 * Change For AARESAA-4157 Update Bank Guarantee Cash Advance for Agents
		 */
		BigDecimal bankGuaranteeAmountLocal = updateAgent.getBankGuaranteeCashAdvanceLocal();
		BigDecimal newBankGuaranteeAmountBase = BLUtil.getAmountInBase(bankGuaranteeAmountLocal, updateAgent.getCurrencyCode(),
				exchangeRateProxy);
		updateAgent.setBankGuaranteeCashAdvance(newBankGuaranteeAmountBase);
		updateAgent.setBankGuaranteeCashAdvanceLocal(bankGuaranteeAmountLocal);
		/** END UPDATE **/

		colAgentCreditLimitToUpd.add(updateAgent);

		Calendar cal = Calendar.getInstance();

		agentCreditHistory.setAgentCode(updateAgent.getAgentCode());
		agentCreditHistory.setCreditLimitamountLocal(newCreditLimitLocal);
		agentCreditHistory.setRemarks(remark);
		agentCreditHistory.setCreatedBy(getLoginName());
		agentCreditHistory.setUpdateDate(cal.getTime());

		colAgentHistory.add(agentCreditHistory);
	}

	private void processPerAgentSpecifiedInBaseCurrency(Agent updateAgent, Collection<AgentCreditHistory> colAgentHistory,
			Collection<Agent> colAgentCreditLimitToUpd, Collection<AgentSummary> colAgentSummaryToUpd,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		BigDecimal newCreditLimitLocal = BLUtil.getAmountInLocal(updateAgent.getCreditLimit(), updateAgent.getCurrencyCode(),
				exchangeRateProxy);

		updateAgent.setCreditLimitLocal(newCreditLimitLocal);

		BigDecimal newBankGuaranteeAmountLocal = BLUtil.getAmountInLocal(updateAgent.getBankGuaranteeCashAdvance(),
				updateAgent.getCurrencyCode(), exchangeRateProxy);
		updateAgent.setBankGuaranteeCashAdvanceLocal(newBankGuaranteeAmountLocal);

		colAgentCreditLimitToUpd.add(updateAgent);

		AgentCreditHistory agentCreditHistory = new AgentCreditHistory();
		agentCreditHistory.setCreditLimitamount(updateAgent.getCreditLimit());
		agentCreditHistory.setCreditLimitamountLocal(newCreditLimitLocal);
		agentCreditHistory.setAgentCode(updateAgent.getAgentCode());
		agentCreditHistory.setRemarks(" Credit Updated by " + getLoginName());
		agentCreditHistory.setCreatedBy(getLoginName());
		agentCreditHistory.setUpdateDate(new Date());

		colAgentHistory.add(agentCreditHistory);
	}

	private Collection<CurrencyExchangeRate> getCurrExchangeList() {
		return currExchangeList;
	}

	private void setCurrExchangeList(Collection<CurrencyExchangeRate> currExchangeList) {
		this.currExchangeList = currExchangeList;
	}

	private String getLoginName() {
		return loginName;
	}

	private void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	public static int recordTemporyPaymentEntry(ReservationContactInfo reservationContactInfo, CardPaymentInfo cardPaymentInfo, TrackInfoDTO trackInfoDTO,
			boolean isCredit, boolean isOfflinePayment){

		int tempTnxId = 0;
		TemporyPaymentTO tempPayment = new TemporyPaymentTO();
		try {
			tempPayment.setAmount(cardPaymentInfo.getTotalAmount());
			tempPayment.setPaymentCurrencyAmount(cardPaymentInfo.getPayCurrencyDTO().getTotalPayCurrencyAmount());
			tempPayment.setPaymentCurrencyCode(cardPaymentInfo.getPayCurrencyDTO().getPayCurrencyCode());
			tempPayment.setFirstName(reservationContactInfo.getFirstName());
			tempPayment.setEmailAddress(reservationContactInfo.getEmail());
			tempPayment.setTelephoneNo(BeanUtils.nullHandler(reservationContactInfo.getPhoneNo()));
			tempPayment.setMobileNo(BeanUtils.nullHandler(reservationContactInfo.getMobileNo()));
			
			tempPayment.setProductType(PromotionCriteriaConstants.ProductType.AGENT_TOPUP);
			tempPayment.setCcNo(cardPaymentInfo.getNo());
			tempPayment.setOfflinePayment(isOfflinePayment);
			tempPayment.setCredit(isCredit);
			
			TempPaymentTnx tempTnx = null;

			tempTnx = AirTravelagentsModuleUtils.getReservationBD().recordTemporyPaymentEntry(tempPayment, trackInfoDTO);
			tempTnxId = tempTnx.getTnxId();

		} catch (ModuleException ex) {
			log.error("VoucherBO ==> recordTempTransactionEntryForBulkIssueXBE", ex);
		}
		return tempTnxId;
	}

}
