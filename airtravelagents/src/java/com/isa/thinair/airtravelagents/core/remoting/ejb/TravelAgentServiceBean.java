/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.airtravelagents.api.dto.AgentTokenTo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.criteria.AdminFeeAgentSearchCriteria;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.util.MasterDataPublishUtil;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.dto.AgentIncDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.SaveIncentiveDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTicketStockTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTypeTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.airtravelagents.api.model.AgentTicketStock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.core.audit.AuditAirTravelAgent;
import com.isa.thinair.airtravelagents.core.bl.AdjustAgentCreditBO;
import com.isa.thinair.airtravelagents.core.bl.TravelAgentBL;
import com.isa.thinair.airtravelagents.core.bl.TravelAgentIncentiveBL;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentIncDAO;
import com.isa.thinair.airtravelagents.core.service.bd.TravelAgentServiceDelegateImpl;
import com.isa.thinair.airtravelagents.core.service.bd.TravelAgentServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Chamindap
 */
@Stateless
@RemoteBinding(jndiBinding = "TravelAgentService.remote")
@LocalBinding(jndiBinding = "TravelAgentService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TravelAgentServiceBean extends PlatformBaseSessionBean implements TravelAgentServiceDelegateImpl,
		TravelAgentServiceLocalDelegateImpl {

	private final int LOC_TYPE_TERRITORY = 101;

	private final Log log = LogFactory.getLog(getClass());

	public Agent getAgent(String agnetId) throws ModuleException {
		Agent agent = null;

		try {
			agent = lookupAgentDAO().getAgent(agnetId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agent;
	}

	public String getAgentCountry(String agentCode) throws ModuleException {
		String agentCountryCode = null;
		try {
			agentCountryCode = AirTravelagentsModuleUtils.getAgentJdbcDAO().getAgentCountry(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentForTerritory airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agentCountryCode;
	}

	public boolean getShowPriceInSelectedCur(String agentId) throws ModuleException {
		boolean agentWSMultiCurrencySupport = false;

		try {
			agentWSMultiCurrencySupport = lookupAgentDAO().getShowPriceInSelectedCur(agentId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agentWSMultiCurrencySupport;
	}

	public Agent getAgentByIATANumber(String agentIATANumber) throws ModuleException {
		Agent agent = null;

		try {
			agent = lookupAgentDAO().getAgentByIATANumber(agentIATANumber);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agent;
	}

	/**
	 * @ejb.interface-method view-type="both"
	 * 
	 */
	public Page getAgentUsers(String agentCode, int startIndex, int pageSize) throws ModuleException {
		Page agents = null;

		try {
			agents = lookupAgentDAO().getAgentUsers(agentCode, startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentUsers airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agents;
	}

	public Agent saveAgent(Agent agent) throws ModuleException {
		try {
			// String channel = "";
			// channel = agent.getServiceChannel();
			// crate table sequences
			// TravelAgentBL.createAgentTicketingSeqeunces(agent);
			if (agent.getVersion() == -1) {
				agent.setServiceChannel(Constants.INTERNAL_CHANNEL);
				TravelAgentBL.saveAgent(agent, getUserPrincipal().getUserId());
			} else {
				TravelAgentBL.saveAgent(agent, getUserPrincipal().getUserId());
			}

			AuditAirTravelAgent.doAudit(agent, getUserPrincipal());
			return agent;
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAgent airtravelagents module failed.", cdaex);
			// Drop table sequences
			// TravelAgentBL.DropAgentTicketingSeqeunces(agent);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void saveAgent(AgentTO agentTO) throws ModuleException {
		try {
			if (PlatformUtiltiies.nullHandler(agentTO.getStationCode()).equals("")
					|| PlatformUtiltiies.nullHandler(agentTO.getReportingToGSA()).equals("")
					|| PlatformUtiltiies.nullHandler(agentTO.getAgentTypeCode()).equals("")) {
				throw new ModuleException("airtravelagent.param.nullempty");
			}

			// This is because External Clients needs only send the station we identify the territory and country
			if (AgentTO.REPORTING_TO_GSA_YES.equals(agentTO.getReportingToGSA())
					|| AgentTypeTO.GSA.equals(agentTO.getAgentTypeCode())) {
				LocationBD locationBD = AirTravelagentsModuleUtils.getLocationBD();
				Station station = locationBD.getStation(agentTO.getStationCode());
				agentTO.setTerritoryCode(station.getTerritoryCode());
			} else {
				agentTO.setTerritoryCode(null);
			}

			String agentId = "";
			String channel = "";
			Agent existAgent = null;
			Agent newAgent = null;
			String channels[] = { Constants.BOTH_CHANNELS, Constants.EXTERNAL_CHANNEL };
			agentId = agentTO.getAgentCode();

			if ((agentId != null) && (!agentId.equals(""))) {
				existAgent = lookupAgentDAO().getAgent(agentId, channels);
				channel = existAgent.getServiceChannel();
				if ((channel != null) && (!channel.equals("") && (channel.equals(Constants.EXTERNAL_CHANNEL)))) {
					newAgent = new Agent(agentTO, channel);
					TravelAgentBL.saveAgent(newAgent, getUserPrincipal().getUserId());
				} else {
					throw new ModuleException("airtravelagent.agent.update.delete.failed");
				}
			} else {
				TravelAgentBL.saveAgent(new Agent(agentTO, Constants.EXTERNAL_CHANNEL), getUserPrincipal().getUserId());
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Removes Agent
	 * 
	 * @param agent
	 * @throws ModuleException
	 */
	public void removeAgent(Agent agent) throws ModuleException {
		try {
			AuditAirTravelAgent.doAudit(agent.getAgentCode(), AuditAirTravelAgent.MANAGE_TRAVELAGENT, getUserPrincipal()
					.getUserId(), null);
			lookupAgentDAO().removeAgent(agent.getAgentCode(), agent.getVersion());
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("removeAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return if the given agentId is belongs to another carrier
	 */
	@Override
	public boolean isDryAgent(String agentId) throws ModuleException {
		try {
			return TravelAgentBL.isDryAgent(agentId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("isOwnAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void removeAgent(String agentCode, long version) throws ModuleException {
		try {
			String channels[] = { Constants.EXTERNAL_CHANNEL, Constants.BOTH_CHANNELS };
			String extChannel = "";
			Agent agent = null;
			AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.MANAGE_TRAVELAGENT, getUserPrincipal().getUserId(), null);
			agent = lookupAgentDAO().getAgent(agentCode, channels);
			extChannel = agent.getServiceChannel();
			if ((agent != null) && (extChannel.equals(Constants.EXTERNAL_CHANNEL))) {
				lookupAgentDAO().removeAgent(agentCode, version);
			} else {
				throw new CommonsDataAccessException("airtravelagent.agent.update.delete.failed");
			}
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("removeAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Page getAgents(int startIndex, int pageSize) throws ModuleException {
		Page agents = null;

		try {
			agents = lookupAgentDAO().getAgents(startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agents;
	}
	
	public Page getActiveAgents(AdminFeeAgentSearchCriteria criteria, int rownum, int pageSize) throws ModuleException {
		Page agents = null;

		try {
			agents = lookupAgentDAO().getActiveAgents(criteria, rownum, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agents;
	}
	
	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAdminFeeAgent(List agentCodesAddList, List agentCodesDeleteList) throws ModuleException {

		try {
			//AuditAirMaster.doAudit(adminFeeAgent, getUserPrincipal());
			lookupAgentDAO().saveAdminFeeAgent(agentCodesAddList, agentCodesDeleteList);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e1, e1.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	public Collection<AgentTO> getAllAgentTOs() throws ModuleException {
		try {
			return lookupAgentDAO().getAllAgentTOs();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAllAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<String> getAllAgents() throws ModuleException {
		try {
			return lookupAgentDAO().getAllAgents();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAllAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<String> getAllOwnAirlineAgents() throws ModuleException {
		try {
			return lookupAgentDAO().getAllOwnAirlineAgents();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAllAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<Agent> getAgentsForGSA(String gsaId) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getAgentsForGSA(gsaId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsForGSA airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Collection<Agent> getReportingAgentsForGSA(String gsaId) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getReportingAgentsForGSA(gsaId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsForGSA airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Collection<Agent> getReportingAgentsForTerritory(String territoryId) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getReportingAgentsForTerritory(territoryId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getReportingAgentsForTerritory airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Collection<Agent> getAgentsForStation(String code, int locType) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getAgentsForStation(code, locType); // test no impl
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsForStation airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Collection<Agent> getAgents(Collection<String> agentCodes) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getAgents(agentCodes);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Map<String, Agent> getAgentsMap(Collection<String> agentCodes) throws ModuleException {
		Map<String, Agent> agents = null;

		try {
			agents = lookupAgentDAO().getAgentsMap(agentCodes);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsMap airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	public Collection<AgentType> getAgentTypes() throws ModuleException {
		Collection<AgentType> agentTypes = null;

		try {
			agentTypes = lookupAgentDAO().getAgentTypes(new ArrayList<ModuleCriterion>(), new ArrayList<String>());
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentTypes airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agentTypes;
	}

	public Page searchAgents(List<ModuleCriterion> criterion, int startIndex, int pageSize) throws ModuleException {
		Page agents = null;

		// for service channel
		ModuleCriterion MCchannel = new ModuleCriterion();
		MCchannel.setCondition(ModuleCriterion.CONDITION_IN);
		MCchannel.setFieldName("serviceChannel");
		List<String> valueChannel = new ArrayList<String>();
		valueChannel.add(Constants.INTERNAL_CHANNEL);// channel for both AA and AH
		valueChannel.add(Constants.BOTH_CHANNELS); // channel for AA
		valueChannel.add(Constants.EXTERNAL_CHANNEL);
		MCchannel.setValue(valueChannel);
		criterion.add(MCchannel);

		try {
			List<String> orderByFieldList = new ArrayList<String>();
			orderByFieldList.add("agentCode");
			agents = lookupAgentDAO().searchAgents(criterion, startIndex, pageSize, orderByFieldList);

		} catch (CommonsDataAccessException cdaex) {
			log.error("searchAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	/**
	 * Method will return all GSA agents with agent code, name and territory id.
	 * 
	 * @return agent collection.
	 */
	public Collection<Agent> getGSAsForTerritories() throws ModuleException {
		Collection<Agent> gSAsForTerritories = null;

		try {
			gSAsForTerritories = lookupAgentDAO().getGSAsForTerritories();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getGSAsForTerritories airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return gSAsForTerritories;
	}

	/**
	 * Method will return all unassign users.
	 * 
	 * @return collection unassign users.
	 * @ejb.interface-method view-type="both"
	 */
	public Collection<User> getUnassignUsers() throws ModuleException {
		Collection<User> users = null;

		try {
			users = lookupAgentDAO().getUnassignUsers();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getUnassignUsers airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return users;
	}

	/**
	 * @param agentCode
	 * @return a collection
	 */
	public Collection<AgentCreditHistory> getCreditHistoryForAgent(String agentCode) throws ModuleException {
		Collection<AgentCreditHistory> agentsCreditHistory = null;

		try {
			agentsCreditHistory = lookupAgentDAO().getCreditHistoryForAgent(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCreditHistoryForAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agentsCreditHistory;
	}

	/**
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 * @param remarks
	 * @param forceCancel
	 * @param loginId
	 * @throws ModuleException
	 */
	public void adjustCreditLimit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit, String remarks,
			boolean forceCancel) throws ModuleException {
		try {
			String userId = this.getUserPrincipal() != null ? this.getUserPrincipal().getUserId() : "";
			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			contents.put("currentCreditLimit", String.valueOf(currentCreditLimit));
			contents.put("newCreditLimit", String.valueOf(newCreditLimit));
			AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.MANAGE_TRAVELAGENT_CREDIT, userId, contents);
			TravelAgentBL.adjustCreditLimit(agentCode, currentCreditLimit, newCreditLimit, remarks, forceCancel, userId, null);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("adjustCreditLimit airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 * @param remarks
	 * @param forceCancel
	 * @param loginId
	 * @param currencySpecifiedIn
	 * @throws ModuleException
	 */
	public void adjustCreditLimit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit, String remarks,
			boolean forceCancel, String currencySpecifiedIn) throws ModuleException {
		try {
			String userId = this.getUserPrincipal() != null ? this.getUserPrincipal().getUserId() : "";
			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			contents.put("currentCreditLimit", String.valueOf(currentCreditLimit));
			contents.put("newCreditLimit", String.valueOf(newCreditLimit));
			AuditAirTravelAgent.doAudit(agentCode, AuditAirTravelAgent.MANAGE_TRAVELAGENT_CREDIT, userId, contents);
			TravelAgentBL.adjustCreditLimit(agentCode, currentCreditLimit, newCreditLimit, remarks, forceCancel, userId,
					currencySpecifiedIn);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("adjustCreditLimit airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Method to get all active agent for given GSA Code.
	 * 
	 * @param gSACode
	 * @return the agent
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	public Collection<Agent> getActiveAgentsForGSA(String gSACode) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getActiveAgentsForGSA(gSACode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getActiveAgentsForGSA airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	/**
	 * Returns the agent type details of a particular agent type code
	 * 
	 * @param remarks
	 * @return
	 * @throws ModuleException
	 */
	public AgentType getAgentType(String agentTypeCode) throws ModuleException {

		try {
			return lookupAgentDAO().getAgentType(agentTypeCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentType airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the agent type details of a particular agent type code
	 * 
	 * @param remarks
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Agent> getAgents(String agentTypeCode) throws ModuleException {
		try {
			return lookupAgentDAO().getAgents(agentTypeCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgents airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return
	 */
	private AgentDAO lookupAgentDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();

		return (AgentDAO) lookupService.getBean("isa:base://modules/" + "airtravelagents?id=agentDAOProxy");
	}

	/**
	 * Get AgentTO for agentID
	 * 
	 * @param agnetId
	 * @return
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	public AgentTO getAgentTO(String agentId) throws ModuleException {
		try {
			String serviceChannels[] = { Constants.EXTERNAL_CHANNEL, Constants.BOTH_CHANNELS };
			Agent agent = lookupAgentDAO().getAgent(agentId, serviceChannels);

			Collection<String> colStationCodes = new ArrayList<String>();
			colStationCodes.add(agent.getStationCode());
			Map<String, String> mapStationCodeAndCountryCode = lookupAgentDAO().getAgentCountryCodes(colStationCodes);
			AgentTO agentTO = agent.toAgentTO();
			agentTO.setCountryCode((String) mapStationCodeAndCountryCode.get(agent.getStationCode()));
			return agentTO;
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentTO airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Search AgentTOs
	 * 
	 * @param criterion
	 * @param startIndex
	 * @param pageSize
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Page searchAgentTOs(List<ModuleCriterion> criterion, int startIndex, int pageSize, List<String> orderByFieldList)
			throws ModuleException {
		try {
			List<ModuleCriterion> newCriterion = new ArrayList<ModuleCriterion>();

			if (criterion != null && criterion.size() > 0) {
				for (Iterator<ModuleCriterion> iter = criterion.iterator(); iter.hasNext();) {
					ModuleCriterion moduleCriterion = (ModuleCriterion) iter.next();

					if (moduleCriterion.getFieldName() != null) {
						// Country Code is not with Agent Object. Needed to load that if needed
						if (moduleCriterion.getFieldName().equals("countryCode")) {
							Map<String, Collection<String>> mapCountryCodeAgentCodes = lookupAgentDAO().getAgentIds(
									moduleCriterion.getValue());

							ModuleCriterion newMCedit = new ModuleCriterion();
							newMCedit.setCondition(ModuleCriterion.CONDITION_IN);
							newMCedit.setFieldName("agentCode");

							List<String> colAllAgentCodes = new ArrayList<String>();
							for (Iterator iterator = mapCountryCodeAgentCodes.values().iterator(); iterator.hasNext();) {
								Collection colAgentCodes = (Collection) iterator.next();
								colAllAgentCodes.addAll(colAgentCodes);
							}
							newMCedit.setValue(colAllAgentCodes);

							if (colAllAgentCodes.size() > 0) {
								newCriterion.add(newMCedit);
							}
						} else {
							newCriterion.add(moduleCriterion);
						}
					}
				}
			}

			ModuleCriterion newMCedit = new ModuleCriterion();
			newMCedit.setCondition(ModuleCriterion.CONDITION_IN);
			newMCedit.setFieldName("serviceChannel");
			List<String> lisChannel = new ArrayList<String>();
			lisChannel.add(Constants.EXTERNAL_CHANNEL);
			lisChannel.add(Constants.BOTH_CHANNELS);
			newMCedit.setValue(lisChannel);
			newCriterion.add(newMCedit);

			Page page = lookupAgentDAO().searchAgents(newCriterion, startIndex, pageSize, orderByFieldList);

			Collection<AgentTO> colAgentTO = new ArrayList<AgentTO>();
			Collection<String> colStationCodes = new HashSet<String>();
			Agent agent;
			AgentTO agentTO;
			for (Iterator<Agent> iter = page.getPageData().iterator(); iter.hasNext();) {
				agent = (Agent) iter.next();
				colStationCodes.add(agent.getStationCode());
				colAgentTO.add(agent.toAgentTO());
			}

			Map<String, String> mapStationCodeAndCountryCode = lookupAgentDAO().getAgentCountryCodes(colStationCodes);
			for (Iterator<AgentTO> iter = colAgentTO.iterator(); iter.hasNext();) {
				agentTO = iter.next();
				agentTO.setCountryCode((String) mapStationCodeAndCountryCode.get(agentTO.getStationCode()));
			}

			page.setPageData(colAgentTO);
			return page;
		} catch (CommonsDataAccessException cdaex) {
			log.error("searchAgentTOs airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return All AgentTypesTO(s)
	 * 
	 * @param criteria
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 */
	public Collection<AgentTypeTO> getAgentTypeTOs(List<ModuleCriterion> criteria, List<String> orderByFieldList)
			throws ModuleException {
		try {
			Collection<AgentType> colAgentType = lookupAgentDAO().getAgentTypes(criteria, orderByFieldList);
			Collection<AgentTypeTO> colAgentTypeTO = new ArrayList<AgentTypeTO>();

			if (colAgentType != null && colAgentType.size() > 0) {
				for (Iterator<AgentType> iter = colAgentType.iterator(); iter.hasNext();) {
					AgentType agentType = (AgentType) iter.next();
					colAgentTypeTO.add(agentType.toAgentTypeTO());
				}
			}

			return colAgentTypeTO;
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentTypeTOs airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * @throws ModuleException
	 */
	public void blockNonPayingAgents() throws ModuleException {
		try {
			log.debug("Block Non Paying Agents Started");

			String userId = this.getUserPrincipal() != null ? this.getUserPrincipal().getUserId() : "";
			TravelAgentBL.blockNonPayingAgents(userId);

			log.debug("blackListUnsettledInvoices in TravelAgentServiceBean Exititng");
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("blackListUnsettledInvoices  module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("Error blackListUnsettledInvoices", e);
			throw new ModuleException("module.runtime.error");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateCreditLimitUpdateStatus(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException {
		try {
			TravelAgentBL.updateCreditLimitUpdateStatus(currExchangeList, status);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateCreditLimitUpdateStatus ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateCreditLimitUpdateStatus ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void adjustCreditLimitForAgents(Date date) throws ModuleException {
		TravelAgentBD travelAgentBD = AirTravelagentsModuleUtils.getTravelAgentBD();
		Collection<CurrencyExchangeRate> currExchangeList = null;

		try {
			if (AppSysParamsUtil.isAgentCreditLimitUpdationEnabled()) {
				log.info("====== adjustCreditLimitForAgents is [ENABLED], executing ======");

				String userId = this.getUserPrincipal() != null ? this.getUserPrincipal().getUserId() : "";
				currExchangeList = BLUtil.getAllUpdatedCurr(date);
				AdjustAgentCreditBO adjustAgentCreditBO = new AdjustAgentCreditBO(currExchangeList, userId);
				adjustAgentCreditBO.execute();
				travelAgentBD.updateCreditLimitUpdateStatus(currExchangeList,
						CommonsConstants.CreditLimitUpdateStatus.SUCCESS.code());
			} else {
				log.info("====== adjustCreditLimitForAgents is [DISABLED], stopping ======");
			}
		} catch (ModuleException ex) {
			log.error("ModuleException :: adjustCreditLimitLocalCurrencyAgents ", ex);
			travelAgentBD.updateCreditLimitUpdateStatus(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException :: adjustCreditLimitLocalCurrencyAgents ", cdaex);
			travelAgentBD.updateCreditLimitUpdateStatus(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<Agent> getAgentForTerritory(String territoryId) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = lookupAgentDAO().getAgentsForStation(territoryId, this.LOC_TYPE_TERRITORY); // test no impl
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentForTerritory airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agents;
	}

	public Agent getGSAForTerritory(String territoryId) throws ModuleException {
		Agent gsaAgent = null;

		try {
			gsaAgent = lookupAgentDAO().getGSAForTerritory(territoryId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentForTerritory airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return gsaAgent;
	}

	public Collection<Agent> getAgentsForCountry(String countryCode) throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = AirTravelagentsModuleUtils.getAgentJdbcDAO().getAgentsByCountry(countryCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentForTerritory airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agents;
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<AgentInfoDTO> getAgentsByLocationAndType(String country, String city, String type) throws ModuleException {
		Collection<AgentInfoDTO> agents = null;
		agents = lookupAgentDAO().getAgentsForLocationAndType(country, city, type);
		return agents;
	}

	@Override
	public void updateAgentSummarySettle(String agentCode, BigDecimal amount) throws ModuleException {

		AirTravelagentsModuleUtils.getAgentCollectionDAO().updateAgentSummarySettle(agentCode, amount);
	}

	@Override
	public HashMap<String, List<AgentInfoDTO>> getAgentsForCountries(List<String> countryCodes) throws ModuleException {
		try {
			return lookupAgentDAO().getAgentsForCountries(countryCodes);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsForCountries airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<AgentInfoDTO> getAllAgenntsForDryOperation(boolean isOnAccount) throws ModuleException {
		try {
			return TravelAgentBL.getAllAgenntsForDryOperation(isOnAccount);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAllAgenntsForDryOperation airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<AgentInfoDTO> getAllReportingAgenntsForDryOperation(String agentCode, boolean isOnAccount) throws ModuleException {
		try {
			return TravelAgentBL.getAllReportingAgenntsForDryOperation(agentCode, isOnAccount);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAllReportingAgenntsForDryOperation airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public BigDecimal getUninvocedTotal(String agentCode, Date startDate, Date endDate) throws ModuleException {

		try {
			return lookupAgentDAO().getUninvocedTotal(agentCode, startDate, endDate);
		} catch (CommonsDataAccessException commonsDataAccessException) {
			log.error("Agent DAO threw exception :" + commonsDataAccessException);
			throw new ModuleException(commonsDataAccessException, commonsDataAccessException.getExceptionCode(),
					"airtravelagent.desc");
		}
	}

	public Page getAgentIncentive(AgentIncDTO incentiveSearchDTO, int startIndex, int pageSize) throws ModuleException {
		Page agentInc = null;
		try {
			agentInc = lookupAgentIncDAO().getAgentIncentive(incentiveSearchDTO, startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentIncentive airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agentInc;
	}

	public AgentIncentive getAgentIncentive(String SchemeName) throws ModuleException {
		AgentIncentive agentInc = null;
		try {
			agentInc = lookupAgentIncDAO().getAgentIncentive(SchemeName);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentIncentive airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agentInc;
	}

	public void saveIncentive(SaveIncentiveDTO incentiveSave) throws ModuleException {
		TravelAgentIncentiveBL.saveIncentive(incentiveSave, getUserPrincipal());
	}

	public void removeIncentive(Integer incentiveID, long version) throws ModuleException {
		lookupAgentIncDAO().removeIncentive(incentiveID, version);
	}

	public Page getAgentIncentiveSlabs(Integer incentiveID, int startIndex, int pageSize) throws ModuleException {
		Page agentInc = null;
		try {
			agentInc = lookupAgentIncDAO().getAgentIncentiveSlabs(incentiveID, startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentIncentive airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return agentInc;
	}

	public Collection<AgentIncentive> getActiveAgentIncentive() throws ModuleException {
		return lookupAgentIncDAO().getActiveAgentIncentive();
	}

	public Collection<AgentIncentive> createIncentivCodes() throws ModuleException {
		return lookupAgentIncDAO().createIncentivCodes();
	}

	public Collection<AgentIncentiveSlab> createIncentivSlabs(Integer schmeId) throws ModuleException {
		return lookupAgentIncDAO().createIncentivSlabs(schmeId);
	}

	@Override
	public boolean saveAgentIncentive(Collection<Integer> lstIncenID, String agentCode) throws ModuleException {
		return TravelAgentIncentiveBL.saveAgentIncentive(lstIncenID, agentCode);
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return
	 */
	private AgentIncDAO lookupAgentIncDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();

		return (AgentIncDAO) lookupService.getBean("isa:base://modules/airtravelagents?id=agentIncDAOProxy");
	}

	/**
	 * @param agentCode
	 * @return a collection
	 */
	public Collection<AgentTicketStock> getAgentTicketStock(String agentCode) throws ModuleException {
		Collection<AgentTicketStock> agentsTicketStocks = null;
		try {
			agentsTicketStocks = lookupAgentDAO().getAgentSTock(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentStock airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agentsTicketStocks;
	}

	@Override
	public void saveAgentStock(AgentTicketStock agentTicketStock) throws ModuleException {
		try {
			Long seriesTo = 0L;
			Long seriesFrom = 0L;
			String lastTicketNumber = AirTravelagentsModuleUtils.getAgentJdbcDAO().getLastSeriesNumber(
					agentTicketStock.getAgentCode());

			if (lastTicketNumber == null || lastTicketNumber.equals("") || Long.parseLong(lastTicketNumber) == 0) {
				seriesFrom = 0L;
				seriesTo = agentTicketStock.getStockQty();
			} else {
				seriesFrom = Long.parseLong(lastTicketNumber) + 1;
				seriesTo = Long.parseLong(lastTicketNumber) + agentTicketStock.getStockQty();
			}

			agentTicketStock.setSeriesFrom(seriesFrom.toString());
			agentTicketStock.setSeriesTo(seriesTo.toString());

			// boolean isAvailable = AirTravelagentsModuleUtils.getAgentJdbcDAO().isDuplicateSeries(agentTicketStock);
			//
			// if(isAvailable){
			// throw new ModuleException("airtravelagent.duplicate.series.exsist");
			// }

			lookupAgentDAO().saveOrUpdateAgentStock(agentTicketStock);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAgentStock airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Page getAgentTicketStocks(String agentCode, int startIndex, int pageSize) throws ModuleException {
		try {
			return lookupAgentDAO().getAgentTicketStocks(agentCode, startIndex, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentUsers airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void saveAgentTicketStock(AgentTicketStockTO agentTicketStockTo) throws ModuleException {
		try {
			TravelAgentBL.saveAgenTicketStock(agentTicketStockTo);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAgent airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<AgentTicketSequnce> getAgentTicketStocks(String agentCode) throws ModuleException {
		return lookupAgentDAO().getAgentTicketStocks(agentCode);
	}

	public Map<String, String> getAgentTransationNominalCodes() throws ModuleException {
		try {
			return lookupAgentDAO().getAgentTransationNominalCodes();
		} catch (CommonsDataAccessException cdaex) {
			log.error("get Agents Nominal codes module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<String> getGSAAirports(String territoryCode) throws ModuleException {
		try {
			return lookupAgentDAO().getGSAAirports(territoryCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("get GSA airports failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void deactivateTravelAgents() throws ModuleException {
		try {
			lookupAgentDAO().deactivateTravelAgents();
		} catch (CommonsDataAccessException cdaex) {
			log.error("deactivate Travel Agents failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		}
	}

	@Override
	public String getAgentTypeByAgentId(String agentId) throws ModuleException {
		try {
			return lookupAgentDAO().getAgentTypeByAgentId(agentId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getting Travel Agent Type by agent id failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Agent> getLCCUnpublishedAgents() throws ModuleException {
		try {
			return lookupAgentDAO().getLCCUnpublishedAgents();
		} catch (CommonsDataAccessException e) {
			log.error("Getting LCC unpublished Agents is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateAgentVersion(String agentCode, long version) throws ModuleException {
		try {
			lookupAgentDAO().updateAgentVersion(agentCode, version);
		} catch (CommonsDataAccessException e) {
			log.error("Update agent version is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	@Override
	public void updateAgentLCCPublishStatus(List<Agent> originalAgents, List<String> updatedAgentCodes) throws ModuleException {
		for (Iterator<String> iterator = updatedAgentCodes.iterator(); iterator.hasNext();) {
			String agentCode = iterator.next();
			Agent agent = lookupAgentDAO().getAgent(agentCode);
			Object tmpAgent = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(agentCode, originalAgents);
			if (tmpAgent != null) {
				Agent previousAgent = (Agent) tmpAgent;
				if (agent.getVersion() == previousAgent.getVersion()) {
					lookupAgentDAO().updateAgentPublishStatus(agentCode, Util.LCC_PUBLISHED);
				}
			}

		}
	}

	@Override
	public String getCreditSharingAgentCode(String agentCode) throws ModuleException {
		try {
			Agent agent = TravelAgentBL.getCreditSharedAgent(agentCode);
			return agent.getAgentCode();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getting credit shared Travel Agent by agent code failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Agent getCreditSharingAgent(String agentCode) throws ModuleException {
		try {
			return TravelAgentBL.getCreditSharedAgent(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getting credit shared Travel Agent by agent code failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void setAgentToken(AgentToken agentToken) throws ModuleException {
		try {
			lookupAgentDAO().setAgentToken(agentToken);
		} catch (CommonsDataAccessException e) {
			log.error("Update agent token is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}
	
	@Override
	public void expireAgentToken(int tokenID) throws ModuleException {
		try {
			lookupAgentDAO().expireAgentToken(tokenID);
		} catch (CommonsDataAccessException e) {
			log.error("Update agent token is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}
	
	@Override
	public AgentToken getAgentToken(String token)throws ModuleException{
		return lookupAgentDAO().getAgentToken(token);
	}
	
	@Override
	public void removeAgentToken(int agentTokenId) throws ModuleException{
		lookupAgentDAO().removeAgentToken(agentTokenId);;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AgentToken getAgentTokenById(String token) throws ModuleException{
		return lookupAgentDAO().getAgentTokenById(token);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getCurrencyCodeByStationCode(String stationCode) throws ModuleException{
		return lookupAgentDAO().getCurrencyCodeByStationCode(stationCode);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Agent getAgentDetailsByTokenId(String token) throws ModuleException {
		Agent agent = null;
		AgentToken agentToken = lookupAgentDAO().getAgentTokenById(token);
		if (agentToken != null) {
			agent = agentToken.getAgent();
		} else {
			log.error("Invalid AgentToken");
		}
		return agent;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void purgeExpiredAgentTokens() throws ModuleException {

		AgentDAO agentDAO = lookupAgentDAO();

		List<AgentTokenTo> expiredAgentTokens = agentDAO.getExpiredAgentTokens();
		List<String> purgingTokens = new ArrayList<String>();
		List<Integer> purgingIds = new ArrayList<Integer>();

		for (AgentTokenTo agentToken : expiredAgentTokens) {
            purgingTokens.add(agentToken.getToken());
			purgingIds.add(agentToken.getTokenId());
		}
		agentDAO.removeAgentTokens(purgingIds);

		log.warn("Tokens Purged : " + purgingTokens.toString());


	}

	
	@Override
	public List<String> getSubAgentTypesByAgentCode(String agentCode) throws ModuleException {
		List<String> subAgentTypes = null;

		try {
			subAgentTypes = lookupAgentDAO().getSubAgentTypesByAgentCode(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getSubAgentTypesByAgentCode module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return subAgentTypes;
	}
	
	public List<AgentSummary> getAgentSummary(String agentCode) throws ModuleException {
		try {
			return lookupAgentDAO().getAgentSummary(agentCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentSummary failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<Agent> getReportingAgentsForGSA(String gsaId, Collection<String> selectedAgentTypes, boolean includeGsa)
			throws ModuleException {
		Collection<Agent> agents = null;

		try {
			agents = TravelAgentBL.getReportingAgentsForGSA(gsaId, selectedAgentTypes, includeGsa);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAgentsForGSA airtravelagents module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return agents;
	}

	@Override
	public List<String> getAgentWiseApplicableRoutes(String agentCode) throws ModuleException {
		return lookupAgentDAO().getAgentWiseApplicableRoutes(agentCode);
	}

	@Override
	public Collection<AgentHandlingFeeModification> getAgentActiveHandlingFeeDefinedForMOD(String agentCode)
			throws ModuleException {
		return lookupAgentDAO().getAgentActiveHandlingFeeDefinedForMOD(agentCode);

	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<String> getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents()  {
		return lookupAgentDAO().getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents();
	}
	
}
