/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airtravelagents.api.dto.AgentIncDTO;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * TravelAgentDAO interface.
 * 
 * @author dumindag
 */
public interface AgentIncDAO {

	public AgentIncentive getAgentIncentive(String agentIncName);

	public AgentIncentive getAgentIncentiveByID(Long agentIncID);

	public Collection<AgentIncentive> getAgentIncentive();

	public Collection<AgentIncentive> getAgentIncentive(Collection<Integer> incentiveIDs);

	public Collection<AgentIncentive> getActiveAgentIncentive();

	public Page getAgentIncentive(AgentIncDTO incentiveSearchDTO, int startIndex, int pageSize);

	public void saveIncentive(AgentIncentive existIncentive);

	public Page getAgentIncentiveSlabs(Integer incentiveID, int startIndex, int pageSize);

	public void removeIncentive(Integer incentiveID, long version) throws ModuleException;

	public Collection<AgentIncentive> createIncentivCodes();

	public Collection<AgentIncentiveSlab> createIncentivSlabs(Integer schemeId);

}
