package com.isa.thinair.airtravelagents.core.persistence.jdbc;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentNotificationDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.airtravelagents.core.persistence.dao.TravelAgentJdbcDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Byorn
 * 
 */
public class TravelAgentJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements TravelAgentJdbcDAO {

	private static Log log = LogFactory.getLog(TravelAgentJdbcDAOImpl.class);

	private static final String COMMA_SEPARATOR = ",";

	// Query Ids
	private static final String UNSETTLED_AGENTS = "UNSETTLED_AGENTS";

	private static final String REMOTE_REFUND_ALL_AGENTS = "remoteRefundAllAgents";

	private static final String REMOTE_REFUND_ALL_ONACCOUNT_AGENTS = "remoteRefundAllOnAccountAgents";

	private static final String REMOTE_REFUND_REPORTING_AGENTS = "remoteRefundReportingAgents";

	private static final String REMOTE_REFUND_ONACCOUNT_REPORTING_AGENTS = "remoteRefundOnAccountReportingAgents";

	private static final String LAST_TICKET_STOCK_NUMBER = "LAST_TICKET_STOCK_NUMBER";

	private static final String TRAVEL_AGENTS_BY_COUNTRY = "TRAVEL_AGENTS_BY_COUNTRY";

	private static final String TRAVEL_AGENT_COUNTRY = "TRAVEL_AGENT_COUNTRY";

	private static final String BANK_GUARANTEE_EXPIRY_PERIOD_AGENTS = "BANK_GUARANTEE_EXPIRY_AGENTS";

	private static final String CREDIT_LIMIT_UTILIZED_AGENTS = "CREDIT_LIMIT_UTILIZATION_AGENTS";

	private static final String BSP_CREDIT_LIMIT_UTILIZED_AGENTS = "BSP_CREDIT_LIMIT_UTILIZATION_AGENTS";

	private static final String NO_OF_DAYS_SINCE_LAST_CL_EMAIL = "NO_OF_DAYS_SINCE_LAST_CL_EMAIL";

	private static final String PENDING_AGENT_COMMISSION_PER_PNR = "PENDING_AGENT_COMMISSION_PER_PNR";

	private static final String OFFERD_AGENT_COMMISSION_PER_AGENT = "OFFERD_AGENT_COMMISSION_PER_AGENT";

	@SuppressWarnings("unchecked")
	public Collection<String> getAgentsWithUnsettledInvoices(Date dateFrom, Date dateTo) {
		if (log.isDebugEnabled()) {
			log.debug("Entered getAgentsWithUnsettledInvoices DAO");
		}
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String query = getQuery(UNSETTLED_AGENTS);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String dateFromStr = "to_date('" + sdf.format(dateFrom) + " 00:00:00','dd-mon-yyyy HH24:mi:ss') ";
		String dateToStr = "to_date('" + sdf.format(dateTo) + " 23:59:59','dd-mon-yyyy HH24:mi:ss') ";

		String sql1 = setStringToPlaceHolderIndex(query, 0, dateFromStr);
		String sql = setStringToPlaceHolderIndex(sql1, 1, dateToStr);

		// print the sql
		if (log.isDebugEnabled()) {
			log.debug("getAgentsWithUnsettledInvoices sql " + sql);
		}

		Collection<String> strAgentCodes = (Collection<String>) templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> strAgentCodes = new ArrayList<String>();
				while (rs.next()) {
					strAgentCodes.add(rs.getString("agent_code"));
				}
				return strAgentCodes;
			}

		});
		if (log.isDebugEnabled()) {
			log.debug("Exiting getAgentsWithUnsettledInvoices DAO");
		}
		return strAgentCodes;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AgentInfoDTO> getAllAgentsForRemoteRefund(boolean isOnAccount) {

		List<AgentInfoDTO> agents = new ArrayList<AgentInfoDTO>();

		if (log.isDebugEnabled()) {
			log.debug("Entered getAllAgentsForRemoteRefund");
		}

		// Why getting DS this way. ?
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = getQuery(isOnAccount ? REMOTE_REFUND_ALL_ONACCOUNT_AGENTS : REMOTE_REFUND_ALL_AGENTS);

		if (log.isDebugEnabled()) {
			log.debug("AREMOTE_REFUND_ALL_AGENTS sql " + sql);
		}

		agents = (List<AgentInfoDTO>) templete.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AgentInfoDTO> agentsList = new ArrayList<AgentInfoDTO>();
				while (rs.next()) {
					AgentInfoDTO agentInfoDTO = new AgentInfoDTO();
					agentInfoDTO.setAgentCode(rs.getString("AGENT_CODE"));
					agentInfoDTO.setAgentName(rs.getString("AGENT_NAME"));
					agentInfoDTO.setCurrencyCode(rs.getString("CURRENCY_CODE"));

					agentsList.add(agentInfoDTO);
				}
				return agentsList;
			}
		});

		return agents;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentInfoDTO> getReportingAgentsForRemoteRefund(String agentCode, boolean isOnAccount) {

		List<AgentInfoDTO> agents = new ArrayList<AgentInfoDTO>();

		if (log.isDebugEnabled()) {
			log.debug("Entered getAllAgentsForRemoteRefund");
		}

		// Why getting DS this way. ? getDataSource()
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = getQuery(isOnAccount ? REMOTE_REFUND_ONACCOUNT_REPORTING_AGENTS : REMOTE_REFUND_REPORTING_AGENTS);

		if (log.isDebugEnabled()) {
			log.debug("REMOTE_REFUND_REPORTING_AGENTS sql " + sql);
		}

		String[] param = { agentCode };

		agents = (List<AgentInfoDTO>) templete.query(sql, param, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AgentInfoDTO> agentsList = new ArrayList<AgentInfoDTO>();
				while (rs.next()) {
					AgentInfoDTO agentInfoDTO = new AgentInfoDTO();
					agentInfoDTO.setAgentCode(rs.getString("AGENT_CODE"));
					agentInfoDTO.setAgentName(rs.getString("AGENT_NAME"));
					agentInfoDTO.setCurrencyCode(rs.getString("CURRENCY_CODE"));

					agentsList.add(agentInfoDTO);
				}
				return agentsList;
			}
		});

		return agents;
	}

	@SuppressWarnings("unused")
	@Override
	public String getLastSeriesNumber(String baseCode) {

		if (log.isDebugEnabled()) {
			log.debug("get last ticket of the ticket series");
		}
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String query = getQuery(LAST_TICKET_STOCK_NUMBER);

		// Put the parameters
		Object[] parameters = new Object[1];
		parameters[0] = baseCode;

		// Put the data types
		int[] dataTypes = new int[] { Types.VARCHAR };

		// print the sql
		if (log.isDebugEnabled()) {
			log.debug("is duplicate ticket series sql " + query);
		}
		String last = "";
		return last = (String) templete.query(query, parameters, dataTypes, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String qtyStartFrom = null;
				if (rs != null) {
					if (rs.next()) {
						qtyStartFrom = BeanUtils.nullHandler(rs.getString(1));
					}
				}
				return qtyStartFrom;
			}
		});

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Agent> getAgentsByCountry(String countryCode) {

		List<Agent> agents = new ArrayList<Agent>();

		// Why getting DS this way. ? getDataSource()
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String[] param = { AppSysParamsUtil.getDefaultAirlineIdentifierCode(), countryCode };

		agents = (List<Agent>) template.query(getQuery(TRAVEL_AGENTS_BY_COUNTRY), param, new RowMapper() {

			@Override
			public Agent mapRow(ResultSet rs, int rowNum) throws SQLException {
				Agent agent = new Agent();
				agent.setAccountCode(rs.getString("account_code"));
				agent.setAddressCity(rs.getString("address_city"));
				agent.setAddressLine1(rs.getString("address_line_1"));
				agent.setAddressLine2(rs.getString("address_line_2"));
				agent.setAddressStateProvince(rs.getString("address_state_province"));
				agent.setAgentCode(rs.getString("agent_code"));
				agent.setAgentCommissionCode(rs.getString("agent_commission_code"));
				agent.setAgentIATANumber(rs.getString("agent_iata_number"));
				agent.setAgentName(rs.getString("agent_name"));
				agent.setAgentTypeCode(rs.getString("agent_type_code"));
				agent.setAirlineCode(rs.getString("airline_code"));
				agent.setAutoInvoice(rs.getString("auto_invoice"));
				agent.setBankGuaranteeCashAdvance(rs.getBigDecimal("bank_guarantee_cash_advance"));
				agent.setBankGuaranteeCashAdvanceLocal(rs.getBigDecimal("bank_guarantee_cash_adv_local"));
				agent.setBillingEmail(rs.getString("billing_email"));
				agent.setCapturePaymentRef(rs.getString("capture_pay_ref"));
				agent.setContactPersonName(rs.getString("contact_person_name"));
				agent.setCreatedBy(rs.getString("created_by"));
				agent.setCreatedDate(rs.getDate("created_date"));
				agent.setCreditLimit(rs.getBigDecimal("credit_limit"));
				agent.setTotalCreditLimit(rs.getBigDecimal("total_credit_limit"));
				agent.setCreditLimitLocal(rs.getBigDecimal("credit_limit_local"));
				agent.setCurrencyCode(rs.getString("currency_code"));
				agent.setCurrencySpecifiedIn(rs.getString("specified_in"));
				agent.setDesignation(rs.getString("contact_person_designation"));
				agent.setEmailId(rs.getString("email_id"));
				agent.setEnableWSMultiCurrency(rs.getString("show_price_in_selected_cur"));
				agent.setFax(rs.getString("fax"));
				agent.setHandlingChargAmount(rs.getBigDecimal("handling_charge_amount"));
				agent.setLanguageCode(rs.getString("language_code"));
				agent.setLicenceNumber(rs.getString("licence_no"));
				agent.setLocationUrl(rs.getString("location_url"));
				agent.setMobile(rs.getString("mobile"));
				agent.setModifiedBy(rs.getString("modified_by"));
				agent.setModifiedDate(rs.getDate("modified_date"));
				agent.setNotes(rs.getString("notes"));
				agent.setOnHold(rs.getString("on_hold"));
				agent.setPayRefMandatory(rs.getString("pay_ref_mandatory"));
				agent.setPostalCode(rs.getString("postal_code"));
				agent.setPrefferedRptFormat(rs.getString("pref_rpt_format"));
				agent.setPublishToWeb(rs.getString("website_visibility"));
				agent.setReportingToGSA(rs.getString("report_to_gsa"));
				agent.setGsaCode(rs.getString("gsa_code"));
				agent.setServiceChannel(rs.getString("service_channel_code"));
				agent.setStationCode(rs.getString("station_code"));
				agent.setStatus(rs.getString("status"));
				agent.setTelephone(rs.getString("telephone"));
				agent.setTerritoryCode(rs.getString("territory_code"));
				agent.setGsaCode(rs.getString("gsa_code"));
				agent.setVersion(rs.getLong("version"));
				agent.setEmailsToNotify(rs.getString("emails_to_notify"));

				return agent;
			}
		});

		return agents;
	}

	@SuppressWarnings("unused")
	@Override
	public String getAgentCountry(String agentCode) {

		if (log.isDebugEnabled()) {
			log.debug("getAgentCountry(agentCode)");
		}
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String query = getQuery(TRAVEL_AGENT_COUNTRY);

		// Put the parameters
		Object[] parameters = new Object[1];
		parameters[0] = agentCode;

		// Put the data types
		int[] dataTypes = new int[] { Types.VARCHAR };

		// print the sql
		if (log.isDebugEnabled()) {
			log.debug("getAgentCountry sql " + query);
		}
		String agentCountryCode = null;
		return agentCountryCode = (String) templete.query(query, parameters, dataTypes, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String agentCountryCode = null;
				if (rs != null) {
					if (rs.next()) {
						agentCountryCode = BeanUtils.nullHandler(rs.getString("country_code"));
					}
				}
				return agentCountryCode;
			}
		});

	}

	/**
	 * Get the agents who falls in the notification period for bank guarantee expire date
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AgentNotificationDTO> getAgentsWithinExpiryNotificationPeriod(List<String> notifyIntervals)
			throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(InvoicingModuleUtils.getDatasource());
		String query = getQuery(BANK_GUARANTEE_EXPIRY_PERIOD_AGENTS);
		List<AgentNotificationDTO> agentList = new ArrayList<AgentNotificationDTO>();

		String strWhere = null;

		String deliiteredString = BLUtil.getDelimiteredStringFromList(notifyIntervals, COMMA_SEPARATOR);

		if (deliiteredString.length() > 0) {
			strWhere = "(" + deliiteredString + ")";
			query = query.replace("#WHERE", strWhere);
		}

		if (log.isDebugEnabled()) {
			log.debug(query);
		}

		String[] param = {};

		agentList = (List<AgentNotificationDTO>) jdbcTemplate.query(query, param, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AgentNotificationDTO> agentsList = new ArrayList<AgentNotificationDTO>();
				while (rs.next()) {
					AgentNotificationDTO agentInfoDTO = new AgentNotificationDTO();

					agentInfoDTO.setAgentCode(rs.getString("AGENT_CODE"));
					agentInfoDTO.setAgentName(rs.getString("AGENT_NAME"));
					agentInfoDTO.setBillingEmail(rs.getString("BILLING_EMAIL"));
					agentInfoDTO.setNoOfDaysToExpire(rs.getInt("NO_OF_DAYS_TO_EXPIRE"));

					agentsList.add(agentInfoDTO);
				}
				return agentsList;
			}
		});

		return agentList;
	}

	/**
	 * Get the set of agents who has reached given credit limits
	 */
	@Override
	public Set<AgentNotificationDTO> getAgentsWithCreditLimitNotification(List<Pair<Double, Double>> valuePiars,
			final Integer notifyIntervals, String paymentMethod) throws ModuleException {

		Set<AgentNotificationDTO> notificationDTOs = new HashSet<AgentNotificationDTO>();
		int pairLevel = 0;
		for (Pair<Double, Double> pair : valuePiars) {
			notificationDTOs.addAll(getAgentsWithCreditLimitNotification(pair, notifyIntervals, paymentMethod, ++pairLevel));
		}

		return notificationDTOs;
	}

	/**
	 * Get an agent list of one pair of values
	 * 
	 * @param pair
	 * @param paymentMethod
	 * @param pairLevel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Set<AgentNotificationDTO> getAgentsWithCreditLimitNotification(final Pair<Double, Double> pair,
			final Integer notifyIntervals, String paymentMethod, int pairLevel) throws ModuleException {
		Set<AgentNotificationDTO> agentSet = new HashSet<AgentNotificationDTO>();

		if (pair != null) {

			final String paymentCode = paymentMethod;
			final int alertLevel = pairLevel;

			String baseQuery = CREDIT_LIMIT_UTILIZED_AGENTS;
			if (Agent.PAYMENT_MODE_ONACCOUNT.equals(paymentMethod)) {
				baseQuery = CREDIT_LIMIT_UTILIZED_AGENTS;
			} else if (Agent.PAYMENT_MODE_BSP.equals(paymentMethod)) {
				baseQuery = BSP_CREDIT_LIMIT_UTILIZED_AGENTS;
			}

			JdbcTemplate jdbcTemplate = new JdbcTemplate(InvoicingModuleUtils.getDatasource());
			String query = getQuery(baseQuery);
			String strWhere = null;

			if (pair != null && pair.getLeft() != null && pair.getRight() != null) {
				strWhere = " AND b.utilized_credit BETWEEN (b.credit_limit *" + pair.getLeft() + ") and (b.credit_limit * "
						+ pair.getRight() + ")";
			} else if (pair != null && pair.getLeft() != null) {
				strWhere = " AND b.utilized_credit >= (b.credit_limit * " + pair.getLeft() + ")";
			}

			if (strWhere != null) {
				query = query.replace("#WHERE", strWhere);

				if (log.isDebugEnabled()) {
					log.debug(query);
				}

				String[] param = {};

				// IF THERE IS NO WHERE CLAUSE, DO NOT EXECUTE, BECAUSE MAIL WILL GO TO ALL THE AGENTS :D
				agentSet = (Set<AgentNotificationDTO>) jdbcTemplate.query(query, param, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Set<AgentNotificationDTO> agentSet = new HashSet<AgentNotificationDTO>();
						Integer days = null;
						while (rs.next()) {
							AgentNotificationDTO agentInfoDTO = new AgentNotificationDTO();
							agentInfoDTO.setAgentCode(rs.getString("AGENT_CODE"));
							agentInfoDTO.setAgentName(rs.getString("AGENT_NAME"));
							agentInfoDTO.setAgentIATACode(PlatformUtiltiies.nullHandler(rs.getString("AGENT_IATA_NUMBER")));
							agentInfoDTO.setBillingEmail(rs.getString("BILLING_EMAIL"));
							agentInfoDTO.setUtilizationPercentage(rs.getDouble("UTILIZATION"));
							agentInfoDTO.setTotalCreditLimit(AccelAeroCalculator.formatAsDecimal(rs.getBigDecimal("CREDIT_LIMIT")));
							agentInfoDTO.setAvailableCreditLimit(AccelAeroCalculator.formatAsDecimal(rs
									.getBigDecimal("AVILABLE_CREDIT")));
							agentInfoDTO.setCheckedPercentage(pair.getLeft());
							agentInfoDTO.setNotificationLevel(alertLevel);


							days = getNumberOfDaysSinceLastReminder(agentInfoDTO.getAgentCode(),
									agentInfoDTO.getCheckedPercentage(), paymentCode);

							if (days != null && days > notifyIntervals) {
								agentSet.add(agentInfoDTO);
							} else if (days == null) {
								// This means no email sent at all
								agentSet.add(agentInfoDTO);
							}
						}
						return agentSet;
					}
				});
			}

		}
		return agentSet;
	}

	/**
	 * check if there was a reminder mail sent for the same thing earlier, and if sent, since how many days
	 * 
	 * @param agentCode
	 * @param percentage
	 * @param paymentCode
	 * @return
	 */
	private Integer getNumberOfDaysSinceLastReminder(String agentCode, Double percentage, String paymentCode) {
		Integer noOfDays = null;

		JdbcTemplate jdbcTemplate = new JdbcTemplate(InvoicingModuleUtils.getDatasource());
		String query = getQuery(NO_OF_DAYS_SINCE_LAST_CL_EMAIL);

		if (log.isDebugEnabled()) {
			log.debug(query);
		}

		// Put the parameters
		Object[] parameters = new Object[3];
		parameters[0] = agentCode;
		parameters[1] = percentage;
		parameters[2] = paymentCode;

		// Put the data types
		int[] dataTypes = new int[] { Types.VARCHAR, Types.DOUBLE, Types.VARCHAR };

		noOfDays = (Integer) jdbcTemplate.query(query, parameters, dataTypes, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer days = null;
				if (rs != null) {
					if (rs.next()) {
						days = rs.getInt("NO_OF_DAYS");
					}
				}
				return days;
			}
		});

		return noOfDays;
	}

	@Override
	public BigDecimal getPendingAgentCommissions(String agentCode, String pnr) throws ModuleException {
		BigDecimal pendingCommission = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AirTravelagentsModuleUtils.getDatasource());
		String sql = getQuery(PENDING_AGENT_COMMISSION_PER_PNR);
		String[] parameters = { pnr, agentCode };

		pendingCommission = (BigDecimal) jdbcTemplate.query(sql, parameters, new ResultSetExtractor() {
			BigDecimal amount = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					if (rs.next()) {
						amount = rs.getBigDecimal("amount");
					}
				}
				return amount;
			}
		});

		return pendingCommission;
	}

	@Override
	public BigDecimal getTotalOfferedAgentCommissions(String agentCode, Date periodStartDate, Date periodEndDate)
			throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AirTravelagentsModuleUtils.getDatasource());

		String sql = getQuery(OFFERD_AGENT_COMMISSION_PER_AGENT);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String dateFromStr = "to_date('" + sdf.format(periodStartDate) + " 00:00:00','dd-mon-yyyy HH24:mi:ss') ";
		String dateToStr = "to_date('" + sdf.format(periodEndDate) + " 23:59:59','dd-mon-yyyy HH24:mi:ss') ";

		sql = setStringToPlaceHolderIndex(sql, 0, "'" + agentCode + "'");
		sql = setStringToPlaceHolderIndex(sql, 1, dateFromStr);
		sql = setStringToPlaceHolderIndex(sql, 2, dateToStr);

		BigDecimal offeredCommissions = (BigDecimal) jdbcTemplate.query(sql, new ResultSetExtractor() {
			BigDecimal totalCommission = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					if (rs.next()) {
						totalCommission = rs.getBigDecimal("total_commission");
					}
				}
				return totalCommission;
			}
		});

		return (offeredCommissions == null) ? AccelAeroCalculator.getDefaultBigDecimalZero() : offeredCommissions;
	}

}
