/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airtravelagents.api.dto.AgentIncDTO;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentIncDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author dumindag
 * @isa.module.dao-impl dao-name="agentIncDAO"
 * 
 */
public class AgentIncDAOImpl extends PlatformHibernateDaoSupport implements AgentIncDAO {

	private static Log log = LogFactory.getLog(AgentDAOImpl.class);

	public AgentIncDAOImpl() {
		super();
	}

	@SuppressWarnings("unchecked")
	public AgentIncentive getAgentIncentive(String agentIncName) {
		AgentIncentive agentIncentive = null;

		List<AgentIncentive> agentList = find(
				"select agentIncentive from AgentIncentive as agentIncentive" + " where agentIncentive.schemeName = ?",
				agentIncName, AgentIncentive.class);

		if (agentList.size() != 0) {
			agentIncentive = (AgentIncentive) agentList.get(0);
		}
		return agentIncentive;
	}

	public Collection<AgentIncentive> getAgentIncentive() {
		return find("from AgentIncentive", AgentIncentive.class);

	}

	@SuppressWarnings("unchecked")
	public Collection<AgentIncentive> getAgentIncentive(Collection<Integer> incentiveIDs) {
		String hql = "select incentive from AgentIncentive incentive where incentive.agentIncSchemeID in (:list)";
		return getSession().createQuery(hql).setParameterList("list", incentiveIDs).list();

	}

	public Collection<AgentIncentive> getActiveAgentIncentive() {
		return find(
				"select agentIncentive from AgentIncentive as agentIncentive " + " where agentIncentive.status='"
						+ AgentIncentive.Status.ACTIVE + "'", AgentIncentive.class);

	}

	@SuppressWarnings("rawtypes")
	public Page getAgentIncentive(AgentIncDTO incentiveSearchDTO, int startIndex, int pageSize) {
		//Collection agentUsersCollection = null;
		Page page = null;
		//int recSize;
		//Query query = null;

		String hql = "select incentive from AgentIncentive as incentive ";
		String hqlWhere = "";
		String mainHql = "";
		String hqlCount = " select count(*) from AgentIncentive as incentive";

		try {
			if (incentiveSearchDTO != null) {
				if (incentiveSearchDTO.getSchemeName() != null) {

					if (hqlWhere.equals("")) {
						hqlWhere += " where incentive.agentIncSchemeID like :incenCode";
					}

				}
				if (incentiveSearchDTO.getStatus() != null) {
					if (hqlWhere.equals("")) {
						hqlWhere += " where incentive.status like :status";
					} else {
						hqlWhere += " and incentive.status like :status";
					}
				}
				if (incentiveSearchDTO.getStrEffectiveFrom() != null && (incentiveSearchDTO.getStrEffectiveTo() != null)) {
					if (hqlWhere.equals("")) {
						hqlWhere += " where incentive.effectiveFrom between :effecFrom1 and :effecTo1 "
								+ " and incentive.effectiveTo between :effecFrom2 and :effecTo2 ";
					} else {
						hqlWhere += " and incentive.effectiveFrom between  :effecFrom1 and  :effecTo1 "
								+ " and incentive.effectiveTo between :effecFrom2 and :effecTo2 ";
					}
				}
			}

			mainHql = hql + hqlWhere;
			hqlCount += hqlWhere;
			Query mainQuery = getSession().createQuery(mainHql).setFirstResult(startIndex).setMaxResults(pageSize);
			Query countQuery = getSession().createQuery(hqlCount);

			if (incentiveSearchDTO != null) {
				if (incentiveSearchDTO.getSchemeName() != null && !incentiveSearchDTO.getSchemeName().equals("")) {
					mainQuery.setParameter("incenCode", incentiveSearchDTO.getSchemeName());
					countQuery.setParameter("incenCode", incentiveSearchDTO.getSchemeName());
				} else if (incentiveSearchDTO.getSchemeName() != null && !incentiveSearchDTO.getSchemeName().equals("")) {
					mainQuery.setParameter("incenCode", "%");
					countQuery.setParameter("incenCode", "%");
				}
				if (incentiveSearchDTO.getStatus() != null && !incentiveSearchDTO.getStatus().equals("")) {
					mainQuery.setParameter("status", incentiveSearchDTO.getStatus());
					countQuery.setParameter("status", incentiveSearchDTO.getStatus());
				} else if (incentiveSearchDTO.getStatus() != null && incentiveSearchDTO.getStatus().equals("")) {
					mainQuery.setParameter("status", "%");
					countQuery.setParameter("status", "%");
				}

				if (incentiveSearchDTO.getStrEffectiveFrom() != null && incentiveSearchDTO.getStrEffectiveTo() != null
						&& !incentiveSearchDTO.getStrEffectiveFrom().equals("")
						&& !incentiveSearchDTO.getStrEffectiveTo().equals("")) {
					mainQuery.setParameter("effecFrom1", incentiveSearchDTO.getStrToDateEffectiveFrom());
					mainQuery.setParameter("effecTo1", incentiveSearchDTO.getStrToDateEffectiveTo());
					mainQuery.setParameter("effecFrom2", incentiveSearchDTO.getStrToDateEffectiveFrom());
					mainQuery.setParameter("effecTo2", incentiveSearchDTO.getStrToDateEffectiveTo());
					countQuery.setParameter("effecFrom1", incentiveSearchDTO.getStrToDateEffectiveFrom());
					countQuery.setParameter("effecTo1", incentiveSearchDTO.getStrToDateEffectiveTo());
					countQuery.setParameter("effecFrom2", incentiveSearchDTO.getStrToDateEffectiveFrom());
					countQuery.setParameter("effecTo2", incentiveSearchDTO.getStrToDateEffectiveTo());
				}
			}
			List listIncentive = mainQuery.list();
			int count = listIncentive.size();// ((Integer) countQuery.uniqueResult()).intValue();

			// recSize = query.list().size();
			// agentUsersCollection = query.setFirstResult(startIndex).setMaxResults(pageSize).list();
			page = new Page(count, startIndex, startIndex + pageSize, listIncentive);

		} catch (DataAccessResourceFailureException e) {
			log.error("ERROR in getAgentIncentive() ", e);
		} catch (HibernateException e) {
			log.error("ERROR in getAgentIncentive() ", e);
		} catch (IllegalStateException e) {
			log.error("ERROR in getAgentIncentive() ", e);
		}
		return page;
	}

	public void saveIncentive(AgentIncentive incentiveSave) {
		hibernateSaveOrUpdate(incentiveSave);
	}

	@SuppressWarnings("unchecked")
	public Page getAgentIncentiveSlabs(Integer incetiveID, int startIndex, int pageSize) {
		Page page = null;

		String hql = "select incSlab from AgentIncentiveSlab as incSlab where incSlab.agentIncSchemeID = ?";

		List<AgentIncentiveSlab> slabs = find(hql, incetiveID, AgentIncentiveSlab.class);
		page = new Page(slabs.size(), startIndex, pageSize, slabs);
		return page;
	}

	public void removeIncentive(Integer incentiveID, long version) throws ModuleException {

		AgentIncentive incentiveOri = (AgentIncentive) load(AgentIncentive.class, incentiveID);

		long versionOri = incentiveOri.getVersion();
		if (version == versionOri) {
			delete(incentiveOri);
		} else {
			throw new ModuleException("Data has changed by another User.");
		}
	}

	public Collection<AgentIncentive> createIncentivCodes() {
		Collection <AgentIncentive>schemeMap;
		String sql = "from AgentIncentive as incen where incen.status='ACT'";
		schemeMap = find(sql, AgentIncentive.class);

		// StringBuffer sb = new StringBuffer();
		// sb.append(" SELECT distinct  t.AGENT_INCENTIVE_SCHEME_ID, t.SCHEME_NAME ,");
		// sb.append(" t.effective_from_date, t.effective_to_date ");
		// sb.append(" from T_AGENT_INCENTIVE_SCHEME t ");
		// sb.append(" where t.STATUS = 'ACT' order by t.AGENT_INCENTIVE_SCHEME_ID ");
		//
		// JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		// sql = sb.toString();
		// schemeMap = (Collection) jdbcTemplate.query(sql, new ResultSetExtractor(){
		//
		// @Override
		// public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		// Collection colScheme = new ArrayList();
		// Integer schemeID = null;
		//
		// if(rs != null){
		// while(rs.next()){
		// AgentIncentive scheme = new AgentIncentive();
		// if(rs.getObject("AGENT_INCENTIVE_SCHEME_ID") != null){
		// schemeID = new Integer(rs.getObject("AGENT_INCENTIVE_SCHEME_ID").toString());
		// }
		// scheme.setAgentIncSchemeID(schemeID);
		// scheme.setSchemeName(PlatformUtiltiies.nullHandler(rs.getObject("SCHEME_NAME")));
		// scheme.setEffectiveFrom(rs.getDate("EFFECTIVE_FROM_DATE"));
		// scheme.setEffectiveTo(rs.getDate("EFFECTIVE_TO_DATE"));
		// colScheme.add(scheme);
		// }
		// return colScheme;
		// }else{
		// return null;
		// }
		//
		// }
		//
		// });
		return schemeMap;
	}

	@SuppressWarnings("unchecked")
	public Collection<AgentIncentiveSlab> createIncentivSlabs(Integer schemeID) {
		Collection<AgentIncentiveSlab> colSlab;
		String sql = " SELECT   slb.range_start_value , slb.range_end_value , slb.percentage ,  slb.agent_inc_scheme_slab_id "
				+ " FROM t_agent_incentive_scheme_slab slb " + " WHERE slb.agent_incentive_scheme_id = '" + schemeID
				+ "' ORDER BY slb.agent_inc_scheme_slab_id desc ";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		colSlab = (Collection<AgentIncentiveSlab>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<AgentIncentiveSlab> scheme = new ArrayList<AgentIncentiveSlab>();
				if (rs != null) {
					while (rs.next()) {
						AgentIncentiveSlab slab = new AgentIncentiveSlab();
						slab.setAgentIncSlabID(new Integer(rs.getObject("AGENT_INC_SCHEME_SLAB_ID").toString()));
						slab.setRateStartValue(new Integer(rs.getInt("RANGE_START_VALUE")));
						slab.setRateEndValue(new Integer(rs.getInt("RANGE_END_VALUE")));
						slab.setPersentage(rs.getDouble("PERCENTAGE"));
						scheme.add(slab);
					}
					return scheme;
				} else {
					return null;
				}

			}

		});
		return colSlab;
	}

	/**
	 * Returns air travel agent module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	@Override
	public AgentIncentive getAgentIncentiveByID(Long agentIncID) {
		AgentIncentive agentIncentive = null;

		List<AgentIncentive> agentList = find(
				"select agentIncentive from AgentIncentive as agentIncentive" + " where agentIncentive.agentIncSchemeID = ?",
				agentIncID.intValue(), AgentIncentive.class);

		if (agentList.size() != 0) {
			agentIncentive = (AgentIncentive) agentList.get(0);
		}
		return agentIncentive;
	}
}