/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentNotificationDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author Byorn
 * 
 */
public interface TravelAgentJdbcDAO {

	public Collection<String> getAgentsWithUnsettledInvoices(Date dateFrom, Date dateTo);

	public List<AgentInfoDTO> getAllAgentsForRemoteRefund(boolean isOnAccount);

	public List<AgentInfoDTO> getReportingAgentsForRemoteRefund(String agentCode, boolean isOnAccount);

	public String getLastSeriesNumber(String agtCode);

	public List<Agent> getAgentsByCountry(String countryCode);

	public String getAgentCountry(String agentCode);

	public List<AgentNotificationDTO> getAgentsWithinExpiryNotificationPeriod(List<String> notifyIntervals)
			throws ModuleException;

	public Set<AgentNotificationDTO> getAgentsWithCreditLimitNotification(List<Pair<Double, Double>> valuePiars,
			Integer notifyIntervals, String paymentMethod) throws ModuleException;

	BigDecimal getPendingAgentCommissions(String agentCode, String pnr) throws ModuleException;

	BigDecimal getTotalOfferedAgentCommissions(String agentCode, Date periodStartDate, Date periodEndDate) throws ModuleException;

}
