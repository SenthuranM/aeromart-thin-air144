/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.core.util;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * factory to get credits and debits
 * 
 * @author Chamindap
 */
public abstract class AgentTransactionFactory {

	private final static Log log = LogFactory.getLog(AgentTransactionFactory.class);

	public static AgentTransaction getCreditInstance(BigDecimal amount, BigDecimal amountInLocal, int nominalCode, int invoiced,
			String notes, String agentCode, String userId, Integer salesChannelCode, String pnr, String currencyCode,
			String carrierCode, String receiptNumber, String creditSharedAgent, Character productType) throws ModuleException {
		AgentTransaction agentTransaction = new AgentTransaction();

		if (amount != null && (amount.compareTo(BigDecimal.ZERO) < 0)
				&& nominalCode != AgentTransactionNominalCode.BSP_SETTLEMENT.getCode()) {
			throw new ModuleException("airtravelagent.param.null");
		}

		agentTransaction.setAmount(amount.negate());
		agentTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.CREDIT);
		agentTransaction.setNominalCode(nominalCode);
		agentTransaction.setTransctionDate(new GregorianCalendar().getTime());
		agentTransaction.setInvoiced(invoiced);
		agentTransaction.setNotes(notes);
		agentTransaction.setAgentCode(agentCode);
		agentTransaction.setUserId(userId);
		agentTransaction.setSalesChannelCode(salesChannelCode);
		agentTransaction.setPnr(pnr);
		agentTransaction.setCurrencyCode(currencyCode);
		if (amountInLocal != null) {
			agentTransaction.setAmountLocal(amountInLocal.negate());
		}
		agentTransaction.setCreditSharedAgent(creditSharedAgent);
		agentTransaction.setAdjustmentCarrierCode(carrierCode);
		agentTransaction.setReceiptNumber(receiptNumber);
		if(productType != null){
			agentTransaction.setProductType(productType);
		}
		return agentTransaction;
	}

	public static AgentTransaction getCreditInstance(BigDecimal amount, int nominalCode, int invoiced, String notes,
			String agentCode, String userId, Integer salesChannelCode, String pnr, PayCurrencyDTO payCurrencyDTO,
			Character productType, String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = new AgentTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("airtravelagent.param.null");
		}

		agentTransaction.setAmount(amount.negate());
		agentTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.CREDIT);
		agentTransaction.setNominalCode(nominalCode);
		agentTransaction.setTransctionDate(new GregorianCalendar().getTime());
		agentTransaction.setInvoiced(invoiced);
		agentTransaction.setNotes(notes);
		agentTransaction.setAgentCode(agentCode);
		agentTransaction.setUserId(userId);
		agentTransaction.setSalesChannelCode(salesChannelCode);
		agentTransaction.setPnr(pnr);
		agentTransaction.setCreditSharedAgent(creditSharedAgent);
		
		if(productType != null){
			agentTransaction.setProductType(productType);
		}

		if (payCurrencyDTO != null) {
			agentTransaction.setCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
			agentTransaction.setAmountLocal(payCurrencyDTO.getTotalPayCurrencyAmount().negate());
		}

		return agentTransaction;
	}

	public static AgentTransaction getCreditInstance(BigDecimal amount, int nominalCode, int invoiced, String notes,
			String agentCode, String userId, Integer salesChannelCode, String pnr, String currencyCode, BigDecimal amountInLocal,
			String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = new AgentTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("airtravelagent.param.null");
		}

		agentTransaction.setAmount(amount.negate());
		agentTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.CREDIT);
		agentTransaction.setNominalCode(nominalCode);
		agentTransaction.setTransctionDate(new GregorianCalendar().getTime());
		agentTransaction.setInvoiced(invoiced);
		agentTransaction.setNotes(notes);
		agentTransaction.setAgentCode(agentCode);
		agentTransaction.setUserId(userId);
		agentTransaction.setSalesChannelCode(salesChannelCode);
		agentTransaction.setPnr(pnr);
		agentTransaction.setCurrencyCode(currencyCode);
		agentTransaction.setAmountLocal(amountInLocal.negate());

		return agentTransaction;
	}

	public static AgentTransaction getDebitInstance(BigDecimal amount, int nominalCode, int invoiced, String notes,
			String agentCode, String userId, Integer salesChannelCode, String pnr, PayCurrencyDTO payCurrencyDTO,
			Character productType, String externalRef, String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = new AgentTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			log.error("Amount is :" + amount + " for reservation : " + pnr);
			throw new ModuleException("airtravelagent.param.null");
		}

		agentTransaction.setAmount(amount);
		agentTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.DEBIT);
		agentTransaction.setNominalCode(nominalCode);
		agentTransaction.setTransctionDate(new GregorianCalendar().getTime());
		agentTransaction.setInvoiced(invoiced);
		agentTransaction.setNotes(notes);
		agentTransaction.setAgentCode(agentCode);
		agentTransaction.setUserId(userId);
		agentTransaction.setSalesChannelCode(salesChannelCode);
		agentTransaction.setPnr(pnr);
		agentTransaction.setCreditSharedAgent(creditSharedAgent);

		agentTransaction.setExternalRef(externalRef);
		if (productType != null) {
			agentTransaction.setProductType(productType);
		}
		if (payCurrencyDTO != null) {
			agentTransaction.setCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
			agentTransaction.setAmountLocal(payCurrencyDTO.getTotalPayCurrencyAmount());
		}

		return agentTransaction;
	}

	public static AgentTransaction getDebitInstance(BigDecimal amount, BigDecimal amountLocal, int nominalCode, int invoiced,
			String notes, String agentCode, String userId, Integer salesChannelCode, String pnr, String currencyCode,
			String carrierCode, String receiptNumber, String externalRef, String creditSharedAgent) throws ModuleException {
		AgentTransaction agentTransaction = new AgentTransaction();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("airtravelagent.param.null");
		}

		agentTransaction.setAmount(amount);
		agentTransaction.setDrOrcr(AirTravelAgentConstants.TnxTypes.DEBIT);
		agentTransaction.setNominalCode(nominalCode);
		agentTransaction.setTransctionDate(new GregorianCalendar().getTime());
		agentTransaction.setInvoiced(invoiced);
		agentTransaction.setNotes(notes);
		agentTransaction.setAgentCode(agentCode);
		agentTransaction.setUserId(userId);
		agentTransaction.setSalesChannelCode(salesChannelCode);
		agentTransaction.setPnr(pnr);
		agentTransaction.setCurrencyCode(currencyCode);
		agentTransaction.setAmountLocal(amountLocal);
		agentTransaction.setAdjustmentCarrierCode(carrierCode);
		agentTransaction.setReceiptNumber(receiptNumber);
		agentTransaction.setCreditSharedAgent(creditSharedAgent);
		agentTransaction.setExternalRef(externalRef);

		return agentTransaction;
	}
}
