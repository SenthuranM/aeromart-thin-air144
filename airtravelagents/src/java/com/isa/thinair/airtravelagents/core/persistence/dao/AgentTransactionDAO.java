/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;

/**
 * @author Chamindap
 * 
 */
public interface AgentTransactionDAO {

	/**
	 * Save or update agent transaction data.
	 * 
	 * @param agentTransaction
	 *            the agentTransaction
	 */
	public void saveOrUpdate(AgentTransaction agentTransaction);

	/**
	 * Remove given AgentTransaction.
	 * 
	 * @param agentTransactionId
	 */
	public void removeAgentTransaction(int agentTransactionId);

	/**
	 * Get a given AgentTransaction.
	 * 
	 * @param agentTransactionId
	 *            agentTransactionId.
	 * @return the agentTransactionId
	 */
	public AgentTransaction getAgentTransaction(int agentTransactionId);

	/**
	 * Get a given AgentTransaction.
	 * 
	 * @param receiptNumber
	 *            receiptNumber.
	 * @return the agentTransaction.
	 */
	public AgentTransaction getAgentTransaction(String receiptNumber);

	public Collection<AgentTransaction> getAgentTransactions(AgentTransactionsDTO transactionsDTO);

	public List<AgentTransaction> isDuplicateRecordExistForAgent(String agentCode, BigDecimal amount, String currency, int nominalCode);

	public AgentTransaction getAgentTransactionByExtRef(String externalRef);
	
	public void updateAgentCreditReveresed(int agentTransactionId);
}