package com.isa.thinair.airtravelagents.core.audit;

import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * 
 * @author Duminda G
 */
public class AuditAirTravelAgent {

	public static final String MANAGE_TRAVELAGENT = "TRAVELAGENT";

	public static final String MANAGE_TRAVELAGENT_CREDIT = "TRAVELAGENTCREDIT";

	public static final String SETTLE_INVOICE = "SETTLEINVOICE";

	public static final String VIEW_INVOICE = "VIEWINVOICE";

	public static final String RECORD_PAYMENT = "RECORDPAYMENT";

	public static final String CODE = "code";

	public static final String STATUS = "status";

	public static void doAudit(String code, String type, String userID, LinkedHashMap<String, String> contents)
			throws ModuleException {

		Audit audit = new Audit();

		if (userID != null) {
			audit.setUserId(userID);
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		if (contents == null) {
			contents = new LinkedHashMap<String, String>();
		}

		if (code != null && !code.isEmpty()) {
			contents.put(CODE, code);
		}

		if (type.equals(MANAGE_TRAVELAGENT)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_DELETE_TRAVEL_AGENTS));
		}

		if (type.equals(MANAGE_TRAVELAGENT_CREDIT)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_UPDATE_TRAVEL_AGENT_CREDIT_LIMIT));
		}

		if (type.equals(VIEW_INVOICE)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_VIEW_TRAVEL_AGENT_RECIPTS));
		}

		if (type.equals(SETTLE_INVOICE)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_SETTLE_TRAVEL_AGENT_INVOICES));
		}

		if (type.equals(RECORD_PAYMENT)) {
			audit.setTaskCode(String.valueOf(TasksUtil.TA_RECORD_TRAVEL_AGENT_PAYMENTS));
		}

		AirTravelagentsModuleUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings("unused")
	public static void doAudit(Persistent persistant, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof Agent) {
			Agent agent = (Agent) persistant;
			Agent existingAgent = null;

			if (agent.getStatus().equals("NEW")) {
				audit.setUserId("");
			}

			if (persistant.getVersion() <= 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.TA_ADD_TRAVEL_AGENTS));
			} else {
				existingAgent = AirTravelagentsModuleUtils.getTravelAgentBD().getAgent(agent.getAgentCode());
				audit.setTaskCode(String.valueOf(TasksUtil.TA_EDIT_TRAVEL_AGENTS));
			}

			contents.put("agentCode", agent.getAgentCode());
			contents.put("agentType", agent.getAgentTypeCode());
			contents.put("agentName", agent.getAgentDisplayName());
			contents.put("currCode", agent.getCurrencyCode());
			contents.put("terrCode", agent.getTerritoryCode());
			contents.put("stationCode", agent.getStationCode());

			StringBuffer paymentMethods = new StringBuffer();
			if (agent.getPaymentMethod() != null && agent.getPaymentMethod().size() > 0) {
				boolean isFirst = true;
				Object value = null;
				for (Iterator<String> payMethoIt = agent.getPaymentMethod().iterator(); payMethoIt.hasNext();) {
					value = payMethoIt.next();
					if (value != null) {
						if (!isFirst) {
							paymentMethods.append(",");
						} else {
							isFirst = false;
						}
						paymentMethods.append(value.toString());
					}
				}
			}

			contents.put("payMethods", paymentMethods.toString());
			contents.put("email", getNullSafeString(agent.getEmailId()));
			contents.put("billingEmail", getNullSafeString(agent.getBillingEmail()));
			contents.put("accCode", getNullSafeString(agent.getAccountCode()));
			contents.put("crLimitSpecifiedCur", agent.getCurrencySpecifiedIn());
			contents.put("crLimit", String.valueOf(agent.getCreditLimit()));
			contents.put("crLimitLocal", String.valueOf(agent.getCreditLimitLocal()));
			contents.put("rptToGSA", getNullSafeString(agent.getReportingToGSA()));
			contents.put("handlingChg", String.valueOf(agent.getHandlingChargAmount()));
			contents.put("bankGur", String.valueOf(agent.getBankGuaranteeCashAdvance()));
			contents.put("bankGurLocal", String.valueOf(agent.getBankGuaranteeCashAdvanceLocal()));
			contents.put("prefLang", getNullSafeString(agent.getLanguageCode()));
			contents.put("autoInvoice", getNullSafeString(agent.getAutoInvoice()));
			contents.put("iataNum", getNullSafeString(agent.getAgentIATANumber()));
			contents.put("licenseNum", getNullSafeString(agent.getLicenceNumber()));
			contents.put(
					"address",
					(getNullSafeString(agent.getAddressLine1())
							+ ((agent.getAddressLine2() != null && !"".equals(agent.getAddressLine2())) ? " " : "") + getNullSafeString(agent
							.getAddressLine2())));
			contents.put("city", getNullSafeString(agent.getAddressCity()));
			contents.put("state", getNullSafeString(agent.getAddressStateProvince()));
			contents.put("postalCode", getNullSafeString(agent.getPostalCode()));
			contents.put("telNo", getNullSafeString(agent.getTelephone()));
			contents.put("fax", getNullSafeString(agent.getFax()));
			contents.put("contact", getNullSafeString(agent.getContactPersonName()));
			contents.put("commCode", getNullSafeString(agent.getAgentCommissionCode()));
			contents.put("status", agent.getStatus());
			contents.put("handlingFeeModificationStatus", agent.getHandlingFeeModificationEnabled());
			contents.put("handlingFeeModificationApplies", agent.getHandlingFeeModificationApllieTo());
			
			
			if (existingAgent != null) {
				try {
					contents.put("changedContent", populateChangedContent(existingAgent, agent));
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		// TODO
		if (persistant instanceof AgentTO) {
			AgentTO agentTO = (AgentTO) persistant;

		}
		AirTravelagentsModuleUtils.getAuditorBD().audit(audit, contents);

	}

	private static String getNullSafeString(Object obj) {
		String value = "";
		if (obj != null) {
			value = obj.toString();
		}
		return value;
	}

	private static String populateChangedContent(Agent existingAgent, Agent modifiedAgent) throws ParseException {

		String changedContent = "";
		String separator = ",";

		PlatformUtiltiies.nullHandler(existingAgent.getAgentTypeCode());

		if (!PlatformUtiltiies.nullHandler(existingAgent.getAgentTypeCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAgentTypeCode()))) {// M
			changedContent += "agent_type_code changed from " + blankHandler(existingAgent.getAgentTypeCode()) + " to "
					+ blankHandler(modifiedAgent.getAgentTypeCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAgentName()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAgentName()))) {// M
			changedContent += "agent_name changed from " + blankHandler(existingAgent.getAgentName()) + " to "
					+ blankHandler(modifiedAgent.getAgentName()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAddressLine1()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAddressLine1()))) {// M
			changedContent += "address_line1 changed from " + blankHandler(existingAgent.getAddressLine1()) + " to "
					+ blankHandler(modifiedAgent.getAddressLine1()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAddressLine2()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAddressLine2()))) {
			changedContent += "address_line2 changed from " + blankHandler(existingAgent.getAddressLine2()) + " to "
					+ blankHandler(modifiedAgent.getAddressLine2()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAddressCity()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAddressCity()))) {// M
			changedContent += "address_city changed from " + blankHandler(existingAgent.getAddressCity()) + " to "
					+ blankHandler(modifiedAgent.getAddressCity()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAddressStateProvince()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAddressStateProvince()))) {
			changedContent += "address_state_province changed from " + blankHandler(existingAgent.getAddressStateProvince())
					+ " to " + blankHandler(modifiedAgent.getAddressStateProvince()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getPostalCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getPostalCode()))) {
			changedContent += "postal_code changed from " + blankHandler(existingAgent.getPostalCode()) + " to "
					+ blankHandler(modifiedAgent.getPostalCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getTelephone()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getTelephone()))) {// M
			changedContent += "telephone_number changed from " + blankHandler(existingAgent.getTelephone()) + " to "
					+ blankHandler(modifiedAgent.getTelephone()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getFax()).equals(PlatformUtiltiies.nullHandler(modifiedAgent.getFax()))) {
			changedContent += "fax_number changed from " + blankHandler(existingAgent.getFax()) + " to "
					+ blankHandler(modifiedAgent.getFax()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getMobile()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getMobile()))) {
			changedContent += "mobile_number changed from " + blankHandler(existingAgent.getMobile()) + " to "
					+ blankHandler(modifiedAgent.getMobile()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getTerritoryCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getTerritoryCode()))) {
			changedContent += "territory_code changed from " + blankHandler(existingAgent.getTerritoryCode()) + " to "
					+ blankHandler(modifiedAgent.getTerritoryCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getStationCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getStationCode()))) {
			changedContent += "station_code changed from " + blankHandler(existingAgent.getStationCode()) + " to "
					+ blankHandler(modifiedAgent.getStationCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getContactPersonName()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getContactPersonName()))) {
			changedContent += "contact_person_name changed from " + blankHandler(existingAgent.getContactPersonName()) + " to "
					+ blankHandler(modifiedAgent.getContactPersonName()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getLicenceNumber()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getLicenceNumber()))) {
			changedContent += "licence_number changed from " + blankHandler(existingAgent.getLicenceNumber()) + " to "
					+ blankHandler(modifiedAgent.getLicenceNumber()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getLanguageCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getLanguageCode()))) {
			changedContent += "language_code changed from "
					+ ("en".equals(existingAgent.getLanguageCode()) ? "English" : "Persian") + " to "
					+ ("en".equals(modifiedAgent.getLanguageCode()) ? "English" : "Persian") + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getPrefferedRptFormat()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getPrefferedRptFormat()))) {
			changedContent += "pref_report_format changed from "
					+ ("FR".equals(existingAgent.getPrefferedRptFormat()) ? "EURO" : "US") + " to "
					+ ("FR".equals(modifiedAgent.getPrefferedRptFormat()) ? "EURO" : "US") + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getLocationUrl()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getLocationUrl()))) {
			changedContent += "google_map_url changed from " + blankHandler(existingAgent.getLocationUrl()) + " to "
					+ blankHandler(modifiedAgent.getLocationUrl()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getCharterAgent()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getCharterAgent()))) {
			changedContent += "charter_agent changed from "
					+ ("Y".equals(existingAgent.getCharterAgent()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getCharterAgent()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getCurrencyCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getCurrencyCode()))) {
			changedContent += "currency_code changed from " + blankHandler(existingAgent.getCurrencyCode()) + " to "
					+ blankHandler(modifiedAgent.getCurrencyCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAgentIATANumber()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAgentIATANumber()))) {
			changedContent += "agent_iata_number changed from " + blankHandler(existingAgent.getAgentIATANumber()) + " to "
					+ blankHandler(modifiedAgent.getAgentIATANumber()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getStatus()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getStatus()))) {
			changedContent += "agent_status changed from " + getStatusChange(existingAgent.getStatus()) + " to "
					+ getStatusChange(modifiedAgent.getStatus()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getNotes()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getNotes()))) {
			changedContent += "agent_notes changed from " + blankHandler(existingAgent.getNotes()) + " to "
					+ blankHandler(modifiedAgent.getNotes()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getPublishToWeb()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getPublishToWeb()))) {
			changedContent += "publish_to_web changed from "
					+ ("Y".equals(existingAgent.getPublishToWeb()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getPublishToWeb()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getOnHold()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getOnHold()))) {
			changedContent += "allow_gds_onhold changed from " + ("Y".equals(existingAgent.getOnHold()) ? "enabled" : "disabled")
					+ " state to " + ("Y".equals(modifiedAgent.getOnHold()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getReportingToGSA()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getReportingToGSA()))) {
			changedContent += "report_to_gsa changed from "
					+ ("Y".equals(existingAgent.getReportingToGSA()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getReportingToGSA()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAgentWiseTicketEnable()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAgentWiseTicketEnable()))) {
			changedContent += "agent_level_eticket changed from "
					+ ("Y".equals(existingAgent.getAgentWiseTicketEnable()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getAgentWiseTicketEnable()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAgentWiseFareMaskEnable()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAgentWiseFareMaskEnable()))) {
			changedContent += "agent_wise_fare_mask changed from "
					+ ("Y".equals(existingAgent.getAgentWiseFareMaskEnable()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getAgentWiseFareMaskEnable()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!existingAgent.getPaymentMethod().isEmpty() && !modifiedAgent.getPaymentMethod().isEmpty()) {

			Set<String> existingPaymentMethods = existingAgent.getPaymentMethod();
			Set<String> newPaymentMethods = modifiedAgent.getPaymentMethod();

			String strRemovedMethods = "";
			String strAddedMethods = "";

			for (String method : existingPaymentMethods) {
				if (!newPaymentMethods.contains(method)) {
					// newly removed from the existing
					strRemovedMethods += ("'" + getPaymentMethod(method) + "'");
				}
			}

			for (String method : newPaymentMethods) {
				if (!existingPaymentMethods.contains(method)) {
					// newly added
					strAddedMethods += ("'" + getPaymentMethod(method) + "'");
				}
			}

			if ("".equals(strRemovedMethods) && !("".equals(strAddedMethods))) {
				changedContent += "payment_methods " + strAddedMethods + " were added" + separator;
			} else if ("".equals(strAddedMethods) && !("".equals(strRemovedMethods))) {
				changedContent += "payment_methods " + strRemovedMethods + " were removed" + separator;
			} else if (!("".equals(strRemovedMethods) && "".equals(strAddedMethods))) {
				changedContent += "payment_methods " + strRemovedMethods + " were removed and " + strAddedMethods + " were added"
						+ separator;
			}

		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getEmailId()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getEmailId()))) {
			changedContent += "email_id changed from " + blankHandler(existingAgent.getEmailId()) + " to "
					+ blankHandler(modifiedAgent.getEmailId()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getBillingEmail()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getBillingEmail()))) {
			changedContent += "billing_email changed from " + blankHandler(existingAgent.getBillingEmail()) + " to "
					+ blankHandler(modifiedAgent.getBillingEmail()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAccountCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAccountCode()))) {
			changedContent += "account_code changed from " + blankHandler(existingAgent.getAccountCode()) + " to "
					+ blankHandler(modifiedAgent.getAccountCode()) + separator;
		}

		if (!((existingAgent.getBankGuaranteeCashAdvance() != null)
				? String.valueOf(existingAgent.getBankGuaranteeCashAdvance().doubleValue())
				: "").equals(((modifiedAgent.getBankGuaranteeCashAdvance() != null) ? String.valueOf(modifiedAgent
				.getBankGuaranteeCashAdvance().doubleValue()) : ""))) {

			changedContent += "bank_gurantee_cash_advance changed from "
					+ ((existingAgent.getBankGuaranteeCashAdvance() != null) ? String.valueOf(existingAgent
							.getBankGuaranteeCashAdvance()) : "none")
					+ " to "
					+ ((modifiedAgent.getBankGuaranteeCashAdvance() != null) ? String.valueOf(modifiedAgent
							.getBankGuaranteeCashAdvance()) : "none") + separator;
		}

		if (!((existingAgent.getBankGuranteeExpiryDate() != null) ? CalendarUtil.formatForSQL(
				existingAgent.getBankGuranteeExpiryDate(), CalendarUtil.PATTERN4).toString() : "").equals(((modifiedAgent
				.getBankGuranteeExpiryDate() != null) ? CalendarUtil.formatForSQL(modifiedAgent.getBankGuranteeExpiryDate(),
				CalendarUtil.PATTERN4).toString() : ""))) {

			changedContent += "bank_gurantee_expiry_date changed from "
					+ ((existingAgent.getBankGuranteeExpiryDate() != null) ? CalendarUtil.formatForSQL(
							existingAgent.getBankGuranteeExpiryDate(), CalendarUtil.PATTERN4).toString() : "")
					+ " to "
					+ ((modifiedAgent.getBankGuranteeExpiryDate() != null) ? CalendarUtil.formatForSQL(
							modifiedAgent.getBankGuranteeExpiryDate(), CalendarUtil.PATTERN4).toString() : "") + separator;

		}
		if (((existingAgent.getBspGuaranteeLocal() != null) && (modifiedAgent.getBspGuaranteeLocal() != null))
				&& (existingAgent.getBspGuaranteeLocal().doubleValue() != modifiedAgent.getBspGuaranteeLocal().doubleValue())) {
			changedContent += "bsp_guarantee changed from "
					+ PlatformUtiltiies.nullHandler(existingAgent.getBspGuaranteeLocal().longValue()) + " to "
					+ PlatformUtiltiies.nullHandler(modifiedAgent.getBspGuaranteeLocal().longValue()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getCurrencyCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getCurrencyCode()))) {
			changedContent += "currency_code changed from " + blankHandler(existingAgent.getCurrencyCode()) + " to "
					+ blankHandler(modifiedAgent.getCurrencyCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getAutoInvoice()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getAutoInvoice()))) {
			changedContent += "auto_invoice changed from "
					+ ("Y".equals(existingAgent.getAutoInvoice()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getAutoInvoice()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (existingAgent.getHandlingFeeAppliesTo() != modifiedAgent.getHandlingFeeAppliesTo()) {
			changedContent += "handling_fee_applies_to changed from "
					+ (existingAgent.getHandlingFeeAppliesTo() == 5 ? "Adult-Child" : "Reservation") + " to "
					+ (existingAgent.getHandlingFeeAppliesTo() == 5 ? "Adult-Child" : "Reservation") + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeOnewayAmount()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeOnewayAmount()))) {
			changedContent += "handling_fee_oneway_amount changed from "
					+ PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeOnewayAmount()) + " to "
					+ PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeOnewayAmount()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeReturnAmount()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeReturnAmount()))) {
			changedContent += "handling_fee_return_amount changed from "
					+ PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeReturnAmount()) + " to "
					+ PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeReturnAmount()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeEnable()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeEnable()))) {
			changedContent += "handling_fee changed from "
					+ ("Y".equals(existingAgent.getHandlingFeeEnable()) ? "enabled" : "disabled") + " state to "
					+ ("Y".equals(modifiedAgent.getHandlingFeeEnable()) ? "enabled" : "disabled") + " state" + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeChargeBasisCode()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeChargeBasisCode()))) {
			changedContent += "handling_charge_basis_code changed from "
					+ blankHandler(existingAgent.getHandlingFeeChargeBasisCode()) + " to "
					+ blankHandler(modifiedAgent.getHandlingFeeChargeBasisCode()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeMinAmount()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeMinAmount()))) {
			changedContent += "handling_fee_min_amount changed from "
					+ PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeMinAmount()) + " to "
					+ PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeMinAmount()) + separator;
		}
		if (!PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeMaxAmount()).equals(
				PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeMaxAmount()))) {
			changedContent += "handling_fee_max_amount changed from "
					+ PlatformUtiltiies.nullHandler(existingAgent.getHandlingFeeMaxAmount()) + " to "
					+ PlatformUtiltiies.nullHandler(modifiedAgent.getHandlingFeeMaxAmount()) + separator;
		}
		
		
		
		if ((existingAgent.getModificationHandlingFees() == null || existingAgent.getModificationHandlingFees().isEmpty())
				&& (modifiedAgent.getModificationHandlingFees() != null
						&& !modifiedAgent.getModificationHandlingFees().isEmpty())) {
			changedContent += "Handling Fee for Modification added" + changedContent;
			for (AgentHandlingFeeModification hfm : modifiedAgent.getModificationHandlingFees()) {
				changedContent += "new handling fee for modification operation "
						+ PlatformUtiltiies.nullHandler(hfm.getOperation()) + separator;
				changedContent += "new handling fee for modification status " + PlatformUtiltiies.nullHandler(hfm.getStatus())
						+ separator;
				changedContent += "new handling fee for modification amount " + PlatformUtiltiies.nullHandler(hfm.getAmount())
						+ separator;
			}
		} else if (existingAgent.getModificationHandlingFees() != null && !existingAgent.getModificationHandlingFees().isEmpty()
				&& modifiedAgent.getModificationHandlingFees() != null
				&& !modifiedAgent.getModificationHandlingFees().isEmpty()) {

			for (AgentHandlingFeeModification hfmOld : existingAgent.getModificationHandlingFees()) {
				for (AgentHandlingFeeModification hfmNew : modifiedAgent.getModificationHandlingFees()) {

					if (!hfmOld.getOperation().equals(hfmNew.getOperation())) {
						changedContent += "handling fee for modification operation changed "
								+ PlatformUtiltiies.nullHandler(hfmOld.getOperation()) + " to "
								+ PlatformUtiltiies.nullHandler(hfmNew.getOperation()) + separator;
					}
					if (!hfmOld.getStatus().equals(hfmNew.getStatus())) {
						changedContent += "handling fee for modification status changed "
								+ PlatformUtiltiies.nullHandler(hfmOld.getStatus()) + " to "
								+ PlatformUtiltiies.nullHandler(hfmNew.getStatus()) + separator;
					}
					if (!hfmOld.getAmount().equals(hfmNew.getAmount())) {
						changedContent += "handling fee for modification operation changed "
								+ PlatformUtiltiies.nullHandler(hfmOld.getAmount()) + " to "
								+ PlatformUtiltiies.nullHandler(hfmNew.getAmount()) + separator;
					}

				}
			}
		}
		
		return changedContent;
	}

	private static String getStatusChange(String status) {
		String stStatus = "";

		if ("ACT".equals(status)) {
			stStatus = "Active";
		} else if ("INA".equals(status)) {
			stStatus = "In-Active";
		} else if ("NEW".equals(status)) {
			stStatus = "New";
		} else {
			stStatus = "Black Listed";
		}
		return stStatus;
	}

	private static String getPaymentMethod(String paymentMethod) {
		String strPaymentMethod = "";

		if ("OA".equals(paymentMethod)) {
			strPaymentMethod = "On Account";
		} else if ("CA".equals(paymentMethod)) {
			strPaymentMethod = "Cash";
		} else if ("CC".equals(paymentMethod)) {
			strPaymentMethod = "Credit Card";
		} else {
			strPaymentMethod = "BSP";
		}
		return strPaymentMethod;
	}

	private static String blankHandler(Object object) {
		if (object == null) {
			return "none";
		} else {
			if ("".equals(object.toString().trim())) {
				return "none";
			} else {
				return object.toString().trim();
			}
		}
	}
}
