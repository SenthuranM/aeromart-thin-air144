/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @author dumindag
 * @version 0.1
 */
public class AgentIncDTO implements Serializable {

	private static final long serialVersionUID = -5531477461052738588L;

	private String schemeName;

	private String status;

	private Date effectiveFrom;

	private Date effectiveTo;

	private String strEffectiveFrom;

	private String strEffectiveTo;

	/**
	 * @return Returns the effectiveFrom.
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom
	 *            The effectiveFrom to set.
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return Returns the effectiveTo.
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @param effectiveTo
	 *            The effectiveTo to set.
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return Returns the schemeName.
	 */
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName
	 *            The schemeName to set.
	 */
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return Returns the strEffectiveFrom.
	 */
	public String getStrEffectiveFrom() {
		return strEffectiveFrom;
	}

	/**
	 * @param strEffectiveFrom
	 *            The strEffectiveFrom to set.
	 */
	public void setStrEffectiveFrom(String strEffectiveFrom) {
		this.strEffectiveFrom = strEffectiveFrom;
	}

	/**
	 * @return Returns the strEffectiveTo.
	 */
	public String getStrEffectiveTo() {
		return strEffectiveTo;
	}

	/**
	 * @param strEffectiveTo
	 *            The strEffectiveTo to set.
	 */
	public void setStrEffectiveTo(String strEffectiveTo) {
		this.strEffectiveTo = strEffectiveTo;
	}

	/**
	 * @return Returns the strEffectiveTo.
	 */
	public Date getStrToDateEffectiveTo() {
		String strEffTo = this.getStrEffectiveTo();
		Calendar cal = Calendar.getInstance();
		if (strEffTo != null && !"".equals(strEffTo))
			cal.set(new Integer(strEffTo.substring(6)), new Integer(strEffTo.substring(3, 5)),
					new Integer(strEffTo.substring(0, 2)));

		Date effTo = cal.getTime();

		return effTo;
	}

	/**
	 * @return Returns the strEffectiveFrom.
	 */
	public Date getStrToDateEffectiveFrom() {
		String strEffFrom = this.getStrEffectiveFrom();
		Calendar cal = Calendar.getInstance();
		if (strEffFrom != null && !"".equals(strEffFrom))
			cal.set(new Integer(strEffFrom.substring(6)), new Integer(strEffFrom.substring(3, 5)),
					new Integer(strEffFrom.substring(0, 2)));

		Date effFrom = cal.getTime();

		return effFrom;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}