package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author eric
 * @hibernate.class table = "T_AGENT_TICKET_SEQUENCE"
 */
public class AgentTicketSequnce extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";
	public static final String STATUS_CLOSED = "CLS";

	public static final String TICKET_SEQ_SUFFIX = "AGT_TICKET_SEQ";

	private Long agentTicketSequnceId;
	private String agentCode;
	private String sequnceName;
	private Long sequnceLowerBound;
	private Long sequnceUpperBound;
	private String cycle;
	private String status;

	/**
	 * @hibernate.id column = "AGENT_TICKET_SEQUENCE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AGENT_TICKET_SEQUENCE"
	 */
	public Long getAgentTicketSequnceId() {
		return agentTicketSequnceId;
	}

	/**
	 * @param agentTicketSequnceId
	 */
	public void setAgentTicketSequnceId(Long agentTicketSequnceId) {
		this.agentTicketSequnceId = agentTicketSequnceId;
	}

	/**
	 * @return
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "SEQUENCE_NAME"
	 */
	public String getSequnceName() {
		return sequnceName;
	}

	/**
	 * @param sequnceName
	 */
	public void setSequnceName(String sequnceName) {
		this.sequnceName = sequnceName;
	}

	/**
	 * @return
	 * @hibernate.property column = "SEQUENCE_MIN"
	 */
	public Long getSequnceLowerBound() {
		return sequnceLowerBound;
	}

	/**
	 * @param sequnceLowerBound
	 */
	public void setSequnceLowerBound(Long sequnceLowerBound) {
		this.sequnceLowerBound = sequnceLowerBound;
	}

	/**
	 * @return
	 * @hibernate.property column = "SEQUENCE_MAX"
	 */
	public Long getSequnceUpperBound() {
		return sequnceUpperBound;
	}

	/**
	 * @param sequnceUpperBound
	 */
	public void setSequnceUpperBound(Long sequnceUpperBound) {
		this.sequnceUpperBound = sequnceUpperBound;
	}

	/**
	 * @return
	 * @hibernate.property column = "CYCLE"
	 */
	public String getCycle() {
		return cycle;
	}

	/**
	 * @param cycle
	 */
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
