/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.util.Set;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Util;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT_TYPE" AgentType is the entity class to repesent a AgentType model.
 */
public class AgentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3573770079232815515L;

	public static final String GSA = "GSA";
	
	public static final String SGSA = "SGSA";
	
	public static final String TA = "TA";
	
	public static final String STA = "STA";	
	
	public static final String CO = "CO";
	
	public static final String SO = "SO";

	private String agentTypeCode;

	private String agentTypeDescription;

	private String creditApplicable;
	
	private Set<String> subAgentTypeCodes;

	/**
	 * 
	 */
	public AgentType() {
		super();
	}

	/**
	 * Change to Agent Type TO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public AgentTypeTO toAgentTypeTO() throws ModuleException {
		AgentTypeTO agentTypeTO = new AgentTypeTO();
		Util.copyProperties(agentTypeTO, this);
		return agentTypeTO;
	}

	/**
	 * returns the agentTypeCode
	 * 
	 * @return Returns the agentTypeCode.
	 * @hibernate.id column = "AGENT_TYPE_CODE" generator-class = "assigned"
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	/**
	 * sets the agentTypeCode
	 * 
	 * @param agentTypeCode
	 *            The agentTypeCode to set.
	 */
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * returns the agentTypeDescription
	 * 
	 * @return Returns the agentTypeDescription.
	 * @hibernate.property column = "AGENT_TYPE_DESCRIPTION"
	 */
	public String getAgentTypeDescription() {
		return agentTypeDescription;
	}

	/**
	 * sets the agentTypeDescription
	 * 
	 * @param agentTypeDescription
	 *            The agentTypeDescription to set.
	 */
	public void setAgentTypeDescription(String agentTypeDescription) {
		this.agentTypeDescription = agentTypeDescription;
	}

	/**
	 * returns the creditApplicable
	 * 
	 * @return Returns the creditApplicable.
	 * @hibernate.property column = "CREDIT_APPLY"
	 */
	public String isCreditApplicable() {
		return creditApplicable;
	}

	/**
	 * sets whether creditApplicable is yes or no
	 * 
	 * @param agentTypeDescription
	 *            The creditApplicable to set.
	 */
	public void setCreditApplicable(String creditApplicable) {
		this.creditApplicable = creditApplicable;
	}
	
	
	/**
	 * @return Returns the subAgentTypeCodes.
	 * 
	 * @hibernate.set table="T_SUB_AGENT_TYPE" cascade="all"
	 * @hibernate.collection-key column="AGENT_TYPE_CODE"
	 * @hibernate.collection-element type="string" column="SUB_AGENT_TYPE_CODE"
	 */
	public Set<String> getSubAgentTypeCodes() {
		return subAgentTypeCodes;
	}

	public void setSubAgentTypeCodes(Set<String> subAgentTypeCodes) {
		this.subAgentTypeCodes = subAgentTypeCodes;
	}
}
