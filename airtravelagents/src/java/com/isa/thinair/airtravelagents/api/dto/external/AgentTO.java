/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * Agent TO represents Agent Data Transfer information for external clients
 * 
 * @author : Nilindra Fernando
 * @since 2.0
 */
public class AgentTO extends Tracking implements Serializable {

	private static final long serialVersionUID = -1363719869810156460L;

	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";

	public static final String REPORTING_TO_GSA_YES = "Y";
	public static final String REPORTING_TO_GSA_NO = "N";

	private String agentCode;

	private String agentName;

	private String addressLine1;

	private String addressLine2;

	private String addressCity;

	private String addressStateProvince;

	private String postalCode;

	private String accountCode;

	private String telephone;

	private String fax;

	private String emailId;

	private String agentIATANumber;

	private BigDecimal creditLimit;

	private BigDecimal bankGuaranteeCashAdvance;

	private String status;

	private String stationCode;

	private String territoryCode;

	private String agentTypeCode;

	private String reportingToGSA;

	private String billingEmail;

	private String currencyCode;

	private String countryCode;

	private BigDecimal handlingChargAmount;

	private String notes;

	private Collection<String> paymentMethods;

	private AgentSummaryTO agentSummaryTO;

	/**
	 * Returns the agentCode
	 * 
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Sets the agentCode
	 * 
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * Returns the agentName
	 * 
	 * @return Returns the agentName.
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the agentName
	 * 
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * Returns the addressLine1
	 * 
	 * @return Returns the addressLine1.
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * Sets the addressLine1
	 * 
	 * @param addressLine1
	 *            The addressLine1 to set.
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * Returns the addressLine2
	 * 
	 * @return Returns the addressLine2.
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * Sets the addressLine2
	 * 
	 * @param addressLine2
	 *            The addressLine2 to set.
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * Returns the addressCity
	 * 
	 * @return Returns the addressCity.
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set.
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Returns the addressStateProvince
	 * 
	 * @return Returns the addressStateProvince.
	 */
	public String getAddressStateProvince() {
		return addressStateProvince;
	}

	/**
	 * Sets the addressStateProvince
	 * 
	 * @param addressStateProvince
	 *            The addressStateProvince to set.
	 */
	public void setAddressStateProvince(String addressStateProvince) {
		this.addressStateProvince = addressStateProvince;
	}

	/**
	 * Returns the postalCode
	 * 
	 * @return Returns the postalCode.
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postalCode
	 * 
	 * @param postalCode
	 *            The postalCode to set.
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Returns the accountCode
	 * 
	 * @return Returns the accountCode.
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * Sets the accountCode
	 * 
	 * @param accountCode
	 *            The accountCode to set.
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * Returns the telephone
	 * 
	 * @return Returns the telephone.
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Sets the telephone
	 * 
	 * @param telephone
	 *            The telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Returns the fax
	 * 
	 * @return Returns the fax.
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax
	 * 
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Returns the emailId
	 * 
	 * @return Returns the emailId.
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Returns the agentIataNumber
	 * 
	 * @return Returns the agentIataNumber.
	 */
	public String getAgentIATANumber() {
		return agentIATANumber;
	}

	/**
	 * Sets the agentIataNumber
	 * 
	 * @param agentIataNumber
	 *            The agentIataNumber to set.
	 */
	public void setAgentIATANumber(String agentIATANumber) {
		this.agentIATANumber = agentIATANumber;
	}

	/**
	 * Returns the creditLimit
	 * 
	 * @return Returns the creditLimit.
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * Sets the creditLimit
	 * 
	 * @param creditLimit
	 *            The creditLimit to set.
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * Returns the bankGuaranteeCashAdvance
	 * 
	 * @return Returns the bankGuaranteeCashAdvance.
	 */
	public BigDecimal getBankGuaranteeCashAdvance() {
		return bankGuaranteeCashAdvance;
	}

	/**
	 * Sets the bankGuaranteeCashAdvance
	 * 
	 * @param bankGuaranteeCashAdvance
	 *            The bankGuaranteeCashAdvance to set.
	 */
	public void setBankGuaranteeCashAdvance(BigDecimal bankGuaranteeCashAdvance) {
		this.bankGuaranteeCashAdvance = bankGuaranteeCashAdvance;
	}

	/**
	 * Returns the status
	 * 
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Returns the station code.
	 * 
	 * @return Returns the stationCode.
	 */
	public String getStationCode() {
		return stationCode;
	}

	/**
	 * Sets the station code
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * Returns the territory Code.
	 * 
	 * @return Returns the territoryCode.
	 */
	public String getTerritoryCode() {
		return territoryCode;
	}

	/**
	 * Sets the territoryCode
	 */
	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	/**
	 * Gets the agent type code.
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	/**
	 * Sets the agentTypeCode
	 * 
	 * @param agentTypeCode
	 *            the agentTypeCode
	 */
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * @return Returns the reportingToGSA.
	 */
	public String getReportingToGSA() {
		return reportingToGSA;
	}

	/**
	 * @param reportingToGSA
	 *            The reportingGSAId to set.
	 */
	public void setReportingToGSA(String reportingToGSA) {
		this.reportingToGSA = reportingToGSA;
	}

	/**
	 * @return Returns the billingEmail.
	 */
	public String getBillingEmail() {
		return billingEmail;
	}

	/**
	 * @param billingEmail
	 *            The billingEmail to set.
	 */
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	/**
	 * @return Returns the currencyCode.
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the handlingChargAmount.
	 */
	public BigDecimal getHandlingChargAmount() {
		return handlingChargAmount;
	}

	/**
	 * @param handlingChargAmount
	 *            The handlingChargAmount to set.
	 */
	public void setHandlingChargAmount(BigDecimal handlingChargAmount) {
		this.handlingChargAmount = handlingChargAmount;
	}

	/**
	 * @return Returns the notes.
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            The notes to set.
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return Returns the paymentMethods.
	 */
	public Collection<String> getPaymentMethods() {
		return paymentMethods;
	}

	/**
	 * @param paymentMethods
	 *            The paymentMethods to set.
	 */
	private void setPaymentMethods(Collection<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	/**
	 * Adds Payment Methods
	 * 
	 * @param paymentCode
	 */
	public void addPaymentMethods(String paymentCode) {
		if (this.getPaymentMethods() == null) {
			this.setPaymentMethods(new HashSet<String>());
		}

		this.getPaymentMethods().add(paymentCode);
	}

	/**
	 * @return Returns the agentSummaryTO.
	 */
	public AgentSummaryTO getAgentSummaryTO() {
		return agentSummaryTO;
	}

	/**
	 * @param agentSummaryTO
	 *            The agentSummaryTO to set.
	 */
	public void setAgentSummaryTO(AgentSummaryTO agentSummaryTO) {
		this.agentSummaryTO = agentSummaryTO;
	}

	/**
	 * @return Returns the countryCode.
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}