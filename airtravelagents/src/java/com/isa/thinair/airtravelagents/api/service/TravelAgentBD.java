/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.criteria.AdminFeeAgentSearchCriteria;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airtravelagents.api.dto.AgentIncDTO;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.airtravelagents.api.dto.SaveIncentiveDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTicketStockTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTypeTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.airtravelagents.api.model.AgentTicketStock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap
 */
public interface TravelAgentBD {

	public static final String SERVICE_NAME = "TravelAgentService";

	/**
	 * Get Agent for agentID
	 * 
	 * @param agnetId
	 *            the agent.
	 * @return Agent
	 */
	public Agent getAgent(String agnetId) throws ModuleException;

	/**
	 * Get Agent ShowPriceInSelectedCur for agentID
	 * 
	 * @param agnetId
	 *            the agent.
	 * @return showPriceInSelectedCur
	 */
	public boolean getShowPriceInSelectedCur(String agentId) throws ModuleException;

	/**
	 * Get Agent for agentIATANumber
	 * 
	 * @param agentIATANumber
	 *            of the agent.
	 * @return Agent
	 */
	public Agent getAgentByIATANumber(String agentIATANumber) throws ModuleException;

	/**
	 * Save the agent.
	 * 
	 * @param agent
	 *            the agent.
	 */
	public Agent saveAgent(Agent agent) throws ModuleException;

	/**
	 * Remove the agent.
	 * 
	 * @param agentId
	 *            the agent.
	 */
	public void removeAgent(Agent agent) throws ModuleException;

	/**
	 * return if the given agent code is belong to own airline or not
	 * 
	 * @param agentId
	 * @return
	 * @throws ModuleException
	 */
	public boolean isDryAgent(String agentId) throws ModuleException;

	/**
	 * Get paged Agent collection.
	 * 
	 * @param startIndex
	 *            starting from.
	 * @param pageSize
	 *            the size.
	 * @return Paged Agent collection
	 */
	public Page getAgents(int startIndex, int pageSize) throws ModuleException;
	
	/**
	 * Get paged Agent collection.
	 * 
	 * @param startIndex
	 *            starting from.
	 * @param pageSize
	 *            the size.
	 * @return Paged Agent collection
	 */
	public Page getActiveAgents(AdminFeeAgentSearchCriteria criteria, int rownum, int pageSize) throws ModuleException;

	public void saveAdminFeeAgent(List agentCodesAddList, List agentCodesDeleteList) throws ModuleException;
	
	/**
	 * Get all the Agents reporting to given GSA
	 * 
	 * @param gsaId
	 *            the gsa id.
	 * @return Agent object collection
	 */
	public Collection<Agent> getAgentsForGSA(String gsaId) throws ModuleException;

	/**
	 * Get all the Agents reporting to given GSA
	 * 
	 * @param gsaId
	 *            the gsa id.
	 * @return Agent object collection
	 */
	public Collection<Agent> getReportingAgentsForGSA(String gsaId) throws ModuleException;

	/**
	 * Get all the Reporting Agents to given Territory
	 * 
	 * @param territoryId
	 *            the territory Id.
	 * @return Agent object collection
	 */
	public Collection<Agent> getReportingAgentsForTerritory(String territoryId) throws ModuleException;

	/**
	 * Get all the Agents reporting to given GSA
	 * 
	 * @param gsaId
	 *            the gsa id.
	 * @return Agent object collection
	 */
	public Collection<Agent> getActiveAgentsForGSA(String gsaId) throws ModuleException;

	/**
	 * Get all the agents for the given territory
	 * 
	 * @param territoryId
	 *            the territory id.
	 * @return Agent Object collection
	 */
	public Collection<Agent> getAgentForTerritory(String territoryId) throws ModuleException;

	/**
	 * Get GSA for the given territory
	 * 
	 * @param territoryId
	 *            the territory id.
	 * @return Agent Object
	 */
	public Agent getGSAForTerritory(String territoryId) throws ModuleException;

	/**
	 * 
	 * @param countryCode
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Agent> getAgentsForCountry(String countryCode) throws ModuleException;

	/**
	 * Get all the agents for the given list of agentCodes
	 * 
	 * @param agentCodes
	 *            the agent code collection.
	 * @return Agent object collection
	 */
	public Collection<Agent> getAgents(Collection<String> agentCodes) throws ModuleException;

	/**
	 * Get all the agents for the given list of agentCodes
	 * 
	 * @param agentCodes
	 *            the agent code collection.
	 * @return Agent object Map
	 */
	public Map<String, Agent> getAgentsMap(Collection<String> agentCodes) throws ModuleException;

	/**
	 * Get Paged Agent collection, matching the criteria
	 * 
	 * @param criterion
	 *            the selection crieterion
	 * @return the agents collection.
	 */
	public Page searchAgents(List<ModuleCriterion> criterion, int startIndex, int pageSize) throws ModuleException;

	/**
	 * Get all the agent types
	 * 
	 * @return AgentType object collection
	 */
	public Collection<AgentType> getAgentTypes() throws ModuleException;

	/**
	 * Get all the GSA agents with agent code, name and territory id.
	 * 
	 * @return Agent object collection with agent code, name and territory id set
	 * @throws ModuleException
	 */
	public Collection<Agent> getGSAsForTerritories() throws ModuleException;

	/**
	 * Get agnet credit history.
	 * 
	 * @param agentCode
	 * @return AgentCreditHistory object collection
	 * @throws ModuleException
	 */
	public Collection<AgentCreditHistory> getCreditHistoryForAgent(String agentCode) throws ModuleException;

	/**
	 * Override current agent credit limit
	 * 
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 * @param remarks
	 * @param forceCancel
	 * @param loginId
	 * @throws ModuleException
	 */
	public void adjustCreditLimit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit, String remarks,
			boolean forceCancel) throws ModuleException;

	/**
	 * Override current agent credit limit
	 * 
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 * @param remarks
	 * @param forceCancel
	 * @param loginId
	 * @throws ModuleException
	 */
	public void adjustCreditLimit(String agentCode, BigDecimal currentCreditLimit, BigDecimal newCreditLimit, String remarks,
			boolean forceCancel, String currencySpecifiedIn) throws ModuleException;

	/**
	 * Get agent type for given agent
	 * 
	 * @param agentTypeCode
	 * @return AgentType
	 * @throws ModuleException
	 */
	public AgentType getAgentType(String agentTypeCode) throws ModuleException;

	/**
	 * Get all the agents of given type
	 * 
	 * @param agentTypeCode
	 * @return Agent object collection
	 * @throws ModuleException
	 */
	public Collection<Agent> getAgents(String agentTypeCode) throws ModuleException;

	/**
	 * 
	 * @throws ModuleException
	 */
	public void blockNonPayingAgents() throws ModuleException;

	public Collection<String> getAllAgents() throws ModuleException;

	public Collection<String> getAllOwnAirlineAgents() throws ModuleException;

	public void adjustCreditLimitForAgents(Date date) throws ModuleException;

	public void updateCreditLimitUpdateStatus(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException;

	/**
	 * Retrieves agents details for a given country/city/ and type. JIRA - AARESAA:2650
	 * 
	 * @param country
	 * @param city
	 * @param type
	 * @return
	 * @throws ModuleException
	 * @author lalanthi
	 */
	public Collection<AgentInfoDTO> getAgentsByLocationAndType(String country, String city, String type) throws ModuleException;

	/**
	 * 
	 * @param agentCode
	 * @param amount
	 */
	public void updateAgentSummarySettle(String agentCode, BigDecimal amount) throws ModuleException;

	public HashMap<String, List<AgentInfoDTO>> getAgentsForCountries(List<String> countryCodes) throws ModuleException;

	/**
	 * Get all agents available for dry user
	 * 
	 * @param isOnAccount
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public List<AgentInfoDTO> getAllAgenntsForDryOperation(boolean isOnAccount) throws ModuleException;

	/**
	 * Get all the reporting agents for dry user
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public List<AgentInfoDTO> getAllReportingAgenntsForDryOperation(String agentCode, boolean isOnAccount) throws ModuleException;

	public BigDecimal getUninvocedTotal(String agentCode, Date startDate, Date endDate) throws ModuleException;

	public AgentIncentive getAgentIncentive(String SchemeName) throws ModuleException;

	public Page getAgentIncentive(AgentIncDTO incentiveSearchDTO, int startIndex, int pageSize) throws ModuleException;

	public void saveIncentive(SaveIncentiveDTO incentiveSave) throws ModuleException;

	public Page getAgentIncentiveSlabs(Integer incentiveID, int startIndex, int pageSize) throws ModuleException;

	public void removeIncentive(Integer incentiveID, long version) throws ModuleException;

	public Collection<AgentIncentive> getActiveAgentIncentive() throws ModuleException;

	public Collection<AgentIncentive> createIncentivCodes() throws ModuleException;

	public Collection<AgentIncentiveSlab> createIncentivSlabs(Integer schmeId) throws ModuleException;

	public boolean saveAgentIncentive(Collection<Integer> lstIncenID, String agentCode) throws ModuleException;

	public Collection<AgentTicketStock> getAgentTicketStock(String agentCode) throws ModuleException;

	public void saveAgentStock(AgentTicketStock agentTicketStock) throws ModuleException;

	/**
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public String getAgentCountry(String agentCode) throws ModuleException;

	public Page getAgentTicketStocks(String agentCode, int startIndex, int pageSize) throws ModuleException;

	public Collection<AgentTicketSequnce> getAgentTicketStocks(String agentCode) throws ModuleException;

	public void saveAgentTicketStock(AgentTicketStockTO agentTicketStockTo) throws ModuleException;

	public Map<String, String> getAgentTransationNominalCodes() throws ModuleException;

	public List<String> getGSAAirports(String territoryCode) throws ModuleException;

	public void deactivateTravelAgents() throws ModuleException;

	public Collection<AgentTO> getAllAgentTOs() throws ModuleException;

	public AgentTO getAgentTO(String agentId) throws ModuleException;

	public Page searchAgentTOs(List<ModuleCriterion> criterion, int startIndex, int pageSize, List<String> orderByFieldList)
			throws ModuleException;

	public Collection<AgentTypeTO> getAgentTypeTOs(List<ModuleCriterion> criteria, List<String> orderByFieldList)
			throws ModuleException;

	public void removeAgent(String agentCode, long version) throws ModuleException;

	public void saveAgent(AgentTO agentTO) throws ModuleException;

	public String getAgentTypeByAgentId(String agentId) throws ModuleException;

	public List<Agent> getLCCUnpublishedAgents() throws ModuleException;

	public void updateAgentVersion(String agentCode, long version) throws ModuleException;

	public void updateAgentLCCPublishStatus(List<Agent> unPublishedAgents, List<String> updatedAgentCodes) throws ModuleException;

	public void setAgentToken(AgentToken agentToken) throws ModuleException;
	
	public AgentToken getAgentToken(String token) throws ModuleException;
	
	public void expireAgentToken(int tokenID) throws ModuleException;
	
	public AgentToken getAgentTokenById(String token) throws ModuleException;
	
	public String getCurrencyCodeByStationCode(String stationCode) throws ModuleException;
	
	public Agent getAgentDetailsByTokenId(String token) throws ModuleException;
	
	public String getCreditSharingAgentCode(String agentCode) throws ModuleException;

	public Agent getCreditSharingAgent(String agentCode) throws ModuleException;
	
	public void purgeExpiredAgentTokens() throws ModuleException;
	
	public void removeAgentToken(int agentTokenId) throws ModuleException;
	
	public List<String> getSubAgentTypesByAgentCode(String agentCode) throws ModuleException;
	
	public List<AgentSummary> getAgentSummary(String agentCode) throws ModuleException;

	public Collection<Agent> getReportingAgentsForGSA(String gsaId, Collection<String> selectedAgentTypes, boolean includeGsa)
			throws ModuleException;
	public List<String> getAgentWiseApplicableRoutes (String agentCode) throws ModuleException;
	
	public Collection<AgentHandlingFeeModification> getAgentActiveHandlingFeeDefinedForMOD(String agentCode)
			throws ModuleException;
	
	public List<String> getWebServiceSpecificFareAgentsAndDifferentCurrencyAgents();

}