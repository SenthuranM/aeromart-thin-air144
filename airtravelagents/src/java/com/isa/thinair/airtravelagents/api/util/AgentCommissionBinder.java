package com.isa.thinair.airtravelagents.api.util;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndChgCommission;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author rimaz
 * 
 */
public class AgentCommissionBinder {

	/**
	 * Reservation level agent commission binder (CR) -- used to track, reservation level removed commissions
	 * 
	 * @param resPaxOndCharge
	 * @param amount
	 * @param agentCode
	 * @param userId
	 * @throws ModuleException
	 */
	public static void bindCreditInstance(ReservationPaxOndCharge resPaxOndCharge, BigDecimal amount, String agentCode,
			String userId, String status) throws ModuleException {
		ReservationPaxOndChgCommission agentCommission = new ReservationPaxOndChgCommission();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("airtravelagent.param.null");
		}
		agentCommission.setResPaxOndCharge(resPaxOndCharge);
		agentCommission.setAmount(amount.negate());
		agentCommission.setDrOrcr(AirTravelAgentConstants.TnxTypes.CREDIT);
		agentCommission.setAgentCode(agentCode);
		agentCommission.setUserId(userId);
		agentCommission.setTransctionDate(new GregorianCalendar().getTime());
		agentCommission.setStatus(status);
		resPaxOndCharge.getResPaxOndChgCommission().add(agentCommission);

	}

	/**
	 * Reservation level agent commission binder (DR) -- used to track, reservation level applied commissions
	 * 
	 * @param resPaxOndCharge
	 * @param amount
	 * @param agentCode
	 * @param userId
	 * @throws ModuleException
	 */
	public static void bindDebitInstance(ReservationPaxOndCharge resPaxOndCharge, BigDecimal amount, String agentCode,
			String userId, String status) throws ModuleException {
		ReservationPaxOndChgCommission agentCommission = new ReservationPaxOndChgCommission();

		if (amount != null && amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("airtravelagent.param.null");
		}
		agentCommission.setResPaxOndCharge(resPaxOndCharge);
		agentCommission.setAmount(amount);
		agentCommission.setDrOrcr(AirTravelAgentConstants.TnxTypes.DEBIT);
		agentCommission.setAgentCode(agentCode);
		agentCommission.setUserId(userId);
		agentCommission.setTransctionDate(new GregorianCalendar().getTime());
		agentCommission.setStatus(status);
		resPaxOndCharge.getResPaxOndChgCommission().add(agentCommission);
	}

}
