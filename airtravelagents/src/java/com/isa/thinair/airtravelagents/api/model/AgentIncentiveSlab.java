/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * <b>Entity class to handle Agent Incentive slab</b>
 * 
 * @author dumindag
 * 
 * @hibernate.class table = "T_AGENT_INCENTIVE_SCHEME_SLAB"
 * 
 */

public class AgentIncentiveSlab extends Tracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3930499706269725227L;

	private Integer agentIncSlabID;

	private Integer agentIncSchemeID;

	private Integer rateStartValue;

	private Integer rateEndValue;

	private double persentage;

	/**
	 * @return Returns the agentIncSlabID.
	 * @hibernate.property column = "AGENT_INCENTIVE_SCHEME_ID"
	 */
	public Integer getAgentIncSchemeID() {
		return agentIncSchemeID;
	}

	/**
	 * 
	 * @param agentIncScemeID
	 */
	public void setAgentIncSchemeID(Integer agentIncSchemeID) {
		this.agentIncSchemeID = agentIncSchemeID;
	}

	/**
	 * @return Returns the agentIncScemeSlabID.
	 * @hibernate.id column ="AGENT_INC_SCHEME_SLAB_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_INCENTIVE_SCHEME_SLAB"
	 */
	public Integer getAgentIncSlabID() {
		return agentIncSlabID;
	}

	/**
	 * @param agentIncSlabID
	 *            The agentIncSlabID to set.
	 */
	public void setAgentIncSlabID(Integer agentIncSlabID) {
		this.agentIncSlabID = agentIncSlabID;
	}

	/**
	 * @return Returns the persentage.
	 * @hibernate.property column = "PERCENTAGE"
	 */
	public double getPersentage() {
		return persentage;
	}

	/**
	 * @param presentage
	 *            The persentage to set.
	 */
	public void setPersentage(double presentage) {
		this.persentage = presentage;
	}

	/**
	 * @return Returns the rateEndValue.
	 * @hibernate.property column = "RANGE_END_VALUE"
	 */
	public Integer getRateEndValue() {
		return rateEndValue;
	}

	/**
	 * @param rateEndValue
	 *            The rateEndValue to set.
	 */
	public void setRateEndValue(Integer rateEndValue) {
		this.rateEndValue = rateEndValue;
	}

	/**
	 * @return Returns the rateStartValue.
	 * @hibernate.property column = "RANGE_START_VALUE"
	 */
	public Integer getRateStartValue() {
		return rateStartValue;
	}

	/**
	 * @param rateStartValue
	 *            The rateStartValue to set.
	 */
	public void setRateStartValue(Integer rateStartValue) {
		this.rateStartValue = rateStartValue;
	}

}