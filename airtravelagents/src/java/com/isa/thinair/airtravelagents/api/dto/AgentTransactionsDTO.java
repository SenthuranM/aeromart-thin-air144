/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTransExtNominalCode;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionsSearchTO;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public class AgentTransactionsDTO implements Serializable {

	private static final long serialVersionUID = -7947972101102816633L;

	private String agentCode;

	private Date tnxFromDate;

	private Date tnxToDate;

	private List<String> adjustmentCarriers;

	private Collection<AgentTransactionNominalCode> agentTransactionNominalCodes;

	public AgentTransactionsDTO() {
		super();
	}

	public AgentTransactionsDTO(AgentTransactionsSearchTO agentTransactionsSearchTO) throws ModuleException {
		this();
		this.setAgentCode(agentTransactionsSearchTO.getAgentCode());
		this.setTnxFromDate(agentTransactionsSearchTO.getTnxFromDate());
		this.setTnxToDate(agentTransactionsSearchTO.getTnxToDate());
		this.getAdjustmentCarriers().addAll(agentTransactionsSearchTO.getAdjustmentCarriers());

		for (AgentTransExtNominalCode agentTransExtNominalCode : agentTransactionsSearchTO.getAgentTransactionNominalCodes()) {
			this.getAgentTransactionNominalCodes().add(
					AgentTransactionNominalCode.getAgentTransactionNominalCode(agentTransExtNominalCode));
		}
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getTnxFromDate() {
		return tnxFromDate;
	}

	public void setTnxFromDate(Date tnxFromDate) {
		this.tnxFromDate = tnxFromDate;
	}

	public Date getTnxToDate() {
		return tnxToDate;
	}

	public void setTnxToDate(Date tnxToDate) {
		this.tnxToDate = tnxToDate;
	}

	public Collection<AgentTransactionNominalCode> getAgentTransactionNominalCodes() {
		if (this.agentTransactionNominalCodes == null) {
			this.agentTransactionNominalCodes = new ArrayList<AgentTransactionNominalCode>();
		}

		return agentTransactionNominalCodes;
	}

	public void setAgentTransactionNominalCodes(Collection<AgentTransactionNominalCode> agentTransactionNominalCodes) {
		this.agentTransactionNominalCodes = agentTransactionNominalCodes;
	}

	public List<String> getAdjustmentCarriers() {
		if (this.adjustmentCarriers == null) {
			this.adjustmentCarriers = new ArrayList<String>();
		}
		return adjustmentCarriers;
	}

	public void setAdjustmentCarriers(List<String> adjustmentCarriers) {
		this.adjustmentCarriers = adjustmentCarriers;
	}

}