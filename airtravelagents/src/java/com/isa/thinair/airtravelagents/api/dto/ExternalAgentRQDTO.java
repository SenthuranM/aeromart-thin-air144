package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;

public class ExternalAgentRQDTO implements Serializable {

	private String amount;

	private String currencyCode;

	private String blockId;
	
	private String orderId;

	public String getAmount() {
		return amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getBlockId() {
		return blockId;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
