/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.ExternalAccoutingSystemsConfig;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Byorn
 * 
 */
public class BLUtil {

	/**
	 * Returns the external data source
	 * 
	 * @return
	 */
	public static XDBConnectionUtil getExternalDBConnectionUtil() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=myXDBDataSourceUtil");
	}

	/**
	 * Returns the external accounting system source
	 * 
	 * @return
	 */
	public static ExternalAccoutingSystemsConfig getExternalAccoutingSystemsImplConfig() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (ExternalAccoutingSystemsConfig) lookup.getBean("isa:base://modules?id=externalAccoutingSystemsImplConfig");
	}

	public static Date getFromDate(SearchInvoicesDTO searchinvoicesDTO) {

		Date periodStartDate = new Date();

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			periodStartDate = getInvoiceFromDate(searchinvoicesDTO.getYear(), searchinvoicesDTO.getMonth() - 1,
					searchinvoicesDTO.getInvoicePeriod()).getTime();
		} else {
			int fromday = searchinvoicesDTO.getInvoicePeriod() == 1 ? 1 : 16;
			periodStartDate = new GregorianCalendar(searchinvoicesDTO.getYear(), searchinvoicesDTO.getMonth() - 1, fromday)
					.getTime();
		}
		return periodStartDate;
	}

	public static Date getPeriodToDate(SearchInvoicesDTO searchinvoicesDTO) {

		Date periodStartDate = new Date();

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			periodStartDate = getInvoiceFromDate(searchinvoicesDTO.getYearTo(), searchinvoicesDTO.getMonthTo() - 1,
					searchinvoicesDTO.getInvoicePeriodTo()).getTime();
		} else {
			int fromday = searchinvoicesDTO.getInvoicePeriodTo() == 1 ? 1 : 16;
			periodStartDate = new GregorianCalendar(searchinvoicesDTO.getYearTo(), searchinvoicesDTO.getMonthTo() - 1, fromday)
					.getTime();
		}
		return periodStartDate;
	}

	public static Date getToDate(SearchInvoicesDTO searchinvoicesDTO) {
		Date periodEndDate = new Date();

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			GregorianCalendar periodStartDate = getInvoiceFromDate(searchinvoicesDTO.getYear(), searchinvoicesDTO.getMonth() - 1,
					searchinvoicesDTO.getInvoicePeriod());
			periodStartDate.add(Calendar.DATE, 6);
			periodStartDate.add(Calendar.HOUR, 23);
			periodStartDate.add(Calendar.MINUTE, 59);
			periodStartDate.add(Calendar.SECOND, 59);
			periodEndDate = periodStartDate.getTime();
		} else {
			int fromday = searchinvoicesDTO.getInvoicePeriod() == 1 ? 1 : 16;
			int today = fromday == 1 ? 15 : -1;
			// to-day would be either 15, 30, or 31..if february it will be either 28 or 29 depending on leap year

			if (today == -1) {
				today = CalendarUtil.getLastDayOfYearAndMonth(searchinvoicesDTO.getYear(), searchinvoicesDTO.getMonth());
			}

			periodEndDate = new GregorianCalendar(searchinvoicesDTO.getYear(), searchinvoicesDTO.getMonth() - 1, today, 23, 59,
					59).getTime();
		}

		return periodEndDate;
	}

	public static GregorianCalendar getInvoiceFromDate(int year, int month, int invoicePeriod) {
		GregorianCalendar periodStartDate = null;
		int startDayNum;
		/**
		 * startDayNum should be as follows for weekly invoice generation 1-8 for period 1 8-15 for period 2 15-22 for
		 * period 3 22-29 for period 4 29+ for period 5
		 */
		if (invoicePeriod == 1) {
			startDayNum = 1;
		} else if (invoicePeriod == 2) {
			startDayNum = 8;
		} else if (invoicePeriod == 3) {
			startDayNum = 15;
		} else if (invoicePeriod == 4) {
			startDayNum = 22;
		} else {
			startDayNum = 29;
		}

		periodStartDate = new GregorianCalendar(year, month, startDayNum);
		while (periodStartDate.get(Calendar.DAY_OF_WEEK) != AppSysParamsUtil.getDayNumberToGenerateInvoices()) {
			periodStartDate.add(Calendar.DATE, 1);
		}
		return periodStartDate;
	}

	public static Date getInvoceDate(int year, int month, int invoicePeriod) {
		SearchInvoicesDTO searchInvoicesFrom = new SearchInvoicesDTO();

		searchInvoicesFrom.setYear(year);
		searchInvoicesFrom.setMonth(month);
		searchInvoicesFrom.setInvoicePeriod(invoicePeriod);

		Date from = getFromDate(searchInvoicesFrom);

		return from;
	}

	public static int getLastMonthFinalInvoicePeriod(int month, int year) {

		GregorianCalendar date = new GregorianCalendar(year, month - 1, 8);
		int lastDayOfMonth = CalendarUtil.getLastDayOfYearAndMonth(year, month);
		int invoiceCount = 0;
		for (int i = 8; i <= lastDayOfMonth; i++) {
			if (date.get(Calendar.DAY_OF_WEEK) == AppSysParamsUtil.getDayNumberToGenerateInvoices()) {
				invoiceCount++;
			}
			date.add(Calendar.DATE, 1);
		}

		return invoiceCount + 1;
	}

	/**
	 * Constructs "" or "agent_code in ('AG1','AG2')"
	 */
	public static String getSQlForFilteringAgents(Collection agentCodes, String column) {

		boolean exceedsThousand = false;
		if (agentCodes != null && agentCodes.size() > 0) {
			if (agentCodes.size() > 900) {
				exceedsThousand = true;
			} else {
				return " and " + column + " in (" + PlatformUtiltiies.constructINStringForCharactors(agentCodes) + ")";
			}

		}

		if (exceedsThousand) {
			String inSql = "and (";
			// TODO Review- splitCollectionsExceedingOneThousand
			Vector v = splitCollectionsExceedingOneThousand(agentCodes);
			for (int i = 0; i < v.size(); i++) {
				Collection chunkagentCodes = (Collection) v.get(i);
				if (i == 0) {
					inSql += " " + column + " in ( " + PlatformUtiltiies.constructINStringForCharactors(chunkagentCodes) + " )";
				} else {
					if (chunkagentCodes != null && chunkagentCodes.size() > 0) {
						inSql += " or " + column + " in ( " + PlatformUtiltiies.constructINStringForCharactors(chunkagentCodes)
								+ " )";
					}
				}

			}
			inSql += ")";
			return inSql;
		}

		return "";

	}

	public static String getSQLForFilteringAgentswithOR(Collection agentCodes, String column) {
		String agents = "";
		if (agentCodes.size() > 0) {
			agents = " AND (";
		}
		for (Iterator iterator = agentCodes.iterator(); iterator.hasNext();) {
			String agentCode = (String) iterator.next();
			agents += " " + column + "='" + agentCode + "' ";
			if (iterator.hasNext()) {
				agents += " OR ";
			} else {
				agents += " ) ";
			}
		}
		return agents;
	}

	public static String getSqlForFilteringZeroInvoice(boolean filterZerorInvoices, String column) {
		if (filterZerorInvoices) {
			return " and " + column + ">0";
		}
		return "";
	}

	public static String getFormattedDateForInvoicing() {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date currentDate = new Date(System.currentTimeMillis());
		String formattedDate = dateFormat.format(currentDate);
		return formattedDate;
	}

	/**
	 * Gets the Invoice Reports Template path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getInvoiceReportTemplate() {

		String fileName = InvoicingInternalConstants.InvoiceAttachment.INVOICE_DETAIL_JASPER_TEMPLATE;

		String reportsRootDir = null;

		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;

		return reportsRootDir;
	}

	/**
	 * Gets the Invoice Reports Template path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getInvoiceSummarryReportTemplate() {

		String fileName = InvoicingInternalConstants.InvoiceAttachment.INVOICE_SUMMARY_JASPER_TEMPLATE;

		String reportsRootDir = null;

		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;

		return reportsRootDir;
	}

	/**
	 * Gets the Invoice Reports Template path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getReportTemplate(String fileName) {

		String reportsRootDir = null;

		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;

		return reportsRootDir;
	}

	/**
	 * Method convert date to dd-MMM-YYYY format
	 * 
	 * @param dateString
	 * @return String
	 */
	public static String convertDate(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	public static Vector splitCollectionsExceedingOneThousand(Collection agentCodes) {

		Vector v = new Vector();
		int totalNumberOfRecs = agentCodes.size();
		double remainder = totalNumberOfRecs % 900;
		int numOfChunks = Math.round(totalNumberOfRecs / 900);

		if (remainder > 0) {
			numOfChunks = numOfChunks + 1;
		}

		for (int i = 0; i < numOfChunks; i++) {

			Collection chunk = new ArrayList();

			java.util.Iterator agentCodesIterator = agentCodes.iterator();

			int j = 0;
			while (agentCodesIterator.hasNext()) {

				chunk.add(agentCodesIterator.next());
				j++;
				agentCodesIterator.remove();
				if (j > 900) {
					break;
				}

			}

			v.add(chunk);
		}
		return v;
	}

	/**
	 * Convert base currency to local
	 * 
	 * @param amountInBase
	 * @param localCurrencyCode
	 * @param exchangeRateProxy
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getAmountInLocal(BigDecimal amountInBase, String localCurrencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		BigDecimal exchangeRate = exchangeRateProxy.getExchangeRate(baseCurrency, localCurrencyCode);
		return AccelAeroCalculator.multiply(amountInBase, exchangeRate);
	}

	/**
	 * Convert local currency to base
	 * 
	 * @param amountInLocal
	 * @param localCurrencyCode
	 * @param exchangeRateProxy
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getAmountInBase(BigDecimal amountInLocal, String localCurrencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		if (amountInLocal.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			BigDecimal exchangeRate = exchangeRateProxy.getExchangeRate(localCurrencyCode, baseCurrency);
			return AccelAeroCalculator.multiply(amountInLocal, exchangeRate);
		} else {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}

	}

	/**
	 * Get currencies that were updated with CREDIT_LIMIT_UPDATE_STATUS AS [P\F].
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date date) throws ModuleException {
		CommonMasterBD commonMasterBD = AirTravelagentsModuleUtils.getCommonMasterBD();
		return commonMasterBD.getAllUpdatedCurr(date);
	}

	/**
	 * <b>Updates Agents Available Credit EXRate</b> <br>
	 * <p>
	 * <b>Due Amount</b> = [current_credit_limit] - [Agents_available_credit]
	 * </p>
	 * <p>
	 * <b>New Available Credit</b> = [new_credit_limit] - [Due_amount] <br>
	 * 
	 * @param agentCode
	 * @param currentCreditLimit
	 * @param newCreditLimit
	 * @param agentSummary
	 * @return
	 */
	public static AgentSummary updateAgentAvailableCreditEXRateChange(String agentCode, BigDecimal currentCreditLimit,
			BigDecimal newCreditLimit, AgentSummary agentSummary) {

		BigDecimal newAvailableCredit;

		BigDecimal dueAmount = AccelAeroCalculator.subtract(currentCreditLimit, agentSummary.getAvailableCredit());

		newAvailableCredit = AccelAeroCalculator.subtract(newCreditLimit, dueAmount);

		if (newAvailableCredit.compareTo(BigDecimal.ZERO) != -1) {
			agentSummary.setAvailableCredit(newAvailableCredit);
			return agentSummary;
		} else {
			return null;
		}
	}

	/**
	 * Update Currency ExRate Status
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static void saveOrUpdateCurrencyExRate(Collection colCurrExRate) throws ModuleException {
		CommonMasterBD commonMasterBD = AirTravelagentsModuleUtils.getCommonMasterBD();
		commonMasterBD.saveOrUpdateCurrencyExRate(colCurrExRate);
	}

	public static String getOracleToDate(Date date) {

		String oracleDateString = " to_date(" + CalendarUtil.getSQLToDateInputString(date) + ") ";
		return oracleDateString;
	}

	/**
	 * Get a dlimitered stirng from list Eg : - if the list has X and Y and the delimiter is comma (,) returned string
	 * will be X,Y
	 * 
	 * @param list
	 * @param delimiter
	 * @return
	 */
	public static String getDelimiteredStringFromList(List<String> list, String delimiter) {

		StringBuffer sb = new StringBuffer();
		for (String str : list) {
			if (sb.length() > 0) {
				sb.append(delimiter);
			}
			sb.append(str);
		}
		return sb.toString();
	}

	/**
	 * Returns the external data source
	 * 
	 * @return
	 */
	public static XDBConnectionUtil getExternalDBConnectionUtilForX3() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=x3DBDataSourceUtil");
	}

}