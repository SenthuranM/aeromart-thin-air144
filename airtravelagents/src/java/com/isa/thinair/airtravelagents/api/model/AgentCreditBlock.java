package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author manoji
 * @hibernate.class table = "T_AGENT_CREDIT_BLOCK"
 */
public class AgentCreditBlock extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;
	private int agentCreditBlockId;
	private String Token;
	private String agentCode;
	private Date creditBlockedTimeStamp;
	private Date blockedCreditReleaseTimeStamp;
	private String status;
	private BigDecimal localAmount;
	private BigDecimal baseAmount;
	private String currencyCode;
	private Integer confirmTxnId;
	
	public static final String BLOCKED = "BLK";
	public static final String REVERSED = "REV";
	public static final String CONFRIMED = "CNF";

	/**
	 * @return
	 * @hibernate.id column = "AGENT_CREDIT_BLOCK_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_CREDIT_BLOCK"
	 */
	public int getAgentCreditBlockId() {
		return agentCreditBlockId;
	}

	public void setAgentCreditBlockId(int agentCreditBlockId) {
		this.agentCreditBlockId = agentCreditBlockId;
	}

	/**
	 * @hibernate.property column = "TOKEN"
	 */
	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.property column = "CREDIT_BLOCKED_TIMESTAMP"
	 */
	public Date getCreditBlockedTimeStamp() {
		return creditBlockedTimeStamp;
	}

	public void setCreditBlockedTimeStamp(Date creditBlockedTimeStamp) {
		this.creditBlockedTimeStamp = creditBlockedTimeStamp;
	}

	/**
	 * @hibernate.property column = "CREDIT_RELEASE_TIMESTAMP"
	 */

	public Date getBlockedCreditReleaseTimeStamp() {
		return blockedCreditReleaseTimeStamp;
	}

	public void setBlockedCreditReleaseTimeStamp(Date blockedCreditReleaseTimeStamp) {
		this.blockedCreditReleaseTimeStamp = blockedCreditReleaseTimeStamp;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "AMOUNT_LOCAL"
	 */
	public BigDecimal getLocalAmount() {
		return localAmount;
	}

	public void setLocalAmount(BigDecimal localAmount) {
		this.localAmount = localAmount;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	/**
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @hibernate.property column = "TXN_ID"
	 */
	public Integer getConfirmTxnId() {
		return confirmTxnId;
	}

	public void setConfirmTxnId(Integer confirmTxnId) {
		this.confirmTxnId = confirmTxnId;
	}
}
