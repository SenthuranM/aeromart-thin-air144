/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTransExtNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap
 */
public class AgentTransactionNominalCode implements Serializable {

	private static final long serialVersionUID = -7916642472578263228L;

	private int code;

	private AgentTransactionNominalCode(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public int getCode() {
		return code;
	}

	public static final AgentTransactionNominalCode ACC_SALE = new AgentTransactionNominalCode(1);
	public static final AgentTransactionNominalCode PAY_INV_CASH = new AgentTransactionNominalCode(2);
	public static final AgentTransactionNominalCode PAY_INV_CHQ = new AgentTransactionNominalCode(3);
	/**
	 * @deprecated
	 */
	public static final AgentTransactionNominalCode PAY_ADV_CASH = new AgentTransactionNominalCode(4);
	/**
	 * @deprecated
	 */
	public static final AgentTransactionNominalCode PAY_ADV_CHQ = new AgentTransactionNominalCode(5);
	public static final AgentTransactionNominalCode ACC_DEBIT = new AgentTransactionNominalCode(6);
	public static final AgentTransactionNominalCode ACC_REFUND = new AgentTransactionNominalCode(7);
	public static final AgentTransactionNominalCode ACC_CREDIT = new AgentTransactionNominalCode(8);
	public static final AgentTransactionNominalCode ACC_PAY_REVERSE = new AgentTransactionNominalCode(9);
	public static final AgentTransactionNominalCode ADJ = new AgentTransactionNominalCode(6);
	public static final AgentTransactionNominalCode EXT_ACC_SALE = new AgentTransactionNominalCode(10);
	public static final AgentTransactionNominalCode EXT_ACC_REFUND = new AgentTransactionNominalCode(11);
	public static final AgentTransactionNominalCode BLOCK_CREDIT = new AgentTransactionNominalCode(17);
	public static final AgentTransactionNominalCode RELEASE_BLOCKED_CREDIT = new AgentTransactionNominalCode(18);
	public static final AgentTransactionNominalCode EXT_PROD_ACC_SALE = new AgentTransactionNominalCode(19);
	public static final AgentTransactionNominalCode EXT_PROD_ACC_REFUND = new AgentTransactionNominalCode(20);

	/**
	 * BSp account payment
	 */
	public static final AgentTransactionNominalCode BSP_ACC_SALE = new AgentTransactionNominalCode(12);
	public static final AgentTransactionNominalCode BSP_REFUND = new AgentTransactionNominalCode(13);
	public static final AgentTransactionNominalCode BSP_SETTLEMENT = new AgentTransactionNominalCode(14);
	public static final AgentTransactionNominalCode EXT_PROD_BSP_ACC_SALE = new AgentTransactionNominalCode(21);
	public static final AgentTransactionNominalCode EXT_PROD_BSP_ACC_REFUND = new AgentTransactionNominalCode(22);


	public static final AgentTransactionNominalCode ACC_COMMISSION_GRANT = new AgentTransactionNominalCode(15);
	public static final AgentTransactionNominalCode ACC_COMMISSION_REMOVE = new AgentTransactionNominalCode(16); 
	
	public static final AgentTransactionNominalCode PAY_INV_CC = new AgentTransactionNominalCode(24);

	public static final AgentTransactionNominalCode SUB_AGENT_OPERATIONS = new AgentTransactionNominalCode(23);

	public static AgentTransactionNominalCode getAgentTransactionNominalCode(AgentTransExtNominalCode agentTransExtNominalCode)
			throws ModuleException {

		if (agentTransExtNominalCode.getCode() == AgentTransactionNominalCode.EXT_ACC_SALE.getCode()) {
			return AgentTransactionNominalCode.EXT_ACC_SALE;
		} else if (agentTransExtNominalCode.getCode() == AgentTransactionNominalCode.EXT_ACC_REFUND.getCode()) {
			return AgentTransactionNominalCode.EXT_ACC_REFUND;
		} else {
			throw new ModuleException("airtravelagent.external.agent.code.mismatch");
		}
	}

	public static List<Integer> getExternalProductBSPAccountTypeNominalCodes() {
		List<Integer> nominalCodes = new ArrayList<Integer>();

		nominalCodes.add(AgentTransactionNominalCode.EXT_PROD_BSP_ACC_SALE.getCode());
		nominalCodes.add(AgentTransactionNominalCode.EXT_PROD_BSP_ACC_REFUND.getCode());

		return nominalCodes;
	}
}
