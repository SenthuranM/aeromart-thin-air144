/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;

/**
 * @author : Nilindra Fernando
 * @since 2.0
 */
public class AgentTypeTO implements Serializable {

	private static final long serialVersionUID = 5082734167718965072L;

	public static final String GSA = "GSA";

	private String agentTypeCode;

	private String agentTypeDescription;

	private String creditApplicable;

	/**
	 * returns the agentTypeCode
	 * 
	 * @return Returns the agentTypeCode.
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	/**
	 * sets the agentTypeCode
	 */
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * returns the agentTypeDescription
	 * 
	 * @return Returns the agentTypeDescription.
	 */
	public String getAgentTypeDescription() {
		return agentTypeDescription;
	}

	/**
	 * sets the agentTypeDescription
	 * 
	 * @param agentTypeDescription
	 *            The agentTypeDescription to set.
	 */
	public void setAgentTypeDescription(String agentTypeDescription) {
		this.agentTypeDescription = agentTypeDescription;
	}

	/**
	 * returns the creditApplicable
	 * 
	 * @return Returns the creditApplicable.
	 */
	public String isCreditApplicable() {
		return creditApplicable;
	}

	/**
	 * sets whether creditApplicable is yes or no
	 * 
	 * @param agentTypeDescription
	 *            The creditApplicable to set.
	 */
	public void setCreditApplicable(String creditApplicable) {
		this.creditApplicable = creditApplicable;
	}

}
