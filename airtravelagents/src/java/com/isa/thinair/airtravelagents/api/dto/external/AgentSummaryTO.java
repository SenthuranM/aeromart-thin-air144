/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author : Nilindra Fernando
 * @since : 2.0
 */
public class AgentSummaryTO extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6342201201869476746L;

	private String agentCode;

	private BigDecimal availableCredit;

	private BigDecimal availableFundsForInv;

	private BigDecimal availableAdvance;

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the availableAdvance.
	 */
	public BigDecimal getAvailableAdvance() {
		return availableAdvance;
	}

	/**
	 * @param availableAdvance
	 *            The availableAdvance to set.
	 */
	public void setAvailableAdvance(BigDecimal availableAdvance) {
		this.availableAdvance = availableAdvance;
	}

	/**
	 * @return Returns the availableCredit.
	 */
	public BigDecimal getAvailableCredit() {
		return availableCredit;
	}

	/**
	 * @param availableCredit
	 *            The availableCredit to set.
	 */
	public void setAvailableCredit(BigDecimal availableCredit) {
		this.availableCredit = availableCredit;
	}

	/**
	 * @return Returns the availableFundsForInv.
	 */
	public BigDecimal getAvailableFundsForInv() {
		return availableFundsForInv;
	}

	/**
	 * @param availableFundsForInv
	 *            The availableFundsForInv to set.
	 */
	public void setAvailableFundsForInv(BigDecimal availableFundsForInv) {
		this.availableFundsForInv = availableFundsForInv;
	}
}
