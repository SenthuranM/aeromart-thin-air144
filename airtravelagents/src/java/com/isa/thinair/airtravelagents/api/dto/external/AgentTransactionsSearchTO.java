/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Byorn
 */
public class AgentTransactionsSearchTO implements Serializable {

	private static final long serialVersionUID = -7947972101102816633L;

	private String agentCode;

	private Date tnxFromDate;

	private Date tnxToDate;

	private List<String> adjustmentCarriers;

	private Collection<AgentTransExtNominalCode> agentTransExtNominalCodes;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getTnxFromDate() {
		return tnxFromDate;
	}

	public void setTnxFromDate(Date tnxFromDate) {
		this.tnxFromDate = tnxFromDate;
	}

	public Date getTnxToDate() {
		return tnxToDate;
	}

	public void setTnxToDate(Date tnxToDate) {
		this.tnxToDate = tnxToDate;
	}

	public Collection<AgentTransExtNominalCode> getAgentTransactionNominalCodes() {
		return agentTransExtNominalCodes;
	}

	public void setAgentTransactionNominalCodes(Collection<AgentTransExtNominalCode> agentTransExtNominalCodes) {
		this.agentTransExtNominalCodes = agentTransExtNominalCodes;
	}

	public List<String> getAdjustmentCarriers() {
		if (this.adjustmentCarriers == null) {
			this.adjustmentCarriers = new ArrayList<String>();
		}
		return adjustmentCarriers;
	}

	public void setAdjustmentCarriers(List<String> adjustmentCarriers) {
		this.adjustmentCarriers = adjustmentCarriers;
	}

}