/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.service;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionsSearchTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTAgentPayTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTPaxPayTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap
 * @author Byorn
 * @since 2.0
 */
public interface TravelAgentFinanceBDExternal {

	public static final String SERVICE_NAME = "TravelAgentFinanceService";

	/**
	 * Debit agent account by the amount specified. Operation results in reduction of agent's available credit.
	 * 
	 * @param agentCode
	 * @param amount
	 *            Amount to debit (in default currency)
	 * @param remarks
	 *            Free text remarks
	 * @throws ModuleException
	 */
	public Integer recordSale(String agentCode, BigDecimal amount, String remarks, String pnr, PayCurrencyDTO payCurrencyDTO)
			throws ModuleException;

	/**
	 * Credit agent account by the amount specified. Operation results in increase of agent's available credit.
	 * 
	 * @param agentCode
	 * @param amount
	 *            Amount to credit
	 * @param remarks
	 *            Free-text remarks
	 * @throws ModuleException
	 */
	public AgentTotalSaledDTO recordRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO) throws ModuleException;

	/**
	 * Get Available agent credit amount.
	 * 
	 * @param agentCode
	 * @return Available agent credit amount
	 * @throws ModuleException
	 * 
	 */
	public BigDecimal getAgentAvailableCredit(String agentCode) throws ModuleException;

	/**
	 * Get Agent Transactions for the given nominal codes for the period specified.
	 * 
	 * @param agentTransactionsSearchTO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<AgentTransactionTO> getAgentTransactions(AgentTransactionsSearchTO agentTransactionsSearchTO)
			throws ModuleException;

	/**
	 * Records the External Payment transactions
	 * 
	 * @param colPaxPays
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param pnr
	 * @throws ModuleException
	 */
	@Deprecated
	public void recordExternalAccountPay(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount, String remarks,
			String pnr) throws ModuleException;

	/**
	 * Records the external payment Refunds
	 * 
	 * @param colPaxPays
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param pnr
	 * @throws ModuleException
	 */
	@Deprecated
	public void recordExternalAccountRefund(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount,
			String remarks, String pnr) throws ModuleException;

	/**
	 * Records the External Payment transactions
	 * 
	 * @param extAgentTo
	 * @throws ModuleException
	 */
	public void recordExternalAccountPay(EXTAgentPayTO extAgentTo) throws ModuleException;

	/**
	 * Records the external payment Refunds
	 * 
	 * @param extAgentTo
	 * @throws ModuleException
	 */
	public void recordExternalAccountRefund(EXTAgentPayTO extAgentTo) throws ModuleException;

}
