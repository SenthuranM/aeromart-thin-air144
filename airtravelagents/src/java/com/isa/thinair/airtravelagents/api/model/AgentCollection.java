/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT_COLLECTION" * AgentCollection is the entity class to repesent a AgentCollection
 *                  model
 * 
 */
public class AgentCollection extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5927953032005308760L;

	private int receiptNumber;

	private Date dateOfPayment;

	private BigDecimal paymentAmount;

	private String chequeNumber;

	private String remarks;

	private String status;

	private Date cancelationDate;

	private String cmCode;

	private String cpCode;

	private String agentCode;

	/**
	 * The constructor.
	 */
	public AgentCollection() {
		super();
	}

	/**
	 * returns the receiptNumber
	 * 
	 * @return Returns the receiptNumber.
	 * @hibernate.id column = "RECEIPT_NUMBER" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_COLLECTION_RECEIPT_NO"
	 */
	public int getReceiptNumber() {
		return receiptNumber;
	}

	/**
	 * sets the receiptNumber
	 * 
	 * @param receiptNumber
	 *            The receiptNumber to set.
	 */
	public void setReceiptNumber(int receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	/**
	 * returns the dateOfPayment
	 * 
	 * @return Returns the dateOfPayment.
	 * @hibernate.property column = "DATE_OF_PAYMENT"
	 */
	public Date getDateOfPayment() {
		return dateOfPayment;
	}

	/**
	 * sets the dateOfPayment
	 * 
	 * @param dateOfPayment
	 *            The dateOfPayment to set.
	 */
	public void setDateOfPayment(Date dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}

	/**
	 * returns the paymentAmount
	 * 
	 * @return Returns the paymentAmount.
	 * @hibernate.property column = "PAYMENT_AMOUNT"
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * sets the paymentAmount
	 * 
	 * @param paymentAmount
	 *            The paymentAmount to set.
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * returns the chequeNumber
	 * 
	 * @return Returns the chequeNumber.
	 * @hibernate.property column = "CHEQUE_NUMBER"
	 */
	public String getChequeNumber() {
		return chequeNumber;
	}

	/**
	 * sets the chequeNumber
	 * 
	 * @param chequeNumber
	 *            The chequeNumber to set.
	 */
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the cancelationDate
	 * 
	 * @return Returns the cancelationDate.
	 * @hibernate.property column = "CANCELATION_DATE"
	 */
	public Date getCancelationDate() {
		return cancelationDate;
	}

	/**
	 * sets the cancelationDate
	 * 
	 * @param cancelationDate
	 *            The cancelationDate to set.
	 */
	public void setCancelationDate(Date cancelationDate) {
		this.cancelationDate = cancelationDate;
	}

	/**
	 * @return Returns the agentCode.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the cmId.
	 * @hibernate.property column = "CM_CODE"
	 */
	public String getCmCode() {
		return cmCode;
	}

	/**
	 * @param cmId
	 *            The cmId to set.
	 */
	public void setCmCode(String cmCode) {
		this.cmCode = cmCode;
	}

	/**
	 * @return Returns the cpId.
	 * @hibernate.property column = "CP_CODE"
	 */
	public String getCpCode() {
		return cpCode;
	}

	/**
	 * @param cpId
	 *            The cpId to set.
	 */
	public void setCpCode(String cpCode) {
		this.cpCode = cpCode;
	}
}
