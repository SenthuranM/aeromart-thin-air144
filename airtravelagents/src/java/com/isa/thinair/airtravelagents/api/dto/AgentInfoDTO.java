package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;

/**
 * DTO required to show agent details in IBE
 * 
 * @author lalanthi //JIRA : AARESAA - 2650
 * 
 */
public class AgentInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7649148035124842250L;

	private String agentCode;

	private String agentName;

	private String addressLine1;

	private String addressLine2;

	private String addressCity;

	private String addressStateProvince;

	private String postalCode;

	private String telephone;

	private String fax;

	private String emailId;

	private String status;

	private String stationCode;

	private String stationName;

	private String reportToGSA;

	private long version;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public String getAddressStateProvince() {
		return addressStateProvince;
	}

	public void setAddressStateProvince(String addressStateProvince) {
		this.addressStateProvince = addressStateProvince;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	public String getAgentTypeDesc() {
		return agentTypeDesc;
	}

	public void setAgentTypeDesc(String agentTypeDesc) {
		this.agentTypeDesc = agentTypeDesc;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	private String agentTypeCode;

	private String agentTypeDesc;

	private String billingEmail;

	private String currencyCode;

	private String locationUrl; // JIRA : AARESAA - 3031

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getReportToGSA() {
		return reportToGSA;
	}

	public void setReportToGSA(String reportToGSA) {
		this.reportToGSA = reportToGSA;
	}
}
