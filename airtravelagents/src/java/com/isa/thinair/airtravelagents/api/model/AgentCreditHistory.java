/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT_CREDIT_HISTORY" * AgentCreditHistory is the entity class to repesent a
 *                  AgentCreditHistory model
 * 
 */
public class AgentCreditHistory extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1393860432931713958L;

	private int agnCrHisId;

	private BigDecimal creditLimitamount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal creditLimitamountLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Date updateDate;

	private String remarks;

	private String specifiedCurrency;

	private String agentCode;

	private String createdBy;

	/**
	 * 
	 */
	public AgentCreditHistory() {
		super();
	}

	/**
	 * returns the agnCrHisId
	 * 
	 * @return Returns the agnCrHisId.
	 * @hibernate.id column = "AGN_CR_HIS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_CREDIT_HISTORY"
	 */
	public int getAgnCrHisId() {
		return agnCrHisId;
	}

	/**
	 * sets the agnCrHisId
	 * 
	 * @param agnCrHisId
	 *            The agnCrHisId to set.
	 */
	public void setAgnCrHisId(int agnCrHisId) {
		this.agnCrHisId = agnCrHisId;
	}

	/**
	 * returns the creditLimitamount
	 * 
	 * @return Returns the creditLimitamount.
	 * @hibernate.property column = "CREDIT_LIMITAMOUNT"
	 */
	public BigDecimal getCreditLimitamount() {
		return creditLimitamount;
	}

	/**
	 * sets the creditLimitamount
	 * 
	 * @param creditLimitamount
	 *            The creditLimitamount to set.
	 */
	public void setCreditLimitamount(BigDecimal creditLimitamount) {
		this.creditLimitamount = creditLimitamount;
	}

	/**
	 * returns the updateDate
	 * 
	 * @return Returns the updateDate.
	 * @hibernate.property column = "UPDATE_DATE"
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * sets the updateDate
	 * 
	 * @param updateDate
	 *            The updateDate to set.
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the agent code.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Return the agent code.
	 * 
	 * @param agentCode
	 *            the agent code.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the created by user id.
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * The created by user id.
	 * 
	 * @param createdBy
	 *            the created by user id..
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * returns the creditLimitamount
	 * 
	 * @return Returns the creditLimitamount.
	 * @hibernate.property column = "CREDIT_LIMITAMOUNT_LOCAL"
	 */
	public BigDecimal getCreditLimitamountLocal() {
		return creditLimitamountLocal;
	}

	public void setCreditLimitamountLocal(BigDecimal creditLimitamountLocal) {
		this.creditLimitamountLocal = creditLimitamountLocal;
	}

	/**
	 * @return the specifiedCurrency
	 * @hibernate.property column = "SPECIFIED_CUR_CODE"
	 */
	public String getSpecifiedCurrency() {
		return specifiedCurrency;
	}

	/**
	 * @param specifiedCurrency
	 *            the specifiedCurrency to set
	 */
	public void setSpecifiedCurrency(String specifiedCurrency) {
		this.specifiedCurrency = specifiedCurrency;
	}
}
