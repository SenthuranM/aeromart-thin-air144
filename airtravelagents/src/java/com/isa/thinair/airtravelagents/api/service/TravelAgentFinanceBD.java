/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.dto.ExternalAgentRQDTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTransactionsSearchTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTAgentPayTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTPaxPayTO;
import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentCreditBlock;
import com.isa.thinair.airtravelagents.api.model.AgentToken;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap, Byorn
 */
public interface TravelAgentFinanceBD {

	public static final String SERVICE_NAME = "TravelAgentFinanceService";

	/**
	 * 
	 * @param agentCode
	 * @param creditLimit
	 * @param availableCredit
	 * @param agentcollection
	 * @param forceCancel
	 * @throws ModuleException
	 */
	public void postAgentCollection(String agentCode, BigDecimal creditLimit, BigDecimal availableCredit,
			AgentCollection agentcollection, boolean forceCancel) throws ModuleException;

	/**
	 * Debit agent account by the amount specified. Operation results in reduction of agent's available credit.
	 * 
	 * @param agentCode
	 * @param amount
	 *            Amount to debit (in default currency)
	 * @param remarks
	 *            Free text remarks
	 * @param productType
	 *            TODO
	 * @param orderId
	 *            TODO
	 * @throws ModuleException
	 */
	public AgentTransaction recordSale(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO PayCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType, String orderId)
			throws ModuleException;

	/**
	 * Credit agent account by the amount specified. Operation results in increase of agent's available credit.
	 * 
	 * @param agentCode
	 * @param amount
	 *            Amount to credit
	 * @param remarks
	 *            Free-text remarks
	 * @param productType
	 *            TODO
	 * @throws ModuleException
	 */
	public AgentTotalSaledDTO recordRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO PayCurrencyDTO, PaymentReferenceTO PaymentReferenceTO, Character productType) throws ModuleException;

	/**
	 * if (-) throw excp.. if (+) make it minus and accept it. with payment nominal code depeding on type. CR. inv 0.
	 * today.
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param strCarrier
	 *            TODO
	 * @param paymentType
	 * @throws ModuleException
	 *             avl credit increases
	 * 
	 */
	public void recordPayment(String agentCode, BigDecimal amount, String remarks, CollectionTypeEnum collectionTypeEnum,
			String amountSpecifiedIn, String strCarrier, String strReceiptNumber) throws ModuleException;

	/**
	 * Taken as a debit tnx..and from the pool values will be reduced.
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param strCarrier
	 *            TODO
	 * @throws ModuleException
	 */
	public void recordPaymentReverse(String agentCode, BigDecimal amount, String remarks, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber) throws ModuleException;

	/**
	 * if (-) throw excp.. if (+) make it minus and accept it. with payment nominal code . DR. inv 0. today.
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param strCarrier
	 *            TODO
	 * @throws ModuleException
	 * 
	 */
	public void recordDebit(String agentCode, BigDecimal amount, String remarks, String pnr, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber) throws ModuleException;

	/**
	 * if (-) throw excp.. if (+) make it minus and accept it. with record credit nominal code . CR. inv 0. today.
	 * 
	 * @param agentCode
	 * @param amount
	 * @param remarks
	 * @param strCarrier
	 *            TODO
	 * @throws ModuleException
	 *             avl credit should be increaseing in sumury.
	 */
	public void recordCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber) throws ModuleException;

	/**
	 * Method to get Agent Transactions for the given nominal codes.
	 * 
	 * @param agentTransactionsDTO
	 * @return Collection of AgentTransaction objects
	 * @throws ModuleException
	 */
	public Collection<AgentTransaction> getAgentTransactions(AgentTransactionsDTO agentTransactionsDTO) throws ModuleException;

	/**
	 * Method to get a given AgentTransaction.
	 * 
	 * @param receiptNumber
	 *            receiptNumber.
	 * @return the agentTransaction.
	 */
	public AgentTransaction getAgentTransaction(String receiptNumber) throws ModuleException;

	/**
	 * Get Available agent credit amount.
	 * 
	 * @param agentCode
	 * @return Available agent credit amount
	 * @throws ModuleException
	 */
	public BigDecimal getAgentAvailableCredit(String agentCode) throws ModuleException;

	public void updateAgentSummarySettle(String agentCode, BigDecimal amount) throws ModuleException;

	public void recordBSPCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber) throws ModuleException;

	/**
	 * Send an email to the agent and airline accounts department when bank guarantee expire date reaches given values
	 * 
	 * @throws ModuleException
	 */
	public void sendBankGuaranteeNotificationEmail() throws ModuleException;

	/**
	 * Send an email to the agent and airline accounts department when utilization reaches given values
	 * 
	 * @throws ModuleException
	 */
	public void sendCreditLimitUtilizationEmail() throws ModuleException;

	/**
	 * Send and email to predefined email address(s) when utilization reaches given value(s)
	 * 
	 * @throws ModuleException
	 */
	public void sendBSPCreditLimitUtilizationEmail() throws ModuleException;

	public Collection<AgentTransactionTO> getAgentTransactions(AgentTransactionsSearchTO agentTransactionsSearchTO)
			throws ModuleException;

	public void recordExternalAccountPay(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount, String remarks,
			String pnr) throws ModuleException;

	public void recordExternalAccountRefund(Collection<EXTPaxPayTO> colPaxPays, String agentCode, BigDecimal amount,
			String remarks, String pnr) throws ModuleException;

	public void recordExternalAccountPay(EXTAgentPayTO extAgentTo) throws ModuleException;

	public void recordExternalAccountRefund(EXTAgentPayTO extAgentTo) throws ModuleException;

	public AgentTransaction recordAgentCommission(String agentCode, BigDecimal amount, String remarks, String pnr,
			String amountSpecifiedIn) throws ModuleException;

	public AgentTransaction removeAgentCommission(String agentCode, String userId, Integer salesChannelCode, BigDecimal amount,
			String remarks, String pnr, String amountSpecifiedIn) throws ModuleException;

	public BigDecimal getPendingAgentCommissions(String agentCode, String pnr) throws ModuleException;

	public BigDecimal getTotalOfferedAgentCommissions(String agentCode, Date periodStartDate, Date periodEndDate)
			throws ModuleException;

	public List<AgentTransaction> isDuplicateRecordExistForAgent(String agentCode, BigDecimal amount, int nominalCode)
			throws ModuleException;

	public void recordCredit(String agentCode, BigDecimal amount, String pnr, int nominalCode, Character productType)
			throws ModuleException;

	public void recordDebit(String agentCode, BigDecimal amount, boolean force, String pnr, int nominalCode)
			throws ModuleException;

	public Integer blockAgentCredit(AgentCreditBlock agentCreditBlock) throws ModuleException;

	public AgentTransaction confirmBlockedCredit(AgentToken agentToken, ExternalAgentRQDTO confirmRQ,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException;

	public AgentTransaction externalPayment(AgentToken agentToken, ExternalAgentRQDTO confirmRQ, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO) throws ModuleException;

	public AgentTransaction recordExternalProductSale(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO, Character productType, String orderId)
			throws ModuleException;

	public void updateAgentCreditBlock(AgentCreditBlock agentCreditBlock) throws ModuleException;

	public AgentCreditBlock getAgentCreditBlock(Integer blockId, String status) throws ModuleException;

	public boolean isExistingExternalReference(String extRef);

	public AgentTotalSaledDTO recordExternalProductRefund(String agentCode, BigDecimal amount, String remarks, String pnr,
			PayCurrencyDTO PayCurrencyDTO, PaymentReferenceTO PaymentReferenceTO, Character productType) throws ModuleException;

	public void recordSubAgentOperationDebit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currency,
			String userId, Integer salesChannelCode, String subAgentOperation) throws ModuleException;

	public void recordSubAgentOperationCredit(String agentCode, BigDecimal amount, BigDecimal amountInLocal, String currency,
			String userId, Integer salesChannelCode, String subAgentOperation) throws ModuleException;
	
	public void reverseCredit(String agentCode, BigDecimal amount, String remarks, String pnr, String amountSpecifieidIn,
			String strCarrier, String strReceiptNumber, String originalTnxId) throws ModuleException;
	
	public Integer makeTemporyPaymentEntry(ReservationContactInfo reservationContactInfo, PaymentInfo paymentInfo,
			TrackInfoDTO trackInfoDTO, boolean isCredit, boolean isOfflinePayment) throws ModuleException;
	
	public void agentTopupEmailNotification(String amount,String agentCode) throws ModuleException;

	public boolean recordselfTopup(String strAgentCode, BigDecimal amount, String strRemarks, CollectionTypeEnum creditcard,
			String strCurrency, String strCarrier, String string) throws ModuleException;
}
