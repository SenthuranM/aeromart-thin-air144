package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;

/**
 * @author eric
 * 
 */
public class AgentTicketStockTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long agentTicketSeqId;

	private String agentCode;

	private long sequenceUpperBound;

	private long sequenceLowerBound;

	private long stockSize;

	private String sequenceName;

	private String status;

	private String cycleSequence;

	private long version = -1;;

	public long getAgentTicketSeqId() {
		return agentTicketSeqId;
	}

	public void setAgentTicketSeqId(long agentTicketSeqId) {
		this.agentTicketSeqId = agentTicketSeqId;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public long getSequenceUpperBound() {
		return sequenceUpperBound;
	}

	public void setSequenceUpperBound(long sequenceUpperBound) {
		this.sequenceUpperBound = sequenceUpperBound;
	}

	public long getSequenceLowerBound() {
		return sequenceLowerBound;
	}

	public void setSequenceLowerBound(long sequenceLowerBound) {
		this.sequenceLowerBound = sequenceLowerBound;
	}

	public String getSequenceName() {
		return sequenceName;
	}

	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}

	public long getStockSize() {
		return stockSize;
	}

	public void setStockSize(long stockSize) {
		this.stockSize = stockSize;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCycleSequence() {
		return cycleSequence;
	}

	public void setCycleSequence(String cycleSequence) {
		this.cycleSequence = cycleSequence;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

}
