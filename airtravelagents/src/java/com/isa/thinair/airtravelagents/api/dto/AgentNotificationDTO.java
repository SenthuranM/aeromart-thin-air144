package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;

public class AgentNotificationDTO implements Serializable {

	private static final long serialVersionUID = -7649148475125842250L;

	private String agentCode;
	private String agentName;
	private String billingEmail;
	private Integer noOfDaysToExpire;
	private Double utilizationPercentage;
	private Double checkedPercentage;

	// BSP notification related params
	private String agentIATACode;
	private String totalCreditLimit;
	private String availableCreditLimit;
	private int notificationLevel;

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the billingEmail
	 */
	public String getBillingEmail() {
		return billingEmail;
	}

	/**
	 * @param billingEmail
	 *            the billingEmail to set
	 */
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	/**
	 * @return the noOfDaysToExpire
	 */
	public Integer getNoOfDaysToExpire() {
		return noOfDaysToExpire;
	}

	/**
	 * @param noOfDaysToExpire
	 *            the noOfDaysToExpire to set
	 */
	public void setNoOfDaysToExpire(Integer noOfDaysToExpire) {
		this.noOfDaysToExpire = noOfDaysToExpire;
	}

	/**
	 * @return the utilizationPercentage
	 */
	public Double getUtilizationPercentage() {
		return utilizationPercentage;
	}

	/**
	 * @param utilizationPercentage
	 *            the utilizationPercentage to set
	 */
	public void setUtilizationPercentage(Double utilizationPercentage) {
		this.utilizationPercentage = utilizationPercentage;
	}
	
	/**
	 * @return the checkedPercentage
	 */
	public Double getCheckedPercentage() {
		return checkedPercentage;
	}

	/**
	 * @param checkedPercentage
	 *            the checkedPercentage to set
	 */
	public void setCheckedPercentage(Double checkedPercentage) {
		this.checkedPercentage = checkedPercentage;
	}

	/**
	 * @return the agentIATACode
	 */
	public String getAgentIATACode() {
		return agentIATACode;
	}

	/**
	 * @param agentIATACode
	 *            the agentIATACode to set
	 */
	public void setAgentIATACode(String agentIATACode) {
		this.agentIATACode = agentIATACode;
	}

	/**
	 * @return the totalCreditLimit
	 */
	public String getTotalCreditLimit() {
		return totalCreditLimit;
	}

	/**
	 * @param totalCreditLimit
	 *            the totalCreditLimit to set
	 */
	public void setTotalCreditLimit(String totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}

	/**
	 * @return the availableCreditLimit
	 */
	public String getAvailableCreditLimit() {
		return availableCreditLimit;
	}

	/**
	 * @param availableCreditLimit
	 *            the availableCreditLimit to set
	 */
	public void setAvailableCreditLimit(String availableCreditLimit) {
		this.availableCreditLimit = availableCreditLimit;
	}

	/**
	 * @return the notificationLevel
	 */
	public int getNotificationLevel() {
		return notificationLevel;
	}

	/**
	 * @param notificationLevel
	 *            the notificationLevel to set
	 */
	public void setNotificationLevel(int notificationLevel) {
		this.notificationLevel = notificationLevel;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AgentNotificationDTO)) {
			return false;
		}
		AgentNotificationDTO object = (AgentNotificationDTO) obj;
		if (this.getAgentCode() == null) {
			if (object.getAgentCode() != null) {
				return false;
			}
		} else if (!(this.getAgentCode().equals(object.getAgentCode()))) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hashCode = 1;
		hashCode = prime * hashCode + ((this.getAgentCode() == null) ? 0 : this.getAgentCode().hashCode());
		hashCode = prime * hashCode + ((this.getBillingEmail() == null) ? 0 : this.getBillingEmail().hashCode());
		return hashCode;
	}
}
