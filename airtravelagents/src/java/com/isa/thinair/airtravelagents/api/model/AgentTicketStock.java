/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_AGENT_TICKET_STOCK" AgentTicketStock is the entity class to represent a AgentTicketStock
 *                  model
 * 
 */
public class AgentTicketStock extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int agentStockId;

	private String seriesFrom;

	private String seriesTo;

	private Date addedDate;

	private String remarks;

	private String agentCode;

	private String createdBy;

	private Long stockQty;

	private Long allocation;

	private String eTicket;

	/**
	 * 
	 */
	public AgentTicketStock() {
		super();
	}

	/**
	 * returns the agentStockId
	 * 
	 * @return Returns the agentStockId.
	 * @hibernate.id column = "AGN_TKT_STOCK_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_TICKET_STOCK"
	 */
	public int getAgentStockId() {
		return agentStockId;
	}

	/**
	 * sets the agentStockId
	 * 
	 * @param agentStockId
	 *            The agentStockId to set.
	 */
	public void setAgentStockId(int agentStockId) {
		this.agentStockId = agentStockId;
	}

	/**
	 * returns the seriesFrom
	 * 
	 * @return Returns the seriesFrom.
	 * @hibernate.property column = "SERIES_FROM"
	 */
	public String getSeriesFrom() {
		return seriesFrom;
	}

	/**
	 * sets the seriesFrom
	 * 
	 * @param seriesFrom
	 *            The seriesFrom to set.
	 */
	public void setSeriesFrom(String seriesFrom) {
		this.seriesFrom = seriesFrom;
	}

	/**
	 * returns the addedDate
	 * 
	 * @return Returns the addedDate.
	 * @hibernate.property column = "ADDED_DATE"
	 */
	public Date getAddedDate() {
		return addedDate;
	}

	/**
	 * sets the addedDate
	 * 
	 * @param addedDate
	 *            The addedDate to set.
	 */
	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the agent code.
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Return the agent code.
	 * 
	 * @param agentCode
	 *            the agent code.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the created by user id.
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * The created by user id.
	 * 
	 * @param createdBy
	 *            the created by user id..
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * returns the seriesTo
	 * 
	 * @return Returns the seriesTo.
	 * @hibernate.property column = "SERIES_TO"
	 */
	public String getSeriesTo() {
		return seriesTo;
	}

	public void setSeriesTo(String seriesTo) {
		this.seriesTo = seriesTo;
	}

	/**
	 * returns the Allocation
	 * 
	 * @return Returns the Allocation.
	 * @hibernate.property column = "ALLOCATION"
	 */
	public Long getAllocation() {
		return allocation;
	}

	/**
	 * Sets the allocation.
	 * 
	 * @param allocation
	 *            the new allocation
	 */
	public void setAllocation(Long allocation) {
		this.allocation = allocation;
	}

	public Long getStockQty() {
		return stockQty;
	}

	public void setStockQty(Long stockQty) {
		this.stockQty = stockQty;
	}

	public String getETicket() {
		return eTicket;
	}

	public void setETicket(String ticket) {
		eTicket = ticket;
	}

}
