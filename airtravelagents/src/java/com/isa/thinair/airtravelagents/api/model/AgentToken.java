package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author manoji
 * @hibernate.class table = "T_AGENT_TOKEN"
 */
public class AgentToken extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;
	private int tokenId;
	private String token;
	private String agentCode;
	private String userName;
	private Date tokenIssueTimeStamp;
	private Date tokenExpiryTimeStamp;
	private String expired;
	private Agent agent;

	public static final String TRUE = "Y";
	public static final String FALSE = "N";

	/**
	 * @return
	 * @hibernate.id column = "AGENT_TOKEN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_TOKEN"
	 */
	public int getTokenId() {
		return tokenId;
	}

	/**
	 * @hibernate.property column = "TOKEN"
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @hibernate.property column = "USER_NAME"
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @hibernate.property column = "TOKEN_ISSUE_TIMESTAMP"
	 */
	public Date getTokenIssueTimeStamp() {
		return tokenIssueTimeStamp;
	}

	/**
	 * @hibernate.property column = "TOKEN_EXPIRY_TIMESTAMP"
	 */
	public Date getTokenExpiryTimeStamp() {
		return tokenExpiryTimeStamp;
	}

	/**
	 * @hibernate.property column = "IS_EXPIRED"
	 */
	public String getExpired() {
		return expired;
	}

	public void setExpired(String expired) {
		this.expired = expired;
	}

	public void setTokenId(int tokenId) {
		this.tokenId = tokenId;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setTokenIssueTimeStamp(Date tokenIssueTimeStamp) {
		this.tokenIssueTimeStamp = tokenIssueTimeStamp;
	}

	public void setTokenExpiryTimeStamp(Date tokenExpiryTimeStamp) {
		this.tokenExpiryTimeStamp = tokenExpiryTimeStamp;
	}

	/**
	 * @return Returns the agent.
	 * @hibernate.many-to-one class="com.isa.thinair.airtravelagents.api.model.Agent" column="AGENT_CODE" insert="false"
	 *                        update="false"
	 */

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public boolean isExpiredToken() {
		if (CalendarUtil.getCurrentZuluDateTime().compareTo(getTokenExpiryTimeStamp()) >= 0) {
			return true;
		}
		return false;
	}

}
