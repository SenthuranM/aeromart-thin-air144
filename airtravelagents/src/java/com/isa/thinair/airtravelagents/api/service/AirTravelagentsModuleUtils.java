/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.service;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.airtravelagents.core.config.AirTravelAgentsConfig;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentIncDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.ExternalTransactionDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.TravelAgentJdbcDAO;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;

/**
 * @author Chamindap, Byorn
 * 
 */
public class AirTravelagentsModuleUtils {

	public static final Log log = LogFactory.getLog(AirTravelagentsModuleUtils.class);

	public static String AGENT_COLLECTIONDAO = "agentCollectionDAO";

	public static String AGENT_JDBC_DAO = "agentJdbcDAO";

	public static String RESERVATION_TNX_DAO = "ReservationTnxDAO";

	public static String AGENT_TNX_DAO = "agentTransactionDAO";

	public static String EXT_TNX_DAO = "externalTransactionDAO";

	/**
	 * Private to get AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	public static AgentCollectionDAO getAgentCollectionDAO() {
		return (AgentCollectionDAO) AirTravelagentsModuleUtils.getDAO(AGENT_COLLECTIONDAO);
	}

	/**
	 * Private to get AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	public static AgentTransactionDAO getAgentTransactionDAO() {
		return (AgentTransactionDAO) getDAO(AGENT_TNX_DAO);
	}

	/**
	 * Private to get AgentCollectionDAO.
	 * 
	 * @return getAgentCollectionDAO.
	 */
	public static ExternalTransactionDAO getExternalTransactionDAO() {
		return (ExternalTransactionDAO) getDAO(EXT_TNX_DAO);
	}

	/**
	 * Returns the IModule interface
	 * 
	 * @return the IModule interface
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(AirTravelAgentConstants.MODULE_NAME);
	}

	/**
	 * Returns the AgentDAO reference.
	 * 
	 * @return AgentDAO reference.
	 */
	public static AgentDAO getAgentDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AgentDAO) lookupService.getBean("isa:base://modules/" + "airtravelagents?id=agentDAOProxy");
	}

	/**
	 * Gets the dao
	 * 
	 * @param module
	 * @param daoModule
	 * @return
	 */
	public static Object getDAO(String daoModule) {
		String beanUri = null;

		beanUri = "isa:base://modules/airtravelagents?id=" + daoModule + "Proxy";

		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(beanUri);
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirTravelAgentConfig(),
				"airtravelagent", "airtravelagent.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirTravelAgentConfig(), "airtravelagent",
				"airtravelagent.config.dependencymap.invalid");
	}

	public static AirTravelAgentsConfig getAirTravelAgentConfig() {
		return (AirTravelAgentsConfig) getInstance().getModuleConfig();
	}

	public static InvoicingBD getInvoicingBD() {
		return (InvoicingBD) lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Return transparent location class delegate
	 * 
	 * @return
	 */
	public static LocationBD getLocationBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/**
	 * Return transparent location class delegate
	 * 
	 * @return
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static ReportingFrameworkBD getReportingFrameworkBD() {
		return (ReportingFrameworkBD) lookupServiceBD(ReportingframeworkConstants.MODULE_NAME,
				ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	public static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, DataExtractionBD.SERVICE_NAME);
	}

	/**
	 * Returns the external data source
	 * 
	 * @return
	 */
	public static XDBConnectionUtil getExternalDBConnectionUtil() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=myXDBDataSourceUtil");
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static TravelAgentJdbcDAO getAgentJdbcDAO() {
		return (TravelAgentJdbcDAO) getInstance().getLocalBean(AGENT_JDBC_DAO);
	}

	/**
	 * Returns the AgentIncentiveDAO reference.
	 * 
	 * @return AgentIncentiveDAO reference.
	 */
	public static AgentIncDAO getAgentIncentiveDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AgentIncDAO) lookupService.getBean("isa:base://modules/airtravelagents?id=agentIncDAOProxy");
	}
	
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
}
