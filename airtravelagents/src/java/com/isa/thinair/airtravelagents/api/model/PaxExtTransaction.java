/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * This is used for Airarabia Holidays interfacing.
 * 
 * @author Thushara
 * @hibernate.class table = "T_PAX_EXT_TRANSACTIONS" External Transaction is the entity class to repesent a pax external
 *                  transaction
 * 
 */
public class PaxExtTransaction extends Persistent implements Serializable {

	private static final long serialVersionUID = -1400222275913491640L;

	private int paxExtTnxId;

	private int pnrPaxId;

	private int agentTnxId;

	private int nominalCode;

	private String drOrcr;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Date transctionDate;

	private String userId;

	private Integer salesChannelCode;

	private BigDecimal amountLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String currencyCode;

	private String creditSharedAgent;

	/**
	 * The constructor.
	 */
	public PaxExtTransaction() {
		super();
	}

	/**
	 * returns the External Transaction id.
	 * 
	 * @return Returns the paxExtTnxId.
	 * @hibernate.id column = "PAX_EXT_TXN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PAX_EXT_TRANSACTIONS"
	 */
	public int getPaxExtTnxId() {
		return paxExtTnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setPaxExtTnxId(int paxExtTnxId) {
		this.paxExtTnxId = paxExtTnxId;
	}

	/**
	 * @return Returns the pnrPaxId.
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public int getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(int pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the Agent Transaction Id.
	 * @hibernate.property column = "TXN_ID"
	 */
	public int getAgentTnxId() {
		return agentTnxId;
	}

	/**
	 * @param agentTnxId
	 *            The agentTnxId to set.
	 */
	public void setAgentTnxId(int agentTnxId) {
		this.agentTnxId = agentTnxId;
	}

	/**
	 * @return Returns the drOrcr.
	 * @hibernate.property column = "CR_DR"
	 */
	public String getDrOrcr() {
		return drOrcr;
	}

	/**
	 * @param drOrcr
	 *            The drOrcr to set.
	 */
	public void setDrOrcr(String drOrcr) {
		this.drOrcr = drOrcr;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the transctionDate.
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTransctionDate() {
		return transctionDate;
	}

	/**
	 * @param transctionDate
	 *            The transctionDate to set.
	 */
	public void setTransctionDate(Date transctionDate) {
		this.transctionDate = transctionDate;
	}

	/**
	 * @return Returns the nominalCode.
	 * @hibernate.property column = "NOMINAL_CODE"
	 */
	public int getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            The nominalCode to set.
	 */
	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the local amount.
	 * @hibernate.property column = "AMOUNT_LOCAL"
	 */
	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	/**
	 * @param amountLocal
	 *            The amount Local to set.
	 */
	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	/**
	 * @return Returns the currency code.
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencycode
	 *            The currency code to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the creditSharedAgent
	 * @hibernate.property column = "DEBIT_CREDIT_AGENT"
	 */
	public String getCreditSharedAgent() {
		return creditSharedAgent;
	}

	/**
	 * @param creditSharedAgent
	 *            the creditSharedAgent to set
	 */
	public void setCreditSharedAgent(String creditSharedAgent) {
		this.creditSharedAgent = creditSharedAgent;
	}

}