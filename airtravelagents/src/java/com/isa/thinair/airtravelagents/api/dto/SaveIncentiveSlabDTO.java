package com.isa.thinair.airtravelagents.api.dto;

import java.io.Serializable;

/**
 * @author harshaa
 * 
 */
public class SaveIncentiveSlabDTO implements Serializable {

	private static final long serialVersionUID = -2056162131683062178L;

	private String agentIncSlabID;
	private String agentIncSchemeID;
	private String rateStartValue;
	private String rateEndValue;
	private String persentage;
	private long version;

	public String getAgentIncSlabID() {
		return agentIncSlabID;
	}

	public void setAgentIncSlabID(String agentIncSlabID) {
		this.agentIncSlabID = agentIncSlabID;
	}

	public String getAgentIncSchemeID() {
		return agentIncSchemeID;
	}

	public void setAgentIncSchemeID(String agentIncSchemeID) {
		this.agentIncSchemeID = agentIncSchemeID;
	}

	public String getRateStartValue() {
		return rateStartValue;
	}

	public void setRateStartValue(String rateStartValue) {
		this.rateStartValue = rateStartValue;
	}

	public String getRateEndValue() {
		return rateEndValue;
	}

	public void setRateEndValue(String rateEndValue) {
		this.rateEndValue = rateEndValue;
	}

	public String getPersentage() {
		return persentage;
	}

	public void setPersentage(String persentage) {
		this.persentage = persentage;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

}
