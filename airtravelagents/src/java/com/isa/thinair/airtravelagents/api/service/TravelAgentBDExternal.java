/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.service;

import java.util.List;

import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.airtravelagents.api.dto.external.EXTAgentPayTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public interface TravelAgentBDExternal {

	public static final String SERVICE_NAME = "TravelAgentExternalService";

	/**
	 * Get AgentTO for agentID
	 * 
	 * @param agnetId
	 * @return
	 * @throws ModuleException
	 */
	public AgentTO getAgentTO(String agentId) throws ModuleException;

	/**
	 * Search AgentTO(s)
	 * 
	 * @param criterion
	 * @param startIndex
	 * @param pageSize
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	public Page searchAgentTOs(List<ModuleCriterion> criterion, int startIndex, int pageSize, List<String> orderByFieldList)
			throws ModuleException;

	/**
	 * Records the External Payment transactions
	 * 
	 * @param extAgentTo
	 * @throws ModuleException
	 */
	public void recordExternalAccountPay(EXTAgentPayTO extAgentTo) throws ModuleException;

	/**
	 * Records the external payment Refunds
	 * 
	 * @param extAgentTo
	 * @throws ModuleException
	 */
	public void recordExternalAccountRefund(EXTAgentPayTO extAgentTo) throws ModuleException;

}
