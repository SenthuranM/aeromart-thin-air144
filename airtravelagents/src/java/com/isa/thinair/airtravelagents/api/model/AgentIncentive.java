/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * <b>Entity class to handle Agent Incentive</b>
 * 
 * @author dumindag
 * 
 * @hibernate.class table = "T_AGENT_INCENTIVE_SCHEME"
 * 
 */

public class AgentIncentive extends Tracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2922058252352833103L;

	private Integer agentIncSchemeID;

	private String schemeName;

	private String status;

	private Date effectiveFrom;

	private Date effectiveTo;

	private String comments;

	private char ticketPriceComponent;

	private char incPromoBookings;

	private char incOwnTRFBookings;

	private char incRepAgentSales;

	private char incentiveBasis;

	private Set<AgentIncentiveSlab> agentIncentiveSlabs;

	public interface Status {

		public static final String ACTIVE = "ACT";

		public static final String INACTIVE = "INA";
	}

	/**
	 * @return Returns the agentIncScemeID.
	 * @hibernate.id column ="AGENT_INCENTIVE_SCHEME_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_INCENTIVE_SCHEME"
	 */
	public Integer getAgentIncSchemeID() {
		return agentIncSchemeID;
	}

	/**
	 * @param agentIncScemeID
	 *            The agentIncScemeID to set.
	 */
	public void setAgentIncSchemeID(Integer agentIncScemeID) {
		this.agentIncSchemeID = agentIncScemeID;
	}

	/**
	 * @return Returns the comments.
	 * @hibernate.property column = "COMMENTS"
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return Returns the effectiveFrom.
	 * @hibernate.property column = "EFFECTIVE_FROM_DATE"
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom
	 *            The effectiveFrom to set.
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return Returns the effectiveTo.
	 * @hibernate.property column = "EFFECTIVE_TO_DATE"
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @param effectiveTo
	 *            The effectiveTo to set.
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return Returns the incentiveBasis.
	 * @hibernate.property column = "INCENTIVE_BASIS"
	 */
	public char getIncentiveBasis() {
		return incentiveBasis;
	}

	/**
	 * @param incentiveBasis
	 *            The incentiveBasis to set.
	 */
	public void setIncentiveBasis(char incentiveBasis) {
		this.incentiveBasis = incentiveBasis;
	}

	/**
	 * @return Returns the incOwnTRFBookings.
	 * @hibernate.property column = "INC_OWN_TRF_BOOKINGS"
	 */
	public char getIncOwnTRFBookings() {
		return incOwnTRFBookings;
	}

	/**
	 * @param incOwnTRFBookings
	 *            The incOwnTRFBookings to set.
	 */
	public void setIncOwnTRFBookings(char incOwnTRFBookings) {
		this.incOwnTRFBookings = incOwnTRFBookings;
	}

	/**
	 * @return Returns the incPromoBookings.
	 * @hibernate.property column = "INC_PROMO_BOOKINGS"
	 */
	public char getIncPromoBookings() {
		return incPromoBookings;
	}

	/**
	 * @param incPromoBookings
	 *            The incPromoBookings to set.
	 */
	public void setIncPromoBookings(char incPromoBookings) {
		this.incPromoBookings = incPromoBookings;
	}

	/**
	 * @return Returns the incRepAgentSales.
	 * @hibernate.property column = "INC_RPT_AGENT_SALES"
	 */
	public char getIncRepAgentSales() {
		return incRepAgentSales;
	}

	/**
	 * @param incRepAgentSales
	 *            The incRepAgentSales to set.
	 */
	public void setIncRepAgentSales(char incRepAgentSales) {
		this.incRepAgentSales = incRepAgentSales;
	}

	/**
	 * @return Returns the schemeName.
	 * @hibernate.property column = "SCHEME_NAME"
	 */
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName
	 *            The schemeName to set.
	 */
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the ticketPriceComponent.
	 * @hibernate.property column = "TICKET_PRICE_COMPONENT"
	 */
	public char getTicketPriceComponent() {
		return ticketPriceComponent;
	}

	/**
	 * @param ticketPriceComponent
	 *            The ticketPriceComponent to set.
	 */
	public void setTicketPriceComponent(char ticketPriceComponent) {
		this.ticketPriceComponent = ticketPriceComponent;
	}

	/**
	 * @return Returns the agentIncentiveSlabs.
	 * @hibernate.set lazy="false" cascade="all" order-by="RANGE_START_VALUE"
	 * @hibernate.collection-key column="AGENT_INCENTIVE_SCHEME_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab"
	 */
	public Set<AgentIncentiveSlab> getAgentIncentiveSlabs() {
		return agentIncentiveSlabs;
	}

	/**
	 * @param agentIncentiveSlabs
	 *            The agentIncentiveSlabs to set.
	 */
	public void setAgentIncentiveSlabs(Set<AgentIncentiveSlab> agentIncentiveSlabs) {
		this.agentIncentiveSlabs = agentIncentiveSlabs;
	}

}