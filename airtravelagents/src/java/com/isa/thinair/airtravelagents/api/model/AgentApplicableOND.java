package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;

/**
 * 
 * @author manoji
 * @hibernate.class table = "T_AGENT_APPLICABLE_OND"
 */

public class AgentApplicableOND implements Serializable {

	private Integer agentAppicableOndId;

	private Agent agent;

	private String ondCOde;

	/**
	 * @return
	 * @hibernate.id column = "AGENT_APP_OND_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_APPLICABLE_OND"
	 */
	public Integer getAgentAppicableOndId() {
		return agentAppicableOndId;
	}

	public void setAgentAppicableOndId(Integer agentAppicableOndId) {
		this.agentAppicableOndId = agentAppicableOndId;
	}

	/**
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCOde() {
		return ondCOde;
	}

	public void setOndCOde(String ondCOde) {
		this.ondCOde = ondCOde;
	}

	/**
	 * @return the meal
	 * @hibernate.many-to-one column="AGENT_CODE" class="com.isa.thinair.airtravelagents.api.model.Agent"
	 * 
	 */
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

}
