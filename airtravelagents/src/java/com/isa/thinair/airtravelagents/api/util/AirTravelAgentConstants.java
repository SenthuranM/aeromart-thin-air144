/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */

package com.isa.thinair.airtravelagents.api.util;

/**
 * To keep track of Travel Agent Constants.
 * 
 * @author ChamindaP
 * @since 1.0
 */
public final class AirTravelAgentConstants {

	/** Denotes Transaction Types */
	public static interface TnxTypes {
		/** Denotes credit transaction type */
		public static final String CREDIT = "CR";

		/** Denotes debit transaction type */
		public static final String DEBIT = "DR";
	}

	public static final String MODULE_NAME = "airtravelagents";
	public static final String REVERSED = "R";
	public static final String AMOUNT = "amt";
	public static final String TRANSFER_TO_EXTERNAL_SYSTEM = "true";
	public static final String X_EXTERNAL_SYSTEM = "X";
	public static final String X3_EXTERNAL_SYSTEM = "X3";

	/** Denotes Agent Types */
	public static interface AgentTypes {
		public static final String GSA = "GSA";
		public static final String TRAVEL_AGENT = "TA";
		public static final String COORPORATE_AGENT = "CO";
		public static final String SALES_OUTLET_AGENT = "SO";
	}

	public static interface EmailStatus {

		public static final String NOT_SENT = "NO";
		public static final String SENT = "YES";
		public static final String PROCESSING = "PRO";

	}

	public static interface PaymentSource {

		public static final int AAHolidays = 8;

	}

	public static interface AgentCommissionStatus {
		public static final String CONFIRMED = "CNF";
		public static final String PENDING = "PND";
		public static final String CANCELED = "CNX";
	}
}
