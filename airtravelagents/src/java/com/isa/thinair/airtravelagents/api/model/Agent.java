/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airtravelagents.api.dto.external.AgentSummaryTO;
import com.isa.thinair.airtravelagents.api.dto.external.AgentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Agent is the entity class to represent a Agent model
 * 
 * @author : Chamindap
 * @hibernate.class table = "T_AGENT"
 */
public class Agent extends Tracking implements Serializable {

	private static final long serialVersionUID = 4452385013471331612L;
	// TODO: Following two static records are newly added. Some code may refer the row values (ie. ACT, INA)
	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";
	public static final String STATUS_NEW = "NEW";
	public static final String STATUS_BLOCKED = "BLK";

	public static final String REPORTING_TO_GSA_YES = "Y";
	public static final String REPORTING_TO_GSA_NO = "N";

	public static final String ONHOLD_YES = "Y";
	public static final String ONHOLD_NO = "N";

	public static final String CURRENCY_LOCAL = "L";
	public static final String CURRENCY_BASE = "B";

	public static final String AUTO_INVOICE_YES = "Y";
	public static final String AUTO_INVOICE_NO = "N";

	public static final String CAPTURE_PAYMENT_REF_YES = "Y";
	public static final String CAPTURE_PAYMENT_REF_NO = "N";

	public static final String CHARTER_AGENT_YES = "Y";
	public static final String CHARTER_AGENT_NO = "N";

	public static final String NOTIFY_AGENT_MODIFICATIONS_EMAIL = "notify_agent_modifications";

	public static final String HAS_CREDIT_LIMIT_YES = "Y";
	public static final String HAS_CREDIT_LIMIT_NO = "N";
	
	public static final String STATUS_HANDLING_FEE_MOD_Y = "Y";
	public static final String STATUS_HANDLING_FEE_MOD_N = "N";

	/**
	 * Agent payment methods
	 */
	public static final String PAYMENT_MODE_ONACCOUNT = "OA";
	public static final String PAYMENT_MODE_CASH = "CA";
	public static final String PAYMENT_MODE_CREDITCARD = "CC";
	public static final String PAYMENT_MODE_BSP = "BSP";
	public static final String PAYMENT_MODE_VOUCHER = "VO";
	public static final String PAYMENT_MODE_OFFLINE = "OL";
	public static final String PAYMENT_MODE_NOT_SUPPORTED = "NS";

	private String agentCode;

	private String agentName;

	private String airlineCode;

	private String addressLine1;

	private String addressLine2;

	private String addressCity;

	private String addressStateProvince;

	private String postalCode;

	private String accountCode;

	private String telephone;

	private String fax;

	private String emailId;

	private String agentIATANumber;

	private BigDecimal creditLimit = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal bankGuaranteeCashAdvance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal creditLimitLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal bankGuaranteeCashAdvanceLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditLimit = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditLimitLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private String stationCode;

	private String territoryCode;

	private String agentTypeCode;

	private String reportingToGSA;

	private String gsaCode;

	private String billingEmail;

	private String currencyCode;

	private BigDecimal handlingChargAmount;

	private Set<String> paymentMethod;

	private AgentSummary agentSummary;

	private String notes;

	private String contactPersonName;

	private String licenceNumber;

	private String onHold;

	private String agentCommissionCode;

	private String currencySpecifiedIn;

	private String autoInvoice;

	private String languageCode;

	private String serviceChannel;

	private String mobile;

	private String designation;

	private String locationUrl; // JIRA : AARESAA - 3031

	private String capturePaymentRef;

	private String prefferedRptFormat; // JIRA AARESAA - 3467

	private String payRefMandatory; // JIRA AARESAA-4237

	private String publishToWeb; // JIRA AARESAA:4553

	private Set<Integer> schemeIDs;

	private String manualTicketStatus; // AARESAA-3623

	private String enableWSMultiCurrency;

	// handling fee change
	private String handlingFeeChargeBasisCode;

	private int handlingFeeAppliesTo;

	private BigDecimal handlingFeeOnewayAmount;

	private BigDecimal handlingFeeReturnAmount;

	private BigDecimal handlingFeeMinAmount;

	private BigDecimal handlingFeeMaxAmount;

	private String handlingFeeEnable;

	private BigDecimal bspGuarantee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal bspGuaranteeLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String agentWiseTicketEnable;

	private String agentWiseFareMaskEnable;

	private String charterAgent;

	private Date bankGuranteeExpiryDate;

	private Date inactiveFromDate;

	/** Holds a Comma separated list of emails to notify modifications about the agent **/
	private String emailsToNotify;

	private String lccPublishStatus;

	private Set<AgentApplicableOND> applicableOndList;
	private String hasCreditLimit;

	private Set<String> visibleStations;
	
	private Integer maxAdultsAllowed;
	
	private Set<String> visibleTerritories;

	private String taxRegNo;
	
	private String handlingFeeModificationEnabled;
	
	private Integer handlingFeeModificationApllieTo;
	
	private Set<AgentHandlingFeeModification> modificationHandlingFees;

	/**
	 * The constructor.
	 */
	public Agent() {
		super();
	}

	/**
	 * Creates Agent Object from AgentTO object
	 * 
	 * @param agentTO
	 * @throws ModuleException
	 */
	public Agent(AgentTO agentTO, String channel) throws ModuleException {
		this();

		AgentSummaryTO agentSummaryTO = agentTO.getAgentSummaryTO();
		Collection<String> colPaymentMethods = agentTO.getPaymentMethods();
		Util.copyProperties(this, agentTO);
		this.setAgentSummary(new AgentSummary(agentSummaryTO));

		if (colPaymentMethods != null && colPaymentMethods.size() > 0) {
			Set<String> setPaymentMethods = new HashSet<String>();
			String paymentMethod;
			for (String string : colPaymentMethods) {
				paymentMethod = string;
				setPaymentMethods.add(paymentMethod);
			}
			this.setPaymentMethod(setPaymentMethods);
		}

		this.setCurrencySpecifiedIn(CURRENCY_BASE);
		this.setOnHold(Agent.ONHOLD_NO);
		this.setAutoInvoice(AUTO_INVOICE_NO);
		this.setLanguageCode("en");
		this.setCapturePaymentRef(CAPTURE_PAYMENT_REF_NO);

		channel = PlatformUtiltiies.nullHandler(channel);
		if (channel.length() > 0) {
			this.setServiceChannel(channel);
		}
	}

	/**
	 * Create Agent TO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public AgentTO toAgentTO() throws ModuleException {
		Collection<String> colPaymentMethods = this.getPaymentMethod();
		AgentSummary agentSummary = this.getAgentSummary();

		AgentTO agentTO = new AgentTO();
		Util.copyProperties(agentTO, this);

		agentTO.setAgentSummaryTO(agentSummary.toAgentSummaryTO());

		if (colPaymentMethods != null && colPaymentMethods.size() > 0) {
			for (String string : colPaymentMethods) {
				String paymentMethod = string;
				agentTO.addPaymentMethods(paymentMethod);
			}
		}

		return agentTO;
	}

	/**
	 * Returns the agentCode
	 * 
	 * @return Returns the agentCode.
	 * @hibernate.id column = "AGENT_CODE" generator-class = "assigned"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Sets the agentCode
	 * 
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * Returns the agentName
	 * 
	 * @return Returns the agentName.
	 * @hibernate.property column = "AGENT_NAME"
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the agentName
	 * 
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the airlineCode
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	/**
	 * return the agent name with the airline carrier which belongs to. With the user/agent share across the hub is
	 * implemented this method should be use to get the full agent display name.
	 * 
	 * @return
	 */
	public String getAgentDisplayName() {
		String airlineCode = getAirlineCode();
		String agentName = getAgentName();
		if (agentName == null) { // not to break the existing fun
			return null;
		} else {
			return agentName + " (" + airlineCode + ")";
		}
	}

	/**
	 * Returns the addressLine1
	 * 
	 * @return Returns the addressLine1.
	 * @hibernate.property column = "ADDRESS_LINE_1"
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * Sets the addressLine1
	 * 
	 * @param addressLine1
	 *            The addressLine1 to set.
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * Returns the addressLine2
	 * 
	 * @return Returns the addressLine2.
	 * @hibernate.property column = "ADDRESS_LINE_2"
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * Sets the addressLine2
	 * 
	 * @param addressLine2
	 *            The addressLine2 to set.
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * Returns the addressCity
	 * 
	 * @return Returns the addressCity.
	 * @hibernate.property column = "ADDRESS_CITY"
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set.
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Returns the addressStateProvince
	 * 
	 * @return Returns the addressStateProvince.
	 * @hibernate.property column = "ADDRESS_STATE_PROVINCE"
	 */
	public String getAddressStateProvince() {
		return addressStateProvince;
	}

	/**
	 * Sets the addressStateProvince
	 * 
	 * @param addressStateProvince
	 *            The addressStateProvince to set.
	 */
	public void setAddressStateProvince(String addressStateProvince) {
		this.addressStateProvince = addressStateProvince;
	}

	/**
	 * Returns the postalCode
	 * 
	 * @return Returns the postalCode.
	 * @hibernate.property column = "POSTAL_CODE"
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postalCode
	 * 
	 * @param postalCode
	 *            The postalCode to set.
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Returns the accountCode
	 * 
	 * @return Returns the accountCode.
	 * @hibernate.property column = "ACCOUNT_CODE"
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * Sets the accountCode
	 * 
	 * @param accountCode
	 *            The accountCode to set.
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * Returns the telephone
	 * 
	 * @return Returns the telephone.
	 * @hibernate.property column = "TELEPHONE"
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Sets the telephone
	 * 
	 * @param telephone
	 *            The telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Returns the fax
	 * 
	 * @return Returns the fax.
	 * @hibernate.property column = "FAX"
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax
	 * 
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Returns the emailId
	 * 
	 * @return Returns the emailId.
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * Sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * Returns the agentIataNumber
	 * 
	 * @return Returns the agentIataNumber.
	 * @hibernate.property column = "AGENT_IATA_NUMBER"
	 */
	public String getAgentIATANumber() {
		return agentIATANumber;
	}

	/**
	 * Sets the agentIataNumber
	 * 
	 * @param agentIataNumber
	 *            The agentIataNumber to set.
	 */
	public void setAgentIATANumber(String agentIATANumber) {
		this.agentIATANumber = agentIATANumber;
	}

	/**
	 * Returns the creditLimit
	 * 
	 * @return Returns the creditLimit.
	 * @hibernate.property column = "CREDIT_LIMIT"
	 */
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	/**
	 * Sets the creditLimit
	 * 
	 * @param creditLimit
	 *            The creditLimit to set.
	 */
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * Returns the bankGuaranteeCashAdvance
	 * 
	 * @return Returns the bankGuaranteeCashAdvance.
	 * @hibernate.property column = "BANK_GUARANTEE_CASH_ADVANCE"
	 */
	public BigDecimal getBankGuaranteeCashAdvance() {
		return bankGuaranteeCashAdvance;
	}

	/**
	 * Sets the bankGuaranteeCashAdvance
	 * 
	 * @param bankGuaranteeCashAdvance
	 *            The bankGuaranteeCashAdvance to set.
	 */
	public void setBankGuaranteeCashAdvance(BigDecimal bankGuaranteeCashAdvance) {
		this.bankGuaranteeCashAdvance = bankGuaranteeCashAdvance;
	}

	/**
	 * Returns the status
	 * 
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Returns the station code.
	 * 
	 * @return Returns the stationCode.
	 * @hibernate.property column = "STATION_CODE"
	 */
	public String getStationCode() {
		return stationCode;
	}

	/**
	 * Sets the station code
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * Returns the territory Code.
	 * 
	 * @return Returns the territoryCode.
	 * @hibernate.property column = "TERRITORY_CODE"
	 */
	public String getTerritoryCode() {
		return territoryCode;
	}

	/**
	 * Sets the territoryCode
	 */
	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	/**
	 * @hibernate.set table="T_AGENT_PAYMENT_METHOD"
	 * @hibernate.collection-key column="AGENT_CODE"
	 * @hibernate.collection-element type="string" column="PAYMENT_CODE"
	 */
	public Set<String> getPaymentMethod() {
		return this.paymentMethod;
	}

	/**
	 * Sets the paymentMethod
	 * 
	 * @param paymentMethod
	 *            The paymentMethod to set.
	 */
	public void setPaymentMethod(Set<String> set) {
		this.paymentMethod = set;
	}

	/**
	 * Gets the agent type code.
	 * 
	 * @hibernate.property column = "AGENT_TYPE_CODE"
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	/**
	 * Sets the agentTypeCode
	 * 
	 * @param agentTypeCode
	 *            the agentTypeCode
	 */
	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * @return Returns the agentSummary.
	 * @hibernate.one-to-one class="com.isa.thinair.airtravelagents.api.model.AgentSummary" property=agentCode
	 *                       constrained="true" cascade="all"
	 */
	public AgentSummary getAgentSummary() {
		return agentSummary;
	}

	/**
	 * @param agentSummary
	 *            The agentSummary to set.
	 */
	public void setAgentSummary(AgentSummary agentSummary) {
		this.agentSummary = agentSummary;
	}

	/**
	 * @return Returns the reportingToGSA.
	 * @hibernate.property column = "REPORT_TO_GSA"
	 */
	public String getReportingToGSA() {
		return reportingToGSA;
	}

	/**
	 * @param reportingToGSA
	 *            The reportingGSAId to set.
	 */
	public void setReportingToGSA(String reportingToGSA) {
		this.reportingToGSA = reportingToGSA;
	}

	/**
	 * @return Returns the billingEmail.
	 * @hibernate.property column = "BILLING_EMAIL"
	 */
	public String getBillingEmail() {
		return billingEmail;
	}

	/**
	 * @param billingEmail
	 *            The billingEmail to set.
	 */
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	/**
	 * @return Returns the currencyCode.
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the handlingChargAmount.
	 * @hibernate.property column = "HANDLING_CHARGE_AMOUNT"
	 */
	public BigDecimal getHandlingChargAmount() {
		return handlingChargAmount;
	}

	/**
	 * @param handlingChargAmount
	 *            The handlingChargAmount to set.
	 */
	public void setHandlingChargAmount(BigDecimal handlingChargAmount) {
		this.handlingChargAmount = handlingChargAmount;
	}

	/**
	 * 
	 * @return Returns the notes.
	 * @hibernate.property column = "NOTES"
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            The notes to set.
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return Returns the agentCommissionCode.
	 * @hibernate.property column = "AGENT_COMMISSION_CODE"
	 */
	public String getAgentCommissionCode() {
		return agentCommissionCode;
	}

	/**
	 * @param agentCommissionCode
	 *            The agentCommissionCode to set.
	 */
	public void setAgentCommissionCode(String agentCommissionCode) {
		this.agentCommissionCode = agentCommissionCode;
	}

	/**
	 * @hibernate.property column = "CREDIT_LIMIT_LOCAL"
	 * @return
	 */
	public BigDecimal getCreditLimitLocal() {
		return creditLimitLocal;
	}

	/**
	 * @param creditLimitLocal
	 */
	public void setCreditLimitLocal(BigDecimal creditLimitLocal) {
		this.creditLimitLocal = creditLimitLocal;
	}

	/**
	 * @hibernate.property column = "BANK_GUARANTEE_CASH_ADV_LOCAL"
	 * @return
	 */
	public BigDecimal getBankGuaranteeCashAdvanceLocal() {
		return bankGuaranteeCashAdvanceLocal;
	}

	/**
	 * @param bankGuaranteeCashAdvanceLocal
	 */
	public void setBankGuaranteeCashAdvanceLocal(BigDecimal bankGuaranteeCashAdvanceLocal) {
		this.bankGuaranteeCashAdvanceLocal = bankGuaranteeCashAdvanceLocal;
	}

	/**
	 * @hibernate.property column = "specified_in"
	 * @return
	 */
	public String getCurrencySpecifiedIn() {
		return currencySpecifiedIn;
	}

	/**
	 * @param currencySpecifiedIn
	 */
	public void setCurrencySpecifiedIn(String currencySpecifiedIn) {
		this.currencySpecifiedIn = currencySpecifiedIn;
	}

	/**
	 * @hibernate.property column = "auto_invoice"
	 * @return
	 */
	public String getAutoInvoice() {
		return autoInvoice;
	}

	public void setAutoInvoice(String autoInvoice) {
		this.autoInvoice = autoInvoice;
	}

	/**
	 * @return the contactPersonName
	 * @hibernate.property column = "CONTACT_PERSON_NAME"
	 */
	public String getContactPersonName() {
		return contactPersonName;
	}

	/**
	 * @param contactPersonName
	 *            the contactPersonName to set
	 */
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	/**
	 * @return the licenceNumber
	 * @hibernate.property column = "LICENCE_NO"
	 */
	public String getLicenceNumber() {
		return licenceNumber;
	}

	/**
	 * @param licenceNumber
	 *            the licenceNumber to set
	 */
	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	/**
	 * @return the licenceNumber
	 * @hibernate.property column = "ON_HOLD"
	 */
	public String getOnHold() {
		return onHold;
	}

	/**
	 * @param onHold
	 *            the onHold to set
	 */
	public void setOnHold(String onHold) {
		this.onHold = onHold;
	}

	/**
	 * @return Returns the languageCode.
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return Returns the languageCode.
	 * @hibernate.property column = "SERVICE_CHANNEL_CODE"
	 */
	public String getServiceChannel() {
		return serviceChannel;
	}

	/**
	 * @param languageCode
	 *            The serviceChannel to set.
	 */
	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	/**
	 * JIRA : AARESAA - 3031 Lalanthi
	 * 
	 * @hibernate.property column = "LOCATION_URL"
	 * @return
	 */
	public String getLocationUrl() {
		return locationUrl;
	}

	/**
	 * JIRA : AARESAA - 3031 Sets location url
	 * 
	 * @param locationUrl
	 */
	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	/**
	 * @return the capturePaymentRef
	 * @hibernate.property column = "CAPTURE_PAY_REF"
	 */
	public String getCapturePaymentRef() {
		return capturePaymentRef;
	}

	/**
	 * @param capturePaymentRef
	 *            the capturePaymentRef to set
	 */
	public void setCapturePaymentRef(String capturePaymentRef) {
		this.capturePaymentRef = capturePaymentRef;
	}

	/**
	 * @return the prefferedRptFormat
	 * @hibernate.property column = "PREF_RPT_FORMAT"
	 */
	public String getPrefferedRptFormat() {
		return prefferedRptFormat;
	}

	/**
	 * @param prefferedRptFormat
	 *            the prefferedRptFormat to set
	 */
	public void setPrefferedRptFormat(String prefferedRptFormat) {
		this.prefferedRptFormat = prefferedRptFormat;
	}

	/**
	 * JIRA ID AARESAA-4237
	 * 
	 * @return the payRefMandatory
	 * @hibernate.property column = "PAY_REF_MANDATORY"
	 */
	public String getPayRefMandatory() {
		return payRefMandatory;
	}

	/**
	 * @param payRefMandatory
	 *            the payRefMandatory to set *
	 */
	public void setPayRefMandatory(String payRefMandatory) {
		this.payRefMandatory = payRefMandatory;
	}

	/**
	 * JIRA ID AARESAA-4553
	 * 
	 * @hibernate.property column = "WEBSITE_VISIBILITY"
	 * @return
	 */
	public String getPublishToWeb() {
		return publishToWeb;
	}

	public void setPublishToWeb(String publishToWeb) {
		this.publishToWeb = publishToWeb;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_AGENT_INCENTIVE_SCHEMES"
	 * @hibernate.collection-element column="AGENT_INCENTIVE_SCHEME_ID" type="integer"
	 * @hibernate.collection-key column="AGENT_CODE"
	 */
	public Set<Integer> getSchemeIDs() {
		return schemeIDs;
	}

	/**
	 * Sets the schemeIDs
	 * 
	 * @param shcemeIDs
	 */
	public void setSchemeIDs(Set<Integer> shcemeIDs) {
		this.schemeIDs = shcemeIDs;
	}

	public String getManualTicketStatus() {
		return manualTicketStatus;
	}

	/**
	 * Sets the manual ticket status.
	 * 
	 * @param manualTicketStatus
	 *            the new manual ticket status
	 */
	public void setManualTicketStatus(String manualTicketStatus) {
		this.manualTicketStatus = manualTicketStatus;
	}

	/**
	 * 
	 * @hibernate.property column = "MOBILE"
	 * @return
	 */
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 
	 * @hibernate.property column = "CONTACT_PERSON_DESIGNATION"
	 * @return
	 */
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * 
	 * @hibernate.property column = "SHOW_PRICE_IN_SELECTED_CUR"
	 * @return
	 */
	public String getEnableWSMultiCurrency() {
		return enableWSMultiCurrency;
	}

	public void setEnableWSMultiCurrency(String enableWSMultiCurrency) {
		this.enableWSMultiCurrency = enableWSMultiCurrency;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_BASIS_CODE"
	 * @return
	 */
	public String getHandlingFeeChargeBasisCode() {
		return handlingFeeChargeBasisCode;
	}

	/**
	 * @param handlingFeeChargeBasisCode
	 */
	public void setHandlingFeeChargeBasisCode(String handlingFeeChargeBasisCode) {
		this.handlingFeeChargeBasisCode = handlingFeeChargeBasisCode;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_APPLIES_TO"
	 * @return
	 */
	public int getHandlingFeeAppliesTo() {
		return handlingFeeAppliesTo;
	}

	/**
	 * @param handlingFeeAppliesTo
	 */
	public void setHandlingFeeAppliesTo(int handlingFeeAppliesTo) {
		this.handlingFeeAppliesTo = handlingFeeAppliesTo;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_OW_AMOUNT"
	 * @return
	 */
	public BigDecimal getHandlingFeeOnewayAmount() {
		return handlingFeeOnewayAmount;
	}

	/**
	 * 
	 * @param handlingFeeOnewayAmount
	 */
	public void setHandlingFeeOnewayAmount(BigDecimal handlingFeeOnewayAmount) {
		this.handlingFeeOnewayAmount = handlingFeeOnewayAmount;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_RT_AMOUNT"
	 * @return
	 */
	public BigDecimal getHandlingFeeReturnAmount() {
		return handlingFeeReturnAmount;
	}

	/**
	 * 
	 * @param handlingFeeReturnAmount
	 */
	public void setHandlingFeeReturnAmount(BigDecimal handlingFeeReturnAmount) {
		this.handlingFeeReturnAmount = handlingFeeReturnAmount;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_MIN_AMOUNT"
	 * @return
	 */
	public BigDecimal getHandlingFeeMinAmount() {
		return handlingFeeMinAmount;
	}

	/**
	 * @param handlingFeeMinAmount
	 */
	public void setHandlingFeeMinAmount(BigDecimal handlingFeeMinAmount) {
		this.handlingFeeMinAmount = handlingFeeMinAmount;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_MAX_AMOUNT"
	 * @return
	 */
	public BigDecimal getHandlingFeeMaxAmount() {
		return handlingFeeMaxAmount;
	}

	/**
	 * @param handlingFeeMaxAmount
	 */
	public void setHandlingFeeMaxAmount(BigDecimal handlingFeeMaxAmount) {
		this.handlingFeeMaxAmount = handlingFeeMaxAmount;
	}

	/**
	 * @hibernate.property column = "HANDLING_CHG_ENABLED"
	 * @return
	 */
	public String getHandlingFeeEnable() {
		return handlingFeeEnable;
	}

	/**
	 * @param handlingFeeEnable
	 */
	public void setHandlingFeeEnable(String handlingFeeEnable) {
		this.handlingFeeEnable = handlingFeeEnable;
	}

	/**
	 * @return
	 * @hibernate.property column = "BSP_GUARANTEE"
	 */
	public BigDecimal getBspGuarantee() {
		return bspGuarantee;
	}

	/**
	 * @param bspGuarantee
	 */
	public void setBspGuarantee(BigDecimal bspGuarantee) {
		this.bspGuarantee = bspGuarantee;
	}

	/**
	 * @return
	 * @hibernate.property column = "BSP_GUARANTEE_LOCAL"
	 */
	public BigDecimal getBspGuaranteeLocal() {
		return bspGuaranteeLocal;
	}

	/**
	 * @param bspGuaranteeLocal
	 */
	public void setBspGuaranteeLocal(BigDecimal bspGuaranteeLocal) {
		this.bspGuaranteeLocal = bspGuaranteeLocal;
	}

	/**
	 * @hibernate.property column = "AGENT_WISE_TKT_ENABLE"
	 */
	public String getAgentWiseTicketEnable() {
		return agentWiseTicketEnable;
	}

	/**
	 * @param agentWiseTicketEnable
	 */
	public void setAgentWiseTicketEnable(String agentWiseTicketEnable) {
		this.agentWiseTicketEnable = agentWiseTicketEnable;
	}

	/**
	 * @hibernate.property column = "CHARTER_AGENT"
	 */
	public String getCharterAgent() {
		return charterAgent;
	}

	/**
	 * @param agentWiseTicketEnable
	 */
	public void setCharterAgent(String charterAgent) {
		this.charterAgent = charterAgent;
	}

	/**
	 * @hibernate.property column = "BANK_GUARANTEE_EXPIRY_DATE"
	 */
	public Date getBankGuranteeExpiryDate() {
		return bankGuranteeExpiryDate;
	}

	/**
	 * @param bankGuranteeExpiryDate
	 *            the bankGuranteeExpiryDate to set
	 */
	public void setBankGuranteeExpiryDate(Date bankGuranteeExpiryDate) {
		this.bankGuranteeExpiryDate = bankGuranteeExpiryDate;
	}

	/**
	 * @hibernate.property column = "FARE_MASK"
	 */
	public String getAgentWiseFareMaskEnable() {
		return agentWiseFareMaskEnable;
	}

	/**
	 * @param agentWiseFareMaskEnable
	 *            the agentWiseFareMaskEnable to set
	 * 
	 */
	public void setAgentWiseFareMaskEnable(String agentWiseFareMaskEnable) {
		this.agentWiseFareMaskEnable = agentWiseFareMaskEnable;
	}

	/**
	 * @hibernate.property column = "GSA_CODE"
	 */
	public String getGsaCode() {
		return gsaCode;
	}

	/**
	 * @param gsaCode
	 *            the gsaCode to set
	 */
	public void setGsaCode(String gsaCode) {
		this.gsaCode = gsaCode;
	}

	/**
	 * @hibernate.property column = "INACTIVE_FROM_DATE"
	 * @return the inactiveFromDate
	 */
	public Date getInactiveFromDate() {
		return inactiveFromDate;
	}

	/**
	 * @param inactiveFromDate
	 *            the inactiveFromDate to set
	 */
	public void setInactiveFromDate(Date inactiveFromDate) {
		this.inactiveFromDate = inactiveFromDate;
	}

	/**
	 * @hibernate.property column = "EMAILS_TO_NOTIFY"
	 * @return the emails list to notify about modifications 
	 */
	public String getEmailsToNotify() {
		return emailsToNotify;
	}

	public void setEmailsToNotify(String emailsToNotify) {
		this.emailsToNotify = emailsToNotify;
	}

	/**
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}
	

	/**
	 * @hibernate.property column = "HAS_CREDIT_LIMIT"
	 * @return the hasCreditLimit
	 */
	public String getHasCreditLimit() {
		return hasCreditLimit;
	}

	/**
	 * @param hasCreditLimit
	 *            the hasCreditLimit to set
	 */
	public void setHasCreditLimit(String hasCreditLimit) {
		this.hasCreditLimit = hasCreditLimit;
	}

	/**
	 * @hibernate.property column = "TOTAL_CREDIT_LIMIT"
	 * @return the sharedCreditLimit
	 */
	public BigDecimal getTotalCreditLimit() {
		return totalCreditLimit;
	}

	/**
	 * @param sharedCreditLimit
	 *            the sharedCreditLimit to set
	 */
	public void setTotalCreditLimit(BigDecimal totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}

	/**
	 * @hibernate.property column = "TOTAL_CREDIT_LIMIT_LOCAL"
	 * @return the sharedCreditLimitLocal
	 */
	public BigDecimal getTotalCreditLimitLocal() {
		return totalCreditLimitLocal;
	}

	/**
	 * @param totalCreditLimitLocal
	 *            the sharedCreditLimitLocal to set
	 */
	public void setTotalCreditLimitLocal(BigDecimal totalCreditLimitLocal) {
		this.totalCreditLimitLocal = totalCreditLimitLocal;
	}

	/**
	 * @return the visibleStations
	 * @hibernate.set lazy="false" table="T_AGENT_STATION"
	 * @hibernate.collection-element column="STATION_CODE" type="string"
	 * @hibernate.collection-key column="AGENT_CODE"
	 */
	public Set<String> getVisibleStations() {
		return visibleStations;
	}

	/**
	 * @param visibleStations the visibleStations to set
	 */
	public void setVisibleStations(Set<String> visibleStations) {
		this.visibleStations = visibleStations;
	}

	/**
	 * @return the visibleTerritories
	 * @hibernate.set lazy="false" table="T_GSA_TERRITORY"
	 * @hibernate.collection-element column="TERRITORY_CODE" type="string"
	 * @hibernate.collection-key column="AGENT_CODE"
	 */
	public Set<String> getVisibleTerritories() {
		return visibleTerritories;
	}

	/**
	 * @param visibleTerritories the visibleTerritories to set
	 */
	public void setVisibleTerritories(Set<String> visibleTerritories) {
		this.visibleTerritories = visibleTerritories;
	}

	public String getTaxRegNo() {
		return taxRegNo;
	}

	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}


	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="AGENT_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airtravelagents.api.model.AgentApplicableOND"
	 */
	public Set<AgentApplicableOND> getApplicableOndList() {
		return applicableOndList;
	}

	public void setApplicableOndList(Set<AgentApplicableOND> applicableOndList) {
		this.applicableOndList = applicableOndList;
	}

	/**
	 * @hibernate.property column = "MOD_HANDLING_CHG_ENABLED"
	 * @return
	 */
	public String getHandlingFeeModificationEnabled() {
		return handlingFeeModificationEnabled;
	}

	/**
	 * @param handlingFeeModificationEnabled
	 */
	public void setHandlingFeeModificationEnabled(String handlingFeeModificationEnabled) {
		this.handlingFeeModificationEnabled = handlingFeeModificationEnabled;
	}

	/**
	 * @hibernate.property column = "MOD_HANDLING_CHG_APPLIES_TO"
	 * @return
	 */
	public Integer getHandlingFeeModificationApllieTo() {
		return handlingFeeModificationApllieTo;
	}

	/**
	 * @param handlingFeeModificationApllieTo
	 */
	public void setHandlingFeeModificationApllieTo(Integer handlingFeeModificationApllieTo) {
		this.handlingFeeModificationApllieTo = handlingFeeModificationApllieTo;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="AGENT_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airtravelagents.api.model.AgentHandlingFeeModification"
	 */
	public Set<AgentHandlingFeeModification> getModificationHandlingFees() {
		return modificationHandlingFees;
	}

	public void setModificationHandlingFees(Set<AgentHandlingFeeModification> modificationHandlingFees) {
		this.modificationHandlingFees = modificationHandlingFees;
	}

	/**
	 * @hibernate.property column = "MAX_ADULT_COUNT"
	 * @return the maxAdultsAllowed
	 */
	public Integer getMaxAdultAllowed() {
		return maxAdultsAllowed;
	}

	/**
	 * @param maxPaxAllowed the maxPaxAllowed to set
	 */
	public void setMaxAdultAllowed(Integer maxAdultAllowed) {
		this.maxAdultsAllowed = maxAdultAllowed;
	}
	
}