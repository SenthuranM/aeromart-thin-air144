/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nilindra Fernando
 */
public class AgentTransactionTO extends Persistent implements Serializable {

	private static final long serialVersionUID = -6760711853103967322L;

	private int tnxId;

	private String agentCode;

	private BigDecimal amount;

	private String drOrcr;

	private int invoiced;

	private Date transctionDate;

	private int nominalCode;

	private String userId;

	private String notes;

	private Integer salesChannelCode;

	private String pnr;

	private String currencyCode;

	private BigDecimal amountLocal;

	private String adjustmentCarrierCode;

	private String receiptNumber;

	private String externalRef;
	
	private boolean creditReversed;

	/**
	 * The constructor.
	 */
	public AgentTransactionTO() {
		super();
	}

	/**
	 * @return the tnxId
	 */
	public int getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            the tnxId to set
	 */
	public void setTnxId(int tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the drOrcr
	 */
	public String getDrOrcr() {
		return drOrcr;
	}

	/**
	 * @param drOrcr
	 *            the drOrcr to set
	 */
	public void setDrOrcr(String drOrcr) {
		this.drOrcr = drOrcr;
	}

	/**
	 * @return the invoiced
	 */
	public int getInvoiced() {
		return invoiced;
	}

	/**
	 * @param invoiced
	 *            the invoiced to set
	 */
	public void setInvoiced(int invoiced) {
		this.invoiced = invoiced;
	}

	/**
	 * @return the transctionDate
	 */
	public Date getTransctionDate() {
		return transctionDate;
	}

	/**
	 * @param transctionDate
	 *            the transctionDate to set
	 */
	public void setTransctionDate(Date transctionDate) {
		this.transctionDate = transctionDate;
	}

	/**
	 * @return the nominalCode
	 */
	public int getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the salesChannelCode
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the amountLocal
	 */
	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	/**
	 * @param amountLocal
	 *            the amountLocal to set
	 */
	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	/**
	 * @return the adjustmentCarrierCode
	 */
	public String getAdjustmentCarrierCode() {
		return adjustmentCarrierCode;
	}

	/**
	 * @param adjustmentCarrierCode
	 *            the adjustmentCarrierCode to set
	 */
	public void setAdjustmentCarrierCode(String adjustmentCarrierCode) {
		this.adjustmentCarrierCode = adjustmentCarrierCode;
	}

	/**
	 * @return the receiptNumber
	 */
	public String getReceiptNumber() {
		return receiptNumber;
	}

	/**
	 * @param receiptNumber
	 *            the receiptNumber to set
	 */
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getExternalRef() {
		return externalRef;
	}

	public void setExternalRef(String externalRef) {
		this.externalRef = externalRef;
	}

	public boolean isCreditReversed() {
		return creditReversed;
	}

	public void setCreditReversed(boolean creditReversed) {
		this.creditReversed = creditReversed;
	}

}