package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.util.GregorianCalendar;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentTransactionDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAgentTransation extends PlatformTestCase {
	AgentTransaction agt = null;	
	AgentTransactionDAO agenttnxDAO = null;
	LookupService lookup = null;
	
	protected void setUp() throws Exception {
		
		lookup = LookupServiceFactory.getInstance();
		agenttnxDAO = (AgentTransactionDAO)lookup.getBean("isa:base://modules/" 
				+ "airtravelagents?id=agentTransactionDAOProxy");
		
		agt = new AgentTransaction();
		
		agt.setAgentCode("AAA2");
		agt.setAmount(123.03);
		agt.setDrOrcr("Dr");
		agt.setInvoiced(1);
		agt.setNominalCode(2);
		agt.setNotes("ddddd");
		agt.setTnxId(1);
		agt.setTransctionDate(new GregorianCalendar().getTime());
		agt.setUserId("TEST_USER");		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentTransactionDAOImpl.saveOrUpdate(AgentTransaction)'
	 */
	public void testSaveOrUpdate() {
	System.out.println(" 1111111 BBGB "+agenttnxDAO);
		assertNotNull(agenttnxDAO);
		agenttnxDAO.saveOrUpdate(agt);	
		
 System.out.println(" 11111111111  ");

	}

	/*
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentTransactionDAOImpl.removeAgentTransaction(int)'
	 */
	public void testRemoveAgentTransaction() {
		// TODO Auto-generated method stub

	}

	/*
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentTransactionDAOImpl.getAgentTransaction(int)'
	 */
	/**
	 * 
	 */
	public void testGetAgentTransaction() {
		// TODO Auto-generated method stub

	}

}
