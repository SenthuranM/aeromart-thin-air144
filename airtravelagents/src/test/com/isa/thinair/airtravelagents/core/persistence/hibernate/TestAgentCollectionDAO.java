/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestAgentCollectionDAO extends PlatformTestCase {
	
	AgentCollectionDAO agentCollectionDAO = null;
	AgentCollection agentCollection = null;
	public static String AGENTCOLLECTIONDAO = "agentCollectionDAO";
	
	/** 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		//Test
		
		agentCollectionDAO = (AgentCollectionDAO) AirTravelagentsModuleUtils
		.getDAO(AGENTCOLLECTIONDAO);
		
		agentCollection = new AgentCollection();
		
		agentCollection.setAgentCode("15");
		agentCollection.setCancelationDate(new GregorianCalendar(2005, 10, 10)
				.getTime());
		agentCollection.setChequeNumber("1112");
		agentCollection.setCmCode("CSH");
		agentCollection.setCpCode("ADV");
		agentCollection.setDateOfPayment(new GregorianCalendar(2005, 9, 10)
				.getTime());
		agentCollection.setPaymentAmount(10000);
		agentCollection.setRemarks("Remarks");
		agentCollection.setStatus("Paid");		
	}

	/**
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.getAgentCollections(String)'
	 */
	public void testGetAgentCollections() {
		String agentCode = "25";
		Collection agentCol = null;
		
		assertNotNull(agentCollectionDAO);
		agentCol = agentCollectionDAO.getAgentCollections(agentCode);
		
		assertEquals("Incorrect size", 2, agentCol.size());		
		Iterator iter = agentCol.iterator();
		
		while (iter.hasNext()) {
			AgentCollection element = (AgentCollection) iter.next();
			
			if (element.getReceiptNumber() == 1) {
				assertEquals(" Incorrect receipt number ", 1, 
						element.getReceiptNumber());
				assertEquals(" Incorrect agent code ", "25", 
						element.getAgentCode());
				assertEquals(" Incorrect payment amount ", 5000, 
						element.getPaymentAmount(), 1);
				assertEquals(" Incorrect date of payment", 
						new GregorianCalendar(2005, 8, 10).getTime(),
						element.getDateOfPayment());
				assertEquals(" Incorrect cp code ", "INV",  
						element.getCpCode());
				assertEquals(" Incorrect Status ", "COLLECTED", 
						element.getStatus());
				assertEquals(" Incorrect cheque number ", "1110", 
						element.getChequeNumber());			
			}		
		}
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.addAgentCollection(AgentCollection)'
	 */
	public void testAddAgentCollection() {
		assertNotNull(agentCollectionDAO);	

		agentCollectionDAO.addAgentCollection(agentCollection);
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.removeAgentCollection(String)'
	 */
//	public void testRemoveAgentCollection() {
//		assertNotNull(agentCollectionDAO);
//		
//		agentCollectionDAO.addAgentCollection(agentCollection);
//		
//		agentCollectionDAO.removeAgentCollection(5);
//	}
	
	
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.searchAgentCollections(List)'
	 */
	public void testSearchAgentCollections() {
//		String agentCode = "15";
//		List criteria = new ArrayList();	
//		
//		List listValues = new ArrayList();
//		listValues.add(agentCode);		
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion.setFieldName("agentCode");
//		criterion.setValue(listValues);
//		
//		List listValues1 = new ArrayList();
//		listValues1.add(new GregorianCalendar(2005, 8, 7).getTime());
//		ModuleCriterion criterion1 = new ModuleCriterion();
//		criterion1.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion1.setFieldName("invoiceDate");
//		criterion1.setValue(listValues1);
		

		
		int receiptNumbere = 25;
		List criteria = new ArrayList();	
		
		List listValues = new ArrayList();
		listValues.add( new Integer(receiptNumbere));		
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterion.setFieldName("receiptNumber");
		criterion.setValue(listValues);

				
//		String agentCode = "15";
//		List criteria2 = new ArrayList();	
//		
//		List listValues2 = new ArrayList();
//		listValues2.add(agentCode);		
//		ModuleCriterion criterion2 = new ModuleCriterion();
//		criterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion2.setFieldName("agentCode");
//		criterion2.setValue(listValues2);
		
		criteria.add(criterion);
		//criteria.add(criterion2);
		
		assertNotNull(agentCollectionDAO);
		
		Collection c = agentCollectionDAO.searchAgentCollections(criteria);
		
		System.out.println("  xacaxaxaxaxxaxaxaxaxax axaxax  "+c);
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.searchAgentReceipts(String)'
	 */
	public void testSearchAgentReceipts() {
		String agentCode = "25";
		Collection agentCollection = null;
		
		assertNotNull(agentCollectionDAO);		
		agentCollection = agentCollectionDAO.searchAgentReceipts(agentCode);
		
		assertEquals("Incorrect size", 2, agentCollection.size());
		
		Iterator iter = agentCollection.iterator();
		
		while (iter.hasNext()) {
			AgentCollection element = (AgentCollection) iter.next();
			
			if (element.getReceiptNumber() == 1) {
				assertEquals(" Incorrect receipt number ", 1, 
						element.getReceiptNumber());
				assertEquals(" Incorrect agent code ", "25", 
						element.getAgentCode());
				assertEquals(" Incorrect payment amount ", 5000, 
						element.getPaymentAmount(), 1);
				assertEquals(" Incorrect date of payment", 
						new GregorianCalendar(2005, 8, 10).getTime(),
						element.getDateOfPayment());			
				assertEquals(" Incorrect cp code ", "INV",  
						element.getCpCode());
				assertEquals(" Incorrect Status ", "COLLECTED", 
						element.getStatus());
				assertEquals(" Incorrect cheque number ", "1110", 
						element.getChequeNumber());			
			}		
		}
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.updateAgentCollection(String)'
	 */
	public void testUpdateAgentCollection() {
		int receiptNo = 2;
		String agentCode = "25";
		Collection agentCol = null;				
				
		assertNotNull(agentCollectionDAO);		
		agentCollectionDAO.updateAgentCollection(receiptNo);
		
		agentCol = agentCollectionDAO.getAgentCollections(agentCode);
		
		assertEquals("Incorrect size", 2, agentCol.size());		
		Iterator iter1 = agentCol.iterator();
		
		while (iter1.hasNext()) {
			AgentCollection element = (AgentCollection) iter1.next();
			
			if (element.getReceiptNumber() == 2) {
				assertEquals(" Incorrect receipt number ", 2, 
						element.getReceiptNumber());
				assertEquals(" Incorrect agent code ", "25", 
						element.getAgentCode());								
				assertEquals(" Incorrect Status ", "REVERSED", 
						element.getStatus());						
			}		
		}
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.updateAgentSummary(String, double,
	 * String, boolean)'
	 */
	public void testUpdateAgentSummaryStringFloatStringBoolean() {
		int amount = 2000;
		String agentCode = "15";
		String cp = "INV";
		boolean isPost = true;		
		AgentSummary agentSummary = null;
				
		assertNotNull(agentCollectionDAO);
		
		agentSummary = agentCollectionDAO.getAgentSummary(agentCode);
		
		double availableCredit = agentSummary.getAvailableCredit();
		double availableFundsForInv = agentSummary.getAvailableFundsForInv();		
		double availableAdvance = agentSummary.getAvailableAdvance();
		
		assertEquals(" Incorrect agent code ", "15", 
				agentSummary.getAgentCode());
		assertEquals(" Incorrect available credit ", availableCredit, 
				agentSummary.getAvailableCredit(), 1);
		assertEquals(" Incorrect available funds for invoice ", 
				availableFundsForInv, agentSummary.getAvailableFundsForInv(), 1);
		assertEquals(" Incorrect available advance ", availableAdvance, 
				agentSummary.getAvailableAdvance(), 1);
		
		//agentCollectionDAO.updateAgentSummary(agentCode, amount, cp, isPost);
		
		agentSummary = agentCollectionDAO.getAgentSummary(agentCode);
		
		assertEquals(" Incorrect available credit ", availableCredit + amount, 
				agentSummary.getAvailableCredit(), 1);
		assertEquals(" Incorrect available funds for invoice ", 
				availableFundsForInv + amount, 
				agentSummary.getAvailableFundsForInv(), 1);
		assertEquals(" Incorrect available advance ", availableAdvance, 
				agentSummary.getAvailableAdvance(), 1);
		
		cp = "ADV";
	//	agentCollectionDAO.updateAgentSummary(agentCode, amount, cp, isPost);
		agentSummary = agentCollectionDAO.getAgentSummary(agentCode);
		assertEquals(" Incorrect available advance ", availableAdvance + amount, 
				agentSummary.getAvailableAdvance(), 1);		
		
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.updateAgentSummary(String, double)'
	 */
	public void testUpdateAgentSummaryStringFloat() {
		int amount = 2000;
		String agentCode = "15";		
		AgentSummary agentSummary = null;
				
		assertNotNull(agentCollectionDAO);		
		agentSummary = agentCollectionDAO.getAgentSummary(agentCode);
		
		double availableCredit = agentSummary.getAvailableCredit();		
		
		assertEquals(" Incorrect agent code ", "15", 
				agentSummary.getAgentCode());
		assertEquals(" Incorrect available credit ", availableCredit, 
				agentSummary.getAvailableCredit(), 1);
		
		agentCollectionDAO.updateAgentSummary(agentCode, amount);
		
		agentSummary = agentCollectionDAO.getAgentSummary(agentCode);		
		assertEquals(" Incorrect available credit ", availableCredit - amount, 
				agentSummary.getAvailableCredit(), 1);
		
	}	

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.AgentCollectionDAOImpl.getCollectionDetails(int)'
	 */
	public void testGetCollectionDetails() {
		AgentCollection agentCollection = null;
		
		assertNotNull(agentCollectionDAO);		
		agentCollection = agentCollectionDAO.getCollectionDetails(1);
		
		assertEquals(" Incorrect receipt number ", 1, 
				agentCollection.getReceiptNumber());
		assertEquals(" Incorrect agent code ", "25", 
				agentCollection.getAgentCode());
		assertEquals(" Incorrect payment amount ", 5000, 
				agentCollection.getPaymentAmount(), 1);
		assertEquals(" Incorrect date of payment", 
				new GregorianCalendar(2005, 8, 10).getTime(),
				agentCollection.getDateOfPayment());
		assertEquals(" Incorrect cp code ", "INV",  
				agentCollection.getCpCode());
		assertEquals(" Incorrect Status ", "COLLECTED", 
				agentCollection.getStatus());
		assertEquals(" Incorrect cheque number ", "1110", 
				agentCollection.getChequeNumber());
		
	}
}