/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.bl;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 *
 */
public class TransferingToExternal extends PlatformTestCase {

	public static void main(String args[]) throws Exception{
		
		TransferingToExternal external = new TransferingToExternal();
		
		external.testTransferAgentInvoicesToExternal();
		
		
	}
	
	/**
	 *  Operation Sequence
	 * (1) Gets Agent codes for Period (2) Transfer Manual External 
	 *                                 <for each Agent>
	 *                                   (2.1) Gets the Invoice Object for each Agent
	 *                                   (2.3) Passess to InvoiceDaoImpl for Transfering to Excternal 
	 * 								     (2.4) Update Status that it was Transfered.
	 * 								   </for each Agent> 
	 */
	public void testTransferAgentInvoicesToExternal() throws Exception{
		System.out.println("starting");
		/** Params To Pass **************************/
		Date startDate = new GregorianCalendar(2006,GregorianCalendar.MAY,16).getTime();
		Date endDate = new GregorianCalendar(2006,GregorianCalendar.MAY,31).getTime();
		String invoiceNumberStartDigits = "060502";
		/*******************************************/
		
        //FIXME
		//Collection agentDetails = ReservationModuleUtils.DAOInstance.SALES_DAO.getAllAgentCodesForInvoicePeriod(invoiceNumberStartDigits);
        Collection agentDetails = null;
		
		TravelAgentFinanceBL agentFinanceBL = new TravelAgentFinanceBL();
		Iterator iterAgentCodes= null;
		
		
		iterAgentCodes = agentDetails.iterator();
		while(iterAgentCodes.hasNext()){
			
			String agentCode = (String) iterAgentCodes.next();
			
			try{
                //FIXME
			    //agentFinanceBL.transferManualExternal(startDate, endDate, agentCode);
             }
			catch(Exception e){
				
				System.out.println("Agent Code Could not transfer : " + agentCode);
			}
			
			
		}
	
	}


	


}