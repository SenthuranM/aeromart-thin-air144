/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airtravelagents.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.airtravelagents.api.model.Invoice;
import com.isa.thinair.airtravelagents.api.model.InvoiceSettlement;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestInvoiceDAO extends PlatformTestCase {

	
	
	public static String INVOICEDAO = "invoiceDAO";
	private InvoiceDAO invoiceDAO = null;
	private Invoice invoice = null;
	
	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		invoiceDAO = (InvoiceDAO) AirTravelagentsModuleUtils.getDAO(INVOICEDAO);
		
		invoice = new Invoice();
		
//		invoice.setInvoiceNumber("5");
		invoice.setAgentCode("20");
		invoice.setBillingPeriod(1);
		invoice.setInviocePeriodFrom(new GregorianCalendar().getTime());
		invoice.setInvoiceAmount(2500);
	//	invoice.setInvoiceDate(new GregorianCalendar().getTime());
		invoice.setInvoiceDate(new GregorianCalendar(2010, 8, 4).getTime());	
		invoice.setInvoicePeriodTo(new GregorianCalendar(4, 12, 2005).getTime());
		invoice.setSettledAmount(500);
		invoice.setStatus("UNSETTLED");		 
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}	

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.InvoiceDAOImpl.getUnsettledInvoicesForAgent(String)'
	 */
/*	public void testGetUnsettledInvoicesForAgent() {
		String agentCode = "15";
		Collection invoiceUnsettled  = null;		
		
		assertNotNull(invoiceDAO);
		
		invoiceUnsettled = invoiceDAO.getUnsettledInvoicesForAgent(agentCode);
		assertEquals(" Incorrect size ", 2, invoiceUnsettled.size());
		
		Iterator iter =  invoiceUnsettled.iterator();
		
		while (iter.hasNext()) {
			Invoice element = (Invoice) iter.next();
			
			if (element.getInvoiceNumber().equals("3")) {
				assertEquals(" Incorrect agent code ", "15", 
						element.getAgentCode());
				assertEquals(" Incorrect invoice amount ", 10000, 
						element.getInvoiceAmount(), 10000);
				assertEquals(" Incorrect settle amount ", 5000,  
						element.getSettledAmount(), 5000);
				assertEquals(" Incorrect Status ", "UNSETTLED", 
						element.getStatus());			
			}			
		}
	}*/

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.InvoiceDAOImpl.getSettledInvoicesForAgent(List, int, int)'
	 */
	public void testGetSettledInvoicesForAgent() {
		//String agentCode = "15";
		String agentCode = "SOP6";
		
		Page settledInvoices = null;
		List criteria = new ArrayList();
		Collection data = null;
		
		assertNotNull(invoiceDAO);
		
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);		
		criterion.setFieldName("agentCode");
		List listValues = new ArrayList();
		listValues.add(agentCode);
		criterion.setValue(listValues);
		
		criteria.add(criterion);
		
		settledInvoices = invoiceDAO.getSettledInvoicesForAgent(criteria, 0, 10);
		
System.out.println(" sizzzzzzzzzzzzzz  "+settledInvoices.getTotalNoOfRecords());
System.out.println(" siz g  zzzzzzz  "+settledInvoices.getPageData().size());
		assertEquals(" Incorrect size ", 14, settledInvoices.getTotalNoOfRecords());
		data = settledInvoices.getPageData();
		
		Iterator iter = data.iterator();
		
		while (iter.hasNext()) {			
			Invoice element = (Invoice) iter.next();
			
			if (element.getInvoiceNumber().equals("0410020004")) {
				assertEquals(" Incorrect agent code ", "15", 
						element.getAgentCode());
				assertEquals(" Incorrect invoice amount ", 2000,  
						element.getInvoiceAmount(), 2000);
				assertEquals(" Incorrect settle amount ", 2000,
						element.getSettledAmount(), 2000);
				assertEquals(" Incorrect Status ", "SETTLED", 
						element.getStatus());

			}
		}		
	}
	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.saveInvoice(Invoice)'
//	 */
//	public void testSaveInvoice() {
//		Invoice inv = null;
//		
//		assertNotNull(invoiceDAO);		
//		
//		invoiceDAO.saveInvoice(invoice);
//		
//		inv = invoiceDAO.getInvoice("5");
//		
//		assertEquals(" Incorrect agent code ", "5", 
//				inv.getInvoiceNumber());
//		assertEquals(" Incorrect agent code ", "20", 
//				inv.getAgentCode());
//		assertEquals(" Incorrect invoice amount ", 2500,  
//				inv.getInvoiceAmount(), 2500);
//		assertEquals(" Incorrect settle amount ", 500,  
//				inv.getSettledAmount(), 500);
//		assertEquals(" Incorrect Status ", "UNSETTLED", 
//				inv.getStatus());
//		
//		invoiceDAO.removeInvoice(invoice.getInvoiceNumber());
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.removeInvoice(String)'
//	 */
//	public void testRemoveInvoice() {
//		Invoice inv = null;
//		invoice.setInvoiceNumber("6");
//		
//		assertNotNull(invoiceDAO);
//		invoiceDAO.saveInvoice(invoice);
//		
//		inv = invoiceDAO.getInvoice("6");
//		
//		assertEquals(" Incorrect agent code ", "6", 
//				inv.getInvoiceNumber());
//		
//		invoiceDAO.removeInvoice(inv.getInvoiceNumber());
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.getInvoice(String)'
//	 */
//	public void testGetInvoice() {
//		Invoice invoice = null;
//		
//		assertNotNull(invoiceDAO);
//		invoice = invoiceDAO.getInvoice("3");	
//		
//		assertEquals(" Incorrect invoice number ", "3", 
//				invoice.getInvoiceNumber());
//		assertEquals(" Incorrect agent code ", "15", 
//				invoice.getAgentCode());
//		assertEquals(" Incorrect invoice amount ", 10000, 
//				invoice.getInvoiceAmount(), 10000);
//		assertEquals(" Incorrect settle amount ", 5000, 
//				invoice.getSettledAmount(), 5000);
//		assertEquals(" Incorrect Status ", "UNSETTLED", 
//				invoice.getStatus());
//	}	
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.updateInvoice(String, double)'
//	 */
//	public void testUpdateInvoice() {
//		Invoice inv = null;		
//		String invoiceNumber = "7";
//		double reversedAmount = 500;
//		
//		invoice.setInvoiceNumber(invoiceNumber);
//		invoice.setAgentCode("20");
//		invoice.setStatus("SETTLED");
//		invoice.setSettledAmount(10000);
//		invoice.setInvoiceAmount(10000);
//				
//		assertNotNull(invoiceDAO);
//		
//		invoiceDAO.saveInvoice(invoice);
//		
//		inv = invoiceDAO.getInvoice("7");
//		
//		assertEquals(" Incorrect settle amount ", 10000, 
//				inv.getSettledAmount(), 10000);
//		assertEquals(" Incorrect Status ", "SETTLED", 
//				inv.getStatus());		
//		invoiceDAO.updateInvoice(invoiceNumber, reversedAmount);
//		
//		inv = invoiceDAO.getInvoice("7");
//		
//		assertEquals(" Incorrect settle amount ", 9500, inv.getSettledAmount(), 1);
//		assertEquals(" Incorrect Status ", "UNSETTLED", 
//				inv.getStatus());		
//		invoiceDAO.removeInvoice(inv.getInvoiceNumber());
//	}
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.addInvoiceSettlement(InvoiceSettlement)'
//	 */
//	public void testAddInvoiceSettlement() {
//		InvoiceSettlement settlement = null;
//		
//		settlement = new InvoiceSettlement();		
//		
//		settlement.setAmountPaid(10000);
//		settlement.setCpCode("INV");		
//		settlement.setInvoiceNo("4");
//		settlement.setSettlementDate(new GregorianCalendar().getTime());
//		
//		assertNotNull(invoiceDAO);		
//		invoiceDAO.addInvoiceSettlement(settlement);
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.addInvoiceSettlement(String, double)'
//	 */
//	public void testAddInvoiceSettlementStringFloat() {		
//		assertNotNull(invoiceDAO);		
//		invoiceDAO.addInvoiceSettlement("3", 500);
//	}
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.updateInvoiceSettlement(String, double)'
//	 */
//	public void testUpdateInvoiceSettlement() {
//		String invoiceNumber = "3";
//		double amount = 5000;
//		Invoice inv = null;	
//		
//		assertNotNull(invoiceDAO);		
//		invoiceDAO.updateInvoiceSettlement(invoiceNumber, amount);
//		inv = invoiceDAO.getInvoice("3");
//		
//		assertEquals(" Incorrect invoice number ", "3", 
//				inv.getInvoiceNumber());
//		assertEquals(" Incorrect agent code ", "15", 
//				inv.getAgentCode());
//		assertEquals(" Incorrect invoice amount ", 10000,  
//				inv.getInvoiceAmount(), 1);
//		assertEquals(" Incorrect settle amount ", 10000,  
//				inv.getSettledAmount(), 1);
//		assertEquals(" Incorrect Status ", "SETTLED", 
//				inv.getStatus());
//		
//		invoiceDAO.updateInvoice("3", 5000);
//	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.InvoiceDAOImpl.getInvoiceForAgent(List)'
	 */
//	public void testGetInvoiceForAgent() {		
//		//String agentCode = "15";
//		List criteria = new ArrayList();	
//		
//		String invoiceNumber = "0410020001";
//		
//		List listValues = new ArrayList();
//		listValues.add(invoiceNumber);		
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion.setFieldName("invoiceNumber");
//		criterion.setValue(listValues);
		
//		List listValues = new ArrayList();
//		listValues.add(agentCode);		
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion.setFieldName("agentCode");
//		criterion.setValue(listValues);
//		
//		List listValues1 = new ArrayList();
//		listValues1.add(new GregorianCalendar(2005, 8, 7).getTime());
//		ModuleCriterion criterion1 = new ModuleCriterion();
//		criterion1.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion1.setFieldName("invoiceDate");
//		criterion1.setValue(listValues1);
//		
//		List listValues2 = new ArrayList();
//		listValues2.add(new Integer(1));
//		ModuleCriterion criterion2 = new ModuleCriterion();
//		criterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion2.setFieldName("billingPeriod");
//		criterion2.setValue(listValues2);
//		
		//criteria.add(criterion);
//		criteria.add(criterion1);
//		criteria.add(criterion2);
		
//		String agentCode = "15";		
//		String year = "2005";
//		String month = "9";
//		int billingPeriod = 2;
//		assertNotNull(invoiceDAO);
//		
//		InvoiceSummaryDTO inv = invoiceDAO.getInvoiceForAgent(agentCode, year,
//				month, billingPeriod);		
//		assertEquals(" Incorrect invoice number ", "0410020004", 
//				inv.getInvoice().getInvoiceNumber());
//		assertEquals(" Incorrect agent code ", "15", 
//				inv.getInvoice().getAgentCode());
//		assertEquals(" Incorrect date ", new GregorianCalendar(2005, 9, 25)
//				.getTime(), inv.getInvoice().getInvoiceDate());
//		assertEquals(" Incorrect invoice amount ", 2000,  
//				inv.getInvoice().getInvoiceAmount(), 1);
//		assertEquals(" Incorrect settle amount ", 2000,  
//				inv.getInvoice().getSettledAmount(), 1);
//		assertEquals(" Incorrect Status ", "SETTLED", 
//				inv.getInvoice().getStatus());
//		assertEquals(" Incorrect Address ", "PO Box 29666 DeiraDubai Sharjah Sharjah", 
//				inv.getAgentAddress());
//	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.InvoiceDAOImpl.getSettlementsForInvoice(String)'
	 */
//	public void testGetInvoiceDetails() {
//		Collection invoice = null;
//		
//		Date fromDate = new GregorianCalendar(2005, 7, 10).getTime();		
//		Date toDate = new GregorianCalendar(2005, 8, 17).getTime();		
//		assertNotNull(invoiceDAO);		
//		invoice = invoiceDAO.getInvoiceDetails(fromDate, toDate);
//		
//		Iterator iter = invoice.iterator();
//		
//		while (iter.hasNext()) {
//			Invoice element = (Invoice) iter.next();
//			
//			if (element.getInvoiceNumber().equals("3")) {
//				assertEquals(" Incorrect agent code ", "15", 
//						element.getAgentCode());
//				assertEquals(" Incorrect invoice amount ", 10000, 
//						element.getInvoiceAmount(), 10000);
//				assertEquals(" Incorrect from date ", 
//						new GregorianCalendar(2005, 8, 7).getTime(),
//						element.getInviocePeriodFrom());
//				assertEquals(" Incorrect to date ",
//						new GregorianCalendar(2005, 8, 15).getTime(),
//						element.getInvoicePeriodTo());	
//				assertEquals(" Incorrect Status ", "UNSETTLED", 
//						element.getStatus());			
//			}			
//		}	
//	}

	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.getSettlementsForInvoice(String)'
//	 */
//	public void testGetSettlementsForInvoice() {
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.invalidateSettlement(InvoiceSettlement)'
//	 */
//	public void testInvalidateSettlement() {
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.getSettlementsForAgent(String)'
//	 */
//	public void testGetSettlementsForAgent() {
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.getInvoicesForAgent(String, Date, Date)'
//	 */
//	public void testGetInvoicesForAgent() {
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.settleInvoice(InvoiceSettlement)'
//	 */
//	public void testSettleInvoiceInvoiceSettlement() {
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
//	 * .hibernate.InvoiceDAOImpl.settleInvoice(Set)'
//	 */
//	public void testSettleInvoiceSet() {
//	}
}