/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.remoting.ejb;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 * 
 */
public class TestTravelAgentServiceDelegateImpl extends PlatformTestCase {

	Agent agent = null;	
	
	/**
	 * Sets up the environment.
	 */
	protected void setUp() throws Exception {
		System.setProperty("repository.home", "/usr/isa/aares/test-config-00");
		super.setUp();
		
//		agent = new Agent();		
//		agent.setAgentCode("a555");
//		agent.setAccountCode("ac01");
//		agent.setAddressCity("London");
//		agent.setAddressLine2("London 1");
//		agent.setAddressLine2("London 2");
//		agent.setAddressStateProvince("South");
//		agent.setAgentIATANumber("UK");
//		agent.setAgentName("London agent");
//		agent.setAgentTypeCode("OT");
//		agent.setBankGuaranteeCashAdvance(14000.0F);
//		agent.setCreditLimit(10000);
//		agent.setEmailId("A@bc");
//		agent.setStatus("Active");
	}

	/**
	 * 
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testBlockNonPayingAgents() throws ModuleException {
		TravelAgentBD travelAgentBD = (TravelAgentBD) AirTravelagentsModuleUtils.getInstance().getServiceBD(AirtravelagentsConstants.FullBDKeys.AIRTRAVELAGENTS_SERVICE_REMOTE);
		
		travelAgentBD.blockNonPayingAgents();
	}

//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgent(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgent() throws ModuleException {
//		Agent agent1 = null;
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();		
//		agent.setStationCode("DEL");
//		
//		travelAgentBD.saveAgent(agent);
//		
//		agent1 = travelAgentBD.getAgent("DEL1");
//			
//		assertNotNull(agent1);		
//		
//		assertEquals(" Incorrect agent code ", "DEL1", agent1.getAgentCode());
//		assertEquals(" Incorrect City ", "London", agent1.getAddressCity());
//		assertEquals(" Incorrect email ", "A@bc", agent1.getEmailId());
//		assertEquals(" Incorrect agent name ", "London agent", 
//				agent1.getAgentName());
//		assertEquals(" Incorrect Credit Limit", 10000, 10000, 
//				agent1.getCreditLimit());
//		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 14000F, 14000F, 
//				agent1.getBankGuaranteeCashAdvance());
//		//travelAgentBD.removeAgent(agent1.getAgentCode());
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentUsers(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentUsersString() throws ModuleException {
//		Page agentsUsers = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();	
//		int startIndex = 0;
//		int pageSize = 10;		
//		String agentCode = "16";
//
//		agentsUsers = travelAgentBD.getAgentUsers(agentCode, startIndex, pageSize);
//		assertNotNull(agentsUsers);
//		assertEquals(" Incorrect Size ", 1 , agentsUsers.getEndPosition());
//		
//		Iterator iter = agentsUsers.getPageData().iterator();		
//		
//		while (iter.hasNext()){
//			User element = (User) iter.next();
//			
//			if (element.getUserId().equals("TEST_USER")) {
//				assertEquals(" Incorrect user id ", "TEST_USER", 
//						element.getUserId());
//				assertEquals(" Incorrect password ", "NASLY", 
//						element.getPassword());
//				assertEquals(" Incorrect first name ", "NASLY MOHAMED", 
//						element.getDisplayName());
//				//assertEquals(" Incorrect last name ", "YOOSUF", 
//				//		element.getLastName());
//				assertEquals(" Incorrect email id ", "nasly@jkcsworld.com", 
//						element.getEmailId());		
//			}		
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.saveAgent(Agent)'
//	 * @throws ModuleException 
//	 */
//	public void testSaveAgent() throws ModuleException {
//		Agent agent1 = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		agent.setStationCode("DEL");	
//		travelAgentBD.saveAgent(agent);
//		
//		agent1 = travelAgentBD.getAgent("DEL1");		
//		assertNotNull(agent1);		
//		assertEquals(" Incorrect agent code ", "DEL1", agent1.getAgentCode());
//		assertEquals(" Incorrect City ", "London", agent1.getAddressCity());
//		assertEquals(" Incorrect email ", "A@bc", agent1.getEmailId());
//		assertEquals(" Incorrect agent name ", "London agent", 
//				agent1.getAgentName());
//		assertEquals(" Incorrect Credit Limit", 10000, 10000, 
//				agent1.getCreditLimit());
//		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 14000F, 14000F, 
//				agent1.getBankGuaranteeCashAdvance());		
//	//	travelAgentBD.removeAgent(agent1.getAgentCode());		
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.removeAgent(String)'
//	 * @throws ModuleException 
//	 */
//	public void testRemoveAgent() throws ModuleException {
//		Agent agent1 = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		agent.setStationCode("DEL");			
//		travelAgentBD.saveAgent(agent);		
//		agent1 = travelAgentBD.getAgent("DEL1");		
//		assertNotNull(agent1);
//		assertEquals(" incorrect code", "DEL1", agent1.getAgentCode());		
//		//travelAgentBD.removeAgent(agent1.getAgentCode());
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgents(int,
//	 * int)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgents() throws ModuleException {
//		Page agents = null;
//		int startIndex = 0;
//		int pageSize = 10; 
//    
//		TravelAgentBD travelAgentBD = getTravelAgentBD();			
//		agents = travelAgentBD.getAgents(startIndex, pageSize);	
//		assertNotNull(agents);
//		assertEquals(" Incorrect Size ", 10 , agents.getTotalNoOfRecords());
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getGSAs()'
//	 * @throws ModuleException 
//	 */
//	public void testGetGSAs() throws ModuleException {
//		Collection allGSAS = null;
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		allGSAS = travelAgentBD.getGSAs();
//		assertNotNull(allGSAS);	
//				
//		assertEquals(" Incorrect Size ", 8, allGSAS.size());
//		
//		Iterator iter = allGSAS.iterator();
//		
//		while(iter.hasNext()){
//			Agent element = (Agent) iter.next();
//	
//			if (element.getAgentCode().equals("10")) {
//				assertEquals(" Incorrect agent code ", "10", 
//						element.getAgentCode());
//				assertEquals(" Incorrect City ", "Damascus", 
//						element.getAddressCity());
//				assertEquals(" Incorrect agent type ", "GSA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect agent name ", "Julia Dumna", 
//						element.getAgentName());
//				assertEquals(" Incorrect Credit Limit", 1, 1, 
//						element.getCreditLimit());
//				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
//						element.getBankGuaranteeCashAdvance());		
//			}
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentsForGSA(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentsForGSA() throws ModuleException {
//		Collection agentsForGSA = null;	
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		String gsaId = "Y";
//			
//		agentsForGSA = travelAgentBD.getAgentsForGSA(gsaId);
//		
//		assertNotNull(agentsForGSA);
//		assertEquals(" Incorrect Size ", 2 , agentsForGSA.size());
//		
//		Iterator iter = agentsForGSA.iterator();		
//		
//		while (iter.hasNext()){	
//			Agent element = (Agent) iter.next();
//			
//			if (element.getAgentCode().equals("15")) {
//				assertEquals(" Incorrect agent code ", "15", 
//						element.getAgentCode());
//				assertEquals(" Incorrect City ", "Sharjah", 
//						element.getAddressCity());
//				assertEquals(" Incorrect agent type ", "TA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
//					element.getAgentName());
//				assertEquals(" Incorrect Credit Limit", 1, 1,
//						element.getCreditLimit());
//				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
//					element.getBankGuaranteeCashAdvance());
//				assertEquals(" Incorrect reporting GSA", "Y", 
//						element.getReportingToGSA());	
//			}
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentForAirport(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentForAirport() throws ModuleException {
//		Collection agentsForAirport = null;	
//		String airportcode = "CMB";
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//						
//		agentsForAirport = travelAgentBD.getAgentForStation(airportcode);
//		
//		assertNotNull(agentsForAirport);
//		assertEquals(" Incorrect Size ", 2 , agentsForAirport.size());		
//				
//		Iterator iter = agentsForAirport.iterator();		
//		
//		while (iter.hasNext()){			
//			Agent element = (Agent) iter.next();
//			
//			if (element.getAgentCode().equals("15")) {
//				assertEquals(" Incorrect agent code ", "15",
//						element.getAgentCode());
//				assertEquals(" Incorrect City ", "Sharjah", 
//						element.getAddressCity());
//				assertEquals(" Incorrect agent type ", "TA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
//						element.getAgentName());
//				assertEquals(" Incorrect Credit Limit", 1, 1,
//						element.getCreditLimit());
//				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
//						element.getBankGuaranteeCashAdvance());
//				assertEquals(" Incorrect reporting GSA", "Y", 
//						element.getReportingToGSA());
//				assertEquals(" Incorrect airport code ", "CMB", 
//						element.getStationCode());	
//			}
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentForTerritory(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentForTerritory() throws ModuleException {
//		Collection agentsForTerritory = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		String territorycode = "BBB";		
//		
//		agentsForTerritory = travelAgentBD.getAgentForTerritory(territorycode);
//		
//		assertNotNull(agentsForTerritory);
//		assertEquals(" Incorrect Size ", 2 , agentsForTerritory.size());		
//						
//		Iterator iter = agentsForTerritory.iterator();
//		
//		while(iter.hasNext()){
//		Agent element = (Agent) iter.next();
//
//			if (element.getAgentCode().equals("18")) {
//				assertEquals(" Incorrect agent code ", "18", 
//						element.getAgentCode());
//				assertEquals(" Incorrect City ", "Sharjah", 
//						element.getAddressCity());
//				assertEquals(" Incorrect agent type ", "TA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
//					element.getAgentName());
//				assertEquals(" Incorrect Credit Limit", 1, 1,
//						element.getCreditLimit());
//				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
//					element.getBankGuaranteeCashAdvance());
//				assertEquals(" Incorrect reporting to GSA", "Y", 
//						element.getReportingToGSA());
//				assertEquals(" Incorrect airport code ", "BOM", 
//						element.getStationCode());
//				assertEquals(" Incorrect territory code ", "BBB", 
//						element.getTerritoryCode());
//			}
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentForCountry(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentForCountry() throws ModuleException {
//		Collection agentsForCountry = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		String countrycode = "AM";
//				
//		agentsForCountry = travelAgentBD.getAgentForCountry(countrycode);
//		
//		assertNotNull(agentsForCountry);
//		assertEquals(" Incorrect Size ", 2 , agentsForCountry.size());
//		
//		Iterator iter = agentsForCountry.iterator();
//		
//		while(iter.hasNext()){
//			Agent element = (Agent) iter.next();			
//		
//			if (element.getAgentCode().equals("18")) {
//				assertEquals(" Incorrect agent code ", "18", 
//						element.getAgentCode());
//				assertEquals(" Incorrect City ", "Sharjah", 
//						element.getAddressCity());
//				assertEquals(" Incorrect agent type ", "TA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
//						element.getAgentName());
//				assertEquals(" Incorrect Credit Limit", 1, 1,
//						element.getCreditLimit());
//				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
//						element.getBankGuaranteeCashAdvance());
//				assertEquals(" Incorrect reporting GSA", "Y", 
//						element.getReportingToGSA());
//				assertEquals(" Incorrect airport code ", "BOM", 
//						element.getStationCode());
//				assertEquals(" Incorrect territory code ", "BBB", 
//						element.getTerritoryCode());		
//			}
//		}
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.assignAgentUser(String,
//	 * String)'
//	 * @throws ModuleException 
//	 */
//	public void testAssignAgentUser() throws ModuleException {
//		Page agentsUsers = null;
//		String agentCode = "15";
//		int startIndex = 0;
//		int pageSize = 10;
//				
//		TravelAgentBD travelAgentBD = getTravelAgentBD();		
//		travelAgentBD.assignAgentUser("U001", agentCode);
//		
//		agentsUsers = travelAgentBD.getAgentUsers(agentCode, startIndex, pageSize);	
//		assertEquals(" Incorrect Size ", 4, agentsUsers.getEndPosition());
//		
//		Iterator iter = agentsUsers.getPageData().iterator();
//				
//		while (iter.hasNext()){
//			User element = (User) iter.next();
//		
//			if (element.getUserId().equals("U001")) {
//				assertEquals(" Incorrect user id ", "U001", 
//						element.getUserId());
//				assertEquals(" Incorrect password ", "password", 
//						element.getPassword());
//				assertEquals(" Incorrect first name ", "Test User", 
//						element.getDisplayName());
//				//assertEquals(" Incorrect last name ", "Last Test", 
//				//		element.getLastName());
//				assertEquals(" Incorrect email id ", "test@jkcs.com", 
//						element.getEmailId());		
//			}		
//		}
//		travelAgentBD.unassignAgentUser("U001", agentCode);
//		agentsUsers = travelAgentBD.getAgentUsers(agentCode, startIndex, pageSize);	
//		assertEquals(" Incorrect Size ", 3, agentsUsers.getEndPosition());
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.unassignAgentUser(String,
//	 * String)'
//	 * @throws ModuleException 
//	 */
//	public void testUnassignAgentUser() throws ModuleException {
//		Page agentsUsers = null;
//		String agentCode = "15";
//		int startIndex = 0;
//		int pageSize = 10;
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();		
//		travelAgentBD.assignAgentUser("U001", agentCode);		
//		agentsUsers = travelAgentBD.getAgentUsers(agentCode, startIndex, pageSize);	
//		assertEquals(" Incorrect Size ", 4, agentsUsers.getEndPosition());
//		
//		Iterator iter = agentsUsers.getPageData().iterator();
//				
//		while (iter.hasNext()){
//			User element = (User) iter.next();
//		
//			if (element.getUserId().equals("U001")) {
//				assertEquals(" Incorrect user id ", "U001", 
//						element.getUserId());
//				assertEquals(" Incorrect password ", "password", 
//						element.getPassword());
//				assertEquals(" Incorrect first name ", "Test User", 
//						element.getDisplayName());
//				//assertEquals(" Incorrect last name ", "Last Test", 
//				//		element.getLastName());
//				assertEquals(" Incorrect email id ", "test@jkcs.com", 
//						element.getEmailId());	
//			}		
//		}
//		travelAgentBD.unassignAgentUser("U001", agentCode);
//		agentsUsers = travelAgentBD.getAgentUsers(agentCode, startIndex, pageSize);	
//		assertEquals(" Incorrect Size ", 3, agentsUsers.getEndPosition());
//	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentForUser(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentForUser() throws ModuleException {
//		Agent agent = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		String userId = "TEST_USER";
//		
//		assertNotNull(travelAgentBD);
//		
//		//agent = travelAgentBD.getAgentForUser(userId);		
//		assertNotNull(agent);
//		assertEquals(" Incorrect agent code ", "16", agent.getAgentCode());
//		assertEquals(" Incorrect City ", "Dubai", agent.getAddressCity());		
//		assertEquals(" Incorrect agent name ", "Al Kazim Travel Agency", 
//				agent.getAgentName());
//		assertEquals(" Incorrect teritory code ", "CMB", agent.getStationCode());
//	}
//	
////	/**
////	 * Test method for
////	 * 'com.isa.thinair.airtravelagents.core.service.bd
////	 * .TravelAgentServiceDelegateImpl.searchAgents(Set)'
////	 */
////	public void testSearchAgents() {
////
////
////	}
//
//	/**
//	 * Test method for
//	 * 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentServiceDelegateImpl.getAgentTypes()'
//	 * @throws ModuleException 
//	 */
//	public void testGetAgentTypes() throws ModuleException {
//		Collection agentTypes = null;
//		
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		agentTypes = travelAgentBD.getAgentTypes();
//		assertNotNull(agentTypes);
//		
//		assertEquals(" Incorrect Size ", 6, agentTypes.size());
//	
//		Iterator iter = agentTypes.iterator();
//		
//		while (iter.hasNext()) {
//			AgentType element = (AgentType) iter.next();
//			
//			if (element.getAgentTypeCode().equals("GSA")) {
//				assertEquals(" Incorrect agent type code ", "GSA", 
//						element.getAgentTypeCode());
//				assertEquals(" Incorrect discription ", "Genaral Sales Agent", 
//						element.getAgentTypeDescription());			
//			}
//		}
//	}
//	
//	/**
//	 * @return .
//	 * @throws ModuleException 
//	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
//	 * .AgentDAO#getAllAgentNames()
//	 */
//	public void testGetAllAgentNames() throws ModuleException {
//		Hashtable hashtable = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		hashtable = travelAgentBD.getAllAgentNames();
//		assertNotNull(hashtable);		
//	
//		assertEquals("Incorrect size ", 576, hashtable.size());			
//		assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
//				hashtable.get("15"));		
//	}
//	
//	/** 
//	 * @throws ModuleException 
//	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
//	 * .AgentDAO#getGSAsForTerritories()
//	 */
//	public void testGetGSAsForTerritories() throws ModuleException {		
//		Collection gSAsForTerritories = null; 
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		assertNotNull(travelAgentBD);
//		
//		
//		gSAsForTerritories = travelAgentBD.getGSAsForTerritories();		
//		assertEquals(" Incorrect size ", 14, gSAsForTerritories.size());
//		
//		Iterator iter = gSAsForTerritories.iterator();
//System.out.println("  WWWWWWWWWWWWWWWWWWWWWW  "+gSAsForTerritories);
//		while(iter.hasNext()){
//			Agent element = (Agent) iter.next();			
//	System.out.println("  ************* "+element.getAgentCode());		
//			if (element.getAgentCode().equals("18")) {
//				assertEquals(" Incorrect agent code ", "18", 
//						element.getAgentCode());				
//				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
//						element.getAgentName());				
//				assertEquals(" Incorrect territory code ", "BBB", 
//						element.getTerritoryCode());		
//			}
//		}	
//	}
//	
//	/**
//	 * @return .
//	 * @throws ModuleException 
//	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO
//	 * #getUnassignUsers()
//	 */
//	public void testGetUnassignUsers() throws ModuleException {
//		Collection users = null;
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		assertNotNull(travelAgentBD);
//		users = travelAgentBD.getUnassignUsers();
//	
//		assertEquals("Incorrect size ", 8, users.size());
//		
//		Iterator iter = users.iterator();
//		
//		while (iter.hasNext()) {
//			User element = (User) iter.next();
//			
//			if (element.getUserId().equals("T 005")) {
//				assertEquals(" Incorrect user id ","T 005", element.getUserId());
//				assertEquals(" Incorrect first name ", "Chaminda", 
//						element.getDisplayName());
//				//assertEquals(" Incorrect last name ", "Perera", 
//				//		element.getLastName());		
//				assertEquals(" Incorrect email address ", "chamindap@jkcsworld.com", 
//						element.getEmailId());
//			}			
//		}	
//	}
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getCreditHistoryForAgent(String)'
//	 * @throws ModuleException 
//	 */
//	public void testGetCreditHistoryForAgent() throws ModuleException {
//		Collection agentCreditHistoryCollection = null;
//		String agentcode = "16";
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		assertNotNull(travelAgentBD);
//		agentCreditHistoryCollection = travelAgentBD.getCreditHistoryForAgent(agentcode);
//
//		assertNotNull(agentCreditHistoryCollection);		
//		Iterator iter = agentCreditHistoryCollection.iterator();
//		
//		while (iter.hasNext()) {
//			AgentCreditHistory element = (AgentCreditHistory) iter.next();
//			
//			if (element.getAgentCode().equals("16")) {		
//				assertEquals(" Incorrect agent code ", "16", 
//						element.getAgentCode());		
//				//assertEquals(" Incorrect Modified by ", "ABCD", 
//				//		element.getModifiedBy());		
//				//assertEquals(" Incorrect modified date ",  
//				//		new GregorianCalendar(2005, 7, 11).getTime(), 
//				//		element.getModifiedDate());
//				assertEquals(" Incorrect credit limit amount ", 10000F, 10000F,
//						element.getCreditLimitamount());
//				assertEquals(" Incorrect credit history id", 1, 
//						element.getAgnCrHisId());
//				//assertEquals(" Incorrect creted date ",  
//				//		new GregorianCalendar(2005, 7, 10).getTime(), 
//				//		element.getCreatedDate());
//				//assertEquals(" Incorrect creted date ",  
//				//		new GregorianCalendar(2005, 7, 10).get(Calendar.MONTH), 
//				//		element.getCreatedDate().getMonth());
//			}			
//		}
//	}
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.adjustCreditLimit(String, double, double,
//	 *  String, boolean)'
//	 * @throws ModuleException 
//	 */
//	public void testAdjustCreditLimit() throws ModuleException {		
//		String agentcode = "16";
//		double currentCreditLimit = 10000;	
//		double newCreditLimit = 2050;
//		String remarks = "Adjust";
//		boolean forceCancel = false;
//		String loginId = "16";
//		
//		TravelAgentBD travelAgentBD = getTravelAgentBD();
//		
//		assertNotNull(travelAgentBD);
//		
//        //FIXME
////		try {
////			travelAgentBD.adjustCreditLimit(agentcode, currentCreditLimit, 
////					newCreditLimit, remarks, forceCancel, loginId);
////		} catch (ModuleException e) {
////			assertEquals(" Incorrect exception code ",
////					"airtravelagent.logic.credit.error", e.getExceptionCode());			
////		}
//	}
//	
//	/**
//	 * Private method to get TravelAgentBD reference.
//	 * @return AirCustomerServiceBD
//	 */
//	private TravelAgentBD getTravelAgentBD() { 
//		TravelAgentBD travelAgentServiceDelegate = null;
//		
//		LookupService lookup = LookupServiceFactory.getInstance();
//		IModule airtravelagentsModule = lookup.getModule("airtravelagents");
//	System.out.println("Travel Agent module looked up successful .........");		
//		try {
//			travelAgentServiceDelegate = (TravelAgentBD) airtravelagentsModule
//				.getServiceBD("airtravelagents.service.remote");
//	System.out.println("Travel Agent module delegate creation successful...");
//			return travelAgentServiceDelegate;
//			
//		} catch (Exception e) {
//			System.out.println("Exception in creating delegate");
//			e.printStackTrace();
//		}
//		return travelAgentServiceDelegate;
//	}
}