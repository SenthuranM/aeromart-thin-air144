/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.remoting.ejb;

import java.util.Collection;
import java.util.GregorianCalendar;

import com.isa.thinair.airtravelagents.api.model.AgentCollection;
import com.isa.thinair.airtravelagents.api.model.InvoiceSettlement;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentCollectionDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestTravelAgentFinanceDelegate extends PlatformTestCase {

	AgentCollectionDAO agentCollectionDAO = null;
	public static String AGENTCOLLECTIONDAO = "agentCollectionDAO";
	
	/**
	 * Sets up the environment.
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		agentCollectionDAO = agentCollectionDAO 
			= (AgentCollectionDAO) AirTravelagentsModuleUtils
		.getDAO(AGENTCOLLECTIONDAO);
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
	 * .TravelAgentFinanceDelegateImpl.postAgentCollection(String,
	 * double, double, AgentCollection, boolean)'
	 * @throws ModuleException 
	 */
	public void testPostAgentCollection() throws ModuleException {
		String agentCode = "16";		
		double creditLimit = 1500;
		double availableCredit = 20000;
		AgentCollection agentCollection = null;
		boolean forceCancel = false; 
		Collection agengtcollection = null;
		TravelAgentFinanceBD travelAgentFinanceBD = null;
		
		agentCollection = new AgentCollection();
		agentCollection.setAgentCode(agentCode);		
		agentCollection.setCancelationDate(new GregorianCalendar(2005, 10, 10)
				.getTime());
		agentCollection.setChequeNumber("1002");
		//agentCollection.setCmCode("CSH");
		agentCollection.setCmCode("CHQ");
		agentCollection.setCpCode("ADV");
		//agentCollection.setCpCode("INV");
		agentCollection.setDateOfPayment(new GregorianCalendar(2005, 9, 10)
				.getTime());
		agentCollection.setPaymentAmount(10000);
		agentCollection.setRemarks("Remarks BD");
		agentCollection.setStatus("P");		
		
		travelAgentFinanceBD = getTravelAgentFinanceBD();		
		travelAgentFinanceBD.postAgentCollection(agentCode, creditLimit,
				availableCredit, agentCollection, forceCancel);

		assertNotNull(agentCollectionDAO);
		agengtcollection = agentCollectionDAO.getAgentCollections(agentCode);
		assertNotNull(agengtcollection);
		
//		Iterator iter = agengtcollection.iterator();
//		
//		while (iter.hasNext()) {
//			AgentCollection element = (AgentCollection) iter.next();
//			
//			if (element.getAgentCode().equals("16") 
//					&& element.getReceiptNumber() == 27) {
//				assertEquals(" Incorrect agent code ", "16", 
//						element.getAgentCode());				
//				assertEquals(" Incorrect payment amount ", 10000, 
//						element.getPaymentAmount(), 1);
//				assertEquals(" Incorrect date of payment", 
//						new GregorianCalendar(2005, 9, 10).getTime(),
//						element.getDateOfPayment());
//				assertEquals(" Incorrect cm code ", "CSH",  
//						element.getCmCode());	
//				assertEquals(" Incorrect cp code ", "ADV",  
//						element.getCpCode());
//				assertEquals(" Incorrect Status ", "Paid @ BD", 
//						element.getStatus());
//				assertEquals(" Incorrect cheque number ", "1002", 
//						element.getChequeNumber());
//			}			
//		}
//		creditLimit = 15000; 
//		agentCollection.setCpCode("INV");
//		agentCollection.setPaymentAmount(8000);
//		
//		travelAgentFinanceBD.postAgentCollection(agentCode, creditLimit,
//				availableCredit, agentCollection, forceCancel);
//		
//		assertNotNull(agentCollectionDAO);
//		agengtcollection = agentCollectionDAO.getAgentCollections(agentCode);
//		assertNotNull(agengtcollection);
//		
//		Iterator iter1 = agengtcollection.iterator();
//		
//		while (iter1.hasNext()) {
//			AgentCollection element = (AgentCollection) iter1.next();
//			
//			if (element.getAgentCode().equals("16") 
//					&& element.getReceiptNumber() == 40) {
//				assertEquals(" Incorrect agent code ", "16", 
//						element.getAgentCode());				
//				assertEquals(" Incorrect payment amount ", 8000, 
//						element.getPaymentAmount(), 1);
//				assertEquals(" Incorrect date of payment", 
//						new GregorianCalendar(2005, 9, 10).getTime(),
//						element.getDateOfPayment());
//				assertEquals(" Incorrect cm code ", "CSH",  
//						element.getCmCode());	
//				assertEquals(" Incorrect cp code ", "ADV",  
//						element.getCpCode());
//				assertEquals(" Incorrect Status ", "Paid @ BD", 
//						element.getStatus());
//				assertEquals(" Incorrect cheque number ", "1002", 
//						element.getChequeNumber());
//			}			
//		}
//		
//		creditLimit = 1500;
//		try {
//			agentCollection.setCpCode("INV");			
//			travelAgentFinanceBD.postAgentCollection(agentCode, creditLimit,
//					availableCredit, agentCollection, forceCancel);	
//		} catch (ModuleException e) {			
//			assertEquals(" Incorrect exception code ", 
//				"TRAVEL_AGENT_CREDIT_LIMIT_EXCEEDS", e.getExceptionCode());
//		}		
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
	 * .TravelAgentFinanceDelegateImpl.reversePayments(String, int, double,
	 *  Hashtable)'
	 * @throws ModuleException 
	 */
/*	public void testReversePayments() throws ModuleException {
		int receiptNumber = 25;
		double receipAmount = 1000;
		String agentCode = "16";
		String[] reason = {"AAA","BBB","CCC"} ;
		//String cpCode = "INV";
		String cpCode = "ADV";		
		double balance = 0;
		Hashtable reverselValues = null;		
		TravelAgentFinanceBD travelAgentFinanceBD = null;
		
		reverselValues = new Hashtable();		
		reverselValues.put("0410020001", "1");
		reverselValues.put("0410020002", "100");
		reverselValues.put("0410020003", "10");
		balance = 100;
		travelAgentFinanceBD = getTravelAgentFinanceBD();
System.out.println("  SIZEEEEE "+reverselValues.size());
		travelAgentFinanceBD.reversePayments(agentCode, receiptNumber,
				receipAmount, reverselValues, cpCode, balance, reason);

	}*/

//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.searchAgentCollections(List)'
//	 * @throws ModuleException 
//	 */
//	public void testSearchAgentCollections() throws ModuleException {
//		TravelAgentFinanceBD travelAgentFinanceBD = null;
//		
//		int receiptNumbere = 26;
//		List criteria = new ArrayList();	
//		
//		List listValues = new ArrayList();
//		listValues.add(new Integer(receiptNumbere));		
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion.setFieldName("receiptNumber");
//		criterion.setValue(listValues);		
//		
//				
//		String agentCode = "16";		
//		List listValues2 = new ArrayList();
//		listValues2.add(agentCode);		
//		ModuleCriterion criterion2 = new ModuleCriterion();
//		criterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		criterion2.setFieldName("agentCode");
//		criterion2.setValue(listValues2);
//		
//		
//		criteria.add(criterion2);
//		criteria.add(criterion);
//		
//		travelAgentFinanceBD = getTravelAgentFinanceBD();
//		
//		Collection agengtcollection  = travelAgentFinanceBD.searchAgentCollections(criteria);
//		
//		Iterator iter = agengtcollection.iterator();
//		
//		while (iter.hasNext()) {
//			AgentCollection element = (AgentCollection) iter.next();
//			
//			if (element.getAgentCode().equals("16") 
//					&& element.getReceiptNumber() == 26) {
//				assertEquals(" Incorrect agent code ", "16", 
//						element.getAgentCode());				
//				assertEquals(" Incorrect payment amount ", 10000, 
//						element.getPaymentAmount(), 1);
//				assertEquals(" Incorrect date of payment", 
//						new GregorianCalendar(2005, 9, 10).getTime(),
//						element.getDateOfPayment());
//				assertEquals(" Incorrect cm code ", "CSH",  
//						element.getCmCode());	
//				assertEquals(" Incorrect cp code ", "ADV",  
//						element.getCpCode());
//				assertEquals(" Incorrect Status ", "Paid @ BD", 
//						element.getStatus());
//				assertEquals(" Incorrect cheque number ", "1002", 
//						element.getChequeNumber());				
//			}
//		}	
//	}

	/**
	 * @throws ModuleException 
	 * 
	 *
	 */
//	public void testGenerateInvoices() throws ModuleException {		
//		TravelAgentFinanceBD travelAgentFinanceBD = null; 
//
//		Date fromDate = new GregorianCalendar(2005, 7, 10).getTime();		
//		Date toDate = new GregorianCalendar(2005, 8, 17).getTime();	
//		
//System.out.println("  777777777777777777777777777777 ");
//		
//		travelAgentFinanceBD = getTravelAgentFinanceBD();
//		
//		assertNotNull(travelAgentFinanceBD);
//		travelAgentFinanceBD.generateInvoices(fromDate, toDate);
//		
//System.out.println("  88888888888888888888888888888888 ");
//	}
	

//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getCollectionDetails(int)'
//	 * @throws ModuleException 
//	 */
//	public void testGetCollectionDetails() {
//		TravelAgentFinanceBD travelAgentFinanceBD = null; 
//
//		
//		travelAgentFinanceBD = getTravelAgentFinanceBD();
//		
//		try {
//			travelAgentFinanceBD.getCollectionDetails(166);
//		} catch (ModuleException e) {
//			assertEquals(" Incorrect exception code", "airtravelagent" +
//					".logic.receipt.already.reversed", 	e.getExceptionCode());
//		}
//
//	}
	
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
	 * .TravelAgentFinanceDelegateImpl.settleInvoice(String, String, double,
	 *  String, InvoiceSettlement)'
	 * @throws ModuleException 
	 */
	public void testSettleInvoice() throws ModuleException {
		double amount = 100;
		String agentCode = "16";
		String invoiceNumber = "0410020003";		
		//String cp = "INV";
		String cp = "ADV";
		InvoiceSettlement settlement = null;
		TravelAgentFinanceBD travelAgentFinanceBD = null;
		
		settlement = new InvoiceSettlement();		
		settlement.setAmountPaid(11110);
		settlement.setCpCode("INV");		
		settlement.setInvoiceNo(invoiceNumber);
		settlement.setSettlementDate(new GregorianCalendar().getTime());
		
		travelAgentFinanceBD = getTravelAgentFinanceBD();	
	
		travelAgentFinanceBD.settleInvoice(agentCode, invoiceNumber, amount, cp,
				settlement);		
	//	invoiceNumber = "4";	
	//	amount = 2450;
	//	try {
	//		travelAgentFinanceBD.settleInvoice(agentCode, invoiceNumber, amount, cp,
	//				settlement);
//		} catch (ModuleException e) {
//			assertEquals(" Incorrect exception code ", 
//					"TRAVEL_AGENT_INVOICEAMOUNT_EXCEEDS", e.getExceptionCode());
//		}	
	}

//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getSettledInvoicesForAgent(List, int,int)'
//	 */
//	public void testGetSettledInvoicesForAgent() {	
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getUnsettledInvoicesForAgent(String)'
//	 */
//	public void testGetUnsettledInvoicesForAgent() {		
//
//	}
//	
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getSettlementsForInvoice(String)'
//	 */
//	public void testGetSettlementsForInvoice() {		
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.invalidateSettlement(InvoiceSettlement)'
//	 */
//	public void testInvalidateSettlement() {		
//
//	}	
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getSettlementsForAgent(String)'
//	 */
//	public void testGetSettlementsForAgent() {		
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.getReceiptDetails(String, String,
//	 * double, Hashtable)'
//	 */
//	public void testGetReceiptDetails() {		
//
//	}
//	
//
//	/**
//	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
//	 * .TravelAgentFinanceDelegateImpl.searchAgentReceipts(String)'
//	 */
//	public void testSearchAgentReceipts() {
//		
//
//	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.service.bd
	 * .TravelAgentFinanceDelegateImpl.searchInvoiceForAgent(List)'
	 * @throws ModuleException 
	 */
/*	public void testSearchInvoiceForAgent() throws ModuleException {
		TravelAgentFinanceBD travelAgentFinanceBD = null;
		travelAgentFinanceBD = getTravelAgentFinanceBD();
		
		assertNotNull(travelAgentFinanceBD);
		
		
		String agentCode = "15";		
		String year = "2005";
		String month = "9";
		int billingPeriod = 2;
		
		
		InvoiceSummaryDTO inv = travelAgentFinanceBD.searchInvoiceForAgent(agentCode, year,
				month, billingPeriod);		
		assertEquals(" Incorrect invoice number ", "0410020004", 
				inv.getInvoice().getInvoiceNumber());	
		assertEquals(" Incorrect agent code ", "15", 
				inv.getInvoice().getAgentCode());
		assertEquals(" Incorrect date ", new GregorianCalendar(2005, 9, 25)
				.getTime(), inv.getInvoice().getInvoiceDate());
		assertEquals(" Incorrect invoice amount ", 2000,  
				inv.getInvoice().getInvoiceAmount(), 1);
		assertEquals(" Incorrect settle amount ", 2000,  
				inv.getInvoice().getSettledAmount(), 1);
		assertEquals(" Incorrect Status ", "SETTLED", 
				inv.getInvoice().getStatus());
		assertEquals(" Incorrect Address ", "PO Box 29666 DeiraDubai Sharjah Sharjah", 
				inv.getAgentAddress());

	}*/
	
	/**
	 * Private method to get the Locatin BD.
	 * @return LocationBD.
	 */
	private TravelAgentFinanceBD getTravelAgentFinanceBD() { 
		TravelAgentFinanceBD travelAgentFinanceBD = null;
		
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule travelAgentModule = lookup.getModule("airtravelagents");
	System.out.println("Travel Agent Finance module looked up successful ......");	
		travelAgentFinanceBD = (TravelAgentFinanceBD) travelAgentModule
		.getServiceBD("airtravelagentsfinance.service.remote");
	System.out.println("Travel Agent Finance module finance bd creation successful....");
		
		return travelAgentFinanceBD;		
	}
	
}