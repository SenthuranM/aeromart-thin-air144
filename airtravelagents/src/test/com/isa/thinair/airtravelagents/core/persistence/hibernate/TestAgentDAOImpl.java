/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airtravelagents.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentCreditHistory;
import com.isa.thinair.airtravelagents.api.model.AgentSummary;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 * 
 */
public class TestAgentDAOImpl extends PlatformTestCase {
	
	Agent agent = null;
	AgentDAO agentDAO = null;
	LookupService lookup = null;
	
	/**
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		lookup = LookupServiceFactory.getInstance();
		agentDAO = (AgentDAO)lookup.getBean("isa:base://modules/" 
				+ "airtravelagents?id=agentDAOProxy");
		
		agent = new Agent();		
		agent.setAccountCode("ac01");
		agent.setAddressCity("London");
		agent.setAddressLine2("London 1");
		agent.setAddressLine2("London 2");
		agent.setAddressStateProvince("South");
		agent.setAgentIATANumber("UK");
		agent.setAgentName("London agent");
		agent.setAgentTypeCode("OT");
		agent.setBankGuaranteeCashAdvance(14000.0F);
		agent.setCreditLimit(10000);
		agent.setEmailId("A@bc");
		agent.setReportingToGSA("N");
		agent.setStatus("Active");
	}

	/**
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgent(String)'
	 */
	public void testGetAgent() {
		Agent agent1 = null;
		
		assertNotNull(agentDAO);
		agent.setStationCode("DEL");
		
		//agentDAO.saveAgent(agent);
		agent1 = agentDAO.getAgent("DEL1");	
		assertNotNull(agent1);	
		assertEquals(" Incorrect agent code ", "DEL1", agent1.getAgentCode());
		assertEquals(" Incorrect City ", "London", agent1.getAddressCity());
		assertEquals(" Incorrect email ", "A@bc", agent1.getEmailId());
		assertEquals(" Incorrect agent name ", "London agent", 
				agent1.getAgentName());
		assertEquals(" Incorrect Credit Limit", 10000F, 10000F, 
				agent1.getCreditLimit());
		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 14000F, 14000F, 
				agent1.getBankGuaranteeCashAdvance());					
		//agentDAO.removeAgent(agent1.getAgentCode());		
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentUsers()'
	 */
	public void testGetAgentUsers() {
		Page agentsUsers = null;		
		String agentCode = "16";
		int startIndex = 0;
		int pageSize = 10;

		assertNotNull(agentDAO);		
		agentsUsers = agentDAO.getAgentUsers(agentCode, startIndex, pageSize);	
		assertEquals(" Incorrect Size ", 1, agentsUsers.getPageData().size());
		
		Iterator iter = agentsUsers.getPageData().iterator();
				
		while (iter.hasNext()){
			User element = (User) iter.next();
		
			if (element.getUserId().equals("TEST_USER")) {
				assertEquals(" Incorrect user id ", "TEST_USER", 
						element.getUserId());
				assertEquals(" Incorrect password ", "NASLY", 
						element.getPassword());
				//assertEquals(" Incorrect first name ", "NASLY MOHAMED", 
				//		element.getFirstName());
				//assertEquals(" Incorrect last name ", "YOOSUF", 
				//		element.getLastName());
				assertEquals(" Incorrect email id ", "nasly@jkcsworld.com", 
						element.getEmailId());		
			}		
		}
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.saveAgent(Agent)'
	 */
	public void testSaveAgent() {
		Agent agent1 = null;
		AgentSummary agentSummary = null;
		
		Set payCode = new HashSet();
		agentSummary = new AgentSummary();		
		agentSummary.setAvailableCredit(agent.getCreditLimit());
		agentSummary.setAvailableFundsForInv(10);
		agentSummary.setAvailableAdvance(100);		
		payCode.add("CC");
		payCode.add("OA");
		payCode.add("CA");
		agent.setPaymentMethod(payCode);		
		agent.setStationCode("DEL");		
		agent.setAgentSummary(agentSummary);
		
		assertNotNull(agentDAO);
		//agentDAO.saveAgent(agent);		
		agent1 = agentDAO.getAgent("DEL1");		

		assertEquals(" Incorrect agent code ", "DEL1", agent1.getAgentCode());
		assertEquals(" Incorrect City ", "London", agent1.getAddressCity());
		assertEquals(" Incorrect email ", "A@bc", agent1.getEmailId());
		assertEquals(" Incorrect agent name ", "London agent", 
				agent1.getAgentName());
		assertEquals(" Incorrect Credit Limit", 10000F, 10000F, 
				agent1.getCreditLimit());
		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 14000F, 14000F, 
				agent1.getBankGuaranteeCashAdvance());
		assertEquals(" Incorrect agent payment method size ", 3,
				agent1.getPaymentMethod().size());
		
		Iterator iter = agent1.getPaymentMethod().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			
			if(element.equals("OA")) {
				assertEquals(" Incorrect agent payment method code ", "OA",
						element);			
			}			
		}
		
		assertEquals(" Incorrect agent summary credit Limit", 10000F,
				agent1.getAgentSummary().getAvailableCredit(), 1);
		assertEquals(" Incorrect agent summary code ", "DEL1", 
				agent1.getAgentSummary().getAgentCode());
		assertEquals(" Incorrect agent summary available advance ", 100, 
				agent1.getAgentSummary().getAvailableAdvance(), 1);
		assertEquals(" Incorrect agent summary available funds to invoice ", 10, 
				agent1.getAgentSummary().getAvailableFundsForInv(), 1);		
		//agentDAO.removeAgent(agent1.getAgentCode());	
	}
	
	/**
	 * Test method for update agent.
	 */
	public void testUpdateAgent() {	
		Agent agent1 = null;
		AgentSummary agentSummary = null;
		
		Set payCode = new HashSet();
		agentSummary = new AgentSummary();		
		agentSummary.setAvailableCredit(agent.getCreditLimit());
		agentSummary.setAvailableFundsForInv(10);
		agentSummary.setAvailableAdvance(100);		
		payCode.add("CC");
		payCode.add("OA");
		payCode.add("CA");
		
		agent.setPaymentMethod(payCode);
		agent.setStationCode("BOM");		
		agent.setAgentSummary(agentSummary);
		
		assertNotNull(agentDAO);
		//agentDAO.saveAgent(agent);		
		agent1 = agentDAO.getAgent("BOM1");
		
		assertEquals(" Incorrect agent code ", "BOM1", agent1.getAgentCode());
		assertEquals(" Incorrect City ", "London", agent1.getAddressCity());
		assertEquals(" Incorrect email ", "A@bc", agent1.getEmailId());
		assertEquals(" Incorrect agent name ", "London agent", 
				agent1.getAgentName());
		assertEquals(" Incorrect Credit Limit", 10000F, 
				agent1.getCreditLimit(), 1);
		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 14000F, 
				agent1.getBankGuaranteeCashAdvance(), 1);
		assertEquals(" Incorrect agent payment method size ", 3,
				agent1.getPaymentMethod().size());
		
		Iterator iter = agent1.getPaymentMethod().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			
			if(element.equals("OA")) {
				assertEquals(" Incorrect agent payment method code ", "OA",
						element);			
			}			
		}
		
		assertEquals(" Incorrect agent summary credit Limit", 10000F,
				agent1.getAgentSummary().getAvailableCredit(), 1);
		assertEquals(" Incorrect agent summary code ", "BOM1", 
				agent1.getAgentSummary().getAgentCode());
		assertEquals(" Incorrect agent summary available advance ", 100, 
				agent1.getAgentSummary().getAvailableAdvance(), 1);
		assertEquals(" Incorrect agent summary available funds to invoice ", 10, 
				agent1.getAgentSummary().getAvailableFundsForInv(), 1);		
		
		payCode.remove("CA");
		//payCode.remove("CC");
		Agent ag = new Agent();
		ag.setAgentCode(agent1.getAgentCode());
		ag.setVersion(agent1.getVersion());
		
		ag.setAccountCode("ac01");
		ag.setAddressCity("London ABC");
		ag.setAddressLine2("Colombo");
		ag.setAddressLine2("Update Road");
		ag.setAddressStateProvince("West");
		ag.setAgentIATANumber("UXK");
		ag.setAgentName("Al Bush Go");
		ag.setAgentTypeCode("OT");
		ag.setBankGuaranteeCashAdvance(15000.0F);
		ag.setCreditLimit(25000);
		ag.setEmailId("AX@bc");
		ag.setReportingToGSA("N");
		ag.setStatus("Active");	
		ag.setPaymentMethod(payCode);
		
		agentSummary.setAgentCode(agent1.getAgentSummary().getAgentCode());
		agentSummary.setVersion(agent1.getAgentSummary().getVersion());
		agentSummary.setAvailableCredit(ag.getCreditLimit());
		agentSummary.setAvailableFundsForInv(1500);
		agentSummary.setAvailableAdvance(1250);
		ag.setAgentSummary(agentSummary);
		
		//agentDAO.saveAgent(ag);	
		
		agent1 = agentDAO.getAgent("BOM1");
		
		assertEquals(" Incorrect agent code ", "BOM1", agent1.getAgentCode());
		assertEquals(" Incorrect City ", "London ABC", agent1.getAddressCity());
		assertEquals(" Incorrect email ", "AX@bc", agent1.getEmailId());
		assertEquals(" Incorrect agent name ", "Al Bush Go", 
				agent1.getAgentName());
		assertEquals(" Incorrect Credit Limit", 25000F, 
				agent1.getCreditLimit(), 1);
		assertEquals(" Incorrect BankGuaranteeCashAdvance ", 15000F,  
				agent1.getBankGuaranteeCashAdvance(), 1);
		assertEquals(" Incorrect agent payment method size ", 2,
				agent1.getPaymentMethod().size());
		
		Iterator iter1 = agent1.getPaymentMethod().iterator();
		while (iter1.hasNext()) {
			String element = (String) iter1.next();
			
			if(element.equals("OA")) {
				assertEquals(" Incorrect agent payment method code ", "OA",
						element);			
			}			
		}		
		assertEquals(" Incorrect agent summary credit Limit", 25000F,
				agent1.getAgentSummary().getAvailableCredit(), 1);
		assertEquals(" Incorrect agent summary code ", "BOM1", 
				agent1.getAgentSummary().getAgentCode());
		assertEquals(" Incorrect agent summary available advance ", 1250, 
				agent1.getAgentSummary().getAvailableAdvance(), 1);
		assertEquals(" Incorrect agent summary available funds to invoice ", 1500, 
				agent1.getAgentSummary().getAvailableFundsForInv(), 1);
		//agentDAO.removeAgent(agent1.getAgentCode());
	}	
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.removeAgent(String)'
	 */
	public void testRemoveAgent() {
		Agent agent1 = null;
		
		agent.setStationCode("DEL");			
		assertNotNull(agentDAO);
		//agentDAO.saveAgent(agent);
		
		agent1 = agentDAO.getAgent("DEL1");
		assertNotNull(agent1);
		//agentDAO.removeAgent(agent1.getAgentCode());	
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgents(int, int)'
	 */
	public void testGetAgents() {
		Page agents = null;
		int startIndex = 0;
		int pageSize = 10; 
    
		assertNotNull(agentDAO);		
		agents = agentDAO.getAgents(startIndex, pageSize);
		assertNotNull(agents);
		assertEquals(" Incorrect Size ", 10 , agents.getEndPosition());
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentsForGSA(String)'
	 */
	public void testGetAgentsForGSA() {
		Collection agentsForGSA = null;	
		String gsaId = "Y";

		assertNotNull(agentDAO);		
		agentsForGSA = agentDAO.getAgentsForGSA(gsaId);
		assertEquals(" Incorrect Size ", 6 , agentsForGSA.size());
		
		Iterator iter = agentsForGSA.iterator();	
		
		while (iter.hasNext()){	
			Agent element = (Agent) iter.next();		
			
			if (element.getAgentCode().equals("15")) {
				assertEquals(" Incorrect agent code ", "15", 
						element.getAgentCode());
				assertEquals(" Incorrect City ", "Sharjah", 
						element.getAddressCity());
				assertEquals(" Incorrect agent type ", "TA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
					element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1, 1,
						element.getCreditLimit());
				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
					element.getBankGuaranteeCashAdvance());
				assertEquals(" Incorrect re4porting to GSA", "Y", 
						element.getReportingToGSA());	
			}
		}
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentForAirport(String)'
	 */
	public void testGetAgentForAirport() {
		Collection agentsForAirport = null;	
		String airportcode = "CMB";

		assertNotNull(agentDAO);		
		agentsForAirport = agentDAO.getAgentForStation(airportcode);
		assertEquals(" Incorrect Size ", 5 , agentsForAirport.size());		
				
		Iterator iter = agentsForAirport.iterator();
		
		while (iter.hasNext()){
			Agent element = (Agent) iter.next();
				
			if (element.getAgentCode().equals("15")) {
				assertEquals(" Incorrect agent code ", "15",
						element.getAgentCode());
				assertEquals(" Incorrect City ", "Sharjah", 
						element.getAddressCity());
				assertEquals(" Incorrect agent type ", "TA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
						element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1, 1, 
						element.getCreditLimit());
				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
						element.getBankGuaranteeCashAdvance());
				assertEquals(" Incorrect Reporting GSA ", "Y", 
						element.getReportingToGSA());
				assertEquals(" Incorrect airport code ", "CMB", 
						element.getStationCode());	
			}
		}
	}

	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentForTerritory(String)'
	 */
	public void testGetAgentForTerritory() {
		Collection agentsForTerritory = null;
		String territorycode = "BBB";		

		assertNotNull(agentDAO);		
		agentsForTerritory = agentDAO.getAgentForTerritory(territorycode);
		assertEquals(" Incorrect Size ", 3 , agentsForTerritory.size());		
						
		Iterator iter = agentsForTerritory.iterator();
		
		while(iter.hasNext()){
			Agent element = (Agent) iter.next();
	
			if (element.getAgentCode().equals("18")) {
				assertEquals(" Incorrect agent code ", "18", 
						element.getAgentCode());
				assertEquals(" Incorrect City ", "Sharjah", 
						element.getAddressCity());
				assertEquals(" Incorrect agent type ", "GSA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
					element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1, 1,
						element.getCreditLimit());
				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
					element.getBankGuaranteeCashAdvance());
				assertEquals(" Incorrect  reporting GSA", "Y", 
						element.getReportingToGSA());
				assertEquals(" Incorrect airport code ", "BOM", 
						element.getStationCode());
				assertEquals(" Incorrect territory code ", "BBB", 
						element.getTerritoryCode());		
			}
		}
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentForCountry(String)'
	 */
	public void testGetAgentForCountry() {
		Collection agentsForCountry = null;		
		String countrycode = "AM";
		
		assertNotNull(agentDAO);		
		agentsForCountry = agentDAO.getAgentsForCountry(countrycode);
		assertEquals(" Incorrect Size ", 3 , agentsForCountry.size());
		
		Iterator iter = agentsForCountry.iterator();
		
		while(iter.hasNext()){
			Agent element = (Agent) iter.next();
	
			if (element.getAgentCode().equals("18")) {
				assertEquals(" Incorrect agent code ", "18", 
						element.getAgentCode());
				assertEquals(" Incorrect City ", "Sharjah", 
						element.getAddressCity());
				assertEquals(" Incorrect agent type ", "GSA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
						element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1, 1,
						element.getCreditLimit());
				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1, 1, 
						element.getBankGuaranteeCashAdvance());
				assertEquals(" Incorrect reporting GSA", "Y", 
						element.getReportingToGSA());
				assertEquals(" Incorrect airport code ", "BOM", 
						element.getStationCode());
				assertEquals(" Incorrect territory code ", "BBB", 
						element.getTerritoryCode());		
			}
		}
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentForUser(String)'
	 */
	public void testGetAgentForUser() {
		Agent agent = null;
		
		String userId = "TEST_USER";
		
		assertNotNull(agentDAO);	
		//agent = agentDAO.getAgentForUser(userId);
		assertNotNull(agent);
		assertEquals(" Incorrect agent code ", "16", agent.getAgentCode());
		assertEquals(" Incorrect City ", "Dubai", agent.getAddressCity());		
		assertEquals(" Incorrect agent name ", "Al Kazim Travel Agency", 
				agent.getAgentName());
		assertEquals(" Incorrect teritory code ", "CMB", agent.getStationCode());
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.getAgentsForLocation(String,int)'
	 */
	public void testGetAgentsForLocation() {
		int LOC_TYPE_AIRPORT = 100; 
		int LOC_TYPE_TERRITORY = 101;
		int LOC_TYPE_COUNTRY =102;		
		int locType = 100;		
		Collection agentsForLocation = null;
		String airportcode = "CMB";
		String territorycode = "BBB";
		String countrycode = "AM";
		
		assertNotNull(agentDAO);	

		switch (locType) {
			case 100:
				agentsForLocation = agentDAO.getAgentsForStation(airportcode, 
						LOC_TYPE_AIRPORT);
				assertEquals(" Incorrect Size ", 5 , agentsForLocation.size());	
			case 101:
				agentsForLocation = agentDAO.getAgentsForStation(territorycode, 
						LOC_TYPE_TERRITORY);
				assertEquals(" Incorrect Size ", 3 , agentsForLocation.size());	
			case 102:
				agentsForLocation = agentDAO.getAgentsForStation(countrycode, 
						LOC_TYPE_COUNTRY);
				assertEquals(" Incorrect Size ", 3 , agentsForLocation.size());		
				break;
			default: 	
		}		
	}
	
	/**
	 * Test method for getGSAs
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO#getGSAs()
	 */
	public void testGetGSAs() {
		Collection allGSAS = null;
		
		assertNotNull(agentDAO);	
		allGSAS = agentDAO.getGSAs();
		assertNotNull(allGSAS);	
				
		assertEquals(" Incorrect Size ", 15, allGSAS.size());
		
		Iterator iter = allGSAS.iterator();
		
		while(iter.hasNext()){
			Agent element = (Agent) iter.next();
	
			if (element.getAgentCode().equals("18")) {
				assertEquals(" Incorrect agent code ", "18", 
						element.getAgentCode());
				assertEquals(" Incorrect City ", "Sharjah", 
						element.getAddressCity());
				assertEquals(" Incorrect agent type ", "GSA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Al Qawadi Travel & Tours", 
						element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1, 
						element.getCreditLimit(), 1);
				assertEquals(" Incorrect BankGuaranteeCashAdvance ", 1,  
						element.getBankGuaranteeCashAdvance(), 1);
				assertEquals(" Incorrect reporting GSA", "Y", 
						element.getReportingToGSA());
				assertEquals(" Incorrect airport code ", "BOM", 
						element.getStationCode());
				assertEquals(" Incorrect territory code ", "BBB", 
						element.getTerritoryCode());		
			}
		}		
	}	

	/** 
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 * .AgentDAO#getAgentTypes()
	 */
	public void testgetAgentTypes() {
		Collection agentTypes = null;
		
		assertNotNull(agentDAO);
        //FIXME
		agentTypes = agentDAO.getAgentTypes(null, null);
		assertNotNull(agentTypes);
		
		assertEquals(" Incorrect Size ", 6, agentTypes.size());
		
		Iterator iter = agentTypes.iterator();
		
		while (iter.hasNext()) {
			AgentType element = (AgentType) iter.next();
			
			if (element.getAgentTypeCode().equals("GSA")) {
				assertEquals(" Incorrect agent type code ", "GSA", 
						element.getAgentTypeCode());
				assertEquals(" Incorrect discription ", "Genaral Sales Agent", 
						element.getAgentTypeDescription());			
			}
		}		
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.assignAgentUser(User)'
	 */
	public void testAssignAgentUser() {
		Page agentsUsers = null;
		String agentCode = "15";
		int startIndex = 0;
		int pageSize = 10;
		
		assertNotNull(agentDAO);		
		agentDAO.assignAgentUser("T2", agentCode, true);		
		agentsUsers = agentDAO.getAgentUsers(agentCode, startIndex, pageSize);
		assertEquals(" Incorrect Size ", 2, agentsUsers.getPageData().size());
		
		Iterator iter = agentsUsers.getPageData().iterator();
				
		while (iter.hasNext()){
			User element = (User) iter.next();
		
			if (element.getUserId().equals("223")) {
				assertEquals(" Incorrect user id ", "223", 
						element.getUserId());
				assertEquals(" Incorrect password ", "lasantha", 
						element.getPassword());
				assertEquals(" Incorrect first name ", "Lasantha", 
						element.getDisplayName());
//				assertEquals(" Incorrect last name ", "Pambagoda", 
//						element.getLastName());
				assertEquals(" Incorrect email id ", "lasantha@jkcs.com", 
						element.getEmailId());		
			}		
		}
		agentDAO.assignAgentUser("T2", agentCode, false);
		agentsUsers = agentDAO.getAgentUsers(agentCode, startIndex, pageSize);	
		assertEquals(" Incorrect Size ", 1, agentsUsers.getPageData().size());
	}
	
	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.unassignAgentUser(String)'
	 */
	public void testGetCreditHistoryForAgent() {
		Collection agentCreditHistoryCollection = null;
		String agentcode = "16";
		
		assertNotNull(agentDAO);
		agentCreditHistoryCollection = agentDAO.getCreditHistoryForAgent(agentcode);		
		assertNotNull(agentCreditHistoryCollection);		
		Iterator iter = agentCreditHistoryCollection.iterator();
	
		while (iter.hasNext()) {
			AgentCreditHistory element = (AgentCreditHistory) iter.next();
			
			if (element.getAgentCode().equals("16")) {		
				assertEquals(" Incorrect agent code ", "16", 
						element.getAgentCode());		
				//assertEquals(" Incorrect Modified by ", "ABCD", 
				//		element.getModifiedBy());		
				//assertEquals(" Incorrect modified date ",  
				//		new GregorianCalendar(2005, 7, 11).getTime(), 
				//		element.getModifiedDate());
				assertEquals(" Incorrect credit limit amount ", 10000F, 10000F,
						element.getCreditLimitamount());
				assertEquals(" Incorrect credit history id", 1, 
						element.getAgnCrHisId());
				//assertEquals(" Incorrect creted date ",  
				//		new GregorianCalendar(2005, 7, 10).getTime(), 
				//		element.getCreatedDate());
				//assertEquals(" Incorrect creted date ",  
				//		new GregorianCalendar(2005, 7, 10).get(Calendar.MONTH), 
				//		element.getCreatedDate().getMonth());
			}			
		}		
	}

	/**
	 * @param agentCode
	 * @param newCreditLimit .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO
	 * #updateAgentCreditLimit(java.lang.String, double)
	 */
	public void testUpdateAgentCreditLimit() {
		String agentCode = "CMB30";
		double newCreditLimit = 1450;
	
		assertNotNull(agentDAO);
		agentDAO.updateAgentCreditLimit(agentCode, newCreditLimit, newCreditLimit);		
		
		Agent element = agentDAO.getAgent(agentCode);
			
		if (element.getAgentCode().equals("CMB30")) {	
			assertEquals(" Incorrect agent code ", "CMB30", 
				element.getAgentCode());				
			assertEquals(" Incorrect agent type ", "TA", 
				element.getAgentTypeCode());
			assertEquals(" Incorrect agent name ", "Gulf Travel", 
				element.getAgentName());
			assertEquals(" Incorrect Credit Limit", newCreditLimit, 
				element.getCreditLimit(), 1);
		}		
	}

//	/**
//	 * @param agentCode
//	 * @param newCreditLimit
//	 * @param remarks .
//	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
//	 * .AgentDAO#addAgentCreditHistory(java.lang.String, double, java.lang.String)
//	 */
////	public void testAddAgentCreditHistory() {
////		String agentCode = "18";
////		double newCreditLimit = 16000;
////		String remarks = "New Limit";		
////		agentDAO.addAgentCreditHistory(agentCode, newCreditLimit, remarks);
////		
////		
////	}
//
//	/**
//	 * @param agentCode
//	 * @param currentCreditLimit
//	 * @param newCreditLimit .
//	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
//	 * .AgentDAO#updateAgentAvailableCredit(java.lang.String, double, double)
//	 */
//	public void testUpdateAgentAvailableCredit() {
//		String agentCode = "25";
//		double currentCreditLimit = 1000;
//		double newCreditLimit = 1400;
//		
//		assertNotNull(agentDAO);
//		agentDAO.updateAgentAvailableCredit(agentCode, currentCreditLimit,
//				newCreditLimit);		
//	}


	/**
	 * Test method for 'com.isa.thinair.airtravelagents.core.persistence
	 * .hibernate.TravelAgentDAOImpl.searchAgents(Set)'
	 */
	public void testSearchAgents() {
		Page searchAgents = null;
		List criteria = new ArrayList();
		int startIndex = 0;
		int pageSize = 10; 
		
		/*ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterion.setFieldName("agentCode");		
		listValues.add("CMB30");		
		criterion.setValue(listValues);
		criteria.add(criterion);
		searchAgents = agentDAO.searchAgents(criteria);	*/		
//		
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);		
//		criterion.setFieldName("agentTypeCode");
//		List listValues = new ArrayList();
//		listValues.add("TA");
//		criterion.setValue(listValues);		
		
	/*	ModuleCriterion criterion1 = new ModuleCriterion();
		criterion1.setCondition(ModuleCriterion.CONDITION_EQUALS);		
		criterion1.setFieldName("stationCode");
		List listValues1 = new ArrayList();	
		listValues1.add("CMB");
		criterion1.setValue(listValues1);*/
		
		ModuleCriterion criterion2 = new ModuleCriterion();			
		criterion2.setCondition(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE);
		criterion2.setFieldName("agentName");		
		List listValues2 = new ArrayList();
		listValues2.add("AZ");
		criterion2.setValue(listValues2);
		
		//criteria.add(criterion);
		//criteria.add(criterion1);
		criteria.add(criterion2);
		
        //FIXME
		searchAgents = agentDAO.searchAgents(criteria, startIndex, pageSize, listValues2);
		
	System.out.println("  searchAgents "+searchAgents.getPageData().size());
	System.out.println("  searchAgents "+searchAgents.getPageData());
	
		Iterator iter = searchAgents.getPageData().iterator();
		
		while (iter.hasNext()) {
			Agent element = (Agent) iter.next();
			
			if (element.getAgentCode().equals("16")) {	
				assertEquals(" Incorrect agent code ", "16", 
					element.getAgentCode());				
				assertEquals(" Incorrect agent type ", "TA", 
					element.getAgentTypeCode());
				assertEquals(" Incorrect agent name ", "Al Kazim Travel Agency", 
					element.getAgentName());
				assertEquals(" Incorrect Credit Limit", 1450, 
					element.getCreditLimit(), 1);
			}			
		}
	}

	/**
	 * @return .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 * .AgentDAO#getAllAgentNames()
	 */
	public void testGetAllAgentNames() {
		Hashtable hashtable = null;
		
		assertNotNull(agentDAO);
		hashtable = agentDAO.getAllAgentNames();
		
		
		assertEquals("Incorrect size ", 90, hashtable.size());			
		assertEquals(" Incorrect agent name ", "Khorfakkan Agency", 
				hashtable.get("15"));		
	}

	/**
	 * @return .
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao.AgentDAO
	 * #getUnassignUsers()
	 */
	public void testGetUnassignUsers() {
		Collection users = null;
		
		assertNotNull(agentDAO);
		users = agentDAO.getUnassignUsers();
	
		assertEquals("Incorrect size ", 8, users.size());
		
		Iterator iter = users.iterator();
		
		while (iter.hasNext()) {
			User element = (User) iter.next();
			
			if (element.getUserId().equals("T 005")) {
				assertEquals(" Incorrect user id ","T 005", element.getUserId());
				assertEquals(" Incorrect first name ", "Chaminda", 
						element.getDisplayName());
				//assertEquals(" Incorrect last name ", "Perera", 
				//		element.getLastName());		
				assertEquals(" Incorrect email address ", "chamindap@jkcsworld.com", 
						element.getEmailId());
			}			
		}	
	}	
}