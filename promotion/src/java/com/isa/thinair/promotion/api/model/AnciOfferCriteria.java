package com.isa.thinair.promotion.api.model;

import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * Class holding the qualifying criteria for Ancillary Offers/Customizations.
 * 
 * @author thihara
 * 
 * @hibernate.class table = "T_ANCI_OFFER_CRITERIA"
 */
public class AnciOfferCriteria extends Tracking {

	private static final long serialVersionUID = 6112316542321123L;

	/** Unique persistence ID assigned to the AnciOfferCriteria this object is representing. */
	private Integer id;

	/** Passenger counts eligible for this AnciOfferCriteria */
	private Set<PaxQuantity> eligiblePaxCounts;

	/**
	 * AnciOfferCriteria type. Which template should be selected (BAGGAGE/SEAT/MEAL) are the only ones currently
	 * supported
	 */
	private String anciTemplateType;

	/** ID of the charge template. (Baggage charge template ID, SeatMap charge template ID etc.) */
	private Integer templateID;

	/** FareBasis codes this AnciOfferCriteria is applicable to */
	private Set<String> bookingClasses;

	/** LCC codes this AnciOfferCriteria is applicable to */
	private Set<String> lccs;

	/** ONDs this AnciOfferCriteria is applicable to */
	private Set<String> onds;

	/** Date range start this AnciOfferCriteria is applicable to */
	private Date applicableFrom;

	/** Date range end this AnciOfferCriteria is applicable to */
	private Date applicableTo;

	/** Boolean denoting whether to apply this criteria to Flexi fare selected segments or not. */
	private Boolean applyToFlexiOnly;

	/** Boolean denoting if this AnciOfferCriteria is applicable for return segments */
	private Boolean applicableForReturn = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is applicable for outbound segments */
	private Boolean applicableForOneWay = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is applicable for one way reservations */
	private Boolean eligibleForOneWay = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is applicable for return reservations */
	private Boolean eligibleForReturn = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is an active offer */
	private Boolean active = Boolean.TRUE;

	/** I18nMessage key corresponding to the name's translation data */
	private String anciOfferNameI18nMessageKey;

	/** I18nMessage key corresponding to the description's translation data */
	private String anciOfferDescriptionI18nMessageKey;

	/**
	 * Values should correspond to {@link java.util.Calendar} DAY_OF_THE_WEEK values to each day. Values will be
	 * separated by commas (,)
	 */
	private String applicableOutboundDaysOfTheWeek;

	/**
	 * Values should correspond to {@link java.util.Calendar} DAY_OF_THE_WEEK values to each day. Values will be
	 * separated by commas (,)
	 */
	private String applicableInboundDaysOfTheWeek;

	/**
	 * @return id
	 * @hibernate.id column = "ANCI_OFFER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ANCI_OFFER_CRITERIA"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the eligiblePaxCounts
	 * 
	 * @hibernate.set lazy="false" table="T_ANCI_OFFER_PAX_QUANTITY" cascade="all" inverse="false";
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.PaxQuantity"
	 * @hibernate.collection-key column="ANCI_OFFER_ID"
	 */
	public Set<PaxQuantity> getEligiblePaxCounts() {
		return eligiblePaxCounts;
	}

	/**
	 * @param eligiblePaxCounts
	 *            the eligiblePaxCounts to set
	 */
	public void setEligiblePaxCounts(Set<PaxQuantity> eligiblePaxCounts) {
		this.eligiblePaxCounts = eligiblePaxCounts;
	}

	/**
	 * @return the anciTemplateType
	 * @hibernate.property column = "ANCI_TEMPLATE_TYPE"
	 */
	public String getAnciTemplateType() {
		return anciTemplateType;
	}

	/**
	 * @param anciTemplateType
	 *            the anciTemplateType to set
	 */
	public void setAnciTemplateType(String anciTemplateType) {
		this.anciTemplateType = anciTemplateType;
	}

	/**
	 * @return the templateID
	 * @hibernate.property column = "ANCI_TEMPLATE_ID"
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	/**
	 * @param templateID
	 *            the templateID to set
	 */
	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return the bookingClass
	 * 
	 * @hibernate.set lazy="false" table="T_ANCI_OFFER_BOOKING_CLASS"
	 * @hibernate.collection-element column="BOOKING_CLASS" type="string"
	 * @hibernate.collection-key column="ANCI_OFFER_ID"
	 */
	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	/**
	 * @param bookingClass
	 *            the bookingClass to set
	 */
	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	/**
	 * @return the lccs
	 * @hibernate.set lazy="false" table="T_ANCI_OFFER_LCC"
	 * @hibernate.collection-element column="LOGICAL_CABIN_CLASS_CODE" type="string"
	 * @hibernate.collection-key column="ANCI_OFFER_ID"
	 */
	public Set<String> getLccs() {
		return lccs;
	}

	/**
	 * @param lccs
	 *            the lccs to set
	 */
	public void setLccs(Set<String> lccs) {
		this.lccs = lccs;
	}

	/**
	 * @return the onds
	 * @hibernate.set lazy="false" table="T_ANCI_OFFER_OND"
	 * @hibernate.collection-element column="OND_CODE" type="string"
	 * @hibernate.collection-key column="ANCI_OFFER_ID"
	 */
	public Set<String> getOnds() {
		return onds;
	}

	/**
	 * @param onds
	 *            the onds to set
	 */
	public void setOnds(Set<String> onds) {
		this.onds = onds;
	}

	/**
	 * @return the applicableFrom
	 * @hibernate.property column = "APPLICABLE_FROM"
	 */
	public Date getApplicableFrom() {
		return applicableFrom;
	}

	/**
	 * @param applicableFrom
	 *            the applicableFrom to set
	 */
	public void setApplicableFrom(Date applicableFrom) {
		this.applicableFrom = applicableFrom;
	}

	/**
	 * @return the applicableTo
	 * @hibernate.property column = "APPLICABLE_TO"
	 */
	public Date getApplicableTo() {
		return applicableTo;
	}

	/**
	 * @param applicableTo
	 *            the applicableTo to set
	 */
	public void setApplicableTo(Date applicableTo) {
		this.applicableTo = applicableTo;
	}

	/**
	 * @return the applicableForReturn
	 * @hibernate.property column = "APPLICABLE_FOR_RETURN" type = "yes_no"
	 */
	public Boolean getApplicableForReturn() {
		return applicableForReturn;
	}

	/**
	 * @param applicableForReturn
	 *            the applicableForReturn to set
	 */
	public void setApplicableForReturn(Boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	/**
	 * @return the applicableForOneWay
	 * @hibernate.property column = "APPLICABLE_FOR_ONEWAY" type = "yes_no"
	 */
	public Boolean getApplicableForOneWay() {
		return applicableForOneWay;
	}

	/**
	 * @param applicableForOneWay
	 *            the applicableForOneWay to set
	 */
	public void setApplicableForOneWay(Boolean applicableForOneWay) {
		this.applicableForOneWay = applicableForOneWay;
	}

	/**
	 * @return the active
	 * @hibernate.property column = "IS_ACTIVE" type = "yes_no"
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "NAME_I18NKEY"
	 */
	public String getAnciOfferNameI18nMessageKey() {
		return anciOfferNameI18nMessageKey;
	}

	public void setAnciOfferNameI18nMessageKey(String anciOfferNameI18nMessageKey) {
		this.anciOfferNameI18nMessageKey = anciOfferNameI18nMessageKey;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "DESCRIPTION_I18NKEY"
	 */
	public String getAnciOfferDescriptionI18nMessageKey() {
		return anciOfferDescriptionI18nMessageKey;
	}

	public void setAnciOfferDescriptionI18nMessageKey(String anciOfferDescriptionI18nMessageKey) {
		this.anciOfferDescriptionI18nMessageKey = anciOfferDescriptionI18nMessageKey;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "APPLICABLE_OB_DAYS_OF_THE_WEEK"
	 */
	public String getApplicableOutboundDaysOfTheWeek() {
		return applicableOutboundDaysOfTheWeek;
	}

	public void setApplicableOutboundDaysOfTheWeek(String applicableOutboundDaysOfTheWeek) {
		this.applicableOutboundDaysOfTheWeek = applicableOutboundDaysOfTheWeek;
	}

	/**
	 * @return
	 * @hibernate.property column = "APPLICABLE_IB_DAYS_OF_THE_WEEK"
	 */
	public String getApplicableInboundDaysOfTheWeek() {
		return applicableInboundDaysOfTheWeek;
	}

	public void setApplicableInboundDaysOfTheWeek(String applicableInboundDaysOfTheWeek) {
		this.applicableInboundDaysOfTheWeek = applicableInboundDaysOfTheWeek;
	}

	/**
	 * @return the applyToFlexiOnly
	 * @hibernate.property column = "APPLY_FOR_FLEXI_ONLY" type = "yes_no"
	 */
	public Boolean getApplyToFlexiOnly() {
		return applyToFlexiOnly;
	}

	/**
	 * @param applyToFlexiOnly
	 *            the applyToFlexiOnly to set
	 */
	public void setApplyToFlexiOnly(Boolean applyToFlexiOnly) {
		this.applyToFlexiOnly = applyToFlexiOnly;
	}

	/**
	 * @return the eligibleForOneWay
	 * @hibernate.property column = "ELIGIBLE_FOR_ONEWAY" type = "yes_no"
	 */
	public Boolean getEligibleForOneWay() {
		return eligibleForOneWay;
	}

	/**
	 * @param eligibleForOneWay
	 *            the eligibleForOneWay to set
	 */
	public void setEligibleForOneWay(Boolean eligibleForOneWay) {
		this.eligibleForOneWay = eligibleForOneWay;
	}

	/**
	 * @return the eligibleForReturn
	 * @hibernate.property column = "ELIGIBLE_FOR_RETURN" type = "yes_no"
	 */
	public Boolean getEligibleForReturn() {
		return eligibleForReturn;
	}

	/**
	 * @param eligibleForReturn
	 *            the eligibleForReturn to set
	 */
	public void setEligibleForReturn(Boolean eligibleForReturn) {
		this.eligibleForReturn = eligibleForReturn;
	}
}
