package com.isa.thinair.promotion.api.model;

import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_OND_PROMOTION" lazy="false"
 */
public class OndPromotion extends Tracking {

	private Long ondPromoId;
	private PromotionTemplate promotionTemplate;
	private String ondCode;
	private String origin;
	private String destination;
	private String status;
	private Set<OndPromotionCharge> ondPromotionCharges;

	/**
	 * @hibernate.id column = "OND_PROMOTION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_OND_PROMOTION"
	 */
	public Long getOndPromoId() {
		return ondPromoId;
	}

	public void setOndPromoId(Long ondPromoId) {
		this.ondPromoId = ondPromoId;
	}

	/**
	 * @hibernate.many-to-one column="PROMOTION_ID" cascade="save-update" update="false"
	 *                        class="com.isa.thinair.promotion.api.model.PromotionTemplate"
	 */
	public PromotionTemplate getPromotionTemplate() {
		return promotionTemplate;
	}

	public void setPromotionTemplate(PromotionTemplate promotionTemplate) {
		this.promotionTemplate = promotionTemplate;
	}

	/**
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_PROMOTION_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.OndPromotionCharge"
	 */
	public Set<OndPromotionCharge> getOndPromotionCharges() {
		return ondPromotionCharges;
	}

	public void setOndPromotionCharges(Set<OndPromotionCharge> ondPromotionCharges) {
		this.ondPromotionCharges = ondPromotionCharges;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getOndCode()).toHashCode();
	}

	public boolean equals(Object obj) {
		OndPromotion other;
		if (obj != null && obj instanceof OndPromotion) {
			other = (OndPromotion) obj;
			return getOndCode() != null && other.getOndCode() != null && getOndCode().equals(other.getOndCode());
		}
		return false;
	}

}
