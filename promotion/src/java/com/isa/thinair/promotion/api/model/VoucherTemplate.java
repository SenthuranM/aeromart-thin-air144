package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @hibernate.class table = "T_VOUCHER_TEMPLATE" lazy="false"
 */

public class VoucherTemplate {

	private Integer voucherId;
	private String voucherName;
	private BigDecimal amount;
	private double amountInLocal;
	private String currencyCode;
	private int voucherValidity;
	private Date salesFrom;
	private Date salesTo;
	private String remarks;
	private String status;

	/**
	 * @hibernate.id column = "VOUCHER_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_VOUCHER_TEMPLATE"
	 */

	public Integer getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Integer voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @hibernate.property column = "VOUCHER_TEMPLATE_NAME"
	 */

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "VALID_PERIOD_DAYS"
	 */

	public int getVoucherValidity() {
		return voucherValidity;
	}

	public void setVoucherValidity(int voucherValidity) {
		this.voucherValidity = voucherValidity;
	}

	/**
	 * @hibernate.property column = "SALES_FROM"
	 */

	public Date getSalesFrom() {
		return salesFrom;
	}

	public void setSalesFrom(Date salesFrom) {
		this.salesFrom = salesFrom;
	}

	/**
	 * @hibernate.property column = "SALES_TO"
	 */
	public Date getSalesTo() {
		return salesTo;
	}

	public void setSalesTo(Date salesTo) {
		this.salesTo = salesTo;
	}

	/**
	 * @hibernate.property column = "REMARKS"
	 */

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @hibernate.property column = "AMOUNT_IN_LOCAL"
	 */

	public double getAmountInLocal() {
		return amountInLocal;
	}

	public void setAmountInLocal(double amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @hibernate.property column = "CURRENCY_CODE"
	 */

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {

		this.currencyCode = currencyCode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
