package com.isa.thinair.promotion.api.utils;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public interface PromotionsInternalConstants {

	String OND_DELIM = "/";
	String COMMON_DELIM = "_";

	String STATUS_ACTIVE = "ACT";
	String STATUS_INACTIVE = "INA";

	public enum PromotionRequestStatus {
		ACTIVE("ACT"), INACTIVE("INA"), APPROVED("APR"), REJECTED("REJ"), EXPIRED("EXP");

		String statusCode;

		private PromotionRequestStatus(String statusCode) {
			this.statusCode = statusCode;
		}

		public String getStatusCode() {
			return statusCode;
		}

	}

	String CHARGES_FARES = ReservationInternalConstants.ChargeGroup.FAR;
	String CHARGES_TAXES = ReservationInternalConstants.ChargeGroup.TAX;
	String CHARGES_SURCHARGES = ReservationInternalConstants.ChargeGroup.SUR;
	String CHARGES_ALL = "ALL_CHARGES";
	String CHARGES_SEAT_CHARGES = "SEAT_CHARGES";
	String CONNECTION_FARE = BookingClass.AllocationType.CONNECTION;

	public static String ADD = "ADD";
	public static String REMOVE = "REMOVE";

	String BOOEAN_FALSE = "N";
	String BOOLEAN_TRUE = "Y";

	String OND_ALL = "ALL";

	int DIRECTION_NONE = 0;
	int DIRECTION_OUTBOUND = 1;
	int DIRECTION_INBOUND = 2;

	int MAXIMUM_SEATS_ALLOWED_NEXT_SEAT_PROMOTION = 1;

	public enum ApplicationEngine {
		IBE, XBE, WS
	}
}
