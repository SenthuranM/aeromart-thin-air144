package com.isa.thinair.promotion.api.to.anciOffer;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Data transfer object for AnciOfferCriteria. All operations apart from direct persistence interactions should be via
 * this class.
 * 
 * @author thihara
 * 
 */
public class AnciOfferCriteriaTO implements Serializable {

	private static final long serialVersionUID = 6363409859771591577L;

	/** Unique persistence ID assigned to the AnciOfferCriteria this object is representing. */
	private Integer id;

	/** Passenger counts eligible for this AnciOfferCriteria : 1Adult,2Children,1Infant -> 1,2,1 */
	private Set<String> eligiblePaxCounts;

	/**
	 * AnciOfferCriteria type. Which template should be selected (BAGGAGE/SEAT/MEAL) are the only ones currently
	 * supported
	 */
	private String anciTemplateType;

	/** ID of the charge template. (Baggage charge template ID, SeatMap charge template ID etc.) */
	private Integer templateID;

	/** FareBasis codes this AnciOfferCriteria is applicable to */
	private Set<String> bookingClasses;

	/** LCC codes this AnciOfferCriteria is applicable to */
	private Set<String> lccs;

	/** ONDs this AnciOfferCriteria is applicable to */
	private Set<String> onds;

	/** Date range start this AnciOfferCriteria is applicable to */
	private Date applicableFrom;

	/** Date range end this AnciOfferCriteria is applicable to */
	private Date applicableTo;

	/** Boolean denoting whether to apply this criteria to Flexi fare selected segments or not. */
	private Boolean applyToFlexiOnly;

	/** Boolean denoting if this AnciOfferCriteria is applicable for return reservations */
	private Boolean applicableForReturn = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is applicable for one way reservations */
	private Boolean applicableForOneWay = Boolean.FALSE;

	private Boolean eligibleForOneWay = Boolean.FALSE;

	private Boolean eligibleForReturn = Boolean.FALSE;

	/** Boolean denoting if this AnciOfferCriteria is active */
	private Boolean active = Boolean.TRUE;

	/** Optimistic lock version for hibernate */
	private Long version;

	/** AnciOfferCriteria name translations. Key -> Language code, Value -> Name translation */
	private Map<String, String> nameTranslations;

	/** I18nMessage key corresponding to the name's translation data */
	private String i18nMessageKeyForName;

	/** AnciOfferCriteria description translations. Key -> Language code, Value -> Description translation */
	private Map<String, String> descriptionTranslations;

	/** I18nMessage key corresponding to the description's translation data */
	private String i18nMessageKeyForDescription;

	/** Values should correspond to {@link java.util.Calendar} DAY_OF_THE_WEEK values to each day */
	private Set<Integer> applicableOutboundDaysOfTheWeek;

	/** Values should correspond to {@link java.util.Calendar} DAY_OF_THE_WEEK values to each day */
	private Set<Integer> applicableInboundDaysOfTheWeek;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the eligiblePaxCounts
	 */
	public Set<String> getEligiblePaxCounts() {
		return eligiblePaxCounts;
	}

	/**
	 * @param eligiblePaxCounts
	 *            the eligiblePaxCounts to set
	 */
	public void setEligiblePaxCounts(Set<String> eligiblePaxCounts) {
		this.eligiblePaxCounts = eligiblePaxCounts;
	}

	/**
	 * @return the anciTemplateType
	 */
	public String getAnciTemplateType() {
		return anciTemplateType;
	}

	/**
	 * @param anciTemplateType
	 *            the anciTemplateType to set
	 */
	public void setAnciTemplateType(String anciTemplateType) {
		this.anciTemplateType = anciTemplateType;
	}

	/**
	 * @return the templateID
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	/**
	 * @param templateID
	 *            the templateID to set
	 */
	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return the fareBasisCodes
	 */
	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	/**
	 * @param bookingClasses
	 *            the fareBasisCodes to set
	 */
	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	/**
	 * @return the lccs
	 */
	public Set<String> getLccs() {
		return lccs;
	}

	/**
	 * @param lccs
	 *            the lccs to set
	 */
	public void setLccs(Set<String> lccs) {
		this.lccs = lccs;
	}

	/**
	 * @return the onds
	 */
	public Set<String> getOnds() {
		return onds;
	}

	/**
	 * @param onds
	 *            the onds to set
	 */
	public void setOnds(Set<String> onds) {
		this.onds = onds;
	}

	/**
	 * @return the applicableFrom
	 */
	public Date getApplicableFrom() {
		return applicableFrom;
	}

	/**
	 * @param applicableFrom
	 *            the applicableFrom to set
	 */
	public void setApplicableFrom(Date applicableFrom) {
		this.applicableFrom = applicableFrom;
	}

	/**
	 * @return the applicableTo
	 */
	public Date getApplicableTo() {
		return applicableTo;
	}

	/**
	 * @param applicableTo
	 *            the applicableTo to set
	 */
	public void setApplicableTo(Date applicableTo) {
		this.applicableTo = applicableTo;
	}

	/**
	 * @return the applicableForReturn
	 */
	public Boolean getApplicableForReturn() {
		return applicableForReturn;
	}

	/**
	 * @param applicableForReturn
	 *            the applicableForReturn to set
	 */
	public void setApplicableForReturn(Boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	/**
	 * @return the applicableForOneWay
	 */
	public Boolean getApplicableForOneWay() {
		return applicableForOneWay;
	}

	/**
	 * @param applicableForOneWay
	 *            the applicableForOneWay to set
	 */
	public void setApplicableForOneWay(Boolean applicableForOneWay) {
		this.applicableForOneWay = applicableForOneWay;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Map<String, String> getNameTranslations() {
		return nameTranslations;
	}

	public void setNameTranslations(Map<String, String> nameTranslations) {
		this.nameTranslations = nameTranslations;
	}

	public Map<String, String> getDescriptionTranslations() {
		return descriptionTranslations;
	}

	public void setDescriptionTranslations(Map<String, String> descriptionTranslations) {
		this.descriptionTranslations = descriptionTranslations;
	}

	public String getI18nMessageKeyForName() {
		return i18nMessageKeyForName;
	}

	public void setI18nMessageKeyForName(String i18nMessageKeyForName) {
		this.i18nMessageKeyForName = i18nMessageKeyForName;
	}

	public String getI18nMessageKeyForDescription() {
		return i18nMessageKeyForDescription;
	}

	public void setI18nMessageKeyForDescription(String i18nMessageKeyForDescription) {
		this.i18nMessageKeyForDescription = i18nMessageKeyForDescription;
	}

	public Set<Integer> getApplicableOutboundDaysOfTheWeek() {
		return applicableOutboundDaysOfTheWeek;
	}

	public void setApplicableOutboundDaysOfTheWeek(Set<Integer> applicableDaysOfTheWeek) {
		this.applicableOutboundDaysOfTheWeek = applicableDaysOfTheWeek;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the applicableInboundDaysOfTheWeek
	 */
	public Set<Integer> getApplicableInboundDaysOfTheWeek() {
		return applicableInboundDaysOfTheWeek;
	}

	/**
	 * @param applicableInboundDaysOfTheWeek
	 *            the applicableInboundDaysOfTheWeek to set
	 */
	public void setApplicableInboundDaysOfTheWeek(Set<Integer> applicableInboundDaysOfTheWeek) {
		this.applicableInboundDaysOfTheWeek = applicableInboundDaysOfTheWeek;
	}

	/**
	 * @return the applyToFlexiOnly
	 */
	public Boolean getApplyToFlexiOnly() {
		return applyToFlexiOnly;
	}

	/**
	 * @param applyToFlexiOnly
	 *            the applyToFlexiOnly to set
	 */
	public void setApplyToFlexiOnly(Boolean applyToFlexiOnly) {
		this.applyToFlexiOnly = applyToFlexiOnly;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AnciOfferCriteriaTO)) {
			return false;
		}
		AnciOfferCriteriaTO other = (AnciOfferCriteriaTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the eligibleForOneWay
	 */
	public Boolean getEligibleForOneWay() {
		return eligibleForOneWay;
	}

	/**
	 * @param eligibleForOneWay the eligibleForOneWay to set
	 */
	public void setEligibleForOneWay(Boolean eligibleForOneWay) {
		this.eligibleForOneWay = eligibleForOneWay;
	}

	/**
	 * @return the eligibleForReturn
	 */
	public Boolean getEligibleForReturn() {
		return eligibleForReturn;
	}

	/**
	 * @param eligibleForReturn the eligibleForReturn to set
	 */
	public void setEligibleForReturn(Boolean eligibleForReturn) {
		this.eligibleForReturn = eligibleForReturn;
	}
}
