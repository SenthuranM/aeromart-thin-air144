package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "T_FLOWN_FILE_ERROR_LOG"
 * 
 */
public class FlownFileErrorLog extends Tracking {

	private static final long serialVersionUID = 4606749190834415153L;

	private Integer errorLogId;
	private String memberAccountId;
	private String title;
	private String lastName;
	private String firstName;
	private String paxType;
	private String dob;
	private String nationality;
	private String operatingAirline;
	private String operaringFlight;
	private String marketingAirline;
	private String marketingFlight;
	private String flightDate;
	private String flightTime;
	private String departureAirport;
	private String arrivalAirport;
	private String cabinClass;
	private String bookingClass;
	private String pnr;
	private String ticketIssuanceDate;
	private String ticketNumber;
	private String couponNumber;
	private String agentInitials;
	private String contactNumber;
	private String contactEmail;
	private String countryOfResidence;
	private String salesChannel;
	private String passportNumber;
	private String status;
	private String productList;

	/**
	 * @hibernate.id column = "FLOWN_FILE_ERROR_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLOWN_FILE_ERROR_LOG"
	 * 
	 * @return the errorLogId
	 */
	public Integer getErrorLogId() {
		return errorLogId;
	}

	/**
	 * @param errorLogId
	 *            the errorLogId to set
	 */
	public void setErrorLogId(Integer errorLogId) {
		this.errorLogId = errorLogId;
	}

	/**
	 * @hibernate.property column = "MEMBER_ACCOUNT_ID"
	 * 
	 * @return the memberAccountId
	 */
	public String getMemberAccountId() {
		return memberAccountId;
	}

	/**
	 * @param memberAccountId
	 *            the memberAccountId to set
	 */
	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	/**
	 * @hibernate.property column = "TITLE"
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @hibernate.property column = "LAST_NAME"
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @hibernate.property column = "FIRST_NAME"
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @hibernate.property column = "PAX_TYPE"
	 * 
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 * 
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}

	/**
	 * @hibernate.property column = "NATIONALITY"
	 * 
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @hibernate.property column = "OPERATING_AIRLINE"
	 * 
	 * @return the operatingAirline
	 */
	public String getOperatingAirline() {
		return operatingAirline;
	}

	/**
	 * @param operatingAirline
	 *            the operatingAirline to set
	 */
	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	/**
	 * @hibernate.property column = "OPERATING_FLT_NUMBER"
	 * 
	 * @return the operaringFlight
	 */
	public String getOperaringFlight() {
		return operaringFlight;
	}

	/**
	 * @param operaringFlight
	 *            the operaringFlight to set
	 */
	public void setOperaringFlight(String operaringFlight) {
		this.operaringFlight = operaringFlight;
	}

	/**
	 * @hibernate.property column = "MKT_AIRLINE"
	 * 
	 * @return the marketingAirline
	 */
	public String getMarketingAirline() {
		return marketingAirline;
	}

	/**
	 * @param marketingAirline
	 *            the marketingAirline to set
	 */
	public void setMarketingAirline(String marketingAirline) {
		this.marketingAirline = marketingAirline;
	}

	/**
	 * @hibernate.property column = "MKT_FLT_NUMBER"
	 * 
	 * @return the marketingFlight
	 */
	public String getMarketingFlight() {
		return marketingFlight;
	}

	/**
	 * @param marketingFlight
	 *            the marketingFlight to set
	 */
	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = marketingFlight;
	}

	/**
	 * @hibernate.property column = "FLT_DATE"
	 * 
	 * @return the flightDate
	 */
	public String getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            the flightDate to set
	 */
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @hibernate.property column = "FLT_TIME"
	 * 
	 * @return the flightTime
	 */
	public String getFlightTime() {
		return flightTime;
	}

	/**
	 * @param flightTime
	 *            the flightTime to set
	 */
	public void setFlightTime(String flightTime) {
		this.flightTime = flightTime;
	}

	/**
	 * @hibernate.property column = "DEP_AIRPORT"
	 * 
	 * @return the departureAirport
	 */
	public String getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            the departureAirport to set
	 */
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @hibernate.property column = "ARR_AIRPORT"
	 * 
	 * @return the arrivalAirport
	 */
	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param arrivalAirport
	 *            the arrivalAirport to set
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 * 
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @hibernate.property column = "BOOKING_CODE"
	 * 
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * @hibernate.property column = "PNR"
	 * 
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column = "TKT_ISSUANCE_DATE"
	 * 
	 * @return the ticketIssuanceDate
	 */
	public String getTicketIssuanceDate() {
		return ticketIssuanceDate;
	}

	/**
	 * @param ticketIssuanceDate
	 *            the ticketIssuanceDate to set
	 */
	public void setTicketIssuanceDate(String ticketIssuanceDate) {
		this.ticketIssuanceDate = ticketIssuanceDate;
	}

	/**
	 * @hibernate.property column = "TKT_NUMBER"
	 * 
	 * @return the ticketNumber
	 */
	public String getTicketNumber() {
		return ticketNumber;
	}

	/**
	 * @param ticketNumber
	 *            the ticketNumber to set
	 */
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	/**
	 * @hibernate.property column = "TKT_COUPON_NUMBER"
	 * 
	 * @return the couponNumber
	 */
	public String getCouponNumber() {
		return couponNumber;
	}

	/**
	 * @param couponNumber
	 *            the couponNumber to set
	 */
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 * 
	 * @return the agentInitials
	 */
	public String getAgentInitials() {
		return agentInitials;
	}

	/**
	 * @param agentInitials
	 *            the agentInitials to set
	 */
	public void setAgentInitials(String agentInitials) {
		this.agentInitials = agentInitials;
	}

	/**
	 * @hibernate.property column = "CONTACT_NUMBER"
	 * 
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *            the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @hibernate.property column = "CONTACT_EMAIL"
	 * 
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * @hibernate.property column = "RESIDENCY_CODE"
	 * 
	 * @return the countryOfResidence
	 */
	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	/**
	 * @param countryOfResidence
	 *            the countryOfResidence to set
	 */
	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	/**
	 * @hibernate.property column = "SALES_CHANNEL"
	 * 
	 * @return the salesChannel
	 */
	public String getSalesChannel() {
		return salesChannel;
	}

	/**
	 * @param salesChannel
	 *            the salesChannel to set
	 */
	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	/**
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 * 
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber
	 *            the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the productList
	 * 
	 * @hibernate.property column = "PRODUCT_LIST"
	 */
	public String getProductList() {
		return productList;
	}

	/**
	 * @param productList
	 *            the productList to set
	 */
	public void setProductList(String productList) {
		this.productList = productList;
	}

	public enum STATUS {
		PROCESSED, PENDING
	};
}
