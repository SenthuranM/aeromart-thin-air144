package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * The model class representing a voucher terms and conditions template. This model is not designed to create new objects.
 * Rather the purpose is to edit objects that are initially inserted into the db manually. equals and hashCode methods
 * are overridden with this purpose in mind (only considering the ID field).
 * 
 * @author chanaka
 * 
 * @hibernate.class table = "T_VOUCHER_TERMS"
 */
public class VoucherTermsTemplate extends Persistent {

	private static final long serialVersionUID = 14654678945612347L;

	private Integer templateID;

	private String templateName;

	private String templateContent;

	private String languageCode;
	
	private Character voucherType;

	/**
	 * @return The template ID
	 * @hibernate.id column = "VOUCHER_TERMS_TEMPLATE_ID" generator-class = "assigned"
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return The name of the terms and condition template. The name is unique.
	 * 
	 * @hibernate.property column = "VOUCHER_TERMS_TEMPLATE_NAME "
	 */
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return Terms and conditions stored in the database.
	 * 
	 * @hibernate.property column = "CONTENT"
	 */
	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	/**
	 * @return The language code of the template.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return The voucher type of the template.
	 * 
	 * @hibernate.property column = "VOUCHER_TYPE"
	 */
	public Character getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(Character voucherType) {
		this.voucherType = voucherType;
	}


	/**
	 * Ignoring super class hashCode implementation. Reflective hashCode is bad for everyone's health. This class is
	 * inserted to the database manually. So ID is always guaranteed to be present. Therefore only ID field is
	 * considered.
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((templateID == null) ? 0 : templateID.hashCode());
		return result;
	}

	/**
	 * Ignoring super class equals implementation. Reflective equals is bad for everyone's health. This class is
	 * inserted to the database manually. So ID is always guaranteed to be present. Therefore only ID field is
	 * considered.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof VoucherTermsTemplate)) {
			return false;
		}
		VoucherTermsTemplate other = (VoucherTermsTemplate) obj;
		if (templateID == null) {
			if (other.templateID != null) {
				return false;
			}
		} else if (!templateID.equals(other.templateID)) {
			return false;
		}
		return true;
	}
}

