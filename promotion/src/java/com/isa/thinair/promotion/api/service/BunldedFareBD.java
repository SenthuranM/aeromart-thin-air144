package com.isa.thinair.promotion.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.promotion.api.to.BundleCategorySearch;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareConfigurationTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplayDTO;	
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplaySettingsDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareSearchCriteriaTO;

/**
 * Interface related to Bundled Fare Configuration
 *
 * @author rumesh
 *
 */
public interface BunldedFareBD {
	public static final String SERVICE_NAME = "BundledFareManager";

	/**
	 *
	 * Search configured bundled fares based on defined criteria
	 *
	 * @param searchCriteriaTO
	 * @param startPosition
	 * @param recSize
	 * @return
	 * @throws ModuleException
	 */
	public Page<BundledFareConfigurationTO> searchBundledFareConfiguration(BundledFareSearchCriteriaTO searchCriteriaTO,
			Integer startPosition, Integer recSize) throws ModuleException;
	
	
	public BundledFare getBundledFareById(Integer bundledFareId) throws ModuleException;

	/**
	 * Get BundledFareDTO required in the reservations flow
	 * @param bundledFarePeriodId
	 * @return
	 * @throws ModuleException
	 */
	public BundledFareDTO getBundledFareDtoByBundlePeriodId(Integer bundledFarePeriodId) throws ModuleException;

	/**
	 * Get BundledFareLiteDTO by bundledFarePeriodId
	 *
	 * @param bundledFarePeriodId
	 * @param preferredLanguage
	 * @return
	 * @throws ModuleException
	 */
	public BundledFareLiteDTO getBundledFareLiteDTO(Integer bundledFarePeriodId, String preferredLanguage) throws ModuleException;

	/**
	 * Get BundledFareDTO list by given bundled fare period IDs
	 *
	 * @param bundledFareIds
	 * @return
	 * @throws ModuleException
	 */
		
	public List<BundledFareDTO> getBundledFareDTOsByBundlePeriodIds(Collection<Integer> bundledFarePeriodIds) throws ModuleException;

	/**
	 * Add new bundled fare configuration
	 *
	 * @param bundledFareTO
	 * @return
	 * @throws ModuleException
	 */
	public Integer addBundledFareConfiguration(BundledFareConfigurationTO bundledFareTO) throws ModuleException;

	/**
	 * Edit existing bundled fare configuration
	 *
	 * @param bundledFareTO
	 * @throws ModuleException
	 */
	public Integer updateBundledFareConfiguration(BundledFareConfigurationTO bundledFareTO) throws ModuleException;

	/**
	 *
	 * @param bundledFareSelectionCriteria
	 * @return
	 */
	public List<BundledFareLiteDTO>
			getApplicableBundledFares(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria);

	/**
	 * returns free services included for a given pnrSegId
	 *
	 * @param pnrSegId
	 * @return
	 */
	public List<BundledFareFreeService> getOfferedServices(String pnrSegId);

	public boolean isChargeCodeLinkedToBundledFare(String chargeCode) throws ModuleException;;

	public Page<BundleFareCategoryDTO> searchBundledFareCategories(BundleCategorySearch bundleSearch) throws ModuleException;

	public BundleFareCategoryDTO createOrUpdateCategory(BundleFareCategoryDTO categories) throws ModuleException;

	public boolean deleteCategory(BundleFareCategoryDTO categories) throws ModuleException;

	public List<String> getAllCategoryPriorities();
	
	public BundledFareDisplayDTO getBundledFareDisplayDTO(Integer bundledFarePeriodId) throws ModuleException;
	
    public void saveBundledFareDisplaySettings(BundledFareDisplaySettingsDTO displaySettingsDTO) throws ModuleException;

	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateById(Integer bundleFareDescTemplateId);

	public void saveOrUpdateBundleFareDescriptionTemplate(BundleFareDescriptionTemplateDTO templateDTO)
			throws ModuleException;

	public Page<BundleFareDescriptionTemplateDTO> getBundleFareDescriptionTemplate(String templateName, Integer start,
			Integer recSize) throws ModuleException;

	public void updateBundleFareDescriptionTranslationTemplate(BundleFareDescriptionTranslationTemplateDTO translationTemplate)
			throws ModuleException;
}
