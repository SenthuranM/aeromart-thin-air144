package com.isa.thinair.promotion.api.model;

/**
 * Enum holding the Anci Offer types.
 * 
 * @author thihara
 * 
 */
public enum AnciOfferType {
	SEAT, MEAL, BAGGAGE, INSURANCE
}
