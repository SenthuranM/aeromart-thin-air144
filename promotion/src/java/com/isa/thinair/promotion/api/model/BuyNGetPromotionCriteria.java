package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Class defining the Buy & get free promotion criteria.
 * 
 * @author thihara
 * 
 * @hibernate.subclass discriminator-value = "BUYNGET"
 */
public class BuyNGetPromotionCriteria extends PromotionCriteria {

	private static final long serialVersionUID = 598445645641L;

	/**
	 * Limiting load factor of a flight for this promotion. Load Factor = Number of Pax / Flight Capacity X 100
	 */
	private BigDecimal limitingLoadFactor;

	private Set<BuyNGetQuantity> freeCriteria;

	private Boolean constPaxName;

	private Boolean constPaxSector;
	
	private String applyTo;

	/**
	 * @hibernate.property column = "LIMITING_LOAD_FACTOR"
	 * @return The limiting load factor for a flight that will cut off this promotion
	 */
	public BigDecimal getLimitingLoadFactor() {
		return limitingLoadFactor;
	}

	public void setLimitingLoadFactor(BigDecimal limitingLoadFactor) {
		this.limitingLoadFactor = limitingLoadFactor;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.BuyNGetQuantity"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * @return
	 */
	public Set<BuyNGetQuantity> getFreeCriteria() {
		return freeCriteria;
	}

	public void setFreeCriteria(Set<BuyNGetQuantity> freeCriteria) {
		this.freeCriteria = freeCriteria;
	}

	/**
	 * @hibernate.property column = "SAME_PAX_RESTRICTION" type = "yes_no"
	 * @see #discountType
	 */
	public Boolean getConstPaxName() {
		return constPaxName;
	}

	public void setConstPaxName(Boolean constPaxName) {
		this.constPaxName = constPaxName;
	}

	/**
	 * @hibernate.property column = "SAME_SECTOR_RESTRICTION" type = "yes_no"
	 * @see #discountType
	 */
	public Boolean getConstPaxSector() {
		return constPaxSector;
	}

	public void setConstPaxSector(Boolean constPaxSector) {
		this.constPaxSector = constPaxSector;
	}
	
	/**
	 * @hibernate.property column = "DISCOUNT_APPLY_TO"
	 * @see #applyTo
	 */
	public String getApplyTo() {
		return applyTo;
	}

	public void setApplyTo(String applyTo) {
		this.applyTo = applyTo;
	}
}
