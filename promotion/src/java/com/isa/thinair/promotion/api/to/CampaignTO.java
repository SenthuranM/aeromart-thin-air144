package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author chanaka
 *
 */
/**
 * @author chanaka
 *
 */
public class CampaignTO implements Serializable {

	private static final long serialVersionUID = -8686708192247074849L;
	
	private String campaignName;
	
	private String campaignDes;
	
	private String emailContent;
	
	private int noOfPromoCodes;
	
	private int noOfCustomers;
	
	private List<String> promoCodes;
	
	private List<String> customers;
	
	private Date startDate;
	
	private Date endDate;
	
	private String interval;
	
	private boolean scheduledCampaign = false;	
	
	private Map<String,String> emailPromoCodeMap;

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the campaignDes
	 */
	public String getCampaignDes() {
		return campaignDes;
	}

	/**
	 * @param campaignDes the campaignDes to set
	 */
	public void setCampaignDes(String campaignDes) {
		this.campaignDes = campaignDes;
	}

	/**
	 * @return the emailContent
	 */
	public String getEmailContent() {
		return emailContent;
	}

	/**
	 * @param emailContent the emailContent to set
	 */
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	/**
	 * @return the noOfPromoCodes
	 */
	public int getNoOfPromoCodes() {
		return noOfPromoCodes;
	}

	/**
	 * @param noOfPromoCodes the noOfPromoCodes to set
	 */
	public void setNoOfPromoCodes(int noOfPromoCodes) {
		this.noOfPromoCodes = noOfPromoCodes;
	}

	/**
	 * @return the noOfCustomers
	 */
	public int getNoOfCustomers() {
		return noOfCustomers;
	}

	/**
	 * @param noOfCustomers the noOfCustomers to set
	 */
	public void setNoOfCustomers(int noOfCustomers) {
		this.noOfCustomers = noOfCustomers;
	}

	/**
	 * @return the promoCodes
	 */
	public List<String> getPromoCodes() {
		return promoCodes;
	}

	/**
	 * @param promoCodes the promoCodes to set
	 */
	public void setPromoCodes(List<String> promoCodes) {
		this.promoCodes = promoCodes;
	}

	/**
	 * @return the customers
	 */
	public List<String> getCustomers() {
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(List<String> customers) {
		this.customers = customers;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the interval
	 */
	public String getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to set
	 */
	public void setInterval(String interval) {
		this.interval = interval;
	}

	/**
	 * @return the emailPromoCodeMap
	 */
	public Map<String, String> getEmailPromoCodeMap() {
		return emailPromoCodeMap;
	}

	/**
	 * @param emailPromoCodeMap the emailPromoCodeMap to set
	 */
	public void setEmailPromoCodeMap(Map<String, String> emailPromoCodeMap) {
		this.emailPromoCodeMap = emailPromoCodeMap;
	}

	/**
	 * @return the scheduledCampaign
	 */
	public boolean isScheduledCampaign() {
		return scheduledCampaign;
	}

	/**
	 * @param scheduledCampaign the scheduledCampaign to set
	 */
	public void setScheduledCampaign(boolean scheduledCampaign) {
		this.scheduledCampaign = scheduledCampaign;
	}

	
}
