package com.isa.thinair.promotion.api.to;

import java.io.Serializable;

/**
 * @author chethiya
 *
 */
public class VoucherSearchRequest implements Serializable{

	String voucherId;
	
	String issuedAgent;
	
	String email;
	
	String firstName;
	
	String lastName;

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getIssuedAgent() {
		return issuedAgent;
	}

	public void setIssuedAgent(String issuedAgent) {
		this.issuedAgent = issuedAgent;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
