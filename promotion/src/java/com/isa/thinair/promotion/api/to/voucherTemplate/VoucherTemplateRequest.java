package com.isa.thinair.promotion.api.to.voucherTemplate;

import java.io.Serializable;

import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * @author chanaka
 *
 */
public class VoucherTemplateRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer voucherId;
	
	private String voucherName;
	
	private String status = PromotionCriteriaConstants.VoucherTemplateStatus.ACTIVE;

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public Integer getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Integer voucherId) {
		this.voucherId = voucherId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
