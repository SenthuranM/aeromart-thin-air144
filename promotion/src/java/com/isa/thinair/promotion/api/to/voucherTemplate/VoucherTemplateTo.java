package com.isa.thinair.promotion.api.to.voucherTemplate;

import java.io.Serializable;
import java.util.Date;

public class VoucherTemplateTo implements Serializable {

	private static final long serialVersionUID = 5677981229264831487L;

	private int voucherId;
	private String voucherName;
	private String amount;
	private String amountInLocal;
	private String currencyCode;
	private int voucherValidity;
	private Date salesFrom;
	private Date salesTo;
	private String remarks;
	private String status;
	private String ccTnxFee;
	private String fullPayment;

	/**
	 * @return the voucherID
	 */
	public int getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherID
	 *            the voucherID to set
	 */
	public void setVoucherID(int voucherID) {
		this.voucherId = voucherID;
	}

	/**
	 * @return the voucherName
	 */
	public String getVoucherName() {
		return voucherName;
	}

	/**
	 * @param voucherName
	 *            the voucherName to set
	 */
	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the amountInLocal
	 */
	public String getAmountInLocal() {
		return amountInLocal;
	}

	/**
	 * @param amountInLocal
	 *            the amountInLocal to set
	 */
	public void setAmountInLocal(String amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the voucherValidity
	 */
	public int getVoucherValidity() {
		return voucherValidity;
	}

	/**
	 * @param voucherValidity
	 *            the voucherValidity to set
	 */
	public void setVoucherValidity(int voucherValidity) {
		this.voucherValidity = voucherValidity;
	}

	/**
	 * @return the salesFrom
	 */
	public Date getSalesFrom() {
		return salesFrom;
	}

	/**
	 * @param date
	 *            the salesFrom to set
	 */
	public void setSalesFrom(Date date) {
		this.salesFrom = date;
	}

	/**
	 * @return the salesTo
	 */
	public Date getSalesTo() {
		return salesTo;
	}

	/**
	 * @param salesTo
	 *            the salesTO to set
	 */
	public void setSalesTo(Date salesTo) {
		this.salesTo = salesTo;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param voucherId
	 *            the voucherId to set
	 */
	public void setVoucherId(int voucherId) {
		this.voucherId = voucherId;
	}

	public String getCcTnxFee() {
		return ccTnxFee;
	}

	public void setCcTnxFee(String ccTnxFee) {
		this.ccTnxFee = ccTnxFee;
	}

	public String getFullPayment() {
		return fullPayment;
	}

	public void setFullPayment(String fullPayment) {
		this.fullPayment = fullPayment;
	}

}
