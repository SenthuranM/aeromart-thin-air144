package com.isa.thinair.promotion.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * @author chethiya
 *
 * @hibernate.class table = "T_VOUCHER"
 */
public class Voucher implements Serializable, Comparable<Voucher> {

	private static final long serialVersionUID = -7262284832045730298L;

	/** Holds the voucher ID */
	private String voucherId;

	/** Holds the voucher valid from date */
	private Date validFrom;

	/** Holds the voucher valid to date */
	private Date validTo;

	/** Holds the voucher value in base currency */
	private BigDecimal amountInBase;

	/** Holds the voucher value in local currency */
	private BigDecimal amountInLocal;

	/** Holds the local currency code */
	private String currencyCode;

	/** Holds the ID of the voucher issued user */
	private String issuedUserId;

	/** Holds the voucher issued time */
	private Date issuedTime;

	/** Holds the template id If the voucher is a gift voucher the */
	private Integer templateId;

	/** Holds whether the template is "ISSUED", "CANCELED" or "REDEEMED" */
	private Character status;

	/** Holds the first name of the passenger the voucher issued to */
	private String paxFirstName;

	/** Holds the last name of the passenger the voucher issued to */
	private String paxLastName;

	/** Holds the email of the passenger the voucher issued to */
	private String email;

	/** Holds the agent/user remarks */
	private String remarks;

	/** Holds the redeemed amount of the voucher after redemption */
	private BigDecimal redeemAmount;

	/** Holds the agent/user cancel remarks */
	private String cancelRemarks;

	/** Holds the voucher ID */
	private String voucherGroupId;

	/** Holds the payment of the particular voucher */
	Set<VoucherPayment> voucherPayment;

	private String mobileNumber;

	/**
	 * @return the voucherId
	 * 
	 * @hibernate.id column = "VOUCHER_ID" generator-class = "assigned"
	 * 
	 */
	public String getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherId
	 *            the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the validFrom
	 * 
	 * @hibernate.property column = "VALID_FROM"
	 * 
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 * 
	 * @hibernate.property column = "VALID_TO"
	 * 
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the amountInBase
	 * 
	 * @hibernate.property column = "AMOUNT"
	 *
	 */
	public BigDecimal getAmountInBase() {
		return amountInBase;
	}

	/**
	 * @param amountInBase
	 *            the amountInBase to set
	 */
	public void setAmountInBase(BigDecimal amountInBase) {
		this.amountInBase = amountInBase;
	}

	/**
	 * @return the amountInLocal
	 * 
	 * @hibernate.property column = "AMOUNT_IN_LOCAL"
	 * 
	 */
	public BigDecimal getAmountInLocal() {
		return amountInLocal;
	}

	/**
	 * @param amountInLocal
	 *            the amountInLocal to set
	 */
	public void setAmountInLocal(BigDecimal amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @return the currencyCode
	 * 
	 * @hibernate.property column = "CURRENCY_CODE"
	 * 
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the issuedUserId
	 * 
	 * @hibernate.property column = "ISSUED_USER_ID"
	 * 
	 */
	public String getIssuedUserId() {
		return issuedUserId;
	}

	/**
	 * @param issuedUserId
	 *            the issuedUserId to set
	 */
	public void setIssuedUserId(String issuedUserId) {
		this.issuedUserId = issuedUserId;
	}

	/**
	 * @return the issuedTime
	 * 
	 * @hibernate.property column = "ISSUED_TIME"
	 * 
	 */
	public Date getIssuedTime() {
		return issuedTime;
	}

	/**
	 * @param issuedTime
	 *            the issuedTime to set
	 */
	public void setIssuedTime(Date issuedTime) {
		this.issuedTime = issuedTime;
	}

	/**
	 * @return the templateId
	 * 
	 * @hibernate.property column = "VOUCHER_TEMPLATE_ID"
	 * 
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the status
	 * 
	 * @hibernate.property column = "STATUS"
	 * 
	 */
	public Character getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Character status) {
		this.status = status;
	}

	/**
	 * @return the paxFirstName
	 * 
	 * @hibernate.property column = "PAX_FIRST_NAME"
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}

	/**
	 * @param paxFirstName
	 *            the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	/**
	 * @return the paxLastName
	 * 
	 * @hibernate.property column = "PAX_LAST_NAME"
	 */
	public String getPaxLastName() {
		return paxLastName;
	}

	/**
	 * @param paxLastName
	 *            the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}

	/**
	 * @return the presentedToEmail
	 * 
	 * @hibernate.property column = "PAX_EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param presentedToEmail
	 *            the presentedToEmail to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the remarks
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the redeemdAmount
	 * 
	 * @hibernate.property column = "REDEEM_AMOUNT"
	 */
	public BigDecimal getRedeemAmount() {
		return redeemAmount;
	}

	/**
	 * @param redeemAmount
	 *            the redeemdAmount to set
	 */
	public void setRedeemAmount(BigDecimal redeemAmount) {
		this.redeemAmount = redeemAmount;
	}

	/**
	 * @return the cancelRemarks
	 * 
	 * @hibernate.property column = "CANCEL_REMARKS"
	 */
	public String getCancelRemarks() {
		return cancelRemarks;
	}

	/**
	 * @param cancelRemarks
	 *            the cancelRemarks to set
	 */
	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="VOUCHER_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.VoucherPayment"
	 */

	public Set<VoucherPayment> getVoucherPayment() {
		return voucherPayment;
	}

	/**
	 * @param voucherPayment
	 *            the voucherPayment to set
	 */
	public void setVoucherPayment(Set<VoucherPayment> voucherPayment) {
		this.voucherPayment = voucherPayment;
	}

	@Override
	public int compareTo(Voucher voucher) {
		BigDecimal thisAmount = this.getAmountInBase();
		BigDecimal otherAmount = voucher.getAmountInBase();
		return thisAmount.compareTo(otherAmount);
	}

	/**
	 * @return the voucherGroupId
	 * @hibernate.property column = "VOUCHER_GROUP_ID"
	 */
	public String getVoucherGroupId() {
		return voucherGroupId;
	}

	/**
	 * @param voucherGroupId
	 *            the voucherGroupId to set
	 */
	public void setVoucherGroupId(String voucherGroupId) {
		this.voucherGroupId = voucherGroupId;
	}

	/**
	 * @return the mobileNumber
	 * 
	 * @hibernate.property column = "MOBILE_NUMBER"
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
