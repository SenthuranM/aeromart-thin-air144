package com.isa.thinair.promotion.api.to.constants;

public interface BundledFareConstants {

	interface DisplayKeys {
		String name = "name";
		String description = "description";
	}

	String IMG_PREFIX = "bundledFare_";
	String IMG_SUFFIX = ".png";

	String IMG_STRIPE_PREFIX = "bundledFareStripe_";
	String IMG_STRIPE_SUFFIX = ".png";

	String IMG_TYPE_BUNDLE = "bundle";
	String IMG_TYPE_STRIPE = "stripe";

}
