package com.isa.thinair.promotion.api.model.lms;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.promotion.api.model.lms.BasicDataType.DATA_F;

/**
 * 
 * @author rumesh
 * 
 */
public abstract class OrderedElementContainer {

	public static String SEPARATOR = "|";

	private List<BasicDataType> elementList = new ArrayList<BasicDataType>();

	private Integer errorRecordReference;

	public List<BasicDataType> getElementList() {
		return elementList;
	}

	public void addElement(String elementValue, DATA_F dataF) {
		BasicDataType basicDataType = new BasicDataType(dataF, elementValue);
		elementList.add(basicDataType);
	}

	public String getElementData(int elementIndex) {
		BasicDataType basicDataType = getElement(elementIndex);
		return (basicDataType == null) ? null : basicDataType.getData();
	}

	public boolean isValidFormat() {
		for (BasicDataType dataType : elementList) {
			if (dataType.getDataField() == DATA_F.MANDATORY && (dataType.getData() == null || "".equals(dataType.getData()))) {
				return false;
			}
		}

		return true;
	}

	public Integer getErrorRecordReference() {
		return errorRecordReference;
	}

	public void setErrorRecordReference(Integer errorRecordReference) {
		this.errorRecordReference = errorRecordReference;
	}

	private BasicDataType getElement(int elementIndex) {
		if (elementList.size() > elementIndex) {
			return elementList.get(elementIndex);
		}

		return null;
	}
}
