package com.isa.thinair.promotion.api.model;

import java.util.Set;

/**
 * Class defining Free service promoiton criteria.
 * 
 * @author thihara
 * 
 * @hibernate.subclass discriminator-value = "FREESERVICE"
 */
public class FreeServicePromotionCriteria extends PromotionCriteria {

	private static final long serialVersionUID = 11145557895565L;

	/**
	 * Ancillaries that will qualify for this promotion.
	 */
	private Set<String> applicableAncillaries;

	/**
	 * Discount details to be added.
	 */
	private DiscountDetails discount;

	/**
	 * @hibernate.set lazy="false" cascade="all" table="T_PROMO_FREE_SERVICE_ANCI"
	 * @hibernate.collection-element column="ANCI_TYPE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableAncillaries
	 */
	public Set<String> getApplicableAncillaries() {
		return applicableAncillaries;
	}

	public void setApplicableAncillaries(Set<String> applicableAncillaries) {
		this.applicableAncillaries = applicableAncillaries;
	}

	/**
	 * @hibernate.component class = "com.isa.thinair.promotion.api.model.DiscountDetails"
	 * @see #discount
	 */
	public DiscountDetails getDiscount() {
		return discount;
	}

	public void setDiscount(DiscountDetails discount) {
		this.discount = discount;
	}
}
