package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;

/**
 * 
 * @author rumesh
 * 
 */
public class PromoSelectionCriteria implements Serializable {

	private static final long serialVersionUID = -8816540714809385621L;

	// common parameters for all promotion types
	private String promoCode;
	private String promoType;
	private List<String> ondList;
	private List<String> flights;
	private Set<Integer> flightIds;
	private Set<Integer> flightSegIds;
	private int salesChannel;
	private Date reservationDate;
	private Map<Integer, Date> ondFlightDates;
	private String agent;
	private Set<String> cabinClasses;
	private Set<String> logicalCabinClasses;
	private Set<String> bookingClasses;
	private JourneyType journeyType;
	private String preferredLanguage;

	// parameters for buy and get free promotions
	private Integer adultCount;
	private Integer childCount;
	private Integer infantCount;

	// parameters for discount promotions
	private Integer bankIdNo;

	private String dryOperatingAirline;
	
	private Map<Integer, String> flightSegWiseLogicalCabinClass;

	private String posCountry;
	
	private boolean isRegisteredMember;
    
    private boolean isLoyalityMember;

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoType() {
		return promoType;
	}

	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}

	public List<String> getOndList() {
		if (ondList == null) {
			ondList = new ArrayList<String>();
		}
		return ondList;
	}

	public void setOndList(List<String> ondList) {
		this.ondList = ondList;
	}

	public List<String> getFlights() {
		if (flights == null) {
			flights = new ArrayList<String>();
		}
		return flights;
	}

	public void setFlights(List<String> flights) {
		this.flights = flights;
	}

	public Set<Integer> getFlightIds() {
		if (flightIds == null) {
			flightIds = new HashSet<Integer>();
		}
		return flightIds;
	}

	public void setFlightIds(Set<Integer> flightIds) {
		this.flightIds = flightIds;
	}

	public Set<Integer> getFlightSegIds() {
		if (flightSegIds == null) {
			flightSegIds = new HashSet<Integer>();
		}
		return flightSegIds;
	}

	public void setFlightSegIds(Set<Integer> flightSegIds) {
		this.flightSegIds = flightSegIds;
	}

	public int getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(int salesChannel) {
		this.salesChannel = salesChannel;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Map<Integer, Date> getOndFlightDates() {
		if (this.ondFlightDates == null) {
			this.ondFlightDates = new HashMap<Integer, Date>();
		}
		return ondFlightDates;
	}

	public void setOndFlightDates(Map<Integer, Date> ondFlightDates) {
		this.ondFlightDates = ondFlightDates;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public Set<String> getCabinClasses() {
		if (this.cabinClasses == null) {
			this.cabinClasses = new HashSet<String>();
		}
		return cabinClasses;
	}

	public void setCabinClasses(Set<String> cabinClasses) {
		this.cabinClasses = cabinClasses;
	}

	public Set<String> getLogicalCabinClasses() {
		if (logicalCabinClasses == null) {
			logicalCabinClasses = new HashSet<String>();
		}
		return logicalCabinClasses;
	}

	public void setLogicalCabinClasses(Set<String> logicalCabinClasses) {
		this.logicalCabinClasses = logicalCabinClasses;
	}

	public Set<String> getBookingClasses() {
		if (bookingClasses == null) {
			bookingClasses = new HashSet<String>();
		}
		return bookingClasses;
	}

	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public JourneyType getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(JourneyType journeyType) {
		this.journeyType = journeyType;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public Integer getBankIdNo() {
		return bankIdNo;
	}

	public void setBankIdNo(Integer bankIdNo) {
		this.bankIdNo = bankIdNo;
	}

	public String getDryOperatingAirline() {
		return dryOperatingAirline;
	}

	public void setDryOperatingAirline(String dryOperatingAirline) {
		this.dryOperatingAirline = dryOperatingAirline;
	}

	public Map<Integer, String> getFlightSegWiseLogicalCabinClass() {
		if (flightSegWiseLogicalCabinClass == null) {
			flightSegWiseLogicalCabinClass = new HashMap<Integer, String>();
		}
		return flightSegWiseLogicalCabinClass;
	}

	public void setFlightSegWiseLogicalCabinClass(Map<Integer, String> flightSegWiseLogicalCabinClass) {
		this.flightSegWiseLogicalCabinClass = flightSegWiseLogicalCabinClass;
	}

	public String getPosCountry() {
		return posCountry;
	}

	public void setPosCountry(String posCountry) {
		this.posCountry = posCountry;
	}

	/**
	 * @return the isRegisteredMember
	 */
	public boolean isRegisteredMember() {
		return isRegisteredMember;
	}

	/**
	 * @param isRegisteredMember the isRegisteredMember to set
	 */
	public void setRegisteredMember(boolean isRegisteredMember) {
		this.isRegisteredMember = isRegisteredMember;
	}

	/**
	 * @return the isLoyalityMember
	 */
	public boolean isLoyalityMember() {
		return isLoyalityMember;
	}

	/**
	 * @param isLoyalityMember the isLoyalityMember to set
	 */
	public void setLoyalityMember(boolean isLoyalityMember) {
		this.isLoyalityMember = isLoyalityMember;
	}
	
	
}
