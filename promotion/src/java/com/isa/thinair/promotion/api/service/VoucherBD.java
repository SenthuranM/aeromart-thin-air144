package com.isa.thinair.promotion.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateDTO;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;

public interface VoucherBD {

	public static final String SERVICE_NAME = "Voucher";

	/**
	 * this saves(Generates) a new voucher
	 * 
	 * @param voucher
	 * @throws ModuleException
	 */
	public void saveVoucher(Voucher voucher) throws ModuleException;

	/**
	 * @param start
	 * @param pageSize
	 * @param voucherSearchRequest
	 * @param giftVoucher
	 * @return returns the Page of Vouchers for the given start and pageSize
	 */
	public Page<VoucherDTO> searchVoucher(int start, int pageSize, VoucherSearchRequest voucherSearchRequest, String voucherType)
			throws ModuleException;

	/**
	 * this updates a given voucher
	 * 
	 * @param voucher
	 * @throws ModuleException
	 */
	public void updateVoucher(Voucher voucher) throws ModuleException;

	/**
	 * @param voucherDTO TODO
	 * @return returns the next Voucher ID
	 * @throws ModuleException
	 */
	public void issueVoucher(VoucherDTO voucherDTO) throws ModuleException;
	
	/**
	 * this cancels a given voucher
	 * 
	 * @param voucher
	 * @throws ModuleException
	 */
	public void cancelVoucher(VoucherDTO voucherDTO, UserPrincipal userPrincipal)
			throws ModuleException;
	
	/**
	 * this emails the voucher
	 * 
	 * @param voucher
	 * @throws ModuleException
	 */
	public void emailVoucher(VoucherDTO voucherDTO) throws ModuleException;
	
	/**
	 * this prints the voucher
	 * 
	 * @param voucher
	 * @throws ModuleException
	 */
	public String printVoucher(VoucherDTO voucherDTO) throws ModuleException;

	/**
	 * @param voucherIdList
	 * @return
	 * @throws ModuleException
	 */
	public VoucherRedeemResponse redeemIssuedVouchers(VoucherRedeemRequest voucherIdList) throws ModuleException;

	/**
	 * This records the voucher redemption by each passenger
	 *
	 * @param voucherID
	 * @param paxTnxID
	 * @param extCarrierPaxTnxID TODO
	 * @throws ModuleException
	 */
	public void recordRedemption(String voucherID, Integer paxTnxID, String redeemAmount, Integer extCarrierPaxTnxID) throws ModuleException;

	/**
	 * This will unblock redeemed vouchers on browser close
	 * 
	 * @param voucherIDList
	 * @throws ModuleException
	 */
	public void unblockRedeemedVouchers(ArrayList<String> voucherIDList) throws ModuleException;
	
	public Page<VoucherTermsTemplateDTO> getVoucherTermsTemplatePage(VoucherTermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize)
			throws ModuleException;
	
	public void updateVoucherTermsTemplate(VoucherTermsTemplateDTO termsTemplateDTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException;
	
	public Collection<VoucherTermsTemplateDTO> getVoucherTermsTemplates() throws ModuleException;
	
	
	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize, ReprotectedPaxSearchrequest reprotectedPaxSearchrequest)
			throws ModuleException;

	public void issueVouchersForReprotectedPax(ArrayList<ReprotectedPaxDTO> selectedPax, ArrayList<VoucherDTO> voucherDTOs, String issueBy);
	
	public void expireVouchers();
	
	public List<VoucherDTO> prepareVouchersForIBEPayment(List<VoucherDTO> voucherDTOs, String payCurrency, String payCurrencyAmount) throws ModuleException;

	public List<VoucherDTO> prepareVouchersForIBEPayment(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmount, BigDecimal payAmountInBase, TrackInfoDTO trackInfoDTO) throws ModuleException;
	
	public void issueGiftVouchersFromIBE(List<VoucherDTO> voucherDTOs) throws ModuleException;
	
	public void issueGiftVouchersFromXBE(List <VoucherDTO> voucherDTOs, CredentialsDTO credentialsDTO, String paymentMethod, String fulAmountInBase) throws ModuleException;
	
	public void unblockVouchers();
	
	public void issueGiftVouchersForIBE(List <VoucherDTO> voucherDTOs, CredentialsDTO credentialsDTO, String fulAmountInBase) throws ModuleException;

	public Voucher getVoucher(String voucherID) throws ModuleException;

	public List<VoucherDTO> getVouchersFromGroupId(String groupId) throws ModuleException;
}	
	
