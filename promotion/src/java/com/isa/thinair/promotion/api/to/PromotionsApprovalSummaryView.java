package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Date;

public class PromotionsApprovalSummaryView implements Serializable {

	private String flightNumber;
	private Date departureDate;
	private String segmentCode;
	private int totalRequests = 0;
	private int activeRequests;
	private int approvedRequests;
	private int rejectedRequests;
	private double refundAmount;
	private int totalSeats;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getTotalRequests() {
		return totalRequests;
	}

	public void addToTotalRequests(int totalRequests) {
		this.totalRequests += totalRequests;
	}

	public int getActiveRequests() {
		return activeRequests;
	}

	public void setActiveRequests(int activeRequests) {
		this.activeRequests = activeRequests;
	}

	public int getApprovedRequests() {
		return approvedRequests;
	}

	public void setApprovedRequests(int approvedRequests) {
		this.approvedRequests = approvedRequests;
	}

	public int getRejectedRequests() {
		return rejectedRequests;
	}

	public void setRejectedRequests(int rejectedRequests) {
		this.rejectedRequests = rejectedRequests;
	}

	public double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

}
