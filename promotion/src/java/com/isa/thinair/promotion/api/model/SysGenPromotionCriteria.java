package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;

/**
 * Class to store system generated promotion with user credits
 * 
 * @author rumesh
 * 
 * @hibernate.subclass discriminator-value = "SYSGEN"
 * 
 */
public class SysGenPromotionCriteria extends PromotionCriteria {

	private static final long serialVersionUID = 5933041835121478139L;

	private String originPnr;

	private BigDecimal usedCredit;

	private DiscountDetails discount;

	/**
	 * @return the originPnr
	 * @hibernate.property column = "ORIGIN_PNR"
	 */
	public String getOriginPnr() {
		return originPnr;
	}

	/**
	 * @param originPnr
	 *            the originPnr to set
	 */
	public void setOriginPnr(String originPnr) {
		this.originPnr = originPnr;
	}

	/**
	 * @hibernate.property column = "USED_CREDIT"
	 */
	public BigDecimal getUsedCredit() {
		return usedCredit;
	}

	public void setUsedCredit(BigDecimal usedCredit) {
		this.usedCredit = usedCredit;
	}

	/**
	 * @hibernate.component class = "com.isa.thinair.promotion.api.model.DiscountDetails"
	 * 
	 */
	public DiscountDetails getDiscount() {
		return discount;
	}

	public void setDiscount(DiscountDetails discount) {
		this.discount = discount;
	}
}
