package com.isa.thinair.promotion.api.model;

import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * Abstract super class with common properties of all the PromoitonCriteria s. Currently
 * {@link BuyNGetPromotionCriteria}, {@link DiscountPromotionCriteria}, {@link FreeServicePromotionCriteria}
 * 
 * @author thihara
 * 
 * @hibernate.class table = "T_PROMOTION_CRITERIA"
 * @hibernate.discriminator column = "DISCRIMINATOR"
 */
public abstract class PromotionCriteria extends Persistent {

	private static final long serialVersionUID = 46543546857L;

	/**
	 * ID field for PromoitonCriteria's
	 */
	protected Long promoCriteriaID;

	/**
	 * ONDs that qualify for this promotion. Will be in the format of ORIGIN/DESTINATION (SHJ/CMB)
	 */
	protected Set<String> applicableONDs;

	/**
	 * Flights that qualify for this promotion.
	 */
	protected Set<String> flightNOs;

	/**
	 * Class Of Services qualifying for this promotion.
	 */
	protected Set<String> applicableCOSs;

	/**
	 * Logical cabin classes qualifying for this promotion.
	 */
	protected Set<String> applicableLCCs;

	/**
	 * Booking lasses qualifying for this promotion
	 */
	protected Set<String> applicableBCs;

	/**
	 * Sales channels qualifying for this promotion.
	 */
	protected Set<String> applicableSalesChannels;

	/**
	 * Agents qualifying for this promotion
	 */
	protected Set<String> applicableAgents;

	/**
	 * Boolean indicating whether a promotion code is enabled for this promotion.
	 */
	protected Boolean promoCodeEnabled = Boolean.FALSE;

	/**
	 * Promotion code required to activate this promotion. (Only applicable if promoCodeEnabled variable is set to true)
	 */
	protected String promoCode;

	/**
	 * Holds description of the promotion
	 */
	protected String promoName;

	/**
	 * Number of times this promotion is allowed to be applied for a single flight
	 */
	protected Integer perFlightLimit;

	/**
	 * Total number of number of times this promotion is allowed to be applied.
	 */
	protected Integer totalLimit;

	/**
	 * Number of times this promotion has already been applied.
	 */
	protected Integer appliedAmount;

	/**
	 * Boolean indicating whether to restrict modification of reservations that this promotion has been applied to.
	 */
	protected Boolean restrictAppliedResModification = Boolean.FALSE;

	/**
	 * Boolean indicating whether to restrict cancellation of reservations that this promotion has been applied to.
	 */
	protected Boolean restrictAppliedResCancelation = Boolean.FALSE;

	/**
	 * Boolean indicating whether to restrict splitting of reservations that this promotion has been applied to.
	 */
	protected Boolean restrictAppliedResSplitting = Boolean.FALSE;

	/**
	 * Boolean indicating whether to restrict remove pax of reservations that this promotion has been applied to.
	 */
	protected Boolean restrictAppliedResRemovePax = Boolean.FALSE;

	/**
	 * String indicating the status of this promotion.
	 */
	protected String status = PromotionCriteriaConstants.PromotionStatus.ACTIVE;

	private Boolean systemGenerated = Boolean.FALSE;

	private Boolean applicableForOneway = Boolean.FALSE;

	private Boolean applicableForReturn = Boolean.FALSE;

	// applicable member selection
	private Boolean applicableForLoyaltyMembers = Boolean.FALSE;

	private Boolean applicableForRegisteredMembers = Boolean.FALSE;

	private Boolean applicableForGuests = Boolean.FALSE;

	private Integer numByAgent;

	private Integer numBySalesChannel;

	private String applicability;

	private String i18nMessageKey;

	protected Set<PromoCode> promoCodes;

	private Set<RegistrationPeriod> registrationPeriods;

	private Set<FlightPeriod> flightPeriods;

	/**
	 * Pos countries qualifying for this promotion
	 */
	protected Set<String> applicablePosCountries;

	/**
	 * @hibernate.id column = "PROMO_CRITERIA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PROMOTION_CRITERIA"
	 * @return ID of the promotion criteria.
	 */
	public Long getPromoCriteriaID() {
		return promoCriteriaID;
	}

	public void setPromoCriteriaID(Long promoCriteriaID) {
		this.promoCriteriaID = promoCriteriaID;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_OND"
	 * @hibernate.collection-element column="OND_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableONDs
	 */
	public Set<String> getApplicableONDs() {
		return applicableONDs;
	}

	public void setApplicableONDs(Set<String> applicableONDs) {
		this.applicableONDs = applicableONDs;
	}

	/**
	 * 
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_FLIGHT"
	 * @hibernate.collection-element column="FLIGHT_NUMBER" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 *                           Directly unmapped fields : T_PROMO_CRITERIA_FLIGHT -> NO_APPLIED (Number of times this
	 *                           promotion has been applied to this flight)
	 * 
	 * @see #flightNOs
	 */
	public Set<String> getFlightNOs() {
		return flightNOs;
	}

	public void setFlightNOs(Set<String> flightNOs) {
		this.flightNOs = flightNOs;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_COS"
	 * @hibernate.collection-element column="CABIN_CLASS_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableCOSs
	 */
	public Set<String> getApplicableCOSs() {
		return applicableCOSs;
	}

	public void setApplicableCOSs(Set<String> applicableCOSs) {
		this.applicableCOSs = applicableCOSs;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_LCC"
	 * @hibernate.collection-element column="LOGICAL_CABIN_CLASS_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableLCCs
	 */
	public Set<String> getApplicableLCCs() {
		return applicableLCCs;
	}

	public void setApplicableLCCs(Set<String> applicableLCCs) {
		this.applicableLCCs = applicableLCCs;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_BC"
	 * @hibernate.collection-element column="BOOKING_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableBCs
	 */
	public Set<String> getApplicableBCs() {
		return applicableBCs;
	}

	public void setApplicableBCs(Set<String> applicableBCs) {
		this.applicableBCs = applicableBCs;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_CHANNEL"
	 * @hibernate.collection-element column="SALES_CHANNEL_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableSalesChannels
	 */
	public Set<String> getApplicableSalesChannels() {
		return applicableSalesChannels;
	}

	public void setApplicableSalesChannels(Set<String> applicableSalesChannels) {
		this.applicableSalesChannels = applicableSalesChannels;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_AGENT"
	 * @hibernate.collection-element column="AGENT_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicableAgents
	 */
	public Set<String> getApplicableAgents() {
		return applicableAgents;
	}

	public void setApplicableAgents(Set<String> applicableAgents) {
		this.applicableAgents = applicableAgents;
	}

	/**
	 * @hibernate.property column = "IS_PROMOCODE_ENABLED" type = "yes_no"
	 * 
	 * @see #promoCodeEnabled
	 */
	public Boolean getPromoCodeEnabled() {
		return promoCodeEnabled;
	}

	public void setPromoCodeEnabled(Boolean promoCodeEnabled) {
		this.promoCodeEnabled = promoCodeEnabled;
	}

	/**
	 * @hibernate.property column = "PROMO_CODE"
	 * 
	 * @see #promoCode
	 */
	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * @hibernate.property column = "PROMO_NAME"
	 * 
	 * @return the promoName
	 */
	public String getPromoName() {
		return promoName;
	}

	/**
	 * @param promoName
	 *            the promoName to set
	 */
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	/**
	 * @hibernate.property column = "PER_FLIGHT_LIMIT"
	 * 
	 * @see #perFlightLimit
	 */
	public Integer getPerFlightLimit() {
		return perFlightLimit;
	}

	public void setPerFlightLimit(Integer perFlightLimit) {
		this.perFlightLimit = perFlightLimit;
	}

	/**
	 * @hibernate.property column = "TOTAL_LIMIT"
	 * 
	 * @see #totalLimit
	 */
	public Integer getTotalLimit() {
		return totalLimit;
	}

	public void setTotalLimit(Integer totalLimit) {
		this.totalLimit = totalLimit;
	}

	/**
	 * @hibernate.property column = "APPLIED_AMOUNT"
	 * 
	 * @see #appliedAmount
	 */
	public Integer getAppliedAmount() {
		return appliedAmount;
	}

	public void setAppliedAmount(Integer appliedAmount) {
		this.appliedAmount = appliedAmount;
	}

	/**
	 * @hibernate.property column = "RESTRICT_MOD" type = "yes_no"
	 * 
	 * @see #restrictAppliedResModification
	 */
	public Boolean getRestrictAppliedResModification() {
		return restrictAppliedResModification;
	}

	public void setRestrictAppliedResModification(Boolean restrictAppliedResModification) {
		this.restrictAppliedResModification = restrictAppliedResModification;
	}

	/**
	 * @hibernate.property column = "RESTRICT_CNX" type = "yes_no"
	 * 
	 * @see #restrictAppliedResCancelation
	 */
	public Boolean getRestrictAppliedResCancelation() {
		return restrictAppliedResCancelation;
	}

	public void setRestrictAppliedResCancelation(Boolean restrictAppliedResCancelation) {
		this.restrictAppliedResCancelation = restrictAppliedResCancelation;
	}

	/**
	 * @hibernate.property column = "RESTRICT_SPLIT" type = "yes_no"
	 * 
	 * @see #restrictAppliedResSplitting
	 */
	public Boolean getRestrictAppliedResSplitting() {
		return restrictAppliedResSplitting;
	}

	public void setRestrictAppliedResSplitting(Boolean restrictAppliedResSplitting) {
		this.restrictAppliedResSplitting = restrictAppliedResSplitting;
	}

	/**
	 * @hibernate.property column = "RESTRICT_REMOVE_PAX" type = "yes_no"
	 * 
	 * @see #restrictAppliedResSplitting
	 */
	public Boolean getRestrictAppliedResRemovePax() {
		return restrictAppliedResRemovePax;
	}

	public void setRestrictAppliedResRemovePax(Boolean restrictAppliedResRemovePax) {
		this.restrictAppliedResRemovePax = restrictAppliedResRemovePax;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 * @see #status
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "SYSTEM_GENERATED" type = "yes_no"
	 * 
	 */
	public Boolean getSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(Boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_FOR_ONEWAY" type = "yes_no"
	 * 
	 */
	public Boolean getApplicableForOneway() {
		return applicableForOneway;
	}

	public void setApplicableForOneway(Boolean applicableForOneway) {
		this.applicableForOneway = applicableForOneway;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_FOR_RETURN" type = "yes_no"
	 */
	public Boolean getApplicableForReturn() {
		return applicableForReturn;
	}

	public void setApplicableForReturn(Boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	// applicable member selection
	/**
	 * @hibernate.property column = "APPLICABLE_FOR_LOYALTY_MEMBERS" type = "yes_no"
	 */
	public Boolean getApplicableForLoyaltyMembers() {
		return applicableForLoyaltyMembers;
	}

	public void setApplicableForLoyaltyMembers(Boolean applicableForLoyaltyMembers) {
		this.applicableForLoyaltyMembers = applicableForLoyaltyMembers;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_FOR_REG_MEMBERS" type = "yes_no"
	 */
	public Boolean getApplicableForRegisteredMembers() {
		return applicableForRegisteredMembers;
	}

	public void setApplicableForRegisteredMembers(Boolean applicableForRegisteredMembers) {
		this.applicableForRegisteredMembers = applicableForRegisteredMembers;
	}

	/**
	 * @hibernate.property column = "APPLICABLE_FOR_GUESTS" type = "yes_no"
	 */
	public Boolean getApplicableForGuests() {
		return applicableForGuests;
	}

	public void setApplicableForGuests(Boolean applicableForGuests) {
		this.applicableForGuests = applicableForGuests;
	}

	/**
	 * @hibernate.property column = "NUM_BY_AGENT"
	 * 
	 * @see #numByAgent
	 */
	public Integer getNumByAgent() {
		return numByAgent;
	}

	public void setNumByAgent(Integer numByAgent) {
		this.numByAgent = numByAgent;
	}

	/**
	 * @hibernate.property column = "NUM_BY_SALES_CHANNEL"
	 * 
	 * @see #numBySalesChannel
	 */
	public Integer getNumBySalesChannel() {
		return numBySalesChannel;
	}

	public void setNumBySalesChannel(Integer numBySalesChannel) {
		this.numBySalesChannel = numBySalesChannel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((promoCriteriaID == null) ? 0 : promoCriteriaID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PromotionCriteria))
			return false;
		PromotionCriteria other = (PromotionCriteria) obj;
		if (promoCriteriaID == null) {
			if (other.promoCriteriaID != null)
				return false;
		} else if (!promoCriteriaID.equals(other.promoCriteriaID))
			return false;
		return true;
	}

	/**
	 * @hibernate.property column = "APPLICABILITY"
	 * 
	 * @see #Applicability
	 */
	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	/**
	 * @hibernate.property column = "I18N_MESSAGE_KEY"
	 * 
	 * @see #i18nMessageKey
	 */
	public String getI18nMessageKey() {
		return i18nMessageKey;
	}

	public void setI18nMessageKey(String i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.PromoCode"
	 */
	public Set<PromoCode> getPromoCodes() {
		return promoCodes;
	}

	/**
	 * @param promoCodes
	 *            the promoCodes to set
	 */
	public void setPromoCodes(Set<PromoCode> promoCodes) {
		this.promoCodes = promoCodes;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PROMO_CRITERIA_POS"
	 * @hibernate.collection-element column="COUNTRY_CODE" type="string"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * 
	 * @see #applicablePosCountries
	 */
	public Set<String> getApplicablePosCountries() {
		return applicablePosCountries;
	}

	public void setApplicablePosCountries(Set<String> applicablePosCountries) {
		this.applicablePosCountries = applicablePosCountries;
	}
	
	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.RegistrationPeriod"
	 * 
	 * @return registrationPeriods
	 */
	public Set<RegistrationPeriod> getRegistrationPeriods() {
		return registrationPeriods;
	}

	/**
	 * @param registrationPeriods
	 */
	public void setRegistrationPeriods(Set<RegistrationPeriod> registrationPeriods) {
		this.registrationPeriods = registrationPeriods;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PROMO_CRITERIA_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.FlightPeriod"
	 * 
	 * @return flightPeriods
	 */
	public Set<FlightPeriod> getFlightPeriods() {
		return flightPeriods;
	}

	/**
	 * @param flightPeriods
	 */
	public void setFlightPeriods(Set<FlightPeriod> flightPeriods) {
		this.flightPeriods = flightPeriods;
	}

}
