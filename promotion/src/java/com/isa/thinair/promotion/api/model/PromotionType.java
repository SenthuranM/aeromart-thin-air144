package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PROMOTION_TYPE" lazy="false"
 */
public class PromotionType extends Tracking {

	private Integer promotionId;
	private String promotionName;
	private String promotionCode;
	private String status;

	/**
	 * @hibernate.id column = "PROMOTION_TYPE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PROMOTION_TYPE"
	 */
	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	/**
	 * @hibernate.property column = "PROMOTION_NAME"
	 */
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	/**
	 * @hibernate.property column = "PROMOTION_CODE"
	 */
	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
