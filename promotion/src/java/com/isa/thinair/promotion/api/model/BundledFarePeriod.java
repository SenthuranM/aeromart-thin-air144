package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * 
 * @hibernate.class table = "T_BUNDLED_FARE_PERIOD"
 */
public class BundledFarePeriod extends Persistent {

	private static final long serialVersionUID = 1847775660951052832L;

	private Integer bundleFarePeriodId;

	private Date flightDateFrom;

	private Date flightDateTo;

	private BigDecimal bundledFee;
	
	private String translationNameKey;
	
	private String translationDecriptionKey;

	private Set<BundledFareFreeService> bundledFareServices;

	private Integer descriptionTemplateId;
	
	private Set<Integer> periodImageIds;

	/**
	 * @hibernate.id column = "BUNDLED_FARE_PERIOD_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLED_FARE_PERIOD"
	 */
	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	/**
	 * @hibernate.property column = "FLIGHT_DATE_FROM"
	 */
	public Date getFlightDateFrom() {
		return flightDateFrom;
	}

	public void setFlightDateFrom(Date flightDateFrom) {
		this.flightDateFrom = flightDateFrom;
	}

	/**
	 * @hibernate.property column = "FLIGHT_DATE_TO"
	 */
	public Date getFlightDateTo() {
		return flightDateTo;
	}

	public void setFlightDateTo(Date flightDateTo) {
		this.flightDateTo = flightDateTo;
	}

	/**
	 * @hibernate.property column = "BUNDLE_FEE"
	 */
	public BigDecimal getBundledFee() {
		return bundledFee;
	}

	public void setBundledFee(BigDecimal bundledFee) {
		this.bundledFee = bundledFee;
	}

	/**
	 * @hibernate.set lazy="false" table="T_BUNDLED_FARE_FREE_SERVICE" cascade="all-delete-orphan" inverse="false";
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.BundledFareFreeService"
	 * @hibernate.collection-key column="BUNDLED_FARE_PERIOD_ID"
	 */
	public Set<BundledFareFreeService> getBundledFareServices() {
		return bundledFareServices;
	}

	public void setBundledFareServices(Set<BundledFareFreeService> bundledFareServices) {
		this.bundledFareServices = bundledFareServices;
	}
	
	
	/**
	 * @hibernate.property column = "BUNDLE_NAME_I18N_KEY"
	 */
	public String getTranslationNameKey() {
		return translationNameKey;
	}

	public void setTranslationNameKey(String translationNameKey) {
		this.translationNameKey = translationNameKey;
	}
	
	/**
	 * @hibernate.property column = "I18N_MESSAGE_KEY"
	 */
	public String getTranslationDecriptionKey() {
		return translationDecriptionKey;
	}

	public void setTranslationDecriptionKey(String translationDecriptionKey) {
		this.translationDecriptionKey = translationDecriptionKey;
	}
	
	
	/**
	 * Description Template object should be loaded on demand
	 * @hibernate.property column = "BUNDLE_DESC_TEMPLATE_ID"
	 */
	public Integer getDescriptionTemplateId() {
		return descriptionTemplateId;
	}

	public void setDescriptionTemplateId(Integer descriptionTemplateId) {
		this.descriptionTemplateId = descriptionTemplateId;
	}
	

	/**
	 * @hibernate.set lazy="false" cascade="all" table="T_BUNDLED_FARE_PERIOD_IMAGE"
	 * @hibernate.collection-element column="BUNDLED_FARE_PERIOD_IMAGE_ID" type="integer"
	 * @hibernate.collection-key column="BUNDLED_FARE_PERIOD_ID"
	 */
	public Set<Integer> getPeriodImageIds() {
		return periodImageIds;
	}

	public void setPeriodImageIds(Set<Integer> periodImageIds) {
		this.periodImageIds = periodImageIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bundleFarePeriodId == null) ? 0 : bundleFarePeriodId.hashCode());
		result = prime * result + ((bundledFareServices == null) ? 0 : bundledFareServices.hashCode());
		result = prime * result + ((bundledFee == null) ? 0 : bundledFee.hashCode());
		result = prime * result + ((flightDateFrom == null) ? 0 : flightDateFrom.hashCode());
		result = prime * result + ((flightDateTo == null) ? 0 : flightDateTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BundledFarePeriod other = (BundledFarePeriod) obj;
		if (bundleFarePeriodId == null) {
			if (other.bundleFarePeriodId != null)
				return false;
		} else if (!bundleFarePeriodId.equals(other.bundleFarePeriodId))
			return false;
		if (bundledFareServices == null) {
			if (other.bundledFareServices != null)
				return false;
		} else if (!bundledFareServices.equals(other.bundledFareServices))
			return false;
		if (bundledFee == null) {
			if (other.bundledFee != null)
				return false;
		} else if (!bundledFee.equals(other.bundledFee))
			return false;
		if (flightDateFrom == null) {
			if (other.flightDateFrom != null)
				return false;
		} else if (!flightDateFrom.equals(other.flightDateFrom))
			return false;
		if (flightDateTo == null) {
			if (other.flightDateTo != null)
				return false;
		} else if (!flightDateTo.equals(other.flightDateTo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BundledFarePeriod [bundleFarePeriodId=" + bundleFarePeriodId + ", flightDateFrom=" + flightDateFrom
				+ ", flightDateTo=" + flightDateTo + ", bundledFee=" + bundledFee + ", bundledFareServices="
				+ bundledFareServices + "]";
	}

}
