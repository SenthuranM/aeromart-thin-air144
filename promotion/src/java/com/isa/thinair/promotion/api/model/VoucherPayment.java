package com.isa.thinair.promotion.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chethiya
 * @hibernate.class table = "T_VOUCHER_PAYMENT"
 */
public class VoucherPayment implements Serializable{

	/** Holds the ID of the voucher payment */
	private Integer voucherPaymentID;

	/** Holds the ID of the voucher, corresponding to the payment */
	private String voucherID;

	/** Holds the nominal code of the payment */
	private Integer nominalCode;

	/** Holds the ID credit card temporary payment transaction ID */
	private Integer tempPaymentTnxID;

	/** Holds the Agent transaction ID */
	private Integer agentTnxID;

	/** Holds the Voucher Amount */
	private BigDecimal amount;

	/** Holds the Credit Card Transaction Fee */
	private BigDecimal ccTransactionFee;

	/**
	 * 
	 * @hibernate.id column = "VOUCHER_PAYMENT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_VOUCHER_PAYMENT"
	 * 
	 * @return voucherPaymentID
	 */
	public Integer getVoucherPaymentID() {
		return voucherPaymentID;
	}

	/**
	 * @param voucherPaymentID
	 *            the voucherPaymentID to set
	 */
	public void setVoucherPaymentID(Integer voucherPaymentID) {
		this.voucherPaymentID = voucherPaymentID;
	}

	/**
	 * 
	 * @hibernate.property column = "VOUCHER_ID"
	 * 
	 * @return the voucherID
	 */
	public String getVoucherID() {
		return voucherID;
	}

	/**
	 * @param voucherID
	 *            the voucherID to set
	 */
	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	/**
	 * 
	 * @hibernate.property column = "NOMINAL_CODE"
	 * 
	 * @return the nominalCode
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * 
	 * @hibernate.property column = "TPT_ID"
	 * 
	 * @return the tempPaymentTnxID
	 */
	public Integer getTempPaymentTnxID() {
		return tempPaymentTnxID;
	}

	/**
	 * @param tempPaymentTnxID
	 *            the tempPaymentTnxID to set
	 */
	public void setTempPaymentTnxID(Integer tempPaymentTnxID) {
		this.tempPaymentTnxID = tempPaymentTnxID;
	}

	/**
	 * 
	 * @hibernate.property column = "TXN_ID"
	 * 
	 * @return the agentTnxID
	 */
	public Integer getAgentTnxID() {
		return agentTnxID;
	}

	/**
	 * @param agentTnxID
	 *            the agentTnxID to set
	 */
	public void setAgentTnxID(Integer agentTnxID) {
		this.agentTnxID = agentTnxID;
	}

	/**
	 * 
	 * @hibernate.property column = "AMOUNT"
	 * 
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @hibernate.property column = "CC_TRANSACTION_FEE"
	 * 
	 * @return the ccTransactionFee
	 */
	public BigDecimal getCcTransactionFee() {
		return ccTransactionFee;
	}

	/**
	 * @param ccTransactionFee
	 *            the ccTransactionFee to set
	 */
	public void setCcTransactionFee(BigDecimal ccTransactionFee) {
		this.ccTransactionFee = ccTransactionFee;
	}

}
