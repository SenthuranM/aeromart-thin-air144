package com.isa.thinair.promotion.api.to.lms;

import java.io.Serializable;
import java.util.Date;

public class FlownRecordsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String memberAccountId;
	private String title;
	private String lastName;
	private String firstName;
	private String paxType;
	private Date dob;
	private String nationality;
	private String operaringFlight;
	private Date flightDateTime;
	private String departureAirport;
	private String arrivalAirport;
	private String cabinClass;
	private String bookingClass;
	private String pnr;
	private Date ticketIssuanceDate;
	private String ticketNumber;
	private String couponNumber;
	private String agentInitials;
	private String contactNumber;
	private String contactEmail;
	private String countryOfResidence;
	private Integer salesChannel;
	private String passportNumber;
	private String productList;

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getOperaringFlight() {
		return operaringFlight;
	}

	public void setOperaringFlight(String operaringFlight) {
		this.operaringFlight = operaringFlight;
	}

	public Date getFlightDateTime() {
		return flightDateTime;
	}

	public void setFlightDateTime(Date flightDateTime) {
		this.flightDateTime = flightDateTime;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getTicketIssuanceDate() {
		return ticketIssuanceDate;
	}

	public void setTicketIssuanceDate(Date ticketIssuanceDate) {
		this.ticketIssuanceDate = ticketIssuanceDate;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getAgentInitials() {
		return agentInitials;
	}

	public void setAgentInitials(String agentInitials) {
		this.agentInitials = agentInitials;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public Integer getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(Integer salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getProductList() {
		return productList;
	}

	public void setProductList(String productList) {
		this.productList = productList;
	}
}
