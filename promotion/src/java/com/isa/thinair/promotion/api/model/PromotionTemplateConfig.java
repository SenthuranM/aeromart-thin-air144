package com.isa.thinair.promotion.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PROMOTION_CONFIG" lazy="false"
 */
public class PromotionTemplateConfig extends Tracking {

	private Integer promotionConfigId;
	private PromotionTemplate promotionTemplate;
	private String promoParam;
	private String paramValue;
	private String status;

	/**
	 * @hibernate.id column = "PROMOTION_CONFIG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PROMOTION_CONFIG"
	 */
	public Integer getPromotionConfigId() {
		return promotionConfigId;
	}

	public void setPromotionConfigId(Integer promotionConfigId) {
		this.promotionConfigId = promotionConfigId;
	}

	/**
	 * @return Returns the parent.
	 * @hibernate.many-to-one column="PROMOTION_ID" cascade="save-update" update="false"
	 *                        class="com.isa.thinair.promotion.api.model.PromotionTemplate"
	 */
	public PromotionTemplate getPromotionTemplate() {
		return promotionTemplate;
	}

	public void setPromotionTemplate(PromotionTemplate promotionTemplate) {
		this.promotionTemplate = promotionTemplate;
	}

	/**
	 * @hibernate.property column = "PROMOTION_CONF"
	 */
	public String getPromoParam() {
		return promoParam;
	}

	public void setPromoParam(String promoParam) {
		this.promoParam = promoParam;
	}

	/**
	 * @hibernate.property column = "CONF_VALUE"
	 */
	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getPromoParam()).toHashCode();
	}

	public boolean equals(Object obj) {
		PromotionTemplateConfig other;
		if (obj != null && obj instanceof PromotionTemplateConfig) {
			other = (PromotionTemplateConfig) obj;
			return (getPromoParam() != null && other.getPromoParam() != null && getPromoParam().equals(other.getPromoParam()));
		}
		return false;
	}
}
