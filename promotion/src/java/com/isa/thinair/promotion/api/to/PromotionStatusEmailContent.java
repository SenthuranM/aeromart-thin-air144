package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class PromotionStatusEmailContent implements Serializable {

	private String pnr;
	private String paxName;
	private String segmentCode;
	private String promoStatus;
	private String contactEmail;
	private Integer pnrPaxID;
	private String preferdLanguage;
	private Integer promotionRequestID;
	private List<String> seats;
	private Date departureDate;
	private Collection<String> journeyOnds;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getPromoStatus() {
		return promoStatus;
	}

	public void setPromoStatus(String promoStatus) {
		this.promoStatus = promoStatus;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public Integer getPnrPaxID() {
		return pnrPaxID;
	}

	public void setPnrPaxID(Integer pnrPaxID) {
		this.pnrPaxID = pnrPaxID;
	}

	public String getPreferdLanguage() {
		return preferdLanguage;
	}

	public void setPreferdLanguage(String preferdLanguage) {
		this.preferdLanguage = preferdLanguage;
	}

	public Integer getPromotionRequestID() {
		return promotionRequestID;
	}

	public void setPromotionRequestID(Integer promotionRequestID) {
		this.promotionRequestID = promotionRequestID;
	}

	public List<String> getSeats() {
		return seats;
	}

	public void setSeats(List<String> seats) {
		this.seats = seats;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Collection<String> getJourneyOnds() {
		return journeyOnds;
	}

	public void setJourneyOnds(Collection<String> journeyOnds) {
		this.journeyOnds = journeyOnds;
	}
}
