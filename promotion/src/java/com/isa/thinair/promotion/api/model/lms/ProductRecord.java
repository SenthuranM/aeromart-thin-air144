package com.isa.thinair.promotion.api.model.lms;

import com.isa.thinair.promotion.api.model.lms.BasicDataType.DATA_F;

/**
 * 
 * @author rumesh
 * 
 */
public class ProductRecord extends OrderedElementContainer {

	public void setProductName(String producName, DATA_F dataF) {
		addElement(producName, dataF);
	}

	public void setProductExtReference(String productExtReference, DATA_F dataF) {
		addElement(productExtReference, dataF);
	}

	public void setDepartureAirport(String departureAirport, DATA_F dataF) {
		addElement(departureAirport, dataF);
	}

	public void setArrivalAirport(String arrivalAirport, DATA_F dataF) {
		addElement(arrivalAirport, dataF);
	}
}
