package com.isa.thinair.promotion.api.utils;

public enum PromotionType {
	PROMOTION_NEXT_SEAT_FREE(1, "NSF"), PROMOTION_FLEXI_DATE(2, "FD");

	private int promotionId;
	private String promotionCode;

	private PromotionType(int promotionId, String promotionCode) {
		this.promotionId = promotionId;
		this.promotionCode = promotionCode;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public static PromotionType resolvePromotionType(int promotionId) {
		for (PromotionType promotionType : PromotionType.values()) {
			if (promotionType.getPromotionId() == promotionId) {
				return promotionType;
			}
		}
		return null;
	}
}
