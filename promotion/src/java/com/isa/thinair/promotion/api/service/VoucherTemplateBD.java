package com.isa.thinair.promotion.api.service;

import java.util.ArrayList;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateRequest;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;

/**
 * @author chanaka
 *
 */
public interface VoucherTemplateBD {

	public static final String SERVICE_NAME = "VoucherTemplate";

	public ServiceResponce createVoucherTemplate(VoucherTemplateTo voucherTemplateTo) throws ModuleException;

	public Page<VoucherTemplateTo> searchVoucherTemplates(VoucherTemplateRequest voucherTemplateRequest, Integer start,
			Integer size) throws ModuleException;

	public ArrayList<VoucherTemplateTo> getVoucherInSalesPeriod() throws ModuleException;

	public ArrayList<VoucherTemplateTo> getVouchers() throws ModuleException;

}
