package com.isa.thinair.promotion.api.model;

/**
 * Qualifying passenger quantity of an anci offer criteria.
 * 
 * While this is better implemented as an element collection, hibernate issues when joining element collections forced
 * this implementation.
 * 
 * @author thihara
 * 
 * @hibernate.class table = "T_ANCI_OFFER_PAX_QUANTITY"
 */
public class PaxQuantity implements java.io.Serializable {

	private static final long serialVersionUID = 123424L;

	public static final String DISPLAY_SEPERATOR = ",";

	private Integer id;
	private Integer adults;
	private Integer children;
	private Integer infants;

	/**
	 * @hibernate.id column = "ANCI_OFFER_PAX_QUANTITY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ANCI_OFFER_PAX_QUANTITY"
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ADULT_COUNT"
	 */
	public Integer getAdults() {
		return adults;
	}

	public void setAdults(Integer adults) {
		this.adults = adults;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CHILDREN_COUNT"
	 */
	public Integer getChildren() {
		return children;
	}

	public void setChildren(Integer children) {
		this.children = children;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "INFANT_COUNT"
	 */
	public Integer getInfants() {
		return infants;
	}

	public void setInfants(Integer infants) {
		this.infants = infants;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adults == null) ? 0 : adults.hashCode());
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((infants == null) ? 0 : infants.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PaxQuantity)) {
			return false;
		}
		PaxQuantity other = (PaxQuantity) obj;
		if (adults == null) {
			if (other.adults != null) {
				return false;
			}
		} else if (!adults.equals(other.adults)) {
			return false;
		}
		if (children == null) {
			if (other.children != null) {
				return false;
			}
		} else if (!children.equals(other.children)) {
			return false;
		}
		if (infants == null) {
			if (other.infants != null) {
				return false;
			}
		} else if (!infants.equals(other.infants)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AD : " + adults + " CH : " + children + " IN : " + infants;

	}
}
