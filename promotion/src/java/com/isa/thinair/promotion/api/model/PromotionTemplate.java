package com.isa.thinair.promotion.api.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PROMOTION" lazy="false"
 */
public class PromotionTemplate extends Tracking {

	private Integer promotionId;
	private PromotionType promotionType;
	private Date startDate;
	private Date endDate;
	private String status;
	private Set<PromotionTemplateConfig> promotionTemplateConfigs;
	private Set<OndPromotion> onds;

	/**
	 * @hibernate.id column = "PROMOTION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PROMOTION"
	 */
	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	/**
	 * @hibernate.many-to-one column="PROMOTION_TYPE_ID" cascade="all"
	 *                        class="com.isa.thinair.promotion.api.model.PromotionType"
	 */
	public PromotionType getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}

	/**
	 * @hibernate.property column = "START_DATE"
	 */
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @hibernate.property column = "END_DATE"
	 */
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="PROMOTION_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.PromotionTemplateConfig"
	 */
	public Set<PromotionTemplateConfig> getPromotionTemplateConfigs() {
		return promotionTemplateConfigs;
	}

	public void setPromotionTemplateConfigs(Set<PromotionTemplateConfig> promotionTemplateConfigs) {
		this.promotionTemplateConfigs = promotionTemplateConfigs;
	}

	public void clearPromotionTemplateConfigs() {
		List<PromotionTemplateConfig> clone = new ArrayList<PromotionTemplateConfig>();
		for (PromotionTemplateConfig templateConfig : promotionTemplateConfigs) {
			templateConfig.setPromotionTemplate(null);
			clone.add(templateConfig);
		}
		for (PromotionTemplateConfig config : clone) {
			promotionTemplateConfigs.remove(config);
		}
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="PROMOTION_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.OndPromotion"
	 */
	public Set<OndPromotion> getOnds() {
		return onds;
	}

	public void setOnds(Set<OndPromotion> onds) {
		this.onds = onds;
	}

	public void clearOndPromotions() {
		List<OndPromotion> clone = new ArrayList<OndPromotion>();
		for (OndPromotion ondPromotion : onds) {
			ondPromotion.setPromotionTemplate(null);
			clone.add(ondPromotion);
		}
		for (OndPromotion ond : clone) {
			onds.remove(ond);
		}
	}

}
