package com.isa.thinair.promotion.api.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.utils.PromotionType;

public interface PromotionAdministrationBD {
	public static final String SERVICE_NAME = "PromotionAdministration";

	public void notifyPromotionOffer(PromotionType promotionType) throws ModuleException;

	public List<Object> getPromotionInquiryView(PromotionType promotionFlexiDate, Integer flightId) throws ModuleException;

	public void approveRejectPromotionRequests(PromotionType promotionFlexiDate, List<HashMap<String, Object>> approvalData) throws ModuleException;

	public void cleanUpPromotionRequests(PromotionType promotionType) throws ModuleException;

	public Page<Object> getAuditView(String flightNo, String status, Date startDate, Date endDate, Integer startRec, Integer size)
			throws ModuleException;

	public ServiceResponce approveFlexiDatePromotionRequest(HashMap<String, Object> params) throws ModuleException;

	public void approveNextSeatFreePromotionRequest(PromotionRequest promoRequest, Reservation reservation,
			FlightSegement flightSegment, Map<String, List<FlightSeatStatusTO>> selectedSeatMap, UserPrincipal userPrincipal) throws ModuleException;

	public BigDecimal processNextSeatChargeAndRefund(PromotionRequestApproveTo promoReq, BigDecimal refund,
			List<String> segCodes,
			Reservation reservation) throws ModuleException;

}
