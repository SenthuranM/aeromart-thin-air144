package com.isa.thinair.promotion.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PROMOTION_REQUEST" lazy="false"
 */
public class PromotionRequest extends Tracking {

	private Integer promotionRequestId;
	private PromotionTemplate promotionTemplate;
	private String status;
	private Integer reservationSegmentId;
	private Integer reservationPaxId;
	private Set<PromotionRequestConfig> promotionRequestConfigs;
	private String pnr;

	/**
	 * @hibernate.id column = "PROMOTION_REQUEST_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PROMOTION_REQUEST"
	 */
	public Integer getPromotionRequestId() {
		return promotionRequestId;
	}

	public void setPromotionRequestId(Integer promotionRequestId) {
		this.promotionRequestId = promotionRequestId;
	}

	/**
	 * @hibernate.many-to-one column="PROMOTION_ID" class="com.isa.thinair.promotion.api.model.PromotionTemplate"
	 */
	public PromotionTemplate getPromotionTemplate() {
		return promotionTemplate;
	}

	public void setPromotionTemplate(PromotionTemplate promotionTemplate) {
		this.promotionTemplate = promotionTemplate;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getReservationSegmentId() {
		return reservationSegmentId;
	}

	public void setReservationSegmentId(Integer reservationSegmentId) {
		this.reservationSegmentId = reservationSegmentId;
	}

	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getReservationPaxId() {
		return reservationPaxId;
	}

	public void setReservationPaxId(Integer reservationPaxId) {
		this.reservationPaxId = reservationPaxId;
	}

	/**
	 * @hibernate.set cascade="all" inverse="true" lazy="false"
	 * @hibernate.collection-key column="PROMOTION_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.PromotionRequestConfig"
	 */
	public Set<PromotionRequestConfig> getPromotionRequestConfigs() {
		return promotionRequestConfigs;
	}

	public void setPromotionRequestConfigs(Set<PromotionRequestConfig> promotionRequestConfigs) {
		this.promotionRequestConfigs = promotionRequestConfigs;
	}

	/**
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
