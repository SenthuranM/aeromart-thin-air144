package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Persistent;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;
import java.util.Set;

/**
 * @hibernate.class table = "T_BUNDLE_DESC_TEMPLATE"
 */
public class BundleFareDescriptionTemplate extends Persistent {

	private static final long serialVersionUID = 146546457658659747L;

	private Integer templateID;
	private String templateName;
	private String defaultFreeServicesTemplate;
	private String defaultPaidServicesTemplate;
	private Date createdDate;

	private Set<BundleFareDescriptionTranslationTemplate> translationTemplates;

	/**
	 * @hibernate.id column = "BUNDLE_DESC_TEMPLATE_ID"  generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLE_DESC_TEMPLATE"
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @hibernate.property column = "BUNDLE_DESC_TEMPLATE_NAME"
	 */
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @hibernate.property column = "DEFAULT_FREE_SERVICES_TEMPLATE"
	 */
	public String getDefaultFreeServicesTemplate() {
		return defaultFreeServicesTemplate;
	}

	public void setDefaultFreeServicesTemplate(String defaultFreeServicesTemplate) {
		this.defaultFreeServicesTemplate = defaultFreeServicesTemplate;
	}

	/**
	 * @hibernate.property column = "DEFAULT_PAID_SERVICES_TEMPLATE"
	 */
	public String getDefaultPaidServicesTemplate() {
		return defaultPaidServicesTemplate;
	}

	public void setDefaultPaidServicesTemplate(String defaultPaidServicesTemplate) {
		this.defaultPaidServicesTemplate = defaultPaidServicesTemplate;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="BUNDLE_DESC_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.BundleFareDescriptionTranslationTemplate"
	 */
	public Set<BundleFareDescriptionTranslationTemplate> getTranslationTemplates() {
		return translationTemplates;
	}

	public void setTranslationTemplates(Set<BundleFareDescriptionTranslationTemplate> translationTemplates) {
		this.translationTemplates = translationTemplates;
	}

	/**
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (object == null || !(object instanceof BundleFareDescriptionTemplate)) {
			return false;
		}

		BundleFareDescriptionTemplate template = (BundleFareDescriptionTemplate) object;

		return new EqualsBuilder().appendSuper(super.equals(object)).append(templateID, template.templateID).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(templateID).toHashCode();
	}
}
