package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;

public class BundledFareDisplayDTO implements Serializable {

	private static final long serialVersionUID = 5104397967027959256L;

	private Integer bundleFarePeriodId;
	private Integer displayTemplateId;

	private BundleFareDescriptionTemplateDTO descriptionTemplateDTO;
	private Map<String, String> thumbnails;
	private Map<String, Map<String, String>> translations;

	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	public Integer getDisplayTemplateId() {
		return displayTemplateId;
	}

	public void setDisplayTemplateId(Integer displayTemplateId) {
		this.displayTemplateId = displayTemplateId;
	}

	public Map<String, String> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(Map<String, String> thumbnails) {
		this.thumbnails = thumbnails;
	}

	public Map<String, Map<String, String>> getTranslations() {
		return translations;
	}

	public void setTranslations(Map<String, Map<String, String>> translations) {
		this.translations = translations;
	}

	public BundleFareDescriptionTemplateDTO getDescriptionTemplateDTO() {
		return descriptionTemplateDTO;
	}

	public void setDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO descriptionTemplateDTO) {
		this.descriptionTemplateDTO = descriptionTemplateDTO;
	}

}
