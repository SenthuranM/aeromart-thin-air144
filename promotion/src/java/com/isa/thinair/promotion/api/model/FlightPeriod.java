package com.isa.thinair.promotion.api.model;

/**
 * @author suventhan
 * 
 * @hibernate.class table = "T_PROMO_CRITERIA_FLIGHT_PERIOD"
 *
 */
public class FlightPeriod extends DatePeriod {

	private static final long serialVersionUID = 7400599889793796267L;
	
	private Long flightPeriodId;

	/**
	 * @hibernate.id column = "PROMO_CRITERIA_FLT_PERIOD_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PROMO_CRITERIA_FLIGHT_PERIOD"
	 * @return
	 */
	public Long getFlightPeriodId() {
		return flightPeriodId;
	}

	/**
	 * @param flightPeriodId
	 */
	public void setFlightPeriodId(Long flightPeriodId) {
		this.flightPeriodId = flightPeriodId;
	}
}
