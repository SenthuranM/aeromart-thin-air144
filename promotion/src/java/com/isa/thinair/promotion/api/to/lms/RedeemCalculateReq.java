package com.isa.thinair.promotion.api.to.lms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Request DTO for calculating product wise redeemable amounts
 * 
 * @author rumesh
 * 
 */
public class RedeemCalculateReq implements Serializable {

	private static final long serialVersionUID = -7834439051655840401L;

	private BigDecimal requestedLoyaltyPointsAmount;

	private Double memberAvailablePoints;

	private Collection<OndFareDTO> ondFareDTOs = null;

	private Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = null;

	private Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount;

	private BigDecimal loyaltyPointsToCurrencyConversionRate;

	private BigDecimal currencyToLoyaltyPointsConversionRate;

	private CurrencyExchangeRate currencyExRate;

	private String memberAccountId;

	private String txnIdntifier;

	private Map<String, List<String>> carrierWiseFlightRPH;

	private boolean issueRewardIds = false;

	private boolean isPayForOHD = false;

	private boolean isFlexiQuote = false;

	private String pnr;
	
	private DiscountRQ discountRQ;

	private Double remainingPoint;
	
	private String memberEnrollingCarrier;

	private String memberExternalId;

	public BigDecimal getRequestedLoyaltyPointsAmount() {
		return requestedLoyaltyPointsAmount;
	}

	public void setRequestedLoyaltyPointsAmount(BigDecimal requestedLoyaltyPointsAmount) {
		this.requestedLoyaltyPointsAmount = requestedLoyaltyPointsAmount;
	}

	public Collection<OndFareDTO> getOndFareDTOs() {
		return ondFareDTOs;
	}

	public void setOndFareDTOs(Collection<OndFareDTO> ondFareDTOs) {
		this.ondFareDTOs = ondFareDTOs;
	}

	public Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> getPaxCarrierExternalCharges() {
		return paxCarrierExternalCharges;
	}

	public Map<String, List<LCCClientExternalChgDTO>> getPaxExternalChargesForCarrier() {
		if (paxCarrierExternalCharges != null && paxCarrierExternalCharges.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			Map<String, List<LCCClientExternalChgDTO>> paxExternalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();
			for (String paxKey : paxCarrierExternalCharges.keySet()) {
				paxExternalCharges.put(paxKey, paxCarrierExternalCharges.get(paxKey).get(carrierCode));
			}

			return paxExternalCharges;
		}
		return new HashMap<String, List<LCCClientExternalChgDTO>>();
	}

	public void
			setPaxCarrierExternalCharges(Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges) {
		this.paxCarrierExternalCharges = paxCarrierExternalCharges;
	}

	public Map<Integer, Map<String, Map<String, BigDecimal>>> getPaxCarrierProductDueAmount() {
		return paxCarrierProductDueAmount;
	}

	public void setPaxCarrierProductDueAmount(Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount) {
		this.paxCarrierProductDueAmount = paxCarrierProductDueAmount;
	}

	public Map<Integer, Map<String, BigDecimal>> getPaxProductDueAmountForCarrier() {
		if (paxCarrierProductDueAmount != null && paxCarrierProductDueAmount.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			Map<Integer, Map<String, BigDecimal>> paxProductDueAmount = new HashMap<Integer, Map<String, BigDecimal>>();
			for (Integer paxSeq : paxCarrierProductDueAmount.keySet()) {
				paxProductDueAmount.put(paxSeq, paxCarrierProductDueAmount.get(paxSeq).get(carrierCode));
			}
			return paxProductDueAmount;
		}
		return null;
	}

	public BigDecimal getLoyaltyPointsToCurrencyConversionRate() {
		return loyaltyPointsToCurrencyConversionRate;
	}

	public void setLoyaltyPointsToCurrencyConversionRate(BigDecimal loyaltyPointsToCurrencyConversionRate) {
		this.loyaltyPointsToCurrencyConversionRate = loyaltyPointsToCurrencyConversionRate;
	}

	public BigDecimal getCurrencyToLoyaltyPointsConversionRate() {
		return currencyToLoyaltyPointsConversionRate;
	}

	public void setCurrencyToLoyaltyPointsConversionRate(BigDecimal currencyToLoyaltyPointsConversionRate) {
		this.currencyToLoyaltyPointsConversionRate = currencyToLoyaltyPointsConversionRate;
	}

	public CurrencyExchangeRate getCurrencyExRate() {
		return currencyExRate;
	}

	public void setCurrencyExRate(CurrencyExchangeRate currencyExRate) {
		this.currencyExRate = currencyExRate;
	}

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public String getTxnIdntifier() {
		return txnIdntifier;
	}

	public void setTxnIdntifier(String txnIdntifier) {
		this.txnIdntifier = txnIdntifier;
	}

	public Double getMemberAvailablePoints() {
		return memberAvailablePoints;
	}

	public void setMemberAvailablePoints(Double memberAvailablePoints) {
		this.memberAvailablePoints = memberAvailablePoints;
	}

	public Map<String, List<String>> getCarrierWiseFlightRPH() {
		return carrierWiseFlightRPH;
	}

	public void setCarrierWiseFlightRPH(Map<String, List<String>> carrierWiseFlightRPH) {
		this.carrierWiseFlightRPH = carrierWiseFlightRPH;
	}

	public boolean isIssueRewardIds() {
		return issueRewardIds;
	}

	public void setIssueRewardIds(boolean issueRewardIds) {
		this.issueRewardIds = issueRewardIds;
	}

	public boolean isPayForOHD() {
		return isPayForOHD;
	}

	public void setPayForOHD(boolean isPayForOHD) {
		this.isPayForOHD = isPayForOHD;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public DiscountRQ getDiscountRQ() {
		return discountRQ;
	}

	public void setDiscountRQ(DiscountRQ discountRQ) {
		this.discountRQ = discountRQ;
	}

	public boolean isFlexiQuote() {
		return isFlexiQuote;
	}

	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	public Double getRemainingPoint() {
		return remainingPoint;
	}

	public void setRemainingPoint(Double remainingPoint) {
		this.remainingPoint = remainingPoint;
	}

	public String getMemberEnrollingCarrier() {
		return memberEnrollingCarrier;
	}

	public void setMemberEnrollingCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier = memberEnrollingCarrier;
	}

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}
}
