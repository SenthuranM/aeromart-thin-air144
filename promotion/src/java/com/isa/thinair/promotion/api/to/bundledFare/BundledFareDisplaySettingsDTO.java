package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;
import java.util.Map;

public class BundledFareDisplaySettingsDTO implements Serializable {

	private static final long serialVersionUID = -4966698633995703086L;

	private Integer bundleFarePeriodId;
	private Integer displayTemplateId;

	private Map<String, String> nameTranslations;
	private Map<String, String> descTranslations;
	private Map<String, String> thumbnails;

	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	public Integer getDisplayTemplateId() {
		return displayTemplateId;
	}

	public void setDisplayTemplateId(Integer displayTemplateId) {
		this.displayTemplateId = displayTemplateId;
	}

	public Map<String, String> getNameTranslations() {
		return nameTranslations;
	}

	public void setNameTranslations(Map<String, String> nameTranslations) {
		this.nameTranslations = nameTranslations;
	}

	public Map<String, String> getDescTranslations() {
		return descTranslations;
	}

	public void setDescTranslations(Map<String, String> descTranslations) {
		this.descTranslations = descTranslations;
	}

	public Map<String, String> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(Map<String, String> thumbnails) {
		this.thumbnails = thumbnails;
	}

}
