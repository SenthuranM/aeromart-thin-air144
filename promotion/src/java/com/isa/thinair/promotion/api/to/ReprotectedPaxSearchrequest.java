package com.isa.thinair.promotion.api.to;

import java.io.Serializable;

/**
 * @author chanaka
 *
 */
public class ReprotectedPaxSearchrequest implements Serializable {
	
	private static final long serialVersionUID = -8945978719259572769L;
	
	String flightNumber;
	
	String date;
	
	String sqlDate; 

	/**
	 * @return the sqlDate
	 */
	public String getSqlDate() {
		return sqlDate;
	}

	/**
	 * @param sqlDate the sqlDate to set
	 */
	public void setSqlDate(String sqlDate) {
		this.sqlDate = sqlDate;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightnumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
