package com.isa.thinair.promotion.api.to.constants;

public interface PromoRequestParam {
	interface PromoInternal {
		String PAYMENT_TNX_ID = "PAYMENT_TNX_ID";
	}

	interface FreeSeat {
		String SEATS_TO_FREE = "SEATS_TO_FREE";
		String MOVE_SEAT = "MOVE_SEAT";
		String ORIGINAL_SEAT_CODE = "ORIG_SEAT_CODE";
		String REFUND_TNX_ID = "REFUND_TNX_ID";
	}

	interface FlexiDate {
		String FLEXI_DATE_LOWER_BOUND = "FLEXI_LOWER";
		String FLEXI_DATE_UPPER_BOUND = "FLEXI_UPPER";
	}
}