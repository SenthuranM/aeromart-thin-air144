package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author rumesh
 * 
 */
public class ApplicablePromotionDetailsTO implements Serializable {
	private static final long serialVersionUID = -3082275941360383166L;

	/*
	 * keep promotion details
	 */
	private Long promoCriteriaId;
	private String promoName;
	private String description;
	private String promoType;
	private String promoCode;
	private boolean systemGenerated;

	/*
	 * keep discount related information
	 */
	private String discountType;
	private float discountValue;
	private int applyTo;
	private String discountAs;

	/*
	 * keep adult, child infant count to apply selected discount.
	 */
	private Integer applicableAdultCount;
	private Integer applicableChildCount;
	private Integer applicableInfantCount;

	/*
	 * keep ancillaries in which the promotion is applicable, this is for FREESERVICE only
	 */
	private List<String> applicableAncillaries;

	private boolean applicableForOneWay;
	private boolean applicableForReturn;
	private String discountApplicability;
	private boolean allowSamePaxOnly;
	private boolean allowSameSectorOnly;
	private String originPnr;

	private Set<String> eligibleOnds;
	private Set<String> applicableOnds;
	private Set<String> applicableFlightNumbers;
	private Set<String> applicableLogicalCCs;
	private Set<String> applicableBCs;
	private Set<Integer> applicableBINs;

	public Long getPromoCriteriaId() {
		return promoCriteriaId;
	}

	public void setPromoCriteriaId(Long promoCriteriaId) {
		this.promoCriteriaId = promoCriteriaId;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPromoType() {
		return promoType;
	}

	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public boolean isSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public float getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(float discountValue) {
		this.discountValue = discountValue;
	}

	public int getApplyTo() {
		return applyTo;
	}

	public void setApplyTo(int applyTo) {
		this.applyTo = applyTo;
	}

	public String getDiscountAs() {
		return discountAs;
	}

	public void setDiscountAs(String discountAs) {
		this.discountAs = discountAs;
	}

	public Integer getApplicableAdultCount() {
		return applicableAdultCount;
	}

	public void setApplicableAdultCount(Integer applicableAdultCount) {
		this.applicableAdultCount = applicableAdultCount;
	}

	public Integer getApplicableChildCount() {
		return applicableChildCount;
	}

	public void setApplicableChildCount(Integer applicableChildCount) {
		this.applicableChildCount = applicableChildCount;
	}

	public Integer getApplicableInfantCount() {
		return applicableInfantCount;
	}

	public void setApplicableInfantCount(Integer applicableInfantCount) {
		this.applicableInfantCount = applicableInfantCount;
	}

	public List<String> getApplicableAncillaries() {
		if (applicableAncillaries == null) {
			applicableAncillaries = new ArrayList<String>();
		}
		return applicableAncillaries;
	}

	public void setApplicableAncillaries(List<String> applicableAncillaries) {
		this.applicableAncillaries = applicableAncillaries;
	}

	public boolean isApplicableForOneWay() {
		return applicableForOneWay;
	}

	public void setApplicableForOneWay(boolean applicableForOneWay) {
		this.applicableForOneWay = applicableForOneWay;
	}

	public boolean isApplicableForReturn() {
		return applicableForReturn;
	}

	public void setApplicableForReturn(boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	public String getDiscountApplicability() {
		return discountApplicability;
	}

	public void setDiscountApplicability(String discountApplicability) {
		this.discountApplicability = discountApplicability;
	}

	public boolean isAllowSamePaxOnly() {
		return allowSamePaxOnly;
	}

	public void setAllowSamePaxOnly(boolean allowSamePaxOnly) {
		this.allowSamePaxOnly = allowSamePaxOnly;
	}

	public boolean isAllowSameSectorOnly() {
		return allowSameSectorOnly;
	}

	public void setAllowSameSectorOnly(boolean allowSameSectorOnly) {
		this.allowSameSectorOnly = allowSameSectorOnly;
	}

	public String getOriginPnr() {
		return originPnr;
	}

	public void setOriginPnr(String originPnr) {
		this.originPnr = originPnr;
	}

	public Set<String> getEligibleOnds() {
		if (eligibleOnds == null) {
			eligibleOnds = new HashSet<String>();
		}
		return eligibleOnds;
	}

	public void setEligibleOnds(Set<String> eligibleOnds) {
		this.eligibleOnds = eligibleOnds;
	}

	public Set<String> getApplicableOnds() {
		if (applicableOnds == null) {
			applicableOnds = new HashSet<String>();
		}
		return applicableOnds;
	}

	public void setApplicableOnds(Set<String> applicableOnds) {
		this.applicableOnds = applicableOnds;
	}

	public Set<String> getApplicableFlightNumbers() {
		if (applicableFlightNumbers == null) {
			applicableFlightNumbers = new HashSet<String>();
		}
		return applicableFlightNumbers;
	}

	public void setApplicableFlightNumbers(Set<String> applicableFlightNumbers) {
		this.applicableFlightNumbers = applicableFlightNumbers;
	}

	public Set<String> getApplicableLogicalCCs() {
		if (applicableLogicalCCs == null) {
			applicableLogicalCCs = new HashSet<String>();
		}
		return applicableLogicalCCs;
	}

	public void setApplicableLogicalCCs(Set<String> applicableLogicalCCs) {
		this.applicableLogicalCCs = applicableLogicalCCs;
	}

	public Set<String> getApplicableBCs() {
		if (applicableBCs == null) {
			applicableBCs = new HashSet<String>();
		}
		return applicableBCs;
	}

	public void setApplicableBCs(Set<String> applicableBCs) {
		this.applicableBCs = applicableBCs;
	}

	public Set<Integer> getApplicableBINs() {
		if (applicableBINs == null) {
			applicableBINs = new HashSet<Integer>();
		}
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}
}
