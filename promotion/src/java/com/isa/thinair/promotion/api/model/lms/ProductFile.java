package com.isa.thinair.promotion.api.model.lms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * 
 * @author rumesh
 * 
 */
public class ProductFile implements FileGeneratable {

	private List<ProductRecord> productRecords;

	public List<ProductRecord> getProductRecords() {
		return productRecords;
	}

	public void addProductRecords(ProductRecord productRecord) {
		if (this.productRecords == null) {
			this.productRecords = new ArrayList<ProductRecord>();
		}

		this.productRecords.add(productRecord);
	}

	@Override
	public String getFormattedData() {
		StringBuilder sb = new StringBuilder("");

		// Add header info
		int headerCounter = 1;
		for (HEADER headerValue : HEADER.values()) {
			sb.append(headerValue.toString());
			if (headerCounter == HEADER.values().length) {
				sb.append("\n");
			} else {
				sb.append(ProductRecord.SEPARATOR);
			}
			headerCounter++;
		}

		if (productRecords != null && !productRecords.isEmpty()) {
			for (ProductRecord productRecord : productRecords) {
				if (productRecord.isValidFormat()) {
					List<BasicDataType> productRecordData = productRecord.getElementList();
					int elemCounter = 1;
					for (BasicDataType basicDataType : productRecordData) {
						sb.append(basicDataType.getData());
						if (elemCounter == productRecordData.size()) {
							sb.append("\n");
						} else {
							sb.append(ProductRecord.SEPARATOR);
						}
						elemCounter++;
					}
				}

			}
		}
		return sb.toString();
	}

	@Override
	public String getFileName(Date processingDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		return AppSysParamsUtil.getDefaultCarrierCode() + "_" + externalConfig.getProductFilePrefix()
				+ sdf.format(processingDate) + externalConfig.getFileExtension();
	}

	private enum HEADER {
		NAME, NAT_UPC, DEPART_ID, ARRIVE_ID
	}
}
