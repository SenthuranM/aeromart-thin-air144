package com.isa.thinair.promotion.api.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;

/**
 * Interface for Loyalty Management System integration
 * 
 * @author rumesh
 * 
 */
public interface LoyaltyManagementBD {
	public static final String SERVICE_NAME = "LoyaltyManagement";

	public boolean isValidLoyaltyAccount(String memberAccountId) throws ModuleException;

	/**
	 * <p>
	 * Method will perform complete process of creating Loyalty Member by executing following operations
	 * </p>
	 * <p>
	 * -- Enroll with member email
	 * </p>
	 * <p>
	 * -- save password
	 * </p>
	 * <p>
	 * -- save phone numbers
	 * </p>
	 * <p>
	 * -- save custom fields
	 * </p>
	 * <p>
	 * -- save preferred communication language
	 * </p>
	 * 
	 * @param customer
	 * @return
	 * @throws ModuleException
	 */
	public String createLoyaltyMember(Customer customer) throws ModuleException;
	
	public void updateMemberProfile(Customer customer) throws ModuleException;

	public LoyaltyMemberCoreDTO getLoyaltyMemberCoreDetails(String memberAccountId) throws ModuleException;

	public LoyaltyPointDetailsDTO getLoyaltyMemberPointBalances(String memberAccountId, String memberExternalId) throws ModuleException;

	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException;
	
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException;

	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException;

	public String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException;

	public List<LoyaltyProduct> getLoyaltyProducts() throws ModuleException;

	public ServiceResponce calculatePaxLoyaltyRedeemableAmounts(RedeemCalculateReq redeemCalculateReqTo) throws ModuleException;

	public ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException;

	public ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException;

	public ServiceResponce cancelRedeemedLoyaltyPoints(String pnr,String[] rewardIds, String memberAccountId) throws ModuleException;

	public void generateFlownFile(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly)
			throws ModuleException;

	public void generateProductFile() throws ModuleException;

	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException;

	public Map<String, String> getLoyaltyLocationExtReferences();
	
	public Map<String, String> getLoyaltyEnrollingChannelExtReferences();

	public LMSMemberDTO getMergeLoyaltyMember(String memberFFID, TrackInfoDTO trackInfo) throws ModuleException;
		
	public int customerNameUpdate(String memberFFID, TrackInfoDTO trackInfo, String title, String firstName, String lastName) throws ModuleException;

	public MemberRemoteLoginResponseDTO memberLogin(String userName, String password) throws ModuleException;
}
