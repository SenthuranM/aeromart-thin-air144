package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PromotionCriteriaTO implements Serializable {

	private static final long serialVersionUID = 23514212345887L;

	private Long promoCriteriaID;

	private Set<String> applicableONDs;

	private Set<String> flightNOs;
	
	private Set<String> applicableCOSs;

	private Set<String> applicableLCCs;

	private Set<String> applicableBCs;

	private Set<String> applicableSalesChannels;

	private Set<String> applicableAgents;

	private Boolean promoCodeEnabled = Boolean.FALSE;

	private String promoCode;

	private Integer perFlightLimit;

	private Integer totalLimit;

	private Integer appliedAmount;

	private Boolean restrictAppliedResModification = Boolean.FALSE;

	private Boolean restrictAppliedResCancelation = Boolean.FALSE;

	private Boolean restrictAppliedResSplitting = Boolean.FALSE;

	private Boolean restrictAppliedResRemovePax = Boolean.FALSE;

	private String status = "ACT";

	private String promotionCriteriaType;

	// Buy and Get

	private BigDecimal limitingLoadFactor;

	/**
	 * 1,1,1 -> 0,1,1 format
	 */
	private Map<String, String> freeCriteria;

	// Free Service

	private Set<String> applicableAncillaries;

	// Discount
	private Set<Integer> applicableBINs;

	// DISCOUNT DETAILS

	// Credit/
	private String applyAs;

	/**
	 * Fare / Fare+Surcharges / Total
	 */
	private String applyTO;

	/**
	 * Value/Percentage
	 */
	private String discountType;

	private BigDecimal discountValue;

	private BigDecimal usedCredits;

	private Long version;

	private String promoName;

	private Boolean applicableForOneway = Boolean.FALSE;

	private Boolean applicableForReturn = Boolean.FALSE;

	// applicable member selection
	private Boolean applicableForLoyaltyMembers = Boolean.FALSE;

	private Boolean applicableForRegisteredMembers = Boolean.FALSE;

	private Boolean applicableForGuests = Boolean.FALSE;

	private Integer numByAgent;

	private Integer numBySalesChannel;

	private String applicability;

	private Boolean constPaxName = Boolean.FALSE;

	private Boolean constPaxSector = Boolean.FALSE;

	private Date constFlightFrom;

	private Date constFlightTo;

	private Date constValidFrom;

	private Date constValidTo;

	private String pcDescForDisplay;

	private Boolean systemGenerated = Boolean.FALSE;

	private Boolean multiCodeGenerated = Boolean.FALSE;

	private List<String> enablePromoCodes;

	private List<String> disablePromoCodes;

	private PromoCodeTo promoCodeTo;

	private Set<String> posCountries;
	
	private Set<PromoCodeTo> promoCodesTo;

	private Set<String> registrationPeriods;

	private Set<String> flightPeriods;

	public Long getPromoCriteriaID() {
		return promoCriteriaID;
	}

	public void setPromoCriteriaID(Long promoCriteriaID) {
		this.promoCriteriaID = promoCriteriaID;
	}

	public Set<String> getApplicableONDs() {
		return applicableONDs;
	}

	public void setApplicableONDs(Set<String> applicableONDs) {
		this.applicableONDs = applicableONDs;
	}

	public Set<String> getFlightNOs() {
		return flightNOs;
	}

	public void setFlightNOs(Set<String> flightNOs) {
		this.flightNOs = flightNOs;
	}

	public Set<String> getApplicableCOSs() {
		return applicableCOSs;
	}

	public void setApplicableCOSs(Set<String> applicableCOSs) {
		this.applicableCOSs = applicableCOSs;
	}

	public Set<String> getApplicableLCCs() {
		return applicableLCCs;
	}

	public void setApplicableLCCs(Set<String> applicableLCCs) {
		this.applicableLCCs = applicableLCCs;
	}

	public Set<String> getApplicableBCs() {
		return applicableBCs;
	}

	public void setApplicableBCs(Set<String> applicableBCs) {
		this.applicableBCs = applicableBCs;
	}

	public Set<String> getApplicableSalesChannels() {
		return applicableSalesChannels;
	}

	public void setApplicableSalesChannels(Set<String> applicableSalesChannels) {
		this.applicableSalesChannels = applicableSalesChannels;
	}

	public Set<String> getApplicableAgents() {
		return applicableAgents;
	}

	public void setApplicableAgents(Set<String> applicableAgents) {
		this.applicableAgents = applicableAgents;
	}

	public Boolean getPromoCodeEnabled() {
		return promoCodeEnabled;
	}

	public void setPromoCodeEnabled(Boolean promoCodeEnabled) {
		this.promoCodeEnabled = promoCodeEnabled;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Integer getPerFlightLimit() {
		return perFlightLimit;
	}

	public void setPerFlightLimit(Integer perFlightLimit) {
		this.perFlightLimit = perFlightLimit;
	}

	public Integer getTotalLimit() {
		return totalLimit;
	}

	public void setTotalLimit(Integer totalLimit) {
		this.totalLimit = totalLimit;
	}

	public Integer getAppliedAmount() {
		return appliedAmount;
	}

	public void setAppliedAmount(Integer appliedAmount) {
		this.appliedAmount = appliedAmount;
	}

	public Boolean getRestrictAppliedResModification() {
		return restrictAppliedResModification;
	}

	public void setRestrictAppliedResModification(Boolean restrictAppliedResModification) {
		this.restrictAppliedResModification = restrictAppliedResModification;
	}

	public Boolean getRestrictAppliedResCancelation() {
		return restrictAppliedResCancelation;
	}

	public void setRestrictAppliedResCancelation(Boolean restrictAppliedResCancelation) {
		this.restrictAppliedResCancelation = restrictAppliedResCancelation;
	}

	public Boolean getRestrictAppliedResSplitting() {
		return restrictAppliedResSplitting;
	}

	public void setRestrictAppliedResSplitting(Boolean restrictAppliedResSplitting) {
		this.restrictAppliedResSplitting = restrictAppliedResSplitting;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPromotionCriteriaType() {
		return promotionCriteriaType;
	}

	public void setPromotionCriteriaType(String promotionCriteriaType) {
		this.promotionCriteriaType = promotionCriteriaType;
	}

	public BigDecimal getLimitingLoadFactor() {
		return limitingLoadFactor;
	}

	public void setLimitingLoadFactor(BigDecimal limitingLoadFactor) {
		this.limitingLoadFactor = limitingLoadFactor;
	}

	public Map<String, String> getFreeCriteria() {
		return freeCriteria;
	}

	public void setFreeCriteria(Map<String, String> freeCriteria) {
		this.freeCriteria = freeCriteria;
	}

	public Set<String> getApplicableAncillaries() {
		return applicableAncillaries;
	}

	public void setApplicableAncillaries(Set<String> applicableAncillaries) {
		this.applicableAncillaries = applicableAncillaries;
	}

	public Set<Integer> getApplicableBINs() {
		return applicableBINs;
	}

	public void setApplicableBINs(Set<Integer> applicableBINs) {
		this.applicableBINs = applicableBINs;
	}

	public String getApplyAs() {
		return applyAs;
	}

	public void setApplyAs(String applyAs) {
		this.applyAs = applyAs;
	}

	public String getApplyTO() {
		return applyTO;
	}

	public void setApplyTO(String applyTO) {
		this.applyTO = applyTO;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public BigDecimal getUsedCredits() {
		return usedCredits;
	}

	public void setUsedCredits(BigDecimal usedCredits) {
		this.usedCredits = usedCredits;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public Boolean getApplicableForOneway() {
		return applicableForOneway;
	}

	public void setApplicableForOneway(Boolean applicableForOneway) {
		this.applicableForOneway = applicableForOneway;
	}

	public Boolean getApplicableForReturn() {
		return applicableForReturn;
	}

	public void setApplicableForReturn(Boolean applicableForReturn) {
		this.applicableForReturn = applicableForReturn;
	}

	// applicable member selection
	public Boolean getApplicableForLoyaltyMembers() {
		return applicableForLoyaltyMembers;
	}

	public void setApplicableForLoyaltyMembers(Boolean applicableForLoyaltyMembers) {
		this.applicableForLoyaltyMembers = applicableForLoyaltyMembers;
	}

	public Boolean getApplicableForRegisteredMembers() {
		return applicableForRegisteredMembers;
	}

	public void setApplicableForRegisteredMembers(Boolean applicableForRegisteredMembers) {
		this.applicableForRegisteredMembers = applicableForRegisteredMembers;
	}

	public Boolean getApplicableForGuests() {
		return applicableForGuests;
	}

	public void setApplicableForGuests(Boolean applicableForGuests) {
		this.applicableForGuests = applicableForGuests;
	}

	public Integer getNumByAgent() {
		return numByAgent;
	}

	public void setNumByAgent(Integer numByAgent) {
		this.numByAgent = numByAgent;
	}

	public Integer getNumBySalesChannel() {
		return numBySalesChannel;
	}

	public void setNumBySalesChannel(Integer numBySalesChannel) {
		this.numBySalesChannel = numBySalesChannel;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public Boolean getConstPaxName() {
		return constPaxName;
	}

	public void setConstPaxName(Boolean constPaxName) {
		this.constPaxName = constPaxName;
	}

	public Date getConstFlightFrom() {
		return constFlightFrom;
	}

	public void setConstFlightFrom(Date constFlightFrom) {
		this.constFlightFrom = constFlightFrom;
	}

	public Date getConstFlightTo() {
		return constFlightTo;
	}

	public void setConstFlightTo(Date constFlightTo) {
		this.constFlightTo = constFlightTo;
	}

	public Boolean getConstPaxSector() {
		return constPaxSector;
	}

	public void setConstPaxSector(Boolean constPaxSector) {
		this.constPaxSector = constPaxSector;
	}

	public Date getConstValidFrom() {
		return constValidFrom;
	}

	public void setConstValidFrom(Date constValidFrom) {
		this.constValidFrom = constValidFrom;
	}

	public Date getConstValidTo() {
		return constValidTo;
	}

	public void setConstValidTo(Date constValidTo) {
		this.constValidTo = constValidTo;
	}

	public String getPcDescForDisplay() {
		return pcDescForDisplay;
	}

	public void setPcDescForDisplay(String pcDescForDisplay) {
		this.pcDescForDisplay = pcDescForDisplay;
	}

	public Boolean getSystemGenerated() {
		return systemGenerated;
	}

	public void setSystemGenerated(Boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}

	public Boolean getRestrictAppliedResRemovePax() {
		return restrictAppliedResRemovePax;
	}

	public void setRestrictAppliedResRemovePax(Boolean restrictAppliedResRemovePax) {
		this.restrictAppliedResRemovePax = restrictAppliedResRemovePax;
	}

	public PromotionCriteriaTO clone() {

		PromotionCriteriaTO promotionCriteriaTO = new PromotionCriteriaTO();
		promotionCriteriaTO.setApplicability(this.applicability);
		promotionCriteriaTO.setApplicableAgents(this.applicableAgents);
		promotionCriteriaTO.setApplicableAncillaries(this.applicableAgents);
		promotionCriteriaTO.setApplicableBCs(this.applicableBCs);
		promotionCriteriaTO.setApplicableBINs(this.applicableBINs);
		promotionCriteriaTO.setApplicableCOSs(this.applicableCOSs);
		promotionCriteriaTO.setApplicableForOneway(this.applicableForOneway);
		promotionCriteriaTO.setApplicableForReturn(this.applicableForReturn);

		// applicable member selection
		promotionCriteriaTO.setApplicableForLoyaltyMembers(this.applicableForLoyaltyMembers);
		promotionCriteriaTO.setApplicableForRegisteredMembers(this.applicableForRegisteredMembers);
		promotionCriteriaTO.setApplicableForGuests(this.applicableForGuests);

		promotionCriteriaTO.setApplicableLCCs(this.applicableLCCs);
		promotionCriteriaTO.setApplicableONDs(this.applicableONDs);
		promotionCriteriaTO.setApplicableSalesChannels(this.applicableSalesChannels);
		promotionCriteriaTO.setAppliedAmount(this.appliedAmount);
		promotionCriteriaTO.setApplyAs(this.applyAs);
		promotionCriteriaTO.setApplyTO(this.applyTO);
		promotionCriteriaTO.setConstFlightFrom(this.constFlightFrom);
		promotionCriteriaTO.setConstFlightTo(this.constFlightTo);
		promotionCriteriaTO.setConstPaxName(this.constPaxName);
		promotionCriteriaTO.setConstPaxSector(this.constPaxSector);
		promotionCriteriaTO.setConstValidFrom(this.constValidFrom);
		promotionCriteriaTO.setConstValidTo(this.constValidTo);
		promotionCriteriaTO.setDiscountType(this.discountType);
		promotionCriteriaTO.setFlightNOs(this.flightNOs);
		promotionCriteriaTO.setFreeCriteria(this.freeCriteria);
		promotionCriteriaTO.setLimitingLoadFactor(this.limitingLoadFactor);
		promotionCriteriaTO.setNumByAgent(this.numByAgent);
		promotionCriteriaTO.setNumBySalesChannel(this.numBySalesChannel);
		promotionCriteriaTO.setPcDescForDisplay(this.pcDescForDisplay);
		promotionCriteriaTO.setPerFlightLimit(this.perFlightLimit);
		promotionCriteriaTO.setPromoCode(this.promoCode);
		promotionCriteriaTO.setPromoCodeEnabled(this.promoCodeEnabled);
		promotionCriteriaTO.setPromoCriteriaID(this.promoCriteriaID);
		promotionCriteriaTO.setPromoName(this.promoName);
		promotionCriteriaTO.setPromotionCriteriaType(this.promotionCriteriaType);
		promotionCriteriaTO.setRestrictAppliedResCancelation(this.restrictAppliedResCancelation);
		promotionCriteriaTO.setRestrictAppliedResModification(this.restrictAppliedResModification);
		promotionCriteriaTO.setRestrictAppliedResRemovePax(this.restrictAppliedResRemovePax);
		promotionCriteriaTO.setRestrictAppliedResSplitting(this.restrictAppliedResSplitting);
		promotionCriteriaTO.setStatus(this.status);
		promotionCriteriaTO.setSystemGenerated(this.systemGenerated);
		promotionCriteriaTO.setTotalLimit(this.totalLimit);
		promotionCriteriaTO.setUsedCredits(this.usedCredits);
		promotionCriteriaTO.setVersion(this.version);
		promotionCriteriaTO.setPosCountries(this.posCountries);
		promotionCriteriaTO.setPromoCodesTo(promoCodesTo);
		promotionCriteriaTO.setRegistrationPeriods(this.registrationPeriods);
		promotionCriteriaTO.setFlightPeriods(this.flightPeriods);

		return promotionCriteriaTO;

	}

	public Boolean getMultiCodeGenerated() {
		return multiCodeGenerated;
	}

	public void setMultiCodeGenerated(Boolean multiCodeGenerated) {
		this.multiCodeGenerated = multiCodeGenerated;
	}

	public List<String> getEnablePromoCodes() {
		return enablePromoCodes;
	}

	public void setEnablePromoCodes(List<String> enablePromoCodes) {
		this.enablePromoCodes = enablePromoCodes;
	}

	public List<String> getDisablePromoCodes() {
		return disablePromoCodes;
	}

	public void setDisablePromoCodes(List<String> disablePromoCodes) {
		this.disablePromoCodes = disablePromoCodes;
	}

	public PromoCodeTo getPromoCodeTo() {
		return promoCodeTo;
	}

	public void setPromoCodeTo(PromoCodeTo promoCodeTo) {
		this.promoCodeTo = promoCodeTo;
	}

	public Set<String> getPosCountries() {
		return posCountries;
	}

	public void setPosCountries(Set<String> posCountries) {
		this.posCountries = posCountries;
	}

	public Set<PromoCodeTo> getPromoCodesTo() {
		return promoCodesTo;
	}

	public void setPromoCodesTo(Set<PromoCodeTo> promoCodesTo) {
		this.promoCodesTo = promoCodesTo;
	}

	public Set<String> getRegistrationPeriods() {
		return registrationPeriods;
	}

	public void setRegistrationPeriods(Set<String> registrationPeriods) {
		this.registrationPeriods = registrationPeriods;
	}

	public Set<String> getFlightPeriods() {
		return flightPeriods;
	}

	public void setFlightPeriods(Set<String> flightPeriods) {
		this.flightPeriods = flightPeriods;
	}

}