package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author rumesh
 * 
 */
public class BundledFareConfigurationTO implements Serializable {
	private static final long serialVersionUID = 7210659899122042086L;

	private Integer id;	

	private String bundledFareName;

	private Date flightDateFrom;

	private Date flightDateTo;

	private Set<String> bookingClasses;

	private Set<String> ondCodes;

	private Set<String> agents;

	private Set<String> flightNumbers;

	private String chargeCode;

	private BigDecimal bundleFee;

	private Boolean status = Boolean.TRUE;

	private Long version;
	
	private Set<BundledFarePeriodTO> bundledFarePeriods;

	private Map<String, String> descriptionTranslations;

	private Map<String, String> nameTranslations;

	private String i18nMessageKeyForDescription;

	private String i18nMessageKeyForName;

	private Set<String> applicableSalesChannels;
	
	private Integer cutOffTime;

	private Integer bundledCategoryID;

	private String defaultBundled;

	private Set<String> pointOfSale;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the bundledFareName
	 */
	public String getBundledFareName() {
		return bundledFareName;
	}

	/**
	 * @param bundledFareName
	 *            the bundledFareName to set
	 */
	public void setBundledFareName(String bundledFareName) {
		this.bundledFareName = bundledFareName;
	}

	/**
	 * @return the flightDateFrom
	 */
	public Date getFlightDateFrom() {
		return flightDateFrom;
	}

	/**
	 * @param flightDateFrom
	 *            the flightDateFrom to set
	 */
	public void setFlightDateFrom(Date flightDateFrom) {
		this.flightDateFrom = flightDateFrom;
	}

	/**
	 * @return the flightDateTo
	 */
	public Date getFlightDateTo() {
		return flightDateTo;
	}

	/**
	 * @param flightDateTo
	 *            the flightDateTo to set
	 */
	public void setFlightDateTo(Date flightDateTo) {
		this.flightDateTo = flightDateTo;
	}

	/**
	 * @return the bookingClasses
	 */
	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	/**
	 * @param bookingClasses
	 *            the bookingClasses to set
	 */
	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	/**
	 * @return the ondCodes
	 */
	public Set<String> getOndCodes() {
		return ondCodes;
	}

	/**
	 * @param ondCodes
	 *            the ondCodes to set
	 */
	public void setOndCodes(Set<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	/**
	 * @return the agents
	 */
	public Set<String> getAgents() {
		return agents;
	}

	/**
	 * @param agents
	 *            the agents to set
	 */
	public void setAgents(Set<String> agents) {
		this.agents = agents;
	}

	/**
	 * @return the flightNumbers
	 */
	public Set<String> getFlightNumbers() {
		return flightNumbers;
	}

	/**
	 * @param flightNumbers
	 *            the flightNumbers to set
	 */
	public void setFlightNumbers(Set<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	/**
	 * @return the chargeCode
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            the chargeCode to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return the bundleFee
	 */
	public BigDecimal getBundleFee() {
		return bundleFee;
	}

	/**
	 * @param bundleFee
	 *            the bundleFee to set
	 */
	public void setBundleFee(BigDecimal bundleFee) {
		this.bundleFee = bundleFee;
	}

	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}
	
	public Set<BundledFarePeriodTO> getBundledFarePeriods() {
		return bundledFarePeriods;
	}

	public void setBundledFarePeriods(Set<BundledFarePeriodTO> bundledFarePeriods) {
		this.bundledFarePeriods = bundledFarePeriods;
	}

	/**
	 * @return the descriptionTranslations
	 */
	public Map<String, String> getDescriptionTranslations() {
		return descriptionTranslations;
	}

	/**
	 * @param descriptionTranslations
	 *            the descriptionTranslations to set
	 */
	public void setDescriptionTranslations(Map<String, String> descriptionTranslations) {
		this.descriptionTranslations = descriptionTranslations;
	}

	public Map<String, String> getNameTranslations() {
		return nameTranslations;
	}

	public void setNameTranslations(Map<String, String> nameTranslations) {
		this.nameTranslations = nameTranslations;
	}

	public Set<String> getApplicableSalesChannels() {
		return applicableSalesChannels;
	}

	public void setApplicableSalesChannels(Set<String> applicableSalesChannels) {
		this.applicableSalesChannels = applicableSalesChannels;
	}

	public Integer getCutOffTime() {
		return cutOffTime;
	}

	public void setCutOffTime(Integer cutOffTime) {
		this.cutOffTime = cutOffTime;
	}

	public Integer getBundledCategoryID() {
		return bundledCategoryID;
	}

	public void setBundledCategoryID(Integer bundledCategoryID) {
		this.bundledCategoryID = bundledCategoryID;
	}

	public String getDefaultBundled() {
		return defaultBundled;
	}

	public void setDefaultBundled(String defaultBundled) {
		this.defaultBundled = defaultBundled;
	}

	public Set<String> getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(Set<String> pointOfSale) {
		this.pointOfSale = pointOfSale;
	}	

}
