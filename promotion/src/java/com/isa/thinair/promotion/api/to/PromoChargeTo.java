package com.isa.thinair.promotion.api.to;

import java.io.Serializable;

public class PromoChargeTo implements Serializable {
	private String ondCode;
	private String chargeCode;
	private String charge;

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}
}
