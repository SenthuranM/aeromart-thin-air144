package com.isa.thinair.promotion.api.model;

import java.util.Date;
import java.util.Set;

/**
 * @author chanaka
 * @hibernate.class table = "T_CAMPAIGN" 
 */
public class Campaign {

	private static final long serialVersionUID = 8208054972272435408L;
	
	private Integer campaignId;
	
	private String campaignName;
	
	private String campaignDes;
	
	private Date createdDate;
	
	private long noOfPromoCodesSent;
	
	private Set<CampaignPromoCode> campaignPromoCodes;
	
	private Date startDate;
	
	private Date endDate;
	
	private String emailContent;
	
	private String cronExpression;

	/**
	 * @return the campaignId
	 * @hibernate.id column = "CAMPAIGN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CAMPAIGN"
	 */
	public Integer getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the campaignName
	 * @hibernate.property column = "CAMPAIGN_NAME"
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the campaignDes
	 * @hibernate.property column = "CAMPAIGN_DESCRIPTION"
	 */
	public String getCampaignDes() {
		return campaignDes;
	}

	/**
	 * @param campaignDes the campaignDes to set
	 */
	public void setCampaignDes(String campaignDes) {
		this.campaignDes = campaignDes;
	}

	/**
	 * @return the createdDate
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the noOfPromoCodesSent
	 * @hibernate.property column = "TOTAL_PROMO_CODE_COUNT"
	 */
	public long getNoOfPromoCodesSent() {
		return noOfPromoCodesSent;
	}

	/**
	 * @param noOfPromoCodesSent the noOfPromoCodesSent to set
	 */
	public void setNoOfPromoCodesSent(long noOfPromoCodesSent) {
		this.noOfPromoCodesSent = noOfPromoCodesSent;
	}

	/**
	 * @return the campaignPromoCodes
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="CAMPAIGN_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.promotion.api.model.CampaignPromoCode"
	 */
	public Set<CampaignPromoCode> getCampaignPromoCodes() {
		return campaignPromoCodes;
	}

	/**
	 * @param campaignPromoCodes the campaignPromoCodes to set
	 */
	public void setCampaignPromoCodes(Set<CampaignPromoCode> campaignPromoCodes) {
		this.campaignPromoCodes = campaignPromoCodes;
	}

	/**
	 * @return the startDate
	 * @hibernate.property column = "START_DATE"
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 * @hibernate.property column = "END_DATE"
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the emailContent
	 * @hibernate.property column = "EMAIL_TEMPLATE"
	 */
	public String getEmailContent() {
		return emailContent;
	}

	/**
	 * @param emailContent the emailContent to set
	 */
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	/**
	 * @return the cronExpression
	 * @hibernate.property column = "CRON_EXPRESSION"
	 */
	public String getCronExpression() {
		return cronExpression;
	}

	/**
	 * @param cronExpression the cronExpression to set
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	
	
}
