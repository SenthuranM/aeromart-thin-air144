package com.isa.thinair.promotion.api.to.constants;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

public abstract class PromotionCriteriaConstants {

	public static final int SYS_GEN_PROMO_CODE_LENGTH = 6;
	public static final String BUY_AND_GET_FREE_ANY = "ANY";
	public static final String BUY_AND_GET_FREE_ALL = "ALL";

	public static class PromotionCriteriaTypes {
		public static final String SYSGEN = "SYSGEN";
		public static final String BUYNGET = "BUYNGET";
		public static final String DISCOUNT = "DISCOUNT";
		public static final String FREESERVICE = "FREESERVICE";
	}

	public static class PromotionCriteriaTypesDesc {
		public static final String BUY_AND_GET_FREE_CRIERIA = "Buy And Get Free";
		public static final String FREE_SERVICE_CRITERIA = "Free Service";
		public static final String DISCOUNT_CRITERIA = "Discount";
		public static final String SYS_GEN_PROMO = "User Credits Received from Promotion";
	}

	public static class PromotionCriteriaFormatConstants {
		public static final String BUY_N_GET_QUANTITY_SEPERATOR = ",";
		public static final String DATE_PERIOD_SEPERATOR = "-";
	}

	public static class PromotionStatus {
		public static final String ACTIVE = "ACT";
		public static final String INACTIVE = "INA";
	}
	
	public static class VoucherTemplateStatus {
		public static final String ACTIVE = "ACT";
		public static final String INACTIVE = "INA";
	}

	public static enum ANCILLARIES {
		BAGGAGE(EXTERNAL_CHARGES.BAGGAGE.toString(), "Baggage"), SSR(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString(),
				"In-flight Service"), SEAT_MAP(EXTERNAL_CHARGES.SEAT_MAP.toString(), "Seat"), AIRPORT_SERVICE(
				EXTERNAL_CHARGES.AIRPORT_SERVICE.toString(), "Airport Service"), MEAL(EXTERNAL_CHARGES.MEAL.toString(), "Meal"), INSURANCE(
				EXTERNAL_CHARGES.INSURANCE.toString(), "Insurance");

		private String code;
		private String name;

		private ANCILLARIES(String code, String name) {
			this.code = code;
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public static String getName(String code) {
			if (BAGGAGE.getCode().equals(code)) {
				return BAGGAGE.getName();
			} else if (SSR.getCode().equals(code)) {
				return SSR.getName();
			} else if (SEAT_MAP.getCode().equals(code)) {
				return SEAT_MAP.getName();
			} else if (AIRPORT_SERVICE.getCode().equals(code)) {
				return AIRPORT_SERVICE.getName();
			} else if (MEAL.getCode().equals(code)) {
				return MEAL.getName();
			} else if (INSURANCE.getCode().equals(code)) {
				return INSURANCE.getName();
			}
			return "";
		}
	}

	public static class ApplyToOptions {
		public static final String FARE = "Fare";
		public static final String FARE_SURCHARGES = "Fare + Surcharges";
		public static final String TOTAL = "Total";
	}

	public static enum DiscountApplyTo {
		FARE("Fare", "PERCENTAGE"), FARE_SURCHARGE("FareSurcharges", "PERCENTAGE"), TOTAL("Total", "PERCENTAGE"), RESERVATION(
				"Reservation", "VALUE"), PAX("Passenger", "VALUE");

		private String name;
		private String discountType;

		private DiscountApplyTo(String name, String discountType) {
			this.name = name;
			this.discountType = discountType;
		}

		public String getName() {
			return name;
		}

		public String getDiscountType() {
			return discountType;
		}
	}

	public static class DiscountTypes {
		public static final String VALUE = "VALUE";
		public static final String PERCENTAGE = "PERCENTAGE";
	}

	public static class DiscountApplyAsTypes {
		public static final String MONEY = "Money";
		public static final String CREDIT = "Credit";
	}

	public static class Flag {
		public static final String YES = "Y";
		public static final String NO = "N";
	}

	public static class ApplicableRoute {
		public static final String OUT_BOUND = "OB";
		public static final String IN_BOUND = "IB";
		public static final String RETURN = "RT";
	}

	public static class VoucherStatus {
		public static final Character VOUCHER_STATUS_ISSUED = 'I';
		public static final Character VOUCHER_STATUS_BLOCKED = 'B';
		public static final Character VOUCHER_STATUS_CANCELED = 'C';
		public static final Character VOUCHER_STATUS_USED = 'U';
		public static final Character VOUCHER_STATUS_EXPIRED = 'E';
	}

	public static class VoucherPaymentType {
		public static final String CASH_PAYMENT = "CA";
		public static final String ONACCOUNT_PAYMENT = "OA";
		public static final String CREDIT_CARD_PAYMENT = "CC";
	}
	
	public static class ProductType {
		public static final Character PNR_RESERVATION = 'P';
		public static final Character GIFT_VOUCHER = 'V';
		public static final Character GOQUO = 'G';
		public static final Character MCO = 'M';
		public static final Character AGENT_TOPUP = 'T';
	}

	public static class VoucherType {
		public static final String VOUCHER = "VOUCHER";
		public static final String GIFT_VOUCHER = "GIFT_VOUCHER";
	}
	
	public static class VoucherTemplateName {
		public static final String VOUCHER = "TermsVoucher";
		public static final String GIFT_VOUCHER = "TermsGiftVoucher";
	}
}
