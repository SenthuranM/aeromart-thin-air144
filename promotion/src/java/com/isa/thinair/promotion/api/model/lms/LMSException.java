package com.isa.thinair.promotion.api.model.lms;

public class LMSException extends Exception {

	private static final long serialVersionUID = -9212372707465540476L;

	public LMSException(String message) {
		super(message);
	}

	public LMSException(String message, Throwable cause) {
		super(message, cause);
	}
}
