package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * 
 * @hibernate.class table = "T_BUNDLED_FARE_PERIOD_IMAGE"
 */
public class BundledFarePeriodImage extends Persistent {

	private static final long serialVersionUID = 7336761319618684595L;

	private Integer periodImageId;
	private Integer bundleFarePeriodId;
	private String languageCode;
	private String imageUrl;

	/**
	 * @hibernate.id column = "BUNDLED_FARE_PERIOD_IMAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLED_FARE_PERIOD_IMAGE"
	 * 
	 * @return the freeServiceId
	 */
	public Integer getPeriodImageId() {
		return periodImageId;
	}

	public void setPeriodImageId(Integer periodImageId) {
		this.periodImageId = periodImageId;
	}

	/**
	 * @hibernate.property column = "BUNDLED_FARE_PERIOD_ID"
	 */
	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	/**
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @hibernate.property column = "IMAGE_URL"
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "BundledFarePeriodImage [periodImageId=" + periodImageId + ", bundleFarePeriodId=" + bundleFarePeriodId
				+ ", languageCode=" + languageCode + ", imageUrl=" + imageUrl + "]";
	}

}
