package com.isa.thinair.promotion.api.model;

import java.io.Serializable;

/**
 * @author chanaka
 * @hibernate.class table = "T_PROMO_CRITERIA_CODE"
 */
public class PromoCode implements Serializable {

	private static final long serialVersionUID = 3578664587088341684L;	
	
	/**
	 * Holds the ID of the promotion code
	 */
	private String promoCode;
	
	/**
	 * ID field for relevant PromoitonCriteria
	 */
	private PromotionCriteria promoCriteria;
	
	/**
	 * Boolean indicating whether a promotion code is fully utilized or not. 
	 */
	private boolean fullyUtilized = Boolean.FALSE;

	/**
	 * @hibernate.id column = "PROMO_CODE" generator-class = "assigned"
	 * 
	 * @return promoCode
	 */
	public String getPromoCode() {
		return promoCode;
	}

	/**
	 * @param promoCode the promoCode to set
	 */
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	/**
	 * @hibernate.property column = "FULLY_UTILIZED" type = "yes_no"
	 * 
	 * @return fullyUtilized
	 */
	public boolean isFullyUtilized() {
		return fullyUtilized;
	}

	/**
	 * @param fullyUtilized the fullyUtilized to set
	 */
	public void setFullyUtilized(boolean fullyUtilized) {
		this.fullyUtilized = fullyUtilized;
	}

	/**
	 * @hibernate.many-to-one column="PROMO_CRITERIA_ID" class="com.isa.thinair.promotion.api.model.PromotionCriteria" 
	 * 
	 * @return promoCriteria
	 */
	public PromotionCriteria getPromoCriteria() {
		return promoCriteria;
	}

	/**
	 * @param promoCriteria the promoCriteria to set
	 */
	public void setPromoCriteria(PromotionCriteria promoCriteria) {
		this.promoCriteria = promoCriteria;
	}

}
