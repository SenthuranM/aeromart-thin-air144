package com.isa.thinair.promotion.api.model;

import java.io.Serializable;
import java.util.Date;

public class DatePeriod implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PromotionCriteria promotionCriteria;

	private Date fromDate;

	private Date toDate;

	/**
	 * @hibernate.many-to-one column="PROMO_CRITERIA_ID" class="com.isa.thinair.promotion.api.model.PromotionCriteria"
	 * 
	 * @return promotionCriteria
	 */
	public PromotionCriteria getPromotionCriteria() {
		return promotionCriteria;
	}

	/**
	 * @param promotionCriteria
	 */
	public void setPromotionCriteria(PromotionCriteria promotionCriteria) {
		this.promotionCriteria = promotionCriteria;
	}

	/**
	 * @hibernate.property column = "FROM_DATE"
	 * 
	 * @return fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @hibernate.property column = "TO_DATE"
	 * 
	 * @return toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
