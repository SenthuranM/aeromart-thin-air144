package com.isa.thinair.promotion.api.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Set;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.core.dto.ReservationSegSummaryTo;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

public abstract class PromotionsUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(PromotionsUtils.class);
	public static final char PROMO_KEYS_SEPERATOR = '_';
	public static final String PROMO_CODE_LANG_KEY = "PRM_DES";

	public static String getFlexibilityCodeByBoundaries(PromotionRequest promotionRequest) {
		Set<PromotionRequestConfig> configs = promotionRequest.getPromotionRequestConfigs();
		String lowerBound = null;
		String upperBound = null;

		for (PromotionRequestConfig config : configs) {
			if (config.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND)) {
				lowerBound = config.getParamValue();
			} else if (config.getPromoReqParam().equals(PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND)) {
				upperBound = config.getParamValue();
			}

			if (lowerBound != null && upperBound != null) {
				break;
			}
		}

		return lowerBound + PROMO_KEYS_SEPERATOR + upperBound;
	}

	/**
	 * 
	 * @param segmentCode
	 * @return Pair< From-Airport , To-Airport >
	 */
	public static Pair<String, String> getOriginAndDestinationAirports(String segmentCode) {
		Pair<String, String> originDestPair = null;
		if (segmentCode != null) {
			String arr[] = segmentCode.split(PromotionsInternalConstants.OND_DELIM);
			originDestPair = Pair.of(arr[0], arr[arr.length - 1]);
		}
		return originDestPair;
	}

	/**
	 * 
	 * @param allSegTos
	 * @return Pair< Outbound-Segs , Inbound-Segs >
	 */
	public static Pair<List<ReservationSegSummaryTo>, List<ReservationSegSummaryTo>>
			seperateOutboundAndInboundSegments(List<ReservationSegSummaryTo> allSegTos) {
		List<ReservationSegSummaryTo> outboundSegs = new ArrayList<ReservationSegSummaryTo>();
		List<ReservationSegSummaryTo> inboundSegs = new ArrayList<ReservationSegSummaryTo>();
		Pair<String, String> originDest;
		Set<String> processedAirportCodes = new HashSet<String>();
		boolean returnStarted = false;

		Collections.sort(allSegTos, new Comparator<ReservationSegSummaryTo>() {
			public int compare(ReservationSegSummaryTo o1, ReservationSegSummaryTo o2) {
				return o1.getDepatureDate().compareTo(o2.getDepatureDate());
			}
		});

		for (ReservationSegSummaryTo segSummaryTo : allSegTos) {

			originDest = getOriginAndDestinationAirports(segSummaryTo.getSegmentCode());

			if (!returnStarted) {
				processedAirportCodes.add(originDest.getLeft());
				if (!processedAirportCodes.contains(originDest.getRight())) {
					segSummaryTo.setInbound(false);
					outboundSegs.add(segSummaryTo);
				} else {
					returnStarted = true;
					segSummaryTo.setInbound(true);
					inboundSegs.add(segSummaryTo);
				}
			} else {
				returnStarted = true;
				segSummaryTo.setInbound(true);
				inboundSegs.add(segSummaryTo);
			}
		}

		return Pair.of(outboundSegs, inboundSegs);
	}

	public static Pair<String, String> getOutboundAndInboundOndCodes(Collection<String> segCodes) {
		String outboundOnd = "";
		String inboundOnd = null;
		Pair<String, String> originDest;
		Set<String> processedAirportCodes = new HashSet<String>();
		boolean returnStarted = false;

		Iterator<String> itrSegCodes = segCodes.iterator();
		for (String segCode = itrSegCodes.next();; segCode = itrSegCodes.next()) {

			originDest = getOriginAndDestinationAirports(segCode);

			if (!returnStarted) {
				processedAirportCodes.add(originDest.getLeft());
				if (!processedAirportCodes.contains(originDest.getRight())) {
					outboundOnd += originDest.getLeft() + PromotionsInternalConstants.OND_DELIM;
					if (!itrSegCodes.hasNext()) {
						outboundOnd += originDest.getRight();
						break;
					}
				} else {
					returnStarted = true;
					inboundOnd = "";
					outboundOnd += originDest.getLeft();
					inboundOnd += originDest.getLeft() + PromotionsInternalConstants.OND_DELIM;
					if (!itrSegCodes.hasNext()) {
						inboundOnd += originDest.getRight();
						break;
					}
				}
			} else {
				inboundOnd += originDest.getLeft() + PromotionsInternalConstants.OND_DELIM;
				if (!itrSegCodes.hasNext()) {
					inboundOnd += originDest.getRight();
					break;
				}
			}
		}

		return Pair.of(outboundOnd, inboundOnd);
	}

	public static Pair<List<PromotionRequestApproveTo>, List<PromotionRequestApproveTo>>
			seperateOutboundAndInboundSegmentsForNextSeat(List<PromotionRequestApproveTo> allSegTos) {
		List<PromotionRequestApproveTo> outboundSegs = new ArrayList<PromotionRequestApproveTo>();
		List<PromotionRequestApproveTo> inboundSegs = new ArrayList<PromotionRequestApproveTo>();
		Pair<String, String> originDest;
		Set<String> processedAirportCodes = new HashSet<String>();
		boolean returnStarted = false;

		Collections.sort(allSegTos, new Comparator<PromotionRequestApproveTo>() {
			public int compare(PromotionRequestApproveTo o1, PromotionRequestApproveTo o2) {
				return o1.getDepartureDate().compareTo(o2.getDepartureDate());
			}
		});

		for (PromotionRequestApproveTo promoReqTO : allSegTos) {

			originDest = getOriginAndDestinationAirports(promoReqTO.getSegmentCode());

			if (!returnStarted) {
				processedAirportCodes.add(originDest.getLeft());
				if (!processedAirportCodes.contains(originDest.getRight())) {

					outboundSegs.add(promoReqTO);
				} else {
					returnStarted = true;

					inboundSegs.add(promoReqTO);
				}
			} else {
				inboundSegs.add(promoReqTO);
			}
		}

		return Pair.of(outboundSegs, inboundSegs);
	}

	public static int getDirectionByOutboundInboundReqs(
			Pair<List<PromotionRequestApproveTo>, List<PromotionRequestApproveTo>> reqs, int flightSegId) {
		for (PromotionRequestApproveTo req : reqs.getLeft()) {
			if (req.getFlightSegId() == flightSegId) {
				return PromotionsInternalConstants.DIRECTION_OUTBOUND;
			}
		}

		if (reqs.getRight() != null) {
			for (PromotionRequestApproveTo req : reqs.getRight()) {
				if (req.getFlightSegId() == flightSegId) {
					return PromotionsInternalConstants.DIRECTION_INBOUND;
				}
			}
		}

		return PromotionsInternalConstants.DIRECTION_NONE;
	}

	public static String getOndCode(List<String> segCodes) {
		String ondCode = "";
		Pair<String, String> originAndDest = null;
		int a = 0;

		for (; a <= segCodes.size() - 1; a++) {
			originAndDest = getOriginAndDestinationAirports(segCodes.get(a));
			ondCode += originAndDest.getLeft() + PromotionsInternalConstants.OND_DELIM;
		}

		originAndDest = getOriginAndDestinationAirports(segCodes.get(segCodes.size() - 1));
		ondCode += originAndDest.getRight();
		return ondCode;
	}

	public static PromoSelectionCriteria buildPromoSelectionCriteria(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, UserPrincipal userPrincipal,TrackInfoDTO trackInfo) throws ModuleException {
		PromoSelectionCriteria promoSelectionCriteria = new PromoSelectionCriteria();
		promoSelectionCriteria.setPromoCode(availableFlightSearchDTO.getPromoCode());

		// set ond list
		List<AvailableIBOBFlightSegment> selectedOndFlights = selectedFlightDTO.getSelectedOndFlights();
		if (selectedOndFlights != null) {
			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedOndFlights) {
				promoSelectionCriteria.getOndList().add(availableIBOBFlightSegment.getOndCode());

				List<FlightSegmentDTO> fltSegDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();
				for (FlightSegmentDTO flightSegmentDTO : fltSegDTOs) {
					promoSelectionCriteria.getFlights().add(flightSegmentDTO.getFlightNumber());
					promoSelectionCriteria.getFlightIds().add(flightSegmentDTO.getFlightId());
					promoSelectionCriteria.getFlightSegIds().add(flightSegmentDTO.getSegmentId());
					promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().put(flightSegmentDTO.getSegmentId(),
							availableIBOBFlightSegment.getSelectedLogicalCabinClass());
				}

				promoSelectionCriteria.getCabinClasses().add(availableIBOBFlightSegment.getCabinClassCode());
				promoSelectionCriteria.getOndFlightDates().put(availableIBOBFlightSegment.getOndSequence(),
						availableIBOBFlightSegment.getDepartureDate());

				String logicalCabinClass = availableIBOBFlightSegment.getSelectedLogicalCabinClass();
				promoSelectionCriteria.getLogicalCabinClasses().add(logicalCabinClass);

				if (availableIBOBFlightSegment instanceof AvailableFlightSegment) {
					FareSummaryDTO fareSummaryDTO = availableIBOBFlightSegment.getFare(logicalCabinClass);
					if (fareSummaryDTO != null) {
						promoSelectionCriteria.getBookingClasses().add(fareSummaryDTO.getBookingClassCode());
					}
				} else if (availableIBOBFlightSegment instanceof AvailableConnectedFlight) {

					FareSummaryDTO fareSummaryDTO = availableIBOBFlightSegment.getFare(logicalCabinClass);
					if (fareSummaryDTO != null) {
						promoSelectionCriteria.getBookingClasses().add(fareSummaryDTO.getBookingClassCode());
					} else {
						List<AvailableFlightSegment> availableFlightSegments = ((AvailableConnectedFlight) availableIBOBFlightSegment)
								.getAvailableFlightSegments();
						if (availableFlightSegments != null) {
							for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
								fareSummaryDTO = availableFlightSegment.getFare(logicalCabinClass);
								if (fareSummaryDTO != null) {
									promoSelectionCriteria.getBookingClasses().add(fareSummaryDTO.getBookingClassCode());
								}
							}
						}
					}

				}

			}
		}

		promoSelectionCriteria.setSalesChannel(availableFlightSearchDTO.getChannelCode());
		promoSelectionCriteria.setReservationDate(CalendarUtil.getCurrentSystemTimeInZulu());
		promoSelectionCriteria.setAgent(availableFlightSearchDTO.getAgentCode());
		promoSelectionCriteria.setAdultCount(availableFlightSearchDTO.getAdultCount());
		promoSelectionCriteria.setChildCount(availableFlightSearchDTO.getChildCount());
		promoSelectionCriteria.setInfantCount(availableFlightSearchDTO.getInfantCount());
		promoSelectionCriteria.setBankIdNo(availableFlightSearchDTO.getBankIdentificationNo());
		promoSelectionCriteria.setJourneyType(availableFlightSearchDTO.getJourneyType());
		promoSelectionCriteria.setPosCountry(getCountryCode(userPrincipal.getIpAddress()));
		if (availableFlightSearchDTO.getPreferredLanguage() != null) {
			promoSelectionCriteria.setPreferredLanguage(availableFlightSearchDTO.getPreferredLanguage());
		} else {
			promoSelectionCriteria.setPreferredLanguage(Locale.ENGLISH.getLanguage());
		}
		if(trackInfo!=null) {
			promoSelectionCriteria.setLoyalityMember(isLoyalityMember(trackInfo.getCustomerId()));
			promoSelectionCriteria.setRegisteredMember(isRegMember(trackInfo.getCustomerId()));
		}else {
			promoSelectionCriteria.setLoyalityMember(false);
			promoSelectionCriteria.setRegisteredMember(false);
		}

		log.debug("The selected promotion criteria is built with following info " + " Sales channel:"
				+ promoSelectionCriteria.getSalesChannel() + " Reservation Date: " + promoSelectionCriteria.getReservationDate()
				+ " Agent: " + promoSelectionCriteria.getAgent() + "Adult Count: " + promoSelectionCriteria.getAdultCount()
				+ " Child count: " + promoSelectionCriteria.getChildCount() + " Infant count: "
				+ promoSelectionCriteria.getInfantCount() + "Bank Id No: " + promoSelectionCriteria.getBankIdNo() + "Journey type"
				+ promoSelectionCriteria.getJourneyType() + " Country code retrived for the IP address: "
				+ userPrincipal.getIpAddress() + " is " + promoSelectionCriteria.getPosCountry());
		return promoSelectionCriteria;
	}

	public static PromoSelectionCriteria buildPromoSelectionCriteria(Collection<OndFareDTO> changedOndFareDTOs,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		PromoSelectionCriteria promoSelectionCriteria = new PromoSelectionCriteria();
		promoSelectionCriteria.setPromoCode(availableFlightSearchDTO.getPromoCode());

		Map<Integer, List<String>> ondWiseSegCode = new HashMap<Integer, List<String>>();

		for (OndFareDTO ondFareDTO : changedOndFareDTOs) {
			if (!ondWiseSegCode.containsKey(ondFareDTO.getOndSequence())) {
				ondWiseSegCode.put(ondFareDTO.getOndSequence(), new ArrayList<String>());
			}

			ondWiseSegCode.get(ondFareDTO.getOndSequence()).add(ondFareDTO.getOndCode());

			Collection<SegmentSeatDistsDTO> segDists = ondFareDTO.getSegmentSeatDistsDTO();
			if (segDists != null) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : segDists) {
					int fltSegId = segmentSeatDistsDTO.getFlightSegId();
					FlightSegmentDTO flightSegmentDTO = ondFareDTO.getFlightSegmentDTO(fltSegId);
					promoSelectionCriteria.getFlights().add(flightSegmentDTO.getFlightNumber());
					promoSelectionCriteria.getFlightIds().add(segmentSeatDistsDTO.getFlightId());
					promoSelectionCriteria.getFlightSegIds().add(fltSegId);
					promoSelectionCriteria.getLogicalCabinClasses().add(segmentSeatDistsDTO.getLogicalCabinClass());
					promoSelectionCriteria.getCabinClasses().add(segmentSeatDistsDTO.getCabinClassCode());
					promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().put(fltSegId,
							segmentSeatDistsDTO.getLogicalCabinClass());
				}
			}

			FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();
			if (fareSummaryDTO != null) {
				promoSelectionCriteria.getBookingClasses().add(fareSummaryDTO.getBookingClassCode());
			}

			promoSelectionCriteria.getOndFlightDates().put(ondFareDTO.getOndSequence(), ondFareDTO.getFirstDepartureDateZulu());
		}

		for (Entry<Integer, List<String>> ondEntry : ondWiseSegCode.entrySet()) {
			String ondCode = "";
			List<String> segCodes = ondEntry.getValue();
			for (int i = 0; i < segCodes.size(); i++) {
				String segCode = segCodes.get(i);
				if (i == 0) {
					ondCode = segCode;
				} else {
					ondCode = ondCode.concat(segCode.substring(segCode.indexOf("/")));
				}
			}

			promoSelectionCriteria.getOndList().add(ondCode);
		}

		promoSelectionCriteria.setSalesChannel(availableFlightSearchDTO.getChannelCode());
		promoSelectionCriteria.setReservationDate(CalendarUtil.getCurrentSystemTimeInZulu());
		promoSelectionCriteria.setAgent(availableFlightSearchDTO.getAgentCode());
		promoSelectionCriteria.setAdultCount(availableFlightSearchDTO.getAdultCount());
		promoSelectionCriteria.setChildCount(availableFlightSearchDTO.getChildCount());
		promoSelectionCriteria.setInfantCount(availableFlightSearchDTO.getInfantCount());
		promoSelectionCriteria.setBankIdNo(availableFlightSearchDTO.getBankIdentificationNo());
		promoSelectionCriteria.setJourneyType(availableFlightSearchDTO.getJourneyType());

		return promoSelectionCriteria;
	}

	public static BigDecimal[] getEffectivePaxDiscountValueWithoutLoss(float discountValue, int discountApplyTo,
			String discountType, int payablePaxCount, int ondCount) {
		BigDecimal effectiveDiscountValue = new BigDecimal(new Float(discountValue).toString());
		int arrLength = 1;
		BigDecimal[] bigDecArr = new BigDecimal[arrLength];
		bigDecArr[0] = effectiveDiscountValue;
		if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
			bigDecArr[0] = effectiveDiscountValue;
		} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
			if (PromotionCriteriaConstants.DiscountApplyTo.RESERVATION.ordinal() == discountApplyTo) {
				if (ondCount != 0) {
					arrLength = (payablePaxCount * ondCount);
					bigDecArr = AccelAeroCalculator.roundAndSplit(effectiveDiscountValue, arrLength);
				} else {
					arrLength = payablePaxCount;
					bigDecArr = AccelAeroCalculator.roundAndSplit(effectiveDiscountValue, payablePaxCount);
				}
			} else if (PromotionCriteriaConstants.DiscountApplyTo.PAX.ordinal() == discountApplyTo) {
				BigDecimal[] tempBigDecArr = new BigDecimal[arrLength];
				if (ondCount != 0) {
					arrLength = ondCount * payablePaxCount;
					tempBigDecArr = AccelAeroCalculator.roundAndSplit(effectiveDiscountValue, ondCount);
				} else {
					arrLength = payablePaxCount;
					tempBigDecArr[0] = effectiveDiscountValue;
				}

				bigDecArr = new BigDecimal[arrLength];
				int count = 0;
				for (BigDecimal tempBigDecval : tempBigDecArr) {
					for (int i = 0; i < payablePaxCount; i++) {
						bigDecArr[count] = tempBigDecval;
						count++;
					}
				}
			}

		}

		return bigDecArr;
	}

	public static Set<Integer> getFlightSegIdsWithInterceptingSegIds(Set<Integer> sourceFltSegIds) throws ModuleException {
		List<Integer> allFlightSegIds = new ArrayList<Integer>(sourceFltSegIds);

		List<Integer> interceptingFltSegIds = PromotionModuleServiceLocator.getFlightInventoryBD()
				.getInterceptingSegmentIDs(allFlightSegIds);

		if (interceptingFltSegIds != null && !interceptingFltSegIds.isEmpty()) {
			allFlightSegIds.addAll(interceptingFltSegIds);
		}

		return new HashSet<Integer>(allFlightSegIds);
	}

	public static BigDecimal[] getPaxWiseTotalDiscount(BigDecimal[] paxWiseOndWiseDiscount, int paxCount) {
		List<BigDecimal> arrayList = new ArrayList<BigDecimal>();
		for (int i = 0; i < paxWiseOndWiseDiscount.length; i++) {
			arrayList.add(paxWiseOndWiseDiscount[i]);
		}

		BigDecimal[] paxDiscount = new BigDecimal[paxCount];

		for (int start = 0; start < arrayList.size(); start += paxCount) {
			int end = Math.min(start + paxCount, arrayList.size());
			List<BigDecimal> sublist = arrayList.subList(start, end);

			for (int i = 0; i < sublist.size(); i++) {
				BigDecimal discountValue = sublist.get(i);

				if (paxDiscount[i] == null) {
					paxDiscount[i] = discountValue;
				} else {
					paxDiscount[i] = AccelAeroCalculator.add(paxDiscount[i], discountValue);
				}

			}
		}

		return paxDiscount;
	}

	public static String getDryOperatingAirline(List<FlightSegmentTO> flightSegmentTOs, String system) {
		if (isDryBooking(flightSegmentTOs, system)) {
			return getOperatingAirlines(flightSegmentTOs).iterator().next();
		}
		return null;
	}

	public static SYSTEM getPreferedSystem(String searchSystem) {

		SYSTEM system = SYSTEM.AA;
		if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
			system = SYSTEM.getEnum(searchSystem, SYSTEM.AA);
		} else {
			if (AppSysParamsUtil.isLCCConnectivityEnabled() && AppSysParamsUtil.showLCCResultsInIBE()) {
				system = SYSTEM.getEnum(searchSystem, SYSTEM.INT);
			} else {
				system = SYSTEM.getEnum(searchSystem, SYSTEM.AA);
			}
		}
		return system;
	}

	public static Map<Integer, Date> getOndFlightDates(List<FlightSegmentTO> flightSegmentTOs) {
		Map<Integer, List<FlightSegmentTO>> ondFlights = new HashMap<Integer, List<FlightSegmentTO>>();
		Map<Integer, Date> ondFlightDates = new HashMap<Integer, Date>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (!ondFlights.containsKey(flightSegmentTO.getOndSequence())) {
				ondFlights.put(flightSegmentTO.getOndSequence(), new ArrayList<FlightSegmentTO>());
			}

			ondFlights.get(flightSegmentTO.getOndSequence()).add(flightSegmentTO);
		}

		for (Entry<Integer, List<FlightSegmentTO>> flightEntry : ondFlights.entrySet()) {
			Integer ondSeq = flightEntry.getKey();
			List<FlightSegmentTO> flights = flightEntry.getValue();
			if (flights != null && !flights.isEmpty()) {
				Collections.sort(flights);
				ondFlightDates.put(ondSeq, flights.iterator().next().getDepartureDateTimeZulu());
			}

		}

		return ondFlightDates;
	}

	private static boolean isDryBooking(List<FlightSegmentTO> flightSegmentTOs, String system) {
		Set<String> operatingAirlines = getOperatingAirlines(flightSegmentTOs);

		if (getPreferedSystem(system) == SYSTEM.INT && operatingAirlines.size() == 1) {
			return true;
		}
		return false;
	}

	private static Set<String> getOperatingAirlines(List<FlightSegmentTO> flightSegmentTOs) {
		Set<String> operatingAirLines = new HashSet<String>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			operatingAirLines.add(flightSegmentTO.getOperatingAirline());
		}

		return operatingAirLines;
	}

	private static String getCountryCode(String ip) throws ModuleException {
		String countryCode = PromotionModuleServiceLocator.getCommonMasterBD().getCountryByIpAddress(ip);
		return countryCode;
	}

	private static boolean isLoyalityMember(String customerId) throws ModuleException {
		if (isRegMember(customerId)) {
			Customer customer=PromotionModuleServiceLocator.getAirCustomerServiceBD().getCustomer(Integer.parseInt(customerId));
			if(customer!=null) {
				if (customer.getIsLmsMember()==PromotionCriteriaConstants.Flag.YES.charAt(0)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isRegMember(String customerId) throws ModuleException {
		if (customerId != null) {
			return true;
		}
		return false;
	}

}
