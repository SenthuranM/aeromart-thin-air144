package com.isa.thinair.promotion.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PROMOTION_REQUEST_CONFIG" lazy="false"
 */
public class PromotionRequestConfig extends Tracking {

	private Integer promoRequestConfigId;
	private PromotionRequest promotionRequest;
	private String promoReqParam;
	private String paramValue;
	private String status;

	/**
	 * @hibernate.id column = "PROMOTION_REQUEST_CONFIG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PROMOTION_REQUEST_CONFIG"
	 */
	public Integer getPromoRequestConfigId() {
		return promoRequestConfigId;
	}

	public void setPromoRequestConfigId(Integer promoRequestConfigId) {
		this.promoRequestConfigId = promoRequestConfigId;
	}

	/**
	 * @hibernate.many-to-one column="PROMOTION_REQUEST_ID" cascade="all"
	 *                        class="com.isa.thinair.promotion.api.model.PromotionRequest"
	 */
	public PromotionRequest getPromotionRequest() {
		return promotionRequest;
	}

	public void setPromotionRequest(PromotionRequest promotionRequest) {
		this.promotionRequest = promotionRequest;
	}

	/**
	 * @hibernate.property column = "REQ_PARAM"
	 */
	public String getPromoReqParam() {
		return promoReqParam;
	}

	public void setPromoReqParam(String promoReqParam) {
		this.promoReqParam = promoReqParam;
	}

	/**
	 * @hibernate.property column = "PARAM_VALUE"
	 */
	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getPromoReqParam()).append(getParamValue()).toHashCode();
	}

	public boolean equals(Object obj) {
		PromotionRequestConfig other;
		if (obj != null && obj instanceof PromotionRequestConfig) {
			other = (PromotionRequestConfig) obj;
			return (getPromoReqParam() != null && other.getPromoReqParam() != null && getPromoReqParam().equals(
					other.getPromoReqParam()))
					&& (getParamValue() != null && other.getParamValue() != null && getParamValue().equals(other.getParamValue()));
		}
		return false;
	}

}
