package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public class BundledFarePeriodTO implements Serializable, Comparable<BundledFarePeriodTO> {

	private static final long serialVersionUID = 3829164601292944104L;

	private Integer bundleFarePeriodId;
	private Date flightFrom;
	private Date flightTo;
	private BigDecimal bundleFee;
	private Set<BundledFareFreeServiceTO> bundledServices;
	private Long version;

	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	public Date getFlightFrom() {
		return flightFrom;
	}

	public void setFlightFrom(Date flightFrom) {
		this.flightFrom = flightFrom;
	}

	public Date getFlightTo() {
		return flightTo;
	}

	public void setFlightTo(Date flightTo) {
		this.flightTo = flightTo;
	}

	public BigDecimal getBundleFee() {
		return bundleFee;
	}

	public void setBundleFee(BigDecimal bundleFee) {
		this.bundleFee = bundleFee;
	}

	public Set<BundledFareFreeServiceTO> getBundledServices() {
		return bundledServices;
	}

	public void setBundledServices(Set<BundledFareFreeServiceTO> bundledServices) {
		this.bundledServices = bundledServices;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	@Override
	public int compareTo(BundledFarePeriodTO other) {
		return this.flightFrom.compareTo(other.getFlightFrom());
	}

	@Override
	public String toString() {
		return "BundledFarePeriodTO [bundleFarePeriodId=" + bundleFarePeriodId + ", flightFrom=" + flightFrom + ", flightTo="
				+ flightTo + ", bundleFee=" + bundleFee + ", bundledServices=" + bundledServices + ", version=" + version + "]";
	}

	

}
