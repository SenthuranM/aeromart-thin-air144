package com.isa.thinair.promotion.api.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Class defining the discount details. Designed to be an embedded class for Subclasses of {@link PromotionCriteria}.
 * Currently used by {@link DiscountPromotionCriteria} and {@link FreeServicePromotionCriteria}
 * 
 * @author thihara
 * 
 */
public class DiscountDetails implements java.io.Serializable {

	private static final long serialVersionUID = 15487442569l;

	/**
	 * How the Discount will be applied. Currently valid values are <B>Money / Credit</B>
	 */
	private String applyAs;

	/**
	 * Where the discount should be applied to. Currently valid options are
	 * 
	 * Fare / Fare + Surcharges / Total
	 * 
	 * Is only valid for {@link DiscountPromotionCriteria} NULL for other.
	 */
	private String applyTo;

	/**
	 * Type of the discount value. Valid values are Value / Percentage
	 */
	private String discountType;

	private BigDecimal discountValue;

	private Boolean constPaxName;

	private Boolean constPaxSector;

	private Date constFlightFrom;

	private Date constFlightTo;

	private Date constValidFrom;

	private Date constValidTo;

	/**
	 * @hibernate.property column = "DISCOUNT_APPLY_AS"
	 * @see #applyAs
	 */
	public String getApplyAs() {
		return applyAs;
	}

	public void setApplyAs(String applyAs) {
		this.applyAs = applyAs;
	}

	/**
	 * @hibernate.property column = "DISCOUNT_APPLY_TO"
	 * @see #applyTo
	 */
	public String getApplyTo() {
		return applyTo;
	}

	public void setApplyTo(String applyTo) {
		this.applyTo = applyTo;
	}

	/**
	 * @hibernate.property column = "DISCOUNT_TYPE"
	 * @see #discountType
	 */
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	/**
	 * @hibernate.property column = "DISCOUNT_VALUE"
	 * @see #discountValue
	 */
	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	/**
	 * @hibernate.property column = "SAME_PAX_RESTRICTION" type = "yes_no"
	 * @see #discountType
	 */
	public Boolean getConstPaxName() {
		return constPaxName;
	}

	public void setConstPaxName(Boolean constPaxName) {
		this.constPaxName = constPaxName;
	}

	/**
	 * @hibernate.property column = "SAME_SECTOR_RESTRICTION" type = "yes_no"
	 * @see #discountType
	 */
	public Boolean getConstPaxSector() {
		return constPaxSector;
	}

	public void setConstPaxSector(Boolean constPaxSector) {
		this.constPaxSector = constPaxSector;
	}

	/**
	 * @hibernate.property column = "DEP_DATE_FROM_RESTRICTION"
	 * @see #discountType
	 */
	public Date getConstFlightFrom() {
		return constFlightFrom;
	}

	public void setConstFlightFrom(Date constFlightFrom) {
		this.constFlightFrom = constFlightFrom;
	}

	/**
	 * @hibernate.property column = "DEP_DATE_TO_RESTRICTION"
	 * @see #discountType
	 */
	public Date getConstFlightTo() {
		return constFlightTo;
	}

	public void setConstFlightTo(Date constFlightTo) {
		this.constFlightTo = constFlightTo;
	}

	/**
	 * @hibernate.property column = "EXPIRY_DATE_FROM_RESTRICTION"
	 * @see #discountType
	 */
	public Date getConstValidFrom() {
		return constValidFrom;
	}

	public void setConstValidFrom(Date constValidFrom) {
		this.constValidFrom = constValidFrom;
	}

	/**
	 * @hibernate.property column = "EXPIRY_DATE_TO_RESTRICTION"
	 * @see #discountType
	 */
	public Date getConstValidTo() {
		return constValidTo;
	}

	public void setConstValidTo(Date constValidTo) {
		this.constValidTo = constValidTo;
	}

}
