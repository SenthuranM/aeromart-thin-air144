package com.isa.thinair.promotion.api.to.lms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 
 * @author rumesh
 * 
 */
public class RedeemCalculateRes implements Serializable {

	private static final long serialVersionUID = 2627494590929805657L;

	Map<Integer, Map<String, Double>> paxWiseProductPoints;

	Map<Integer, Map<String, BigDecimal>> paxWiseProductValue;

	public Map<Integer, Map<String, Double>> getPaxWiseProductPoints() {
		return paxWiseProductPoints;
	}

	public void setPaxWiseProductPoints(Map<Integer, Map<String, Double>> paxWiseProductPoints) {
		this.paxWiseProductPoints = paxWiseProductPoints;
	}

	public Map<Integer, Map<String, BigDecimal>> getPaxWiseProductValue() {
		return paxWiseProductValue;
	}

	public void setPaxWiseProductValue(Map<Integer, Map<String, BigDecimal>> paxWiseProductValue) {
		this.paxWiseProductValue = paxWiseProductValue;
	}

}
