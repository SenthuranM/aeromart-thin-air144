package com.isa.thinair.promotion.api.utils;

import static com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaFormatConstants.BUY_N_GET_QUANTITY_SEPERATOR;
import static com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaFormatConstants.DATE_PERIOD_SEPERATOR;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.model.BuyNGetPromotionCriteria;
import com.isa.thinair.promotion.api.model.BuyNGetQuantity;
import com.isa.thinair.promotion.api.model.DatePeriod;
import com.isa.thinair.promotion.api.model.DiscountDetails;
import com.isa.thinair.promotion.api.model.DiscountPromotionCriteria;
import com.isa.thinair.promotion.api.model.FlightPeriod;
import com.isa.thinair.promotion.api.model.FreeServicePromotionCriteria;
import com.isa.thinair.promotion.api.model.PromoCode;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.RegistrationPeriod;
import com.isa.thinair.promotion.api.model.SysGenPromotionCriteria;
import com.isa.thinair.promotion.api.to.PromoCodeTo;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypesDesc;

/**
 * This utility class contains methods to transform PromotionCriteria data from the domain model (hibernate objects) to
 * DTOs and vice versa.
 * 
 * @author thihara
 * 
 */
public class PromotionCriteriaTransformer {

	/**
	 * Transforms a {@link PromotionCriteriaTO} object into the domain model's {@link PromotionCriteria} object.
	 * Depending on the {@link PromotionCriteriaTO#getPromotionCriteriaType()} specified this method will create the
	 * appropriate subclass of the {@link PromotionCriteria}
	 * 
	 * @param promotionCriteriaTO
	 *            {@link PromotionCriteriaTO} object with the
	 *            {@link PromotionCriteriaTytoStatusChangePromotionCriteriapesDesc} set and other data to be populated
	 *            into the {@link PromotionCriteria}
	 * 
	 * @return A subclass of {@link PromotionCriteria} with the relevant data set. Currently one of
	 *         {@link BuyNGetPromotionCriteria}, {@link FreeServicePromotionCriteria}, {@link DiscountPromotionCriteria}
	 * 
	 * @throws IllegalArgumentException
	 *             If the {@link PromotionCriteriaTO#getPromotionCriteriaType()} is not one of
	 *             {@link PromotionCriteriaTypesDesc}
	 * @throws ParseException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static PromotionCriteria toPromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws IllegalArgumentException, IllegalAccessException, InstantiationException, ParseException {
		PromotionCriteria promotionCriteria = null;

		if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			BuyNGetPromotionCriteria criteria = new BuyNGetPromotionCriteria();
			criteria.setFreeCriteria(transformBuyNGetQuantityFromDTO(promotionCriteriaTO.getFreeCriteria()));
			criteria.setLimitingLoadFactor(promotionCriteriaTO.getLimitingLoadFactor());
			criteria.setConstPaxName(promotionCriteriaTO.getConstPaxName());
			criteria.setConstPaxSector(promotionCriteriaTO.getConstPaxSector());
			criteria.setApplyTo(promotionCriteriaTO.getApplyTO());
			promotionCriteria = criteria;

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			DiscountPromotionCriteria criteria = new DiscountPromotionCriteria();
			criteria.setDiscount(transformDiscountDetailsFromDTO(promotionCriteriaTO));
			criteria.setApplicableBINs(promotionCriteriaTO.getApplicableBINs());

			promotionCriteria = criteria;

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			FreeServicePromotionCriteria criteria = new FreeServicePromotionCriteria();
			criteria.setDiscount(transformDiscountDetailsFromDTO(promotionCriteriaTO));
			criteria.setApplicableAncillaries(promotionCriteriaTO.getApplicableAncillaries());

			promotionCriteria = criteria;

		} else {
			throw new IllegalArgumentException("Unsupported Promotion Type Detected");
		}

		promotionCriteria = setCommonPromotionCriteriaValues(promotionCriteriaTO, promotionCriteria);
		return promotionCriteria;
	}

	public static PromotionCriteria toMinumumPromotionCriteria(PromotionCriteriaTO promotionCriteriaTO)
			throws IllegalArgumentException {
		PromotionCriteria promotionCriteria = null;

		if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new BuyNGetPromotionCriteria();

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new DiscountPromotionCriteria();

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new FreeServicePromotionCriteria();

		} else {
			throw new IllegalArgumentException("Unsupported Promotion Type Detected");
		}

		promotionCriteria.setPromoName(promotionCriteriaTO.getPromoName());
		promotionCriteria.setPromoCode(promotionCriteriaTO.getPromoCode());
		promotionCriteria.setPromoCriteriaID(promotionCriteriaTO.getPromoCriteriaID());
		promotionCriteria.setVersion(promotionCriteriaTO.getVersion());

		return promotionCriteria;
	}

	public static PromotionCriteria toStatusChangePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO)
			throws IllegalArgumentException {
		PromotionCriteria promotionCriteria = null;

		if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new BuyNGetPromotionCriteria();

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new DiscountPromotionCriteria();

		} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(promotionCriteriaTO
				.getPromotionCriteriaType())) {

			promotionCriteria = new FreeServicePromotionCriteria();

		} else {
			throw new IllegalArgumentException("Unsupported Promotion Type Detected");
		}

		promotionCriteria.setPromoCriteriaID(promotionCriteriaTO.getPromoCriteriaID());
		promotionCriteria.setVersion(promotionCriteriaTO.getVersion());
		promotionCriteria.setStatus(promotionCriteriaTO.getStatus());
		return promotionCriteria;
	}

	/**
	 * Sets the common values of the PromotionCriteria class hierarchy. In effect the fields defined in
	 * {@link PromotionCriteria}
	 * 
	 * @param promoCriteriaDTO
	 *            {@link PromotionCriteriaTO} with the data to be populated into {@link PromotionCriteria}
	 * 
	 * @param promotionCriteria
	 *            An instantiated subclass of {@link PromotionCriteria}
	 * 
	 * @return The passed instance of {@link PromotionCriteria} with the data from passed promoCriteriaDTO populated.
	 * @throws ParseException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	private static PromotionCriteria setCommonPromotionCriteriaValues(PromotionCriteriaTO promoCriteriaDTO,
			PromotionCriteria promotionCriteria) throws IllegalAccessException, InstantiationException, ParseException {

		promotionCriteria.setApplicableAgents(promoCriteriaDTO.getApplicableAgents());
		promotionCriteria.setApplicableBCs(promoCriteriaDTO.getApplicableBCs());
		promotionCriteria.setApplicableCOSs(promoCriteriaDTO.getApplicableCOSs());
		promotionCriteria.setApplicableLCCs(promoCriteriaDTO.getApplicableLCCs());
		promotionCriteria.setApplicableONDs(promoCriteriaDTO.getApplicableONDs());
		promotionCriteria.setApplicableSalesChannels(promoCriteriaDTO.getApplicableSalesChannels());
		promotionCriteria.setAppliedAmount(promoCriteriaDTO.getAppliedAmount());

		promotionCriteria.setFlightNOs(promoCriteriaDTO.getFlightNOs());
		promotionCriteria.setApplicablePosCountries(promoCriteriaDTO.getPosCountries());

		promotionCriteria.setPerFlightLimit(promoCriteriaDTO.getPerFlightLimit());
		Set<PromoCode> promoCodes = new HashSet<PromoCode>();
		if (!promoCriteriaDTO.getMultiCodeGenerated()) {

			if (!promoCriteriaDTO.getPromoCode().equals("")) {
				PromoCode promoCode = new PromoCode();
				promoCode.setPromoCriteria(promotionCriteria);
				promoCode.setPromoCode(promoCriteriaDTO.getPromoCode());
				promoCodes.add(promoCode);
				promotionCriteria.setPromoCode(promoCriteriaDTO.getPromoCode());
			}

		} else {
			if (promoCriteriaDTO.getEnablePromoCodes() != null) {
				for (String code : promoCriteriaDTO.getEnablePromoCodes()) {
					PromoCode promoCode = new PromoCode();
					promoCode.setPromoCode(code);
					promoCode.setPromoCriteria(promotionCriteria);
					promoCodes.add(promoCode);
				}
			}
		}
		if(promoCodes.size()!=0){
			promotionCriteria.setPromoCodes(promoCodes);
		}
		promotionCriteria.setPromoCriteriaID(promoCriteriaDTO.getPromoCriteriaID());
//		promotionCriteria.setPromoCode(promoCriteriaDTO.getPromoCode());
		promotionCriteria.setPromoCodeEnabled(promoCriteriaDTO.getPromoCodeEnabled());

		promotionCriteria.setRestrictAppliedResCancelation(promoCriteriaDTO.getRestrictAppliedResCancelation());
		promotionCriteria.setRestrictAppliedResModification(promoCriteriaDTO.getRestrictAppliedResModification());
		promotionCriteria.setRestrictAppliedResSplitting(promoCriteriaDTO.getRestrictAppliedResSplitting());
		promotionCriteria.setRestrictAppliedResRemovePax(promoCriteriaDTO.getRestrictAppliedResRemovePax());

		promotionCriteria.setStatus(promoCriteriaDTO.getStatus());

		promotionCriteria.setTotalLimit(promoCriteriaDTO.getTotalLimit());

		promotionCriteria.setVersion(promoCriteriaDTO.getVersion());

		promotionCriteria.setPromoName(promoCriteriaDTO.getPromoName());

		promotionCriteria.setApplicableForOneway(promoCriteriaDTO.getApplicableForOneway());

		promotionCriteria.setApplicableForReturn(promoCriteriaDTO.getApplicableForReturn());
		
		//applicable member selection
		promotionCriteria.setApplicableForLoyaltyMembers(promoCriteriaDTO.getApplicableForLoyaltyMembers());
		promotionCriteria.setApplicableForRegisteredMembers(promoCriteriaDTO.getApplicableForRegisteredMembers());
		promotionCriteria.setApplicableForGuests(promoCriteriaDTO.getApplicableForGuests());

		promotionCriteria.setNumByAgent(promoCriteriaDTO.getNumByAgent());
		promotionCriteria.setNumBySalesChannel(promoCriteriaDTO.getNumBySalesChannel());

		promotionCriteria.setApplicability(promoCriteriaDTO.getApplicability());

		promotionCriteria.setRegistrationPeriods(transformDatePeriodFromDTO(promoCriteriaDTO.getRegistrationPeriods(),
				RegistrationPeriod.class, promotionCriteria));
		promotionCriteria.setFlightPeriods(
				transformDatePeriodFromDTO(promoCriteriaDTO.getFlightPeriods(), FlightPeriod.class, promotionCriteria));

		return promotionCriteria;
	}

	/**
	 * Retrieves the discount details form the {@link PromotionCriteriaTO} and creates a new {@link DiscountDetails}
	 * object.
	 * 
	 * @param promotionCriteriaDTO
	 *            With the relevant data set.
	 * 
	 * @return The created {@link DiscountDetails}
	 */
	public static DiscountDetails transformDiscountDetailsFromDTO(PromotionCriteriaTO promotionCriteriaDTO) {
		DiscountDetails discountDetails = new DiscountDetails();

		discountDetails.setApplyAs(promotionCriteriaDTO.getApplyAs());
		discountDetails.setApplyTo(promotionCriteriaDTO.getApplyTO());
		discountDetails.setDiscountType(promotionCriteriaDTO.getDiscountType().toUpperCase());
		discountDetails.setDiscountValue(promotionCriteriaDTO.getDiscountValue());
		discountDetails.setConstPaxName(promotionCriteriaDTO.getConstPaxName());
		discountDetails.setConstPaxSector(promotionCriteriaDTO.getConstPaxSector());
		discountDetails.setConstFlightFrom(promotionCriteriaDTO.getConstFlightFrom());
		if (promotionCriteriaDTO.getConstFlightTo() != null) {
			discountDetails.setConstFlightTo(CalendarUtil.getEndTimeOfDate(promotionCriteriaDTO.getConstFlightTo()));
		}
		discountDetails.setConstValidFrom(promotionCriteriaDTO.getConstValidFrom());
		if (promotionCriteriaDTO.getConstValidTo() != null) {
			discountDetails.setConstValidTo(CalendarUtil.getEndTimeOfDate(promotionCriteriaDTO.getConstValidTo()));
		}

		return discountDetails;
	}

	/**
	 * Retrieves the discount details from the {@link DiscountDetails} and populates the {@link PromotionCriteriaTO}
	 * 
	 * @param discountDetails
	 *            With the relevant data to be populated
	 * 
	 * @param promotionCriteriaDTO
	 *            The {@link PromotionCriteriaTO} that need to be populated.
	 * 
	 * @return The passed {@link PromotionCriteriaTO} with the data populated.
	 */
	public static PromotionCriteriaTO transformDiscountDetailsToDTO(DiscountDetails discountDetails,
			PromotionCriteriaTO promotionCriteriaDTO) {

		// TODO RW : Remove the null check. This was a temporary null check due to the urgency of a Demo for Shameel. A
		// Discount should always be there for FreeServicePromotionCriteria and DiscountPromotionCriteria
		if (discountDetails != null) {
			promotionCriteriaDTO.setApplyAs(discountDetails.getApplyAs());
			promotionCriteriaDTO.setApplyTO(discountDetails.getApplyTo());
			promotionCriteriaDTO.setDiscountType(discountDetails.getDiscountType());
			promotionCriteriaDTO.setDiscountValue(discountDetails.getDiscountValue());
			promotionCriteriaDTO.setConstPaxName(discountDetails.getConstPaxName());
			promotionCriteriaDTO.setConstPaxSector(discountDetails.getConstPaxSector());
			promotionCriteriaDTO.setConstFlightFrom(discountDetails.getConstFlightFrom());
			promotionCriteriaDTO.setConstFlightTo(discountDetails.getConstFlightTo());
			promotionCriteriaDTO.setConstValidFrom(discountDetails.getConstValidFrom());
			promotionCriteriaDTO.setConstValidTo(discountDetails.getConstValidTo());

		}
		return promotionCriteriaDTO;
	}

	/**
	 * Parses the Buy and get quantity details from the {@link PromotionCriteriaTO}. And creates the
	 * {@link BuyNGetQuantity} objects.
	 * 
	 * @param buyNGetQuantityMap
	 *            The Map with the buy and get quantity details. Key -> Qualifying pax count. Value -> Free pax count.
	 *            Both Key and Value follows the same format. Adult Count,Child Count,Infant Count. If the
	 *            BuyNGetQuantity is Buy 1 Adult 2 Children and 0 Infant and get 0 Adult, 0 Children, 2 Infant then the
	 *            related entry in the Map will constitute to Key = 1,1,0 Value = 0,0,2
	 * 
	 * @return The {@link Set} of populated {@link BuyNGetQuantity} objects.
	 * 
	 * @throws IllegalArgumentException
	 *             If the specified format is incorrect.
	 */
	public static Set<BuyNGetQuantity> transformBuyNGetQuantityFromDTO(Map<String, String> buyNGetQuantityMap)
			throws IllegalArgumentException {
		Set<BuyNGetQuantity> qtyMap = new HashSet<BuyNGetQuantity>();
		for (Entry<String, String> entry : buyNGetQuantityMap.entrySet()) {

			String qualifyingPaxArr[] = entry.getKey().split(BUY_N_GET_QUANTITY_SEPERATOR);

			String freePaxArr[] = entry.getValue().split(BUY_N_GET_QUANTITY_SEPERATOR);

			if (qualifyingPaxArr.length != 3 || freePaxArr.length != 3) {
				throw new IllegalArgumentException(
						"Malformed BuyNGetQuantity entry. BuyNGetQuantity must be defined in Adult,Child,Infant format. I.E. : 1,0,0");
			}

			BuyNGetQuantity qtyEntry = new BuyNGetQuantity();
			
			qtyEntry.setQualifiedAdultCount(getBuyNGetPaxCount(qualifyingPaxArr[0].trim(),false));
			qtyEntry.setQualifiedChildCount(getBuyNGetPaxCount(qualifyingPaxArr[1].trim(),false));
			qtyEntry.setQualifiedInfantCount(getBuyNGetPaxCount(qualifyingPaxArr[2].trim(),false));
			
			qtyEntry.setFreeAdultCount(getBuyNGetPaxCount(freePaxArr[0].trim(),true));
			qtyEntry.setFreeChildCount(getBuyNGetPaxCount(freePaxArr[1].trim(),true));
			qtyEntry.setFreeInfantCount(getBuyNGetPaxCount(freePaxArr[2].trim(),true));

			qtyMap.add(qtyEntry);
		}
		return qtyMap;
	}

	/**
	 * Transforms a Set of {@link BuyNGetQuantity} to a {@link Map<String,String>} to be set to the
	 * {@link PromotionCriteriaTO}.
	 * 
	 * @param buyNGetQtySet
	 *            Set of {@link BuyNGetQuantity} to be transformed.
	 * 
	 * @return A Map<String,String> with the BuyNGetQuantity data. Format is Key -> Qualifying pax count. Value -> Free
	 *         pax count. Both Key and Value follows the same format. Adult Count,Child Count,Infant Count. If the
	 *         BuyNGetQuantity is Buy 1 Adult 2 Children and 0 Infant and get 0 Adult, 0 Children, 2 Infant then the
	 *         related entry in the Map will constitute to Key = 1,1,0 Value = 0,0,2
	 */
	public static Map<String, String> transformBuyNGetQuantityToDTO(Set<BuyNGetQuantity> buyNGetQtySet) {
		Map<String, String> buyNGetMap = new HashMap<String, String>();
		for (BuyNGetQuantity buyNGetQuantity : buyNGetQtySet) {
			String qualifyingQtyString = getBuyNGetPaxCount(buyNGetQuantity.getQualifiedAdultCount(),false)
					+ BUY_N_GET_QUANTITY_SEPERATOR + getBuyNGetPaxCount(buyNGetQuantity.getQualifiedChildCount(),false)
					+ BUY_N_GET_QUANTITY_SEPERATOR + getBuyNGetPaxCount(buyNGetQuantity.getQualifiedInfantCount(),false);

			String freeQtyString = getBuyNGetPaxCount(buyNGetQuantity.getFreeAdultCount(),true) + BUY_N_GET_QUANTITY_SEPERATOR
					+ getBuyNGetPaxCount(buyNGetQuantity.getFreeChildCount(),true) + BUY_N_GET_QUANTITY_SEPERATOR
					+ getBuyNGetPaxCount(buyNGetQuantity.getFreeInfantCount(),true);

			buyNGetMap.put(qualifyingQtyString, freeQtyString);
		}
		return buyNGetMap;
	}

	/**
	 * Transforms a subclass of {@link PromotionCriteria} to a {@link PromotionCriteriaTO}.
	 * 
	 * @param promoCriteria
	 *            A Subclass of {@link PromotionCriteria} to be transformed to the DTO.
	 * 
	 * @return {@link PromotionCriteriaTO} populated with the data form the PromotionCriteria.
	 * 
	 * @throws IllegalArgumentException
	 *             if the passed promotionCriteria is not a valid subclass. Currently supported sub classes are
	 *             {@link BuyNGetPromotionCriteria}, {@link FreeServicePromotionCriteria},
	 *             {@link DiscountPromotionCriteria}
	 */
	public static PromotionCriteriaTO toPromotionCriteriaTO(PromotionCriteria promoCriteria,PromoCodeTo promoCodeTo) throws IllegalArgumentException {
		PromotionCriteriaTO promoCriteriaDTO = new PromotionCriteriaTO();

		promoCriteriaDTO.setApplicableAgents(promoCriteria.getApplicableAgents());
		promoCriteriaDTO.setApplicableBCs(promoCriteria.getApplicableBCs());
		promoCriteriaDTO.setApplicableCOSs(promoCriteria.getApplicableCOSs());
		promoCriteriaDTO.setApplicableLCCs(promoCriteria.getApplicableLCCs());
		promoCriteriaDTO.setApplicableONDs(promoCriteria.getApplicableONDs());
		promoCriteriaDTO.setApplicableSalesChannels(promoCriteria.getApplicableSalesChannels());
		promoCriteriaDTO.setAppliedAmount(promoCriteria.getAppliedAmount());
		promoCriteriaDTO.setPosCountries(promoCriteria.getApplicablePosCountries());

		promoCriteriaDTO.setFlightNOs(promoCriteria.getFlightNOs());

		promoCriteriaDTO.setPerFlightLimit(promoCriteria.getPerFlightLimit());
		promoCriteriaDTO.setPromoCode(promoCriteria.getPromoCode());
		promoCriteriaDTO.setPromoCodeEnabled(promoCriteria.getPromoCodeEnabled());
		promoCriteriaDTO.setPromoCriteriaID(promoCriteria.getPromoCriteriaID());
		
		if (promoCodeTo!=null){
			promoCriteriaDTO.setPromoCodeTo(promoCodeTo);
		}

		promoCriteriaDTO.setRestrictAppliedResCancelation(promoCriteria.getRestrictAppliedResCancelation());
		promoCriteriaDTO.setRestrictAppliedResModification(promoCriteria.getRestrictAppliedResModification());
		promoCriteriaDTO.setRestrictAppliedResSplitting(promoCriteria.getRestrictAppliedResSplitting());
		promoCriteriaDTO.setRestrictAppliedResRemovePax(promoCriteria.getRestrictAppliedResRemovePax());

		promoCriteriaDTO.setStatus(promoCriteria.getStatus());

		promoCriteriaDTO.setTotalLimit(promoCriteria.getTotalLimit());
		promoCriteriaDTO.setNumBySalesChannel(promoCriteria.getNumBySalesChannel());
		promoCriteriaDTO.setNumByAgent(promoCriteria.getNumByAgent());

		promoCriteriaDTO.setVersion(promoCriteria.getVersion());
		promoCriteriaDTO.setPromoName(promoCriteria.getPromoName());
		promoCriteriaDTO.setApplicableForOneway(promoCriteria.getApplicableForOneway());
		promoCriteriaDTO.setApplicableForReturn(promoCriteria.getApplicableForReturn());
		//applicable member selection
		promoCriteriaDTO.setApplicableForLoyaltyMembers(promoCriteria.getApplicableForLoyaltyMembers());
		promoCriteriaDTO.setApplicableForRegisteredMembers(promoCriteria.getApplicableForRegisteredMembers());
		promoCriteriaDTO.setApplicableForGuests(promoCriteria.getApplicableForGuests());
		
		promoCriteriaDTO.setApplicability(promoCriteria.getApplicability());
		
		promoCriteriaDTO.setRegistrationPeriods(transformDatePeriodToDTO(promoCriteria.getRegistrationPeriods()));
		promoCriteriaDTO.setFlightPeriods(transformDatePeriodToDTO(promoCriteria.getFlightPeriods()));

		if (promoCriteria instanceof BuyNGetPromotionCriteria) {

			BuyNGetPromotionCriteria criteria = (BuyNGetPromotionCriteria) promoCriteria;
			promoCriteriaDTO.setLimitingLoadFactor(criteria.getLimitingLoadFactor());
			promoCriteriaDTO.setFreeCriteria(transformBuyNGetQuantityToDTO(criteria.getFreeCriteria()));

			promoCriteriaDTO
					.setPromotionCriteriaType(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA);
			promoCriteriaDTO.setApplyTO(criteria.getApplyTo());

		} else if (promoCriteria instanceof FreeServicePromotionCriteria) {

			FreeServicePromotionCriteria criteria = (FreeServicePromotionCriteria) promoCriteria;
			promoCriteriaDTO.setApplicableAncillaries(criteria.getApplicableAncillaries());

			promoCriteriaDTO = transformDiscountDetailsToDTO(criteria.getDiscount(), promoCriteriaDTO);

			promoCriteriaDTO
					.setPromotionCriteriaType(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA);

		} else if (promoCriteria instanceof DiscountPromotionCriteria) {

			DiscountPromotionCriteria criteria = (DiscountPromotionCriteria) promoCriteria;
			promoCriteriaDTO.setApplicableBINs(criteria.getApplicableBINs());
			promoCriteriaDTO = transformDiscountDetailsToDTO(criteria.getDiscount(), promoCriteriaDTO);

			promoCriteriaDTO.setPromotionCriteriaType(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA);

		} else if (promoCriteria instanceof SysGenPromotionCriteria) {

			SysGenPromotionCriteria criteria = (SysGenPromotionCriteria) promoCriteria;
			promoCriteriaDTO = transformDiscountDetailsToDTO(criteria.getDiscount(), promoCriteriaDTO);
			promoCriteriaDTO.setPromotionCriteriaType(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA);
			promoCriteriaDTO.setSystemGenerated(true);

		} else {
			throw new IllegalArgumentException("Invalid Subclass of PromotionCriteria detected.");
		}

		return promoCriteriaDTO;
	}

	private static Integer getBuyNGetPaxCount(String numberOfPax, boolean freeCount) {
		if (freeCount) {
			return numberOfPax.equalsIgnoreCase(PromotionCriteriaConstants.BUY_AND_GET_FREE_ALL) ? -1 : Integer
					.valueOf(numberOfPax);
		}
		return numberOfPax.equalsIgnoreCase(PromotionCriteriaConstants.BUY_AND_GET_FREE_ANY) ? -1 : Integer.valueOf(numberOfPax);
	}

	private static String getBuyNGetPaxCount(Integer paxCount,boolean freeCount) {
		if(freeCount){
			return paxCount.intValue() < 0 ? PromotionCriteriaConstants.BUY_AND_GET_FREE_ALL : paxCount.toString();
		}
		return paxCount.intValue() < 0 ? PromotionCriteriaConstants.BUY_AND_GET_FREE_ANY : paxCount.toString();
	}
	
	public static Collection<PromotionCriteriaTO>  prepareCriteriaDTOForMultiplePromoCodes (Page<PromotionCriteria> resultPage){
		
		Collection<PromotionCriteriaTO> transformedDTOs = new HashSet<PromotionCriteriaTO>();
		
		for (PromotionCriteria result : resultPage.getPageData()) {
			PromotionCriteriaTO criteriaTo = toPromotionCriteriaTO(result, null);
			if((result.getPromoCode()==null)&&(result.getPromoCodes().size() > 0)) {
				Set<PromoCodeTo> promoCodes = new HashSet<>();
				for(PromoCode code: result.getPromoCodes()) {
					PromoCodeTo codeTo = new PromoCodeTo();
					codeTo.setPromoCode(code.getPromoCode());
					codeTo.setFullyUtilized(code.isFullyUtilized());
					promoCodes.add(codeTo);
				}
				criteriaTo.setPromoCodesTo(promoCodes);
			}
			transformedDTOs.add(criteriaTo);
		}
		return transformedDTOs;
	}
	
	private static <T extends DatePeriod> Set<T> transformDatePeriodFromDTO(Set<String> datePeriodSet, Class<T> clazz,
			PromotionCriteria promotionCriteria) throws ParseException, IllegalAccessException, InstantiationException {
		Set<T> datePeriods = new HashSet<>();
		SimpleDateFormat dateFormatForDB = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		for (String date : datePeriodSet) {
			T datePeriod = clazz.newInstance();
			String[] dates = date.split(DATE_PERIOD_SEPERATOR);
			datePeriod.setFromDate(dateFormatForDB.parse(dates[0] + " 00:00:00"));
			datePeriod.setToDate(dateFormatForDB.parse(dates[1] + " 23:59:59"));
			datePeriod.setPromotionCriteria(promotionCriteria);
			datePeriods.add(datePeriod);
		}

		return datePeriods;
	}

	private static <T extends DatePeriod> Set<String> transformDatePeriodToDTO(Set<T> datePeriodSet) {
		Set<String> datePeriods = new HashSet<>();
		SimpleDateFormat dateFormatForTO = new SimpleDateFormat("dd/MM/yyyy");
		for (T dateObj : datePeriodSet) {
			if ((dateObj.getFromDate() == null) && (dateObj.getToDate() == null)) {
				continue;
			}
			String from = (dateObj.getFromDate() != null) ? dateFormatForTO.format(dateObj.getFromDate()) : "";
			String to = (dateObj.getToDate() != null) ? dateFormatForTO.format(dateObj.getToDate()) : "";
			datePeriods.add(from + DATE_PERIOD_SEPERATOR + to);
		}
		return datePeriods;
	}
	
}
