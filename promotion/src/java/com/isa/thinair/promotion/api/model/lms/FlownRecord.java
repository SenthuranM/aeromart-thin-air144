package com.isa.thinair.promotion.api.model.lms;

import com.isa.thinair.promotion.api.model.lms.BasicDataType.DATA_F;

/**
 * 
 * @author rumesh
 * 
 */
public class FlownRecord extends OrderedElementContainer {

	public String getMemberAccountId() {
		return getElementData(RECORD_COLUMN.MEMBER_ACCOUNT_ID.ordinal());
	}

	public void setMemberAccountId(String memberAccountId, DATA_F dataF) {
		addElement(memberAccountId, dataF);
	}

	public String getTitle() {
		return getElementData(RECORD_COLUMN.TITLE.ordinal());
	}

	public void setTitle(String title, DATA_F dataF) {
		addElement(title, dataF);
	}

	public String getLastName() {
		return getElementData(RECORD_COLUMN.LAST_NAME.ordinal());
	}

	public void setLastName(String lastName, DATA_F dataF) {
		addElement(lastName, dataF);
	}

	public String getFirstName() {
		return getElementData(RECORD_COLUMN.FIRST_NAME.ordinal());
	}

	public void setFirstName(String firstName, DATA_F dataF) {
		addElement(firstName, dataF);
	}

	public String getPassengerType() {
		return getElementData(RECORD_COLUMN.PAX_TYPE.ordinal());
	}

	public void setPassengerType(String paxType, DATA_F dataF) {
		addElement(paxType, dataF);
	}

	public String getDOB() {
		return getElementData(RECORD_COLUMN.DATE_OF_BIRTH.ordinal());
	}

	public void setDOB(String dob, DATA_F dataF) {
		addElement(dob, dataF);
	}

	public String getNationality() {
		return getElementData(RECORD_COLUMN.NATIONALITY.ordinal());
	}

	public void setNationality(String nationality, DATA_F dataF) {
		addElement(nationality, dataF);
	}

	public String getOperatingAirlineCode() {
		return getElementData(RECORD_COLUMN.OPERATING_AIRLINE.ordinal());
	}

	public void setOperatingAirlineCode(String operatingAirline, DATA_F dataF) {
		addElement(operatingAirline, dataF);
	}

	public String getOperatingAirlineFlight() {
		return getElementData(RECORD_COLUMN.OPERATING_FLT_NUMBER.ordinal());
	}

	public void setOperatingAirlineFlight(String operatingAirlineFlight, DATA_F dataF) {
		addElement(operatingAirlineFlight, dataF);
	}

	public String getMarktingAirlineCode() {
		return getElementData(RECORD_COLUMN.MKRT_AIRLINE.ordinal());
	}

	public void setMarktingAirlineCode(String marketingAirlineCode, DATA_F dataF) {
		addElement(marketingAirlineCode, dataF);
	}

	public String getMarketingAirlineFlight() {
		return getElementData(RECORD_COLUMN.MKRT_FLT_NUMBER.ordinal());
	}

	public void setMarketingAirlineFlight(String marketingAirlineFlight, DATA_F dataF) {
		addElement(marketingAirlineFlight, dataF);
	}

	public String getFlightDate() {
		return getElementData(RECORD_COLUMN.FLT_DATE.ordinal());
	}

	public void setFlightDate(String flightDate, DATA_F dataF) {
		addElement(flightDate, dataF);
	}

	public String getFlightTime() {
		return getElementData(RECORD_COLUMN.FLT_TIME.ordinal());
	}

	public void setFlightTime(String flightTime, DATA_F dataF) {
		addElement(flightTime, dataF);
	}

	public String getDepartureAirport() {
		return getElementData(RECORD_COLUMN.DEPT_AIRPORT.ordinal());
	}

	public void setDepartureAirport(String departureAirport, DATA_F dataF) {
		addElement(departureAirport, dataF);
	}

	public String getArrivalAirport() {
		return getElementData(RECORD_COLUMN.ARR_AIRPORT.ordinal());
	}

	public void setArrivalAirport(String arrivalAirport, DATA_F dataF) {
		addElement(arrivalAirport, dataF);
	}

	public String getCabinClassCode() {
		return getElementData(RECORD_COLUMN.CABIN_CLASS.ordinal());
	}

	public void setCabinClassCode(String cabinClassCode, DATA_F dataF) {
		addElement(cabinClassCode, dataF);
	}

	public String getBookingClassCode() {
		return getElementData(RECORD_COLUMN.BOOKING_CLASS.ordinal());
	}

	public void setBookingClassCode(String bookingClassCode, DATA_F dataF) {
		addElement(bookingClassCode, dataF);
	}

	public String getPnr() {
		return getElementData(RECORD_COLUMN.PNR.ordinal());
	}

	public void setPnr(String pnr, DATA_F dataF) {
		addElement(pnr, dataF);
	}

	public String getTicketIssuanceDate() {
		return getElementData(RECORD_COLUMN.TKT_ISSUANCE_DATE.ordinal());
	}

	public void setTicketIssuanceDate(String ticketIssuanceDate, DATA_F dataF) {
		addElement(ticketIssuanceDate, dataF);
	}

	public String getTicketNumber() {
		return getElementData(RECORD_COLUMN.TKT_NUMBER.ordinal());
	}

	public void setTicketNumber(String ticketNumber, DATA_F dataF) {
		addElement(ticketNumber, dataF);
	}

	public String getTicketCouponNumber() {
		return getElementData(RECORD_COLUMN.TKT_COUPON_NUMBER.ordinal());
	}

	public void setTicketCouponNumber(String ticketCouponNumber, DATA_F dataF) {
		addElement(ticketCouponNumber, dataF);
	}

	public String getBookingAgentInitials() {
		return getElementData(RECORD_COLUMN.BOOKING_AGENT_CODE.ordinal());
	}

	public void setBookingAgentInitials(String agentInitials, DATA_F dataF) {
		addElement(agentInitials, dataF);
	}

	public String getContactNumber() {
		return getElementData(RECORD_COLUMN.CONTACT_NUMBER.ordinal());
	}

	public void setContactNumber(String contactNumber, DATA_F dataF) {
		addElement(contactNumber, dataF);
	}

	public String getContactEmail() {
		return getElementData(RECORD_COLUMN.CONTACT_EMAIL.ordinal());
	}

	public void setContactEmail(String contactEmail, DATA_F dataF) {
		addElement(contactEmail, dataF);
	}

	public String getCountryOfResidnece() {
		return getElementData(RECORD_COLUMN.RESIDENCY_CODE.ordinal());
	}

	public void setCountryOfResidnece(String countryOfResidence, DATA_F dataF) {
		addElement(countryOfResidence, dataF);
	}

	public String getSalesChannel() {
		return getElementData(RECORD_COLUMN.SALES_CHANNEL.ordinal());
	}

	public void setSalesChannel(String salesChannel, DATA_F dataF) {
		addElement(salesChannel, dataF);
	}

	public String getPassportNumber() {
		return getElementData(RECORD_COLUMN.PASSPORT_NUMER.ordinal());
	}

	public void setPassportNumber(String passportNumber, DATA_F dataF) {
		addElement(passportNumber, dataF);
	}

	public String getProductList() {
		return getElementData(RECORD_COLUMN.PRODUCT_LIST.ordinal());
	}

	public void setProductList(String productList, DATA_F dataF) {
		addElement(productList, dataF);
	}

	private enum RECORD_COLUMN {
		MEMBER_ACCOUNT_ID, TITLE, LAST_NAME, FIRST_NAME, PAX_TYPE, DATE_OF_BIRTH, NATIONALITY, OPERATING_AIRLINE, OPERATING_FLT_NUMBER, MKRT_AIRLINE, MKRT_FLT_NUMBER, FLT_DATE, FLT_TIME, DEPT_AIRPORT, ARR_AIRPORT, CABIN_CLASS, BOOKING_CLASS, PNR, TKT_ISSUANCE_DATE, TKT_NUMBER, TKT_COUPON_NUMBER, BOOKING_AGENT_CODE, CONTACT_NUMBER, CONTACT_EMAIL, RESIDENCY_CODE, SALES_CHANNEL, PASSPORT_NUMER, PRODUCT_LIST
	}
}
