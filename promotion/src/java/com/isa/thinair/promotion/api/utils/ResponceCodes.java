package com.isa.thinair.promotion.api.utils;

public interface ResponceCodes {

	String PROMOTION_SUBSRIBED = "promotion.subsribed";
	String PROMOTION_SUBSCRIBE_REPEAT = "promotion.subsribe.repeat";
	String PROMOTION_SUBSCRIBE_MULTIPLE = "promotion.subscribe.multiple";
	String PROMOTION_UNSUBSCRIBE_PROCESSED_REPEAT = "promotion.unsubscribe.processed.repeat";
	String PROMOTION_ONDS_OVERLAPPED = "promotion.ond.overlap";
	String PROMOTION_TEMPLATE_NOT_ACTIVE = "promotion.template.not.active";
	String PROMOTION_EDIT_FAILED_ACT_REQS = "promotion.edit.failed.active.requests";
	String PROMOTION_REQUEST_OUT_OF_VALIDITY = "promotion.filtered.empty.routes";

	public interface ResponseParams {
		public static final String AVAILABLE_POINTS = "AVAILABLE_POINTS";
		public static final String AVAILABLE_POINTS_AMOUNT = "AVAILABLE_POINTS_AMOUNT";
		public static final String PAX_PRODUCT_POINTS = "PAX_PRODUCT_POINTS";
		public static final String PAX_PRODUCT_AMOUNT = "PAX_PRODUCT_AMOUNT";
		public static final String LOYALTY_REWARD_IDS = "LOYALTY_REWARD_IDS";
		public static final String CARRIER_LOYALTY_PAYMENTS = "CARRIER_LOYALTY_PAYMENTS";
		public static final String SUCCESS = "SUCCESS";
	}
}