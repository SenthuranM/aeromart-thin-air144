package com.isa.thinair.promotion.api.model;

import java.io.Serializable;
import java.util.Set;

/**
 * Hold loyalty product information
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "t_loyalty_product"
 */
public class LoyaltyProduct implements Serializable, Comparable<LoyaltyProduct> {

	private static final long serialVersionUID = -2607116510379109182L;

	private String productId;

	private String productName;

	private Set<String> chargeCodes;

	private Integer redeemRank;

	private Double thresholdValue;

	private boolean externalCharge;

	/**
	 * @return the productId
	 * @hibernate.id column = "LOYALTY_PRODUCT_CODE" generator-class = "assigned"
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the productName
	 * @hibernate.property column = "LOYALTY_PRODUCT_NAME"
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the chargeCodes
	 * 
	 * @hibernate.set lazy="false" table="t_loyalty_product_charge"
	 * @hibernate.collection-element column="CHARGE_CODE" type="string"
	 * @hibernate.collection-key column="LOYALTY_PRODUCT_CODE"
	 */
	public Set<String> getChargeCodes() {
		return chargeCodes;
	}

	/**
	 * @param chargeCodes
	 *            the chargeCodes to set
	 */
	public void setChargeCodes(Set<String> chargeCodes) {
		this.chargeCodes = chargeCodes;
	}

	/**
	 * @return the redeemRank
	 * @hibernate.property column = "REDEEM_RANK"
	 */
	public Integer getRedeemRank() {
		return redeemRank;
	}

	/**
	 * @param redeemRank
	 *            the redeemRank to set
	 */
	public void setRedeemRank(Integer redeemRank) {
		this.redeemRank = redeemRank;
	}

	/**
	 * @return the thresholdValue
	 * @hibernate.property column = "THRESHOLD_VALUE"
	 */
	public Double getThresholdValue() {
		return thresholdValue;
	}

	/**
	 * @param thresholdValue
	 *            the thresholdValue to set
	 */
	public void setThresholdValue(Double thresholdValue) {
		this.thresholdValue = thresholdValue;
	}

	/**
	 * @return the externalCharge
	 * @hibernate.property column = "EXTERNAL_CHARGE" type = "yes_no"
	 */
	public boolean isExternalCharge() {
		return externalCharge;
	}

	/**
	 * @param externalCharge
	 *            the externalCharge to set
	 */
	public void setExternalCharge(boolean externalCharge) {
		this.externalCharge = externalCharge;
	}

	@Override
	public int compareTo(LoyaltyProduct o) {
		return this.redeemRank.compareTo(o.getRedeemRank());
	}

}
