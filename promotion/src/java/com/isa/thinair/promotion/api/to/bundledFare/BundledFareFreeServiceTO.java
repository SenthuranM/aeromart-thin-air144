package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;
import java.util.Set;

public class BundledFareFreeServiceTO implements Serializable {

	private static final long serialVersionUID = 3124184458948724918L;

	private Integer id;

	private String serviceName;

	private Integer templateId;

	private Set<Integer> includedFreeServices;

	private boolean allowMultipleSelect;

	private Long version;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the templateId
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the includedFreeServices
	 */
	public Set<Integer> getIncludedFreeServices() {
		return includedFreeServices;
	}

	/**
	 * @param includedFreeServices
	 *            the includedFreeServices to set
	 */
	public void setIncludedFreeServices(Set<Integer> includedFreeServices) {
		this.includedFreeServices = includedFreeServices;
	}

	/**
	 * @return the allowMultipleSelect
	 */
	public boolean isAllowMultipleSelect() {
		return allowMultipleSelect;
	}

	/**
	 * @param allowMultipleSelect
	 *            the allowMultipleSelect to set
	 */
	public void setAllowMultipleSelect(boolean allowMultipleSelect) {
		this.allowMultipleSelect = allowMultipleSelect;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	public String toString() {
		return "{" +
				"serviceName=" + serviceName +
				", id=" + id +
				", templateId=" + templateId +
				", includedFreeServices=" + (includedFreeServices != null ? includedFreeServices.toString() : "NULL") +
				'}';
	}
}
