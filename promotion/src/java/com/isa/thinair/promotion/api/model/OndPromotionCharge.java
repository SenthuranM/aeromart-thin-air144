package com.isa.thinair.promotion.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_OND_PROMOTION_CHARGE" lazy="false"
 */
public class OndPromotionCharge extends Tracking {

	private Long ondPromoChargeId;
	private OndPromotion ondPromotion;
	private Integer chargeRate;
	private Double amount;
	private String chargeCode;
	private Byte rewardFlag;
	private String status;

	/**
	 * @hibernate.id column = "OND_PROMOTION_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_OND_PROMOTION_CHARGE"
	 */
	public Long getOndPromoChargeId() {
		return ondPromoChargeId;
	}

	public void setOndPromoChargeId(Long ondPromoChargeId) {
		this.ondPromoChargeId = ondPromoChargeId;
	}

	/**
	 * @hibernate.many-to-one column="OND_PROMOTION_ID" cascade="all"
	 *                        class="com.isa.thinair.promotion.api.model.OndPromotion"
	 */
	public OndPromotion getOndPromotion() {
		return ondPromotion;
	}

	public void setOndPromotion(OndPromotion ondPromotion) {
		this.ondPromotion = ondPromotion;
	}

	/**
	 * @hibernate.property column = "CHARGE_RATE_ID"
	 */
	public Integer getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(Integer chargeRate) {
		this.chargeRate = chargeRate;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "PROMOTION_CHARGE_CODE"
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @hibernate.property column = "REWARD_FLAG"
	 */
	public Byte getRewardFlag() {
		return rewardFlag;
	}

	public void setRewardFlag(Byte rewardFlag) {
		this.rewardFlag = rewardFlag;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getChargeCode()).toHashCode();
	}

	public boolean equals(Object obj) {
		OndPromotionCharge other;
		if (obj != null && obj instanceof OndPromotion) {
			other = (OndPromotionCharge) obj;
			return getChargeCode() != null && other.getChargeCode() != null && getChargeCode().equals(other.getChargeCode());
		}
		return false;
	}
}
