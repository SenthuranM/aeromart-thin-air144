package com.isa.thinair.promotion.api.to.bundledFare;

import java.io.Serializable;

public class BundledFareSearchCriteriaTO implements Serializable {

	private static final long serialVersionUID = 154733834199945873L;

	private String bundledFareName;

	private String bundledCategoryID;

	private String ondCode;

	private String bookingClass;

	private String status;

	public String getBundledFareName() {
		return bundledFareName;
	}

	public void setBundledFareName(String bundledFareName) {
		this.bundledFareName = bundledFareName;
	}

	public String getBundledCategoryID() {
		return bundledCategoryID;
	}

	public void setBundledCategoryID(String bundledCategoryID) {
		this.bundledCategoryID = bundledCategoryID;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
