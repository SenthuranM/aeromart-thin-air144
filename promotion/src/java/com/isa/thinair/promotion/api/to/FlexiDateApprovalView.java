package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FlexiDateApprovalView implements Serializable {

	private String pnr;
	private Integer passengerCount;
	private String flightNumber;
	private Date departureDate;
	private Double revenue;
	private Long versionId;
	private Integer resSegId;
	private Integer flightSegId;
	private String flightSegCode;
	private int promotionId;
	private int promotionReqId;
	private Double transferedSegmentsFare;
	private Double rewards;
	private Double currentSellingFare;
	private List<String> approvingSegs;
	private List<Map<String, String>> segmentTransferData;
	private boolean transferExists = true;

	public FlexiDateApprovalView() {
		approvingSegs = new ArrayList<String>();
		segmentTransferData = new ArrayList<Map<String, String>>();
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Double getRevenue() {
		return revenue;
	}

	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	public Integer getResSegId() {
		return resSegId;
	}

	public void setResSegId(Integer resSegId) {
		this.resSegId = resSegId;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getFlightSegCode() {
		return flightSegCode;
	}

	public void setFlightSegCode(String flightSegCode) {
		this.flightSegCode = flightSegCode;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public int getPromotionReqId() {
		return promotionReqId;
	}

	public void setPromotionReqId(int promotionReqId) {
		this.promotionReqId = promotionReqId;
	}

	public Double getTransferedSegmentsFare() {
		return transferedSegmentsFare;
	}

	public void setTransferedSegmentsFare(Double transferedSegmentsFare) {
		this.transferedSegmentsFare = transferedSegmentsFare;
	}

	public Double getRewards() {
		return rewards;
	}

	public void setRewards(Double rewards) {
		this.rewards = rewards;
	}

	public Double getCurrentSellingFare() {
		return currentSellingFare;
	}

	public void setCurrentSellingFare(Double currentSellingFare) {
		this.currentSellingFare = currentSellingFare;
	}

	public List<String> getApprovingSegs() {
		return approvingSegs;
	}

	public void setApprovingSegs(List<String> approvingSegs) {
		this.approvingSegs = approvingSegs;
	}

	public void addApprovingSegs(String approvingSegs) {
		this.approvingSegs.add(approvingSegs);
	}
	
	public List<Map<String, String>> getSegmentTransferData() {
		return segmentTransferData;
	}

	public void setSegmentTransferData(List<Map<String, String>> segmentTransferData) {
		this.segmentTransferData = segmentTransferData;
	}

	public void addSegmentTransferData(Map<String, String> segmentTransferData) {
		this.segmentTransferData.add(segmentTransferData);
	}

	public boolean isTransferExists() {
		return transferExists;
	}

	public void setTransferExists(boolean transferExists) {
		this.transferExists = transferExists;
	}

}
