package com.isa.thinair.promotion.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "T_BUNDLED_FARE_CATEGORY"
 */
public class BundledFareCategory extends Tracking {

	private static final long serialVersionUID = 7776456365403090318L;

	public static final String DELETED = "DEL";

	private Integer bundledFareCategoryId;

	private String status;

	private Integer priority;

	/**
	 * @hibernate.id column = "BUNDLED_FARE_CATEGORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BUNDLED_FARE_CATEGORY"
	 */
	public Integer getBundledFareCategoryId() {
		return bundledFareCategoryId;
	}

	public void setBundledFareCategoryId(Integer bundledFareCategoryId) {
		this.bundledFareCategoryId = bundledFareCategoryId;
	}

	/**
	 * @hibernate.property column = "PRIORITY"
	 */
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
