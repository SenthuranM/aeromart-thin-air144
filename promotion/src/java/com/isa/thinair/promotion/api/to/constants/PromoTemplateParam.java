package com.isa.thinair.promotion.api.to.constants;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public interface PromoTemplateParam {

	interface TemplateConfig {
		String FLEXI_DATE_LOWER_BOUND = "FLEXI_LOWER";
		String FLEXI_DATE_UPPER_BOUND = "FLEXI_UPPER";

		String NEXT_SEAT_FREE_SEATS_MIN = "FREE_SEATS_MIN";
		String NEXT_SEAT_FREE_SEATS_MAX = "FREE_SEATS_MAX";
		String NEXT_SEAT_FREE_MIN_VACANT_SEATS = "MIN_VACANT_SEATS";
		String NEXT_SEAT_FREE_PRESERVE_SEATS = "PRESERVE_SEATS";
	}

	enum ChargeAndReward {

		NEXT_SEAT_FREE_SEAT_CHRG {
			public String getFreeSeatChargeParam(int seats) {
				return getPrefix() + seats;
			}

			public String getPrefix() {
				return "NEXT_SEAT_FREE_CHRG_SEATS_";
			}

			public String getChargeCode() {
				return "AS";
			}

			public boolean isRefundable() {
				return true;
			}

			public boolean isRefundToCC() {
				return true;
			}

			public String getChargeDescription() {
				return "Seat Allocation Charge For Next-Seat-Free Promo";
			}
			
			public String getRefundDescription() {
				return "Refund For Next-Seat-Free Promo";
			}

		},
		NEXT_SEAT_FREE_REG_CHRG {
			public String getPrefix() {
				return "NEXT_SEAT_FREE_REG_CHRG_";
			}

			public String getChargeCode() {
				return "SR";
			}

			public String getChargeDescription() {
				return "Registration Charge For Next-Seat-Free Promo";
			}



			public String getFreeSeatRegistrationChargeParam(int seats) {
				return getPrefix() + seats;
			}
			
			public String getRefundDescription() {
				return "Registration Refund For Next-Seat-Free Promo";
			}
		},
		FLEXI_DATE_DAYS_REWARD {
			public String getPrefix() {
				return "FLEXI_DATE_REWARD_DAYS_";
			}

			public String getFlexibilityRewardParam(int days) {
				return getPrefix() + days;
			}

			public byte isReward() {
				return IS_REWARD;
			}

			public String getChargeCode() {
				return "PR";
			}

			public String getChargeDescription() {
				return "Flexibility Reward For Flexi-Date Promo";
			}
			
			public String getRefundDescription() {
				return "Flexibility Refund For Flexi-Date Promo";
			}
		};

		public static final byte IS_CHARGE = 0;
		public static final byte IS_REWARD = 1;

		public String getPrefix() {
			return null;
		}

		public String getCode() {
			return null;
		}

		public abstract String getChargeCode();

		public abstract String getChargeDescription();
		
		public abstract String getRefundDescription();

		public String getFreeSeatChargeParam(int seats) {
			return null;
		}

		public String getFreeSeatRegistrationChargeParam(int seats) {
			return null;
		}

		public String getFlexibilityRewardParam(int days) {
			return null;
		}

		public byte isReward() {
			return IS_CHARGE;
		}

		public boolean isRefundable() {
			return false;
		}

		public boolean isRefundToCC() {
			return false;
		}

		public static byte getRewardFlagByCode(String code) {
			for (ChargeAndReward chargeAndReward : ChargeAndReward.values()) {
				if (chargeAndReward.getPrefix() != null && code.indexOf(chargeAndReward.getPrefix()) == 0) {
					return chargeAndReward.isReward();
				}
			}
			return IS_CHARGE;
		}

		public static boolean getRefundabilityByCode(String code) {
			for (ChargeAndReward chargeAndReward : ChargeAndReward.values()) {
				if (chargeAndReward.getPrefix() != null && code.indexOf(chargeAndReward.getPrefix()) == 0) {
					return chargeAndReward.isRefundable();
				}
			}
			return false;
		}

		public static String resolveChargeCode(String promotionChargeCode) {
			String chargeCode = null;

			for (ChargeAndReward chargeAndReward : ChargeAndReward.values()) {

				if (chargeAndReward.getPrefix() != null && promotionChargeCode.indexOf(chargeAndReward.getPrefix()) == 0) {
					chargeCode = chargeAndReward.getChargeCode();
					break;
				} else if (chargeAndReward.getCode() != null && chargeAndReward.getCode().equals(promotionChargeCode)) {
					chargeCode = chargeAndReward.getChargeCode();
					break;
				}
			}

			return chargeCode;
		}

		public static ChargeAndReward resolveChargeAndRewardByCode(String promotionChargeCode) {
			for (ChargeAndReward chargeAndReward : ChargeAndReward.values()) {

				if ((chargeAndReward.getPrefix() != null && promotionChargeCode.indexOf(chargeAndReward.getPrefix()) == 0)
						|| (chargeAndReward.getCode() != null && chargeAndReward.getCode().equals(promotionChargeCode))) {
					return chargeAndReward;
				}
			}
			return null;
		}

		public static String
				getFreeSeatRegistraionCharge(String paxName, String ondCode, BigDecimal amount, boolean isRefundable) {
			StringBuilder sb = new StringBuilder();
			sb.append("Passenger ").append(paxName).append(" subscribed for Next Seat Promotion on ").append(ondCode)
					.append(" for ");
			if (isRefundable) {
				sb.append(" Refundable ");
			} else {
				sb.append(" Non Refundable ");
			}
			sb.append(amount.toString()).append(" ").append(AppSysParamsUtil.getBaseCurrency());
			return sb.toString();
		}

		public static String getFreeSeatRefundCharge(String paxName, String ondCode, BigDecimal amount) {

			StringBuilder sb = new StringBuilder();
			sb.append("Next Seat Free request rejected:Passenger ").append(paxName).append(" refunded for ").append(ondCode)
					.append(" ").append(amount.toString()).append(" ").append(AppSysParamsUtil.getBaseCurrency());
			return sb.toString();
		}

		public static String getFlexiDateRewardCharge(String paxName, String ondCode, BigDecimal amount) {
			StringBuilder sb = new StringBuilder();
			sb.append("Flexi Date Promotion request Approved:Passenger ").append(paxName).append("Rewarded for ").append(ondCode)
					.append(" ").append(amount).append(" ")
					.append(AppSysParamsUtil.getBaseCurrency());
			return sb.toString();
		}

		public static String getFlexiDateRegistrationUserNote(String lowerBound, String upperBound, String ondCode) {
			StringBuilder sb = new StringBuilder();
			sb.append("Passenger(s) subcribed for Flexi Date Promotion for ").append(ondCode).append(" with ");
			sb.append("Lower Bound ").append(lowerBound).append(" Days and Upper Bound ").append(upperBound).append(" Days.");
			return sb.toString();
		}
	}
}
