package com.isa.thinair.promotion.api.to;

import java.io.Serializable;
import java.util.Date;

public class PromoCriteriaSearchTO implements Serializable {

	private static final long serialVersionUID = 1223111122231L;

	private String OND;
	private Date reservationFrom = null;
	private Date reservationTo = null;
	private String promotionCode;
	private String status;

	public String getOND() {
		return OND;
	}

	public void setOND(String oND) {
		if (oND != null && (oND.isEmpty() || oND.equalsIgnoreCase("null"))) {
			OND = null;
		} else {
			OND = oND;
		}
	}

	public Date getReservationFrom() {
		return reservationFrom;
	}

	public void setReservationFrom(Date reservationFrom) {
		this.reservationFrom = reservationFrom;
	}

	public Date getReservationTo() {
		return reservationTo;
	}

	public void setReservationTo(Date reservationTo) {
		this.reservationTo = reservationTo;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		if (promotionCode != null && (promotionCode.isEmpty() || promotionCode.equalsIgnoreCase("null"))) {
			this.promotionCode = null;
		} else {
			this.promotionCode = promotionCode;
		}

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if (status != null && (status.isEmpty() || status.equalsIgnoreCase("null"))) {
			this.status = null;
		} else {
			this.status = status;
		}
	}
}
