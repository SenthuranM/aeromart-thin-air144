package com.isa.thinair.promotion.core.bl.common;

import java.util.List;

import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.core.persistence.dao.NextSeatFreePromotionAdminDao;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

public abstract class ApprovePromoRequestOnFlightDeparture extends PromotionApprover {

	private int flightSegId;

	public ServiceResponce execute() throws ModuleException {
		FlightBD flightBD = PromotionModuleServiceLocator.getFlightServiceBD();
		List<Integer> flightSegIds = getFlightSegmentIds();

		for (Integer flightSegId : flightSegIds) {
			this.flightSegId = flightSegId;
			setParameter(CommandParamNames.FLIGHT_SEGMENT, flightBD.getFlightSegment(flightSegId));
			super.execute();
		}

		return new DefaultServiceResponse(true);
	}

	public List<Integer> getFlightSegmentIds() {

		PromotionType promotionType = (PromotionType) getParameter(CommandParamNames.PROMOTION_TYPE);

		NextSeatFreePromotionAdminDao adminDao = (NextSeatFreePromotionAdminDao) PromotionsHandlerFactory
				.getPromotionAdministrationDao(promotionType);

		return adminDao.getFlightSegIdsForScheduler();
	}

	public int getFlightSegId() {
		return flightSegId;
	}

}
