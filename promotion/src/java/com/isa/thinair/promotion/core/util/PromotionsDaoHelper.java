package com.isa.thinair.promotion.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.dto.ReservationSegSummaryTo;
import com.isa.thinair.promotion.core.dto.ReservationSummaryTo;
import com.isa.thinair.promotion.core.dto.TransferFlightTo;
import com.isa.thinair.promotion.core.dto.nextseat.NextSeatFreePromoResTo;

public abstract class PromotionsDaoHelper {

	public static AvailableFlightSearchDTO createAvailableFlightSearchDTO(ReservationSummaryTo reservationSummaryTo, UserPrincipal userPrincipal) {
		AvailableFlightSearchDTO availFlightSearchDTO = new AvailableFlightSearchDTO();
		String origin = null;
		String dest = null;
		ReservationSegSummaryTo resSegSummaryTo = null;
		boolean isConnection = false;

		if (reservationSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_OUTBOUND) {
			origin = PromotionsUtils.getOriginAndDestinationAirports(
					reservationSummaryTo.getOutboundSegs().get(0).getSegmentCode()).getLeft();
			dest = PromotionsUtils.getOriginAndDestinationAirports(
					reservationSummaryTo.getOutboundSegs().get(reservationSummaryTo.getOutboundSegs().size() - 1)
							.getSegmentCode()).getRight();
			resSegSummaryTo = reservationSummaryTo.getOutboundSegs().get(0);
			isConnection = reservationSummaryTo.getOutboundSegs().size() > 1;

		} else if (reservationSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_INBOUND) {
			origin = PromotionsUtils.getOriginAndDestinationAirports(
					reservationSummaryTo.getInboundSegs().get(0).getSegmentCode()).getLeft();
			dest = PromotionsUtils.getOriginAndDestinationAirports(
					reservationSummaryTo.getInboundSegs().get(reservationSummaryTo.getInboundSegs().size() - 1).getSegmentCode())
					.getRight();
			resSegSummaryTo = reservationSummaryTo.getInboundSegs().get(0);
			isConnection = reservationSummaryTo.getInboundSegs().size() > 1;
		}

		int dateVarienceLower = Integer.parseInt(resSegSummaryTo.getPromoRequestParams()
				.get(PromoRequestParam.FlexiDate.FLEXI_DATE_LOWER_BOUND).toString());
		int dateVarienceUpper = Integer.parseInt(resSegSummaryTo.getPromoRequestParams()
				.get(PromoRequestParam.FlexiDate.FLEXI_DATE_UPPER_BOUND).toString());

		Date depDate = null;
		Date retDate = null;
		int stay;
		if (!reservationSummaryTo.getInboundSegs().isEmpty()) {
			depDate = reservationSummaryTo.getOutboundSegs().get(reservationSummaryTo.getOutboundSegs().size() - 1)
					.getDepatureDate();
			retDate = reservationSummaryTo.getInboundSegs().get(0).getDepatureDate();
			stay = CalendarUtil.getTimeDifferenceInDays(depDate, retDate);

			if (resSegSummaryTo.isInbound()) {
				dateVarienceLower = dateVarienceLower > stay ? stay : dateVarienceLower;
			} else {
				dateVarienceUpper = dateVarienceUpper > stay ? stay : dateVarienceUpper;
			}
		}

		Date departureStart = CalendarUtil.addDateVarience(resSegSummaryTo.getDepatureDate(), -dateVarienceLower);
		Date departureEnd = CalendarUtil.addDateVarience(resSegSummaryTo.getDepatureDate(), dateVarienceUpper);

		availFlightSearchDTO.setFromAirport(origin);
		availFlightSearchDTO.setToAirport(dest);
		availFlightSearchDTO.setDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setDepatureDateTimeEnd(departureEnd);
		availFlightSearchDTO.setSelectedDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setSelectedDepatureDateTimeEnd(departureEnd);
		availFlightSearchDTO.setAdultCount(reservationSummaryTo.getPaxCount().get(PaxTypeTO.ADULT));
		availFlightSearchDTO.setChildCount(reservationSummaryTo.getPaxCount().get(PaxTypeTO.CHILD));

		Map<String, List<Integer>> logicalCcSelection = new HashMap<String, List<Integer>>();
		logicalCcSelection.put(reservationSummaryTo.getLogicalCcCode(), null);
		availFlightSearchDTO.setLogicalCabinClassSelection(logicalCcSelection);

		availFlightSearchDTO.setQuoteFares(true);
		availFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		availFlightSearchDTO.setAppIndicator(userPrincipal.getApplicationEngine().toString());
		availFlightSearchDTO.setChannelCode(userPrincipal.getSalesChannel());
		availFlightSearchDTO.setFareCategoryType("A");
		availFlightSearchDTO.setInboundFareQuote(false);
		availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.OUT_BOUND, true);
		availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.IN_BOUND, false);
		availFlightSearchDTO.setReturnFlag(false);



		OriginDestinationInfoDTO ondInfo = new OriginDestinationInfoDTO();
		ondInfo.setOrigin(origin);
		ondInfo.setDestination(dest);
		ondInfo.setDepartureDateTimeStart(departureStart);
		ondInfo.setDepartureDateTimeEnd(CalendarUtil.addDateVarience(departureEnd, 1));
		ondInfo.setPreferredLogicalCabin(reservationSummaryTo.getLogicalCcCode());

		ondInfo.setPreferredDateTimeStart(departureStart);
		ondInfo.setPreferredDateTimeEnd(CalendarUtil.addDateVarience(departureEnd, 1));

		availFlightSearchDTO.addOriginDestination(ondInfo);


		if (isConnection) {
			availFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.CONNECTED_FLIGHTS_ONLY);
		}

		return availFlightSearchDTO;
	}

	public static AvailableFlightSearchDTO createAvailableFlightSearchDTO(FlightSegement flightSegment,
			ReservationSummaryTo resSummaryTo, UserPrincipal userPrincipal) {
		AvailableFlightSearchDTO availFlightSearchDTO = new AvailableFlightSearchDTO();
		List<ReservationSegSummaryTo> resSegs = new ArrayList<ReservationSegSummaryTo>();
		String origin = null;
		String dest = null;
		
		Date firstSegDepStartDate = null;

		if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_OUTBOUND) {
			resSegs = resSummaryTo.getOutboundSegs();
			origin = PromotionsUtils.getOriginAndDestinationAirports(resSummaryTo.getOutboundSegs().get(0).getSegmentCode())
					.getLeft();
			dest = PromotionsUtils.getOriginAndDestinationAirports(
					resSummaryTo.getOutboundSegs().get(resSummaryTo.getOutboundSegs().size() - 1).getSegmentCode()).getRight();
			firstSegDepStartDate = resSegs.get(0).getDepartureDateLocal();
			
		} else if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_INBOUND) {
			resSegs = resSummaryTo.getInboundSegs();
			origin = PromotionsUtils.getOriginAndDestinationAirports(resSummaryTo.getInboundSegs().get(0).getSegmentCode())
					.getLeft();
			dest = PromotionsUtils.getOriginAndDestinationAirports(
					resSummaryTo.getInboundSegs().get(resSummaryTo.getInboundSegs().size() - 1).getSegmentCode()).getRight();
			firstSegDepStartDate = resSegs.get(0).getDepartureDateLocal();
		}

		Date departureStart = CalendarUtil.getStartTimeOfDate(firstSegDepStartDate);
		Date departureEnd = CalendarUtil.getEndTimeOfDate(firstSegDepStartDate);

		availFlightSearchDTO.setFromAirport(origin);
		availFlightSearchDTO.setToAirport(dest);
		availFlightSearchDTO.setDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setDepatureDateTimeEnd(CalendarUtil.addDateVarience(departureEnd, 1));
		availFlightSearchDTO.setSelectedDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setSelectedDepatureDateTimeEnd(CalendarUtil.addDateVarience(departureEnd, 1));
		availFlightSearchDTO.setAdultCount(1);

		Map<String, List<Integer>> logicalCcSelection = new HashMap<String, List<Integer>>();
		List<Integer> flightSegs = new ArrayList<Integer>();
		for (ReservationSegSummaryTo resSeg : resSegs) {
			flightSegs.add(resSeg.getFlightSegId());
		}
		logicalCcSelection.put(resSummaryTo.getLogicalCcCode(), flightSegs);
		availFlightSearchDTO.setLogicalCabinClassSelection(logicalCcSelection);

		if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_OUTBOUND) {
			availFlightSearchDTO.setOutBoundFlights(flightSegs);
			availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.OUT_BOUND, true);
			availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.IN_BOUND, false);
		} else if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_INBOUND) {
			availFlightSearchDTO.setOutBoundFlights(flightSegs);
			availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.OUT_BOUND, true);
			availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.IN_BOUND, false);
		}

		if (flightSegs.size() > 1) {
			availFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.CONNECTED_FLIGHTS_ONLY);
		}

		availFlightSearchDTO.setQuoteFares(true);
		availFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		
		//XBE only select minimum fare flight. IBE select date all flights
		availFlightSearchDTO.setAppIndicator(ApplicationEngine.XBE.toString()); //TODO Check why XBE is sent here
		availFlightSearchDTO.setChannelCode(userPrincipal.getSalesChannel());
		availFlightSearchDTO.setFareCategoryType("A");
		availFlightSearchDTO.setReturnFlag(false);

		OriginDestinationInfoDTO ondInfo = new OriginDestinationInfoDTO();
		ondInfo.setFlightSegmentIds(flightSegs);
		ondInfo.setOrigin(origin);
		ondInfo.setDestination(dest);
		ondInfo.setDepartureDateTimeStart(departureStart);
		ondInfo.setDepartureDateTimeEnd(CalendarUtil.addDateVarience(departureEnd, 1));
		ondInfo.setPreferredLogicalCabin(resSummaryTo.getLogicalCcCode());

		ondInfo.setPreferredDateTimeStart(CalendarUtil.add(firstSegDepStartDate, 0, 0, 0, 0, -5, 0));
		ondInfo.setPreferredDateTimeEnd(CalendarUtil.add(firstSegDepStartDate, 0, 0, 0, 0, 5, 0));

		availFlightSearchDTO.addOriginDestination(ondInfo);


		return availFlightSearchDTO;
	}

	public static AvailableFlightSearchDTO createAvailableFlightSearchDTOForFares(FlightSegement flightSegment, String logicalCc,UserPrincipal userPrincipal) {
		AvailableFlightSearchDTO availFlightSearchDTO = new AvailableFlightSearchDTO();

		Pair<String, String> originDest = PromotionsUtils.getOriginAndDestinationAirports(flightSegment.getSegmentCode());

		Date departureStart = CalendarUtil.getStartTimeOfDate(flightSegment.getEstTimeDepatureZulu());
		Date departureEnd = CalendarUtil.getEndTimeOfDate(flightSegment.getEstTimeDepatureZulu());

		availFlightSearchDTO.setFromAirport(originDest.getLeft());
		availFlightSearchDTO.setToAirport(originDest.getRight());
		availFlightSearchDTO.setDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setDepatureDateTimeEnd(departureEnd);
		availFlightSearchDTO.setSelectedDepatureDateTimeStart(departureStart);
		availFlightSearchDTO.setSelectedDepatureDateTimeEnd(departureEnd);
		availFlightSearchDTO.setAdultCount(1);

		Map<String, List<Integer>> logicalCcSelection = new HashMap<String, List<Integer>>();
		List<Integer> flightSegs = new ArrayList<Integer>();
		flightSegs.add(flightSegment.getFltSegId());
		logicalCcSelection.put(logicalCc, flightSegs);
		availFlightSearchDTO.setLogicalCabinClassSelection(logicalCcSelection);
		availFlightSearchDTO.setCabinClassSelection(logicalCcSelection);
		List<Integer> outBoundFlights = new ArrayList<Integer>();
		outBoundFlights.add(flightSegment.getFltSegId());
		availFlightSearchDTO.setOutBoundFlights(outBoundFlights);

		availFlightSearchDTO.setQuoteFares(true);
		availFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		availFlightSearchDTO.setAppIndicator(userPrincipal.getApplicationEngine().toString());
		availFlightSearchDTO.setChannelCode(userPrincipal.getSalesChannel());
		availFlightSearchDTO.setFareCategoryType("A");
		availFlightSearchDTO.setInboundFareQuote(false);
		availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.OUT_BOUND, true);
		availFlightSearchDTO.getOndQuoteFlexi().put(OndSequence.IN_BOUND, true);
		availFlightSearchDTO.setReturnFlag(false);
		availFlightSearchDTO.setEnforceFareCheckForModOnd(false);
		availFlightSearchDTO.setFQWithinValidity(false);

		return availFlightSearchDTO;
	}

	public static List<Map<Pair<String, String>, TransferFlightTo>> toTransferFlightTos(AvailableFlightDTO availableFlightDTO) {
		List<Map<Pair<String, String>, TransferFlightTo>> transferFlightTos = new ArrayList<Map<Pair<String, String>, TransferFlightTo>>();
		Map<Pair<String, String>, TransferFlightTo> transferFlight;
		TransferFlightTo transferFlightTo;
		SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();
		if (selectedFlightDTO.isSeatsAvailable()) {
			List<AvailableIBOBFlightSegment> selectedFlights = selectedFlightDTO.getSelectedOndFlights();
			for (AvailableIBOBFlightSegment ibobflightSegment : selectedFlights) {
				transferFlight = new HashMap<Pair<String, String>, TransferFlightTo>();
				for (FlightSegmentDTO flightSegmentDTO : ibobflightSegment.getFlightSegmentDTOs()) {
					transferFlightTo = new TransferFlightTo();
					transferFlightTo.setFlightNumber(flightSegmentDTO.getFlightNumber());
					transferFlightTo.setDepartureDate(flightSegmentDTO.getDepartureDateTimeZulu());
					transferFlightTo.setFlightSegId(flightSegmentDTO.getSegmentId());

					BigDecimal totalFare = BigDecimal.ZERO;
					totalFare = new BigDecimal(selectedFlightDTO.getTotalFareMap().get(PaxTypeTO.ADULT));
					transferFlightTo.putCharge(PromotionsInternalConstants.CHARGES_FARES, totalFare);
					BigDecimal totalCharges = BigDecimal.ZERO;
					totalCharges = new BigDecimal(selectedFlightDTO.getTotalCharges().get(0));
					transferFlightTo.putCharge(PromotionsInternalConstants.CHARGES_ALL, totalCharges);

					transferFlight.put(PromotionsUtils.getOriginAndDestinationAirports(flightSegmentDTO.getSegmentCode()),
							transferFlightTo);
				}
				transferFlightTos.add(transferFlight);
			}
		}

		return transferFlightTos;
	}

	public static Map<Pair<String, String>, TransferFlightTo> toTransferFlightTo(SelectedFlightDTO selectedFlightDTO) {

		Map<Pair<String, String>, TransferFlightTo> transferFlights = new HashMap<Pair<String, String>, TransferFlightTo>();
		TransferFlightTo transferFlightTo;
		BigDecimal totalFare;
		BigDecimal totalCharges;

		if (selectedFlightDTO.isSeatsAvailable()) {
			List<AvailableIBOBFlightSegment> selectedFlights = selectedFlightDTO.getSelectedOndFlights();
			for (AvailableIBOBFlightSegment ibobflightSegment : selectedFlights) {
				for (FlightSegmentDTO flightSegmentDTO : ibobflightSegment.getFlightSegmentDTOs()) {

					transferFlightTo = new TransferFlightTo();
					transferFlightTo.setFlightNumber(flightSegmentDTO.getFlightNumber());
					transferFlightTo.setDepartureDate(flightSegmentDTO.getDepartureDateTimeZulu());
					totalFare = BigDecimal.ZERO;
					totalFare = new BigDecimal(selectedFlightDTO.getTotalFareMap().get(PaxTypeTO.ADULT));
					transferFlightTo.putCharge(PromotionsInternalConstants.CHARGES_FARES, totalFare);
					totalCharges = BigDecimal.ZERO;
					totalCharges = new BigDecimal(selectedFlightDTO.getTotalCharges().get(0));
					transferFlightTo.putCharge(PromotionsInternalConstants.CHARGES_ALL, totalCharges);

					transferFlights.put(PromotionsUtils.getOriginAndDestinationAirports(flightSegmentDTO.getSegmentCode()),
							transferFlightTo);
				}
			}
		}

		return transferFlights;
	}

	public static Integer getChargeRateId(String promoChargeCode) throws ModuleException {
		String chargeCode = PromoTemplateParam.ChargeAndReward.resolveChargeCode(promoChargeCode);
		QuotedChargeDTO serviceChargeDTO = PromotionModuleServiceLocator.getChargeBD().getCharge(chargeCode,
				CalendarUtil.getCurrentSystemTimeInZulu(), null, null, false);
		return serviceChargeDTO.getChargeRateId();
	}

	public static String getSegmentCode(String pnr, int pnrSegId) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		for (ReservationSegmentDTO segmentDto : reservation.getSegmentsView()) {
			if (segmentDto.getPnrSegId().equals(pnrSegId)) {
				return segmentDto.getSegmentCode();
			}
		}

		return null;
	}

	public static Pair<List<String>, List<String>> getFilterdSegmentCodes(String pnr, Integer promotionId) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		List<ReservationSegmentDTO> segmentDtos = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		Collections.sort(segmentDtos, new Comparator<ReservationSegmentDTO>() {
			public int compare(ReservationSegmentDTO o1, ReservationSegmentDTO o2) {
				return o1.compareTo(o2);
			}
		});

		PromotionTemplateTo promoTemplate = PromotionModuleServiceLocator.getPromotionManagementBD().loadPromotionTemplateTo(
				promotionId);

		List<String> outbound = new ArrayList<String>();
		List<String> inbound = new ArrayList<String>();

		List<ReservationSegmentDTO> outboundSegs = new ArrayList<ReservationSegmentDTO>();
		List<ReservationSegmentDTO> inboundSegs = new ArrayList<ReservationSegmentDTO>();

		for (ReservationSegmentDTO segDto : segmentDtos) {
			if (PromotionValidatorUtil.isConfirmedFutureSegment(segDto, promoTemplate)) {
				if (segDto.getReturnFlag().equals(PromotionsInternalConstants.BOOLEAN_TRUE)) {
					inbound.add(segDto.getSegmentCode());
					inboundSegs.add(segDto);
				} else {
					outbound.add(segDto.getSegmentCode());
					outboundSegs.add(segDto);
				}
			}
		}

		List<String> promoDefinedOutbound = PromotionValidatorUtil.retrievePromoFilteredSegs(outbound, outboundSegs,
				promoTemplate);
		List<String> promoDefinedInbound = PromotionValidatorUtil.retrievePromoFilteredSegs(inbound, inboundSegs, promoTemplate);

		return Pair.of(promoDefinedOutbound, promoDefinedInbound);
	}

	/**
	 * @param pnr
	 * @return <pnr_seg_id, direction>
	 * @throws ModuleException
	 */
	public static Map<Integer, Integer> getConfirmedDirectionsOfSegs(String pnr) throws ModuleException {
		Map<Integer, Integer> directions = new HashMap<Integer, Integer>();

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		List<ReservationSegmentDTO> segmentDtos = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		Collections.sort(segmentDtos, new Comparator<ReservationSegmentDTO>() {
			public int compare(ReservationSegmentDTO o1, ReservationSegmentDTO o2) {
				return o1.compareTo(o2);
			}
		});

		for (ReservationSegmentDTO segDto : segmentDtos) {
			if (segDto.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				directions.put(segDto.getPnrSegId(), segDto.getReturnFlag().equals(PromotionsInternalConstants.BOOLEAN_TRUE)
						? PromotionsInternalConstants.DIRECTION_INBOUND
						: PromotionsInternalConstants.DIRECTION_OUTBOUND);
			}
		}

		return directions;
	}


	/**
	 * 
	 * @param pnr
	 * @return <direction, {pnr_seg_ids} >
	 * @throws ModuleException
	 */
	public static Map<Integer, List<Integer>>  getDirectionWiseSegments(String pnr) throws ModuleException{

		Map<Integer, List<Integer>> directionWiseSegments= new HashMap<Integer, List<Integer>>();
		Map<Integer, Integer> directionOfSegments = PromotionsDaoHelper.getConfirmedDirectionsOfSegs(pnr);

		for(Integer pnrSegId : directionOfSegments.keySet()){
			Integer segDirection = directionOfSegments.get(pnrSegId);

			if(directionWiseSegments.containsKey(segDirection)){
				List<Integer> countedSegs  = directionWiseSegments.get(segDirection);
				countedSegs.add(pnrSegId);
				directionWiseSegments.put(segDirection, countedSegs);
			}else{
				List<Integer> segsToConsider = new ArrayList<Integer>();
				segsToConsider.add(pnrSegId);
				directionWiseSegments.put(segDirection, segsToConsider);
			}
		}

		return directionWiseSegments;

	}

	public static Map<String, List<LCCSelectedSegmentAncillaryDTO>> constructSeatAncilaries(
			Map<String, List<FlightSeatStatusTO>> selectedSeatMap, Reservation reservation, PromotionRequest promotionRequest,
			FlightSegement flightSegment) throws ModuleException {

		Map<String, List<LCCSelectedSegmentAncillaryDTO>> selectedLCCSeatAnciMap = new HashMap<String, List<LCCSelectedSegmentAncillaryDTO>>();
		List<LCCSelectedSegmentAncillaryDTO> selectedSeatsAnciDTOs = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		List<LCCSelectedSegmentAncillaryDTO> removedSeatsAnciDTOs = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		ReservationPax pax = null;

		FlightSegmentTO flightSegmentTO = PromotionsDaoHelper.getFlightSegmentTO(reservation.getSegmentsView(), flightSegment);

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (reservationPax.getPnrPaxId().intValue() == promotionRequest.getReservationPaxId().intValue()) {
				pax = reservationPax;
				break;
			}
		}

		for (FlightSeatStatusTO addingflightSeat : selectedSeatMap.get(PromotionsInternalConstants.ADD)) {
			LCCSelectedSegmentAncillaryDTO selectedSeatSegmentAnciDTO = new LCCSelectedSegmentAncillaryDTO();
			selectedSeatSegmentAnciDTO.setTravelerRefNumber(PaxTypeUtils.travelerReference(pax));
			selectedSeatSegmentAnciDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
			selectedSeatSegmentAnciDTO.setFlightSegmentTO(flightSegmentTO);
			LCCAirSeatDTO seat = new LCCAirSeatDTO();
			if (addingflightSeat.getSeatCharge() != null && addingflightSeat.getSeatCharge().doubleValue() != 0) {
				seat.setSeatCharge(addingflightSeat.getSeatCharge());
			} else {
				seat.setSeatCharge(new BigDecimal(0));
			}
			seat.setSeatNumber(addingflightSeat.getSeatCode());
			seat.setBookedPassengerType(ReservationInternalConstants.PassengerType.ADULT);

			selectedSeatSegmentAnciDTO.setAirSeatDTO(seat);
			selectedSeatsAnciDTOs.add(selectedSeatSegmentAnciDTO);

		}

		for (FlightSeatStatusTO removingflightSeat : selectedSeatMap.get(PromotionsInternalConstants.REMOVE)) {

			LCCSelectedSegmentAncillaryDTO removedSeatSegmentAnciDTO = new LCCSelectedSegmentAncillaryDTO();
			removedSeatSegmentAnciDTO.setTravelerRefNumber(PaxTypeUtils.travelerReference(pax));
			removedSeatSegmentAnciDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
			removedSeatSegmentAnciDTO.setFlightSegmentTO(flightSegmentTO);
			LCCAirSeatDTO seat = new LCCAirSeatDTO();
			if (removingflightSeat.getSeatCharge() != null && removingflightSeat.getSeatCharge().doubleValue() != 0) {
				seat.setSeatCharge(removingflightSeat.getSeatCharge());
			} else {
				seat.setSeatCharge(new BigDecimal(0));
			}
			seat.setSeatNumber(removingflightSeat.getSeatCode());
			seat.setBookedPassengerType(ReservationInternalConstants.PassengerType.ADULT);

			removedSeatSegmentAnciDTO.setAirSeatDTO(seat);
			removedSeatsAnciDTOs.add(removedSeatSegmentAnciDTO);

		}

		selectedLCCSeatAnciMap.put(PromotionsInternalConstants.ADD, selectedSeatsAnciDTOs);
		selectedLCCSeatAnciMap.put(PromotionsInternalConstants.REMOVE, removedSeatsAnciDTOs);
		return selectedLCCSeatAnciMap;
	}

	private static FlightSegmentTO
			getFlightSegmentTO(Collection<ReservationSegmentDTO> reservationSegmentDTOs, FlightSegement fs)
					throws ModuleException {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		for (ReservationSegmentDTO segDTO : reservationSegmentDTOs) {
			if (segDTO.getFlightSegId().intValue() == fs.getFltSegId().intValue()) {
				flightSegmentTO.setArrivalDateTime(segDTO.getArrivalDate());
				flightSegmentTO.setCabinClassCode(segDTO.getCabinClassCode());
				flightSegmentTO.setDepartureDateTime(segDTO.getDepartureDate());
				flightSegmentTO.setFlightNumber(segDTO.getFlightNo());
				flightSegmentTO.setSegmentCode(segDTO.getSegmentCode());
				flightSegmentTO.setOperatingAirline(segDTO.getCarrierCode());
				flightSegmentTO.setOperationType(segDTO.getOperationTypeID());
				flightSegmentTO.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE.equals(segDTO
						.getReturnFlag()));
				flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(segDTO));
				break;
			}

		}
		return flightSegmentTO;
	}

	public static CommonReservationContactInfo transFormContactInfo(ReservationContactInfo reservationContactInfo) {

		CommonReservationContactInfo conInfo = new CommonReservationContactInfo();
		conInfo.setFirstName(reservationContactInfo.getFirstName());
		conInfo.setLastName(reservationContactInfo.getLastName());
		if (reservationContactInfo.getNationalityCode() != null) {
			conInfo.setNationalityCode(Integer.parseInt(reservationContactInfo.getNationalityCode()));
		}
		conInfo.setMobileNo(reservationContactInfo.getMobileNo());
		conInfo.setPhoneNo(reservationContactInfo.getPhoneNo());
		conInfo.setTitle(reservationContactInfo.getTitle());
		conInfo.setStreetAddress1(reservationContactInfo.getStreetAddress1());
		conInfo.setStreetAddress2(reservationContactInfo.getStreetAddress2());
		conInfo.setCity(reservationContactInfo.getCity());
		conInfo.setState(reservationContactInfo.getState());
		conInfo.setCountryCode(reservationContactInfo.getCountryCode());
		conInfo.setEmail(reservationContactInfo.getEmail());
		conInfo.setFax(reservationContactInfo.getFax());
		conInfo.setCustomerId(reservationContactInfo.getCustomerId());
		return conInfo;
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit, String airlineCode,
			String marketingAirlineCode) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true; // Set True
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.IBE);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);

		if (StringUtils.isNotEmpty(marketingAirlineCode)) {
			if (marketingAirlineCode.trim().length() == 2) {
				pnrModesDTO.setMarketingAirlineCode(marketingAirlineCode);
			}
		}

		if (StringUtils.isNotEmpty(airlineCode)) {
			if (airlineCode.trim().length() == 2) {
				pnrModesDTO.setAirlineCode(airlineCode);
			}
		}

		return pnrModesDTO;
	}

	public static List<NextSeatFreePromoResTo> transformToPromotionRequestTO(List<Object[]> result, int flightSegId) {
		List<NextSeatFreePromoResTo> summary = new ArrayList<NextSeatFreePromoResTo>();
		// Map < pnr , List<PromotionRequestApproveTo> >
		Map<String, List<PromotionRequestApproveTo>> requestsMap = new HashMap<String, List<PromotionRequestApproveTo>>();
		PromotionRequest promotionRequest;
		String pnr;
		List<PromotionRequestApproveTo> requests;
		Pair<List<PromotionRequestApproveTo>, List<PromotionRequestApproveTo>> obIbSeqs;
		NextSeatFreePromoResTo resTo;

		List<PromotionRequestApproveTo> requesList = new ArrayList<PromotionRequestApproveTo>();
		for (Object[] obj : result) {
			promotionRequest = ((PromotionRequest) obj[3]);
			pnr = promotionRequest.getPnr();
			if (!requestsMap.containsKey(pnr)) {
				requestsMap.put(pnr, new ArrayList<PromotionRequestApproveTo>());
			}

			requests = requestsMap.get(pnr);
			PromotionRequestApproveTo promoReqTO = new PromotionRequestApproveTo();
			promoReqTO.setFlightSegId((Integer) obj[0]);
			promoReqTO.setDepartureDate((Date) obj[1]);
			promoReqTO.setSegmentCode((String) obj[2]);
			promoReqTO.setPromotionRequest(promotionRequest);
			requests.add(promoReqTO);
		}

		lbl_1: for (String tempPnr : requestsMap.keySet()) {
			obIbSeqs = PromotionsUtils.seperateOutboundAndInboundSegmentsForNextSeat(requestsMap.get(tempPnr));
			resTo = new NextSeatFreePromoResTo();
			resTo.setPnr(tempPnr);
			resTo.setOutboundSegs(obIbSeqs.getLeft());
			resTo.setInboundSegs(obIbSeqs.getRight());
			resTo.setDirection(PromotionsUtils.getDirectionByOutboundInboundReqs(obIbSeqs, flightSegId));

			for (PromotionRequestApproveTo promoReq : resTo.getPromoReqsForApproval()) {
				if (promoReq.getStatus().equals(PromotionsInternalConstants.PromotionRequestStatus.APPROVED.getStatusCode())
						|| promoReq.getStatus().equals(
								PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode())) {
					continue lbl_1;
				}
			}

			summary.add(resTo);
		}

		return summary;

	}
}
