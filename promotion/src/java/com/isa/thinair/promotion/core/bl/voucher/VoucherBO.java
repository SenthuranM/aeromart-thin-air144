package com.isa.thinair.promotion.core.bl.voucher;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.VoucherPaymentOption;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.model.VoucherPayment;
import com.isa.thinair.promotion.api.model.VoucherRedemption;
import com.isa.thinair.promotion.api.model.VoucherTermsTemplate;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.core.persistence.dao.VoucherDAO;
import com.isa.thinair.promotion.core.persistence.dao.VoucherTermsTemplateDAO;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * @author chethiya
 *
 */
public class VoucherBO {

	private static Log log = LogFactory.getLog(VoucherBO.class);

	private static boolean resultBefore = false;

	private static boolean resultAfter = false;

	public static final String VOUCHER_PRINT_TEMPLATE = "email/voucher_print";

	private static final String greetingCardHolderName = "Someone in your friends or family";

	public static String generateVoucherID() throws ModuleException {
		String voucherID = null;
		try {
			String timeStamp = String.valueOf(new Date().getTime()).substring(0, 9);
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucherID = voucherManagementDAO.getNextVoucherId();
			Long voucherIdPrefix = Long.parseLong(voucherID);
			Long voucherIdSuffix = voucherIdPrefix % 7; // mod 7 values
			voucherID = timeStamp + voucherIdPrefix.toString() + voucherIdSuffix.toString();
		} catch (Exception ex) {
			log.error("VoucherBO ==> generateVoucherID(Voucher)", ex);
		}
		return voucherID;
	}

	public static void saveVoucher(VoucherDTO voucherDTO) {

		try {
			Voucher voucher = transformToModel(voucherDTO);
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucherManagementDAO.saveVoucher(voucher);
		} catch (Exception ex) {
			log.error("VoucherBO ==> saveVoucher(Voucher)", ex);
		}
	}

	public static Page<VoucherDTO> searchVoucher(int start, int pageSize, VoucherSearchRequest voucherSearchRequest,
			String voucherType) {
		Page<VoucherDTO> voucherDTOPage = null;
		List<VoucherDTO> voucherDTOList = null;
		try {
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			Page<Voucher> voucherPage = voucherManagementDAO.searchVoucher(start, pageSize, voucherSearchRequest, voucherType);
			List<Voucher> vouchtList = (List<Voucher>) voucherPage.getPageData();
			voucherDTOList = new ArrayList<VoucherDTO>();
			Iterator<Voucher> iterator = vouchtList.iterator();
			while (iterator.hasNext()) {
				voucherDTOList.add(transfomToDTO(iterator.next()));
			}
			Collections.sort(voucherDTOList, new Comparator<VoucherDTO>() {
				@Override
				public int compare(VoucherDTO a, VoucherDTO b) {
					return (b.getVoucherId()).compareTo (a.getVoucherId());
				}
			});
			voucherDTOPage = new Page<VoucherDTO>(voucherPage.getTotalNoOfRecords(), voucherPage.getStartPosition(),
					voucherPage.getEndPosition(), voucherPage.getTotalNoOfRecordsInSystem(), voucherDTOList);
		} catch (Exception ex) {
			log.error("VoucherBO ==> searchVoucher(Voucher)", ex);
		}
		return voucherDTOPage;

	}

	public static BigDecimal getBaseCurrencyAmount(String currencyCode, BigDecimal value) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal convertedValue = null;
		if (!baseCurrencyCode.equals(currencyCode) && value != null) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			convertedValue = AccelAeroRounderPolicy.convertAndRound(value, exchangeRate, null, null);
		} else {
			convertedValue = value;
		}
		return convertedValue;
	}

	public static BigDecimal getPayCurrencyAmount(String payCurrencyCode, String currencyCode, BigDecimal value)
			throws ModuleException {
		BigDecimal convertedValue = null;
		if (!payCurrencyCode.equals(currencyCode) && value != null) {
			convertedValue = AccelAeroRounderPolicy.convertAndRound(value,
					new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
							.getExchangeRate(currencyCode, payCurrencyCode), null, null);
		} else {
			convertedValue = value;
		}
		return convertedValue;
	}

	public static VoucherDTO transfomToDTO(Voucher voucher) {
		VoucherDTO voucherDTO = new VoucherDTO();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String validFrom = "";
		String validTo = "";
		String issuedTime = "";
		voucherDTO.setAmountInBase(AccelAeroCalculator.scaleValueDefault(voucher.getAmountInBase()).toString());
		voucherDTO.setAmountInLocal(AccelAeroCalculator.scaleValueDefault(voucher.getAmountInLocal()).toString());
		voucherDTO.setCurrencyCode(voucher.getCurrencyCode());
		voucherDTO.setEmail(voucher.getEmail());
		voucherDTO.setMobileNumber(voucher.getMobileNumber());
		if (voucher.getIssuedTime() != null) {
			issuedTime = dateFormat.format(voucher.getIssuedTime());
		}
		voucherDTO.setIssuedTime(issuedTime);
		voucherDTO.setIssuedUserId(voucher.getIssuedUserId());
		voucherDTO.setPaxFirstName(voucher.getPaxFirstName());
		voucherDTO.setPaxLastName(voucher.getPaxLastName());
		if (voucher.getRedeemAmount() != null) {
			voucherDTO.setRedeemdAmount(voucher.getRedeemAmount().toString());
		}
		if (voucher.getRemarks() != null) {
			voucherDTO.setRemarks(voucher.getRemarks());
		}
		voucherDTO.setStatus(voucher.getStatus());
		if (voucher.getTemplateId() != null) {
			voucherDTO.setTemplateId(voucher.getTemplateId());
		}
		if (voucher.getValidFrom() != null) {
			validFrom = dateFormat.format(voucher.getValidFrom());
		}
		voucherDTO.setValidFrom(validFrom);
		if (voucher.getValidTo() != null) {
			validTo = dateFormat.format(voucher.getValidTo());
		}
		voucherDTO.setValidTo(validTo);
		voucherDTO.setVoucherId(voucher.getVoucherId());
		if (voucher.getCancelRemarks() != null) {
			voucherDTO.setCancelRemarks(voucher.getCancelRemarks());
		}
		return voucherDTO;
	}

	public static Voucher transformToModel(VoucherDTO voucherDTO) throws ModuleException {
		Voucher voucher = new Voucher();
		Set<VoucherPayment> voucherPayments = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String validFrom = "";
		String validTo = "";

		if (voucherDTO.getAmountInLocal() != null ) {
			voucher.setAmountInLocal(
					AccelAeroCalculator.scaleValueDefault(new BigDecimal(voucherDTO.getAmountInLocal())));
		} else {
			log.error("Error transfroming DTO to Model");
			throw new ModuleException("promotions.voucher.amount.local.null");
		}
		
		BigDecimal amountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
		try {
			if (voucherDTO.getAmountInBase() != null) {
				amountInBase = AccelAeroCalculator.scaleValueDefault(new BigDecimal(voucherDTO.getAmountInBase()));
			} else if (voucherDTO.getAmountInLocal() != null) {
				BigDecimal baseCurrencyAmount = getBaseCurrencyAmount(voucherDTO.getCurrencyCode(), voucher.getAmountInLocal());
				amountInBase = AccelAeroCalculator.scaleValueDefault(baseCurrencyAmount);
			}
		} catch (ModuleException ex) {
			log.error("VoucherBO ==> transformToModel(VoucherDTO)", ex);
		}
		voucher.setAmountInBase(amountInBase);

		voucher.setVoucherId(voucherDTO.getVoucherId());
		if (voucherDTO.getVoucherPaymentDTO() != null) {
			VoucherPayment voucherPayment = new VoucherPayment();
			VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
			voucherPayments = new HashSet<VoucherPayment>();
			voucherPayment.setNominalCode(voucherPaymentDTO.getNominalCode());
			voucherPayment.setVoucherID(voucher.getVoucherId());
			if (voucherPaymentDTO.getTempPaymentID() > 0) {
				voucherPayment.setTempPaymentTnxID(voucherPaymentDTO.getTempPaymentID());
				BigDecimal tnxFee = AccelAeroCalculator
						.scaleValueDefault(new BigDecimal(voucherPaymentDTO.getCardTxnFeeLocal()));
				voucherPayment.setAmount(AccelAeroCalculator.add(amountInBase, tnxFee));
				voucherPayment.setCcTransactionFee(tnxFee);
			} else {
				voucherPayment.setAmount(amountInBase);
			}
			if (voucherPaymentDTO.getAgentTransactionID() > 0) {
				voucherPayment.setAgentTnxID(voucherPaymentDTO.getAgentTransactionID());
			}
			voucherPayments.add(voucherPayment);
		}
		voucher.setVoucherPayment(voucherPayments);
		voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_ISSUED);
		voucher.setCurrencyCode(voucherDTO.getCurrencyCode());
		voucher.setEmail(voucherDTO.getEmail().toUpperCase());
		voucher.setMobileNumber(voucherDTO.getMobileNumber());
		voucher.setIssuedTime(new Date());
		voucher.setIssuedUserId(voucherDTO.getIssuedUserId());
		voucher.setPaxFirstName(voucherDTO.getPaxFirstName());
		voucher.setPaxLastName(voucherDTO.getPaxLastName());
		if (voucherDTO.getRedeemdAmount() != null) {
			voucher.setRedeemAmount(new BigDecimal(voucherDTO.getRedeemdAmount()));
		}
		voucher.setRemarks(voucherDTO.getRemarks());
		if (voucherDTO.getTemplateId() > 0) {
			voucher.setTemplateId(voucherDTO.getTemplateId());
		}
		if (voucherDTO.getVoucherGroupId() != null) {
			voucher.setVoucherGroupId(voucherDTO.getVoucherGroupId());
		}
		validFrom = voucherDTO.getValidFrom();
		if (!validFrom.equals("") && (validFrom != null)) {
			if (validFrom.indexOf(' ') != -1) {
				validFrom = validFrom.substring(0, validFrom.indexOf(' '));
			}
			if (validFrom.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			}
			Date startOfValidity = null;
			try {
				startOfValidity = dateFormat.parse(validFrom);
			} catch (Exception ex) {
				log.error("VoucherBO ==> transformToModel(VoucherDTO)", ex);
			}
			voucher.setValidFrom(CalendarUtil.getStartTimeOfDate(startOfValidity));
		}

		validTo = voucherDTO.getValidTo();
		if (!validTo.equals("") && (validTo != null)) {
			if (validTo.indexOf(' ') != -1) {
				validTo = validTo.substring(0, validTo.indexOf(' '));
			}
			if (validTo.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			}
			Date endOfValidity = null;
			try {
				endOfValidity = dateFormat.parse(validTo);
			} catch (Exception ex) {
				log.error("VoucherBO ==> transformToModel(VoucherDTO)", ex);
			}
			voucher.setValidTo(CalendarUtil.getEndTimeOfDate(endOfValidity));
		}

		return voucher;
	}

	public static void cancelVoucher(VoucherDTO voucherDTO) {

		try {
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucherManagementDAO.cancelVoucher(voucherDTO);

		} catch (Exception ex) {
			log.error("VoucherBO ==> cancel(Voucher)", ex);
		}
	}

	public static void emailVoucher(VoucherDTO voucherDTO) {

		try {
			MessagingServiceBD messagingServiceBD = PromotionModuleServiceLocator.getMessagingServiceBD();

			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			HashMap<String, Object> emailDataMap = (HashMap<String, Object>) getEMailDataMap(voucherDTO);
			UserMessaging voucherOwner = new UserMessaging();
			voucherOwner.setFirstName(voucherDTO.getPaxFirstName());
			voucherOwner.setLastName(voucherDTO.getPaxLastName());
			voucherOwner.setToAddres(voucherDTO.getEmail());
			messages.add(voucherOwner);

			Topic topic = new Topic();
			topic.setMailServerConfigurationName(MessagingCustomConstants.DEFAULT_MAIL_SERVER_KEY);
			topic.setTopicParams(emailDataMap);
			topic.setLocale(new Locale(voucherDTO.getPrintLanguage()));
			topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.VOUCHER_EMAIL_TEMPLATE);
			topic.setLocale(null);
			topic.setAttachMessageBody(true);

			MessageProfile messageProfile = new MessageProfile();
			messageProfile.setUserMessagings(messages);
			messageProfile.setTopic(topic);

			messagingServiceBD.sendMessage(messageProfile);

		} catch (Exception ex) {
			log.error("VoucherBO ==> email(Voucher)", ex);
		}

	}

	public static String printVoucher(VoucherDTO voucherDTO) {

		String htmlContent = "";
		try {
			HashMap<String, Object> voucherDataMap = (HashMap<String, Object>) getEMailDataMap(voucherDTO);
			String languageCode = voucherDTO.getPrintLanguage();
			String templateName = VOUCHER_PRINT_TEMPLATE + "_" + languageCode + ".xml.vm";
			StringWriter writer = new StringWriter();
			try {
				new TemplateEngine().writeTemplate(voucherDataMap, templateName, writer);
			} catch (Exception e) {
				log.error("Error creating template", e);
			}

			htmlContent += writer.toString();

		} catch (Exception ex) {
			log.error("VoucherBO ==> print(Voucher)", ex);
		}
		return htmlContent;
	}

	public static Map<String, Object> getEMailDataMap(VoucherDTO voucherDTO) {

		String termsNConditions = "No Terms and Conditions Applied";
		String termsTemplateName = PromotionCriteriaConstants.VoucherTemplateName.GIFT_VOUCHER;
		if (voucherDTO.getTemplateId() == 0) {
			termsTemplateName = PromotionCriteriaConstants.VoucherTemplateName.VOUCHER;
		}

		try {
			VoucherTermsTemplateDAO voucherTermsTemplateDAO = PromotionsHandlerFactory.getVoucherTermsTemplateDAO();
			String languageCode = voucherDTO.getPrintLanguage();
			if (languageCode == null) {
				languageCode = "en";
			}
			VoucherTermsTemplate temp = voucherTermsTemplateDAO.getVoucherTerms(termsTemplateName, languageCode);
			if (temp != null) {
				termsNConditions = StringUtil.getUnicode(temp.getTemplateContent());
			}
		} catch (Exception ex) {
			log.error("VoucherBO ==> Error creating terms and Conditions", ex);
		}

		Map<String, Object> emailDataMap = new HashMap<String, Object>();
		emailDataMap.put("voucherId", voucherDTO.getVoucherId());
		emailDataMap.put("amount", voucherDTO.getAmountInLocal());
		emailDataMap.put("firstName", voucherDTO.getPaxFirstName());
		emailDataMap.put("lastName", voucherDTO.getPaxLastName());
		emailDataMap.put("currency", voucherDTO.getCurrencyCode());
		emailDataMap.put("validFrom", voucherDTO.getValidFrom());
		emailDataMap.put("validTo", voucherDTO.getValidTo());
		emailDataMap.put("terms", termsNConditions);
		emailDataMap.put("message", voucherDTO.getRemarks());
		emailDataMap.put("cardOwner",
				voucherDTO.getVoucherPaymentDTO() != null && voucherDTO.getVoucherPaymentDTO().getCardHolderName() != null ?
						voucherDTO.getVoucherPaymentDTO().getCardHolderName() :
						greetingCardHolderName);
		return emailDataMap;
	}

	public static Object[] voucherCCPayment(List<VoucherDTO> voucherDTOs, Collection<Integer> colTnxIds,
			CredentialsDTO credentialsDTO, List<CardDetailConfigDTO> cardDetailConfigData, String totalInBase)
			throws ModuleException {
		log.debug("Inside creditPayment");

		PaymentBrokerBD paymentBrokerBD = PromotionModuleServiceLocator.getPaymentBrokerBD();

		// Create credit card payment
		CreditCardPayment creditCardPayment = createCreditCardPayment(voucherDTOs, totalInBase);
		String operationType = ReservationTemplateUtils.AUDIT_PAYMENT_TEXT;
		String authorizationId = "";

		// Send CC payment request and capture the response
		ServiceResponce serviceResponce = paymentBrokerBD.charge(creditCardPayment, creditCardPayment.getPnr(),
				creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode(), cardDetailConfigData);

		if (serviceResponce != null) {
			String ccLast4Digits = (String) serviceResponce.getResponseParam(PaymentConstants.PARAM_CC_LAST_DIGITS);
			if (!serviceResponce.isSuccess()) {

				// Updating the payment(s) information
				PromotionModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, ccLast4Digits, null, null);
				Object errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
				Object errorMsg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

				// Audit the failurs
				auditFailures(creditCardPayment, operationType, errorCode + " " + errorMsg, credentialsDTO);

				throw new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR,
						ReservationTemplateUtils.getStandardPaymentErrorMap(errorCode, errorMsg));
			} else {
				Object msg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID);

				authorizationId = BeanUtils.nullHandler(msg);
				// Updating the payment(s) information
				PromotionModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS, null, ccLast4Digits, null, null);
			}
		} else {
			// Updating the payment(s) information
			PromotionModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, null, null, null);
			// Audit the failurs
			auditFailures(creditCardPayment, operationType, ReservationTemplateUtils.NO_RESPONSE_TEXT, credentialsDTO);
			throw new ModuleException("airreservations.arg.cc.noresponse");
		}

		log.debug("Exit creditPayment");
		return new Object[] { serviceResponce.getResponseCode(), operationType, authorizationId };

	}

	public static CreditCardPayment createCreditCardPayment(List<VoucherDTO> voucherDTOs, String totalInBase)
			throws ModuleException {

		VoucherPaymentDTO voucherPaymentDTO = voucherDTOs.get(0).getVoucherPaymentDTO();
		CreditCardPayment creditCardPayment = new CreditCardPayment();
		String payCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();

		creditCardPayment.setCardholderName(voucherPaymentDTO.getCardHolderName());
		creditCardPayment.setCardNumber(voucherPaymentDTO.getCardNumber());
		String expiry = voucherPaymentDTO.getCardExpiry();
		creditCardPayment.setExpiryDate(expiry.substring(2, 4) + expiry.substring(0, 2));
		BigDecimal amount = getPayCurrencyAmount(payCurrencyCode, AppSysParamsUtil.getBaseCurrency(), new BigDecimal(totalInBase));
		creditCardPayment.setAmount(AccelAeroCalculator.scaleValueDefault(amount).toString());
		creditCardPayment.setUserIP(voucherPaymentDTO.getUserIp());
		creditCardPayment.setCurrency(AppSysParamsUtil.getBaseCurrency());
		creditCardPayment.setCvvField(voucherPaymentDTO.getCardCVV());
		creditCardPayment.setPnr(voucherDTOs.get(0).getVoucherGroupId());
		creditCardPayment.setAppIndicator(voucherPaymentDTO.getAppIndicator());
		TnxModeEnum tnxModEnum = null;
		if (voucherPaymentDTO.getTnxModeCode() == TnxModeEnum.MAIL_TP_ORDER.getCode()) {
			creditCardPayment.setTnxMode(TnxModeEnum.MAIL_TP_ORDER);
		} else if (voucherPaymentDTO.getTnxModeCode() == TnxModeEnum.SECURE_3D.getCode()) {
			creditCardPayment.setTnxMode(TnxModeEnum.SECURE_3D);
		}
		creditCardPayment.setTemporyPaymentId(voucherPaymentDTO.getTempPaymentID());
		// creditCardPayment.setPaymentBrokerRefNo(Integer.parseInt(voucherDTOs.get(0).getVoucherGroupId()));
		creditCardPayment.setIpgIdentificationParamsDTO(
				prepareIPGIdentificationParamsDTO(payCurrencyCode, voucherPaymentDTO.getIpgId()));
		// creditCardPayment.setCardType(Integer.parseInt(voucherPaymentDTO.getCardType()));

		return creditCardPayment;
	}

	private static void auditFailures(CreditCardPayment creditCardPayment, String operationType, String errorDescription,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationBD reservationBD = PromotionModuleServiceLocator.getReservationBD();
		reservationBD.auditCreditCardFailures(creditCardPayment.getPnr(), new BigDecimal(creditCardPayment.getAmount()),
				operationType, errorDescription, credentialsDTO.getTrackInfoDTO());
	}

	protected static void
			createInvoices(String strPNR, String agentCode, Collection<Integer> colTnxIds, Date lastModificatinTime)
					throws ModuleException {
		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			Date formattedDate = formatDateInfo(lastModificatinTime);
			SearchInvoicesReceiptsDTO invoicesReceiptsDTO = new SearchInvoicesReceiptsDTO();
			invoicesReceiptsDTO.setPnr(strPNR);
			invoicesReceiptsDTO.setAgentCode(agentCode);
			// invoicesReceiptsDTO.setTnxId(tnxID);
			invoicesReceiptsDTO.setColTnxIds(colTnxIds);
			invoicesReceiptsDTO.setTxnDate(formattedDate);

			PromotionModuleServiceLocator.getInvoicingBD().generateInvoicesRecipts(invoicesReceiptsDTO);
		}
	}

	private static Date formatDateInfo(Date lastModificatinTime) {
		if (lastModificatinTime == null) {
			String strTimeZone = "GMT";
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime();
		} else {
			return lastModificatinTime;
		}
	}

	private static IPGIdentificationParamsDTO prepareIPGIdentificationParamsDTO(String payCurrencyCode, Integer ipgId)
			throws ModuleException {

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, payCurrencyCode);
		boolean isPGExists = PromotionModuleServiceLocator.getPaymentBrokerBD().checkForIPG(ipgIdentificationParamsDTO);
		if (AppSysParamsUtil.isXBECreditCardPaymentsEnabled() && !isPGExists) {
			throw new ModuleException("error.cardpay.ipgconfig.notfound");
		}
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param voucherRedeemRequest
	 * @return the voucher list on the issued status and block them
	 * @throws ModuleException
	 */
	public static VoucherRedeemResponse redeemIssuedVouchers(VoucherRedeemRequest voucherRedeemRequest) throws ModuleException {
		ArrayList<Voucher> voucherList = new ArrayList<Voucher>();
		boolean invalidVouchersAvailable = false;
		boolean voucherPaxNamesValid = false;
		String invalidVouchers = "";
		VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		BigDecimal requestedTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		String voucherID = voucherRedeemRequest.getVoucherIDList().get(0);
		Voucher voucher = voucherDAO.getVoucher(voucherID);
		log.info("Executing redeemIssuedVouchers() for Voucher ID:" + voucherID + " in Promotion");
		if (voucher != null && voucher.getStatus().equals(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_ISSUED)) {
			if (!(new Date()).after(voucher.getValidFrom()) || !(new Date()).before(voucher.getValidTo())) {
				ModuleException moduleException = new ModuleException("error.voucher.valid.period.invalid");
				throw moduleException;
			}
			if (voucher.getTemplateId() != null && (AppSysParamsUtil.isOTPEnabled()
					&& (voucherRedeemRequest.getOTP() == null || voucherRedeemRequest.getOTP().isEmpty()))) {
				if (AppSysParamsUtil.isVoucherNameValidationEnabled()) {
					for (String name : voucherRedeemRequest.getPaxNameList()) {
						if (name.equalsIgnoreCase(voucher.getPaxFirstName() + " " + voucher.getPaxLastName())) {
							voucherPaxNamesValid = true;
							break;
						}
					}
					if (voucherPaxNamesValid) {
						VoucherRedeemResponse redeemResponse = new VoucherRedeemResponse();
						String generatedotp = generateOtp(6);
						if (!StringUtil.isNullOrEmpty(voucher.getMobileNumber())) {
							sendSMS(voucher.getMobileNumber(), generatedotp, voucher);
							redeemResponse.setOtp(generatedotp);
							return redeemResponse;
						} else {
							ModuleException moduleException = new ModuleException(
									"error.voucher.mobile.number.invalid");
							throw moduleException;
						}
					}
				} else {
					VoucherRedeemResponse redeemResponse = new VoucherRedeemResponse();
					String generatedotp = generateOtp(6);
					if (!StringUtil.isNullOrEmpty(voucher.getMobileNumber())) {
						sendSMS(voucher.getMobileNumber(), generatedotp, voucher);
						redeemResponse.setOtp(generatedotp);
						return redeemResponse;
					} else {
						ModuleException moduleException = new ModuleException("error.voucher.mobile.number.invalid");
						throw moduleException;
					}
				}
			} else {
				if (AppSysParamsUtil.isVoucherNameValidationEnabled()) {
					for (String name : voucherRedeemRequest.getPaxNameList()) {
						if (name.equalsIgnoreCase(voucher.getPaxFirstName() + " " + voucher.getPaxLastName())) {
							voucherPaxNamesValid = true;
							break;
						}
					}
					if (voucherPaxNamesValid) {
						voucherList.add(voucher);
						requestedTotal = AccelAeroCalculator.add(requestedTotal, voucher.getAmountInBase());
					}
				} else {
					voucherList.add(voucher);
					requestedTotal = AccelAeroCalculator.add(requestedTotal, voucher.getAmountInBase());
				}
			}
		} else {
			invalidVouchersAvailable = true;
			if (!invalidVouchers.equals("")) {
				invalidVouchers += " ,";
			}
			invalidVouchers += voucherID;
		}

		if (invalidVouchersAvailable) {
			ModuleException moduleException = new ModuleException("error.voucher.redeem.invalid");
			moduleException.setCustumValidateMessage(invalidVouchers);
			throw moduleException;
		}

		if (!voucherPaxNamesValid && AppSysParamsUtil.isVoucherNameValidationEnabled()) {
			ModuleException moduleException = new ModuleException("error.voucher.pax.name.invalid");
			throw moduleException;
		}

		Collections.sort(voucherList, new Comparator<Voucher>() {
			@Override
			public int compare(Voucher v1, Voucher v2) {
				return (v1.getAmountInBase().compareTo(v2.getAmountInBase()));
			}
		});
		return deriveRedeemResponse(voucherList, requestedTotal, new BigDecimal(voucherRedeemRequest.getBalanceToPay()));
	}
	

	public static void unblockVoucherList(ArrayList<String> voucherIDList) {
		VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		List<Voucher> voucherList = voucherDAO.getVoucherList(voucherIDList,
				PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_BLOCKED);
		for (Voucher voucher : voucherList) {
			voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_ISSUED);
		}
		voucherDAO.saveOrUpdateAll(voucherList);
	}

	private static void blockVoucherList(ArrayList<Voucher> voucherList) {
		VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		for (Voucher voucher : voucherList) {
			voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_BLOCKED);
		}
		voucherDAO.saveOrUpdateAll(voucherList);
	}

	private static VoucherRedeemResponse deriveRedeemResponse(ArrayList<Voucher> voucherList, BigDecimal requestedTotal,
			BigDecimal balanceToPay) {

		VoucherRedeemResponse voucherRedeemResponse = new VoucherRedeemResponse();
		String redeemTotal = null;
		BigDecimal vouchersTotal = requestedTotal;
		VoucherPaymentOption voucherPaymentOption = VoucherPaymentOption.NONE;

		if (balanceToPay.compareTo(requestedTotal) > -1) {
			redeemTotal = requestedTotal.toString();
			voucherPaymentOption = VoucherPaymentOption.PART;
		} else {
			BigDecimal gap = AccelAeroCalculator.subtract(requestedTotal, balanceToPay);
			while (gap.compareTo(voucherList.get(0).getAmountInBase()) > -1) {
				vouchersTotal = AccelAeroCalculator.subtract(vouchersTotal, voucherList.get(0).getAmountInBase());
				voucherList.remove(0);
				gap = AccelAeroCalculator.subtract(gap, voucherList.get(0).getAmountInBase());
			}
			redeemTotal = balanceToPay.toString();
			voucherPaymentOption = VoucherPaymentOption.TOTAL;
		}

		blockVoucherList(voucherList);

		voucherRedeemResponse.setRedeemVoucherList(getVoucherInfoList(voucherList));
		voucherRedeemResponse.setRedeemedTotal(redeemTotal);
		voucherRedeemResponse.setVouchersTotal(vouchersTotal.toString());
		voucherRedeemResponse.setVoucherPaymentOption(voucherPaymentOption);
		return voucherRedeemResponse;
	}

	public static ArrayList<VoucherDTO> getVoucherInfoList(List<Voucher> voucherList) {

		ArrayList<VoucherDTO> voucherDTOList = new ArrayList<VoucherDTO>();
		for (Voucher voucher : voucherList) {
			VoucherDTO voucherDTO = transfomToDTO(voucher);
			BigDecimal redeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			voucherDTO.setRedeemdAmount(redeemAmount.toString());
			voucherDTOList.add(voucherDTO);
		}
		return voucherDTOList;
	}

	private static void setUsedStatus(Voucher voucher) {
		VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_USED);
		voucherDAO.updateVoucher(voucher);
	}

	public static void recordRedemption(String voucherID, Integer paxTnxID, String redeemAmount, Integer extCarrierPaxTnxID) {
		VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		Voucher voucher = voucherDAO.getVoucher(voucherID);
		BigDecimal redeemdAmount = new BigDecimal(redeemAmount);
		BigDecimal totalRedeemd = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (voucher.getRedeemAmount() == null) {
			totalRedeemd = redeemdAmount.abs();
		} else {
			totalRedeemd = AccelAeroCalculator.add(redeemdAmount.abs(), voucher.getRedeemAmount().abs());
		}
		voucher.setRedeemAmount(AccelAeroCalculator.scaleValueDefault(totalRedeemd));
		setUsedStatus(voucher);

		VoucherRedemption voucherRedemption = new VoucherRedemption();
		voucherRedemption.setTransactionID(paxTnxID);
		voucherRedemption.setExtCarrierPaxTnxID(extCarrierPaxTnxID);
		voucherRedemption.setVoucherID(voucherID);
		voucherDAO.saveVoucherRedemption(voucherRedemption);

	}

	public static Collection<VoucherTermsTemplateDTO> toVoucherTermsTemplateDTOCOllection(
			Collection<VoucherTermsTemplate> termsTemplates) {
		ArrayList<VoucherTermsTemplateDTO> dtos = new ArrayList<VoucherTermsTemplateDTO>();
		for (VoucherTermsTemplate template : termsTemplates) {
			dtos.add(toVoucherTermsTemplateDTO(template));
		}
		return dtos;
	}

	public static Page<VoucherTermsTemplateDTO> convertVoucherPageToDTO(Page<VoucherTermsTemplate> termsTemplatePage) {

		Page<VoucherTermsTemplateDTO> dtoPage = new Page<VoucherTermsTemplateDTO>(termsTemplatePage.getTotalNoOfRecords(),
				termsTemplatePage.getStartPosition(), termsTemplatePage.getEndPosition(),
				toVoucherTermsTemplateDTOCOllection(termsTemplatePage.getPageData()));

		return dtoPage;
	}

	public static VoucherTermsTemplate toVoucherTermsTemplate(VoucherTermsTemplateDTO termsTemplateDTO) {

		VoucherTermsTemplate tmp = new VoucherTermsTemplate();

		tmp.setLanguageCode(termsTemplateDTO.getLanguage());

		// Translate normal text to hex so it can be saved in the database.
		tmp.setTemplateContent(StringUtil.convertToHex(termsTemplateDTO.getTemplateContent()));
		tmp.setTemplateID(termsTemplateDTO.getTemplateID());
		tmp.setTemplateName(termsTemplateDTO.getTemplateName());
		tmp.setVersion(termsTemplateDTO.getVersion());
		tmp.setVoucherType(termsTemplateDTO.getVoucherType());

		return tmp;
	}

	public static VoucherTermsTemplateDTO toVoucherTermsTemplateDTO(VoucherTermsTemplate termsTemplate) {
		VoucherTermsTemplateDTO tmp = new VoucherTermsTemplateDTO();

		tmp.setLanguage(termsTemplate.getLanguageCode());

		// Translate DB saved template content from hex to normal text
		tmp.setTemplateContent(StringUtil.getUnicode(termsTemplate.getTemplateContent()));

		tmp.setTemplateID(termsTemplate.getTemplateID());
		tmp.setTemplateName(termsTemplate.getTemplateName());
		tmp.setVersion(termsTemplate.getVersion());
		tmp.setVoucherType(termsTemplate.getVoucherType());

		return tmp;
	}

	public static Audit generateAudit(VoucherTermsTemplate termsTemplate, TrackInfoDTO trackInfoDTO) {
		String details = "";
		int tempId = termsTemplate.getTemplateID();
		char type = termsTemplate.getVoucherType();
		String ipAddress = trackInfoDTO.getIpAddress();

		details += "voucher_terms_template_id := " + tempId + "||voucher_type := " + String.valueOf(type) + "||changed_from := "
				+ ipAddress;

		return new Audit(TasksUtil.EDIT_VOUCHER_TERMS_TEMPLATE, new Date(), "airadmin", trackInfoDTO.getOriginUserId(), details);
	}

	public static Page<ReprotectedPaxDTO> searchReprotectedPassengers(int start, int pageSize,
			ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {
		Page<ReprotectedPaxDTO> rePaxDTOPage = null;
		try {
			reprotectedPaxSearchrequest.setSqlDate(convertDate(reprotectedPaxSearchrequest.getDate()));
			rePaxDTOPage = PromotionModuleServiceLocator.getReprotectedPassengerBD().searchReprotectedPax(start, pageSize,
					reprotectedPaxSearchrequest);

		} catch (Exception ex) {
			log.error("VoucherBO ==> searchReprotectedPassengers(ReprotectedPaxSearchrequest)", ex);
		}
		return rePaxDTOPage;
	}

	public static String convertDate(String dateString) throws ModuleException {
		String date = "";
		if (!"".equals(dateString)) {

			int day = Integer.parseInt(dateString.substring(0, 2));
			int month = Integer.parseInt(dateString.substring(3, 5));
			int year = Integer.parseInt(dateString.substring(6));

			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar cal = new GregorianCalendar(year, (month - 1), day);
			date = dateFormat.format(cal.getTime());
		}

		return date;
	}

	public static void issueVouchersForReprotectedPax(ArrayList<ReprotectedPaxDTO> selectedPaxList,
			ArrayList<VoucherDTO> voucherDTOs, String voucherIssueType) {

		try {
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();

			Map<Integer, List<ReprotectedPassenger>> paxFilteredMap = getReprotectedPaxMap(selectedPaxList);

			List<Voucher> generatedVoucherList = new ArrayList<Voucher>();
			Collection<ReprotectedPassenger> updatedReprotectedPaxList = new ArrayList<ReprotectedPassenger>();
			Collection<VoucherDTO> voucherDTOsToEmail = new ArrayList<VoucherDTO>();
			List<Integer> reprotectedPaxIds = new ArrayList<Integer>();
			List<Integer> pnrSegIds = new ArrayList<Integer>();

			for (ReprotectedPaxDTO rePaxDTO : selectedPaxList) {
				reprotectedPaxIds.add(rePaxDTO.getPnrPaxId());
				pnrSegIds.add(rePaxDTO.getPnrSegmentId());
			}

			Integer frmFltSegId = selectedPaxList.get(0).getFrmFltSegId();
			Map<Integer, String> paxAmountMap = new HashMap<Integer, String>();
			String issueTypePF = AirPricingCustomConstants.ChargeTypes.PERCENTAGE_OF_FARE_PF.getChargeTypes();

			if (voucherIssueType.equals(issueTypePF)) {
				paxAmountMap = voucherManagementDAO.getFlightFarePaxWise(reprotectedPaxIds, pnrSegIds);
			}
			if (voucherDTOs.size() == selectedPaxList.size()) {
				for (VoucherDTO voucherDTO : voucherDTOs) {
					for (ReprotectedPaxDTO rePaxDTO : selectedPaxList) {
						if (voucherDTO.getReprotectedPaxid().equals(rePaxDTO.getReprotectedPaxid())) {
							Integer pnrPaxId = rePaxDTO.getPnrPaxId();
							List<ReprotectedPassenger> recordsForPax = paxFilteredMap.get(pnrPaxId);
							Integer toFltSegId = rePaxDTO.getToFltSegId();
							// Global variables to find whether voucher is issued or not before
							// or after in recursive function
							resultBefore = false;
							resultAfter = false;

							if (!isVoucherIssuedForPaxBefore(frmFltSegId, recordsForPax, 0)
									&& !isVoucherIssuedForPaxAfter(toFltSegId, recordsForPax, 0)) {

								voucherDTO.setVoucherId(VoucherBO.generateVoucherID());
								// Here PF value is come under amount attribute in voucherDTO. It can be identified from
								// the
								// voucherIssueType variable
								if (voucherIssueType.equals(issueTypePF)) {
									String pF = voucherDTO.getPercentage();
									String fareAmount = paxAmountMap.get(pnrPaxId);
									BigDecimal amount = AccelAeroCalculator.calculatePercentage(new BigDecimal(fareAmount),
											Float.parseFloat(pF), RoundingMode.UP);
									voucherDTO.setAmountInBase(amount.toString());
									voucherDTO.setAmountInLocal(amount.toString());
								}
								rePaxDTO.setVoucherId(voucherDTO.getVoucherId());
								Voucher voucher = transformToModel(voucherDTO);
								generatedVoucherList.add(voucher);
								voucherDTOsToEmail.add(voucherDTO);

								ReprotectedPassenger rePax = transformToReprotectedPassengerModel(rePaxDTO);
								updatedReprotectedPaxList.add(rePax);

							}
						}
					}
				}
			}

			voucherManagementDAO.saveOrUpdateAll(generatedVoucherList);
			PromotionModuleServiceLocator.getReprotectedPassengerBD().saveAllReprotectedPassengers(updatedReprotectedPaxList);
			for (VoucherDTO voucherDTOEmail : voucherDTOsToEmail) {
				emailVoucher(voucherDTOEmail);
			}

		} catch (ModuleException e) {
			log.error("VoucherBO ==> issueVouchersForReprotectedPax (paxList,voucherList)", e);
		}
	}

	private static Map<Integer, List<ReprotectedPassenger>> getReprotectedPaxMap(ArrayList<ReprotectedPaxDTO> selectedPaxList) {

		List<Integer> pnrPaxIds = new ArrayList<Integer>();
		List<Integer> reprotectedPaxIds = new ArrayList<Integer>();
		for (ReprotectedPaxDTO rePax : selectedPaxList) {
			pnrPaxIds.add(rePax.getPnrPaxId());
			reprotectedPaxIds.add(rePax.getReprotectedPaxid());
		}

		List<ReprotectedPassenger> recordList = PromotionModuleServiceLocator.getReprotectedPassengerBD().getAllEntriesOfPaxList(
				pnrPaxIds);

		Map<Integer, List<ReprotectedPassenger>> pnrPaxIdWisePaxMap = new HashMap<Integer, List<ReprotectedPassenger>>();

		for (ReprotectedPassenger rePax : recordList) {
			if (!reprotectedPaxIds.contains(rePax.getReprotectedPaxID())) {
				if (!pnrPaxIdWisePaxMap.containsKey(rePax.getPnrPaxId())) {
					List<ReprotectedPassenger> paxList = new ArrayList<ReprotectedPassenger>();
					paxList.add(rePax);
					pnrPaxIdWisePaxMap.put(rePax.getPnrPaxId(), paxList);
				} else {
					List<ReprotectedPassenger> paxList = pnrPaxIdWisePaxMap.get(rePax.getPnrPaxId());
					paxList.add(rePax);
					pnrPaxIdWisePaxMap.put(rePax.getPnrPaxId(), paxList);
				}
			}
		}
		return pnrPaxIdWisePaxMap;
	}

	private static boolean
			isVoucherIssuedForPaxBefore(Integer frmFltSegId, List<ReprotectedPassenger> recordsForPax, int iterator) {
		if (recordsForPax == null) {
			resultBefore = false;
		} else if (recordsForPax.size() == 0 || recordsForPax.size() == iterator) {
			return resultBefore;
		} else {
			ReprotectedPassenger pax = recordsForPax.get(iterator);
			if (pax.getToFlightSegId().equals(frmFltSegId) && pax.getVoucherId() != null) {
				resultBefore = true;
			} else if (pax.getToFlightSegId().equals(frmFltSegId) && pax.getVoucherId() == null) {
				Integer newFromFlightSegId = pax.getFromFlightSegId();
				recordsForPax.remove(pax);
				isVoucherIssuedForPaxBefore(newFromFlightSegId, recordsForPax, 0);
			} else if (iterator < recordsForPax.size()) {
				isVoucherIssuedForPaxBefore(frmFltSegId, recordsForPax, iterator + 1);
			}
		}
		return resultBefore;
	}

	private static boolean isVoucherIssuedForPaxAfter(Integer toFltSegId, List<ReprotectedPassenger> recordsForPax, int iterator) {

		if (recordsForPax == null) {
			resultAfter = false;
		}

		else if (recordsForPax.size() == 0 || recordsForPax.size() == iterator) {
			return resultAfter;
		}

		else {
			ReprotectedPassenger pax = recordsForPax.get(iterator);
			if (pax.getFromFlightSegId().equals(toFltSegId) && pax.getVoucherId() != null) {
				resultAfter = true;
			} else if (pax.getFromFlightSegId().equals(toFltSegId) && pax.getVoucherId() == null) {
				Integer newToFlightSegId = pax.getToFlightSegId();
				recordsForPax.remove(pax);
				isVoucherIssuedForPaxAfter(newToFlightSegId, recordsForPax, 0);
			} else if (iterator < recordsForPax.size()) {
				isVoucherIssuedForPaxAfter(toFltSegId, recordsForPax, iterator + 1);
			}
		}
		return resultAfter;
	}

	private static ReprotectedPassenger transformToReprotectedPassengerModel(ReprotectedPaxDTO rePaxDTO) {
		ReprotectedPassenger rePax = new ReprotectedPassenger();
		rePax.setReprotectedPaxID(rePaxDTO.getReprotectedPaxid());
		rePax.setPnrPaxId(rePaxDTO.getPnrPaxId());
		rePax.setFromFlightSegId(rePaxDTO.getFrmFltSegId());
		rePax.setToFlightSegId(rePaxDTO.getToFltSegId());
		rePax.setVoucherId(rePaxDTO.getVoucherId());
		rePax.setPnrSegId(rePaxDTO.getPnrSegmentId());
		return rePax;
	}

	@SuppressWarnings("null")
	public static void expireVouchers() {
		VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		List<Voucher> expiredVoucherList = voucherManagementDAO.getExpiredVouchers();
		List<Voucher> updatedExpiredVoucherList = new ArrayList<Voucher>();
		if (expiredVoucherList != null || expiredVoucherList.size() != 0) {
			for (Voucher voucher : expiredVoucherList) {
				voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_EXPIRED);
				updatedExpiredVoucherList.add(voucher);
			}
			voucherManagementDAO.saveOrUpdateAll(updatedExpiredVoucherList);
		}
	}

	public static String generateVoucherGroupID() throws ModuleException {
		String voucherGroupID = null;
		try {
			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucherGroupID = voucherManagementDAO.getNextVoucherGroupId();
		} catch (Exception ex) {
			log.error("VoucherBO ==> generateVoucherID(Voucher)", ex);
		}
		return voucherGroupID;
	}

	public static List<VoucherDTO> recordTempTransactionEntryForBulkIssue(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmount) throws ModuleException {
			BigDecimal TotalToPayInBase = new BigDecimal(0);
			for (VoucherDTO voucherDTO : voucherDTOs) {
				VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
				BigDecimal fullAmountLocal = AccelAeroCalculator.add(new BigDecimal(voucherPaymentDTO.getAmountLocal()),
						new BigDecimal(voucherPaymentDTO.getCardTxnFeeLocal()));
				voucherPaymentDTO.setAmount(getBaseCurrencyAmount(voucherDTO.getCurrencyCode(), fullAmountLocal).toString());
				TotalToPayInBase = AccelAeroCalculator.add(TotalToPayInBase, new BigDecimal(voucherPaymentDTO.getAmount()));
			}
			voucherDTOs.get(0).getVoucherPaymentDTO().setAmount(TotalToPayInBase.toString());
			TemporyPaymentTO tempPayment = VoucherBO.getTempPaymentForBulkIssue(voucherDTOs, payCurrency, payCurrencyAmount,
					TotalToPayInBase);
			TempPaymentTnx tempTnx = null;
			tempTnx = PromotionModuleServiceLocator.getReservationBD().recordTemporyPaymentEntry(tempPayment);
			for (VoucherDTO voucherDTO : voucherDTOs) {
				voucherDTO.getVoucherPaymentDTO().setTempPaymentID(tempTnx.getTnxId());
			}
		    return voucherDTOs;
	}

	public static List<VoucherDTO> recordTempTransactionEntryForBulkIssue(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmount, BigDecimal payAmountInBase, TrackInfoDTO trackInfoDTO) throws ModuleException {
		TemporyPaymentTO tempPayment = VoucherBO.getTempPaymentForBulkIssue(voucherDTOs, payCurrency, payCurrencyAmount,
				payAmountInBase);
		TempPaymentTnx tempTnx = null;
		tempTnx = PromotionModuleServiceLocator.getReservationBD().recordTemporyPaymentEntry(tempPayment, trackInfoDTO);
		for (VoucherDTO voucherDTO : voucherDTOs) {
			voucherDTO.getVoucherPaymentDTO().setTempPaymentID(tempTnx.getTnxId());
		}
		return voucherDTOs;
	}

	public static TemporyPaymentTO getTempPaymentForBulkIssue(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmountPG, BigDecimal TotalToPayInBase) {
		TemporyPaymentTO tempPayment = new TemporyPaymentTO();
		VoucherPaymentDTO voucherPaymentDTO = voucherDTOs.get(0).getVoucherPaymentDTO();
		String payCurrencyCode = payCurrency;
		tempPayment.setAmount(AccelAeroCalculator.scaleValueDefault(TotalToPayInBase));
		BigDecimal payCurrencyAmount = new BigDecimal(payCurrencyAmountPG);
		tempPayment.setPaymentCurrencyAmount(AccelAeroCalculator.scaleValueDefault(payCurrencyAmount));
		tempPayment.setCcNo(voucherPaymentDTO.getCardNumber());
		tempPayment.setCredit(true);
		tempPayment.setGroupPnr(voucherDTOs.get(0).getVoucherGroupId());
		tempPayment.setPaymentCurrencyCode(payCurrencyCode);
		tempPayment.setFirstName(voucherDTOs.get(0).getPaxFirstName());
		tempPayment.setLastName(voucherDTOs.get(0).getPaxLastName());
		tempPayment.setEmailAddress(voucherDTOs.get(0).getEmail());
		tempPayment.setMobileNo(voucherDTOs.get(0).getMobileNumber());
		tempPayment.setProductType(PromotionCriteriaConstants.ProductType.GIFT_VOUCHER);
		return tempPayment;
	}

	public static void saveVouchers(List<VoucherDTO> voucherDTOs) {
		try {
			List<Voucher> voucherList = new ArrayList<Voucher>();
			for (VoucherDTO voucherDTO : voucherDTOs) {
				Voucher voucher = transformToModel(voucherDTO);
				voucherList.add(voucher);
			}

			VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucherManagementDAO.saveVouchers(voucherList);
		} catch (Exception ex) {
			log.error("VoucherBO ==> saveVoucher(Voucher)", ex);
		}
	}

	public static int recordTempTransactionEntryForBulkIssueXBE(List<VoucherDTO> voucherDTOs, String totalInBase,
			String voucherGroupID) {

		int tempTnxId = 0;
		// To get common attributes use first element of the list
		VoucherDTO voucherDTO = voucherDTOs.get(0);
		VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
		TemporyPaymentTO tempPayment = new TemporyPaymentTO();
		String payCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
		try {
			tempPayment.setAmount(AccelAeroCalculator.scaleValueDefault(new BigDecimal(totalInBase)));
			BigDecimal payCurrencyAmount = getPayCurrencyAmount(payCurrencyCode, AppSysParamsUtil.getBaseCurrency(),
					new BigDecimal(totalInBase));
			tempPayment.setPaymentCurrencyAmount(AccelAeroCalculator.scaleValueDefault(payCurrencyAmount));
			tempPayment.setCcNo(voucherPaymentDTO.getCardNumber());
			tempPayment.setCredit(true);
			tempPayment.setGroupPnr(voucherGroupID);
			tempPayment.setPaymentCurrencyCode(payCurrencyCode);
			tempPayment.setFirstName(voucherDTO.getPaxFirstName());
			tempPayment.setLastName(voucherDTO.getPaxLastName());
			tempPayment.setEmailAddress(voucherDTO.getEmail());
			tempPayment.setProductType(PromotionCriteriaConstants.ProductType.GIFT_VOUCHER);

			TempPaymentTnx tempTnx = null;
			tempTnx = PromotionModuleServiceLocator.getReservationBD().recordTemporyPaymentEntry(tempPayment);
			tempTnxId = tempTnx.getTnxId();

		} catch (ModuleException ex) {
			log.error("VoucherBO ==> recordTempTransactionEntryForBulkIssueXBE", ex);
		}
		return tempTnxId;
	}

	public static List<VoucherDTO> setPaymentAmountData(List<VoucherDTO> voucherDTOs) {
		Collection<EXTERNAL_CHARGES> colExternalCharges = new ArrayList<EXTERNAL_CHARGES>();
		colExternalCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalCharges;
		try {
			externalCharges = PromotionModuleServiceLocator.getReservationBD().getQuotedExternalCharges(colExternalCharges, null,
					ChargeRateOperationType.ANY);
			ExternalChgDTO externalChgDTO = externalCharges.get(EXTERNAL_CHARGES.CREDIT_CARD);
			for (VoucherDTO voucherDTO : voucherDTOs) {
				VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
				String amount = voucherDTO.getAmountInLocal();
				externalChgDTO.calculateAmount(new BigDecimal(amount));
				String ccTxnFee = AccelAeroCalculator.scaleValueDefault(externalChgDTO.getAmount()).toString();
				voucherPaymentDTO.setCardTxnFeeLocal(ccTxnFee);
			}
		} catch (ModuleException e) {
			log.error("VoucherBO ==> recordTempTransactionEntryForBulkIssueXBE", e);
		}
		return voucherDTOs;
	}

	@SuppressWarnings("null")
	public static void unblockVouchers() {
		VoucherDAO voucherManagementDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
		List<Voucher> blockedVoucherList = voucherManagementDAO.getBlockedVouchers();
		List<Voucher> updatedExpiredVoucherList = new ArrayList<Voucher>();
		if (blockedVoucherList != null || blockedVoucherList.size() != 0) {
			for (Voucher voucher : blockedVoucherList) {
				voucher.setStatus(PromotionCriteriaConstants.VoucherStatus.VOUCHER_STATUS_ISSUED);
				updatedExpiredVoucherList.add(voucher);
			}
			voucherManagementDAO.saveOrUpdateAll(updatedExpiredVoucherList);
		}
	}

	public static String generateOtp(int len) {
		String values = "0123456789";
		Random rndm_method = new Random();
		char[] otpStr = new char[len];
		for (int i = 0; i < len; i++) {
			otpStr[i] = values.charAt(rndm_method.nextInt(values.length()));
		}
		return String.valueOf(otpStr);
	}

	/**
	 * Sends SMS
	 * 
	 * @param strMobileNo
	 * @param otp
	 * @param voucher
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendSMS(String strMobileNo, String otp, Voucher voucher) {
		log.info("Sendign SMSss to :" + strMobileNo + " VOUCHERID:" + voucher.getVoucherId() + " OTP:" + otp);
		UserMessaging userMsg = new UserMessaging();
		userMsg.setToAddres(strMobileNo);
		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(userMsg);
		HashMap map = new HashMap();
		map.put("user", userMsg);
		map.put("voucherID", voucher.getVoucherId());
		map.put("customerName", voucher.getPaxFirstName() + "" + voucher.getPaxLastName());
		map.put("otp", otp);
		Topic topic = new Topic();
		topic.setTopicParams(map);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.OTP_REDEEM_VOUCHER);
		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);
		msgProfile.setTopic(topic);
		List<MessageProfile> profileList = new ArrayList<MessageProfile>();
		profileList.add(msgProfile);
		PromotionModuleServiceLocator.getMessagingServiceBD().sendMessages(profileList);
	}
}
