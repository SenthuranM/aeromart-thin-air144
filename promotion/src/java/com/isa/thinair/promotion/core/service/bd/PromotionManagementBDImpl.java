package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.PromotionManagementBD;

@Remote
public interface PromotionManagementBDImpl extends PromotionManagementBD {

}
