package com.isa.thinair.promotion.core.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.promotion.api.model.VoucherTemplate;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateRequest;

/**
 * @author chanaka
 *
 */
public interface VoucherTemplateDao {

	public <T, K extends Serializable> T findEntity(Class<T> type, K key);

	public <T> void saveEntity(T t);

	public <T> void updateEntity(T t);

	public <T> void deleteEntities(Collection<T> entities);

	public <T> void saveEntities(Collection<T> entities);

	public void saveVoucherTemplate(VoucherTemplate voucherTemplate);

	public void updateVoucherTemplate(VoucherTemplate voucherTemplate);

	public Page<VoucherTemplate> searchVoucherTemplates(VoucherTemplateRequest vouchersSearchTo, Integer start, Integer size);

	public List<VoucherTemplate> getVoucherInSalesPeriod(Date date);

	public List<VoucherTemplate> getVouchers();

}
