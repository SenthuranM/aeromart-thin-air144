package com.isa.thinair.promotion.core.bl;

import com.isa.thinair.commons.core.framework.Command;

/**
 * @isa.module.command.macro-def
 */
public final class MacroCommandDef {

	private MacroCommandDef() {
	}

	/**
	 * @isa.module.command.macro-command name="notifyNextSeatFreePromotionMacro"
	 * @isa.module.command.macro-map order="1" inner-command="nextSeatFreeMailNotifier"
	 */
	private Command notifyNextSeatFreePromotionMacro;

	/**
	 * @isa.module.command.macro-command name="notifyFlexiDatePromotionMacro"
	 * @isa.module.command.macro-map order="1" inner-command="flexiDateMailNotifier"
	 */
	private Command notifyFlexiDatePromotionMacro;

	/**
	 * @isa.module.command.macro-command name="approveNextSeatFreePromotionMacro"
	 * @isa.module.command.macro-map order="1" inner-command="approveNextSeatFreeRequest"
	 */
	private Command approveNextSeatFreePromotionMacro;

	/**
	 * @isa.module.command.macro-command name="approveFlexiDatePromotionMacro"
	 * @isa.module.command.macro-map order="1" inner-command="approveFlexiDateRequests"
	 */
	private Command approveFlexiDatePromotionMacro;

	/**
	 * @isa.module.command.macro-command name="flexiDateAdminInquiryHandlerMacro"
	 * @isa.module.command.macro-map order="1" inner-command="flexiDateAdminInquiryHandler"
	 */
	private Command flexiDateAdminInquiryHandlerMacro;

	/**
	 * @isa.module.command.macro-command name="nextSeatAdminInquiryHandler"
	 * @isa.module.command.macro-map order="1" inner-command="nextSeatAdminInquiryHandler"
	 */
	private Command nextSeatAdminInquiryHandler;

	/**
	 * @isa.module.command.macro-command name="flexiDateChargesRefundsHandlerMacro"
	 * @isa.module.command.macro-map order="1" inner-command="flexiDateChargeAndRefund"
	 */
	private Command flexiDateChargesRefundsHandler;

	/**
	 * @isa.module.command.macro-command name="nextSeatFreeChargesRefundsHandlerMacro"
	 * @isa.module.command.macro-map order="1" inner-command="nextSeatFreeChargeAndRefund"
	 */
	private Command nextSeatFreeChargesRefundsHandler;

	/**
	 * @isa.module.command.macro-command name="issueVoucherMacro"
	 * @isa.module.command.macro-map order="1" inner-command="createVoucher"
	 */

	private Command issueVoucherMacro;
	
	/**
	 * @isa.module.command.macro-command name="cancelVoucherMacro"
	 * @isa.module.command.macro-map order="1" inner-command="cancelVoucher"
	 */

	private Command cancelVoucherMacro;
	
	

	/**
	 * @isa.module.command.macro-command name="sellGiftVoucherMacro"
	 * @isa.module.command.macro-map order="1" inner-command="makeVoucherPayment"
	 * @isa.module.command.macro-map order="2" inner-command="createVouchers"
	 */

	private Command sellGiftVoucherMacro;

	/**
	 * @isa.module.command.macro-command name="sellGiftVoucherIbeMacro"
	 * @isa.module.command.macro-map order="1" inner-command="createVoucher"
	 */
	private Command sellGiftVoucherIbeMacro;
	
	/**
	 * @isa.module.command.macro-command name="sellGiftVouchersIbeMacro"
	 * @isa.module.command.macro-map order="1" inner-command="createVouchers"
	 */
	private Command sellGiftVouchersIbeMacro;
	

	/**
	 * @isa.module.command.macro-command name="saveCampaignAndSendPromoCodesMacro"
	 * @isa.module.command.macro-map order="1" inner-command="createCampaign"
	 * @isa.module.command.macro-map order="2" inner-command="emailPromoCodes"
	 */
	
	private Command saveCampaignAndSendPromoCodesMacro;

}
