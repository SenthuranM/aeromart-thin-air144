package com.isa.thinair.promotion.core.util;

import static com.isa.thinair.promotion.core.utils.CommandNames.APPROVE_FLEXI_DATE_PROMOTION_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.APPROVE_NEXT_SEAT_FREE_PROMOTION_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.FLEXI_DATE_ADMIN_INQUIRY_HANDLER_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.FLEXI_DATE_CHARGES_REFUNDS_HANDLER_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.NEXT_SEAT_FREE_CHARGES_REFUNDS_HANDLER_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.NOTIFY_FLEXI_DATE_PROMOTION_MACRO;
import static com.isa.thinair.promotion.core.utils.CommandNames.NOTIFY_NEXT_SEAT_FREE_PROMOTION_MACRO;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.core.bl.nextseatfree.NextSeatFreePromotionBL;
import com.isa.thinair.promotion.core.bl.nextseatfree.NextSeatFreeSeatsSelector;
import com.isa.thinair.promotion.core.persistence.dao.*;
import com.isa.thinair.promotion.core.persistence.dao.BundleFareDescriptionDAO;

public class PromotionsHandlerFactory {

	private static class PromotionHandlerConfiguration {
		private String notifier;
		private String approver;
		private String adminDao;
		private String inquiryHandler;
		private String chargesRefundsHandler;

		public PromotionHandlerConfiguration(String notifier, String approver, String adminDao, String inquiryHandler,
				String chargesRefundsHandler) {
			this.notifier = notifier;
			this.approver = approver;
			this.adminDao = adminDao;
			this.inquiryHandler = inquiryHandler;
			this.chargesRefundsHandler = chargesRefundsHandler;
		}

		public String getNotifier() {
			return notifier;
		}

		public String getApprover() {
			return approver;
		}

		public String getAdminDao() {
			return adminDao;
		}

		public String getInquiryHandler() {
			return inquiryHandler;
		}

		public String getChargesRefundsHandler() {
			return chargesRefundsHandler;
		}

	}

	private static final Map<PromotionType, PromotionHandlerConfiguration> promotionsHandlersMapping = new HashMap<PromotionType, PromotionsHandlerFactory.PromotionHandlerConfiguration>();

	private static final String PROMOTIONS_MGMT_DAO = "PromotionManagementDaoProxy";
	private static final String PROMOTION_CRITERIA_DAO = "PromotionCriteriaDAOProxy";
	private static final String PROMOTIONS_ADMIN_DAO_NEXT_SEAT_FREE = "NextSeatFreePromotionAdminDaoProxy";
	private static final String PROMOTIONS_ADMIN_DAO_FLEXI_DATE = "FlexiDatePromotionAdminDaoProxy";

	private static final String ANCI_OFFER_CRITERIA_DAO = "AnciOfferCriteriaDAOProxy";
	
	private static final String BUNDLED_FARE_DAO = "BundledFareDAOProxy";
	private static final String BUNDLED_FARE_CATEGORY_DAO = "BundledFareCategoryDAOProxy";
	private static final String BUNDLED_FARE_DISPLAY_SETTINGS_DAO = "BundledFareDisplaySettingsDaoProxy";
	
	private static final String LOYALTY_MANAGMENT_DAO = "LoyaltyManagementDAOProxy";
	private static final String VOUCHER_MANAGMENT_DAO = "VoucherDAOProxy";
	private static final String VOUCHER_TEMPLATE_DAO = "VoucherTemplateDaoProxy";
	private static final String VOUCHER_TERMS_TEMPLATE_DAO = "VoucherTermsTemplateDAOProxy";
	private static final String BUNDLE_FARE_DESC_TEMPLATE_DAO = "BundleFareDescTemplateDAOProxy";

	static {

		// Handler Groups Configuration
		promotionsHandlersMapping.put(PromotionType.PROMOTION_NEXT_SEAT_FREE, new PromotionHandlerConfiguration(
				NOTIFY_NEXT_SEAT_FREE_PROMOTION_MACRO, APPROVE_NEXT_SEAT_FREE_PROMOTION_MACRO,
				PROMOTIONS_ADMIN_DAO_NEXT_SEAT_FREE, null, NEXT_SEAT_FREE_CHARGES_REFUNDS_HANDLER_MACRO));

		promotionsHandlersMapping.put(PromotionType.PROMOTION_FLEXI_DATE, new PromotionHandlerConfiguration(
				NOTIFY_FLEXI_DATE_PROMOTION_MACRO, APPROVE_FLEXI_DATE_PROMOTION_MACRO, PROMOTIONS_ADMIN_DAO_FLEXI_DATE,
				FLEXI_DATE_ADMIN_INQUIRY_HANDLER_MACRO, FLEXI_DATE_CHARGES_REFUNDS_HANDLER_MACRO));

	}

	public static DefaultBaseCommand getPromotionsNotifier(PromotionType promotionType) {
		if (promotionsHandlersMapping.containsKey(promotionType)
				&& promotionsHandlersMapping.get(promotionType).getNotifier() != null) {
			return (DefaultBaseCommand) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
					.getLocalBean(promotionsHandlersMapping.get(promotionType).getNotifier());
		}
		return null;
	}

	public static DefaultBaseCommand getPromotionRequestApprover(PromotionType promotionType) {
		if (promotionsHandlersMapping.containsKey(promotionType)
				&& promotionsHandlersMapping.get(promotionType).getApprover() != null) {
			return (DefaultBaseCommand) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
					.getLocalBean(promotionsHandlersMapping.get(promotionType).getApprover());
		}
		return null;
	}

	public static DefaultBaseCommand getPromotionAdminInquiryHandler(PromotionType promotionType) {
		if (promotionsHandlersMapping.containsKey(promotionType)
				&& promotionsHandlersMapping.get(promotionType).getInquiryHandler() != null) {
			return (DefaultBaseCommand) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
					.getLocalBean(promotionsHandlersMapping.get(promotionType).getInquiryHandler());
		}
		return null;
	}

	public static DefaultBaseCommand getPromotionChargesRefundsHandler(PromotionType promotionType) {
		if (promotionsHandlersMapping.containsKey(promotionType)
				&& promotionsHandlersMapping.get(promotionType).getChargesRefundsHandler() != null) {
			return (DefaultBaseCommand) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
					.getLocalBean(promotionsHandlersMapping.get(promotionType).getChargesRefundsHandler());
		}
		return null;
	}

	public static PromotionAdministrationDao getPromotionAdministrationDao(PromotionType promotionType) {
		if (promotionsHandlersMapping.containsKey(promotionType)
				&& promotionsHandlersMapping.get(promotionType).getAdminDao() != null) {
			return (PromotionAdministrationDao) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
					.getLocalBean(promotionsHandlersMapping.get(promotionType).getAdminDao());
		}
		return null;
	}

	public static PromotionManagementDao getPromotionManagementDao() {
		return (PromotionManagementDao) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(PROMOTIONS_MGMT_DAO);
	}
	
	public static VoucherTemplateDao getVoucherTemplateDao() {
		return (VoucherTemplateDao) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(VOUCHER_TEMPLATE_DAO);
	}
	

	/**
	 * Retrieves a instance of PromotionCriteriaDAO
	 * 
	 * @return The retrieved instance.
	 */
	public static PromotionCriteriaDAO getPromotionCriteriaDAO() {
		return (PromotionCriteriaDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(PROMOTION_CRITERIA_DAO);
	}

	public static PromotionAdministrationBD getPromotionAdministrationBD() {
		return (PromotionAdministrationBD) lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionAdministrationBD.SERVICE_NAME);
	}

	public static NextSeatFreeSeatsSelector getNextSeatFreeSeatsSelector() {
		return new NextSeatFreePromotionBL();
	}

	public static AnciOfferCriteriaDAO getAnciOfferCriteriaDAO() {
		return (AnciOfferCriteriaDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(ANCI_OFFER_CRITERIA_DAO);
	}

	public static BundledFareDAO getBundledfareDAO() {
		return (BundledFareDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(BUNDLED_FARE_DAO);
	}

	public static BundledFareCategoryDAO getBundledFareCategoryDAO() {
		return (BundledFareCategoryDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(BUNDLED_FARE_CATEGORY_DAO);
	}
	
	public static BundledFareDisplaySettingsDao getBundledFareDisplaySettingsDao() {
		return (BundledFareDisplaySettingsDao) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(BUNDLED_FARE_DISPLAY_SETTINGS_DAO);
	}

	public static LoyaltyManagementDAO getLoyaltyManagmentDAO() {
		return (LoyaltyManagementDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(LOYALTY_MANAGMENT_DAO);
	}

	public static VoucherDAO getVoucherManagmentDAO() {
		return (VoucherDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(VOUCHER_MANAGMENT_DAO);
	}	
	
	public static VoucherTermsTemplateDAO getVoucherTermsTemplateDAO() {
		return (VoucherTermsTemplateDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(VOUCHER_TERMS_TEMPLATE_DAO);
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName,
				LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME).getModuleConfig(),
				PromotionConstants.MODULE_NAME, "promotion.config.dependencymap.invalid");
	}

	public static BundleFareDescriptionDAO getBundleFareDescriptionDAO() {
		return (BundleFareDescriptionDAO) LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME)
				.getLocalBean(BUNDLE_FARE_DESC_TEMPLATE_DAO);
	}

}
