package com.isa.thinair.promotion.core.bl.lms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;

public abstract class AbstractLoyalatyManagementBL {

	protected static Log log = LogFactory.getLog(AbstractLoyalatyManagementBL.class);

	// Member Related
	public abstract String createLoyaltyMember(Customer customer) throws ModuleException;

	public abstract void updateMemberProfile(Customer customer) throws ModuleException;

	public abstract String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException;
	
	// Reward Related

	public abstract ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException;

	public abstract ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException;

	public abstract ServiceResponce cancelRedeemedLoyaltyPoints(String pnr,String[] rewardIds, String memberAccountId) throws ModuleException;
	
	
	//File upload related 
	public abstract void updatePendingFiles(FILE_TYPE fileType) throws ModuleException;

}
