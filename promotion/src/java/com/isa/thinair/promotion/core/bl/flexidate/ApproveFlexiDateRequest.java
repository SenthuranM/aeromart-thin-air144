package com.isa.thinair.promotion.core.bl.flexidate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.core.bl.common.PromotionApprover;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @isa.module.command name="approveFlexiDateRequests"
 */
public class ApproveFlexiDateRequest extends PromotionApprover {

	private Reservation reservation;
	private String eventType;

	protected ServiceResponce approvePromotionRequest() throws ModuleException {
		ServiceResponce serviceResponce = null;

		if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVE)) {
			HashMap<String, Object> promoRequest = (HashMap<String, Object>) getPromotionRequest();
			serviceResponce = getAdministrationBD().approveFlexiDatePromotionRequest(promoRequest);
		}
		return serviceResponce;
	}

	protected List<?> getPromotionRequests() throws ModuleException {
		List promotionRequests = null;
		eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);

		if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVE)) {
			promotionRequests = (List) getParameter(CommandParamNames.PROMOTION_REQUEST_CUSTOM);
		} else  if (eventType.equals(CommandParamNames.EVENT_PROMO_EXPIRE)) {
			promotionRequests = (List) getParameter(CommandParamNames.PROMOTION_REQUESTS);
		}

		return promotionRequests;
	}

	protected void promotionRequestPreProcess() throws ModuleException {
		super.promotionRequestPreProcess();
		HashMap<String, Object> promoRequest = (HashMap<String, Object>) getPromotionRequest();

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr((String) promoRequest.get(CommandParamNames.PNR));

		if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVE)) {
			pnrModesDTO.setLoadSeatingInfo(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadFares(true);
		}

		reservation = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

	}

	protected void promotionRequestPostProcess() throws ModuleException {

		NotificationReceiverTo notificationReceiverTo;
		if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVE)) {

			HashMap<String, Object> promoRequest = (HashMap<String, Object>) getPromotionRequest();
			LCCClientPnrModesDTO pnrModesDTO = PromotionsDaoHelper.getPnrModesDTO(reservation.getPnr(), false, false, null, null);

			CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
			commonItineraryParam.setItineraryLanguage(reservation.getContactInfo().getPreferredLanguage());
			commonItineraryParam.setIncludePaxFinancials(true);
			commonItineraryParam.setIncludePaymentDetails(true);
			commonItineraryParam.setIncludeTicketCharges(false);
			commonItineraryParam.setIncludeTermsAndConditions(true);
			commonItineraryParam.setAppIndicator(ApplicationEngine.IBE);
			commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(false));

			TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);

			PromotionModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
					trackInfoDTO);

			// Rufund
			Integer promoRequestId = Integer.parseInt((String) promoRequest.get(CommandParamNames.PROMOTION_REQUEST_ID));
			List<String> flightSegCodes = (List<String>) promoRequest.get(CommandParamNames.FLIGHT_SEGMENT_CODES);
			int promotionId = Integer.parseInt((String) promoRequest.get(CommandParamNames.PROMOTION_ID));
			List<Map<String, String>> transferSegs = (List<Map<String, String>>) promoRequest
					.get(CommandParamNames.TRANSFER_SEGMENTS);

			List<Integer> reservationSegIdsTransfered = new ArrayList<Integer>();
			for (Map<String, String> transferSeg : transferSegs) {
				Integer pnrSegId = Integer.parseInt(transferSeg.get(CommandParamNames.RESERVATION_SEGMENT_ID));
				reservationSegIdsTransfered.add(pnrSegId);
			}

			Map<Integer, List<PromotionRequest>> promoReqsMapping = PromotionsHandlerFactory.getPromotionManagementDao()
					.getPromotionRequests(reservation.getPnr(), promotionId, true,
							PromotionsInternalConstants.PromotionRequestStatus.INACTIVE);

			if (promoReqsMapping.containsKey(promotionId)) {
				List<PromotionRequest> promoReqs = promoReqsMapping.get(promotionId);

				List<Integer> processedSegsForRefund = new ArrayList<Integer>();
				Map<Integer, Integer> directionOfSegments = PromotionsDaoHelper.getConfirmedDirectionsOfSegs(reservation.getPnr());
				Map<Integer, List<Integer>> directionWiseSegments = PromotionsDaoHelper.getDirectionWiseSegments(reservation
						.getPnr());

				for (PromotionRequest promoReq : promoReqs) {

					if (reservationSegIdsTransfered.contains(promoReq.getReservationSegmentId())) {

						Integer currentResSegid = promoReq.getReservationSegmentId();
						Integer currentDirection = directionOfSegments.get(currentResSegid);
						List<Integer> allSegsInDirection = directionWiseSegments.get(currentDirection);

						// refund for just one time for in a direction
						if (!processedSegsForRefund.contains(currentResSegid)) {

							DefaultBaseCommand refundHandler = PromotionsHandlerFactory
									.getPromotionChargesRefundsHandler(PromotionType.PROMOTION_FLEXI_DATE);
							refundHandler.setParameter(CommandParamNames.PROMO_PROCESS_CATEGORY,
									CommandParamNames.PROCESS_REFUNDS);
							refundHandler.setParameter(CommandParamNames.PROMOTION_REQUEST, promoReq);
							refundHandler.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODES, flightSegCodes);
							refundHandler.setParameter(CommandParamNames.REQUEST_TYPE, CommandParamNames.REQUEST_EXECUTE);
							refundHandler.execute();
							processedSegsForRefund.addAll(allSegsInDirection);

						}

						promoReq.setStatus(PromotionsInternalConstants.PromotionRequestStatus.APPROVED.getStatusCode());
						PromotionsHandlerFactory.getPromotionManagementDao().updateEntity(promoReq);

						Integer FlightSegId = Integer.parseInt((String) promoRequest.get(CommandParamNames.FLIGHT_SEGMENT_ID));
						Flight transferedFlight = PromotionModuleServiceLocator.getFlightBD().getFlight(FlightSegId);
						Integer originalPnrSegId = Integer.parseInt((String) promoRequest
								.get(CommandParamNames.RESERVATION_SEGMENT_ID));
						Integer originalFlightID = null;
						Date originalFlightDate = null;

						for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
							if (seg.getPnrSegId().intValue() == originalPnrSegId.intValue()) {
								originalFlightID = seg.getFlightId();
								originalFlightDate = seg.getDepartureDate();
								break;
							}

						}

						HashMap<String, Object> contentMapForAudit = new HashMap<String, Object>();

						contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.APPROVAL_STATUS,
								promoReq.getStatus());
						contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.PNR,
								promoReq.getPnr());
						contentMapForAudit.put(
								AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_FLIGHT_ID,
								originalFlightID);
						contentMapForAudit.put(
								AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_DEPARTURE_DATE,
								originalFlightDate);

						PromotionModuleServiceLocator.getReservationBD().saveAuditFlexiDatePromotionRequest(promoReq.getPnr(),
								promoReq.getStatus(), contentMapForAudit);
					}
				}
			}

		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_EXPIRE)) {
			PromotionManagementDao promotionManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			Map<Integer, List<PromotionRequest>> promoReqsMap = PromotionsHandlerFactory.getPromotionManagementDao()
					.getPromotionRequests(reservation.getPnr(), null, false,
							PromotionsInternalConstants.PromotionRequestStatus.ACTIVE);

			for (List<PromotionRequest> promoReqs : promoReqsMap.values()) {
				for (PromotionRequest promoReq : promoReqs) {
					promoReq.setStatus(PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode());
				}

				promotionManagementDao.saveEntities(promoReqs);
			}
		}


		// Notify Mail
		List<NotificationReceiverTo> receivers = new ArrayList<NotificationReceiverTo>();
		notificationReceiverTo = new NotificationReceiverTo();
		notificationReceiverTo.setPnr(reservation.getPnr());
		notificationReceiverTo.setReceiverAddr(reservation.getContactInfo().getEmail());
		notificationReceiverTo.setReceiverName(reservation.getContactInfo().getEmail());
		notificationReceiverTo.setLanguage("en");

		receivers.add(notificationReceiverTo);

		DefaultBaseCommand notifyCommand = PromotionsHandlerFactory.getPromotionsNotifier(PromotionType.PROMOTION_FLEXI_DATE);
		notifyCommand.setParameter(CommandParamNames.PROMOTION_NOTIFICATION_DESTINATIONS, receivers);
		notifyCommand.setParameter(CommandParamNames.EVENT_TYPE, eventType);
		notifyCommand.execute();

	}

}