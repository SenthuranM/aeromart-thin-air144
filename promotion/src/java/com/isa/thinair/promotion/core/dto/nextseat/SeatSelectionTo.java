package com.isa.thinair.promotion.core.dto.nextseat;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;

public class SeatSelectionTo implements Serializable {

	private Map<String, List<FlightSeatStatusTO>> seatSelection;
	private FlightSeatStatusTO originalSeat;
	private boolean relocated;

	public Map<String, List<FlightSeatStatusTO>> getSeatSelection() {
		return seatSelection;
	}

	public void setSeatSelection(Map<String, List<FlightSeatStatusTO>> seatSelection) {
		this.seatSelection = seatSelection;
	}

	public FlightSeatStatusTO getOriginalSeat() {
		return originalSeat;
	}

	public void setOriginalSeat(FlightSeatStatusTO originalSeat) {
		this.originalSeat = originalSeat;
	}

	public boolean isRelocated() {
		return relocated;
	}

	public void setRelocated(boolean relocated) {
		this.relocated = relocated;
	}
}
