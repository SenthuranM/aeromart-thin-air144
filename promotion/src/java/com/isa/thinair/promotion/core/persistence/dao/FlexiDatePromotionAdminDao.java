package com.isa.thinair.promotion.core.persistence.dao;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.core.dto.ReservationSummaryTo;

public interface FlexiDatePromotionAdminDao extends PromotionAdministrationDao {

	List<ReservationSummaryTo> getBasicDataForFlexiDateApproval(int flightSegId) throws ModuleException;
}
