package com.isa.thinair.promotion.core.dto.nextseat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.promotion.api.to.PromotionRequestApproveTo;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class NextSeatFreePromoResTo implements Serializable {

	private String pnr;
	private int direction;
	private List<PromotionRequestApproveTo> inboundSegs;
	private List<PromotionRequestApproveTo> outboundSegs;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public List<PromotionRequestApproveTo> getInboundSegs() {
		return inboundSegs;
	}

	public void setInboundSegs(List<PromotionRequestApproveTo> inboundSegs) {
		this.inboundSegs = inboundSegs;
	}

	public List<PromotionRequestApproveTo> getOutboundSegs() {
		return outboundSegs;
	}

	public void setOutboundSegs(List<PromotionRequestApproveTo> outboundSegs) {
		this.outboundSegs = outboundSegs;
	}

	public List<PromotionRequestApproveTo> getPromoReqsForApproval() {
		if (direction == PromotionsInternalConstants.DIRECTION_OUTBOUND) {
			return outboundSegs;
		} else if (direction == PromotionsInternalConstants.DIRECTION_INBOUND) {
			return inboundSegs;
		} else {
			return new ArrayList<PromotionRequestApproveTo>();
		}
	}

}
