package com.isa.thinair.promotion.core.persistence.hibernate;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.promotion.api.model.BundledFarePeriod;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareSearchCriteriaTO;
import com.isa.thinair.promotion.core.persistence.dao.BundledFareDAO;

/**
 * 
 * @author rumesh
 * 
 *  
 */
public class BundledFareDAOImpl extends PlatformHibernateDaoSupport implements BundledFareDAO {
	
	private static Log log = LogFactory.getLog(BundledFareDAOImpl.class);
	
	private CacheManager cacheManager;
	
	private String cacheOperationCode;
	
	private boolean enabledCache;	

	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public String getCacheOperationCode() {
		return cacheOperationCode;
	}

	public void setCacheOperationCode(String cacheOperationCode) {
		this.cacheOperationCode = cacheOperationCode;
	}

	public boolean isEnabledCache() {
		return enabledCache;
	}

	public void setEnabledCache(boolean enabledCache) {
		this.enabledCache = enabledCache;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<BundledFare> searchBundledFareConfiguration(BundledFareSearchCriteriaTO searchCriteriaTO, Integer startPosition,
			Integer recSize) {

		StringBuilder countQueryStr = new StringBuilder("SELECT count(*) ");
		StringBuilder searchQueryStr = new StringBuilder("FROM BundledFare bundledFare");
		StringBuilder limitingCriteria = new StringBuilder(" WHERE");
		StringBuilder sortingCriteria = new StringBuilder(" ORDER BY bundledFare.bundledFareId");

		// boolean indicating if there were no filtering criteria (and clauses) added
		boolean noWhereClausesAdded = true;
		boolean andClausesAdded = false;

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBundledFareName())) {
			limitingCriteria.append(" bundledFare.bundledFareName = :bundledFareName ");
			noWhereClausesAdded = false;
			andClausesAdded = true;
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBundledCategoryID())) {
			if (andClausesAdded) {
				limitingCriteria.append(" and ");
			}
			limitingCriteria.append(" bundledFare.bundledCategoryID = :bundledCategoryID ");
			noWhereClausesAdded = false;
			andClausesAdded = true;
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getOndCode())) {
			if (andClausesAdded) {
				limitingCriteria.append(" and ");
			}
			limitingCriteria.append(" :ondCode  in elements(bundledFare.ondCodes) ");
			noWhereClausesAdded = false;
			andClausesAdded = true;
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBookingClass())) {
			if (andClausesAdded) {
				limitingCriteria.append(" and ");
			}
			limitingCriteria.append(" :bookingClass in elements(bundledFare.bookingClasses) ");
			noWhereClausesAdded = false;
			andClausesAdded = true;
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getStatus())) {
			if (andClausesAdded) {
				limitingCriteria.append(" and ");
			}
			limitingCriteria.append(" bundledFare.status = :status ");
			noWhereClausesAdded = false;
		}

		if (!noWhereClausesAdded) {
			searchQueryStr.append(limitingCriteria);
		}

		searchQueryStr.append(sortingCriteria);

		Query searchQuery = getSession().createQuery(searchQueryStr.toString());
		Query countQuery = getSession().createQuery(countQueryStr.append(searchQueryStr).toString());

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBundledFareName())) {
			searchQuery.setString("bundledFareName", searchCriteriaTO.getBundledFareName());
			countQuery.setString("bundledFareName", searchCriteriaTO.getBundledFareName());
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBundledCategoryID())) {
			searchQuery.setString("bundledCategoryID", searchCriteriaTO.getBundledCategoryID());
			countQuery.setString("bundledCategoryID", searchCriteriaTO.getBundledCategoryID());
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getOndCode())) {
			searchQuery.setParameter("ondCode", searchCriteriaTO.getOndCode());
			countQuery.setParameter("ondCode", searchCriteriaTO.getOndCode());
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getBookingClass())) {
			searchQuery.setParameter("bookingClass", searchCriteriaTO.getBookingClass());
			countQuery.setParameter("bookingClass", searchCriteriaTO.getBookingClass());
		}

		if (!StringUtil.isNullOrEmpty(searchCriteriaTO.getStatus())) {
			searchQuery.setString("status", searchCriteriaTO.getStatus());
			countQuery.setString("status", searchCriteriaTO.getStatus());
		}

		List<BundledFare> resultList = searchQuery.setFirstResult(startPosition).setMaxResults(recSize).list();

		Long totalRecords = (Long) countQuery.uniqueResult();

		return new Page<BundledFare>(totalRecords.intValue(), startPosition, startPosition + recSize, resultList);
	}

	@Override
	public BundledFare getBundledFare(Integer bundledFareId) throws CommonsDataAccessException {
		return (BundledFare) get(BundledFare.class, bundledFareId);
	}
	
	@Override
	public BundledFare getApplicableBundleFare(Integer bundledFarePeriodId) throws CommonsDataAccessException {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT bundledFare FROM BundledFare AS bundledFare ");
		queryBuilder.append(" JOIN bundledFare.bundledFarePeriods as servicePeriods ");
		queryBuilder.append(" WHERE servicePeriods.bundleFarePeriodId=:bundleFarePeriodId");

		Query query = getSession().createQuery(queryBuilder.toString());
		query.setInteger("bundleFarePeriodId", bundledFarePeriodId);

		BundledFare bundledFare = (BundledFare) query.uniqueResult();
		return bundledFare;
	}
	
	@Override
	public Integer saveBundledFareConfiguration(BundledFare bundledFare) {
		return (Integer) hibernateSave(bundledFare);
	}

	@Override
	public void updateBundledFareConfiguration(BundledFare bundledFare) {
		update(bundledFare);
	}

	@Override
	public boolean isBundledFareUsedByReservation(Set<Integer> bundledFarePeriodIds) {
		StringBuilder queryBuilder = new StringBuilder("SELECT count(pnr_seg_id) AS count");
		queryBuilder.append(" FROM t_pnr_segment WHERE bundled_fare_period_id IN (:bundledFarePeriodIds) and rownum=1");
		SQLQuery query = getSession().createSQLQuery(queryBuilder.toString());
		query.setParameterList("bundledFarePeriodIds", bundledFarePeriodIds);
		query.addScalar("count", IntegerType.INSTANCE);
		int count = ((Integer) query.uniqueResult()).intValue();
		return count > 0;
	}

	/**
	 * This will return the applicable bundle fares for a given criteria as follows.
	 *    1. Fist check in the cache and if exists return
	 *    2. Else hit the database and return
	 *    
	 *  Cache time and configuration are in promotion-cache-config.mod.xml file.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<BundledFare> getApplicableBundledFares(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria)
			throws CommonsDataAccessException {	
		
		List<BundledFare> bundleFares = null;
		boolean filterByPos = bundledFareSelectionCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_WEB
				&& !StringUtil.isNullOrEmpty(bundledFareSelectionCriteria.getPointOfSale());
		
		String cacheKey = getBFCacheKey(bundledFareSelectionCriteria, filterByPos);
		
		// check the applicable bundles in the cache first
		if (cacheKey != null && enabledCache) {	
			try {
				
				Cache cache = cacheManager.getCache(cacheOperationCode);				
				bundleFares = cache.get(cacheKey) == null ? null : (List<BundledFare>) cache.get(cacheKey).get();
				
			} catch (Exception ex) {				
				log.error("Error in cache Applicable BF : ", ex);
				// make shock proof
				bundleFares = Collections.emptyList();				
			}
		}
		

		// hit the db if not in cache
		if (bundleFares == null) {
			 bundleFares = getApplicableBundledFareFromDatabase(bundledFareSelectionCriteria, filterByPos);
			 if (cacheKey != null && enabledCache && !CollectionUtils.isEmpty(bundleFares)) {
				 try {
					 Cache cache = cacheManager.getCache(cacheOperationCode);
					 cache.put(cacheKey, bundleFares);
					 
				 } catch(Exception ex) {
					 log.error("Error in cache Applicable BF : ", ex);
				 }
			 }
		}
		
		// return empty list if null so only basic will show without issue
		if (bundleFares ==  null) {
			bundleFares = Collections.emptyList();
		}		
		
		return bundleFares;		
	}

	private String getBFCacheKey(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria, boolean filterByPos) {
		
		String CACHE_DATA_SEPARATOR = "_";
		
		StringBuilder cacheKey =  new StringBuilder();
		cacheKey.append(AppSysParamsUtil.getEnvironmentIdentificationCode()).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(cacheOperationCode).append(CACHE_DATA_SEPARATOR);
		cacheKey.append( CalendarUtil.formatDateYYYYMMDD(bundledFareSelectionCriteria.getFirstDepartureFlightDate())).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(bundledFareSelectionCriteria.getOndCode()).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(bundledFareSelectionCriteria.getOndCodeForAnyDepAirport()).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(bundledFareSelectionCriteria.getOndCodeForAnyArrAirport()).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(bundledFareSelectionCriteria.getOndCodeForAnyAirport()).append(CACHE_DATA_SEPARATOR);
		cacheKey.append(bundledFareSelectionCriteria.getChannelCode()).append(CACHE_DATA_SEPARATOR);
		
		if (filterByPos) {
			cacheKey.append(bundledFareSelectionCriteria.getPointOfSale()).append(CACHE_DATA_SEPARATOR);
			cacheKey.append(SalesChannelsUtil.SALES_CHANNEL_WEB);			
		}		
	
		return cacheKey.toString();
	}

	private List<BundledFare>
			getApplicableBundledFareFromDatabase(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria, boolean filterByPos) {
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("SELECT DISTINCT bundledFare FROM BundledFare AS bundledFare ");

		StringBuilder fromClause = new StringBuilder("JOIN bundledFare.ondCodes AS onds ");
		fromClause.append("JOIN bundledFare.applicableSalesChannels AS channels ");
		fromClause.append("JOIN bundledFare.bundledFarePeriods as servicePeriods ");
		if(filterByPos){
			fromClause.append("LEFT OUTER JOIN bundledFare.pointOfSale AS pos ");
		}

		StringBuilder whereClause = new StringBuilder("WHERE ");
		whereClause.append(" :flightDate ");
		whereClause.append("BETWEEN bundledFare.flightDateFrom AND bundledFare.flightDateTo ");
		whereClause.append(" AND ");
		whereClause.append(" :flightDate ");
		whereClause.append(" BETWEEN servicePeriods.flightDateFrom AND servicePeriods.flightDateTo ");
		whereClause.append("AND (onds LIKE :ond OR onds =:anyDepOnd OR onds =:anyArrOnd OR onds =:anyAnyOnd) ");
		whereClause.append("AND ( channels =:publicChannel OR channels =:channel) ");
		whereClause.append("AND bundledFare.status =:status ");
		whereClause.append("AND :flightDate-(bundledFare.cutOffTime/24) > SYSDATE ");
		if (filterByPos) {
			whereClause.append("AND (channels =:publicChannel OR ( channels =:webChannel  AND (pos is null OR pos =:pos) ) )");
		}

		String orderByClause = " ";	

		queryBuilder.append(fromClause).append(whereClause).append(orderByClause);
		String query = queryBuilder.toString();
				
		Date firstFlightDepartureDate = CalendarUtil.safeFormat(bundledFareSelectionCriteria.getFirstDepartureFlightDate(),
				CalendarUtil.PATTERN8);
		
		Query searchQuery = getSession().createQuery(query);
		searchQuery.setTimestamp("flightDate", firstFlightDepartureDate);
		searchQuery.setString("ond", bundledFareSelectionCriteria.getOndCode());
		searchQuery.setString("anyDepOnd", bundledFareSelectionCriteria.getOndCodeForAnyDepAirport());
		searchQuery.setString("anyArrOnd", bundledFareSelectionCriteria.getOndCodeForAnyArrAirport());
		searchQuery.setString("anyAnyOnd", bundledFareSelectionCriteria.getOndCodeForAnyAirport());
		searchQuery.setInteger("publicChannel", SalesChannelsUtil.SALES_CHANNEL_PUBLIC);
		searchQuery.setInteger("channel", bundledFareSelectionCriteria.getChannelCode());
		searchQuery.setString("status", BundledFare.ACTIVE);
		
		if (filterByPos) {
			searchQuery.setString("pos", bundledFareSelectionCriteria.getPointOfSale());
			searchQuery.setInteger("webChannel", SalesChannelsUtil.SALES_CHANNEL_WEB);
		}

		List<BundledFare> bundleFares = searchQuery.list();
		
		for (BundledFare bundledFare : bundleFares) {
			// Due to Xdoclet doesn't support many to one lazy loading
			bundledFare.getBundledFareCategory().getPriority();
		}
		return bundleFares;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<BundledFareFreeService> getOfferedServices(String pnrSegId) throws CommonsDataAccessException {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT {bffs.*} ");
		hql.append(" FROM t_pnr_segment ps, ");
		hql.append("  t_bundled_fare_period bfp, ");
		hql.append("  t_bundled_fare_free_service bffs ");
		hql.append("    WHERE ps.pnr_seg_id            =:pnrSegId ");
		hql.append("    AND ps.bundled_fare_period_id IS NOT NULL ");
		hql.append("    AND ps.bundled_fare_period_id  =bfp.bundled_fare_period_id ");
		hql.append("    AND bfp.bundled_fare_period_id =bffs.bundled_fare_period_id ");

		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.setParameter("pnrSegId", Integer.valueOf(pnrSegId));
		query.addEntity("bffs", BundledFareFreeService.class);

		return (List<BundledFareFreeService>) query.list();

	}

	@Override
	public boolean isChargeCodeLinkedToBundledFare(String chargeCode) throws CommonsDataAccessException {

		String queryString = "FROM BundledFare AS bundledFare WHERE bundledFare.chargeCode = :chargeCode";

		Query query = getSession().createQuery(queryString);
		query.setString("chargeCode", chargeCode);

		return !query.list().isEmpty();
	}

	@Override
	public boolean isBundledFareExistsWithDescription(String description) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT count(BUNDLED_FARE_ID) as count");
		queryBuilder.append(" FROM t_bundled_fare ");
		queryBuilder.append(" WHERE upper(BUNDLED_FARE_NAME)=:description");

		SQLQuery query = getSession().createSQLQuery(queryBuilder.toString());
		query.setParameter("description", description.toUpperCase());
		query.addScalar("count", IntegerType.INSTANCE);
		int count = ((Integer) query.uniqueResult()).intValue();
		return count > 0;
	}
	
	private void removeUnMatchingBundleServicePeriods(BundledFare bundledFare, Date firstFlightDepartureDate) {
		if (bundledFare != null) {
			Set<BundledFarePeriod> servicePeriods = bundledFare.getBundledFarePeriods();
			if (servicePeriods != null && !servicePeriods.isEmpty()) {
				Iterator<BundledFarePeriod> periodsIterator = servicePeriods.iterator();
				while (periodsIterator.hasNext()) {
					BundledFarePeriod bundleFarePeriod = periodsIterator.next();
					if (!CalendarUtil.isBetween(firstFlightDepartureDate, bundleFarePeriod.getFlightDateFrom(),
							bundleFarePeriod.getFlightDateTo())) {
						periodsIterator.remove();
					}
				}
				Set<BundledFarePeriod> filteredPeriods = bundledFare.getBundledFarePeriods();
				if (filteredPeriods != null && !filteredPeriods.isEmpty()) {
					bundledFare.setPeriodSelected(true);
				}
			}
		}
	}
	
	
	private void removeUnMatchingBundleServicePeriods(BundledFare bundledFare, Integer bundledFarePeriodId) {
		if (bundledFare != null) {
			Set<BundledFarePeriod> servicePeriods = bundledFare.getBundledFarePeriods();
			if (servicePeriods != null && !servicePeriods.isEmpty()) {
				Iterator<BundledFarePeriod> periodsIterator = servicePeriods.iterator();
				while (periodsIterator.hasNext()) {
					BundledFarePeriod bundleFarePeriod = periodsIterator.next();
					if (!bundleFarePeriod.getBundleFarePeriodId().equals(bundledFarePeriodId)) {
						periodsIterator.remove();
					}
				}
				Set<BundledFarePeriod> bundleFarePeriods = bundledFare.getBundledFarePeriods();
				if (bundleFarePeriods != null && !bundleFarePeriods.isEmpty()) {
					bundledFare.setPeriodSelected(true);
				}
			}
		}
	}

}
