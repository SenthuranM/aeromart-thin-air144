package com.isa.thinair.promotion.core.bl.voucher;

import java.util.List;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * @author chanaka
 *
 *@isa.module.command name="createVouchers"
 */
public class CreateVouchers extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() throws ModuleException {
		@SuppressWarnings("unchecked")
		List<VoucherDTO> voucherDTOs = (List<VoucherDTO>) getParameter(CommandParamNames.VOUCHER_LIST_DETAILS);
		VoucherBO.saveVouchers(voucherDTOs);
		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}
	
}
