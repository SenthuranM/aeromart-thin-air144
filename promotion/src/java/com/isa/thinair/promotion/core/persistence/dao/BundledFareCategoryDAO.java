package com.isa.thinair.promotion.core.persistence.dao;

import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.model.BundledFareCategory;
import com.isa.thinair.promotion.api.to.BundleCategorySearch;

import java.util.List;


public interface BundledFareCategoryDAO {

    BundleFareCategoryDTO saveBundleFareCategory(BundleFareCategoryDTO category, UserPrincipal up)  throws CommonsDataAccessException;

    BundledFareCategory getCategory(Integer id)  throws CommonsDataAccessException;

    Page<BundleFareCategoryDTO> searchCategories(BundleCategorySearch bundleSearch, List<Integer> ids) throws CommonsDataAccessException;

    boolean deleteCategory(BundleFareCategoryDTO category) throws CommonsDataAccessException;

    List<String> getAllCategoryPriorities() throws CommonsDataAccessException;
}
