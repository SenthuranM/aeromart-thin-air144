package com.isa.thinair.promotion.core.bl.lms;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.promotion.core.util.LMSHelperUtil;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.LMSGravtyWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;

public class GravtyLoyaltyManagementBL extends AbstractLoyalatyManagementBL {

	public static final String MEMBER_ID_KEY = "&memberId=";
	public static final String LANGUAGE_ID_KEY = "&LanguageId=";
	public static final String CROSS_LOGIN_KEY = "&CrossLoginKey=";

	@Override
	public String createLoyaltyMember(Customer customer) throws ModuleException {
		LmsMember lmsMember = customer.getLMSMemberDetails();
		String sbInternalMemberId = PromotionModuleServiceLocator.getWSClientBD().enrollMemberWithEmail(customer);
		if (!StringUtils.isEmpty(sbInternalMemberId)) {
			String memberAccountId = lmsMember.getFfid();
			// call set password
			String password = lmsMember.getPassword();
			try {
				PromotionModuleServiceLocator.getWSClientBD().setMemberPassword(sbInternalMemberId, password);
			} catch (Exception me) {
				if (log.isInfoEnabled()) {
					log.info("LMS member enrollment succeed for " + memberAccountId + ". Other user details saving failed.");
				}
				log.error("Error in saving password of LMS member ", me);
			}
		}
		return sbInternalMemberId;
	}

	@Override
	public void updateMemberProfile(Customer customer) throws ModuleException {
		if (customer.getLMSMemberDetails() != null) {
			LmsMember lmsMember = customer.getLMSMemberDetails();
			if (PromotionModuleServiceLocator.getWSClientBD().getLoyaltyMemberCoreDetails(lmsMember.getFfid()).getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				 PromotionModuleServiceLocator.getWSClientBD().updateMember(customer);
			}
		}

	}

	@Override
	public ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(false);

		BigDecimal pointToCurrencyConversionRate = redeemLoyaltyPointsReq.getLoyaltyPointsToCurrencyConversionRate();

		// Issue variable rewards
		Map<String, BigDecimal> productCurrencyValue = LMSHelperUtil.convertLoyaltyPointsToCurrencyValue(
				redeemLoyaltyPointsReq.getProductPoints(), pointToCurrencyConversionRate);

		String locationExtReference = LMSCommonUtil.getLocationExternalReference(redeemLoyaltyPointsReq.getAppIndicator());

		IssueRewardRequest issueRewardRequest = LMSHelperUtil.populateIssueRewardRequest(
				redeemLoyaltyPointsReq.getMemberAccountId(), locationExtReference, productCurrencyValue);

		issueRewardRequest.setMemberEnrollingCarrier(redeemLoyaltyPointsReq.getMemberEnrollingCarrier());
		issueRewardRequest.setMemberExternalId(redeemLoyaltyPointsReq.getMemberExternalId());

		IssueRewardResponse issueRewardResponse = PromotionModuleServiceLocator.getWSClientBD().issueVariableRewards(
				issueRewardRequest, redeemLoyaltyPointsReq.getPnr(), redeemLoyaltyPointsReq.getProductPoints());

		if (issueRewardResponse != null) {
			String[] rewardIds = issueRewardResponse.getIssuedRewardIds();
			response.addResponceParam(ResponceCodes.ResponseParams.LOYALTY_REWARD_IDS, rewardIds);
			response.setSuccess(true);

			log.info("Redeem Issue success, Reward IDs: " + Arrays.toString(rewardIds) + " for Loyalty Member: "
					+ redeemLoyaltyPointsReq.getMemberAccountId() + " pnr " + redeemLoyaltyPointsReq.getPnr());
		}

		return response;

	}

	@Override
	public ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException {
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (rewardIds != null && rewardIds.length > 0) {
			response.setSuccess(true);
		}

		return response;
	}

	@Override
	public ServiceResponce cancelRedeemedLoyaltyPoints(String pnr, String[] rewardIds, String memberAccountId)
			throws ModuleException {
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (rewardIds != null && rewardIds.length > 0) {
			try {

				log.info("CALLING REWARD CANCELLATION FOR REWARD IDS : "+ Arrays.toString(rewardIds));

				boolean cancelResponse = PromotionModuleServiceLocator.getWSClientBD().cancelMemberRewards(pnr, rewardIds,
						memberAccountId);
				PromotionModuleServiceLocator.getReservationBD().changeLMSBlockCreditStatusToCancel(rewardIds, pnr,
						memberAccountId);
				response.setSuccess(cancelResponse);

			} catch (ModuleException me) {
				log.error("Cancellation Failed for redeem Ids " + Arrays.toString(rewardIds));
				response.setSuccess(false);
				if (!AppSysParamsUtil.proceedingPaymentWithLmsCreditCancelServiceFailiure()) {
					throw new ModuleException(me, me.getExceptionCode());
				}
			}
		}
		return response;
	}

	@Override
	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException {
		GravtyFileUploader gravtyFileUploader = new GravtyFileUploader();
		gravtyFileUploader.uploadPendingFiles(fileType);
	}

	@Override
	public String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException {
		String url = null;
		String memberExternalRef = lmsMember.getMemberExternalId();
		try {

			url = LMSGravtyWebServiceInvoker.gravtyConfig.getDashboardUrls().getProperty(menuItemIntRef);
			url = url + LANGUAGE_ID_KEY + getMemberCommunicationLanguage(lmsMember);

			if (!StringUtils.isEmpty(memberExternalRef)) {
				MemberRemoteLoginResponseDTO memberLoginResponse = PromotionModuleServiceLocator.getWSClientBD().memberLogin(
						memberExternalRef, lmsMember.getPassword());
				url = url + MEMBER_ID_KEY + memberExternalRef + CROSS_LOGIN_KEY + memberLoginResponse.getToken();
			}

		} catch (Exception me) {
			log.error(
					"LMS member remote Login Failed for lms member Id / emailid:" + memberExternalRef + " / "
							+ lmsMember.getFfid(), me);
		}
		return url;
	}

	private String getMemberCommunicationLanguage(LmsMember lmsMember) {
		String communicationLanguage = LMSGravtyWebServiceInvoker.gravtyConfig.getSupportingLanguages().getProperty(
				lmsMember.getLanguage());
		communicationLanguage = StringUtils.isEmpty(communicationLanguage) ? LMSGravtyWebServiceInvoker.gravtyConfig
				.getDefaultLanguage() : communicationLanguage;
		return communicationLanguage;
	}

}
