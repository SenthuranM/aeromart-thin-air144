package com.isa.thinair.promotion.core.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;
import com.isa.thinair.promotion.api.model.PaxQuantity;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

/**
 * Helper/Utility class to manage miscellaneous operations related to AnciOfferCriteria.
 * 
 * @author thihara
 * 
 */
public class AnciOfferHelper {
	/**
	 * Transforms an {@link AnciOfferCriteriaTO} into an {@link AnciOfferCriteria}
	 * 
	 * @param anciCriteriaDTO
	 *            The anci offer to be transformed.
	 * @return The transformed anci offer.
	 */
	public static AnciOfferCriteria transformToAnciOfferCriteria(AnciOfferCriteriaTO anciCriteriaDTO) {
		AnciOfferCriteria criteria = new AnciOfferCriteria();

		criteria.setAnciTemplateType(anciCriteriaDTO.getAnciTemplateType());
		criteria.setApplicableForOneWay(anciCriteriaDTO.getApplicableForOneWay());
		criteria.setApplicableForReturn(anciCriteriaDTO.getApplicableForReturn());
		criteria.setEligibleForOneWay(anciCriteriaDTO.getEligibleForOneWay());
		criteria.setEligibleForReturn(anciCriteriaDTO.getEligibleForReturn());
		criteria.setApplicableFrom(anciCriteriaDTO.getApplicableFrom());
		criteria.setApplicableTo(anciCriteriaDTO.getApplicableTo());

		criteria.setEligiblePaxCounts(convertToPaxQuantity(anciCriteriaDTO.getEligiblePaxCounts()));

		criteria.setBookingClasses(anciCriteriaDTO.getBookingClasses());
		criteria.setApplyToFlexiOnly(anciCriteriaDTO.getApplyToFlexiOnly());
		criteria.setId(anciCriteriaDTO.getId());
		criteria.setLccs(anciCriteriaDTO.getLccs());
		criteria.setOnds(anciCriteriaDTO.getOnds());
		criteria.setTemplateID(anciCriteriaDTO.getTemplateID());
		criteria.setVersion(anciCriteriaDTO.getVersion());
		criteria.setActive(anciCriteriaDTO.getActive());

		criteria.setApplicableOutboundDaysOfTheWeek(getStringViewForApplicableDays(anciCriteriaDTO
				.getApplicableOutboundDaysOfTheWeek()));
		criteria.setApplicableInboundDaysOfTheWeek(getStringViewForApplicableDays(anciCriteriaDTO
				.getApplicableInboundDaysOfTheWeek()));

		return criteria;
	}

	/**
	 * Transforms an {@link AnciOfferCriteria} into an {@link AnciOfferCriteriaTO}
	 * 
	 * @param anciCriteria
	 *            Anci offer to be transformed.
	 * @return Transformed anci offer.
	 */
	public static AnciOfferCriteriaTO transformToAnciOfferCriteriaTO(AnciOfferCriteria anciCriteria) {
		AnciOfferCriteriaTO criteriaTO = new AnciOfferCriteriaTO();

		criteriaTO.setAnciTemplateType(anciCriteria.getAnciTemplateType());
		criteriaTO.setApplicableForOneWay(anciCriteria.getApplicableForOneWay());
		criteriaTO.setApplicableForReturn(anciCriteria.getApplicableForReturn());
		criteriaTO.setEligibleForOneWay(anciCriteria.getEligibleForOneWay());
		criteriaTO.setEligibleForReturn(anciCriteria.getEligibleForReturn());
		criteriaTO.setApplicableFrom(anciCriteria.getApplicableFrom());
		criteriaTO.setApplicableTo(anciCriteria.getApplicableTo());

		criteriaTO.setEligiblePaxCounts(convertToPaxCounts(anciCriteria.getEligiblePaxCounts()));

		criteriaTO.setBookingClasses(anciCriteria.getBookingClasses());
		criteriaTO.setApplyToFlexiOnly(anciCriteria.getApplyToFlexiOnly());
		criteriaTO.setId(anciCriteria.getId());
		criteriaTO.setLccs(anciCriteria.getLccs());
		criteriaTO.setOnds(anciCriteria.getOnds());
		criteriaTO.setTemplateID(anciCriteria.getTemplateID());
		criteriaTO.setVersion(anciCriteria.getVersion());
		criteriaTO.setActive(anciCriteria.getActive());

		criteriaTO.setI18nMessageKeyForName(anciCriteria.getAnciOfferNameI18nMessageKey());
		criteriaTO.setI18nMessageKeyForDescription(anciCriteria.getAnciOfferDescriptionI18nMessageKey());

		criteriaTO.setApplicableOutboundDaysOfTheWeek(getSetViewForApplicableDays(anciCriteria
				.getApplicableOutboundDaysOfTheWeek()));
		criteriaTO
				.setApplicableInboundDaysOfTheWeek(getSetViewForApplicableDays(anciCriteria.getApplicableInboundDaysOfTheWeek()));

		return criteriaTO;
	}

	/**
	 * Converts a {@link Page} of AnciOfferCriteria into an Page of AnciOfferCriteriaTO
	 * 
	 * @param pageToConvert
	 *            Page to convert.
	 * @return Converted Page.
	 */
	public static Page<AnciOfferCriteriaTO> convertPageToDTO(Page<AnciOfferCriteria> pageToConvert) {
		Page<AnciOfferCriteriaTO> convertedPage = new Page<AnciOfferCriteriaTO>(pageToConvert.getTotalNoOfRecords(),
				pageToConvert.getStartPosition(), pageToConvert.getEndPosition(),
				toAnciOfferDTOCollection(pageToConvert.getPageData()));

		return convertedPage;
	}

	/**
	 * Converts a Collection of {@link AnciOfferCriteriaTO} into a Collection of {@link AnciOfferCriteria}
	 * 
	 * @param criteriaCollection
	 *            The collection to be converted.
	 * @return The converted collection.
	 */
	public static Collection<AnciOfferCriteriaTO> toAnciOfferDTOCollection(Collection<AnciOfferCriteria> criteriaCollection) {
		Collection<AnciOfferCriteriaTO> dtoCollection = new ArrayList<AnciOfferCriteriaTO>();
		for (AnciOfferCriteria criteria : criteriaCollection) {
			dtoCollection.add(transformToAnciOfferCriteriaTO(criteria));
		}
		return dtoCollection;
	}

	/**
	 * Parses a Set of pax count strings into a Set of {@link PaxQuantity} objects. The pax count string should be in
	 * the form of adult count, child count, infant count (1,0,0).
	 * 
	 * @param paxCounts
	 *            The pax count string Set to be parsed.
	 * @return Parsed Set of PaxQuantity.
	 */
	public static Set<PaxQuantity> convertToPaxQuantity(Set<String> paxCounts) {

		Set<PaxQuantity> paxQuantities = new HashSet<PaxQuantity>();

		for (String paxCount : paxCounts) {
			PaxQuantity paxQty = new PaxQuantity();

			String[] qtyStrs = paxCount.split(PaxQuantity.DISPLAY_SEPERATOR);
			paxQty.setAdults(Integer.parseInt(qtyStrs[0]));
			paxQty.setChildren(Integer.parseInt(qtyStrs[1]));
			paxQty.setInfants(Integer.parseInt(qtyStrs[2]));

			paxQuantities.add(paxQty);
		}

		return paxQuantities;
	}

	/**
	 * Parses a Set of {@link PaxQuantity} into an Set of their String representations.
	 * 
	 * @param paxQuantities
	 *            Set of PaxQuantity objects to be parsed.
	 * @return Set of parsed pax quantities.
	 */
	public static Set<String> convertToPaxCounts(Set<PaxQuantity> paxQuantities) {
		Set<String> paxCounts = new HashSet<String>();
		for (PaxQuantity paxQuantity : paxQuantities) {
			String paxCount = paxQuantity.getAdults() + PaxQuantity.DISPLAY_SEPERATOR + paxQuantity.getChildren()
					+ PaxQuantity.DISPLAY_SEPERATOR + paxQuantity.getInfants();
			paxCounts.add(paxCount);
		}
		return paxCounts;
	}

	/**
	 * Retrieves the I18nMessage keys from an {@link AnciOfferCriteria} Collection.
	 * 
	 * @param anciOffers
	 *            Collection of AnciOfferCriteria s whose I18nMessageKeys need to be retrieved.
	 * @return A Collection of the retrieved I18nMessage keys.
	 */
	public static Collection<String> getI18nMessageKeys(Collection<AnciOfferCriteria> anciOffers) {
		Collection<String> msgKeys = new HashSet<String>();
		for (AnciOfferCriteria anciOffer : anciOffers) {
			msgKeys.add(anciOffer.getAnciOfferNameI18nMessageKey());
			msgKeys.add(anciOffer.getAnciOfferDescriptionI18nMessageKey());
		}
		return msgKeys;
	}

	/**
	 * Sets the translation message details to a Collection of {@link AnciOfferCriteriaTO}
	 * 
	 * @param criterias
	 *            Anci offer criterias to be populated with the translation details.
	 * @param translations
	 *            The translation data in the following format. Map<i18nMessage key, Map<language, translated content>>
	 * @return same criterias reference with the translations details now populated.
	 */
	public static Collection<AnciOfferCriteriaTO> setTranslatedMessages(Collection<AnciOfferCriteriaTO> criterias,
			Map<String, Map<String, String>> translations) {

		for (AnciOfferCriteriaTO criteria : criterias) {

			criteria.setNameTranslations(translations.get(criteria.getI18nMessageKeyForName()));
			criteria.setDescriptionTranslations(translations.get(criteria.getI18nMessageKeyForDescription()));
		}

		return criterias;
	}

	/**
	 * Parses a String representation of a day of the week into a Set of Integers by splitting the string on "," (comma)
	 * and then parsing each element into an Integer.
	 * 
	 * @param applicableDaysStringView
	 *            A String with the days of the week seperated with commas. (1,4,5). The values should correspond to
	 *            values of the {@link Calendar#DAY_OF_WEEK} field.
	 * @return A parsed Set of Integers.
	 */
	public static Set<Integer> getSetViewForApplicableDays(String applicableDaysStringView) {
		Set<Integer> setView = new HashSet<Integer>(8);

		if (applicableDaysStringView != null) {
			String applicableDays[] = applicableDaysStringView.split(",");
			for (String applicableDay : applicableDays) {
				setView.add(Integer.parseInt(applicableDay));
			}
		}
		return setView;
	}

	/**
	 * Parses a Set of Integers into a String representation by joining them via "," (comma)
	 * 
	 * @param applicableDaysSetView
	 *            Set of Integers to be parsed. Or null if the Set is null or is empty.
	 * @return Parsed String representation. Format : 1,2,4
	 */
	public static String getStringViewForApplicableDays(Set<Integer> applicableDaysSetView) {
		if (applicableDaysSetView == null || applicableDaysSetView.isEmpty()) {
			return null;
		}

		StringBuilder stringViewBuilder = new StringBuilder();
		boolean isFirst = true;
		for (Integer applicableDay : applicableDaysSetView) {
			if (!isFirst) {
				stringViewBuilder.append(",");
			}
			isFirst = false;

			stringViewBuilder.append(applicableDay.toString());
		}
		return stringViewBuilder.toString();
	}
}
