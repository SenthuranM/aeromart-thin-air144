package com.isa.thinair.promotion.core.bl.campaign;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.CampaignTO;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * @author chanaka
 * 
 * @isa.module.command name="createCampaign"
 *
 */
public class CreateCampaign extends DefaultBaseCommand{

	@Override
	public ServiceResponce execute() throws ModuleException {
		
		CampaignTO campaignTO = (CampaignTO)getParameter(CommandParamNames.CAMPAIGN_DTO);		
		CampaignBO.createCampaign(campaignTO);
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		return response;
	}

}
