package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.AnciOfferBD;

/**
 * Remote interface of the AnciOfferBD
 * 
 * @author thihara
 * 
 */
@Remote
public interface AnciOfferBDRemote extends AnciOfferBD {

}
