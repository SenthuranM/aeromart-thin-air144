package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.promotion.api.service.VoucherBD;

/**
 * @author chethiya
 *
 */
@Local
public interface VoucherBDLocal extends VoucherBD {

}
