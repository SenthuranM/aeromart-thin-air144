package com.isa.thinair.promotion.core.persistence.hibernate.flexidate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.dto.ReservationSegSummaryTo;
import com.isa.thinair.promotion.core.dto.ReservationSummaryTo;
import com.isa.thinair.promotion.core.persistence.dao.FlexiDatePromotionAdminDao;
import com.isa.thinair.promotion.core.persistence.hibernate.PromotionAdministrationDaoImpl;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * @isa.module.dao-impl dao-name="FlexiDatePromotionAdminDao"
 */
public class FlexiDatePromotionAdminDaoImpl extends PromotionAdministrationDaoImpl implements FlexiDatePromotionAdminDao {

	public List<ReservationSummaryTo> getBasicDataForFlexiDateApproval(int flightSegId) throws ModuleException {

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("		SELECT r.pnr pnr , r.total_pax_count pax_count , r.total_pax_child_count child_count , rs.pnr_seg_id pnr_seg_id , fs.flight_id flight_id , 	")
				.append("		    fs.segment_code seg_code , fs.est_time_departure_zulu  dep_date , poc.charge_group_code charge_group ,	")
				.append("		    SUM(poc.amount) amount , prc.req_param req_param , MIN(prc.param_value) param_val , bc.logical_cabin_class_code logicalcc ,	")
				.append("		    rs.pnr_seg_id pnr_seg_id , r.version version_id , fs.flt_seg_id flt_seg_id , pr.promotion_request_id promo_req_id ,")
				.append("          pr.promotion_id promo_id ,fs.est_time_departure_local dep_date_local, bc.allocation_type allocation_type	")
				.append("		FROM t_reservation r , t_pnr_segment rs , t_pnr_pax_fare_segment pfs , t_pnr_pax_ond_charges poc , t_promotion_request pr ,	")
				.append("		    t_promotion_request_config prc , t_flight_segment fs , t_booking_class bc	")
				.append("		WHERE r.pnr = rs.pnr AND rs.pnr_seg_id = pr.pnr_seg_id(+) AND rs.pnr_seg_id = pfs.pnr_seg_id AND pfs.ppf_id = poc.ppf_id	")
				.append("		    AND pr.promotion_request_id = prc.promotion_request_id (+) AND rs.flt_seg_id = fs.flt_seg_id AND r.pnr IN	")
				.append("		        (SELECT rs.pnr FROM t_pnr_segment rs , t_promotion_request pr	")
				.append("		            WHERE rs.pnr_seg_id = pr.pnr_seg_id AND rs.flt_seg_id = :flightSegId )	")
				.append("		    AND pfs.booking_code = bc.booking_code AND rs.status = 'CNF' AND pr.status = 'ACT'	")
				.append("            AND fs.est_time_departure_zulu > SYSDATE +  ")
				.append(AppSysParamsUtil.getFlexiDatePromotionApproveCutOffTime())
				.append("		GROUP BY r.pnr , r.total_pax_count , r.total_pax_child_count , rs.pnr_seg_id , fs.flight_id , fs.segment_code , fs.est_time_departure_zulu , prc.req_param , 	")
				.append("		    poc.charge_group_code , bc.logical_cabin_class_code , rs.pnr_seg_id, r.version , fs.flt_seg_id , pr.promotion_request_id , ")
				.append(" 			pr.promotion_id , fs.est_time_departure_local, bc.allocation_type	")
				.append("		ORDER BY pnr , dep_date	");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("flightSegId", flightSegId);
		query.addScalar("pnr", StringType.INSTANCE);
		query.addScalar("pax_count", IntegerType.INSTANCE);
		query.addScalar("child_count", IntegerType.INSTANCE);
		query.addScalar("pnr_seg_id", IntegerType.INSTANCE);
		query.addScalar("flight_id", IntegerType.INSTANCE);
		query.addScalar("seg_code", StringType.INSTANCE);
		query.addScalar("dep_date", TimestampType.INSTANCE);
		query.addScalar("charge_group", StringType.INSTANCE);
		query.addScalar("amount", BigDecimalType.INSTANCE);
		query.addScalar("req_param", StringType.INSTANCE);
		query.addScalar("param_val", StringType.INSTANCE);
		query.addScalar("logicalcc", StringType.INSTANCE);
		query.addScalar("pnr_seg_id", IntegerType.INSTANCE);
		query.addScalar("version_id", LongType.INSTANCE);
		query.addScalar("flt_seg_id", IntegerType.INSTANCE);
		query.addScalar("promo_req_id", IntegerType.INSTANCE);
		query.addScalar("promo_id", IntegerType.INSTANCE);
		query.addScalar("dep_date_local", TimestampType.INSTANCE);
		query.addScalar("allocation_type", StringType.INSTANCE);

		List<Object[]> results = query.list();

		return toReservationSummaryToList(results, flightSegId);
	}

	private List<ReservationSummaryTo> toReservationSummaryToList(List<Object[]> results, int flightSegId) throws ModuleException {

		List<ReservationSummaryTo> reservationSummaryTos = new ArrayList<ReservationSummaryTo>();

		// < PNR , ResSummary >
		Map<String, ReservationSummaryTo> reservations = new HashMap<String, ReservationSummaryTo>();
		// < PNR , < ResSegId, ResSegSummary > >
		Map<String, Map<Integer, ReservationSegSummaryTo>> resSegs = new HashMap<String, Map<Integer, ReservationSegSummaryTo>>();
		String pnr;
		ReservationSummaryTo resSummaryTo;
		int resSegId;
		ReservationSegSummaryTo resSegSummaryTo;
		Map<Integer, ReservationSegSummaryTo> resSegSummaryToMapping;
		PromotionRequest promotionRequest;

		for (Object[] singleRec : results) {
			pnr = (String) singleRec[0];
			if (!reservations.containsKey(pnr)) {
				resSummaryTo = new ReservationSummaryTo();
				resSummaryTo.setPnr(pnr);
				resSummaryTo.putPaxCount(PaxTypeTO.ADULT, (Integer) singleRec[1]);
				resSummaryTo.putPaxCount(PaxTypeTO.CHILD, (Integer) singleRec[2]);
				resSummaryTo.setVersionId((Long) singleRec[13]);
				resSummaryTo.setLogicalCcCode((String) singleRec[11]);
				reservations.put(pnr, resSummaryTo);
			} else {
				resSummaryTo = reservations.get(pnr);
			}

			resSegId = (Integer) singleRec[3];
			if (!resSegs.containsKey(pnr)) {
				resSegSummaryToMapping = new HashMap<Integer, ReservationSegSummaryTo>();
				resSegSummaryTo = new ReservationSegSummaryTo();
				resSegSummaryTo.setResSegId(resSegId);
				resSegSummaryTo.setFlightId((Integer) singleRec[4]);
				resSegSummaryTo.setSegmentCode((String) singleRec[5]);
				resSegSummaryTo.setDepatureDate((Date) singleRec[6]);
				resSegSummaryTo.setDepartureDateLocal((Date) singleRec[17]);
				resSegSummaryTo.setResSegId((Integer) singleRec[12]);
				resSegSummaryTo.setFlightSegId((Integer) singleRec[14]);
				resSegSummaryTo.setBcAllocationType((String) singleRec[18]);
				resSegSummaryToMapping.put(resSegId, resSegSummaryTo);

				resSegs.put(pnr, resSegSummaryToMapping);

			} else if (!resSegs.get(pnr).containsKey(resSegId)) {
				resSegSummaryTo = new ReservationSegSummaryTo();
				resSegSummaryTo.setResSegId(resSegId);
				resSegSummaryTo.setFlightId((Integer) singleRec[4]);
				resSegSummaryTo.setSegmentCode((String) singleRec[5]);
				resSegSummaryTo.setDepatureDate((Date) singleRec[6]);
				resSegSummaryTo.setDepartureDateLocal((Date) singleRec[17]);
				resSegSummaryTo.setResSegId((Integer) singleRec[12]);
				resSegSummaryTo.setFlightSegId((Integer) singleRec[14]);
				resSegSummaryTo.setBcAllocationType((String) singleRec[18]);

				resSegSummaryToMapping = resSegs.get(pnr);
				resSegSummaryToMapping.put(resSegId, resSegSummaryTo);
			} else {
				resSegSummaryToMapping = resSegs.get(pnr);
				resSegSummaryTo = resSegSummaryToMapping.get(resSegId);
			}

			resSegSummaryTo.putCharge((String) singleRec[7], (BigDecimal) singleRec[8]);

			// Promotion Request might not being sent every res-seg
			if (singleRec[9] != null && singleRec[10] != null) {
				resSegSummaryTo.putPromoRequestParam((String) singleRec[9], (String) singleRec[10]);
			}
			if (singleRec[15] != null && resSegSummaryTo.getPromotionRequestId() == 0) {
				resSegSummaryTo.setPromotionRequestId((Integer) singleRec[15]);
				resSegSummaryTo.setPromotionId((Integer) singleRec[16]);
			}
		}

		Pair<List<ReservationSegSummaryTo>, List<ReservationSegSummaryTo>> seperatedInboundOutboundSegs;
		List<String> segCodes;
		int promoReqId = 0;
		for (String pnr_1 : resSegs.keySet()) {
			segCodes = new ArrayList<String>();
			resSegSummaryToMapping = resSegs.get(pnr_1);
			seperatedInboundOutboundSegs = PromotionsUtils
					.seperateOutboundAndInboundSegments(new ArrayList<ReservationSegSummaryTo>(resSegSummaryToMapping.values()));

			resSummaryTo = reservations.get(pnr_1);
			resSummaryTo.setOutboundSegs(seperatedInboundOutboundSegs.getLeft());
			resSummaryTo.setInboundSegs(seperatedInboundOutboundSegs.getRight());

			for (ReservationSegSummaryTo summaryTo : resSegSummaryToMapping.values()) {
				if (summaryTo.getFlightSegId() == flightSegId) {
					resSummaryTo.setPromoApproveDirection(summaryTo.isInbound()
							? PromotionsInternalConstants.DIRECTION_INBOUND
							: PromotionsInternalConstants.DIRECTION_OUTBOUND);
					promoReqId = summaryTo.getPromotionRequestId();
					for (ReservationSegSummaryTo segSummary : summaryTo.isInbound()
							? resSummaryTo.getInboundSegs()
							: resSummaryTo.getOutboundSegs()) {
						segCodes.add(segSummary.getSegmentCode());
					}
					break;

				}
			}

			if (promoReqId != 0) {
				promotionRequest = findEntity(PromotionRequest.class, promoReqId);

				DefaultBaseCommand refundCharge = PromotionsHandlerFactory
						.getPromotionChargesRefundsHandler(PromotionType.PROMOTION_FLEXI_DATE);
				refundCharge.setParameter(CommandParamNames.PROMOTION_REQUEST, promotionRequest);
				refundCharge.setParameter(CommandParamNames.PROMO_PROCESS_CATEGORY, CommandParamNames.PROCESS_REFUNDS);
				refundCharge.setParameter(CommandParamNames.REQUEST_TYPE, CommandParamNames.REQUEST_CALCULATION_ONLY);
				refundCharge.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODES, segCodes);

				ServiceResponce resp = refundCharge.execute();
				List<OndPromotionCharge> charges = (List<OndPromotionCharge>) resp
						.getResponseParam(CommandParamNames.APPLICABLE_CHARGES_REFUNDS);

				for (ReservationSegSummaryTo summaryTo : resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_OUTBOUND
						? resSummaryTo.getOutboundSegs()
						: resSummaryTo.getInboundSegs()) {
					for (OndPromotionCharge charge : charges) {
						summaryTo.putPromotionChargesAndRefunds(charge.getChargeCode(), new BigDecimal(charge.getAmount()));
					}
				}
			}
			reservationSummaryTos.add(resSummaryTo);
		}

		return reservationSummaryTos;
	}
}
