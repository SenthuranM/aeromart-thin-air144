package com.isa.thinair.promotion.core.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;

public class PromotionValidatorUtil {

	public static boolean isConfirmedFutureSegment(ReservationSegmentDTO segment, PromotionTemplateTo promoTemplate) {
		boolean match = false;
		if (segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
			if (segment.getZuluDepartureDate().getTime() > CalendarUtil.getCurrentSystemTimeInZulu().getTime()) {
				match = true;
			}
		}
		return match;
	}

	public static boolean isBetweenPromoValidity(Date zuluDepDate, PromotionTemplateTo promo) {
		return CalendarUtil.isBetweenIncludingLimits(CalendarUtil.getStartTimeOfDate(zuluDepDate), promo.getPromoStartDate(),
				promo.getPromoEndDate());
	}

	public static List<String> resolveMatchingSegments(List<String> promoRequestedSegs) {

		String routeCode = PromotionsUtils.getOndCode(promoRequestedSegs);

		Pair<String, String> origAndDest = PromotionsUtils.getOriginAndDestinationAirports(routeCode);
		List<String> matchingOrder = new ArrayList<String>();

		if (promoRequestedSegs.size() == 1) {
			// For Single-Seg ---- A/B, Multi seg
			// A/B > A/ALL > ALL/B > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder
					.add(origAndDest.getLeft() + PromotionsInternalConstants.OND_DELIM + PromotionsInternalConstants.OND_ALL);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ origAndDest.getRight());
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);

		} else {
			// For Connection-Seg ---- A/B + B/C
			// A/B/C > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);

		}
		return matchingOrder;

	}

	public static List<String> retrievePromoFilteredSegs(List<String> boundSegs, List<ReservationSegmentDTO> boundDetailSegs,
			PromotionTemplateTo promoTemplate) {

		List<String> filterdOutbound = new ArrayList<String>();

		if (!boundDetailSegs.isEmpty()) {

			List<String> outboundPossibleCombinations = resolveMatchingSegments(boundSegs);
			List<String> promoDefinedRoutes = promoTemplate.getRoutes();

			for (ReservationSegmentDTO resSeg : boundDetailSegs) {
				for (String possibleMatchingSegment : outboundPossibleCombinations) {
					if (promoDefinedRoutes.contains(possibleMatchingSegment)
							&& isBetweenPromoValidity(resSeg.getZuluDepartureDate(), promoTemplate)) {
						filterdOutbound.add(resSeg.getSegmentCode());
						break;
					}

				}
			}
		}

		return filterdOutbound;

	}

}
