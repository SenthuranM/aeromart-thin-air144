package com.isa.thinair.promotion.core.persistence.hibernate;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.promotion.api.model.BundledFarePeriodImage;
import com.isa.thinair.promotion.core.persistence.dao.BundledFareDisplaySettingsDao;

/**
 * @isa.module.dao-impl dao-name="BundledFareDisplaySettingsDao"
 */
public class BundledFareDisplaySettingsDaoImpl extends PlatformHibernateDaoSupport implements BundledFareDisplaySettingsDao {

	@Override
	public List<BundledFarePeriodImage> getBundledFarePeriodImages(Set<Integer> bundleFarePeriodIds)
			throws CommonsDataAccessException {

		StringBuilder hql = new StringBuilder();
		hql.append("FROM BundledFarePeriodImage as periodImage WHERE periodImage.bundleFarePeriodId IN (:bundleFarePeriodIds) ");

		Query query = getSession().createQuery(hql.toString());
		query.setParameterList("bundleFarePeriodIds", bundleFarePeriodIds);

		@SuppressWarnings("unchecked")
		List<BundledFarePeriodImage> bundledFarePeriodImages = query.list();
		return bundledFarePeriodImages;
	}

	@Override
	public void saveBundledFareDisplaySettings(Collection<BundledFarePeriodImage> bundledFarePeriodImages)
			throws CommonsDataAccessException {
		hibernateSaveOrUpdateAll(bundledFarePeriodImages);
	}

	@Override
	public void updateBundledFareTranlationKeys(Integer bundledFarePeriodId, String nameKey, String descKey)
			throws CommonsDataAccessException {

		StringBuilder hql = new StringBuilder();
		hql.append("update t_bundled_fare_period set bundle_name_i18n_key =:nameKey, i18n_message_key =:descKey, version =version+1 where bundled_fare_period_id =:bundledFarePeriodId ");

		Query query = getSession().createSQLQuery(hql.toString());
		query.setParameter("bundledFarePeriodId", bundledFarePeriodId);
		query.setParameter("nameKey", nameKey);
		query.setParameter("descKey", descKey);
		query.executeUpdate();
	}
	
	@Override
	public void updateBundledFareTemplateId(Integer bundledFarePeriodId, Integer descTemplateID)
			throws CommonsDataAccessException {
		StringBuilder hql = new StringBuilder();
		hql.append(
				"update t_bundled_fare_period set bundle_desc_template_id =:descTemplateID , version =version+1 where bundled_fare_period_id =:bundledFarePeriodId ");
		Query query = getSession().createSQLQuery(hql.toString());
		query.setParameter("bundledFarePeriodId", bundledFarePeriodId);
		query.setParameter("descTemplateID", descTemplateID);
		query.executeUpdate();

	}

}
