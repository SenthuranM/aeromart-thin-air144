package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.core.bl.common.PromotionChargeAndRefundHandler;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * @isa.module.command name="nextSeatFreeChargeAndRefund"
 */
public class NextSeatFreeChargeAndRefundHandler extends PromotionChargeAndRefundHandler {

	protected List<OndPromotionCharge>
			filterApplicableCharges(Set<OndPromotionCharge> charges, PromotionRequest promotionRequest) {
		List<OndPromotionCharge> ondPromoChargesRefunds = new ArrayList<OndPromotionCharge>();

		String processCategory = (String) getParameter(CommandParamNames.PROMO_PROCESS_CATEGORY);

		int freeSeatsRequested = 0;
		for (PromotionRequestConfig promotionRequestConfig : promotionRequest.getPromotionRequestConfigs()) {
			if (promotionRequestConfig.getPromoReqParam().equals(PromoRequestParam.FreeSeat.SEATS_TO_FREE)) {
				freeSeatsRequested = Integer.parseInt(promotionRequestConfig.getParamValue());
			}
		}

		String seatsToFreeChargeCode = PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG
				.getFreeSeatChargeParam(freeSeatsRequested);
		String nextSeatRegChargeCode = PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_REG_CHRG
				.getFreeSeatRegistrationChargeParam(freeSeatsRequested);

		for (OndPromotionCharge charge : charges) {

			// if this is refund or charge -- then add next-seat-free-seats charge
			// if this is charge -- then add registration charge
			if (((processCategory.equals(CommandParamNames.PROCESS_REFUNDS) || processCategory
					.equals(CommandParamNames.PROCESS_CHARGES)) && charge.getChargeCode().equals(seatsToFreeChargeCode))
					|| (processCategory.equals(CommandParamNames.PROCESS_CHARGES) && (charge.getChargeCode()
							.equals(nextSeatRegChargeCode)))) {
				ondPromoChargesRefunds.add(charge);
			}
		}

		return ondPromoChargesRefunds;
	}

}
