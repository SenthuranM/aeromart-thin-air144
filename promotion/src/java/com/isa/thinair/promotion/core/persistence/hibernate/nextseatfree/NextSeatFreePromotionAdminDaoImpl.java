package com.isa.thinair.promotion.core.persistence.hibernate.nextseatfree;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.PromotionsApprovalSummaryView;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.core.persistence.dao.NextSeatFreePromotionAdminDao;
import com.isa.thinair.promotion.core.persistence.hibernate.PromotionAdministrationDaoImpl;

/**
 * @isa.module.dao-impl dao-name="NextSeatFreePromotionAdminDao"
 */
public class NextSeatFreePromotionAdminDaoImpl extends PromotionAdministrationDaoImpl implements NextSeatFreePromotionAdminDao {

	public List<Integer> getFlightSegIdsForScheduler() {
		String config = AppSysParamsUtil.getNextSeatFreeApprovalConfigs();
		String[] configs = config.split(",");

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT DISTINCT fs.flt_seg_id flt_seg_id FROM t_flight_segment fs,t_pnr_segment ps,t_promotion_request pr ")
				.append(" WHERE fs.flt_seg_id=ps.flt_seg_id and ps.pnr_seg_id = pr.pnr_seg_id ")
				.append(" and fs.status = 'OPN' and pr.status = 'ACT' and ps.status = 'CNF' ")
				.append(" and fs.est_time_departure_zulu - sysdate BETWEEN  ")
				.append(configs[0] + " AND ( "  + configs[1] + " ) ");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.addScalar("flt_seg_id", IntegerType.INSTANCE);

		List<Object> results = query.list();
		List<Integer> flightSegIds = new ArrayList<Integer>();
		for (Object rec : results) {
			flightSegIds.add((Integer) rec);
		}

		return flightSegIds;
	}

	public Page<PromotionsApprovalSummaryView> getPromotionsApprovalSummaryView(String flightNo, String status, Date startDate, Date endDate,
			Integer startRec, Integer size) {

		boolean filterByFltNo = (flightNo != null && !flightNo.isEmpty()) ? true : false;

		StringBuilder queryContent = new StringBuilder();
		queryContent

				.append("	 SELECT fs.flt_seg_id flt_seg_id , f.flight_number flight_number , fs.est_time_departure_local departure_date , 	")
				.append("        fs.segment_code segment_code , COUNT ( 1 ) requests , promo.status status , SUM ( promo.refund_amount ) refund_amount , 	")
				.append("        SUM ( promo.seats_to_free ) seats_to_free	")
				.append("    FROM t_flight f , t_flight_segment fs , (SELECT rs.flt_seg_id , p.promotion_type_id promotion_type_id , pr.status status ,	")
				.append("        NVL ( MAX ( DECODE ( aa.promotion_attribute , 'refundAmount' , aa.value ) ) , 0 ) refund_amount ,	")
				.append("        NVL ( MAX ( DECODE ( aa.promotion_attribute , 'numberOfSeats' , aa.value ) ) , 0 ) seats_to_free	")
				.append("            FROM t_pnr_segment rs , t_flight_segment fs , t_promotion_request pr , t_promotion p , ")
				.append("                t_pnr_promotion aa	")
				.append("            WHERE rs.flt_seg_id = fs.flt_seg_id AND rs.pnr = pr.pnr AND pr.promotion_id = p.promotion_id	")
				.append("                AND rs.pnr = aa.pnr(+) AND p.promotion_type_id = 1	")
				.append("            GROUP BY rs.flt_seg_id , p.promotion_type_id , pr.status ) promo	")
				.append("    WHERE f.flight_id = fs.flight_id AND fs.flt_seg_id = promo.flt_seg_id	")
				.append("    AND trunc ( fs.est_time_departure_local ) BETWEEN trunc ( :startDate ) AND trunc ( :endDate )	");
		if (filterByFltNo) {
			queryContent.append("  AND f.flight_number = :flightNumber ");
		}

		if (!status.equals("*")) {
			queryContent.append("   AND promo.status = :req_status  ");
		}

		queryContent
				.append("    GROUP BY fs.flt_seg_id , fs.est_time_departure_local , f.flight_number , promo.status , fs.segment_code	");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		if (filterByFltNo) {
			query.setParameter("flightNumber", flightNo.trim());
		}
		if (!status.equals("*")) {
			query.setParameter("req_status", status);
		}

		query.addScalar("flt_seg_id", IntegerType.INSTANCE);
		query.addScalar("flight_number", StringType.INSTANCE);
		query.addScalar("departure_date", TimestampType.INSTANCE);
		query.addScalar("segment_code", StringType.INSTANCE);
		query.addScalar("requests", IntegerType.INSTANCE);
		query.addScalar("status", StringType.INSTANCE);
		query.addScalar("refund_amount", DoubleType.INSTANCE);
		query.addScalar("seats_to_free", IntegerType.INSTANCE);

		List<Object[]> results = query.list();

		Map<Integer, PromotionsApprovalSummaryView> tempView = new LinkedHashMap<Integer, PromotionsApprovalSummaryView>();
		PromotionsApprovalSummaryView summaryView;
		int flightSegId;
		String reqStatus;
		for (Object[] single : results) {
			flightSegId = (Integer) single[0];
			if (!tempView.containsKey(flightSegId)) {
				tempView.put(flightSegId, new PromotionsApprovalSummaryView());
			}

			summaryView = tempView.get(flightSegId);
			summaryView.setFlightNumber((String) single[1]);
			summaryView.setDepartureDate((Date) single[2]);
			summaryView.setSegmentCode((String) single[3]);
			summaryView.addToTotalRequests((Integer) single[4]);

			reqStatus = (String) single[5];
			if (reqStatus.equals(PromotionsInternalConstants.PromotionRequestStatus.APPROVED.getStatusCode())) {
				summaryView.setApprovedRequests(summaryView.getApprovedRequests() + (Integer) single[4]);
			} else if (reqStatus.equals(PromotionsInternalConstants.PromotionRequestStatus.REJECTED.getStatusCode())) {
				summaryView.setRejectedRequests(summaryView.getRejectedRequests() + (Integer) single[4]);
				summaryView.setRefundAmount(summaryView.getRefundAmount() + (Double) single[6]);
			} else if (reqStatus.equals(PromotionsInternalConstants.PromotionRequestStatus.ACTIVE.getStatusCode())) {
				summaryView.setActiveRequests(summaryView.getActiveRequests() + (Integer) single[4]);
			}
		}

		PromotionsApprovalSummaryView[] arr = tempView.values().toArray(new PromotionsApprovalSummaryView[tempView.size()]);
		List<PromotionsApprovalSummaryView> view = new ArrayList<PromotionsApprovalSummaryView>();
		int endRec = startRec + size > tempView.size() ? tempView.size() : startRec + size;
		for (int a = startRec; a < endRec; a++) {
			view.add(arr[a]);
		}

		return new Page<PromotionsApprovalSummaryView>(tempView.size() , startRec, startRec + view.size(), view);

	}
}