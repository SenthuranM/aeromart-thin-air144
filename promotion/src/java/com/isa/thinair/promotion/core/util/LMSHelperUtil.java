package com.isa.thinair.promotion.core.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.FlownFileErrorLog;
import com.isa.thinair.promotion.api.model.lms.BasicDataType.DATA_F;
import com.isa.thinair.promotion.api.model.lms.FlownFile;
import com.isa.thinair.promotion.api.model.lms.FlownRecord;
import com.isa.thinair.promotion.api.model.lms.OrderedElementContainer;
import com.isa.thinair.promotion.api.to.lms.FlownFileRecordDTO;
import com.isa.thinair.promotion.api.to.lms.FlownRecordsDTO;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.RewardData;

/**
 * 
 * @author rumesh
 * 
 */
public class LMSHelperUtil {

	private static final String DELIM = "_";

	public static List<FlownFileRecordDTO> genareteFlownFileRecordDTOs(List<FlownRecordsDTO> flownRecordsDTOs,
			CurrencyExchangeRate currencyExchangeRate) throws ModuleException {
		List<FlownFileRecordDTO> flownFileRecordDTOs = new ArrayList<FlownFileRecordDTO>();
		if (flownRecordsDTOs != null && !flownRecordsDTOs.isEmpty()) {
			for (FlownRecordsDTO flownRecordsDTO : flownRecordsDTOs) {
				FlownFileRecordDTO flownFileRecordDTO = new FlownFileRecordDTO();
				flownFileRecordDTO.setMemberAccountId(flownRecordsDTO.getMemberAccountId());
				flownFileRecordDTO.setTitle(flownRecordsDTO.getTitle());
				flownFileRecordDTO.setLastName(flownRecordsDTO.getLastName());
				flownFileRecordDTO.setFirstName(flownRecordsDTO.getFirstName());
				flownFileRecordDTO.setPaxType(flownRecordsDTO.getPaxType());
				flownFileRecordDTO.setDob(formatDate(flownRecordsDTO.getDob(), "yyyyMMdd"));
				flownFileRecordDTO.setNationality(flownRecordsDTO.getNationality());
				flownFileRecordDTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
				String flightNumber = getFlightNumberDigits(flownRecordsDTO.getOperaringFlight());
				flownFileRecordDTO.setOperaringFlight(flightNumber);
				flownFileRecordDTO.setMarketingAirline(AppSysParamsUtil.getDefaultCarrierCode());
				flownFileRecordDTO.setMarketingFlight(flightNumber);
				flownFileRecordDTO.setFlightDate(formatDate(flownRecordsDTO.getFlightDateTime(), "yyyyMMdd"));
				flownFileRecordDTO.setFlightTime(formatDate(flownRecordsDTO.getFlightDateTime(), "HHmm"));
				flownFileRecordDTO.setDepartureAirport(flownRecordsDTO.getDepartureAirport());
				flownFileRecordDTO.setArrivalAirport(flownRecordsDTO.getArrivalAirport());
				flownFileRecordDTO.setCabinClass(flownRecordsDTO.getCabinClass());
				flownFileRecordDTO.setBookingClass(flownRecordsDTO.getBookingClass());
				flownFileRecordDTO.setPnr(flownRecordsDTO.getPnr());
				flownFileRecordDTO.setTicketIssuanceDate(formatDate(flownRecordsDTO.getTicketIssuanceDate(), "yyyyMMdd"));
				flownFileRecordDTO.setTicketNumber(flownRecordsDTO.getTicketNumber());
				flownFileRecordDTO.setCouponNumber(flownRecordsDTO.getCouponNumber());
				flownFileRecordDTO.setAgentInitials(flownRecordsDTO.getAgentInitials());
				flownFileRecordDTO.setContactNumber(flownRecordsDTO.getContactNumber());
				flownFileRecordDTO.setContactEmail(flownRecordsDTO.getContactEmail());
				flownFileRecordDTO.setCountryOfResidence(flownRecordsDTO.getCountryOfResidence());
				flownFileRecordDTO.setSalesChannel(getLocationExtReference(flownRecordsDTO.getSalesChannel()));
				flownFileRecordDTO.setPassportNumber(flownRecordsDTO.getPassportNumber());
				String actualFlightNumber = getOperatingCarrierCodeFromFlightNumber(flownRecordsDTO.getOperaringFlight())
						+ flightNumber;
				flownFileRecordDTO.setProductList(manipulateProductList(flownRecordsDTO.getProductList(), actualFlightNumber,
						currencyExchangeRate));
				flownFileRecordDTOs.add(flownFileRecordDTO);
			}
		}
		return flownFileRecordDTOs;
	}

	public static Map<String, BigDecimal> convertLoyaltyPointsToCurrencyValue(Map<String, Double> productPointsValue,
			BigDecimal conversionRate) {
		Map<String, BigDecimal> productCurrencyValue = null;
		if (productPointsValue != null) {
			productCurrencyValue = new LinkedHashMap<String, BigDecimal>();

			for (Entry<String, Double> pointsEntry : productPointsValue.entrySet()) {
				String productId = pointsEntry.getKey();
				Double points = pointsEntry.getValue();

				BigDecimal currencyValue = AccelAeroCalculator.multiplyDefaultScale(new BigDecimal(points), conversionRate);

				productCurrencyValue.put(productId, currencyValue);
			}
		}

		return productCurrencyValue;
	}

	public static IssueRewardRequest populateIssueRewardRequest(String memberAccountId, String locationExtRef,
			Map<String, BigDecimal> productCurrencyValue) {
		IssueRewardRequest issueRewardRequest = new IssueRewardRequest();
		issueRewardRequest.setMemberAccountId(memberAccountId);
		issueRewardRequest.setLocationExternalReference(locationExtRef);

		List<RewardData> rewardDataList = new ArrayList<RewardData>();
		for (Entry<String, BigDecimal> productEntry : productCurrencyValue.entrySet()) {
			String productId = productEntry.getKey();
			BigDecimal rewardAmount = productEntry.getValue();

			RewardData rewardData = new RewardData();
			rewardData.setRewardTypeExtReference(productId);
			rewardData.setRewardAmount(rewardAmount);
			rewardDataList.add(rewardData);
		}

		issueRewardRequest.setRewards(rewardDataList);

		return issueRewardRequest;
	}

	public static String buildPaxKey(Integer paxSequence, String paxType, boolean isParent) {
		StringBuilder sb = new StringBuilder(paxSequence).append(DELIM);
		sb.append(paxType).append(DELIM);
		sb.append(isParent ? "Y" : "N");

		return sb.toString();
	}

	public static Integer getPaxSequence(String paxKey) {
		String paxSeq = BeanUtils.nullHandler(paxKey).split(DELIM)[0];
		return (paxSeq == "") ? 0 : Integer.parseInt(paxSeq);
	}

	public static String getPaxType(String paxKey) {
		return BeanUtils.nullHandler(paxKey).split(DELIM)[1];
	}

	public static boolean isParent(String paxKey) {
		String paxParentFlag = BeanUtils.nullHandler(paxKey).split(DELIM)[2];

		if (paxParentFlag != null) {
			return (paxParentFlag.equals("Y")) ? true : false;
		}

		return false;
	}

	/**
	 * This will format flight number into [carrier code + 4 digit number]
	 * 
	 * @param flightNumber
	 * @return
	 */
	public static String formatFlightNumber(String flightNumber) {
		StringBuilder sb = new StringBuilder();
		String carrierCode = flightNumber.substring(0, 2);
		String fltNumDigits = getFlightNumberDigits(flightNumber);
		sb.append(carrierCode).append(fltNumDigits);
		return sb.toString();
	}

	/**
	 * Return 4 digit flight number only
	 * 
	 * @param flightNumber
	 * @return
	 */
	public static String getFlightNumberDigits(String flightNumber) {
		int standardFlightNumLength = 4;
		String flightNumberDigits = "";
		if (flightNumber != null && !"".equals(flightNumber)) {
			flightNumberDigits = flightNumber.substring(2);
			if (flightNumberDigits.length() < standardFlightNumLength) {
				for (int i = flightNumberDigits.length(); i < standardFlightNumLength; i++) {
					flightNumberDigits = "0" + flightNumberDigits;
				}
			}
		}
		return flightNumberDigits;
	}

	public static String getOperatingCarrierCodeFromFlightNumber(String flightNumber) {
		String fltOperatingCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		if (flightNumber != null && !"".equals(flightNumber)) {
			fltOperatingCarrier = flightNumber.substring(0, 2);
		}

		return fltOperatingCarrier;
	}

	public static String formatProductName(String departureAirport, String arrivalAirport, String flightNumber) {
		StringBuilder sb = new StringBuilder();
		sb.append(departureAirport).append("/").append(arrivalAirport);
		sb.append(" ").append(flightNumber);
		return sb.toString();
	}

	public static List<FlownFileRecordDTO> populateFlownFileRecordDTOs(List<FlownFileErrorLog> flownFileErrorLogs) {
		List<FlownFileRecordDTO> flownFileRecordDTOs = null;

		if (flownFileErrorLogs != null && !flownFileErrorLogs.isEmpty()) {
			flownFileRecordDTOs = new ArrayList<FlownFileRecordDTO>();
			for (FlownFileErrorLog flownFileErrorLog : flownFileErrorLogs) {
				FlownFileRecordDTO flownFileRecordDTO = new FlownFileRecordDTO();
				flownFileRecordDTO.setMemberAccountId(flownFileErrorLog.getMemberAccountId());
				flownFileRecordDTO.setTitle(flownFileErrorLog.getTitle());
				flownFileRecordDTO.setLastName(flownFileErrorLog.getLastName());
				flownFileRecordDTO.setFirstName(flownFileErrorLog.getFirstName());
				flownFileRecordDTO.setPaxType(flownFileErrorLog.getPaxType());
				flownFileRecordDTO.setDob(flownFileErrorLog.getDob());
				flownFileRecordDTO.setNationality(flownFileErrorLog.getNationality());
				flownFileRecordDTO.setOperatingAirline(flownFileErrorLog.getOperatingAirline());
				flownFileRecordDTO.setOperaringFlight(flownFileErrorLog.getOperaringFlight());
				flownFileRecordDTO.setMarketingAirline(flownFileErrorLog.getMarketingAirline());
				flownFileRecordDTO.setMarketingFlight(flownFileErrorLog.getMarketingFlight());
				flownFileRecordDTO.setFlightDate(flownFileErrorLog.getFlightDate());
				flownFileRecordDTO.setFlightTime(flownFileErrorLog.getFlightTime());
				flownFileRecordDTO.setDepartureAirport(flownFileErrorLog.getArrivalAirport());
				flownFileRecordDTO.setCabinClass(flownFileErrorLog.getCabinClass());
				flownFileRecordDTO.setBookingClass(flownFileErrorLog.getBookingClass());
				flownFileRecordDTO.setPnr(flownFileErrorLog.getPnr());
				flownFileRecordDTO.setTicketIssuanceDate(flownFileErrorLog.getTicketIssuanceDate());
				flownFileRecordDTO.setTicketNumber(flownFileErrorLog.getTicketNumber());
				flownFileRecordDTO.setCouponNumber(flownFileErrorLog.getCouponNumber());
				if (flownFileErrorLog.getAgentInitials() == null) {
					flownFileRecordDTO.setAgentInitials("WEB");
				} else {
					flownFileRecordDTO.setAgentInitials(flownFileErrorLog.getAgentInitials());
				}
				flownFileRecordDTO.setContactNumber(flownFileErrorLog.getContactNumber());
				flownFileRecordDTO.setContactEmail(flownFileErrorLog.getContactEmail());
				flownFileRecordDTO.setCountryOfResidence(flownFileErrorLog.getCountryOfResidence());
				flownFileRecordDTO.setSalesChannel(flownFileErrorLog.getSalesChannel());
				flownFileRecordDTO.setPassportNumber(flownFileErrorLog.getStatus());
				flownFileRecordDTO.setProductList(flownFileErrorLog.getProductList());
				flownFileRecordDTO.setErrorRecordReference(flownFileErrorLog.getErrorLogId());

				flownFileRecordDTOs.add(flownFileRecordDTO);
			}
		}

		return flownFileRecordDTOs;
	}

	public static void populateFlownFile(FlownFile flownFile, List<FlownFileRecordDTO> flownFileRecordDTOs) {
		if (flownFileRecordDTOs != null && !flownFileRecordDTOs.isEmpty()) {
			for (FlownFileRecordDTO flownFileRecordDTO : flownFileRecordDTOs) {
				if (flownFileRecordDTO.getProductList() != null && !"".equals(flownFileRecordDTO.getProductList())) {
					FlownRecord flownRecord = new FlownRecord();
					flownRecord.setMemberAccountId(flownFileRecordDTO.getMemberAccountId(), DATA_F.OPTIONAL);
					flownRecord.setTitle(flownFileRecordDTO.getTitle(), DATA_F.MANDATORY);
					flownRecord.setLastName(flownFileRecordDTO.getLastName(), DATA_F.MANDATORY);
					flownRecord.setFirstName(flownFileRecordDTO.getFirstName(), DATA_F.MANDATORY);
					flownRecord.setPassengerType(flownFileRecordDTO.getPaxType(), DATA_F.OPTIONAL);
					flownRecord.setDOB(flownFileRecordDTO.getDob(), DATA_F.OPTIONAL);
					flownRecord.setNationality(flownFileRecordDTO.getNationality(), DATA_F.MANDATORY);
					flownRecord.setOperatingAirlineCode(flownFileRecordDTO.getOperatingAirline(), DATA_F.MANDATORY);
					flownRecord.setOperatingAirlineFlight(flownFileRecordDTO.getOperaringFlight(), DATA_F.MANDATORY);
					flownRecord.setMarktingAirlineCode(flownFileRecordDTO.getMarketingAirline(), DATA_F.MANDATORY);
					flownRecord.setMarketingAirlineFlight(flownFileRecordDTO.getMarketingFlight(), DATA_F.MANDATORY);
					flownRecord.setFlightDate(flownFileRecordDTO.getFlightDate(), DATA_F.MANDATORY);
					flownRecord.setFlightTime(flownFileRecordDTO.getFlightTime(), DATA_F.MANDATORY);
					flownRecord.setDepartureAirport(flownFileRecordDTO.getDepartureAirport(), DATA_F.MANDATORY);
					flownRecord.setArrivalAirport(flownFileRecordDTO.getArrivalAirport(), DATA_F.OPTIONAL);
					flownRecord.setCabinClassCode(flownFileRecordDTO.getCabinClass(), DATA_F.MANDATORY);
					flownRecord.setBookingClassCode(flownFileRecordDTO.getBookingClass(), DATA_F.MANDATORY);
					flownRecord.setPnr(flownFileRecordDTO.getPnr(), DATA_F.MANDATORY);
					flownRecord.setTicketIssuanceDate(flownFileRecordDTO.getTicketIssuanceDate(), DATA_F.MANDATORY);
					flownRecord.setTicketNumber(flownFileRecordDTO.getTicketNumber(), DATA_F.MANDATORY);
					flownRecord.setTicketCouponNumber(flownFileRecordDTO.getCouponNumber(), DATA_F.OPTIONAL);
					flownRecord.setBookingAgentInitials(flownFileRecordDTO.getAgentInitials(), DATA_F.OPTIONAL);
					flownRecord.setContactNumber(flownFileRecordDTO.getContactNumber(), DATA_F.OPTIONAL);
					flownRecord.setContactEmail(flownFileRecordDTO.getContactEmail(), DATA_F.OPTIONAL);
					flownRecord.setCountryOfResidnece(flownFileRecordDTO.getCountryOfResidence(), DATA_F.OPTIONAL);
					flownRecord.setSalesChannel(flownFileRecordDTO.getSalesChannel(), DATA_F.OPTIONAL);
					flownRecord.setPassportNumber(flownFileRecordDTO.getPassportNumber(), DATA_F.OPTIONAL);
					flownRecord.setProductList(flownFileRecordDTO.getProductList(), DATA_F.MANDATORY);

					flownRecord.setErrorRecordReference(flownFileRecordDTO.getErrorRecordReference());

					flownFile.addFlownRecords(flownRecord);
				}
			}
		}
	}

	public static Map<Date, List<FlownFileErrorLog>> getDateWiseFailedFlownRecords(List<FlownFileErrorLog> failedFlownRecords)
			throws ParseException {
		Map<Date, List<FlownFileErrorLog>> dateWiseFailedRecords = new HashMap<Date, List<FlownFileErrorLog>>();
		for (FlownFileErrorLog flownFileErrorLog : failedFlownRecords) {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date dateOnly = formatter.parse(formatDate(flownFileErrorLog.getCreatedDate(), "dd/MM/yyyy"));

			if (!dateWiseFailedRecords.containsKey(dateOnly)) {
				dateWiseFailedRecords.put(dateOnly, new ArrayList<FlownFileErrorLog>());
			}

			List<FlownFileErrorLog> dateErroFileLogs = dateWiseFailedRecords.get(dateOnly);
			dateErroFileLogs.add(flownFileErrorLog);
		}
		return dateWiseFailedRecords;
	}

	private static String formatDate(Date date, String pattern) {
		String dtStr = null;
		if (date != null) {
			DateFormat formatter = new SimpleDateFormat(pattern);
			dtStr = formatter.format(date);
		}
		return dtStr;
	}

	private static String getLocationExtReference(Integer salesChannelCode) {
		String locationExtReference = null;
		if (salesChannelCode != null) {
			AppIndicatorEnum appIndicator = SalesChannelsUtil.getAppIndicatorFromSalesChannel(salesChannelCode);
			if (appIndicator != null) {
				Map<String, String> locationExtReferences = PromotionModuleServiceLocator.getLoyaltyExternalConfig()
						.getLocationExtReference();
				if (locationExtReferences.containsKey(appIndicator.toString())) {
					locationExtReference = locationExtReferences.get(appIndicator.toString());
				}
			}
		}
		return locationExtReference;
	}

	private static String
			manipulateProductList(String productList, String flightNumber, CurrencyExchangeRate currencyExchangeRate)
					throws ModuleException {
		String[] productArr = productList.split(Pattern.quote("|"));
		StringBuilder sb = new StringBuilder("");

		if (productArr != null && productArr.length > 0) {
			for (String product : productArr) {
				String[] productValueArr = product.split("=");
				if (productValueArr != null && productValueArr.length > 0) {
					String productName = productValueArr[0];
					if (FlownFile.PRODUCT_CODE.FLIGHT.toString().equals(productName)) {
						productName = flightNumber;
					}
					String amountInBase = productValueArr[1];
					String amountInLoyaltyCurrency = "";
					if (amountInBase != null && !"".equals(amountInBase)) {
						BigDecimal amountInBaseVal = new BigDecimal(amountInBase);

						BigDecimal baseToCurrencyRate = currencyExchangeRate.getMultiplyingExchangeRate();
						BigDecimal converedAmount = AccelAeroCalculator.multiply(amountInBaseVal, baseToCurrencyRate);
						amountInLoyaltyCurrency = AccelAeroCalculator.scaleDefaultRoundingDown(converedAmount).toString();
					}

					sb.append(productName).append(OrderedElementContainer.SEPARATOR).append(amountInLoyaltyCurrency)
							.append(OrderedElementContainer.SEPARATOR).append(OrderedElementContainer.SEPARATOR);
				}
			}
		}

		return sb.toString();
	}

}
