package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.util.HashMap;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.promotion.core.bl.common.PromotionMailNotifier;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.util.CommandParamNames;

/**
 * 
 * @isa.module.command name="nextSeatFreeMailNotifier"
 */
public class NextSeatFreeMailNotifier extends PromotionMailNotifier {

	protected HashMap<String, Object> getTopicParams(NotificationReceiverTo notificationReceiver) {
		String eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);
		HashMap<String, Object> topicParams = new HashMap<String, Object>();

		topicParams.put("pnr", notificationReceiver.getPnr());
		topicParams.put("ibe_url", AppSysParamsUtil.getSecureIBEUrl());
		topicParams.put("ibe_url_root", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));

		if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {
			topicParams.put("promo_id", notificationReceiver.getPromotionId());
			topicParams.put("receiver_name", notificationReceiver.getReceiverName());
			topicParams.put("dep_date", CalendarUtil.formatDate(notificationReceiver.getDepartureTime(), "dd.MM.yyyy"));
			topicParams.put("destination", notificationReceiver.getDestination());
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_SUBSCRIBE)) {
			topicParams.put("promo_id", notificationReceiver.getPromotionId());
			topicParams.put("receiver_name", notificationReceiver.getReceiverName());
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVED_REJECTED)) {
			topicParams.put("receiver_name", notificationReceiver.getReceiverName());
			topicParams.put("passengerPromoStatus", notificationReceiver.getPassengerPromoStatus());
			topicParams.put("journeyOnds", notificationReceiver.getJourneyOnds());
			topicParams.put("dep_date", CalendarUtil.formatDate(notificationReceiver.getDepartureTime(), "dd.MM.yyyy"));
		}

		return topicParams;
	}

	protected String getTopicName() {
		String eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);

		if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {
			return "promotion_offer_next_seat_free";
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_SUBSCRIBE)) {
			return "promotion_subscribed_next_seat_free";
		} else if (eventType.equals(CommandParamNames.EVENT_PROMO_APPROVED_REJECTED)) {
			return "promotion_status_next_seat_free";
		}

		return null;
	}

}
