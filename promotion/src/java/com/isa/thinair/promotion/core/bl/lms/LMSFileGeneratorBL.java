package com.isa.thinair.promotion.core.bl.lms;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.promotion.api.model.FlownFileErrorLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_STATUS;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.model.lms.BasicDataType.DATA_F;
import com.isa.thinair.promotion.api.model.lms.FileGeneratable;
import com.isa.thinair.promotion.api.model.lms.FlownFile;
import com.isa.thinair.promotion.api.model.lms.FlownRecord;
import com.isa.thinair.promotion.api.model.lms.ProductFile;
import com.isa.thinair.promotion.api.model.lms.ProductRecord;
import com.isa.thinair.promotion.api.to.lms.FlownFileRecordDTO;
import com.isa.thinair.promotion.api.to.lms.FlownRecordsDTO;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.LMSHelperUtil;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * 
 * @author rumesh
 * 
 */
public class LMSFileGeneratorBL {

	private Log log = LogFactory.getLog(LMSFileGeneratorBL.class);

	public void generateFlownFile(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly,
			UserPrincipal userPrincipal) throws ModuleException {
		try {
			if (!processErrorsOnly) {
				List<FlownRecordsDTO> flownRecordsDTOs = PromotionsHandlerFactory.getLoyaltyManagmentDAO().getFlownRecords(
						rptPeriodStartDate, rptPeriodEndDate);
				CurrencyExchangeRate currencyExchangeRate = PromotionModuleServiceLocator.getCommonMasterBD().getExchangeRate(
						AppSysParamsUtil.getLoyaltyManagedCurrencyCode(), CalendarUtil.getCurrentSystemTimeInZulu());
				List<FlownFileRecordDTO> flownFileRecordDTOs = LMSHelperUtil.genareteFlownFileRecordDTOs(flownRecordsDTOs,
						currencyExchangeRate);

				FlownFile flownFile = new FlownFile();
				LMSHelperUtil.populateFlownFile(flownFile, flownFileRecordDTOs);
				// Prepare and upload file to FTP location
				publishLoyaltyFile(flownFile, rptPeriodEndDate, userPrincipal, false);
				// Handle error occurred records
				logFailedFlownRecords(flownFile, userPrincipal);
			}

			LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator
					.getLoyaltyExternalConfig();
			if (externalConfig.isProcessErrorRecords()) {
				// Process failed flown records
				processFailedFlownRecords(userPrincipal);
			}

		} catch (IOException e) {
			log.error("Error occured while generating Flown File ", e);
			throw new ModuleException("Error occured while generating Flown File ", e);
		} catch (Exception e) {

			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			}

			log.error("Error occured while generating Flown File ", e);
			throw new ModuleException("Error occured while generating Flown File ", e);
		}
	}

	public void generateProductFile(UserPrincipal userPrincipal) throws ModuleException {

		try {
			Date lastPublishedTime = PromotionsHandlerFactory.getLoyaltyManagmentDAO().getLastProductFilePublishTime();

			Collection<DisplayFlightDTO> displayFlightDTOs = PromotionModuleServiceLocator.getFlightBD().getFlightsToPublish(
					lastPublishedTime);
			ProductFile productFile = new ProductFile();
			if (displayFlightDTOs != null && !displayFlightDTOs.isEmpty()) {
				for (DisplayFlightDTO displayFlightDTO : displayFlightDTOs) {
					Flight flight = displayFlightDTO.getFlight();
					ProductRecord productRecord = new ProductRecord();
					String flightNumber = LMSHelperUtil.formatFlightNumber(flight.getFlightNumber());
					String DepartureCode = flight.getOriginAptCode();
					String arrivalCode = flight.getDestinationAptCode();

					productRecord.setProductName(LMSHelperUtil.formatProductName(DepartureCode, arrivalCode, flightNumber),
							DATA_F.MANDATORY);
					productRecord.setProductExtReference(flightNumber, DATA_F.OPTIONAL);
					productRecord.setDepartureAirport(DepartureCode, DATA_F.OPTIONAL);
					productRecord.setArrivalAirport(arrivalCode, DATA_F.OPTIONAL);
					productFile.addProductRecords(productRecord);
				}

			}

			// Prepare and upload file to FTP location
			publishLoyaltyFile(productFile, CalendarUtil.getCurrentSystemTimeInZulu(), userPrincipal, false);
		} catch (IOException e) {
			log.error("Error occured while generating Product File ", e);
			throw new ModuleException("Error occured while generating Product File ", e);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			}
			log.error("Error occured while generating Product File ", e);
			throw new ModuleException("Error occured while generating Product File ", e);
		}
	}

	private void processFailedFlownRecords(UserPrincipal userPrincipal) throws ParseException {
		List<FlownFileErrorLog> failedFlownRecords = PromotionsHandlerFactory.getLoyaltyManagmentDAO()
				.getFailedFlownFileRecords();

		if (failedFlownRecords != null && !failedFlownRecords.isEmpty()) {
			Map<Date, List<FlownFileErrorLog>> dateWiseFailedRecords = LMSHelperUtil
					.getDateWiseFailedFlownRecords(failedFlownRecords);

			for (Entry<Date, List<FlownFileErrorLog>> flownEntry : dateWiseFailedRecords.entrySet()) {
				Date processedDate = flownEntry.getKey();
				List<FlownFileErrorLog> dayFailedFlownRecords = flownEntry.getValue();
				if (dayFailedFlownRecords != null && !dayFailedFlownRecords.isEmpty()) {

					SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
					Date actualDate = fmt.parse(fmt.format(processedDate)); // Convert the date to yyyy/MM/dd 00:00

					// fromDate = (yyyy/MM/(dd-1) 00:00)
					actualDate = new Date(actualDate.getTime() - TimeUnit.DAYS.toMillis(1));

					FlownFile flownFile = new FlownFile();
					List<FlownFileRecordDTO> flownFileRecordDTOs = LMSHelperUtil
							.populateFlownFileRecordDTOs(dayFailedFlownRecords);
					LMSHelperUtil.populateFlownFile(flownFile, flownFileRecordDTOs);
					// Prepare and upload file to FTP location
					String fileName = flownFile.getFileName(actualDate);
					try {
						boolean publishStatus = publishLoyaltyFile(flownFile, actualDate, userPrincipal, true);

						if (publishStatus) {
							// update failed record status
							for (FlownFileErrorLog flownFileErrorLog : dayFailedFlownRecords) {
								if(!flownFile.isErrorRecordExists(flownFileErrorLog.getErrorLogId())){									
									flownFileErrorLog.setStatus(FlownFileErrorLog.STATUS.PROCESSED.toString());
									PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateFlowFileErrorLog(flownFileErrorLog);
								}
							}
						}

					} catch (ModuleException e) {
						log.error("Error occured while generating Flown File for Failed Records: " + actualDate + ", File Name: "
								+ fileName, e);
					} catch (IOException e) {
						log.error("Error occured while generating Flown File for Failed Records: " + actualDate + ", File Name: "
								+ fileName, e);
					}
				}
			}
		}
	}

	private boolean publishLoyaltyFile(FileGeneratable file, Date processingDate, UserPrincipal userPrincipal,
			boolean isFailedRecordProcess) throws IOException, ModuleException {
		if (file != null) {
			List<String> failedFileList = new ArrayList<String>();
			Exception lastException = null;

			boolean fileCreationSuccess = false;
			boolean fileLoggingSuccess = false;

			String fileName = file.getFileName(processingDate);
			String folderPath = "";
			if (file instanceof FlownFile) {
				folderPath = PlatformConstants.getAbsLoyaltyFlownFileUploadPath();
			} else if (file instanceof ProductFile) {
				folderPath = PlatformConstants.getAbsLoyaltyProductFileUploadPath();
			}

			try {
				log.info("Started Writing File: " + fileName);
				String fileContent = file.getFormattedData();

				// For failed record process don't publish blank file.
				if (isFailedRecordProcess && (fileContent == null || "".equals(fileContent))) {
					return false;
				} else {
					writeTextFile(folderPath, fileName, fileContent);
					log.info("Finished Writing File: " + fileName);
					fileCreationSuccess = true;

					// Save file creation log entry
					addFileLog(file, fileName, userPrincipal, LoyaltyFileLog.FILE_STATUS.CREATED);
					fileLoggingSuccess = true;
				}
			} catch (Exception e) {
				log.error("Failed in generation Loyalty file:" + fileName, e);
				failedFileList.add(fileName);
				lastException = e;
			} finally {
				if (fileCreationSuccess && !fileLoggingSuccess) {
					File loyaltyFile = new File(folderPath + File.separatorChar + fileName);
					loyaltyFile.delete();
				}
				if (!fileCreationSuccess || (fileCreationSuccess && !fileLoggingSuccess)) {
					if (!fileLoggingSuccess && !isFailedRecordProcess) {
						PromotionsHandlerFactory.getLoyaltyManagmentDAO().deleteLoyaltyFileLog(fileName);
					}
					if (!isFailedRecordProcess && (file instanceof FlownFile)) {
						logSuccessRecords((FlownFile) file, userPrincipal);
					}
				}
			}

			if (!failedFileList.isEmpty()) {
				Throwable throwable = new Throwable(failedFileList.toString(), lastException);
				throw new ModuleException(throwable, "promotion.loyalty.file.generation.failed");
			}

			return fileCreationSuccess && fileLoggingSuccess;
		}

		return true;
	}

	private void addFileLog(FileGeneratable file, String fileName, UserPrincipal userPrincipal,
			LoyaltyFileLog.FILE_STATUS fileStatus) {
		
		if (file instanceof FlownFile) {
			addFileLogForFlownFile(fileName, userPrincipal, fileStatus);
		} else {
			addFileLogForProductFile(fileName, userPrincipal, fileStatus);
		}		
	}

	private void addFileLogForProductFile(String fileName, UserPrincipal userPrincipal, LoyaltyFileLog.FILE_STATUS fileStatus) {

		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		List<String> productFileUploadEnableLMSProviders = externalConfig.getProductFileUploadEnableLMSProviders();

		for (String serviceProvider : productFileUploadEnableLMSProviders) {
			LoyaltyFileLog loyaltyFileLog = composeFileLog(fileName, userPrincipal, fileStatus);

			loyaltyFileLog.setFileType(LoyaltyFileLog.FILE_TYPE.PRODUCT_FILE.toString());
			loyaltyFileLog.setServiceProvider(serviceProvider);

			PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateLoyaltyFileLog(loyaltyFileLog);
		}
	}

	private void addFileLogForFlownFile(String fileName, UserPrincipal userPrincipal, LoyaltyFileLog.FILE_STATUS fileStatus) {

		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		List<String> flownFileUploadEnableLMSProviders = externalConfig.getFlownFileUploadEnableLMSProviders();

		for (String serviceProvider : flownFileUploadEnableLMSProviders) {
			LoyaltyFileLog loyaltyFileLog = composeFileLog(fileName, userPrincipal, fileStatus);

			loyaltyFileLog.setFileType(LoyaltyFileLog.FILE_TYPE.FLOWN_FILE.toString());
			loyaltyFileLog.setServiceProvider(serviceProvider);

			PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateLoyaltyFileLog(loyaltyFileLog);
		}
	}

	private LoyaltyFileLog composeFileLog(String fileName, UserPrincipal userPrincipal, LoyaltyFileLog.FILE_STATUS fileStatus) {
		Date date = CalendarUtil.getCurrentSystemTimeInZulu();

		LoyaltyFileLog loyaltyFileLog = new LoyaltyFileLog();
		loyaltyFileLog.setFileName(fileName);
		loyaltyFileLog.setStatus(fileStatus.toString());
		loyaltyFileLog.setCreatedBy(userPrincipal.getUserId());
		loyaltyFileLog.setCreatedDate(date);
		loyaltyFileLog.setModifiedBy(userPrincipal.getUserId());
		loyaltyFileLog.setModifiedDate(date);
		return loyaltyFileLog;
	}

	private void writeTextFile(String folderPath, String fileName, String content) throws IOException {

		File folderCheck = new File(folderPath);

		if (!folderCheck.exists()) {
			folderCheck.mkdirs();
		}

		if (folderCheck.canWrite()) {
			StringBuilder localFilePath = new StringBuilder();
			localFilePath.append(folderPath).append(File.separatorChar);
			localFilePath.append(fileName);

			File file = new File(localFilePath.toString());
			FileUtils.writeStringToFile(file, content);
		} else {
			throw new IOException(fileName + ":Loyalty file writing failed due to limited permissions");
		}

	}

	private void logSuccessRecords(FlownFile flownFile, UserPrincipal userPrincipal) {
		if (flownFile.getFlownRecords() != null && !flownFile.getFlownRecords().isEmpty()) {
			logFailedFlownRecords(flownFile.getFlownRecords(), userPrincipal);
		}
	}

	private void logFailedFlownRecords(FlownFile flownFile, UserPrincipal userPrincipal) {
		if (flownFile.getFlownErrorRecords() != null && !flownFile.getFlownErrorRecords().isEmpty()) {
			logFailedFlownRecords(flownFile.getFlownErrorRecords(), userPrincipal);
		}
	}

	private void logFailedFlownRecords(List<FlownRecord> flownRecords, UserPrincipal userPrincipal) {
		for (FlownRecord flownRecord : flownRecords) {
			FlownFileErrorLog flownFileErrorLog = new FlownFileErrorLog();
			flownFileErrorLog.setMemberAccountId(flownRecord.getMemberAccountId());
			flownFileErrorLog.setTitle(flownRecord.getTitle());
			flownFileErrorLog.setLastName(flownRecord.getLastName());
			flownFileErrorLog.setFirstName(flownRecord.getFirstName());
			flownFileErrorLog.setPaxType(flownRecord.getPassengerType());
			flownFileErrorLog.setDob(flownRecord.getDOB());
			flownFileErrorLog.setNationality(flownRecord.getNationality());
			flownFileErrorLog.setOperatingAirline(flownRecord.getOperatingAirlineCode());
			flownFileErrorLog.setOperaringFlight(flownRecord.getOperatingAirlineFlight());
			flownFileErrorLog.setMarketingAirline(flownRecord.getMarktingAirlineCode());
			flownFileErrorLog.setMarketingFlight(flownRecord.getMarketingAirlineFlight());
			flownFileErrorLog.setFlightDate(flownRecord.getFlightDate());
			flownFileErrorLog.setFlightTime(flownRecord.getFlightTime());
			flownFileErrorLog.setDepartureAirport(flownRecord.getDepartureAirport());
			flownFileErrorLog.setArrivalAirport(flownRecord.getArrivalAirport());
			flownFileErrorLog.setCabinClass(flownRecord.getCabinClassCode());
			flownFileErrorLog.setBookingClass(flownRecord.getBookingClassCode());
			flownFileErrorLog.setPnr(flownRecord.getPnr());
			flownFileErrorLog.setTicketIssuanceDate(flownRecord.getTicketIssuanceDate());
			flownFileErrorLog.setTicketNumber(flownRecord.getTicketNumber());
			flownFileErrorLog.setCouponNumber(flownRecord.getTicketCouponNumber());
			if (!"WEB".equals(flownRecord.getBookingAgentInitials())) {
				flownFileErrorLog.setAgentInitials(flownRecord.getBookingAgentInitials());
			}

			String contactNumber = flownRecord.getContactNumber();
			if (contactNumber != null) {
				contactNumber = contactNumber.replaceAll("-", "");
			}

			flownFileErrorLog.setContactNumber(contactNumber);
			flownFileErrorLog.setContactEmail(flownRecord.getContactEmail());
			flownFileErrorLog.setCountryOfResidence(flownRecord.getCountryOfResidnece());
			flownFileErrorLog.setSalesChannel(flownRecord.getSalesChannel());
			flownFileErrorLog.setPassportNumber(flownRecord.getPassportNumber());
			flownFileErrorLog.setProductList(flownRecord.getProductList());
			flownFileErrorLog.setStatus(FlownFileErrorLog.STATUS.PENDING.toString());

			Date date = new Date();
			flownFileErrorLog.setCreatedBy(userPrincipal.getUserId());
			flownFileErrorLog.setCreatedDate(date);
			flownFileErrorLog.setModifiedBy(userPrincipal.getUserId());
			flownFileErrorLog.setModifiedDate(date);

			PromotionsHandlerFactory.getLoyaltyManagmentDAO().saveOrUpdateFlowFileErrorLog(flownFileErrorLog);
		}
	}
}
