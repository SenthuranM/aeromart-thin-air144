package com.isa.thinair.promotion.core.bl.lms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

public class LoyaltyManageBLFactory {

	private static Log log = LogFactory.getLog(LoyaltyManageBLFactory.class);

	private static final LMS_SERVICE_PROVIDER serviceProvider = WSClientModuleUtils.getModuleConfig().getLmsClientConfig()
			.getDefaultLmsProviderCode();

	public static AbstractLoyalatyManagementBL getLoyaltyManageBL() throws ModuleException {

		AbstractLoyalatyManagementBL loyalatyManagementBL;

		if (serviceProvider.equals(LMS_SERVICE_PROVIDER.SMART_BUTTON)) {
			loyalatyManagementBL = new SmartButtonLoyaltyManagementBL();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.AERO_REWARD)) {
			loyalatyManagementBL = new AeroRewardsLoyaltyManagementBL();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.GRAVTY)) {
			loyalatyManagementBL = new GravtyLoyaltyManagementBL();
		} else {
			log.error("Loyalty service unsupported Lms service provider code detected");
			throw new ModuleException("promotion.loyalty.unsupported.service.invocation.error");
		}
		return loyalatyManagementBL;
	}

}
