package com.isa.thinair.promotion.core.persistence.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.isa.thinair.promotion.api.model.VoucherTermsTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateSearchDTO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.promotion.core.persistence.dao.VoucherTermsTemplateDAO;

/**
 * Class implementing data access operations related to {@link VoucherTermsTemplate} 
 * 
 * @author chanaka
 * 
 * @isa.module.dao-impl dao-name="VoucherTermsTemplateDAO"
 */
public class VoucherTermsTemplateDAOImpl extends PlatformBaseHibernateDaoSupport implements VoucherTermsTemplateDAO {

	@Override
	public void updateVoucherTermsTemplate(VoucherTermsTemplate termsTemplate) {

		getSession().merge(termsTemplate);
		
	}

	@Override
	public Page<VoucherTermsTemplate> getVoucherTermsTemplatePage(VoucherTermsTemplateSearchDTO searchCriteria, Integer start,
			Integer recSize) {

		Criteria query = getSession().createCriteria(VoucherTermsTemplate.class);
		Criteria queryForSize = getSession().createCriteria(VoucherTermsTemplate.class);

		if (searchCriteria.getName() != null) {
			query.add(Restrictions.like("templateName", searchCriteria.getName()));
			queryForSize.add(Restrictions.like("templateName", searchCriteria.getName()));
		}

		if (searchCriteria.getLanguage() != null) {
			query.add(Restrictions.eq("languageCode", searchCriteria.getLanguage()));
			queryForSize.add(Restrictions.eq("languageCode", searchCriteria.getLanguage()));
		}

		@SuppressWarnings("unchecked")
		List<VoucherTermsTemplate> resultList = query.setFirstResult(start).setMaxResults(recSize).list();

		Integer totalRecords = ((Long) queryForSize.setProjection(Projections.rowCount()).uniqueResult()).intValue();

		return new Page<VoucherTermsTemplate>(totalRecords.intValue(), start, start + recSize, resultList);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VoucherTermsTemplate> getAllVoucherTermsTemplates() {
		return loadAll(VoucherTermsTemplate.class);
	}


	@SuppressWarnings("unchecked")
	@Override
	public VoucherTermsTemplate getVoucherTerms(String voucherTemplateName, String languageCode) {
		VoucherTermsTemplate voucherT = null;
		List<VoucherTermsTemplate> list = null;

		String hql = "from VoucherTermsTemplate as voucherT where voucherT.templateName=:templateName and voucherT.languageCode=:languageCode";

		Query query = getSession().createQuery(hql).setString("templateName", voucherTemplateName)
				.setString("languageCode", languageCode);

		list = query.list();

		if (list.size() != 0) {
			voucherT = (VoucherTermsTemplate) list.get(0);
		}

		return voucherT;
	}
}