package com.isa.thinair.promotion.core.persistence.dao;

import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;

/**
 * Interface defining the data access operaions related to AnciOfferCriteria,
 * 
 * @author thihara
 * 
 */
public interface AnciOfferCriteriaDAO {

	/**
	 * Retrieves a {@link Page} of {@link AnciOfferCriteria} according to the given search criteria.
	 * 
	 * @param searchCriteria
	 *            The search criteria.
	 * @param start
	 *            Starting position of the result numbers.
	 * @param recSize
	 *            Number of results to be retrieved.
	 * 
	 * @return A Page with the search results.
	 */
	public Page<AnciOfferCriteria> searchAnciOfferCriterias(AnciOfferCriteriaSearchTO searchCriteria, Integer start,
			Integer recSize);

	/**
	 * Updates an existing AnciOfferCriteria.
	 * 
	 * @param anciOfferCriteria
	 *            The anci offer criteria to be updated.
	 */
	public void updateAnciOfferCriteria(AnciOfferCriteria anciOfferCriteria);

	/**
	 * Add a new AnciOfferCriteria. Creates new I18nMessageKeys.
	 * 
	 * @param anciOfferCriteria
	 *            The new AnciOfferCriteria to be added.
	 * @return The ID of the newly persisted AnciOfferCriteria
	 */
	public Integer addAnciOfferCriteria(AnciOfferCriteria anciOfferCriteria);

	/**
	 * Gets an AnciOfferCriteria applicable to the given search criteria.
	 * 
	 * @param searchCriteria
	 *            The search criteria determining which AnciOfferCriteria's will be retrieved.
	 * @return A List of matching AnciOfferCriteria
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public List<AnciOfferCriteria> searchApplicableAnciOffers(AnciOfferCriteriaSearchTO searchCriteria);

	public AnciOfferCriteria getAnciOferByID(Integer id);

	public List<AnciOfferCriteria> getActiveCriterisForType(String anciOfferType);
}
