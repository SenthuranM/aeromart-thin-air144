package com.isa.thinair.promotion.core.bl.campaign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DateBuilder;
import org.quartz.SchedulerException;
import org.quartz.TriggerUtils;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.promotion.api.model.Campaign;
import com.isa.thinair.promotion.api.model.CampaignPromoCode;
import com.isa.thinair.promotion.api.to.CampaignTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.scheduledservices.core.client.CampaignScheduler;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

/**
 * @author chanaka
 *
 */
public class CampaignBO {

	private static Log log = LogFactory.getLog(CampaignBO.class);

	private static final String DAILY = "DAILY";

	private static final String HOURLY = "HOURLY";

	private static final String TWICE_A_DAY = "TWICE_A_DAY";

	private static final String THRICE_A_DAY = "THRICE_A_DAY";

	private static final String WEEKlY = "WEEKlY";

	private static final String CAMPAIGN_SCHEDULER = "campaignSchedulerJob";

	public static void createCampaign(CampaignTO campaignTO) throws ModuleException {

		Campaign campaign = transformToCampaignModel(campaignTO);
		PromotionsHandlerFactory.getPromotionManagementDao().saveCampaign(campaign);
		if(campaignTO.isScheduledCampaign()){
			scheduleCampaign(campaign);
		}
	}

	private static Campaign transformToCampaignModel(CampaignTO campaignTo) {

		Campaign campaign = new Campaign();
		campaign.setCampaignName(campaignTo.getCampaignName());
		campaign.setCampaignDes(campaignTo.getCampaignDes());
		campaign.setNoOfPromoCodesSent(Math.min(campaignTo.getNoOfCustomers(), campaignTo.getNoOfPromoCodes()));
		campaign.setCreatedDate(new Date());
		campaign.setCronExpression(campaignTo.getInterval());
		campaign.setStartDate(campaignTo.getStartDate());
		campaign.setEndDate(campaignTo.getEndDate());
		campaign.setEmailContent(StringUtil.convertToHex(campaignTo.getEmailContent()));

		Set<CampaignPromoCode> campaignPromoCodes = createCampaignPromoCodes(campaignTo);
		for (CampaignPromoCode campaignPromoCode : campaignPromoCodes) {
			campaignPromoCode.setCampaign(campaign);
		}

		campaign.setCampaignPromoCodes(campaignPromoCodes);
		return campaign;

	}

	public static void sendPromoCodesToCustomers(CampaignTO campaignTO) {

		Map<String, String> customerPromoCodeMap = campaignTO.getEmailPromoCodeMap();
		try {

			for (Map.Entry<String, String> entry : customerPromoCodeMap.entrySet()) {

				String emailID = entry.getKey();
				String promoCode = entry.getValue();

				MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				HashMap<String, Object> emailDataMap = new HashMap<String, Object>();
				emailDataMap.put("promoCode", promoCode);
				emailDataMap.put("bodyContent", campaignTO.getEmailContent());

				UserMessaging customer = new UserMessaging();
				customer.setToAddres(emailID);
				messages.add(customer);

				Topic topic = new Topic();
				topic.setTopicParams(emailDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.PROMO_CODE_EMAIL_TEMPLATE);
				topic.setLocale(null);
				topic.setAttachMessageBody(false);

				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messagingServiceBD.sendMessage(messageProfile);

			}

		} catch (Exception ex) {
			log.error("CampaignBO ==> sendPromoCodesToCustomers(CampaignTO campaignTO) ", ex);
		}
	}

	public static CampaignTO generateCustomerPromoCodeMap(CampaignTO campaignTO) {

		Map<String, String> cusPromoMap = new HashMap<String, String>();
		Iterator<String> itCustomer = campaignTO.getCustomers().iterator();
		Iterator<String> itPromoCode = campaignTO.getPromoCodes().iterator();

		while (itCustomer.hasNext() && itPromoCode.hasNext()) {
			cusPromoMap.put(itCustomer.next(), itPromoCode.next());
		}
		campaignTO.setEmailPromoCodeMap(cusPromoMap);
		return campaignTO;
		
	}

	private static Set<CampaignPromoCode> createCampaignPromoCodes(CampaignTO campaignTO) {

		Set<CampaignPromoCode> campaignPromoCodes = new HashSet<CampaignPromoCode>();

		for(Map.Entry<String, String> entry : campaignTO.getEmailPromoCodeMap().entrySet()){
			CampaignPromoCode campaignPromoCode = new CampaignPromoCode();
			campaignPromoCode.setEmailId(entry.getKey());
			campaignPromoCode.setPromoCode(entry.getValue());
			campaignPromoCodes.add(campaignPromoCode);
		}

		return campaignPromoCodes;
	}

	public static void scheduleCampaign(Campaign campaign) {
		
		String interval = campaign.getCronExpression();
		Date startDate = campaign.getStartDate();
		Date endDate = campaign.getEndDate();

		Timestamp startTimestamp = new Timestamp(startDate.getTime());
		Timestamp endTimestamp = new Timestamp(endDate.getTime());

		Long timeGapInMili = 0L;

		if (interval.equals(HOURLY)) {
			timeGapInMili = 1000 * 60 * 60L;
		} else if (interval.equals(TWICE_A_DAY)) {
			timeGapInMili = 1000 * 60 * 60 * 12L;
		} else if (interval.equals(THRICE_A_DAY)) {
			timeGapInMili = 1000 * 60 * 60 * 8L;
		} else if (interval.equals(DAILY)) {
			timeGapInMili = 1000 * 60 * 60 * 24L;
		} else if (interval.equals(WEEKlY)) {
			timeGapInMili = 1000 * 60 * 60 * 24 * 7L;
		}

		while ((startTimestamp.getTime() + timeGapInMili) < endTimestamp.getTime()) {

			Date campaignStartTime = getDateOf(new Timestamp(startTimestamp.getTime() + timeGapInMili));
			String jobID = CAMPAIGN_SCHEDULER + "/" + campaign.getCampaignId() + "/" + campaignStartTime.toString();
			JobDetail campaignJobDetail = new JobDetail();
			campaignJobDetail.getJobDataMap().put(Job.PROP_JOB_ID, jobID);
			campaignJobDetail.getJobDataMap().put(Job.PROP_JOB_GROUP, SSInternalConstants.JOB_TYPE.EMAIL_PROMO_CODES_CAMPAIGN);
			campaignJobDetail.getJobDataMap().put(Job.PROP_CAMP_ID, campaign.getCampaignId());
			campaignJobDetail.setJobClass(CampaignScheduler.class);

			try {
				log.debug("JOB: " + jobID + "SCHEDULING CAMPAIGN CAMPAIGN_ID: " + campaign.getCampaignId() + " PNL TIME : "
						+ campaignStartTime);
				ScheduleManager.scheduleJob(campaignJobDetail, campaignStartTime, null, 0, 0);
			} catch (SchedulerException exception) {
				log.error("JOB: " + jobID + " DID NOT SCHEDULE CAMPAIGN POSSIBLLY CAMPAIGN TIME ALREADY SCHEDULED. ", exception);
			} catch (Exception e) {
				log.error("JOB: " + jobID + " DID NOT SCHEDULE CAMPAIGN POSSIBLLY CAMPAIGN TIME ALREADY SCHEDULED. ", e);
			}
			startTimestamp = new Timestamp(campaignStartTime.getTime());
		}
	}

	private static Date getDateOf(Timestamp timestamp) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(new Date(timestamp.getTime()));

		int scheduledYear = gregorianCalendar.get(Calendar.YEAR);
		int scheduledMonth = gregorianCalendar.get(Calendar.MONTH) + 1;
		int scheduledDay = gregorianCalendar.get(Calendar.DATE);
		int scheduledHour = gregorianCalendar.get(Calendar.HOUR_OF_DAY);
		int scheduledMinutes = gregorianCalendar.get(Calendar.MINUTE);
		int scheduledSeconds = gregorianCalendar.get(Calendar.SECOND);

		return DateBuilder.dateOf(scheduledHour, scheduledMinutes,
				scheduledSeconds, scheduledDay, scheduledMonth, scheduledYear);
		
	}
	
	public static void setUpCampaignForEmailingPromoCodes(Campaign campaign){
		Set<CampaignPromoCode> campPromos = campaign.getCampaignPromoCodes(); 
		Map<String,String> emailPromoCodeMap = new HashMap<String, String>();
		
		for(CampaignPromoCode campPromo : campPromos){
			emailPromoCodeMap.put(campPromo.getEmailId(), campPromo.getPromoCode());		
		}
		
		CampaignTO campaignTO = new CampaignTO();
		campaignTO.setEmailPromoCodeMap(emailPromoCodeMap);
		campaignTO.setEmailContent(StringUtil.getUnicode(campaign.getEmailContent()));
		
		sendPromoCodesToCustomers(campaignTO);
		
	}
	
	public static CustomerPromoCodeSelectionRequest prepareCustomerSearchForCampaignManagement(
			CustomerPromoCodeSelectionRequest req) {
		
		Set<String> ondSet = req.getApplicableONDs();
		Set<String> anyOriginationSet = new HashSet<String>();
		Set<String> anyDestinationSet = new HashSet<String>();
		for (String ond : ondSet) {
			if (ond.split("/")[0].equals("ALL")) {
				anyOriginationSet.add((ond.split("/")[1]));
				req.setAnyOrigination(true);
			}
			if (ond.split("/")[1].equals("ALL")) {
				anyDestinationSet.add((ond.split("/")[0]));
				req.setAnyDestination(true);
			}
		}
		req.setAnyDestinationList(anyDestinationSet);
		req.setAnyOriginationList(anyOriginationSet);

		Set<String> singleSegments = new HashSet<String>();
		for (String ond : ondSet) {
			if (ond.split("/").length == 2) {
				singleSegments.add(ond);
			} else if (ond.split("/").length > 2) {
				for (int i = 0; i < ond.split("/").length - 1; i++) {
					String[] ondArr = ond.split("/");
					String singleSegment = ondArr[i] + "/" + ondArr[i + 1];
					singleSegments.add(singleSegment);
				}
			}
		}
		req.setApplicableONDs(singleSegments);
		return req;

	}

}
