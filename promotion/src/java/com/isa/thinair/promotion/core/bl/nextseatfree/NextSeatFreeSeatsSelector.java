package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.core.dto.nextseat.SeatSelectionTo;

public interface NextSeatFreeSeatsSelector {
	SeatSelectionTo getSeatsToAllocate(Integer flightSegId, PromotionRequest promotionRequest,
			Map<String, FlightSeatStatusTO> seatInventory, String paxType, PaxSeatTO seat, String logicalCabinClassCode,
			Integer noOfSeatsRequested, String cabinClassCode, boolean relocate);
}