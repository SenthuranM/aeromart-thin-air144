package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.promotion.api.service.PromotionManagementBD;

@Local
public interface PromotionManagementDBLocalImpl extends PromotionManagementBD {

}
