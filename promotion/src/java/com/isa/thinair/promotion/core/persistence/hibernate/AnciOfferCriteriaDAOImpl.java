package com.isa.thinair.promotion.core.persistence.hibernate;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.core.persistence.dao.AnciOfferCriteriaDAO;

/**
 * 
 * @author thihara
 * 
 * @isa.module.dao-impl dao-name="AnciOfferCriteriaDAO"
 */
public class AnciOfferCriteriaDAOImpl extends PlatformHibernateDaoSupport implements AnciOfferCriteriaDAO {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page<AnciOfferCriteria> searchAnciOfferCriterias(AnciOfferCriteriaSearchTO searchCriteria, Integer start,
			Integer recSize) {

		StringBuilder countQueryStr = new StringBuilder("SELECT count(*) ");
		StringBuilder searchQueryStr = new StringBuilder("FROM AnciOfferCriteria anciOffer");
		StringBuilder limitingCriteria = new StringBuilder(" WHERE");
		StringBuilder sortingCriteria = new StringBuilder(" ORDER BY anciOffer.");

		// boolean indicating if there were no filtering criteria (and clauses) added
		boolean noWhereClausesAdded = true;

		// boolean indicating whether the next building step should add a AND clause to the query first.
		boolean addAndClause = false;

		if (searchCriteria.getOND() != null) {
			searchQueryStr.append(" join anciOffer.onds anciONDs");
			limitingCriteria.append(" anciONDs like :OND");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (searchCriteria.getFrom() != null && searchCriteria.getTo() != null) {
			if (addAndClause) {
				limitingCriteria.append(" and ");
			}

			limitingCriteria.append(" anciOffer.applicableFrom >= :flightFrom and ");
			limitingCriteria.append(" anciOffer.applicableTo <= :flightTo ");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (searchCriteria.getLcc() != null) {
			if (addAndClause) {
				limitingCriteria.append(" and ");
			}

			searchQueryStr.append(" join anciOffer.lccs anciLCCs");
			limitingCriteria.append(" anciLCCs like :lcc");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (searchCriteria.getBookingClass() != null) {
			if (addAndClause) {
				limitingCriteria.append(" and ");
			}

			searchQueryStr.append(" join anciOffer.bookingClasses bookingClass");
			limitingCriteria.append(" bookingClass like :bookingClass");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (searchCriteria.getActive() != null) {
			if (addAndClause) {
				limitingCriteria.append(" and ");
			}

			limitingCriteria.append(" anciOffer.active =:isActiveCriteria ");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (searchCriteria.getAnciOfferType() != null) {
			if (addAndClause) {
				limitingCriteria.append(" and ");
			}

			limitingCriteria.append(" anciOffer.anciTemplateType =:offerType ");

			addAndClause = true;
			noWhereClausesAdded = false;
		}

		if (!noWhereClausesAdded) {
			searchQueryStr.append(limitingCriteria);
		}

		sortingCriteria.append(searchCriteria.getSortColumn());
		sortingCriteria.append(" ");
		sortingCriteria.append(searchCriteria.getSortOrder());

		searchQueryStr.append(sortingCriteria);

		Query searchQuery = getSession().createQuery(searchQueryStr.toString());
		Query countQuery = getSession().createQuery(countQueryStr.append(searchQueryStr).toString());

		if (searchCriteria.getOND() != null) {
			searchQuery.setString("OND", searchCriteria.getOND());
			countQuery.setString("OND", searchCriteria.getOND());
		}

		if (searchCriteria.getFrom() != null && searchCriteria.getTo() != null) {
			searchQuery.setDate("flightFrom", searchCriteria.getFrom());
			searchQuery.setDate("flightTo", searchCriteria.getTo());

			countQuery.setDate("flightFrom", searchCriteria.getFrom());
			countQuery.setDate("flightTo", searchCriteria.getTo());
		}

		if (searchCriteria.getLcc() != null) {
			searchQuery.setString("lcc", searchCriteria.getLcc());
			countQuery.setString("lcc", searchCriteria.getLcc());
		}

		if (searchCriteria.getBookingClass() != null) {
			searchQuery.setString("bookingClass", searchCriteria.getBookingClass());
			countQuery.setString("bookingClass", searchCriteria.getBookingClass());
		}

		if (searchCriteria.getActive() != null) {
			searchQuery.setString("isActiveCriteria", searchCriteria.getActive());
			countQuery.setString("isActiveCriteria", searchCriteria.getActive());
		}

		if (searchCriteria.getAnciOfferType() != null) {
			searchQuery.setString("offerType", searchCriteria.getAnciOfferType().toString());
			countQuery.setString("offerType", searchCriteria.getAnciOfferType().toString());
		}

		@SuppressWarnings("unchecked")
		List<AnciOfferCriteria> resultList = searchQuery.setFirstResult(start).setMaxResults(recSize).list();

		Long totalRecords = (Long) countQuery.uniqueResult();

		return new Page<AnciOfferCriteria>(totalRecords.intValue(), start, start + recSize, resultList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateAnciOfferCriteria(AnciOfferCriteria anciOfferCriteria) {
		update(anciOfferCriteria);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer addAnciOfferCriteria(AnciOfferCriteria anciOfferCriteria) {
		return (Integer) hibernateSave(anciOfferCriteria);

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AnciOfferCriteria> searchApplicableAnciOffers(AnciOfferCriteriaSearchTO searchCriteria) {

		StringBuilder offerSearchQueryBuilder = new StringBuilder("FROM AnciOfferCriteria anciOffer ");

		StringBuilder offerSearchJoinQueryBuilder = new StringBuilder(
				" JOIN anciOffer.onds anciONDs JOIN anciOffer.lccs anciLCCs ");

		StringBuilder offerSearchWhereClauseBuilder = new StringBuilder(
				" WHERE :flightDate BETWEEN anciOffer.applicableFrom AND anciOffer.applicableTo AND anciOffer.active = 'Y' AND");

		offerSearchWhereClauseBuilder.append(" anciOffer.anciTemplateType =:offerType AND ");
		offerSearchWhereClauseBuilder.append(" (");
		offerSearchWhereClauseBuilder.append(" anciONDs in(:OND) AND anciLCCs like :lcc AND ");

		if (searchCriteria.isJourneyReturn() && searchCriteria.getInverseSegmentDepartureDate() != null) {
			if (searchCriteria.isSegmentReturn()) {
				offerSearchWhereClauseBuilder.append(" anciOffer.applicableOutboundDaysOfTheWeek like :inverseDayOfTheWeek AND ");
				offerSearchWhereClauseBuilder.append(" anciOffer.applicableInboundDaysOfTheWeek like :dayOfTheWeek AND ");
			} else {
				offerSearchWhereClauseBuilder.append(" anciOffer.applicableOutboundDaysOfTheWeek like :dayOfTheWeek AND ");
				offerSearchWhereClauseBuilder.append(" anciOffer.applicableInboundDaysOfTheWeek like :inverseDayOfTheWeek AND ");
			}
		} else {
			offerSearchWhereClauseBuilder.append(" anciOffer.applicableOutboundDaysOfTheWeek like :dayOfTheWeek AND ");
		}

		if (searchCriteria.isSegmentReturn()) {
			offerSearchWhereClauseBuilder.append(" anciOffer.applicableForReturn = 'Y' AND ");
		} else {
			offerSearchWhereClauseBuilder.append(" anciOffer.applicableForOneWay = 'Y' AND ");
		}

		offerSearchJoinQueryBuilder.append(" JOIN anciOffer.bookingClasses bookingClass ");
		offerSearchWhereClauseBuilder.append(" (bookingClass like :bookingClass OR bookingClass = 'ALL') AND ");

		if (searchCriteria.getFlexiID() == null) {
			offerSearchWhereClauseBuilder.append(" anciOffer.applyToFlexiOnly = 'N' AND ");
		} else {
			offerSearchWhereClauseBuilder.append(" anciOffer.applyToFlexiOnly = 'Y' AND ");
		}

		if (searchCriteria.isJourneyReturn()) {
			offerSearchWhereClauseBuilder.append(" anciOffer.eligibleForReturn = 'Y' AND ");
		} else {
			offerSearchWhereClauseBuilder.append(" anciOffer.eligibleForOneWay = 'Y' AND ");
		}

		offerSearchWhereClauseBuilder.append(" (");
		offerSearchWhereClauseBuilder
				.append(" (anciOffer.eligiblePaxCounts.adults =:adultCount OR anciOffer.eligiblePaxCounts.adults = -1) AND ");
		offerSearchWhereClauseBuilder
				.append(" (anciOffer.eligiblePaxCounts.children =:childCount OR anciOffer.eligiblePaxCounts.children = -1) AND ");
		offerSearchWhereClauseBuilder
				.append(" (anciOffer.eligiblePaxCounts.infants =:infantCount OR anciOffer.eligiblePaxCounts.infants = -1) ");
		offerSearchWhereClauseBuilder.append(" )");
		offerSearchWhereClauseBuilder.append(" )");

		offerSearchQueryBuilder.append(offerSearchJoinQueryBuilder).append(offerSearchWhereClauseBuilder);

		Query offerSearchQuery = getSession().createQuery(offerSearchQueryBuilder.toString());

		offerSearchQuery.setDate("flightDate", searchCriteria.getDepartureDate());
		offerSearchQuery.setString("offerType", searchCriteria.getAnciOfferType().toString());
		offerSearchQuery.setParameterList("OND", searchCriteria.getSearchONDSet());
		offerSearchQuery.setString("lcc", searchCriteria.getLcc());
		offerSearchQuery.setString("bookingClass", searchCriteria.getBookingClass());

		Calendar cal = Calendar.getInstance();
		cal.setTime(searchCriteria.getDepartureDate());
		int dayOfTheWeek = cal.get(Calendar.DAY_OF_WEEK);
		offerSearchQuery.setString("dayOfTheWeek", "%" + dayOfTheWeek + "%");

		if (searchCriteria.isJourneyReturn() && searchCriteria.getInverseSegmentDepartureDate() != null) {
			Calendar calInverse = Calendar.getInstance();
			calInverse.setTime(searchCriteria.getInverseSegmentDepartureDate());
			int inverseDayOfTheWeek = calInverse.get(Calendar.DAY_OF_WEEK);
			offerSearchQuery.setString("inverseDayOfTheWeek", "%" + inverseDayOfTheWeek + "%");
		}

		offerSearchQuery.setInteger("adultCount", searchCriteria.getAdults());
		offerSearchQuery.setInteger("childCount", searchCriteria.getChildren());
		offerSearchQuery.setInteger("infantCount", searchCriteria.getInfants());

		return offerSearchQuery.list();
	}

	@Override
	public AnciOfferCriteria getAnciOferByID(Integer id) {
		return (AnciOfferCriteria) get(AnciOfferCriteria.class, id);
	}

	@Override
	public List<AnciOfferCriteria> getActiveCriterisForType(String anciOfferType) {
		String baseQuery = "FROM AnciOfferCriteria anciOffer WHERE anciOffer.active = 'Y' AND anciOffer.anciTemplateType =:offerType";

		Query query = getSession().createQuery(baseQuery);
		query.setString("offerType", anciOfferType);

		@SuppressWarnings("unchecked")
		List<AnciOfferCriteria> results = query.list();

		return results;
	}
}
