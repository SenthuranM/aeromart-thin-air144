package com.isa.thinair.promotion.core.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.promotion.api.model.Campaign;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionTemplate;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.PromotionsSearchTo;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public interface PromotionManagementDao {

	public <T, K extends Serializable> T findEntity(Class<T> type, K key);

	public <T> void saveEntity(T t);

	public <T> void updateEntity(T t);

	public <T> void deleteEntities(Collection<T> entities);

	public <T> void saveEntities(Collection<T> entities);

	public void savePromotionTemplate(PromotionTemplate promotionTemplate);

	public void updatePromotionTemplate(PromotionTemplate promotionTemplate);

	public Page<PromotionTemplate> searchPromotionTemplates(PromotionsSearchTo promotionsSearchTo, Integer start, Integer size);

	public void savePromotionRequest(PromotionRequest promotionRequest);

	public void savePromotionRequests(Collection<PromotionRequest> promotionRequests);
	
	public PromotionRequest getApprovedPromotions(PromotionType promotionType, Integer pnrPaxId, Integer pnrSegId);

	public List<PromotionRequest> loadPaxPromotionRequests(Collection<Integer> pnrPaxIds);

	// Map < promotion_template_id, List<promotion_requests> >
	public Map<Integer, List<PromotionRequest>> getPromotionRequests(String pnr, Integer promotionId, boolean statusExclude,
			PromotionsInternalConstants.PromotionRequestStatus... statuses);

	// Map < pnr_pax_id , request_status >
	public Map<Integer, String> getPaxWisePromotions(String pnr);

	public List<String> getOverlappedPromotions(PromotionTemplate promotionTemplate);

	public List<ApplicablePromotionDetailsTO> pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria);

	public boolean isPromoCodeExists(String promoCode);

	public boolean checkPromotionConstraints(Long promoCriteriaId, List<Integer> flightIds, List<Integer> fltSegIds,
			String agentCode, Integer channelCode, int paxCount, int promoAppliedPaxCount) throws CommonsDataAccessException;
	
	public List<String> getCustomerEmailsForPromoCodes(CustomerPromoCodeSelectionRequest customerDetails);
	
	public void saveCampaign(Campaign campaign);
	
	public Campaign getCampaign(Integer campaignID);
}