package com.isa.thinair.promotion.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.model.DatePeriod;
import com.isa.thinair.promotion.api.model.FlightPeriod;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.RegistrationPeriod;
import com.isa.thinair.promotion.api.model.SysGenPromotionCriteria;
import com.isa.thinair.promotion.api.to.PromoCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.persistence.dao.PromotionCriteriaDAO;

/**
 * Class implementing the PromotionCriteria class hierachy's data access operations.
 * 
 * @author thihara
 * 
 * @isa.module.dao-impl dao-name="PromotionCriteriaDAO"
 */
public class PromotionCriteriaDAOImpl extends PlatformBaseHibernateDaoSupport implements PromotionCriteriaDAO {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PromotionCriteria findPromotionCriteria(Long promotionCriteriaID) {
		return (PromotionCriteria) get(PromotionCriteria.class, promotionCriteriaID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long savePromotionCriteria(PromotionCriteria promotionCriteria) {

		if (!isPromoCodeExist(promotionCriteria)) {
			return (long) -1;
		} else {
			return (Long) hibernateSave(promotionCriteria);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long updatePromotionCriteria(PromotionCriteria promotionCriteria) {

		PromotionCriteria existPromotion = findPromotionCriteria(promotionCriteria.getPromoCriteriaID());
		if (existPromotion == null) {
			return (long) -1;
		} else {
			if (existPromotion.getI18nMessageKey() != null) {
				String messageKey = PromotionsUtils.PROMO_CODE_LANG_KEY + promotionCriteria.getPromoCriteriaID();
				promotionCriteria.setI18nMessageKey(messageKey);
			}
			removeDatePeriodsById(promotionCriteria.getPromoCriteriaID(), RegistrationPeriod.class);
			removeDatePeriodsById(promotionCriteria.getPromoCriteriaID(), FlightPeriod.class);
			evict(existPromotion);
			update(promotionCriteria);
			return 1;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deletePromotionCriteria(PromotionCriteria promotionCriteria) {
		delete(promotionCriteria);

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<PromotionCriteria> searchPromotionCriteria(PromoCriteriaSearchTO promoCriteriaSearch, Integer start, Integer size) {

		/*
		 * While the criteria API is better suited for a query like this it is not possible to carry out filtering on
		 * element collections. On the current ancient version of hibernate we are using or in the newest hibernate
		 * versions. But hibernate4 will probably have this feature implemented. If so anyone modifying this in the
		 * future please upgrade and switch to Criteria.
		 * 
		 * One to many mapping is not possible due to modularization (most of the classes are in other modules).
		 * Criteria query limitation doesn't apply to one to many mappings etc. Only to element collections.
		 */

		// Query builder
		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder(
				"SELECT pc FROM PromotionCriteria pc ");
		// if (promoCriteriaSearch.getPromotionCode() != null) {
		// }
		StringBuilder promoConditionClause = new StringBuilder(" WHERE pc.systemGenerated=:sysGen ");
		StringBuilder promoOrderClause = new StringBuilder(" ORDER BY pc.promoCriteriaID DESC ");
		/*
		 * String updateStr = "update t_promotion_criteria set status='" + promotionCriteria.getStatus() +
		 * "' where promo_criteria_id=" + promotionCriteria.getPromoCriteriaID(); int result = bulkUpdate(updateStr);
		 */
		// Final query string
		String promoCriteriaQueryString = null;

		if (promoCriteriaSearch.getOND() != null) {
			promoCriteriaQueryStringBuilder.append(" join pc.applicableONDs pconds ");
			promoConditionClause.append(" AND pconds like :OND ");
		}
		if (promoCriteriaSearch.getReservationFrom() != null && promoCriteriaSearch.getReservationTo() != null) {
			promoCriteriaQueryStringBuilder.append(" join pc.registrationPeriods prp ");
			promoConditionClause.append(" AND prp.fromDate >= :reservationFrom ");
			promoConditionClause.append(" AND prp.toDate <= :reservationTo ");
		}
		// Changed according to multiple promo
		if (promoCriteriaSearch.getPromotionCode() != null) {
			promoCriteriaQueryStringBuilder.append(" join pc.promoCodes code ");
			promoConditionClause.append(" AND upper(code.promoCode) = :promoCode ");
		}
		// if (promoCriteriaSearch.getPromotionCode() != null) {
		// promoConditionClause.append(" AND upper(pc.promoCode) = :promoCode ");
		// }
		if (promoCriteriaSearch.getStatus() != null) {
			promoConditionClause.append(" AND pc.status = :status ");
		}

		// If no search parameters are defined then select all data.
		promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString()
				+ promoOrderClause.toString();

		Query query = getSession().createQuery(promoCriteriaQueryString);

		query.setCharacter("sysGen", 'N');

		// Bind the parameters
		if (promoCriteriaSearch.getReservationFrom() != null && promoCriteriaSearch.getReservationTo() != null) {
			query.setDate("reservationFrom", promoCriteriaSearch.getReservationFrom());
			query.setDate("reservationTo", promoCriteriaSearch.getReservationTo());
		}
		if (promoCriteriaSearch.getOND() != null) {
			query.setParameter("OND", promoCriteriaSearch.getOND());
		}
		if (promoCriteriaSearch.getPromotionCode() != null) {
			query.setString("promoCode", promoCriteriaSearch.getPromotionCode().toUpperCase());
		}
		if (promoCriteriaSearch.getStatus() != null) {
			query.setString("status", promoCriteriaSearch.getStatus());
		}

		int count = query.list().size();

		query.setFirstResult(start).setMaxResults(size);

		Collection<PromotionCriteria> results = query.list();

		return new Page<PromotionCriteria>(count, start, start + results.size(), results);
	}

	@Override
	public void updateSystemGeneratedPromotion(Long promotionId, BigDecimal consumedCredits, String status)
			throws CommonsDataAccessException {

		PromotionCriteria promotionCriteria = findPromotionCriteria(promotionId);
		if (promotionCriteria instanceof SysGenPromotionCriteria) {
			SysGenPromotionCriteria sysGenPromotionCriteria = (SysGenPromotionCriteria) promotionCriteria;
			if (PromotionCriteriaConstants.PromotionStatus.INACTIVE.equals(sysGenPromotionCriteria.getStatus())) {
				throw new CommonsDataAccessException("promotion.already.used");
			}
			sysGenPromotionCriteria.setUsedCredit(consumedCredits);
			sysGenPromotionCriteria.setStatus(status);
			savePromotionCriteria(sysGenPromotionCriteria);
		} else {
			throw new CommonsDataAccessException("promotion.invalid.promotion");
		}

	}

	@Override
	public void updateSystemGeneratedPromotionForOHD(Long promotionId, BigDecimal consumedCredits, String status)
			throws CommonsDataAccessException {

		PromotionCriteria promotionCriteria = findPromotionCriteria(promotionId);
		if (promotionCriteria instanceof SysGenPromotionCriteria) {
			SysGenPromotionCriteria sysGenPromotionCriteria = (SysGenPromotionCriteria) promotionCriteria;
			sysGenPromotionCriteria.setUsedCredit(consumedCredits);
			sysGenPromotionCriteria.setStatus(status);
			savePromotionCriteria(sysGenPromotionCriteria);
		} else {
			throw new CommonsDataAccessException("promotion.invalid.promotion");
		}

	}

	@Override
	public boolean isPromoCodeExist(PromotionCriteria promotionCriteria) {

		StringBuilder promoCriteriaQueryStringBuilder = new StringBuilder(
				"SELECT DISTINCT pc FROM PromotionCriteria pc, PromoCode code");
		StringBuilder promoConditionClause = new StringBuilder(" WHERE pc.promoCodeEnabled='Y' ");

		if (promotionCriteria.getPromoCode() != null) {
			promoConditionClause.append("AND pc = code.promoCriteria ");
			promoConditionClause.append("AND code.promoCode=:promoCode ");
		}
		if (promotionCriteria.getPromoCriteriaID() != null) {
			promoConditionClause.append("AND pc.promoCriteriaID!=:promoCodeID ");
		}
		String promoCriteriaQueryString = promoCriteriaQueryStringBuilder.toString() + promoConditionClause.toString();
		Query query = getSession().createQuery(promoCriteriaQueryString);
		if (promotionCriteria.getPromoCode() != null) {
			query.setString("promoCode", promotionCriteria.getPromoCode());
		}
		if (promotionCriteria.getPromoCriteriaID() != null) {
			query.setLong("promoCodeID", promotionCriteria.getPromoCriteriaID());
		}

		int count = query.list().size();

		if (count == 1) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public long updatePromotionCriteriaStatus(PromotionCriteria promotionCriteria) {
		PromotionCriteria promotionCriteriaUpdate = findPromotionCriteria(promotionCriteria.getPromoCriteriaID());
		promotionCriteriaUpdate.setStatus(promotionCriteria.getStatus());
		update(promotionCriteriaUpdate);
		return 1;

	}

	@Override
	@SuppressWarnings("rawtypes")
	public PromotionCriteria getPromotionCriteria(String originPnr) {
		String hql = "SELECT sysGen FROM SysGenPromotionCriteria sysGen WHERE sysGen.originPnr='" + originPnr + "'";
		Query qry = getSession().createQuery(hql);
		List lst = qry.list();
		return (lst != null && !lst.isEmpty()) ? (PromotionCriteria) lst.iterator().next() : null;
	}

	@Override
	public void activateSystemGeneratedPromotion(String originPnr) {
		StringBuilder hql = new StringBuilder("UPDATE SysGenPromotionCriteria SET status=:status ");
		hql.append("WHERE originPnr=:originPnr AND usedCredit=:usedCredit");

		Query qry = getSession().createQuery(hql.toString())
				.setParameter("status", PromotionCriteriaConstants.PromotionStatus.ACTIVE).setParameter("originPnr", originPnr)
				.setParameter("usedCredit", AccelAeroCalculator.getDefaultBigDecimalZero());
		qry.executeUpdate();

	}

	@Override
	public void updatePromotionCodeDescKey(long criteriaId, String messageKey) {
		messageKey = PromotionsUtils.PROMO_CODE_LANG_KEY + criteriaId;
		StringBuilder hql = new StringBuilder("UPDATE PromotionCriteria SET i18nMessageKey=:messageKey ");
		hql.append("WHERE promoCriteriaID=:criteriaId");

		Query qry = getSession().createQuery(hql.toString()).setParameter("criteriaId", criteriaId).setParameter("messageKey",
				messageKey);
		qry.executeUpdate();

	}

	@Override
	public void updatepromoCodeUtilization(String promoCode) {
		StringBuilder sql = new StringBuilder("UPDATE T_PROMO_CRITERIA_CODE SET FULLY_UTILIZED = 'Y' ");
		sql.append("WHERE PROMO_CODE = '" + promoCode + "' ");

		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.executeUpdate();

	}

	@Override
	public List<String> getPromoCodesById(String promoCriteriaId) {
		StringBuilder sql = new StringBuilder("SELECT PROMO_CODE FROM T_PROMO_CRITERIA_CODE ");
		sql.append("WHERE PROMO_CRITERIA_ID = '" + promoCriteriaId + "' ");

		SQLQuery query = getSession().createSQLQuery(sql.toString());
		List<String> resultCodes = query.list();

		return resultCodes;
	}

	@Override
	public <T extends DatePeriod> void removeDatePeriodsById(long promoCriteriaId, Class<T> clazz) {
		StringBuilder sql = new StringBuilder("DELETE FROM ");
		if (clazz.isInstance(new RegistrationPeriod())) {
			sql.append("T_PROMO_CRITERIA_RES_PERIOD ");
		} else {
			sql.append("T_PROMO_CRITERIA_FLIGHT_PERIOD ");
		}
		sql.append("WHERE PROMO_CRITERIA_ID = '" + promoCriteriaId + "' ");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}
}
