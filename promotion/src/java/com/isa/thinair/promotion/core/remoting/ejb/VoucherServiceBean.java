package com.isa.thinair.promotion.core.remoting.ejb;

import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateDTO;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.Voucher;
import com.isa.thinair.promotion.api.model.VoucherTermsTemplate;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;
import com.isa.thinair.promotion.api.to.VoucherSearchRequest;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.core.bl.voucher.VoucherBO;
import com.isa.thinair.promotion.core.persistence.dao.VoucherDAO;
import com.isa.thinair.promotion.core.service.bd.VoucherBDLocal;
import com.isa.thinair.promotion.core.service.bd.VoucherBDRemote;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.promotion.core.utils.CommandNames;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author chethiya
 *
 */
@Stateless
@LocalBinding(jndiBinding = "Voucher.local")
@RemoteBinding(jndiBinding = "Voucher.remote")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class VoucherServiceBean extends PlatformBaseSessionBean implements VoucherBDLocal, VoucherBDRemote {

	Log log = LogFactory.getLog(VoucherServiceBean.class);

	@Override
	public void saveVoucher(Voucher voucher) throws ModuleException {
		try {
			VoucherDAO VoucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			VoucherDAO.saveVoucher(voucher);
		} catch (Exception ex) {
			log.error("VoucherBean ==> saveVoucher(Voucher)", ex);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public VoucherRedeemResponse redeemIssuedVouchers(VoucherRedeemRequest voucherRedeemRequest) throws ModuleException {

		VoucherRedeemResponse voucherRedeemResponse = null;
		try {
			voucherRedeemResponse = VoucherBO.redeemIssuedVouchers(voucherRedeemRequest);
		} catch (ModuleException ex) {
			log.error("VoucherBean ==> saveVoucher(Voucher)", ex);
			if (ex.getExceptionCode().equals("error.voucher.redeem.invalid") || ex.getExceptionCode()
					.equals("error.voucher.redeem.canceled") || ex.getExceptionCode()
					.equals("error.voucher.pax.name.invalid") || ex.getExceptionCode()
					.equals("error.voucher.mobile.number.invalid") || ex.getExceptionCode()
					.equals("error.voucher.otp.code.invalid") || ex.getExceptionCode()
					.equals("error.voucher.valid.period.invalid")) {
				throw ex;
			}
		}
		return voucherRedeemResponse;
	}

	@Override
	public Page<VoucherDTO> searchVoucher(int start, int pageSize, VoucherSearchRequest voucherSearchRequest, String voucherType)
			throws ModuleException {
		Page<VoucherDTO> voucherPage = null;
		try {
			voucherPage = VoucherBO.searchVoucher(start, pageSize, voucherSearchRequest, voucherType);
		} catch (Exception ex) {
			log.error("VoucherBean ==> issueVoucher()", ex);
		}
		return voucherPage;
	}

	@Override
	public void updateVoucher(Voucher voucher) throws ModuleException {
		try {
			VoucherDAO VoucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			VoucherDAO.updateVoucher(voucher);
		} catch (Exception ex) {
			log.error("VoucherBean ==> updateVoucher(Voucher)", ex);
		}
	}

	@Override
	public List<VoucherDTO> getVouchersFromGroupId(String groupId) throws ModuleException {
		try {
			VoucherDAO VoucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			return VoucherBO.getVoucherInfoList(VoucherDAO.getVouchersFromGroupId(groupId));
		} catch (Exception ex) {
			log.error("VoucherBean ==> updateVoucher(Voucher)", ex);
			throw ex;
		}
	}

	@Override
	public void issueVoucher(VoucherDTO voucherDTO) throws ModuleException {

		try {
			voucherDTO.setVoucherId(VoucherBO.generateVoucherID());
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.ISSUE_VOUCHER_MACRO);
			command.setParameter(CommandParamNames.VOUCHER_DETAILS, voucherDTO);
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("VoucherBean ==> issueVoucher()", ex);
		}
	}

	@Override
	public void cancelVoucher(VoucherDTO voucherDTO, UserPrincipal userPrincipal) throws ModuleException {
		try {
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.CANCEL_VOUCHER_MACRO);
			command.setParameter(CommandParamNames.VOUCHER_CANCEL, voucherDTO);
			command.setParameter(CommandParamNames.VOUCHER_CANCEL_USER, userPrincipal);
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("VoucherBean ==> cancelVoucher()", ex);
		}

	}

	@Override
	public void emailVoucher(VoucherDTO voucherDTO) throws ModuleException {
		try {
			VoucherBO.emailVoucher(voucherDTO);
		} catch (Exception ex) {
			log.error("VoucherBean ==> emailVoucher()", ex);
		}

	}

	@Override
	public String printVoucher(VoucherDTO voucherDTO) throws ModuleException {

		String content = "";
		try {
			content += VoucherBO.printVoucher(voucherDTO);
		} catch (Exception ex) {
			log.error("VoucherBean ==> printVoucher()", ex);
		}
		return content;
	}

	@Override
	public void recordRedemption(String voucherID, Integer paxTnxID, String redeemAmount, Integer extCarrierPaxTnxID)
			throws ModuleException {
		try{
			VoucherBO.recordRedemption(voucherID, paxTnxID, redeemAmount, extCarrierPaxTnxID);
		} catch (Exception ex) {
			log.error("VoucherBean ==> recordRedemption(String,Integer)", ex);
		}
	}

	@Override
	public void unblockRedeemedVouchers(ArrayList<String> voucherIDList) throws ModuleException {
		try {
			VoucherBO.unblockVoucherList(voucherIDList);
		} catch (Exception ex) {
			log.error("VoucherBean ==> unblockRedeemedVouchers(ArrayList<String>)", ex);
		}
	}
	
	@Override
	public Page<VoucherTermsTemplateDTO> getVoucherTermsTemplatePage(VoucherTermsTemplateSearchDTO searchCriteria, Integer start,
			Integer recSize) throws ModuleException {
		try {
			Page<VoucherTermsTemplate> termsTemplatePage = PromotionsHandlerFactory.getVoucherTermsTemplateDAO().getVoucherTermsTemplatePage(
					searchCriteria, start, recSize);

			return VoucherBO.convertVoucherPageToDTO(termsTemplatePage);

		} catch (Exception e) {
			throw new ModuleException(e, "module.runtime.error");
		}
	}
	
	@Override
	public void updateVoucherTermsTemplate(VoucherTermsTemplateDTO termsTemplateDTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		try {
			VoucherTermsTemplate termsTemplate = VoucherBO.toVoucherTermsTemplate(termsTemplateDTO);
			PromotionsHandlerFactory.getVoucherTermsTemplateDAO().updateVoucherTermsTemplate(termsTemplate);
			
			Audit audit = VoucherBO.generateAudit(termsTemplate, trackInfoDTO);
			try {
				PromotionModuleServiceLocator.getAuditorBD().audit(audit, null);
			} catch (ModuleException e) {
				throw new ModuleException(e.getMessage(), e);
			}

		} catch (Exception e) {
			throw new ModuleException(e.getMessage(), e);
		}
	}
	
	@Override
	public Collection<VoucherTermsTemplateDTO> getVoucherTermsTemplates() throws ModuleException {
		List<VoucherTermsTemplate> termsTemplates = PromotionsHandlerFactory.getVoucherTermsTemplateDAO().getAllVoucherTermsTemplates();
		return VoucherBO.toVoucherTermsTemplateDTOCOllection(termsTemplates);
	}
	
	@Override
	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize,
			ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) throws ModuleException {
		Page<ReprotectedPaxDTO> rePaxPage = null;
		try {
			rePaxPage = VoucherBO.searchReprotectedPassengers(start, pageSize, reprotectedPaxSearchrequest);
		} catch (Exception ex) {
			log.error("VoucherBean ==> issueVoucher()", ex);
		}
		return rePaxPage;

	}

	@Override
	public void issueVouchersForReprotectedPax(ArrayList<ReprotectedPaxDTO> selectedPax, ArrayList<VoucherDTO> voucherDTOs, String issueBy) {
		try {
			VoucherBO.issueVouchersForReprotectedPax(selectedPax, voucherDTOs, issueBy);
		} catch (Exception ex) {
			log.error("VoucherBean ==> issueVoucherForReprotectedPax()", ex);
		}
		
	}

	@Override
	public void expireVouchers() {
		try {
			VoucherBO.expireVouchers();
		} catch (Exception ex) {
			log.error("VoucherBean ==> expireVouchers()", ex);
		}		
	}
	
	public List<VoucherDTO> prepareVouchersForIBEPayment(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmount)
			throws ModuleException {
		populateVoucherInfo(voucherDTOs);
			voucherDTOs = VoucherBO.recordTempTransactionEntryForBulkIssue(voucherDTOs, payCurrency, payCurrencyAmount);
			return voucherDTOs;
	}

	public List<VoucherDTO> prepareVouchersForIBEPayment(List<VoucherDTO> voucherDTOs, String payCurrency,
			String payCurrencyAmount, BigDecimal payAmountInBase, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		populateVoucherInfo(voucherDTOs);
		voucherDTOs = VoucherBO.recordTempTransactionEntryForBulkIssue(voucherDTOs, payCurrency, payCurrencyAmount,
				payAmountInBase, trackInfoDTO);
		return voucherDTOs;
	}

	private void populateVoucherInfo(List<VoucherDTO> voucherDTOs) throws ModuleException {
		String voucherGroupID = VoucherBO.generateVoucherGroupID();
		int nominalCode = PaymentType
				.getReservationTnxNominalCodeForPayment(Integer.parseInt(voucherDTOs.get(0).getVoucherPaymentDTO().getCardType())).getCode();
		for(VoucherDTO voucherDTO : voucherDTOs){
			String voucherID = VoucherBO.generateVoucherID();
			voucherDTO.setVoucherId(voucherID);
			voucherDTO.setVoucherGroupId(voucherGroupID);
			voucherDTO.getVoucherPaymentDTO().setVoucherID(voucherID);
			voucherDTO.getVoucherPaymentDTO().setNominalCode(nominalCode);
		}
	}

	@Override
	public void issueGiftVouchersFromIBE(List<VoucherDTO> voucherDTOs) throws ModuleException {		
		try {
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.SELL_GIFT_VOUCHERS_IBE_MACRO);
			command.setParameter(CommandParamNames.VOUCHER_LIST_DETAILS, voucherDTOs);
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("VoucherBean ==> issueVouchersFromIBE()", ex);
			throw ex;
		}
	}

	@Override
	public void issueGiftVouchersFromXBE(List<VoucherDTO> voucherDTOs, CredentialsDTO credentialsDTO, String paymentMethod, String fullAmountInBase) throws ModuleException {
		try {
			String voucherGroupID = VoucherBO.generateVoucherGroupID();
			int nominalCode = 0;
			int tempTnxId = 0;
			if(paymentMethod.equals(PromotionCriteriaConstants.VoucherPaymentType.CASH_PAYMENT)){
				nominalCode = ReservationTnxNominalCode.CASH_PAYMENT.getCode();
			} else if (paymentMethod.equals(PromotionCriteriaConstants.VoucherPaymentType.ONACCOUNT_PAYMENT)) {
				nominalCode = ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode();
			} else if (paymentMethod.equals(PromotionCriteriaConstants.VoucherPaymentType.CREDIT_CARD_PAYMENT)){
				nominalCode = PaymentType.getReservationTnxNominalCodeForPayment(
						Integer.parseInt(voucherDTOs.get(0).getVoucherPaymentDTO().getCardType())).getCode();
				tempTnxId = VoucherBO.recordTempTransactionEntryForBulkIssueXBE(voucherDTOs, fullAmountInBase, voucherGroupID);
			}
			for (VoucherDTO voucherDTO : voucherDTOs){
				voucherDTO.setVoucherId(VoucherBO.generateVoucherID());
				voucherDTO.setVoucherGroupId(voucherGroupID);
				voucherDTO.getVoucherPaymentDTO().setNominalCode(nominalCode);
				voucherDTO.getVoucherPaymentDTO().setVoucherID(voucherDTO.getVoucherId());
				voucherDTO.getVoucherPaymentDTO().setTempPaymentID(tempTnxId);
				String voucherAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero().toString();
				if (voucherDTO.getAmountInBase() != null) {
					voucherAmountInBase = voucherDTO.getAmountInBase();
				} else if (voucherDTO.getCurrencyCode() != null && AppSysParamsUtil.getBaseCurrency()
						.equals(voucherDTO.getCurrencyCode())) {
					voucherAmountInBase = voucherDTO.getAmountInLocal();
				}
				voucherDTO.setAmountInBase(voucherAmountInBase);
			}
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.SELL_GIFT_VOUCHER_MACRO);
			command.setParameter(CommandParamNames.VOUCHER_LIST_DETAILS, voucherDTOs);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.TOTAL_BASE, fullAmountInBase);
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("VoucherBean ==> sellGiftVoucher()", ex);
			throw new ModuleException("airreservations.arg.cc.error", ex);
		}
		
	}

	@Override
	public void unblockVouchers() {
		try {
			VoucherBO.unblockVouchers();
		} catch (Exception ex) {
			log.error("VoucherBean ==> unblockVouchers()", ex);
		}			
	}
	
	@Override
	public void issueGiftVouchersForIBE(List<VoucherDTO> voucherDTOs, CredentialsDTO credentialsDTO, String fulAmountInBase)
			throws ModuleException {
		try {
			String voucherGroupID = voucherDTOs.get(0).getVoucherGroupId();
			int nominalCode = 0;
			int tempTnxId = 0;
			nominalCode = PaymentType.getReservationTnxNominalCodeForPayment(
					Integer.parseInt(voucherDTOs.get(0).getVoucherPaymentDTO().getCardType())).getCode();
			tempTnxId = VoucherBO.recordTempTransactionEntryForBulkIssueXBE(voucherDTOs, fulAmountInBase, voucherGroupID);
			voucherDTOs = VoucherBO.setPaymentAmountData(voucherDTOs);

			for (VoucherDTO voucherDTO : voucherDTOs) {
				voucherDTO.getVoucherPaymentDTO().setNominalCode(nominalCode);
				voucherDTO.getVoucherPaymentDTO().setTempPaymentID(tempTnxId);
			}
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.SELL_GIFT_VOUCHER_MACRO);
			command.setParameter(CommandParamNames.VOUCHER_LIST_DETAILS, voucherDTOs);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.TOTAL_BASE, fulAmountInBase);
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("VoucherBean ==> sellGiftVoucher()", ex);
			throw new ModuleException("airreservations.arg.cc.error", ex);
		}
	}

	@Override
	public Voucher getVoucher(String voucherID) throws ModuleException {
		Voucher voucher = null;
		try {
			VoucherDAO voucherDAO = PromotionsHandlerFactory.getVoucherManagmentDAO();
			voucher = voucherDAO.getVoucher(voucherID);
		} catch (Exception ex) {
			log.error("VoucherBean ==> voucher not available", ex);
		}
		return voucher;
	}
}

	
		

	
	
	
	

