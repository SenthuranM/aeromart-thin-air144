package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * 
 * 1) Parent and Child cannot have "EXIT" seats
 * 
 * 2) Maximum allowed infants in a row chunk is 1
 * 
 * 3) Parents are allowed to reserve seats in seat map with
 * 
 * a)even row_group_ids
 * 
 * b)odd row_group_ids and odd col_id (Suggestion: why we cant move this restriction criteria to db config, it will
 * offer much flexibilities than this!
 * 
 */
public class FlightSeatValidator {

	private static final Log log = LogFactory.getLog(FlightSeatValidator.class);
	private static final int MAX_INFANT_COUNT_IN_A_ROW_CHUNK = 1;

	protected static boolean isAllowedSeatCombination(Integer flightSegId, String paxType, FlightSeatStatusTO currentSeat,
			List<FlightSeatStatusTO> nominalSeats) {
		boolean allowed = false;

		if (paxType.equals(ReservationInternalConstants.PassengerType.PARENT)) {
			log.info("Parent found");
			if (!isExitSeat(currentSeat, nominalSeats)) {
				if (isSeatAllowedForParent(currentSeat, nominalSeats)) {
					if (!isMaximimInfantCountReached(flightSegId, currentSeat, nominalSeats)) {
						allowed = true;
					}

				}
			}
		} else if (paxType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
			log.info("Child found");
			if (!isExitSeat(currentSeat, nominalSeats)) {
				allowed = true;
			}
		} else {
			allowed = true;
		}

		return allowed;

	}

	/**
	 * Parent and Child cannot have "EXIT" seats
	 * 
	 */
	private static boolean isExitSeat(FlightSeatStatusTO currentSeat, List<FlightSeatStatusTO> nominalSeats) {
		List<FlightSeatStatusTO> seatsToCheck = new ArrayList<FlightSeatStatusTO>();
		seatsToCheck.add(currentSeat);
		seatsToCheck.addAll(nominalSeats);

		for (FlightSeatStatusTO applicantSeat : seatsToCheck) {
			String seatType = applicantSeat.getSeatType();
			if (seatType != null && seatType.equalsIgnoreCase("EXIT")) {
				return true;
			}

		}
		return false;

	}

	/**
	 * 
	 * Parents are allowed to reserve seats in seat map with
	 * 
	 * a)even row_group_ids
	 * 
	 * b)odd row_group_ids and odd col_id
	 */
	private static boolean isSeatAllowedForParent(FlightSeatStatusTO currentSeat, List<FlightSeatStatusTO> nominalSeats) {
		boolean allowed = false;

		List<FlightSeatStatusTO> seatsToCheck = new ArrayList<FlightSeatStatusTO>();
		seatsToCheck.add(currentSeat);
		seatsToCheck.addAll(nominalSeats);

		for (FlightSeatStatusTO applicant : seatsToCheck) {

			Integer rowGroupId = currentSeat.getRowGroupId();
			boolean isInOddRowGroup = (rowGroupId % 2 != 0) ? true : false;
			Integer colId = currentSeat.getColId();
			boolean isOddColId = (colId % 2 != 0) ? true : false;

			if (isInOddRowGroup) {
				if (isOddColId) {
					allowed = true;
					break;
				}
			} else {
				allowed = true;
				break;
			}
		}

		return allowed;
	}

	/**
	 * 
	 * Maximum allowed infants in a row chunk is 1
	 * 
	 */
	private static boolean isMaximimInfantCountReached(Integer flightSegId, FlightSeatStatusTO currentSeat,
			List<FlightSeatStatusTO> nominalSeats) {
		boolean reached = false;
		SeatMapDAO seatMapDAO = PromotionModuleServiceLocator.getSeatMapDAO();
		List<FlightSeat> subordinateSeats = seatMapDAO.getFlightSeatsInaRowChunck(flightSegId, currentSeat.getCabinClassCode(),
				currentSeat.getLogicalCabinClassCode(), currentSeat.getRowGroupId(), currentSeat.getColId());

		List<String> seatsForPromoOffer = new ArrayList<String>();
		seatsForPromoOffer.add(currentSeat.getSeatCode());

		for (FlightSeatStatusTO nominalSeat : nominalSeats) {
			seatsForPromoOffer.add(nominalSeat.getSeatCode());
		}

		int infantsInARow = 1;
		if (!subordinateSeats.isEmpty()) {
			for (FlightSeat subbordinateSeat : subordinateSeats) {
				if (!seatsForPromoOffer.contains(subbordinateSeat.getSeatCode())) {
					// other seats in the row chunck
					String seatStatus = subbordinateSeat.getStatus();
					if (seatStatus.equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)) {
						String neighbourPaxType = subbordinateSeat.getPaxType();

						if (neighbourPaxType != null
								&& neighbourPaxType.equals(ReservationInternalConstants.PassengerType.PARENT)) {
							infantsInARow++;
						}

					}
				}
			}
		}

		if (infantsInARow > MAX_INFANT_COUNT_IN_A_ROW_CHUNK) {
			reached = true;
		}

		return reached;
	}

}
