package com.isa.thinair.promotion.core.persistence.hibernate;

import java.io.Serializable;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.promotion.api.model.Campaign;
import com.isa.thinair.promotion.api.model.OndPromotion;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionTemplate;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.PromotionsSearchTo;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * @isa.module.dao-impl dao-name="PromotionManagementDao"
 */
public class PromotionManagementDaoImpl extends PlatformBaseHibernateDaoSupport implements PromotionManagementDao {

	public <T, K extends Serializable> T findEntity(Class<T> type, K key) {
		return (T) get(type, key);
	}

	public <T> void saveEntity(T t) {
		super.hibernateSaveOrUpdate(t);
	}

	public <T> void updateEntity(T t) {
		super.update(t);
	}

	public <T> void deleteEntities(Collection<T> entities) {
		super.deleteAll(entities);
	}

	public <T> void saveEntities(Collection<T> entities) {
		super.hibernateSaveOrUpdateAll(entities);
	}

	public void savePromotionTemplate(PromotionTemplate promotionTemplate) {
		hibernateSaveOrUpdate(promotionTemplate);

	}

	public void updatePromotionTemplate(PromotionTemplate promotionTemplate) {
		hibernateSaveOrUpdate(promotionTemplate);

	}

	public Page<PromotionTemplate> searchPromotionTemplates(PromotionsSearchTo promotionsSearchTo, Integer start, Integer size) {
		boolean filterByType = false;
		boolean filterByStatus = false;
		String dataSec = " select t ";
		String countSec = " select count(*) ";
		StringBuilder sbCommonSec = new StringBuilder();
		sbCommonSec.append(" from  PromotionTemplate t where ");

		if (promotionsSearchTo.getPromotionStartDate() == null) {
			promotionsSearchTo.setPromotionStartDate(new Date());
		}

		if (promotionsSearchTo.getPromotionEndDate() == null) {
			promotionsSearchTo.setPromotionEndDate(new Date());
		}

		if (promotionsSearchTo.getPromotionType().equals(-1)) {
			filterByType = true;
		} else {
			sbCommonSec.append(" t.promotionType.promotionId = :promotionTypeId and  ");
		}

		if (!promotionsSearchTo.getPromotionStatus().equals("ALL")) {
			sbCommonSec.append(" t.status = :status and  ");
			filterByStatus = true;
		}

		sbCommonSec.append(" ( :endDate >= t.startDate and :startDate <= t.endDate ) ");

		String commonSec = sbCommonSec.toString();

		Query query = getSession().createQuery(dataSec + commonSec);
		if (!filterByType) {
			query.setParameter("promotionTypeId", promotionsSearchTo.getPromotionType());
		}
		if (filterByStatus) {
			query.setParameter("status", promotionsSearchTo.getPromotionStatus());
		}

		query.setParameter("startDate", promotionsSearchTo.getPromotionStartDate());
		query.setParameter("endDate", promotionsSearchTo.getPromotionEndDate());
		List<PromotionTemplate> results = query.setFirstResult(start).setMaxResults(size).list();

		Query queryCount = getSession().createQuery(countSec + commonSec);
		if (!filterByType) {
			queryCount.setParameter("promotionTypeId", promotionsSearchTo.getPromotionType());
		}
		if (filterByStatus) {
			queryCount.setParameter("status", promotionsSearchTo.getPromotionStatus());
		}
		queryCount.setParameter("startDate", promotionsSearchTo.getPromotionStartDate());
		queryCount.setParameter("endDate", promotionsSearchTo.getPromotionEndDate());
		int count = ((Long) queryCount.uniqueResult()).intValue();

		return new Page<PromotionTemplate>(count, start, start + results.size(), results);
	}

	public void savePromotionRequest(PromotionRequest promotionRequest) {
		hibernateSaveOrUpdate(promotionRequest);

	}

	public void savePromotionRequests(Collection<PromotionRequest> promotionRequests) {
		hibernateSaveOrUpdateAll(promotionRequests);
	}

	public Map<Integer, List<PromotionRequest>> getPromotionRequests(String pnr, Integer promotionId, boolean statusExclude,
			PromotionsInternalConstants.PromotionRequestStatus... statuses) {

		Map<Integer, List<PromotionRequest>> promotionReqsMapping = new HashMap<Integer, List<PromotionRequest>>();
		List<PromotionRequest> promoReqs;
		List<String> excludeStatusLabels = new ArrayList<String>();
		Criteria criteria = getSession().createCriteria(PromotionRequest.class);

		if (pnr != null) {
			criteria.add(Restrictions.eq("pnr", pnr));
		}

		if (statuses.length > 0) {
			for (PromotionsInternalConstants.PromotionRequestStatus status : statuses) {
				excludeStatusLabels.add(status.getStatusCode());
			}

			if (statusExclude) {
				criteria.add(Restrictions.not(Restrictions.in("status", excludeStatusLabels)));
			} else {
				criteria.add(Restrictions.in("status", excludeStatusLabels));
			}
		}
		if (promotionId != null) {
			criteria.add(Restrictions.eq("promotionTemplate.promotionId", promotionId));
		}

		promoReqs = criteria.list();

		for (PromotionRequest promotionReq : promoReqs) {
			if (!promotionReqsMapping.containsKey(promotionReq.getPromotionTemplate().getPromotionId())) {
				promotionReqsMapping.put(promotionReq.getPromotionTemplate().getPromotionId(), new ArrayList<PromotionRequest>());
			}

			promotionReqsMapping.get(promotionReq.getPromotionTemplate().getPromotionId()).add(promotionReq);
		}

		return promotionReqsMapping;
	}

	public Map<Integer, String> getPaxWisePromotions(String pnr) {
		Map<Integer, String> paxWisePromos = new HashMap<Integer, String>();

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT rp.pnr_pax_id pnr_pax_id , pr.status status FROM t_pnr_passenger rp , t_promotion_request pr ")
				.append("   WHERE rp.pnr_pax_id = pr.pnr_pax_id AND rp.pnr = :pnr ");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("pnr", pnr);
		query.addScalar("pnr_pax_id", IntegerType.INSTANCE);
		query.addScalar("status", StringType.INSTANCE);

		List<Object[]> results = query.list();

		for (Object[] singleResult : results) {
			paxWisePromos.put((Integer) singleResult[0], (String) singleResult[1]);
		}

		return paxWisePromos;
	}

	public List<String> getOverlappedPromotions(PromotionTemplate promotionTemplate) {

		StringBuilder queryContent = new StringBuilder();
		queryContent.append("		SELECT DISTINCT op.ond_code ond_code FROM t_promotion p , t_ond_promotion op	").append(
				"	    WHERE p.promotion_id = op.promotion_id AND ( :startDate < p.end_date AND  :endDate >  p.start_date )	")
				.append("	        AND op.ond_code IN 	( :overlappingOnds ) AND p.promotion_type_id = :promotionTypeId AND p.status = 'ACT'	");
		if (promotionTemplate.getPromotionId() != null) {
			queryContent.append("	AND p.promotion_id != :promotionId	");
		}

		List<String> overlappingOnds = new ArrayList<String>();
		Pair<String, String> originDest;
		for (OndPromotion ondPromotion : promotionTemplate.getOnds()) {
			overlappingOnds.add(ondPromotion.getOndCode());
		}

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("startDate", promotionTemplate.getStartDate());
		query.setParameter("endDate", promotionTemplate.getEndDate());
		query.setParameterList("overlappingOnds", overlappingOnds);
		query.setParameter("promotionTypeId", promotionTemplate.getPromotionType().getPromotionId());
		if (promotionTemplate.getPromotionId() != null) {
			query.setParameter("promotionId", promotionTemplate.getPromotionId());
		}
		query.addScalar("ond_code", StringType.INSTANCE);
		List<String> results = query.list();

		return results;
	}

	@Override
	public PromotionRequest getApprovedPromotions(PromotionType promotionType, Integer pnrPaxId, Integer pnrSegId) {

		PromotionRequest approvedRequest = null;
		StringBuilder hql = new StringBuilder();
		hql.append("select {pr.*} from t_promotion_request pr, t_promotion p ");
		hql.append("where pr.status='APR' ");
		hql.append("and p.status ='ACT' ");
		hql.append("AND p.promotion_id= pr.promotion_id ");
		hql.append("and p.promotion_type_id=:typeId ");
		hql.append("and pr.pnr_pax_id = :pnr_pax_id ");
		hql.append("and pr.pnr_seg_id =:pnr_seg_id ");

		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.setParameter("pnr_pax_id", pnrPaxId);
		query.setParameter("pnr_seg_id", pnrSegId);
		query.setParameter("typeId", promotionType.getPromotionId());
		query.addEntity("pr", PromotionRequest.class);
		List<Object> results = query.list();

		if (results != null && !results.isEmpty()) {
			approvedRequest = (PromotionRequest) results.get(0);
		}
		return approvedRequest;
	}

	@Override
	public List<PromotionRequest> loadPaxPromotionRequests(Collection<Integer> pnrPaxIds) {
		List<PromotionRequest> promotionRequests = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select {pr.*} from t_promotion_request pr ");
		hql.append(" where pr.pnr_pax_id in ( " + Util.constructINStringForCharactors(pnrPaxIds) + ") ");
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.addEntity("pr", PromotionRequest.class);
		promotionRequests = (List<PromotionRequest>) query.list();
		return promotionRequests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApplicablePromotionDetailsTO> pickApplicablePromotions(final PromoSelectionCriteria promoSelectionCriteria) {

		final String promoType = promoSelectionCriteria.getPromoType();
		Integer searchedPaxCount = promoSelectionCriteria.getAdultCount() + promoSelectionCriteria.getChildCount();
		String eligiblePaxCount = searchedPaxCount.toString();

		StringBuilder strSelectClause = new StringBuilder();
		// strSelectClause.append("SELECT pc.promo_criteria_id, pc.discriminator,pc.promo_code, pc.promo_name ");
		strSelectClause.append("SELECT pc.promo_criteria_id, pc.discriminator, pc.promo_name ");
		strSelectClause.append(", pc.discount_type, pc.discount_value, pc.discount_apply_to, pc.discount_apply_as ");
		strSelectClause.append(", pc.system_generated, pc.applicable_for_oneway, pc.applicable_for_return, pc.applicability ");
		strSelectClause.append(", pc.same_pax_restriction, pc.same_sector_restriction, pc.origin_pnr, msg.message_content ");

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.SYSGEN.equals(promoType)) {
			strSelectClause.append(", null AS flight_number, null AS logical_cabin_class_code, null AS booking_code ");
		}

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promoType)) {
			eligiblePaxCount = " (pcbqty.free_adult_count + pcbqty.free_child_count) ";
			strSelectClause.append(",pcbqty.free_adult_count, pcbqty.free_child_count, pcbqty.free_infant_count ");
		} else {
			strSelectClause.append(",null AS free_adult_count, null AS free_child_count, null AS free_infant_count ");
		}

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(promoType)) {
			strSelectClause.append(",pdb.applicable_bin ");
		} else {
			strSelectClause.append(",null AS applicable_bin ");
		}

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promoType)) {
			strSelectClause.append(",pfsa.anci_type ");
		} else {
			strSelectClause.append(",null AS anci_type ");
		}

		StringBuilder strFromClause = new StringBuilder();
		StringBuilder strWhereClause = new StringBuilder();

		strFromClause.append(" FROM ");
		strWhereClause.append(" WHERE pc.discriminator='" + promoSelectionCriteria.getPromoType() + "' ");
		strWhereClause.append(" AND pc.status='" + PromotionCriteriaConstants.PromotionStatus.ACTIVE + "' ");

		if (promoSelectionCriteria.getPromoCode() != null && !promoSelectionCriteria.getPromoCode().equals("")) {
			strSelectClause.append(", pcode.promo_code ");
			strFromClause.append(" t_promo_criteria_code pcode, ");
			strWhereClause.append(" AND pcode.promo_criteria_id = pc.promo_criteria_id ");
			strWhereClause.append(" AND (( pcode.fully_utilized = 'N' AND  pc.promo_code is NULL) OR (pcode.promo_code= pc.promo_code)) ");
		} else {
			strSelectClause.append(", null as promo_code ");
		}
		strFromClause.append(" t_promotion_criteria pc LEFT OUTER JOIN t_i18n_message msg ");
		strFromClause.append(" ON pc.i18n_message_key = msg.i18n_message_key(+) ");
		strFromClause.append(" AND msg.MESSAGE_LOCALE ='" + promoSelectionCriteria.getPreferredLanguage() + "' ");

		// strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_code pcode ON pcode.promo_criteria_id =
		// pc.promo_criteria_id ");

		strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_res_period prp ON pc.promo_criteria_id=prp.promo_criteria_id");
		strWhereClause.append(" AND ((prp.from_date IS NULL AND prp.to_date IS NULL) ");
		strWhereClause.append(" OR (to_date(" + CalendarUtil.formatForSQL_toDate(promoSelectionCriteria.getReservationDate()));
		strWhereClause.append(") BETWEEN prp.from_date AND prp.to_date)) ");

		strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_flight_period pfp ON pc.promo_criteria_id=pfp.promo_criteria_id");
		strWhereClause.append(" AND ((pfp.from_date IS NULL AND pfp.to_date IS NULL) ");
		strWhereClause.append(" OR ( ");
		strWhereClause.append(buildFlightDateCondition(promoSelectionCriteria.getOndFlightDates()));
		strWhereClause.append(" ))");

		if (!promoSelectionCriteria.getOndList().isEmpty()) {
			strSelectClause.append(", pco.ond_code ");
			strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_ond pco ON pc.promo_criteria_id=pco.promo_criteria_id");
		} else {
			strSelectClause.append(", null AS ond_code ");
		}

		if (PromotionCriteriaConstants.PromotionCriteriaTypes.SYSGEN.equals(promoType)) {
			strWhereClause
					.append(" AND pc.system_generated='Y' AND pcode.promo_code='" + promoSelectionCriteria.getPromoCode() + "' ");
		} else {
			if (promoSelectionCriteria.getPromoCode() != null && !promoSelectionCriteria.getPromoCode().equals("")) {
				strWhereClause
						.append(" AND upper(pcode.promo_code)='" + promoSelectionCriteria.getPromoCode().toUpperCase() + "' ");
			} else {
				strWhereClause.append(" AND pc.is_promocode_enabled='N' ");
			}

			if (!promoSelectionCriteria.getFlights().isEmpty()) {
				strSelectClause.append(", pcf.flight_number ");
				strFromClause
						.append(" LEFT OUTER JOIN t_promo_criteria_flight pcf ON pc.promo_criteria_id=pcf.promo_criteria_id");
			} else {
				strSelectClause.append(", null AS flight_number ");
			}

			strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_channel pcch ON pc.promo_criteria_id=pcch.promo_criteria_id");
			strWhereClause.append(" AND (pcch.sales_channel_code IS NULL OR (pcch.sales_channel_code= "
					+ SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY)
					+ " OR pcch.sales_channel_code = " + promoSelectionCriteria.getSalesChannel() + ")) ");

			strWhereClause.append(
					" AND (pc.num_by_sales_channel IS NULL OR pc.num_by_sales_channel >= (SELECT (NVL(SUM(chr.promo_pax_count),0) + "
							+ eligiblePaxCount + ") FROM t_reservation chr WHERE chr.origin_channel_code="
							+ promoSelectionCriteria.getSalesChannel() + " AND chr.promo_criteria_id=pc.promo_criteria_id)) ");

			if (promoSelectionCriteria.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_WEB) {
				String strCaseClause = "CASE WHEN pcch.sales_channel_code <> "
						+ SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY) + " THEN";
				strSelectClause.append(", pcpos.country_code ");
				strFromClause
						.append(" LEFT OUTER JOIN t_promo_criteria_pos pcpos ON pc.promo_criteria_id=pcpos.promo_criteria_id ");
				strWhereClause.append(" AND (pcpos.country_code IS NULL OR pcpos.country_code = " + strCaseClause + " '"
						+ promoSelectionCriteria.getPosCountry() + "' END)");

				if (promoSelectionCriteria.isRegisteredMember()) {
					if (promoSelectionCriteria.isLoyalityMember()) {
						strWhereClause.append("AND pc.applicable_for_loyalty_members = " + strCaseClause + " 'Y' ELSE 'N' END ");
					} else {
						strWhereClause.append("AND pc.applicable_for_reg_members = " + strCaseClause + " 'Y' ELSE 'N' END ");
					}
				} else {
					strWhereClause.append("AND pc.applicable_for_guests = " + strCaseClause + " 'Y' ELSE 'N' END ");
				}
			} else {
				strSelectClause.append(", null AS country_code ");
			}

			if (promoSelectionCriteria.getAgent() != null && !promoSelectionCriteria.getAgent().equals("")) {
				strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_agent pca ON pc.promo_criteria_id=pca.promo_criteria_id");
				strWhereClause
						.append(" AND (pca.agent_code IS NULL OR pca.agent_code='" + promoSelectionCriteria.getAgent() + "') ");
				strWhereClause
						.append(" AND (pc.num_by_agent IS NULL OR pc.num_by_agent >= (SELECT (NVL(SUM(agr.promo_pax_count),0) + "
								+ eligiblePaxCount + ") FROM t_reservation agr WHERE agr.origin_agent_code='"
								+ promoSelectionCriteria.getAgent() + "' AND agr.promo_criteria_id =pc.promo_criteria_id)) ");
			}

			if (!promoSelectionCriteria.getCabinClasses().isEmpty()) {
				strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_cos pcc ON pc.promo_criteria_id=pcc.promo_criteria_id");
				strWhereClause.append(" AND (pcc.cabin_class_code IS NULL OR pcc.cabin_class_code IN("
						+ Util.buildStringInClauseContent(promoSelectionCriteria.getCabinClasses()) + ")) ");
			}

			if (AppSysParamsUtil.isLogicalCabinClassEnabled() && promoSelectionCriteria.getLogicalCabinClasses() != null
					&& !promoSelectionCriteria.getLogicalCabinClasses().isEmpty()) {
				strSelectClause.append(", pcl.logical_cabin_class_code ");
				strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_lcc pcl ON pc.promo_criteria_id=pcl.promo_criteria_id");
			} else {
				strSelectClause.append(", null AS logical_cabin_class_code ");
			}

			if (promoSelectionCriteria.getBookingClasses() != null && !promoSelectionCriteria.getBookingClasses().isEmpty()) {
				strSelectClause.append(", pcb.booking_code ");
				strFromClause.append(" LEFT OUTER JOIN t_promo_criteria_bc pcb ON pc.promo_criteria_id=pcb.promo_criteria_id");
			} else {
				strSelectClause.append(", null AS booking_code ");
			}

			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promoType)) {
				strFromClause.append(
						" LEFT OUTER JOIN t_promo_code_buy_n_get_qty pcbqty ON pc.promo_criteria_id=pcbqty.promo_criteria_id");
				strWhereClause.append(" AND (");
				strWhereClause.append("( (pcbqty.qualifying_adult_count < 0)");
				strWhereClause.append(" OR (pcbqty.qualifying_adult_count=" + promoSelectionCriteria.getAdultCount() + "))");
				strWhereClause.append(" AND ");
				strWhereClause.append("( (pcbqty.qualifying_child_count < 0)");
				strWhereClause.append(" OR (pcbqty.qualifying_child_count=" + promoSelectionCriteria.getChildCount() + "))");
				strWhereClause.append("AND");
				strWhereClause.append("( (pcbqty.qualifying_infant_count < 0 )");
				strWhereClause.append(" OR (pcbqty.qualifying_infant_count=" + promoSelectionCriteria.getInfantCount() + "))");
				strWhereClause.append(")");

				if (!promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().isEmpty()) {
					strWhereClause.append(" AND (pc.limiting_load_factor IS NULL OR (pc.limiting_load_factor > ALL ( ");

					int size = promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().size();
					for (Entry<Integer, String> entry : promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().entrySet()) {
						strWhereClause.append(
								" SELECT ROUND(((fccsa.sold_seats + fccsa.on_hold_seats) * 100)/nullif((fccsa.allocated_seats+ fccsa.oversell_seats- fccsa.curtailed_seats),0)) ");
						strWhereClause.append(" FROM t_fcc_seg_alloc fccsa WHERE flt_seg_id = " + entry.getKey()
								+ " AND logical_cabin_class_code = '" + entry.getValue() + "' ");
						size--;
						if (size > 0) {
							strWhereClause.append(" UNION ");
						}

					}
					strWhereClause.append(")))");
				}
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT.equals(promoType)) {
				strFromClause.append(" LEFT OUTER JOIN t_promo_discount_bin pdb ON pc.promo_criteria_id=pdb.promo_criteria_id");
				if (promoSelectionCriteria.getBankIdNo() != null) {
					strWhereClause.append(" AND (pdb.applicable_bin IS NULL OR pdb.applicable_bin = "
							+ promoSelectionCriteria.getBankIdNo() + " ) ");
				} else {
					strWhereClause.append(" AND pdb.applicable_bin IS NULL ");
				}

			} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE.equals(promoType)) {
				strFromClause
						.append(" LEFT OUTER JOIN t_promo_free_service_anci pfsa ON pc.promo_criteria_id=pfsa.promo_criteria_id");
			}

			// check total promotion count if any
			strWhereClause.append(" AND (pc.total_limit IS NULL OR pc.total_limit >= (SELECT (NVL(SUM(r.promo_pax_count),0) + "
					+ eligiblePaxCount + ") FROM t_reservation r WHERE r.promo_criteria_id=pc.promo_criteria_id)) ");

			// check flight wise promotion count if any

			strWhereClause.append(" AND (pc.per_flight_limit IS NULL OR (pc.per_flight_limit >= ALL (");
			strWhereClause.append(" SELECT (NVL(SUM(promo_pax_count),0) + " + eligiblePaxCount + ") ");
			strWhereClause.append(" FROM(SELECT fs.flight_id,r.pnr,r.promo_criteria_id,r.promo_pax_count");
			strWhereClause.append(" FROM t_reservation r, t_pnr_segment ps, t_flight_segment fs");
			strWhereClause.append(" WHERE r.pnr =ps.pnr AND ps.flt_seg_id =fs.flt_seg_id");
			strWhereClause.append(" AND flight_id in(");
			if (!promoSelectionCriteria.getFlightIds().isEmpty()) {
				strWhereClause.append(Util.buildIntegerInClauseContent(promoSelectionCriteria.getFlightIds()) + ")");
			} else {
				strWhereClause.append("SELECT flight_id FROM t_flight_segment WHERE flt_seg_id IN (");
				strWhereClause.append(Util.buildIntegerInClauseContent(promoSelectionCriteria.getFlightSegIds()) + "))");
			}
			strWhereClause.append(
					" GROUP BY fs.flight_id,r.pnr,r.promo_criteria_id,r.promo_pax_count) WHERE promo_criteria_id = pc.promo_criteria_id GROUP BY flight_id");
			strWhereClause.append(" ) AND pc.per_flight_limit >= " + eligiblePaxCount + ")) ");

			strWhereClause.append(" ORDER BY pc.promo_criteria_id");
		}

		String query = strSelectClause.toString() + strFromClause.toString() + strWhereClause.toString();

		if (promoSelectionCriteria.getJourneyType().equals(JourneyType.ROUNDTRIP)) {
			query = buildFlightConditionForRoundTrip(query, promoSelectionCriteria.getOndFlightDates());
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(PromotionModuleServiceLocator.getDatasource());

		List<ApplicablePromotionDetailsTO> filteredPromotionDetailsTOs = (List<ApplicablePromotionDetailsTO>) jdbcTemplate
				.query(query, new ResultSetExtractor() {

					@Override
					public List<ApplicablePromotionDetailsTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
						LinkedHashMap<Long, ApplicablePromotionDetailsTO> filteredMap = new LinkedHashMap<Long, ApplicablePromotionDetailsTO>();
						while (rs.next()) {
							String anciType = rs.getString("anci_type");
							long promoCriteriaId = rs.getLong("promo_criteria_id");
							String promoCode = rs.getString("promo_code");
							String promoName = rs.getString("promo_name");
							String discountType = rs.getString("discount_type");
							String discountValue = rs.getString("discount_value");
							String discountApplyTo = rs.getString("discount_apply_to");
							String discountApplyAs = rs.getString("discount_apply_as");
							String systemGenerated = rs.getString("system_Generated");
							String ondCode = rs.getString("ond_code");
							String flightNumber = rs.getString("flight_number");
							String applicablForOneway = rs.getString("applicable_for_oneway");
							String applicablForReturn = rs.getString("applicable_for_return");
							String logicalcc = rs.getString("logical_cabin_class_code");
							String bcCode = rs.getString("booking_code");
							String applicableBIN = rs.getString("applicable_bin");
							String discountApplicability = rs.getString("applicability");
							String samePaxRestriction = rs.getString("same_pax_restriction");
							String sameSectorRestriction = rs.getString("same_sector_restriction");
							String originPnr = rs.getString("origin_pnr");
							Clob description = rs.getClob("message_content");

							ApplicablePromotionDetailsTO filteredPromotionDetailsTO = null;
							if (filteredMap.containsKey(promoCriteriaId)) {
								filteredPromotionDetailsTO = filteredMap.get(promoCriteriaId);
							} else {
								filteredPromotionDetailsTO = new ApplicablePromotionDetailsTO();
								filteredPromotionDetailsTO.setPromoCriteriaId(promoCriteriaId);
								filteredPromotionDetailsTO.setPromoName(promoName);

								if (PromotionCriteriaConstants.PromotionCriteriaTypes.SYSGEN.equals(promoType)) {
									filteredPromotionDetailsTO
											.setPromoType(PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT);
								} else {
									filteredPromotionDetailsTO.setPromoType(promoType);
								}

								filteredPromotionDetailsTO.setPromoCode(promoCode);
								filteredPromotionDetailsTO
										.setSystemGenerated(PromotionCriteriaConstants.Flag.YES.equals(systemGenerated));

								filteredPromotionDetailsTO.setDiscountType(
										(discountType == null) ? PromotionCriteriaConstants.DiscountTypes.PERCENTAGE
												: discountType);
								filteredPromotionDetailsTO
										.setDiscountValue((discountValue == null) ? 100 : Float.parseFloat(discountValue));
								filteredPromotionDetailsTO.setApplyTo(
										(discountApplyTo == null) ? PromotionCriteriaConstants.DiscountApplyTo.TOTAL.ordinal()
												: Integer.parseInt(discountApplyTo));
								filteredPromotionDetailsTO.setDiscountAs(
										(discountApplyAs == null) ? PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY
												: discountApplyAs);

								int freeAdultCount = rs.getInt("free_adult_count") < 0 ? promoSelectionCriteria.getAdultCount()
										: rs.getInt("free_adult_count");
								int freeChildCount = rs.getInt("free_child_count") < 0 ? promoSelectionCriteria.getChildCount()
										: rs.getInt("free_child_count");
								int freeInfantCount = rs.getInt("free_infant_count") < 0 ? promoSelectionCriteria.getInfantCount()
										: rs.getInt("free_infant_count");

								filteredPromotionDetailsTO.setApplicableAdultCount(freeAdultCount);
								filteredPromotionDetailsTO.setApplicableChildCount(freeChildCount);
								filteredPromotionDetailsTO.setApplicableInfantCount(freeInfantCount);

								filteredPromotionDetailsTO
										.setApplicableForOneWay(PromotionCriteriaConstants.Flag.YES.equals(applicablForOneway));
								filteredPromotionDetailsTO
										.setApplicableForReturn(PromotionCriteriaConstants.Flag.YES.equals(applicablForReturn));
								filteredPromotionDetailsTO.setDiscountApplicability(discountApplicability);

								filteredPromotionDetailsTO
										.setAllowSamePaxOnly(PromotionCriteriaConstants.Flag.YES.equals(samePaxRestriction));
								filteredPromotionDetailsTO.setAllowSameSectorOnly(
										PromotionCriteriaConstants.Flag.YES.equals(sameSectorRestriction));

								filteredPromotionDetailsTO.setOriginPnr(originPnr);

								if (description != null) {
									String clobString = StringUtil.clobToString(description);
									if (clobString != null && !"".equals(clobString) && !"null".equals(clobString)) {
										filteredPromotionDetailsTO.setDescription(StringUtil.getUnicode(clobString));
									}
								}

							}

							if (anciType != null) {
								filteredPromotionDetailsTO.getApplicableAncillaries().add(anciType);
							}

							if (ondCode != null) {
								filteredPromotionDetailsTO.getEligibleOnds().add(ondCode);
							}

							if (flightNumber != null) {
								filteredPromotionDetailsTO.getApplicableFlightNumbers().add(flightNumber);
							}

							if (logicalcc != null) {
								filteredPromotionDetailsTO.getApplicableLogicalCCs().add(logicalcc);
							}

							if (bcCode != null) {
								filteredPromotionDetailsTO.getApplicableBCs().add(bcCode);
							}

							if (applicableBIN != null && !"".equals(applicableBIN)) {
								filteredPromotionDetailsTO.getApplicableBINs().add(Integer.parseInt(applicableBIN));
							}

							filteredMap.put(promoCriteriaId, filteredPromotionDetailsTO);
						}
						return new ArrayList<ApplicablePromotionDetailsTO>(filteredMap.values());
					}
				});
		return filteredPromotionDetailsTOs;
	}

	private String buildFlightDateCondition(Map<Integer, Date> ondFlightDates) {
		StringBuilder flightCondition = new StringBuilder();
		if (ondFlightDates != null && !ondFlightDates.isEmpty()) {
			Date obFlightDate = ondFlightDates.get(OndSequence.OUT_BOUND);
			Date ibFlightDate = ondFlightDates.get(OndSequence.IN_BOUND);

			flightCondition.append("(pc.applicability IN ('OB', 'RT')  AND to_date("
					+ CalendarUtil.formatForSQL_toDate(obFlightDate) + ") BETWEEN pfp.from_date AND pfp.to_date)");

			if (ibFlightDate != null) {
				flightCondition.append(" OR ((pc.applicability = 'IB' AND to_date("
						+ CalendarUtil.formatForSQL_toDate(ibFlightDate) + ") BETWEEN pfp.from_date AND pfp.to_date))");
			}

		}
		return flightCondition.toString();
	}

	@Override
	public boolean isPromoCodeExists(String promoCode) {
		String queryStr = "SELECT DISTINCT count(*) AS count FROM t_promo_criteria_code WHERE promo_code=:promoCode";
		SQLQuery query = getSession().createSQLQuery(queryStr.toString());
		query.setParameter("promoCode", promoCode);
		query.addScalar("count", LongType.INSTANCE);
		int count = ((Long) query.uniqueResult()).intValue();
		return count > 0;
	}

	@Override
	public boolean checkPromotionConstraints(Long promoCriteriaId, List<Integer> flightIds, List<Integer> fltSegIds,
			String agentCode, Integer channelCode, int paxCount, int promoAppliedPaxCount) throws CommonsDataAccessException {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT count(pc.promo_criteria_id) AS count FROM t_promotion_criteria pc ");
		queryStr.append("WHERE pc.promo_criteria_id=:promoId AND pc.status=:status AND (pc.per_flight_limit IS NULL OR ");
		queryStr.append("(pc.per_flight_limit >= ALL ( ");
		queryStr.append(" SELECT (NVL(SUM(promo_pax_count),0) + " + promoAppliedPaxCount + ")");
		queryStr.append(" FROM(SELECT fs.flight_id,r.pnr,r.promo_criteria_id,r.promo_pax_count");
		queryStr.append(" FROM t_reservation r, t_pnr_segment ps, t_flight_segment fs");
		queryStr.append(" WHERE r.pnr =ps.pnr AND ps.flt_seg_id =fs.flt_seg_id");
		queryStr.append(" AND flight_id in(" + Util.buildIntegerInClauseContent(flightIds) + ")");
		queryStr.append(
				" GROUP BY fs.flight_id,r.pnr,r.promo_criteria_id,r.promo_pax_count) WHERE promo_criteria_id = pc.promo_criteria_id GROUP BY flight_id");
		queryStr.append(" ) AND pc.per_flight_limit >= " + promoAppliedPaxCount + ")) ");

		if (agentCode != null && !"".equals(agentCode)) {
			queryStr.append("AND (pc.num_by_agent IS NULL OR pc.num_by_agent >= (SELECT (NVL(SUM(agr.promo_pax_count),0) + "
					+ promoAppliedPaxCount + ") FROM t_reservation agr ");
			queryStr.append("WHERE agr.origin_agent_code=:agentCode AND agr.promo_criteria_id=pc.promo_criteria_id)) ");
		}

		queryStr.append(
				"AND (pc.num_by_sales_channel IS NULL OR pc.num_by_sales_channel >= (SELECT (NVL(SUM(chr.promo_pax_count),0) + "
						+ promoAppliedPaxCount + ") FROM t_reservation chr ");
		queryStr.append("WHERE chr.origin_channel_code=:salesChannel AND chr.promo_criteria_id=pc.promo_criteria_id)) ");
		queryStr.append("AND (pc.total_limit IS NULL OR pc.total_limit >= (SELECT (NVL(SUM(r.promo_pax_count),0) + "
				+ promoAppliedPaxCount + ") FROM t_reservation r ");
		queryStr.append("WHERE r.promo_criteria_id=pc.promo_criteria_id)) ");
		queryStr.append(
				"AND (pc.discriminator<>:promoType OR (pc.discriminator=:promoType AND (pc.limiting_load_factor IS NULL OR (pc.limiting_load_factor > ALL ( ");
		queryStr.append(
				"SELECT  DECODE(SUM(fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats),0,100,round( ( (SUM(fccsa.sold_seats + fccsa.on_hold_seats) - "
						+ paxCount + ") / SUM(fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats) ) * 100) )");

		queryStr.append("FROM t_fcc_seg_alloc fccsa WHERE flt_seg_id IN(" + Util.buildIntegerInClauseContent(fltSegIds)
				+ ") GROUP BY flt_seg_id)))))");

		SQLQuery query = getSession().createSQLQuery(queryStr.toString());
		query.setParameter("promoId", promoCriteriaId);
		query.setParameter("status", PromotionCriteriaConstants.PromotionStatus.ACTIVE);
		query.setParameter("salesChannel", channelCode);
		query.setParameter("promoType", PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET);
		if (agentCode != null && !"".equals(agentCode)) {
			query.setParameter("agentCode", agentCode);
		}

		query.addScalar("count", LongType.INSTANCE);
		Long count = (Long) query.uniqueResult();

		if (count > 0) {
			return true;
		} else {
			throw new CommonsDataAccessException("promotion.not.exists");
		}
	}

	@Override
	public List<String> getCustomerEmailsForPromoCodes(CustomerPromoCodeSelectionRequest customerDetails) {

		StringBuilder selectClause = new StringBuilder();
		StringBuilder fromClause = new StringBuilder();
		StringBuilder whereClause = new StringBuilder();

		selectClause.append("SELECT DISTINCT cus.email_id ");

		fromClause.append(
				"FROM t_reservation res,t_reservation_contact con, t_customer cus, t_pnr_segment pseg, t_flight flt, t_flight_segment fs, t_pnr_passenger pax, t_pnr_pax_fare pf, t_ond_fare ondf, t_fare_rule fr ");

		whereClause.append("WHERE res.pnr=con.pnr ");
		whereClause.append("AND res.pnr=pseg.pnr ");
		whereClause.append("AND pseg.flt_seg_id=fs.flt_seg_id ");
		whereClause.append("AND fs.flight_id=flt.flight_id ");

		whereClause.append("AND res.pnr=con.pnr ");
		whereClause.append("AND con.customer_id=cus.customer_id ");
		whereClause.append("AND res.pnr=pax.pnr ");
		whereClause.append("AND pax.pnr_pax_id=pf.pnr_pax_id ");
		whereClause.append("AND pf.fare_id = ondf.fare_id ");
		whereClause.append("AND ondf.fare_rule_id = fr.fare_rule_id ");

		if (!customerDetails.getLocations().isEmpty()) {
			if (!customerDetails.getLocations().contains("ALL")) {
				whereClause.append(" AND cus.country_code IN ( ");
				whereClause.append(Util.buildStringInClauseContent(customerDetails.getLocations()));
				whereClause.append(") ");
			}
		}

		if (!customerDetails.getAgeFrom().equals("")) {
			whereClause.append(
					" AND FLOOR(MONTHS_BETWEEN (SYSDATE, cus.Date_of_birth) / 12) > " + customerDetails.getAgeFrom() + " ");
		}

		if (!customerDetails.getAgeTo().equals("")) {
			whereClause
					.append(" AND FLOOR(MONTHS_BETWEEN (SYSDATE, cus.Date_of_birth) / 12) < " + customerDetails.getAgeTo() + " ");
		}

		if (!customerDetails.getFareTypes().isEmpty()) {
			whereClause.append(" AND fr.return_flag IN ( ");
			whereClause.append(Util.buildStringInClauseContent(customerDetails.getFareTypes()));
			whereClause.append(") ");
		}

		if (!customerDetails.getApplicableONDs().isEmpty()) {
			if (!customerDetails.getApplicableONDs().contains("ALL/ALL")) {
				whereClause.append(" AND (fs.segment_code IN ( ");
				whereClause.append(Util.buildStringInClauseContent(customerDetails.getApplicableONDs()));
				whereClause.append(") ");

				if (customerDetails.isAnyOrigination()) {
					whereClause.append(" OR  SUBSTR(fs.segment_code,5,3) IN ( ");
					whereClause.append(Util.buildStringInClauseContent(customerDetails.getAnyOriginationList()));
					whereClause.append(") ");
				}

				if (customerDetails.isAnyDestination()) {
					whereClause.append(" OR SUBSTR(fs.segment_code,1,3) IN ( ");
					whereClause.append(Util.buildStringInClauseContent(customerDetails.getAnyDestinationList()));
					whereClause.append(") ");
				}

				whereClause.append(") ");
			}
		}

		whereClause.append("AND res.pnr=pseg.pnr ");
		whereClause.append("AND res.booking_timestamp ");
		whereClause.append("BETWEEN to_date(" + CalendarUtil.formatForSQL_toDate(customerDetails.getReservationFromDate()));
		whereClause.append(" ) AND to_date(" + CalendarUtil.formatForSQL_toDate(customerDetails.getReservationToDate()) + ") ");
		whereClause.append(" AND flt.departure_date ");
		whereClause.append(" BETWEEN to_date(" + CalendarUtil.formatForSQL_toDate(customerDetails.getFlightFromDate()));
		whereClause.append(" )AND to_date(" + CalendarUtil.formatForSQL_toDate(customerDetails.getFlightToDate()) + ") ");

		String query = selectClause.toString() + fromClause.toString() + whereClause.toString();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(PromotionModuleServiceLocator.getDatasource());

		@SuppressWarnings("unchecked")
		List<String> emailIDs = (List<String>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> emails = new ArrayList<String>();
				if (rs != null) {
					while (rs.next()) {
						String email = BeanUtils.nullHandler(rs.getString("email_id"));
						emails.add(email);
					}
				}
				return emails;
			}
		});
		return emailIDs;
	}

	@Override
	public void saveCampaign(Campaign campaign) {
		hibernateSaveOrUpdate(campaign);
	}

	@Override
	public Campaign getCampaign(Integer campaignID) {
		String hql = "SELECT campaign FROM Campaign campaign WHERE campaign.campaignId='" + campaignID + "'";
		Query qry = getSession().createQuery(hql);
		@SuppressWarnings("rawtypes")
		List lst = qry.list();
		return (lst != null && !lst.isEmpty()) ? (Campaign) lst.iterator().next() : null;
	}

	private String buildFlightConditionForRoundTrip(String query, Map<Integer, Date> ondFlightDates) {
		StringBuilder strSqlQuery = new StringBuilder();
		strSqlQuery.append("SELECT a.* FROM (");
		strSqlQuery.append(query + ") a");
		if (ondFlightDates != null && !ondFlightDates.isEmpty()) {
			Date ibFlightDate = ondFlightDates.get(OndSequence.IN_BOUND);
			if (ibFlightDate != null) {
				strSqlQuery.append(
						" LEFT OUTER JOIN t_promo_criteria_flight_period pfp1 ON a.promo_criteria_id = pfp1.promo_criteria_id ");
				strSqlQuery.append("WHERE (pfp1.from_date IS NULL AND pfp1.to_date IS NULL) ");
				strSqlQuery.append("OR ((a.applicability = 'OB' OR a.applicability = 'IB') OR (a.applicability = 'RT' ");
				strSqlQuery.append("AND (to_date(" + CalendarUtil.formatForSQL_toDate(ibFlightDate)
						+ ") BETWEEN pfp1.from_date AND pfp1.to_date)))");
			}
		}
		return strSqlQuery.toString();
	}
}
