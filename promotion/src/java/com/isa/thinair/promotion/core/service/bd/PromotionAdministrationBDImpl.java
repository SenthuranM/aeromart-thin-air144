package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;

@Remote
public interface PromotionAdministrationBDImpl extends PromotionAdministrationBD {

}
