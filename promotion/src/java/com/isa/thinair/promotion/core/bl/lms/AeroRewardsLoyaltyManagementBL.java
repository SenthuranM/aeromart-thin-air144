package com.isa.thinair.promotion.core.bl.lms;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;

public class AeroRewardsLoyaltyManagementBL extends AbstractLoyalatyManagementBL {

	@Override
	public String createLoyaltyMember(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateMemberProfile(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce cancelRedeemedLoyaltyPoints(String pnr,String[] rewardIds, String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

}
