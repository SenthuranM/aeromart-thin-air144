package com.isa.thinair.promotion.core.util;

public interface ResponceCodes {

	String PROMOTION_SUBSCRIBE_REPEAT = "promotion.subsribe.repeat";
	String PROMOTION_SUBSCRIBE_MULTIPLE = "promotion.subscribe.multiple";
	String PROMOTION_UNSUBSCRIBE_PROCESSED_REPEAT = "promotion.unsubscribe.processed.repeat";
	String PROMOTION_ONDS_OVERLAPPED = "promotion.ond.overlap";
	String PROMOTION_TEMPLATE_NOT_ACTIVE = "promotion.template.not.active";
	String PROMOTION_EDIT_FAILED_ACT_REQS = "promotion.edit.failed.active.requests";
}