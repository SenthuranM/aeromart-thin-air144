package com.isa.thinair.promotion.core.bl.lms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.model.lms.FlownFile.PRODUCT_CODE;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateRes;
import com.isa.thinair.promotion.core.util.LMSHelperUtil;

/**
 * Business logics related to redeem loyalty points
 * 
 * @author rumesh
 * 
 */
public class BurnLoyaltyPointsBL {

	private static Log logger = LogFactory.getLog(BurnLoyaltyPointsBL.class);

	private String memberAccountId = null;
	private double memberAvailablePoints;
	private Collection<OndFareDTO> ondFareDTOs = null;
	private Map<String, List<LCCClientExternalChgDTO>> paxExternalCharges = null;
	private Map<Integer, Map<String, BigDecimal>> paxProductAmountDue = null;
	private List<LoyaltyProduct> loyaltyProductDTOs = null;
	private BigDecimal pointToCurrencyConversionRate = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal currencyToPointConversionRate = AccelAeroCalculator.getDefaultBigDecimalZero();
	private CurrencyExchangeRate currencyExchangeRate = null;
	private Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();

	private double availablePointsInitial;
	final static private double THREASHOLD_POINTS = 2;

	public BurnLoyaltyPointsBL() {
		super();
	}

	public BurnLoyaltyPointsBL(Map<Integer, Map<String, BigDecimal>> paxProductAmountDue, List<LoyaltyProduct> loyaltyProductDTOs,
			BigDecimal pointToCurrencyConversionRate, BigDecimal currencyToPointConversionRate,
			CurrencyExchangeRate currencyExchangeRate) {
		this.paxProductAmountDue = paxProductAmountDue;
		this.loyaltyProductDTOs = loyaltyProductDTOs;
		this.pointToCurrencyConversionRate = pointToCurrencyConversionRate;
		this.currencyToPointConversionRate = currencyToPointConversionRate;
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public BurnLoyaltyPointsBL(Collection<OndFareDTO> ondFareDTOs, Map<String, List<LCCClientExternalChgDTO>> paxExternalCharges,
			List<LoyaltyProduct> loyaltyProductDTOs, BigDecimal pointToCurrencyConversionRate,
			BigDecimal currencyToPointConversionRate, CurrencyExchangeRate currencyExchangeRate) {
		this.ondFareDTOs = ondFareDTOs;
		this.paxExternalCharges = paxExternalCharges;
		this.loyaltyProductDTOs = loyaltyProductDTOs;
		this.pointToCurrencyConversionRate = pointToCurrencyConversionRate;
		this.currencyToPointConversionRate = currencyToPointConversionRate;
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public RedeemCalculateRes calculatePaxLoyaltyRedeemableAmounts(BigDecimal requestedLoyaltyPointsInBaseCurrency,
			DiscountRQ discountRQ) throws ModuleException {
		RedeemCalculateRes redeemCalculateRes = new RedeemCalculateRes();
		Map<Integer, Map<String, Double>> paxWiseProductPoints = new LinkedHashMap<Integer, Map<String, Double>>();
		Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts = new LinkedHashMap<Integer, Map<String, BigDecimal>>();

		ReservationDiscountDTO resDiscountDTO = null;

		if (discountRQ != null) {
			resDiscountDTO = ReservationModuleUtils.getReservationBD().calculateDiscount(discountRQ, null);
		}

		if (paxProductAmountDue != null) {
			calculateRedeemAmountForModification(requestedLoyaltyPointsInBaseCurrency, paxWiseProductAmounts,
					paxWiseProductPoints);
		} else {
			calculateRedeemAmountForCreateReservation(requestedLoyaltyPointsInBaseCurrency, paxWiseProductAmounts,
					paxWiseProductPoints, resDiscountDTO);
		}

		redeemCalculateRes.setPaxWiseProductPoints(paxWiseProductPoints);
		redeemCalculateRes.setPaxWiseProductValue(paxWiseProductAmounts);

		return redeemCalculateRes;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public void setAvailablePoints(double availablePoints) {
		this.availablePointsInitial = availablePoints;
		this.memberAvailablePoints = availablePoints;
	}

	private void calculateRedeemAmountForModification(BigDecimal requestedLoyaltyPointsInBaseCurrency,
			Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts, Map<Integer, Map<String, Double>> paxWiseProductPoints)
			throws ModuleException {

		if (loyaltyProductDTOs != null) {
			// sort loyalty products by redeem rank
			Collections.sort(loyaltyProductDTOs);

			if (AccelAeroCalculator.isGreaterThan(requestedLoyaltyPointsInBaseCurrency,
					AccelAeroCalculator.getDefaultBigDecimalZero())) {
				for (LoyaltyProduct loyaltyProductDTO : loyaltyProductDTOs) {

					for (Entry<Integer, Map<String, BigDecimal>> paxEntry : paxProductAmountDue.entrySet()) {
						Integer paxSeq = paxEntry.getKey();
						Map<String, BigDecimal> productDueAmounts = paxEntry.getValue();

						if (!paxWiseProductPoints.containsKey(paxSeq)) {
							paxWiseProductPoints.put(paxSeq, new LinkedHashMap<String, Double>());
						}

						if (!paxWiseProductAmounts.containsKey(paxSeq)) {
							paxWiseProductAmounts.put(paxSeq, new LinkedHashMap<String, BigDecimal>());
						}

						Map<String, BigDecimal> productAmounts = paxWiseProductAmounts.get(paxSeq);
						Map<String, Double> productPoints = paxWiseProductPoints.get(paxSeq);

						if (productDueAmounts == null) {
							continue;
						}

						BigDecimal dueAmount = productDueAmounts.get(loyaltyProductDTO.getProductId());

						if (dueAmount != null
								&& AccelAeroCalculator.isGreaterThan(dueAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							BigDecimal productRedeemableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

							if (loyaltyProductDTO.getThresholdValue() != null
									&& availablePointsInitial < loyaltyProductDTO.getThresholdValue()) {

								if (logger.isDebugEnabled()) {
									StringBuilder sb = new StringBuilder("Loyalty Member Account: ");
									sb.append(memberAccountId);
									sb.append(" Threshold: ").append(loyaltyProductDTO.getThresholdValue());
									sb.append(" Product Code: ").append(loyaltyProductDTO.getProductId());
									sb.append(" Available points: ").append(memberAvailablePoints);
									sb.append(" . Available point less than threshold value.");
									logger.debug(sb.toString());
								}

							} else if (AccelAeroCalculator.isLessThan(requestedLoyaltyPointsInBaseCurrency, dueAmount)) {
								productRedeemableAmount = requestedLoyaltyPointsInBaseCurrency;
							} else {
								productRedeemableAmount = dueAmount;
							}

							if (AccelAeroCalculator.isGreaterThan(productRedeemableAmount,
									AccelAeroCalculator.getDefaultBigDecimalZero())) {
								productAmounts.put(loyaltyProductDTO.getProductId(), productRedeemableAmount);

								Double correspondingPointsValue = getPointsAmount(productRedeemableAmount, memberAvailablePoints);

								if (correspondingPointsValue > memberAvailablePoints) {
									throw new ModuleException("promotion.loyalty.redeem.exceed.available.points");
								}

								productPoints.put(loyaltyProductDTO.getProductId(), correspondingPointsValue);

								memberAvailablePoints = memberAvailablePoints - correspondingPointsValue;
								requestedLoyaltyPointsInBaseCurrency = AccelAeroCalculator
										.subtract(requestedLoyaltyPointsInBaseCurrency, productRedeemableAmount);
							}

						}
					}
				}
			}

		}
	}

	private void calculateRedeemAmountForCreateReservation(BigDecimal requestedLoyaltyPointsInBaseCurrency,
			Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts, Map<Integer, Map<String, Double>> paxWiseProductPoints,
			ReservationDiscountDTO resDiscountDTO) throws ModuleException {
		if (loyaltyProductDTOs != null) {
			// sort loyalty products by redeem rank
			Collections.sort(loyaltyProductDTOs);
			boolean isFreeServicePromo = false;

			if (AccelAeroCalculator.isGreaterThan(requestedLoyaltyPointsInBaseCurrency,
					AccelAeroCalculator.getDefaultBigDecimalZero())) {

				for (LoyaltyProduct loyaltyProductDTO : loyaltyProductDTOs) {
					for (Entry<String, List<LCCClientExternalChgDTO>> paxEntry : paxExternalCharges.entrySet()) {
						String paxKey = paxEntry.getKey();
						Integer paxSeq = LMSHelperUtil.getPaxSequence(paxKey);
						String paxType = LMSHelperUtil.getPaxType(paxKey);
						boolean isParent = LMSHelperUtil.isParent(paxKey);
						PaxDiscountDetailTO paxDiscountDetailTO = null;
						if (resDiscountDTO != null && resDiscountDTO.isDiscountExist()
								&& PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY.equals(resDiscountDTO.getDiscountAs())) {
							paxDiscountDetailTO = resDiscountDTO.getPaxDiscountDetail(paxSeq);

							if (ChargeCodes.FREE_SERVICE_PROMO.equals(resDiscountDTO.getDiscountCode())) {
								isFreeServicePromo = true;
							}
						}

						if (!paxWiseProductAmounts.containsKey(paxSeq)) {
							paxWiseProductAmounts.put(paxSeq, new LinkedHashMap<String, BigDecimal>());
						}

						if (!paxWiseProductPoints.containsKey(paxSeq)) {
							paxWiseProductPoints.put(paxSeq, new LinkedHashMap<String, Double>());
						}

						Map<String, BigDecimal> productAmounts = paxWiseProductAmounts.get(paxSeq);
						Map<String, Double> productPoints = paxWiseProductPoints.get(paxSeq);

						BigDecimal paxProductAmount = getPaxProductTotalAmount(loyaltyProductDTO, paxType, isParent, paxKey,
								paxDiscountDetailTO, isFreeServicePromo);

						if (AccelAeroCalculator.isGreaterThan(paxProductAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							BigDecimal productRedeemableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							if (loyaltyProductDTO.getThresholdValue() != null
									&& availablePointsInitial < loyaltyProductDTO.getThresholdValue()) {

								if (logger.isDebugEnabled()) {
									StringBuilder sb = new StringBuilder("Loyalty Member Account: ");
									sb.append(memberAccountId);
									sb.append(" Threshold: ").append(loyaltyProductDTO.getThresholdValue());
									sb.append(" Product Code: ").append(loyaltyProductDTO.getProductId());
									sb.append(" Available points: ").append(memberAvailablePoints);
									sb.append(" . Available point less than threshold value.");
									logger.debug(sb.toString());
								}
							} else if (AccelAeroCalculator.isLessThan(requestedLoyaltyPointsInBaseCurrency, paxProductAmount)) {
								productRedeemableAmount = requestedLoyaltyPointsInBaseCurrency;
							} else {
								productRedeemableAmount = paxProductAmount;
							}

							if (AccelAeroCalculator.isGreaterThan(productRedeemableAmount,
									AccelAeroCalculator.getDefaultBigDecimalZero())) {
								Double correspondingPointsValue = getPointsAmount(productRedeemableAmount, memberAvailablePoints);
								BigDecimal pointEquivalentBaseAmount = AccelAeroCalculator
										.scaleDefaultRoundingDown(getPointsValueByBaseCurrency(correspondingPointsValue));
								if(productRedeemableAmount.compareTo(pointEquivalentBaseAmount)> 0){
									productRedeemableAmount = pointEquivalentBaseAmount;
								}
								productAmounts.put(loyaltyProductDTO.getProductId(), productRedeemableAmount);
								if (correspondingPointsValue > memberAvailablePoints) {
									throw new ModuleException("promotion.loyalty.redeem.exceed.available.points");
								}

								productPoints.put(loyaltyProductDTO.getProductId(), correspondingPointsValue);

								memberAvailablePoints = memberAvailablePoints - correspondingPointsValue;
								requestedLoyaltyPointsInBaseCurrency = AccelAeroCalculator
										.subtract(requestedLoyaltyPointsInBaseCurrency, productRedeemableAmount);
							}
						}
					}
				}
			}
		}
	}

	public BigDecimal getPointsValueByBaseCurrency(double pointsValue) {
		BigDecimal loyaltyCurrencyValue = AccelAeroCalculator.multiply(new BigDecimal(pointsValue),
				pointToCurrencyConversionRate);

		BigDecimal baseCurrencyAmount = AccelAeroCalculator.multiply(loyaltyCurrencyValue,
				currencyExchangeRate.getExrateCurToBaseNumber());
		return AccelAeroCalculator.scaleDefaultRoundingDown(baseCurrencyAmount);
	}

	private BigDecimal getPaxProductTotalAmount(LoyaltyProduct loyaltyProductDTO, String paxType, boolean isParent, String paxKey,
			PaxDiscountDetailTO paxDiscountDetailTO, boolean isFreeServicePromo) {
		BigDecimal paxProductTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if ((loyaltyProductDTO.getChargeCodes() == null || loyaltyProductDTO.getChargeCodes().isEmpty())
				&& !loyaltyProductDTO.getProductId().equals(PRODUCT_CODE.BUNDLE_FARE.toString())) {
			double farePortion = 0;
			if (ondFareDTOs != null) {
				for (OndFareDTO ondFareDTO : ondFareDTOs) {

					double[] surcharges = ChargeQuoteUtils.calculateTotalChargeForChargeGroup(ondFareDTO.getAllCharges(),
							ChargeGroup.SUR, true);

					if (PaxTypeTO.INFANT.equals(paxType)) {
						double[] infCharges = ChargeQuoteUtils.calculateTotalChargeForChargeGroup(ondFareDTO.getAllCharges(),
								ChargeGroup.INF, true);
						farePortion += ondFareDTO.getInfantFare() + surcharges[1] + infCharges[1];
					} else if (PaxTypeTO.PARENT.equals(paxType) || PaxTypeTO.ADULT.equals(paxType)) {
						farePortion += ondFareDTO.getAdultFare() + surcharges[0];
					} else if (PaxTypeTO.CHILD.equals(paxType)) {
						farePortion += ondFareDTO.getChildFare() + surcharges[2];
					}
					if (!isFreeServicePromo) {
						farePortion = discountedEffectiveTotalCharges(ondFareDTO, farePortion, paxDiscountDetailTO, true);
					}

				}
			}
			paxProductTotalAmount = AccelAeroCalculator.parseBigDecimal(farePortion);
		} else if (!loyaltyProductDTO.isExternalCharge()
				&& loyaltyProductDTO.getProductId().equals(PRODUCT_CODE.BUNDLE_FARE.toString())
				&& loyaltyProductDTO.getChargeCodes().size() > 0) {
			double bundleFarePortion = 0;
			if (ondFareDTOs != null) {
				for (OndFareDTO ondFareDTO : ondFareDTOs) {

					double[] allSurcharges = ChargeQuoteUtils.calculateTotalChargeForChargeGroup(ondFareDTO.getAllCharges(),
							ChargeGroup.SUR, false);
					double[] allWithoutBundleChgs = ChargeQuoteUtils
							.calculateTotalChargeForChargeGroup(ondFareDTO.getAllCharges(), ChargeGroup.SUR, true);

					if (PaxTypeTO.INFANT.equals(paxType)) {
						bundleFarePortion += (allSurcharges[1] - allWithoutBundleChgs[1]);
					} else if (PaxTypeTO.PARENT.equals(paxType) || PaxTypeTO.ADULT.equals(paxType)) {
						bundleFarePortion += (allSurcharges[0] - allWithoutBundleChgs[0]);
					} else if (PaxTypeTO.CHILD.equals(paxType)) {
						bundleFarePortion += (allSurcharges[2] - allWithoutBundleChgs[2]);
					}
					if (!isFreeServicePromo) {
						bundleFarePortion = discountedEffectiveTotalCharges(ondFareDTO, bundleFarePortion, paxDiscountDetailTO,
								false);
					}

				}
			}

			paxProductTotalAmount = AccelAeroCalculator.parseBigDecimal(bundleFarePortion);
		} else {
			if (loyaltyProductDTO.getChargeCodes() != null) {
				for (String productChargeCode : loyaltyProductDTO.getChargeCodes()) {
					if (paxExternalCharges != null) {
						List<LCCClientExternalChgDTO> externalCharges = paxExternalCharges.get(paxKey);
						Map<String, List<LCCClientExternalChgDTO>> externalChgByChargeCodeMap = getChargesListByChargeCode(
								externalCharges);
						if (externalChgByChargeCodeMap != null && !externalChgByChargeCodeMap.isEmpty()) {

							List<LCCClientExternalChgDTO> filteredExtCharges = externalChgByChargeCodeMap.get(productChargeCode);
							if (filteredExtCharges != null && !filteredExtCharges.isEmpty()) {
								for (LCCClientExternalChgDTO externalChgDTO : filteredExtCharges) {
									paxProductTotalAmount = AccelAeroCalculator.add(paxProductTotalAmount,
											externalChgDTO.getAmount());
								}
								if (paxProductTotalAmount.doubleValue() > 0 && paxDiscountDetailTO != null
										&& isFreeServicePromo) {
									BigDecimal discount = paxDiscountDetailTO
											.getTotalDiscountByExternalChargeCode(productChargeCode);
									if (paxProductTotalAmount.doubleValue() >= discount.doubleValue()) {
										paxProductTotalAmount = paxProductTotalAmount.subtract(discount);
									}

								}
							}
						}
					}
				}
			}
		}
		return paxProductTotalAmount;
	}

	private double getPointsAmount(BigDecimal productAmount, double memberAvailablePoints) {
		BigDecimal baseToCurrencyRate = currencyExchangeRate.getMultiplyingExchangeRate();
		BigDecimal converedAmount = AccelAeroCalculator.multiply(productAmount, baseToCurrencyRate);
		converedAmount = AccelAeroCalculator.scaleValueDefault(converedAmount);
		BigDecimal pointsAmount = AccelAeroCalculator.multiply(converedAmount, currencyToPointConversionRate);

		pointsAmount = AccelAeroCalculator.scaleValueDefault(pointsAmount);

		if ((memberAvailablePoints < pointsAmount.doubleValue())
				&& ((memberAvailablePoints - pointsAmount.doubleValue()) <= THREASHOLD_POINTS)) {
			pointsAmount = new BigDecimal(memberAvailablePoints);
		}

		return pointsAmount.doubleValue();
	}

	/**
	 * functionality to calculate the actual charges when discounts are applied for a given charge.
	 */
	private double discountedEffectiveTotalCharges(OndFareDTO ondFareDTO, double farePortion,
			PaxDiscountDetailTO paxDiscountDetailTO, boolean includeINFChg) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxDiscountDetailTO != null && farePortion > 0) {
			Collection<String> chargeGroups = new ArrayList<String>();
			if (includeINFChg) {
				chargeGroups.add(ChargeGroup.INF);
			}
			chargeGroups.add(ChargeGroup.SUR);
			chargeGroups.add(ChargeGroup.FAR);
			discount = paxDiscountDetailTO.getSegmentDiscountTotalByChargeGroup(ondFareDTO.getFlightSegmentIds(), chargeGroups,
					true);
			if (farePortion >= discount.doubleValue()) {
				farePortion = farePortion - discount.doubleValue();
			}

		}
		return farePortion;
	}

	private Map<String, List<LCCClientExternalChgDTO>> getChargesListByChargeCode(List<LCCClientExternalChgDTO> externalCharges) {
		Map<String, List<LCCClientExternalChgDTO>> externalChgByChargeCode = new HashMap<String, List<LCCClientExternalChgDTO>>();
		if (externalCharges != null && !externalCharges.isEmpty()) {
			for (LCCClientExternalChgDTO externalChgDTO : externalCharges) {
				String chargeCode = externalChargesMap.get(externalChgDTO.getExternalCharges().toString());
				if (externalChgByChargeCode.get(chargeCode) == null) {
					externalChgByChargeCode.put(chargeCode, new ArrayList<LCCClientExternalChgDTO>());
				}
				externalChgByChargeCode.get(chargeCode).add(externalChgDTO);
			}
		}

		return externalChgByChargeCode;
	}
}
