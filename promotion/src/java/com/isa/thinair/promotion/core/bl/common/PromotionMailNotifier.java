package com.isa.thinair.promotion.core.bl.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationSegmentNotification;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

public abstract class PromotionMailNotifier extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {
		List<NotificationReceiverTo> receivers = (List<NotificationReceiverTo>) getParameter(CommandParamNames.PROMOTION_NOTIFICATION_DESTINATIONS);
		String eventType = (String) getParameter(CommandParamNames.EVENT_TYPE);

		Locale locale;
		String emailAddr;
		List<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();
		ReservationSegmentNotification notification;
		List<ReservationSegmentNotification> notifications = new ArrayList<ReservationSegmentNotification>();
		Map<String, NotificationReceiverTo> filteredNotifications = new HashMap<String, NotificationReceiverTo>();

		if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {

			for (NotificationReceiverTo receiver : receivers) {
				if (filteredNotifications.containsKey(receiver.getPnr())) {
					if (filteredNotifications.get(receiver.getPnr()).isBetterMatch(receiver)) {
						filteredNotifications.get(receiver.getPnr()).setDiscardedNotification(true);
						filteredNotifications.put(receiver.getPnr(), receiver);
					} else {
						receiver.setDiscardedNotification(true);
					}
				} else {
					filteredNotifications.put(receiver.getPnr(), receiver);
				}
			}

		}

		for (NotificationReceiverTo receiver : receivers) {

			if (!receiver.isDiscardedNotification()) {

				emailAddr = receiver.getReceiverAddr();

				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(receiver.getReceiverName());
				userMessaging.setLastName("");
				userMessaging.setToAddres(emailAddr);

				if (receiver.getLanguage() != null) {
					locale = new Locale(receiver.getLanguage());
				} else {
					locale = Locale.getDefault();
				}

				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				messages.add(userMessaging);

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(getTopicParams(receiver));
				topic.setTopicName(getTopicName());
				topic.setLocale(locale);
				topic.setAttachMessageBody(true);

				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messageProfileList.add(messageProfile);

			}

			// Notification To Save
			if (eventType.equals(CommandParamNames.EVENT_PROMO_NOTIFY)) {

				if (!receiver.isDiscardedNotification()) {
					for (Integer reservationSegId : receiver.getReservationSegIds().keySet()) {
						notification = new ReservationSegmentNotification();
						notification.setNoficationRefId(receiver.getPromotionId());
						notification.setNotificationType(ReservationSegmentNotification.NOTIFICATION_TYPE_PROMOTION);
						notification.setReservationSegId(reservationSegId);
						notifications.add(notification);
					}

					PromotionModuleServiceLocator.getReservationSegmentDAO().saveReservationSegmentNotifications(notifications);
				}
			}
		}

		// Send the list of messages
		if (messageProfileList != null && messageProfileList.size() > 0) {
			PromotionModuleServiceLocator.getMessagingServiceBD().sendMessages(messageProfileList);
		}

		return new DefaultServiceResponse(true);
	}

	protected abstract HashMap<String, Object> getTopicParams(NotificationReceiverTo notificationReceiver);

	protected abstract String getTopicName();
}
