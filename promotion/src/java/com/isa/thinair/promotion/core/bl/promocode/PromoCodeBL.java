package com.isa.thinair.promotion.core.bl.promocode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.promotion.api.model.DiscountDetails;
import com.isa.thinair.promotion.api.model.PromoCode;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.SysGenPromotionCriteria;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.CampaignTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

public class PromoCodeBL {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(PromoCodeBL.class);

	public static ApplicablePromotionDetailsTO getApplicablePromotionDetails(PromoSelectionCriteria promoSelectionCriteria)
			throws ModuleException {

		List<String> promoListByPriority = AppSysParamsUtil.getPromoTypesByPriority();
		PromotionManagementDao promoManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
		Set<Integer> allFlightSegIds = PromotionsUtils.getFlightSegIdsWithInterceptingSegIds(promoSelectionCriteria
				.getFlightSegIds());
		promoSelectionCriteria.setFlightSegIds(allFlightSegIds);

		for (String promoType : promoListByPriority) {

			if (PromotionCriteriaConstants.PromotionCriteriaTypes.SYSGEN.equals(promoType)
					&& (promoSelectionCriteria.getPromoCode() == null || "".equals(promoSelectionCriteria.getPromoCode().trim()))) {
				continue;
			}

			promoSelectionCriteria.setPromoType(promoType);

			try {
				List<ApplicablePromotionDetailsTO> filteredPromoTOs = promoManagementDao
						.pickApplicablePromotions(promoSelectionCriteria);

				if (filteredPromoTOs != null && !filteredPromoTOs.isEmpty()) {
					for (ApplicablePromotionDetailsTO filteredPromoTO : filteredPromoTOs) {
						log.debug("Iterate selected promotion from database for the given selection criteria is "
								+ filteredPromoTO.getPromoCode());
						boolean ondMatched = false;
						List<String> matchedOndCombination = null;
						if (filteredPromoTO.getEligibleOnds().isEmpty()) {

							if (filteredPromoTO.isApplicableForOneWay() && !filteredPromoTO.isApplicableForReturn()
									&& promoSelectionCriteria.getJourneyType() != JourneyType.SINGLE_SECTOR) {
								ondMatched = false;
							} else if (filteredPromoTO.isApplicableForReturn() && !filteredPromoTO.isApplicableForOneWay()
									&& promoSelectionCriteria.getJourneyType() != JourneyType.ROUNDTRIP) {
								ondMatched = false;
							} else {
								ondMatched = true;
								matchedOndCombination = promoSelectionCriteria.getOndList();
							}

						} else if (PromotionCriteriaConstants.PromotionCriteriaTypes.SYSGEN.equals(promoType)
								&& !filteredPromoTO.getEligibleOnds().isEmpty() && !filteredPromoTO.isAllowSameSectorOnly()) {
							ondMatched = true;
							matchedOndCombination = promoSelectionCriteria.getOndList();
						} else {
							List<List<String>> allPossibleOndList = getAllPossibleOnds(filteredPromoTO.getEligibleOnds(),
									filteredPromoTO.isApplicableForOneWay(), filteredPromoTO.isApplicableForReturn());
							for (List<String> ondList : allPossibleOndList) {
								if ((promoSelectionCriteria.getOndList().size() == ondList.size())
										&& promoSelectionCriteria.getOndList().containsAll(ondList)
										&& ondSequenceMatched(promoSelectionCriteria.getOndList(), ondList)) {
									matchedOndCombination = ondList;
									ondMatched = true;
									break;
								}
							}
						}

						boolean flightNumbersMatched = false;
						if (filteredPromoTO.getApplicableFlightNumbers().isEmpty()) {
							flightNumbersMatched = true;
						} else if (filteredPromoTO.getApplicableFlightNumbers().containsAll(promoSelectionCriteria.getFlights())) {
							flightNumbersMatched = true;
						}

						boolean lccMatched = false;
						if (filteredPromoTO.getApplicableLogicalCCs().isEmpty()) {
							lccMatched = true;
						} else if (filteredPromoTO.getApplicableLogicalCCs().containsAll(
								promoSelectionCriteria.getLogicalCabinClasses())) {
							lccMatched = true;
						}

						boolean bcMatched = false;
						if (filteredPromoTO.getApplicableBCs().isEmpty()) {
							bcMatched = true;
						} else if (filteredPromoTO.getApplicableBCs().containsAll(promoSelectionCriteria.getBookingClasses())) {
							bcMatched = true;
						}

						if (ondMatched && flightNumbersMatched && lccMatched && bcMatched) {
							getDiscountApplicableSegments(matchedOndCombination, filteredPromoTO);
							if (promoType.equals(PromotionCriteriaConstants.PromotionCriteriaTypes.FREESERVICE)) {
								removeDuplicateAncillaries(filteredPromoTO);
							}
							log.debug("The promotion applicable for the given selection criteria is "
									+ filteredPromoTO.getPromoCode());
							return filteredPromoTO;
						}
					}
				}

			} catch (Exception e) {
				throw new ModuleException(e, e.getMessage());
			}
		}

		return null;
	}

	private static void removeDuplicateAncillaries(ApplicablePromotionDetailsTO filteredPromoTO) {
		List<String> lstAncis = filteredPromoTO.getApplicableAncillaries();
		if (lstAncis != null && !lstAncis.isEmpty()) {
			Set<String> setAncis = new HashSet<String>(lstAncis);
			filteredPromoTO.setApplicableAncillaries(new ArrayList<String>(setAncis));
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sendPromoCodeNotificationEmail(PromotionCriteria promoDetails, ReservationContactInfo resContact) {
		if (resContact != null && resContact.getEmail() != null && !"".equals(resContact.getEmail())
				&& promoDetails instanceof SysGenPromotionCriteria) {

			if (promoDetails instanceof SysGenPromotionCriteria) {
				DiscountDetails discountDetails = ((SysGenPromotionCriteria) promoDetails).getDiscount();

				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setFirstName(resContact.getFirstName());
				userMessaging.setLastName(resContact.getLastName());
				userMessaging.setToAddres(resContact.getEmail());

				List messages = new ArrayList();
				messages.add(userMessaging);
				
				StringBuilder promoCodeVal = new StringBuilder("");
				if(promoDetails.getPromoCodes()!=null){
					
					for(PromoCode promoCodeOb : promoDetails.getPromoCodes()){
						promoCodeVal.append(promoCodeOb.getPromoCode()+ " ");
					}
				}

				HashMap emailDataMap = new HashMap();
				emailDataMap.put("title", resContact.getTitle());
				emailDataMap.put("lastName", resContact.getLastName());
				emailDataMap.put("currency", AppSysParamsUtil.getBaseCurrency());
				emailDataMap.put("amount", AccelAeroCalculator.formatAsDecimal(discountDetails.getDiscountValue()));
				emailDataMap.put("promoCode", promoCodeVal);
//				emailDataMap.put("promoCode", promoDetails.getPromoCode());
				emailDataMap.put("carrier", AppSysParamsUtil.getDefaultCarrierName());
				emailDataMap
						.put("ibe_url_root", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));

				// Topic
				Topic topic = new Topic();
				topic.setTopicParams(emailDataMap);
				topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.PROMO_CODE_NOTIFY_EMAIL);
				topic.setLocale(null);
				topic.setAttachMessageBody(false);

				// User Profile
				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				// Send the message
				PromotionModuleServiceLocator.getMessagingServiceBD().sendMessage(messageProfile);
			}

		}
	}

	private static boolean ondSequenceMatched(List<String> searchedOnds, List<String> configuredOnds) {
		int ondCount = searchedOnds.size();
		for (int i = 0; i < ondCount; i++) {
			if (!searchedOnds.get(i).equals(configuredOnds.get(i))) {
				return false;
			}
		}

		return true;
	}

	private static List<List<String>> getAllPossibleOnds(Set<String> filteredOnds, boolean isOneWayValid, boolean isReturnValid) {
		List<List<String>> allPossibleOnds = new ArrayList<List<String>>();

		for (String ond : filteredOnds) {
			List<String> ondList = new ArrayList<String>();
			allPossibleOnds.add(ondList);
			if (isOneWayValid && isReturnValid) {
				List<String> dupOndList = new ArrayList<String>();
				dupOndList.add(ond);
				allPossibleOnds.add(dupOndList);
				ondList.add(ond);
				ondList.add(StringUtil.getReverseOnDCode(ond));
			} else if (isOneWayValid) {
				ondList.add(ond);
			} else if (isReturnValid) {
				ondList.add(ond);
				ondList.add(StringUtil.getReverseOnDCode(ond));
			}
		}

		return allPossibleOnds;
	}

	private static void getDiscountApplicableSegments(List<String> matchingOndCombination,
			ApplicablePromotionDetailsTO filteredPromoTO) {
		if (matchingOndCombination != null && !matchingOndCombination.isEmpty()) {
			String discountApplicability = filteredPromoTO.getDiscountApplicability();
			String outboundOnd = matchingOndCombination.get(OndSequence.OUT_BOUND);
			Set<String> discountApplicableOnds = new HashSet<String>();

			if (PromotionCriteriaConstants.ApplicableRoute.OUT_BOUND.equals(discountApplicability)) {
				discountApplicableOnds.add(outboundOnd);
			} else if (PromotionCriteriaConstants.ApplicableRoute.IN_BOUND.equals(discountApplicability)) {
				discountApplicableOnds.add(StringUtil.getReverseOnDCode(outboundOnd));
			} else if (PromotionCriteriaConstants.ApplicableRoute.RETURN.equals(discountApplicability)) {
				discountApplicableOnds.add(outboundOnd);
				discountApplicableOnds.add(StringUtil.getReverseOnDCode(outboundOnd));
			}

			filteredPromoTO.setApplicableOnds(discountApplicableOnds);

		}
	}
	
	public static List<String> generatePromotionCodes(int numOfCodes) throws ModuleException {
		List<String> generatedCodes = new ArrayList<String>();
		try {
			PromotionManagementDao promoManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			for (int i=0 ; i<numOfCodes ; i ++) {
				boolean promoCodeExists;
				String promoCode;
				do {
					promoCode = StringUtil.generateRandomString(PromotionCriteriaConstants.SYS_GEN_PROMO_CODE_LENGTH);
					promoCodeExists = promoManagementDao.isPromoCodeExists(promoCode);
				} while (promoCodeExists);
	
				generatedCodes.add(promoCode);
			}
		} catch (Exception e) {
			throw new ModuleException(e, e.getMessage());
		}
		return generatedCodes;
	}
}