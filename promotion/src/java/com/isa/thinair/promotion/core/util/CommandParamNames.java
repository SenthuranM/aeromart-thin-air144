package com.isa.thinair.promotion.core.util;

public interface CommandParamNames {

	String PROMOTION_TYPE = "PROMOTION_TYPE";
	String PROMOTION_NOTIFICATION_DESTINATIONS = "PROMOTION_NOTIFICATION_DESTS";
	String PROMOTION_REQUEST_ID = "PROMOTION_REQUEST_ID";
	String FLIGHT_ID = "FLIGHT_ID";
	String PROMOTION_ID = "PROMOTION_ID";

	String PNR = "PNR";
	String RESERVATION_SEGMENT_ID = "RESERVATION_SEGMENT_ID";
	String FLIGHT_SEGMENT_ID = "FLIGHT_SEGMENT_ID";
	String VERSION_ID = "VERSION_ID";
	String USER_PRINCPAL = "USER_PRINCPAL";
	String FLIGHT_SEGMENT_CODE = "FLIGHT_SEGMENT_CODE";
	String FLIGHT_SEGMENT_CODES = "FLIGHT_SEGMENT_CODES";
	String TRANSFER_SEGMENTS = "TRANSFER_SEGMENTS";
	String PAYMENT_TRANSACTION = "PAYMENT_TRANSACTION";

	String OPERATION_CODE = "OPERATION_CODE";
	String OPERATION_APPROVAL_VIEW = "OPERATION_APPROVAL_VIEW";

	String RESP_APPROVAL_VIEW = "RESP_APPROVAL_VIEW";
	String RESP_PROMO_REQUESTS_SUMMARY = "RESP_PROMO_REQUESTS_SUMMARY";
	String PROMOTION_REQUEST = "PROMOTION_REQUEST";
	String PROMOTION_REQUEST_CUSTOM = "PROMOTION_REQUEST";
	String PROMOTION_REQUESTS = "PROMOTION_REQUESTS";
	String RESERVATION = "RESERVATION";
	String SEAT_INVENTORY = "SEAT_INVENTORY";
	String FLIGHT_SEGMENT = "FLIGHT_SEGMENT";
	String FLIGHT_SEAT_FACTOR = "FLIGHT_SEAT_FACTOR";
	String PROMOTION_SEAT_FACTOR = "PROMOTION_SEAT_FACTOR";
	String APPLICABLE_CHARGES_REFUNDS = "APPLICABLE_CHARGES_REFUNDS";
	String TOTAL_CHARGES_REFUNDS = "TOTAL_CHARGES_REFUNDS";
	String PROMOTION_REQUEST_STATUS = "PROMOTION_REQUEST_STATUS";

	String PROMO_PROCESS_CATEGORY = "PROMO_PROCESS_CATEGORY";
	String PROCESS_CHARGES = "PROCESS_CHARGES";
	String PROCESS_REFUNDS = "PROCESS_REFUNDS";

	String EVENT_TYPE = "EVENT_TYPE";
	String EVENT_PROMO_NOTIFY = "PROMO_NOTIFY";
	String EVENT_PROMO_APPROVE = "PROMO_APPROVE";  //only for flexi date
	String EVENT_PROMO_EXPIRE = "PROMO_EXPIRE";
	String EVENT_PROMO_SUBSCRIBE = "PROMO_SUBSCRIBE";
	String EVENT_PROMO_APPROVED_REJECTED = "PROMO_APPROVED_REJECTED";  //only for next seat

	String REQUEST_TYPE = "REQUEST_TYPE";
	String REQUEST_CALCULATION_ONLY = "REQUEST_CALCULATION";
	String REQUEST_EXECUTE = "REQUEST_EXECUTE";

	String NEXT_SEAT_FREE_REQUESTED_SEATS_COUNT = "NEXT_SEAT_FREE_REQUESTED_SEATS_COUNT";
	String NEXT_SEAT_FREE_CONFIRMED_SEATS_COUNT = "NEXT_SEAT_FREE_CONFIRMED_SEATS_COUNT";
	String NEXT_SEAT_FREE_REJECTED_SEATS_COUNT = "NEXT_SEAT_FREE_REJECTED_SEATS_COUNT";
	String FREE_SEATS_BEFORE_PROMOTION_APPROVAL = "FREE_SEATS_BEFORE_PROMOTION_APPROVAL";

	String NUMBER_OF_MOVED_PASSENGERS = "NUMBER_OF_MOVED_PASSENGERS";

	String EMAIL_CONTENT_FOR_APPROVED_REJECTED_PROMOS = "EMAIL_CONTENT_FOR_APPROVED_REJECTED_PROMOS";

	String APPLICABLE_PROMOTION_DETAILS = "applicablePromotionDetails";
	
	String VOUCHER_DETAILS = "VOUCHER_DETAILS";
	String VOUCHER_SEARCH_REQUEST = "VOUCHER_SEARCH_REQUEST";
	String VOUCHER_PAGE_START = "VOUCHER_PAGE_START";
	String VOUCHER_PAGE_END = "VOUCHER_PAGE_END";
	String VOUCHER_SEARCH_RESULT = "VOUCHER_SEARCH_RESULT";

	String VOUCHER_PAYMENT_METHOD = "VOUCHER_PAYMENT_METHOD";
	String VOUCHER_PAYMENT = "VOUCHER_PAYMENT";
	String VOUCHER_TYPE = "VOUCHER_TYPE";

	String VOUCHER_CANCEL = "VOUCHER_CANCEL";
	String VOUCHER_CANCEL_USER = "VOUCHER_CANCEL_USER";

	String PAYMENT_AUTHID_MAP = "PAYMENT_AUTHID_MAP";
	String PAYMENT_BROKER_REF_MAP = "PAYMENT_BROKER_REF_MAP";
	String CREDENTIALS_DTO = "CREDENTIALS_DTO";
	
	String CAMPAIGN_DTO = "CAMPAIGN_DTO";
	String VOUCHER_LIST_DETAILS = "VOUCHER_LIST_DETAILS";
	String TOTAL_BASE = "TOTAL_BASE";


}