package com.isa.thinair.promotion.core.bl.common;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.platform.api.ServiceResponce;

public abstract class PromotionAdminInquiryHandler extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {
		return handleRequest();
	}

	protected abstract ServiceResponce handleRequest() throws ModuleException;

}
