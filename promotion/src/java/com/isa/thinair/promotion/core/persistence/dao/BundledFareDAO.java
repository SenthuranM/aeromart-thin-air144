package com.isa.thinair.promotion.core.persistence.dao;

import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareSearchCriteriaTO;

/**
 * 
 * @author rumesh
 * 
 */
public interface BundledFareDAO {

	public Page<BundledFare> searchBundledFareConfiguration(BundledFareSearchCriteriaTO searchCriteriaTO, Integer startPosition,
			Integer recSize);
	
	public BundledFare getBundledFare(Integer bundledFareId) throws CommonsDataAccessException;
	
	public BundledFare getApplicableBundleFare(Integer bundledFarePeriodId) throws CommonsDataAccessException;

	public Integer saveBundledFareConfiguration(BundledFare bundledFare);

	public void updateBundledFareConfiguration(BundledFare bundledFare);

	public boolean isBundledFareUsedByReservation(Set<Integer> bundledFarePeriodIds);

	public List<BundledFare> getApplicableBundledFares(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria)
			throws CommonsDataAccessException;

	public List<BundledFareFreeService> getOfferedServices(String pnrSegId) throws CommonsDataAccessException;
	
	public boolean isChargeCodeLinkedToBundledFare(String chargeCode) throws CommonsDataAccessException;
	
	public boolean isBundledFareExistsWithDescription(String description);
}
