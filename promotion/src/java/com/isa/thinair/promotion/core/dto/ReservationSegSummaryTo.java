package com.isa.thinair.promotion.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class ReservationSegSummaryTo implements Serializable {

	private int resSegId;
	private int flightId;
	private String segmentCode;
	private int flightSegId;
	private Date depatureDate;
	private Date departureDateLocal;
	private boolean inbound;
	private String bcAllocationType;

	// charges per segment ---- Map < charges category @ PromotionsInternalConstants , amount >
	private Map<String, BigDecimal> charges;
	private int promotionId;
	private int promotionRequestId;
	private Map<String, Object> promoRequestParams;
	private Map<String, BigDecimal> promotionChargesAndRefunds;
	private TransferFlightTo currentFlightTo;
	private TransferFlightTo transferFlightTo;

	public ReservationSegSummaryTo() {
		this.charges = new HashMap<String, BigDecimal>();
		this.promoRequestParams = new HashMap<String, Object>();
		this.promotionChargesAndRefunds = new HashMap<String, BigDecimal>();
		inbound = false;
	}

	public ReservationSegSummaryTo(String segmentCode, Date depatureDate) {
		this();
		setSegmentCode(segmentCode);
		setDepatureDate(depatureDate);
	}

	public int getResSegId() {
		return resSegId;
	}

	public void setResSegId(int resSegId) {
		this.resSegId = resSegId;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public Date getDepartureDateLocal() {
		return departureDateLocal;
	}

	public void setDepartureDateLocal(Date departureDateLocal) {
		this.departureDateLocal = departureDateLocal;
	}

	public Map<String, BigDecimal> getCharges() {
		return charges;
	}

	public void putCharge(String category, BigDecimal amount) {
		this.charges.put(category, amount);
	}
	
	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public int getPromotionRequestId() {
		return promotionRequestId;
	}

	public void setPromotionRequestId(int promotionRequestId) {
		this.promotionRequestId = promotionRequestId;
	}

	public Map<String, Object> getPromoRequestParams() {
		return promoRequestParams;
	}

	public void putPromoRequestParam(String param, Object val) {
		this.promoRequestParams.put(param, val);
	}

	public Map<String, BigDecimal> getPromotionChargesAndRefunds() {
		return promotionChargesAndRefunds;
	}

	public void putPromotionChargesAndRefunds(String chargeCode, BigDecimal promotionChargeOrRefund) {
		this.promotionChargesAndRefunds.put(chargeCode, promotionChargeOrRefund);
	}

	public TransferFlightTo getCurrentFlightTo() {
		return currentFlightTo;
	}

	public void setCurrentFlightTo(TransferFlightTo currentFlightTo) {
		this.currentFlightTo = currentFlightTo;
	}

	public TransferFlightTo getTransferFlightTo() {
		return transferFlightTo;
	}

	public void setTransferFlightTo(TransferFlightTo transferFlightTo) {
		this.transferFlightTo = transferFlightTo;
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	@Deprecated
	public BigDecimal getTotalCharges() {
		BigDecimal totalCharges = BigDecimal.ZERO;

		if (charges.containsKey(PromotionsInternalConstants.CHARGES_ALL)) {
			totalCharges = charges.get(PromotionsInternalConstants.CHARGES_ALL);
		} else {
			if (charges.containsKey(PromotionsInternalConstants.CHARGES_SURCHARGES)) {
				totalCharges = totalCharges.add(charges.get(PromotionsInternalConstants.CHARGES_SURCHARGES));
			}
			if (charges.containsKey(PromotionsInternalConstants.CHARGES_TAXES)) {
				totalCharges = totalCharges.add(charges.get(PromotionsInternalConstants.CHARGES_TAXES));
			}
		}

		return totalCharges;
	}

	public BigDecimal getFare() {
		BigDecimal fare = BigDecimal.ZERO;

		if (charges.containsKey(PromotionsInternalConstants.CHARGES_FARES)) {
			fare = charges.get(PromotionsInternalConstants.CHARGES_FARES);
		}

		return fare;
	}

	public boolean isSubscribedForPromo() {
		return !promoRequestParams.isEmpty();
	}

	public boolean isSelectedToSendForPromoApproval() {
		return transferFlightTo != null;
	}

	public BigDecimal getEffectiveChargeOrRefund() {
		BigDecimal effectiveChargeRefund = BigDecimal.ZERO;
		for (BigDecimal val : promotionChargesAndRefunds.values()) {
			effectiveChargeRefund = effectiveChargeRefund.add(val);
		}
		return effectiveChargeRefund;
	}

	public String getBcAllocationType() {
		return bcAllocationType;
	}

	public void setBcAllocationType(String bcAllocationType) {
		this.bcAllocationType = bcAllocationType;
	}

}
