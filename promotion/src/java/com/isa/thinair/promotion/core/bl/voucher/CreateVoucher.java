package com.isa.thinair.promotion.core.bl.voucher;

import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.bl.voucher.VoucherBO;
/**
 * @author chethiya
 * 
 * @isa.module.command name="createVoucher"
 * 
 */

public class CreateVoucher extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() throws ModuleException {
		VoucherDTO voucherDTO = (VoucherDTO) getParameter(CommandParamNames.VOUCHER_DETAILS);
		VoucherBO.saveVoucher(voucherDTO);
		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

}
