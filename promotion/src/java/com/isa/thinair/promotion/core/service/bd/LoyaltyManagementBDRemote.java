package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;

/**
 * Remote interface for LoyaltyManagementBD
 * 
 * @author rumesh
 * 
 */
@Remote
public interface LoyaltyManagementBDRemote extends LoyaltyManagementBD {

}
