package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.promotion.api.service.AnciOfferBD;

/**
 * Local interface of AnciOfferBD
 * 
 * @author thihara
 * 
 */
@Local
public interface AnciOfferBDLocal extends AnciOfferBD {

}
