package com.isa.thinair.promotion.core.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.model.DatePeriod;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.to.PromoCriteriaSearchTO;

/**
 * Interface defining PromotionCriteria class hierachy's data access operations
 * 
 * @author thihara
 * 
 */
public interface PromotionCriteriaDAO {

	/**
	 * Finds the {@link PromotionCriteria} mapped to the given ID.
	 * 
	 * @param promotionCriteriaID
	 *            ID of the promotion criteria that need to be retrieved.
	 * @return The retrieved subclass instance of {@link PromotionCriteria} or null if the ID doesn't exist.
	 */
	public PromotionCriteria findPromotionCriteria(Long promotionCriteriaID);

	/**
	 * Saves the promotion criteria given.
	 * 
	 * @param promotionCriteria
	 *            Subclass of {@link PromotionCriteria} to be saved.
	 * @return The assigned ID of the promotion criteria.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public Long savePromotionCriteria(PromotionCriteria promotionCriteria);

	/**
	 * Updates the promotion criteria.
	 * 
	 * @param promotionCriteria
	 *            PromotionCriteria with the changed data populated.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public long updatePromotionCriteria(PromotionCriteria promotionCriteria);

	/**
	 * Deletes the specified promotion criteria. This operation only requires the ID and the version to be set.
	 * 
	 * @param promotionCriteria
	 *            Subclass instance of {@link PromotionCriteria} with the necessary data populated.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	public void deletePromotionCriteria(PromotionCriteria promotionCriteria);

	/**
	 * Searches across promotion criteria.
	 * 
	 * @param promoCriteriaSearch
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 * @throws ModuleException
	 *             If the underlying operation fails.
	 */
	public Page<PromotionCriteria> searchPromotionCriteria(PromoCriteriaSearchTO promoCriteriaSearch, Integer start, Integer size);

	/**
	 * Update consumed credits & status of given system generated promotion
	 * 
	 * @param promotionId
	 * @param consumedCredits
	 * @param status
	 * @throws CommonsDataAccessException
	 */
	public void updateSystemGeneratedPromotion(Long promotionId, BigDecimal consumedCredits, String status)
			throws CommonsDataAccessException;

	/**
	 * Check that promo code is already exist
	 * 
	 * @param promoCode
	 * @return
	 */
	public boolean isPromoCodeExist(PromotionCriteria promotionCriteria);

	/**
	 * 
	 * @param promotionCriteria
	 * @return
	 */
	public long updatePromotionCriteriaStatus(PromotionCriteria promotionCriteria);

	/**
	 * Get Promotion criteria obj by originated PNR
	 * 
	 * @param originPnr
	 * @return
	 */
	public PromotionCriteria getPromotionCriteria(String originPnr);

	/**
	 * Activate system generated promotion, after payment made for onhold reservations
	 * 
	 * @param originPnr
	 */
	public void activateSystemGeneratedPromotion(String originPnr);

	/**
	 * Add the language desc key
	 * 
	 * @param criteriaId
	 * @param messageKey
	 */

	public void updatePromotionCodeDescKey(long criteriaId, String messageKey);

	/**
	 * Update consumed credits & status of given system generated promotion when OHD reservation cancels
	 * 
	 * @param promotionId
	 * @param consumedCredits
	 * @param status
	 * @throws CommonsDataAccessException
	 */

	public void updateSystemGeneratedPromotionForOHD(Long promotionId, BigDecimal consumedCredits, String status)
			throws CommonsDataAccessException;

	/**
	 * update promo code utilization after used
	 * 
	 * @param promoCode
	 */
	public void updatepromoCodeUtilization(String promoCode);

	/**
	 * get promo codes by criteria ID
	 * 
	 * @param promoCriteriaId
	 */
	public List<String> getPromoCodesById(String promoCriteriaId);

	/**
	 * remove date periods by criteria ID
	 * 
	 * @param promoCriteriaId
	 * @param clazz
	 */
	public <T extends DatePeriod> void removeDatePeriodsById(long promoCriteriaId, Class<T> clazz);

}
