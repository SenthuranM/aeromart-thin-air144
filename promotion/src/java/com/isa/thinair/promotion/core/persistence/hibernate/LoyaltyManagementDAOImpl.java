package com.isa.thinair.promotion.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.aircustomer.api.model.SystemLMSAuthToken;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.model.FlownFileErrorLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.to.lms.FlownRecordsDTO;
import com.isa.thinair.promotion.core.persistence.dao.LoyaltyManagementDAO;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

/**
 * 
 * @author rumesh
 * 
 * @isa.module.dao-impl dao-name="LoyaltyManagementDAO"
 * 
 */
public class LoyaltyManagementDAOImpl extends PlatformHibernateDaoSupport implements LoyaltyManagementDAO {

	Log log = LogFactory.getLog(LoyaltyManagementDAOImpl.class);

	@Override
	@SuppressWarnings("unchecked")
	public List<LoyaltyProduct> getLoyaltyProducts() throws CommonsDataAccessException {
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("FROM LoyaltyProduct AS loyaltyProduct order by redeemRank");
		Query searchQuery = getSession().createQuery(queryBuilder.toString());
		return (List<LoyaltyProduct>) searchQuery.list();
	}

	@Override
	public void saveOrUpdateFlowFileErrorLog(FlownFileErrorLog flownFileErrorLog) throws CommonsDataAccessException {
		getSession().saveOrUpdate(flownFileErrorLog);
	}

	@Override
	public void saveOrUpdateLoyaltyFileLog(LoyaltyFileLog loyaltyFileLog) throws CommonsDataAccessException {
		getSession().saveOrUpdate(loyaltyFileLog);
	}

	@Override
	public void deleteLoyaltyFileLog(String fileName) {
		List fileLogList = null;

		fileLogList = find(
				"from LoyaltyFileLog where fileName = " + fileName + " order by createdDate desc", LoyaltyFileLog.class);

		if (fileLogList != null && !fileLogList.isEmpty()) {
			LoyaltyFileLog fileLog = (LoyaltyFileLog) fileLogList.get(0);
			if (log.isDebugEnabled()) {
				log.debug("Deleting Loyalty File log : " + fileLog.getFileName());
			}
			delete(fileLog);
		}
	}

	@Override
	public Date getLastProductFilePublishTime() {
		String queryString = null;
		queryString = "SELECT MAX(log.CREATED_DATE) AS max_time FROM t_loyalty_file_log log WHERE log.loyalty_file_log_type='"
				+ LoyaltyFileLog.FILE_TYPE.PRODUCT_FILE.toString() + "'";

		JdbcTemplate templete = new JdbcTemplate(getDatasource());

		return (Date) templete.query(queryString, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Date lastPublishDate = null;
				if (rs != null) {
					while (rs.next()) {
						lastPublishDate = toDate(rs.getTimestamp("max_time"));
					}
				}
				return lastPublishDate;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<LoyaltyFileLog> getUploadPendingFiles(FILE_TYPE fileType, LMS_SERVICE_PROVIDER serviceProvider) {
		List<String> uploadEligibleStatuses = new ArrayList<String>();
		uploadEligibleStatuses.add(LoyaltyFileLog.FILE_STATUS.CREATED.toString());
		uploadEligibleStatuses.add(LoyaltyFileLog.FILE_STATUS.UPLOAD_FAILED.toString());

		Query query = getSession()
				.createQuery("FROM LoyaltyFileLog WHERE status IN (:eligibleStatuses) AND fileType = :fileType AND serviceProvider =:serviceProvider")
				.setParameterList("eligibleStatuses", uploadEligibleStatuses)
				.setParameter("fileType", fileType.toString())
				.setParameter("serviceProvider", serviceProvider.toString());

		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FlownFileErrorLog> getFailedFlownFileRecords() {
		Query query = getSession().createQuery("FROM FlownFileErrorLog WHERE status = :recordStatus").setParameter(
				"recordStatus", FlownFileErrorLog.STATUS.PENDING.toString());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FlownRecordsDTO> getFlownRecords(Date rptPeriodStartDate, Date rptPeriodEndDate) {

		String startDtStr = getDateStr(rptPeriodStartDate) + " 00:00:00";
		String endDtStr = getDateStr(rptPeriodEndDate) + " 23:59:59";

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT memberAccountId, emailConfirmed, title, lastName, firstName, paxType, dob, nationality, operaringFlight, ");
		sb.append("flightTime, departureAirport, arrivalAirport, cabinClass, bookingClass, pnr, ticketIssuanceDate, ");
		sb.append("ticketNumber, couponNumber, agentInitials, contactNumber, contactEmail, countryOfResidence, ");
		sb.append("salesChannel, passportNumber, REPLACE(WM_CONCAT(productName ||'=' || amount ),',','|') AS product ");
		sb.append("FROM (SELECT memberAccountId, emailConfirmed, title, lastName, firstName, paxType, dob, nationality, operaringFlight, ");
		sb.append("flightTime, departureAirport, arrivalAirport, cabinClass, bookingClass, pnr, ticketIssuanceDate, ticketNumber, ");
		sb.append("couponNumber, agentInitials, contactNumber, contactEmail, countryOfResidence, salesChannel, ");
		sb.append("passportNumber, productName, SUM(amount) AMOUNT ");
		sb.append("FROM (SELECT LM.EMAIL_ID AS memberAccountId, lm.EMAIL_CONFIRMED as emailConfirmed, DECODE(PP.PAX_TYPE_CODE, 'IN', 'INF', PP.TITLE) AS title, ");
		sb.append("PP.LAST_NAME AS lastName, PP.FIRST_NAME AS firstName, DECODE(PP.PAX_TYPE_CODE, 'AD', 'ADT', 'CH', 'CHD', 'IN', 'INF') AS paxType, ");
		sb.append("PP.DATE_OF_BIRTH AS dob, (SELECT N.ICAO_CODE FROM T_NATIONALITY n  ");
		sb.append("WHERE N.NATIONALITY_CODE = PP.NATIONALITY_CODE) AS nationality, F.FLIGHT_NUMBER  AS operaringFlight, ");
		sb.append("FS.EST_TIME_DEPARTURE_LOCAL AS flightTime, F.ORIGIN AS departureAirport, F.DESTINATION AS arrivalAirport, ");
		sb.append("(SELECT BC.CABIN_CLASS_CODE FROM T_BOOKING_CLASS bc ");
		sb.append("WHERE BC.BOOKING_CODE  in (select booking_code from t_pnr_pax_fare_segment where pnr_seg_id=ps.pnr_seg_id and ppf_id in(select ppf_id from t_pnr_pax_fare where pnr_pax_id=PP.PNR_PAX_ID))) cabinClass, ");
		sb.append("(select booking_code from t_pnr_pax_fare_segment where pnr_seg_id=ps.pnr_seg_id and ppf_id in(select ppf_id from t_pnr_pax_fare where pnr_pax_id=PP.PNR_PAX_ID)and rownum=1) AS bookingClass, ");
		sb.append("ps.pnr, R.BOOKING_TIMESTAMP AS ticketIssuanceDate, ");
		sb.append("(SELECT E.E_TICKET_NUMBER FROM T_PAX_E_TICKET e,T_PNR_PAX_FARE_SEG_E_TICKET ES WHERE  E.pax_e_ticket_id=ES.pax_e_ticket_id AND ");
		sb.append("ES.ppfs_id IN(SELECT PPFS_ID FROM T_PNR_PAX_FARE_SEGMENT WHERE PNR_SEG_ID IN (SELECT PNR_SEG_ID FROM ");
		sb.append("T_PNR_PAX_FARE_SEGMENT WHERE PPFS_ID=PPFS.ppfs_id )) AND e.pnr_pax_id = PP.PNR_PAX_ID AND rownum=1) AS ticketNumber, ");
		sb.append("(select COUPON_NUMBER  from T_PNR_PAX_FARE_SEG_E_TICKET where ppfs_id=ppfs.ppfs_id and rownum=1)  AS couponNumber, ");
		sb.append("DECODE(R.OWNER_AGENT_CODE, NULL, 'WEB', R.OWNER_AGENT_CODE) AS agentInitials, RC.C_MOBILE_NO AS contactNumber, ");
		sb.append("RC.C_EMAIL AS contactEmail, (SELECT C.ICAO_CODE FROM T_COUNTRY c WHERE C.COUNTRY_CODE = RC.C_COUNTRY_CODE) AS countryOfResidence, ");
		sb.append("R.ORIGIN_CHANNEL_CODE AS salesChannel, PPA.PASSPORT_NUMBER AS passportNumber, ");
		sb.append("(CASE WHEN o.loyalty_product_code IS NOT NULL THEN o.loyalty_product_code WHEN ppoc.charge_group_code='FAR' THEN 'FLIGHT' WHEN ppoc.charge_group_code in('SUR','INF') AND o.loyalty_product_code IS NULL THEN 'FLIGHT'  END)productName, ");
		sb.append("PPSP.AMOUNT ");
		sb.append("FROM T_PNR_PASSENGER pp, T_PNR_PAX_ADDITIONAL_INFO ppa, T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, ");
		sb.append("t_flight f, T_PNR_PAX_FARE ppf, T_PNR_PAX_OND_CHARGES ppoc, T_PNR_PAX_SEG_CHARGES ppsc, ");
		sb.append("T_PNR_PAX_SEG_PAYMENTS ppsp, T_PNR_PAX_OND_PAYMENTS ppop, T_PAX_TRANSACTION pt, ");
		sb.append("T_PNR_PAX_FARE_SEGMENT ppfs, T_LMS_MEMBER lm, T_RESERVATION_CONTACT rc, T_RESERVATION r , ");
		sb.append("T_CHARGE_RATE cr, t_loyalty_product_charge o ");
		sb.append("WHERE pp.pnr = ps.pnr AND rc.pnr = pp.pnr AND r.pnr = pp.pnr AND r.pnr = ps.pnr AND r.pnr = rc.pnr ");
		sb.append("AND PS.STATUS  = 'CNF' and pp.pax_type_code<>'IN' AND PPF.PPF_ID = PPOC.PPF_ID AND PPOC.PFT_ID = PPOP.PFT_ID ");
		sb.append("AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND FS.FLIGHT_ID = F.FLIGHT_ID AND PPOP.PAYMENT_TXN_ID = PT.TXN_ID ");
		sb.append("AND decode(pp.pax_type_code,'IN',adult_id,PP.PNR_PAX_ID) = ppa.PNR_PAX_ID ");
		sb.append("AND UPPER(PPA.FFID) = UPPER(LM.EMAIL_ID (+)) AND PPOC.PFT_ID = PPSC.PFT_ID AND PPSC.PPFS_ID = PPFS.PPFS_ID ");
		sb.append("AND PPFS.PPF_ID = PPF.PPF_ID AND PPFS.PNR_SEG_ID = PS.PNR_SEG_ID AND PPSP.PPOP_ID = PPOP.PPOP_ID ");
		sb.append("and ppsp.pfst_id=ppsc.pfst_id and ppop.pnr_pax_id=pt.pnr_pax_id AND decode(pp.pax_type_code,'IN',adult_id,PP.PNR_PAX_ID) = pt.PNR_PAX_ID ");
		sb.append("AND R.BOOKING_CATEGORY_CODE = 'STD' AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID (+) and o.charge_code(+)=cr.charge_code ");
		sb.append("AND PPOC.CHARGE_GROUP_CODE IN ('FAR', 'SUR', 'INF') AND PT.NOMINAL_CODE  IN (36,30,28,15,16,17,18,19,6) ");
		sb.append("and FS.EST_TIME_DEPARTURE_LOCAL between to_date('").append(startDtStr).append("', 'DD-MM-YYYY HH24:MI:SS') ");
		sb.append("and to_date('").append(endDtStr).append("', 'DD-MM-YYYY HH24:MI:SS')) ");
		sb.append("group by memberAccountId,emailConfirmed,title,lastName,firstName,paxType,dob,nationality, ");
		sb.append("operaringFlight,flightTime, departureAirport,arrivalAirport,cabinClass,bookingClass,pnr,ticketIssuanceDate, ");
		sb.append("ticketNumber,couponNumber,agentInitials,contactNumber,contactEmail, ");
		sb.append("countryOfResidence,salesChannel,passportNumber,productName ");
		sb.append(")GROUP BY memberAccountId,emailConfirmed,title,lastName,firstName,paxType,dob,nationality, ");
		sb.append("operaringFlight,flightTime, departureAirport,arrivalAirport,cabinClass,bookingClass,pnr,ticketIssuanceDate, ");
		sb.append("ticketNumber,couponNumber,agentInitials,contactNumber,contactEmail, ");
		sb.append("countryOfResidence,salesChannel,passportNumber order by pnr ");

		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDatasource());

		List<FlownRecordsDTO> flowRecordsDTOs = (List<FlownRecordsDTO>) jdbcTemplate.query(sb.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<FlownRecordsDTO> flowRecordsDTOs = null;
						boolean isEmailConfirmed = false;
						if (rs != null) {
							flowRecordsDTOs = new ArrayList<FlownRecordsDTO>();
							while (rs.next()) {
								FlownRecordsDTO flownRecordsDTO = new FlownRecordsDTO();
								isEmailConfirmed = (rs.getString("emailConfirmed") != null && rs.getString("emailConfirmed")
										.equalsIgnoreCase("Y")) ? true : false;
								if (isEmailConfirmed) {
									flownRecordsDTO.setMemberAccountId(rs.getString("memberAccountId"));
								}
								flownRecordsDTO.setTitle(rs.getString("title"));
								flownRecordsDTO.setLastName(rs.getString("lastName"));
								flownRecordsDTO.setFirstName(rs.getString("firstName"));
								flownRecordsDTO.setPaxType(rs.getString("paxType"));
								flownRecordsDTO.setDob(rs.getDate("dob"));
								flownRecordsDTO.setNationality(rs.getString("nationality"));
								flownRecordsDTO.setOperaringFlight(rs.getString("operaringFlight"));
								Timestamp timestamp = rs.getTimestamp("flightTime");
								flownRecordsDTO.setFlightDateTime(new Date(timestamp.getTime()));
								flownRecordsDTO.setDepartureAirport(rs.getString("departureAirport"));
								flownRecordsDTO.setArrivalAirport(rs.getString("arrivalAirport"));
								flownRecordsDTO.setCabinClass(rs.getString("cabinClass"));
								flownRecordsDTO.setBookingClass(rs.getString("bookingClass"));
								flownRecordsDTO.setPnr(rs.getString("pnr"));
								flownRecordsDTO.setTicketIssuanceDate(rs.getDate("ticketIssuanceDate"));
								flownRecordsDTO.setTicketNumber(rs.getString("ticketNumber"));
								flownRecordsDTO.setCouponNumber(rs.getString("couponNumber"));
								flownRecordsDTO.setAgentInitials(rs.getString("agentInitials"));
								flownRecordsDTO.setContactNumber(rs.getString("contactNumber"));
								flownRecordsDTO.setContactEmail(rs.getString("contactEmail"));
								flownRecordsDTO.setCountryOfResidence(rs.getString("countryOfResidence"));
								flownRecordsDTO.setSalesChannel(rs.getInt("salesChannel"));
								flownRecordsDTO.setPassportNumber(rs.getString("passportNumber"));
								flownRecordsDTO.setProductList(rs.getString("product"));
								flowRecordsDTOs.add(flownRecordsDTO);
							}
						}

						return flowRecordsDTOs;
					}
				});

		return flowRecordsDTOs;
	}

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	private static Date toDate(Timestamp timestamp) {

		if (timestamp != null) {
			long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
			return new java.util.Date(milliseconds);
		} else {
			return timestamp;
		}
	}

	private String getDateStr(Date dt) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(dt);
	}

	@Override
	public void saveGravtyAuthToken(String token, String expiresIn) throws CommonsDataAccessException {
		SystemLMSAuthToken systemToken = getGravtyAuthTokenDB(token);
		Timestamp currenttimestamp = new Timestamp(new Date().getTime());
		//String format ="yyyy/mm/dd hh24:mi:ss;
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDatasource());
		if(systemToken.getCreatedTimeStamp() != null){
			//to delete existing token from db
			jdbcTemplate.update("truncate table T_LMS_SYSTEM_AUTH_TOKEN");
		}
		jdbcTemplate.update(
			    "INSERT INTO T_LMS_SYSTEM_AUTH_TOKEN (AUTH_TOKEN,CREATED_TIME_STAMP,TTL) VALUES (?, ?, ?)",
			    token, toDate(currenttimestamp), expiresIn
			);
	}

	@SuppressWarnings("unchecked")
	@Override
	public SystemLMSAuthToken getGravtyAuthTokenDB(String authToken) throws CommonsDataAccessException {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDatasource());
		StringBuilder queryBuilder = new StringBuilder();
		
		queryBuilder.append("SELECT * FROM T_LMS_SYSTEM_AUTH_TOKEN WHERE AUTH_TOKEN="+authToken);
		List<SystemLMSAuthToken> authData = (List<SystemLMSAuthToken>) jdbcTemplate.query(queryBuilder.toString(),new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SystemLMSAuthToken> tokens = null;
				if(rs !=null){
					tokens = new ArrayList<SystemLMSAuthToken>();
					while (rs.next()) {
						SystemLMSAuthToken sToken = new SystemLMSAuthToken();
						sToken.setAuthToken(rs.getString("AUTH_TOKEN"));
						sToken.setCreatedTimeStamp(toDate(rs.getTimestamp("CREATED_TIME_STAMP")));
						sToken.setTtl(rs.getLong("TTL"));
						tokens.add(sToken);
						
					} 
				}
				return tokens;
				
				}
			});
		//if(authData.size() > 0)
			return authData.get(0);
		//return null;
		
	}
	@Override
	@SuppressWarnings("unchecked")
	public Nationality getNationalityByCode(Integer code) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDatasource());
		
		
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("select * from t_nationality where NATIONALITY_CODE="+code);
		List<Nationality> nationality = (List<Nationality>) jdbcTemplate.query(queryBuilder.toString(),new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Nationality> tokens = null;
				if(rs !=null){
					tokens = new ArrayList<Nationality>();
					while (rs.next()) {
						Nationality n = new Nationality();
						n.setDescription(rs.getString("DESCRIPTION"));
						n.setIcaoCode(rs.getString("ICAO_CODE"));
						n.setIsoCode(rs.getString("ISO_CODE"));
						n.setNationalityCode(Integer.parseInt(rs.getString("NATIONALITY_CODE")));
						tokens.add(n);
					} 
				}
				return tokens;
				
				}
			});
		
		return nationality.get(0);
	}
	@Override
	@SuppressWarnings("unchecked")
	public Country getCountryByCode(String code) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDatasource());
		
		
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("select * from t_country where COUNTRY_CODE='"+code+"\'");
		List<Country> country = (List<Country>) jdbcTemplate.query(queryBuilder.toString(),new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Country> tokens = null;
				if(rs !=null){
					tokens = new ArrayList<Country>();
					while (rs.next()) {
						Country c = new Country();
						c.setCountryCode(rs.getString("COUNTRY_CODE"));
						c.setCountryName(rs.getString("COUNTRY_NAME"));
						tokens.add(c);
					} 
				}
				return tokens;
				
				}
			});
		
		return country.get(0);
		
	}

}
