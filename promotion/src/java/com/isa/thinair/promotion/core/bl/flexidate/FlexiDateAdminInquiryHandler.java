package com.isa.thinair.promotion.core.bl.flexidate;

import static com.isa.thinair.promotion.core.util.CommandParamNames.FLIGHT_ID;
import static com.isa.thinair.promotion.core.util.CommandParamNames.OPERATION_APPROVAL_VIEW;
import static com.isa.thinair.promotion.core.util.CommandParamNames.OPERATION_CODE;
import static com.isa.thinair.promotion.core.util.CommandParamNames.RESP_APPROVAL_VIEW;
import static com.isa.thinair.promotion.core.util.CommandParamNames.USER_PRINCPAL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.FlexiDateApprovalView;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.bl.common.PromotionAdminInquiryHandler;
import com.isa.thinair.promotion.core.dto.ReservationSegSummaryTo;
import com.isa.thinair.promotion.core.dto.ReservationSummaryTo;
import com.isa.thinair.promotion.core.dto.TransferFlightTo;
import com.isa.thinair.promotion.core.persistence.dao.FlexiDatePromotionAdminDao;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * 
 * @isa.module.command name="flexiDateAdminInquiryHandler"
 */
public class FlexiDateAdminInquiryHandler extends PromotionAdminInquiryHandler {

	private static final Log log = LogFactory.getLog(FlexiDateAdminInquiryHandler.class);

	private FlightBD flightBD;
	private FlexiDatePromotionAdminDao flexiDateAdminDao;

	public FlexiDateAdminInquiryHandler() {
		flightBD = PromotionModuleServiceLocator.getFlightBD();
		flexiDateAdminDao = (FlexiDatePromotionAdminDao) PromotionsHandlerFactory
				.getPromotionAdministrationDao(PromotionType.PROMOTION_FLEXI_DATE);
	}

	protected ServiceResponce handleRequest() throws ModuleException {
		DefaultServiceResponse serviceResponce = new DefaultServiceResponse(false);

		if (getParameter(OPERATION_CODE).equals(OPERATION_APPROVAL_VIEW)) {
			serviceResponce.setSuccess(true);
			constructFlexiDateAdminView(serviceResponce);
		}

		return serviceResponce;
	}

	private void constructFlexiDateAdminView(ServiceResponce serviceResponce) throws ModuleException {

		int flightId = (Integer) getParameter(FLIGHT_ID);
		UserPrincipal userPrincipal = (UserPrincipal)getParameter(USER_PRINCPAL);
		
		int flightSegId;
		List<ReservationSummaryTo> resultApprovals = new ArrayList<ReservationSummaryTo>();
		List<ReservationSummaryTo> basicDataForFlexiDateApproval;
		List<Map<Pair<String, String>, TransferFlightTo>> transferFlightOptions;
		AvailableFlightSearchDTO searchDTO;
		Map<Pair<String, String>, TransferFlightTo> currentFlight;
		TransferFlightTo selectedFlight;
		ReservationSummaryTo resSummaryTo;
		List<ReservationSegSummaryTo> resSegsForApproval = null;

		Flight flight = flightBD.getFlight(flightId);

		for (FlightSegement flightSegment : flight.getFlightSegements()) {
			flightSegId = flightSegment.getFltSegId();

			basicDataForFlexiDateApproval = getBasicDataForFlexiDateApproval(flightSegId);

			for (int a = 0; a < basicDataForFlexiDateApproval.size(); a++) {

				resSummaryTo = basicDataForFlexiDateApproval.get(a);
				String pnr = resSummaryTo.getPnr();

				try {

					searchDTO = PromotionsDaoHelper.createAvailableFlightSearchDTO(flightSegment, resSummaryTo, userPrincipal);
					AvailableFlightDTO currentFlightDTO = PromotionModuleServiceLocator.getAirReservationQueryBD()
							.getAvailableFlightsWithAllFares(searchDTO, null);

					if (currentFlightDTO != null && currentFlightDTO.getSelectedFlight() != null) {

						currentFlight = PromotionsDaoHelper.toTransferFlightTo(currentFlightDTO.getSelectedFlight());

						transferFlightOptions = getFlightOptionsForTransferReservation(resSummaryTo, userPrincipal);

						if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_OUTBOUND) {
							resSegsForApproval = resSummaryTo.getOutboundSegs();
						} else if (resSummaryTo.getPromoApproveDirection() == PromotionsInternalConstants.DIRECTION_INBOUND) {
							resSegsForApproval = resSummaryTo.getInboundSegs();
						}

						for (ReservationSegSummaryTo resSegSummary : resSegsForApproval) {
							// Can't re-protect ---- route isn't identical
							if (!currentFlight.containsKey(PromotionsUtils.getOriginAndDestinationAirports(resSegSummary
									.getSegmentCode()))) {
								resSummaryTo.setPromotionNotExists(true);
								break;
							}
							resSegSummary.setCurrentFlightTo(currentFlight.get(PromotionsUtils
									.getOriginAndDestinationAirports(resSegSummary.getSegmentCode())));

							if (!transferFlightOptions.isEmpty()) {
								if (!transferFlightOptions.get(0).containsKey(
										PromotionsUtils.getOriginAndDestinationAirports(resSegSummary.getSegmentCode()))) {
									resSummaryTo.setPromotionNotExists(true);
									break;
								}
								selectedFlight = transferFlightOptions.get(0).get(
										PromotionsUtils.getOriginAndDestinationAirports(resSegSummary.getSegmentCode()));

								if (resSegSummary.getCurrentFlightTo().getFlightSegId() == selectedFlight.getFlightSegId()) {
									resSummaryTo.setPromotionNotExists(true);
									break;
								}

								resSegSummary.setTransferFlightTo(selectedFlight);
							} else {
								resSummaryTo.setPromotionNotExists(true);
								break;
							}

						}
					} else {
						resSummaryTo.setPromotionNotExists(true);
					}

				} catch (Exception e) {
					// failing of single user does not affect other users
					log.error("flexi requests evaluation failed for [PNR] : " + pnr + e);
					resSummaryTo.setPromotionNotExists(true);
				}

			}
			resultApprovals.addAll(basicDataForFlexiDateApproval);
		}

		serviceResponce.addResponceParam(RESP_APPROVAL_VIEW, getFlexiDateApprovalView(resultApprovals));
	}

	private List<Map<Pair<String, String>, TransferFlightTo>> getFlightOptionsForTransferReservation(
			ReservationSummaryTo reservationSummaryTo, UserPrincipal userPrincipal) throws ModuleException {
		List<Map<Pair<String, String>, TransferFlightTo>> flightOptions = new ArrayList<Map<Pair<String, String>, TransferFlightTo>>();

		AvailableFlightSearchDTO searchDTO = PromotionsDaoHelper.createAvailableFlightSearchDTO(reservationSummaryTo, userPrincipal);

		AvailableFlightDTO availableFlightDTO = PromotionModuleServiceLocator.getAirReservationQueryBD()
				.getAvailableFlightsWithAllFares(searchDTO, null);

		if (availableFlightDTO != null && availableFlightDTO.getSelectedFlight() != null) {
			flightOptions = PromotionsDaoHelper.toTransferFlightTos(availableFlightDTO);
		}

		return flightOptions;
	}

	private List<FlexiDateApprovalView> getFlexiDateApprovalView(List<ReservationSummaryTo> basicDataForFlexiDateApproval) {

		Map<String, FlexiDateApprovalView> approvalView = new HashMap<String, FlexiDateApprovalView>();
		FlexiDateApprovalView singleRec;
		Map<String, String> transferSegData;

		for (ReservationSummaryTo resSummaryTo : basicDataForFlexiDateApproval) {

			String pnr = resSummaryTo.getPnr();

			if (!resSummaryTo.isPromotionNotExists()) {

				for (ReservationSegSummaryTo resSegSummaryTo : resSummaryTo.getPromotionRequestedResSegs()) {
					if (resSegSummaryTo.isSelectedToSendForPromoApproval()) {
						if (!approvalView.containsKey(resSummaryTo.getPnr())) {

							singleRec = new FlexiDateApprovalView();
							singleRec.setPnr(resSummaryTo.getPnr());
							singleRec.setPassengerCount(resSummaryTo.getPaxCount().get(PaxTypeTO.ADULT)
									+ resSummaryTo.getPaxCount().get(PaxTypeTO.CHILD));
							singleRec.setFlightNumber(resSegSummaryTo.getTransferFlightTo().getFlightNumber());
							singleRec.setDepartureDate(resSegSummaryTo.getTransferFlightTo().getDepartureDate());
							singleRec.setRevenue(resSummaryTo.getRevenueFromTransfer().doubleValue());
							singleRec.setVersionId(resSummaryTo.getVersionId());
							singleRec.setResSegId(resSegSummaryTo.getResSegId());
							singleRec.setFlightSegId(resSegSummaryTo.getTransferFlightTo().getFlightSegId());
							singleRec.setFlightSegCode(resSegSummaryTo.getSegmentCode());
							singleRec.setPromotionId(resSegSummaryTo.getPromotionId());
							singleRec.setPromotionReqId(resSegSummaryTo.getPromotionRequestId());
							singleRec.setCurrentSellingFare(resSegSummaryTo.getCurrentFlightTo().getFare().doubleValue());
							singleRec.setTransferedSegmentsFare(resSegSummaryTo.getTransferFlightTo().getFare().doubleValue());
							singleRec.setRewards(resSegSummaryTo.getEffectiveChargeOrRefund().doubleValue());

							approvalView.put(resSummaryTo.getPnr(), singleRec);
						} else {
							singleRec = approvalView.get(resSummaryTo.getPnr());
						}
						transferSegData = new HashMap<String, String>();
						transferSegData.put(CommandParamNames.RESERVATION_SEGMENT_ID,
								String.valueOf(resSegSummaryTo.getResSegId()));
						transferSegData.put(CommandParamNames.FLIGHT_SEGMENT_ID,
								String.valueOf(resSegSummaryTo.getTransferFlightTo().getFlightSegId()));
						singleRec.addSegmentTransferData(transferSegData);
						singleRec.addApprovingSegs(resSegSummaryTo.getSegmentCode());

					} else {
						log.info("some transfer segments are missing for the [pnr]  : " + pnr);
					}
				}
			} else {
				// handle view for transfer not exists for all segs
				FlexiDateApprovalView dummyFlexiView = new FlexiDateApprovalView();
				dummyFlexiView.setPnr(pnr);
				dummyFlexiView.setFlightNumber("No match / Same Flight");
				dummyFlexiView.setTransferExists(false);
				approvalView.put(pnr, dummyFlexiView);
				log.info("No transfer flights avaliable for pnr : " + pnr);
			}
		}

		return new ArrayList<FlexiDateApprovalView>(approvalView.values());
	}

	private List<ReservationSummaryTo> getBasicDataForFlexiDateApproval(int flightSegId) throws ModuleException {
		return flexiDateAdminDao.getBasicDataForFlexiDateApproval(flightSegId);
	}

}
