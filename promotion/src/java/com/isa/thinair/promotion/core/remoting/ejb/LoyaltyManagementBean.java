package com.isa.thinair.promotion.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateRes;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.promotion.core.bl.lms.BurnLoyaltyPointsBL;
import com.isa.thinair.promotion.core.bl.lms.LMSFileGeneratorBL;
import com.isa.thinair.promotion.core.bl.lms.LMSFileUploaderBL;
import com.isa.thinair.promotion.core.bl.lms.LoyaltyManageBLFactory;
import com.isa.thinair.promotion.core.service.bd.LoyaltyManagementBDLocal;
import com.isa.thinair.promotion.core.service.bd.LoyaltyManagementBDRemote;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * 
 * @author rumesh
 * 
 */

@Stateless
@LocalBinding(jndiBinding = "LoyaltyManagement.local")
@RemoteBinding(jndiBinding = "LoyaltyManagement.remote")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LoyaltyManagementBean extends PlatformBaseSessionBean implements LoyaltyManagementBDLocal, LoyaltyManagementBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isValidLoyaltyAccount(String memberAccountId) throws ModuleException {

		try {
			LoyaltyMemberStateDTO memberStateDTO = PromotionModuleServiceLocator.getWSClientBD().getLoyaltyMemberState(
					memberAccountId);

			if (memberStateDTO != null && memberStateDTO.isLoyaltyMember() && !memberStateDTO.isMemberSuspended()) {
				return true;
			}

			return false;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String createLoyaltyMember(Customer customer) throws ModuleException {

		try {

			return LoyaltyManageBLFactory.getLoyaltyManageBL().createLoyaltyMember(customer);

		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void updateMemberProfile(Customer customer) throws ModuleException {

		try {

			LoyaltyManageBLFactory.getLoyaltyManageBL().updateMemberProfile(customer);

		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyMemberCoreDTO getLoyaltyMemberCoreDetails(String memberAccountId) throws ModuleException {
		try {
			return PromotionModuleServiceLocator.getWSClientBD().getLoyaltyMemberCoreDetails(memberAccountId);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyPointDetailsDTO getLoyaltyMemberPointBalances(String memberAccountId, String memberExternalId) throws ModuleException {
		try {
			return PromotionModuleServiceLocator.getWSClientBD().getLoyaltyMemberPointBalances(memberAccountId, memberExternalId);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException {
		try {
			return PromotionModuleServiceLocator.getWSClientBD().setMemberPassword(memberAccountId, password);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String getCrossPortalLoginUrl(LmsMember lmsMember, String menuItemIntRef) throws ModuleException {
		try {
			
			return LoyaltyManageBLFactory.getLoyaltyManageBL().getCrossPortalLoginUrl(lmsMember, menuItemIntRef);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LoyaltyProduct> getLoyaltyProducts() throws ModuleException {
		try {
			return PromotionsHandlerFactory.getLoyaltyManagmentDAO().getLoyaltyProducts();
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce calculatePaxLoyaltyRedeemableAmounts(RedeemCalculateReq redeemCalculateReqTo) throws ModuleException {
		try {
			DefaultServiceResponse response = new DefaultServiceResponse(true);

			List<LoyaltyProduct> loyaltyProductDTOs = getLoyaltyProducts();
			BigDecimal pointToCurrencyConversionRate = redeemCalculateReqTo.getLoyaltyPointsToCurrencyConversionRate();
			BigDecimal currencyToPointConversionRate = redeemCalculateReqTo.getCurrencyToLoyaltyPointsConversionRate();

			BurnLoyaltyPointsBL burnLoyaltyPointsBL = null;
			if (redeemCalculateReqTo.getOndFareDTOs() == null && redeemCalculateReqTo.getPaxCarrierExternalCharges() == null) {
				burnLoyaltyPointsBL = new BurnLoyaltyPointsBL(redeemCalculateReqTo.getPaxProductDueAmountForCarrier(),
						loyaltyProductDTOs, pointToCurrencyConversionRate, currencyToPointConversionRate,
						redeemCalculateReqTo.getCurrencyExRate());
			} else {
				burnLoyaltyPointsBL = new BurnLoyaltyPointsBL(redeemCalculateReqTo.getOndFareDTOs(),
						redeemCalculateReqTo.getPaxExternalChargesForCarrier(), loyaltyProductDTOs,
						pointToCurrencyConversionRate, currencyToPointConversionRate, redeemCalculateReqTo.getCurrencyExRate());
			}

			LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = getLoyaltyMemberPointBalances(redeemCalculateReqTo
					.getMemberAccountId(), redeemCalculateReqTo.getMemberExternalId());

			burnLoyaltyPointsBL.setMemberAccountId(redeemCalculateReqTo.getMemberAccountId());
			if (redeemCalculateReqTo.getMemberAvailablePoints() != null) {
				burnLoyaltyPointsBL.setAvailablePoints(redeemCalculateReqTo.getMemberAvailablePoints());
			} else {
				burnLoyaltyPointsBL.setAvailablePoints(loyaltyPointDetailsDTO.getMemberPointsAvailable());
			}

			if (redeemCalculateReqTo.getRequestedLoyaltyPointsAmount() == null) {
				redeemCalculateReqTo.setRequestedLoyaltyPointsAmount(burnLoyaltyPointsBL
						.getPointsValueByBaseCurrency(loyaltyPointDetailsDTO.getMemberPointsAvailable()));
			}
			if (redeemCalculateReqTo.getDiscountRQ() != null) {
				redeemCalculateReqTo.getDiscountRQ().setOndFareDTOs(redeemCalculateReqTo.getOndFareDTOs());
			}
			RedeemCalculateRes redeemCalculateRes = burnLoyaltyPointsBL.calculatePaxLoyaltyRedeemableAmounts(
					redeemCalculateReqTo.getRequestedLoyaltyPointsAmount(), redeemCalculateReqTo.getDiscountRQ());

			Map<Integer, Map<String, Double>> paxWiseProductPoints = redeemCalculateRes.getPaxWiseProductPoints();
			Map<Integer, Map<String, BigDecimal>> paxWiseProductAmount = redeemCalculateRes.getPaxWiseProductValue();

			response.addResponceParam(ResponceCodes.ResponseParams.PAX_PRODUCT_POINTS, paxWiseProductPoints);
			response.addResponceParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT, paxWiseProductAmount);
			response.addResponceParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS,
					loyaltyPointDetailsDTO.getMemberPointsAvailable());
			response.addResponceParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS_AMOUNT,
					burnLoyaltyPointsBL.getPointsValueByBaseCurrency(loyaltyPointDetailsDTO.getMemberPointsAvailable()));

			return response;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce issueLoyaltyRewards(RedeemLoyaltyPointsReq redeemLoyaltyPointsReq) throws ModuleException {
		try {

			return LoyaltyManageBLFactory.getLoyaltyManageBL().issueLoyaltyRewards(redeemLoyaltyPointsReq);

		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce redeemIssuedRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String memberAccountId)
			throws ModuleException {
		try {
			
			return LoyaltyManageBLFactory.getLoyaltyManageBL().redeemIssuedRewards(rewardIds, appIndicator, memberAccountId);
			
		} catch (Exception e) {
			log.error("=================Rewards Points issue failed====");
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce cancelRedeemedLoyaltyPoints(String pnr,String[] rewardIds, String memberAccountId) throws ModuleException {
		try {
			if(StringUtil.isNullOrEmpty(pnr)){
				log.error("====================================================");
				log.error("=================PNR IS NULL @ CANCELL REDEEMING====");
				log.error("====================================================");
			}
			return LoyaltyManageBLFactory.getLoyaltyManageBL().cancelRedeemedLoyaltyPoints(pnr, rewardIds, memberAccountId);

		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(1900)
	public void generateFlownFile(Date rptPeriodStartDate, Date rptPeriodEndDate, boolean processErrorsOnly)
			throws ModuleException {
		LMSFileGeneratorBL lmsFileGeneratorBL = new LMSFileGeneratorBL();
		lmsFileGeneratorBL.generateFlownFile(rptPeriodStartDate, rptPeriodEndDate, processErrorsOnly, getUserPrincipal());

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(1900)
	public void generateProductFile() throws ModuleException {
		LMSFileGeneratorBL lmsFileGeneratorBL = new LMSFileGeneratorBL();
		lmsFileGeneratorBL.generateProductFile(getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updatePendingFiles(FILE_TYPE fileType) throws ModuleException {
		LMSFileUploaderBL lmsFileUploaderBL = new LMSFileUploaderBL();
		lmsFileUploaderBL.updatePendingFiles(fileType);
	}

	@Override
	public Map<String, String> getLoyaltyLocationExtReferences() {
		return PromotionModuleServiceLocator.getLoyaltyExternalConfig().getLocationExtReference();
	}

	@Override
	public Map<String, String> getLoyaltyEnrollingChannelExtReferences() {
		return PromotionModuleServiceLocator.getLoyaltyExternalConfig().getEnrollingChannelExtReference();
	}
	
	@Override
	public LMSMemberDTO getMergeLoyaltyMember(String memberFFID, TrackInfoDTO trackInfo) throws ModuleException {
		return PromotionModuleServiceLocator.getLCCLoyaltyManagementBD().getMergeLoyaltyManagement(memberFFID, trackInfo);
	}

	@Override
	public int customerNameUpdate(String memberFFID, TrackInfoDTO trackInfo, String title, String firstName, String lastName) throws ModuleException {
		return PromotionModuleServiceLocator.getLCCLoyaltyManagementBD().customerNameUpdate(memberFFID, trackInfo, title,
				firstName, lastName);
	}

	@Override
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException {
		try {
			return PromotionModuleServiceLocator.getWSClientBD()
					.saveMemberHeadOfHouseHold(memberAccountId, headOfHosueHold);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException {
		try {
			return PromotionModuleServiceLocator.getWSClientBD()
					.removeMemberHeadOfHouseHold(memberAccountId);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}
	
	

	
	@Override
	public MemberRemoteLoginResponseDTO memberLogin(String userName, String password) throws ModuleException {
		
		try {
			return PromotionModuleServiceLocator.getWSClientBD().memberLogin(userName, password);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}
}
