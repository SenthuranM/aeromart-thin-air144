package com.isa.thinair.promotion.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class ReservationSummaryTo implements Serializable {

	private String pnr;
	private Map<String, Integer> paxCount;
	private int promoApproveDirection = PromotionsInternalConstants.DIRECTION_NONE;
	private List<ReservationSegSummaryTo> outboundSegs;
	private List<ReservationSegSummaryTo> inboundSegs;
	private String logicalCcCode;
	private long versionId;
	private boolean promotionNotExists = false;

	public ReservationSummaryTo() {
		outboundSegs = new ArrayList<ReservationSegSummaryTo>();
		inboundSegs = new ArrayList<ReservationSegSummaryTo>();
		paxCount = new HashMap<String, Integer>();
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<String, Integer> getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(Map<String, Integer> paxCount) {
		this.paxCount = paxCount;
	}

	public void putPaxCount(String paxType, Integer count) {
		this.paxCount.put(paxType, count);
	}

	public List<ReservationSegSummaryTo> getOutboundSegs() {
		return outboundSegs;
	}

	public void addOutboundSeg(ReservationSegSummaryTo to) {
		this.outboundSegs.add(to);
	}

	public void setOutboundSegs(List<ReservationSegSummaryTo> outboundSegs) {
		this.outboundSegs = outboundSegs;
	}

	public List<ReservationSegSummaryTo> getInboundSegs() {
		return inboundSegs;
	}

	public void addInboundSeg(ReservationSegSummaryTo to) {
		this.inboundSegs.add(to);
	}

	public void setInboundSegs(List<ReservationSegSummaryTo> inboundSegs) {
		this.inboundSegs = inboundSegs;
	}

	public int getPromoApproveDirection() {
		return promoApproveDirection;
	}

	public void setPromoApproveDirection(int promoApproveDirection) {
		this.promoApproveDirection = promoApproveDirection;
	}

	public String getLogicalCcCode() {
		return logicalCcCode;
	}

	public void setLogicalCcCode(String logicalCcCode) {
		this.logicalCcCode = logicalCcCode;
	}

	public long getVersionId() {
		return versionId;
	}

	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}

	/**
	 * Revenue = n * (Current fare selling in original flight - Current fare in the tranfer flight) + fare at which
	 * ticket was purchased for n pax - reward amount
	 * 
	 * where n = number of adults + number of childs
	 * 
	 * reward amount For a specific direction(at a time for outbound or inbound) for whole reservation
	 * 
	 */
	public BigDecimal getRevenueFromTransfer() {
		BigDecimal revenueFromTransfer = BigDecimal.ZERO;

		BigDecimal currentResValue;
		BigDecimal transferedResValue;
		BigDecimal effectiveChargeRefund;
		BigDecimal originalFareForAllPaxs = BigDecimal.ZERO;
		List<ReservationSegSummaryTo> consideringResSegs = getPromotionRequestedResSegs();

		boolean countedForDirection = false;
		boolean connectionFareFound = false;
		for (ReservationSegSummaryTo resSegSummaryTo : consideringResSegs) {
			if (!countedForDirection) {
				currentResValue = resSegSummaryTo.getCurrentFlightTo().getFare();
				transferedResValue = resSegSummaryTo.getTransferFlightTo().getFare();
				revenueFromTransfer = revenueFromTransfer.add(currentResValue).subtract(transferedResValue);
				countedForDirection = true;
			}

			if (resSegSummaryTo.getBcAllocationType().equals(PromotionsInternalConstants.CONNECTION_FARE)) {
				if (!connectionFareFound) {
					originalFareForAllPaxs = originalFareForAllPaxs.add(resSegSummaryTo.getFare());
					connectionFareFound = true;
				}
			} else {
				originalFareForAllPaxs = originalFareForAllPaxs.add(resSegSummaryTo.getFare());
			}
		}
		effectiveChargeRefund = consideringResSegs.get(0).getEffectiveChargeOrRefund();

		revenueFromTransfer = revenueFromTransfer.multiply(new BigDecimal(paxCount.get(PaxTypeTO.ADULT)
				+ paxCount.get(PaxTypeTO.CHILD)));
		revenueFromTransfer = revenueFromTransfer.add(originalFareForAllPaxs).subtract(effectiveChargeRefund);

		return revenueFromTransfer;
	}

	public List<ReservationSegSummaryTo> getPromotionRequestedResSegs() {
		List<ReservationSegSummaryTo> resSegs = new ArrayList<ReservationSegSummaryTo>();

		if (promoApproveDirection == PromotionsInternalConstants.DIRECTION_INBOUND) {
			for (ReservationSegSummaryTo resSegSummaryTo : inboundSegs) {
				if (resSegSummaryTo.isSubscribedForPromo()) {
					resSegs.add(resSegSummaryTo);
				}
			}
		} else if (promoApproveDirection == PromotionsInternalConstants.DIRECTION_OUTBOUND) {

			for (ReservationSegSummaryTo resSegSummaryTo : outboundSegs) {
				if (resSegSummaryTo.isSubscribedForPromo()) {
					resSegs.add(resSegSummaryTo);
				}
			}
		}

		return resSegs;
	}

	public boolean isPromotionNotExists() {
		return promotionNotExists;
	}

	public void setPromotionNotExists(boolean promotionNotExists) {
		this.promotionNotExists = promotionNotExists;
	}

}
