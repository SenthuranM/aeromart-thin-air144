package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;

@Local
public interface PromotionAdministrationBDLocalImpl extends PromotionAdministrationBD {

}
