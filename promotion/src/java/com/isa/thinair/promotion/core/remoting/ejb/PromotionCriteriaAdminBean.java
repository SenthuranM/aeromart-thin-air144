package com.isa.thinair.promotion.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.promotion.api.model.BuyNGetPromotionCriteria;
import com.isa.thinair.promotion.api.model.DiscountPromotionCriteria;
import com.isa.thinair.promotion.api.model.FreeServicePromotionCriteria;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionCriteriaTransformer;
import com.isa.thinair.promotion.core.bl.promocode.PromoCodeBL;
import com.isa.thinair.promotion.core.persistence.dao.PromotionCriteriaDAO;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;
import com.isa.thinair.promotion.core.service.bd.PromotionCriteriaAdminBDLocal;
import com.isa.thinair.promotion.core.service.bd.PromotionCriteriaAdminBDRemote;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * Stateless EJB implementing the methods for PromotionCriteria class hierarchy management.
 * 
 * @author thihara
 * 
 */

@Stateless
@RemoteBinding(jndiBinding = "PromotionCriteriaAdmin.remote")
@LocalBinding(jndiBinding = "PromotionCriteriaAdmin.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PromotionCriteriaAdminBean extends PlatformBaseSessionBean implements PromotionCriteriaAdminBDLocal,
		PromotionCriteriaAdminBDRemote {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PromotionCriteriaTO findPromotionCriteria(Long promotionCriteriaID) throws ModuleException {
		try {

			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();

			PromotionCriteria promotionCriteria = promotionCriteriaDAO.findPromotionCriteria(promotionCriteriaID);

			return promotionCriteria != null ? PromotionCriteriaTransformer.toPromotionCriteriaTO(promotionCriteria,null) : null;

		} catch (Exception x) {
			throw handleException(x, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long savePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException {
		try {
			PromotionCriteria criteriaToSave = PromotionCriteriaTransformer.toPromotionCriteria(promotionCriteriaTO);
			if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(promotionCriteriaTO
					.getPromotionCriteriaType())) {
				AuditAirMaster.doAudit(((BuyNGetPromotionCriteria) criteriaToSave), getUserPrincipal());
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(promotionCriteriaTO
					.getPromotionCriteriaType())) {
				AuditAirMaster.doAudit(((DiscountPromotionCriteria) criteriaToSave), getUserPrincipal());
			} else {
				AuditAirMaster.doAudit(((FreeServicePromotionCriteria) criteriaToSave), getUserPrincipal());
			}
	
			criteriaToSave = savePromotionCriteria(criteriaToSave);

			return criteriaToSave.getPromoCriteriaID();

		} catch (Exception x) {
			throw handleException(x, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PromotionCriteria savePromotionCriteria(PromotionCriteria promotionCriteria) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			Long promotionCriteriaID = promotionCriteriaDAO.savePromotionCriteria(promotionCriteria);
			promotionCriteria.setPromoCriteriaID(promotionCriteriaID);

			return promotionCriteria;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public long updatePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException {

		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();

			PromotionCriteria promotionCriteriaToUpdate = PromotionCriteriaTransformer.toPromotionCriteria(promotionCriteriaTO);

			if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(promotionCriteriaTO
					.getPromotionCriteriaType())) {
				AuditAirMaster.doAudit(((BuyNGetPromotionCriteria) promotionCriteriaToUpdate), getUserPrincipal());
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(promotionCriteriaTO
					.getPromotionCriteriaType())) {
				AuditAirMaster.doAudit(((DiscountPromotionCriteria) promotionCriteriaToUpdate), getUserPrincipal());
			} else if (PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(promotionCriteriaTO
					.getPromotionCriteriaType())){
				AuditAirMaster.doAudit(((FreeServicePromotionCriteria) promotionCriteriaToUpdate), getUserPrincipal());
			}
			return promotionCriteriaDAO.updatePromotionCriteria(promotionCriteriaToUpdate);

		} catch (Exception x) {
			throw handleException(x, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deletePromotionCriteria(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();

			PromotionCriteria promotionCriteriaToDelete = PromotionCriteriaTransformer
					.toMinumumPromotionCriteria(promotionCriteriaTO);
			String code = promotionCriteriaToDelete.getPromoCriteriaID().toString() + "-"
					+ promotionCriteriaToDelete.getPromoCode() + "-" + promotionCriteriaToDelete.getPromoName();
			AuditAirMaster.doAudit(code, AuditAirMaster.PROMO_CODE, getUserPrincipal());
			promotionCriteriaDAO.deletePromotionCriteria(promotionCriteriaToDelete);

		} catch (Exception x) {
			throw handleException(x, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page<PromotionCriteriaTO> searchPromotionCriteria(PromoCriteriaSearchTO promoCriteriaSearch, Integer start,
			Integer size) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			Page<PromotionCriteria> resultPage = promotionCriteriaDAO.searchPromotionCriteria(promoCriteriaSearch, start, size);

			Collection<PromotionCriteriaTO> transformedDTOs = new HashSet<PromotionCriteriaTO>();			
			transformedDTOs = PromotionCriteriaTransformer.prepareCriteriaDTOForMultiplePromoCodes(resultPage);

			Page<PromotionCriteriaTO> resultTOPage = new Page<PromotionCriteriaTO>(resultPage.getTotalNoOfRecords(),
					resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);

			return resultTOPage;
		} catch (Exception x) {
			throw handleException(x, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSystemGeneratedPromotion(Long promotionId, BigDecimal consumedCredits, String status)
			throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			promotionCriteriaDAO.updateSystemGeneratedPromotion(promotionId, consumedCredits, status);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long updatePromotionCriteriaStatus(PromotionCriteriaTO promotionCriteriaTO) throws ModuleException {

		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();

			PromotionCriteria promotionCriteriaToUpdate = PromotionCriteriaTransformer
					.toStatusChangePromotionCriteria(promotionCriteriaTO);
			return promotionCriteriaDAO.updatePromotionCriteriaStatus(promotionCriteriaToUpdate);

		} catch (Exception x) {
			throw handleException(x, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activateSystemGeneratedPromotion(String originPnr) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			promotionCriteriaDAO.activateSystemGeneratedPromotion(originPnr);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PromotionCriteria getPromotionCriteria(String originatedPnr) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			return promotionCriteriaDAO.getPromotionCriteria(originatedPnr);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePromotionCodeDescKey(long criteriaId, String messageKey) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			promotionCriteriaDAO.updatePromotionCodeDescKey(criteriaId, messageKey);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	public List<String> generatePromoCodes(int numOfCodes) throws ModuleException {
		List<String> codeList = new ArrayList<String>();
		try {
			codeList = PromoCodeBL.generatePromotionCodes(numOfCodes);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
		return codeList;
	}

	@Override
	public void updatePromoCodeUtilization(String promoCode) throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			promotionCriteriaDAO.updatepromoCodeUtilization(promoCode);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
		
	}

	@Override
	public List<String> getPromoCodesByID(int promoCriteriaId) {
		PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
		List<String> promoCodes = promotionCriteriaDAO.getPromoCodesById(Integer.toString(promoCriteriaId));
		return promoCodes;
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSystemGeneratedPromotionforOHD(Long promotionId, BigDecimal consumedCredits, String status)
			throws ModuleException {
		try {
			PromotionCriteriaDAO promotionCriteriaDAO = PromotionsHandlerFactory.getPromotionCriteriaDAO();
			promotionCriteriaDAO.updateSystemGeneratedPromotionForOHD(promotionId, consumedCredits, status);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

}
