package com.isa.thinair.promotion.core.audit;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

public abstract class AuditPromotion {

	private static final Log log = LogFactory.getLog(AuditPromotion.class);

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public static void doAudit(Persistent persistant, Persistent existingPersistent, UserPrincipal principal)
			throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof BundledFare) {
			BundledFare bundledFare = (BundledFare) persistant;
			BundledFare existingBundledFare = null;

			if (bundledFare.getVersion() < 0) {
				audit.setTaskCode(TasksUtil.BUNDLED_FARE_ADD);
			} else {
				if (existingPersistent == null) {
					existingBundledFare = PromotionsHandlerFactory.getBundledfareDAO().getBundledFare(
							bundledFare.getBundledFareId());
				} else {
					existingBundledFare = (BundledFare) existingPersistent;
				}
				audit.setTaskCode(TasksUtil.BUNDLED_FARE_EDIT);
			}

			contents.put("Bundled fare ID", String.valueOf(bundledFare.getBundledFareId()));
			contents.put("Bundled fare name", BeanUtils.nullHandler(bundledFare.getBundledFareName()));
			contents.put("booking classes", BeanUtils.nullHandler(bundledFare.getBookingClasses()));
			contents.put("Flight date from", BeanUtils.nullHandler(bundledFare.getFlightDateFrom()));
			contents.put("Flighr date to", BeanUtils.nullHandler(bundledFare.getFlightDateTo()));
			contents.put("OnD codes", BeanUtils.nullHandler(bundledFare.getOndCodes()));
			contents.put("Agents", String.valueOf(bundledFare.getAgents()));
			contents.put("Flight Numbers", String.valueOf(bundledFare.getFlightNumbers()));
			contents.put("Charge code", BeanUtils.nullHandler(bundledFare.getChargeCode()));
			contents.put("Bundle Fee", String.valueOf(bundledFare.getDefaultBundledFee()));
			contents.put("Status", BeanUtils.nullHandler(bundledFare.getStatus()));
			contents.put("Periods", bundledFare.getBundledFarePeriods().toString());

			if (existingBundledFare != null) {
				try {
					contents.put("changedContent", populateChangedContent(existingBundledFare, bundledFare));
				} catch (ParseException e) {
					log.error("Error in auditing bundled fare", e);
				}
			}
		}

		PromotionModuleServiceLocator.getAuditorBD().audit(audit, contents);
	}

	private static String populateChangedContent(BundledFare existingBundledFare, BundledFare modifiedBundledFare)
			throws ParseException {
		StringBuilder changedContent = new StringBuilder();
		String separator = "||";

		if (!existingBundledFare.getBundledFareName().equals(modifiedBundledFare.getBundledFareName())) {
			changedContent.append("Bundled fare name changed from ");
			changedContent.append(blankHandler(existingBundledFare.getBundledFareName()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getBundledFareName()));
			changedContent.append(separator);
		}

		// if (!existingBundledFare.getBookingClass().equals(modifiedBundledFare.getBookingClass())) {
		// changedContent.append("Booking class changed from ");
		// changedContent.append(blankHandler(existingBundledFare.getBookingClass()));
		// changedContent.append(" to ");
		// changedContent.append(blankHandler(modifiedBundledFare.getBookingClass()));
		// changedContent.append(separator);
		// }

		if ((existingBundledFare.getBookingClasses() != null && modifiedBundledFare.getBookingClasses() != null)
				|| (existingBundledFare.getBookingClasses() == null && modifiedBundledFare.getBookingClasses() != null)
				|| (existingBundledFare.getBookingClasses() != null && !existingBundledFare.getBookingClasses().containsAll(
						modifiedBundledFare.getBookingClasses()))) {
			changedContent.append("Booking class changed from ");
			changedContent.append(blankHandler(existingBundledFare.getBookingClasses()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getBookingClasses()));
			changedContent.append(separator);
		}

		if (!((existingBundledFare.getFlightDateFrom() != null) ? CalendarUtil.formatForSQL(
				existingBundledFare.getFlightDateFrom(), CalendarUtil.PATTERN4).toString() : "").equals((modifiedBundledFare
				.getFlightDateFrom() != null) ? CalendarUtil.formatForSQL(modifiedBundledFare.getFlightDateFrom(),
				CalendarUtil.PATTERN4).toString() : "")) {

			changedContent.append("Flight from date changed from ");
			changedContent.append((existingBundledFare.getFlightDateFrom() != null) ? CalendarUtil.formatForSQL(
					existingBundledFare.getFlightDateFrom(), CalendarUtil.PATTERN4).toString() : "");
			changedContent.append(" to ");
			changedContent.append((modifiedBundledFare.getFlightDateFrom() != null) ? CalendarUtil.formatForSQL(
					modifiedBundledFare.getFlightDateFrom(), CalendarUtil.PATTERN4).toString() : "");
			changedContent.append(separator);

		}

		if (!((existingBundledFare.getFlightDateTo() != null) ? CalendarUtil.formatForSQL(existingBundledFare.getFlightDateTo(),
				CalendarUtil.PATTERN4).toString() : "").equals((modifiedBundledFare.getFlightDateTo() != null) ? CalendarUtil
				.formatForSQL(modifiedBundledFare.getFlightDateTo(), CalendarUtil.PATTERN4).toString() : "")) {

			changedContent.append("Flight to date changed from ");
			changedContent.append((existingBundledFare.getFlightDateTo() != null) ? CalendarUtil.formatForSQL(
					existingBundledFare.getFlightDateTo(), CalendarUtil.PATTERN4).toString() : "");
			changedContent.append(" to ");
			changedContent.append((modifiedBundledFare.getFlightDateTo() != null) ? CalendarUtil.formatForSQL(
					modifiedBundledFare.getFlightDateTo(), CalendarUtil.PATTERN4).toString() : "");
			changedContent.append(separator);
		}

		if (!((existingBundledFare.getOndCodes() != null) ? existingBundledFare.getOndCodes() : "").equals((modifiedBundledFare
				.getOndCodes() != null) ? modifiedBundledFare.getOndCodes() : "")) {
			changedContent.append("Ond Codes changed from ");
			changedContent.append(blankHandler(existingBundledFare.getOndCodes()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getOndCodes()));
			changedContent.append(separator);
		}

		if (!((existingBundledFare.getAgents() != null) ? existingBundledFare.getAgents() : "").equals((modifiedBundledFare
				.getAgents() != null) ? modifiedBundledFare.getAgents() : "")) {
			changedContent.append("Agents changed from ");
			changedContent.append(blankHandler(existingBundledFare.getAgents()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getAgents()));
			changedContent.append(separator);
		}

		if (!((existingBundledFare.getFlightNumbers() != null) ? existingBundledFare.getFlightNumbers() : "")
				.equals((modifiedBundledFare.getFlightNumbers() != null) ? modifiedBundledFare.getFlightNumbers() : "")) {
			changedContent.append("Flight numbers changed from ");
			changedContent.append(blankHandler(existingBundledFare.getFlightNumbers()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getFlightNumbers()));
			changedContent.append(separator);
		}

		if (!existingBundledFare.getChargeCode().equals(modifiedBundledFare.getChargeCode())) {
			changedContent.append("Charge code changed from ");
			changedContent.append(blankHandler(existingBundledFare.getChargeCode()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getChargeCode()));
			changedContent.append(separator);
		}

		if (!AccelAeroCalculator.isEqual(existingBundledFare.getDefaultBundledFee(), modifiedBundledFare.getDefaultBundledFee())) {
			changedContent.append("Bundled fee changed from ");
			changedContent.append(blankHandler(existingBundledFare.getDefaultBundledFee()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getDefaultBundledFee()));
			changedContent.append(separator);
		}

		if (!existingBundledFare.getStatus().equals(modifiedBundledFare.getStatus())) {
			changedContent.append("Status changed from ");
			changedContent.append(blankHandler(existingBundledFare.getStatus()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getStatus()));
			changedContent.append(separator);
		}

		if (!existingBundledFare.getBundledFarePeriods().equals(existingBundledFare.getBundledFarePeriods())) {
			changedContent.append("Periods changed from ");
			changedContent.append(blankHandler(existingBundledFare.getBundledFarePeriods()));
			changedContent.append(" to ");
			changedContent.append(blankHandler(modifiedBundledFare.getBundledFarePeriods()));
			changedContent.append(separator);
		}

		return changedContent.toString();
	}

	private static String blankHandler(Object object) {	
		if (object == null) {
			return "none";
		} else {
			if ("".equals(object.toString().trim())) {
				return "none";
			} else {
				return object.toString().trim();
			}
		}
	}
}
