package com.isa.thinair.promotion.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;

public class TransferFlightTo implements Serializable {

	private String flightNumber;
	private Date departureDate;
	private int flightSegId;

	// charges per segment ---- Map < charges category @ PromotionsInternalConstants , amount >
	private Map<String, BigDecimal> charges;

	public TransferFlightTo() {
		this.charges = new HashMap<String, BigDecimal>();
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Map<String, BigDecimal> getCharges() {
		return charges;
	}

	public void putCharge(String category, BigDecimal amount) {
		this.charges.put(category, amount);
	}

	@Deprecated
	public BigDecimal getTotalCharges() {
		BigDecimal totalCharges = BigDecimal.ZERO;

		if (charges.containsKey(PromotionsInternalConstants.CHARGES_ALL)) {
			totalCharges = charges.get(PromotionsInternalConstants.CHARGES_ALL);
		} else {
			if (charges.containsKey(PromotionsInternalConstants.CHARGES_SURCHARGES)) {
				totalCharges = totalCharges.add(charges.get(PromotionsInternalConstants.CHARGES_SURCHARGES));
			}
			if (charges.containsKey(PromotionsInternalConstants.CHARGES_TAXES)) {
				totalCharges = totalCharges.add(charges.get(PromotionsInternalConstants.CHARGES_TAXES));
			}
		}

		return totalCharges;
	}

	/**
	 * @return cumulative fares in a direction
	 */
	public BigDecimal getFare() {
		BigDecimal fare = BigDecimal.ZERO;

		if (charges.containsKey(PromotionsInternalConstants.CHARGES_FARES)) {
			fare = charges.get(PromotionsInternalConstants.CHARGES_FARES);
		}

		return fare;
	}

}
