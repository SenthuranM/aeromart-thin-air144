/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * AirScheduleModuleServiceImpl.java
 *
 * ===============================================================================
 *
 *@Virsion $$Id$$
 */
package com.isa.thinair.promotion.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author TF
 * @isa.module.service-interface module-name="promotion" description="module responsible for handling promotions"
 */
public class PromotionModuleService extends DefaultModule {
}
