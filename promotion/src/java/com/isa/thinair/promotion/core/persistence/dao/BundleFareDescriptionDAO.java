package com.isa.thinair.promotion.core.persistence.dao;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTemplate;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTranslationTemplate;

public interface BundleFareDescriptionDAO {

	public void updateBundleFareDescriptionTemplate(BundleFareDescriptionTemplate bundleFareDescriptionTemplate);

	public Page<BundleFareDescriptionTemplate> getBundleFareDescriptionTemplate(String templateName, Integer start,
			Integer recSize);

	public void updateBundleFareDescriptionTranslationTemplate(BundleFareDescriptionTranslationTemplate translationTemplate);
	
	public BundleFareDescriptionTemplate  getBundleFareDescriptionTemplateById(Integer templateId);

}
	