package com.isa.thinair.promotion.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.Campaign;
import com.isa.thinair.promotion.api.model.PromotionCriteria;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.model.PromotionRequestConfig;
import com.isa.thinair.promotion.api.model.PromotionTemplate;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.CampaignTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.PromotionRequestTo;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.PromotionsSearchTo;
import com.isa.thinair.promotion.api.to.constants.PromoRequestParam;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.promotion.core.bl.campaign.CampaignBO;
import com.isa.thinair.promotion.core.bl.promocode.PromoCodeBL;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.persistence.dao.PromotionManagementDao;
import com.isa.thinair.promotion.core.service.bd.PromotionManagementBDImpl;
import com.isa.thinair.promotion.core.service.bd.PromotionManagementDBLocalImpl;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;
import com.isa.thinair.promotion.core.util.PromotionsDataHelper;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;
import com.isa.thinair.promotion.core.utils.CommandNames;

@Stateless
@RemoteBinding(jndiBinding = "PromotionMangement.remote")
@LocalBinding(jndiBinding = "PromotionMangement.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PromotionManagementBean extends PlatformBaseSessionBean implements PromotionManagementBDImpl,
		PromotionManagementDBLocalImpl {

	private static final Log log = LogFactory.getLog(PromotionManagementBean.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce savePromotionTemplate(PromotionTemplateTo promotionTemplateTo) throws ModuleException {

		try {
			PromotionManagementDao promotionManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			DefaultServiceResponse resp = new DefaultServiceResponse(true);

			PromotionTemplate promotionTemplate = PromotionsDataHelper
					.toPromtionTemplate(promotionTemplateTo, getUserPrincipal());
			List<String> overlappedPromotions = promotionManagementDao.getOverlappedPromotions(promotionTemplate);
			if (!overlappedPromotions.isEmpty()) {
				resp.setSuccess(false);
				resp.setResponseCode(ResponceCodes.PROMOTION_ONDS_OVERLAPPED);
				sessionContext.setRollbackOnly();
			}

			if (resp.isSuccess()) {
				promotionManagementDao.savePromotionTemplate(promotionTemplate);
			}

			return resp;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce updatePromotionTemplate(PromotionTemplateTo promotionTemplateTo) throws ModuleException {

		try {
			PromotionManagementDao promotionManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			List<String> overlappedPromotions;
			DefaultServiceResponse resp = new DefaultServiceResponse(true);

			PromotionTemplate promotionTemplate = promotionManagementDao.findEntity(PromotionTemplate.class,
					promotionTemplateTo.getPromotinId());

			promotionTemplate = PromotionsDataHelper.mergeToPromotionTemplate(promotionTemplateTo, promotionTemplate,
					getUserPrincipal());

			overlappedPromotions = promotionManagementDao.getOverlappedPromotions(promotionTemplate);
			if (!overlappedPromotions.isEmpty()) {
				resp.setSuccess(false);
				resp.setResponseCode(ResponceCodes.PROMOTION_ONDS_OVERLAPPED);
			}

			if (resp.isSuccess()) {
				Map<Integer, List<PromotionRequest>> subscribedReqs = PromotionsHandlerFactory.getPromotionManagementDao()
						.getPromotionRequests(null, promotionTemplateTo.getPromotinId(), false,
								PromotionsInternalConstants.PromotionRequestStatus.ACTIVE);

				if (!subscribedReqs.isEmpty()) {
					resp.setSuccess(false);
					resp.setResponseCode(ResponceCodes.PROMOTION_EDIT_FAILED_ACT_REQS);
				}
			}

			if (resp.isSuccess()) {
				promotionManagementDao.updatePromotionTemplate(promotionTemplate);
			} else {
				sessionContext.setRollbackOnly();
			}

			return resp;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page<PromotionTemplateTo> searchPromotionTemplates(PromotionsSearchTo promotionsSearchTo, Integer start, Integer size)
			throws ModuleException {

		try {
			Page<PromotionTemplate> promotionTemplates = PromotionsHandlerFactory.getPromotionManagementDao()
					.searchPromotionTemplates(promotionsSearchTo, start, size);

			List<PromotionTemplateTo> promotionTemplateTos = new ArrayList<PromotionTemplateTo>();

			for (PromotionTemplate promotionTemplate : promotionTemplates.getPageData()) {
				promotionTemplateTos.add(PromotionsDataHelper.toPromotionTemplateTo(promotionTemplate));
			}

			return new Page<PromotionTemplateTo>(promotionTemplates.getTotalNoOfRecords(), promotionTemplates.getStartPosition(),
					promotionTemplates.getTotalNoOfRecordsInSystem(), promotionTemplateTos);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePromotionRequest(PromotionRequest promotionRequest) throws ModuleException {
		PromotionsHandlerFactory.getPromotionManagementDao().savePromotionRequest(promotionRequest);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce savePromotionRequests(List<PromotionRequestTo> promotionRequestTos,
			LCCClientBalancePayment balancePayment, PromotionType promotionType, TrackInfoDTO trackInfoDTO) throws ModuleException {

		try {
			List<PromotionRequest> promotionRequests = new ArrayList<PromotionRequest>();
			PromotionRequest promotionRequest;
			String pnr = null;
			ServiceResponce serviceResponce;
			List<Integer> requestedPnrPaxIds = new ArrayList<Integer>();
			Map<Integer, Collection<ReservationTnx>> paxTransactions;
			Map<Integer, ReservationTnx> promoPaymentTransactions = null;
			Integer promoTemplateId = null;

			// FIXME ---- move to another place -- need to refactor
			if (!promotionRequestTos.isEmpty()) {
				pnr = promotionRequestTos.get(0).getPnr();
				promoTemplateId = promotionRequestTos.get(0).getPromotionId();

				serviceResponce = checkPromotionSubscriptions(pnr, promoTemplateId);
				if (!serviceResponce.isSuccess()) {
					return serviceResponce;
				}
			}

			/*
			 * 
			 * if (balancePayment != null) { LCCClientReservation reservation = loadReservation(pnr);
			 * balancePayment.setReservationStatus(reservation.getStatus()); balancePayment.setGroupPNR(pnr);
			 * balancePayment.setPnrSegments(reservation.getSegments());
			 * balancePayment.setPassengers(reservation.getPassengers());
			 * 
			 * PromotionModuleServiceLocator.getAirproxyReservationBD().balancePayment(balancePayment,
			 * reservation.getVersion(), false,
			 * AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_WEB), null, null);
			 * 
			 * 
			 * paxTransactions =
			 * PromotionModuleServiceLocator.getPassengerBD().getPNRPaxPaymentsAndRefundsTnx(requestedPnrPaxIds);
			 * promoPaymentTransactions = new HashMap<Integer, ReservationTnx>(); Collection<ReservationTnx> colTnx;
			 * List<ReservationTnx> listTnx;
			 * 
			 * for (Integer pnrPaxId : paxTransactions.keySet()) { colTnx = paxTransactions.get(pnrPaxId); listTnx = new
			 * ArrayList<ReservationTnx>(colTnx); Collections.sort(listTnx, new Comparator<ReservationTnx>() { // last
			 * payment first public int compare(ReservationTnx o1, ReservationTnx o2) { return
			 * o2.getDateTime().compareTo(o1.getDateTime()); } });
			 * 
			 * if (!listTnx.isEmpty()) { promoPaymentTransactions.put(pnrPaxId, listTnx.iterator().next()); } } }
			 */

			Pair<List<String>, List<String>> outboundInbounSegs = PromotionsDaoHelper
					.getFilterdSegmentCodes(pnr, promoTemplateId);
			Map<Integer, Integer> segsDirection = PromotionsDaoHelper.getConfirmedDirectionsOfSegs(pnr);
			int direction;
			List<String> segCodes = null;
			Map<Integer, List<String>> segCodesDirectionWise = new HashMap<Integer, List<String>>();

			// direction, counted pnr_pax_id
			Map<Integer, List<Integer>> distinctPaxsInDirection = new HashMap<Integer, List<Integer>>();

			for (PromotionRequestTo promoRequestTo : promotionRequestTos) {

				promotionRequest = PromotionsDataHelper.toPromotionRequest(promoRequestTo, getUserPrincipal());
				promotionRequests.add(promotionRequest);

				Integer currentPaxId = promotionRequest.getReservationPaxId();
				requestedPnrPaxIds.add(currentPaxId);

				direction = segsDirection.get(Integer.valueOf(promoRequestTo.getReservationSegId()));
				segCodes = direction == PromotionsInternalConstants.DIRECTION_OUTBOUND
						? outboundInbounSegs.getLeft()
						: outboundInbounSegs.getRight();

				segCodesDirectionWise.put(direction, segCodes);
				if (distinctPaxsInDirection.containsKey(direction)) {
					List<Integer> currentPaxs = distinctPaxsInDirection.get(direction);

					if (!currentPaxs.contains(currentPaxId)) {
						currentPaxs.add(currentPaxId);
						distinctPaxsInDirection.put(direction, currentPaxs);
						// handlePromotionChargeAndRefunds(promotionRequest, segCodes,
						// promoPaymentTransactions.get(currentPaxId));
						handlePromotionChargeAndRefunds(promotionRequest, segCodes, null);
					}

				} else {
					List<Integer> paxsInOnd = new ArrayList<Integer>();
					paxsInOnd.add(currentPaxId);
					distinctPaxsInDirection.put(direction, paxsInOnd);
					// handlePromotionChargeAndRefunds(promotionRequest, segCodes,
					// promoPaymentTransactions.get(currentPaxId));
					handlePromotionChargeAndRefunds(promotionRequest, segCodes, null);
				}
			}

			if (balancePayment != null) {
				LCCClientReservation reservation = loadReservation(pnr);
				balancePayment.setReservationStatus(reservation.getStatus());
				balancePayment.setGroupPNR(pnr);
				balancePayment.setPnrSegments(reservation.getSegments());
				balancePayment.setPassengers(reservation.getPassengers());

				PromotionModuleServiceLocator.getAirproxyReservationBD().balancePayment(balancePayment, reservation.getVersion(),
						false, AppSysParamsUtil.isFraudCheckEnabled(trackInfoDTO.getOriginChannelId()), null, null, false,
						false);

				paxTransactions = PromotionModuleServiceLocator.getPassengerBD().getPNRPaxPaymentsAndRefundsTnx(
						requestedPnrPaxIds);
				promoPaymentTransactions = new HashMap<Integer, ReservationTnx>();
				Collection<ReservationTnx> colTnx;
				List<ReservationTnx> listTnx;

				for (Integer pnrPaxId : paxTransactions.keySet()) {
					colTnx = paxTransactions.get(pnrPaxId);
					listTnx = new ArrayList<ReservationTnx>(colTnx);
					Collections.sort(listTnx, new Comparator<ReservationTnx>() {
						// last payment first
						public int compare(ReservationTnx o1, ReservationTnx o2) {
							return o2.getDateTime().compareTo(o1.getDateTime());
						}
					});

					if (!listTnx.isEmpty()) {
						promoPaymentTransactions.put(pnrPaxId, listTnx.iterator().next());
					}

				}

				for (PromotionRequest promoReq : promotionRequests) {
					PromotionRequestConfig promoReqConf = new PromotionRequestConfig();
					promoReqConf.setPromotionRequest(promoReq);
					promoReqConf.setPromoReqParam(PromoRequestParam.PromoInternal.PAYMENT_TNX_ID);
					promoReqConf.setParamValue(String.valueOf(promoPaymentTransactions.get(promoReq.getReservationPaxId())
							.getTnxId()));
					promoReqConf.setStatus(PromotionsInternalConstants.STATUS_ACTIVE);
					promoReqConf.setCreatedBy(getUserPrincipal().getName());
					promoReqConf.setCreatedDate(new Date());
					promoReq.getPromotionRequestConfigs().add(promoReqConf);
				}

			}

			PromotionsHandlerFactory.getPromotionManagementDao().savePromotionRequests(promotionRequests);

			auditPromotionRequests(promotionType, pnr, promotionRequests, segsDirection, segCodesDirectionWise);

			sendPromotionSubscriptionNotification(promotionRequestTos);

			return new DefaultServiceResponse(true);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PromotionRequest getApprovedPromotions(PromotionType promotionType, Integer pnrPaxId, Integer pnrSegId)
			throws ModuleException {
		try {
			PromotionManagementDao promotionManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			return promotionManagementDao.getApprovedPromotions(promotionType, pnrPaxId, pnrSegId);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	private void handlePromotionChargeAndRefunds(PromotionRequest promotionRequest, List<String> segCodes, ReservationTnx tnx)
			throws ModuleException {

		PromotionType promotionType = PromotionType.resolvePromotionType(promotionRequest.getPromotionTemplate()
				.getPromotionType().getPromotionId());

		if (log.isDebugEnabled())
			log.debug("Refund and Charge Handling : [pnr pax id  : " + promotionRequest.getReservationPaxId() + "] [pnr Seg Id :"
					+ promotionRequest.getReservationSegmentId() + "]");

		DefaultBaseCommand refundHandler = PromotionsHandlerFactory.getPromotionChargesRefundsHandler(promotionType);
		refundHandler.setParameter(CommandParamNames.PROMO_PROCESS_CATEGORY, CommandParamNames.PROCESS_CHARGES);
		refundHandler.setParameter(CommandParamNames.REQUEST_TYPE, CommandParamNames.REQUEST_EXECUTE);
		refundHandler.setParameter(CommandParamNames.PROMOTION_REQUEST, promotionRequest);
		refundHandler.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODES, segCodes);
		refundHandler.execute();

	}

	private LCCClientReservation loadReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);

		return PromotionModuleServiceLocator.getAirproxyReservationBD().searchResByPNR(pnrModesDTO, null, null);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PromotionTemplateTo loadPromotionTemplateTo(Integer promotionId) throws ModuleException {

		try {
			PromotionTemplate promotionTemplate = PromotionsHandlerFactory.getPromotionManagementDao().findEntity(
					PromotionTemplate.class, promotionId);
			PromotionTemplateTo promoTemplateTo = PromotionsDataHelper.toPromotionTemplateTo(promotionTemplate);
			return promoTemplateTo;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce withdrawPromotionRequest(String pnr, Integer promotionTemplateId) throws ModuleException {
		DefaultServiceResponse serviceResponce = null;
		Map<Integer, List<PromotionRequest>> requestsMapping;

		try {
			PromotionManagementDao promoManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();
			requestsMapping = promoManagementDao.getPromotionRequests(pnr, promotionTemplateId, false,
					PromotionsInternalConstants.PromotionRequestStatus.ACTIVE);

			if (promotionTemplateId != null && !requestsMapping.containsKey(promotionTemplateId)) {
				serviceResponce = new DefaultServiceResponse();
				serviceResponce.setSuccess(false);
				serviceResponce.setResponseCode(ResponceCodes.PROMOTION_UNSUBSCRIBE_PROCESSED_REPEAT);
				return serviceResponce;
			}

			for (Integer promoTemplateId : requestsMapping.keySet()) {
				Pair<List<String>, List<String>> outboundInbounSegs = PromotionsDaoHelper.getFilterdSegmentCodes(pnr,
						promoTemplateId);
				Map<Integer, Integer> segsDirection = PromotionsDaoHelper.getConfirmedDirectionsOfSegs(pnr);
				int direction;
				List<String> segCodes;

				for (PromotionRequest request : requestsMapping.get(promoTemplateId)) {
					Integer resSegId = request.getReservationSegmentId();
					if (segsDirection.containsKey(resSegId)) {
						direction = segsDirection.get(resSegId);
						segCodes = direction == PromotionsInternalConstants.DIRECTION_OUTBOUND
								? outboundInbounSegs.getLeft()
								: outboundInbounSegs.getRight();

						if (segCodes.isEmpty()) {
							continue;
						}

						DefaultBaseCommand refundHandler = PromotionsHandlerFactory
								.getPromotionChargesRefundsHandler(PromotionType.PROMOTION_NEXT_SEAT_FREE);
						refundHandler.setParameter(CommandParamNames.PROMO_PROCESS_CATEGORY, CommandParamNames.PROCESS_REFUNDS);
						refundHandler.setParameter(CommandParamNames.PROMOTION_REQUEST, request);
						refundHandler.setParameter(CommandParamNames.FLIGHT_SEGMENT_CODES, segCodes);
						refundHandler.setParameter(CommandParamNames.REQUEST_TYPE, CommandParamNames.REQUEST_EXECUTE);
						ServiceResponce resp = refundHandler.execute();
					}

					request.setStatus(PromotionsInternalConstants.PromotionRequestStatus.INACTIVE.getStatusCode());
					promoManagementDao.updateEntity(request);
				}
			}

			return new DefaultServiceResponse(true);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce checkPromotionSubscriptions(String pnr, Integer promotionTemplateId) throws ModuleException {
		try {
			PromotionManagementDao promoManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();

			DefaultServiceResponse serviceResponce = null;
			PromotionTemplate promotionTemplate = promoManagementDao.findEntity(PromotionTemplate.class, promotionTemplateId);

			if (promotionTemplate == null || !promotionTemplate.getStatus().equals(PromotionsInternalConstants.STATUS_ACTIVE)) {
				serviceResponce = new DefaultServiceResponse();
				serviceResponce.setSuccess(false);
				serviceResponce.setResponseCode(ResponceCodes.PROMOTION_TEMPLATE_NOT_ACTIVE);
				return serviceResponce;
			} else if (!promoManagementDao.getPromotionRequests(pnr, promotionTemplateId, false,
					PromotionsInternalConstants.PromotionRequestStatus.ACTIVE).isEmpty()) {
				serviceResponce = new DefaultServiceResponse();
				serviceResponce.setSuccess(false);
				serviceResponce.setResponseCode(ResponceCodes.PROMOTION_SUBSRIBED);
				return serviceResponce;
			} else if (!promoManagementDao.getPromotionRequests(pnr, promotionTemplateId, true,
					PromotionsInternalConstants.PromotionRequestStatus.INACTIVE).isEmpty()) {
				serviceResponce = new DefaultServiceResponse();
				serviceResponce.setSuccess(false);
				serviceResponce.setResponseCode(ResponceCodes.PROMOTION_SUBSCRIBE_REPEAT);
				return serviceResponce;
			} else if (!promoManagementDao.getPromotionRequests(pnr, null, true,
					PromotionsInternalConstants.PromotionRequestStatus.INACTIVE).isEmpty()) {
				serviceResponce = new DefaultServiceResponse();
				serviceResponce.setSuccess(false);
				serviceResponce.setResponseCode(ResponceCodes.PROMOTION_SUBSCRIBE_MULTIPLE);
				return serviceResponce;
			}
			// else if (!promoManagementDao.getPaxWisePromotions(pnr).isEmpty()) {
			// serviceResponce = new DefaultServiceResponse();
			// serviceResponce.setSuccess(false);
			// serviceResponce.setResponseCode(ResponceCodes.PROMOTION_SUBSCRIBE_MULTIPLE);
			// return serviceResponce;
			// }

			return new DefaultServiceResponse(true);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendPromotionSubscriptionNotification(List<PromotionRequestTo> promotionRequestTos) throws ModuleException {

		try {
			Collection<String> sentSubscriptionsPNRWise = new HashSet<String>();
			for (PromotionRequestTo promoRequestTO : promotionRequestTos) {
				if (!sentSubscriptionsPNRWise.contains(promoRequestTO.getPnr())) {
					try {
						LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
						pnrModesDTO.setPnr(promoRequestTO.getPnr());
						pnrModesDTO.setLoadSeatingInfo(true);
						pnrModesDTO.setLoadSegView(true);
						pnrModesDTO.setLoadSegViewBookingTypes(true);
						pnrModesDTO.setLoadFares(true);

						Reservation reservation = PromotionModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO,
								null);

						PromotionRequest promotionRequest = PromotionsDataHelper.toPromotionRequest(promoRequestTO,
								getUserPrincipal());

						List<NotificationReceiverTo> receivers = new ArrayList<NotificationReceiverTo>();
						NotificationReceiverTo notificationReceiverTo = new NotificationReceiverTo();
						notificationReceiverTo.setPnr(reservation.getPnr());
						notificationReceiverTo.setReceiverAddr(reservation.getContactInfo().getEmail());
						ReservationContactInfo contactInfo = reservation.getContactInfo();
						notificationReceiverTo.setReceiverName(StringUtil.composeInitCapString(contactInfo.getTitle(),
								contactInfo.getFirstName(), contactInfo.getLastName()));
						notificationReceiverTo.setLanguage("en");
						notificationReceiverTo.setPromotionId(promotionRequest.getPromotionTemplate().getPromotionId());

						receivers.add(notificationReceiverTo);

						DefaultBaseCommand notifyCommand = PromotionsHandlerFactory
								.getPromotionsNotifier(PromotionType.resolvePromotionType(promotionRequest.getPromotionTemplate()
										.getPromotionType().getPromotionId()));
						notifyCommand.setParameter(CommandParamNames.PROMOTION_NOTIFICATION_DESTINATIONS, receivers);
						notifyCommand.setParameter(CommandParamNames.EVENT_TYPE, CommandParamNames.EVENT_PROMO_SUBSCRIBE);
						notifyCommand.execute();
						sentSubscriptionsPNRWise.add(promoRequestTO.getPnr());
					} catch (Exception e) {
						log.error("sendPromotionSubscriptionNotification() failed", e);
					}
				}
			}
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce pickApplicablePromotion(PromoSelectionCriteria promoSelectionCriteria) throws ModuleException {
		try {
			DefaultServiceResponse serviceResponce = new DefaultServiceResponse(false);
			ApplicablePromotionDetailsTO applicablePromoTO = PromoCodeBL.getApplicablePromotionDetails(promoSelectionCriteria);
			if (applicablePromoTO != null) {
				serviceResponce.setSuccess(true);
				serviceResponce.addResponceParam(CommandParamNames.APPLICABLE_PROMOTION_DETAILS, applicablePromoTO);
			}

			return serviceResponce;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String generatePromotionCode() throws ModuleException {
		try {
			PromotionManagementDao promoManagementDao = PromotionsHandlerFactory.getPromotionManagementDao();

			boolean promoCodeExists;
			String promoCode;
			do {
				promoCode = StringUtil.generateRandomString(PromotionCriteriaConstants.SYS_GEN_PROMO_CODE_LENGTH);
				promoCodeExists = promoManagementDao.isPromoCodeExists(promoCode);
			} while (promoCodeExists);

			return promoCode;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendPromoCodeEmailNotification(PromotionCriteria promoDetails, ReservationContactInfo resContact)
			throws ModuleException {
		try {
			PromoCodeBL.sendPromoCodeNotificationEmail(promoDetails, resContact);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean checkPromotionConstraints(Long promoCriteriaId, List<Integer> flightIds, List<Integer> fltSegIds,
			String agentCode, Integer channelCode, int paxCount, int promoAppliedPaxCount) throws ModuleException {
		try {
			return PromotionsHandlerFactory.getPromotionManagementDao().checkPromotionConstraints(promoCriteriaId, flightIds,
					fltSegIds, agentCode, channelCode, paxCount, promoAppliedPaxCount);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	private void auditPromotionRequests(PromotionType promotionType, String pnr, List<PromotionRequest> promotionRequests,
			Map<Integer, Integer> segsDirection, Map<Integer, List<String>> segCodesDirectionWise) throws ModuleException {

		if (promotionType == PromotionType.PROMOTION_FLEXI_DATE) {

			boolean outConfigFound = false;
			boolean inConfigFound = false;

			String[] outConfigs = new String[2];
			String[] inConfigs = new String[2];

			for (PromotionRequest promoRequest : promotionRequests) {
				Integer resSegId = promoRequest.getReservationSegmentId();

				if (!outConfigFound && segsDirection.get(resSegId).equals(PromotionsInternalConstants.DIRECTION_OUTBOUND)) {
					outConfigs = PromotionsDataHelper.getFlexidateRanges(promoRequest);
					outConfigFound = true;

				} else if (!inConfigFound && segsDirection.get(resSegId).equals(PromotionsInternalConstants.DIRECTION_INBOUND)) {
					inConfigs = PromotionsDataHelper.getFlexidateRanges(promoRequest);
					inConfigFound = true;

				}

			}

			if (outConfigFound) {
				auditPromotionSubscription(promotionType, pnr, outConfigs, "outbound", segCodesDirectionWise);
			}

			if (inConfigFound) {
				auditPromotionSubscription(promotionType, pnr, inConfigs, "inbound", segCodesDirectionWise);
			}

		}

	}

	private void auditPromotionSubscription(PromotionType promotionType, String pnr, String[] ranges, String direction,
			Map<Integer, List<String>> segCodesDirectionWise) throws ModuleException {

		Map<String, Object> contentMapForAudit = new HashMap<String, Object>();

		contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.DIRECTION, direction);
		contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.REQUEST_STATUS,
				PromotionsInternalConstants.STATUS_ACTIVE);
		contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.LOWER_BOUND, ranges[0]);
		contentMapForAudit.put(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.UPPER_BOUND, ranges[1]);

		PromotionModuleServiceLocator.getReservationBD().saveAuditPromotionRequestSubcriptions(promotionType, pnr,
				PromotionsInternalConstants.STATUS_ACTIVE, contentMapForAudit, segCodesDirectionWise);

	}

	@Override
	public void savePromotionRequests(Collection<PromotionRequest> promotionRequests) throws ModuleException {
		try {
			PromotionsHandlerFactory.getPromotionManagementDao().savePromotionRequests(promotionRequests);
		} catch (Exception e) {
			throw handleException(e, true, log);
		}

	}

	@Override
	public void removePassengersPromotionRequests(Collection<Integer> pnrPaxIds) throws ModuleException {
		try {
			List<PromotionRequest> promotionRequests = PromotionsHandlerFactory.getPromotionManagementDao()
					.loadPaxPromotionRequests(pnrPaxIds);
			if (promotionRequests != null && !promotionRequests.isEmpty()) {
				PromotionsHandlerFactory.getPromotionManagementDao().deleteEntities(promotionRequests);
			}
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	public List<String> getCustomerEmailsForPromoCodes(CustomerPromoCodeSelectionRequest customerDetails) throws ModuleException {
		try{
			List<String> emailsIDs = PromotionsHandlerFactory.getPromotionManagementDao().getCustomerEmailsForPromoCodes(
					CampaignBO.prepareCustomerSearchForCampaignManagement(customerDetails));
			return emailsIDs;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}		
	}

	@Override
	public void saveCampaignAndEmailPromoCodes(CampaignTO campaignTO)  {
		try {
			Command command = (Command) PromotionModuleServiceLocator.getBean(CommandNames.SAVE_CAMPAIGN_AND_SEND_PROMO_CODES_MACRO);
			command.setParameter(CommandParamNames.CAMPAIGN_DTO, CampaignBO.generateCustomerPromoCodeMap(campaignTO));		
			ServiceResponce serviceResponse = command.execute();
		} catch (Exception ex) {
			log.error("PromotionManagementBean ==> saveCampaignAndEmailPromoCodes()", ex);
		}
		
	}

	@Override
	public void sendEmailsForScheduledCampaigns(Integer campaignID) {
		try {
			Campaign campaign = PromotionsHandlerFactory.getPromotionManagementDao().getCampaign(campaignID);
			CampaignBO.setUpCampaignForEmailingPromoCodes(campaign);
		} catch (Exception ex) {
			log.error("PromotionManagementBean ==> sendEmailsForScheduledCampaigns(Integer campaignID)", ex);
		}
	}

}