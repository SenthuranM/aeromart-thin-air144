package com.isa.thinair.promotion.core.util;

import javax.sql.DataSource;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReprotectedPassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.lccclient.api.service.LCCLoyaltyManagementBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

public class PromotionModuleServiceLocator {

	// EJB Lookups
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static ReservationQueryBD getAirReservationQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}
	
	public static ReprotectedPassengerBD getReprotectedPassengerBD() {
		return (ReprotectedPassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReprotectedPassengerBD.SERVICE_NAME);
	}

	public static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, PromotionManagementBD.SERVICE_NAME);
	}

	public static AirproxyReservationQueryBD getAirproxyReservationQueryBD() {
		return (AirproxyReservationQueryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationQueryBD.SERVICE_NAME);
	}

	public static FlightBD getFlightServiceBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public final static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static SegmentBD getResSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME, PaymentBrokerBD.SERVICE_NAME);
	}

	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirTravelAgentConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirTravelAgentConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static InvoicingBD getInvoicingBD() {
		return (InvoicingBD) lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	// DAO Lookups
	public static FlightInventoryDAO getFlightInventoryDAO() {
		return (FlightInventoryDAO) LookupServiceFactory.getInstance().getModule(AirinventoryConstants.MODULE_NAME)
				.getLocalBean("FlightInventoryDAOImplProxy");
	}

	public static ReservationSegmentDAO getReservationSegmentDAO() {
		return (ReservationSegmentDAO) LookupServiceFactory.getInstance().getModule(AirreservationConstants.MODULE_NAME)
				.getLocalBean("ReservationSegmentDAOProxy");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName,
				LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME).getModuleConfig(),
				PromotionConstants.MODULE_NAME, "promotion.config.dependencymap.invalid");
	}

	private static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getInstance().getModuleConfig(),
				PromotionConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	public static SeatMapDAO getSeatMapDAO() {
		return (SeatMapDAO) LookupServiceFactory.getInstance().getModule(AirinventoryConstants.MODULE_NAME)
				.getLocalBean("SeatMapDAOImplProxy");
	}

	public static AirproxyReservationBD getAirproxyReservationBD() {
		return (AirproxyReservationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyReservationBD.SERVICE_NAME);
	}

	public static FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) PromotionModuleServiceLocator.lookupEJB2ServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	public static ReservationAuxilliaryDAO getReservationAuxilliaryDAO() {
		return (ReservationAuxilliaryDAO) LookupServiceFactory.getInstance().getModule(AirreservationConstants.MODULE_NAME)
				.getLocalBean("ReservationAuxilliaryDAOProxy");
	}

	/**
	 * Get AirproxyPassengerBD
	 * 
	 * @return AirproxyPassengerBD the Proxy Reservation Delegate
	 */
	public final static AirproxyPassengerBD getAirproxyPassengerBD() {
		return (AirproxyPassengerBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyPassengerBD.SERVICE_NAME);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	/**
	 * Returns promotion module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public final static LmsMemberServiceBD getLmsMemberServiceBD() {
		return (LmsMemberServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, LmsMemberServiceBD.SERVICE_NAME);

	}

	public static LCCLoyaltyManagementBD getLCCLoyaltyManagementBD() {
		return (LCCLoyaltyManagementBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCLoyaltyManagementBD.SERVICE_NAME);
	}

	public static LoyaltyExternalConfig getLoyaltyExternalConfig() {
		return (LoyaltyExternalConfig) LookupServiceFactory.getInstance()
				.getBean("isa:base://modules/promotion?id=loyaltyExternalConfig");
	}

	public static LoyaltyCustomerServiceBD getLoyaltyCustomerBD() {
		return (LoyaltyCustomerServiceBD) PromotionModuleServiceLocator.lookupEJB3Service(AircustomerConstants.MODULE_NAME,
				LoyaltyCustomerServiceBD.SERVICE_NAME);
	}

	public static Object getBean(String bName) {
		return LookupServiceFactory.getInstance().getModule(PromotionConstants.MODULE_NAME).getLocalBean(bName);
	}

	public static AirCustomerServiceBD getAirCustomerServiceBD() {
		return (AirCustomerServiceBD) PromotionModuleServiceLocator.lookupEJB3Service(AircustomerConstants.MODULE_NAME,
				AirCustomerServiceBD.SERVICE_NAME);
	}

}
