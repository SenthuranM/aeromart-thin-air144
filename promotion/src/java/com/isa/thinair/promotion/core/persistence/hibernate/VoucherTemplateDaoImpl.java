package com.isa.thinair.promotion.core.persistence.hibernate;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.promotion.api.model.VoucherTemplate;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateRequest;
import com.isa.thinair.promotion.core.persistence.dao.VoucherTemplateDao;

/**
 *
 * @author chanaka
 * 
 * @isa.module.dao-impl dao-name="VoucherTemplateDao"
 */
public class VoucherTemplateDaoImpl extends PlatformHibernateDaoSupport implements VoucherTemplateDao {

	@SuppressWarnings("unchecked")
	public <T, K extends Serializable> T findEntity(Class<T> type, K key) {
		return (T) getSession().get(type, key);
	}

	public <T> void saveEntity(T t) {
		getSession().saveOrUpdate(t);
	}

	public <T> void updateEntity(T t) {
		getSession().update(t);
	}

	public <T> void deleteEntities(Collection<T> entities) {
		deleteAll(entities);
	}

	public <T> void saveEntities(Collection<T> entities) {
		hibernateSaveOrUpdateAll(entities);
	}

	public void saveVoucherTemplate(VoucherTemplate voucherTemplate) {
		getSession().saveOrUpdate(voucherTemplate);

	}

	public void updateVoucherTemplate(VoucherTemplate voucherTemplate) {
		getSession().saveOrUpdate(voucherTemplate);

	}

	public Page<VoucherTemplate> searchVoucherTemplates(VoucherTemplateRequest vouchersSearchTo, Integer start,
			Integer size) {

		boolean getAll = false;

		String dataSec = " select t ";
		String countSec = " select count(*) ";
		StringBuilder sbCommonSec = new StringBuilder();
		sbCommonSec.append(" from  VoucherTemplate t ");
		sbCommonSec.append(" where t.status = :status ");
		if (vouchersSearchTo.getVoucherName() == null || vouchersSearchTo.getVoucherName().equals("")) {
			getAll = true;
		} else {
			sbCommonSec.append(" and t.voucherName = :voucherName ");
		}
		sbCommonSec.append(" ORDER BY t.voucherId DESC ");
		String commonSec = sbCommonSec.toString();

		Query query = getSession().createQuery(dataSec + commonSec);
		query.setParameter("status", vouchersSearchTo.getStatus());
		if (!getAll) {
			query.setParameter("voucherName", vouchersSearchTo.getVoucherName());
		}

		@SuppressWarnings("unchecked")
		List<VoucherTemplate> results = query.setFirstResult(start).setMaxResults(size).list();

		Query queryCount = getSession().createQuery(countSec + commonSec);
		queryCount.setParameter("status", vouchersSearchTo.getStatus());
		if (!getAll) {
			queryCount.setParameter("voucherName", vouchersSearchTo.getVoucherName());
		}

		int count = ((Long) queryCount.uniqueResult()).intValue();

		return new Page<VoucherTemplate>(count, start, start + results.size(), results);
	}

	@Override
	public List<VoucherTemplate> getVoucherInSalesPeriod(Date date) {
		String hql = "SELECT vt FROM VoucherTemplate vt WHERE :date BETWEEN vt.salesFrom AND vt.salesTo AND vt.status=:status ";
		Query templateQuery = getSession().createQuery(hql).setDate("date", date)
				.setString("status", PromotionCriteriaConstants.VoucherTemplateStatus.ACTIVE);
		@SuppressWarnings("unchecked")
		List<VoucherTemplate> voucherList = templateQuery.list();
		return voucherList;
	}

	@Override
	public List<VoucherTemplate> getVouchers() {
		String hql = "SELECT vt FROM VoucherTemplate vt";
		Query templateQuery = getSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<VoucherTemplate> voucherList = templateQuery.list();
		return voucherList;
	}
}
