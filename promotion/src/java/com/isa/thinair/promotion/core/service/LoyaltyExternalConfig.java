package com.isa.thinair.promotion.core.service;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author rumesh
 * @isa.module.config-bean
 */
public class LoyaltyExternalConfig {

	private Map<String, String> locationExtReference;
	private Map<String, String> enrollingChannelExtReference;
	// LMS FTP config data
	private String lmsFtpServerName;
	private String lmsFtpServerUserName;
	private String lmsFtpServerPassword;

	private String productFileRemotePath;
	private String productFilePrefix;

	private String flownFileRemotePath;
	private String flownFilePrefix;
	private String fileExtension;

	private int sponserId;
	private int batchId;

	private List<String> productFileUploadEnableLMSProviders;

	private List<String> flownFileUploadEnableLMSProviders;

	// Flown file related config
	private boolean processErrorRecords;

	public Map<String, String> getLocationExtReference() {
		return locationExtReference;
	}

	public void setLocationExtReference(Map<String, String> locationExtReference) {
		this.locationExtReference = locationExtReference;
	}

	public String getLmsFtpServerName() {
		return lmsFtpServerName;
	}

	public void setLmsFtpServerName(String lmsFtpServerName) {
		this.lmsFtpServerName = lmsFtpServerName;
	}

	public String getLmsFtpServerUserName() {
		return lmsFtpServerUserName;
	}

	public void setLmsFtpServerUserName(String lmsFtpServerUserName) {
		this.lmsFtpServerUserName = lmsFtpServerUserName;
	}

	public String getLmsFtpServerPassword() {
		return lmsFtpServerPassword;
	}

	public void setLmsFtpServerPassword(String lmsFtpServerPassword) {
		this.lmsFtpServerPassword = lmsFtpServerPassword;
	}

	public String getProductFileRemotePath() {
		return productFileRemotePath;
	}

	public void setProductFileRemotePath(String productFileRemotePath) {
		this.productFileRemotePath = productFileRemotePath;
	}

	public String getProductFilePrefix() {
		return productFilePrefix;
	}

	public void setProductFilePrefix(String productFilePrefix) {
		this.productFilePrefix = productFilePrefix;
	}

	public String getFlownFileRemotePath() {
		return flownFileRemotePath;
	}

	public void setFlownFileRemotePath(String flownFileRemotePath) {
		this.flownFileRemotePath = flownFileRemotePath;
	}

	public String getFlownFilePrefix() {
		return flownFilePrefix;
	}

	public void setFlownFilePrefix(String flownFilePrefix) {
		this.flownFilePrefix = flownFilePrefix;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public boolean isProcessErrorRecords() {
		return processErrorRecords;
	}

	public void setProcessErrorRecords(boolean processErrorRecords) {
		this.processErrorRecords = processErrorRecords;
	}

	public int getSponserId() {
		return sponserId;
	}

	public void setSponserId(int sponserId) {
		this.sponserId = sponserId;
	}

	public int getBatchId() {
		return batchId;
	}

	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}

	public Map<String, String> getEnrollingChannelExtReference() {
		return enrollingChannelExtReference;
	}

	public void setEnrollingChannelExtReference(Map<String, String> enrollingChannelExtReference) {
		this.enrollingChannelExtReference = enrollingChannelExtReference;
	}

	public List<String> getProductFileUploadEnableLMSProviders() {
		return productFileUploadEnableLMSProviders;
	}

	public void setProductFileUploadEnableLMSProviders(List<String> productFileUploadEnableLMSProviders) {
		this.productFileUploadEnableLMSProviders = productFileUploadEnableLMSProviders;
	}

	public List<String> getFlownFileUploadEnableLMSProviders() {
		return flownFileUploadEnableLMSProviders;
	}

	public void setFlownFileUploadEnableLMSProviders(List<String> flownFileUploadEnableLMSProviders) {
		this.flownFileUploadEnableLMSProviders = flownFileUploadEnableLMSProviders;
	}

}
