package com.isa.thinair.promotion.core.bl.common;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

public abstract class PromotionApprover extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(PromotionApprover.class);
	
	private DefaultServiceResponse requestScopeResp;
	private DefaultServiceResponse summarizedResp;

	private Object promotionRequest;

	public ServiceResponce execute() throws ModuleException {
		List<?> promotionRequests = getPromotionRequests();
		try {
			summarizedResp = new DefaultServiceResponse(true);
			promotionsApprovalPreProcess();
			for (Object promoRequest : promotionRequests) {
				promotionRequest = promoRequest;

				requestScopeResp = new DefaultServiceResponse();
				promotionRequestPreProcess();
				if (requestScopeResp.isSuccess()) {
					approvePromotionRequest();
				}
				promotionRequestPostProcess();
				promotionRequest = null;
			}
			promotionsApprovalPostProcess();
		} catch (Exception e) {
			summarizedResp = new DefaultServiceResponse(false);
			log.error("execute ", e);
		}

		return summarizedResp;
	}

	protected abstract List<?> getPromotionRequests() throws ModuleException;
	
	protected void promotionsApprovalPreProcess() throws ModuleException {
	}

	protected void promotionRequestPreProcess() throws ModuleException {
	}

	protected abstract ServiceResponce approvePromotionRequest() throws ModuleException;

	protected abstract void promotionRequestPostProcess() throws ModuleException;
	
	protected void promotionsApprovalPostProcess() throws ModuleException{
		
	}

	public DefaultServiceResponse getRequestScopeResp() {
		return requestScopeResp;
	}

	public DefaultServiceResponse getSummarizedResp() {
		return summarizedResp;
	}

	public Object getPromotionRequest() {
		return promotionRequest;
	}

	public PromotionAdministrationBD getAdministrationBD() {
		return PromotionsHandlerFactory.getPromotionAdministrationBD();
	}

}
