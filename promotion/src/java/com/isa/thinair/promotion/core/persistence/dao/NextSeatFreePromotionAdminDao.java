package com.isa.thinair.promotion.core.persistence.dao;

import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.promotion.api.to.PromotionsApprovalSummaryView;

public interface NextSeatFreePromotionAdminDao extends PromotionAdministrationDao {

	public List<Integer> getFlightSegIdsForScheduler();

	public Page<PromotionsApprovalSummaryView> getPromotionsApprovalSummaryView(String flightNo, String status, Date startDate, Date endDate,
			Integer startRec, Integer size);
}
