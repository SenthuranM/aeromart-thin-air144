package com.isa.thinair.promotion.core.persistence.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTemplate;
import com.isa.thinair.promotion.api.model.BundleFareDescriptionTranslationTemplate;
import com.isa.thinair.promotion.core.persistence.dao.BundleFareDescriptionDAO;

/**	
 * @isa.module.dao-impl dao-name="BundleFareDescTemplateDAO"
 */
public class BundleFareDescriptionDAOImpl extends PlatformBaseHibernateDaoSupport implements BundleFareDescriptionDAO {

	@Override
	public void updateBundleFareDescriptionTemplate(BundleFareDescriptionTemplate bundleFareDescriptionTemplate) {
		hibernateSaveOrUpdate(bundleFareDescriptionTemplate);
	}

	@Override
	public Page<BundleFareDescriptionTemplate> getBundleFareDescriptionTemplate(String templateName, Integer start,
			Integer recSize) {
		Criteria query = getSession().createCriteria(BundleFareDescriptionTemplate.class);
		query.addOrder(Order.asc("templateID"));
		Criteria queryForSize = getSession().createCriteria(BundleFareDescriptionTemplate.class);

		if (!StringUtil.isNullOrEmpty(templateName)) {
			query.add(Restrictions.like("templateName", templateName, MatchMode.ANYWHERE).ignoreCase());
			queryForSize.add(Restrictions.like("templateName", templateName, MatchMode.ANYWHERE).ignoreCase());
		}
		@SuppressWarnings("unchecked") List<BundleFareDescriptionTemplate> resultList = query.setFirstResult(start)
				.setMaxResults(recSize).list();

		Long totalRecords = (Long) queryForSize.setProjection(Projections.rowCount()).uniqueResult();

		return new Page<>(totalRecords.intValue(), start, start + recSize, resultList);
	}

	@Override
	public BundleFareDescriptionTemplate getBundleFareDescriptionTemplateById(Integer templateId) {
		BundleFareDescriptionTemplate retrieveTemplate = null;
		Criteria criteria = getSession().createCriteria(BundleFareDescriptionTemplate.class);
		criteria.add(Restrictions.eq("templateID", templateId));
		if(!criteria.list().isEmpty()) {
			retrieveTemplate = (BundleFareDescriptionTemplate) criteria.list().get(0);
			return retrieveTemplate;
		}
		return retrieveTemplate;
	}

	@Override
	public void updateBundleFareDescriptionTranslationTemplate(
			BundleFareDescriptionTranslationTemplate translationTemplate) {
		hibernateSaveOrUpdate(translationTemplate);
		
	}
}
