package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;

@Remote
public interface PromotionCriteriaAdminBDRemote extends PromotionCriteriaAdminBD {

}
