package com.isa.thinair.promotion.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;

public class NotificationReceiverTo implements Serializable {

	private PromotionType promotionType;
	private String receiverAddr;
	private String receiverName;
	private String language;
	private String pnr;
	private int promotionId;
	private Set<String> promotionOnds;
	private Map<Integer, String> reservationSegIds;
	private Map<Integer, String> passengerPromoStatus;
	private String journeyOnds;
	private boolean seatInventoryDefined = false;
	private Date departureTime;

	private Boolean applicableNotification = null;
	private String matchedPromoOnd;
	// lesser index is a better match -------- 0 = A/B , 1 = A/ALL , 2 ...
	private int matchedPromoOndIndex;
	private boolean discardedNotification = false;

	public NotificationReceiverTo() {
		promotionOnds = new HashSet<String>();
		reservationSegIds = new LinkedHashMap<Integer, String>();
	}

	public PromotionType getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}

	public String getReceiverAddr() {
		return receiverAddr;
	}

	public void setReceiverAddr(String receiverAddr) {
		this.receiverAddr = receiverAddr;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public Set<String> getPromotionOnds() {
		return promotionOnds;
	}

	public void addPromotionOnd(String ond) {
		this.promotionOnds.add(ond);
	}

	public String getJourneyOnds() {
		return journeyOnds;
	}

	public void setJourneyOnds(String journeyOnds) {
		this.journeyOnds = journeyOnds;
	}

	public Map<Integer, String> getReservationSegIds() {
		return reservationSegIds;
	}

	public void addReservationSegId(Integer reservationSegId, String segmentCode) {
		this.reservationSegIds.put(reservationSegId, segmentCode);
	}

	public String getNotificationKey() {
		return pnr + PromotionsInternalConstants.COMMON_DELIM + promotionId;
	}

	public Map<Integer, String> getPassengerPromoStatus() {
		return passengerPromoStatus;
	}

	public void setPassengerPromoStatus(Map<Integer, String> passengerPromoStatus) {
		this.passengerPromoStatus = passengerPromoStatus;
	}

	public boolean isSeatInventoryDefined() {
		return seatInventoryDefined;
	}

	public void setSeatInventoryDefined(boolean seatInventoryDefined) {
		this.seatInventoryDefined = seatInventoryDefined;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public String getDestination() {
		Object[] segCodes = reservationSegIds.values().toArray();
		String[] airportsInLastSeg = ((String) segCodes[segCodes.length - 1]).split("/");
		return airportsInLastSeg[airportsInLastSeg.length - 1];
	}

	public Boolean isApplicableNotification() {
		if (applicableNotification == null) {
			checkApplicability();
		}
		return applicableNotification;
	}

	public void setApplicableNotification(Boolean applicableNotification) {
		this.applicableNotification = applicableNotification;
	}

	public String getMatchedPromoOnd() {
		return matchedPromoOnd;
	}

	public void setMatchedPromoOnd(String matchedPromoOnd) {
		this.matchedPromoOnd = matchedPromoOnd;
	}

	public boolean isDiscardedNotification() {
		return discardedNotification;
	}

	public void setDiscardedNotification(boolean discardedNotification) {
		this.discardedNotification = discardedNotification;
	}

	public boolean systemRequirementsViolated() {
		if (promotionType == PromotionType.PROMOTION_NEXT_SEAT_FREE) {
			return !isSeatInventoryDefined();
		}

		return false;
	}

	public void checkApplicability() {
		Pair<String, String> outboundInboundOnd = PromotionsUtils.getOutboundAndInboundOndCodes(reservationSegIds.values());
		Pair<String, String> originAndDest;
		List<String> possibleOnds = new ArrayList<String>();
		boolean promoApplicable = false;
		String matchedOnd = null;
		int matchedIndex = Integer.MAX_VALUE;

		if (promotionType == PromotionType.PROMOTION_NEXT_SEAT_FREE && !isSeatInventoryDefined()) {
			applicableNotification = false;
		}

		if ((outboundInboundOnd.getRight() == null && reservationSegIds.size() > 1)
				|| (outboundInboundOnd.getRight() != null && reservationSegIds.size() > 2)) {
			// Connection Flights
			possibleOnds.add(outboundInboundOnd.getLeft());

			if (outboundInboundOnd.getRight() != null) {
				possibleOnds.add(outboundInboundOnd.getRight());
			}

			possibleOnds.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);
		} else {
			// Single Segment
			originAndDest = PromotionsUtils.getOriginAndDestinationAirports(outboundInboundOnd.getLeft());
			possibleOnds.add(outboundInboundOnd.getLeft());
			possibleOnds.add(originAndDest.getLeft() + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);
			possibleOnds.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ originAndDest.getRight());

			if (outboundInboundOnd.getRight() != null) {
				possibleOnds.add(outboundInboundOnd.getRight());

				possibleOnds.add(originAndDest.getRight() + PromotionsInternalConstants.OND_DELIM
						+ PromotionsInternalConstants.OND_ALL);
				possibleOnds.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
						+ originAndDest.getLeft());
			}

			possibleOnds.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);

		}

		for (int a = 0; a < possibleOnds.size(); a++) {
			if (promotionOnds.contains(possibleOnds.get(a))) {
				promoApplicable = true;
				matchedOnd = possibleOnds.get(a);
				matchedIndex = a;
			}
		}

		applicableNotification = promoApplicable;
		matchedPromoOnd = matchedOnd;
		matchedPromoOndIndex = matchedIndex;

	}

	public boolean isBetterMatch(NotificationReceiverTo o) {

		isApplicableNotification();
		if ((!o.isApplicableNotification()) || (!pnr.equals(o.pnr))) {
			return false;
		}

		return matchedPromoOndIndex - o.matchedPromoOndIndex > 0 ? true : false;
	}
}
