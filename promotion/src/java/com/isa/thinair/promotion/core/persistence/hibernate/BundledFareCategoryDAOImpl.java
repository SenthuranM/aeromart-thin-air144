package com.isa.thinair.promotion.core.persistence.hibernate;

import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.BundledFareCategory;
import com.isa.thinair.promotion.api.to.BundleCategorySearch;
import com.isa.thinair.promotion.core.persistence.dao.BundledFareCategoryDAO;
import org.hibernate.Query;

import java.util.ArrayList;
import java.util.List;


/**
 * @isa.module.dao-impl dao-name="BundledFareCategoryDAO"
 */
public class BundledFareCategoryDAOImpl  extends PlatformHibernateDaoSupport implements BundledFareCategoryDAO {

    @Override
    public BundleFareCategoryDTO saveBundleFareCategory(BundleFareCategoryDTO category, UserPrincipal up) throws CommonsDataAccessException {

        BundledFareCategory categoryModel = null;

        if(category.getBundledCategoryId() != null) {

            categoryModel = get(BundledFareCategory.class, category.getBundledCategoryId());
            categoryModel.setPriority(category.getPriority());
            categoryModel.setStatus(category.getStatus());
            categoryModel.setVersion(category.getVersion());

        } else {

            categoryModel = new BundledFareCategory();
            categoryModel.setPriority(category.getPriority());
            categoryModel.setStatus(category.getStatus());
            categoryModel.setUserDetails(up);
        }

        getSession().saveOrUpdate(categoryModel);

        category.setBundledCategoryId(categoryModel.getBundledFareCategoryId());
        return category;
    }

    @Override
    public BundledFareCategory getCategory(Integer id)  throws CommonsDataAccessException {
        return get(BundledFareCategory.class, id);
    }

    @Override
    public Page<BundleFareCategoryDTO> searchCategories(BundleCategorySearch bundleSearch, List<Integer> ids) throws CommonsDataAccessException {

        int startPosition = (bundleSearch.getPageNumber() -1) * bundleSearch.getPageSize();
        int endPosition = startPosition + bundleSearch.getPageSize();

        StringBuilder countQueryStr = new StringBuilder("SELECT count(*) ");
        StringBuilder searchQueryStr = new StringBuilder("FROM BundledFareCategory bundledFareCategory WHERE bundledFareCategory.status != 'DEL' ");
        StringBuilder sortingCriteria = new StringBuilder(" ORDER BY bundledFareCategory.priority");


        if (ids != null && ids.size() !=0) {
            searchQueryStr.append(" and bundledFareCategory.bundledFareCategoryId in :ids ");
        }

        if (!StringUtil.isNullOrEmpty(bundleSearch.getStatus())) {
            searchQueryStr.append(" and bundledFareCategory.status = :status ");
        }

        if (bundleSearch.getPriority() != 0) {
            searchQueryStr.append(" and bundledFareCategory.priority = :priority ");
        }

        searchQueryStr.append(sortingCriteria);

        Query searchQuery = getSession().createQuery(searchQueryStr.toString());
        Query countQuery = getSession().createQuery(countQueryStr.append(searchQueryStr).toString());

        if (ids != null && ids.size() !=0) {
            searchQuery.setParameterList("ids", ids);
            countQuery.setParameterList("ids", ids);
        }

        if (!StringUtil.isNullOrEmpty(bundleSearch.getStatus())) {
            searchQuery.setString("status", bundleSearch.getStatus());
            countQuery.setString("status", bundleSearch.getStatus());
        }

        if (bundleSearch.getPriority() != 0) {
            searchQuery.setParameter("priority", bundleSearch.getPriority());
            countQuery.setParameter("priority", bundleSearch.getPriority());
        }

        List<BundledFareCategory> resultList = searchQuery.setFirstResult(startPosition).setMaxResults(endPosition).list();

        List<BundleFareCategoryDTO> dtoList = new ArrayList<>();
        if(resultList != null) {
            for (BundledFareCategory category: resultList) {

                BundleFareCategoryDTO dto = new BundleFareCategoryDTO();
                dto.setBundledCategoryId(category.getBundledFareCategoryId());
                dto.setStatus(category.getStatus());
                dto.setPriority(category.getPriority());
                dto.setVersion(category.getVersion());

                dtoList.add(dto);
            }
        }

        Long totalRecords = (Long) countQuery.uniqueResult();

        return new Page<>(totalRecords.intValue(), startPosition, endPosition, dtoList);
    }

    @Override
    public boolean deleteCategory(BundleFareCategoryDTO category) throws CommonsDataAccessException {

        BundledFareCategory categoryModel = get(BundledFareCategory.class, category.getBundledCategoryId());
        categoryModel.setStatus(BundledFareCategory.DELETED);
        getSession().saveOrUpdate(categoryModel);

        return true;
    }

    @Override
    public List<String> getAllCategoryPriorities() throws CommonsDataAccessException {

        StringBuilder hql = new StringBuilder("select distinct bcat.priority from BundledFareCategory as bcat where bcat.status != 'DEL'");
        return getSession().createQuery(hql.toString()).list();
    }

}
