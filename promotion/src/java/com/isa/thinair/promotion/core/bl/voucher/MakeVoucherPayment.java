package com.isa.thinair.promotion.core.bl.voucher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * @author chethiya
 * 
 * @isa.module.command name="makeVoucherPayment"
 * 
 */

public class MakeVoucherPayment extends DefaultBaseCommand {
	@Override
	public ServiceResponce execute() throws ModuleException {
		@SuppressWarnings("unchecked")
		List<VoucherDTO> voucherDTOs = (List<VoucherDTO>) getParameter(CommandParamNames.VOUCHER_LIST_DETAILS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) getParameter(CommandParamNames.CREDENTIALS_DTO);
		String totalInBase = (String) getParameter(CommandParamNames.TOTAL_BASE);
		
		//As the required details are common for all the voucherPaymentDTOs 
		VoucherPaymentDTO voucherPaymentDTO = voucherDTOs.get(0).getVoucherPaymentDTO();
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// Checking params
		List<CardDetailConfigDTO> cardDetailConfigData = new ArrayList<CardDetailConfigDTO>();
		PaymentBrokerBD paymentBrokerBD = PromotionModuleServiceLocator.getPaymentBrokerBD();
		Collection<Integer> colAgentSaleTxId = new ArrayList<Integer>();

		if (voucherPaymentDTO.getPaymentMethod().equals(PromotionCriteriaConstants.VoucherPaymentType.CASH_PAYMENT)) {
			// Nothing to do here for Cash Payments
		} else if (voucherPaymentDTO.getPaymentMethod().equals(PromotionCriteriaConstants.VoucherPaymentType.ONACCOUNT_PAYMENT)) {

			TravelAgentFinanceBD travelAgentFinanceBD = (TravelAgentFinanceBD) PromotionModuleServiceLocator
					.getTravelAgentFinanceBD();

			TravelAgentBD travelAgentBD = (TravelAgentBD) PromotionModuleServiceLocator.getTravelAgentBD();
			Agent travelAgent = travelAgentBD.getAgent(voucherDTOs.get(0).getVoucherPaymentDTO().getAgentCode());
			String currencyCode = travelAgent.getCurrencyCode();
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			Integer agentSaleTxId = null;
			PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currencyCode, exchangeRate);
			payCurrencyDTO.setTotalPayCurrencyAmount(VoucherBO.getPayCurrencyAmount(currencyCode,
					AppSysParamsUtil.getBaseCurrency(), new BigDecimal(totalInBase)));
			AgentCreditInfo agentCreditInfo = new AgentCreditInfo(voucherPaymentDTO.getAgentCode(),
					AccelAeroCalculator.scaleValueDefault(new BigDecimal(totalInBase)), payCurrencyDTO, new PaymentReferenceTO(),
					AppSysParamsUtil.getDefaultCarrierCode(), null, null);
			AgentTransaction agentTransaction = travelAgentFinanceBD.recordSale(agentCreditInfo.getAgentCode(), agentCreditInfo
					.getTotalAmount(), voucherDTOs.get(0).getVoucherGroupId(), voucherDTOs.get(0).getVoucherGroupId(),
					agentCreditInfo.getPayCurrencyDTO(), agentCreditInfo.getPaymentReferenceTO(),
					PromotionCriteriaConstants.ProductType.GIFT_VOUCHER, null);
			agentSaleTxId = agentTransaction.getTnxId();
			for (VoucherDTO voucherDTO : voucherDTOs) {
				voucherDTO.getVoucherPaymentDTO().setAgentTransactionID(agentTransaction.getTnxId());
			}

			colAgentSaleTxId.add(agentSaleTxId);
			String agentCode = agentCreditInfo.getAgentCode();
			VoucherBO.createInvoices(voucherDTOs.get(0).getVoucherGroupId(), agentCode, colAgentSaleTxId, null);
		} else if (voucherPaymentDTO.getPaymentMethod().equals(PromotionCriteriaConstants.VoucherPaymentType.CREDIT_CARD_PAYMENT)) {

			// TODO EuroCommerce Fraud Checking to be done on request
			Integer paymentGatewayID = voucherPaymentDTO.getIpgId();
			if (paymentGatewayID != null) {
				cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(paymentGatewayID.toString());
			}
			Collection<Integer> colTnxIds = new ArrayList<Integer>();
			colTnxIds.add(voucherPaymentDTO.getTempPaymentID());
			VoucherBO.voucherCCPayment(voucherDTOs, colTnxIds, credentialsDTO, cardDetailConfigData, totalInBase);

		} else {

		}
		// constructing response
		responce.addResponceParam(CommandParamNames.VOUCHER_LIST_DETAILS, voucherDTOs);

		// return command response
		return responce;
	}

}
