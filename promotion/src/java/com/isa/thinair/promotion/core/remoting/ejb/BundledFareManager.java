package com.isa.thinair.promotion.core.remoting.ejb;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.promotion.api.model.BundledFarePeriod;
import com.isa.thinair.promotion.api.model.BundledFarePeriodImage;
import com.isa.thinair.promotion.api.to.BundleCategorySearch;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareConfigurationTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplayDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplaySettingsDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareSearchCriteriaTO;
import com.isa.thinair.promotion.api.utils.BundleFareDescriptionTemplateUtils;
import com.isa.thinair.promotion.core.audit.AuditPromotion;
import com.isa.thinair.promotion.core.service.bd.BunldedFareBDLocal;
import com.isa.thinair.promotion.core.service.bd.BunldedFareBDRemote;
import com.isa.thinair.promotion.core.util.BundleCategoryUtil;
import com.isa.thinair.promotion.core.util.BundledFareHelper;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsHandlerFactory;

/**
 * BundledFareBD Implementation
 *
 * @author rumesh
 * 
 */
@Stateless
@RemoteBinding(jndiBinding = "BundledFareManager.remote")
@LocalBinding(jndiBinding = "BundledFareManager.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class BundledFareManager extends PlatformBaseSessionBean implements BunldedFareBDLocal, BunldedFareBDRemote {
	private static Log log = LogFactory.getLog(BundledFareManager.class);

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page<BundledFareConfigurationTO> searchBundledFareConfiguration(BundledFareSearchCriteriaTO searchCriteriaTO,
			Integer startPosition, Integer recSize) throws ModuleException {
		try {
			Page<BundledFare> page = PromotionsHandlerFactory.getBundledfareDAO().searchBundledFareConfiguration(
					searchCriteriaTO, startPosition, recSize);

			Page<BundledFareConfigurationTO> pageTO = BundledFareHelper.transformToBundledFarePageDTO(page);
			return pageTO;
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	public BundledFare getBundledFareById(Integer bundledFareId) throws ModuleException {
		return PromotionsHandlerFactory.getBundledfareDAO().getBundledFare(bundledFareId);
	}

	@Override
	public BundledFareDTO getBundledFareDtoByBundlePeriodId(Integer bundledFarePeriodId) throws ModuleException {
		try {
			BundledFare bundleFare = PromotionsHandlerFactory.getBundledfareDAO().getApplicableBundleFare(bundledFarePeriodId);
			BundledFareHelper.removeUnMatchingBundleServicePeriods(bundleFare, bundledFarePeriodId);
			return BundledFareHelper.transformToBundledFareDTO(bundleFare);
		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	public BundledFareLiteDTO getBundledFareLiteDTO(Integer bundledFarePeriodId, String preferredLanguage) throws ModuleException {
		try {
			BundledFare existingBundledFare = PromotionsHandlerFactory.getBundledfareDAO().getApplicableBundleFare(
					bundledFarePeriodId);
			BundledFareHelper.removeUnMatchingBundleServicePeriods(existingBundledFare, bundledFarePeriodId);

			Collection<String> translationKeys = BundledFareHelper.getSelectedPeriodI18nMessageKeys(existingBundledFare);
			Map<String, Map<String, String>> translatedMessages = null;
			if (translationKeys != null && !translationKeys.isEmpty()) {
				translatedMessages = PromotionModuleServiceLocator.getCommonMasterBD().getTranslatedMessagesForKeys(
						translationKeys);
			}

			return BundledFareHelper.transformToLiteDTO(existingBundledFare, translatedMessages, preferredLanguage, null);

		} catch (Exception e) {
			throw handleException(e, false, log);
		}
	}

	@Override
	public List<BundledFareDTO> getBundledFareDTOsByBundlePeriodIds(Collection<Integer> bundledFarePeriodIds)
			throws ModuleException {
		List<BundledFareDTO> bundleFareDTOs = new ArrayList<BundledFareDTO>();
		for (Integer bundledFarePeriodId : bundledFarePeriodIds) {
			BundledFareDTO bundledFareDTO = getBundledFareDtoByBundlePeriodId(bundledFarePeriodId);
			if (bundledFareDTO != null) {
				bundleFareDTOs.add(bundledFareDTO);
			}
		}
		return bundleFareDTOs;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer addBundledFareConfiguration(BundledFareConfigurationTO bundledFareTO) throws ModuleException {
		try {
			validateBundleFareDescription(bundledFareTO.getBundledFareName());
			BundledFare bundledFare = BundledFareHelper.transformDTOtoModel(bundledFareTO);
			bundledFare.setUserDetails(getUserPrincipal());
			Integer bundledFareId = PromotionsHandlerFactory.getBundledfareDAO().saveBundledFareConfiguration(bundledFare);
			AuditPromotion.doAudit(bundledFare, null, getUserPrincipal());
			return bundledFareId;
		} catch (Exception e) {
			throw handleException(e, true, log);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer updateBundledFareConfiguration(BundledFareConfigurationTO bundledFareTO) throws ModuleException {
		Integer targetBundledFareId = null;
		try {

			BundledFare existingBundledFare = PromotionsHandlerFactory.getBundledfareDAO().getBundledFare(bundledFareTO.getId());
			if (existingBundledFare == null) {
				throw handleException(new ModuleException("promotion.bundled.fare.not.exists"), true, log);
			}

			Set<Integer> bundledFarePeriodIds = null;
			Set<BundledFarePeriod> bundledFarePeriods = existingBundledFare.getBundledFarePeriods();
			if (bundledFarePeriods != null && !bundledFarePeriods.isEmpty()) {
				bundledFarePeriodIds = new HashSet<Integer>();
				for (BundledFarePeriod bundledFarePeriod : bundledFarePeriods) {
					bundledFarePeriodIds.add(bundledFarePeriod.getBundleFarePeriodId());
				}
			}

			if (bundledFarePeriodIds != null && !bundledFarePeriodIds.isEmpty()) {
				// In Edit mode,
				// always no matter it is used in the reservations or not, make the existing one INA
				validateBundleFareDescription(bundledFareTO.getBundledFareName());

				existingBundledFare.setStatus(BundledFare.INACTIVE);
				PromotionsHandlerFactory.getBundledfareDAO().updateBundledFareConfiguration(existingBundledFare);
				AuditPromotion.doAudit(existingBundledFare, null, getUserPrincipal());
				Integer sourceBundledId = existingBundledFare.getBundledFareId();

				// create a new one
				BundledFare bundledFare = BundledFareHelper.transformDTOtoModel(bundledFareTO);
				bundledFare.setSourceBundledFareId(sourceBundledId);
				bundledFare.setVersion(-1);
				bundledFare.setUserDetails(getUserPrincipal());

				targetBundledFareId = PromotionsHandlerFactory.getBundledfareDAO().saveBundledFareConfiguration(bundledFare);
				AuditPromotion.doAudit(bundledFare, null, getUserPrincipal());

			}

		} catch (Exception e) {
			throw handleException(e, true, log);
		}

		return targetBundledFareId;
	}

	@Override
	public List<String> getAllCategoryPriorities() {
		return PromotionsHandlerFactory.getBundledFareCategoryDAO().getAllCategoryPriorities();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page<BundleFareCategoryDTO> searchBundledFareCategories(BundleCategorySearch bundleSearch) throws ModuleException {
		Page<BundleFareCategoryDTO> categories = PromotionsHandlerFactory.getBundledFareCategoryDAO().searchCategories(
				bundleSearch, null);

		String defaultLanguage = AppSysParamsUtil.getSystemDefaultLanguage();
		Collection<BundleFareCategoryDTO> dtoList = categories.getPageData();
		if (dtoList != null) {
			for (BundleFareCategoryDTO dto : dtoList) {
				populateBundleCategoryTranslations(dto, defaultLanguage);
			}
		}

		return categories;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public BundleFareCategoryDTO createOrUpdateCategory(BundleFareCategoryDTO category) throws ModuleException {

		boolean isNewCategory = category.getBundledCategoryId() == null;
		BundleFareCategoryDTO updateCategory = PromotionsHandlerFactory.getBundledFareCategoryDAO().saveBundleFareCategory(
				category, getUserPrincipal());
		BundleCategoryUtil.saveBundleCategoryTranslations(updateCategory);
		return updateCategory;
	}

	private void populateBundleCategoryTranslations(BundleFareCategoryDTO categoryDTO, String defaultLanguage)
			throws ModuleException {

		categoryDTO.setTranslations(new HashMap<>());

		String bundleCatNameKey = I18nMessageCategory.BUNDLE_CATEGORY_NAME.toString() + categoryDTO.getBundledCategoryId();
		String bundleCatDescKey = I18nMessageCategory.BUNDLE_CATEGORY_DESC.toString() + categoryDTO.getBundledCategoryId();

		List<String> allI18nKeys = new ArrayList<>();
		allI18nKeys.add(bundleCatNameKey);
		allI18nKeys.add(bundleCatDescKey);

		Map<String, Map<String, String>> translations = PromotionModuleServiceLocator.getCommonMasterBD()
				.getTranslatedMessagesForKeys(allI18nKeys);
		Map<String, String> bundleCatNameTranslations = translations.get(bundleCatNameKey);

		if (bundleCatNameTranslations != null) {

			for (String language : bundleCatNameTranslations.keySet()) {

				String bundleCatNameTranslation = bundleCatNameTranslations.get(language);
				String buncleCatDescTranslation = null;

				Map<String, String> bundleDescNameTranslations = translations.get(bundleCatDescKey);
				if (bundleDescNameTranslations != null) {
					for (String desLanguage : bundleDescNameTranslations.keySet()) {
						if (desLanguage.equals(language)) {
							buncleCatDescTranslation = bundleDescNameTranslations.get(desLanguage);
							break;
						}
					}
				}

				if (bundleCatNameTranslation != null) {

					categoryDTO.getTranslations().put(
							language,
							new BundleFareCategoryDTO.BundledFareCategoryTranslation(bundleCatNameTranslation,
									buncleCatDescTranslation));

					if (language.equals(defaultLanguage)) {
						categoryDTO.setLanguage(defaultLanguage);
						categoryDTO.setCategoryName(bundleCatNameTranslation);
						categoryDTO.setDescription(buncleCatDescTranslation);
					}
				}

			}
		}
	}

	@Override
	public boolean deleteCategory(BundleFareCategoryDTO category) throws ModuleException {
		return PromotionsHandlerFactory.getBundledFareCategoryDAO().deleteCategory(category);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<BundledFareLiteDTO>
			getApplicableBundledFares(ApplicableBundledFareSelectionCriteria bundledFareSelectionCriteria) {
		List<BundledFareLiteDTO> bundledFareLiteDTOs = null;
		try {
			List<BundledFare> bundledFares = PromotionsHandlerFactory.getBundledfareDAO().getApplicableBundledFares(
					bundledFareSelectionCriteria);

			Date firstFlightDepartureDate = CalendarUtil.safeFormat(bundledFareSelectionCriteria.getFirstDepartureFlightDate(),
					CalendarUtil.PATTERN8);
			BundledFareHelper.removeUnMatchingBundleServicePeriods(bundledFares, firstFlightDepartureDate);

			List<BundledFare> filteredBundledFares = BundledFareHelper.filterBundledFares(bundledFares,
					bundledFareSelectionCriteria);

			Map<String, Map<String, String>> translatedMessages = new HashMap<String, Map<String, String>>();
			Collection<String> filteredTranslationKeys = new ArrayList<String>();

			for (BundledFare filteredBundledFare : filteredBundledFares) {
				Collection<String> translationKeys = BundledFareHelper.getSelectedPeriodI18nMessageKeys(filteredBundledFare);
				if (translationKeys != null && !translationKeys.isEmpty()) {
					filteredTranslationKeys.addAll(translationKeys);
				}
			}

			if (filteredTranslationKeys != null && !filteredTranslationKeys.isEmpty()) {
				translatedMessages = PromotionModuleServiceLocator.getCommonMasterBD().getTranslatedMessagesForKeys(
						filteredTranslationKeys);
			}

			BundledFare defaultBundled = BundledFareHelper.getDefaultBundle(filteredBundledFares);

			bundledFareLiteDTOs = BundledFareHelper.transformToLiteDTOs(filteredBundledFares, translatedMessages,
					bundledFareSelectionCriteria.getSelectedLanguage(), defaultBundled);

			Collections.sort(bundledFareLiteDTOs, new Comparator<BundledFareLiteDTO>() {
				public int compare(BundledFareLiteDTO bundledFareLiteDTO1, BundledFareLiteDTO bundledFareLiteDTO2) {
					return bundledFareLiteDTO1.getPerPaxBundledFee().compareTo(bundledFareLiteDTO2.getPerPaxBundledFee());
				}
			});

		} catch (Exception e) {
			log.error("ERROR in retrieving bundled services", e);
		}

		return bundledFareLiteDTOs;
	}

	@Override
	public List<BundledFareFreeService> getOfferedServices(String pnrSegId) {
		try {
			return PromotionsHandlerFactory.getBundledfareDAO().getOfferedServices(pnrSegId);
		} catch (Exception e) {
			log.error("ERROR in retrieving bundled free services", e);
			return null;
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isChargeCodeLinkedToBundledFare(String chargeCode) throws ModuleException {
		try {
			return PromotionsHandlerFactory.getBundledfareDAO().isChargeCodeLinkedToBundledFare(chargeCode);
		} catch (Exception e) {
			throw new ModuleException(e, "module.dataaccess.exception");
		}
	}

	private void validateBundleFareDescription(String updatedDescription) throws ModuleException {
		boolean descriptionExists = PromotionsHandlerFactory.getBundledfareDAO().isBundledFareExistsWithDescription(
				updatedDescription);
		if (descriptionExists) {
			throw new ModuleException("um.promotion.bundled.fare.name.already.exists");
		}
	}

	@Override
	public BundledFareDisplayDTO getBundledFareDisplayDTO(Integer bundledFarePeriodId) throws ModuleException {

		BundledFareDisplayDTO bundledFareDisplayDTO = new BundledFareDisplayDTO();

		BundledFare bundledFare = PromotionsHandlerFactory.getBundledfareDAO().getApplicableBundleFare(bundledFarePeriodId);
		BundledFareHelper.removeUnMatchingBundleServicePeriods(bundledFare, bundledFarePeriodId);
		BundledFarePeriod bundledFarePeriod = bundledFare.getSelectedBundledFarePeriod();
		bundledFareDisplayDTO.setBundleFarePeriodId(bundledFarePeriodId);

		//1. Populate Description templates
		Integer descriptionTemplateId = bundledFarePeriod.getDescriptionTemplateId();
		BundleFareDescriptionTemplateDTO descriptioTemplateDTO = null;
		if (descriptionTemplateId != null) {
			descriptioTemplateDTO = getBundleFareDescriptionTemplateById(descriptionTemplateId);
			bundledFareDisplayDTO.setDisplayTemplateId(descriptionTemplateId);
			bundledFareDisplayDTO.setDescriptionTemplateDTO(descriptioTemplateDTO);
		}

		//2. Populate Thumbnails
		Set<Integer> bundlePeriodIds = new HashSet<Integer>();
		bundlePeriodIds.add(bundledFarePeriodId);
		List<BundledFarePeriodImage> bundledFarePeriodImages = PromotionsHandlerFactory.getBundledFareDisplaySettingsDao()
				.getBundledFarePeriodImages(bundlePeriodIds);
		Map<String, String> thumbnails = BundledFareHelper.transformToBundledFarePeriodImagesMap(bundledFarePeriodImages);
		bundledFareDisplayDTO.setThumbnails(thumbnails);

		//3. Populate Translations
		BundledFareHelper.fillTranslations(bundledFareDisplayDTO, bundledFarePeriod);

		return bundledFareDisplayDTO;
	}

	@Override
	public void saveBundledFareDisplaySettings(BundledFareDisplaySettingsDTO displaySettingsDTO)
			throws ModuleException {

		Integer bundleFarePeriodId = displaySettingsDTO.getBundleFarePeriodId();
		BundledFare bundledFare = PromotionsHandlerFactory.getBundledfareDAO()
				.getApplicableBundleFare(bundleFarePeriodId);
		if (bundledFare == null) {
			throw handleException(new ModuleException("promotion.bundled.fare.not.exists"), true, log);
		}

		Map<String, String> thumbnails = displaySettingsDTO.getThumbnails();
		if (thumbnails != null && !thumbnails.isEmpty()) {
			List<BundledFarePeriodImage> bundledFarePeriodImages = new ArrayList<BundledFarePeriodImage>();
			BundledFarePeriodImage bundledFarePeriodImage = null;
			for (String languageCode : thumbnails.keySet()) {
				if (languageCode != null && !languageCode.isEmpty()) {
					String imageUrl = thumbnails.get(languageCode);
					bundledFarePeriodImage = new BundledFarePeriodImage();
					bundledFarePeriodImage.setBundleFarePeriodId(bundleFarePeriodId);
					bundledFarePeriodImage.setLanguageCode(languageCode);
					bundledFarePeriodImage.setImageUrl(imageUrl);
					bundledFarePeriodImages.add(bundledFarePeriodImage);
				}
			}
			PromotionsHandlerFactory.getBundledFareDisplaySettingsDao()
					.saveBundledFareDisplaySettings(bundledFarePeriodImages);
		}

		Map<String, String> nameTranslations = displaySettingsDTO.getNameTranslations();
		String nameKey = I18nTranslationUtil.getI18nBundledFareKey(bundleFarePeriodId,
				I18nMessageCategory.BUNDLED_FARE_NAME);
		PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(nameTranslations, nameKey,
				I18nMessageCategory.BUNDLED_FARE_NAME.toString());

		Map<String, String> descTranslations = displaySettingsDTO.getDescTranslations();
		String descKey = I18nTranslationUtil.getI18nBundledFareKey(bundleFarePeriodId,
				I18nMessageCategory.BUNDLED_FARE_DESC);
		PromotionModuleServiceLocator.getCommonMasterBD().saveTranslations(descTranslations, descKey,
				I18nMessageCategory.BUNDLED_FARE_DESC.toString());

		PromotionsHandlerFactory.getBundledFareDisplaySettingsDao().updateBundledFareTranlationKeys(bundleFarePeriodId,
				nameKey, descKey);

		Integer displayTemplateId = displaySettingsDTO.getDisplayTemplateId();

		if (displayTemplateId != null) {
			PromotionsHandlerFactory.getBundledFareDisplaySettingsDao().updateBundledFareTemplateId(bundleFarePeriodId,
					displayTemplateId);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdateBundleFareDescriptionTemplate(BundleFareDescriptionTemplateDTO templateDTO) throws ModuleException {
		try {
			PromotionsHandlerFactory.getBundleFareDescriptionDAO().updateBundleFareDescriptionTemplate(
					BundleFareDescriptionTemplateUtils.toBundleFareDescriptionTemplate(templateDTO));
		} catch (ParseException e) {
			log.debug(e);
			throw new ModuleException(e.toString());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page<BundleFareDescriptionTemplateDTO> getBundleFareDescriptionTemplate(String templateName, Integer start,
			Integer recSize) throws ModuleException {
		return BundleFareDescriptionTemplateUtils.convertPageToDTO(PromotionsHandlerFactory.getBundleFareDescriptionDAO()
				.getBundleFareDescriptionTemplate(templateName, start, recSize));
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void
			updateBundleFareDescriptionTranslationTemplate(BundleFareDescriptionTranslationTemplateDTO translationTemplateDTO)
					throws ModuleException {
		try {
			PromotionsHandlerFactory.getBundleFareDescriptionDAO().updateBundleFareDescriptionTranslationTemplate(
					BundleFareDescriptionTemplateUtils.toBundleFareDescriptionTranslationTemplate(translationTemplateDTO));
		} catch (ParseException e) {
			log.debug(e);
			throw new ModuleException(e.toString());
		}
	}

	@Override
	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateById(Integer templateId) {
		return BundleFareDescriptionTemplateUtils.toBundleFareDescriptionTemplateDTO(PromotionsHandlerFactory
				.getBundleFareDescriptionDAO().getBundleFareDescriptionTemplateById(templateId));
	}

}
