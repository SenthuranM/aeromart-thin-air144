package com.isa.thinair.promotion.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.promotion.api.service.VoucherBD;

/**
 * @author chethiya
 *
 */
@Remote
public interface VoucherBDRemote extends VoucherBD {

}
