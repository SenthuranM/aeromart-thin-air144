package com.isa.thinair.promotion.core.persistence.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.core.dto.NotificationReceiverTo;
import com.isa.thinair.promotion.core.dto.nextseat.NextSeatFreePromoResTo;

public interface PromotionAdministrationDao {
	<T, K extends Serializable> T findEntity(Class<T> type, K key);

	List<NotificationReceiverTo> getNotificationDestsEntitledForPromotion(Map<String, Object> params);

	List<NextSeatFreePromoResTo> getPromotionRequests(int flightSegId, PromotionType promotionType);

	List<Map<String, Object>> getPromotionRequestsToExpire(PromotionType promotionType);
}
