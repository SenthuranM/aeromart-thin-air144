package com.isa.thinair.promotion.core.bl.voucher;

import java.util.Date;

import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;

/**
 * @author chanaka
 * 
 * @isa.module.command name="cancelVoucher"
 * 
 */

public class CancelVoucher extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() throws ModuleException {
		VoucherDTO voucherDTO = (VoucherDTO) getParameter(CommandParamNames.VOUCHER_CANCEL);
		UserPrincipal userPrincipal = (UserPrincipal) getParameter(CommandParamNames.VOUCHER_CANCEL_USER);

		String voucherId = voucherDTO.getVoucherId();
		String cancelRemarks = voucherDTO.getCancelRemarks();
		int templateId = voucherDTO.getTemplateId();
		String userId = userPrincipal.getUserId();

		VoucherBO.cancelVoucher(voucherDTO);
		
		Audit audit = generateAudit(voucherId, cancelRemarks, userId, templateId);
		PromotionModuleServiceLocator.getAuditorBD().audit(audit, null);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		return response;
	}

	private static Audit generateAudit(String voucherId, String cancelRemarks, String userId, int templateId) {
		String details = "";
		if (templateId == 0) {
			details += "voucher_id := " + voucherId + "||cancel_remarks := " + cancelRemarks;
		} else {
			details += "voucher_id := " + voucherId + "||voucher_template_id := " + templateId + "||cancel_remarks := "
					+ cancelRemarks;
		}
		return new Audit(TasksUtil.CANCEL_VOUCHER, new Date(), "xbe", userId, details);
	}

}
