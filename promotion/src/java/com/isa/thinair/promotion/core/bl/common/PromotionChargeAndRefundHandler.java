package com.isa.thinair.promotion.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.OndPromotion;
import com.isa.thinair.promotion.api.model.OndPromotionCharge;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.promotion.core.util.CommandParamNames;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.promotion.core.util.PromotionsDaoHelper;

public abstract class PromotionChargeAndRefundHandler extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {
		ServiceResponce resp = new DefaultServiceResponse();

		PromotionRequest promotionRequest = (PromotionRequest) getParameter(CommandParamNames.PROMOTION_REQUEST);
		String processCategory = (String) getParameter(CommandParamNames.PROMO_PROCESS_CATEGORY);
		String requestType = (String) getParameter(CommandParamNames.REQUEST_TYPE);

		List<OndPromotionCharge> applicableChargesRefunds;
		PromoTemplateParam.ChargeAndReward chargeAndReward;
		BigDecimal totalAmount = BigDecimal.ZERO;

		applicableChargesRefunds = applicableChargesRefunds();
		resp.addResponceParam(CommandParamNames.APPLICABLE_CHARGES_REFUNDS, applicableChargesRefunds);

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(promotionRequest.getPnr());
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		long reservationVersion;

		BigDecimal amount = BigDecimal.ZERO;

		if (applicableChargesRefunds != null) {

			for (OndPromotionCharge ondPromotionCharge : applicableChargesRefunds) {

				chargeAndReward = PromoTemplateParam.ChargeAndReward.resolveChargeAndRewardByCode(ondPromotionCharge
						.getChargeCode());
				int chargeRateId = PromotionsDaoHelper.getChargeRateId(chargeAndReward.getPrefix());

				if (processCategory.equals(CommandParamNames.PROCESS_REFUNDS)) {
					amount = new BigDecimal(ondPromotionCharge.getAmount()).negate();
				} else if (processCategory.equals(CommandParamNames.PROCESS_CHARGES)) {
					amount = new BigDecimal(ondPromotionCharge.getAmount());
				}


				// pax-wise charge entries
				for (ReservationPax pax : reservation.getPassengers()) {
					if (pax.getPnrPaxId().equals(promotionRequest.getReservationPaxId())) {
						for (ReservationPaxFare pnrPaxFare : pax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment pnrPaxFareSeg : pnrPaxFare.getPaxFareSegments()) {
								if ( // pnrPaxFareSeg.getSegment().getStatus().equals(ReservationSegmentStatus.CONFIRMED) &&
										pnrPaxFareSeg.getSegment().getPnrSegId().equals(promotionRequest.getReservationSegmentId())) {
//									TODO -- why is LoadFares = true
//									pnrModesDTO.setLoadFares(false);
									reservationVersion = ReservationProxy.getReservation(pnrModesDTO).getVersion();

									totalAmount = totalAmount.add(amount);

									if (requestType.equals(CommandParamNames.REQUEST_EXECUTE)) {
										String name = pax.getFirstName() + " " + pax.getLastName();

										String ondCode = null;
										/*
										 * for (ReservationSegmentDTO reservationSeg : reservation.getSegmentsView()) {
										 * if (reservationSeg.getPnrSegId().intValue() == promotionRequest
										 * .getReservationSegmentId().intValue()) { ondCode =
										 * reservationSeg.getSegmentCode(); } }
										 */

//										Map<Integer, String> segMap = new TreeMap<Integer, String>();
										String returnFlag = pnrPaxFareSeg.getSegment().getReturnFlag();
										Integer segSeq = null;
										List<ReservationSegmentDTO> oneWaySegs = new ArrayList<ReservationSegmentDTO>();

//										for (ReservationSegmentDTO reservationSeg : reservation.getSegmentsView()) {
//											if (reservationSeg.getPnrSegId().intValue() == promotionRequest
//													.getReservationSegmentId().intValue()) {
//												returnFlag = reservationSeg.getReturnFlag();
//												segSeq = reservationSeg.getSegmentSeq();
//												segMap.put(reservationSeg.getSegmentSeq(),reservationSeg.getSegmentCode());
//												break;
//											}
//										}
										
										for (ReservationSegmentDTO reservationSeg : reservation.getSegmentsView()) {
											if (returnFlag.equals(reservationSeg.getReturnFlag()) &&
											reservationSeg.getStatus().equals(ReservationSegmentStatus.CONFIRMED)) {
												oneWaySegs.add(reservationSeg);
											}
										}
//										List<ReservationSegmentDTO> oneWaySortedList = new ArrayList<ReservationSegmentDTO>(oneWaySegs);
										Collections.sort(oneWaySegs);
										List<String> segCodes = new ArrayList<String>();
										for (ReservationSegmentDTO resSegDto : oneWaySegs) {
											segCodes.add(resSegDto.getSegmentCode());
										}

										// For flexi promotions remove cnx segments
//										if (promotionRequest.getPromotionTemplate().getPromotionType().getPromotionId()
//												.equals(PromotionType.PROMOTION_FLEXI_DATE.getPromotionId())) {
//											ListIterator<ReservationSegmentDTO> segIterator = oneWaySortedList.listIterator();
//											while (segIterator.hasNext()) {
//												ReservationSegmentDTO seg = segIterator.next();
//												if (seg.getStatus().equals(ReservationSegmentStatus.CANCEL)) {
//													segIterator.remove();
//												}
//											}
//										}

//										int tempSegSeq = 0;
//
//										tempSegSeq = segSeq;
//										ListIterator<ReservationSegmentDTO> listIterator = oneWaySortedList.listIterator();
//										while (listIterator.hasNext()) {
//											ReservationSegmentDTO seg = listIterator.next();
//											if (tempSegSeq + 1 == seg.getSegmentSeq().intValue()) {
//												tempSegSeq += 1;
//												segMap.put(seg.getSegmentSeq(),seg.getSegmentCode());
//											}
//										}
//
//										tempSegSeq = segSeq;
//										ListIterator<ReservationSegmentDTO> listIteratorReverse = oneWaySortedList
//												.listIterator(oneWaySortedList.size() - 1);
//										while (listIterator.hasPrevious()) {
//											ReservationSegmentDTO seg = listIterator.previous();
//											if (tempSegSeq - 1 == seg.getSegmentSeq().intValue()) {
//												tempSegSeq -= 1;
//												segMap.put(seg.getSegmentSeq(), seg.getSegmentCode());
//											}
//										}

//										ondCode = PromotionsUtils.getOndCode(new ArrayList<String>(segMap.values()));
										ondCode = PromotionsUtils.getOndCode(segCodes);


										if (promotionRequest.getPromotionTemplate().getPromotionType().getPromotionId()
												.equals(PromotionType.PROMOTION_NEXT_SEAT_FREE.getPromotionId())) {
											/*
											 * String name = pax.getFirstName() + " " + pax.getLastName(); String
											 * ondCode = ondPromotionCharge.getOndPromotion().getOndCode();
											 */
											boolean isRefundable = chargeAndReward.isRefundable();
											String userNoteText = null;
											if (AccelAeroCalculator.isLessThan(amount, BigDecimal.ZERO)) {
												userNoteText = chargeAndReward.getFreeSeatRefundCharge(name, ondCode,
														amount.negate());
											} else {
												userNoteText = chargeAndReward.getFreeSeatRegistraionCharge(name, ondCode,
														amount, isRefundable);
											}
											PromotionModuleServiceLocator.getPassengerBD().adjustCreditManual(
													reservation.getPnr(), pnrPaxFare.getPnrPaxFareId(), chargeRateId, amount,
													userNoteText, reservationVersion, null, null, null);
										} else {

											String userNoteText = chargeAndReward.getFlexiDateRewardCharge(name, ondCode,
													amount.negate());
													
											PromotionModuleServiceLocator.getPassengerBD().adjustCreditManual(
													reservation.getPnr(), pnrPaxFare.getPnrPaxFareId(), chargeRateId, amount,
													userNoteText, reservationVersion, null, null, null);
										}



									}
									/*
									if (requestType.equals(CommandParamNames.REQUEST_EXECUTE)
											&& processCategory.equals(CommandParamNames.PROCESS_CHARGES)) {

										amount = new BigDecimal(ondPromotionCharge.getAmount()).negate();
										//totalAmount = totalAmount.add(amount);
										reservationVersion = ReservationProxy.getReservation(pnrModesDTO).getVersion();

										PromotionModuleServiceLocator.getPassengerBD().adjustCreditManual(reservation.getPnr(),
												pnrPaxFare.getPnrPaxFareId(), chargeRateId, amount,
												chargeAndReward.getChargeDescription(), reservationVersion, null);

									}
									*/

								}
							}
						}
					}
				}
			}

		}

		resp.addResponceParam(CommandParamNames.TOTAL_CHARGES_REFUNDS, totalAmount);

		return resp;
	}

	private List<OndPromotionCharge> applicableChargesRefunds() {
		List<OndPromotionCharge> applicableChargesRefunds = null;
		PromotionRequest promotionRequest = (PromotionRequest) getParameter(CommandParamNames.PROMOTION_REQUEST);
		List<String> flightSegCodes = (List<String>) getParameter(CommandParamNames.FLIGHT_SEGMENT_CODES);

		Set<OndPromotionCharge> charges = getApplicableOndCharges(promotionRequest.getPromotionTemplate().getOnds(), flightSegCodes);

		if (charges != null) {
			applicableChargesRefunds = filterApplicableCharges(charges, promotionRequest);
		}

		return applicableChargesRefunds;
	}

	protected abstract List<OndPromotionCharge> filterApplicableCharges(Set<OndPromotionCharge> charges,
			PromotionRequest promotionRequest);

	private Set<OndPromotionCharge> getApplicableOndCharges(Set<OndPromotion> ondPromos, List<String> promoRequestedSegs) {

		String routeCode = PromotionsUtils.getOndCode(promoRequestedSegs);

		Map<String, OndPromotion> ondPromotionsMapping = new HashMap<String, OndPromotion>();
		for (OndPromotion promo : ondPromos) {
			ondPromotionsMapping.put(promo.getOndCode(), promo);
		}

		Pair<String, String> origAndDest = PromotionsUtils.getOriginAndDestinationAirports(routeCode);
		OndPromotion applicableOndPromo = null;
		List<String> matchingOrder = new ArrayList<String>();
		if (promoRequestedSegs.size() == 1) {
			// For Single-Seg ---- A/B
			// A/B > A/ALL > ALL/B > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder
					.add(origAndDest.getLeft() + PromotionsInternalConstants.OND_DELIM + PromotionsInternalConstants.OND_ALL);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ origAndDest.getRight());
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);
		} else {
			// For Multi-Seg ---- A/B + B/C
			// A/B/C > ALL/ALL
			matchingOrder.add(routeCode);
			matchingOrder.add(PromotionsInternalConstants.OND_ALL + PromotionsInternalConstants.OND_DELIM
					+ PromotionsInternalConstants.OND_ALL);
		}

		for (String ondToMatch : matchingOrder) {
			if (ondPromotionsMapping.containsKey(ondToMatch)) {
				applicableOndPromo = ondPromotionsMapping.get(ondToMatch);
				break;
			}
		}

		return applicableOndPromo.getOndPromotionCharges();
	}
}
