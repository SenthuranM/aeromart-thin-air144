package com.isa.thinair.promotion.core.bl.nextseatfree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.model.PromotionRequest;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.core.dto.nextseat.SeatSelectionTo;

public class NextSeatFreePromotionBL implements NextSeatFreeSeatsSelector {

	public SeatSelectionTo getSeatsToAllocate(Integer flightSegId, PromotionRequest promotionRequest,
			Map<String, FlightSeatStatusTO> seatInventory, String paxType, PaxSeatTO seat, String logicalCabinClassCode,
			Integer noOfSeatsRequested, String cabinClassCode, boolean relocate) {

		SeatSelectionTo seatSelectionTo = new SeatSelectionTo();

		Map<String, List<FlightSeatStatusTO>> selectedSeatMap = new HashMap<String, List<FlightSeatStatusTO>>();

		List<FlightSeatStatusTO> allocatedSeats = new ArrayList<FlightSeatStatusTO>();
		List<FlightSeatStatusTO> removedSeats = new ArrayList<FlightSeatStatusTO>();

		int rowsToSkip = AppSysParamsUtil.getNextSeatFreePromoSeatRowsToSkip();

		if (noOfSeatsRequested > PromotionsInternalConstants.MAXIMUM_SEATS_ALLOWED_NEXT_SEAT_PROMOTION || seatInventory == null
				|| seatInventory.size() == 0) {
			return seatSelectionTo;
		}

		if (seat != null) {
			char rowId = seat.getSeatCode().charAt(seat.getSeatCode().length() - 1);
			Integer ColId = Integer.parseInt(seat.getSeatCode().substring(0, seat.getSeatCode().length() - 1));

			String nextSeatAfterCode = ColId.toString() + (char) (rowId + 1);
			String nextSeatBeforeCode = ColId.toString() + (char) (rowId - 1);

			FlightSeatStatusTO currentSelectedSeat = seatInventory.get(seat.getSeatCode());
			FlightSeatStatusTO nextSeatAfterCurrentSeat = seatInventory.get(nextSeatAfterCode);
			FlightSeatStatusTO nextSeatBeforeCurrentSeat = seatInventory.get(nextSeatBeforeCode);

			if (noOfSeatsRequested == 1) {

				if (nextSeatAfterCurrentSeat != null
						&& nextSeatAfterCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatAfterCurrentSeat.getCabinClassCode().equals(cabinClassCode)
						&& nextSeatAfterCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatAfterCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatAfterCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()) {

					FlightSeatStatusTO allocatedSeat = new FlightSeatStatusTO();
					allocatedSeat.setSeatCode(nextSeatAfterCode);
					allocatedSeats.add(allocatedSeat);

					selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
					selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);

					seatSelectionTo.setSeatSelection(selectedSeatMap);
					seatSelectionTo.setOriginalSeat(currentSelectedSeat);
					return seatSelectionTo;

				} else if (nextSeatBeforeCurrentSeat != null
						&& nextSeatBeforeCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatBeforeCurrentSeat.getCabinClassCode().equals(cabinClassCode)
						&& nextSeatBeforeCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatBeforeCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatBeforeCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()) {

					FlightSeatStatusTO allocatedSeat = new FlightSeatStatusTO();
					allocatedSeat.setSeatCode(nextSeatBeforeCode);
					allocatedSeats.add(allocatedSeat);

					selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
					selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);

					seatSelectionTo.setSeatSelection(selectedSeatMap);
					seatSelectionTo.setOriginalSeat(currentSelectedSeat);
					return seatSelectionTo;

				}

			} else if (noOfSeatsRequested == 2) {
				String nextSeatAfterNextCode = ColId.toString() + (char) (rowId + 2);
				String nextSeatBeforeNextCode = ColId.toString() + (char) (rowId - 2);

				FlightSeatStatusTO nextSeatAfterNextCurrentSeat = seatInventory.get(nextSeatAfterNextCode);
				FlightSeatStatusTO nextSeatBeforeNextCurrentSeat = seatInventory.get(nextSeatBeforeNextCode);

				if (nextSeatAfterCurrentSeat != null && nextSeatBeforeCurrentSeat != null
						&& nextSeatAfterCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatAfterCurrentSeat.getCabinClassCode().equals(cabinClassCode)
						&& nextSeatAfterCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatAfterCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatAfterCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()
						&& nextSeatBeforeCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatBeforeCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatBeforeCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatBeforeCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()) {

					FlightSeatStatusTO allocatedSeat1 = new FlightSeatStatusTO();
					allocatedSeat1.setSeatCode(nextSeatAfterCode);
					allocatedSeats.add(allocatedSeat1);
					FlightSeatStatusTO allocatedSeat2 = new FlightSeatStatusTO();
					allocatedSeat2.setSeatCode(nextSeatBeforeCode);
					allocatedSeats.add(allocatedSeat2);

					selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
					selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);

					seatSelectionTo.setSeatSelection(selectedSeatMap);
					seatSelectionTo.setOriginalSeat(currentSelectedSeat);
					return seatSelectionTo;

				} else if (nextSeatAfterNextCurrentSeat != null
						&& nextSeatAfterCurrentSeat != null
						&& nextSeatAfterCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatAfterCurrentSeat.getCabinClassCode().equals(cabinClassCode)
						&& nextSeatAfterCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatAfterCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatAfterCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()
						&& nextSeatAfterNextCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatAfterNextCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatAfterNextCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatAfterNextCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId()
								.intValue()) {

					FlightSeatStatusTO allocatedSeat1 = new FlightSeatStatusTO();
					allocatedSeat1.setSeatCode(nextSeatAfterCode);
					allocatedSeats.add(allocatedSeat1);
					FlightSeatStatusTO allocatedSeat2 = new FlightSeatStatusTO();
					allocatedSeat2.setSeatCode(nextSeatAfterNextCode);
					allocatedSeats.add(allocatedSeat2);

					selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
					selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);

					seatSelectionTo.setSeatSelection(selectedSeatMap);
					seatSelectionTo.setOriginalSeat(currentSelectedSeat);
					return seatSelectionTo;

				} else if (nextSeatBeforeNextCurrentSeat != null
						&& nextSeatBeforeCurrentSeat != null
						&& nextSeatBeforeNextCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatBeforeNextCurrentSeat.getCabinClassCode().equals(cabinClassCode)
						&& nextSeatBeforeNextCurrentSeat.getStatus()
								.equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatBeforeNextCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatBeforeNextCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId()
								.intValue() && nextSeatBeforeCurrentSeat.getLogicalCabinClassCode().equals(logicalCabinClassCode)
						&& nextSeatBeforeCurrentSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
						&& nextSeatBeforeCurrentSeat.getColId().intValue() == currentSelectedSeat.getColId().intValue()
						&& nextSeatBeforeCurrentSeat.getRowGroupId().intValue() == currentSelectedSeat.getRowGroupId().intValue()) {

					FlightSeatStatusTO allocatedSeat1 = new FlightSeatStatusTO();
					allocatedSeat1.setSeatCode(nextSeatBeforeCode);
					allocatedSeats.add(allocatedSeat1);
					FlightSeatStatusTO allocatedSeat2 = new FlightSeatStatusTO();
					allocatedSeat2.setSeatCode(nextSeatBeforeNextCode);
					allocatedSeats.add(allocatedSeat2);

					selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
					selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);

					seatSelectionTo.setSeatSelection(selectedSeatMap);
					seatSelectionTo.setOriginalSeat(currentSelectedSeat);
					return seatSelectionTo;

				}
			}

		}

		if (allocatedSeats.size() <= 0 && relocate) {

			Iterator<Map.Entry<String, FlightSeatStatusTO>> currentSeatIterator = seatInventory.entrySet().iterator();
			Iterator<Map.Entry<String, FlightSeatStatusTO>> nextFirstSeatIterator = seatInventory.entrySet().iterator();
			nextFirstSeatIterator.next();
			Iterator<Map.Entry<String, FlightSeatStatusTO>> nextSecondSeatIterator = seatInventory.entrySet().iterator();
			nextSecondSeatIterator.next();
			nextSecondSeatIterator.next();

			FlightSeatStatusTO currentSeatData = null;
			FlightSeatStatusTO nextFirstSeatData = null;
			FlightSeatStatusTO nextSecondSeatData = null;

			if (noOfSeatsRequested == 1) {

				while (currentSeatIterator.hasNext()) {
					currentSeatData = (FlightSeatStatusTO) currentSeatIterator.next().getValue();
					if (nextFirstSeatIterator.hasNext()) {
						nextFirstSeatData = (FlightSeatStatusTO) nextFirstSeatIterator.next().getValue();
					} else {
						nextFirstSeatData = null;
					}
					if (currentSeatData != null && nextFirstSeatData != null && currentSeatData.getColId() > rowsToSkip) {

						if (currentSeatData.getLogicalCabinClassCode().equals(logicalCabinClassCode)
								&& currentSeatData.getCabinClassCode().equals(cabinClassCode)
								&& currentSeatData.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {

							if (nextFirstSeatData.getLogicalCabinClassCode().equals(logicalCabinClassCode)
									&& nextFirstSeatData.getCabinClassCode().equals(cabinClassCode)
									&& nextFirstSeatData.getStatus()
											.equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
									&& currentSeatData.getColId().intValue() == nextFirstSeatData.getColId().intValue()
									&& currentSeatData.getRowGroupId().intValue() == nextFirstSeatData.getRowGroupId().intValue()) {

								List<FlightSeatStatusTO> nominalSeats = new ArrayList<FlightSeatStatusTO>();
								nominalSeats.add(nextFirstSeatData);
								if (FlightSeatValidator.isAllowedSeatCombination(flightSegId, paxType, currentSeatData,
										nominalSeats)) {

									if (seat != null) {
										currentSeatData.setSeatCharge(seat.getChgDTO().getAmount());
									}
									allocatedSeats.add(currentSeatData);
									allocatedSeats.add(nextFirstSeatData);

									if (seat != null) {
										FlightSeatStatusTO removedSeat = new FlightSeatStatusTO();
										removedSeat.setSeatCharge(seat.getChgDTO().getAmount());
										removedSeat.setSeatCode(seat.getSeatCode());
										removedSeats.add(removedSeat);
									}

									selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
									selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);


								seatSelectionTo.setSeatSelection(selectedSeatMap);
								seatSelectionTo.setRelocated(true);
								return seatSelectionTo;
								}

							}

						}
					}
				}
			} else if (noOfSeatsRequested == 2) {

				while (currentSeatIterator.hasNext()) {

					currentSeatData = (FlightSeatStatusTO) currentSeatIterator.next().getValue();

					if (nextFirstSeatIterator.hasNext()) {
						nextFirstSeatData = (FlightSeatStatusTO) nextFirstSeatIterator.next().getValue();
					} else {
						nextFirstSeatData = null;
					}
					if (nextSecondSeatIterator.hasNext()) {
						nextSecondSeatData = (FlightSeatStatusTO) nextSecondSeatIterator.next().getValue();
					} else {
						nextSecondSeatData = null;
					}
					if (currentSeatData != null && nextFirstSeatData != null && nextSecondSeatData != null
							&& currentSeatData.getColId() > rowsToSkip) {

						if (currentSeatData.getLogicalCabinClassCode().equals(logicalCabinClassCode)
								&& currentSeatData.getCabinClassCode().equals(cabinClassCode)
								&& currentSeatData.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {

							if (nextFirstSeatData.getLogicalCabinClassCode().equals(logicalCabinClassCode)
									&& nextFirstSeatData.getCabinClassCode().equals(cabinClassCode)
									&& nextFirstSeatData.getStatus()
											.equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
									&& currentSeatData.getColId().intValue() == nextFirstSeatData.getColId().intValue()
									&& currentSeatData.getRowGroupId().intValue() == nextFirstSeatData.getRowGroupId()) {

								if (nextSecondSeatData.getLogicalCabinClassCode().equals(logicalCabinClassCode)
										&& nextSecondSeatData.getCabinClassCode().equals(cabinClassCode)
										&& nextSecondSeatData.getStatus().equals(
												AirinventoryCustomConstants.FlightSeatStatuses.VACANT)
										&& nextFirstSeatData.getColId().intValue() == nextSecondSeatData.getColId().intValue()
										&& nextFirstSeatData.getRowGroupId().intValue() == nextSecondSeatData.getRowGroupId()
												.intValue()) {

									List<FlightSeatStatusTO> nominalSeats = new ArrayList<FlightSeatStatusTO>();
									nominalSeats.add(nextFirstSeatData);
									nominalSeats.add(nextSecondSeatData);

									if (FlightSeatValidator.isAllowedSeatCombination(flightSegId, paxType, currentSeatData,
											nominalSeats)) {

										if (seat != null) {
											currentSeatData.setSeatCharge(seat.getChgDTO().getAmount());
										}
										allocatedSeats.add(currentSeatData);
										allocatedSeats.add(nextFirstSeatData);
										allocatedSeats.add(nextSecondSeatData);

										if (seat != null) {
											FlightSeatStatusTO removedSeat = new FlightSeatStatusTO();
											removedSeat.setSeatCharge(seat.getChgDTO().getAmount());
											removedSeat.setSeatCode(seat.getSeatCode());
											removedSeats.add(removedSeat);
										}

										selectedSeatMap.put(PromotionsInternalConstants.ADD, allocatedSeats);
										selectedSeatMap.put(PromotionsInternalConstants.REMOVE, removedSeats);


									seatSelectionTo.setSeatSelection(selectedSeatMap);         								seatSelectionTo.setRelocated(true);
									seatSelectionTo.setRelocated(true);
									return seatSelectionTo;
									}

								}
							}
						}

					}

				}
			}
		}
		return seatSelectionTo;
	}
}