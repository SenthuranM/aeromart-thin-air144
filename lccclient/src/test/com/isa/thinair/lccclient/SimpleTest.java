package com.isa.thinair.lccclient;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.lccclient.api.dto.availability.FlightAvailRQ;
import com.isa.thinair.lccclient.api.dto.commons.OriginDestination;
import com.isa.thinair.lccclient.api.transform.TOT;

public class SimpleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		FlightAvailRQ availRQ = new FlightAvailRQ();
		OriginDestination depatureOndInfo = availRQ.addNewOriginDestination();
		depatureOndInfo.setOrigin("CMB");
		depatureOndInfo.setDestination("SHJ");
		
		availRQ.createPOS();
		availRQ.createTravelerSummary();
		availRQ.createTravelPreferences();
		availRQ.createAvailPreferences();
		
		AvailableFlightSearchDTO availableSearchDTO = TOT.transformTo(availRQ, AvailableFlightSearchDTO.class);
		System.out.println(availableSearchDTO);
	}

}
