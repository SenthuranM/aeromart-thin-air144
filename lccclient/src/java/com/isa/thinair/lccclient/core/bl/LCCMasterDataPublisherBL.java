package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCCity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCStation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCTerritory;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCMasterDataPublishRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCMasterDataPublishRS;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airproxy.api.model.masterDataPublish.AirportDSTPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRSDataTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCCPublishDataUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCMasterDataPublisherBL {

	private MasterDataPublishRQDataTO masterDataPublishRQDataTO;

	public LCCMasterDataPublisherBL(MasterDataPublishRQDataTO airportPublishRQDataTO) {
		this.masterDataPublishRQDataTO = airportPublishRQDataTO;
	}

	public MasterDataPublishRSDataTO execute() throws ModuleException {
		LCCMasterDataPublishRQ lccMasterDataPublishRQ = this.createLCCMasterDataPublishRQ();
		LCCMasterDataPublishRS lccMasterDataPublishRS = LCConnectorUtils.getMaxicoExposedWS().publishMasterData(
				lccMasterDataPublishRQ);
		return this.setMasterDataPublishRSDataTO(lccMasterDataPublishRS);
	}

	private LCCMasterDataPublishRQ createLCCMasterDataPublishRQ() {
		LCCMasterDataPublishRQ lccMasterDataPublishRQ = new LCCMasterDataPublishRQ();
		lccMasterDataPublishRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccMasterDataPublishRQ.setLccPos(LCCClientCommonUtils.createPOS(null));

		if (masterDataPublishRQDataTO.getPublishAirports() != null && masterDataPublishRQDataTO.getPublishAirports().size() > 0) {
			createLCCPublishAirports(lccMasterDataPublishRQ);
		}

		if (masterDataPublishRQDataTO.getPublishStations() != null && masterDataPublishRQDataTO.getPublishStations().size() > 0) {
			createLCCPublishStations(lccMasterDataPublishRQ);
		}

		if (masterDataPublishRQDataTO.getPublishTerritories() != null
				&& masterDataPublishRQDataTO.getPublishTerritories().size() > 0) {
			createLCCPublishTerritories(lccMasterDataPublishRQ);
		}
		
		if (masterDataPublishRQDataTO.getPublishCities() != null
				&& masterDataPublishRQDataTO.getPublishCities().size() > 0) {
			createLCCPublishCities(lccMasterDataPublishRQ);
		}

		return lccMasterDataPublishRQ;
	}

	private void createLCCPublishCities(LCCMasterDataPublishRQ lccMasterDataPublishRQ) {
		List<City> cities = masterDataPublishRQDataTO.getPublishCities();
		List<LCCCity> lccCities = new ArrayList<>();
		for (City city : cities) {
			LCCCity lccCity = new LCCCity();
			lccCity.setCityCode(city.getCityCode());
			lccCity.setCityName(city.getCityName());
			lccCity.setCountryCode(city.getCountryCode());
			lccCity.setPublishCarrier(AppSysParamsUtil.getDefaultCarrierCode());
			lccCity.setLastModifiedBy(AppSysParamsUtil.getDefaultCarrierCode() + " - "
					+ LCCPublishDataUtil.ROUTE_INFO_SCHEDULAR_USER_NAME);
			lccCity.setStatus(city.getStatus());
			lccCity.setVersion(city.getVersion());
			lccCities.add(lccCity);
		}
		lccMasterDataPublishRQ.getCityList().addAll(lccCities);
	}


	private void createLCCPublishStations(LCCMasterDataPublishRQ lccMasterDataPublishRQ) {
		List<LCCStation> colLCCStations = new ArrayList<LCCStation>();
		Iterator<Station> stationIte = masterDataPublishRQDataTO.getPublishStations().iterator();

		while (stationIte.hasNext()) {
			Station airStation = stationIte.next();
			LCCStation lccStation = new LCCStation();

			lccStation.setStationCode(airStation.getStationCode());
			lccStation.setStationName(airStation.getStationName());
			// for other carriers this station should be offline as per initial logic
			lccStation.setOnlineStatus(0);
			lccStation.setStatus(airStation.getStatus());
			lccStation.setRemarks(airStation.getRemarks());
			lccStation.setTerritoryCode(airStation.getTerritoryCode());
			lccStation.setCreatedBy(airStation.getCreatedBy());
			lccStation.setCreatedDate(airStation.getCreatedDate());
			lccStation.setModifiedBy(airStation.getModifiedBy());
			lccStation.setModifiedDate(airStation.getModifiedDate());
			lccStation.setCountryCode(airStation.getCountryCode());
			lccStation.setContact(airStation.getContact());
			lccStation.setTelephone(airStation.getTelephone());
			lccStation.setFax(airStation.getFax());
			lccStation.setThresholdTime(airStation.getNameChangeThresholdTime());
			lccStation.setVersion(airStation.getVersion());

			colLCCStations.add(lccStation);
		}

		lccMasterDataPublishRQ.getStationList().addAll(colLCCStations);
	}

	private void createLCCPublishTerritories(LCCMasterDataPublishRQ lccMasterDataPublishRQ) {
		List<LCCTerritory> colLCCTerritories = new ArrayList<LCCTerritory>();
		Iterator<Territory> territoryIte = masterDataPublishRQDataTO.getPublishTerritories().iterator();

		while (territoryIte.hasNext()) {
			Territory airTerritory = territoryIte.next();
			LCCTerritory lccTerritory = new LCCTerritory();

			lccTerritory.setTerritoryCode(airTerritory.getTerritoryCode());
			lccTerritory.setDescription(airTerritory.getDescription());
			lccTerritory.setRemarks(airTerritory.getRemarks());
			lccTerritory.setStatus(airTerritory.getStatus());
			lccTerritory.setCreatedBy(airTerritory.getCreatedBy());
			lccTerritory.setCreatedDate(airTerritory.getCreatedDate());
			lccTerritory.setModifiedBy(airTerritory.getModifiedBy());
			lccTerritory.setModifiedDate(airTerritory.getModifiedDate());
			lccTerritory.setAirlineCode(airTerritory.getAirlineCode());
			lccTerritory.setSwiftCode(airTerritory.getSwiftCode());
			lccTerritory.setBankAddress(airTerritory.getBankAddress());
			lccTerritory.setAccountNumber(airTerritory.getAccountNumber());
			lccTerritory.setBank(airTerritory.getBank());
			lccTerritory.setIbanNo(airTerritory.getIBANCode());
			lccTerritory.setBeneficiaryName(airTerritory.getBeneficiaryName());
			lccTerritory.setVersion(airTerritory.getVersion());

			colLCCTerritories.add(lccTerritory);
		}

		lccMasterDataPublishRQ.getTerritoryList().addAll(colLCCTerritories);
	}

	private void createLCCPublishAirports(LCCMasterDataPublishRQ lccMasterDataPublishRQ) {

		List<com.isa.connectivity.profiles.maxico.v1.common.dto.Airport> colLCCAirports = new ArrayList<com.isa.connectivity.profiles.maxico.v1.common.dto.Airport>();
		Iterator<AirportDSTPublishRQDataTO> airportIterator = masterDataPublishRQDataTO.getPublishAirports().iterator();

		while (airportIterator.hasNext()) {
			AirportDSTPublishRQDataTO airportDSTPublishRQDataTO = airportIterator.next();
			Airport airlineAirport = airportDSTPublishRQDataTO.getAiport();
			com.isa.connectivity.profiles.maxico.v1.common.dto.Airport lccAirport = new com.isa.connectivity.profiles.maxico.v1.common.dto.Airport();

			// Set airport details
			lccAirport.setAirportCode(airlineAirport.getAirportCode());
			lccAirport.setAirportName(airlineAirport.getAirportName());
			lccAirport.setGmtOffsetAction(airlineAirport.getGmtOffsetAction());
			lccAirport.setGmtOffsetHours(airlineAirport.getGmtOffsetHours());
			lccAirport.setLastModifiedBy(AppSysParamsUtil.getDefaultCarrierCode() + " - "
					+ LCCPublishDataUtil.ROUTE_INFO_SCHEDULAR_USER_NAME);
			lccAirport.setLatitude(airlineAirport.getLatitude());
			lccAirport.setLatitudeNorthSouth(airlineAirport.getLatitudeNorthSouth());
			lccAirport.setLongitude(airlineAirport.getLongitude());
			lccAirport.setLongitudeEastWest(airlineAirport.getLongitudeEastWest());
			lccAirport.setOperatedCarriers(airlineAirport.getCarrierCode());
			lccAirport.setPublishCarrier(AppSysParamsUtil.getDefaultCarrierCode());
			lccAirport.setIsSurfaceStation(airlineAirport.getIsSurfaceStation());
			lccAirport.setConnectingAirport(airlineAirport.getConnectingAirport());
			lccAirport.setCountryCode(airportDSTPublishRQDataTO.getCountryCode());
			lccAirport.setCityCode(airlineAirport.getCityCode());
			lccAirport.setStateCode(airlineAirport.getStateCode());
			// If airport is off line status is set to inactive.
			lccAirport.setStatus((airlineAirport.getOnlineStatus() == 0) ? Airport.STATUS_INACTIVE : airlineAirport.getStatus());
			// Set DST details
			for (Iterator<AirportDST> dstIterator = airportDSTPublishRQDataTO.getAirportDSTs().iterator(); dstIterator.hasNext();) {
				AirportDST airportDST = dstIterator.next();
				com.isa.connectivity.profiles.maxico.v1.common.dto.AirportDST lccAirportDST = new com.isa.connectivity.profiles.maxico.v1.common.dto.AirportDST();
				lccAirportDST.setAirportCode(airportDST.getAirportCode());
				lccAirportDST.setDstAdjustTime(new BigDecimal(airportDST.getDstAdjustTime()));
				lccAirportDST.setDstEndTime(airportDST.getDstEndDateTime());
				lccAirportDST.setDstStartTime(airportDST.getDstStartDateTime());
				lccAirportDST.setModifiedBy(AppSysParamsUtil.getDefaultCarrierCode() + " - "
						+ LCCPublishDataUtil.ROUTE_INFO_SCHEDULAR_USER_NAME);
				lccAirportDST.setModifiedDate((airportDST.getModifiedDate() != null) ? airportDST.getModifiedDate() : airportDST
						.getCreatedDate());
				lccAirportDST.setStatus(airportDST.getDSTStatus());
				lccAirport.getAirportDsts().add(lccAirportDST);
			}

			colLCCAirports.add(lccAirport);

		}
		lccMasterDataPublishRQ.getAirportList().addAll(colLCCAirports);
	}

	private MasterDataPublishRSDataTO setMasterDataPublishRSDataTO(LCCMasterDataPublishRS lccMasterDataPublishRS) {
		MasterDataPublishRSDataTO masterDataPublishRSDataTO = new MasterDataPublishRSDataTO();
		masterDataPublishRSDataTO.getUpdatedAirportCodes().addAll(lccMasterDataPublishRS.getUpdatedAirportCodes());
		masterDataPublishRSDataTO.setUpdatedStationCodes(lccMasterDataPublishRS.getUpdatedStationCodes());
		masterDataPublishRSDataTO.setUpdatedTerritoryCodes(lccMasterDataPublishRS.getUpdatedTerritoryCodes());
		masterDataPublishRSDataTO.setUpdatedCityCodes(lccMasterDataPublishRS.getUpdatedCityCodes());
		if (lccMasterDataPublishRS.getResponseAttributes().getErrors() != null) {
			for (Iterator<LCCError> iterator = lccMasterDataPublishRS.getResponseAttributes().getErrors().iterator(); iterator
					.hasNext();) {
				LCCError lccError = iterator.next();
				masterDataPublishRSDataTO.getErrors().add(lccError.getErrorCode() + " [" + lccError.getDescription() + " ]");
			}
		}
		return masterDataPublishRSDataTO;
	}

}
