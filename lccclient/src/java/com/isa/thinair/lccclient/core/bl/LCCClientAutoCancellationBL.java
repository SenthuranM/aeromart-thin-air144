package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAutoCancellationBalanceToPayRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAutoCancellationBalanceToPayRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCExecuteAutoCancellationRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * BL to execute auto cancellation for interline/dry reservations
 * 
 * @author rumesh
 * 
 */

public class LCCClientAutoCancellationBL {
	private static final Log log = LogFactory.getLog(LCCClientAutoCancellationBL.class);

	public static boolean excuteAutoCancellation(UserPrincipal userPrincipal) throws ModuleException {
		try {
			LCCExecuteAutoCancellationRQ lccExecuteAutoCancellationRQ = new LCCExecuteAutoCancellationRQ();
			lccExecuteAutoCancellationRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccExecuteAutoCancellationRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, null));
			LCConnectorUtils.getMaxicoExposedWS().executeAutoCancellation(lccExecuteAutoCancellationRQ);
		} catch (Exception e) {
			log.error("Exception caught in interline auto cancellation", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return false;
	}

	public static BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resTO,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) throws ModuleException {

		try {
			LCCAutoCancellationBalanceToPayRQ balanceToPayRQ = populateAutoCnxBalanceToPayRQ(resTO, trackInfoDTO, userPrincipal);
			LCCAutoCancellationBalanceToPayRS balanceToPayRS = LCConnectorUtils.getMaxicoExposedWS()
					.calculateAutoCancellationBalanceToPay(balanceToPayRQ);

			if (balanceToPayRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(balanceToPayRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			return balanceToPayRS.getBalanceToPay();
		} catch (Exception e) {
			log.error("Exception caught in interline auto cancellation balance to pay calculation", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}

	private static LCCAutoCancellationBalanceToPayRQ populateAutoCnxBalanceToPayRQ(LCCClientResAlterQueryModesTO resTO,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) {
		LCCAutoCancellationBalanceToPayRQ balanceToPayRQ = new LCCAutoCancellationBalanceToPayRQ();

		balanceToPayRQ.setGroupPnr(resTO.getGroupPNR());
		balanceToPayRQ.setVersion(resTO.getVersion());

		if (resTO.getOldPnrSegs() != null && !resTO.getOldPnrSegs().isEmpty()) {
			for (LCCClientReservationSegment lccResSeg : resTO.getOldPnrSegs()) {
				String carrier = FlightRefNumberUtil.getOperatingAirline(lccResSeg.getFlightSegmentRefNumber());
				StringStringMap carrierPnrSegObj = new StringStringMap();
				carrierPnrSegObj.setKey(carrier);
				carrierPnrSegObj.setValue(lccResSeg.getBookingFlightSegmentRefNumber());
				balanceToPayRQ.getCarrierCancelingSegments().add(carrierPnrSegObj);
			}
		}

		if (resTO.getPaxInfantList() != null && !resTO.getPaxInfantList().isEmpty()) {
			balanceToPayRQ.getRemovingInfants().addAll(resTO.getPaxInfantList());
		}

		balanceToPayRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		balanceToPayRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));

		return balanceToPayRQ;
	}
}
