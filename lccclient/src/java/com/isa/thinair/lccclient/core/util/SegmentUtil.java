package com.isa.thinair.lccclient.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;

public class SegmentUtil {

	private final static String DELIM = "$";
	private final static String SPLIT_DELIM = "\\$";

	/**
	 * Given the LCCClientReservationSegment return the Interline compatible RPH
	 * 
	 * @param flightSegment
	 * @param segmentId
	 * @return String flightRPH
	 * @see #rphBuilder(String, String, String, Date, Date)
	 */
	public static String composeFlightRPH(FlightSegment flightSegment) {
		String operatingAirlineCode = flightSegment.getOperatingAirline();
		String segmentCode = flightSegment.getSegmentCode();
		Date dDate = flightSegment.getDepatureDateTime();
		Date aDate = flightSegment.getArrivalDateTime();
		String segId = flightSegment.getFlightRefNumber() + "";
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);

	}

	/**
	 * sample format G9$SHJ/SAW$13123$20091011123501$20091012123501$
	 * 
	 * @param operatingAirlineCode
	 * @param segmentCode
	 * @param segmentId
	 * @param dDate
	 * @param aDate
	 * @return
	 */
	private static String rphBuilder(String operatingAirlineCode, String segmentCode, String segmentId, Date dDate, Date aDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		String depatureDateTime = format.format(dDate);
		String arrivalDateTime = format.format(aDate);
		String fltRPH = operatingAirlineCode + DELIM + segmentCode + DELIM + segmentId + DELIM + depatureDateTime + DELIM
				+ arrivalDateTime + DELIM + "";
		return fltRPH;
	}

}
