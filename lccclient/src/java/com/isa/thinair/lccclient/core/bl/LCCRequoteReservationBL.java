/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.lccclient.core.bl;

/**
 * @author Jagath Kumara
 */
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.log4j.Logger;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseFlightRPH;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientExternalChg;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientPaymentAssembler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredJourneyDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerPaxNameInfoMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerStringMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerPaymentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerRefPaymentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxNameInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRequoteRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRequoteRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResRequoteModesRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResRequoteModesRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

//import com.isa.thinair.airproxy.core.utils.transformer.ReservationConvertUtil;

public class LCCRequoteReservationBL {

	private static final Logger LOGGER = Logger.getLogger(LCCRequoteReservationBL.class);

	public static ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ lccClientResRequoteQueryTO,
			TrackInfoDTO trackInfo, UserPrincipal userPrincipal) throws ModuleException {

		LCCAirBookRequoteRQ lccAirBookRequoteRQ = new LCCAirBookRequoteRQ();
		lccAirBookRequoteRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(lccClientResRequoteQueryTO
				.getTransactionIdentifier()));
		lccAirBookRequoteRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookRequoteRQ.setVersion(lccClientResRequoteQueryTO.getVersion());
		lccAirBookRequoteRQ.setGroupPnr(lccClientResRequoteQueryTO.getGroupPnr());
		lccAirBookRequoteRQ.setFqWithinValidity(lccClientResRequoteQueryTO.isFQWithinValidity());
		lccAirBookRequoteRQ.setLastFareQuotedDate(lccClientResRequoteQueryTO.getLastFareQuoteDate());
		lccAirBookRequoteRQ.setSameFlightModification(lccClientResRequoteQueryTO.isSameFlightModification());
		lccAirBookRequoteRQ.getRemovedSegIds().addAll(lccClientResRequoteQueryTO.getRemovedSegmentIds());
		lccAirBookRequoteRQ.getRequoteSegmentMap().addAll(
				composeRequoteSegmentMap(lccClientResRequoteQueryTO.getRequoteSegmentMap()));
		if (lccClientResRequoteQueryTO.getExcludedSegFarePnrSegIds() != null) {
			lccAirBookRequoteRQ.getExcludedSegFarePnrSegIds().addAll(lccClientResRequoteQueryTO.getExcludedSegFarePnrSegIds());
		}
		lccAirBookRequoteRQ.getGroundFltSegByFltSegIdMap().addAll(
				composeGroundSegIDByAirSegID(lccClientResRequoteQueryTO.getGroundFltSegByFltSegId()));
		lccAirBookRequoteRQ.getPaxExtChgMap().addAll(
				composePassengerExternalChargeMap(lccClientResRequoteQueryTO.getPaxExtChgMap(), new Date()));
		lccAirBookRequoteRQ.getCarrierWiseFlightRPHList().addAll(
				composeCarrierWiseFlightRPHList(lccClientResRequoteQueryTO.getCarrierWiseFlightRPHMap()));
		lccAirBookRequoteRQ.getOldFareIdFltSegIdMap().addAll(
				composeOldFareIdFltSegIdMap(lccClientResRequoteQueryTO.getOldFareIdByFltSegIdMap()));
		lccAirBookRequoteRQ.getOndFareTypeFareIdMap().addAll(
				composeOndFareTypeFareIdMap(lccClientResRequoteQueryTO.getOndFareTypeByFareIdMap()));
		lccAirBookRequoteRQ.setChargeOverride(getChargeOverride(lccClientResRequoteQueryTO.getCustomCharges()));
		lccAirBookRequoteRQ.getPaxWiseAdjustmentAmount().addAll(
				composePaxWiseAdjustmentAmount(lccClientResRequoteQueryTO.getPaxWiseAdjustmentAmountMap()));

		lccAirBookRequoteRQ.getNameChangedPaxMap().addAll(
				composePaxWiseNameInfo(lccClientResRequoteQueryTO.getNameChangedPaxMap()));
		lccAirBookRequoteRQ.setSkipDuplicateNameCheck(lccClientResRequoteQueryTO.isDuplicateNameCheck());
		lccAirBookRequoteRQ.setRemainingLoyaltyPoints(lccClientResRequoteQueryTO.getRemainingLoyaltyPoints());

		LCCAirBookRequoteRS lccAirBookRequoteRS = LCConnectorUtils.getMaxicoExposedWS().requoteReservationQuery(
				lccAirBookRequoteRQ);
		return Transformer.transformBalances(lccAirBookRequoteRS);
	}

	private static ChargerOverride getChargeOverride(CustomChargesTO customCharges) {
		return LCCReservationUtil.createChargeOverride(customCharges, null);
	}

	private static List<PerPaxExternalCharges> composePassengerExternalChargeMap(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setUserNote(lccClientExternalChgDTO.getUserNote());
				externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
				externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
				externalCharge.setOperationMode(lccClientExternalChgDTO.getOperationMode());

				if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
					Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
							.getFlexiBilities();
					Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
					for (FlexiInfoTO flexiInfoTO : flexiBilities) {
						FlexiInfo flexiInfo = new FlexiInfo();
						flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
						flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
						flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
						flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
						flexiInfos.add(flexiInfo);
					}
					externalCharge.getAdditionalDetails().addAll(flexiInfos);
				}

				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}

	private static List<CarrierWiseFlightRPH> composeCarrierWiseFlightRPHList(Map<String, List<String>> carrierWiseFlightRPHMap) {
		List<CarrierWiseFlightRPH> carrierWiseFlightRPHList = new ArrayList<CarrierWiseFlightRPH>();
		for (String carrierCode : carrierWiseFlightRPHMap.keySet()) {
			CarrierWiseFlightRPH carrierWiseFlightRPH = new CarrierWiseFlightRPH();
			FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();
			flightRefNumberRPHList.getFlightRefNumber().addAll(carrierWiseFlightRPHMap.get(carrierCode));

			carrierWiseFlightRPH.setCarrierCode(carrierCode);
			carrierWiseFlightRPH.setFlightRefNumberRPHList(flightRefNumberRPHList);
			carrierWiseFlightRPHList.add(carrierWiseFlightRPH);
		}

		return carrierWiseFlightRPHList;
	}

	private static List<IntegerIntegerMap> composeOldFareIdFltSegIdMap(Map<Integer, Integer> oldFareIdFltSegIdMap) {
		List<IntegerIntegerMap> oldFareIdFltSegIdList = new ArrayList<IntegerIntegerMap>();
		for (Integer key : oldFareIdFltSegIdMap.keySet()) {
			IntegerIntegerMap fareIdFltSegIdMap = new IntegerIntegerMap();
			fareIdFltSegIdMap.setKey(key);
			fareIdFltSegIdMap.setValue(oldFareIdFltSegIdMap.get(key));
			oldFareIdFltSegIdList.add(fareIdFltSegIdMap);
		}
		return oldFareIdFltSegIdList;
	}

	private static List<IntegerStringMap> composeOndFareTypeFareIdMap(Map<Integer, String> ondFareIdFareTypeMap) {
		List<IntegerStringMap> ondFareTypeFareIdList = new ArrayList<IntegerStringMap>();
		for (Integer key : ondFareIdFareTypeMap.keySet()) {
			IntegerStringMap fareTypeFareIdMap = new IntegerStringMap();
			fareTypeFareIdMap.setKey(key);
			fareTypeFareIdMap.setValue(ondFareIdFareTypeMap.get(key));
			ondFareTypeFareIdList.add(fareTypeFareIdMap);
		}
		return ondFareTypeFareIdList;
	}

	private static List<StringStringMap> composeRequoteSegmentMap(Map<String, String> requoteSegmentMap) {
		List<StringStringMap> requoteSegmentMapList = new ArrayList<StringStringMap>();
		for (String key : requoteSegmentMap.keySet()) {
			StringStringMap fltSegIdPnrSegIdMap = new StringStringMap();
			fltSegIdPnrSegIdMap.setKey(key);
			fltSegIdPnrSegIdMap.setValue(requoteSegmentMap.get(key));
			requoteSegmentMapList.add(fltSegIdPnrSegIdMap);
		}
		return requoteSegmentMapList;
	}

	private static List<StringStringMap> composeGroundSegIDByAirSegID(Map<String, String> groundFltSegByFltSegIdMap) {
		List<StringStringMap> requoteSegmentMapList = new ArrayList<StringStringMap>();
		if (groundFltSegByFltSegIdMap != null && groundFltSegByFltSegIdMap.size() > 0) {
			for (String key : groundFltSegByFltSegIdMap.keySet()) {
				StringStringMap fltSegIdGroundSegIdMap = new StringStringMap();
				fltSegIdGroundSegIdMap.setKey(key);
				fltSegIdGroundSegIdMap.setValue(groundFltSegByFltSegIdMap.get(key));
				requoteSegmentMapList.add(fltSegIdGroundSegIdMap);
			}
		}
		return requoteSegmentMapList;
	}

	private static List<IntegerDecimalMap> composePaxWiseAdjustmentAmount(Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap) {
		List<IntegerDecimalMap> requoteSegmentMapList = new ArrayList<IntegerDecimalMap>();
		if (paxWiseAdjustmentAmountMap != null && paxWiseAdjustmentAmountMap.size() > 0) {
			for (Integer key : paxWiseAdjustmentAmountMap.keySet()) {
				IntegerDecimalMap paxWiseDueAmount = new IntegerDecimalMap();
				paxWiseDueAmount.setKey(key);
				paxWiseDueAmount.setValue(paxWiseAdjustmentAmountMap.get(key));
				requoteSegmentMapList.add(paxWiseDueAmount);
			}
		}
		return requoteSegmentMapList;
	}

	public static ServiceResponce lccRequoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal, SessionContext sessionContext) throws ModuleException {

		PaymentAssembler paymentAssembler = new PaymentAssembler();
		String groupPnr = requoteModifyRQ.getGroupPnr();
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();

		boolean performReversal = false;

		CommonReservationContactInfo contactInfo = requoteModifyRQ.getContactInfo();
		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(contactInfo);

		try {
			LCCClientPaymentWorkFlow.setLccUniqueTnxId(requoteModifyRQ.getLccPassengerPayments().values());
			for (LCCClientPaymentAssembler clientPaymentAsm : requoteModifyRQ.getLccPassengerPayments().values()) {
				LCCClientPaymentWorkFlow.transform(paymentAssembler, clientPaymentAsm);
			}

			CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfo, userPrincipal);

			Map<Integer, CardPaymentInfo> tempPaymentMap = LCCClientPaymentWorkFlow.populateTemPaymentEntries(requoteModifyRQ
					.getLccTemporaryTnxMap());

			Map<Integer, LCCClientPaymentAssembler> lccpaymentAssemblerMap = new HashMap<Integer, LCCClientPaymentAssembler>();

			if (paymentAssembler.getPayments().size() > 0) {
				ReservationModuleUtils.getReservationBD().getPaymentInfo(paymentAssembler, groupPnr);
				// Make the temporary payment entry
				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
						tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				ServiceResponce response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfo, paymentAssembler,
						groupPnr, transformContactInfo, tempTnxIds);
				performReversal = true;

				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);

				for (String travelerRefNumber : requoteModifyRQ.getLccPassengerPayments().keySet()) {
					Integer paxSeq = PaxTypeUtils.getPaxSeq(travelerRefNumber);
					lccpaymentAssemblerMap.put(paxSeq, requoteModifyRQ.getLccPassengerPayments().get(travelerRefNumber));
				}

				LCCReservationUtil.combineActualCreditPayments(lccpaymentAssemblerMap, paxCreditPayments, paymentAuthIdMap,
						paymentBrokerRefMap);

			}

			LCCResRequoteModesRQ lccResRequoteModesRQ = new LCCResRequoteModesRQ();

			lccResRequoteModesRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(requoteModifyRQ.getTransactionIdentifier()));
			lccResRequoteModesRQ.setGroupPnr(groupPnr);
			lccResRequoteModesRQ.setVersion(requoteModifyRQ.getVersion());
			lccResRequoteModesRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

			lccResRequoteModesRQ.setPaymentType(requoteModifyRQ.getPaymentType());
			lccResRequoteModesRQ.setUserNotes(requoteModifyRQ.getUserNotes());
			lccResRequoteModesRQ.setUserNoteType(requoteModifyRQ.getUserNoteType());
			lccResRequoteModesRQ.setEndorsementNotes(requoteModifyRQ.getEndorsementNotes());
			lccResRequoteModesRQ.setLastModificationTimestamp(requoteModifyRQ.getLastModificationTimestamp());
			lccResRequoteModesRQ.setLastCurrencyCode(requoteModifyRQ.getLastCurrencyCode());
			lccResRequoteModesRQ.setReleaseTimeStampZulu(requoteModifyRQ.getReleaseTimeStampZulu());
			lccResRequoteModesRQ.setHasBufferTimeAutoCnxPrivilege(requoteModifyRQ.isHasBufferTimeAutoCnxPrivilege());
			lccResRequoteModesRQ.setActualPayment(requoteModifyRQ.isActualPayment());
			lccResRequoteModesRQ.setAutoCancellationEnabled(requoteModifyRQ.isAutoCancellationEnabled());
			lccResRequoteModesRQ.getRemovedSegIds().addAll(requoteModifyRQ.getRemovedSegmentIds());
			lccResRequoteModesRQ.getRequoteSegmentMap().addAll(composeRequoteSegmentMap(requoteModifyRQ.getRequoteSegmentMap()));
			lccResRequoteModesRQ.getGroundFltSegByFltSegIdMap().addAll(
					composeGroundSegIDByAirSegID(requoteModifyRQ.getGroundFltSegByFltSegId()));
			lccResRequoteModesRQ.getPaxWiseAdjustmentAmount().addAll(
					composePaxWiseAdjustmentAmount(requoteModifyRQ.getPaxWiseAdjustmentAmountMap()));

			lccResRequoteModesRQ.setLastFareQuotedDate(requoteModifyRQ.getLastFareQuoteDate());
			lccResRequoteModesRQ.setFqWithinValidity(requoteModifyRQ.isFQWithinValidity());
			lccResRequoteModesRQ.setReservationStatus(requoteModifyRQ.getReservationStatus());
			lccResRequoteModesRQ.getFltRefOndBaggageGroup().addAll(
					Transformer.convertToLccStringMap(requoteModifyRQ.getFlightRefWiseOndBaggageGroup()));


			lccResRequoteModesRQ.getPaxExtChgMap().addAll(
					composePassengerExternalChargeMap(requoteModifyRQ.getPaxExtChgMap(), new Date()));
			lccResRequoteModesRQ.getCarrierWiseFlightRPHList().addAll(
					composeCarrierWiseFlightRPHList(requoteModifyRQ.getCarrierWiseFlightRPHMap()));
			lccResRequoteModesRQ.getOldFareIdFltSegIdMap().addAll(
					composeOldFareIdFltSegIdMap(requoteModifyRQ.getOldFareIdByFltSegIdMap()));
			lccResRequoteModesRQ.getOndFareTypeFareIdMap().addAll(
					composeOndFareTypeFareIdMap(requoteModifyRQ.getOndFareTypeByFareIdMap()));

			lccResRequoteModesRQ.getLccTemporaryTnxMap().addAll(
					LCCClientCommonUtils.createLCCTmpPaymentMap(requoteModifyRQ.getLccTemporaryTnxMap()));
			lccResRequoteModesRQ.setChargeOverride(getChargeOverride(requoteModifyRQ.getCustomCharges()));

			if (requoteModifyRQ.getExternalChargesMap() != null) {
				lccResRequoteModesRQ.getExternalCharge().addAll(composeExternalCharges(requoteModifyRQ.getExternalChargesMap()));
			}

			lccResRequoteModesRQ.getPassengerRefPaymentMap().addAll(
					composePassengerReferenceWisePaymentMap(requoteModifyRQ.getLccPassengerPayments(),
							requoteModifyRQ.getPaxExtChgMap()));

			if (requoteModifyRQ.getInsurances() != null && !requoteModifyRQ.getInsurances().isEmpty()) {
				List<InsuranceRequest> insuranceRequests = composeInsuranceRequest(requoteModifyRQ.getInsurances());
				lccResRequoteModesRQ.setInsuranceRequest(insuranceRequests.get(0));
			}

			boolean isFirstPayment = (lccResRequoteModesRQ.getPaymentType() == ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT
					.intValue() && lccResRequoteModesRQ.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD));

			lccResRequoteModesRQ.setFulfillment(composeFulfillment(lccpaymentAssemblerMap, userPrincipal, groupPnr, isFirstPayment));

			lccResRequoteModesRQ.setContactInfo(LCCClientCommonUtils.createContactInfo(transformContactInfo));
			lccResRequoteModesRQ.getNameChangedPaxMap().addAll(composePaxWiseNameInfo(requoteModifyRQ.getNameChangedPaxMap()));
			lccResRequoteModesRQ.setSkipDuplicateNameCheck(requoteModifyRQ.isDuplicateNameCheck());
			if(!StringUtil.isNullOrEmpty(requoteModifyRQ.getReasonForAllowBlPax())){
				lccResRequoteModesRQ.setReasonForAllowBlPax(requoteModifyRQ.getReasonForAllowBlPax());
			}
			LCCResRequoteModesRS lccResRequoteModesRS = LCConnectorUtils.getMaxicoExposedWS().lccRequoteModifySegments(
					lccResRequoteModesRQ);

			if (lccResRequoteModesRS.getResponseAttributes().getSuccess() != null) {
				LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);
			}
		} catch (Exception ex) {
			LOGGER.error("Error in lccRequoteModifySegments", ex);
			if (performReversal) {
				sessionContext.setRollbackOnly();
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE,
						"Payment failed for lccRequoteModifySegments", null, null, null);
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);
				if (!paymentAssembler.getPayments().isEmpty()) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfo, paymentAssembler, groupPnr,
							transformContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for lccRequoteModifySegments", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return null;

	}

	private static List<PassengerPaymentMap>
			composePassengerPaymentMap(Map<String, LCCClientPaymentAssembler> lccPassengerPayments,
					Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap) {

		List<PassengerPaymentMap> passengerPaymentMap = new ArrayList<PassengerPaymentMap>();

		for (String travelerRefNumber : lccPassengerPayments.keySet()) {

			Integer paxSeq = PaxTypeUtils.getPaxSeq(travelerRefNumber);
			LCCClientPaymentAssembler lccClientPaymentAssembler = lccPassengerPayments.get(travelerRefNumber);
			Collection<LCCClientExternalChgDTO> externalCharges = paxExtChgMap.get(paxSeq);
			BigDecimal totalPayAmount = lccClientPaymentAssembler.getTotalPayAmount();
			boolean isPaymentExist = lccClientPaymentAssembler.isPaymentExist();

			ClientPaymentAssembler clientPaymentAssembler = new ClientPaymentAssembler();
			if (lccClientPaymentAssembler.getPaxType() != null) {
				clientPaymentAssembler.setPaxType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(lccClientPaymentAssembler
						.getPaxType()));
			}
			clientPaymentAssembler.setPaymentExist(isPaymentExist);
			clientPaymentAssembler.setTotalAmount(totalPayAmount);

			for (LCCClientExternalChgDTO externalChgDTO : (ArrayList<LCCClientExternalChgDTO>) externalCharges) {
				ClientExternalChg clientExternalChg = new ClientExternalChg();
				clientExternalChg.setAmount(externalChgDTO.getAmount());
				clientExternalChg.setCarrierCode(externalChgDTO.getCarrierCode());
				clientExternalChg.setCode(externalChgDTO.getCode());
				clientExternalChg.setExternalCharges(externalChgDTO.getExternalCharges().toString());
				clientExternalChg.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
				clientExternalChg.setIsAmountConsumedForPayment(externalChgDTO.isAmountConsumedForPayment());
				clientExternalChg.setAirportCode(externalChgDTO.getAirportCode());
				clientPaymentAssembler.getPerPaxExternalCharges().add(clientExternalChg);
			}

			PassengerPaymentMap paymentMap = new PassengerPaymentMap();
			paymentMap.setKey(paxSeq);
			paymentMap.setValue(clientPaymentAssembler);

			passengerPaymentMap.add(paymentMap);
		}

		return passengerPaymentMap;
	}

	private static List<PassengerRefPaymentMap>
			composePassengerReferenceWisePaymentMap(Map<String, LCCClientPaymentAssembler> lccPassengerPayments,
					Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap) {

		List<PassengerRefPaymentMap> passengerPaymentMap = new ArrayList<PassengerRefPaymentMap>();

		for (String travelerRefNumber : lccPassengerPayments.keySet()) {

			LCCClientPaymentAssembler lccClientPaymentAssembler = lccPassengerPayments.get(travelerRefNumber);
			BigDecimal totalPayAmount = lccClientPaymentAssembler.getTotalPayAmount();
			boolean isPaymentExist = lccClientPaymentAssembler.isPaymentExist();

			ClientPaymentAssembler clientPaymentAssembler = new ClientPaymentAssembler();
			if (lccClientPaymentAssembler.getPaxType() != null) {
				clientPaymentAssembler.setPaxType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(lccClientPaymentAssembler
						.getPaxType()));
			}
			clientPaymentAssembler.setPaymentExist(isPaymentExist);
			clientPaymentAssembler.setTotalAmount(totalPayAmount);
			Collection<LCCClientExternalChgDTO> perPaxExternalCharges = lccClientPaymentAssembler.getPerPaxExternalCharges();
			for (LCCClientExternalChgDTO externalChgDTO : perPaxExternalCharges) {
				ClientExternalChg clientExternalChg = new ClientExternalChg();
				clientExternalChg.setAmount(externalChgDTO.getAmount());
				if (externalChgDTO.getCarrierCode() == null) {
					clientExternalChg.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					clientExternalChg.setCarrierCode(externalChgDTO.getCarrierCode());
				}
				clientExternalChg.setCode(externalChgDTO.getCode());
				clientExternalChg.setExternalCharges(externalChgDTO.getExternalCharges().toString());
				clientExternalChg.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
				clientExternalChg.setIsAmountConsumedForPayment(externalChgDTO.isAmountConsumedForPayment());
				clientExternalChg.setAirportCode(externalChgDTO.getAirportCode());
				clientExternalChg.setOperationMode(externalChgDTO.getOperationMode());
				clientPaymentAssembler.getPerPaxExternalCharges().add(clientExternalChg);
			}

			PassengerRefPaymentMap paymentMap = new PassengerRefPaymentMap();
			paymentMap.setKey(travelerRefNumber);
			paymentMap.setValue(clientPaymentAssembler);

			passengerPaymentMap.add(paymentMap);
		}

		return passengerPaymentMap;
	}

	private static Fulfillment composeFulfillment(Map<Integer, LCCClientPaymentAssembler> lccpaymentAssemblerMap,
			UserPrincipal userPrincipal, String groupPnr, boolean isFirstPayment) throws ModuleException {
		Fulfillment fulfillment = new Fulfillment();
		if (lccpaymentAssemblerMap.size() > 0) {
			fulfillment.getCustomerFulfillments().addAll(
					LCCReservationUtil.parseTravelerFulfillments(lccpaymentAssemblerMap, userPrincipal, groupPnr, false, isFirstPayment));
		}
		return fulfillment;
	}

	private static List<InsuranceRequest> composeInsuranceRequest(List<IInsuranceRequest> insurance) throws ModuleException {

		List<InsuranceRequest> insuranceRequests = new ArrayList<InsuranceRequest>();
		InsuranceRequest insuranceRequest = null;

		for (IInsuranceRequest insReq : insurance) {
			insuranceRequest = new InsuranceRequest();
			InsuranceRequestAssembler insuranceReq = (InsuranceRequestAssembler) insReq;
			insuranceRequest.setAmount(insuranceReq.getTotalPremiumAmount());
			insuranceRequest.setApplicablePaxCount(insuranceReq.getNoOfPax());
			InsureSegment insureSegment = insuranceReq.getInsureSegment();
			insuranceRequest.setDestination(insureSegment.getToAirportCode());
			insuranceRequest.setOrigin(insureSegment.getFromAirportCode());
			insuranceRequest.setOperatingCarrierCode(insuranceReq.getOperatingAirline());
			insuranceRequest.setDateOfTravel(insureSegment.getDepartureDate());
			insuranceRequest.setDateOfReturn(insureSegment.getArrivalDate());
			insuranceRequest.setInsuranceRefNumber(insuranceReq.getInsuranceId());
			insuranceRequest.setSsrFeeCode(insuranceReq.getSsrFeeCode());
			insuranceRequest.setPlanCode(insuranceReq.getPlanCode());

			InsuredJourneyDetails insuredJourney = new InsuredJourneyDetails();
			insuredJourney.setJourneyStartAirportCode(insureSegment.getFromAirportCode());
			insuredJourney.setJourneyEndAirportCode(insureSegment.getToAirportCode());
			insuredJourney.setJourneyStartDate(insureSegment.getDepartureDate());
			insuredJourney.setJourneyEndDate(insureSegment.getArrivalDate());
			insuredJourney.setJourneyStartDateOffset(insureSegment.getDepartureDateOffset());
			insuredJourney.setJourneyEndDateOffset(insureSegment.getDepartureDateOffset());
			insuredJourney.setRoundTrip(insureSegment.isRoundTrip());

			insuranceRequest.setInsuredJourneyDetails(insuredJourney);
			insuranceRequests.add(insuranceRequest);
		}

		return insuranceRequests;
	}

	private static List<ExternalCharge> composeExternalCharges(Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap) {
		List<ExternalCharge> lstExternalCharge = new ArrayList<ExternalCharge>();

		for (EXTERNAL_CHARGES ext_chg : externalChargesMap.keySet()) {
			ExternalChgDTO externalChgDTO = externalChargesMap.get(ext_chg);
			ExternalCharge externalCharge = new ExternalCharge();

			externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(ext_chg));
			externalCharge.setAmount(externalChgDTO.getAmount());
			externalCharge.setApplicableDateTime(new Date());
			externalCharge.setJourneyType(externalChgDTO.getJourneyType());
			externalCharge.setCode(externalChgDTO.getChargeCode());
			// TODO check additional info required or not

			lstExternalCharge.add(externalCharge);
		}
		return lstExternalCharge;
	}

	private static List<IntegerPaxNameInfoMap> composePaxWiseNameInfo(Map<Integer, NameDTO> nameChangedPaxMap) {
		List<IntegerPaxNameInfoMap> requoteSegmentMapList = new ArrayList<IntegerPaxNameInfoMap>();
		if (nameChangedPaxMap != null && nameChangedPaxMap.size() > 0) {
			for (Integer pnrPaxID : nameChangedPaxMap.keySet()) {
				NameDTO nameDTO = nameChangedPaxMap.get(pnrPaxID);
				IntegerPaxNameInfoMap paxNameInfo = new IntegerPaxNameInfoMap();
				paxNameInfo.setKey(pnrPaxID);
				PaxNameInfo paxInfo = new PaxNameInfo();
				paxInfo.setTitle(nameDTO.getTitle());
				paxInfo.setFirstName(nameDTO.getFirstname());
				paxInfo.setLastName(nameDTO.getLastName());
				paxInfo.setFfid(nameDTO.getFFID());
				paxInfo.setPnrPaxId(nameDTO.getPnrPaxId());
				paxInfo.setPaxReference(nameDTO.getPaxReference());
				paxNameInfo.setValue(paxInfo);
				requoteSegmentMapList.add(paxNameInfo);

			}
		}

		return requoteSegmentMapList;
	}

}
