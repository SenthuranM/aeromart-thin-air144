package com.isa.thinair.lccclient.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRSDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRSDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRSDataTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.core.bl.LCCMasterDataPublisherBL;
import com.isa.thinair.lccclient.core.bl.LCCOndPublisherBL;
import com.isa.thinair.lccclient.core.bl.LCCRouteInfoPublisherBL;
import com.isa.thinair.lccclient.core.bl.LCCUserInfoPublisherBL;
import com.isa.thinair.lccclient.core.service.bd.LCCMasterDataPublisherBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCMasterDataPublisherBDRemote;

@Stateless
@RemoteBinding(jndiBinding = "LCCMasterDataPublisherSevice.remote")
@LocalBinding(jndiBinding = "LCCMasterDataPublisherSevice.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCMasterDataPublisherSeviceBean extends PlatformBaseSessionBean implements LCCMasterDataPublisherBDLocal,
		LCCMasterDataPublisherBDRemote {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RouteInfoPublishRSDataTO publishRouteInfoToLCC(RouteInfoPublishRQDataTO routeInfoRS) throws ModuleException {
		LCCRouteInfoPublisherBL routeInfoPublisherBL = new LCCRouteInfoPublisherBL(routeInfoRS);
		return routeInfoPublisherBL.execute();
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public MasterDataPublishRSDataTO publishMasterDataToLCC(MasterDataPublishRQDataTO masterDataPublishRQDataTO)
			throws ModuleException {
		LCCMasterDataPublisherBL lccMasterDataPublisherBL = new LCCMasterDataPublisherBL(masterDataPublishRQDataTO);
		return lccMasterDataPublisherBL.execute();
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Boolean publishOndCombinationsViaLCC(String invokingCarrierCode) throws ModuleException {
		LCCOndPublisherBL lccOndPublisherBL = new LCCOndPublisherBL(invokingCarrierCode);
		return lccOndPublisherBL.execute();
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public UserInfoPublishRSDataTO publishUserInfoToLCC(UserInfoPublishRQDataTO userInfoPublishRQDataTO) throws ModuleException {
		LCCUserInfoPublisherBL lccUserInfoPublisherBL = new LCCUserInfoPublisherBL(userInfoPublishRQDataTO);
		return lccUserInfoPublisherBL.execute();
	}

}
