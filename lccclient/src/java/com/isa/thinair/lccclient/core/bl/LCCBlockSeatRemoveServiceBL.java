package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCBlockSeatRQ;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCBlockSeatRemoveServiceBL {
	private static final Log log = LogFactory.getLog(LCCBlockSeatRemoveServiceBL.class);

	private String transactionID;

	private UserPrincipal userPrincipal;

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public void execute(BasicTrackInfo trackInfo) {
		LCCBlockSeatRQ lccBlockSeatRQ = new LCCBlockSeatRQ();
		lccBlockSeatRQ.setTransactionID(transactionID);
		lccBlockSeatRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		try {
			LCConnectorUtils.getMaxicoExposedWS().releaseBlockSeat(lccBlockSeatRQ);
		} catch (ModuleException e) {
			log.error("Block seat removal failed");
		}
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
}
