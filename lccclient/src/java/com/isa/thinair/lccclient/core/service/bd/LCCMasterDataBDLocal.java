package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.lccclient.api.service.LCCMasterDataBD;

@Local
public interface LCCMasterDataBDLocal extends LCCMasterDataBD {
}
