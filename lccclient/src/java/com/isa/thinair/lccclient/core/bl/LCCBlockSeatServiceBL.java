package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCBlockSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCBlockSeatRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.InterlineBlockSeatRQInfoTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCBlockSeatServiceBL {
	private static final Log log = LogFactory.getLog(LCCBlockSeatServiceBL.class);

	private InterlineBlockSeatRQInfoTO interlineBlockSeatRQInfoTO;

	private UserPrincipal userPrincipal;

	public boolean execute(BasicTrackInfo trackInfo) {
		try {
			LCCBlockSeatRQ lccBlockSeatRQ = new LCCBlockSeatRQ();
			lccBlockSeatRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccBlockSeatRQ.setTransactionID(interlineBlockSeatRQInfoTO.getTransactionIdentifier());
			lccBlockSeatRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(interlineBlockSeatRQInfoTO
					.getTransactionIdentifier()));

			LCCBlockSeatRS lccBlockSeatRS = LCConnectorUtils.getMaxicoExposedWS().blockSeat(lccBlockSeatRQ);
			if (lccBlockSeatRS.getResponseAttributes().getSuccess() == null
					|| lccBlockSeatRS.getResponseAttributes().getErrors().size() > 0) {
				log.error("Block seat failed");
				throw new ModuleException("Block seat failed");
			}
			return true;
		} catch (Exception ex) {
			log.error("LCC invocation error for seat blocking");
			return false;
		}
	}

	public void setInterlineBlockSeatRQInfoTO(InterlineBlockSeatRQInfoTO interlineBlockSeatRQInfoTO) {
		this.interlineBlockSeatRQInfoTO = interlineBlockSeatRQInfoTO;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

}
