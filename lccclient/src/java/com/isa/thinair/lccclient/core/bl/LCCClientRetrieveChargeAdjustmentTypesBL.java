package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseChargeAdjustmentTypes;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentType;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCRetrieveChargeAdjustmentTypesRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCRetrieveChargeAdjustmentTypesRS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * LCC client logic for retrieving the charge adjustment types of other carriers.
 * 
 * @author sanjaya
 */
public class LCCClientRetrieveChargeAdjustmentTypesBL {

	/** Class Logger */
	private static final Log log = LogFactory.getLog(LCCClientRetrieveChargeAdjustmentTypesBL.class);

	/**
	 * 
	 * @param carrierCodes
	 *            : The set of carrier codes from which the charge adjustment types should be retrieved.
	 * @param userPrincipal
	 *            : The {@link UserPrincipal} object
	 * @param trackInfo
	 *            : The {@link BasicTrackInfo} object
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Collection<ChargeAdjustmentTypeDTO>> getCarrierWiseChargeAdjustmentTypes(Set<String> carrierCodes,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {

		try {

			Map<String, Collection<ChargeAdjustmentTypeDTO>> carrierWiseMap = new HashMap<String, Collection<ChargeAdjustmentTypeDTO>>();

			LCCRetrieveChargeAdjustmentTypesRQ retrieveChargeAdjustmentTypesRQ = new LCCRetrieveChargeAdjustmentTypesRQ();

			retrieveChargeAdjustmentTypesRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			retrieveChargeAdjustmentTypesRQ.getCarrierCodes().addAll(carrierCodes);

			LCCRetrieveChargeAdjustmentTypesRS retrieveChargeAdjustmentTypesRS = LCConnectorUtils.getMaxicoExposedWS()
					.retrieveChargeAdjustmentTypes(retrieveChargeAdjustmentTypesRQ);

			if (retrieveChargeAdjustmentTypesRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(retrieveChargeAdjustmentTypesRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			for (CarrierWiseChargeAdjustmentTypes chargeAdjustmentType : retrieveChargeAdjustmentTypesRS.getAdjustmentTypesMap()) {
				carrierWiseMap.put(chargeAdjustmentType.getCarrierCode(),
						transform(chargeAdjustmentType.getChargeAdjustmentTypes()));
			}

			return carrierWiseMap;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "lccclient.reservation.invocationError");
			}
		}
	}

	// Converts the adjustment types.
	private static Collection<ChargeAdjustmentTypeDTO> transform(List<ChargeAdjustmentType> chargeAdjustmentTypes) {
		Collection<ChargeAdjustmentTypeDTO> adjTypesCollection = new ArrayList<ChargeAdjustmentTypeDTO>();
		if (chargeAdjustmentTypes != null) {
			for (ChargeAdjustmentType adjType : chargeAdjustmentTypes) {
				adjTypesCollection.add(Transformer.transform(adjType));
			}
		}
		return adjTypesCollection;
	}
}
