package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LMSMember;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCMergeLoyaltymemberRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCMergeLoyaltymemberRS;
import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientMergeLoyaltyManagementBL {
	private static Log log = LogFactory.getLog(LCCClientMergeLoyaltyManagementBL.class);

	private String memberFFID;
	private BasicTrackInfo trackInfo;
	private UserPrincipal userPrincipal;

	public LCCClientMergeLoyaltyManagementBL(String memberFFID, BasicTrackInfo trackInfo, UserPrincipal userPrincipal) {
		this.memberFFID = memberFFID;
		this.trackInfo = trackInfo;
		this.userPrincipal = userPrincipal;
	}

	public LMSMemberDTO getMergeLoyaltyMember() throws ModuleException {
		LMSMemberDTO lmsMemberDTO = null;

		LCCMergeLoyaltymemberRQ lccMergeMemberRQ = populateMergeMemberRQ();

		try {
			LCCMergeLoyaltymemberRS lccResponse = LCConnectorUtils.getMaxicoExposedWS().getMergeLoyaltyMember(lccMergeMemberRQ);

			if (lccResponse.getResponseAttributes().getSuccess() != null && lccResponse.getLmsMember() != null) {
				LMSMember lccLmsMember = lccResponse.getLmsMember();
				lmsMemberDTO = new LMSMemberDTO();
				lmsMemberDTO.setDateOfBirth(lccLmsMember.getDateOfBirth());
				lmsMemberDTO.setEnrollmentLocExtRef(lccLmsMember.getEnrollmentLocation());
				lmsMemberDTO.setFfid(lccLmsMember.getFfid());
				lmsMemberDTO.setFirstName(lccLmsMember.getFirstName());
				lmsMemberDTO.setGenderTypeId(lccLmsMember.getGenderTypeId());
				lmsMemberDTO.setHeadOFEmailId(lccLmsMember.getHeadOfFamily());
				lmsMemberDTO.setLanguage(lccLmsMember.getLanguage());
				lmsMemberDTO.setLastName(lccLmsMember.getLastName());
				lmsMemberDTO.setLmsMemberId(lccLmsMember.getLmsMemberId());
				lmsMemberDTO.setMiddleName(lccLmsMember.getMiddleName());
				lmsMemberDTO.setMiddleName(lccLmsMember.getMobileNumber());
				lmsMemberDTO.setNationalityCode(lccLmsMember.getNationalityCode());
				lmsMemberDTO.setPassportNum(lccLmsMember.getPassportNum());
				lmsMemberDTO.setPassword(lccLmsMember.getPassword());
				lmsMemberDTO.setPhoneNumber(lccLmsMember.getPhoneNumber());
				lmsMemberDTO.setRefferedEmail(lccLmsMember.getRefereeEmail());
				lmsMemberDTO.setRegDate(lccLmsMember.getRegisteredDate());
				lmsMemberDTO.setSbInternalId(lccLmsMember.getSbInternalId());
				lmsMemberDTO.setMemberExternalId(lccLmsMember.getMemberExternalId());
				lmsMemberDTO.setEnrollingCarrier(lccLmsMember.getEnrollingCarrier());
				lmsMemberDTO.setEnrollingChannelExtRef(lccLmsMember.getEnrollingChannelExtRef());
				lmsMemberDTO.setEnrollingChannelIntRef(lccLmsMember.getEnrollingChannelIntRef());
				
//				lmsMemberDTO.setConfirmed(lccLmsMember.isConfirmed() ? 'Y' : 'N');
			}

		} catch (ModuleException ex) {
			log.error("Error in retrieving merge loyalty member", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return lmsMemberDTO;
	}

	private LCCMergeLoyaltymemberRQ populateMergeMemberRQ() {
		LCCMergeLoyaltymemberRQ lccMergeMemberRQ = new LCCMergeLoyaltymemberRQ();
		lccMergeMemberRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccMergeMemberRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccMergeMemberRQ.setMemberFFID(memberFFID);
		return lccMergeMemberRQ;
	}
}
