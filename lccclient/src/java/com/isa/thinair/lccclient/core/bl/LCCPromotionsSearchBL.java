package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicablePromotionDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PromotionSelectionCriteria;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPromoSelectionCriteriaRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPromoSelectionCriteriaRS;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

public class LCCPromotionsSearchBL {

	private static final Log log = LogFactory.getLog(LCCPromotionsSearchBL.class);

	private UserPrincipal userPrincipal;

	private PromoSelectionCriteria promoSelectionCriteria;

	public ApplicablePromotionDetailsTO execute(BasicTrackInfo trackInfo) throws ModuleException {

		LCCPromoSelectionCriteriaRQ lccPrmoSelCriteriaRQ = createLCCPrmotionSelectionCriteria(trackInfo);
		LCCPromoSelectionCriteriaRS applicablePromotionsRS = LCConnectorUtils.getMaxicoExposedWS().pickApplicablePromotions(
				lccPrmoSelCriteriaRQ);
		ApplicablePromotionDetails applicablePromotionDetails = applicablePromotionsRS.getApplicablePromotionDetails();
		return LCCClientCommonUtils.populateApplicablePromotionDetailsTO(applicablePromotionDetails);
	}

	private LCCPromoSelectionCriteriaRQ createLCCPrmotionSelectionCriteria(BasicTrackInfo trackInfo) {
		LCCPromoSelectionCriteriaRQ lccPromoSelectionCriteriaRQ = new LCCPromoSelectionCriteriaRQ();
		PromotionSelectionCriteria lccPromoSelectionCriteria = Transformer
				.transformFromPromoSelectionCriteria(promoSelectionCriteria);
		lccPromoSelectionCriteriaRQ.setPromoSelectionCriteria(lccPromoSelectionCriteria);
		lccPromoSelectionCriteriaRQ.setExecutingCarrierCode(promoSelectionCriteria.getDryOperatingAirline());
		lccPromoSelectionCriteriaRQ.setLccPos(LCCClientCommonUtils.createPOS(this.userPrincipal, trackInfo));
		return lccPromoSelectionCriteriaRQ;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public PromoSelectionCriteria getPromoSelectionCriteria() {
		return promoSelectionCriteria;
	}

	public void setPromoSelectionCriteria(PromoSelectionCriteria promoSelectionCriteria) {
		this.promoSelectionCriteria = promoSelectionCriteria;
	}

}
