package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRS;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCNewGroupPnrGeneratorBL {

	private static final Log log = LogFactory.getLog(LCCNewGroupPnrGeneratorBL.class);

	public static String generateNewPnr(String originatingCarrier, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {

		String groupPnr = null;

		LCCTicketingInfoRQ lccTicketingInfoRQ = new LCCTicketingInfoRQ();
		lccTicketingInfoRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccTicketingInfoRQ.setCarrierCode(originatingCarrier);

		LCCTicketingInfoRS lccTicketingInfoRS = LCConnectorUtils.getMaxicoExposedWS().getTicketingInfo(lccTicketingInfoRQ);
		if (lccTicketingInfoRS != null) {
			groupPnr = lccTicketingInfoRS.getGroupPnr();
		}
		log.error("LCC Group pnr generation fails");
		return groupPnr;
	}
}
