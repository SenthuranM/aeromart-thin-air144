package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCOndCombinationsPublishRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCOndCombinationsPublishRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCOndPublisherBL {

	private final static Log log = LogFactory.getLog(LCCOndPublisherBL.class);

	private String invokingCarrierCode;

	public LCCOndPublisherBL(String invokingCarrierCode) {
		this.invokingCarrierCode = invokingCarrierCode;
	}

	public Boolean execute() throws ModuleException {

		try {

			LCCOndCombinationsPublishRQ lccOndCombinationsPublishRQ = new LCCOndCombinationsPublishRQ();
			lccOndCombinationsPublishRQ.setInvokingCarrier(invokingCarrierCode);
			lccOndCombinationsPublishRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccOndCombinationsPublishRQ.setLccPos(LCCClientCommonUtils.createPOS(null));

			LCCOndCombinationsPublishRS lccOndCombinationsPublishRS = LCConnectorUtils.getMaxicoExposedWS()
					.publishOndCombinations(lccOndCombinationsPublishRQ);

			if (lccOndCombinationsPublishRS != null) {
				return processResponse(lccOndCombinationsPublishRS);
			} else {
				throw new ModuleException("lccclient.ondpublish.invocationError");
			}

		} catch (Exception ex) {
			log.error("Ond combinations publish error", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.ondpublish.invocationError", ex);
			}
		}
	}

	public void setInvokingCarrierCode(String invokingCarrierCode) {
		this.invokingCarrierCode = invokingCarrierCode;
	}

	private Boolean processResponse(LCCOndCombinationsPublishRS lccOndCombinationsPublishRS) {

		if (lccOndCombinationsPublishRS.getResponseAttributes().getSuccess() != null) {
			return true;
		} else {
			return false;
		}
	}

}
