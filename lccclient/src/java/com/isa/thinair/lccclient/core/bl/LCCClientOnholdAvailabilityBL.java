package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.OhdParam;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OhdParamMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCOnholdAvailabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCOnholdAvailabilityRS;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * 
 * @author rumesh
 * 
 */

public class LCCClientOnholdAvailabilityBL {
	private static final Log log = LogFactory.getLog(LCCClientOnholdAvailabilityBL.class);

	private Map<String, ResOnholdValidationDTO> carrierWiseOnholdValidationDTO;

	private UserPrincipal userPrincipal;

	public boolean execute(BasicTrackInfo trackInfo) throws ModuleException {
		LCCOnholdAvailabilityRQ onholdRQ = populateOnholdAvailabityRQ(trackInfo);
		LCCOnholdAvailabilityRS onholdAvailabilityRS = LCConnectorUtils.getMaxicoExposedWS().checkOnholdAvailability(onholdRQ);

		if (onholdAvailabilityRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = (LCCError) BeanUtils.getFirstElement(onholdAvailabilityRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}

		return onholdAvailabilityRS.isOnholdAvailability();
	}

	public Map<String, ResOnholdValidationDTO> getCarrierWiseOnholdValidationDTO() {
		return carrierWiseOnholdValidationDTO;
	}

	public void setCarrierWiseOnholdValidationDTO(Map<String, ResOnholdValidationDTO> carrierWiseOnholdValidationDTO) {
		this.carrierWiseOnholdValidationDTO = carrierWiseOnholdValidationDTO;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	private LCCOnholdAvailabilityRQ populateOnholdAvailabityRQ(BasicTrackInfo trackInfo) {
		LCCOnholdAvailabilityRQ onholdRQ = new LCCOnholdAvailabilityRQ();

		for (Iterator iterator = carrierWiseOnholdValidationDTO.entrySet().iterator(); iterator.hasNext();) {
			Map.Entry<String, ResOnholdValidationDTO> onholdType = (Map.Entry<String, ResOnholdValidationDTO>) iterator.next();

			OhdParamMap ohdParamMap = new OhdParamMap();
			ohdParamMap.setKey(onholdType.getKey());
			ohdParamMap.setValue(populateOhdParam(onholdType.getValue()));
			onholdRQ.getCarrierWiseParams().add(ohdParamMap);
		}

		onholdRQ.setLccPos(LCCClientCommonUtils.createPOS(this.userPrincipal, trackInfo));

		return onholdRQ;
	}

	private OhdParam populateOhdParam(ResOnholdValidationDTO resOnholdValidationDTO) {
		OhdParam ohdParam = new OhdParam();
		ohdParam.setIpAddress(resOnholdValidationDTO.getIpAddress());
		ohdParam.getFlightSegIdList().addAll(resOnholdValidationDTO.getFlightSegIdList());
		ohdParam.getDepartureDateList().addAll(resOnholdValidationDTO.getDepatureDateList());
		ohdParam.getPaxList().addAll(populatePersonName(resOnholdValidationDTO.getPaxList()));
		ohdParam.setContactEmail(resOnholdValidationDTO.getContactEmail());
		ohdParam.setValidationType(resOnholdValidationDTO.getValidationType());
		ohdParam.setSalesChannel(resOnholdValidationDTO.getSalesChannel());
		return ohdParam;
	}

	private List<PersonName> populatePersonName(List<ReservationPax> paxList) {
		List<PersonName> personNames = new ArrayList<PersonName>();

		for (ReservationPax pax : paxList) {
			PersonName p = new PersonName();
			p.setTitle(pax.getTitle());
			p.setFirstName(pax.getFirstName());
			p.setSurName(pax.getLastName());
			personNames.add(p);
		}

		return personNames;
	}
}
