package com.isa.thinair.lccclient.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.api.service.LCCLoyaltyManagementBD;
import com.isa.thinair.lccclient.core.bl.LCCClientCustomerNameUpdateBL;
import com.isa.thinair.lccclient.core.bl.LCCClientMergeLoyaltyManagementBL;
import com.isa.thinair.lccclient.core.service.bd.LCCLoyaltyManagmentBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCLoyaltyManagmentBDRemote;

@Stateless
@RemoteBinding(jndiBinding = LCCLoyaltyManagementBD.SERVICE_NAME + ".remote")
@LocalBinding(jndiBinding = LCCLoyaltyManagementBD.SERVICE_NAME + ".local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCLoyaltyManagementServiceBean extends PlatformBaseSessionBean implements LCCLoyaltyManagmentBDLocal,
		LCCLoyaltyManagmentBDRemote {

	@Override
	public LMSMemberDTO getMergeLoyaltyManagement(String memberFFID, BasicTrackInfo trackInfo) throws ModuleException {
		LCCClientMergeLoyaltyManagementBL lccClientMergeLoyaltyManagementBL = new LCCClientMergeLoyaltyManagementBL(memberFFID,
				trackInfo, this.getUserPrincipal());
		return lccClientMergeLoyaltyManagementBL.getMergeLoyaltyMember();
	}

	@Override
	public int customerNameUpdate(String ffid, BasicTrackInfo trackInfo, String title, String firstName, String lastName) throws ModuleException {
		LCCClientCustomerNameUpdateBL lccClientCustomerNameUpdateBL = new LCCClientCustomerNameUpdateBL(ffid, trackInfo,
				this.getUserPrincipal(), title, firstName, lastName);
		return lccClientCustomerNameUpdateBL.customerNameUpdate();
	}

}
