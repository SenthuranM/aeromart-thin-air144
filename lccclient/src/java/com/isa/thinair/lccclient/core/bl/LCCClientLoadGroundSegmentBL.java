package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.GroundSegment;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCGroundSegmentRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCGroundSegmentRS;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * @author Manoj Dhanushka
 */
public class LCCClientLoadGroundSegmentBL {

	private static Log log = LogFactory.getLog(LCCClientLoadGroundSegmentBL.class);

	public static List[] getGroundSegment(GroundSegmentTO groundSegmentTO, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCGroundSegmentRQ lCCGroundSegmentRQ = new LCCGroundSegmentRQ();
		lCCGroundSegmentRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lCCGroundSegmentRQ.setSelectedOnd(groundSegmentTO.getSelectedOnd());
		lCCGroundSegmentRQ.setAddGroundSegment(groundSegmentTO.isAddGroundSegment());
		lCCGroundSegmentRQ.setCarrierCode(groundSegmentTO.getOperatingCarrier());

		LCCGroundSegmentRS lccGroundSegmentRS = LCConnectorUtils.getMaxicoExposedWS().getGroundSegmentData(lCCGroundSegmentRQ);
		return populateGroundSegmentsObj(lccGroundSegmentRS);
	}

	private static List[] populateGroundSegmentsObj(LCCGroundSegmentRS lccGroundSegmentRS) {
		List<String[]> availableGroundStationsFrom = new ArrayList<String[]>();
		List<String[]> availableGroundStationsTo = new ArrayList<String[]>();

		if (lccGroundSegmentRS != null) {
			List<GroundSegment> groundStationsFrom = lccGroundSegmentRS.getGroundStationsFrom();
			List<GroundSegment> groundStationsTo = lccGroundSegmentRS.getGroundStationsTo();

			if (groundStationsFrom != null && groundStationsFrom.size() > 0) {
				for (GroundSegment groundSegment : groundStationsFrom) {
					String[] code = new String[] { groundSegment.getAirportCode(), groundSegment.getAirportName() };
					availableGroundStationsFrom.add(code);
				}
			}
			if (groundStationsTo != null && groundStationsTo.size() > 0) {
				for (GroundSegment groundSegment : groundStationsTo) {
					String[] code = new String[] { groundSegment.getAirportCode(), groundSegment.getAirportName() };
					availableGroundStationsTo.add(code);
				}
			}
		}
		return new List[] { availableGroundStationsFrom, availableGroundStationsTo };
	}

}
