package com.isa.thinair.lccclient.core.remoting.ejb;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoCriteriaDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.core.bl.LCCFlightServiceBL;
import com.isa.thinair.lccclient.core.service.bd.LCCFlightServiceBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCFlightServiceBDRemote;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

@Stateless
@RemoteBinding(jndiBinding = "LCCFlightService.remote")
@LocalBinding(jndiBinding = "LCCFlightService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCFlightServiceBean extends PlatformBaseSessionBean implements LCCFlightServiceBDLocal, LCCFlightServiceBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchFlightsForDisplay(FlightManifestSearchDTO searchDTO, BasicTrackInfo trackInfo) throws ModuleException {
		return LCCFlightServiceBL.searchFlightsForDisplay(searchDTO, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object getFlightManifestData(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCFlightServiceBL.getFlightManifestData(flightManifestOptionsDTO, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FlightLoadReportDataDTO> getFlightLoadData(ReportsSearchCriteria search, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCFlightServiceBL.getFlightLoadData(search, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<FlightSegmentInfoDTO> getFlightSegmentInfo(FlightSegmentInfoCriteriaDTO flightSegmentInfoCriteriaDTO, BasicTrackInfo trackInfo)
		throws ModuleException {
		return LCCFlightServiceBL.getFlightSegmentInfo(flightSegmentInfoCriteriaDTO, getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FlightSegement getFlightSegment(String flightNumber, Date departureDate) throws ModuleException {
		return LCCFlightServiceBL.getFlightSegment(flightNumber, departureDate, getUserPrincipal());
	}
	
}
