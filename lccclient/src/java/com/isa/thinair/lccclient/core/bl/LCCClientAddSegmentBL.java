package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredJourneyDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialReqDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Ticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingMethod;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Manoj Dhanushka
 */
public class LCCClientAddSegmentBL {

	private static Log log = LogFactory.getLog(LCCClientAddSegmentBL.class);

	private static void checkAddSegmentConstraints(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO)
			throws ModuleException {
		MODIFY_MODES modifyModes = lccClientResAlterQueryModesTO.getModifyModes();

		if (modifyModes != MODIFY_MODES.ADD_OND) {
			throw new ModuleException("lccclient.invalid.add.reservation.request");
		}

		String groupPNR = PlatformUtiltiies.nullHandler(lccClientResAlterQueryModesTO.getGroupPNR());

		if (groupPNR.length() == 0) {
			throw new ModuleException("lccclient.reservation.empty.group.pnr");
		}

		// if (lccClientResAlterQueryModesTO.getOldPnrSegs() == null ||
		// lccClientResAlterQueryModesTO.getOldPnrSegs().size() == 0) {
		// throw new ModuleException("lccclient.segment.references.empty");
		// }

		// if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() == null
		// || lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSegmentsMap().size() == 0) {
		// throw new ModuleException("lccclient.segment.references.empty");
		// }
	}

	public static LCCClientReservation execute(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, SessionContext sessionContext, TrackInfoDTO trackInfo) throws ModuleException {

		String groupPNR = lccClientResAlterQueryModesTO.getGroupPNR();
		PaymentAssembler paymentAssembler = new PaymentAssembler();
		LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();
		TrackInfoDTO trackInfoDTO = CommonUtil.getTrackInfoDto(userPrincipal);
		CommonReservationContactInfo contactInfo = lccClientSegmentAssembler.getContactInfo();
		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(contactInfo);
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		// If payment failed in the capture payment itself, we have to skip the reversal in again.
		// following boolean will be used for that. Until we capture the payment successfully and return back we don't
		// need to perform reversal
		boolean performReversal = false;
		boolean payFullPayment = false;

		try {
			checkAddSegmentConstraints(lccClientResAlterQueryModesTO);
			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = lccClientSegmentAssembler.getPassengerPaymentsMap();
			LCCClientPaymentWorkFlow.setLccUniqueTnxId(paxSeqWisePayAssemblers.values());
			for (LCCClientPaymentAssembler lccClientPaymentAssembler : paxSeqWisePayAssemblers.values()) {
				LCCClientPaymentWorkFlow.transform(paymentAssembler, lccClientPaymentAssembler);
			}

			boolean bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());

			if (paymentAssembler.getPayments().size() > 0) {
				ReservationBO.getPaymentInfo(paymentAssembler, groupPNR);

				Map<Integer, CardPaymentInfo> tempPaymentMap = LCCClientPaymentWorkFlow
						.populateTemPaymentEntries(lccClientSegmentAssembler.getTemporyPaymentMap());
				// Make the temporary payment entry
				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
						tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				ServiceResponce response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler,
						groupPNR, transformContactInfo, tempTnxIds);
				performReversal = true;
				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);

				LCCReservationUtil.combineActualCreditPayments(paxSeqWisePayAssemblers, paxCreditPayments, paymentAuthIdMap,
						paymentBrokerRefMap);
			}

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_5_ADD_OND, userPrincipal, trackInfo);

			// Setting the external charges
			lccAirBookModifyRQ
					.getLccReservation()
					.getAirReservation()
					.setPriceInfo(
							LCCReservationUtil.parsePriceInfo(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
									.getPassengerPaymentsMap()));

			addSpecialRequestDetails(lccAirBookModifyRQ.getLccReservation(), lccClientSegmentAssembler);

			if (lccClientResAlterQueryModesTO instanceof LCCClientResAlterModesTO) {
				if (((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO).getPaymentTypes() != null
						&& ((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO).getPaymentTypes().intValue() == ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT
								.intValue()) {
					composeFulFillments(lccAirBookModifyRQ, lccClientResAlterQueryModesTO, userPrincipal,
							!((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO).HasMadePayments());
					payFullPayment = true;
				}
			}

			// add passenger e ticket details to the modify request if the booking is confirmed or make full payment
			if (payFullPayment
					|| lccClientResAlterQueryModesTO.getReservationStatus().equals(
							com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus.CONFIRMED)
					|| (lccClientResAlterQueryModesTO.getReservationStatus().equals(
							com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus.CANCEL) && ((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO)
							.HasMadePayments())) {

				lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.CREATE);
				// Generate E tickets
				List<LCCClientReservationSegment> allLccSegments = new ArrayList<LCCClientReservationSegment>();
				List<LCCClientReservationSegment> unflownLccSegments = new ArrayList<LCCClientReservationSegment>();

				// add all existing segments
				allLccSegments.addAll(lccClientResAlterQueryModesTO.getExistingAllSegs());
				// add new segments
				allLccSegments.addAll(lccClientSegmentAssembler.getSegmentsMap().values());
				// get unflown segments
				unflownLccSegments = ETicketUtil.getUnflownSegments(allLccSegments);

				CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(trackInfo);
				ticketInfoDto.setBSPPayment(bspPayment);

				Map<Integer, Map<Integer, EticketDTO>> passengerEtickets = ETicketUtil.generateIATAETicketNumbers(
						lccClientResAlterQueryModesTO.getPassengers(), unflownLccSegments, ticketInfoDto);
				Iterator<LCCClientReservationPax> passngerIterator = lccClientResAlterQueryModesTO.getPassengers().iterator();
				while (passngerIterator.hasNext()) {
					LCCClientReservationPax lcclientReservationPax = passngerIterator.next();
					AirTraveler airTraveler = Transformer.transform(lcclientReservationPax, lccAirBookModifyRQ
							.getLccReservation().getAirReservation().getTravelerInfo());
					if (passengerEtickets.keySet().contains(lcclientReservationPax.getPaxSequence())) {
						airTraveler.getETickets().addAll(
								ETicketUtil.createPaxEticket(allLccSegments, lcclientReservationPax.getPaxSequence(),
										passengerEtickets.get(lcclientReservationPax.getPaxSequence())));
					}
				}
			}

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);
			return Transformer.transform(lccAirBookRS, tempTnxIds);
		} catch (Exception ex) {
			log.error("Error in modify segment", ex);
			if (performReversal) {
				sessionContext.setRollbackOnly();
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, "Payment failed for add segment",
						null, null, null);
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);
				if (!paymentAssembler.getPayments().isEmpty()) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler, groupPNR,
							transformContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for add segment", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static void addSpecialRequestDetails(LCCReservation lccReservation,
			LCCClientSegmentAssembler lccClientSegmentAssembler) {
		TravelerInfo travelerInfo = null;

		if (lccReservation.getAirReservation().getTravelerInfo() == null) {
			travelerInfo = new TravelerInfo();
		} else {
			travelerInfo = lccReservation.getAirReservation().getTravelerInfo();
		}

		SpecialReqDetails specialReqDetails = new SpecialReqDetails();
		List<InsuranceRequest> clientReservationInsurances = transform(lccClientSegmentAssembler.getResInsurances());
		if (clientReservationInsurances != null && !clientReservationInsurances.isEmpty()) {
			specialReqDetails.getInsuranceRequests().addAll(clientReservationInsurances);
		}

		populateSSRFromExternalCharges(lccClientSegmentAssembler.getPassengerExtChargeMap(), specialReqDetails);

		travelerInfo.setSpecialReqDetails(specialReqDetails);
		lccReservation.getAirReservation().setTravelerInfo(travelerInfo);

		if (lccClientSegmentAssembler.getPnrZuluReleaseTimeStamp() != null) {
			TicketingInfo ticketingInfo = new TicketingInfo();
			ticketingInfo.setTicketingStatus(TicketingStatus.TICKETTYPE_ONHOLD_TICKETED);
			ticketingInfo.setTicketAdvisory("Reservation is onhold. To avoid cancellation, pay before "
					+ lccClientSegmentAssembler.getPnrZuluReleaseTimeStamp() + " GMT");
			ticketingInfo.setTicketTimeLimit(lccClientSegmentAssembler.getPnrZuluReleaseTimeStamp());
			ticketingInfo.setTicketType(Ticket.ETICKET);
			ticketingInfo.setTicketBookingDate(lccClientSegmentAssembler.getLastFareQuotedDate());
			lccReservation.getAirReservation().setTicketing(ticketingInfo);
		}

	}

	private static void populateSSRFromExternalCharges(Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap,
			SpecialReqDetails specialReqDetails) {
		for (Entry<Integer, List<LCCClientExternalChgDTO>> entry : passengerExtChargeMap.entrySet()) {
			Integer travelerRef = entry.getKey();
			List<LCCClientExternalChgDTO> extChargeList = entry.getValue();

			for (LCCClientExternalChgDTO extChgDTO : extChargeList) {
				if (EXTERNAL_CHARGES.AIRPORT_SERVICE.equals(extChgDTO.getExternalCharges())) {
					AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
					airportServiceRequest.setSsrCode(extChgDTO.getCode());
					airportServiceRequest.setServiceCharge(extChgDTO.getAmount());
					airportServiceRequest.setAirportCode(extChgDTO.getAirportCode());
					airportServiceRequest.setCarrierCode(extChgDTO.getCarrierCode());
					airportServiceRequest.setFlightRefNumber(extChgDTO.getFlightRefNumber());
					airportServiceRequest.setTravelerRefNumber(travelerRef.toString() + "$");
					airportServiceRequest.setApplyOn(extChgDTO.getApplyOn());

					specialReqDetails.getAirportServiceRequests().add(airportServiceRequest);

				} else if (EXTERNAL_CHARGES.INFLIGHT_SERVICES.equals(extChgDTO.getExternalCharges())) {
					SpecialServiceRequest ssr = new SpecialServiceRequest();
					ssr.setCarrierCode(extChgDTO.getCarrierCode());
					ssr.setCharge(extChgDTO.getAmount());
					ssr.setFlightRefNumber(extChgDTO.getFlightRefNumber());
					ssr.setSsrCode(extChgDTO.getCode());
					ssr.setTravelerRefNumber(travelerRef.toString() + "$");
					ssr.setText(extChgDTO.getUserNote());

					specialReqDetails.getSpecialServiceRequests().add(ssr);
				}
			}

		}
	}

	private static List<InsuranceRequest> transform(List<LCCClientReservationInsurance> resInsurances) {

		List<InsuranceRequest> insuranceRQs = new ArrayList<InsuranceRequest>();

		InsuranceRequest insuranceRQ = null;
		if (resInsurances != null && !resInsurances.isEmpty()) {
			for (LCCClientReservationInsurance resInsurance : resInsurances) {
				insuranceRQ = new InsuranceRequest();
				insuranceRQ.setAmount(resInsurance.getQuotedTotalPremium());
				insuranceRQ.setDestination(resInsurance.getDestination());
				insuranceRQ.setOrigin(resInsurance.getOrigin());
				insuranceRQ.setOperatingCarrierCode(resInsurance.getOperatingAirline());
				insuranceRQ.setDateOfTravel(resInsurance.getDateOfTravel());
				insuranceRQ.setDateOfReturn(resInsurance.getDateOfReturn());
				insuranceRQ.setInsuranceRefNumber(Integer.parseInt(resInsurance.getInsuranceQuoteRefNumber()));
				insuranceRQ.setApplicablePaxCount(resInsurance.getApplicablePaxCount());
				insuranceRQ.setSsrFeeCode(resInsurance.getSsrFeeCode());
				insuranceRQ.setPlanCode(resInsurance.getPlanCode());
				
				InsuredJourneyDetails insuredJourney = new InsuredJourneyDetails();
				insuredJourney.setJourneyStartAirportCode(resInsurance.getInsuredJourneyDTO().getJourneyStartAirportCode());
				insuredJourney.setJourneyEndAirportCode(resInsurance.getInsuredJourneyDTO().getJourneyEndAirportCode());
				insuredJourney.setJourneyStartDate(resInsurance.getInsuredJourneyDTO().getJourneyStartDate());
				insuredJourney.setJourneyEndDate(resInsurance.getInsuredJourneyDTO().getJourneyEndDate());
				insuredJourney.setJourneyStartDateOffset(resInsurance.getInsuredJourneyDTO().getJourneyStartDateOffset());
				insuredJourney.setJourneyEndDateOffset(resInsurance.getInsuredJourneyDTO().getJourneyEndDateOffset());
				insuredJourney.setRoundTrip(resInsurance.getInsuredJourneyDTO().isRoundTrip());

				insuranceRQ.setInsuredJourneyDetails(insuredJourney);
				insuranceRQs.add(insuranceRQ);
			}

		}
		return insuranceRQs;
	}

	private static void composeFulFillments(LCCAirBookModifyRQ lccAirBookModifyRQ,
			LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, UserPrincipal userPrincipal, boolean isFirstPayment) throws ModuleException {

		LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();

		if (lccClientResAlterQueryModesTO instanceof LCCClientResAlterModesTO) {
			if (lccClientSegmentAssembler.getPassengerPaymentsMap().size() > 0) {
				// Fulfillment
				Fulfillment fulfillment = new Fulfillment();
				lccAirBookModifyRQ.getLccReservation().getAirReservation().setFulfillment(fulfillment);
				fulfillment.getCustomerFulfillments().addAll(
						LCCReservationUtil.parseTravelerFulfillments(lccClientSegmentAssembler.getPassengerPaymentsMap(),
								userPrincipal, lccClientResAlterQueryModesTO.getGroupPNR(), false, isFirstPayment));
			}
		}
	}

	public static ReservationBalanceTO query(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkAddSegmentConstraints(lccClientResAlterQueryModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_BALANCES_1_FOR_ADD_OND, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyResQuery(lccAirBookModifyRQ);
			return Transformer.transformBalances(lccAirBookRS);
		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static LCCAirBookModifyRQ process(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			LCCModificationTypeCode lccModificationTypeCode, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		lccAirBookModifyRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());
		lccAirBookModifyRQ.setModificationTypeCode(lccModificationTypeCode);
		lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(lccClientResAlterQueryModesTO
				.getLccClientSegmentAssembler().getLccTransactionIdentifier()));

		LCCReservation lccReservation = composeAirReservation(lccClientResAlterQueryModesTO.getGroupPNR(),
				lccClientResAlterQueryModesTO.getVersion(), lccClientResAlterQueryModesTO.getOldPnrSegs(),
				lccClientResAlterQueryModesTO.getExistingAllSegs());

		if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null) {
			setAirTravelRefType(lccReservation, lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
					.getPassengerPaymentsMap());
		}

		AirReservationSummary airs = new AirReservationSummary();
		airs.setAdminInfo(new AdminInfo());
		airs.getAdminInfo().setOwnerAgentCode(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getOwnerAgent());
		lccReservation.getAirReservation().setAirReservationSummary(airs);
		lccReservation.getAirReservation().setContactInfo(
				LCCClientApiUtils.transform(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getContactInfo()));
		lccAirBookModifyRQ.setLccReservation(lccReservation);
		lccAirBookModifyRQ.getPerPaxExtChargeMap().addAll(
				composePassengerExternalChargeMap(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
						.getPassengerExtChargeMap(), new Date()));

		LCCReservation newLccReservation = composeAirReservation(lccClientResAlterQueryModesTO.getGroupPNR(),
				lccClientResAlterQueryModesTO.getVersion(), lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
						.getSegmentsMap().values(), null);
		lccAirBookModifyRQ.setNewLCCReservation(newLccReservation);
		if (lccClientResAlterQueryModesTO instanceof LCCClientResAlterModesTO) {
			lccAirBookModifyRQ.setAppIndicator(((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO).getAppIndicator()
					.toString());
			lccAirBookModifyRQ.setSelectedPnrSegID(((LCCClientResAlterModesTO) lccClientResAlterQueryModesTO)
					.getSelectedPnrSegID());
		}

		lccAirBookModifyRQ.setAutoCancellationEnabled(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
				.isAutoCancellationEnabled());

		lccAirBookModifyRQ.setServiceTaxRatio(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getServiceTaxRatio());

		return lccAirBookModifyRQ;
	}

	/*
	 * This method set traveler seq & traveler type to AirTraveler Object. This is used in identifying actual pax type
	 * for airport service charge calculation
	 */
	private static void setAirTravelRefType(LCCReservation lccReservation,
			Map<Integer, LCCClientPaymentAssembler> paymentAssemblerMap) {
		for (Entry<Integer, LCCClientPaymentAssembler> entry : paymentAssemblerMap.entrySet()) {
			Integer PaxSeq = entry.getKey();

			AirTraveler airTraveler = new AirTraveler();
			airTraveler.setTravelerRefNumber(PaxSeq.toString());
			airTraveler.setTravelerSequence(PaxSeq);
			airTraveler.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(entry.getValue().getPaxType()));

			if (lccReservation.getAirReservation().getTravelerInfo() == null) {
				lccReservation.getAirReservation().setTravelerInfo(new TravelerInfo());
			}

			lccReservation.getAirReservation().getTravelerInfo().getAirTraveler().add(airTraveler);

		}
	}

	private static List<PerPaxExternalCharges> composePassengerExternalChargeMap(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setUserNote(lccClientExternalChgDTO.getUserNote());
				externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
				externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}

	private static LCCReservation composeAirReservation(String groupPNR, String version,
			Collection<LCCClientReservationSegment> colLCCReservationSegments,
			Collection<LCCClientReservationSegment> colExistingAllSegs) {
		LCCReservation lccReservation = new LCCReservation();
		lccReservation.setGroupPnr(groupPNR);

		AirReservation airReservation = new AirReservation();
		lccReservation.setAirReservation(airReservation);

		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOption = new OriginDestinationOption();

		if (colLCCReservationSegments != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : colLCCReservationSegments) {
				originDestinationOption.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));
			}
		}

		originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
		airReservation.setAirItinerary(originDestinationOptions);

		OriginDestinationOptions originDestinationOptionsAll = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOptionAll = new OriginDestinationOption();
		if (colExistingAllSegs != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : colExistingAllSegs) {
				originDestinationOptionAll.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));
			}
		}
		originDestinationOptionsAll.getOriginDestinationOption().add(originDestinationOptionAll);
		airReservation.setExistingAirItinerary(originDestinationOptionsAll);

		airReservation.setVersion(version);

		return lccReservation;
	}

}
