package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCouponAudit;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCouponUpdateRequest;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCGroupPaxCouponUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCGroupPaxCouponUpdateRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCouponUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCouponUpdateRS;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientPassengerCouponBL {
	private static final Log log = LogFactory.getLog(LCCClientPassengerCouponBL.class);

	public static void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal) throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("LCCClient updatePassengerCoupon called.");
		}
		try {

			LCCPaxCouponUpdateRQ lccpaxCouponUpdateRQ = process(paxCouponUpdateRQ, userPrincipal, trackInfo);

			LCCPaxCouponUpdateRS lccpaxCouponUpdateRS = LCConnectorUtils.getMaxicoExposedWS().updatePassengerCoupon(
					lccpaxCouponUpdateRQ);

			if (log.isDebugEnabled()) {
				log.debug("LCCClient raiseOrClearAlert called.");
			}

			if (lccpaxCouponUpdateRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils
						.getFirstElement(lccpaxCouponUpdateRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

		} catch (Exception e) {
			log.error("ERROR ", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}
	
	
	public static void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal) throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("LCCClient updatePassengerCoupon called.");
		}
		try {

			LCCGroupPaxCouponUpdateRQ lccGroupPaxCouponUpdateRQ = process(groupPaxCouponUpdateRQ, userPrincipal, trackInfo);

			LCCGroupPaxCouponUpdateRS lccGroupPaxCouponUpdateRS = LCConnectorUtils.getMaxicoExposedWS().updateGroupPassengerCoupon(lccGroupPaxCouponUpdateRQ);

			if (log.isDebugEnabled()) {
				log.debug("LCCClient raiseOrClearAlert called.");
			}

			if (lccGroupPaxCouponUpdateRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils
						.getFirstElement(lccGroupPaxCouponUpdateRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

		} catch (Exception e) {
			log.error("ERROR ", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}

	private static LCCPaxCouponUpdateRQ process(PassengerCouponUpdateRQ paxCouponUpdateRQ, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) {
		LCCPaxCouponUpdateRQ lccpaxCouponUpdateRQ = new LCCPaxCouponUpdateRQ();

		if (paxCouponUpdateRQ != null) {
			lccpaxCouponUpdateRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			PassengerTicketCouponAuditDTO couponAudit = paxCouponUpdateRQ.getCouponAudit();
			lccpaxCouponUpdateRQ.setPnr(couponAudit.getPnr());
			PaxCouponUpdateRequest lccPaxCouponUpdateRequest = new PaxCouponUpdateRequest();
			lccPaxCouponUpdateRequest.setCarrierCode(paxCouponUpdateRQ.getCarrierCode());
			lccPaxCouponUpdateRequest.setEticketId(paxCouponUpdateRQ.getEticketId());
			lccPaxCouponUpdateRequest.setEticketStatus(paxCouponUpdateRQ.getEticketStatus());
			lccPaxCouponUpdateRequest.setPaxStatus(paxCouponUpdateRQ.getPaxStatus());
			PaxCouponAudit lccCouponAudit = new PaxCouponAudit();
			lccCouponAudit.setPnr(couponAudit.getPnr());
			lccCouponAudit.setPassengerName(couponAudit.getPassengerName());
			lccCouponAudit.setRemark(couponAudit.getRemark());
			lccPaxCouponUpdateRequest.setCouponAudit(lccCouponAudit);
			lccpaxCouponUpdateRQ.setPaxCouponUpdateRequest(lccPaxCouponUpdateRequest);
		}
		return lccpaxCouponUpdateRQ;
	}
	
	private static LCCGroupPaxCouponUpdateRQ process(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQList, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) {

		LCCGroupPaxCouponUpdateRQ lccGroupPaxCouponUpdateRQ = new LCCGroupPaxCouponUpdateRQ();
		List<PaxCouponUpdateRequest> paxCouponUpdateRequestList = new ArrayList<PaxCouponUpdateRequest>();
		for (PassengerCouponUpdateRQ groupPaxCouponUpdateRQ : groupPaxCouponUpdateRQList) {

			lccGroupPaxCouponUpdateRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			PassengerTicketCouponAuditDTO couponAudit = groupPaxCouponUpdateRQ.getCouponAudit();
			lccGroupPaxCouponUpdateRQ.setPnr(couponAudit.getPnr());
			PaxCouponUpdateRequest lccPaxCouponUpdateRequest = new PaxCouponUpdateRequest();
			lccPaxCouponUpdateRequest.setCarrierCode(groupPaxCouponUpdateRQ.getCarrierCode());
			lccPaxCouponUpdateRequest.setEticketId(groupPaxCouponUpdateRQ.getEticketId());
			lccPaxCouponUpdateRequest.setEticketStatus(groupPaxCouponUpdateRQ.getEticketStatus());
			lccPaxCouponUpdateRequest.setPaxStatus(groupPaxCouponUpdateRQ.getPaxStatus());
			PaxCouponAudit lccCouponAudit = new PaxCouponAudit();
			lccCouponAudit.setPnr(couponAudit.getPnr());
			lccCouponAudit.setPassengerName(couponAudit.getPassengerName());
			lccCouponAudit.setRemark(couponAudit.getRemark());
			lccPaxCouponUpdateRequest.setCouponAudit(lccCouponAudit);
			
			paxCouponUpdateRequestList.add(lccPaxCouponUpdateRequest);
		}
		
		lccGroupPaxCouponUpdateRQ.getGroupPaxCouponUpdateRequest().addAll(paxCouponUpdateRequestList);
		return lccGroupPaxCouponUpdateRQ;
	}

}
