package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightPriceRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCPriceQuoteBL {

	private static final Log log = LogFactory.getLog(LCCPriceQuoteBL.class);

	private FlightPriceRQ flightPriceRQ;

	private FlightPriceRS flightPriceRS = new FlightPriceRS();

	private UserPrincipal userPrincipal;

	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	public FlightPriceRS execute(BasicTrackInfo trackInfo) throws ModuleException {

		try {
			LCCFlightPriceRQ lccFlightPriceRQ = createLCCFlightPriceRQ(trackInfo);
			// set flag to identify price quote done by availability search or from fare quote
			lccFlightPriceRQ.getAvailPreferences().setFromFareQuote(true);

			LCCFlightPriceRS lCCFlightPriceRS = LCConnectorUtils.getMaxicoExposedWS().quoteFlightPrice(lccFlightPriceRQ);

			if (lCCFlightPriceRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lCCFlightPriceRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			createFlightPriceRS(lCCFlightPriceRS, flightPriceRQ.getAvailPreferences().getOndFlexiSelected());
		} catch (Exception ex) {
			log.error("Error in price quote", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
		return flightPriceRS;
	}

	private LCCFlightPriceRQ createLCCFlightPriceRQ(BasicTrackInfo trackInfo) throws ModuleException {

		LCCFlightPriceRQ lccFlightPriceRQ = new LCCFlightPriceRQ();

		// populate origin destination list
		lccFlightPriceRQ.getOriginDestinationInformation().addAll(
				LCCClientAvailUtil.createOriginDestinationInfomationList(flightPriceRQ.getOriginDestinationInformationList(),
						true));

		// populate traveler information
		lccFlightPriceRQ.setTravelerInfoSummary(LCCClientAvailUtil.createTravelerInfoSummary(flightPriceRQ
				.getTravelerInfoSummary()));

		// populate travel preferences
		lccFlightPriceRQ.setTravelPreferences(LCCClientAvailUtil.createTravelPreferences(flightPriceRQ.getTravelPreferences()));

		// populate avail preferences
		lccFlightPriceRQ.setAvailPreferences(LCCClientAvailUtil.createAvailreferences(flightPriceRQ));

		// populate pos detail
		lccFlightPriceRQ.setLccPos(LCCClientCommonUtils.createPOS(this.userPrincipal, trackInfo));

		// populate header info
		lccFlightPriceRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(flightPriceRQ.getTransactionIdentifier()));

		return lccFlightPriceRQ;
	}

	private void createFlightPriceRS(LCCFlightPriceRS lccFlightPriceRS, Map<Integer, Boolean> ondwiseFlexiSelected) {

		Object[] res = LCCClientAvailUtil.createOriginDestinationInfomation(lccFlightPriceRS.getOriginDestinationInformation(),
				this.userPrincipal.getSalesChannel(), false);

		// Get segment fare options.
		Object[] resSeg = LCCClientAvailUtil.createOriginDestinationInfomation(
				lccFlightPriceRS.getOriginDestinationInformation(), this.userPrincipal.getSalesChannel(), true);

		// Merge segment fare options to the results if available.
		LCCClientAvailUtil.mergeSegFareOndOptions(res[0], resSeg[0]);

		flightPriceRS.setTransactionIdentifier(lccFlightPriceRS.getHeaderInfo().getTransactionIdentifier());
		// setting the ond information list
		flightPriceRS.getOriginDestinationInformationList().addAll((Collection<? extends OriginDestinationInformationTO>) res[0]);

		Boolean resetSelectedFare = (Boolean) res[1];

		// setting the selected flight price info
		if (!resetSelectedFare) {
			flightPriceRS.setSelectedPriceFlightInfo(LCCClientPriceUtil.createPriceInfoTO(lccFlightPriceRS.getPriceInfo()
					.getPriceInfo(), ondwiseFlexiSelected));

			PriceInfo segFarePrinceInfoTO = lccFlightPriceRS.getSegFarePriceInfo().getPriceInfo();
			if (segFarePrinceInfoTO != null) {
				flightPriceRS.setSelectedSegmentFlightPriceInfo(LCCClientPriceUtil.createPriceInfoTO(segFarePrinceInfoTO,
						ondwiseFlexiSelected));
			}

		}
		if (lccFlightPriceRS.getPriceInfo() != null && lccFlightPriceRS.getPriceInfo().getReservationParams() != null) {
			List<String> reservationParams = new ArrayList<String>();
			reservationParams.addAll(lccFlightPriceRS.getPriceInfo().getReservationParams());
			flightPriceRS.setReservationParms(reservationParams);
		}

		// flightPriceRS.setOnHoldEnabled(lccFlightPriceRS.isIbeOnholdEnabled());

		flightPriceRS
				.setApplicableServiceTaxes(LCCClientAvailUtil.populateServiceTaxContainer(lccFlightPriceRS.getServiceTaxes()));
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
}
