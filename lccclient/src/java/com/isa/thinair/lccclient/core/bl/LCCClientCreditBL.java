package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CreditOperationDetails;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCExtendORReinstateCreditRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCExtendORReinstateCreditRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientCreditBL {

	private static final Log log = LogFactory.getLog(LCCClientCreditBL.class);

	public static void extendOrReinstateCredits(String mode, int creditId, Date expiryDate, String note, String pnr,
			int pnrPaxId, int paxTxnId, BigDecimal amount, BigDecimal oCAmount, String carrierCode,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Going to " + mode.toLowerCase() + " credits in " + pnr + " with creditId - " + creditId + " expiryDate - "
					+ expiryDate + " pnrPaxId - " + pnrPaxId + " in carrier - " + carrierCode);
		}
		CreditOperationDetails creditOperationDetails = new CreditOperationDetails();
		creditOperationDetails.setMcAmount(amount);
		creditOperationDetails.setAmount(oCAmount);
		creditOperationDetails.setCarrierCode(carrierCode);
		creditOperationDetails.setCreditId(creditId);
		creditOperationDetails.setExpireDate(expiryDate);
		creditOperationDetails.setTxnId(paxTxnId);
		creditOperationDetails.setUserNote(note);
		creditOperationDetails.setPnr(pnr);
		creditOperationDetails.setPnrPaxId(pnrPaxId);
		creditOperationDetails.setMode(mode);
		creditOperationDetails.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());

		LCCExtendORReinstateCreditRQ lccExtendORReinstateCreditRQ = new LCCExtendORReinstateCreditRQ();
		lccExtendORReinstateCreditRQ.setCreditOperationDetails(creditOperationDetails);
		lccExtendORReinstateCreditRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
		LCCExtendORReinstateCreditRS lccExtendORReinstateCreditRS = LCConnectorUtils.getMaxicoExposedWS()
				.extendOrReinstateCredits(lccExtendORReinstateCreditRQ);

		if (lccExtendORReinstateCreditRS.getResponseAttributes().getSuccess() == null) {
			log.error(" Error occured in extendOrReinstateCredits for pnr " + pnr + " in carrier - " + carrierCode);
			LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccExtendORReinstateCreditRS.getResponseAttributes()
					.getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}
		if (log.isDebugEnabled()) {
			log.debug(" Successfully extendOrReinstateCredits for pnr " + pnr + " in carrier - " + carrierCode);
		}
	}

}
