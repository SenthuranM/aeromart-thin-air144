package com.isa.thinair.lccclient.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.api.service.LCCPaxCreditBD;
import com.isa.thinair.lccclient.core.bl.LCCClientCreditBL;
import com.isa.thinair.lccclient.core.bl.LCCPaxCreditSearviceBL;
import com.isa.thinair.lccclient.core.service.bd.LCCPaxCreditBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCPaxCreditBDRemote;

@Stateless
@RemoteBinding(jndiBinding = LCCPaxCreditBD.SERVICE_NAME + ".remote")
@LocalBinding(jndiBinding = LCCPaxCreditBD.SERVICE_NAME + ".local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCPaxCreditSearviceBean extends PlatformBaseSessionBean implements LCCPaxCreditBDLocal, LCCPaxCreditBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationPaxDTO> getPaxCreditAccrossCarriers(ReservationSearchDTO reservationSearchDTO,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCPaxCreditSearviceBL.getPaxCreditFromOtherCarriers(reservationSearchDTO, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PaxCreditDTO> getPaxCreditAccrossCarriers(List<String> pnrList, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCPaxCreditSearviceBL.getPaxCreditFromOtherCarriers(pnrList, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void extendOrReinstateCredits(String mode, int creditId, Date expiryDate, String note, String pnr, int pnrPaxId,
			int paxTxnId, BigDecimal amount, BigDecimal oCAmount, String carrierCode, TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientCreditBL.extendOrReinstateCredits(mode, creditId, expiryDate, note, pnr, pnrPaxId, paxTxnId, amount, oCAmount,
				carrierCode, this.getUserPrincipal(), trackInfoDTO);
	}
}
