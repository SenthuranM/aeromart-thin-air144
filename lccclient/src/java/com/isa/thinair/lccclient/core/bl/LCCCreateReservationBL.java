package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.SessionContext;

import com.isa.connectivity.profiles.maxico.v1.common.dto.*;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCOfflinePaymentDetails;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCOfflinePaymentDetails;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Nilindra Fernando
 */
public class LCCCreateReservationBL {

	private static final Log log = LogFactory.getLog(LCCCreateReservationBL.class);

	@SuppressWarnings("unchecked")
	public static LCCClientReservation book(CommonReservationAssembler lcclientReservationAssembler, UserPrincipal userPrincipal,
			SessionContext sessionContext, TrackInfoDTO trackInfoDTO) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("book method called.");

		String groupPNR = "";
		PaymentAssembler paymentAssembler = new PaymentAssembler();
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();
		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(lcclientReservationAssembler
				.getLccreservation().getContactInfo());
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		// If payment failed in the capture payment itself, we have to skip the reversal in again.
		// following boolean will be used for that. Until we capture the payment successfully and return back we don't
		// need to perform reversal
		boolean performReversal = false;

		try {
			// For IBE booking, GroupPNR is already generated
			if (lcclientReservationAssembler.getLccreservation().getPNR() == null) {
				groupPNR = BeanUtils.nullHandler(getGroupPNR(lcclientReservationAssembler.getLccreservation(), userPrincipal,
						trackInfoDTO));
				if (groupPNR.length() == 0) {
					throw new ModuleException("lccclient.reservation.empty.ticketing.info");
				}
			} else {
				groupPNR = lcclientReservationAssembler.getLccreservation().getPNR();
			}

			if (log.isDebugEnabled()) {
				log.debug("Generated Group PNR : " + groupPNR);
			}

			lcclientReservationAssembler.getLccreservation().setPNR(groupPNR);
			if (log.isDebugEnabled()) {
				log.debug("Capturing payments for resevation.");
			}

			LCCClientPaymentWorkFlow.setLccUniqueTnxId(lcclientReservationAssembler);
			for (LCCClientReservationPax lccClientReservationPax : lcclientReservationAssembler.getLccreservation()
					.getPassengers()) {
				LCCClientPaymentWorkFlow.transform(paymentAssembler, lccClientReservationPax.getLccClientPaymentAssembler());
			}

			Map<Integer, CardPaymentInfo> tempPaymentMap = LCCClientPaymentWorkFlow
					.populateTemPaymentEntries(lcclientReservationAssembler.getTemporaryPaymentMap());
			LCCOfflinePaymentDetails offlinePaymentDetails = null;

			boolean bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());
			ServiceResponce response = null;
			if (paymentAssembler.getPayments().size() > 0) {
				if (log.isDebugEnabled()) {
					log.debug("Is BSP Payment : " + LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments()));
				}
				ReservationBO.getPaymentInfo(paymentAssembler, groupPNR);
				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
						tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				// Release date is set to make sure release time is populated for offline payments
				paymentAssembler.setZuluOhdReleaseTime(lcclientReservationAssembler.getPnrZuluReleaseTimeStamp());
				response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler,
						lcclientReservationAssembler.getLccreservation().getPNR(), transformContactInfo, tempTnxIds);
				// Payment captured successfully. Need reversal if any exception occurred after this level.

				if (response.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY) != null) {
					offlinePaymentDetails = new LCCOfflinePaymentDetails();
					offlinePaymentDetails.setPaymentGatewayReferenceKey(
							(String) response.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY));
					offlinePaymentDetails.setPaymentGatewayReference(
							(String) response.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER));
				}

				performReversal = true;
				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection) response.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map) response.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);
				LCCReservationUtil.combineActualCreditPayments(paxCreditPayments, lcclientReservationAssembler, paymentAuthIdMap,
						paymentBrokerRefMap);
				if (log.isDebugEnabled()) {
					log.debug("Is BSP Payment : " + LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments()));
					log.debug("Payment Collection Size : " + paymentAssembler.getPayments().size());
				}
			}

			LCCAirBookRQ lccAirBookRQ = process(lcclientReservationAssembler, trackInfoDTO, userPrincipal, bspPayment);
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().book(lccAirBookRQ);
			LCCClientReservation reservation= Transformer.transform(lccAirBookRS, tempTnxIds);
			
			if (response != null) {
				Object faturatiRef = response.getResponseParam(CommandParamNames.CMI_FATOURATI_REF);
				if (faturatiRef != null) {
					reservation.setPaymentRef((String) faturatiRef);
				}
			}
			return reservation;
		} catch (Exception ex) {
			log.error("Error in book", ex);
			sessionContext.setRollbackOnly();
			if (performReversal) {
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE,
						"Payment failed for create reservation", null, null, null);
				// Need only credit card type payments for refund. Other modes will
				// be reverted automatically
				// We dont need to worry about other carrier pax credit because it is handled in LCC and reversal should
				// done in LCC itself.
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);
				if (paymentAssembler.getPayments().size() > 0) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler,
							lcclientReservationAssembler.getLccreservation().getPNR(), transformContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for create reservation", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static LCCAirBookRQ process(CommonReservationAssembler lcclientReservationAssembler, TrackInfoDTO trackInfoDTO,
			UserPrincipal userPrincipal, boolean bspPayment) throws ModuleException {

		List<LCCClientReservationInsurance> reservationInsurance = lcclientReservationAssembler.getLccreservation()
				.getReservationInsurances();

		CommonReservationContactInfo lcclientReservationContactInfo = lcclientReservationAssembler.getLccreservation()
				.getContactInfo();
		Set<LCCClientReservationSegment> reservationSegments = lcclientReservationAssembler.getLccreservation().getSegments();
		
		boolean firstPayment = false;
		// generate e ticket numbers
		Map<Integer, Map<Integer, EticketDTO>> eTicketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();
		if (!lcclientReservationAssembler.isOnHoldBooking()) {
			firstPayment = true;
			CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(trackInfoDTO);
			ticketInfoDto.setBSPPayment(bspPayment);
			List<LCCClientReservationSegment> unflownLccSegments = ETicketUtil.getUnflownSegments(reservationSegments);
			eTicketNumbers = ETicketUtil.generateIATAETicketNumbers(lcclientReservationAssembler.getLccreservation()
					.getPassengers(), unflownLccSegments, ticketInfoDto);
		}

		LCCAirBookRQ lccAirBookRQ = new LCCAirBookRQ();
		lccAirBookRQ.setGroupPnr(lcclientReservationAssembler.getLccreservation().getPNR());
		lccAirBookRQ.setContactInfo(LCCClientApiUtils.transform(lcclientReservationContactInfo));
		lccAirBookRQ.setLastUserNote(Transformer.transform(lcclientReservationAssembler.getLccreservation().getLastUserNote(),lcclientReservationAssembler.getLccreservation().getUserNoteType()));
		// Add private userNote privilege status of user to lccAirBookRQ
		lccAirBookRQ.getLastUserNote().setIsAllowClassifyUN(lcclientReservationAssembler.isAllowClassifyUN());
		lccAirBookRQ.setAppIndicator(lcclientReservationAssembler.getAppIndicator().toString());
		// lccAirBookRQ.setTravelerInfo(Transformer.transform(lcclientReservationAssembler.getLccreservation().getPassengers()));
		lccAirBookRQ.setTravelerInfo(Transformer.transform(lcclientReservationAssembler.getLccreservation().getPassengers(),
				lcclientReservationAssembler.getLccreservation().getPreferrenceInfo(), eTicketNumbers, reservationSegments));
		lccAirBookRQ.setOriginCountryOfCall(lcclientReservationAssembler.getOriginCountryOfCall());
		lccAirBookRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());

		lccAirBookRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
		lccAirBookRQ.setAirItineraryPricingInfo(parsePricingInfo(
				lcclientReservationAssembler.getLccreservation().getPassengers(), reservationInsurance, reservationSegments));

		lccAirBookRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(lcclientReservationAssembler
				.getLccTransactionIdentifier()));
		lccAirBookRQ.setAirItinerary(transform(reservationSegments, lcclientReservationAssembler.getAnciOfferTemplateIDs()));
		// Current design MC is capturing the payment. We need to skip the
		// makePayment macro in each carrier.
		// We need to population fulfillment objects as usual because it is used
		// for pax transaction records as well.
		// Introduced a new boolean to check whether we need to capture
		// payments.

		Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers = new HashMap<Integer, LCCClientPaymentAssembler>();
		for (LCCClientReservationPax lccClientReservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
			paxWisePaymentAssemblers.put(lccClientReservationPax.getPaxSequence(),
					lccClientReservationPax.getLccClientPaymentAssembler());
		}
		
		lccAirBookRQ.getFulfillment().addAll(
				LCCReservationUtil.parseTravelerFulfillments(paxWisePaymentAssemblers, userPrincipal,
						lcclientReservationAssembler.getLccreservation().getPNR(), false, firstPayment));
		lccAirBookRQ.setCapturePayments(false);
		ExpectedBookingStates expectedBookingStates = lcclientReservationAssembler.isOnHoldBooking() ? ExpectedBookingStates.ON_HOLD
				: ExpectedBookingStates.AUTO;
		lccAirBookRQ.setExpectedBookingStates(expectedBookingStates);
		lccAirBookRQ.setAppIndicator(lcclientReservationAssembler.getAppIndicator().toString());
		lccAirBookRQ.setDiscountedFareDetails(LCCReservationUtil.populateLccDiscountedFareDetails(lcclientReservationAssembler
				.getDiscountedFareDetails()));
		lccAirBookRQ.setDiscountAmount(lcclientReservationAssembler.getDiscountAmount());
		lccAirBookRQ.setServiceTaxRatio(lcclientReservationAssembler.getServiceTaxRatio());
		if(lcclientReservationAssembler.getPgwWiswZuluReleaseTimeStamp() != null){
			lccAirBookRQ.setPgwWiseOnholdTime(lcclientReservationAssembler.getPgwWiswZuluReleaseTimeStamp());
		}

		if(!StringUtil.isNullOrEmpty(lcclientReservationAssembler.getReasonForAllowBlPaxRes())){
			lccAirBookRQ.setReasonForAllowBlPax(lcclientReservationAssembler.getReasonForAllowBlPaxRes());
		}
		

		return lccAirBookRQ;
	}

	private static PriceInfo parsePricingInfo(Collection<LCCClientReservationPax> passengers,
			List<LCCClientReservationInsurance> reservationInsurances, Set<LCCClientReservationSegment> reservationSegments) {
		
		PriceInfo priceInfo = new PriceInfo();
		Date applicableDate = new Date();
		Set<LCCClientReservationSegment> operatingSegments = new HashSet<LCCClientReservationSegment>();
		
		for (LCCClientReservationSegment seg : reservationSegments) {
			if (reservationInsurances != null && !reservationInsurances.isEmpty()) {
				for (LCCClientReservationInsurance reservationInsurance : reservationInsurances) {
					if (reservationInsurance != null
							&& seg.getBookingFlightSegmentRefNumber().startsWith(reservationInsurance.getOperatingAirline())) {
						operatingSegments.add(seg);
					}
				}
			}
		}

		for (LCCClientReservationPax lccClientReservationPax : passengers) {

			LCCClientPaymentAssembler lcclientPaymentAssembler = (LCCClientPaymentAssembler) lccClientReservationPax
					.getLccClientPaymentAssembler();

			if (lcclientPaymentAssembler.getPerPaxExternalCharges() != null
					&& lcclientPaymentAssembler.getPerPaxExternalCharges().size() > 0) {

				Collection<ExternalCharge> colExternalCharge = new ArrayList<ExternalCharge>();
				BigDecimal totalExternalChgAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientExternalChgDTO lccClientExternalChgDTO : lcclientPaymentAssembler.getPerPaxExternalCharges()) {

					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INSURANCE) {

						String composedStr = composedInsuranceRefs(reservationInsurances);

						if (composedStr.length() > 0) {
							int noOfPortions = operatingSegments.size();
							BigDecimal insPerSeg[] = AccelAeroCalculator.roundAndSplit(lccClientExternalChgDTO.getAmount(),
									noOfPortions);

							int portionCount = 0;
							for (LCCClientReservationSegment fltSeg : operatingSegments) {
								ExternalCharge externalCharge = new ExternalCharge();
								externalCharge.setCode(composedStr);
								externalCharge.setCarrierCode(reservationInsurances.get(0).getOperatingAirline()); // all
																													// will
																													// be
																													// having
																													// same
								externalCharge.setAmount(insPerSeg[portionCount]);
								// externalCharge.setSsrFeeCode(reservationInsurance.getSsrFeeCode());
								// externalCharge.setPlanCode(reservationInsurance.getPlanCode());
								externalCharge.setApplicableDateTime(applicableDate);
								externalCharge.setType(
										LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

								if (externalCharge.getCarrierCode() == null) {
									externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								}
								externalCharge.setFlightRefNumber(fltSeg.getBookingFlightSegmentRefNumber());
								externalCharge.setJourneyType(lccClientExternalChgDTO.getJourneyType());
								colExternalCharge.add(externalCharge);
								portionCount++;
							}
						}
					} else if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX) {

						ExternalCharge externalCharge = new ExternalCharge();
						externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
						externalCharge.setApplicableDateTime(applicableDate);
						externalCharge.setType(
								LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

						if (lccClientExternalChgDTO.getCarrierCode() == null
								&& lccClientExternalChgDTO.getExternalCharges() != EXTERNAL_CHARGES.JN_OTHER) {
							externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						} else {
							externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
						}
						externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
						externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
						externalCharge.setCode(lccClientExternalChgDTO.getCode());
						externalCharge.getApplicablePassengerType()
								.add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(lccClientReservationPax.getPaxType()));
						externalCharge.setJourneyType(lccClientExternalChgDTO.getJourneyType());
						externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
						externalCharge.setSegmentSequence(lccClientExternalChgDTO.getSegmentSequence());

						externalCharge.setTicketingRevenue(lccClientExternalChgDTO.isTicketingRevenue());
						externalCharge.setTaxableAmount(lccClientExternalChgDTO.getTaxableAmount());
						externalCharge.setNonTaxableAmount(lccClientExternalChgDTO.getNonTaxableAmount());
						externalCharge.setTaxDepositCountryCode(lccClientExternalChgDTO.getTaxDepositCountryCode());
						externalCharge.setTaxDepositStateCode(lccClientExternalChgDTO.getTaxDepositStateCode());

						colExternalCharge.add(externalCharge);

					} else {
						ExternalCharge externalCharge = new ExternalCharge();
						externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
						externalCharge.setApplicableDateTime(applicableDate);
						externalCharge.setType(
								LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

						if (lccClientExternalChgDTO.getCarrierCode() == null
								&& lccClientExternalChgDTO.getExternalCharges() != EXTERNAL_CHARGES.JN_OTHER) {
							externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						} else {
							externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
						}
						externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
						externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
						externalCharge.setCode(lccClientExternalChgDTO.getCode());
						externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
						externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
						externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
						externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
						externalCharge.getApplicablePassengerType()
								.add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(lccClientReservationPax.getPaxType()));
						if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
							Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
									.getFlexiBilities();
							Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
							for (FlexiInfoTO flexiInfoTO : flexiBilities) {
								FlexiInfo flexiInfo = new FlexiInfo();
								flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
								flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
								flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
								flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
								flexiInfos.add(flexiInfo);
							}
							externalCharge.getAdditionalDetails().addAll(flexiInfos);
						}
						externalCharge.setJourneyType(lccClientExternalChgDTO.getJourneyType());
						externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
						externalCharge.setSegmentSequence(lccClientExternalChgDTO.getSegmentSequence());
						colExternalCharge.add(externalCharge);
					}
					totalExternalChgAmount = AccelAeroCalculator.add(totalExternalChgAmount, lccClientExternalChgDTO.getAmount());
				}

				PerPaxPriceInfo perPaxPriceInfo = new PerPaxPriceInfo();
				perPaxPriceInfo.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(lccClientReservationPax.getPaxType()));
				perPaxPriceInfo.setTravelerRefNumber(PaxTypeUtils.travelerReference(lccClientReservationPax));
				perPaxPriceInfo.setPassengerPrice(new FareType());

				perPaxPriceInfo.getPassengerPrice().getExternalCharge().addAll(colExternalCharge);
				perPaxPriceInfo.getPassengerPrice().setTotalExternalCharge(totalExternalChgAmount);

				priceInfo.getPerPaxPriceInfo().add(perPaxPriceInfo);
			}
		}

		return priceInfo;
	}

	private static String composedInsuranceRefs(List<LCCClientReservationInsurance> reservationInsurances) {
		StringBuilder sb = new StringBuilder();
		if (reservationInsurances != null && !reservationInsurances.isEmpty()) {
			for (LCCClientReservationInsurance reservationInsurance : reservationInsurances) {
				String refNo = reservationInsurance.getInsuranceQuoteRefNumber();
				if (sb.length() > 0) {
					sb.append(",");
				}
				sb.append(refNo);
			}
		}
		return sb.toString();
	}

	private static OriginDestinationOptions transform(Collection<LCCClientReservationSegment> segments,
			Map<String, Map<EXTERNAL_CHARGES, Integer>> selectedAnciOfferTemplateIDs) {

		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();

		for (LCCClientReservationSegment lcclientReservationSegment : segments) {
			FlightSegment flightSegment = new FlightSegment();
			flightSegment.setFlightRefNumber(lcclientReservationSegment.getFlightSegmentRefNumber());
			flightSegment.setRouteRefNumber(lcclientReservationSegment.getRouteRefNumber());
			flightSegment.setSegmentSequence(lcclientReservationSegment.getSegmentSeq());
			flightSegment.setReturnFlag(lcclientReservationSegment.getReturnFlag());

			flightSegment.setDepartureAirportCode(lcclientReservationSegment.getSegmentCode().split("/")[0]);
			flightSegment.setArrivalAirportCode(lcclientReservationSegment.getSegmentCode().split("/")[1]);

			flightSegment.setDepatureDateTime(lcclientReservationSegment.getDepartureDate());
			flightSegment.setArrivalDateTime(lcclientReservationSegment.getArrivalDate());
			flightSegment.setDepatureDateTimeZulu(lcclientReservationSegment.getDepartureDateZulu());
			flightSegment.setArrivalDateTimeZulu(lcclientReservationSegment.getArrivalDateZulu());
			flightSegment.setSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(lcclientReservationSegment
					.getBookingFlightSegmentRefNumber()));
			flightSegment.setFlightStatus("OPN");

			flightSegment.setFlightNumber(lcclientReservationSegment.getFlightNo());
			flightSegment.setOperatingAirline(AppSysParamsUtil.extractCarrierCode(lcclientReservationSegment.getFlightNo()));
			flightSegment.setSegmentCode(lcclientReservationSegment.getSegmentCode());
			flightSegment.setStatus(SegmentStatus.CNF);
			flightSegment.setSubStationShortName(lcclientReservationSegment.getSubStationShortName());

			flightSegment.setMarketingAgentCode(null);
			flightSegment.setMarketingCarrierCode(null);
			flightSegment.setMarketingStationCode(null);
			flightSegment.setStatusModifiedDate(new Date());
			flightSegment.setStatusModifiedChannelCode(null);
			flightSegment.setCabinClassCode(lcclientReservationSegment.getCabinClassCode());
			flightSegment.setLogicalCabinClassCode(lcclientReservationSegment.getLogicalCabinClass());
			flightSegment.setBaggageONDGroupId(lcclientReservationSegment.getBaggageONDGroupId());

			OriginDestinationOption originDestinationOption = new OriginDestinationOption();

			originDestinationOption.getFlightSegment().add(flightSegment);

			Map<EXTERNAL_CHARGES, Integer> selecteOfferTemplates = selectedAnciOfferTemplateIDs.get(lcclientReservationSegment
					.getBookingFlightSegmentRefNumber());

			if (selecteOfferTemplates != null && !selecteOfferTemplates.isEmpty()) {

				StringStringIntegerMap selTemplateIDs = new StringStringIntegerMap();
				selTemplateIDs.setKey(lcclientReservationSegment.getBookingFlightSegmentRefNumber());

				for (Entry<EXTERNAL_CHARGES, Integer> selectedTemplateIDEntry : selecteOfferTemplates.entrySet()) {
					StringIntegerMap offerTemplateIDEntry = new StringIntegerMap();
					offerTemplateIDEntry.setKey(selectedTemplateIDEntry.getKey().toString());
					offerTemplateIDEntry.setValue(selectedTemplateIDEntry.getValue());

					selTemplateIDs.getValue().add(offerTemplateIDEntry);
				}
				originDestinationOption.getSegWiseSelectedAnciOfferTemplateIDs().add(selTemplateIDs);
			}

			originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
		}

		return originDestinationOptions;
	}

	// generates the new PNR.
	private static String getGroupPNR(LCCClientReservation lccClientReservation, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) throws ModuleException {
		String groupPnr = null;
		String originatingCarrierCode = getOriginatingCarrierCode(new ArrayList<LCCClientReservationSegment>(
				lccClientReservation.getSegments()));

		// if the first segments carrier is this carrier, we don't need to make
		// a lcc call.
		if (AppSysParamsUtil.getDefaultCarrierCode().equals(originatingCarrierCode)) {
			// we are requesting a new PNR - so passing null arguments is ok.
			groupPnr = LCCClientModuleUtil.getReservationBD().getNewPNR(false, null);
		} else {

			LCCTicketingInfoRQ lccTicketingInfoRQ = new LCCTicketingInfoRQ();
			lccTicketingInfoRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccTicketingInfoRQ.setCarrierCode(originatingCarrierCode);

			LCCTicketingInfoRS lccTicketingInfoRS = LCConnectorUtils.getMaxicoExposedWS().getTicketingInfo(lccTicketingInfoRQ);
			if (lccTicketingInfoRS != null) {
				groupPnr = lccTicketingInfoRS.getGroupPnr();
			}
		}

		return groupPnr;
	}

	// returns the carrier code of the first segment of the reservation.
	private static String getOriginatingCarrierCode(List<LCCClientReservationSegment> segmentList) {
		String carrierCode = null;

		if (segmentList != null && segmentList.size() > 0) {
			Collections.sort(segmentList);
			LCCClientReservationSegment firstSegment = segmentList.get(0);
			if (firstSegment.getFlightNo() != null && firstSegment.getFlightNo().length() > 2) {
				carrierCode = firstSegment.getFlightNo().substring(0, 2);
			}
		}
		return carrierCode;
	}
}
