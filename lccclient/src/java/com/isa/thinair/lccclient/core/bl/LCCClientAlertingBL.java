package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertPage;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAddAlertsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAddAlertsRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAlertRetrievalRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAlertRetrievalRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCRemoveAlertsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCRemoveAlertsRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.services.MaxicoExposedWS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * lcc client logic for alert management.
 * 
 * @author sanjaya
 * 
 */
public class LCCClientAlertingBL {

	/**
	 * Retrieve the alerts from a provided airline.
	 * 
	 * @param searchCriteria
	 *            : The alerts search criteria.
	 * @param userPrincipal
	 *            :
	 * @param trackInfo
	 *            : TODO
	 * @return The Page object containing the results.
	 * 
	 * @throws ModuleException
	 */
	public static Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		try {
			LCCAlertRetrievalRQ alertRetrievalRQ = new LCCAlertRetrievalRQ();
			alertRetrievalRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			alertRetrievalRQ.setSearchCriteria(composeSearchCriteria(searchCriteria));
			alertRetrievalRQ.setCarrierCode(searchCriteria.getSearchAirline());

			LCCAlertRetrievalRS alertRetrievalRS = LCConnectorUtils.getMaxicoExposedWS().retrieveAlerts(alertRetrievalRQ);

			if (alertRetrievalRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(alertRetrievalRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			Page pageResult = composePage(alertRetrievalRS.getPage());
			return pageResult;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "lccclient.reservation.invocationError");
			}
		}
	}

	public static void addAlerts(List<AlertDetailDTO> alertDTOList, UserPrincipal userPrincipal, BasicTrackInfo trackInfo,
			String carrierCode) throws ModuleException {
		LCCAddAlertsRQ addAlertRQ = new LCCAddAlertsRQ();
		addAlertRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		addAlertRQ.setCarrierCode(carrierCode);
		addAlertRQ.getAlerts().addAll(composeAlert(alertDTOList));
		LCCAddAlertsRS addAlertRS = LCConnectorUtils.getMaxicoExposedWS().addAlerts(addAlertRQ);
		if (addAlertRS.getResponseAttributes().getSuccess() == null) {
			LCCError lccError = (LCCError) BeanUtils.getFirstElement(addAlertRS.getResponseAttributes().getErrors());
			throw new ModuleException(lccError.getErrorCode().value());
		}
	}

	private static List<AlertDetail> composeAlert(List<AlertDetailDTO> alertDTOList) {
		List<AlertDetail> alertList = new ArrayList<AlertDetail>();
		for (AlertDetailDTO alertDTO : alertDTOList) {
			AlertDetail alert = new AlertDetail();
			alert.setAlertTypeId(alertDTO.getAlertTypeId());
			alert.setContent(alertDTO.getContent());
			alert.setPnrSegId(alertDTO.getPnrSegId());
			alert.setPriorityCode(alertDTO.getPriorityCode());
			alert.setOriginDepDate(alertDTO.getDepDate());
			alertList.add(alert);
		}
		return alertList;
	}

	/**
	 * Calls the {@link MaxicoExposedWS} to clear the alerts
	 * 
	 * @param carrierCode
	 * @param alertIDs
	 * @param userPrincipal
	 * @param trackInfo
	 *            TODO
	 * @throws ModuleException
	 */
	public static void clearAlerts(String carrierCode, Collection<Integer> alertIDs, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		try {
			LCCRemoveAlertsRQ removeAlertsRQ = new LCCRemoveAlertsRQ();
			removeAlertsRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			if (alertIDs != null) {
				removeAlertsRQ.getAlertIdList().addAll(alertIDs);
			}
			removeAlertsRQ.setCarrierCode(carrierCode);

			LCCRemoveAlertsRS lccRemoveAlertsRs = LCConnectorUtils.getMaxicoExposedWS().removeAlerts(removeAlertsRQ);

			if (lccRemoveAlertsRs.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccRemoveAlertsRs.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "lccclient.reservation.invocationError");
			}
		}
	}

	private static AlertDetail composeSearchCriteria(AlertDetailDTO alertSearchCriteriaDTO) {
		// TODO - in the original AlertBD the search criteria and the search result is saved in the same AlertDetailDTO
		// object
		// change this when possible. Using the same notion is the LCC data flow as well.
		AlertDetail alertSearchCriteria = new AlertDetail();
		alertSearchCriteria.setFlightNumber(alertSearchCriteriaDTO.getFlightNumber());
		alertSearchCriteria.setDepDate(alertSearchCriteriaDTO.getDepDate());
		alertSearchCriteria.setPrivilege(alertSearchCriteriaDTO.getPrivilege());
		alertSearchCriteria.setFromDate(alertSearchCriteriaDTO.getFromDate());
		alertSearchCriteria.setToDate(alertSearchCriteriaDTO.getToDate());
		alertSearchCriteria.setStartRec(alertSearchCriteriaDTO.getStartRec());
		alertSearchCriteria.setEndRec(alertSearchCriteriaDTO.getEndRec());
		alertSearchCriteria.setOriginAirport(alertSearchCriteriaDTO.getOriginAirport());
		alertSearchCriteria.setAgentCode(alertSearchCriteriaDTO.getAgentCode());
		alertSearchCriteria.setViewAnyAlert(alertSearchCriteriaDTO.isViewAnyAlert());
		alertSearchCriteria.setViewOwnAlert(alertSearchCriteriaDTO.isViewOwnAlert());
		alertSearchCriteria.setViewReptAgentAlert(alertSearchCriteriaDTO.isViewReptAgentAlert());
		alertSearchCriteria.setOriginAirport(alertSearchCriteriaDTO.getOriginAirport());
		alertSearchCriteria.setSearchAirline(alertSearchCriteriaDTO.getSearchAirline());
		alertSearchCriteria.setOriginDepDate(alertSearchCriteriaDTO.getOriginDepDate());
		alertSearchCriteria.setAnciReprotectStatus(alertSearchCriteriaDTO.getAnciReprotectStatus());

		return alertSearchCriteria;
	}

	/**
	 * Creates the page object from {@link AlertPage}
	 * 
	 * @param alertPage
	 *            The alert page to be converted
	 * 
	 * @return The converted {@link Page} object.
	 * @throws ModuleException
	 */
	private static Page composePage(AlertPage alertPage) throws ModuleException {
		if (alertPage != null) {
			int totalNumberOfRecords = alertPage.getTotalNoOfRecords();
			int totalNumberOfRecordsInSystem = alertPage.getTotalNoOfRecordsInSystem();
			int startPosition = alertPage.getStartPosition();
			int endPosition = alertPage.getEndPosition();
			List<AlertDetailDTO> alertList = new ArrayList<AlertDetailDTO>();

			if (alertPage.getAlertData() != null) {
				for (AlertDetail alertDetail : alertPage.getAlertData()) {
					if (alertDetail != null) {
						alertList.add(convertAlertDetail(alertDetail));
					}
				}
			}

			Page page = new Page(totalNumberOfRecords, startPosition, endPosition, totalNumberOfRecordsInSystem, alertList);

			return page;
		} else {
			throw new ModuleException("Alert results retrieved from LCC is null");
		}
	}

	private static AlertDetailDTO convertAlertDetail(AlertDetail alertDetail) {
		// TODO - in the original AlertBD the search criteria and the search result is saved in the same AlertDetailDTO
		// object
		// change this when possible. Using the same notion is the LCC data flow as well.
		AlertDetailDTO alertDetailDTO = null;
		if (alertDetail != null) {
			alertDetailDTO = new AlertDetailDTO();
			alertDetailDTO.setAlertId(alertDetail.getAlertId());
			alertDetailDTO.setAlertDate(alertDetail.getAlertDate());
			alertDetailDTO.setFlightNumber(alertDetail.getFlightNumber());
			alertDetailDTO.setDepDate(alertDetail.getDepDate());
			alertDetailDTO.setPnr(alertDetail.getPnr());
			alertDetailDTO.setContent(alertDetail.getContent());
			alertDetailDTO.setPrivilege(alertDetail.getPrivilege());
			alertDetailDTO.setFromDate(alertDetail.getFromDate());
			alertDetailDTO.setToDate(alertDetail.getToDate());
			alertDetailDTO.setTotalRecs(alertDetail.getTotalRecs());
			alertDetailDTO.setStartRec(alertDetail.getStartRec());
			alertDetailDTO.setEndRec(alertDetail.getEndRec());
			alertDetailDTO.setOriginAirport(alertDetail.getOriginAirport());
			alertDetailDTO.setAgentCode(alertDetail.getAgentCode());
			alertDetailDTO.setViewAnyAlert(alertDetail.isViewAnyAlert());
			alertDetailDTO.setViewOwnAlert(alertDetail.isViewOwnAlert());
			alertDetailDTO.setViewReptAgentAlert(alertDetail.isViewReptAgentAlert());
			alertDetailDTO.setOriginAirport(alertDetail.getOriginAirport());
			alertDetailDTO.setSearchAirline(alertDetail.getSearchAirline());
			alertDetailDTO.setOriginDepDate(alertDetail.getOriginDepDate());
			alertDetailDTO.setOriginatorPnr(alertDetail.getOriginatorPnr());
		}
		return alertDetailDTO;
	}
}
