package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTransferSegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTransferSegmentsRS;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientTransferSegmentsBL {

	private static final Log log = LogFactory.getLog(LCCClientTransferSegmentsBL.class);

	public static void transferSegments(LCCClientTransferSegmentTO transferSegmentTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo, String salesChannelKey) throws ModuleException {
		try {
			// Transform to Request object
			LCCTransferSegmentsRQ transferSegmentsRQ = Transformer.transform(transferSegmentTO, userPrincipal, trackInfo, salesChannelKey);

			// Call web service method
			LCCTransferSegmentsRS transferSegmentRS = LCConnectorUtils.getMaxicoExposedWS().transferSegments(transferSegmentsRQ);
			if (transferSegmentRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(transferSegmentRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
		} catch (Exception ex) {
			log.error("Error in transfer segment", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}
}
