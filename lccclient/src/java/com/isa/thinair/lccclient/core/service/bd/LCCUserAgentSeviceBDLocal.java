package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.lccclient.api.service.LCCUserAgentSeviceBD;

@Local
public interface LCCUserAgentSeviceBDLocal extends LCCUserAgentSeviceBD {

}
