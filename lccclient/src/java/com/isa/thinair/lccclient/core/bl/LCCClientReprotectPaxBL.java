package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReprotectSegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReprotectSegmentsRS;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientReprotectPaxBL {

	private static final Log log = LogFactory.getLog(LCCClientReprotectPaxBL.class);

	public static Collection<String> reprotect(LCCClientTransferSegmentTO transferSegmentTO,
			Map<Integer, String> fltSegToPnrSegMap, UserPrincipal userPrincipal) throws ModuleException {

		Collection<String> failedResrvations = new ArrayList<String>();
		try {
			LCCReprotectSegmentsRQ lccReprotectSegmentsRQ = Transformer.transform(transferSegmentTO, fltSegToPnrSegMap,
					userPrincipal);
			LCCReprotectSegmentsRS reprotectSegmentRS = LCConnectorUtils.getMaxicoExposedWS().reprotectSegment(
					lccReprotectSegmentsRQ);

			if (reprotectSegmentRS.getResponseAttributes().getSuccess() == null) {
				failedResrvations.addAll(reprotectSegmentRS.getFailedPnrs());
			}

			return failedResrvations;
		} catch (Exception ex) {
			log.error("LCC invocationError when transfer segment", ex);
			throw new ModuleException("lccclient.reservation.invocationError", ex);
		}
	}
}
