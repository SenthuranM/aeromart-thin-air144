/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.lccclient.core.bl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.ModifyReservationUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * BL to Handle Remove PAX back end:
 * 
 * <br>
 * 1. Querying for cancellation charges, <br>
 * 2. Removing the passenger.
 * 
 * @author Abheek Das Gupta
 */
public class LCCClientRemovePaxBL {

	private static final Log log = LogFactory.getLog(LCCClientRemovePaxBL.class);

	/**
	 * Queries the participating carriers for the removal charges for the selected passengers.
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param userPrincipal
	 * @param trackInfo
	 *            TODO
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationBalanceTO query(Set<LCCClientReservationPax> passengers,
			LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {
		try {
			lccClientResAlterQueryModesTO.setModifyModes(MODIFY_MODES.CANCEL_RES);
			ReservationBalanceTO balanceTO = LCCClientCancelReservationBL.query(lccClientResAlterQueryModesTO, userPrincipal,
					trackInfo);

			Collection<LCCClientPassengerSummaryTO> passengerSummaryTOs = balanceTO.getPassengerSummaryList();

			List<String> paxAdults = lccClientResAlterQueryModesTO.getPaxAdultsList();
			List<String> paxInfants = lccClientResAlterQueryModesTO.getPaxInfantList();

			Collection<LCCClientPassengerSummaryTO> updatedPassengerSummaryTOs = ModifyReservationUtil.getUpdatedPaxSummaryTOs(
					passengerSummaryTOs, paxAdults, paxInfants, passengers, lccClientResAlterQueryModesTO.isInfantPaymentSeparated());

			balanceTO.setPassengerSummaryList(updatedPassengerSummaryTOs);
			ModifyReservationUtil.updatePayments(balanceTO,  lccClientResAlterQueryModesTO.isInfantPaymentSeparated());

			return balanceTO;
		} catch (Exception ex) {
			log.error("Error in cancel reservation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * Modifies the reservation by removing the passenger, and adjusting the charges associated with the removal.
	 * 
	 * @param groupPnr
	 * @param version
	 * @param reservationPaxs
	 * @param userPrincipal
	 * @param trackInfoDTO
	 * @param paxHandlingFeeCharges 
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation removePassenger(String groupPnr, String version,
			List<LCCClientReservationPax> reservationPaxs, CustomChargesTO customChargesTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO, Map<Integer, List<LCCClientExternalChgDTO>>  paxExternalCharges) throws ModuleException {

		LCCClientReservation clientReservation = new LCCClientReservation();
		try {
			/*
			 * Reservation reservation = LCCClientModuleUtil.getReservationBD().getReservation(groupPnr, false);
			 * 
			 * if(reservation.getPassengers().size() <= reservationPaxs.size()){ throw new
			 * ModuleException("airreservation.modify.reservation.all.pax.remove"); }
			 */

			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = process(groupPnr, version, reservationPaxs, customChargesTO);
			lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_14_REMOVE_PAX);
			
			if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {
				lccAirBookModifyRQ.getPerPaxExtChargeMap()
						.addAll(LCCReservationUtil.composePassengerExternalChargeMap(paxExternalCharges, new Date()));
			}

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			// populate LCCClientReservation from lccAirBookRS.
			clientReservation.setPNR(lccAirBookRS.getLccReservation().getGroupPnr());
		} catch (Exception e) {
			log.error("Error in removePassenger", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return clientReservation;
	}

	private static LCCAirBookModifyRQ process(String groupPnr, String version, List<LCCClientReservationPax> reservationPaxs,
			CustomChargesTO customChargesTO) throws ModuleException {
		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		LCCReservation lccReservation = new LCCReservation();
		lccReservation.setGroupPnr(groupPnr);
		AirReservation airReservation = new AirReservation();
		airReservation.setTravelerInfo(Transformer.transform(reservationPaxs));
		airReservation.setVersion(version);
		lccReservation.setAirReservation(airReservation);
		lccAirBookModifyRQ.setLccReservation(lccReservation);
		lccAirBookModifyRQ.setChargeOverride(LCCReservationUtil.createChargeOverrideForCancel(customChargesTO));
		return lccAirBookModifyRQ;
	}
}
