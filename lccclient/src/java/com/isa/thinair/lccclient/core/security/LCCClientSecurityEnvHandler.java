package com.isa.thinair.lccclient.core.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import com.sun.xml.wss.impl.callback.PasswordCallback;
import com.sun.xml.wss.impl.callback.UsernameCallback;

/**
 * @author Nilindra Fernando
 */
public class LCCClientSecurityEnvHandler implements CallbackHandler {

	private String userName;

	private String password;

	public LCCClientSecurityEnvHandler(String userName, String password) {
		setUserName(userName);
		setPassword(password);
	}

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof UsernameCallback) {
				UsernameCallback cb = (UsernameCallback) callbacks[i];
				cb.setUsername(getUserName());
			} else if (callbacks[i] instanceof PasswordCallback) {
				PasswordCallback cb = (PasswordCallback) callbacks[i];
				cb.setPassword(getPassword());
			} else {
				throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
			}
		}
	}

	/**
	 * @return the userName
	 */
	private String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	private void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	private String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	private void setPassword(String password) {
		this.password = password;
	}
}