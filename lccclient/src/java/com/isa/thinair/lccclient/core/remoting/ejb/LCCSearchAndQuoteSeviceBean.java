package com.isa.thinair.lccclient.core.remoting.ejb;

import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.InterlineBlockSeatRQInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.bl.LCCAvailabilitySearchBL;
import com.isa.thinair.lccclient.core.bl.LCCBlockSeatRemoveServiceBL;
import com.isa.thinair.lccclient.core.bl.LCCBlockSeatServiceBL;
import com.isa.thinair.lccclient.core.bl.LCCClientChangeFareBL;
import com.isa.thinair.lccclient.core.bl.LCCClientOnholdAvailabilityBL;
import com.isa.thinair.lccclient.core.bl.LCCPriceQuoteBL;
import com.isa.thinair.lccclient.core.bl.LCCPromotionsSearchBL;
import com.isa.thinair.lccclient.core.bl.LCCQuoteServiceTaxBL;
import com.isa.thinair.lccclient.core.bl.LCCScheduleAvailabilityBL;
import com.isa.thinair.lccclient.core.service.bd.LCCSearchAndQuoteBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCSearchAndQuoteBDRemote;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

@Stateless
@RemoteBinding(jndiBinding = "LCCSearchAndQuoteService.remote")
@LocalBinding(jndiBinding = "LCCSearchAndQuoteService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCSearchAndQuoteSeviceBean extends PlatformBaseSessionBean implements LCCSearchAndQuoteBDLocal,
		LCCSearchAndQuoteBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightAvailRS searchAvailableFlights(FlightAvailRQ flightAvailRQ, BasicTrackInfo trackInfo) throws ModuleException {
		LCCAvailabilitySearchBL availSearch = new LCCAvailabilitySearchBL();
		availSearch.setFlightAvailRQ(flightAvailRQ);
		availSearch.setUserPrincipal(this.getUserPrincipal());
		return availSearch.execute(trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightPriceRS quoteFlightPrice(FlightPriceRQ priceQuoteRQ, BasicTrackInfo trackInfo) throws ModuleException {
		LCCPriceQuoteBL priceQuoteBL = new LCCPriceQuoteBL();
		priceQuoteBL.setFlightPriceRQ(priceQuoteRQ);
		priceQuoteBL.setUserPrincipal(this.getUserPrincipal());
		return priceQuoteBL.execute(trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ScheduleAvailRS getAvailableSchedules(ScheduleAvailRQ scheduleAvailRQ, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCScheduleAvailabilityBL lccScheduleAvailabilityBL = new LCCScheduleAvailabilityBL();
		lccScheduleAvailabilityBL.setScheduleAvailRQ(scheduleAvailRQ);
		lccScheduleAvailabilityBL.setUserPrincipal(this.getUserPrincipal());
		return lccScheduleAvailabilityBL.execute(trackInfo);
	}

	@Override
	public FareAvailRS searchFares(FareAvailRQ fareAvailRQ, UserPrincipal userPrinciple, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCClientChangeFareBL lccClientChangeFareBL = new LCCClientChangeFareBL();
		return lccClientChangeFareBL.searchFares(fareAvailRQ, userPrinciple, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightPriceRS changeFlightPrice(ChangeFareRQ changeFareRQ, BasicTrackInfo trackInfo) throws ModuleException {
		LCCClientChangeFareBL lccClientChangeFareBL = new LCCClientChangeFareBL();
		return lccClientChangeFareBL.changeFare(changeFareRQ, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean priviledgeUserBlockSeat(InterlineBlockSeatRQInfoTO infoTO, BasicTrackInfo trackInfo) throws ModuleException {
		LCCBlockSeatServiceBL lccBlockSeatServiceBL = new LCCBlockSeatServiceBL();
		lccBlockSeatServiceBL.setInterlineBlockSeatRQInfoTO(infoTO);
		lccBlockSeatServiceBL.setUserPrincipal(this.getUserPrincipal());

		return lccBlockSeatServiceBL.execute(trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void removeBlockSeats(String trxID, BasicTrackInfo trackInfo) throws ModuleException {
		LCCBlockSeatRemoveServiceBL lccBlockSeatRemoveServiceBL = new LCCBlockSeatRemoveServiceBL();
		lccBlockSeatRemoveServiceBL.setTransactionID(trxID);
		lccBlockSeatRemoveServiceBL.setUserPrincipal(this.getUserPrincipal());

		lccBlockSeatRemoveServiceBL.execute(trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isReservationOnholdable(Map<String, ResOnholdValidationDTO> carrierWiseOnholdDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCClientOnholdAvailabilityBL onholdAvailabilityBL = new LCCClientOnholdAvailabilityBL();
		onholdAvailabilityBL.setCarrierWiseOnholdValidationDTO(carrierWiseOnholdDTO);
		onholdAvailabilityBL.setUserPrincipal(this.getUserPrincipal());

		return onholdAvailabilityBL.execute(trackInfo);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ApplicablePromotionDetailsTO pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria,
			BasicTrackInfo trackInfo) throws ModuleException {
		LCCPromotionsSearchBL promotionSearchBL = new LCCPromotionsSearchBL();
		promotionSearchBL.setUserPrincipal(this.getUserPrincipal());
		promotionSearchBL.setPromoSelectionCriteria(promoSelectionCriteria);
		return promotionSearchBL.execute(trackInfo);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal)
			throws ModuleException {
		LCCQuoteServiceTaxBL lccQuoteServiceTaxBL = new LCCQuoteServiceTaxBL();
		return lccQuoteServiceTaxBL.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO, userPrincipal);
	}
}
