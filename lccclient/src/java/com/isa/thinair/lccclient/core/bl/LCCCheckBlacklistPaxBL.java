package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCCheckBlacklistPaxRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCCheckBlacklistPaxRS;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * @author subash
 * 
 * */

public class LCCCheckBlacklistPaxBL {
	
	private static Log log = LogFactory.getLog(LCCCheckBlacklistPaxBL.class);

	public static List<BlacklistPAX> getBalcklistedPaxReservation(List<LCCClientReservationPax> paxList,
			boolean onlyFirstElement, List<String> participatingOperatingCarriers,  TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("LCCClient getBalcklistedPaxReservation called.");
		}

		try {
			LCCCheckBlacklistPaxRQ checkBLPaxRq = new LCCCheckBlacklistPaxRQ();
			checkBLPaxRq.setTravelerInfo(Transformer.transform(paxList));
			checkBLPaxRq.setOnlyFirstElement(onlyFirstElement);  
			checkBLPaxRq.getParticipatingOperatingCarriers().addAll(participatingOperatingCarriers);
			checkBLPaxRq.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			LCCCheckBlacklistPaxRS checkBLPaxRs = LCConnectorUtils.getMaxicoExposedWS()
					.getBalcklistedPaxReservation(checkBLPaxRq);
			
			return transform(checkBLPaxRs);

		} catch (Exception e) {
			log.error("ERROR @ getBalcklistedPaxReservation: " + e.getCause());
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}

	private static List<BlacklistPAX> transform(LCCCheckBlacklistPaxRS checkBLPaxRs) {
		List<BlacklistPAX> blacklistPaxList = new ArrayList<BlacklistPAX>();
		if (checkBLPaxRs != null && !checkBLPaxRs.getTravelerInfo().getAirTraveler().isEmpty()) {
			for (AirTraveler airtravlPax : checkBLPaxRs.getTravelerInfo().getAirTraveler()) {
				BlacklistPAX blpax = new BlacklistPAX();
				if(airtravlPax.getPersonName() != null){
					blpax.setFullName(airtravlPax.getPersonName().getFirstName());
				}

				blacklistPaxList.add(blpax);
			}
		}
		return blacklistPaxList;
	}

}
