package com.isa.thinair.lccclient.core.bl;

import javax.ejb.SessionContext;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * @author Nilindra Fernando
 * @since 6:36 PM 11/5/2009
 */
public class LCCClientResAlterationBL {

	public static LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientCancelReservationBL.execute(lccClientResAlterModesTO, userPrincipal, trackInfo);
	}

	public static LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientCancelSegmentBL.execute(lccClientResAlterModesTO, userPrincipal, trackInfo);
	}

	public static LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterModesTO,
			UserPrincipal userPrincipal, SessionContext sessionContext, TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientModifySegmentBL.execute(lccClientResAlterModesTO, userPrincipal, sessionContext, trackInfo);
	}

	public static LCCClientReservation addSegments(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, SessionContext sessionContext, TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientAddSegmentBL.execute(lccClientResAlterQueryModesTO, userPrincipal, sessionContext, trackInfo);
	}
}
