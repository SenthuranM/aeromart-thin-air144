package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCreditSearchOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCreditSearchByPnrListRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCreditSearchByPnrListRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCreditSearchRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxCreditSearchRS;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCPaxCreditSearviceBL {

	private static Log log = LogFactory.getLog(LCCPaxCreditSearviceBL.class);

	public static Collection<ReservationPaxDTO> getPaxCreditFromOtherCarriers(ReservationSearchDTO resSearchDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {

		try {
			LCCPaxCreditSearchRQ paxCreditSearchRQ = new LCCPaxCreditSearchRQ();
			paxCreditSearchRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

			// paxCreditSearchRQ.setRequestingCarrier(defaultCarrierCode);
			PaxCreditSearchOptions paxCreditSearchOptions = new PaxCreditSearchOptions();
			paxCreditSearchOptions.setPnr(resSearchDTO.getPnr());
			paxCreditSearchOptions.setAgentCode(resSearchDTO.getOriginAgentCode());
			PersonName personName = new PersonName();
			personName.setFirstName(resSearchDTO.getFirstName());
			personName.setSurName(resSearchDTO.getLastName());
			paxCreditSearchOptions.setPersonName(personName);
			paxCreditSearchOptions.setFromAirport(resSearchDTO.getFromAirport());
			paxCreditSearchOptions.setToAirport(resSearchDTO.getToAirport());
			paxCreditSearchOptions.setPhoneNo(resSearchDTO.getTelephoneNo());
			paxCreditSearchOptions.setDepatureDate(resSearchDTO.getDepartureDate());
			paxCreditSearchOptions.setReturnDate(resSearchDTO.getReturnDate());
			paxCreditSearchOptions.setFilterZeroCredits(resSearchDTO.isFilterZeroCredits());
			paxCreditSearchOptions.setFlightNo(resSearchDTO.getFlightNo());
			if (resSearchDTO.getOriginChannelIds() != null) {
				paxCreditSearchOptions.getOriginChannelIds().addAll(resSearchDTO.getOriginChannelIds());
			}

			if (resSearchDTO instanceof ReservationPaymentDTO) {
				ReservationPaymentDTO reservationPaymentDTO = (ReservationPaymentDTO) resSearchDTO;
				paxCreditSearchOptions.setCreditCardNo(reservationPaymentDTO.getCreditCardNo());
				paxCreditSearchOptions.setCardExpiry(reservationPaymentDTO.getEDate());
			}
			paxCreditSearchRQ.setPaxCreditSearchOptions(paxCreditSearchOptions);
			LCCPaxCreditSearchRS responce = LCConnectorUtils.getMaxicoExposedWS()
					.getPaxCreditFromOtherCarriers(paxCreditSearchRQ);
			return Transformer.transform(responce);
		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				log.error("lccclient.getPaxCreditFromOtherCarriers.retrivalError", ex);
				throw new ModuleException("lccclient.getPaxCreditFromOtherCarriers.retrivalError", ex);
			}
		}
	}

	public static Collection<PaxCreditDTO> getPaxCreditFromOtherCarriers(List<String> pnrList, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		try {
			LCCPaxCreditSearchByPnrListRQ lccPaxCreditSearchByPnrListRQ = new LCCPaxCreditSearchByPnrListRQ();
			lccPaxCreditSearchByPnrListRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccPaxCreditSearchByPnrListRQ.getPnrList().addAll(pnrList);

			LCCPaxCreditSearchByPnrListRS lccPaxCreditSearchByPnrListRS = LCConnectorUtils.getMaxicoExposedWS()
					.getCustomerCreditFromOtherCarriers(lccPaxCreditSearchByPnrListRQ);

			Collection<PaxCreditDTO> colPaxCredit = new ArrayList<PaxCreditDTO>();

			Transformer.transform(lccPaxCreditSearchByPnrListRS.getCustomerCredit(), colPaxCredit);

			return colPaxCredit;
		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				log.error("lccclient.getPaxCreditFromOtherCarriers.retrivalError", ex);
				throw new ModuleException("lccclient.getPaxCreditFromOtherCarriers.retrivalError", ex);
			}
		}
	}

}
