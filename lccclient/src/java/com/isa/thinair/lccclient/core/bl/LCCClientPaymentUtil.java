package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DirectBill;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LMSPaymentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductAmounts;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCardType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCurrencyAmount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerVoucherPayment;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airreservation.api.dto.LccPaxPaymentTO;
import com.isa.thinair.airreservation.api.dto.LccResPaymentTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard.FieldName;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.util.CreditCardUtil;

public class LCCClientPaymentUtil {

	private static Log log = LogFactory.getLog(LCCClientPaymentUtil.class);

	public static Map<Integer, LCCClientPaymentAssembler> getLCCClientPaymentAssemblers(
			CommonReservationAssembler lcclientReservationAssembler) {

		Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = populate(lcclientReservationAssembler);

		return paxSeqWisePayAssemblers;
	}

	private static Map<Integer, LCCClientPaymentAssembler> populate(CommonReservationAssembler lcclientReservationAssembler) {
		if (log.isDebugEnabled()) {
			log.debug("Organising paymentAssemblers based on pax sequence.");
		}

		Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = new HashMap<Integer, LCCClientPaymentAssembler>();

		for (LCCClientReservationPax lccClientReservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
			if (lccClientReservationPax.getLccClientPaymentAssembler() != null) {
				paxSeqWisePayAssemblers.put(lccClientReservationPax.getPaxSequence(),
						lccClientReservationPax.getLccClientPaymentAssembler());
			}
		}
		return paxSeqWisePayAssemblers;
	}

	public static PaymentDetails transform(LCCClientPaymentInfo lcclientPaymentInfo, String pnr) throws ModuleException {
		PaymentDetails paymentDetails = createPaymentDetails();
		paymentDetails.setDirectBill(null);
		paymentDetails.setPaymentCard(null);
		paymentDetails.setTravelerCreditPayment(null);
		if (lcclientPaymentInfo.getLccUniqueTnxId() == null || lcclientPaymentInfo.getLccUniqueTnxId().equals("")) {
			// TODO : Charith - It seems we dont need this check. can randomly set a id here. but need to check
			lcclientPaymentInfo.setLccUniqueTnxId(UniqueIDGenerator.generate());
		}

		if (lcclientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
			CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lcclientPaymentInfo;

			PaymentCardType paymentCardType = PaymentCardType.GENERIC;
			Integer paymentGatewayID = lccClientCreditCardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId();
			List<CardDetailConfigDTO> cardDetailConfigData = LCCClientModuleUtil.getPaymentBrokerBD()
					.getPaymentGatewayCardConfigData(paymentGatewayID.toString());
			Map<String, String> storeDataMap = CreditCardUtil.getDBStroreData(cardDetailConfigData,
					lccClientCreditCardPaymentInfo.getNo(), lccClientCreditCardPaymentInfo.getCardHoldersName(),
					lccClientCreditCardPaymentInfo.geteDate(), lccClientCreditCardPaymentInfo.getSecurityCode());

			if (lccClientCreditCardPaymentInfo != null) {
				if (PaymentType.CARD_VISA.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.VISA;
				} else if (PaymentType.CARD_MASTER.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.MASTER;
				} else if (PaymentType.CARD_AMEX.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.AMERICAN_EXPRESS;
				} else if (PaymentType.CARD_DINERS.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.DINERS_CLUB;
				} else if (PaymentType.CARD_CMI.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.CMI;
				} else if (PaymentType.CARD_GENERIC.getTypeValue() == lccClientCreditCardPaymentInfo.getType()) {
					paymentCardType = PaymentCardType.GENERIC;
				}
			}

			CommonCreditCardPayment paymentCard = new CommonCreditCardPayment();
			paymentCard.setAddress(lccClientCreditCardPaymentInfo.getAddress());
			paymentCard.setAppIndicator(lccClientCreditCardPaymentInfo.getAppIndicator().toString());
			paymentCard.setAuthorizationId(lccClientCreditCardPaymentInfo.getAuthorizationId());
			paymentCard.setCardHolderName(storeDataMap.get(FieldName.CARDHOLDERNAME.code()));
			paymentCard.setEDate(storeDataMap.get(FieldName.EXPIRYDATE.code()));
			paymentCard.setFirst4Digits(lccClientCreditCardPaymentInfo.getNoFirstDigits());
			paymentCard.setLast4Digits(lccClientCreditCardPaymentInfo.getNoLastDigits());
			paymentCard.setGroupPNR(lccClientCreditCardPaymentInfo.getGroupPNR());
			paymentCard.setIsPaymentSuccess(lccClientCreditCardPaymentInfo.isPaymentSuccess());
			paymentCard.setName(lccClientCreditCardPaymentInfo.getName());
			paymentCard.setNo(storeDataMap.get(FieldName.CARDNUMBER.code()));
			paymentCard.setOldCCRecordId(lccClientCreditCardPaymentInfo.getOldCCRecordId());
			paymentCard.setPaymentBrokerId(lccClientCreditCardPaymentInfo.getPaymentBrokerId());
			paymentCard.setSecurityCode(storeDataMap.get(FieldName.CVV.code()));
			paymentCard.setTemporyPaymentId(lccClientCreditCardPaymentInfo.getTemporyPaymentId());
			paymentCard.setTnxMode(lccClientCreditCardPaymentInfo.getTnxMode().getCode());
			paymentCard.setIpgId(paymentGatewayID);
			paymentCard.setPaymentCurrencyCode(lccClientCreditCardPaymentInfo.getIpgIdentificationParamsDTO()
					.getPaymentCurrencyCode());
			paymentCard.setCardType(paymentCardType);
			paymentDetails.setPaymentCard(paymentCard);
		} else if (lcclientPaymentInfo instanceof LCCClientOnAccountPaymentInfo) {
			paymentDetails.setDirectBill(new DirectBill());
			LCCClientOnAccountPaymentInfo lccClientOnAccountPaymentInfo = (LCCClientOnAccountPaymentInfo) lcclientPaymentInfo;
			paymentDetails.getDirectBill().setAgentCode(lccClientOnAccountPaymentInfo.getAgentCode());
			paymentDetails.getPaymentSummary().setPaymentMethod(lccClientOnAccountPaymentInfo.getPaymentMethod());
		} else if (lcclientPaymentInfo instanceof LCCClientCashPaymentInfo) {
			paymentDetails.setCash(true);
		} else if (lcclientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo) {
			LCCClientPaxCreditPaymentInfo lcclientPaxPaymentInfo = ((LCCClientPaxCreditPaymentInfo) lcclientPaymentInfo);
			TravelerCreditPayment travelerCreditPayment = new TravelerCreditPayment();
			travelerCreditPayment.setDebitTravelerRefNumber(lcclientPaxPaymentInfo.getDebitPaxRefNo().toString());
			travelerCreditPayment.setCarrierBaseAmount(lcclientPaxPaymentInfo.getResidingCarrierAmount());
			travelerCreditPayment.setEDate(lcclientPaxPaymentInfo.getExpiryDate());
			travelerCreditPayment.setBasePnr(lcclientPaxPaymentInfo.getResidingPnr());
			travelerCreditPayment.setPaxCreditId(lcclientPaxPaymentInfo.getPaxCreditId());
			travelerCreditPayment.setUtilizingPnr(pnr);
			paymentDetails.setTravelerCreditPayment(travelerCreditPayment);
		} else if (lcclientPaymentInfo instanceof LCCClientLMSPaymentInfo) {
			LCCClientLMSPaymentInfo lccClientLMSPaymentInfo = (LCCClientLMSPaymentInfo) lcclientPaymentInfo;
			LMSPaymentInfo lmsPaymentInfo = new LMSPaymentInfo();
			lmsPaymentInfo.setMemberAccountId(lccClientLMSPaymentInfo.getLoyaltyMemberAccountId());
			lmsPaymentInfo.setTotalPaymentAmount(lccClientLMSPaymentInfo.getTotalAmount());
			List<String> rewardIdList = new ArrayList<String>();
			for (String rewardId : lccClientLMSPaymentInfo.getRewardIDs()) {
				rewardIdList.add(rewardId);
			}
			lmsPaymentInfo.getRewardIds().addAll(rewardIdList);
			Collection<PaxProductAmounts> paxProductAmounts = new ArrayList<PaxProductAmounts>();
			for (Integer paxSeq : lccClientLMSPaymentInfo.getPaxProductRedeemedAmounts().keySet()) {
				PaxProductAmounts perPaxProductAmounts = new PaxProductAmounts();
				perPaxProductAmounts.setKey(paxSeq);
				perPaxProductAmounts.getValue().addAll(
						Transformer.populateStringDecimalMap(lccClientLMSPaymentInfo.getPaxProductRedeemedAmounts().get(
								paxSeq)));
				paxProductAmounts.add(perPaxProductAmounts);
			}
			lmsPaymentInfo.getPaxProductRedeemBreakdown().addAll(paxProductAmounts);
			paymentDetails.setLoyaltyFFID(lccClientLMSPaymentInfo.getLoyaltyMemberAccountId());
			paymentDetails.setLmsPaymentInfo(lmsPaymentInfo);
		} else if (lcclientPaymentInfo instanceof LCCClientVoucherPaymentInfo) {
			LCCClientVoucherPaymentInfo lcclientPaxPaymentInfo = ((LCCClientVoucherPaymentInfo) lcclientPaymentInfo);
			TravelerVoucherPayment voucherPayment = new TravelerVoucherPayment();
			voucherPayment.setVoucherAmount(new BigDecimal(lcclientPaxPaymentInfo.getVoucherDTO().getAmountInBase()));
			voucherPayment.setVoucherID(lcclientPaxPaymentInfo.getVoucherDTO().getVoucherId());
			voucherPayment.setVoucherRedeemAmount(new BigDecimal(lcclientPaxPaymentInfo.getVoucherDTO().getRedeemdAmount()));
			paymentDetails.setVoucherPayment(voucherPayment);
		}
		// Note : set actual payment carrier code. Eg. credit residing carrier. LCC calculation will depend on
		// this and will used to transfer credit from residing carrier to operating carrier
		paymentDetails.getPaymentSummary().setPaymentCarrier(lcclientPaymentInfo.getCarrierCode());
		paymentDetails.getPaymentSummary().setApplicableDateTime(lcclientPaymentInfo.getTxnDateTime());
		paymentDetails.getPaymentSummary().setPaymentAmount(lcclientPaymentInfo.getTotalAmount());
		paymentDetails.getPaymentSummary().setCurrencyCodeGroup(FindReservationUtil.getDefaultCurrencyCodeGroup());

		paymentDetails.setPaymentCurrencyAmount(getPaymentCurrencyAmount(lcclientPaymentInfo.getPayCurrencyDTO()));

		paymentDetails.setCarrierVisePayments(lcclientPaymentInfo.getCarrierVisePayments()); // used for refund payments

		paymentDetails.setUniqueTnxId(lcclientPaymentInfo.getLccUniqueTnxId());
		//for refund
		paymentDetails.setOriginalPaymentUID(lcclientPaymentInfo.getOriginalPaymentUID());
		paymentDetails.setOriginalPaymentTnxDateTime(lcclientPaymentInfo.getPaymentTnxDateTime());
		paymentDetails.setOriginalPaymentTnxRefNumber(lcclientPaymentInfo.getPaymentTnxReference());
	
		return paymentDetails;
	}

	public static PaymentDetails createPaymentDetails() {
		// FIXME : if JAXB constructor injection is possible, this can be removed.
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentSummary(new PaymentSummary());
		return paymentDetails;
	}

	public static PaymentDetails createPaymentDetails(String carrier, BigDecimal amount) {
		PaymentDetails paymentDetails = createPaymentDetails();
		paymentDetails.getPaymentSummary().setPaymentCarrier(carrier);
		paymentDetails.getPaymentSummary().setPaymentAmount(amount);
		return paymentDetails;
	}

	public static PaymentDetails createPaymentDetails(String carrier, BigDecimal amount, String agentCode) {
		PaymentDetails paymentDetails = createPaymentDetails(carrier, amount);
		DirectBill directBill = new DirectBill();
		directBill.setAgentCode(agentCode);
		paymentDetails.setDirectBill(directBill);
		return paymentDetails;
	}

	public static Map<Integer, Collection<LccPaxPaymentTO>> getPaxSeqAndLccPaxPaymentTO(
			Collection<LccResPaymentTO> customerPayments) {
		Map<Integer, Collection<LccPaxPaymentTO>> paxSeqAndLccPaxPaymentTOMap = new HashMap<Integer, Collection<LccPaxPaymentTO>>();

		for (LccResPaymentTO lccResPaymentTO : customerPayments) {
			for (LccPaxPaymentTO lccPaxPaymentTO : lccResPaymentTO.getColLCCPaxPaymentTO()) {
				Collection<LccPaxPaymentTO> colLccPaxPaymentTO = paxSeqAndLccPaxPaymentTOMap
						.get(lccPaxPaymentTO.getPaxSequence());

				if (colLccPaxPaymentTO == null) {
					colLccPaxPaymentTO = new ArrayList<LccPaxPaymentTO>();
					colLccPaxPaymentTO.add(lccPaxPaymentTO);

					paxSeqAndLccPaxPaymentTOMap.put(lccPaxPaymentTO.getPaxSequence(), colLccPaxPaymentTO);
				} else {
					colLccPaxPaymentTO.add(lccPaxPaymentTO);
				}
			}
		}

		return paxSeqAndLccPaxPaymentTOMap;
	}

	/**
	 * we need to propagate all pay currency info to the lcc side as we need to save the actual paycurrency amount in
	 * each operating carrier
	 * 
	 * @param payCurrencyDTO
	 * @return
	 */
	public static PaymentCurrencyAmount getPaymentCurrencyAmount(PayCurrencyDTO payCurrencyDTO) {
		PaymentCurrencyAmount paymentCurrencyAmount = new PaymentCurrencyAmount();
		paymentCurrencyAmount.setPaymentCurrencyAmount(payCurrencyDTO.getTotalPayCurrencyAmount());
		CurrencyCodeGroup codeGroup = new CurrencyCodeGroup();
		codeGroup.setCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
		paymentCurrencyAmount.setCurrencyCodeGroup(codeGroup);
		paymentCurrencyAmount.setPayCurrMultiplyingExchangeRate(payCurrencyDTO.getPayCurrMultiplyingExchangeRate());
		paymentCurrencyAmount.setBoundaryValue(payCurrencyDTO.getBoundaryValue());
		paymentCurrencyAmount.setBreakPointValue(payCurrencyDTO.getBreakPointValue());
		return paymentCurrencyAmount;
	}

}
