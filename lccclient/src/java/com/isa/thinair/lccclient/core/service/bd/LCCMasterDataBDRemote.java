package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.lccclient.api.service.LCCMasterDataBD;

@Remote
public interface LCCMasterDataBDRemote extends LCCMasterDataBD {
}
