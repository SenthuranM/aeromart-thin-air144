/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.lccclient.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.core.bl.LCCCalculateDiscountBL;
import com.isa.thinair.lccclient.core.bl.LCCCheckBlacklistPaxBL;
import com.isa.thinair.lccclient.core.bl.LCCClientAutoCancellationBL;
import com.isa.thinair.lccclient.core.bl.LCCClientClearAlertBL;
import com.isa.thinair.lccclient.core.bl.LCCClientLoadGroundSegmentBL;
import com.isa.thinair.lccclient.core.bl.LCCClientLoadPaxConfigBL;
import com.isa.thinair.lccclient.core.bl.LCCClientLoyaltyRedeemBL;
import com.isa.thinair.lccclient.core.bl.LCCClientModifyReservationBL;
import com.isa.thinair.lccclient.core.bl.LCCClientPassengerCouponBL;
import com.isa.thinair.lccclient.core.bl.LCCClientPassengerRefundBL;
import com.isa.thinair.lccclient.core.bl.LCCClientPaymentWorkFlow;
import com.isa.thinair.lccclient.core.bl.LCCClientReconsileDummyCarrierForOwnBookingBL;
import com.isa.thinair.lccclient.core.bl.LCCClientRemovePaxBL;
import com.isa.thinair.lccclient.core.bl.LCCClientReprotectPaxBL;
import com.isa.thinair.lccclient.core.bl.LCCClientResAlterationBL;
import com.isa.thinair.lccclient.core.bl.LCCClientResAlterationQueryBL;
import com.isa.thinair.lccclient.core.bl.LCCClientRetrieveChargeAdjustmentTypesBL;
import com.isa.thinair.lccclient.core.bl.LCCClientSearchReservationBL;
import com.isa.thinair.lccclient.core.bl.LCCClientTransferSegmentsBL;
import com.isa.thinair.lccclient.core.bl.LCCCreateReservationBL;
import com.isa.thinair.lccclient.core.bl.LCCETicketInfoBL;
import com.isa.thinair.lccclient.core.bl.LCCNewGroupPnrGeneratorBL;
import com.isa.thinair.lccclient.core.bl.LCCRequoteReservationBL;
import com.isa.thinair.lccclient.core.service.bd.LCCReservationBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCReservationBDRemote;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;

@Stateless
@RemoteBinding(jndiBinding = "LCCReservationService.remote")
@LocalBinding(jndiBinding = "LCCReservationService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCReservationServiceBean extends PlatformBaseSessionBean implements LCCReservationBDLocal, LCCReservationBDRemote {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCClientSearchReservationBL.searchReservations(reservationSearchDTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCClientReservation searchReservationByPNR(LCCClientPnrModesDTO lcclientPnrModesDTO,
			ModificationParamRQInfo paramRQInfo, TrackInfoDTO trackInfoDTO) throws ModuleException {
		return LCCClientSearchReservationBL.searchReservationByPNR(lcclientPnrModesDTO, this.getUserPrincipal(), paramRQInfo,
				trackInfoDTO);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation book(CommonReservationAssembler lcclientReservationAssembler, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCCreateReservationBL.book(lcclientReservationAssembler, this.getUserPrincipal(), this.sessionContext,
				trackInfoDTO);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationBalanceTO getResAlterationBalanceSummary(Set<LCCClientReservationPax> passengers,
			LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientResAlterationQueryBL.getReservationBalanceSummary(passengers, lccClientResAlterQueryModesTO,
				this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationBalanceTO
			getRequoteBalanceSummary(RequoteBalanceQueryRQ lccClientResRequoteQueryTO, TrackInfoDTO trackInfo)
					throws ModuleException {
		return LCCRequoteReservationBL.getRequoteBalanceSummary(lccClientResRequoteQueryTO, trackInfo, this.getUserPrincipal());
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce lccRequoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		return LCCRequoteReservationBL.lccRequoteModifySegments(requoteModifyRQ, trackInfo, this.getUserPrincipal(),
				this.sessionContext);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		return LCCClientResAlterationBL.cancelReservation(lccClientResAlterModesTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		return LCCClientResAlterationBL.cancelSegments(lccClientResAlterModesTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCClientResAlterationBL.modifySegments(lccClientResAlterQueryModesTO, this.getUserPrincipal(),
				this.sessionContext, trackInfoDTO);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation
			addSegments(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfoDTO)
					throws ModuleException {
		return LCCClientResAlterationBL.addSegments(lccClientResAlterQueryModesTO, this.getUserPrincipal(), this.sessionContext,
				trackInfoDTO);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List[] getGroundSegment(GroundSegmentTO groundSegmentTO, BasicTrackInfo trackInfo) throws ModuleException {
		return LCCClientLoadGroundSegmentBL.getGroundSegment(groundSegmentTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyContactInfo(String pnr, String version, CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, String appIndicator, TrackInfoDTO trackInfo)
			throws ModuleException {
		LCCClientModifyReservationBL.modifyContactInfo(pnr, version, lccClientReservationContactInfo,
				oldLccClientReservationContactInfo, this.getUserPrincipal(), appIndicator, trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyPassengerInfo(LCCClientReservation lccClientReservation, LCCClientReservation oldLccClientReservation,
			Collection<String> allowedOperations, String appIndicator, TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientModifyReservationBL.modifyPassengerInfo(lccClientReservation, oldLccClientReservation, allowedOperations,
				this.getUserPrincipal(), appIndicator, trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version,
			BasicTrackInfo trackInfo, boolean isInfantPaymentSeparated) throws ModuleException {
		return LCCClientModifyReservationBL.balancePayment(lccClientBalancePayment, version, this.getUserPrincipal(),
				this.sessionContext, trackInfo, isInfantPaymentSeparated);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCClientModifyReservationBL.splitReservation(lccClientReservation, this.getUserPrincipal(), trackInfoDTO);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCClientModifyReservationBL.extendOnHold(groupPNR, extendDateTimeZulu, version, this.getUserPrincipal(),
				trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation adjustCharge(String groupPNr, String version, List<LCCClientChargeAdustment> chargeAdjustments,
			ClientCommonInfoDTO clientInfoDto, BasicTrackInfo trackInfo, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS,
			Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax) throws ModuleException {
		return LCCClientModifyReservationBL.adjustCharge(groupPNr, version, chargeAdjustments, getUserPrincipal(clientInfoDto),
				trackInfo, serviceTaxRS, handlingFeeByPax);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCClientModifyReservationBL.transferOwnership(groupPNR, transfereeAgent, version, this.getUserPrincipal(),
				trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserNoteTO> searchReservationAudit(String pnr, String carrierCode, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCClientModifyReservationBL.searchReservationHistory(pnr, carrierCode, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserNoteTO> searchUserNoteHistory(String pnr, String carrierCode, BasicTrackInfo trackInfo, boolean isClassifyUN)
			throws ModuleException {
		return LCCClientModifyReservationBL.searchUserNotes(pnr, carrierCode, this.getUserPrincipal(), trackInfo, isClassifyUN);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearSegmentAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCClientClearAlertBL.clearSegmentAlerts(lccClientClearAlertDTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transferSegments(LCCClientTransferSegmentTO clientTransferSegmentTO, BasicTrackInfo trackInfo, String salesCahnnelKey)
			throws ModuleException {
		if (clientTransferSegmentTO.getNewSegmentDetails().size() != clientTransferSegmentTO.getOldSegmentDetails().size()) {
			throw new ModuleException("airreservations.transfersegment.segment.notmatch");
		}
		LCCClientTransferSegmentsBL.transferSegments(clientTransferSegmentTO, this.getUserPrincipal(), trackInfo, salesCahnnelKey);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<String> reprotect(LCCClientTransferSegmentTO transferSegmentTO, Map<Integer, String> fltSegToPnrSegMap)
			throws ModuleException {
		return LCCClientReprotectPaxBL.reprotect(transferSegmentTO, fltSegToPnrSegMap, this.getUserPrincipal());
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<Integer, CommonCreditCardPaymentInfo> makeTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo,
			Collection<LCCClientPaymentInfo> colLCCClientPaymentInfo, boolean isCredit, String originatorCarrierCode,
			TrackInfoDTO trackInfo) throws ModuleException {
		return LCCClientPaymentWorkFlow.makeTemporyPaymentEntry(groupPNR, lccClientReservationContactInfo,
				colLCCClientPaymentInfo, isCredit, getCallerCredentials(trackInfo), this.sessionContext, originatorCarrierCode,
				this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, LCCClientPaymentAssembler> makePayment(CommonReservationContactInfo contactInfo, String pnr,
			String transactionIdentifier, Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO) throws ModuleException {
		return LCCClientPaymentWorkFlow.makePayment(contactInfo, pnr, transactionIdentifier, paxWisePaymentAssemblers,
				trackInfoDTO, userPrincipal, this.sessionContext);
	}

	// Note : used REQUIRES_NEW because transaction might have rolled back in airreservation module, so we need to
	// begin reversal in a new transaction
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void refundPayment(CommonReservationContactInfo contactInfo, String pnr, String transactionIdentifier,
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientPaymentWorkFlow.refundPayment(contactInfo, pnr, transactionIdentifier, paxWisePaymentAssemblers, trackInfoDTO,
				userPrincipal, this.sessionContext);
	}

	private CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal = this.getUserPrincipal();

		if (userPrincipal == null || userPrincipal.getName() == null) {
			throw new ModuleException("module.credential.error");
		}

		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setTrackInfoDTO(trackInfoDTO);

		credentialsDTO.setUserId(userPrincipal.getUserId());
		credentialsDTO.setCustomerId(userPrincipal.getCustomerId());
		credentialsDTO.setAgentCode(userPrincipal.getAgentCode());
		credentialsDTO.setAgentCurrencyCode(userPrincipal.getAgentCurrencyCode());
		credentialsDTO.setAgentStation(userPrincipal.getAgentStation());
		credentialsDTO.setSalesChannelCode(new Integer(userPrincipal.getSalesChannel()));
		credentialsDTO.setColUserDST(userPrincipal.getColUserDST());
		credentialsDTO.setDefaultCarrierCode(userPrincipal.getDefaultCarrierCode());
		if(userPrincipal.getUserId() != null){
			credentialsDTO.setDisplayName(LCCClientModuleUtil.getSecurityBD().getUser(userPrincipal.getUserId()).getDisplayName());
		}
		return credentialsDTO;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation addInfant(LCCClientReservation lccClientReservation, TrackInfoDTO trackInfoDTO,
			Map<Integer, LCCClientPaymentAssembler> payments, boolean autoCancellationEnabled, boolean mcETGenerationEligible)
			throws ModuleException {
		return LCCClientModifyReservationBL.addInfant(lccClientReservation, this.getUserPrincipal(), trackInfoDTO, payments,
				autoCancellationEnabled, mcETGenerationEligible);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean refundPassengers(CommonReservationAssembler reservationAssembler, String userNotes, TrackInfoDTO trackInfoDTO,
			Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds, boolean isManualRefund)
			throws ModuleException {
		return LCCClientPassengerRefundBL.refundPassengers(reservationAssembler, userNotes, this.getUserPrincipal(),
				this.sessionContext, trackInfoDTO, preferredRefundOrder, pnrPaxOndChgIds);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public LCCClientReservation removePassenger(String groupPnr, String version, List<LCCClientReservationPax> reservationPaxs,
			CustomChargesTO customChargesTO, TrackInfoDTO trackInfoDTO,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException {
		return LCCClientRemovePaxBL.removePassenger(groupPnr, version, reservationPaxs, customChargesTO, this.getUserPrincipal(),
				trackInfoDTO, paxExternalCharges);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reconcileDummyCarrierReservation(LCCClientPnrModesDTO pnrModesDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCClientReconsileDummyCarrierForOwnBookingBL.reconcileDummyCarrierReservation(pnrModesDTO, this.getUserPrincipal(),
				trackInfo);
	}

	public UserPrincipal getUserPrincipal(ClientCommonInfoDTO clientInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal;
		if (this.getUserPrincipal() != null) {
			userPrincipal = this.getUserPrincipal();
		} else {
			userPrincipal = new UserPrincipal();
		}

		userPrincipal.setIpAddress(clientInfoDTO.getIpAddress());
		userPrincipal.setDefaultCarrierCode(clientInfoDTO.getCarrierCode());
		return userPrincipal;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PaxContactConfigDTO loadPaxContactConfig(String appName, List<String> carrierList, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCClientLoadPaxConfigBL.loadPaxContactConfig(appName, carrierList, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Collection<ChargeAdjustmentTypeDTO>> getCarrierWiseChargeAdjustmentTypes(Set<String> carriercodes,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCClientRetrieveChargeAdjustmentTypesBL.getCarrierWiseChargeAdjustmentTypes(carriercodes,
				this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Pair<String, Integer>, Set<ETicketInfoTO>> getETicketInformation(List<String> lccUniqueIDs,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCETicketInfoBL.getETicketInformation(lccUniqueIDs, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean executeAutoCancellation() throws ModuleException {
		return LCCClientAutoCancellationBL.excuteAutoCancellation(this.getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCClientAutoCancellationBL.calculateBalanceToPayAfterAutoCancellation(resTO, trackInfoDTO,
				this.getUserPrincipal());
	}

	@Override
	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientPassengerCouponBL.updatePassengerCoupon(paxCouponUpdateRQ, trackInfo, this.getUserPrincipal());
	}
	
	@Override
	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		LCCClientPassengerCouponBL.updateGroupPassengerCoupon(groupPaxCouponUpdateRQ, trackInfo, this.getUserPrincipal());

	}
	
	@Override
	public ReservationDiscountDTO calculateDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCCalculateDiscountBL.calculateDiscount(promotionRQ, trackInfoDTO, this.getUserPrincipal());

	}

	@Override
	public ServiceResponce calculatePaxLoyaltyRedeemableAmounts(RedeemCalculateReq redeemCalculateReqTo,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {
		return LCCClientLoyaltyRedeemBL.getLoyaltyRedeemableAmounts(redeemCalculateReqTo, userPrincipal, trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientModifyReservationBL.addUserNote(userNoteTO, trackInfoDTO, this.getUserPrincipal());
	}

	@Override
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<LCCClientReservationPax> paxList, boolean getOnlyFirstElement,List<String> participatingOperatingCarriers, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return LCCCheckBlacklistPaxBL.getBalcklistedPaxReservation(paxList,getOnlyFirstElement,participatingOperatingCarriers, trackInfoDTO, this.getUserPrincipal());
		 
	}
	
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo, String operatingCarrier) throws ModuleException{
		return LCCClientModifyReservationBL.getSelfReprotectFlights(alertId, trackInfo, this.getUserPrincipal(), operatingCarrier);
	}	

	public LCCClientReservation reverseRefundableCharges(String groupPNr, String version, boolean isNoShowTaxReverse,
			List<LCCClientChargeReverse> chargeReverseList, ClientCommonInfoDTO clientInfoDto, BasicTrackInfo trackInfo)
			throws ModuleException{
		return LCCClientModifyReservationBL.reverseRefundableCharges(groupPNr, version,isNoShowTaxReverse, chargeReverseList, getUserPrincipal(clientInfoDto),
				trackInfo);
	}

	@Override
	public String generateNewPnr(String originatingCarrier, BasicTrackInfo trackInfo) throws ModuleException {
		return LCCNewGroupPnrGeneratorBL.generateNewPnr(originatingCarrier, this.getUserPrincipal(), trackInfo);
	}
}