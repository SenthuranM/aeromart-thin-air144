package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierExternalChargesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierPaxProductAmounts;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierProductCodeWiseAmountDue;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierRewardIds;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseFlightRPH;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DiscountInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCarrierExternalChargesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCarrierProductAmountDue;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductAmounts;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductPoints;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCLoyaltyRedeemRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCLoyaltyRedeemRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;
import com.isa.thinair.promotion.api.utils.ResponceCodes;

public class LCCClientLoyaltyRedeemBL {
	private static final Log log = LogFactory.getLog(LCCClientLoyaltyRedeemBL.class);

	public static ServiceResponce getLoyaltyRedeemableAmounts(RedeemCalculateReq redeemCalculateReqTo,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO) throws ModuleException {

		LCCLoyaltyRedeemRQ lccLoyaltyRedeemRQ = new LCCLoyaltyRedeemRQ();

		lccLoyaltyRedeemRQ.setMemberAccountId(redeemCalculateReqTo.getMemberAccountId());

		lccLoyaltyRedeemRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(redeemCalculateReqTo.getTxnIdntifier()));
		lccLoyaltyRedeemRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));

		if (redeemCalculateReqTo.getPaxCarrierExternalCharges() != null) {
			lccLoyaltyRedeemRQ.getPaxCarrierExtChgMap().addAll(
					composePassengerExternalChargeMap(redeemCalculateReqTo.getPaxCarrierExternalCharges(), new Date()));
		}

		lccLoyaltyRedeemRQ.getCarrierWiseFlightRPHList().addAll(
				composeCarrierWiseFlightRPHList(redeemCalculateReqTo.getCarrierWiseFlightRPH()));
		lccLoyaltyRedeemRQ.setRedeemRequestAmount(redeemCalculateReqTo.getRequestedLoyaltyPointsAmount());
		lccLoyaltyRedeemRQ.setAppIndicator(trackInfoDTO.getAppIndicator().toString());
		lccLoyaltyRedeemRQ.setIssueRewardIds(redeemCalculateReqTo.isIssueRewardIds());
		lccLoyaltyRedeemRQ.setPayForOHD(redeemCalculateReqTo.isPayForOHD());
		lccLoyaltyRedeemRQ.setOriginatorPnr(redeemCalculateReqTo.getPnr());		
		lccLoyaltyRedeemRQ.setDiscountInfo(composeDiscountDetails(redeemCalculateReqTo.getDiscountRQ()));
		lccLoyaltyRedeemRQ.setFlexiQuote(redeemCalculateReqTo.isFlexiQuote());
		lccLoyaltyRedeemRQ.setRemainingPoint(redeemCalculateReqTo.getRemainingPoint());
		lccLoyaltyRedeemRQ.setMemberEnrollingCarrier(redeemCalculateReqTo.getMemberEnrollingCarrier());
		lccLoyaltyRedeemRQ.setMemberExternalId(redeemCalculateReqTo.getMemberExternalId());

		if (redeemCalculateReqTo.getPaxCarrierProductDueAmount() != null
				&& redeemCalculateReqTo.getPaxCarrierProductDueAmount().size() > 0) {
			List<PaxCarrierProductAmountDue> paxCarrierProductAmountDueList = new ArrayList<PaxCarrierProductAmountDue>();
			for (Integer paxSeq : redeemCalculateReqTo.getPaxCarrierProductDueAmount().keySet()) {
				PaxCarrierProductAmountDue paxCarrierProductAmountDue = new PaxCarrierProductAmountDue();
				paxCarrierProductAmountDue.setKey(paxSeq);
				Map<String, Map<String, BigDecimal>> carrierProductDue = redeemCalculateReqTo.getPaxCarrierProductDueAmount()
						.get(paxSeq);
				for (String carrierCode : carrierProductDue.keySet()) {
					CarrierProductCodeWiseAmountDue carrierProductCodeWiseAmountDue = new CarrierProductCodeWiseAmountDue();
					carrierProductCodeWiseAmountDue.setKey(carrierCode);
					carrierProductCodeWiseAmountDue.getValue().addAll(
							Transformer.populateStringDecimalMap(carrierProductDue.get(carrierCode)));
					paxCarrierProductAmountDue.getValue().add(carrierProductCodeWiseAmountDue);
				}

				paxCarrierProductAmountDueList.add(paxCarrierProductAmountDue);
			}
			lccLoyaltyRedeemRQ.getPaxCarrierProductAmountDue().addAll(paxCarrierProductAmountDueList);
		}

		LCCLoyaltyRedeemRS lccLoyaltyRedeemRS = LCConnectorUtils.getMaxicoExposedWS().getLoyaltyRedeemableAmounts(
				lccLoyaltyRedeemRQ);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Map<Integer, Map<String, BigDecimal>> paxWiseProductAmount = new HashMap<Integer, Map<String, BigDecimal>>();
		Map<Integer, Map<String, Double>> paxWiseProductPoints = new HashMap<Integer, Map<String, Double>>();

		for (PaxProductAmounts paxProductAmounts : lccLoyaltyRedeemRS.getPerPaxProductAmounts()) {
			Integer paxSeq = paxProductAmounts.getKey();
			paxWiseProductAmount.put(paxSeq, Transformer.transformStringDecimalMap(paxProductAmounts.getValue()));
		}

		for (PaxProductPoints paxProductPoints : lccLoyaltyRedeemRS.getPerPaxProductPoints()) {
			Integer paxSeq = paxProductPoints.getKey();
			paxWiseProductPoints.put(paxSeq, Transformer.transformStringDoubleMap(paxProductPoints.getValue()));
		}

		List<CarrierPaxProductAmounts> carrierWisePaxProductAmounts = lccLoyaltyRedeemRS.getCarrierPaxProductAmounts();
		List<CarrierRewardIds> carrierWiseRewardIds = lccLoyaltyRedeemRS.getCarrierWiseRedeemIds();

		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayInfoMap = composeCarrierWiseLoyaltyPayInfoMap(
				lccLoyaltyRedeemRS.getMemberAccountId(), carrierWisePaxProductAmounts, carrierWiseRewardIds);

		response.addResponceParam(ResponceCodes.ResponseParams.PAX_PRODUCT_POINTS, paxWiseProductPoints);
		response.addResponceParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT, paxWiseProductAmount);
		response.addResponceParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS, lccLoyaltyRedeemRS.getMemberAvailablePoints());
		response.addResponceParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS_AMOUNT,
				lccLoyaltyRedeemRS.getMemberAvailableAmount());
		response.addResponceParam(ResponceCodes.ResponseParams.CARRIER_LOYALTY_PAYMENTS, carrierWiseLoyaltyPayInfoMap);

		return response;
	}

	private static Map<String, LoyaltyPaymentInfo> composeCarrierWiseLoyaltyPayInfoMap(String memberAccountId,
			List<CarrierPaxProductAmounts> carrierWisePaxProductAmounts, List<CarrierRewardIds> carrierWiseRewardIds) {
		Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayInfoMap = new HashMap<String, LoyaltyPaymentInfo>();
		if (carrierWisePaxProductAmounts != null && carrierWisePaxProductAmounts.size() > 0 && carrierWiseRewardIds != null
				&& carrierWiseRewardIds.size() > 0) {
			for (CarrierPaxProductAmounts carrierPaxProductAmount : carrierWisePaxProductAmounts) {
				String carrierCode = carrierPaxProductAmount.getKey();

				Map<Integer, Map<String, BigDecimal>> paxWiseProductAmount = new HashMap<Integer, Map<String, BigDecimal>>();
				for (PaxProductAmounts paxProductAmounts : carrierPaxProductAmount.getPaxProductAmounts()) {
					Integer paxSeq = paxProductAmounts.getKey();
					paxWiseProductAmount.put(paxSeq, Transformer.transformStringDecimalMap(paxProductAmounts.getValue()));
				}

				LoyaltyPaymentInfo loyaltyPaymentInfo = new LoyaltyPaymentInfo();
				loyaltyPaymentInfo.setTotalPayment(AccelAeroCalculator.parseBigDecimal(AirProxyReservationUtil
						.getTotalRedeemedPoints(paxWiseProductAmount).doubleValue()));
				loyaltyPaymentInfo.setPaxProductPaymentBreakdown(paxWiseProductAmount);
				loyaltyPaymentInfo.setLoyaltyRewardIds(getCarrierRewardIds(carrierCode, carrierWiseRewardIds));
				loyaltyPaymentInfo.setMemberAccountId(memberAccountId);

				carrierWiseLoyaltyPayInfoMap.put(carrierCode, loyaltyPaymentInfo);
			}

		}
		return carrierWiseLoyaltyPayInfoMap;
	}

	private static String[] getCarrierRewardIds(String carrierCode, List<CarrierRewardIds> carrierWiseRewardIds) {
		for (CarrierRewardIds carrierRewardIds : carrierWiseRewardIds) {
			if (carrierRewardIds.getKey().equals(carrierCode)) {
				List<String> rewardIdList = carrierRewardIds.getRewardIds();
				String[] rewardIds = new String[rewardIdList.size()];
				int i = 0;
				for (String e : rewardIdList) {
					rewardIds[i++] = e;
				}
				return rewardIds;
			}
		}
		return null;
	}
	private static String[] convertIntArrayToStringArray(int[] rewardsIds){
		String[] convertedStringArray = new String[rewardsIds.length];
		for (int i = 0; i < rewardsIds.length; i++)
			convertedStringArray[i] = String.valueOf(rewardsIds[i]);
		return convertedStringArray;
	}

	private static List<PaxCarrierExternalChargesMap> composePassengerExternalChargeMap(
			Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges, Date date) {

		List<PaxCarrierExternalChargesMap> paxCarrierExtChgList = new ArrayList<PaxCarrierExternalChargesMap>();
		for (String paxKey : paxCarrierExternalCharges.keySet()) {
			List<CarrierExternalChargesMap> carrierExternalChargesList = new ArrayList<CarrierExternalChargesMap>();
			for (String carrierCode : paxCarrierExternalCharges.get(paxKey).keySet()) {
				CarrierExternalChargesMap carrierExternalChargesMap = new CarrierExternalChargesMap();
				List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
				for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxCarrierExternalCharges.get(paxKey).get(carrierCode)) {
					ExternalCharge externalCharge = new ExternalCharge();
					externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
					externalCharge.setApplicableDateTime(date);
					externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO
							.getExternalCharges()));

					if (lccClientExternalChgDTO.getCarrierCode() == null) {
						externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					} else {
						externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
					}
					externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
					externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
					externalCharge.setCode(lccClientExternalChgDTO.getCode());
					externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
					externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
					externalCharge.setUserNote(lccClientExternalChgDTO.getUserNote());
					externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
					externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
					externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
					externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());

					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
						Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
								.getFlexiBilities();
						Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
						for (FlexiInfoTO flexiInfoTO : flexiBilities) {
							FlexiInfo flexiInfo = new FlexiInfo();
							flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
							flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
							flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
							flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
							flexiInfos.add(flexiInfo);
						}
						externalCharge.getAdditionalDetails().addAll(flexiInfos);
					} 

					externalChargeList.add(externalCharge);
				}

				carrierExternalChargesMap.setCarrierCode(carrierCode);
				carrierExternalChargesMap.getExternalCharges().addAll(externalChargeList);
				carrierExternalChargesList.add(carrierExternalChargesMap);
			}

			PaxCarrierExternalChargesMap paxCarrierExternalChargesMap = new PaxCarrierExternalChargesMap();
			paxCarrierExternalChargesMap.setPaxKey(paxKey);
			paxCarrierExternalChargesMap.getCarrierExternalCharges().addAll(carrierExternalChargesList);
			paxCarrierExtChgList.add(paxCarrierExternalChargesMap);
		}
		return paxCarrierExtChgList;
	}

	private static List<CarrierWiseFlightRPH> composeCarrierWiseFlightRPHList(Map<String, List<String>> carrierWiseFlightRPHMap) {
		List<CarrierWiseFlightRPH> carrierWiseFlightRPHList = new ArrayList<CarrierWiseFlightRPH>();
		for (String carrierCode : carrierWiseFlightRPHMap.keySet()) {
			CarrierWiseFlightRPH carrierWiseFlightRPH = new CarrierWiseFlightRPH();
			FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();
			flightRefNumberRPHList.getFlightRefNumber().addAll(carrierWiseFlightRPHMap.get(carrierCode));

			carrierWiseFlightRPH.setCarrierCode(carrierCode);
			carrierWiseFlightRPH.setFlightRefNumberRPHList(flightRefNumberRPHList);
			carrierWiseFlightRPHList.add(carrierWiseFlightRPH);
		}

		return carrierWiseFlightRPHList;
	}
		
	private static DiscountInfo composeDiscountDetails(DiscountRQ discountRQ) {
		DiscountInfo discountInfo = null;
		if (discountRQ != null && discountRQ.getActionType() != null) {
			discountInfo = new DiscountInfo();
			discountInfo.setActionType(discountRQ.getActionType().toString());
			discountInfo.setDiscountedFareDetails(LCCReservationUtil.populateLccDiscountedFareDetails(discountRQ
					.getDiscountInfoDTO()));
			discountInfo.getPaxCharges().addAll(LCCReservationUtil.populateLccPaxCharges(discountRQ.getPaxChargesList()));

		}

		return discountInfo;

	}

}
