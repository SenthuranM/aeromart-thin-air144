/**
 * 
 */
package com.isa.thinair.lccclient.core.bl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxNameInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCDuplicateNameCheckRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCDuplicateNameCheckRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;


/**
 * @author suneth
 *
 */
public class LCCClientDuplicateNameCheckBL {

	private Set<LCCClientReservationPax> lccResPaxs;
	private Set<FlightSegmentTO> fltSegs;
	private BasicTrackInfo trackInfo;
	private UserPrincipal userPrincipal;
	
	public LCCClientDuplicateNameCheckBL(List<FlightSegmentTO> fltSegs , List<LCCClientReservationPax> lccResPaxs , BasicTrackInfo trackInfo , UserPrincipal userPrincipal){
		
		this.fltSegs = new HashSet<FlightSegmentTO>(fltSegs);
		this.lccResPaxs = new HashSet<LCCClientReservationPax>(lccResPaxs);
		this.trackInfo = trackInfo;
		this.userPrincipal = userPrincipal;
		
	}

	public boolean duplicateNameCheck(){
		
		LCCDuplicateNameCheckRQ lccDuplicateNameCheckRQ = new LCCDuplicateNameCheckRQ();
		
		lccDuplicateNameCheckRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		
		for(FlightSegmentTO fltSegTO : this.fltSegs){
			
			FlightSegment fltSeg = new FlightSegment();
			fltSeg.setFlightRefNumber(fltSegTO.getFlightRefNumber());
			fltSeg.setMarketingCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			fltSeg.setOperatingAirline(fltSegTO.getOperatingAirline());
			
			lccDuplicateNameCheckRQ.getFlightSegment().add(fltSeg);
			
		}
		
		for(LCCClientReservationPax lccResPax : lccResPaxs){
			
			PaxNameInfo paxNameInfo = new PaxNameInfo();
			
			paxNameInfo.setTitle(lccResPax.getTitle());
			paxNameInfo.setFirstName(lccResPax.getFirstName());
			paxNameInfo.setLastName(lccResPax.getLastName());
			paxNameInfo.setPaxReference(lccResPax.getPaxType());
			
			lccDuplicateNameCheckRQ.getReservationPax().add(paxNameInfo);
			
		}

		try {
			
			LCCDuplicateNameCheckRS lccDuplicateNameCheckRS =  LCConnectorUtils.getMaxicoExposedWS().duplicateNameCheck(lccDuplicateNameCheckRQ);
			return lccDuplicateNameCheckRS.isHasDuplicates();
			
		} catch (ModuleException e) {

			e.printStackTrace();
			return false;
			
		}
		
		
		
	}
	
}
