package com.isa.thinair.lccclient.core.bl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceKey;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequestMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingAirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingInsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSeatRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSpecialRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Country;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceExternalContentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredAirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredJourneyDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerSelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeatMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingMethod;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciModifyRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciTaxApplicabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciTaxApplicabilityRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryAvailabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryAvailabilityRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryBlockSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryBlockSeatRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryReleaseSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryReleaseSeatRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRS;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationRequest;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDetailDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.LccAncillaryUtils;
import com.isa.thinair.lccclient.core.util.LCCAncillaryUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * LCC WebService client used to request of interlined ancillary services.
 * 
 * @author Abheek Das Gupta
 * @since 1.0
 */
public class LCCAncillaryServiceBL {

	private static Log log = LogFactory.getLog(LCCAncillaryServiceBL.class);

	/**
	 * Returns the seat map details for the requested flight segments.
	 * 
	 * @param lccSeatMapDTO
	 *            the wrapper for the list of {@link LCCFlightSegmentDTO} objects.
	 * @param selectedLanguage
	 * @param trackInfo
	 * @return the populated {@link LCCSeatMapDTO} object with a list of {@link LCCSegmentSeatMapDTO} objects.
	 * @throws ModuleException
	 */
	public static LCCSeatMapDTO getSeatMap(LCCSeatMapDTO lccSeatMapDTO, UserPrincipal principal, String selectedLanguage,
			BasicTrackInfo trackInfo) throws ModuleException {
		LCCSeatMapDTO seatMapDTOResult = null;

		try {
			String transactionId = lccSeatMapDTO.getTransactionIdentifier();

			LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
			lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(principal, trackInfo));
			lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));
			FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();

			for (FlightSegmentTO flightSegmentTO : lccSeatMapDTO.getFlightDetails()) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
				flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
				flightSegment.setOndSequence(flightSegmentTO.getOndSequence());

				flightSegment.setAdultCount(flightSegmentTO.getAdultCount());
				flightSegment.setChildCount(flightSegmentTO.getChildCount());
				flightSegment.setInfantCount(flightSegmentTO.getInfantCount());
				flightSegment.setFareBasisCode(flightSegmentTO.getFareBasisCode());
				flightSegment.setFlexiID(flightSegmentTO.getFlexiID() == null ? null : BigInteger.valueOf(flightSegmentTO
						.getFlexiID()));
				flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
				flightSegment.setParticipatingJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
				flightSegment.setSectorONDCode(flightSegmentTO.getSectorONDCode());
				flightSegment.setInverseSegmentDeparture(flightSegmentTO.getInverseSegmentDeparture());
				flightSegment.setBookingClass(flightSegmentTO.getBookingClass());

				flightRefNumberRPHList.getFlightRefNumber().add(flightSegmentTO.getFlightRefNumber());
				lccAncillaryRQ.getBookingFlightSegment().add(flightSegment);
			}

			BookingSeatRequest bookingSeatRequest = new BookingSeatRequest();
			bookingSeatRequest.setFlightRefNumberRPHList(flightRefNumberRPHList);

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setSeatRequest(bookingSeatRequest);
			bookingSpecialRequests.setSelectedLanguage(selectedLanguage);
			bookingSpecialRequests.setPnr(lccSeatMapDTO.getPnr());

			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);

			// Call web service method
			LCCAncillaryRS lccAncillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			if (lccAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
			seatMapDTOResult = transformToSeatMapDTO(lccAncillaryRS);

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return seatMapDTOResult;
	}

	/**
	 * Returns the availability statuses of requested ancillaries for the given flightSegmentss
	 * 
	 * @param lccAncillaryAvailabilityDTO
	 * @param userPrincipal
	 * @param trackInfo
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static AnciAvailabilityRS getAncillaryAvailability(LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {

		AnciAvailabilityRS anciAvailabilityRS = new AnciAvailabilityRS();
		try {

			LCCAncillaryAvailabilityRQ ancillaryAvailabilityRQ = LCCAncillaryUtil.populateLCCAncillaryAvailabilityRQ(
					lccAncillaryAvailabilityDTO, userPrincipal, trackInfo);

			// Call web service method
			LCCAncillaryAvailabilityRS lccAncillaryAvailabilityRS = LCConnectorUtils.getMaxicoExposedWS()
					.getAvailableAncillaries(ancillaryAvailabilityRQ);

			if (lccAncillaryAvailabilityRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryAvailabilityRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			List<LCCAncillaryAvailabilityOutDTO> outAncillaryAvailabilityOutDTOs = LCCAncillaryUtil
					.populateGetAncillaryAvailabilityOutDTO(lccAncillaryAvailabilityRS);
			anciAvailabilityRS.setLccAncillaryAvailabilityOutDTOs(outAncillaryAvailabilityOutDTOs);
			anciAvailabilityRS.setJnTaxApplicable(lccAncillaryAvailabilityRS.isJnTaxApplicable());
			anciAvailabilityRS.setTaxRatio(lccAncillaryAvailabilityRS.getTaxRatio());
			anciAvailabilityRS.setApplyPenaltyForModification(lccAncillaryAvailabilityRS.isApplyPenaltyForModification());

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return anciAvailabilityRS;
	}

	public static LCCAncillaryQuotation getSelectedAncillaryDetails(LCCAncillaryQuotation selectedAncillary, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCAncillaryQuotation lccAncillaryQuotation = new LCCAncillaryQuotation();
		try {
			LCCSelectedAncillaryRQ selectedAncillaryRQ = LccAncillaryUtils.populateLCCSelectedAncillaryRQ(selectedAncillary, userPrincipal, trackInfo);
			
			selectedAncillaryRQ.setPnr(selectedAncillary.getPnr());
			selectedAncillaryRQ.setModifyAnci(selectedAncillary.isModifyAnci());
			selectedAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(selectedAncillary.getTransactionId()));
			selectedAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

			List<FlightSegment> flightSegments = (List<FlightSegment>)LCCAncillaryUtil.getBookingFlightSegments(selectedAncillary);
			for (FlightSegment flightSegment : flightSegments) {
				flightSegment.setFlightRefNumber(String.valueOf(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber())));
			}


			// Call web service method
			LCCSelectedAncillaryRS lccSelectedAncillaryRS = LCConnectorUtils.getMaxicoExposedWS()
					.getSelectedAncillaryDetails(selectedAncillaryRQ, flightSegments);

			if (lccSelectedAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccSelectedAncillaryRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			lccAncillaryQuotation = LccAncillaryUtils.populateLCCSelectedAncillaryRS(lccSelectedAncillaryRS);

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return lccAncillaryQuotation;
	}

	public static LCCMealResponceDTO getAvailableMeals(List<LCCMealRequestDTO> lccMealRequestDTOs, String transactionIdentifier,
			UserPrincipal userPrincipal, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> flightRefWiseSelectedMeals, String pnr) throws ModuleException {

		LCCMealResponceDTO lccMealResponceDTO = new LCCMealResponceDTO();
		boolean isMultipleMealSelectionEnabled = false;

		try {
			LCCAncillaryRQ lccAncillaryRQ = LCCAncillaryUtil.populatelccAncillaryRQForMeal(lccMealRequestDTOs,
					transactionIdentifier, userPrincipal, selectedLanguage, trackInfo, flightRefWiseSelectedMeals, pnr);

			// Call web service method
			LCCAncillaryRS lccAncillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			if (lccAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			for (FlightSegmentMeals flightSegmentMeals : lccAncillaryRS.getFlightSegmentMeals()) {

				if (flightSegmentMeals.getFlightSegment() != null && flightSegmentMeals.getMeal() != null) {
					LCCFlightSegmentMealsDTO flightSegmentMealsDTO = new LCCFlightSegmentMealsDTO();

					FlightSegmentTO flightSegmentTO = LCCAncillaryUtil.populateFlightSegmentTO(flightSegmentMeals);
					flightSegmentMealsDTO.setFlightSegmentTO(flightSegmentTO);

					flightSegmentMealsDTO.setAnciOffer(flightSegmentMeals.isIsAnciOffer());
					flightSegmentMealsDTO.setAnciOfferName(flightSegmentMeals.getOfferName());
					flightSegmentMealsDTO.setAnciOfferDescription(flightSegmentMeals.getOfferDescription());
					flightSegmentMealsDTO.setOfferedTemplateID(flightSegmentMeals.getOfferedTemplateID());

					for (Meal meal : flightSegmentMeals.getMeal()) {
						LCCMealDTO lccMealDTO = new LCCMealDTO();

						lccMealDTO.setMealCode(meal.getMealCode());
						lccMealDTO.setMealName(meal.getMealName());
						lccMealDTO.setMealDescription(meal.getMealDescription());
						lccMealDTO.setAllocatedMeals(meal.getAllocatedMeals());
						lccMealDTO.setAvailableMeals(meal.getAvailableMeals());
						lccMealDTO.setSoldMeals(meal.getSoldMeals());
						lccMealDTO.setMealCharge(meal.getMealCharge());
						lccMealDTO.setMealImagePath(meal.getMealImageLink());
						lccMealDTO.setMealThumbnailImagePath(meal.getMealThumbnailImageLink());
						lccMealDTO.setMealCategoryID(meal.getMealCategoryID());
						lccMealDTO.setMealCategoryCode(meal.getMealCategoryCode());
						lccMealDTO.setMealStatus(meal.getMealStatus());
						lccMealDTO.setPopularity(meal.getPopularity());
						lccMealDTO.setCategoryRestricted(meal.isCategoryRestricted());
						flightSegmentMealsDTO.getMeals().add(lccMealDTO);
					}

					// set segment level logical cabin class based multi meal enable/disable
					flightSegmentMealsDTO.setMultiMealEnabledForLogicalCabinClass(flightSegmentMeals
							.isMultipleMealSelectionEnabled());

					lccMealResponceDTO.getFlightSegmentMeals().add(flightSegmentMealsDTO);
				}

				isMultipleMealSelectionEnabled = isMultipleMealSelectionEnabled
						|| flightSegmentMeals.isMultipleMealSelectionEnabled();
			}

			lccMealResponceDTO.setMultipleMealSelectionEnabled(isMultipleMealSelectionEnabled);

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return lccMealResponceDTO;
	}

	public static LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, UserPrincipal userPrincipal, String selectedLanguage,
			LCCReservationBaggageSummaryTo baggageSummaryTo, String pnr, BasicTrackInfo trackInfo, boolean isRequote,
			Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID)
			throws ModuleException {

		LCCBaggageResponseDTO lccBaggageResponseDTO = new LCCBaggageResponseDTO();

		try {
			LCCAncillaryRQ lccAncillaryRQ = LCCAncillaryUtil.populatelccAncillaryRQForBaggage(lccBaggageRequestDTOs,
					transactionIdentifier, userPrincipal, selectedLanguage, baggageSummaryTo, pnr, trackInfo, isRequote,
					carrierWiseOndWiseBundlePeriodID);

			// Call web service method
			LCCAncillaryRS lccAncillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			if (lccAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			for (FlightSegmentBaggages flightSegmentBaggages : lccAncillaryRS.getFlightSegmentBaggages()) {

				if (flightSegmentBaggages.getFlightSegment() != null && flightSegmentBaggages.getBaggage() != null) {
					LCCFlightSegmentBaggagesDTO flightSegmentBaggagesDTO = new LCCFlightSegmentBaggagesDTO();

					FlightSegmentTO flightSegmentTO = LCCAncillaryUtil.populateFlightSegmentTO(flightSegmentBaggages);
					flightSegmentBaggagesDTO.setFlightSegmentTO(flightSegmentTO);
					flightSegmentBaggagesDTO.setAnciOffer(flightSegmentBaggages.isIsAnciOffer());
					flightSegmentBaggagesDTO.setOfferName(flightSegmentBaggages.getOfferName());
					flightSegmentBaggagesDTO.setOfferDescription(flightSegmentBaggages.getOfferDescription());
					flightSegmentBaggagesDTO.setOfferedTemplateID(flightSegmentBaggages.getOfferedTemplateID());

					Set<String> ondGroupIds = new HashSet<String>();

					for (Baggage baggage : flightSegmentBaggages.getBaggage()) {
						LCCBaggageDTO lccBaggageDTO = new LCCBaggageDTO();

						lccBaggageDTO.setBaggageName(baggage.getBaggageName());
						lccBaggageDTO.setBaggageDescription(baggage.getBaggageDescription());
						lccBaggageDTO.setBaggageCharge(baggage.getBaggageCharge());
						lccBaggageDTO.setDefaultBaggage(baggage.getDefaultBaggage());
						lccBaggageDTO.setOndBaggageChargeId(baggage.getOndChargeId());
						lccBaggageDTO.setOndBaggageGroupId(baggage.getOndGroupId());
						lccBaggageDTO.setInactiveSelectedBaggage(baggage.isIsSelectedInactiveBaggage());
						// lccBaggageDTO.setAvailableWeight(baggage.get);

						if (baggage.getOndGroupId() != null) {
							ondGroupIds.add(baggage.getOndGroupId());
						}

						flightSegmentBaggagesDTO.getBaggages().add(lccBaggageDTO);
					}

					if (ondGroupIds.size() > 1) {
						throw new ModuleException("airinventory.baggage.ond.should.be.either.ond.or.segment");
					}

					String ondGroupId = BeanUtils.nullHandler(BeanUtils.getFirstElement(ondGroupIds));
					if (ondGroupId.length() > 0) {
						flightSegmentTO.setBaggageONDGroupId(ondGroupId);
					}

					lccBaggageResponseDTO.getFlightSegmentBaggages().add(flightSegmentBaggagesDTO);
				}
			}
		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return lccBaggageResponseDTO;
	}

	public static List<LCCFlightSegmentSSRDTO> getSpecialServiceRequests(List<FlightSegmentTO> flightSegmentTOs,
			String transactionId, UserPrincipal userPrincipal, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> flightRefWiseSelectedSSRs, boolean isModifyAnci) throws ModuleException {

		List<LCCFlightSegmentSSRDTO> flightSegmentSSRDTOs = new ArrayList<LCCFlightSegmentSSRDTO>();
		try {
			LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
			lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));
			FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
				flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
				if (flightSegmentTO.getSegmentCode() != null && !"".equals(flightSegmentTO.getSegmentCode())) {
					flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
				} else {
					flightSegment.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightSegmentTO
							.getFlightRefNumber()));
				}
				flightRefNumberRPHList.getFlightRefNumber().add(flightSegmentTO.getFlightRefNumber());
				lccAncillaryRQ.getBookingFlightSegment().add(flightSegment);
			}

			BookingSpecialServiceRequest bookingSpecialServiceRequest = new BookingSpecialServiceRequest();
			bookingSpecialServiceRequest.setFlightRefNumberRPHList(flightRefNumberRPHList);

			List<StringStringListMap> flightRefWiseList = LCCAncillaryUtil
					.populateFlightRefWiseSelectedAnciMap(flightRefWiseSelectedSSRs);

			if (flightRefWiseList != null) {
				bookingSpecialServiceRequest.getFlightRefWiseSelectedSSRs().addAll(flightRefWiseList);
			}

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setSpecialServiceRequest(bookingSpecialServiceRequest);
			bookingSpecialRequests.setSelectedLanguage(selectedLanguage);
			bookingSpecialRequests.setModifyAnci(isModifyAnci);

			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);

			// Call web service method
			LCCAncillaryRS lccAncillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			if (lccAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			for (FlightSegmentSpecialServiceRequests flightSegmentSpecialServiceRequests : lccAncillaryRS
					.getFlightSegmentSpecialServiceRequests()) {

				LCCFlightSegmentSSRDTO flightSegmentSSRDTO = new LCCFlightSegmentSSRDTO();
				flightSegmentSSRDTO.setFlightSegmentTO(LCCAncillaryUtil
						.populateFlightSegmentTO(flightSegmentSpecialServiceRequests.getFlightSegment()));

				for (SpecialServiceRequest specialServiceRequest : flightSegmentSpecialServiceRequests.getSpecialServiceRequest()) {

					LCCSpecialServiceRequestDTO serviceRequest = new LCCSpecialServiceRequestDTO();
					serviceRequest.setSsrCode(specialServiceRequest.getSsrCode());
					serviceRequest.setSsrName(specialServiceRequest.getSsrName());
					serviceRequest.setDescription(specialServiceRequest.getDescription());
					serviceRequest.setCarrierCode(specialServiceRequest.getCarrierCode());
					serviceRequest.setStatus(specialServiceRequest.getStatus());
					serviceRequest.setServiceQuantity(specialServiceRequest.getServiceQuantity());
					serviceRequest.setCharge(specialServiceRequest.getCharge());
					serviceRequest.setAvailableQty(specialServiceRequest.getAvailableQty());

					if (!flightSegmentSSRDTO.getSpecialServiceRequest().contains(serviceRequest)) {
						flightSegmentSSRDTO.getSpecialServiceRequest().add(serviceRequest);
					}
				}

				flightSegmentSSRDTOs.add(flightSegmentSSRDTO);
			}

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}

		return flightSegmentSSRDTOs;
	}

	public static Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAirportServiceRequests(
			List<FlightSegmentTO> flightSegmentTOs, Map<String, Integer> serviceCount, int serviceCategory,
			AppIndicatorEnum appIndicator, String transactionId, String selectedLanguage, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo, boolean isModifyAnci, String pnr) throws ModuleException {
		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> resultMap = new LinkedHashMap<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO>();

		try {
			LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
			lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				lccAncillaryRQ.getBookingFlightSegment().add(getBookingFlightSegment(flightSegmentTO));
			}

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setSelectedLanguage(selectedLanguage);
			bookingSpecialRequests.setPnr(pnr);

			BookingAirportServiceRequest airportServiceRQ = new BookingAirportServiceRequest();
			airportServiceRQ.setAppIndicator(appIndicator.toString());
			airportServiceRQ.setServiceCategory(serviceCategory);

			if (serviceCount != null) {
				for (Entry<String, Integer> entry : serviceCount.entrySet()) {
					StringIntegerMap strIntObj = new StringIntegerMap();
					strIntObj.setKey(entry.getKey());
					strIntObj.setValue(entry.getValue());
					airportServiceRQ.getServiceCount().add(strIntObj);
				}
			}

			bookingSpecialRequests.setAirportServiceRequest(airportServiceRQ);
			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);

			// Call web service method
			LCCAncillaryRS lccAncillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			if (lccAncillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			for (AirportServiceRequestMap apServiceRQMapObj : lccAncillaryRS.getFlightSegmentAirportServiceRequests()) {

				FlightSegmentAirportServiceRequests fltSegAPServiceRQ = apServiceRQMapObj.getValue();

				LCCFlightSegmentAirportServiceDTO fltSegAirportServiceDTO = new LCCFlightSegmentAirportServiceDTO();
				fltSegAirportServiceDTO.setFlightSegmentTO(getFlightSegmentTO(fltSegAPServiceRQ.getFlightSegment()));

				List<LCCAirportServiceDTO> airportServices = new ArrayList<LCCAirportServiceDTO>();
				for (AirportServiceRequest apServiceRequest : fltSegAPServiceRQ.getAirportServices()) {
					boolean addService = false;
					if (isModifyAnci && AppIndicatorEnum.APP_XBE.equals(appIndicator) && "N".equals(apServiceRequest.getStatus())) {
						addService = true;
					} else if ("Y".equals(apServiceRequest.getStatus())) {
						addService = true;
					}
					if (addService) {
						LCCAirportServiceDTO lccAirportServiceDTO = new LCCAirportServiceDTO();
						lccAirportServiceDTO.setAdultAmount(apServiceRequest.getAdultAmount());
						lccAirportServiceDTO.setAirportCode(apServiceRequest.getAirportCode());
						lccAirportServiceDTO.setApplicabilityType(apServiceRequest.getApplicabilityType());
						lccAirportServiceDTO.setCarrierCode(apServiceRequest.getCarrierCode());
						lccAirportServiceDTO.setChildAmount(apServiceRequest.getChildAmount());
						lccAirportServiceDTO.setInfantAmount(apServiceRequest.getInfantAmount());
						lccAirportServiceDTO.setReservationAmount(apServiceRequest.getReservationAmount());
						lccAirportServiceDTO.setServiceCharge(apServiceRequest.getServiceCharge());
						lccAirportServiceDTO.setSsrCode(apServiceRequest.getSsrCode());
						lccAirportServiceDTO.setSsrName(apServiceRequest.getSsrName());
						lccAirportServiceDTO.setSsrDescription(apServiceRequest.getDescription());
						lccAirportServiceDTO.setStatus(apServiceRequest.getStatus());
						lccAirportServiceDTO.setSsrImagePath(apServiceRequest.getSsrImagePath());
						lccAirportServiceDTO.setSsrThumbnailImagePath(apServiceRequest.getSsrThumbnailImagePath());
						airportServices.add(lccAirportServiceDTO);
					}

				}

				fltSegAirportServiceDTO.setAirportServices(airportServices);

				resultMap.put(getAirportServiceKeyTO(apServiceRQMapObj.getKey()), fltSegAirportServiceDTO);

			}

		} catch (Exception e) {
			log.error("Execption caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}

		return resultMap;
	}

	private static AirportServiceKeyTO getAirportServiceKeyTO(AirportServiceKey airportServiceKey) {
		return new AirportServiceKeyTO(airportServiceKey.getAirport(), airportServiceKey.getAirportType(),
				airportServiceKey.getDepartureDateTime(), airportServiceKey.getSegmentCode());
	}

	private static BookingFlightSegment getBookingFlightSegment(FlightSegmentTO flightSegmentTO) {
		BookingFlightSegment bookingFlightSegment = new BookingFlightSegment();
		bookingFlightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
		bookingFlightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
		bookingFlightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
		bookingFlightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
		bookingFlightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
		bookingFlightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
		bookingFlightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
		bookingFlightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
		bookingFlightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
		bookingFlightSegment.setSegmentSequence(flightSegmentTO.getSegmentSequence());
		bookingFlightSegment.setOndSequence(flightSegmentTO.getOndSequence());
		bookingFlightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
		bookingFlightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
		return bookingFlightSegment;
	}

	private static FlightSegmentTO getFlightSegmentTO(BookingFlightSegment bookingFlightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());
		flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
		flightSegmentTO.setFlightSegId(Integer.parseInt(bookingFlightSegment.getFlightRefNumber()));
		flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
		flightSegmentTO.setReturnFlag("Y".equals(bookingFlightSegment.getReturnFlag()) ? true : false);
		flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
		flightSegmentTO.setSegmentSequence(bookingFlightSegment.getSegmentSequence());
		flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
		flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
		flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
		return flightSegmentTO;
	}

	public static boolean blockSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionId, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		boolean result = false;
		try {

			LCCAncillaryBlockSeatRQ blockSeatRQ = new LCCAncillaryBlockSeatRQ();

			List<SegmentSeats> segmentSeats = new ArrayList<SegmentSeats>();

			for (LCCSegmentSeatDTO lccBlockSeatDTO : blockSeatDTOs) {

				List<PassengerSelectedSeat> selectedSeats = new ArrayList<PassengerSelectedSeat>();

				List<LCCAirSeatDTO> airSeatDTOs = lccBlockSeatDTO.getAirSeatDTOs();
				for (LCCAirSeatDTO lccAirSeatDTO : airSeatDTOs) {

					PassengerType passengerType = LCCClientCommonUtils.getLCCPaxTypeCode(lccAirSeatDTO.getBookedPassengerType());

					PassengerSelectedSeat selectedSeat = new PassengerSelectedSeat();
					selectedSeat.setPassengerType(passengerType);
					selectedSeat.setSeatNumbers(lccAirSeatDTO.getSeatNumber());

					selectedSeats.add(selectedSeat);
				}
				if (selectedSeats.size() > 0) {
					SegmentSeats segmentSeat = new SegmentSeats();

					FlightSegmentTO flightSegmentTO = lccBlockSeatDTO.getFlightSegmentTO();
					FlightSegment flightSegment = new FlightSegment();
					flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
					flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
					flightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
					flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
					flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
					flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
					segmentSeat.setFlightSegments(flightSegment);
					segmentSeat.getPassengerSelectedSeat().addAll(selectedSeats);
					segmentSeats.add(segmentSeat);
				}
			}

			blockSeatRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			blockSeatRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));
			blockSeatRQ.getSegmentSeats().addAll(segmentSeats);

			// Call web service method
			LCCAncillaryBlockSeatRS blockSeatRS = LCConnectorUtils.getMaxicoExposedWS().blockSeats(blockSeatRQ);

			if (blockSeatRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(blockSeatRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			} else {
				result = true;
			}
		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return result;
	}

	public static boolean releaseSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionId, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		boolean result = false;
		try {

			LCCAncillaryReleaseSeatRQ releaseSeatRQ = new LCCAncillaryReleaseSeatRQ();

			List<SegmentSeats> segmentSeats = new ArrayList<SegmentSeats>();

			for (LCCSegmentSeatDTO lccBlockSeatDTO : blockSeatDTOs) {
				SegmentSeats segmentSeat = new SegmentSeats();

				FlightSegmentTO flightSegmentTO = lccBlockSeatDTO.getFlightSegmentTO();
				FlightSegment flightSegment = new FlightSegment();
				flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
				flightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
				flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
				flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
				flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());

				List<PassengerSelectedSeat> selectedSeats = new ArrayList<PassengerSelectedSeat>();

				List<LCCAirSeatDTO> airSeatDTOs = lccBlockSeatDTO.getAirSeatDTOs();
				for (LCCAirSeatDTO lccAirSeatDTO : airSeatDTOs) {

					PassengerSelectedSeat selectedSeat = new PassengerSelectedSeat();
					selectedSeat.setSeatNumbers(lccAirSeatDTO.getSeatNumber());

					selectedSeats.add(selectedSeat);
				}
				segmentSeat.setFlightSegments(flightSegment);
				segmentSeat.getPassengerSelectedSeat().addAll(selectedSeats);

				segmentSeats.add(segmentSeat);
			}

			releaseSeatRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			releaseSeatRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));
			releaseSeatRQ.getSegmentSeats().addAll(segmentSeats);

			// Call web service method
			LCCAncillaryReleaseSeatRS releaseSeatRS = LCConnectorUtils.getMaxicoExposedWS().releaseSeats(releaseSeatRQ);

			if (releaseSeatRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(releaseSeatRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			} else {
				result = true;
			}
		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return result;
	}

	public static List<LCCInsuranceQuotationDTO> getInsuranceQuotations(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCInsuredPassengerDTO> insuredPassengerDTOs, LCCInsuredJourneyDTO insuredJourneyDTO,
			UserPrincipal userPrincipal, String transactionId, BasicTrackInfo trackInfo, LCCInsuredContactInfoDTO contactInfo)
			throws ModuleException {

		List<LCCInsuranceQuotationDTO> quotationDTOs = new ArrayList<LCCInsuranceQuotationDTO>();

		LCCInsuranceQuotationDTO quotationDTO = null;
		try {
			LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
			lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionId));
			FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
				flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
				flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
				flightSegment.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
				flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
				flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
				flightSegment.setReturnFlag(Boolean.toString(flightSegmentTO.isReturnFlag()));

				flightSegment.setAdultCount(flightSegmentTO.getAdultCount());
				flightSegment.setChildCount(flightSegmentTO.getChildCount());
				flightSegment.setInfantCount(flightSegmentTO.getInfantCount());
				flightSegment.setFareBasisCode(flightSegmentTO.getFareBasisCode());
				flightSegment.setFlexiID(flightSegmentTO.getFlexiID() == null ? null : BigInteger.valueOf(flightSegmentTO
						.getFlexiID()));
				flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
				flightSegment.setParticipatingJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
				flightSegment.setSectorONDCode(flightSegmentTO.getSectorONDCode());
				flightSegment.setInverseSegmentDeparture(flightSegmentTO.getInverseSegmentDeparture());
				flightSegment.setBookingClass(flightSegmentTO.getBookingClass());

				flightRefNumberRPHList.getFlightRefNumber().add(flightSegmentTO.getFlightRefNumber());
				lccAncillaryRQ.getBookingFlightSegment().add(flightSegment);
			}

			BookingInsuranceRequest insuranceRequest = new BookingInsuranceRequest();
			insuranceRequest.setInsuredJourneyDetails(populateInsuredJourneyDetails(insuredJourneyDTO));
			insuranceRequest.setSelectedLanguage(insuredJourneyDTO.getSelectedLanguage());
			insuranceRequest.setSalesChannel(insuredJourneyDTO.getSalesChannel());
			populateFlightSegmentList(flightSegmentTOs, insuranceRequest);
			for (LCCInsuredPassengerDTO lccInsuredPassengerDTO : insuredPassengerDTOs) {
				InsuredAirTraveler airTraveler = populateInsuredAirTraveler(lccInsuredPassengerDTO);
				insuranceRequest.getAirTravelers().add(airTraveler);
			}
			insuranceRequest.setInsuredContactInfoDTO(populateInsuredContactInfo(contactInfo));

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setInsuranceRequest(insuranceRequest);

			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);

			LCCAncillaryRS ancillaryRS = LCConnectorUtils.getMaxicoExposedWS().getAncillaryDetail(lccAncillaryRQ);

			List<InsuranceQuotation> insuranceQuotations = ancillaryRS.getInsuranceQuotations();
			if (ancillaryRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(ancillaryRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			} else if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {

				for (InsuranceQuotation insuranceQuotation : insuranceQuotations) {

					if (insuranceQuotation.isSuccess()) {
						quotationDTO = new LCCInsuranceQuotationDTO();
						quotationDTO.setPolicyCode(insuranceQuotation.getPolicyCode());
						quotationDTO.setOperatingAirline(insuranceQuotation.getOperatingAirline());
						quotationDTO.setQuotedCurrencyCode(insuranceQuotation.getQuotedCurrencyCode());
						quotationDTO.setQuotedTotalPremiumAmount(insuranceQuotation.getQuotedTotalPremiumAmount());
						quotationDTO.setInsuranceRefNumber(String.valueOf(insuranceQuotation.getInsuranceRefNumber()));
						quotationDTO.setIsEuropianCountry(insuranceQuotation.isIsEuropianCountry());
						quotationDTO.setAnciOffer(insuranceQuotation.isAnciOffer());
						quotationDTO.setSelectedLanguageAnciOfferName(insuranceQuotation.getAnciOfferName());
						quotationDTO.setSelectedLanguageAnciOfferDescription(insuranceQuotation.getAnciOfferDescription());
						quotationDTO.setSsrFeeCode(insuranceQuotation.getSsrFeeCode());
						quotationDTO.setPlanCode(insuranceQuotation.getPlanCode());

						quotationDTO.setRank(insuranceQuotation.getRank());
						quotationDTO.setGroupID(insuranceQuotation.getGroupID());
						quotationDTO.setSubGroupID(insuranceQuotation.getSubGroupID());
						quotationDTO.setExternalContent(insuranceQuotation.isExternalContent());
						quotationDTO.setIsEuropianCountry(insuranceQuotation.isIsEuropianCountry());
						quotationDTO.setPopup(insuranceQuotation.isPopup());
						quotationDTO.setDisplayName(insuranceQuotation.getDisplayName());
						quotationDTO.setInsuranceExternalContent(getExternalContentMap(insuranceQuotation.getInsuranceExternalContentMap()));

						quotationDTOs.add(quotationDTO);

					}

				}

			}

			Collections.sort(quotationDTOs);
		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}

		return quotationDTOs;
	}

	private static InsuredAirTraveler populateInsuredAirTraveler(LCCInsuredPassengerDTO lccInsuredPassengerDTO) {
		InsuredAirTraveler airTraveler = new InsuredAirTraveler();

		PersonName personName = new PersonName();
		personName.setFirstName(lccInsuredPassengerDTO.getNameDTO().getFirstName());
		personName.setSurName(lccInsuredPassengerDTO.getNameDTO().getLastName());

		Telephone telephone = new Telephone();
		telephone.setPhoneNumber(lccInsuredPassengerDTO.getHomePhoneNumber());

		Country country = new Country();
		country.setCountryName(lccInsuredPassengerDTO.getAddressDTO().getCountryName());

		Address address = new Address();
		address.getAddressLine().add(lccInsuredPassengerDTO.getAddressDTO().getStreetLnNm1());
		address.getAddressLine().add(lccInsuredPassengerDTO.getAddressDTO().getStreetLnNm2());
		address.setCityName(lccInsuredPassengerDTO.getAddressDTO().getCityName());
		address.setPostalCode(lccInsuredPassengerDTO.getAddressDTO().getZipCode());
		address.setCountry(country);

		airTraveler.setPersonName(personName);
		airTraveler.setAddress(address);
		airTraveler.setEmail(lccInsuredPassengerDTO.getEmail());
		airTraveler.setPolicyHolder(lccInsuredPassengerDTO.isPolicyHolder());
		airTraveler.setTelephone(telephone);

		return airTraveler;
	}

	private static InsuredJourneyDetails populateInsuredJourneyDetails(LCCInsuredJourneyDTO insuredJourneyDTO) {
		InsuredJourneyDetails insuredJourneyDetails = new InsuredJourneyDetails();
		insuredJourneyDetails.setJourneyStartAirportCode(insuredJourneyDTO.getJourneyStartAirportCode());
		insuredJourneyDetails.setJourneyEndAirportCode(insuredJourneyDTO.getJourneyEndAirportCode());
		insuredJourneyDetails.setJourneyStartDate(insuredJourneyDTO.getJourneyStartDate());
		insuredJourneyDetails.setJourneyStartDateOffset(insuredJourneyDTO.getJourneyStartDateOffset());
		insuredJourneyDetails.setJourneyEndDate(insuredJourneyDTO.getJourneyEndDate());
		insuredJourneyDetails.setJourneyEndDateOffset(insuredJourneyDTO.getJourneyEndDateOffset());
		insuredJourneyDetails.setRoundTrip(insuredJourneyDTO.isRoundTrip());
		return insuredJourneyDetails;
	}

	private static InsuredContactInfo populateInsuredContactInfo(LCCInsuredContactInfoDTO contactInfo) {
		InsuredContactInfo insuredContactInfo = new InsuredContactInfo();
		insuredContactInfo.setContactPerson(contactInfo.getContactPerson());
		insuredContactInfo.setAddress1(contactInfo.getAddress1());
		insuredContactInfo.setAddress2(contactInfo.getAddress2());
		insuredContactInfo.setAddress3(contactInfo.getAddress3());
		insuredContactInfo.setCountry(contactInfo.getCountry());
		insuredContactInfo.setEmailAddress(contactInfo.getEmailAddress());
		insuredContactInfo.setPrefLanguage(contactInfo.getPrefLanguage());

		return insuredContactInfo;
	}

	private static LCCSeatMapDTO transformToSeatMapDTO(LCCAncillaryRS lccAncillaryRS) {
		LCCSeatMapDTO lccSeatMapDTO = new LCCSeatMapDTO();

		List<LCCSegmentSeatMapDTO> lccSegmentSeatMapDTOs = new ArrayList<LCCSegmentSeatMapDTO>();

		for (SegmentSeatMap segmentSeatMap : lccAncillaryRS.getSegmentSeatMaps()) {

			FlightSegmentTO flightSegmentDTO = LCCAncillaryUtil.getLCCFlightSegmentDTO(segmentSeatMap);
			LCCSeatMapDetailDTO seatMapDetailDTO = LCCAncillaryUtil.getLCCSeatMapDetailDTO(segmentSeatMap);

			LCCSegmentSeatMapDTO segmentSeatMapDTO = new LCCSegmentSeatMapDTO();

			segmentSeatMapDTO.setFlightSegmentDTO(flightSegmentDTO);
			segmentSeatMapDTO.setLccSeatMapDetailDTO(seatMapDetailDTO);

			segmentSeatMapDTO.setAnciOffer(segmentSeatMap.isIsAnciOffer());
			segmentSeatMapDTO.setAnciOfferName(segmentSeatMap.getOfferName());
			segmentSeatMapDTO.setAnciOfferDescription(segmentSeatMap.getOfferDescription());
			segmentSeatMapDTO.setOfferedAnciTemplateID(segmentSeatMap.getOfferedTemplateID());

			lccSegmentSeatMapDTOs.add(segmentSeatMapDTO);
		}

		lccSeatMapDTO.setLccSegmentSeatMapDTOs(lccSegmentSeatMapDTOs);

		return lccSeatMapDTO;
	}

	@SuppressWarnings("unchecked")
	public static LCCAnciModificationResponse modifyAncillaries(LCCAnciModificationRequest anciModificationRequest,
			SessionContext sessionContext) throws ModuleException {

		LCCAnciModificationResponse response = new LCCAnciModificationResponse();
		PaymentAssembler paymentAssembler = new PaymentAssembler();
		TrackInfoDTO trackInfoDTO = anciModificationRequest.getTrackInfoDTO();
		CommonReservationContactInfo contactInfo = anciModificationRequest.getContactInfo();
		ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(contactInfo);
		String pnr = anciModificationRequest.getCommonAncillaryModifyAssembler().getPnr();
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO,
				anciModificationRequest.getUserPrincipal());

		boolean isPaymentToBeCpatured = anciModificationRequest.isInterlinePaymentAllowed();

		// If payment failed in the capture payment itself, we have to skip the reversal in again.
		// following boolean will be used for that. Until we capture the payment successfully and return back we don't
		// need to perform reversal
		boolean performReversal = false;
		boolean hasPayments = false;
		boolean bspPayment = false;
		boolean isFirstPayment = false;

		try {

			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = anciModificationRequest
					.getCommonAncillaryModifyAssembler().getPassengerPaymentMap();
			
			updateCreditCardExternalChargesMissingFlightRef(
					anciModificationRequest.getCommonAncillaryModifyAssembler().getLccSegments(), paxSeqWisePayAssemblers);
			
			if (isPaymentToBeCpatured) {
				LCCClientPaymentWorkFlow.setLccUniqueTnxId(paxSeqWisePayAssemblers.values());
				for (LCCClientPaymentAssembler lccClientPaymentAssembler : paxSeqWisePayAssemblers.values()) {
					LCCClientPaymentWorkFlow.transform(paymentAssembler, lccClientPaymentAssembler);
				}

				bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());
				hasPayments = false;
				for (LCCClientPaymentAssembler payAssemblers : paxSeqWisePayAssemblers.values()) {
					if (payAssemblers.getPayments() != null && payAssemblers.getPayments().size() > 0) {
						hasPayments = true;
						break;
					}
				}
				if (paymentAssembler.getPayments().size() > 0) {

					ReservationBO.getPaymentInfo(paymentAssembler, pnr);

					Map<Integer, CardPaymentInfo> tempPaymentMap = LCCClientPaymentWorkFlow
							.populateTemPaymentEntries(anciModificationRequest.getCommonAncillaryModifyAssembler()
									.getTemporyPaymentMap());

					// Make the temporary payment entry
					tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
							tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

					ServiceResponce res = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler, pnr,
							transformContactInfo, tempTnxIds);
					performReversal = true;
					Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) res
							.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
					Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) res
							.getResponseParam(CommandParamNames.PAYMENT_INFO);
					Map<String, String> paymentAuthIdMap = (Map<String, String>) res
							.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
					Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) res
							.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

					// Since we return form other module we need updated list.
					paymentAssembler.getPayments().clear();
					paymentAssembler.getPayments().addAll(paymentInfos);

					LCCReservationUtil.combineActualCreditPayments(paxSeqWisePayAssemblers, paxCreditPayments, paymentAuthIdMap,
							paymentBrokerRefMap);

				}
				isFirstPayment = anciModificationRequest.getCommonAncillaryModifyAssembler().getReservationStatus()
						.equals(ReservationStatus.ON_HOLD)
						&& hasPayments;
			}

			anciModificationRequest.getCommonAncillaryModifyAssembler().setPassengerPaymentMap(paxSeqWisePayAssemblers);
			LCCAnciModifyRQ lccAnciModifyRQ = Transformer.transformAnciModification(anciModificationRequest);
			// lccAnciModifyRQ.setCarrierFulfillment(getCarrierFulfillment(anciModificationRequest.getCommonAncillaryModifyAssembler(),
			// anciModificationRequest.getUserPrincipal()));
			lccAnciModifyRQ.setInterlinePaymentsAllowed(anciModificationRequest.isInterlinePaymentAllowed());
			lccAnciModifyRQ.setAirReservation(getAirReservation(anciModificationRequest.getCommonAncillaryModifyAssembler(),
					anciModificationRequest.getUserPrincipal(), contactInfo, isFirstPayment));
			lccAnciModifyRQ.setAutoCnxEnabled(anciModificationRequest.isAutoCnxEnabled());

			// add e ticket details
			if (hasPayments) {
				// Generate E tickets
				List<LCCClientReservationSegment> allLccSegments = new ArrayList<LCCClientReservationSegment>();
				List<LCCClientReservationSegment> unflownLccSegments = new ArrayList<LCCClientReservationSegment>();

				allLccSegments.addAll(anciModificationRequest.getCommonAncillaryModifyAssembler().getLccSegments());
				// get unflown segments
				unflownLccSegments = ETicketUtil.getUnflownSegments(allLccSegments);

				CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(trackInfoDTO);
				ticketInfoDto.setBSPPayment(bspPayment);
				List<LCCClientReservationPax> etGenerationEligiblePax = ETicketUtil.getETGenerationEligiblePaxFromPayment(
						anciModificationRequest.getCommonAncillaryModifyAssembler().getLccPassengers(), paxSeqWisePayAssemblers);

				Map<Integer, Map<Integer, EticketDTO>> passengerEtickets = ETicketUtil.generateIATAETicketNumbers(
						etGenerationEligiblePax, unflownLccSegments, ticketInfoDto);
				Iterator<LCCClientReservationPax> passngerIterator = anciModificationRequest.getCommonAncillaryModifyAssembler()
						.getLccPassengers().iterator();
				boolean etAdded = false;
				while (passngerIterator.hasNext()) {
					LCCClientReservationPax lcclientReservationPax = passngerIterator.next();
					AirTraveler airTraveler = Transformer.transform(lcclientReservationPax, lccAnciModifyRQ.getAirReservation()
							.getTravelerInfo());
					if (passengerEtickets.keySet().contains(lcclientReservationPax.getPaxSequence())) {
						etAdded = true;
						airTraveler.getETickets().addAll(
								ETicketUtil.createPaxEticket(allLccSegments, lcclientReservationPax.getPaxSequence(),
										passengerEtickets.get(lcclientReservationPax.getPaxSequence())));
					}
				}
				if (etAdded) {
					if (isFirstPayment) {
						lccAnciModifyRQ.setTicketingMethod(TicketingMethod.CREATE);
					} else {
						lccAnciModifyRQ.setTicketingMethod(TicketingMethod.EXCHANGE);
					}					
				}
			}

			LCCAnciModifyRS lccAnciModifyRS = LCConnectorUtils.getMaxicoExposedWS().modifyAncillaries(lccAnciModifyRQ);

			if (lccAnciModifyRS.getResponseAttributes().getSuccess() != null) {
				// TODO : update temp payment tnx to success state
				response.setModifcationSuccess(true);
			} else {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAnciModifyRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, "payment success for ancillary", null,
					null, null);
		} catch (Exception ex) {
			log.error(" Anci modification failed ");
			response.setModifcationSuccess(false);
			sessionContext.setRollbackOnly();
			if (isPaymentToBeCpatured && performReversal) {
				log.error("Error in anci modification", ex);
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE, "Payment failed for ancillary",
						null, null, null);
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);
				if (!paymentAssembler.getPayments().isEmpty()) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler, pnr,
							transformContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for ancillary", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return response;
	}

	public static AnciAvailabilityRS getTaxApplicabilityForAncillaries(String carrierCode, String segmentCode,
			EXTERNAL_CHARGES extChg, UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {
		AnciAvailabilityRS anciAvailabilityRS = new AnciAvailabilityRS();
		try {

			LCCAnciTaxApplicabilityRQ lccAnciTaxApplicabilityRQ = LCCAncillaryUtil.populateAnciTaxApplicabilityRQ(carrierCode,
					segmentCode, extChg, userPrincipal, trackInfo);

			LCCAnciTaxApplicabilityRS lccAncillaryAvailabilityRS = LCConnectorUtils.getMaxicoExposedWS().getAnciTaxApplicability(
					lccAnciTaxApplicabilityRQ);

			if (lccAncillaryAvailabilityRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAncillaryAvailabilityRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			anciAvailabilityRS.setJnTaxApplicable(lccAncillaryAvailabilityRS.isJnTaxApplicable());
			anciAvailabilityRS.setTaxRatio(lccAncillaryAvailabilityRS.getTaxRatio());

		} catch (Exception e) {
			log.error("Exception caught", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return anciAvailabilityRS;
	}

	private static AirReservation getAirReservation(CommonAncillaryModifyAssembler commonAncillaryModifyAssembler,
			UserPrincipal userPrincipal, CommonReservationContactInfo contactInfo, boolean isFirstPayment) throws ModuleException {
		AirReservation reservation = new AirReservation();
		reservation.setVersion(commonAncillaryModifyAssembler.getVersion());
		Fulfillment fulfillment = new Fulfillment();
		fulfillment.getCustomerFulfillments().addAll(
				LCCReservationUtil.parseTravelerFulfillments(commonAncillaryModifyAssembler.getPassengerPaymentMap(),
						userPrincipal, commonAncillaryModifyAssembler.getPnr(), false, isFirstPayment));
		reservation.setFulfillment(fulfillment);
		reservation.setContactInfo(LCCClientApiUtils.transform(contactInfo));
		reservation.setPriceInfo(LCCReservationUtil.parsePriceInfo(commonAncillaryModifyAssembler.getPassengerPaymentMap()));
		AirReservationSummary airs = new AirReservationSummary();
		airs.setAdminInfo(new AdminInfo());
		airs.getAdminInfo().setOwnerAgentCode(commonAncillaryModifyAssembler.getOwnerAgentCode());
		reservation.setAirReservationSummary(airs);
		reservation.setTravelerInfo(new TravelerInfo());
		return reservation;
	}

	private static void
			populateFlightSegmentList(List<FlightSegmentTO> flightSegmentTOs, BookingInsuranceRequest insuranceRequest) {
		// List<FlightSegment> flightSegments = new ArrayList<FlightSegment>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			FlightSegment flightSegment = new FlightSegment();
			flightSegment.setDepartureAirportCode(flightSegmentTO.getSegmentCode().substring(0, 3));
			flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
			flightSegment.setArrivalAirportCode(flightSegmentTO.getSegmentCode().substring(4, 7));
			flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
			flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
			flightSegment.setReturnFlag(Boolean.toString(flightSegmentTO.isReturnFlag()));
			insuranceRequest.getFlightSegments().add(flightSegment);
		}

		// return flightSegments;

	}
	
	private static Map<String, String> getExternalContentMap(List<InsuranceExternalContentMap> insuranceExternalContentForDisplay){
		Map<String, String> insuranceExternalContent = new HashMap<String, String>();
		for (InsuranceExternalContentMap insuranceExternalContentMap : insuranceExternalContentForDisplay) {
			insuranceExternalContent.put(insuranceExternalContentMap.getKey(), insuranceExternalContentMap.getValue());			
		}
		return insuranceExternalContent;
	}
	
	/**
	 * current CreditCard charges ignored at the LCC and in order to transform we are setting the flightRef
	 */
	private static void updateCreditCardExternalChargesMissingFlightRef(Collection<LCCClientReservationSegment> segments,
			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers) {

		if (segments != null && !segments.isEmpty() && paxSeqWisePayAssemblers != null && !paxSeqWisePayAssemblers.isEmpty()
				&& paxSeqWisePayAssemblers.values() != null && !paxSeqWisePayAssemblers.values().isEmpty()) {

			String flightRefNumber = null;
			for (LCCClientReservationSegment flightSegment : segments) {
				if (!flightSegment.isFlownSegment() && !StringUtil.isNullOrEmpty(flightSegment.getFlightSegmentRefNumber())) {
					flightRefNumber = flightSegment.getFlightSegmentRefNumber();
					break;
				}

			}

			for (LCCClientPaymentAssembler payAssemblers : paxSeqWisePayAssemblers.values()) {
				Collection<LCCClientExternalChgDTO> allExternalCharges = payAssemblers.getPerPaxExternalCharges();

				if (allExternalCharges != null && !allExternalCharges.isEmpty()) {
					for (LCCClientExternalChgDTO externalCharge : allExternalCharges) {
						if (EXTERNAL_CHARGES.CREDIT_CARD.equals(externalCharge.getExternalCharges())
								&& StringUtil.isNullOrEmpty(externalCharge.getFlightRefNumber())
								&& !StringUtil.isNullOrEmpty(flightRefNumber)) {
							externalCharge.setFlightRefNumber(flightRefNumber);
						}

					}
				}
			}

		}

	}
}
