package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirRouteInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirlineRoutePublishRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirlineRoutePublishRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.services.MaxicoExposedWS;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRSDataTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCCPublishDataUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * BL for publishing Route Info to LCC
 * 
 * @author Navod Ediriweera
 * @since Oct 21, 2009
 */
public class LCCRouteInfoPublisherBL {

	private static final Log log = LogFactory.getLog(LCCRouteInfoPublisherBL.class);

	private MaxicoExposedWS maxicoExposedWS;
	private RouteInfoPublishRQDataTO routeInfoRQDataTO;

	public LCCRouteInfoPublisherBL(RouteInfoPublishRQDataTO routeInfoRQDataTO) {
		this.routeInfoRQDataTO = routeInfoRQDataTO;
	};

	public RouteInfoPublishRSDataTO execute() throws ModuleException {
		try {
			LCCAirlineRoutePublishRQ request = createLCCAirlineRoutePublishRQ(routeInfoRQDataTO);
			this.innitMaxicoExposed();
			// send WS Request
			LCCAirlineRoutePublishRS response = maxicoExposedWS.publishAirlineRoute(request);
			return this.setRSDataTO(response);
		} catch (Exception ex) {
			log.error("Error in publish routes", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * Retrives the Web Service
	 * 
	 * @throws ModuleException
	 */
	private void innitMaxicoExposed() throws ModuleException {
		maxicoExposedWS = LCConnectorUtils.getMaxicoExposedWS();
	}

	/**
	 * Creates the WS Request
	 * 
	 * @param reouteInfoList
	 * @return
	 * @throws ModuleException
	 */
	public static LCCAirlineRoutePublishRQ createLCCAirlineRoutePublishRQ(RouteInfoPublishRQDataTO routeInfoRS)
			throws ModuleException {
		LCCAirlineRoutePublishRQ createdRQ = new LCCAirlineRoutePublishRQ();
		createdRQ.setModifiedBy(AppSysParamsUtil.getDefaultCarrierCode() + " - "
				+ LCCPublishDataUtil.ROUTE_INFO_SCHEDULAR_USER_NAME);
		createdRQ.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());
		createdRQ.setSenderID(LCCPublishDataUtil.AA_CALLER_ID);
		AirRouteInfo airRouteInfo = null;
		for (Iterator<RouteInfo> iterator = routeInfoRS.getPublishedRouteInfo().iterator(); iterator.hasNext();) {
			RouteInfo routeInfo = (RouteInfo) iterator.next();
			airRouteInfo = new AirRouteInfo();
			airRouteInfo.setDistance(routeInfo.getDistance());
			airRouteInfo.setDuration(new BigDecimal("" + routeInfo.getDuration()));
			airRouteInfo.setDurationTolerance(routeInfo.getDurationTolerance());
			airRouteInfo.setOldOndCode(routeInfo.getRouteCode());
			airRouteInfo.setStatus(routeInfo.getStatus());
			airRouteInfo.setRouteInfoIdAA(Long.toString(routeInfo.getRouteId()));
			createdRQ.getRouteInfoList().add(airRouteInfo);
		}

		createdRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		return createdRQ;
	}

	/**
	 * Creates the Response DTO
	 * 
	 * @param lccResponse
	 * @return
	 */
	private RouteInfoPublishRSDataTO setRSDataTO(LCCAirlineRoutePublishRS lccResponse) {
		RouteInfoPublishRSDataTO infoPublishRSDataTO = new RouteInfoPublishRSDataTO();
		infoPublishRSDataTO.getUpdatedRInfoIDs().addAll(lccResponse.getUpdatedRouteInfoIDs());
		if (lccResponse.getResponseAttributes().getErrors() != null) {
			for (Iterator<LCCError> iterator = lccResponse.getResponseAttributes().getErrors().iterator(); iterator.hasNext();) {
				LCCError lccError = iterator.next();
				infoPublishRSDataTO.getErrors().add(
						BeanUtils.nullHandler(lccError.getErrorCode()) + " [" + BeanUtils.nullHandler(lccError.getDescription())
								+ "]");
			}
		}
		return infoPublishRSDataTO;
	}

}
