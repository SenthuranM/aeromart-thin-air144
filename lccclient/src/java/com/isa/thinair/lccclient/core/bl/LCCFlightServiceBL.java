package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DisplayFlightInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightInfoDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLegInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLoadReportDataInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLoadReportSearchCriteria;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightManifestOptionsInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightManifestPage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightManifestSearchInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSearchCriteria;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModuleCriterianInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightLoadReportDataRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightLoadReportDataRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightManifestDataRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightManifestDataRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightManifestSearchRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightManifestSearchRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightSegmentRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCFlightSegmentRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSegmentInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSegmentInfoRS;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoCriteriaDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class LCCFlightServiceBL {

	public static Page searchFlightsForDisplay(FlightManifestSearchDTO searchDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		FlightManifestSearchInfo flightManifestSearchInfo = new FlightManifestSearchInfo();
		flightManifestSearchInfo.setCarrierCode(searchDTO.getCarrierCode());
		if (searchDTO.getFlightCriteria() != null) {
			flightManifestSearchInfo.setFlightCriteria(composeFlightCriteria(searchDTO));
		} else {
			flightManifestSearchInfo.setFlightCriteria(null);
		}
		flightManifestSearchInfo.setIsFlightManifest(searchDTO.isFlightManifest());
		flightManifestSearchInfo.setIsInterline(searchDTO.isInterline());
		flightManifestSearchInfo.setPageSize(searchDTO.getPageSize());
		flightManifestSearchInfo.setStartIndex(searchDTO.getStartIndex());
		flightManifestSearchInfo.getValue().addAll(composeCriterianList(searchDTO));
		flightManifestSearchInfo.setIsDatesInLocal(searchDTO.isDatesInLocal());
		flightManifestSearchInfo.setDataFromReporting(searchDTO.isDataFromReporting());

		LCCFlightManifestSearchRQ lccFlightManifestSearchRQ = new LCCFlightManifestSearchRQ();
		lccFlightManifestSearchRQ.setSearchCriteria(flightManifestSearchInfo);
		lccFlightManifestSearchRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		LCCFlightManifestSearchRS lccFlightManifestSearchRS = LCConnectorUtils.getMaxicoExposedWS().searchFlightsForManifest(
				lccFlightManifestSearchRQ);
		if (lccFlightManifestSearchRS.getResponseAttributes().getSuccess() == null) {
			throw new ModuleException("LCC invocation failed for flight Manifest");
		}
		if (lccFlightManifestSearchRS.getPage() != null) {
			return composePageInfo(lccFlightManifestSearchRS.getPage());
		} else {
			return null;
		}
	}

	public static Object getFlightManifestData(FlightManifestOptionsDTO flightManifestOptionsDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		FlightManifestOptionsInfo flightManifestOptionsInfo = composeFlightManifestOptionsInfo(flightManifestOptionsDTO);

		LCCFlightManifestDataRQ lccFlightManifestDataRQ = new LCCFlightManifestDataRQ();
		lccFlightManifestDataRQ.setManifestOptions(flightManifestOptionsInfo);
		lccFlightManifestDataRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		LCCFlightManifestDataRS lccFlightManifestDataRS = LCConnectorUtils.getMaxicoExposedWS().getFlightManifestData(
				lccFlightManifestDataRQ);
		if (lccFlightManifestDataRS.getResponseAttributes().getSuccess() == null) {
			throw new ModuleException("LCC invocation failed for flight Manifest");
		}
		if (!lccFlightManifestDataRS.isEmailResponse()) {
			if (lccFlightManifestDataRS.getResponseHTML() != null) {
				return lccFlightManifestDataRS.getResponseHTML();
			} else {
				return null;
			}
		} else {
			return new Boolean(lccFlightManifestDataRS.isEmailSuccess());
		}

	}

	public static List<FlightLoadReportDataDTO> getFlightLoadData(ReportsSearchCriteria search, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		FlightLoadReportSearchCriteria criteria = composeFlightLoadReportSearchCriteria(search);

		LCCFlightLoadReportDataRQ lccFlightLoadReportDataRQ = new LCCFlightLoadReportDataRQ();
		lccFlightLoadReportDataRQ.setCriteria(criteria);
		lccFlightLoadReportDataRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		LCCFlightLoadReportDataRS lccFlightLoadReportDataRS = LCConnectorUtils.getMaxicoExposedWS().getFlightLoadReportData(
				lccFlightLoadReportDataRQ);
		if (lccFlightLoadReportDataRS.getResponseAttributes().getSuccess() == null) {
			throw new ModuleException("LCC invocation failed for flight Manifest");
		}

		return composeFlightLoadData(lccFlightLoadReportDataRS.getResultList());

	}

	public static List<FlightSegmentInfoDTO> getFlightSegmentInfo(FlightSegmentInfoCriteriaDTO flightSegmentInfoCriteriaDTO,
	                                                              UserPrincipal userPrincipal, BasicTrackInfo trackInfo)  throws ModuleException {
		LCCSegmentInfoRS lccSegmentInfoRS;

		List<FlightSegmentInfoDTO> flightSegmentInfoDTOs = new ArrayList<FlightSegmentInfoDTO>();
		FlightSegmentInfoDTO flightSegmentInfoDTO;
		LCCSegmentInfoRQ lccSegmentInfoRQ = new LCCSegmentInfoRQ();
		lccSegmentInfoRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal));
		BookingFlightSegment bookingFlightSegment;

		for (FlightSegmentDTO flightSegmentDTO : flightSegmentInfoCriteriaDTO.getFlightSegmentDTOs()) {
			bookingFlightSegment = new BookingFlightSegment();
			bookingFlightSegment.setOperatingAirline(flightSegmentDTO.getOperatingAirline());
			bookingFlightSegment.setFlightRefNumber(String.valueOf(flightSegmentDTO.getFlightSegId()));
			lccSegmentInfoRQ.getFlightSegment().add(bookingFlightSegment);
		}
		lccSegmentInfoRQ.setCalculateSeatFactor(true);
		lccSegmentInfoRQ.setCalculateTotalBaggageWeight(true);


		lccSegmentInfoRS = LCConnectorUtils.getMaxicoExposedWS().getLccSegmentInfo(lccSegmentInfoRQ);

		for (LCCSegmentInfo lccSegInfo : lccSegmentInfoRS.getSegmentInfo()) {
			flightSegmentInfoDTO = new FlightSegmentInfoDTO();
			flightSegmentInfoDTO.setFlightSegId(Integer.parseInt(lccSegInfo.getFlightSegment().getFlightRefNumber()));
			flightSegmentInfoDTO.setBaggageWeight(lccSegInfo.getTotalWeight());
			flightSegmentInfoDTO.setLoadFactor(lccSegInfo.getSeatFactor());

			flightSegmentInfoDTOs.add(flightSegmentInfoDTO);
		}

		return flightSegmentInfoDTOs;
	}
	
	public static FlightSegement getFlightSegment(String flightNumber, Date departureDate, UserPrincipal userPrincipal)
			throws ModuleException {
		FlightSegement flightSegment = new FlightSegement();
		LCCFlightSegmentRS lccFlightSegmentRS;
		LCCFlightSegmentRQ lccFlightSegmentRQ = new LCCFlightSegmentRQ();
		lccFlightSegmentRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal));
		lccFlightSegmentRQ.setDepartureDateLocal(departureDate);
		lccFlightSegmentRQ.setFlightNumber(flightNumber);

		lccFlightSegmentRS = LCConnectorUtils.getMaxicoExposedWS().getLccFlightSegment(lccFlightSegmentRQ);

		flightSegment.setSegmentCode(lccFlightSegmentRS.getSegmentCode());
		flightSegment.setFltSegId(lccFlightSegmentRS.getFlightSegmentId());
		flightSegment.setEstTimeArrivalLocal(lccFlightSegmentRS.getArrivalDateLocal());
		flightSegment.setEstTimeArrivalZulu(lccFlightSegmentRS.getArrivalDateZulu());
		flightSegment.setEstTimeDepatureLocal(lccFlightSegmentRS.getDepartureDateLocal());
		flightSegment.setEstTimeDepatureZulu(lccFlightSegmentRS.getDepartureDateZulu());

		return flightSegment;

	}

		private static FlightSegmentSearchCriteria composeFlightCriteria(FlightManifestSearchDTO searchDTO) {
		FlightSegmentSearchCriteria criteria = new FlightSegmentSearchCriteria();

		criteria.setCheckinStatus(searchDTO.getFlightCriteria().getCheckinStatus());
		criteria.setEstEndTimeZulu(searchDTO.getFlightCriteria().getEstEndTimeZulu());
		criteria.setEstStartTimeZulu(searchDTO.getFlightCriteria().getEstStartTimeZulu());
		criteria.setFlightNumber(searchDTO.getFlightCriteria().getFlightNumber());
		criteria.setSegmentCode(searchDTO.getFlightCriteria().getSegmentCode());
		criteria.setStatus(searchDTO.getFlightCriteria().getStatus());

		return criteria;
	}

	private static List<ModuleCriterianInfo> composeCriterianList(FlightManifestSearchDTO searchDTO) {
		List<ModuleCriterianInfo> lccCiterianList = new ArrayList<ModuleCriterianInfo>();

		List criterian = searchDTO.getCriteria();
		for (int i = 0; i < criterian.size(); ++i) {
			ModuleCriterion moduleCriterion = (ModuleCriterion) criterian.get(i);

			ModuleCriterianInfo lccModuleCriterianInfo = new ModuleCriterianInfo();
			lccModuleCriterianInfo.setCondition(moduleCriterion.getCondition());
			lccModuleCriterianInfo.setFieldName(moduleCriterion.getFieldName());
			lccModuleCriterianInfo.setLeaf(moduleCriterion.isLeaf());
			lccModuleCriterianInfo.getValue().addAll(moduleCriterion.getValue());

			lccCiterianList.add(lccModuleCriterianInfo);
		}

		return lccCiterianList;
	}

	private static Page composePageInfo(FlightManifestPage flightManifestPage) {

		List<DisplayFlightInfo> flightInfoList = flightManifestPage.getPageData();

		List dtoList = new ArrayList();

		for (DisplayFlightInfo info : flightInfoList) {
			FlightInfoDetail flightInfoDetail = info.getFlightInfo();
			Flight flight = composeFlight(flightInfoDetail);

			DisplayFlightDTO displayFlightDTO = new DisplayFlightDTO(flight);
			displayFlightDTO.setAllocated(info.getAllocated());
			displayFlightDTO.setCheckedIn(info.getCheckedIn());
			displayFlightDTO.setOnHold(info.getOnHold());
			displayFlightDTO.setReservatinsExists(info.isReservatinsExists());
			displayFlightDTO.setSeatsSold(info.getSeatsSold());
			displayFlightDTO.setStndBy(info.getStndBy());

			dtoList.add(displayFlightDTO);

		}

		Page page = new Page(flightManifestPage.getTotalNoOfRecords(), flightManifestPage.getStartPosition(),
				flightManifestPage.getEndPosition(), flightManifestPage.getTotalNoOfRecordsInSystem(), dtoList);

		return page;
	}

	private static Flight composeFlight(FlightInfoDetail info) {
		Flight flight = new Flight();

		flight.setAvailableSeatKilometers(info.getAvailableSeatKilometers());
		flight.setDayNumber(info.getDayNumber());
		flight.setDepartureDate(info.getDepartureDate());
		flight.setDepartureDateLocal(info.getDepartureDateLocal());
		flight.setDestinationAptCode(info.getDestinationAptCode());
		flight.setFlightId(info.getFlightId());
		flight.setFlightLegs(composeFlightLegs(info.getFlightLegs()));
		flight.setFlightNumber(info.getFlightNumber());
		flight.setFlightSegements(composeFlightSegements(info.getFlightSegements()));
		for (Object obj : info.getGdsIds()) {
			flight.addGdsId((Integer) obj);
		}
		flight.setManuallyChanged(info.isManuallyChanged());
		flight.setMealNotificationAttempts(info.getMealNotificationAttempts());
		flight.setMealNotificationStatus(info.getMealNotificationStatus());
		flight.setModelNumber(info.getModelNumber());
		flight.setOperationTypeId(info.getOperationTypeId());
		flight.setOriginAptCode(info.getOriginAptCode());
		flight.setOverlapingFlightId(info.getOverlapingFlightId());
		flight.setScheduleId(info.getScheduleId());
		flight.setStatus(info.getStatus());
		flight.setTailNumber(info.getTailNumber());

		return flight;
	}

	private static Set composeFlightLegs(List<FlightLegInfo> legInfoList) {
		Set legs = new HashSet();

		for (FlightLegInfo info : legInfoList) {
			FlightLeg leg = new FlightLeg();

			leg.setDestination(info.getDestination());
			leg.setDuration(info.getDuration());
			leg.setDurationError(info.getDurationError());
			leg.setEstArrivalDayOffset(info.getEstArrivalDayOffset());
			leg.setEstArrivalDayOffsetLocal(info.getEstArrivalDayOffsetLocal());
			leg.setEstArrivalTimeLocal(info.getEstArrivalTimeLocal());
			leg.setEstArrivalTimeZulu(info.getEstArrivalTimeZulu());
			leg.setEstDepartureDayOffset(info.getEstDepartureDayOffset());
			leg.setEstDepartureDayOffsetLocal(info.getEstDepartureDayOffsetLocal());
			leg.setEstDepartureTimeLocal(info.getEstDepartureTimeLocal());
			leg.setEstDepartureTimeZulu(info.getEstDepartureTimeZulu());
			leg.setFlightId(info.getFlightId());
			leg.setId(info.getId());
			leg.setLegNumber(info.getLegNumber());
			leg.setModelRouteId(info.getModelRouteId());
			leg.setOrigin(info.getOrigin());

			legs.add(leg);
		}

		return legs;
	}

	private static Set composeFlightSegements(List<FlightSegmentInfo> infoList) {
		Set segments = new HashSet();
		for (FlightSegmentInfo info : infoList) {
			FlightSegement segement = new FlightSegement();

			segement.setArrivalTerminalId(info.getArrivalTerminalId());
			segement.setCheckinStatus(info.getCheckinStatus());
			segement.setDepartureTerminalId(info.getDepartureTerminalId());
			segement.setEstTimeArrivalLocal(info.getEstArrivalTimeLocal());
			segement.setEstTimeArrivalZulu(info.getEstArrivalTimeZulu());
			segement.setEstTimeDepatureLocal(info.getEstDepartureTimeLocal());
			segement.setEstTimeDepatureZulu(info.getEstDepatureTimeZulu());
			segement.setFlightId(info.getFlightId());
			segement.setFltSegId(info.getFltSegId());
			segement.setOverlapSegmentId(info.getOverlapSegmentId());
			segement.setSegmentCode(info.getSegmentCode());
			segement.setStatus(info.getStatus());
			segement.setValidFlag(info.isValidFlag());

			segments.add(segement);
		}
		return segments;
	}

	private static FlightManifestOptionsInfo composeFlightManifestOptionsInfo(FlightManifestOptionsDTO dto) {
		FlightManifestOptionsInfo info = new FlightManifestOptionsInfo();

		info.setCabinClass(dto.getCabinClass());
		info.setLogicalCabinClass(dto.getLogicalCabinClass());
		info.setCheckInwardConnections(dto.isCheckInwardConnections());
		info.setCheckOutwardConnections(dto.isCheckOutwardConnections());
		info.setConfirmedReservation(dto.isConfirmedReservation());
		info.setEmailId(dto.getEmailId());
		info.setFilterByCabinClass(dto.isFilterByCabinClass());
		info.setFilterByLogicalCabinClass(dto.isFilterByLogicalCabinClass());
		info.setFlightDate(dto.getFlightDate());
		info.setFlightId(dto.getFlightId());
		info.setFlightNo(dto.getFlightNo());
		info.setInwardBoardingAirport(dto.getInwardBoardingAirport());
		info.setInwardConnectionOrigin(dto.getInwardConnectionOrigin());
		info.setInwardFlightNo(dto.getInwardFlightNo());
		info.setIsEmail(dto.isEmail());
		info.setMaxInwardConnectionTime(dto.getMaxInwardConnectionTime());
		info.setMaxOutwardConnectionTime(dto.getMaxOutwardConnectionTime());
		info.setOutwardBoardingAirport(dto.getOutwardBoardingAirport());
		info.setOutwardConnectionDestination(dto.getOutwardConnectionDestination());
		info.setOutwardFlightNo(dto.getOutwardFlightNo());
		info.setRoute(dto.getRoute());
		info.setShowAirportServices(dto.isShowAirportServices());
		info.setShowCreditCardDetails(dto.isShowCreditCardDetails());
		info.setShowMeal(dto.isShowMeal());
		info.setShowPaxIndentification(dto.isShowPaxIndentification());
		info.setShowSeatMap(dto.isShowSeatMap());
		info.setShowSsr(dto.isShowSsr());
		info.setSortOption(dto.getSortOption());
		info.setSummaryMode(dto.getSummaryMode());
		info.setSummaryPosition(dto.getSummaryPosition());
		info.setLoadByPnr(dto.isLoadByPnr());
		info.setViewOption(dto.getViewOption());
		info.setSearchTriggerdByDifferentCarrier(true);
		info.setDataFromReporting(ReportsSearchCriteria.DATASOURCE_TYPE_RPT.equals(dto.getDataSourceMode()) ? true : false);
		return info;
	}

	private static FlightLoadReportSearchCriteria composeFlightLoadReportSearchCriteria(ReportsSearchCriteria search) {
		FlightLoadReportSearchCriteria criteria = new FlightLoadReportSearchCriteria();

		criteria.setConTime(search.getConTime());
		criteria.setDateRangeFrom(search.getDateRangeFrom());
		criteria.setDateRangeTo(search.getDateRangeTo());
		criteria.setFlightNumber(search.getFlightNumber());
		criteria.setFrom(search.getFrom());
		criteria.setOperationType(search.getOperationType());
		criteria.setStatus(search.getStatus());
		criteria.setTo(search.getTo());
		criteria.setCarrierCode(search.getCarrierCode());

		return criteria;
	}

	private static List<FlightLoadReportDataDTO> composeFlightLoadData(List<FlightLoadReportDataInfo> infoList) {
		List<FlightLoadReportDataDTO> dtoList = new ArrayList<FlightLoadReportDataDTO>();

		for (FlightLoadReportDataInfo info : infoList) {
			FlightLoadReportDataDTO dto = new FlightLoadReportDataDTO();

			dto.setConnChild(info.getConnChild());
			dto.setConnectingFlightNumber(info.getConnectingFlightNumber());
			dto.setConnectingSegment(info.getConnectingSegment());
			dto.setConnFemale(info.getConnFemale());
			dto.setConnInfant(info.getConnInfant());
			dto.setConnMale(info.getConnMale());
			dto.setConnOther(info.getConnOther());
			dto.setConnTotal(info.getConnTotal());
			dto.setOriginChild(info.getOriginChild());
			dto.setOriginDate(info.getOriginDate());
			dto.setOriginFemale(info.getOriginFemale());
			dto.setOriginFlightnumber(info.getOriginFlightnumber());
			dto.setOriginInfant(info.getOriginInfant());
			dto.setOriginMale(info.getOriginMale());
			dto.setOriginOther(info.getOriginOther());
			dto.setOriginSegment(info.getOriginSegment());
			dto.setOriginTotal(info.getOriginTotal());

			dtoList.add(dto);
		}

		return dtoList;
	}
}
