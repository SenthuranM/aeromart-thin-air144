package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReadRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientReconsileDummyCarrierForOwnBookingBL {

	private static final Log log = LogFactory.getLog(LCCClientReconsileDummyCarrierForOwnBookingBL.class);

	public static boolean reconcileDummyCarrierReservation(LCCClientPnrModesDTO pnrModesDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		try {
			// FIXME send in the trackinfo DTO
			LCCReadRQ lccReadRQ = Transformer.transform(pnrModesDTO, userPrincipal, null, trackInfo);
			LCCAirBookRS reconsileDummyResRS = LCConnectorUtils.getMaxicoExposedWS().reconsileDummyReservation(lccReadRQ);

			if (reconsileDummyResRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(reconsileDummyResRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			return true;
		} catch (Exception ex) {
			log.error("Error in reconcileDummyCarrierReservation for owm booking", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

}
