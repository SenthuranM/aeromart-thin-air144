package com.isa.thinair.lccclient.core.remoting.ejb;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationRequest;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.lccclient.core.bl.LCCAncillaryServiceBL;
import com.isa.thinair.lccclient.core.service.bd.LCCAncillaryBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCAncillaryBDRemote;

@Stateless
@RemoteBinding(jndiBinding = "LCCAncillaryService.remote")
@LocalBinding(jndiBinding = "LCCAncillaryService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCAncillaryServiceBean extends PlatformBaseSessionBean implements LCCAncillaryBDLocal, LCCAncillaryBDRemote {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCSeatMapDTO getSeatMap(LCCSeatMapDTO lccSeatMapDTO, String selectedLanguage, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCAncillaryServiceBL.getSeatMap(lccSeatMapDTO, this.getUserPrincipal(), selectedLanguage, trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AnciAvailabilityRS getAncillaryAvailability(LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCAncillaryServiceBL.getAncillaryAvailability(lccAncillaryAvailabilityDTO, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCAncillaryQuotation getSelectedAncillaryDetails(LCCAncillaryQuotation selectedAncillary, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCAncillaryServiceBL.getSelectedAncillaryDetails(selectedAncillary, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCMealResponceDTO getAvailableMeals(List<LCCMealRequestDTO> lccMealRequestDTOs, String transactionIdentifier,
			String selectedLanguage, BasicTrackInfo trackInfo, Map<String, Set<String>> flightRefWiseSelectedMeals, String pnr)
			throws ModuleException {
		return LCCAncillaryServiceBL.getAvailableMeals(lccMealRequestDTOs, transactionIdentifier, this.getUserPrincipal(),
				selectedLanguage, trackInfo, flightRefWiseSelectedMeals, pnr);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, String selectedLanguage, LCCReservationBaggageSummaryTo baggageSummaryTo, String pnr,
			BasicTrackInfo trackInfo, boolean isRequote, Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID)
			throws ModuleException {
		return LCCAncillaryServiceBL.getAvailableBaggages(lccBaggageRequestDTOs, transactionIdentifier, this.getUserPrincipal(),
				selectedLanguage, baggageSummaryTo, pnr, trackInfo, isRequote, carrierWiseOndWiseBundlePeriodID);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCFlightSegmentSSRDTO> getSpecialServiceRequests(List<FlightSegmentTO> flightSegmentTOs,
			String transactionIdentifier, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> flightRefWiseSelectedSSRs, boolean isModifyAnci) throws ModuleException {
		return LCCAncillaryServiceBL.getSpecialServiceRequests(flightSegmentTOs, transactionIdentifier, this.getUserPrincipal(),
				selectedLanguage, trackInfo, flightRefWiseSelectedSSRs, isModifyAnci);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAirportServiceRequests(
			List<FlightSegmentTO> flightSegmentTOs, Map<String, Integer> serviceCount, int serviceCategory,
			AppIndicatorEnum appIndicator, String transactionIdentifier, String selectedLanguage, BasicTrackInfo trackInfo,
			boolean isModifyAnci, String pnr) throws ModuleException {
		return LCCAncillaryServiceBL.getAirportServiceRequests(flightSegmentTOs, serviceCount, serviceCategory, appIndicator,
				transactionIdentifier, selectedLanguage, this.getUserPrincipal(), trackInfo, isModifyAnci, pnr);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean blockSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCAncillaryServiceBL.blockSeats(blockSeatDTOs, transactionIdentifier, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean releaseSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, BasicTrackInfo trackInfo)
			throws ModuleException {
		return LCCAncillaryServiceBL.releaseSeats(blockSeatDTOs, transactionIdentifier, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCInsuranceQuotationDTO> getInsuranceQuotations(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCInsuredPassengerDTO> insuredPassengerDTOs, LCCInsuredJourneyDTO insuredJourneyDTO,
			String transactionIdentifier, UserPrincipal userPrincipal, BasicTrackInfo trackInfo,
			LCCInsuredContactInfoDTO contactInfo) throws ModuleException {
		return LCCAncillaryServiceBL.getInsuranceQuotations(flightSegmentTOs, insuredPassengerDTOs, insuredJourneyDTO,
				userPrincipal, transactionIdentifier, trackInfo, contactInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCAnciModificationResponse modifyAncillary(LCCAnciModificationRequest anciModificationRequest) throws ModuleException {
		return LCCAncillaryServiceBL.modifyAncillaries(anciModificationRequest, this.sessionContext);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AnciAvailabilityRS getTaxApplicabilityForAncillaries(String carrierCode, String segmentCode, EXTERNAL_CHARGES extChg,
			BasicTrackInfo trackInfo) throws ModuleException {
		return LCCAncillaryServiceBL.getTaxApplicabilityForAncillaries(carrierCode, segmentCode, extChg, getUserPrincipal(),
				trackInfo);
	}

}
