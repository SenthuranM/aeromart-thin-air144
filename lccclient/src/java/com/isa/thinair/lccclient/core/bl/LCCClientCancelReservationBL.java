package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 * @since 3:57 PM 11/6/2009
 */
public class LCCClientCancelReservationBL {

	private static final Log log = LogFactory.getLog(LCCClientCancelReservationBL.class);

	private static void checkCancelReservationConstraints(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO)
			throws ModuleException {
		MODIFY_MODES modifyModes = lccClientResAlterQueryModesTO.getModifyModes();

		if (modifyModes != MODIFY_MODES.CANCEL_RES) {
			throw new ModuleException("lccclient.invalid.cancel.reservation.request");
		}

		String groupPNR = PlatformUtiltiies.nullHandler(lccClientResAlterQueryModesTO.getGroupPNR());

		if (groupPNR.length() == 0) {
			throw new ModuleException("lccclient.reservation.empty.group.pnr");
		}
	}

	public static LCCClientReservation execute(LCCClientResAlterModesTO lccClientResAlterModesTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkCancelReservationConstraints(lccClientResAlterModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterModesTO,
					LCCModificationTypeCode.MODTYPE_1_CANCEL_RESERVATION, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);
			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in cancel reservation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static ReservationBalanceTO query(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkCancelReservationConstraints(lccClientResAlterQueryModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_BALANCES_4_FOR_CANCEL_RESERVATION, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyResQuery(lccAirBookModifyRQ);
			return Transformer.transformBalances(lccAirBookRS);
		} catch (Exception ex) {
			log.error("Balance query for reservation alteration failed", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static LCCAirBookModifyRQ process(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			LCCModificationTypeCode lccModificationTypeCode, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		lccAirBookModifyRQ.setLccReservation(new LCCReservation());
		lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());

		lccAirBookModifyRQ.getLccReservation().setGroupPnr(lccClientResAlterQueryModesTO.getGroupPNR());
		lccAirBookModifyRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());
		lccAirBookModifyRQ.setModificationTypeCode(lccModificationTypeCode);
		lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(lccClientResAlterQueryModesTO.getVersion());

		// passing the charge override params to the lcc
		ChargerOverride chargerOverride = LCCReservationUtil.createChargeOverride(lccClientResAlterQueryModesTO);
		LCCClientSegmentAssembler lccSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();
		if (lccSegmentAssembler != null && lccSegmentAssembler.getPassengerExtChargeMap() != null
				&& !lccSegmentAssembler.getPassengerExtChargeMap().isEmpty()) {
			lccAirBookModifyRQ.getPerPaxExtChargeMap().addAll(LCCReservationUtil
					.composePassengerExternalChargeMap(lccSegmentAssembler.getPassengerExtChargeMap(), new Date()));
		}

		lccAirBookModifyRQ.setChargeOverride(chargerOverride);
		lccAirBookModifyRQ.setAutoRefundEnabled(AppSysParamsUtil.isAgentCreditRefundEnabled());
		lccAirBookModifyRQ.getAutoRefundEnabledAgentTypes().addAll(AppSysParamsUtil.getAgentTypesWhichAgentCreditRefundEnabled());
		lccAirBookModifyRQ.setUserNote(lccClientResAlterQueryModesTO.getUserNotes());

		return lccAirBookModifyRQ;
	}
	
	private static List<PerPaxExternalCharges> composePassengerExternalChargeMap(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
				externalCharge.setOperationMode(lccClientExternalChgDTO.getOperationMode());

				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}
}
