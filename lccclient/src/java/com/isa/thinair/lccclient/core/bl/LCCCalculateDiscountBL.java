package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCDiscountRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCDiscountRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * BL to execute calculate Discount for interline/dry reservations
 * 
 * @author M.Rikaz
 * 
 */

public class LCCCalculateDiscountBL {
	private static final Log log = LogFactory.getLog(LCCCalculateDiscountBL.class);

	public static ReservationDiscountDTO calculateDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal) throws ModuleException {

		LCCDiscountRQ lccDiscountRQ = new LCCDiscountRQ();
		lccDiscountRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(promotionRQ.getTransactionIdentifier()));
		lccDiscountRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccDiscountRQ.setValidateCriteria(promotionRQ.isValidateCriteria());
		lccDiscountRQ.setActionType(promotionRQ.getActionType().toString());
		lccDiscountRQ.setCalculateTotalOnly(promotionRQ.isCalculateTotalOnly());
		lccDiscountRQ.setDiscountedFareDetails(LCCReservationUtil.populateLccDiscountedFareDetails(promotionRQ
				.getDiscountInfoDTO()));

		if (promotionRQ.getPaxQtyList() != null && !promotionRQ.getPaxQtyList().isEmpty()) {
			for (PassengerTypeQuantityTO paxQuantityTO : promotionRQ.getPaxQtyList()) {
				PassengerTypeQuantity paxQuantity = new PassengerTypeQuantity();
				paxQuantity.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(paxQuantityTO.getPassengerType()));
				paxQuantity.setQuantity(paxQuantityTO.getQuantity());
				lccDiscountRQ.getPassengerTypeQuantity().add(paxQuantity);
			}
		}

		lccDiscountRQ.getPaxCharges().addAll(LCCReservationUtil.populateLccPaxCharges(promotionRQ.getPaxChargesList()));

		LCCDiscountRS lccDiscountRS = LCConnectorUtils.getMaxicoExposedWS().calculateDiscount(lccDiscountRQ);

		return Transformer.transformReservationDiscount(lccDiscountRS);
	}

}
