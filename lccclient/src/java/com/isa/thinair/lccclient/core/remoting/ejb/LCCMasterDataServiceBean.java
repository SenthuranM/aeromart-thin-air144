package com.isa.thinair.lccclient.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAgreementMasterDataRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAgreementMasterDataRS;
import com.isa.thinair.airmaster.api.dto.MasterDataCriteriaDTO;
import com.isa.thinair.airmaster.api.dto.MasterDataDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.core.bl.Transformer;
import com.isa.thinair.lccclient.core.service.bd.LCCMasterDataBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCMasterDataBDRemote;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

@Stateless
@RemoteBinding(jndiBinding = "MasterDataService.remote")
@LocalBinding(jndiBinding = "MasterDataService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCMasterDataServiceBean extends PlatformBaseSessionBean implements LCCMasterDataBDLocal,
		LCCMasterDataBDRemote {

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public MasterDataDTO getLccAgreementMasterData(MasterDataCriteriaDTO masterDataCriteriaDTO) throws ModuleException {

		LCCAgreementMasterDataRQ masterDataRQ =  Transformer.transform(masterDataCriteriaDTO, this.getUserPrincipal());
		LCCAgreementMasterDataRS lccAgreementMasterDataRS = LCConnectorUtils.getMaxicoExposedWS()
				.getLccAgreementMasterData(masterDataRQ);
		return Transformer.transform(lccAgreementMasterDataRS);
	}
}
