package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCCustomerNameUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCCustomerNameUpdateRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientCustomerNameUpdateBL {

	private static Log log = LogFactory.getLog(LCCClientMergeLoyaltyManagementBL.class);

	String memberFFID = null;
	String title = null;
	String firstName = null;
	String lastName = null;
	private BasicTrackInfo trackInfo;
	private UserPrincipal userPrincipal;

	public LCCClientCustomerNameUpdateBL(String memberFFID, BasicTrackInfo trackInfo, UserPrincipal userPrincipal, String title, String firstName, String lastName) {
		this.memberFFID = memberFFID;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.trackInfo = trackInfo;
		this.userPrincipal = userPrincipal;
	}

	public int customerNameUpdate() throws ModuleException {
		LCCCustomerNameUpdateRQ lccCustomerNameUpdateRQ = populateLCCCustomerNameUpdateRQ();
		LCCCustomerNameUpdateRS lccCustomerNameUpdateRS = null;
		try {
			lccCustomerNameUpdateRS = LCConnectorUtils.getMaxicoExposedWS().customerNameUpdate(
					lccCustomerNameUpdateRQ);
		} catch (ModuleException ex) {
			log.error("Error in removing FFID from PNRs", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
		return lccCustomerNameUpdateRS.getResult();
	}

	private LCCCustomerNameUpdateRQ populateLCCCustomerNameUpdateRQ() {
		LCCCustomerNameUpdateRQ lccCustomerNameUpdateRQ = new LCCCustomerNameUpdateRQ();
		lccCustomerNameUpdateRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccCustomerNameUpdateRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccCustomerNameUpdateRQ.getLccPos().setMarketingIPAddress(((TrackInfoDTO) trackInfo).getIpAddress());
		lccCustomerNameUpdateRQ.setMemberFFID(memberFFID);
		lccCustomerNameUpdateRQ.setTitle(title);
		lccCustomerNameUpdateRQ.setFirstName(firstName);
		lccCustomerNameUpdateRQ.setLastName(lastName);
		return lccCustomerNameUpdateRQ;
	}

}
