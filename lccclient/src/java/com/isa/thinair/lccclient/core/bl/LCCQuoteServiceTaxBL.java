package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseServiceTax;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseServiceTaxMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxTypeWiseServiceTaxesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxWiseExternalChargesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxWiseServiceTaxesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ServiceTax;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SimplifiedChargeDTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SimplifiedPaxDTO;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCServiceTaxQuoteForTktRevRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCServiceTaxQuoteForTktRevRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCQuoteServiceTaxBL {

	public Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal)
			throws ModuleException {
		LCCServiceTaxQuoteForTktRevRQ serviceTaxQuoteForTktRevRQ = new LCCServiceTaxQuoteForTktRevRQ();
		serviceTaxQuoteForTktRevRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
		serviceTaxQuoteForTktRevRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(serviceTaxQuoteCriteriaDTO
				.getLccTransactionIdentifier()));

		if (serviceTaxQuoteCriteriaDTO.getBaseAvailRQ() != null
				&& serviceTaxQuoteCriteriaDTO.getBaseAvailRQ().getTravelerInfoSummary() != null
				&& serviceTaxQuoteCriteriaDTO.getBaseAvailRQ().getTravelerInfoSummary().getPassengerTypeQuantityList() != null
				&& !serviceTaxQuoteCriteriaDTO.getBaseAvailRQ().getTravelerInfoSummary().getPassengerTypeQuantityList().isEmpty()) {
			for (PassengerTypeQuantityTO paxQuantityTO : serviceTaxQuoteCriteriaDTO.getBaseAvailRQ().getTravelerInfoSummary()
					.getPassengerTypeQuantityList()) {
				PassengerTypeQuantity paxQuantity = new PassengerTypeQuantity();
				paxQuantity.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(paxQuantityTO.getPassengerType()));
				paxQuantity.setQuantity(paxQuantityTO.getQuantity());
				serviceTaxQuoteForTktRevRQ.getPassengerTypeQuantity().add(paxQuantity);
			}
		}
		serviceTaxQuoteForTktRevRQ.setAgentCode(userPrincipal.getAgentCode());
		serviceTaxQuoteForTktRevRQ.setPaxState(serviceTaxQuoteCriteriaDTO.getPaxState());
		serviceTaxQuoteForTktRevRQ.setPaxCountryCode(serviceTaxQuoteCriteriaDTO.getPaxCountryCode());
		serviceTaxQuoteForTktRevRQ.setPaxTaxRegistered(serviceTaxQuoteCriteriaDTO.isPaxTaxRegistered());
		serviceTaxQuoteForTktRevRQ.setAvailPreferences(LCCClientAvailUtil.createAvailreferences(serviceTaxQuoteCriteriaDTO
				.getBaseAvailRQ()));
		serviceTaxQuoteForTktRevRQ.setCalculateOnlyForExternalCharges(serviceTaxQuoteCriteriaDTO.isCalculateOnlyForExternalCharges());
		
		if (serviceTaxQuoteCriteriaDTO.getPaxWiseExternalCharges().values() != null
				&& !serviceTaxQuoteCriteriaDTO.getPaxWiseExternalCharges().values().isEmpty()) {
			List<PaxWiseExternalChargesMap> paxWiseExternalCharges = new ArrayList<PaxWiseExternalChargesMap>();
			Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			for (Entry<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalChargeEntry : serviceTaxQuoteCriteriaDTO
					.getPaxWiseExternalCharges().entrySet()) {
				if (paxWiseExternalChargeEntry.getValue() != null && !paxWiseExternalChargeEntry.getValue().isEmpty()) {
					PaxWiseExternalChargesMap paxWiseExternalChargesMap = new PaxWiseExternalChargesMap();
					List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
					SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
					paxDTO.setPaxSequence(paxWiseExternalChargeEntry.getKey());
					if (serviceTaxQuoteCriteriaDTO.getPaxWisePaxTypes() != null
							&& serviceTaxQuoteCriteriaDTO.getPaxWisePaxTypes().get(paxWiseExternalChargeEntry.getKey()) != null) {
						paxDTO.setPaxType(serviceTaxQuoteCriteriaDTO.getPaxWisePaxTypes()
								.get(paxWiseExternalChargeEntry.getKey()));
					} else {
						paxDTO.setPaxType(PaxTypeTO.ADULT);
					}
					for (LCCClientExternalChgDTO externalChgDTO : paxWiseExternalChargeEntry.getValue()) {
						if (externalChgDTO.getAmount() != null && externalChgDTO.getAmount().doubleValue() > 0) {
							SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
							chargeDTO.setAmount(externalChgDTO.getAmount());
							chargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
							chargeDTO.setCarrierCode(externalChgDTO.getCarrierCode());
							chargeDTO.setChargeCode(externalChargesMap.get(externalChgDTO.getExternalCharges().toString()));
							chargeDTO.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
							chargeDTO.setSegmentCode(externalChgDTO.getSegmentCode());
							chargeDTO.setSegmentSequence(externalChgDTO.getSegmentSequence());
							chargesList.add(chargeDTO);
						}
					}
					if (!chargesList.isEmpty()) {
						paxWiseExternalChargesMap.setKey(paxDTO);
						paxWiseExternalChargesMap.getValue().addAll(chargesList);
						paxWiseExternalCharges.add(paxWiseExternalChargesMap);
					}
				}
			}
			serviceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges().addAll(paxWiseExternalCharges);
		}

		List<FlightSegment> simplifiedFlightSegmentDTOs = new ArrayList<FlightSegment>();
		for (FlightSegmentTO flightSegmentTO : serviceTaxQuoteCriteriaDTO.getFlightSegmentTOs()) {
			FlightSegment simplifiedFlightSegmentDTO = new FlightSegment();
			simplifiedFlightSegmentDTO.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
			simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
			String logicalCabinClassCode = flightSegmentTO.getLogicalCabinClassCode();
			if (StringUtil.isNullOrEmpty(logicalCabinClassCode) && !StringUtil.isNullOrEmpty(flightSegmentTO.getCabinClassCode())) {
				logicalCabinClassCode = flightSegmentTO.getCabinClassCode();
			}
			simplifiedFlightSegmentDTO.setLogicalCabinClassCode(logicalCabinClassCode);
			simplifiedFlightSegmentDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
			simplifiedFlightSegmentDTO.setOndSequence(flightSegmentTO.getOndSequence());
			simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
			simplifiedFlightSegmentDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());
			simplifiedFlightSegmentDTO.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
			simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getOperatingAirline());
			simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
		}
		serviceTaxQuoteForTktRevRQ.getFlightSegments().addAll(simplifiedFlightSegmentDTOs);

		LCCServiceTaxQuoteForTktRevRS lccServiceTaxQuoteForTktRevRS = LCConnectorUtils.getMaxicoExposedWS()
				.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);
		return adaptServiceTaxQuoteForTicketingRevenueRS(lccServiceTaxQuoteForTktRevRS);
	}

	private static Map<String, ServiceTaxQuoteForTicketingRevenueResTO> adaptServiceTaxQuoteForTicketingRevenueRS(
			LCCServiceTaxQuoteForTktRevRS serviceTaxRS) {
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> lccServiceTaxQuoteForTktRevRSMap = new HashMap<String, ServiceTaxQuoteForTicketingRevenueResTO>();
		for (CarrierWiseServiceTaxMap carrierWiseServiceTaxMap : serviceTaxRS.getCarrierWiseServiceTaxes()) {
			if (carrierWiseServiceTaxMap.getValue() != null && carrierWiseServiceTaxMap.getValue().get(0) != null) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxResTO = new ServiceTaxQuoteForTicketingRevenueResTO();
				CarrierWiseServiceTax carrierServiceTaxRS = carrierWiseServiceTaxMap.getValue().get(0);
				serviceTaxResTO.setServiceTaxDepositeCountryCode(carrierServiceTaxRS.getServiceTaxDepositeCountryCode());
				serviceTaxResTO.setServiceTaxDepositeStateCode(carrierServiceTaxRS.getServiceTaxDepositeStateCode());
				if (carrierServiceTaxRS.getPaxTypeWiseServiceTaxes() != null
						&& !carrierServiceTaxRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
					Map<String, List<ServiceTaxTO>> paxTypeWiseServiceTaxes = new HashMap<String, List<ServiceTaxTO>>();
					for (PaxTypeWiseServiceTaxesMap paxTypeWiseServiceTaxesMapEntry : carrierServiceTaxRS
							.getPaxTypeWiseServiceTaxes()) {
						paxTypeWiseServiceTaxes.put(paxTypeWiseServiceTaxesMapEntry.getKey(),
								adaptServiceTaxList(paxTypeWiseServiceTaxesMapEntry.getValue()));
					}
					serviceTaxResTO.setPaxTypeWiseServiceTaxes(paxTypeWiseServiceTaxes);
				}
				if (carrierServiceTaxRS.getPaxWiseServiceTaxes() != null
						&& !carrierServiceTaxRS.getPaxWiseServiceTaxes().isEmpty()) {
					Map<Integer, List<ServiceTaxTO>> paxWiseServiceTaxes = new HashMap<Integer, List<ServiceTaxTO>>();
					for (PaxWiseServiceTaxesMap paxWiseServiceTaxesMapEntry : carrierServiceTaxRS.getPaxWiseServiceTaxes()) {
						paxWiseServiceTaxes.put(paxWiseServiceTaxesMapEntry.getKey(),
								adaptServiceTaxList(paxWiseServiceTaxesMapEntry.getValue()));
					}
					serviceTaxResTO.setPaxWiseServiceTaxes(paxWiseServiceTaxes);
				}
				lccServiceTaxQuoteForTktRevRSMap.put(carrierWiseServiceTaxMap.getKey(), serviceTaxResTO);
			}
		}
		return lccServiceTaxQuoteForTktRevRSMap;
	}

	private static List<ServiceTaxTO> adaptServiceTaxList(List<ServiceTax> serviceTaxDTOs) {
		List<ServiceTaxTO> serviceTaxTOs = new ArrayList<ServiceTaxTO>();
		for (ServiceTax serviceTaxDTO : serviceTaxDTOs) {
			ServiceTaxTO serviceTaxTO = new ServiceTaxTO();
			serviceTaxTO.setAmount(serviceTaxDTO.getAmount());
			serviceTaxTO.setCarrierCode(serviceTaxDTO.getCarrierCode());
			serviceTaxTO.setChargeCode(serviceTaxDTO.getChargeCode());
			serviceTaxTO.setChargeGroupCode(serviceTaxDTO.getChargeGroupCode());
			serviceTaxTO.setChargeRateId(serviceTaxDTO.getChargeRateId());
			serviceTaxTO.setFlightRefNumber(serviceTaxDTO.getFlightRefNumber());
			serviceTaxTO.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
			serviceTaxTO.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
			serviceTaxTOs.add(serviceTaxTO);
		}
		return serviceTaxTOs;
	}
}
