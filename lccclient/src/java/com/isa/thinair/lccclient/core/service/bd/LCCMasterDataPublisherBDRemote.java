package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.lccclient.api.service.LCCMasterDataPublisherBD;

@Remote
public interface LCCMasterDataPublisherBDRemote extends LCCMasterDataPublisherBD {

}
