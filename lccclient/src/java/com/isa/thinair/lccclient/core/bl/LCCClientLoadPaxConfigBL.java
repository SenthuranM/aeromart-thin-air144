package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BasePaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IBEContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IBEPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.WSPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.XBEContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.XBEPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxContactConfigRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaxContactConfigRS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BaseContactConfigDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxValildationTO;
import com.isa.thinair.commons.api.dto.WSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

/**
 * 
 * @author rumesh
 * 
 */
public class LCCClientLoadPaxConfigBL {
	private static final Log log = LogFactory.getLog(LCCClientLoadPaxConfigBL.class);

	public static PaxContactConfigDTO loadPaxContactConfig(String appName, List<String> carrierList, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		try {
			LCCPaxContactConfigRQ lccContactConfigRQ = new LCCPaxContactConfigRQ();
			lccContactConfigRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccContactConfigRQ.setAppName(appName);
			if (carrierList != null) {
				lccContactConfigRQ.getCarrierList().addAll(carrierList);
			}
			LCCPaxContactConfigRS lccPaxContactConfigRS = LCConnectorUtils.getMaxicoExposedWS().loadPaxContactConfig(
					lccContactConfigRQ);

			if (lccPaxContactConfigRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccPaxContactConfigRS.getResponseAttributes()
						.getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			return pupolatePaxContactConfigObj(lccPaxContactConfigRS);
		} catch (Exception ex) {
			log.error("Load pax & contact config failed in LCCClient ", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static PaxContactConfigDTO pupolatePaxContactConfigObj(LCCPaxContactConfigRS lccPaxContactConfigRS) {
		PaxContactConfigDTO paxContactConfig = new PaxContactConfigDTO();
		paxContactConfig.setPaxConfigList(populatePaxConfigDTO(lccPaxContactConfigRS.getPaxConfig()));
		paxContactConfig.setContactConfigList(populateContactConfigDTO(lccPaxContactConfigRS.getContactConfig()));
		PaxValildationTO paxValidationTO = new PaxValildationTO();
		paxValidationTO.setAdultAgeCutOverYears(lccPaxContactConfigRS.getPaxCutOverAge().getAdultCutOverAge());
		paxValidationTO.setChildAgeCutOverYears(lccPaxContactConfigRS.getPaxCutOverAge().getChildCutOverAge());
		paxValidationTO.setInfantAgeCutOverYears(lccPaxContactConfigRS.getPaxCutOverAge().getInfantCutOverAge());
		paxContactConfig.setPaxValidation(paxValidationTO);
		return paxContactConfig;
	}

	private static List<PaxContactConfigDTO> populatePaxConfigDTO(List<BasePaxConfig> basePaxConfigList) {
		List paxConfigList = new ArrayList();

		for (BasePaxConfig basePaxConfig : basePaxConfigList) {
			if (basePaxConfig instanceof XBEPaxConfig) {
				XBEPaxConfig xbePaxConfig = (XBEPaxConfig) basePaxConfig;
				XBEPaxConfigDTO xbePaxDTO = new XBEPaxConfigDTO();
				xbePaxDTO.setPaxConfigId(xbePaxConfig.getPaxConfigId());
				xbePaxDTO.setFieldName(xbePaxConfig.getFieldName());
				xbePaxDTO.setPaxCatCode(xbePaxConfig.getPaxCatCode());
				xbePaxDTO.setPaxTypeCode(xbePaxConfig.getPaxTypeCode());
				xbePaxDTO.setSsrCode(xbePaxConfig.getSsrCode());
				xbePaxDTO.setUniqueness(xbePaxConfig.getUniqueness());
				xbePaxDTO.setUniquenessGroup(xbePaxConfig.getUniquenessGroup());
				xbePaxDTO.setXbeVisibility(xbePaxConfig.isXbeVisibility());
				xbePaxDTO.setXbeMandatory(xbePaxConfig.isXbeMandatory());
				xbePaxDTO.setAutoCheckinVisibility(xbePaxConfig.isAutoCheckinVisibility());
				xbePaxDTO.setAutoCheckinMandatory(xbePaxConfig.isAutoCheckinMandatory());
				paxConfigList.add(xbePaxDTO);
			} else if (basePaxConfig instanceof IBEPaxConfig) {
				IBEPaxConfig ibePaxConfig = (IBEPaxConfig) basePaxConfig;
				IBEPaxConfigDTO ibePaxDTO = new IBEPaxConfigDTO();
				ibePaxDTO.setPaxConfigId(ibePaxConfig.getPaxConfigId());
				ibePaxDTO.setFieldName(ibePaxConfig.getFieldName());
				ibePaxDTO.setPaxCatCode(ibePaxConfig.getPaxCatCode());
				ibePaxDTO.setPaxTypeCode(ibePaxConfig.getPaxTypeCode());
				ibePaxDTO.setSsrCode(ibePaxConfig.getSsrCode());
				ibePaxDTO.setUniqueness(ibePaxConfig.getUniqueness());
				ibePaxDTO.setUniquenessGroup(ibePaxConfig.getUniquenessGroup());
				ibePaxDTO.setIbeVisibility(ibePaxConfig.isIbeVisibility());
				ibePaxDTO.setIbeMandatory(ibePaxConfig.isIbeMandatory());
				ibePaxDTO.setAutoCheckinVisibility(ibePaxConfig.isAutoCheckinVisibility());
				ibePaxDTO.setAutoCheckinMandatory(ibePaxConfig.isAutoCheckinMandatory());
				paxConfigList.add(ibePaxDTO);
			} else if (basePaxConfig instanceof WSPaxConfig) {
				WSPaxConfig wsPaxConfig = (WSPaxConfig) basePaxConfig;
				WSPaxConfigDTO wsPaxDTO = new WSPaxConfigDTO();
				wsPaxDTO.setPaxConfigId(wsPaxConfig.getPaxConfigId());
				wsPaxDTO.setFieldName(wsPaxConfig.getFieldName());
				wsPaxDTO.setPaxCatCode(wsPaxConfig.getPaxCatCode());
				wsPaxDTO.setPaxTypeCode(wsPaxConfig.getPaxTypeCode());
				wsPaxDTO.setSsrCode(wsPaxConfig.getSsrCode());
				wsPaxDTO.setUniqueness(wsPaxConfig.getUniqueness());
				wsPaxDTO.setUniquenessGroup(wsPaxConfig.getUniquenessGroup());
				wsPaxDTO.setWsVisibility(wsPaxConfig.isWsVisibility());
				wsPaxDTO.setWsMandatory(wsPaxConfig.isWsMandatory());
				wsPaxConfig.setAutoCheckinVisibility(wsPaxConfig.isAutoCheckinVisibility());
				wsPaxConfig.setAutoCheckinMandatory(wsPaxConfig.isAutoCheckinMandatory());
				paxConfigList.add(wsPaxDTO);
			}
		}

		return paxConfigList;
	}

	/**
	 * Populate BaseContactConfigDTO from BaseContactConfig
	 * 
	 * @param baseConfigList
	 * @return
	 */
	private static List<BaseContactConfigDTO> populateContactConfigDTO(List<BaseContactConfig> baseConfigList) {
		List contactConfigList = new ArrayList();

		for (BaseContactConfig baseConfig : baseConfigList) {
			if (baseConfig instanceof XBEContactConfig) {
				XBEContactConfig xbeConfig = (XBEContactConfig) baseConfig;
				XBEContactConfigDTO xbeDTO = new XBEContactConfigDTO();
				xbeDTO.setFieldName(xbeConfig.getFieldName());
				xbeDTO.setGroupId(xbeConfig.getGroupId());
				xbeDTO.setXbeVisibility(xbeConfig.isXbeVisibility());
				xbeDTO.setMandatory(xbeConfig.isMandatory());
				xbeDTO.setValidationGroup(xbeConfig.getValidationGroup());
				contactConfigList.add(xbeDTO);
			} else {
				IBEContactConfig ibeConfig = (IBEContactConfig) baseConfig;
				IBEContactConfigDTO ibeDTO = new IBEContactConfigDTO();
				ibeDTO.setFieldName(ibeConfig.getFieldName());
				ibeDTO.setGroupId(ibeConfig.getGroupId());
				ibeDTO.setIbeVisibility(ibeConfig.isIbeVisibility());
				ibeDTO.setMandatory(ibeConfig.isMandatory());
				ibeDTO.setValidationGroup(ibeConfig.getValidationGroup());
				contactConfigList.add(ibeDTO);
			}
		}

		return contactConfigList;
	}
}