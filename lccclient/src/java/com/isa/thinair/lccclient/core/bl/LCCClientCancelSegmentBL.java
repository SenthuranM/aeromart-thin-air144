package com.isa.thinair.lccclient.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 * @since 3:57 PM 11/6/2009
 */
public class LCCClientCancelSegmentBL {

	private static final Log log = LogFactory.getLog(LCCClientCancelSegmentBL.class);

	private static void checkCancelSegmentConstraints(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO)
			throws ModuleException {
		MODIFY_MODES modifyModes = lccClientResAlterQueryModesTO.getModifyModes();

		if (modifyModes != MODIFY_MODES.CANCEL_OND) {
			throw new ModuleException("lccclient.invalid.cancel.segment.request");
		}

		String groupPNR = PlatformUtiltiies.nullHandler(lccClientResAlterQueryModesTO.getGroupPNR());

		if (groupPNR.length() == 0) {
			throw new ModuleException("lccclient.reservation.empty.group.pnr");
		}

		if (lccClientResAlterQueryModesTO.getOldPnrSegs() == null || lccClientResAlterQueryModesTO.getOldPnrSegs().size() == 0) {
			throw new ModuleException("lccclient.segment.references.empty");
		}
	}

	public static LCCClientReservation execute(LCCClientResAlterModesTO lccClientResAlterModesTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkCancelSegmentConstraints(lccClientResAlterModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterModesTO,
					LCCModificationTypeCode.MODTYPE_7_CANCEL_OND, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);
			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in cancel segment", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static ReservationBalanceTO query(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {

		try {
			checkCancelSegmentConstraints(lccClientResAlterQueryModesTO);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_BALANCES_3_FOR_CANCEL_OND, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyResQuery(lccAirBookModifyRQ);

			return Transformer.transformBalances(lccAirBookRS);
		} catch (Exception ex) {
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static LCCAirBookModifyRQ process(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			LCCModificationTypeCode lccModificationTypeCode, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		lccAirBookModifyRQ.setLccReservation(new LCCReservation());
		lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());

		lccAirBookModifyRQ.getLccReservation().setGroupPnr(lccClientResAlterQueryModesTO.getGroupPNR());

		// adding the reservation contact info
		ContactInfo transformedContactInfo = null;

		if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null) {
			transformedContactInfo = LCCClientApiUtils.transform(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
					.getContactInfo());
		}
		lccAirBookModifyRQ.getLccReservation().getAirReservation().setContactInfo(transformedContactInfo);

		lccAirBookModifyRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());
		lccAirBookModifyRQ.setModificationTypeCode(lccModificationTypeCode);
		lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));

		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOption = new OriginDestinationOption();

		for (LCCClientReservationSegment lccClientReservationSegment : lccClientResAlterQueryModesTO.getOldPnrSegs()) {
			originDestinationOption.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));
		}

		originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
		lccAirBookModifyRQ.getLccReservation().getAirReservation().setAirItinerary(originDestinationOptions);

		OriginDestinationOptions originDestinationOptionsAll = new OriginDestinationOptions();
		OriginDestinationOption originDestinationOptionAll = new OriginDestinationOption();
		if (lccClientResAlterQueryModesTO.getExistingAllSegs() != null) {
			for (LCCClientReservationSegment lccClientReservationSegment : lccClientResAlterQueryModesTO.getExistingAllSegs()) {
				originDestinationOptionAll.getFlightSegment().add(Transformer.transform(lccClientReservationSegment));
			}
		}
		originDestinationOptionsAll.getOriginDestinationOption().add(originDestinationOptionAll);
		lccAirBookModifyRQ.getLccReservation().getAirReservation().setExistingAirItinerary(originDestinationOptionsAll);

		lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(lccClientResAlterQueryModesTO.getVersion());

		lccAirBookModifyRQ.getLccReservation().getAirReservation()
				.setStatus(lccClientResAlterQueryModesTO.getReservationStatus());

		// passing the charge override params to the lcc
		ChargerOverride chargerOverride = LCCReservationUtil.createChargeOverride(lccClientResAlterQueryModesTO);
		lccAirBookModifyRQ.setChargeOverride(chargerOverride);

		lccAirBookModifyRQ.setAutoRefundEnabled(AppSysParamsUtil.isAgentCreditRefundEnabled());
		lccAirBookModifyRQ.getAutoRefundEnabledAgentTypes().addAll(AppSysParamsUtil.getAgentTypesWhichAgentCreditRefundEnabled());
		lccAirBookModifyRQ.setUserNote(lccClientResAlterQueryModesTO.getUserNotes());

		return lccAirBookModifyRQ;
	}
}
