package com.isa.thinair.lccclient.core.remoting.ejb;

import java.util.Collection;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.lccclient.core.bl.LCCClientAlertingBL;
import com.isa.thinair.lccclient.core.service.bd.LCCAlertingBDLocal;
import com.isa.thinair.lccclient.core.service.bd.LCCAlertingBDRemote;

@Stateless
@RemoteBinding(jndiBinding = "LCCAlertingService.remote")
@LocalBinding(jndiBinding = "LCCAlertingService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LCCAlertingServiceBean extends PlatformBaseSessionBean implements LCCAlertingBDLocal, LCCAlertingBDRemote {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, BasicTrackInfo trackInfo) throws ModuleException {
		return LCCClientAlertingBL.retrieveAlertsForSearchCriteria(searchCriteria, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearAlerts(String airline, Collection<Integer> alertIDs, BasicTrackInfo trackInfo) throws ModuleException {
		LCCClientAlertingBL.clearAlerts(airline, alertIDs, this.getUserPrincipal(), trackInfo);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addAlerts(List<AlertDetailDTO> alertDTOList, BasicTrackInfo trackInfo, String carrierCode) throws ModuleException {
		LCCClientAlertingBL.addAlerts(alertDTOList, this.getUserPrincipal(), trackInfo, carrierCode);
	}

}
