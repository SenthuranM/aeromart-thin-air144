package com.isa.thinair.lccclient.core.bl;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReadRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSearchReservationRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSearchReservationRS;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientSearchReservationBL {

	private static final Log log = LogFactory.getLog(LCCClientSearchReservationBL.class);

	public static Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {
		try {
			// Transform to Request object
			LCCSearchReservationRQ lccSearchReservationRQ = Transformer.transform(reservationSearchDTO, userPrincipal, trackInfo);

			// Call search reservation web service method
			LCCSearchReservationRS lccSearchReservationRS = LCConnectorUtils.getMaxicoExposedWS().searchReservations(
					lccSearchReservationRQ);

			return Transformer.transform(lccSearchReservationRS);
		} catch (Exception ex) {
			log.error("Error in search reservations", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static LCCClientReservation searchReservationByPNR(LCCClientPnrModesDTO lcclientPnrModesDTO,
			UserPrincipal userPrincipal, ModificationParamRQInfo paramRQInfo, TrackInfoDTO trackInfo) throws ModuleException {
		try {
			// Transform to Request object
			LCCReadRQ lccReadRQ = Transformer.transform(lcclientPnrModesDTO, userPrincipal, paramRQInfo, trackInfo);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().searchReservationByPNR(lccReadRQ);

			return Transformer.transform(lccAirBookRS, lcclientPnrModesDTO, null);
		} catch (Exception ex) {
			log.error("Error in search reservation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}
}