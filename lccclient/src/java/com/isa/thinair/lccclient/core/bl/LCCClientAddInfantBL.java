package com.isa.thinair.lccclient.core.bl;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCClientAddInfantBL {
	private static final Log log = LogFactory.getLog(LCCClientAddInfantBL.class);

	public static ReservationBalanceTO query(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		try {
			lccClientResAlterQueryModesTO.setModifyModes(MODIFY_MODES.ADD_INF);

			LCCAirBookModifyRQ lccAirBookModifyRQ = process(lccClientResAlterQueryModesTO,
					LCCModificationTypeCode.MODTYPE_13_ADD_INFANT, userPrincipal, trackInfo);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyResQuery(lccAirBookModifyRQ);

			return Transformer.transformBalances(lccAirBookRS);

		} catch (Exception e) {
			log.error("Error in add infant ", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
	}

	private static LCCAirBookModifyRQ process(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			LCCModificationTypeCode lccModificationTypeCode, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
		lccAirBookModifyRQ.setLccReservation(new LCCReservation());
		lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());

		lccAirBookModifyRQ.getLccReservation().setGroupPnr(lccClientResAlterQueryModesTO.getGroupPNR());
		lccAirBookModifyRQ.setLoadDataOptions(LCCClientCommonUtils.getDefaultLoadOptions());
		lccAirBookModifyRQ.setModificationTypeCode(lccModificationTypeCode);
		lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(lccClientResAlterQueryModesTO.getVersion());
		
		LCCClientSegmentAssembler lccSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();
		if (lccSegmentAssembler != null && lccSegmentAssembler.getPassengerExtChargeMap() != null
				&& !lccSegmentAssembler.getPassengerExtChargeMap().isEmpty()) {
			lccAirBookModifyRQ.getPerPaxExtChargeMap().addAll(LCCReservationUtil
					.composePassengerExternalChargeMap(lccSegmentAssembler.getPassengerExtChargeMap(), new Date()));
		}

		// passing the charge override params to the lcc
		ChargerOverride chargerOverride = LCCReservationUtil.createChargeOverride(lccClientResAlterQueryModesTO);

		lccAirBookModifyRQ.setChargeOverride(chargerOverride);

		lccAirBookModifyRQ.getAdPaxList().addAll(lccClientResAlterQueryModesTO.getPaxAdultsList());

		return lccAirBookModifyRQ;
	}
}
