package com.isa.thinair.lccclient.core.bl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentETicketInfo;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCETicketInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCETicketInfoRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCETicketInfoBL {
	public static Map<Pair<String, Integer>, Set<ETicketInfoTO>> getETicketInformation(List<String> lccUniqueIDs, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoMap = new HashMap<Pair<String, Integer>, Set<ETicketInfoTO>>();

		if (lccUniqueIDs != null && lccUniqueIDs.size() > 0) {
			LCCETicketInfoRQ lcceTicketInfoRQ = new LCCETicketInfoRQ();
			lcceTicketInfoRQ.getLccUniqueIDs().addAll(lccUniqueIDs);

			lcceTicketInfoRQ.setPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

			LCCETicketInfoRS lccETicketInfoRS = LCConnectorUtils.getMaxicoExposedWS().getETicketInformation(lcceTicketInfoRQ);

			if (lccETicketInfoRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccETicketInfoRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			List<SegmentETicketInfo> eTicketInfo = lccETicketInfoRS.getEticketInfo();
			for (SegmentETicketInfo info : eTicketInfo) {
				String lccUniqueID = info.getLccUniqueID();
				int paxSequence = info.getPaxSequence();

				Pair<String, Integer> pair = Pair.of(lccUniqueID, paxSequence);

				ETicketInfoTO eTicketInfoTO = new ETicketInfoTO();

				eTicketInfoTO.setDepDateZulu(info.getDepartureDateZulu());
				eTicketInfoTO.seteTicketNo(info.getETicketNumber());
				eTicketInfoTO.setFlightNumber(info.getFlightNumber());
				eTicketInfoTO.setSegmentCode(info.getSegmentCode());

				if (eTicketInfoMap.get(pair) == null) {
					eTicketInfoMap.put(pair, new HashSet<ETicketInfoTO>());
				}
				eTicketInfoMap.get(pair).add(eTicketInfoTO);

			}
		}
		
		return eTicketInfoMap;

	}
}
