package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeReverseInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingMethod;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResAuditRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResAuditReadRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCReservation;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelfReprotectFlightsRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelfReprotectFlightsRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCUserNoteRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCUserNoteRS;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.LCCUtils;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Zaki Saimeh
 */
public class LCCClientModifyReservationBL {

	private static Log log = LogFactory.getLog(LCCClientModifyReservationBL.class);

	public static LCCClientReservation adjustCharge(String groupPNr, String version,
			List<LCCClientChargeAdustment> chargeAdjustments, UserPrincipal userPrincipal, BasicTrackInfo trackInfo,
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS, Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax)
			throws ModuleException {
		try {
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_10_ADJUST_CHARGE);
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());

			if (handlingFeeByPax != null && !handlingFeeByPax.isEmpty()) {
				lccAirBookModifyRQ.getPerPaxExtChargeMap()
						.addAll(composePassengerExternalChargeMap(handlingFeeByPax, new Date()));
			}
			
			AirReservation airReservation = new AirReservation();
			airReservation.getChargeAdjustments().addAll(Transformer.transformAdjustmentList(chargeAdjustments));

			lccAirBookModifyRQ.getLccReservation().setAirReservation(airReservation);
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(groupPNr);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(version);
			if(serviceTaxRS != null){
				lccAirBookModifyRQ.setCarrierWiseServiceTax(Transformer.transformServiceTaxRS(serviceTaxRS));
			}
			
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);
			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in adjustCharge", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		LCCClientReservation clientReservation = new LCCClientReservation();
		try {
			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();

			LCCReservation lccReservation = new LCCReservation();
			{
				AirReservation airReservation = new AirReservation();
				{
					Set<LCCClientReservationPax> passengers = lccClientReservation.getPassengers();

					TravelerInfo travelerInfo = new TravelerInfo();
					List<AirTraveler> airTravelers = travelerInfo.getAirTraveler();

					for (LCCClientReservationPax lccClientReservationPax : passengers) {

						AirTraveler airTraveler = new AirTraveler();
						{
							PersonName personName = new PersonName();
							{
								personName.setFirstName(lccClientReservationPax.getFirstName());
								personName.setSurName(lccClientReservationPax.getLastName());
								personName.setTitle(lccClientReservationPax.getTitle());
							}
							airTraveler.setPersonName(personName);
							airTraveler.setTravelerSequence(lccClientReservationPax.getPaxSequence());
							airTraveler.setTravelerRefNumber(lccClientReservationPax.getTravelerRefNumber());

						}

						airTravelers.add(airTraveler);
					}
					airReservation.setTravelerInfo(travelerInfo);
					airReservation.setTicketing(LCCClientCommonUtils.populateTicketingInfo(lccClientReservation.getStatus(),
							lccClientReservation.getTotalAvailableBalance()));
					airReservation.setVersion(String.valueOf(lccClientReservation.getVersion()));
				}

				lccReservation.setAirReservation(airReservation);
				lccReservation.setGroupPnr(lccClientReservation.getPNR());
			}

			lccAirBookModifyRQ.setLccReservation(lccReservation);
			lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_8_SPLIT_PASSENGER);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			// populate LCCClientReservation from lccAirBookRS.
			clientReservation.setPNR(lccAirBookRS.getLccReservation().getGroupPnr());

		} catch (Exception ex) {
			log.error("Error in splitReservation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}

		return clientReservation;
	}

	public static void modifyContactInfo(String pnr, String version,
			CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, UserPrincipal userPrincipal, String appIndicator,
			TrackInfoDTO trackInfo) throws ModuleException {

		try {
			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();

			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_2_MODIFY_RESERVATION_CONTACT_INFO);
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());
			lccAirBookModifyRQ.setOldLCCReservation(new LCCReservation());

			AirReservation airReservation = new AirReservation();
			airReservation.setVersion(version);
			lccAirBookModifyRQ.getLccReservation().setAirReservation(airReservation);
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(pnr);
			lccAirBookModifyRQ.getLccReservation().getAirReservation()
					.setContactInfo(LCCClientApiUtils.transform(lccClientReservationContactInfo));

			AirReservation oldAirReservation = new AirReservation();
			airReservation.setVersion(version);
			lccAirBookModifyRQ.getOldLCCReservation().setAirReservation(oldAirReservation);
			lccAirBookModifyRQ.getOldLCCReservation().setGroupPnr(pnr);
			lccAirBookModifyRQ.getOldLCCReservation().getAirReservation()
					.setContactInfo(LCCClientApiUtils.transform(oldLccClientReservationContactInfo));

			lccAirBookModifyRQ.setAppIndicator(appIndicator);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
		} catch (Exception ex) {
			log.error("Error in modifyContactInfo", ex);
			// TODO : Since there is no rollback implemented we must save info in GroupHistory

			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static void
			modifyPassengerInfo(LCCClientReservation lccClientReservation, LCCClientReservation oldLccClientReservation,
			                    Collection<String> allowedOperations, UserPrincipal userPrincipal, String appIndicator, TrackInfoDTO trackInfo) throws ModuleException {
		try {
			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();

			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_3_MODIFY_RESERVATION_PAX_INFO);
			LCCReservation lccReservation = new LCCReservation();
			AirReservation airReservation = new AirReservation();
			airReservation.setVersion(lccClientReservation.getVersion());
			airReservation.setTravelerInfo(Transformer.transform(lccClientReservation.getPassengers()));
			airReservation.setLastUserNote(Transformer.transform(lccClientReservation.getLastUserNote(), null));
			airReservation.setOriginCountryOfCall(lccClientReservation.getOriginCountryOfCall());
			lccReservation.setAirReservation(airReservation);
			lccReservation.setGroupPnr(lccClientReservation.getPNR());
			lccAirBookModifyRQ.setLccReservation(lccReservation);
			lccAirBookModifyRQ.setAppIndicator(appIndicator);

			// Create old reservation
			LCCReservation oldLccReservation = new LCCReservation();
			AirReservation oldAirReservation = new AirReservation();
			oldAirReservation.setVersion(oldLccClientReservation.getVersion());
			oldAirReservation.setTravelerInfo(Transformer.transform(oldLccClientReservation.getPassengers()));
			oldAirReservation.setLastUserNote(Transformer.transform(oldLccClientReservation.getLastUserNote(), null));
			oldLccReservation.setAirReservation(oldAirReservation);
			lccAirBookModifyRQ.setOldLCCReservation(oldLccReservation);

			if (allowedOperations != null) {
				lccAirBookModifyRQ.getAllowedOperations().addAll(allowedOperations);
			}
			lccAirBookModifyRQ.setReasonForAllowBlPax(lccClientReservation.getReasonForAllowBlPax());

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
		} catch (Exception ex) {
			log.error("Error in modifyPassengerInfo", ex);
			// TODO : Since there is no rollback implemented we must save info in GroupHistory

			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static LCCClientReservation balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version,
			UserPrincipal userPrincipal, SessionContext sessionContext, BasicTrackInfo trackInfo, boolean isInfantPaymentSeparated) throws ModuleException {

		String groupPNR = lccClientBalancePayment.getGroupPNR();
		PaymentAssembler paymentAssembler = new PaymentAssembler();
		TrackInfoDTO trackInfoDTO = LCCUtils.getTrackInfoDto(userPrincipal);
		CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
		Collection<Integer> tempTnxIds = new ArrayList<Integer>();
		boolean isFirstPayment = false;
		
		ReservationContactInfo reservationContactInfo = AirProxyReservationUtil.transformContactInfo(lccClientBalancePayment
				.getContactInfo());
		// If payment failed in the capture payment itself, we have to skip the reversal in again.
		// following boolean will be used for that. Until we capture the payment successfully and return back we don't
		// need to perform reversal
		boolean performReversal = false;

		try {
			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers = lccClientBalancePayment
					.getPaxSeqWisePayAssemblers();
			LCCClientPaymentWorkFlow.setLccUniqueTnxId(paxSeqWisePayAssemblers.values());
			for (LCCClientPaymentAssembler lccClientPaymentAssembler : paxSeqWisePayAssemblers.values()) {
				LCCClientPaymentWorkFlow.transform(paymentAssembler, lccClientPaymentAssembler);
			}

			boolean bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());

			if (paymentAssembler.getPayments().size() > 0) {

				ReservationBO.getPaymentInfo(paymentAssembler, groupPNR);

				if (lccClientBalancePayment.getTemporyPaymentMap() != null
						&& lccClientBalancePayment.getTemporyPaymentMap().size() > 0) {
					// This is initiated from IBE
					tempTnxIds = new ArrayList<Integer>(lccClientBalancePayment.getTemporyPaymentMap().keySet());
				} else {
					// Make the temporary payment entry
					tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
							new HashMap<Integer, CardPaymentInfo>(), reservationContactInfo, paymentAssembler.getPayments(),
							credentialsDTO, true, false);
				}

				ServiceResponce response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler,
						groupPNR, reservationContactInfo, tempTnxIds);
				performReversal = true;

				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);

				LCCReservationUtil.combineActualCreditPayments(paxSeqWisePayAssemblers, paxCreditPayments, paymentAuthIdMap,
						paymentBrokerRefMap);
			}

			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

			// MOD TYPE
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_4_BALANCE_PAYMENTS);

			// LCC Reservation
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(groupPNR);
			lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(version);
			lccAirBookModifyRQ.getLccReservation().getAirReservation()
					.setContactInfo(LCCClientApiUtils.transform(lccClientBalancePayment.getContactInfo()));
			AirReservationSummary airs = new AirReservationSummary();
			airs.setAdminInfo(new AdminInfo());
			airs.getAdminInfo().setOwnerAgentCode(lccClientBalancePayment.getOwnerAgent());
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setAirReservationSummary(airs);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setTravelerInfo(new TravelerInfo());

			// add passenger e ticket details to the modify request
			if (lccClientBalancePayment.getReservationStatus() != null
					&& lccClientBalancePayment.getReservationStatus().equals(ReservationStatus.ON_HOLD)) {
				lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.CREATE);
				isFirstPayment = true;
			} else {
				lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.EXCHANGE);
			}
			List<LCCClientReservationSegment> allLccSegments = new ArrayList<LCCClientReservationSegment>(
					lccClientBalancePayment.getPnrSegments());
			// get unflown segments
			List<LCCClientReservationSegment> unflownLccSegments = ETicketUtil.getUnflownSegments(allLccSegments);

			CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto((TrackInfoDTO) trackInfo);
			ticketInfoDto.setBSPPayment(bspPayment);
			List<LCCClientReservationPax> etGenerationEligiblePax = ETicketUtil.getETGenerationEligiblePax(
					lccClientBalancePayment.getPassengers(), paxSeqWisePayAssemblers, isFirstPayment, isInfantPaymentSeparated);

			// Generate E tickets
			Map<Integer, Map<Integer, EticketDTO>> paxEticketMap = ETicketUtil.generateIATAETicketNumbers(
					etGenerationEligiblePax, unflownLccSegments, ticketInfoDto);
			
			Iterator<LCCClientReservationPax> passngerIterator = lccClientBalancePayment.getPassengers().iterator();
			while (passngerIterator.hasNext()) {
				LCCClientReservationPax lcclientReservationPax = passngerIterator.next();
				AirTraveler airTraveler = Transformer.transform(lcclientReservationPax, lccAirBookModifyRQ
						.getLccReservation().getAirReservation().getTravelerInfo());
				if (paxEticketMap.keySet().contains(lcclientReservationPax.getPaxSequence())) {
					airTraveler.getETickets().addAll(
							ETicketUtil.createPaxEticket(allLccSegments, lcclientReservationPax.getPaxSequence(),
									paxEticketMap.get(lcclientReservationPax.getPaxSequence())));
				}
			}

			// Setting the external charges
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setPriceInfo(parsePriceInfo(lccClientBalancePayment));

			// Set Fulfillment
			composeFulFillments(lccAirBookModifyRQ, paxSeqWisePayAssemblers, userPrincipal, groupPNR, isFirstPayment);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			return Transformer.transform(lccAirBookRS, tempTnxIds);
		} catch (Exception ex) {
			log.error("Error in balancePayment", ex);
			if (performReversal) {
				sessionContext.setRollbackOnly();
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE,
						"Payment failed for modify reservation", null, null, null);
				// Need only credit card type payments for refund. Other modes will
				// be reverted automatically
				// We dont need to worry about other carrier pax credit because it
				// is handled in LCC and reversal should
				// done in LCC itself.
				LCCClientPaymentWorkFlow.extractCreditCardPayments(paymentAssembler);

				if (!paymentAssembler.getPayments().isEmpty()) {
					LCCClientModuleUtil.getReservationBD().refundPayment(trackInfoDTO, paymentAssembler, groupPNR,
							reservationContactInfo, tempTnxIds);
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(tempTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS,
							"Undo success for modify reservation", null, null, null);
				}
			}
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static PriceInfo parsePriceInfo(LCCClientBalancePayment lccClientBalancePayment) {
		PriceInfo priceInfo = new PriceInfo();
		Date applicableDate = new Date();

		for (Entry<Integer, LCCClientPaymentAssembler> entry : lccClientBalancePayment.getPaxSeqWisePayAssemblers().entrySet()) {
			Integer paxSeq = entry.getKey();
			LCCClientPaymentAssembler lccClientPaymentAssembler = entry.getValue();

			if (lccClientPaymentAssembler.getPerPaxExternalCharges().size() > 0) {
				Collection<ExternalCharge> colExternalCharge = new ArrayList<ExternalCharge>();
				BigDecimal totalExternalChgAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientExternalChgDTO lccClientExternalChgDTO : lccClientPaymentAssembler.getPerPaxExternalCharges()) {

					ExternalCharge externalCharge = new ExternalCharge();
					externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
					externalCharge.setApplicableDateTime(applicableDate);
					externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO
							.getExternalCharges()));

					if (lccClientExternalChgDTO.getCarrierCode() == null && lccClientExternalChgDTO.getExternalCharges() != EXTERNAL_CHARGES.JN_OTHER) {
						externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					} else {
						externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
					}
					externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
					externalCharge.setCode(lccClientExternalChgDTO.getCode());
					externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
					externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
						Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
								.getFlexiBilities();
						Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
						for (FlexiInfoTO flexiInfoTO : flexiBilities) {
							FlexiInfo flexiInfo = new FlexiInfo();
							flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
							flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
							flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
							flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
							flexiInfos.add(flexiInfo);
						}
						externalCharge.getAdditionalDetails().addAll(flexiInfos);
					} else if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_OTHER) {
						externalCharge.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(lccClientExternalChgDTO
								.getFlightRefNumber()));
					} else if(lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX){
						externalCharge.setTicketingRevenue(lccClientExternalChgDTO.isTicketingRevenue());
						externalCharge.setTaxableAmount(lccClientExternalChgDTO.getTaxableAmount());
						externalCharge.setNonTaxableAmount(lccClientExternalChgDTO.getNonTaxableAmount());
						externalCharge.setTaxDepositCountryCode(lccClientExternalChgDTO.getTaxDepositCountryCode());
						externalCharge.setTaxDepositStateCode(lccClientExternalChgDTO.getTaxDepositStateCode());
					} else if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.HANDLING_CHARGE) {
						externalCharge.setOperationMode(lccClientExternalChgDTO.getOperationMode());
					}

					colExternalCharge.add(externalCharge);
					totalExternalChgAmount = AccelAeroCalculator.add(totalExternalChgAmount, externalCharge.getAmount());
				}

				PerPaxPriceInfo perPaxPriceInfo = new PerPaxPriceInfo();
				perPaxPriceInfo.setPassengerType(PassengerType.ADT); // FIXME it's hard coded make it configurable
				perPaxPriceInfo.setTravelerRefNumber(paxSeq + ""); // FIXME
				perPaxPriceInfo.setPassengerPrice(new FareType());

				perPaxPriceInfo.getPassengerPrice().getExternalCharge().addAll(colExternalCharge);
				perPaxPriceInfo.getPassengerPrice().setTotalExternalCharge(totalExternalChgAmount);

				priceInfo.getPerPaxPriceInfo().add(perPaxPriceInfo);
			}
		}

		return priceInfo;
	}

	public static LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {
		try {
			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());
			lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setPriceInfo(new PriceInfo());

			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_9_EXTEND_ON_HOLD);
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(groupPNR);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(version);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().getPriceInfo()
					.setOnholdReleaseTimeZulu(extendDateTimeZulu);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in extendOnHold", ex);
			// TODO : Since there is no rollback implemented we must save info in GroupHistory

			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	public static LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException {
		try {
			AdminInfo adminInfo = new AdminInfo();
			adminInfo.setOwnerAgentCode(transfereeAgent);

			// Transform to Request object
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());
			lccAirBookModifyRQ.getLccReservation().setAirReservation(new AirReservation());
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setAirReservationSummary(new AirReservationSummary());

			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_11_TRANSFER_OWNERSHIP);
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(groupPNR);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(version);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().getAirReservationSummary().setAdminInfo(adminInfo);

			// Call web service method
			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in transferOwnership", ex);
			// TODO : Since there is no rollback implemented we must save info in GroupHistory

			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * Loads reservation history for a given pnr and carrierCode, if the carrier code is not given, then loads the
	 * interline reservation history.
	 * 
	 * @param pnr
	 * @param carrierCode
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static List<UserNoteTO> searchReservationHistory(String pnr, String carrierCode, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		try {
			LCCResAuditReadRQ lccResAuditReadRQ = new LCCResAuditReadRQ();
			List<UserNoteTO> reservationHistoryList = new ArrayList<UserNoteTO>();
			lccResAuditReadRQ.setGroupPnr(pnr);
			lccResAuditReadRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			if (carrierCode == null) { // Load interline reservation history.
				lccResAuditReadRQ.setLoadLccHistory(true);
			} else { // Load history for selected carrier.
				lccResAuditReadRQ.getLoadHistoryForCarrier().add(carrierCode);
			}
			LCCResAuditRS lccResAuditRS = LCConnectorUtils.getMaxicoExposedWS().getReservationAudit(lccResAuditReadRQ);

			if (lccResAuditRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = BeanUtils.getFirstElement(lccResAuditRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			if (lccResAuditRS != null && lccResAuditRS.getReservationHistory() != null) {
				UserNoteTO userNoteTO = null;
				for (UserNote userNote : lccResAuditRS.getReservationHistory()) {
					userNoteTO = new UserNoteTO();
					userNoteTO.setAction(userNote.getAction());
					userNoteTO.setModifiedDate(userNote.getTimestamp());
					userNoteTO.setSystemNote(userNote.getSystemText());
					userNoteTO.setUserName(userNote.getUsername());
					userNoteTO.setUserText(userNote.getNoteText());
					userNoteTO.setCarrierCode(carrierCode);
					reservationHistoryList.add(userNoteTO);
				}
			}
			return reservationHistoryList;
		} catch (Exception ex) {
			log.error("Error in searchReservationHistory", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * Loads user notes history for a given pnr and carrierCode
	 * 
	 * @param pnr
	 * @param carrierCode
	 * @param trackInfo
	 *            TODO
	 * @param isClassifyUN
	 * @return
	 * @throws ModuleException
	 */
	public static List<UserNoteTO> searchUserNotes(String pnr, String carrierCode, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo, boolean isClassifyUN) throws ModuleException {
		try {
			LCCResAuditReadRQ lccResAuditReadRQ = new LCCResAuditReadRQ();
			LCCResAuditRS lccResAuditRS = null;
			List<UserNoteTO> userNotesHistoryList = new ArrayList<UserNoteTO>();
			lccResAuditReadRQ.setGroupPnr(pnr);
			lccResAuditReadRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccResAuditReadRQ.setLoadUserNotes(true);
			lccResAuditReadRQ.setClassifyUN(isClassifyUN);
			lccResAuditRS = LCConnectorUtils.getMaxicoExposedWS().getReservationAudit(lccResAuditReadRQ);

			if (lccResAuditRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = BeanUtils.getFirstElement(lccResAuditRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}

			if (lccResAuditRS != null && lccResAuditRS.getReservationNotes() != null) {
				UserNoteTO userNoteTO = null;
				for (UserNote userNote : lccResAuditRS.getReservationNotes()) {
					userNoteTO = new UserNoteTO();
					userNoteTO.setAction(userNote.getAction());
					userNoteTO.setModifiedDate(userNote.getTimestamp());
					userNoteTO.setSystemNote(userNote.getSystemText());
					userNoteTO.setUserName(userNote.getUsername());
					userNoteTO.setUserText(userNote.getNoteText());
					StringBuilder carrierCodes = new StringBuilder();
					for (String carrier : userNote.getCarrierCode()) {
						carrierCodes.append(carrier);
						carrierCodes.append(",");
					}
					if (carrierCodes.lastIndexOf(",") > 0) {
						userNoteTO.setCarrierCode(carrierCodes.substring(0, carrierCodes.lastIndexOf(",")));
					} else {
						userNoteTO.setCarrierCode(carrierCodes.toString());
					}
					userNotesHistoryList.add(userNoteTO);
				}
			}
			return userNotesHistoryList;
		} catch (Exception ex) {
			log.error("Error in searchUserNotes", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	/**
	 * Compose LCC request and call the web service to invoke add Infant operation
	 * 
	 * @param lccClientReservation
	 * @param userPrincipal
	 * @param autoCancellationEnabled
	 * @param payments
	 * @return LCCClientReservation
	 * @throws ModuleException
	 */
	public static LCCClientReservation addInfant(LCCClientReservation lccClientReservation, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO, Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers,
			boolean autoCancellationEnabled, boolean mcETGenerationEligible) throws ModuleException {
		try {
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			PaymentAssembler paymentAssembler = new PaymentAssembler();
			Collection<Integer> tempTnxIds = new ArrayList<Integer>();

			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_13_ADD_INFANT);

			LCCReservation lccReservation = new LCCReservation();
			AirReservation airReservation = new AirReservation();
			TravelerInfo travelerInfo = Transformer.transform(lccClientReservation.getPassengers());
			airReservation.setTravelerInfo(travelerInfo);
			airReservation.setVersion(lccClientReservation.getVersion());
			CommonReservationContactInfo contactInfo = lccClientReservation.getContactInfo();
			ReservationContactInfo transformContactInfo = AirProxyReservationUtil.transformContactInfo(contactInfo);
			airReservation.setContactInfo(LCCClientApiUtils.transform(contactInfo));
			lccReservation.setAirReservation(airReservation);
			lccReservation.setGroupPnr(lccClientReservation.getPNR());
			lccAirBookModifyRQ.setLccReservation(lccReservation);
			lccAirBookModifyRQ.getLccReservation().setAirReservation(airReservation);
			lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccAirBookModifyRQ.setAutoCancellationEnabled(autoCancellationEnabled);
			CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);

			LCCClientPaymentWorkFlow.setLccUniqueTnxId(paxSeqWisePayAssemblers.values());
			
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = extractHandlingFeeExternalCharges(paxSeqWisePayAssemblers);			
			
			for (LCCClientPaymentAssembler lccClientPaymentAssembler : paxSeqWisePayAssemblers.values()) {				
				LCCClientPaymentWorkFlow.transform(paymentAssembler, lccClientPaymentAssembler);
			}
			// Setting the external charges
			LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
			lccClientBalancePayment.setPaxSeqWisePayAssemblers(paxSeqWisePayAssemblers);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setPriceInfo(parsePriceInfo(lccClientBalancePayment));
			boolean bspPayment = LCCReservationUtil.isBSPpayment(paymentAssembler.getPayments());

			if (paymentAssembler.getPayments().size() > 0) {
				ReservationBO.getPaymentInfo(paymentAssembler, lccClientReservation.getPNR());

				/*
				 * Assign null because Add Infant not available in IBE
				 */
				Map<Integer, CardPaymentInfo> tempPaymentMap = null;

				// Make the temporary payment entry
				tempTnxIds = (Collection<Integer>) ReservationModuleUtils.getReservationBD().makeTemporyPaymentEntry(
						tempPaymentMap, transformContactInfo, paymentAssembler.getPayments(), credentialsDTO, true, false);

				ServiceResponce response = LCCClientModuleUtil.getReservationBD().makePayment(trackInfoDTO, paymentAssembler,
						lccClientReservation.getPNR(), transformContactInfo, tempTnxIds);

				Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
						.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
				Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
						.getResponseParam(CommandParamNames.PAYMENT_INFO);
				Map<String, String> paymentAuthIdMap = (Map<String, String>) response
						.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
				Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) response
						.getResponseParam(CommandParamNames.PAYMENT_BROKER_REF_MAP);

				// Since we return form other module we need updated list.
				paymentAssembler.getPayments().clear();
				paymentAssembler.getPayments().addAll(paymentInfos);

				LCCReservationUtil.combineActualCreditPayments(paxSeqWisePayAssemblers, paxCreditPayments, paymentAuthIdMap,
						paymentBrokerRefMap);
			}
			
			if (paxExtChgMap != null && !paxExtChgMap.isEmpty()) {
				lccAirBookModifyRQ.getPerPaxExtChargeMap()
						.addAll(LCCReservationUtil.composePassengerExternalChargeMap(paxExtChgMap, new Date()));
			}

			boolean isFirstPayment = lccClientReservation.getStatus() != null
					&& lccClientReservation.getStatus().equals(ReservationStatus.ON_HOLD);
			composeFulFillments(lccAirBookModifyRQ, paxSeqWisePayAssemblers, userPrincipal,
					lccClientReservation.getPNR(), isFirstPayment);

			// add passenger e ticket details to the modify request
			if (lccClientReservation.getStatus() != null && lccClientReservation.getStatus().equals(ReservationStatus.CONFIRMED)
					&& mcETGenerationEligible) {

				lccAirBookModifyRQ.setTicketingMethod(TicketingMethod.CREATE);

				List<LCCClientReservationSegment> allLccSegments = new ArrayList<LCCClientReservationSegment>(
						lccClientReservation.getSegments());
				// get unflown segments
				List<LCCClientReservationSegment> unflownLccSegments = ETicketUtil.getUnflownSegments(allLccSegments);

				// get selected infant
				Collection<LCCClientReservationPax> infantList = new ArrayList<LCCClientReservationPax>();
				for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {
					if (reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
						infantList.add(reservationPax);
					}
				}

				CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(trackInfoDTO);
				ticketInfoDto.setBSPPayment(bspPayment);

				// Generate E tickets
				Map<Integer, Map<Integer, EticketDTO>> paxEticketMap = ETicketUtil.generateIATAETicketNumbers(infantList,
						unflownLccSegments, ticketInfoDto);
				Iterator<LCCClientReservationPax> passngerIterator = infantList.iterator();
				while (passngerIterator.hasNext()) {
					LCCClientReservationPax lcclientReservationPax = passngerIterator.next();
					AirTraveler airTraveler = Transformer.transform(lcclientReservationPax, lccAirBookModifyRQ
							.getLccReservation().getAirReservation().getTravelerInfo());
					if (paxEticketMap.keySet().contains(lcclientReservationPax.getPaxSequence())) {
						airTraveler.getETickets().addAll(
								ETicketUtil.createPaxEticket(allLccSegments, lcclientReservationPax.getPaxSequence(),
										paxEticketMap.get(lcclientReservationPax.getPaxSequence())));
					}
				}
			}

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);

			if (lccAirBookRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = BeanUtils.getFirstElement(lccAirBookRS.getResponseAttributes().getErrors());
				throw new ModuleException(lccError.getErrorCode().value());
			}
			if (log.isDebugEnabled()) {
				log.debug("Web service call succeeded for add infant");
			}
			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in addInfant", ex);
			// TODO : Since there is no rollback implemented we must save info in GroupHistory

			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}

	private static void composeFulFillments(LCCAirBookModifyRQ lccAirBookModifyRQ,
			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers, UserPrincipal userPrincipal,
			String groupPNR, boolean isFirstPayment) throws ModuleException {

		if (paxSeqWisePayAssemblers.size() > 0) {
			// Fulfillment
			Fulfillment fulfillment = new Fulfillment();
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setFulfillment(fulfillment);
			fulfillment.getCustomerFulfillments().addAll(
					LCCReservationUtil.parseTravelerFulfillments(paxSeqWisePayAssemblers, userPrincipal, groupPNR,
							false, isFirstPayment));
		}
	}

	public static String addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal)
			throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("LCCClient addUserNote called.");
		}
		String returnUn;
		try {
			LCCUserNoteRQ lccUserNoteRQ = Transformer.transform(userNoteTO, trackInfoDTO);
			lccUserNoteRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			LCCUserNoteRS lccUserNoteRS = LCConnectorUtils.getMaxicoExposedWS().addUserNote(lccUserNoteRQ);
			returnUn = lccUserNoteRS.getUserNote().getUserNoteType();
		} catch (Exception e) {
			log.error("ERROR @ addUserNote", e);
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", e);
			}
		}
		return returnUn;

	}
	
	public static LCCClientReservation reverseRefundableCharges(String groupPNr, String version, boolean isNoShowTaxReverse,
			List<LCCClientChargeReverse> chargeReverseList, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {

		try {
			LCCAirBookModifyRQ lccAirBookModifyRQ = new LCCAirBookModifyRQ();
			lccAirBookModifyRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
			lccAirBookModifyRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccAirBookModifyRQ.setModificationTypeCode(LCCModificationTypeCode.MODTYPE_15_REVERSE_CHARGE);
			lccAirBookModifyRQ.setLccReservation(new LCCReservation());

			AirReservation airReservation = new AirReservation();

			ChargeReverseInfo chargeReverseInfo = new ChargeReverseInfo();
			chargeReverseInfo.setNoShowTaxReverse(isNoShowTaxReverse);
			chargeReverseInfo.getChargeReverseList().addAll(Transformer.transformChargeReverseList(chargeReverseList));
			airReservation.setChargeReverseInfo(chargeReverseInfo);

			lccAirBookModifyRQ.getLccReservation().setAirReservation(airReservation);
			lccAirBookModifyRQ.getLccReservation().setGroupPnr(groupPNr);
			lccAirBookModifyRQ.getLccReservation().getAirReservation().setVersion(version);

			LCCAirBookRS lccAirBookRS = LCConnectorUtils.getMaxicoExposedWS().modifyReservation(lccAirBookModifyRQ);
			return Transformer.transform(lccAirBookRS, null);
		} catch (Exception ex) {
			log.error("Error in adjustCharge", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException("lccclient.reservation.invocationError", ex);
			}
		}
	}
	
	public static Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal, String operatingCarrier) throws ModuleException {
		
		LCCSelfReprotectFlightsRQ lccSelfReprotectFlightsRQ = Transformer.transform(trackInfo, alertId);
		lccSelfReprotectFlightsRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccSelfReprotectFlightsRQ.setOperatingCarrier(operatingCarrier);
		
		LCCSelfReprotectFlightsRS lccSelfReprotectFlightsRS = LCConnectorUtils.getMaxicoExposedWS().getSelfReprotectFlights(lccSelfReprotectFlightsRQ);
	
		Collection<SelfReprotectFlightDTO> flightList = new ArrayList<SelfReprotectFlightDTO>();
		
		for(FlightSegment flightSegment : lccSelfReprotectFlightsRS.getFlightSegments()){
			SelfReprotectFlightDTO selfReprotectFlightDTO = new SelfReprotectFlightDTO();
			
			selfReprotectFlightDTO.setArrivalDate(flightSegment.getArrivalDateTime());
			selfReprotectFlightDTO.setDepartureDate(flightSegment.getDepatureDateTime());
			selfReprotectFlightDTO.setFlightId(flightSegment.getFlightId());
			selfReprotectFlightDTO.setFlightNumber(flightSegment.getFlightNumber());
			selfReprotectFlightDTO.setFltSegId(flightSegment.getFlightRefNumber());
			selfReprotectFlightDTO.setSegmentCode(flightSegment.getSegmentCode());
			selfReprotectFlightDTO.setFlightDuration(flightSegment.getFlightDuration());
			
			flightList.add(selfReprotectFlightDTO);
			
		}
		
		return flightList;
	}


	private static List<PerPaxExternalCharges> composePassengerExternalChargeMap(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
				externalCharge.setOperationMode(lccClientExternalChgDTO.getOperationMode());

				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}
	
	private static Map<Integer, List<LCCClientExternalChgDTO>>
			extractHandlingFeeExternalCharges(Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<>();

		if (paxSeqWisePayAssemblers != null && !paxSeqWisePayAssemblers.isEmpty()) {
			for (Integer key : paxSeqWisePayAssemblers.keySet()) {
				LCCClientPaymentAssembler lccClientPaymentAssembler = paxSeqWisePayAssemblers.get(key);
				if (lccClientPaymentAssembler != null && lccClientPaymentAssembler.getPerPaxExternalCharges() != null
						&& !lccClientPaymentAssembler.getPerPaxExternalCharges().isEmpty()) {
					Collection<LCCClientExternalChgDTO> tmpExternalChgDTO = lccClientPaymentAssembler
							.getPerPaxExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
					if (tmpExternalChgDTO != null && !tmpExternalChgDTO.isEmpty()) {
						paxExtChgMap.put(key, new ArrayList<LCCClientExternalChgDTO>(tmpExternalChgDTO));
					}
				}
			}
		}

		return paxExtChgMap;
	}

}