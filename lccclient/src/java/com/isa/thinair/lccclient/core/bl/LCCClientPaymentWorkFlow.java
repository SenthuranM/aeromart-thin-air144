package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.ejb.SessionContext;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonOfflinePaymentInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaymentRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPaymentRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCTicketingInfoRS;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonLoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.LoggingUtil;
import com.isa.thinair.lccclient.core.util.LCCReservationUtil;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Nilindra Fernando
 */
public class LCCClientPaymentWorkFlow {

	private static Log log = LogFactory.getLog(LCCClientPaymentWorkFlow.class);

	private enum PaymentTypes {
		ON_ACCOUNT, CREDIT_CARD, CASH, PAX_CREDIT, LMS, VOUCHER, OFFLINE
	};

	public static void reversePayments(String groupPNR, Collection<LCCClientPaymentInfo> manualReversablePayments)
			throws ModuleException {
		reverseCreditCardPayments(groupPNR, manualReversablePayments);
	}

	public static void reservationSuccess(Collection<LCCClientPaymentInfo> manualReversablePayments) throws ModuleException {
		Collection<Integer> colTnxIds = new ArrayList<Integer>();

		for (LCCClientPaymentInfo lccClientPaymentInfo : manualReversablePayments) {
			if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo tmpLCCClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;

				colTnxIds.add(tmpLCCClientCreditCardPaymentInfo.getTemporyPaymentId());
			}
		}

		if (colTnxIds.size() > 0) {
			LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS, null, null, null, null);
		}
	}

	private static void reverseCreditCardPayments(String groupPNR, Collection<LCCClientPaymentInfo> manualReversablePayments)
			throws ModuleException {
		for (LCCClientPaymentInfo lccClientPaymentInfo : manualReversablePayments) {

			if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo tmpLCCClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;

				// Only if the payment success only we need to revert
				if (tmpLCCClientCreditCardPaymentInfo.isPaymentSuccess()) {
					CreditCardPayment creditCardPayment = tmpLCCClientCreditCardPaymentInfo.transform(groupPNR);
					PreviousCreditCardPayment previousCreditCardPayment = tmpLCCClientCreditCardPaymentInfo
							.transformForPreviousCreditCardPayment(groupPNR);
					creditCardPayment.setPreviousCCPayment(previousCreditCardPayment);

					Collection<Integer> colTnxIds = new ArrayList<Integer>();
					colTnxIds.add(tmpLCCClientCreditCardPaymentInfo.getTemporyPaymentId());

					ServiceResponce serviceResponce = LCCClientModuleUtil.getPaymentBrokerBD().reverse(creditCardPayment,
							groupPNR, tmpLCCClientCreditCardPaymentInfo.getAppIndicator(),
							tmpLCCClientCreditCardPaymentInfo.getTnxMode());

					if (serviceResponce != null && serviceResponce.isSuccess()) {
						LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(colTnxIds,
								ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS, null, null, null, null);
					} else {
						LCCClientModuleUtil.getReservationBD().updateTempPaymentEntry(colTnxIds,
								ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE, null, null, null, null);

						throw new ModuleException("airreservations.arg.cc.noresponse");
					}
				}
			}
		}
	}

	public static void setLccUniqueTnxIdPerPayments(Collection<LCCClientPaymentInfo> payments,
			Map<PaymentTypes, String> lccUniqueTnxIds) throws ModuleException {
		for (LCCClientPaymentInfo payment : payments) {
			if (payment instanceof LCCClientCashPaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.CASH));
			} else if (payment instanceof LCCClientLMSPaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.LMS));
			} else if (payment instanceof LCCClientOnAccountPaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.ON_ACCOUNT));
			} else if (payment instanceof CommonCreditCardPaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.CREDIT_CARD));
			} else if (payment instanceof LCCClientPaxCreditPaymentInfo) {
				// new UID for each pax credit.
				payment.setLccUniqueTnxId(UniqueIDGenerator.generate());
			} else if (payment instanceof LCCClientVoucherPaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.VOUCHER));
			} else if (payment instanceof CommonOfflinePaymentInfo) {
				payment.setLccUniqueTnxId(lccUniqueTnxIds.get(PaymentTypes.OFFLINE));
			} else {
				throw new ModuleException("lccclient.reservation.unsupportedPaymentType");
			}
		}
	}

	public static void setLccUniqueTnxId(CommonReservationAssembler lcclientReservationAssembler) throws ModuleException {
		Map<PaymentTypes, String> lccUniqueTnxIds = createLccTnxUID();
		for (LCCClientReservationPax lccClientReservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
			if (!lccClientReservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
				setLccUniqueTnxIdPerPayments(lccClientReservationPax.getLccClientPaymentAssembler().getPayments(),
						lccUniqueTnxIds);
			}
		}
	}

	public static void setLccUniqueTnxId(Collection<LCCClientPaymentAssembler> lccClientPaymentAssemblers) throws ModuleException {
		Map<PaymentTypes, String> lccUniqueTnxIds = createLccTnxUID();
		for (LCCClientPaymentAssembler lccClientPaymentAssembler : lccClientPaymentAssemblers) {
			setLccUniqueTnxIdPerPayments(lccClientPaymentAssembler.getPayments(), lccUniqueTnxIds);
		}
	}

	public static void setLccUniqueTnxId(LCCClientPaymentAssembler lccClientPaymentAssembler) throws ModuleException {
		Map<PaymentTypes, String> lccUniqueTnxIds = createLccTnxUID();
		setLccUniqueTnxIdPerPayments(lccClientPaymentAssembler.getPayments(), lccUniqueTnxIds);
	}

	public static void setLccUniqueTnxIdsForGroupRefund(LCCClientPaymentAssembler lccClientPaymentAssembler)
			throws ModuleException {
		for (LCCClientPaymentInfo payment : lccClientPaymentAssembler.getPayments()) {
			if (payment instanceof LCCClientCashPaymentInfo || payment instanceof LCCClientOnAccountPaymentInfo
					|| payment instanceof CommonOfflinePaymentInfo || payment instanceof CommonCreditCardPaymentInfo || payment instanceof LCCClientPaxCreditPaymentInfo) {
				payment.setLccUniqueTnxId(UniqueIDGenerator.generate());
			} else {
				throw new ModuleException("lccclient.reservation.unsupportedPaymentType");
			}
		}

	}

	/**
	 * generate per tnx per paymetMode lcc unique tnx id values
	 * 
	 * @return
	 */
	private static Map<PaymentTypes, String> createLccTnxUID() {

		Map<PaymentTypes, String> perPaymentModeLccUTnxID = new HashMap<LCCClientPaymentWorkFlow.PaymentTypes, String>();

		for (PaymentTypes paymentTypes : PaymentTypes.values()) {
			perPaymentModeLccUTnxID.put(paymentTypes, UniqueIDGenerator.generate());
		}

		return perPaymentModeLccUTnxID;
	}

	public static Map<Integer, CommonCreditCardPaymentInfo> makeTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo,
			Collection<LCCClientPaymentInfo> colLCCClientPaymentInfo, boolean isCredit, CredentialsDTO credentialsDTO,
			SessionContext sessionContext, String originatorCarrierCode, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {

		try {
			if (colLCCClientPaymentInfo == null || colLCCClientPaymentInfo.size() == 0) {
				throw new ModuleException("lccclient.temporyPayment.missingParameters");
			}

			groupPNR = BeanUtils.nullHandler(groupPNR);

			if (groupPNR.length() == 0
					&& (BeanUtils.nullHandler(lccClientReservationContactInfo.getFirstName()).length() == 0
							|| BeanUtils.nullHandler(lccClientReservationContactInfo.getLastName()).length() == 0 || BeanUtils
							.nullHandler(lccClientReservationContactInfo.getPhoneNo()).length() == 0)) {
				throw new ModuleException("lccclient.temporyPayment.missingParameters");
			}

			if (groupPNR.length() == 0) {
				if (StringUtils.isNotEmpty(originatorCarrierCode)) {
					groupPNR = BeanUtils.nullHandler(getGroupPNR(originatorCarrierCode, userPrincipal, trackInfo));
				}

				if (groupPNR.length() == 0) {
					throw new ModuleException("lccclient.reservation.empty.ticketing.info");
				}
			}

			for (LCCClientPaymentInfo lccClientPaymentInfo : colLCCClientPaymentInfo) {
				PayCurrencyDTO payCurrencyDTO = lccClientPaymentInfo.getPayCurrencyDTO();
				BigDecimal totalPayAmountRoundedInPayCurr = AccelAeroRounderPolicy.convertAndRound(
						lccClientPaymentInfo.getTotalAmount(), payCurrencyDTO.getPayCurrMultiplyingExchangeRate(),
						payCurrencyDTO.getBoundaryValue(), payCurrencyDTO.getBreakPointValue());
				payCurrencyDTO.setTotalPayCurrencyAmount(totalPayAmountRoundedInPayCurr);
				// Should be base currency amount
				// Need to review
				lccClientPaymentInfo.setPayCurrencyAmount(lccClientPaymentInfo.getTotalAmount());
			}

			return recordTemporyPaymentEntry(groupPNR, lccClientReservationContactInfo, colLCCClientPaymentInfo, credentialsDTO,
					isCredit);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::makeTemporyPaymentEntry ", ex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::makeTemporyPaymentEntry ", cdaex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private static Map<Integer, CommonCreditCardPaymentInfo> recordTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo, Collection<LCCClientPaymentInfo> colPaymentInfo,
			CredentialsDTO credentialsDTO, boolean isCredit) throws ModuleException {

		Collection<CommonCreditCardPaymentInfo> colCardPaymentInfo = new ArrayList<CommonCreditCardPaymentInfo>();
		boolean isOfflinePayment = false;

		for (LCCClientPaymentInfo lccClientPaymentInfo : colPaymentInfo) {
			if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
				if (!isOfflinePayment) {
					isOfflinePayment = lccClientPaymentInfo.isOfflinePayment();
				}
				CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;
				lccClientCreditCardPaymentInfo.setGroupPNR(groupPNR);

				cardPaymentInfoCompressor(lccClientCreditCardPaymentInfo, colCardPaymentInfo);
			}
		}

		Map<Integer, CommonCreditCardPaymentInfo> mapTmpPaymentIdAndCardInfo = new HashMap<Integer, CommonCreditCardPaymentInfo>();

		for (CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo : colCardPaymentInfo) {
			TemporyPaymentTO temporyPaymentTO = composeTemporyPaymentTO(groupPNR, lccClientReservationContactInfo,
					lccClientCreditCardPaymentInfo, isCredit);
			temporyPaymentTO.setOfflinePayment(isOfflinePayment);
			TempPaymentTnx tempPaymentTnx = LCCClientModuleUtil.getReservationBD().recordTemporyPaymentEntry(temporyPaymentTO);
			lccClientCreditCardPaymentInfo.setTemporyPaymentId(tempPaymentTnx.getTnxId());

			mapTmpPaymentIdAndCardInfo.put(tempPaymentTnx.getTnxId(), lccClientCreditCardPaymentInfo);
		}

		// If any card payments exist setting the tempory payment id
		if (colCardPaymentInfo.size() > 0) {
			// Capture the card payment

			for (CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfoObj : colCardPaymentInfo) {

				for (LCCClientPaymentInfo lccClientPaymentInfo : colPaymentInfo) {
					if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
						CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;

						// If match found
						if (CommonCreditCardPaymentInfo
								.compare(lccClientCreditCardPaymentInfoObj, lccClientCreditCardPaymentInfo)) {
							lccClientCreditCardPaymentInfo.setTemporyPaymentId(lccClientCreditCardPaymentInfoObj
									.getTemporyPaymentId());
						}
					}
				}
			}
		}

		return mapTmpPaymentIdAndCardInfo;
	}

	private static TemporyPaymentTO composeTemporyPaymentTO(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo,
			CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo, boolean isCredit) {

		TemporyPaymentTO temporyPaymentTO = new TemporyPaymentTO();
		temporyPaymentTO.setAmount(lccClientCreditCardPaymentInfo.getTotalAmount());
		temporyPaymentTO.setCcNo(lccClientCreditCardPaymentInfo.getNo());

		temporyPaymentTO.setCredit(isCredit);
		temporyPaymentTO.setFirstName(lccClientReservationContactInfo.getFirstName());
		temporyPaymentTO.setLastName(lccClientReservationContactInfo.getLastName());
		temporyPaymentTO.setGroupPnr(groupPNR);
		temporyPaymentTO.setMobileNo(lccClientReservationContactInfo.getMobileNo());
		temporyPaymentTO.setTelephoneNo(lccClientReservationContactInfo.getPhoneNo());
		temporyPaymentTO.setPaymentCurrencyAmount(lccClientCreditCardPaymentInfo.getPayCurrencyDTO().getTotalPayCurrencyAmount());
		temporyPaymentTO.setPaymentCurrencyCode(lccClientCreditCardPaymentInfo.getPayCurrencyDTO().getPayCurrencyCode());
		temporyPaymentTO.setEmailAddress(lccClientReservationContactInfo.getEmail());

		return temporyPaymentTO;
	}

	// generates the new PNR.
	private static String getGroupPNR(String originatingCarrier, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		String groupPnr = null;

		// if the first segments carrier is this carrier, we don't need to make a lcc call.
		if (AppSysParamsUtil.getDefaultCarrierCode().equals(originatingCarrier)) {
			// we are requesting a new PNR - so passing null arguments is ok.
			groupPnr = LCCClientModuleUtil.getReservationBD().getNewPNR(false, null);
		} else {

			LCCTicketingInfoRQ lccTicketingInfoRQ = new LCCTicketingInfoRQ();
			lccTicketingInfoRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
			lccTicketingInfoRQ.setCarrierCode(originatingCarrier);

			LCCTicketingInfoRS lccTicketingInfoRS = LCConnectorUtils.getMaxicoExposedWS().getTicketingInfo(lccTicketingInfoRQ);
			if (lccTicketingInfoRS != null) {
				groupPnr = lccTicketingInfoRS.getGroupPnr();
			}
		}

		return groupPnr;
	}

	/**
	 * Compress card payment info objects
	 * 
	 * @param cardPaymentInfo
	 * @param colCardPaymentInfo
	 * @return
	 */
	public static Collection<CommonCreditCardPaymentInfo> cardPaymentInfoCompressor(CommonCreditCardPaymentInfo cardPaymentInfo,
			Collection<CommonCreditCardPaymentInfo> colCardPaymentInfo) {
		CommonCreditCardPaymentInfo cloneCardPaymentInfo = (CommonCreditCardPaymentInfo) cardPaymentInfo.clone();

		PayCurrencyDTO payCurrencyDTO;
		PayCurrencyDTO clonePayCurrencyDTO;
		boolean newRecord = true;

		for (CommonCreditCardPaymentInfo cardPayInfo : colCardPaymentInfo) {

			if (CommonCreditCardPaymentInfo.compare(cardPayInfo, cloneCardPaymentInfo)) {
				newRecord = false;
				cardPayInfo.setTotalAmount(AccelAeroCalculator.add(cardPayInfo.getTotalAmount(),
						cloneCardPaymentInfo.getTotalAmount()));
				payCurrencyDTO = cardPayInfo.getPayCurrencyDTO();
				clonePayCurrencyDTO = cloneCardPaymentInfo.getPayCurrencyDTO();

				payCurrencyDTO.setTotalPayCurrencyAmount(AccelAeroCalculator.add(payCurrencyDTO.getTotalPayCurrencyAmount(),
						clonePayCurrencyDTO.getTotalPayCurrencyAmount()));

				break;
			}
		}

		// If its a newly created record
		if (newRecord) {
			colCardPaymentInfo.add(cloneCardPaymentInfo);
		}

		return colCardPaymentInfo;
	}

	private static BigDecimal getAmountInPayCurrency(BigDecimal amountInBaseCurrency, PayCurrencyDTO payCurrencyDTO) {
		return AccelAeroRounderPolicy.convertAndRound(amountInBaseCurrency, payCurrencyDTO.getPayCurrMultiplyingExchangeRate(),
				payCurrencyDTO.getBoundaryValue(), payCurrencyDTO.getBreakPointValue());
	}

	public static void extractCreditCardPayments(PaymentAssembler paymentAssembler) {
		for (Iterator<PaymentInfo> iterator = paymentAssembler.getPayments().iterator(); iterator.hasNext();) {
			PaymentInfo paymentInfo = iterator.next();
			if (!(paymentInfo instanceof CardPaymentInfo))
				iterator.remove();
		}
	}

	public static void transform(PaymentAssembler ipaymentPassenger, LCCClientPaymentAssembler paymentAssembler)
			throws ModuleException {
		for (LCCClientPaymentInfo payment : paymentAssembler.getPayments()) {
			// add any carrier LMS payment
			if (payment instanceof LCCClientLMSPaymentInfo) {
				LCCClientLMSPaymentInfo payOpt = (LCCClientLMSPaymentInfo) payment;
				ipaymentPassenger.addLMSPayment(payOpt.getLoyaltyMemberAccountId(), payOpt.getRewardIDs(),
						payOpt.getTotalAmount(), payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), null, null);
			} else if (payment.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {

				if (payment instanceof CommonLoyaltyPaymentInfo) {
					CommonLoyaltyPaymentInfo payOpt = (CommonLoyaltyPaymentInfo) payment;
					ipaymentPassenger.addLoyaltyCreditPayment(payOpt.getAgentCode(), payOpt.getTotalAmount(),
							payOpt.getLoyaltyAccount(), null, payOpt.getPayCurrencyDTO(), null, null);
				} else if (payment instanceof CommonCreditCardPaymentInfo) {
					CommonCreditCardPaymentInfo payOpt = (CommonCreditCardPaymentInfo) payment;
					ipaymentPassenger.addCardPayment(payOpt.getType(), payOpt.geteDate(), payOpt.getNo(), payOpt.getName(),
							payOpt.getAddress(), payOpt.getSecurityCode(), payOpt.getTotalAmount(), payOpt.getAppIndicator(),
							payOpt.getTnxMode(), payOpt.getOldCCRecordId(), payOpt.getIpgIdentificationParamsDTO(),
							payOpt.getPayReference(), payOpt.getActualPaymentMethod(), null, payOpt.getPayCurrencyDTO(), null,
							null, payOpt.getPaymentBrokerId(), null, null);
				} else if (payment instanceof LCCClientCashPaymentInfo) {
					LCCClientCashPaymentInfo payOpt = (LCCClientCashPaymentInfo) payment;
					ipaymentPassenger.addCashPayment(payOpt.getTotalAmount(), payOpt.getPayReference(),
							payOpt.getActualPaymentMethod(), null, payOpt.getPayCurrencyDTO(), null, null);

				} else if (payment instanceof LCCClientOnAccountPaymentInfo) {
					LCCClientOnAccountPaymentInfo payOpt = (LCCClientOnAccountPaymentInfo) payment;
					ipaymentPassenger.addAgentCreditPayment(payOpt.getAgentCode(), payOpt.getTotalAmount(),
							payOpt.getPayReference(), payOpt.getActualPaymentMethod(), null, payOpt.getPayCurrencyDTO(), null,
							payOpt.getPaymentMethod(), null);

				} else if (payment instanceof LCCClientPaxCreditPaymentInfo) {
					LCCClientPaxCreditPaymentInfo payOpt = (LCCClientPaxCreditPaymentInfo) payment;
					// pass the carrier code and pnr to facilitate any carrier credit utilization
					ipaymentPassenger.addCreditPayment(payOpt.getTotalAmount(), payOpt.getDebitPaxRefNo() + "",
							payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(),
							payOpt.getPaxSequence(), null, payOpt.getResidingPnr(), null, null, null);
				} else if (payment instanceof LCCClientVoucherPaymentInfo) {
					LCCClientVoucherPaymentInfo payOpt = (LCCClientVoucherPaymentInfo) payment;
					ipaymentPassenger.addVoucherPayment(payOpt.getVoucherDTO(), payOpt.getTotalAmount(), payOpt.getCarrierCode(),
							payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), null);
				} else if (payment instanceof CommonOfflinePaymentInfo) {
					CommonOfflinePaymentInfo payOpt = (CommonOfflinePaymentInfo) payment;
					ipaymentPassenger.addOfflinePayment(payOpt.getTotalAmount(), payOpt.getCarrierCode(),
							payOpt.getPayCurrencyDTO(), payOpt.getIpgIdentificationParamsDTO(), null);
				} else {
					throw new ModuleException("lccclient.reservation.incorrect.payment.mode");
				}
			}
		}
	}

	public static Map<Integer, CardPaymentInfo> populateTemPaymentEntries(
			Map<Integer, CommonCreditCardPaymentInfo> temporaryPaymentMap) {
		Map<Integer, CardPaymentInfo> cardPayInfoMap = new HashMap<Integer, CardPaymentInfo>();
		if (temporaryPaymentMap != null && temporaryPaymentMap.size() > 0) {
			Set<Integer> tmpKeySet = temporaryPaymentMap.keySet();
			for (Integer key : tmpKeySet) {
				CommonCreditCardPaymentInfo commonInfo = temporaryPaymentMap.get(key);
				CardPaymentInfo cardPayInfo = new CardPaymentInfo();
				cardPayInfo.setAddress(commonInfo.getAddress());
				cardPayInfo.setAppIndicator(commonInfo.getAppIndicator());
				cardPayInfo.setAuthorizationId(commonInfo.getAuthorizationId());
				cardPayInfo.setEDate(commonInfo.geteDate());
				cardPayInfo.setIpgIdentificationParamsDTO(commonInfo.getIpgIdentificationParamsDTO());
				cardPayInfo.setName(commonInfo.getName());
				cardPayInfo.setNo(commonInfo.getNo());
				cardPayInfo.setNoFirstDigits(commonInfo.getNoFirstDigits());
				cardPayInfo.setNoLastDigits(commonInfo.getNoLastDigits());
				cardPayInfo.setPayCurrencyDTO(commonInfo.getPayCurrencyDTO());
				cardPayInfo.setPaymentBrokerRefNo(commonInfo.getPaymentBrokerId());
				cardPayInfo.setTemporyPaymentId(commonInfo.getTemporyPaymentId());
				cardPayInfo.setTnxMode(commonInfo.getTnxMode());
				cardPayInfo.setType(commonInfo.getType());
				cardPayInfo.setTotalAmount(commonInfo.getTotalAmount());
				cardPayInfo.setSecurityCode(commonInfo.getSecurityCode());
				cardPayInfo.setPnr(commonInfo.getGroupPNR());
				cardPayInfo.setSuccess(commonInfo.isPaymentSuccess());

				cardPayInfoMap.put(key, cardPayInfo);
			}
		}
		return cardPayInfoMap;
	}

	public static Map<Integer, LCCClientPaymentAssembler> makePayment(CommonReservationContactInfo contactInfo, String pnr,
			String transactionIdentifier, Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal, SessionContext sessionContext) throws ModuleException {
		try {
			LCCPaymentRQ lccPaymentRQ = new LCCPaymentRQ();
			lccPaymentRQ.setContactInfo(LCCClientApiUtils.transform(contactInfo));
			lccPaymentRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			lccPaymentRQ.setPnr(pnr);
			lccPaymentRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionIdentifier));

			// inject the lccUniqTnx id to the payments
			LCCClientPaymentWorkFlow.setLccUniqueTnxId(paxWisePaymentAssemblers.values());
			lccPaymentRQ.getFulfillment().addAll(
					LCCReservationUtil.parseTravelerFulfillments(paxWisePaymentAssemblers, userPrincipal, pnr, true, false));
			// This should not be ZERO. If it is zero please filter this initial stage.
			if (lccPaymentRQ.getFulfillment().isEmpty())
				throw new ModuleException("lccclient.reservation.incorrect.payment.flow");
			if (log.isDebugEnabled()) {
				log.debug(" Going to capture payments of [" + LoggingUtil.getPaymentDetails(lccPaymentRQ.getFulfillment()) + " ]"
						+ " for reservation " + pnr);
			}
			LCCPaymentRS lccPaymentRS = LCConnectorUtils.getMaxicoExposedWS().makePayment(lccPaymentRQ);
			if (lccPaymentRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccPaymentRS.getResponseAttributes().getErrors());
				log.error(" Exception::makepayment for other carriers " + lccError.getDescription());
				sessionContext.setRollbackOnly();
				throw new ModuleException(lccError.getErrorCode().value());
			}
			if (log.isDebugEnabled()) {
				log.debug(" Successfully captured payments of ["
						+ LoggingUtil.getPaymentDetails(lccPaymentRS.getTravelerFulfillments()) + " ]" + " for reservation "
						+ pnr);
			}
			return LCCReservationUtil.combineActualCreditPayments(paxWisePaymentAssemblers,
					lccPaymentRS.getTravelerFulfillments());
		} catch (Exception exception) {
			log.error(" Exception::makepayment for other carriers ", exception);
			sessionContext.setRollbackOnly();
			throw new ModuleException(exception, exception.getMessage());
		}
	}

	public static void refundPayment(CommonReservationContactInfo contactInfo, String pnr, String transactionIdentifier,
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers, TrackInfoDTO trackInfoDTO,
			UserPrincipal userPrincipal, SessionContext sessionContext) throws ModuleException {
		try {
			LCCPaymentRQ lccPaymentRQ = new LCCPaymentRQ();
			lccPaymentRQ.setContactInfo(LCCClientApiUtils.transform(contactInfo));
			lccPaymentRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfoDTO));
			lccPaymentRQ.setPnr(pnr);
			lccPaymentRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionIdentifier));
			lccPaymentRQ.getFulfillment().addAll(
					LCCReservationUtil.parseTravelerFulfillments(paxWisePaymentAssemblers, userPrincipal, pnr, true, false));
			// This should not be ZERO. If it is zero please filter this initial stage.
			if (lccPaymentRQ.getFulfillment().isEmpty())
				throw new ModuleException("lccclient.reservation.incorrect.payment.flow");
			LCCPaymentRS lccPaymentRS = LCConnectorUtils.getMaxicoExposedWS().refundPayment(lccPaymentRQ);
			if (lccPaymentRS.getResponseAttributes().getSuccess() == null) {
				LCCError lccError = (LCCError) BeanUtils.getFirstElement(lccPaymentRS.getResponseAttributes().getErrors());
				log.error(" Exception::makepayment for other carriers " + lccError.getDescription());
				sessionContext.setRollbackOnly();
				throw new ModuleException(lccError.getErrorCode().value());
			}
		} catch (Exception exception) {
			log.error(" Exception::makepayment for other carriers ", exception);
			sessionContext.setRollbackOnly();
			throw new ModuleException(exception, exception.getMessage());
		}
	}
}
