package com.isa.thinair.lccclient.core.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.exposed.services.MaxicoExposedWS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.core.security.LCCClientSecurityHandler;

/**
 * @author Nilindra Fernando
 * @since 3/25/2010
 */
public class LCConnectorUtils {

	private static final Log log = LogFactory.getLog(LCConnectorUtils.class);

	private static MaxicoExposedWS maxicoWS;

	private LCConnectorUtils() {

	}

	public static MaxicoExposedWS getMaxicoExposedWS() throws ModuleException {
		if (maxicoWS == null) {
			String lccURL = LCCClientModuleUtil.getConfig().getLccWsUrl();

			if (log.isInfoEnabled()) {
				log.info("LCC Exposed Services URL = " + lccURL);
			}

			URL url;
			try {
				url = new URL(lccURL);
			} catch (MalformedURLException e) {
				log.error(e);
				throw new ModuleException("lccclient.invalid.lcconnect.url");
			}

			try {
				QName qname = new QName(MaxicoExposedWS.targetNamespace, MaxicoExposedWS.serviceName);

				Service service = Service.create(url, qname);

				maxicoWS = (MaxicoExposedWS) service.getPort(MaxicoExposedWS.class);

				String[] credentials = AppSysParamsUtil.getLCCConnectivityCredentials();
				if (log.isInfoEnabled()) {
					log.info("LCC Connectivity Username = " + credentials[0] + ", password length = "
							+ (credentials[1] != null ? credentials[1].length() : 0));
				}

				List<Handler> handlerChain = new ArrayList<Handler>();
				handlerChain.add(new LCCClientSecurityHandler(credentials[0], credentials[1]));
				((BindingProvider) maxicoWS).getBinding().setHandlerChain(handlerChain);

				((BindingProvider) maxicoWS).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
			} catch (javax.xml.ws.WebServiceException e) {
				log.error("Error in dynamically binding LCC Exposed webservices", e);
				throw e;
			}
		}

		return maxicoWS;
	}

}
