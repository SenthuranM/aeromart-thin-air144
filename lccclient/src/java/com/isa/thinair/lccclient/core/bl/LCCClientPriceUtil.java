package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingClassAllocation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareRule;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSegmentChargeTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fee;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBCAllocation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ONDExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndFareSegmentChargeTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxJourneyFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Surcharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Tax;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.lccclient.core.util.LCCAncillaryUtil;

public class LCCClientPriceUtil {

	public static PriceInfoTO createPriceInfoTO(PriceInfo priceInfo, Map<Integer, Boolean> ondwiseFlexiSelected) {
		PriceInfoTO priceInfoTO = new PriceInfoTO();

		// setting the root fare type
		priceInfoTO.setFareTypeTO(createFareTypeTO(priceInfo));
		priceInfoTO.setFareSegChargeTO(createFareSegCharges(priceInfo.getFareSegmentChargeTO(), priceInfo.isOnholdRestricted()));
		createOndWiseSeatcAvailability(priceInfoTO, priceInfo.getOndSeatAvailability(), ondwiseFlexiSelected);
		return priceInfoTO;
	}

	public static PriceInfoTO createPriceInfoTO(List<PriceInfo> priceInfoTypes, Map<Integer, Boolean> ondwiseFlexiSelected) {
		PriceInfoTO priceInfoTO = new PriceInfoTO();

		for (PriceInfo priceInfo : priceInfoTypes) {
			// setting the root fare type
			priceInfoTO.setFareTypeTO(createFareTypeTO(priceInfo));
			priceInfoTO.setFareSegChargeTO(createFareSegCharges(priceInfo.getFareSegmentChargeTO(), priceInfo.isOnholdRestricted()));
			createOndWiseSeatcAvailability(priceInfoTO, priceInfo.getOndSeatAvailability(), ondwiseFlexiSelected);
			createOnDWiseFlexiSelection(priceInfoTO);
		}

		return priceInfoTO;
	}

	private static void createOnDWiseFlexiSelection(PriceInfoTO priceInfoTO) {
		FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();
		if (fareTypeTO.getOndExternalCharges() != null && fareTypeTO.getOndExternalCharges().size() > 0) {
			for (ONDExternalChargeTO ondExternalChargeTO : fareTypeTO.getOndExternalCharges()) {
				if (fareTypeTO.hasOndExternalCharge(ondExternalChargeTO.getOndSequence())) {
					fareTypeTO.getOndFlexiSelection().put(ondExternalChargeTO.getOndSequence(), true);

				}
			}
		}

	}

	private static FareTypeTO createFareTypeTO(PriceInfo priceInfo) {

		FareTypeTO fare = createFareType(priceInfo.getFareType());
		fare.setOnHoldRestricted(priceInfo.isOnholdRestricted());
		fare.setOnHoldStartTime(priceInfo.getOnholdBufferTimeZulu());
		fare.setOnHoldStopTime(priceInfo.getFlightCloseTimeZulu());

		// setting the per pax price info
		if (priceInfo.getPerPaxPriceInfo() != null) {
			fare.setPerPaxPriceInfo(new ArrayList<PerPaxPriceInfoTO>());

			for (PerPaxPriceInfo perPaxPriceInfo : priceInfo.getPerPaxPriceInfo()) {

				PerPaxPriceInfoTO perPaxPriceInfoTO = new PerPaxPriceInfoTO();
				perPaxPriceInfoTO.setPassengerPrice(createFareType(perPaxPriceInfo.getPassengerPrice()));
				perPaxPriceInfoTO.setTravelerRefNumber(perPaxPriceInfo.getTravelerRefNumber());
				perPaxPriceInfoTO.setPassengerType(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(perPaxPriceInfo
						.getPassengerType()));

				fare.getPerPaxPriceInfo().add(perPaxPriceInfoTO);
			}
		}

		if (priceInfo.getOndFareType() != null) {
			for (IntegerIntegerMap mapObj : priceInfo.getOndFareType()) {
				fare.getOndWiseFareType().put(mapObj.getKey(), mapObj.getValue());
			}
		}

		fare.setApplicableFareRules(getFareRuleDTOList(priceInfo.getApplicableFareRule()));

		// Set Applied Promotion Obj
		fare.setPromotionTO(LCCClientCommonUtils.populateApplicablePromotionDetailsTO(priceInfo.getPromotionDetails()));
		fare.setServiceTaxAmount(priceInfo.getTotalServiceTaxAmount());

		return fare;
	}

	/**
	 * @param priceInfoTO
	 * @param inBoundOutBoundSeatAvailability
	 */
	private static void createOndWiseSeatcAvailability(PriceInfoTO priceInfoTO, List<OndSeatAvailability> ondSeatAvailabilities,
			Map<Integer, Boolean> ondwiseFlexiSelected) {

		for (OndSeatAvailability ondSeatAvail : ondSeatAvailabilities) {

			boolean ondFlexSelected = false;

			List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses = new ArrayList<LogicalCabinClassInfoTO>();
			Map<String, List<LogicalCabinClassInfoTO>> ondWiseAvailability = new HashMap<String, List<LogicalCabinClassInfoTO>>();

			for (SeatAvailability seatAvailability : ondSeatAvail.getSeatAvailability()) {
				if (ondWiseAvailability.get(seatAvailability.getJourneyOndCode()) == null) {
					ondWiseAvailability.put(seatAvailability.getJourneyOndCode(), new ArrayList<LogicalCabinClassInfoTO>());
				}
				if (containsSeatAvailability(ondWiseAvailability.get(seatAvailability.getJourneyOndCode()),
						seatAvailability.getLogicalCabinClass(), seatAvailability.isWithFlexi(),
						seatAvailability.getBundledFarePeriodId())) {
					continue;
				}
				LogicalCabinClassInfoTO logicalCabinClassInfoTO = new LogicalCabinClassInfoTO();
				logicalCabinClassInfoTO.setSelected(seatAvailability.isSelected());
				logicalCabinClassInfoTO.setComment(seatAvailability.getComment());
				logicalCabinClassInfoTO.setLogicalCCCode(seatAvailability.getLogicalCabinClass());
				logicalCabinClassInfoTO.setLogicalCCDesc(seatAvailability.getLogicalCabinClassDescription());
				logicalCabinClassInfoTO.setWithFlexi(seatAvailability.isWithFlexi());
				logicalCabinClassInfoTO.setFlexiAvailable(seatAvailability.isFlexiAvailable());
				logicalCabinClassInfoTO.setRank(seatAvailability.getNestRank());
				logicalCabinClassInfoTO.setFlexiRuleID(seatAvailability.getFlexiRuleID() == 0 ? null : seatAvailability
						.getFlexiRuleID());
				logicalCabinClassInfoTO.setBundledFarePeriodId(seatAvailability.getBundledFarePeriodId());
				logicalCabinClassInfoTO.setBundledFareFee(seatAvailability.getBundledFareFee());
				logicalCabinClassInfoTO.setImageUrl(seatAvailability.getImageUrl());
				logicalCabinClassInfoTO.setBundleFareDescriptionTemplateDTO(Transformer.transform(seatAvailability.getBundleFareDescriptionInfo()));
							
				logicalCabinClassInfoTO.setBookingClasses(new HashSet<String>(seatAvailability.getBookingCodes()));
				logicalCabinClassInfoTO.setSegmentBookingClasses(Transformer.convertLCCMapToAAMap(seatAvailability
						.getSegmentBookingClasses()));
				logicalCabinClassInfoTO.setBundledFareFreeServiceName(LCCAncillaryUtil
						.convertToBundledFareFreeServiceNames(seatAvailability.getBundledFareFreeServices()));

				segmentLogicalCabinClasses.add(logicalCabinClassInfoTO);
				ondWiseAvailability.get(seatAvailability.getJourneyOndCode()).add(logicalCabinClassInfoTO);
			}

			for (String ondCode : ondWiseAvailability.keySet()) {
				Collections.sort(ondWiseAvailability.get(ondCode));
				priceInfoTO.addAvailableLogicalCC(ondCode, ondSeatAvail.getOndSequence(), ondWiseAvailability.get(ondCode), null);
			}

			if (priceInfoTO.getFareTypeTO() != null) {
				if (ondwiseFlexiSelected != null && !ondwiseFlexiSelected.isEmpty()
						&& ondwiseFlexiSelected.get(ondSeatAvail.getOndSequence()) != null) {
					ondFlexSelected = ondwiseFlexiSelected.get(ondSeatAvail.getOndSequence());
				}
				priceInfoTO.getFareTypeTO().getOndFlexiSelection().put(ondSeatAvail.getOndSequence(), ondFlexSelected);
			}

			LogicalCabinClassInfoTO selectedLogicalCabinClass = getSelectedLogicalCabinClass(segmentLogicalCabinClasses);
			if(selectedLogicalCabinClass != null) {
				populateBundleFareDelta(selectedLogicalCabinClass, segmentLogicalCabinClasses);
			}
		}

	}


	private static void populateBundleFareDelta(LogicalCabinClassInfoTO selectedLogicalCabinClass,
												List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses) {

		for (LogicalCabinClassInfoTO logicalCabinClass : segmentLogicalCabinClasses) {
			if(!logicalCabinClass.isSelected()) {

				if(logicalCabinClass.getBundledFareFee() != null && selectedLogicalCabinClass.getBundledFareFee() != null) {

					BigDecimal bundleFareDelta = AccelAeroCalculator
							.subtract(logicalCabinClass.getBundledFareFee(), selectedLogicalCabinClass.getBundledFareFee());
					logicalCabinClass.setBundledFareDelta(bundleFareDelta);

				} else if(logicalCabinClass.getBundledFareFee() != null && selectedLogicalCabinClass.getBundledFareFee() == null) {

					logicalCabinClass.setBundledFareDelta(logicalCabinClass.getBundledFareFee());

				} else if(logicalCabinClass.getBundledFareFee() == null && selectedLogicalCabinClass.getBundledFareFee() != null) {

					BigDecimal bundleFareDelta = AccelAeroCalculator
							.subtract(AccelAeroCalculator.getDefaultBigDecimalZero(), selectedLogicalCabinClass.getBundledFareFee());
					logicalCabinClass.setBundledFareDelta(bundleFareDelta);
				}
			}
		}
	}

	private static LogicalCabinClassInfoTO getSelectedLogicalCabinClass(List<LogicalCabinClassInfoTO> segmentLogicalCabinClasses) {

		LogicalCabinClassInfoTO selectedLogicalCabinClass = null;
		for (LogicalCabinClassInfoTO logicalCabinClassInfoTO: segmentLogicalCabinClasses) {
			if(logicalCabinClassInfoTO.isSelected()){
				selectedLogicalCabinClass = logicalCabinClassInfoTO;
				break;
			}
		}
		return selectedLogicalCabinClass;
	}

	/**
	 * @param logicalCabinClassInfoTOs
	 * @param logicalCabinCalss
	 * @param withFlexi
	 * @param bundledFarePeriodId
	 * @return
	 */
	private static boolean containsSeatAvailability(List<LogicalCabinClassInfoTO> logicalCabinClassInfoTOs,
			String logicalCabinCalss, boolean withFlexi, Integer bundledFarePeriodId) {

		for (LogicalCabinClassInfoTO logiCabinClassInfoTO : logicalCabinClassInfoTOs) {
			if (logiCabinClassInfoTO.getLogicalCCCode().equals(logicalCabinCalss)
					&& logiCabinClassInfoTO.isWithFlexi() == withFlexi
					&& isBundledFareMatched(logiCabinClassInfoTO.getBundledFarePeriodId(), bundledFarePeriodId)) {
				return true;
			}
		}
		return false;
	}

	private static boolean isBundledFareMatched(Integer existingBundledFarePeriodId, Integer newBundledFarePeriodId) {

		boolean bundledFareMatched = true;

		if (existingBundledFarePeriodId != null && newBundledFarePeriodId != null) {
			if (!(existingBundledFarePeriodId.equals(newBundledFarePeriodId))) {
				bundledFareMatched = false;
			}
		} else if ((existingBundledFarePeriodId == null && newBundledFarePeriodId != null)
				|| (existingBundledFarePeriodId != null && newBundledFarePeriodId == null)) {
			bundledFareMatched = false;
		}

		return bundledFareMatched;
	}

	private static FareSegChargeTO createFareSegCharges(FareSegmentChargeTO fareSegmentChargeTO, boolean isOHDRestricted) {
		FareSegChargeTO fareSegChargeTO = createFareSegCharges(fareSegmentChargeTO);
		if (fareSegChargeTO != null) {
			fareSegChargeTO.setAllowOnhold(!isOHDRestricted);
		}
		return fareSegChargeTO;
	}

	private static FareSegChargeTO createFareSegCharges(FareSegmentChargeTO fareSegmentChargeTO) {
		if (fareSegmentChargeTO == null) {
			return null;
		}
		FareSegChargeTO fscTO = new FareSegChargeTO();

		List<OndFareSegChargeTO> ondFareSegChargeTOs = new ArrayList<OndFareSegChargeTO>();

		boolean isOutboundOndAvailable = false;
		boolean isInboundOndAvailable = false;

		for (OndFareSegmentChargeTO ondFSCIn : fareSegmentChargeTO.getOndFareSegmentChargeTOs()) {
			OndFareSegChargeTO ondFSCOut = new OndFareSegChargeTO();
			ondFSCOut.setFareID(ondFSCIn.getFareID());
			ondFSCOut.setFarePercentage(ondFSCIn.getFarePercentage());
			ondFSCOut.setFareType(ondFSCIn.getFareTypeId());
			ondFSCOut.setFareBasisCode(ondFSCIn.getFareBasisCode());
			ondFSCOut.setInbound(ondFSCIn.isIsInbound());
			ondFSCOut.setModifyInbound(ondFSCIn.isIsModifyInbound());
			ondFSCOut.setOpenReturn(ondFSCIn.isIsOpenReturn());
			ondFSCOut.setBusSegmentExists(ondFSCIn.isBusSegmentExists());

			if (ondFSCIn.isIsInbound()) {
				isInboundOndAvailable = true;
			} else {
				isOutboundOndAvailable = true;
			}

			if (ondFSCIn.isIsInbound()) {
				isInboundOndAvailable = true;
			} else {
				isOutboundOndAvailable = true;
			}

			ondFSCOut.setChargeRateIds(ondFSCIn.getChargeRateIds());
			LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc = new LinkedHashMap<Integer, LinkedList<BookingClassAlloc>>();

			for (FlightSegmentBCAllocation bcAllocIn : ondFSCIn.getFsegBCAlloc()) {

				LinkedList<BookingClassAlloc> bcAllocList = new LinkedList<BookingClassAlloc>();
				for (BookingClassAllocation bcAIn : bcAllocIn.getBcAllocation()) {
					BookingClassAlloc bcA = new BookingClassAlloc(bcAIn.getBcCode(), bcAllocIn.getCabinClass(),
							bcAllocIn.getLogicalCabinClass(), bcAIn.getNoOfAdults(), null, false, false, false, false, false,
							false);
					bcAllocList.add(bcA);
				}

				if (!fsegBCAlloc.containsKey(bcAllocIn.getFlightSegId())) {
					fsegBCAlloc.put(bcAllocIn.getFlightSegId(), bcAllocList);
				} else {
					fsegBCAlloc.get(bcAllocIn.getFlightSegId()).addAll(bcAllocList);
				}
			}

			ondFSCOut.setFsegBCAlloc(fsegBCAlloc);
			ondFSCOut.setFlexiChargeIds(ondFSCIn.getFlexiChargeIds());
			ondFareSegChargeTOs.add(ondFSCOut);
		}

		if (isOutboundOndAvailable && isInboundOndAvailable) {
			for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
				if (ondFareSegChargeTO.isInbound() && (ondFareSegChargeTO.getOndSequence() != OndSequence.IN_BOUND)) {
					ondFareSegChargeTO.setOndSequence(OndSequence.IN_BOUND);
				}
			}
		}

		Collections.sort(ondFareSegChargeTOs);
		fscTO.setOndFareSegChargeTOs(ondFareSegChargeTOs);

		Map<String, Double> totalJourneyFare = new HashMap<String, Double>();
		for (PerPaxJourneyFare ppjFare : fareSegmentChargeTO.getPerPaxTotalJourneyFare()) {
			totalJourneyFare.put(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(ppjFare.getPaxType()), ppjFare.getFareAmount());
		}

		fscTO.setTotalJourneyFare(totalJourneyFare);
		return fscTO;
	}

	private static FareTypeTO createFareType(FareType fareType) {

		FareTypeTO fareTypeTO = new FareTypeTO();
		fareTypeTO.setTotalBaseFare(fareType.getTotalFare());
		fareTypeTO.setTotalTaxes(fareType.getTotalTaxes());
		fareTypeTO.setTotalSurcharges(fareType.getTotalSurcharges());
		fareTypeTO.setTotalFees(fareType.getTotalFees());
		fareTypeTO.setTotalPrice(fareType.getTotalPrice());
		fareTypeTO.setTotalPriceWithInfant(fareType.getTotalPriceWithInfant());
		fareTypeTO.setBaseFares(createBaseFareTOList(fareType.getBaseFare()));
		fareTypeTO.setTaxes(createTaxTOList(fareType.getTaxes()));
		fareTypeTO.setSurcharges(createSurchargeTOList(fareType.getSurcharges()));
		fareTypeTO.setFees(createFeeTOList(fareType.getFees()));

		if (fareType.getOndExternalCharges() != null && fareType.getOndExternalCharges().size() > 0) {
			for (ONDExternalCharge ondExternalCharge : fareType.getOndExternalCharges()) {
				fareTypeTO.addOndExternalCharge(ondExternalCharge.getOndSequence()).addExternalCharges(
						createExternalChargeTOList(ondExternalCharge.getExternalCharges(), ondExternalCharge.getOndSequence()));
			}
		}

		return fareTypeTO;
	}

	private static List<ExternalChargeTO> createExternalChargeTOList(List<ExternalCharge> extChargeList, int ondSequence) {
		List<ExternalChargeTO> extChargeTOList = null;
		if (extChargeList != null) {
			extChargeTOList = new ArrayList<ExternalChargeTO>();
			for (ExternalCharge extCharge : extChargeList) {
				ExternalChargeTO extChargeTO = new ExternalChargeTO();
				extChargeTO.setAmount(extCharge.getAmount());
				extChargeTO.setCarrierCode(extCharge.getCarrierCode());
				extChargeTO.setSegmentCode(extCharge.getSegmentCode());
				extChargeTO.setSurchargeCode(extCharge.getCode());
				extChargeTO.setSurchargeName(extCharge.getName());
				extChargeTO.setApplicableTime(extCharge.getApplicableDateTime());
				for (PassengerType paxType : extCharge.getApplicablePassengerType()) {
					extChargeTO.getApplicablePassengerTypeCode().add(LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(paxType));
				}
				Collection<FlexiInfoTO> flexiInfoTOs = new ArrayList<FlexiInfoTO>();
				for (FlexiInfo flexiInfo : extCharge.getAdditionalDetails()) {
					FlexiInfoTO flexiInfoTO = new FlexiInfoTO();
					flexiInfoTO.setFlexiRateId(flexiInfo.getFlexiRateId());
					flexiInfoTO.setAvailableCount(flexiInfo.getAvailableCount());
					flexiInfoTO.setCutOverBufferInMins(flexiInfo.getCutOverBufferInMins());
					flexiInfoTO.setFlexibilityTypeId(flexiInfo.getFlexibilityTypeId());
					flexiInfoTOs.add(flexiInfoTO);
				}
				extChargeTO.setAdditionalDetails(flexiInfoTOs);
				extChargeTO.setComments(null);
				extChargeTO.setOndSequence(ondSequence);
				extChargeTO.setType(getExternalChargeType(extCharge.getType()));
				extChargeTOList.add(extChargeTO);
			}
		}
		return extChargeTOList;
	}

	private static List<FeeTO> createFeeTOList(List<Fee> feeList) {

		List<FeeTO> feeTOList = null;
		if (feeList != null) {
			feeTOList = new ArrayList<FeeTO>();
			for (Fee fee : feeList) {
				FeeTO feeTO = new FeeTO();
				feeTO.setAmount(fee.getAmount());
				feeTO.setCarrierCode(fee.getCarrierCode());
				feeTO.setSegmentCode(fee.getSegmentCode());
				feeTO.setFeeCode(fee.getFeeCode());
				if (fee.getApplicablePassengerType() != null) {
					for (PassengerType passengerType : fee.getApplicablePassengerType()) {
						feeTO.getApplicablePassengerTypeCode().add(
								LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(passengerType));
					}
				}
				feeTO.setOndSequence(fee.getOndSequence());
				feeTOList.add(feeTO);
			}
		}
		return feeTOList;
	}

	private static List<SurchargeTO> createSurchargeTOList(List<Surcharge> surchargeList) {

		List<SurchargeTO> surchargeTOList = null;
		if (surchargeList != null) {
			surchargeTOList = new ArrayList<SurchargeTO>();
			for (Surcharge surcharge : surchargeList) {
				SurchargeTO surchargeTO = new SurchargeTO();
				surchargeTO.setAmount(surcharge.getAmount());
				surchargeTO.setCarrierCode(surcharge.getCarrierCode());
				surchargeTO.setSegmentCode(surcharge.getSegmentCode());
				surchargeTO.setSurchargeCode(surcharge.getSurchargeCode());
				surchargeTO.setSurchargeName(surcharge.getSurchargeName());
				if (surcharge.getApplicablePassengerType() != null) {
					for (PassengerType passengerType : surcharge.getApplicablePassengerType()) {
						surchargeTO.getApplicablePassengerTypeCode().add(
								LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(passengerType));
					}
				}
				surchargeTO.setOndSequence(surcharge.getOndSequence());
				surchargeTOList.add(surchargeTO);
			}
		}
		return surchargeTOList;
	}

	private static List<TaxTO> createTaxTOList(List<Tax> taxList) {

		List<TaxTO> taxTOList = null;
		if (taxList != null) {
			taxTOList = new ArrayList<TaxTO>();
			for (Tax tax : taxList) {
				TaxTO taxTO = new TaxTO();
				taxTO.setAmount(tax.getAmount());
				taxTO.setCarrierCode(tax.getCarrierCode());
				taxTO.setSegmentCode(tax.getSegmentCode());
				taxTO.setTaxName(tax.getTaxName());
				taxTO.setTaxCode(tax.getTaxCode());
				if (tax.getApplicablePassengerType() != null) {
					for (PassengerType passengerType : tax.getApplicablePassengerType()) {
						taxTO.getApplicablePassengerTypeCode().add(
								LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(passengerType));
					}
				}
				taxTO.setOndSequence(tax.getOndSequence());
				taxTOList.add(taxTO);
			}
		}
		return taxTOList;
	}

	private static List<BaseFareTO> createBaseFareTOList(List<BaseFare> baseFares) {

		List<BaseFareTO> baseFareTOList = null;
		if (baseFares != null) {
			baseFareTOList = new ArrayList<BaseFareTO>();
			for (BaseFare baseFare : baseFares) {
				BaseFareTO baseFareTO = new BaseFareTO();
				baseFareTO.setAmount(baseFare.getAmount());
				baseFareTO.setCarrierCode(baseFare.getCarrierCode());
				baseFareTO.setOndSequence(baseFare.getOndSequence());
				baseFareTO.setSegmentCode(baseFare.getSegmentCode());
				if (baseFare.getApplicablePassengerType() != null) {
					for (PassengerType passengerType : baseFare.getApplicablePassengerType()) {
						baseFareTO.getApplicablePassengerTypeCode().add(
								LCCClientCommonUtils.convertLCCPaxCodeToAAPaxCodes(passengerType));
					}
				}
				baseFareTOList.add(baseFareTO);
				;
			}
		}
		return baseFareTOList;
	}

	private static List<FareRuleDTO> getFareRuleDTOList(List<FareRule> fareRules) {

		List<FareRuleDTO> fareRuleDTOs = new ArrayList<FareRuleDTO>();
		if (fareRules != null) {
			for (FareRule fareRule : fareRules) {
				FareRuleDTO fareRuleDTO = new FareRuleDTO();

				fareRuleDTO.setComments(fareRule.getComments());
				fareRuleDTO.setOrignNDest(fareRule.getSegmentCode());
				fareRuleDTO.setOndSequence(fareRule.getOndSequence());
				fareRuleDTO.setDescription(fareRule.getDescription());
				fareRuleDTO.setFareRuleCode(fareRule.getFareRuleCode());
				fareRuleDTO.setFareBasisCode(fareRule.getFareBasisCode());
				fareRuleDTO.setFareCategoryCode(fareRule.getFareCategoryCode());
				fareRuleDTO.setCabinClassCode(fareRule.getCabinClassCode());
				fareRuleDTO.setBookingClassCode(fareRule.getBookingClassCode());
				fareRuleDTO.setFareRuleId(fareRule.getFareRuleId());

				if (fareRule.isInterlineFare()) {
					fareRuleDTO.setOrignNDest(fareRule.getInterlineSegmentCode());
				}

				if (fareRule.getAdultFareCharacteristics() != null) {
					fareRuleDTO.setAdultFareAmount(formatAsDecimal(fareRule.getAdultFareCharacteristics().getFareAmount()));
					fareRuleDTO.setAdultFareApplicable(fareRule.getAdultFareCharacteristics().isApplicable());
					fareRuleDTO.setAdultFareRefundable(fareRule.getAdultFareCharacteristics().isRefundable());
				}

				if (fareRule.getChildFareCharacteristics() != null) {
					fareRuleDTO.setChildFareType(fareRule.getChildFareCharacteristics().getFareType());
					fareRuleDTO.setChildFareAmount(formatAsDecimal(fareRule.getChildFareCharacteristics().getFareAmount()));
					fareRuleDTO.setChildFareApplicable(fareRule.getChildFareCharacteristics().isApplicable());
					fareRuleDTO.setChildFareRefundable(fareRule.getChildFareCharacteristics().isRefundable());
				}

				if (fareRule.getInfantFareCharacteristics() != null) {
					fareRuleDTO.setInfantFareType(fareRule.getInfantFareCharacteristics().getFareType());
					fareRuleDTO.setInfantFareAmount(formatAsDecimal(fareRule.getInfantFareCharacteristics().getFareAmount()));
					fareRuleDTO.setInfantFareApplicable(fareRule.getInfantFareCharacteristics().isApplicable());
					fareRuleDTO.setInfantFareRefundable(fareRule.getInfantFareCharacteristics().isRefundable());
				}

				fareRuleDTOs.add(fareRuleDTO);
			}

		}
		return fareRuleDTOs;
	}

	public static EXTERNAL_CHARGES getExternalChargeType(ExternalChargeType lccExternalCharges) {
		if (lccExternalCharges == ExternalChargeType.CREDIT_CARD) {
			return EXTERNAL_CHARGES.CREDIT_CARD;
		} else if (lccExternalCharges == ExternalChargeType.HANDLING_CHARGE) {
			return EXTERNAL_CHARGES.HANDLING_CHARGE;
		} else if (lccExternalCharges == ExternalChargeType.SEAT_MAP) {
			return EXTERNAL_CHARGES.SEAT_MAP;
		} else if (lccExternalCharges == ExternalChargeType.MEAL) {
			return EXTERNAL_CHARGES.MEAL;
		} else if (lccExternalCharges == ExternalChargeType.INSURANCE) {
			return EXTERNAL_CHARGES.INSURANCE;
		} else if (lccExternalCharges == ExternalChargeType.INFLIGHT_SERVICES) {
			return EXTERNAL_CHARGES.INFLIGHT_SERVICES;
		} else if (lccExternalCharges == ExternalChargeType.FLEXI_CHARGES) {
			return EXTERNAL_CHARGES.FLEXI_CHARGES;
		} else if (lccExternalCharges == ExternalChargeType.AIRPORT_SERVICE) {
			return EXTERNAL_CHARGES.AIRPORT_SERVICE;
		} else if (lccExternalCharges == ExternalChargeType.PROMOTION_REWARD) {
			return EXTERNAL_CHARGES.PROMOTION_REWARD;
		} else if (lccExternalCharges == ExternalChargeType.ADDITONAL_SEAT_CHARGE) {
			return EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE;
		} else if (lccExternalCharges == ExternalChargeType.BAGGAGE) {
			return EXTERNAL_CHARGES.BAGGAGE;
		} else if (lccExternalCharges == ExternalChargeType.JN_ANCI) {
			return EXTERNAL_CHARGES.JN_ANCI;
		} else if (lccExternalCharges == ExternalChargeType.JN_OTHER) {
			return EXTERNAL_CHARGES.JN_OTHER;
		} else if (lccExternalCharges == ExternalChargeType.BSP_FEE) {
			return EXTERNAL_CHARGES.BSP_FEE;
		}

		return null;
	}

	public static Map<Integer, List<FlightSegmentTO>> getOBIBFlightSegements(
			List<OriginDestinationInformationTO> ondOptionResultList) {
		Map<Integer, List<FlightSegmentTO>> flightMap = new HashMap<Integer, List<FlightSegmentTO>>();

		for (OriginDestinationInformationTO originDestinationInformationTO : ondOptionResultList) {

			List<OriginDestinationOptionTO> orignDestinationOptions = originDestinationInformationTO.getOrignDestinationOptions();

			if (orignDestinationOptions != null && orignDestinationOptions.size() > 0) {
				int ondSequence = OndSequence.OUT_BOUND;
				if (originDestinationInformationTO.isReturnFlag()) {
					ondSequence = OndSequence.IN_BOUND;
				}

				if (!flightMap.containsKey(ondSequence)) {
					flightMap.put(ondSequence, new ArrayList<FlightSegmentTO>());
				}

				for (OriginDestinationOptionTO originDestinationOptionTO : orignDestinationOptions) {
					if (originDestinationOptionTO.getFlightSegmentList() != null) {
						flightMap.get(ondSequence).addAll(originDestinationOptionTO.getFlightSegmentList());
					}
				}
			}

		}

		return flightMap;
	}

	public static boolean isSelectedFlightAvailabilitySearch(List<FlightSegmentTO> flightSegments,
			Map<String, List<Integer>> logicalCabinClassSelection) {
		boolean isAvailSearch = true;

		if (logicalCabinClassSelection != null) {
			for (FlightSegmentTO flightSegmentTO : flightSegments) {
				Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentTO.getFlightRefNumber());
				String logicalCC = FareQuoteUtil.getClassOfserviceFromFlightSegId(logicalCabinClassSelection, flightSegId);
				if (logicalCC != null) {
					isAvailSearch = false;
					break;
				}
			}
		}

		return isAvailSearch;
	}

	private static BigDecimal formatAsDecimal(BigDecimal in) {
		return new BigDecimal(AccelAeroCalculator.formatAsDecimal(in));
	}
}
