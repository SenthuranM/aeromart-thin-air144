package com.isa.thinair.lccclient.core.bl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAgent;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCRole;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCUser;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCError;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCUserInfoPublishRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCUserInfoPublishRS;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRSDataTO;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.RoleAccessibility;
import com.isa.thinair.airsecurity.api.model.RoleVisibility;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.lccclient.core.util.LCConnectorUtils;

public class LCCUserInfoPublisherBL {
	private UserInfoPublishRQDataTO userInfoPublishRQDataTO;

	public LCCUserInfoPublisherBL(UserInfoPublishRQDataTO userInfoPublishRQDataTO) {
		this.userInfoPublishRQDataTO = userInfoPublishRQDataTO;
	}

	public UserInfoPublishRSDataTO execute() throws ModuleException {
		LCCUserInfoPublishRQ lccUserInfoPublishRQ = this.createLCCUserInfoPublishRQ();
		LCCUserInfoPublishRS lccUserInfoPublishRS = LCConnectorUtils.getMaxicoExposedWS().publishUserInfo(lccUserInfoPublishRQ);
		return this.setUserInfoPublishRSDataTO(lccUserInfoPublishRS);
	}

	private UserInfoPublishRSDataTO setUserInfoPublishRSDataTO(LCCUserInfoPublishRS lccUserInfoPublishRS) {
		UserInfoPublishRSDataTO userInfoPublishRSDataTO = new UserInfoPublishRSDataTO();
		userInfoPublishRSDataTO.getUpdatedAgentCodes().addAll(lccUserInfoPublishRS.getUpdatedAgentCodes());
		userInfoPublishRSDataTO.getUpdatedRoleIds().addAll(lccUserInfoPublishRS.getUpdatedRoleIds());
		userInfoPublishRSDataTO.getUpdatedUserIds().addAll(lccUserInfoPublishRS.getUpdatedUserIds());
		if (lccUserInfoPublishRS.getResponseAttributes().getErrors() != null) {
			for (Iterator<LCCError> iterator = lccUserInfoPublishRS.getResponseAttributes().getErrors().iterator(); iterator
					.hasNext();) {
				LCCError lccError = iterator.next();
				userInfoPublishRSDataTO.getErrors().add(lccError.getErrorCode() + " [" + lccError.getDescription() + " ]");
			}
		}
		return userInfoPublishRSDataTO;
	}

	private LCCUserInfoPublishRQ createLCCUserInfoPublishRQ() {
		LCCUserInfoPublishRQ lccUserInfoPublishRQ = new LCCUserInfoPublishRQ();
		lccUserInfoPublishRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccUserInfoPublishRQ.setLccPos(LCCClientCommonUtils.createPOS(null));

		if (userInfoPublishRQDataTO.getPublishUsers() != null && userInfoPublishRQDataTO.getPublishUsers().size() > 0) {
			createLCCPublishUsers(lccUserInfoPublishRQ);
		}

		if (userInfoPublishRQDataTO.getPublishRoles() != null && userInfoPublishRQDataTO.getPublishRoles().size() > 0) {
			createLCCPublishRoles(lccUserInfoPublishRQ);
		}

		if (userInfoPublishRQDataTO.getPublishAgents() != null && userInfoPublishRQDataTO.getPublishAgents().size() > 0) {
			createLCCPublishAgents(lccUserInfoPublishRQ);
		}

		return lccUserInfoPublishRQ;
	}

	private void createLCCPublishAgents(LCCUserInfoPublishRQ lccUserInfoPublishRQ) {
		List<LCCAgent> colLCCAgents = new ArrayList<LCCAgent>();
		Iterator<Agent> agentIte = userInfoPublishRQDataTO.getPublishAgents().iterator();

		String airlineAgentType = AppSysParamsUtil.getCarrierAgent();

		while (agentIte.hasNext()) {
			Agent airAgent = agentIte.next();
			LCCAgent lccAgent = new LCCAgent();
			lccAgent.setAgentCode(airAgent.getAgentCode());
			if (airAgent.getAgentTypeCode().equals(airlineAgentType)) {
				lccAgent.setAgentTypeCode(Util.AIRLINE_AGENT_TYPE);
			} else {
				lccAgent.setAgentTypeCode(airAgent.getAgentTypeCode());
			}
			lccAgent.setAgentName(airAgent.getAgentName());
			lccAgent.setStationCode(airAgent.getStationCode());
			lccAgent.setTerritoryCode(airAgent.getTerritoryCode());
			lccAgent.setTelephone(airAgent.getTelephone());
			lccAgent.setFax(airAgent.getFax());
			lccAgent.setEmailId(airAgent.getEmailId());
			lccAgent.setAgentIATANumber(airAgent.getAgentIATANumber());
			lccAgent.setStatus(airAgent.getStatus());
			lccAgent.setCreatedBy(airAgent.getCreatedBy());
			lccAgent.setCreatedDate(airAgent.getCreatedDate());
			lccAgent.setModifiedBy(airAgent.getModifiedBy());
			lccAgent.setModifiedDate(airAgent.getModifiedDate());
			lccAgent.setVersion(airAgent.getVersion());
			lccAgent.setReportToGSA(airAgent.getReportingToGSA());
			lccAgent.setBillingEmail(airAgent.getBillingEmail());
			lccAgent.setCurrencyCode(airAgent.getCurrencyCode());
			lccAgent.setNotes(airAgent.getNotes());
			lccAgent.setSpecifiedIn(airAgent.getCurrencySpecifiedIn());
			lccAgent.setAutoInvoice(airAgent.getAutoInvoice());
			lccAgent.setLanguageCode(airAgent.getLanguageCode());
			lccAgent.setOnhold(airAgent.getOnHold());
			lccAgent.setServiceChannelCode(airAgent.getServiceChannel());
			lccAgent.setCapturePayRef(airAgent.getCapturePaymentRef());
			lccAgent.setPrefRPTFormat(airAgent.getPrefferedRptFormat());
			lccAgent.setPayRefMandatory(airAgent.getPayRefMandatory());
			lccAgent.setWebsiteVisibility(airAgent.getPublishToWeb());
			lccAgent.setMobile(airAgent.getMobile());
			lccAgent.setShowPriceInSelCur(airAgent.getEnableWSMultiCurrency());
			lccAgent.setAirlineCode(airAgent.getAirlineCode());
			lccAgent.setCharterAgent(airAgent.getCharterAgent());
			lccAgent.setGsaCode(airAgent.getGsaCode());
			lccAgent.setFareMask(airAgent.getAgentWiseFareMaskEnable());
			lccAgent.setHandlingChgBasis(airAgent.getHandlingFeeChargeBasisCode());
			lccAgent.setHandlingChgAppliesTo(airAgent.getHandlingFeeAppliesTo());
			lccAgent.setEnableHandlingChg(airAgent.getHandlingFeeEnable());
			lccAgent.setEnableAgentWiseTicket(airAgent.getAgentWiseTicketEnable());
			lccAgent.setHasCreditLimit(airAgent.getHasCreditLimit());
			lccAgent.setHandlingChgAppliesToMod(airAgent.getHandlingFeeModificationApllieTo());
			lccAgent.setEnableHandlingChgForMod(airAgent.getHandlingFeeModificationEnabled());

			List<String> agentPayMethods = new ArrayList<String>();
			for (String payMethod : airAgent.getPaymentMethod()) {
				agentPayMethods.add(payMethod);
			}
			lccAgent.getPaymentCodes().addAll(agentPayMethods);

			colLCCAgents.add(lccAgent);
		}

		lccUserInfoPublishRQ.getAgentList().addAll(colLCCAgents);
	}

	private void createLCCPublishRoles(LCCUserInfoPublishRQ lccUserInfoPublishRQ) {
		List<LCCRole> colLCCRoles = new ArrayList<LCCRole>();
		Iterator<Role> roleIte = userInfoPublishRQDataTO.getPublishRoles().iterator();

		String airlineAgentType = AppSysParamsUtil.getCarrierAgent();

		while (roleIte.hasNext()) {
			Role airRole = roleIte.next();
			LCCRole lccRole = new LCCRole();
			lccRole.setRoleId(airRole.getRoleId());
			lccRole.setRoleName(airRole.getRoleName());
			lccRole.setRemarks(airRole.getRemarks());
			lccRole.setCreatedBy(airRole.getCreatedBy());
			lccRole.setCreatedDate(airRole.getCreatedDate());
			lccRole.setModifiedBy(airRole.getModifiedBy());
			lccRole.setModifiedDate(airRole.getModifiedDate());
			lccRole.setVersion(airRole.getVersion());
			lccRole.setStatus(airRole.getStatus());
			lccRole.setServiceChannelCode(airRole.getServiceChannel());
			lccRole.setAirlineCode(airRole.getAirlineCode());
			lccRole.setIncludeExclude(airRole.getIncludeExcludeFlag());

			List<String> visibleAgentTypes = new ArrayList<String>();
			for (RoleVisibility roleVisibility : airRole.getAgentTypeVisibilities()) {
				if (roleVisibility.getAgentTypeCode().equals(airlineAgentType)) {
					visibleAgentTypes.add(Util.AIRLINE_AGENT_TYPE);
				} else {
					visibleAgentTypes.add(roleVisibility.getAgentTypeCode());
				}
			}
			lccRole.getVisibleAgentTypeCodes().addAll(visibleAgentTypes);

			List<String> assignableAgentTypes = new ArrayList<String>();
			for (RoleAccessibility roleAccessibility : airRole.getAgentTypeAccessiblity()) {
				if (roleAccessibility.getAgentTypeCode().equals(airlineAgentType)) {
					assignableAgentTypes.add(Util.AIRLINE_AGENT_TYPE);
				} else {
					assignableAgentTypes.add(roleAccessibility.getAgentTypeCode());
				}
			}
			lccRole.getAssignableAgentTypeCodes().addAll(assignableAgentTypes);

			List<String> rolePrivileges = new ArrayList<String>();
			for (Privilege rolePriv : airRole.getPrivileges()) {
				rolePrivileges.add(rolePriv.getPrivilegeId());
			}
			lccRole.getPrivilegeIds().addAll(rolePrivileges);

			colLCCRoles.add(lccRole);
		}
		lccUserInfoPublishRQ.getRoleList().addAll(colLCCRoles);
	}

	private void createLCCPublishUsers(LCCUserInfoPublishRQ lccUserInfoPublishRQ) {
		List<LCCUser> colLCCUsers = new ArrayList<LCCUser>();
		Iterator<User> userIte = userInfoPublishRQDataTO.getPublishUsers().iterator();

		while (userIte.hasNext()) {
			User airUser = userIte.next();
			LCCUser lccUser = new LCCUser();
			lccUser.setUserId(airUser.getUserId());
			lccUser.setPassword(airUser.getPassword());
			lccUser.setDisplayName(airUser.getDisplayName());
			lccUser.setEmailId(airUser.getEmailId());
			lccUser.setStatus(airUser.getStatus());
			lccUser.setCreatedBy(airUser.getCreatedBy());
			lccUser.setCreatedDate(airUser.getCreatedDate());
			lccUser.setModifiedBy(airUser.getModifiedBy());
			lccUser.setModifiedDate(airUser.getModifiedDate());
			lccUser.setVersion(airUser.getVersion());
			lccUser.setAgentCode(airUser.getAgentCode());
			lccUser.setDefaultCarrierCode(airUser.getDefaultCarrierCode());
			lccUser.setAdminUserCreateStatus(airUser.getAdminUserCreateStatus());
			lccUser.setNormalUserCreateStatus(airUser.getNormalUserCreateStatus());
			lccUser.setServiceChannelCode(airUser.getServiceChannel());
			lccUser.setPasswordResetted(airUser.getPasswordReseted());
			lccUser.setAirlineCode(airUser.getAirlineCode());
			lccUser.setMyIdCarrierCode(airUser.getMyIDCarrierCode());

			List<String> lccRoleIds = new ArrayList<String>();
			for (Role role : airUser.getRoles()) {
				lccRoleIds.add(role.getRoleId());
			}
			lccUser.getRoleIds().addAll(lccRoleIds);

			if (airUser.getCarriers() != null) {
				lccUser.getCarrierCodes().addAll(airUser.getCarriers());
			}

			colLCCUsers.add(lccUser);
		}
		lccUserInfoPublishRQ.getUserList().addAll(colLCCUsers);
	}
}
