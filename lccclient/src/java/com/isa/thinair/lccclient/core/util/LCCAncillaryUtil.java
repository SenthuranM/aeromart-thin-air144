package com.isa.thinair.lccclient.core.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AppIndicator;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingBaggageRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingMealRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSpecialRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAncillaryAvailabilityStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationBaggageParams;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationSummaryLite;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapCabinClass;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapColumnGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapRow;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapRowGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeatMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAnciTaxApplicabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryAvailabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryAvailabilityRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCAncillaryRQ;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirColumnGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCCabinClassDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCPassengerTypeQuantityDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDetailDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.lccclient.core.bl.LCCClientCommonUtils;

public class LCCAncillaryUtil {

	public static List<LCCAncillaryAvailabilityOutDTO> populateGetAncillaryAvailabilityOutDTO(
			LCCAncillaryAvailabilityRS ancillaryAvailabilityRS) {

		List<LCCAncillaryAvailabilityOutDTO> outAncillaryAvailabilityDTOList = new ArrayList<LCCAncillaryAvailabilityOutDTO>();

		List<FlightSegmentAncillaryAvailabilityStatus> ancillaryAvailabilityStatuses = ancillaryAvailabilityRS
				.getAncillaryAvailabilityStatus();
		for (FlightSegmentAncillaryAvailabilityStatus ancillaryAvailabilityStatus : ancillaryAvailabilityStatuses) {
			BookingFlightSegment flightSegment = ancillaryAvailabilityStatus.getFlightSegments();

			FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
			flightSegmentTO.setOperatingAirline(flightSegment.getOperatingAirline());
			flightSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
			flightSegmentTO.setFlightRefNumber(flightSegment.getFlightRefNumber());
			flightSegmentTO.setDepartureDateTime(flightSegment.getDepatureDateTime());
			flightSegmentTO.setDepartureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
			flightSegmentTO.setFlightNumber(flightSegment.getFlightNumber());
			flightSegmentTO.setArrivalDateTime(flightSegment.getArrivalDateTime());
			flightSegmentTO.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
			flightSegmentTO.setOperationType(flightSegment.getOperationType());
			flightSegmentTO.setAirportCode(flightSegment.getAirportCode());
			flightSegmentTO.setSsrInventoryCheck(flightSegment.isSsrInventoryCheck());
			flightSegmentTO.setReturnFlag(flightSegment.getReturnFlag().equals("Y") ? true : false);
			flightSegmentTO.setPnrSegId(flightSegment.getBookingFlightRefNumber());
			flightSegmentTO.setOndSequence(flightSegment.getOndSequence());
			flightSegmentTO.setCabinClassCode(flightSegment.getCabinClassCode());
			flightSegmentTO.setLogicalCabinClassCode(flightSegment.getLogicalCabinClassCode());
			flightSegmentTO
					.setSurfaceSegment(flightSegment.isSurfaceSegment() != null ? flightSegment.isSurfaceSegment() : false);

			Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
			String logicalCabinClass = flightSegment.getLogicalCabinClassCode();
			if (logicalCabinClass != null && !"".equals(logicalCabinClass)) {
				LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(logicalCabinClass);
				flightSegmentTO.setFreeSeatEnabled(logicalCabinClassDTO.isFreeSeatEnabled());
				flightSegmentTO.setFreeMealEnabled(logicalCabinClassDTO.isAllowSingleMealOnly());
			}

			LCCAncillaryAvailabilityOutDTO outAncillaryAvailabilityDTO = new LCCAncillaryAvailabilityOutDTO();
			outAncillaryAvailabilityDTO.setFlightSegmentTO(flightSegmentTO);

			for (AncillaryAvailability ancillaryAvailability : ancillaryAvailabilityStatus.getAncillaryAvailability()) {

				LCCAncillaryStatus ancillaryStatus = new LCCAncillaryStatus();
				ancillaryStatus.setAvailable(ancillaryAvailability.isAvailability() && ancillaryAvailability.isEnabled());
				ancillaryStatus.setAncillaryType(getLCCAncillaryType(ancillaryAvailability.getAncillary()));
				outAncillaryAvailabilityDTO.addAncillaryStatus(ancillaryStatus);
			}
			outAncillaryAvailabilityDTOList.add(outAncillaryAvailabilityDTO);
		}

		return outAncillaryAvailabilityDTOList;
	}

	public static LCCAncillaryAvailabilityRQ populateLCCAncillaryAvailabilityRQ(
			LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException {
		LCCAncillaryAvailabilityRQ ancillaryAvailabilityRQ = new LCCAncillaryAvailabilityRQ();

		String txnId = lccAncillaryAvailabilityDTO.getTransactionIdentifier();
		ancillaryAvailabilityRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(txnId));
		ancillaryAvailabilityRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		StringStringMap stringStringMap;

		for (FlightSegmentTO flightSegmentTO : lccAncillaryAvailabilityDTO.getFlightDetails()) {
			BookingFlightSegment flightSegment = new BookingFlightSegment();
			flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
			flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
			flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
			flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
			flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
			flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
			flightSegment.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
			flightSegment.setBookingFlightRefNumber(flightSegmentTO.getPnrSegId());
			flightSegment.setForInsAvailability(flightSegmentTO.isForInsuranceAvailability());
			flightSegment.setOndSequence(flightSegmentTO.getOndSequence());

			ancillaryAvailabilityRQ.getFlightSegment().add(flightSegment);
		}

		for (LCCAncillaryType ancillaryType : lccAncillaryAvailabilityDTO.getAncillaryTypes()) {
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.SEAT_MAP))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.SEAT_MAP);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.SSR))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.SSR);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.MEALS))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.MEALS);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.BAGGAGE))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.BAGGAGE);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.INSURANCE))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.INSURANCE);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.AIRPORT_SERVICE))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.AIRPORT_SERVICE);
			if (ancillaryType.getAncillaryType().equals(LCCAncillaryType.FLEXI))
				ancillaryAvailabilityRQ.getAncillaryType().add(AncillaryType.FLEXI);
		}

		ancillaryAvailabilityRQ.setInsuranceOrigin(lccAncillaryAvailabilityDTO.getInsuranceOrigin());

		if (ancillaryAvailabilityRQ.getAncillaryType().size() == 0) {
			throw new ModuleException("");
		}

		if (lccAncillaryAvailabilityDTO.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)) {
			ancillaryAvailabilityRQ.setAppIndicator(AppIndicator.C);
		} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(lccAncillaryAvailabilityDTO.getAppIndicator())) {
			ancillaryAvailabilityRQ.setAppIndicator(AppIndicator.W);
		}
		ancillaryAvailabilityRQ.setModifyAnci(lccAncillaryAvailabilityDTO.isModifyAncillary());
		ancillaryAvailabilityRQ.setRequote(lccAncillaryAvailabilityDTO.isRequote());
		ancillaryAvailabilityRQ.setDepartureSegmentCode(lccAncillaryAvailabilityDTO.getDepartureSegmentCode());

		if (lccAncillaryAvailabilityDTO.getAirportServiceCountMap() != null) {
			for (Entry<String, Integer> entry : lccAncillaryAvailabilityDTO.getAirportServiceCountMap().entrySet()) {
				StringIntegerMap stringIntegerMap = new StringIntegerMap();
				stringIntegerMap.setKey(entry.getKey());
				stringIntegerMap.setValue(entry.getValue());
				ancillaryAvailabilityRQ.getAirportServiceCountMap().add(stringIntegerMap);
			}
		}

		if (lccAncillaryAvailabilityDTO.getFlightRefWiseSelectedMeals() != null) {
			for (Entry<String, Set<String>> mealEntry : lccAncillaryAvailabilityDTO.getFlightRefWiseSelectedMeals().entrySet()) {
				StringStringListMap mealObj = new StringStringListMap();
				mealObj.setKey(mealEntry.getKey());
				mealObj.getValue().addAll(mealEntry.getValue());
				ancillaryAvailabilityRQ.getFlightRefWiseSelectedMeals().add(mealObj);
			}
		}

		if (lccAncillaryAvailabilityDTO.getFlightRefWiseSelectedSSR() != null) {
			for (Entry<String, Set<String>> ssrEntry : lccAncillaryAvailabilityDTO.getFlightRefWiseSelectedSSR().entrySet()) {
				StringStringListMap ssrObj = new StringStringListMap();
				ssrObj.setKey(ssrEntry.getKey());
				ssrObj.getValue().addAll(ssrEntry.getValue());
				ancillaryAvailabilityRQ.getFlightRefWiseSelectedSSRs().add(ssrObj);
			}
		}

		if (lccAncillaryAvailabilityDTO.getBaggageSummaryTo() != null) {
			ReservationBaggageParams baggageParams = new ReservationBaggageParams();
			for (String ondCode : lccAncillaryAvailabilityDTO.getBaggageSummaryTo().getBookingClasses().keySet()) {
				stringStringMap = new StringStringMap();
				stringStringMap.setKey(ondCode);
				stringStringMap.setValue(lccAncillaryAvailabilityDTO.getBaggageSummaryTo().getBookingClasses().get(ondCode));
				baggageParams.getBookingClass().add(stringStringMap);
			}
			for (String ondCode : lccAncillaryAvailabilityDTO.getBaggageSummaryTo().getClassesOfService().keySet()) {
				stringStringMap = new StringStringMap();
				stringStringMap.setKey(ondCode);
				stringStringMap.setValue(lccAncillaryAvailabilityDTO.getBaggageSummaryTo().getClassesOfService().get(ondCode));
				baggageParams.getClassesOfService().add(stringStringMap);
			}
			baggageParams.setAgent(userPrincipal.getAgentCode());
			baggageParams.setSalesChannel(userPrincipal.getSalesChannel());
			baggageParams.setAllowUntilFinalCutOver(lccAncillaryAvailabilityDTO.getBaggageSummaryTo().isAllowTillFinalCutOver());
			ancillaryAvailabilityRQ.setReservationBaggageParams(baggageParams);
		}

		return ancillaryAvailabilityRQ;
	}



	public static List<? extends FlightSegment> getBookingFlightSegments(LCCAncillaryQuotation ancillaryQuotation)
			throws ModuleException {
		List<BookingFlightSegment> flightSegments = new ArrayList<>();
		BookingFlightSegment flightSegment;
		FlightSegmentTO flightSegmentTO;

		for (LCCSelectedSegmentAncillaryDTO segmentAncillary : ancillaryQuotation.getSegmentQuotations())  {
			flightSegmentTO = segmentAncillary.getFlightSegmentTO();

			flightSegment = new BookingFlightSegment();
			flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
			flightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
			flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
			flightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
			flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
			flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
			flightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
			flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
			flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
			flightSegment.setSegmentSequence(flightSegmentTO.getSegmentSequence());
			flightSegment.setOndSequence(flightSegmentTO.getOndSequence());
			flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
			flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
			flightSegment.setAirportCode(flightSegmentTO.getAirportCode());

			flightSegment.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
			flightSegments.add(flightSegment);
		}

		return flightSegments;
	}


	private static FlightSegmentTO getFlightSegmentTO(BookingFlightSegment bookingFlightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());
		flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
		flightSegmentTO.setFlightRefNumber(bookingFlightSegment.getFlightRefNumber());
		flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
		if(bookingFlightSegment.getReturnFlag().equals("Y")){
			flightSegmentTO.setReturnFlag(true);
		} else {
			flightSegmentTO.setReturnFlag(false);
		}
		flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
		flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
		flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
		return flightSegmentTO;
	}
	
	private static BookingFlightSegment getBookingFlightSegment(FlightSegmentTO flightSegmentTO) {
		BookingFlightSegment bookingFlightSegment = new BookingFlightSegment();
		bookingFlightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
		bookingFlightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
		bookingFlightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
		bookingFlightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
		bookingFlightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
		bookingFlightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
		bookingFlightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
		bookingFlightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
		bookingFlightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
		bookingFlightSegment.setSegmentSequence(flightSegmentTO.getSegmentSequence());
		bookingFlightSegment.setOndSequence(flightSegmentTO.getOndSequence());
		bookingFlightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
		bookingFlightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
		return bookingFlightSegment;
	}

	public static LCCAnciTaxApplicabilityRQ populateAnciTaxApplicabilityRQ(String carrierCode, String segmentCode,
			EXTERNAL_CHARGES extChg, UserPrincipal userPrincipal, BasicTrackInfo trackInfo) {
		LCCAnciTaxApplicabilityRQ lccAnciTaxApplicabilityRQ = new LCCAnciTaxApplicabilityRQ();
		lccAnciTaxApplicabilityRQ.setCarrier(carrierCode);
		lccAnciTaxApplicabilityRQ.setSegmentCode(segmentCode);
		lccAnciTaxApplicabilityRQ.setExternalCharge(LCCClientCommonUtils.getExternalChargeType(extChg));
		lccAnciTaxApplicabilityRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(null));
		lccAnciTaxApplicabilityRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));

		return lccAnciTaxApplicabilityRQ;
	}

	public static FlightSegmentTO getLCCFlightSegmentDTO(SegmentSeatMap segmentSeatMap) {
		FlightSegmentTO flightSegmentDTO = populateFlightSegmentTO(segmentSeatMap.getFlightSegment());
		return flightSegmentDTO;
	}

	public static LCCSeatMapDetailDTO getLCCSeatMapDetailDTO(SegmentSeatMap segmentSeatMap) {
		LCCSeatMapDetailDTO seatMapDetailDTO = new LCCSeatMapDetailDTO();

		SeatMapDetail seatMapDetail = segmentSeatMap.getSeatMapDetails();
		List<LCCCabinClassDTO> cabinClassDTOs = getLCCCabinClassDTOs(seatMapDetail);
		seatMapDetailDTO.setCabinClass(cabinClassDTOs);

		return seatMapDetailDTO;
	}

	public static FlightSegmentTO populateFlightSegmentTO(FlightSegmentMeals flightSegmentMeals) {
		FlightSegment flightSegment = flightSegmentMeals.getFlightSegment();
		FlightSegmentTO flightSegmentTO = populateFlightSegmentTO(flightSegment);
		return flightSegmentTO;
	}

	public static FlightSegmentTO populateFlightSegmentTO(FlightSegmentBaggages flightSegmentBaggages) {
		FlightSegment flightSegment = flightSegmentBaggages.getFlightSegment();
		FlightSegmentTO flightSegmentTO = populateFlightSegmentTO(flightSegment);
		flightSegmentTO.setReturnFlag(flightSegment.getReturnFlag().equals("Y") ? true : false);
		return flightSegmentTO;
	}

	public static LCCAncillaryRQ populatelccAncillaryRQForMeal(List<LCCMealRequestDTO> lccMealRequestDTOs,
			String transactionIdentifier, UserPrincipal userPrincipal, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> flightRefWiseSelectedMeals, String pnr) {

		LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
		lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionIdentifier));
		ReservationSummaryLite reservationSummaryLite = new ReservationSummaryLite();
		ReservationSummaryLite.AirSegment airSegment;

		FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();

		for (LCCMealRequestDTO lccMealRequestDTO : lccMealRequestDTOs) {

			for (FlightSegmentTO flightSegmentTO : lccMealRequestDTO.getFlightSegment()) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				if (flightSegmentTO.getSegmentCode() != null && !"".equals(flightSegmentTO.getSegmentCode())) {
					flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
				} else {
					flightSegment.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightSegmentTO
							.getFlightRefNumber()));
				}
				flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
				flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
				flightSegment.setOndSequence(flightSegmentTO.getOndSequence());
				flightSegment.setAdultCount(flightSegmentTO.getAdultCount());
				flightSegment.setChildCount(flightSegmentTO.getChildCount());
				flightSegment.setInfantCount(flightSegmentTO.getInfantCount());
				flightSegment.setFareBasisCode(flightSegmentTO.getFareBasisCode());
				flightSegment.setFlexiID(flightSegmentTO.getFlexiID() == null ? null : BigInteger.valueOf(flightSegmentTO
						.getFlexiID()));
				flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
				flightSegment.setParticipatingJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
				flightSegment.setSectorONDCode(flightSegmentTO.getSectorONDCode());
				flightSegment.setInverseSegmentDeparture(flightSegmentTO.getInverseSegmentDeparture());
				flightSegment.setBookingClass(flightSegmentTO.getBookingClass());

				flightRefNumberRPHList.getFlightRefNumber().add(flightSegmentTO.getFlightRefNumber());

				lccAncillaryRQ.getBookingFlightSegment().add(flightSegment);

				if (flightSegmentTO.getPnrSegId() != null && !flightSegmentTO.getPnrSegId().isEmpty()) {
					airSegment = new ReservationSummaryLite.AirSegment();
					airSegment.setAirSegmentId(Integer.parseInt(flightSegmentTO.getPnrSegId()));
					reservationSummaryLite.getAirSegment().add(airSegment);
				}
			}

			BookingMealRequest bookingMealRequest = new BookingMealRequest();
			bookingMealRequest.setFlightRefNumberRPHList(flightRefNumberRPHList);
			bookingMealRequest.setSelectedLanguage(selectedLanguage);

			List<StringStringListMap> flightRefWiseList = populateFlightRefWiseSelectedAnciMap(flightRefWiseSelectedMeals);
			if (flightRefWiseList != null) {
				bookingMealRequest.getFlightRefWiseSelectedMeals().addAll(flightRefWiseList);
			}

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setMealRequest(bookingMealRequest);
			bookingSpecialRequests.setPnr(pnr);
			bookingSpecialRequests.setReservationSummary(reservationSummaryLite);

			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);
		}
		return lccAncillaryRQ;
	}

	public static List<StringStringListMap> populateFlightRefWiseSelectedAnciMap(
			Map<String, Set<String>> flightRefWiseSelectedAnci) {
		List<StringStringListMap> mapList = null;

		if (flightRefWiseSelectedAnci != null) {
			mapList = new ArrayList<StringStringListMap>();
			for (Entry<String, Set<String>> entry : flightRefWiseSelectedAnci.entrySet()) {
				if (entry.getKey() != null && entry.getValue() != null) {
					StringStringListMap mapObj = new StringStringListMap();
					mapObj.setKey(entry.getKey());
					mapObj.getValue().addAll(entry.getValue());
					mapList.add(mapObj);
				}
			}
		}

		return mapList;
	}

	public static LCCAncillaryRQ populatelccAncillaryRQForBaggage(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, UserPrincipal userPrincipal, String selectedLanguage,
			LCCReservationBaggageSummaryTo baggageSummaryTo, String pnr, BasicTrackInfo trackInfo, boolean isRequote,
			Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID) {

		LCCAncillaryRQ lccAncillaryRQ = new LCCAncillaryRQ();
		lccAncillaryRQ.setLccPos(LCCClientCommonUtils.createPOS(userPrincipal, trackInfo));
		lccAncillaryRQ.setHeaderInfo(LCCClientCommonUtils.createHeaderInfo(transactionIdentifier));

		FlightRefNumberRPHList flightRefNumberRPHList = new FlightRefNumberRPHList();

		for (LCCBaggageRequestDTO lccBaggageRequestDTO : lccBaggageRequestDTOs) {

			String cabinClass = lccBaggageRequestDTO.getCabinClass();

			FlightSegmentTO flightSegmentTO = lccBaggageRequestDTO.getFlightSegment();
			BookingFlightSegment flightSegment = new BookingFlightSegment();
			flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
			flightSegment.setCabinClassCode(cabinClass);
			flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
			flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
			flightSegment.setOndSequence(flightSegmentTO.getOndSequence());
			if (flightSegmentTO.getSegmentCode() != null && !"".equals(flightSegmentTO.getSegmentCode())) {
				flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
			} else {
				flightSegment
						.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightSegmentTO.getFlightRefNumber()));
			}
			flightRefNumberRPHList.getFlightRefNumber().add(flightSegmentTO.getFlightRefNumber());
			flightSegment.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
			flightSegment.setBookingFlightRefNumber(flightSegmentTO.getPnrSegId());
			flightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
			flightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());

			flightSegment.setAdultCount(flightSegmentTO.getAdultCount());
			flightSegment.setChildCount(flightSegmentTO.getChildCount());
			flightSegment.setInfantCount(flightSegmentTO.getInfantCount());
			flightSegment.setFareBasisCode(flightSegmentTO.getFareBasisCode());
			flightSegment.setFlexiID(flightSegmentTO.getFlexiID() == null ? null : BigInteger.valueOf(flightSegmentTO
					.getFlexiID()));
			flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
			flightSegment.setParticipatingJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
			flightSegment.setSectorONDCode(flightSegmentTO.getSectorONDCode());
			flightSegment.setInverseSegmentDeparture(flightSegmentTO.getInverseSegmentDeparture());
			flightSegment.setJourneySeq(flightSegmentTO.getOndSequence());
			flightSegment.setBookingClass(flightSegmentTO.getBookingClass());

			lccAncillaryRQ.getBookingFlightSegment().add(flightSegment);

			BookingBaggageRequest bookingBaggageRequest = new BookingBaggageRequest();
			bookingBaggageRequest.setFlightRefNumberRPHList(flightRefNumberRPHList);
			bookingBaggageRequest.setSelectedLanguage(selectedLanguage);

			BookingSpecialRequests bookingSpecialRequests = new BookingSpecialRequests();
			bookingSpecialRequests.setBaggageRequest(bookingBaggageRequest);
			bookingSpecialRequests.setPnr(pnr);
			if (carrierWiseOndWiseBundlePeriodID != null && !carrierWiseOndWiseBundlePeriodID.isEmpty()) {
				for (Entry<String, Map<String, Integer>> carrierWiseEntry : carrierWiseOndWiseBundlePeriodID.entrySet()) {
					StringStringIntegerMap carrierWiseOndWiseBundlePeriodIDEntry = new StringStringIntegerMap();
					List<StringIntegerMap> ondWiseBundlePeriodIDEntryList = new ArrayList<>();
					for (Entry<String, Integer> entry : carrierWiseEntry.getValue().entrySet()) {
						StringIntegerMap ondWiseBundlePeriodIDEntry = new StringIntegerMap();
						ondWiseBundlePeriodIDEntry.setKey(entry.getKey());
						ondWiseBundlePeriodIDEntry.setValue(entry.getValue());
						ondWiseBundlePeriodIDEntryList.add(ondWiseBundlePeriodIDEntry);
					}
					carrierWiseOndWiseBundlePeriodIDEntry.setKey(carrierWiseEntry.getKey());
					carrierWiseOndWiseBundlePeriodIDEntry.getValue().addAll(ondWiseBundlePeriodIDEntryList);
					bookingSpecialRequests.getOndWiseBundlePeriodIDMap().add(carrierWiseOndWiseBundlePeriodIDEntry);
				}
			}
			bookingSpecialRequests.setRequote(isRequote);

			ReservationBaggageParams baggageParams = new ReservationBaggageParams();
			baggageParams.setAgent(userPrincipal.getAgentCode());
			for (String ondCode : baggageSummaryTo.getBookingClasses().keySet()) {
				StringStringMap entry = new StringStringMap();
				entry.setKey(ondCode);
				entry.setValue(baggageSummaryTo.getBookingClasses().get(ondCode));
				baggageParams.getBookingClass().add(entry);
			}
			for (String ondCode : baggageSummaryTo.getClassesOfService().keySet()) {
				StringStringMap entry = new StringStringMap();
				entry.setKey(ondCode);
				entry.setValue(baggageSummaryTo.getClassesOfService().get(ondCode));
				baggageParams.getClassesOfService().add(entry);
			}
			bookingBaggageRequest.setReservationBaggageParams(baggageParams);

			lccAncillaryRQ.setSpecialRequests(bookingSpecialRequests);
		}
		return lccAncillaryRQ;
	}

	public static FlightSegmentTO populateFlightSegmentTO(FlightSegment flightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(flightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setDepartureDateTime(flightSegment.getDepatureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
		flightSegmentTO.setFlightNumber(flightSegment.getFlightNumber());
		flightSegmentTO.setFlightRefNumber(flightSegment.getFlightRefNumber());
		flightSegmentTO.setOperatingAirline(flightSegment.getOperatingAirline());
		flightSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
		flightSegmentTO.setOperationType(flightSegment.getOperationType());

		if (flightSegment instanceof BookingFlightSegment) {
			BookingFlightSegment bookingFlightSegment = (BookingFlightSegment) flightSegment;
			flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
			flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
		}
		return flightSegmentTO;
	}

	private static List<LCCCabinClassDTO> getLCCCabinClassDTOs(SeatMapDetail seatMapDetail) {
		List<LCCCabinClassDTO> cabinClassDTOs = new ArrayList<LCCCabinClassDTO>();
		for (SeatMapCabinClass cabinClass : seatMapDetail.getCabinClass()) {
			List<LCCAirRowGroupDTO> airRowGroupDTOs = getLCCAirRowGroupDTOs(cabinClass);

			LCCCabinClassDTO cabinClassDTO = new LCCCabinClassDTO();
			cabinClassDTO.setCabinType(cabinClass.getCabinType());
			cabinClassDTO.setAirRowGroupDTOs(airRowGroupDTOs);

			cabinClassDTOs.add(cabinClassDTO);
		}
		return cabinClassDTOs;
	}

	private static List<LCCAirRowGroupDTO> getLCCAirRowGroupDTOs(SeatMapCabinClass cabinClass) {
		List<LCCAirRowGroupDTO> airRowGroupDTOs = new ArrayList<LCCAirRowGroupDTO>();
		for (SeatMapRowGroup airRowGroup : cabinClass.getAirRowGroups()) {
			List<LCCAirColumnGroupDTO> airColumnGroupDTOs = getLCCAirColumnGroupDTOs(airRowGroup);

			LCCAirRowGroupDTO airRowGroupDTO = new LCCAirRowGroupDTO();
			airRowGroupDTO.setAirColumnGroups(airColumnGroupDTOs);
			airRowGroupDTO.setRowGroupId(airRowGroup.getRowGroupId());

			airRowGroupDTOs.add(airRowGroupDTO);
		}
		return airRowGroupDTOs;
	}

	private static List<LCCAirColumnGroupDTO> getLCCAirColumnGroupDTOs(SeatMapRowGroup airRowGroup) {
		List<LCCAirColumnGroupDTO> airColumnGroupDTOs = new ArrayList<LCCAirColumnGroupDTO>();
		for (SeatMapColumnGroup airColumnGroup : airRowGroup.getAirColumnGroups()) {
			List<LCCAirRowDTO> airRowDTOs = getLCCAirRowDTOs(airColumnGroup);

			LCCAirColumnGroupDTO airColumnGroupDTO = new LCCAirColumnGroupDTO();
			airColumnGroupDTO.setAirRows(airRowDTOs);
			airColumnGroupDTO.setColumnGroupId(airColumnGroup.getColumnGroupId());
			airColumnGroupDTOs.add(airColumnGroupDTO);
		}

		return airColumnGroupDTOs;
	}

	private static List<LCCAirRowDTO> getLCCAirRowDTOs(SeatMapColumnGroup airColumnGroup) {
		List<LCCAirRowDTO> airRowDTOs = new ArrayList<LCCAirRowDTO>();
		for (SeatMapRow airRow : airColumnGroup.getAirRows()) {
			List<LCCAirSeatDTO> airSeatDTOs = getLCCAirSeatDTOs(airRow);

			LCCAirRowDTO airRowDTO = new LCCAirRowDTO();

			airRowDTO.setRowNumber(airRow.getRowNumber());
			airRowDTO.setPassengerTypeQuantity(getPassengerTypeQuantity(airRow.getPassengerTypeQuantity()));
			airRowDTO.setAirSeats(airSeatDTOs);
			airRowDTO.setSeatsVacant(airRow.isSeatsVacant());

			airRowDTOs.add(airRowDTO);
		}

		return airRowDTOs;
	}

	private static List<LCCAirSeatDTO> getLCCAirSeatDTOs(SeatMapRow airRow) {
		List<LCCAirSeatDTO> airSeatDTOs = new ArrayList<LCCAirSeatDTO>();
		for (Seat airSeat : airRow.getAirSeats()) {
			LCCAirSeatDTO airSeatDTO = new LCCAirSeatDTO();

			airSeatDTO.setSeatAvailability(airSeat.getSeatAvailability());
			airSeatDTO.setSeatMessage(airSeat.getSeatMessage());
			airSeatDTO.setSeatCharge(airSeat.getSeatCharge());
			airSeatDTO.setSeatNumber(airSeat.getSeatNumber());
			airSeatDTO.setBookedPassengerType(getPassengerType(airSeat.getBookedPassengerType()));
			airSeatDTO.setNotAllowedPassengerType(getNotAllowedPassengerType(airSeat.getNotAllowedPassengerType()));
			airSeatDTO.setLogicalCabinClassCode(airSeat.getLogicalCabinClass());
			airSeatDTO.setSeatLocationType(airSeat.getSeatLocationType());
			if (airSeat.getSeatType() != null) {
				airSeatDTO.setSeatType(airSeat.getSeatType().value());
			}

			airSeatDTOs.add(airSeatDTO);
		}
		return airSeatDTOs;
	}

	private static List<LCCPassengerTypeQuantityDTO>
			getPassengerTypeQuantity(List<PassengerTypeQuantity> passengerTypeQuantities) {

		List<LCCPassengerTypeQuantityDTO> lccPassengerTypeQuantities = new ArrayList<LCCPassengerTypeQuantityDTO>();
		for (PassengerTypeQuantity passengerTypeQuantity : passengerTypeQuantities) {

			LCCPassengerTypeQuantityDTO lccPassengerTypeQuantityDTO = new LCCPassengerTypeQuantityDTO();
			lccPassengerTypeQuantityDTO.setPaxType(getPassengerType(passengerTypeQuantity.getPassengerType()));
			lccPassengerTypeQuantityDTO.setQuantity(passengerTypeQuantity.getQuantity());

			lccPassengerTypeQuantities.add(lccPassengerTypeQuantityDTO);
		}

		return lccPassengerTypeQuantities;
	}

	private static List<String> getNotAllowedPassengerType(List<PassengerType> notAllowedPassengerTypes) {
		List<String> lccNotAllowedPassengerTypes = new ArrayList<String>();
		for (PassengerType passengerType : notAllowedPassengerTypes) {
			lccNotAllowedPassengerTypes.add(getPassengerType(passengerType));
		}

		return lccNotAllowedPassengerTypes;
	}

	private static String getPassengerType(PassengerType bookedPassengerType) {
		if (bookedPassengerType == null) {
			return null;
		}

		String paxType = null;
		switch (bookedPassengerType) {
		case ADT:
			paxType = PaxTypeTO.ADULT;
			break;
		case CHD:
			paxType = PaxTypeTO.CHILD;
			break;
		case PRT:
			paxType = PaxTypeTO.PARENT;
			break;
		case INF:
			paxType = PaxTypeTO.INFANT;
			break;
		default:
			paxType = null;
		}
		return paxType;
	}

	private static LCCAncillaryType getLCCAncillaryType(AncillaryType ancillaryType) {
		LCCAncillaryType lccAncillaryType = null;
		if (ancillaryType != null) {
			switch (ancillaryType) {
			case SEAT_MAP:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.SEAT_MAP);
				break;
			case SSR:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.SSR);
				break;
			case HALA:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.HALA);
				break;
			case INSURANCE:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.INSURANCE);
				break;
			case MEALS:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.MEALS);
				break;
			case BAGGAGE:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.BAGGAGE);
				break;
			case AIRPORT_SERVICE:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.AIRPORT_SERVICE);
				break;
			case FLEXI:
				lccAncillaryType = new LCCAncillaryType(LCCAncillaryType.FLEXI);
				break;

			}
		}
		return lccAncillaryType;
	}

	public static Set<String> convertToBundledFareFreeServiceNames(List<ExternalChargeType> externalChargeTypes) {
		Set<String> bundledFareFreeServiceNames = new HashSet<String>();

		for (ExternalChargeType externalChargeType : externalChargeTypes) {
			if (externalChargeType == ExternalChargeType.SEAT_MAP) {
				bundledFareFreeServiceNames.add(EXTERNAL_CHARGES.SEAT_MAP.toString());
			} else if (externalChargeType == ExternalChargeType.MEAL) {
				bundledFareFreeServiceNames.add(EXTERNAL_CHARGES.MEAL.toString());
			} else if (externalChargeType == ExternalChargeType.BAGGAGE) {
				bundledFareFreeServiceNames.add(EXTERNAL_CHARGES.BAGGAGE.toString());
			} else if (externalChargeType == ExternalChargeType.AIRPORT_SERVICE) {
				bundledFareFreeServiceNames.add(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());
			} else if (externalChargeType == ExternalChargeType.FLEXI_CHARGES) {
				bundledFareFreeServiceNames.add(EXTERNAL_CHARGES.FLEXI_CHARGES.toString());
			}
		}

		return bundledFareFreeServiceNames;
	}

}
