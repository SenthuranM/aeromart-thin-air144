package com.isa.thinair.lccclient.core.bl;

import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * @author Nilindra Fernando
 * @since 6:36 PM 11/5/2009
 */
public class LCCClientResAlterationQueryBL {

	public static ReservationBalanceTO getReservationBalanceSummary(Set<LCCClientReservationPax> passengers,
			LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

		ReservationBalanceTO balanceTO = null;

		switch (lccClientResAlterQueryModesTO.getModifyModes()) {
		case CANCEL_RES:
			balanceTO = LCCClientCancelReservationBL.query(lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		case CANCEL_OND:
			balanceTO = LCCClientCancelSegmentBL.query(lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		case MODIFY_OND:
			balanceTO = LCCClientModifySegmentBL.query(lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		case ADD_OND:
			balanceTO = LCCClientAddSegmentBL.query(lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		case REMOVE_PAX:
			balanceTO = LCCClientRemovePaxBL.query(passengers, lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		case ADD_INF:
			balanceTO = LCCClientAddInfantBL.query(lccClientResAlterQueryModesTO, userPrincipal, trackInfo);
			break;
		default:
			throw new ModuleException("lccclient.reservation.invocationError");
		}

		return balanceTO;
	}

}
