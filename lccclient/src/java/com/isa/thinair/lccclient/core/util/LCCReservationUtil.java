package com.isa.thinair.lccclient.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AppIndicator;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicableBasis;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CustomChargeDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCDiscountedFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LoadReservationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifcationParamRQ;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCurrencyAmount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.RouteTypes;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.core.bl.LCCClientCommonUtils;
import com.isa.thinair.lccclient.core.bl.LCCClientPaymentUtil;

public class LCCReservationUtil {

	/*
	 * Creates LoadReservationOptions Object from reservation search.
	 */
	public static LoadReservationOptions createLoadReservationOptions() {
		// Comments extracted from Zaki's notes
		// If you are doing any modification please check it need to be applied in the
		// Lcconnect/lcc-engine-maxico/src/main/java/com/isa/lcc/maxico/core/util/CommonUtil.java
		// class as well.
		LoadReservationOptions loadReservationOptions = new LoadReservationOptions();
		loadReservationOptions.setLoadFares(true); // Loads ReservationPax into Reservation object
		loadReservationOptions.setLoadLocalTimes(true); // For performance I think it's better we convert ourselves at
														// the end
		loadReservationOptions.setLoadSegView(true); // Loads ReservationSegmentDTO into Reservation object
		loadReservationOptions.setLoadOndChargesView(true); // If this is true, then [loadFares] is set to true in code
		loadReservationOptions.setLoadSegViewReturnGroupId(true); // Loads the return group id if needed.
																	// "Use only for open return bookings"
		loadReservationOptions.setLoadSegViewBookingTypes(true); // Loads booking types
		loadReservationOptions.setLoadSegViewFareCategoryTypes(true); // If true then loadSegViewBookingTypes is set to
																		// true
		loadReservationOptions.setLoadPaxAvaBalance(true); // Zaki [Feb.11.2009] : Always keep true
		loadReservationOptions.setLoadSeatingInfo(true); // Loads Collection<PaxSeatTO> into Reservation object
		loadReservationOptions.setLoadMealInfo(true); // Cause we don't have meals in AAAirBookRS
		loadReservationOptions.setLoadSSRInfo(true);
		loadReservationOptions.setRecordAudit(true); // I belive we should always make an audit, unless LCC will do it
														// for us.
		loadReservationOptions.setLoadLastUserNote(true); // -- BIZ DECISION = ALWAYS SET TO TRUE --
		loadReservationOptions.setLoadPaxPaymentOndBreakdownView(true); // loads the pax payment breakdown for payments
		loadReservationOptions.setLoadExternalPaxPayments(true);
		loadReservationOptions.setLoadEtickets(true);
		loadReservationOptions.setLoadOriginCountryCode(false);
		loadReservationOptions.setLoadRefundableChargeDetails(false);
		loadReservationOptions.setLoadGOQUOAmounts(true);
		return loadReservationOptions;
	}

	/*
	 * Creates LoadReservationOptions Object from reservation search. Please refer createLoadReservationOptions() method
	 * for the comments
	 */
	public static LoadReservationOptions createLoadReservationOptions(LCCClientPnrModesDTO lccClientPnrModesDTO) {
		LoadReservationOptions loadReservationOptions = new LoadReservationOptions();
		loadReservationOptions.setLoadFares(lccClientPnrModesDTO.isLoadFares());
		loadReservationOptions.setLoadLocalTimes(lccClientPnrModesDTO.isLoadLocalTimes());
		loadReservationOptions.setLoadSegView(lccClientPnrModesDTO.isLoadSegView());
		loadReservationOptions.setLoadOndChargesView(lccClientPnrModesDTO.isLoadOndChargesView());
		loadReservationOptions.setLoadSegViewReturnGroupId(lccClientPnrModesDTO.isLoadSegViewReturnGroupId());
		loadReservationOptions.setLoadSegViewBookingTypes(lccClientPnrModesDTO.isLoadSegViewBookingTypes());
		loadReservationOptions.setLoadSegViewFareCategoryTypes(lccClientPnrModesDTO.isLoadSegViewFareCategoryTypes());
		loadReservationOptions.setLoadPaxAvaBalance(lccClientPnrModesDTO.isLoadPaxAvaBalance());
		loadReservationOptions.setLoadSeatingInfo(lccClientPnrModesDTO.isLoadSeatingInfo());
		loadReservationOptions.setLoadMealInfo(lccClientPnrModesDTO.isLoadMealInfo());
		loadReservationOptions.setLoadBaggageInfo(lccClientPnrModesDTO.isLoadBaggageInfo());
		loadReservationOptions.setLoadSSRInfo(lccClientPnrModesDTO.isLoadSSRInfo());
		loadReservationOptions.setRecordAudit(lccClientPnrModesDTO.isRecordAudit());
		loadReservationOptions.setLoadLastUserNote(lccClientPnrModesDTO.isLoadLastUserNote());
		loadReservationOptions.setLoadPaxPaymentOndBreakdownView(lccClientPnrModesDTO.isLoadPaxPaymentOndBreakdownView());
		loadReservationOptions.setLoadExternalPaxPayments(lccClientPnrModesDTO.isLoadExternalPaxPayments());
		loadReservationOptions.setLoadEtickets(lccClientPnrModesDTO.isLoadEtickets());
		loadReservationOptions.setLoadOriginCountryCode(lccClientPnrModesDTO.isLoadOriginCountry());
		loadReservationOptions.setLoadGOQUOAmounts(lccClientPnrModesDTO.isLoadGOQUOAmounts());
		loadReservationOptions.setLoadRefundableChargeDetails(lccClientPnrModesDTO.isLoadRefundableTaxInfo());
		loadReservationOptions.setLoadClassifyUN(lccClientPnrModesDTO.isLoadClassifyUN());
		return loadReservationOptions;
	}

	/**
	 * 
	 * @param paxWisePaymentAssemblers
	 * @param userPrincipal
	 * @param pnr
	 * @param otherCarrierPaymentsOnly
	 *            - this is to identify whether we need to assemble other carrier payments only or all. This method is
	 *            used for carrier payments & create booking. Depending on the method we need to separate the payment
	 *            details
	 * @param firstPayment TODO
	 * @return
	 * @throws ModuleException
	 */
	public static List<TravelerFulfillment> parseTravelerFulfillments(
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers, UserPrincipal userPrincipal, String pnr,
			boolean otherCarrierPaymentsOnly, boolean firstPayment) throws ModuleException {
		// Note we should not get infant payment assemblers here.
		List<TravelerFulfillment> travelerFulfillments = new ArrayList<TravelerFulfillment>();
		for (Integer paxSequence : paxWisePaymentAssemblers.keySet()) {
			LCCClientPaymentAssembler lcclientPaymentAssembler = paxWisePaymentAssemblers.get(paxSequence);
			if (lcclientPaymentAssembler.getPayments().size() > 0) {
				TravelerFulfillment travelerFulfillment = (TravelerFulfillment) LCCReservationUtil.parseFulfillment(
						lcclientPaymentAssembler, userPrincipal, otherCarrierPaymentsOnly, pnr, firstPayment)[0];
				travelerFulfillment.setTravelerRefNumber(paxSequence + "");// We need to remove either refNumber or
																			// sequence
				travelerFulfillment.setPaxSequence(paxSequence);
				travelerFulfillments.add(travelerFulfillment);
			}
		}
		return travelerFulfillments;
	}

	/**
	 * 
	 * @param lccClientPaymentAssembler
	 * @param userPrincipal
	 * @param otherCarrierPaymentsOnly
	 *            - boolean to handle filter other carrier payments or not
	 * @param firstPayment TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] parseFulfillment(LCCClientPaymentAssembler lccClientPaymentAssembler, UserPrincipal userPrincipal,
			boolean otherCarrierPaymentsOnly, String pnr, boolean firstPayment) throws ModuleException {
		TravelerFulfillment travelerFulfillment = new TravelerFulfillment();
		BigDecimal fullPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<String> colPaymentRefNumber = new HashSet<String>();
		for (Iterator<LCCClientPaymentInfo> iterator = lccClientPaymentAssembler.getPayments().iterator(); iterator.hasNext();) {
			LCCClientPaymentInfo lcclientPaymentInfo = iterator.next();
			if ((!otherCarrierPaymentsOnly)
					|| (otherCarrierPaymentsOnly && !lcclientPaymentInfo.getCarrierCode().equals(
							AppSysParamsUtil.getDefaultCarrierCode()))) {
				PaymentDetails paymentDetails = LCCClientPaymentUtil.transform(lcclientPaymentInfo, pnr);
				paymentDetails.setFirstPayment(firstPayment);
				paymentDetails.getPaymentSummary().setCurrencyCodeGroup(FindReservationUtil.getDefaultCurrencyCodeGroup());
				paymentDetails.setRemarks(lcclientPaymentInfo.getRemarks());
				travelerFulfillment.getPaymentDetails().add(paymentDetails);
				colPaymentRefNumber.add(lcclientPaymentInfo.getLccUniqueTnxId());
				fullPaymentAmount = AccelAeroCalculator.add(fullPaymentAmount, lcclientPaymentInfo.getTotalAmount());
			}
			// We are removing other carrier payments here because for a single credit payment there might be several
			// credit CF
			// payments linked. We need to remove existing other carrier credit payments and add the exact pax credit
			// payments.
			// Currently we are having other carrier pax credit only. If need to change this to other carrier agent
			// payments
			// We dont need to remove that payment.
			// actual payments will be added in a calling method.
			if (otherCarrierPaymentsOnly
					&& !lcclientPaymentInfo.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())
					&& lcclientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo)
				iterator.remove();

		}
		return new Object[] { travelerFulfillment, fullPaymentAmount, colPaymentRefNumber };
	}

	/***
	 * Used for own airline flow having other carrier payments
	 * 
	 * @param paxWisePaymentAssemblers
	 * @param travelerFulfillments
	 * @return
	 */
	public static Map<Integer, LCCClientPaymentAssembler> combineActualCreditPayments(
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers, List<TravelerFulfillment> travelerFulfillments) {

		for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
			Integer paxSequence = travelerFulfillment.getPaxSequence();
			if (paxWisePaymentAssemblers.get(paxSequence) == null)
				paxWisePaymentAssemblers.put(paxSequence, new LCCClientPaymentAssembler());

			for (PaymentDetails paymentDetails : travelerFulfillment.getPaymentDetails()) {
				TravelerCreditPayment travelerCreditPayment = paymentDetails.getTravelerCreditPayment();

				PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(paymentDetails.getPaymentCurrencyAmount());

				// Note: Only pax credit will be supported at the moment.
				paxWisePaymentAssemblers.get(paxSequence).addPaxCreditPayment(
						paymentDetails.getPaymentSummary().getPaymentCarrier(),
						Integer.valueOf(travelerCreditPayment.getDebitTravelerRefNumber()),
						paymentDetails.getPaymentSummary().getPaymentAmount(), CalendarUtil.getCurrentSystemTimeInZulu(),
						travelerCreditPayment.getCarrierBaseAmount(), payCurrencyDTO, travelerCreditPayment.getEDate(),
						travelerFulfillment.getPaxSequence(), paymentDetails.getUniqueTnxId(),
						travelerCreditPayment.getBasePnr(), travelerCreditPayment.getPaxCreditId());
			}
		}
		return paxWisePaymentAssemblers;
	}

	/***
	 * Used for interline flow
	 * 
	 * @param carrierPayments
	 * @param lcclientReservationAssembler
	 */
	public static void combineActualCreditPayments(Map<Integer, Collection<TnxCreditPayment>> carrierPayments,
			CommonReservationAssembler lcclientReservationAssembler, Map<String, String> paymentAuthIdMap,
			Map<String, Integer> paymentBrokerRefNoMap) {
		for (Integer paxSequence : carrierPayments.keySet()) {
			Collection<TnxCreditPayment> paxCreditPayments = carrierPayments.get(paxSequence);
			for (LCCClientReservationPax reservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
				if (reservationPax.getPaxSequence().intValue() == paxSequence.intValue()) {
					LCCClientPaymentAssembler paymentAssembler = reservationPax.getLccClientPaymentAssembler();
					// Remove own airline credit payments be because we have the exact payment details now
					for (Iterator<LCCClientPaymentInfo> paymentInfoIterator = paymentAssembler.getPayments().iterator(); paymentInfoIterator
							.hasNext();) {
						LCCClientPaymentInfo lccClientPaymentInfo = paymentInfoIterator.next();
						if (lccClientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo) {
							if (lccClientPaymentInfo.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
								paymentInfoIterator.remove();
							}
						} else if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
							CommonCreditCardPaymentInfo cardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;
							String key = getPaymentKey(cardPaymentInfo);
							cardPaymentInfo.setAuthorizationId(paymentAuthIdMap.get(key));
							cardPaymentInfo.setPaymentBrokerId(paymentBrokerRefNoMap.get(key));
						}
					}
					for (TnxCreditPayment tnxCreditPayment : paxCreditPayments) {
						paymentAssembler.addPaxCreditPayment(tnxCreditPayment.getPaymentCarrier(),
								Integer.valueOf(tnxCreditPayment.getPnrPaxId()), tnxCreditPayment.getAmount(),
								CalendarUtil.getCurrentSystemTimeInZulu(), tnxCreditPayment.getAmount(),
								tnxCreditPayment.getPayCurrencyDTO(), tnxCreditPayment.getExpiryDate(), paxSequence,
								tnxCreditPayment.getLccUniqueTnxId(), tnxCreditPayment.getPnr(),
								tnxCreditPayment.getPaxCreditId());
					}
				}
			}
		}

		for (LCCClientReservationPax reservationPax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
			if (reservationPax.getLccClientPaymentAssembler() != null) {
				for (LCCClientPaymentInfo lccClientPaymentInfo : reservationPax.getLccClientPaymentAssembler().getPayments()) {
					if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
						CommonCreditCardPaymentInfo cardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;
						String key = getPaymentKey(cardPaymentInfo);
						cardPaymentInfo.setAuthorizationId(paymentAuthIdMap.get(key));
						cardPaymentInfo.setPaymentBrokerId(paymentBrokerRefNoMap.get(key));
					}
				}
			}
		}
	}

	/**
	 * Build key - This is not correct key
	 * 
	 * @param cardPaymentInfo
	 * @return
	 */
	/**
	 * TODO Build unique key
	 */
	public static String getPaymentKey(CommonCreditCardPaymentInfo cardPaymentInfo) {
		return ((cardPaymentInfo.getPaymentBrokerId() == null ? "0" : cardPaymentInfo.getPaymentBrokerId()) + "_" + StringUtil
				.getNotNullString(cardPaymentInfo.getNo()));
	}

	public static void combineActualCreditPayments(Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers,
			Map<Integer, Collection<TnxCreditPayment>> carrierPayments, Map<String, String> paymentAuthIdMap,
			Map<String, Integer> paymentBrokerRefNoMap) {
		for (Integer paxSequence : carrierPayments.keySet()) {
			// This is a credit payment. Pax should have a credit payment in the original map for sure.

			// Remove own airline credit payments be because we have the exact payment details now
			if (paxSeqWisePayAssemblers.get(paxSequence) != null) {
				for (Iterator<LCCClientPaymentInfo> paymentInfoIterator = paxSeqWisePayAssemblers.get(paxSequence).getPayments()
						.iterator(); paymentInfoIterator.hasNext();) {
					LCCClientPaymentInfo lccClientPaymentInfo = paymentInfoIterator.next();
					if (lccClientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo) {
						if (lccClientPaymentInfo.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
							paymentInfoIterator.remove();
						}
					}
				}

				// Add new carrier credit payments
				LCCClientPaymentAssembler paymentAssembler = paxSeqWisePayAssemblers.get(paxSequence);
				for (TnxCreditPayment tnxCreditPayment : carrierPayments.get(paxSequence)) {
					paymentAssembler.addPaxCreditPayment(tnxCreditPayment.getPaymentCarrier(),
							Integer.valueOf(tnxCreditPayment.getPnrPaxId()), tnxCreditPayment.getAmount(),
							CalendarUtil.getCurrentSystemTimeInZulu(), tnxCreditPayment.getAmount(),
							tnxCreditPayment.getPayCurrencyDTO(), tnxCreditPayment.getExpiryDate(), paxSequence,
							tnxCreditPayment.getLccUniqueTnxId(), tnxCreditPayment.getPnr(), tnxCreditPayment.getPaxCreditId());
				}
			}
		}

		for (Integer paxSequence : paxSeqWisePayAssemblers.keySet()) {
			for (LCCClientPaymentInfo lccClientPaymentInfo : paxSeqWisePayAssemblers.get(paxSequence).getPayments()) {
				if (lccClientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
					CommonCreditCardPaymentInfo cardPaymentInfo = (CommonCreditCardPaymentInfo) lccClientPaymentInfo;
					String key = getPaymentKey(cardPaymentInfo);
					cardPaymentInfo.setAuthorizationId(paymentAuthIdMap.get(key));
					cardPaymentInfo.setPaymentBrokerId(paymentBrokerRefNoMap.get(key));
				}
			}
		}
	}
	
	public static ModifcationParamRQ createModificationParamRQ(ModificationParamRQInfo paramRQInfo) {
		if (paramRQInfo != null) {
			ModifcationParamRQ modifcationParamRQ = new ModifcationParamRQ();
			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(paramRQInfo.getAppIndicator())) {
				modifcationParamRQ.setAppIndicator(AppIndicator.W);
			} else if (paramRQInfo.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)) {
				modifcationParamRQ.setAppIndicator(AppIndicator.C);
			}
			modifcationParamRQ.setIsRegisteredUser(paramRQInfo.getIsRegisteredUser());

			return modifcationParamRQ;
		} else {
			return null;
		}
	}

	/**
	 * convert the charge override dto to maxio compatible
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @return
	 */
	public static ChargerOverride createChargeOverride(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO) {
		ChargerOverride chargerOverride = new ChargerOverride();

		chargerOverride.setSendingAbosoluteCharge(lccClientResAlterQueryModesTO.isSendingAbosoluteCharge());

		chargerOverride.setCustomAdultChargeValue(lccClientResAlterQueryModesTO.getCustomAdultCharge());
		chargerOverride.setCustomChildChargeValue(lccClientResAlterQueryModesTO.getCustomChildCharge());
		chargerOverride.setCustomInfantChargeValue(lccClientResAlterQueryModesTO.getCustomInfantCharge());
		chargerOverride.setExistingModificationChargeAdult(lccClientResAlterQueryModesTO.getDefaultCustomAdultCharge());
		chargerOverride.setExistingModificationChargeChild(lccClientResAlterQueryModesTO.getDefaultCustomChildCharge());

		chargerOverride
				.setAdultCustomChargeDetail(getCustomChargerDetail(lccClientResAlterQueryModesTO.getAdultCustomChargeTO()));
		chargerOverride
				.setChildCustomChargeDetail(getCustomChargerDetail(lccClientResAlterQueryModesTO.getChildCustomChargeTO()));
		chargerOverride.setInfantCustomChargeDetail(getCustomChargerDetail(lccClientResAlterQueryModesTO
				.getInfantCustomChargeTO()));

		if (lccClientResAlterQueryModesTO.getRouteType() != null && !lccClientResAlterQueryModesTO.getRouteType().isEmpty()
				&& !"undefined".equals(lccClientResAlterQueryModesTO.getRouteType())) {
			chargerOverride.setApplicableBasis(ApplicableBasis.valueOf(lccClientResAlterQueryModesTO.getRouteType()));
		}

		return chargerOverride;
	}

	public static ChargerOverride createChargeOverride(CustomChargesTO customChargesTO, RouteTypes routeTypes) {
		if (customChargesTO == null) {
			return null;
		}
		ChargerOverride chargerOverride = new ChargerOverride();

		chargerOverride.setSendingAbosoluteCharge(customChargesTO.isAbsoluteValuesPassed());

		chargerOverride.setCustomAdultChargeValue(customChargesTO.getCustomAdultCCharge());
		chargerOverride.setCustomChildChargeValue(customChargesTO.getCustomChildCCharge());
		chargerOverride.setCustomInfantChargeValue(customChargesTO.getCustomInfantCCharge());

		chargerOverride.setAdultCustomChargeDetail(getCustomChargerDetail(customChargesTO.getCustomAdultChargeTO()));
		chargerOverride.setChildCustomChargeDetail(getCustomChargerDetail(customChargesTO.getCustomChildChargeTO()));
		chargerOverride.setInfantCustomChargeDetail(getCustomChargerDetail(customChargesTO.getCustomInfantChargeTO()));
		chargerOverride.setExistingModificationChargeAdult(customChargesTO.getDefaultCustomAdultCharge());
		chargerOverride.setExistingModificationChargeChild(customChargesTO.getDefaultCustomChildCharge());
		chargerOverride.setExistingModificationChargeInfant(customChargesTO.getDefaultCustomInfantCharge());

		if (routeTypes != null) {
			chargerOverride.setApplicableBasis(ApplicableBasis.valueOf(routeTypes.getRouteTypes()));
		} else if (customChargesTO.getRouteType() != null && !customChargesTO.getRouteType().isEmpty()
				&& !"undefined".equals(customChargesTO.getRouteType())) {
			chargerOverride.setApplicableBasis(ApplicableBasis.valueOf(customChargesTO.getRouteType()));
		}

		return chargerOverride;
	}

	public static ChargerOverride createChargeOverrideForCancel(CustomChargesTO customChargesTO) {
		return createChargeOverride(customChargesTO, RouteTypes.TOTAL_ROUTE);

	}

	private static CustomChargeDetail getCustomChargerDetail(PnrChargeDetailTO pnrChargeDetailTO) {
		CustomChargeDetail chargeDetail = null;

		if (pnrChargeDetailTO != null) {

			if (pnrChargeDetailTO.getCancellationChargeType() != null && !pnrChargeDetailTO.getCancellationChargeType().isEmpty()) {
				chargeDetail = new CustomChargeDetail();
				chargeDetail.setChargeType(pnrChargeDetailTO.getCancellationChargeType());
				chargeDetail.setMaxAmount(pnrChargeDetailTO.getMaximumCancellationAmount());
				chargeDetail.setMinAmount(pnrChargeDetailTO.getMinCancellationAmount());
				chargeDetail.setPercentage(pnrChargeDetailTO.getChargePercentage());

			} else if (pnrChargeDetailTO.getModificationChargeType() != null
					&& !pnrChargeDetailTO.getModificationChargeType().isEmpty()) {
				chargeDetail = new CustomChargeDetail();
				chargeDetail.setChargeType(pnrChargeDetailTO.getModificationChargeType());
				chargeDetail.setMaxAmount(pnrChargeDetailTO.getMaximumModificationAmount());
				chargeDetail.setMinAmount(pnrChargeDetailTO.getMinModificationAmount());
				chargeDetail.setPercentage(pnrChargeDetailTO.getChargePercentage());

			}
		}

		return chargeDetail;
	}

	public static PriceInfo parsePriceInfo(Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap) {
		PriceInfo priceInfo = new PriceInfo();
		Date applicableDate = new Date();

		for (Entry<Integer, LCCClientPaymentAssembler> entry : passengerPaymentMap.entrySet()) {
			Integer paxSeq = entry.getKey();
			LCCClientPaymentAssembler lccClientPaymentAssembler = entry.getValue();

			if (lccClientPaymentAssembler.getPerPaxExternalCharges().size() > 0) {
				Collection<ExternalCharge> colExternalCharge = new ArrayList<ExternalCharge>();
				BigDecimal totalExternalChgAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (LCCClientExternalChgDTO lccClientExternalChgDTO : lccClientPaymentAssembler.getPerPaxExternalCharges()) {

					ExternalCharge externalCharge = new ExternalCharge();
					externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
					externalCharge.setApplicableDateTime(applicableDate);
					externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO
							.getExternalCharges()));
					externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
					externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
					externalCharge.setCode(lccClientExternalChgDTO.getCode());
					externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
					externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());

					if (lccClientExternalChgDTO.getCarrierCode() == null && lccClientExternalChgDTO.getExternalCharges() != EXTERNAL_CHARGES.JN_OTHER) {
						externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					} else {
						externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
					}

					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
						Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
								.getFlexiBilities();
						Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
						for (FlexiInfoTO flexiInfoTO : flexiBilities) {
							FlexiInfo flexiInfo = new FlexiInfo();
							flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
							flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
							flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
							flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
							flexiInfos.add(flexiInfo);
						}
						externalCharge.getAdditionalDetails().addAll(flexiInfos);
					}

					if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
						externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
						externalCharge.setSegmentSequence(lccClientExternalChgDTO.getSegmentSequence());
						externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
					} else if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
						externalCharge.setUserNote(lccClientExternalChgDTO.getUserNote());
						externalCharge.setSegmentSequence(lccClientExternalChgDTO.getSegmentSequence());
					} else if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.BAGGAGE) {
						externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
						externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
					}

					colExternalCharge.add(externalCharge);
					totalExternalChgAmount = AccelAeroCalculator.add(totalExternalChgAmount, externalCharge.getAmount());
				}

				PerPaxPriceInfo perPaxPriceInfo = new PerPaxPriceInfo();
				perPaxPriceInfo.setPassengerType(PassengerType.ADT); // FIXME it's hard coded make it configurable
				perPaxPriceInfo.setTravelerRefNumber(paxSeq + ""); // FIXME
				perPaxPriceInfo.setPassengerPrice(new FareType());

				perPaxPriceInfo.getPassengerPrice().getExternalCharge().addAll(colExternalCharge);
				perPaxPriceInfo.getPassengerPrice().setTotalExternalCharge(totalExternalChgAmount);

				priceInfo.getPerPaxPriceInfo().add(perPaxPriceInfo);
			}
		}

		return priceInfo;
	}

	private static PayCurrencyDTO getPayCurrencyDTO(PaymentCurrencyAmount paymentCurrencyAmount) {
		String payCurrencyCode = paymentCurrencyAmount.getCurrencyCodeGroup().getCurrencyCode();
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode,
				paymentCurrencyAmount.getPayCurrMultiplyingExchangeRate(), paymentCurrencyAmount.getBoundaryValue(),
				paymentCurrencyAmount.getBreakPointValue());
		return payCurrencyDTO;
	}

	/**
	 * @param colPaymentInfo
	 * @return
	 */
	public static boolean isBSPpayment(Collection colPaymentInfo) {

		if (colPaymentInfo != null) {
			Iterator itColPaymentInfo = colPaymentInfo.iterator();

			while (itColPaymentInfo.hasNext()) {
				PaymentInfo paymentInfo = (PaymentInfo) itColPaymentInfo.next();
				if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					if (agentCreditInfo.getPaymentReferenceTO() != null
							&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType() != null
							&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType().toString()
									.equals(PAYMENT_REF_TYPE.BSP.toString())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static LCCDiscountedFareDetails populateLccDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		LCCDiscountedFareDetails lccDiscountedFareDetails = null;

		if (discountedFareDetails != null) {
			lccDiscountedFareDetails = new LCCDiscountedFareDetails();
			lccDiscountedFareDetails.setFarePercentage(discountedFareDetails.getFarePercentage());
			lccDiscountedFareDetails.setNotes(discountedFareDetails.getNotes());
			lccDiscountedFareDetails.setCode(discountedFareDetails.getCode());
			lccDiscountedFareDetails.setDomFareDiscountPercentage(discountedFareDetails.getDomFareDiscountPercentage());
			lccDiscountedFareDetails.setDomesticDiscountApplied(discountedFareDetails.isDomesticDiscountApplied());

			if (discountedFareDetails.getDomesticSegmentCodeList() != null
					&& !discountedFareDetails.getDomesticSegmentCodeList().isEmpty()) {
				lccDiscountedFareDetails.getDomesticSegmentCodeList().addAll(discountedFareDetails.getDomesticSegmentCodeList());
			}

			lccDiscountedFareDetails.setPromotionId(discountedFareDetails.getPromotionId());
			lccDiscountedFareDetails.setPromoType(discountedFareDetails.getPromotionType());
			lccDiscountedFareDetails.setPromoCode(discountedFareDetails.getPromoCode());
			lccDiscountedFareDetails.setSystemGenerated(discountedFareDetails.isSystemGenerated());
			lccDiscountedFareDetails.setAllowSamePaxOnly(discountedFareDetails.isAllowSamePaxOnly());
			lccDiscountedFareDetails.setOriginPnr(discountedFareDetails.getOriginPnr());
			lccDiscountedFareDetails.setDiscountType(discountedFareDetails.getDiscountType());
			lccDiscountedFareDetails.setDiscountApplyTo(discountedFareDetails.getDiscountApplyTo());
			lccDiscountedFareDetails.setDiscountAs(discountedFareDetails.getDiscountAs());

			if (discountedFareDetails.getApplicablePaxCount() != null && !discountedFareDetails.getApplicablePaxCount().isEmpty()) {
				for (Entry<String, Integer> entry : discountedFareDetails.getApplicablePaxCount().entrySet()) {
					StringIntegerMap strIntObj = new StringIntegerMap();
					strIntObj.setKey(entry.getKey());
					strIntObj.setValue(entry.getValue());
					lccDiscountedFareDetails.getApplicablePaxCount().add(strIntObj);
				}
			}

			if (discountedFareDetails.getApplicableOnds() != null && !discountedFareDetails.getApplicableOnds().isEmpty()) {
				lccDiscountedFareDetails.getApplicableOnds().addAll(discountedFareDetails.getApplicableOnds());
			}

			if (discountedFareDetails.getApplicableAncillaries() != null
					&& !discountedFareDetails.getApplicableAncillaries().isEmpty()) {
				lccDiscountedFareDetails.getApplicableAncillaries().addAll(discountedFareDetails.getApplicableAncillaries());
			}

			if (discountedFareDetails.getApplicableBINs() != null && !discountedFareDetails.getApplicableBINs().isEmpty()) {
				lccDiscountedFareDetails.getApplicableBINs().addAll(discountedFareDetails.getApplicableBINs());
			}

		}

		return lccDiscountedFareDetails;
	}

	public boolean isReservationEligibleForAutoRefund() {
		return false;
	}
	
	public static List<PaxCharges> populateLccPaxCharges(Collection<PaxChargesTO> paxChargesList) {
		List<PaxCharges> lccPaxCharges = new ArrayList<PaxCharges>();
		if (paxChargesList != null && !paxChargesList.isEmpty()) {

			for (PaxChargesTO paxChargesTO : paxChargesList) {
				PaxCharges paxCharge = new PaxCharges();
				paxCharge.setParent(paxChargesTO.isParent());
				paxCharge.setPassengerType(LCCClientCommonUtils.getLCCPaxTypeCode(paxChargesTO.getPaxTypeCode()));
				paxCharge.setPaxSequence(paxChargesTO.getPaxSequence());

				if (paxChargesTO.getExtChgList() != null && !paxChargesTO.getExtChgList().isEmpty()) {

					paxCharge.getExternalCharges().addAll(composePaxExternalChargeMap(paxChargesTO.getExtChgList()));
				}
				lccPaxCharges.add(paxCharge);
			}
		}

		return lccPaxCharges;
	}

	private static List<ExternalCharge> composePaxExternalChargeMap(List<LCCClientExternalChgDTO> extChgList) {

		List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
		for (LCCClientExternalChgDTO lccClientExternalChgDTO : extChgList) {
			ExternalCharge externalCharge = new ExternalCharge();
			externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
			// externalCharge.setApplicableDateTime(new Date());
			externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

			if (lccClientExternalChgDTO.getCarrierCode() == null) {
				externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			} else {
				externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
			}
			externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
			externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
			externalCharge.setCode(lccClientExternalChgDTO.getCode());
			externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
			externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
			externalCharge.setUserNote(lccClientExternalChgDTO.getUserNote());
			externalCharge.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
			externalCharge.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
			externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
			externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());

			if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
				Collection<FlexiInfoTO> flexiBilities = ((LCCClientFlexiExternalChgDTO) lccClientExternalChgDTO)
						.getFlexiBilities();
				Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
				for (FlexiInfoTO flexiInfoTO : flexiBilities) {
					FlexiInfo flexiInfo = new FlexiInfo();
					flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
					flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
					flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
					flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
					flexiInfos.add(flexiInfo);
				}
				externalCharge.getAdditionalDetails().addAll(flexiInfos);
			} 

			externalChargeList.add(externalCharge);
		}

		return externalChargeList;
	}
	
	public static List<PerPaxExternalCharges>
			composePassengerExternalChargeMap(Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, Date date) {
		List<PerPaxExternalCharges> ppExtChgList = new ArrayList<PerPaxExternalCharges>();
		for (Integer seqNo : paxExtChgMap.keySet()) {
			List<ExternalCharge> externalChargeList = new ArrayList<ExternalCharge>();
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxExtChgMap.get(seqNo)) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(lccClientExternalChgDTO.getAmount());
				externalCharge.setApplicableDateTime(date);
				externalCharge.setType(LCCClientCommonUtils.getExternalChargeType(lccClientExternalChgDTO.getExternalCharges()));

				if (lccClientExternalChgDTO.getCarrierCode() == null) {
					externalCharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				} else {
					externalCharge.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
				}
				externalCharge.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
				externalCharge.setCode(lccClientExternalChgDTO.getCode());
				externalCharge.setAirportCode(lccClientExternalChgDTO.getAirportCode());
				externalCharge.setApplyOn(lccClientExternalChgDTO.getApplyOn());
				externalCharge.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
				externalCharge.setIsAnciOffer(lccClientExternalChgDTO.isAnciOffer());
				externalCharge.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
				externalCharge.setOperationMode(lccClientExternalChgDTO.getOperationMode());

				externalChargeList.add(externalCharge);
			}
			PerPaxExternalCharges ppExtCharge = new PerPaxExternalCharges();
			ppExtCharge.setPaxSequence(seqNo);
			ppExtCharge.getExternalCharges().addAll(externalChargeList);
			ppExtChgList.add(ppExtCharge);
		}
		return ppExtChgList;
	}

}
