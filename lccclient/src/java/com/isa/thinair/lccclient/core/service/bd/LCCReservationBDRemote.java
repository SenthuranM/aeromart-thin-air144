package com.isa.thinair.lccclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.lccclient.api.service.LCCReservationBD;

@Remote
public interface LCCReservationBDRemote extends LCCReservationBD {

}
