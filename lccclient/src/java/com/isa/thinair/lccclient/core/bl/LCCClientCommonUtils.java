package com.isa.thinair.lccclient.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicablePromotionDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Country;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CreditStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.HeaderInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LoadDataOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCardType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingStatus;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCPOS;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO.status;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.lccclient.api.util.LCCClientModuleUtil;
import com.isa.thinair.lccclient.api.util.LCCUtils;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

/**
 * @author Nilindra Fernando
 */
public class LCCClientCommonUtils {

	private static Log log = LogFactory.getLog(LCCClientCommonUtils.class);

	public static PassengerType getLCCPaxTypeCode(String paxType) {
		PassengerType passengerType = null;
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			passengerType = PassengerType.ADT;
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			passengerType = PassengerType.CHD;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			passengerType = PassengerType.INF;
		} else if (ReservationInternalConstants.PassengerType.PARENT.equals(paxType)) {
			passengerType = PassengerType.PRT;
		}
		return passengerType;
	}

	public static String convertLCCPaxCodeToAAPaxCodes(PassengerType passengerType) {
		if (passengerType == PassengerType.ADT) {
			return PaxTypeTO.ADULT;
		} else if (passengerType == PassengerType.CHD) {
			return PaxTypeTO.CHILD;
		} else if (passengerType == PassengerType.INF) {
			return PaxTypeTO.INFANT;
		}

		// undefined pax type found, return null
		return null;
	}

	public static LCCPOS createPOS(UserPrincipal userPrincipal, BasicTrackInfo trackInfo) {
		LCCPOS lccPos = createPOS(userPrincipal);
		lccPos.setUserSessionId(trackInfo.getSessionId());
		lccPos.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());
		lccPos.setCallingInstanceId(trackInfo.getCallingInstanceId());
		if(trackInfo.getMarketingUserId() != null && trackInfo.getMarketingUserId() != ""){
			lccPos.setMarketingUserId(trackInfo.getMarketingUserId());
		}
		return lccPos;
	}

	public static LCCPOS createPOS(UserPrincipal userPrincipal) {

		LCCPOS lccPos = new LCCPOS();
		lccPos.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());

		if (userPrincipal != null && userPrincipal.getSalesChannel() == ReservationInternalConstants.SalesChannel.WEB) {
			lccPos.setBookingChannel(BookingSystems.AARES_IBE);
			lccPos.setCallingInstanceId(userPrincipal.getCallingInstanceId());
			String customerId = BeanUtils.nullHandler(userPrincipal.getCustomerId());

			if (customerId.length() > 0) {
				lccPos.setMarketingCustomerId(customerId);
			}
		} else if (userPrincipal != null
				&& userPrincipal.getSalesChannel() == ReservationInternalConstants.SalesChannel.TRAVEL_AGENT) {
			lccPos.setBookingChannel(BookingSystems.AARES_XBE);
			lccPos.setAirportCode(userPrincipal.getAgentStation());
			lccPos.setMarketingAgentCode(userPrincipal.getAgentCode());
			lccPos.setMarketingUserId(userPrincipal.getUserId());
			lccPos.setCallingInstanceId(userPrincipal.getCallingInstanceId());
			lccPos.setUserSessionId(userPrincipal.getSessionId());
			// FIXME remove the try catch and throw the error
			try {
				User user = LCCClientModuleUtil.getSecurityBD().getUser(userPrincipal.getUserId());
				lccPos.setMarketingUserPassword(LCCClientModuleUtil.getCryptoServiceBD().decrypt(user.getPassword()));
			} catch (ModuleException e) {
				log.error(e);
			}
		} else if (userPrincipal != null
				&& (userPrincipal.getSalesChannel() == ReservationInternalConstants.SalesChannel.EBIATM || userPrincipal
						.getSalesChannel() == ReservationInternalConstants.SalesChannel.EBICDM)) {
			lccPos.setBookingChannel(BookingSystems.AARES_XBE);
			lccPos.setAirportCode(userPrincipal.getAgentStation());
			lccPos.setMarketingAgentCode(userPrincipal.getAgentCode());
			lccPos.setMarketingUserId(userPrincipal.getUserId());
			lccPos.setUserSessionId(userPrincipal.getSessionId());
			// FIXME remove the try catch and throw the error
			try {
				User user = LCCClientModuleUtil.getSecurityBD().getUser(userPrincipal.getUserId());
				lccPos.setMarketingUserPassword(LCCClientModuleUtil.getCryptoServiceBD().decrypt(user.getPassword()));
			} catch (ModuleException e) {
				log.error(e);
			}
		} else if (userPrincipal != null && userPrincipal.getSalesChannel() == ReservationInternalConstants.SalesChannel.IOS) {
			lccPos.setBookingChannel(BookingSystems.AARES_IOS);
			lccPos.setCallingInstanceId(userPrincipal.getCallingInstanceId());
			String customerId = BeanUtils.nullHandler(userPrincipal.getCustomerId());

			if (customerId.length() > 0) {
				lccPos.setMarketingCustomerId(customerId);
			}
		} else if (userPrincipal != null && userPrincipal.getSalesChannel() == ReservationInternalConstants.SalesChannel.ANDROID) {
			lccPos.setBookingChannel(BookingSystems.AARES_ANDROID);
			lccPos.setCallingInstanceId(userPrincipal.getCallingInstanceId());
			String customerId = BeanUtils.nullHandler(userPrincipal.getCustomerId());

			if (customerId.length() > 0) {
				lccPos.setMarketingCustomerId(customerId);
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("USER PRINCIPLE AT LCCPOS :" + (userPrincipal == null ? "null" : userPrincipal.toString()));
		}

		return lccPos;
	}

	public static LCCPOS createPOS(UserPrincipal userPrincipal, TrackInfoDTO trackInfoTO) {

		LCCPOS lccPos = createPOS(userPrincipal);
		if (trackInfoTO != null) {
			lccPos.setMarketingIPAddress(getIPAddress(trackInfoTO));
			lccPos.setUserSessionId(trackInfoTO.getSessionId());
			lccPos.setCallingInstanceId(trackInfoTO.getCallingInstanceId());
		}

		return lccPos;
	}

	public static String getIPAddress(TrackInfoDTO trackInfoTO) {
		if (trackInfoTO != null) {
			String ipAddress = BeanUtils.nullHandler(trackInfoTO.getIpAddress());

			if (ipAddress.length() > 0) {
				return ipAddress;
			}
		}

		return null;
	}

	public static HeaderInfo createHeaderInfo(String transactionId) {
		HeaderInfo headerInfo = new HeaderInfo();

		headerInfo.setTimeStamp(CalendarUtil.getCurrentSystemTimeInZulu());
		headerInfo.setTransactionIdentifier(transactionId);
		headerInfo.setRequestResponseIdentifier(UUID.randomUUID().toString());

		return headerInfo;
	}

	public static LoadDataOptions getDefaultLoadOptions() {
		// TODO Let the frontend decide which items are needed. This is for tempory purposes only.
		// Please improve this.
		LoadDataOptions loadDataOptions = new LoadDataOptions();
		loadDataOptions.setLoadAccSummary(true);
		loadDataOptions.setLoadTravelerInfo(true);
		loadDataOptions.setLoadAirItinery(true);
		loadDataOptions.setLoadPriceInfoTotals(true);
		loadDataOptions.setLoadPTCPriceInfo(true);
		loadDataOptions.setLoadFullFilment(true);
		loadDataOptions.setLoadPTCFullFilment(true);
		loadDataOptions.setLoadPTCAccSummary(true);

		return loadDataOptions;
	}

	public static PaymentType getPaymentCardType(PaymentCardType paymentCardType) {
		PaymentType paymentType = PaymentType.CARD_GENERIC;

		if (paymentCardType != null) {
			if (PaymentCardType.VISA == paymentCardType) {
				paymentType = PaymentType.CARD_VISA;
			} else if (PaymentCardType.MASTER == paymentCardType) {
				paymentType = PaymentType.CARD_MASTER;
			} else if (PaymentCardType.AMERICAN_EXPRESS == paymentCardType) {
				paymentType = PaymentType.CARD_AMEX;
			} else if (PaymentCardType.DINERS_CLUB == paymentCardType) {
				paymentType = PaymentType.CARD_DINERS;
			} else if (PaymentCardType.CMI == paymentCardType) {
				paymentType = PaymentType.CARD_CMI;
			} else if (PaymentCardType.GENERIC == paymentCardType) {
				paymentType = PaymentType.CARD_GENERIC;
			}
		}

		return paymentType;
	}

	public static ExternalChargeType getExternalChargeType(EXTERNAL_CHARGES lccExternalCharges) {
		if (lccExternalCharges == EXTERNAL_CHARGES.CREDIT_CARD) {
			return ExternalChargeType.CREDIT_CARD;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.HANDLING_CHARGE) {
			return ExternalChargeType.HANDLING_CHARGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.SEAT_MAP) {
			return ExternalChargeType.SEAT_MAP;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.MEAL) {
			return ExternalChargeType.MEAL;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.BAGGAGE) {
			return ExternalChargeType.BAGGAGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.INSURANCE) {
			return ExternalChargeType.INSURANCE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
			return ExternalChargeType.INFLIGHT_SERVICES;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.FLEXI_CHARGES) {
			return ExternalChargeType.FLEXI_CHARGES;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
			return ExternalChargeType.AIRPORT_SERVICE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.PROMOTION_REWARD) {
			return ExternalChargeType.PROMOTION_REWARD;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE) {
			return ExternalChargeType.ADDITONAL_SEAT_CHARGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.JN_ANCI) {
			return ExternalChargeType.JN_ANCI;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.JN_OTHER) {
			return ExternalChargeType.JN_OTHER;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.ANCI_PENALTY) {
			return ExternalChargeType.ANCI_PENALTY;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.SERVICE_TAX) {
			return ExternalChargeType.SERVICE_TAX;	
		} else if (lccExternalCharges == EXTERNAL_CHARGES.BSP_FEE) {
			return ExternalChargeType.BSP_FEE;	
		}

		return null;
	}

	public static status convertLCCCreditStatusToAACreditStatus(CreditStatus lccCreditStatus) {
		if (lccCreditStatus == CreditStatus.AVAILABLE) {
			return status.AVAILABLE;
		} else {
			return status.EXPIRED;
		}
	}

	public static TicketingInfo populateTicketingInfo(String reservationStatus, BigDecimal availableBalance) {
		TicketingInfo ticketingInfo = new TicketingInfo();
		TicketingStatus ticketingStatus = null;
		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservationStatus)) {
			// In modification process we should not depend on the existing available balance. If we need partial status
			// for that flow we may need the differentiation
			if (availableBalance != null && availableBalance.doubleValue() > 0) {
				ticketingStatus = TicketingStatus.TICKETTYPE_PARTIAL_TICKETED;
			} else {
				ticketingStatus = TicketingStatus.TICKETTYPE_TICKETED;
			}
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
			ticketingStatus = TicketingStatus.TICKETTYPE_ONHOLD_TICKETED;
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservationStatus)) {
			ticketingStatus = TicketingStatus.TICKETTYPE_CANCELLED;
		}
		ticketingInfo.setTicketingStatus(ticketingStatus);
		return ticketingInfo;
	}

	public static ApplicablePromotionDetailsTO populateApplicablePromotionDetailsTO(ApplicablePromotionDetails lccPromoDetails) {
		ApplicablePromotionDetailsTO promotionDetailsTO = null;
		if (lccPromoDetails != null) {
			promotionDetailsTO = new ApplicablePromotionDetailsTO();
			promotionDetailsTO.setPromoCriteriaId(lccPromoDetails.getPromoCriteriaId());
			promotionDetailsTO.setPromoName(lccPromoDetails.getPromoName());
			promotionDetailsTO.setDescription(lccPromoDetails.getDescription());
			promotionDetailsTO.setPromoType(lccPromoDetails.getPromoType());
			promotionDetailsTO.setPromoCode(lccPromoDetails.getPromoCode());
			promotionDetailsTO.setSystemGenerated(lccPromoDetails.isSystemGenerated());
			promotionDetailsTO.setAllowSamePaxOnly(lccPromoDetails.isAllowSamePaxOnly());
			promotionDetailsTO.setOriginPnr(lccPromoDetails.getOriginPnr());
			promotionDetailsTO.setDiscountType(lccPromoDetails.getDiscountType());
			promotionDetailsTO.setDiscountValue(lccPromoDetails.getDiscountValue());
			promotionDetailsTO.setDiscountAs(lccPromoDetails.getDiscountAs());
			promotionDetailsTO.setApplyTo(lccPromoDetails.getApplyTo());
			promotionDetailsTO.setApplicableAdultCount(lccPromoDetails.getApplicableAdultCount());
			promotionDetailsTO.setApplicableChildCount(lccPromoDetails.getApplicableChildCount());
			promotionDetailsTO.setApplicableInfantCount(lccPromoDetails.getApplicableInfantCount());
			promotionDetailsTO.setApplicableAncillaries(lccPromoDetails.getApplicableAncillaries());

			if (!lccPromoDetails.getApplicableOnds().isEmpty()) {
				promotionDetailsTO.setApplicableOnds(new HashSet<String>(lccPromoDetails.getApplicableOnds()));
			}

			if (!lccPromoDetails.getApplicableBINs().isEmpty()) {
				promotionDetailsTO.setApplicableBINs(new HashSet<Integer>(lccPromoDetails.getApplicableBINs()));
			}
		}

		return promotionDetailsTO;
	}

	public static List<PaymentMap> createLCCTmpPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> lccTemporaryTnxMap) {
		List<PaymentMap> lccTmpPayment = new ArrayList<PaymentMap>();

		for (Integer paxSeq : lccTemporaryTnxMap.keySet()) {
			CommonCreditCardPayment commonCreditCardPayment = new CommonCreditCardPayment();
			PaymentMap paymentMap = new PaymentMap();

			CommonCreditCardPaymentInfo commonCreditCardPaymentInfo = lccTemporaryTnxMap.get(paxSeq);

			commonCreditCardPayment.setAddress(commonCreditCardPaymentInfo.getAddress());
			commonCreditCardPayment.setAppIndicator(commonCreditCardPaymentInfo.getAppIndicator().toString());
			commonCreditCardPayment.setAuthorizationId(commonCreditCardPaymentInfo.getAuthorizationId());
			commonCreditCardPayment.setCardHolderName(commonCreditCardPaymentInfo.getCardHoldersName());
			commonCreditCardPayment.setEDate(commonCreditCardPaymentInfo.geteDate());
			commonCreditCardPayment.setGroupPNR(commonCreditCardPaymentInfo.getGroupPNR());
			commonCreditCardPayment.setIsPaymentSuccess(commonCreditCardPaymentInfo.isPaymentSuccess());
			commonCreditCardPayment.setName(commonCreditCardPaymentInfo.getName());
			commonCreditCardPayment.setNo(commonCreditCardPaymentInfo.getNo());
			commonCreditCardPayment.setFirst4Digits(commonCreditCardPaymentInfo.getNoFirstDigits());
			commonCreditCardPayment.setLast4Digits(commonCreditCardPaymentInfo.getNoLastDigits());
			commonCreditCardPayment.setOldCCRecordId(commonCreditCardPaymentInfo.getOldCCRecordId());
			commonCreditCardPayment.setPaymentBrokerId(commonCreditCardPaymentInfo.getPaymentBrokerId());
			commonCreditCardPayment.setSecurityCode(commonCreditCardPaymentInfo.getSecurityCode());
			commonCreditCardPayment.setTemporyPaymentId(commonCreditCardPaymentInfo.getTemporyPaymentId());
			commonCreditCardPayment.setTnxMode(commonCreditCardPaymentInfo.getTnxMode().getCode());
			paymentMap.setKey(paxSeq);
			paymentMap.setValue(commonCreditCardPayment);

			lccTmpPayment.add(paymentMap);
		}

		return lccTmpPayment;
	}

	public static ContactInfo createContactInfo(ReservationContactInfo resContactInfo) throws ModuleException {
		ContactInfo contactInfo = new ContactInfo();
		contactInfo.setEmail(resContactInfo.getEmail());
		contactInfo.setMarketingCarrierCustomerId(resContactInfo.getCustomerId());

		contactInfo.setPersonName(transform(resContactInfo.getTitle(), resContactInfo.getFirstName(),
				resContactInfo.getLastName()));

		Telephone telephone = new Telephone();
		telephone.setPhoneNumber(resContactInfo.getPhoneNo());
		contactInfo.setTelephone(telephone);

		Telephone mobile = new Telephone();
		mobile.setPhoneNumber(resContactInfo.getMobileNo());
		contactInfo.setMobile(mobile);

		Address address = new Address();
		address.setCityName(resContactInfo.getCity());

		String addressLine1 = BeanUtils.nullHandler(resContactInfo.getStreetAddress1());
		if (!addressLine1.isEmpty()) {
			address.getAddressLine().add(addressLine1);
		}

		String addressLine2 = BeanUtils.nullHandler(resContactInfo.getStreetAddress2());
		if (!addressLine2.isEmpty()) {
			address.getAddressLine().add(addressLine2);
		}
		address.setState(resContactInfo.getState());
		Country country = new Country();
		address.setCountry(country);
		country.setCountryCode(resContactInfo.getCountryCode());
		country.setCountryName(LCCUtils.getCountryName(resContactInfo.getCountryCode()));
		contactInfo.setAddress(address);

		Telephone fax = new Telephone();
		contactInfo.setFax(fax);
		fax.setPhoneNumber(resContactInfo.getFax());

		contactInfo.setZipCode(resContactInfo.getZipCode());

		if (resContactInfo.getNationalityCode() != null) {
			Integer nationalityCode = Integer.parseInt(resContactInfo.getNationalityCode());
			if (nationalityCode != null) {
				contactInfo.setNationalityCode(LCCUtils.getNationalityIsoCode(nationalityCode));
			}
		}

		contactInfo.setPreferredLanguage(resContactInfo.getPreferredLanguage());

		contactInfo.setEmgnPersonName(transform(resContactInfo.getEmgnTitle(), resContactInfo.getEmgnFirstName(),
				resContactInfo.getEmgnLastName()));

		Telephone emgnPhone = new Telephone();
		contactInfo.setEmgnTelephone(emgnPhone);
		emgnPhone.setPhoneNumber(resContactInfo.getEmgnPhoneNo());

		contactInfo.setEmgnEmail(resContactInfo.getEmgnEmail());
		contactInfo.setSendPromoEmail(resContactInfo.getSendPromoEmail());

		return contactInfo;
	}

	public static PersonName transform(String title, String firstName, String lastName) {
		PersonName personName = new PersonName();
		personName.setTitle(title);
		personName.setFirstName(firstName);
		personName.setSurName(lastName);

		return personName;
	}

}
