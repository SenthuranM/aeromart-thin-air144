package com.isa.thinair.lccclient.api.service;

import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface LCCUserAgentSeviceBD {

	public static final String SERVICE_NAME = "LCCUserAgentSevice";

	/**
	 * Invaldates the user session for the given user in the aaservices across all the carriers.
	 * 
	 * @param userId
	 *            The user id of which the session should be invalidated.
	 * @param trackInfo
	 *            TODO
	 * @throws ModuleException
	 */
	public void invalidateAAServiceSessionAcrossCarriers(String userId, BasicTrackInfo trackInfo) throws ModuleException;

}
