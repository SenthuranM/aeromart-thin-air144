package com.isa.thinair.lccclient.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * This is used to retrieve all the pax credit across the carriers link to lcc
 * 
 * @author mekanayake
 * 
 */
public interface LCCPaxCreditBD {

	public static final String SERVICE_NAME = "LCCPaxCreditService";

	public Collection<ReservationPaxDTO> getPaxCreditAccrossCarriers(ReservationSearchDTO searchReservationDTO,
			BasicTrackInfo trackInfo) throws ModuleException;

	public Collection<PaxCreditDTO> getPaxCreditAccrossCarriers(List<String> pnrList, BasicTrackInfo trackInfo)
			throws ModuleException;

	public void extendOrReinstateCredits(String mode, int creditId, Date expiryDate, String note, String pnr, int pnrPaxId, int paxTxnId,
			BigDecimal amount, BigDecimal oCAmount, String carrierCode, TrackInfoDTO trackInfoDTO) throws ModuleException;
	
}
