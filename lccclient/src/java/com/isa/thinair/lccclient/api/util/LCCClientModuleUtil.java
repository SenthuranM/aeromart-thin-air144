package com.isa.thinair.lccclient.api.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.lccclient.api.service.LCCReservationBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.lccclient.core.config.LCCClientConfig;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Nilindra Fernando
 */
public class LCCClientModuleUtil {

	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(LCCClientModuleUtil.class);

	public static LCCClientConfig getConfig() {
		return (LCCClientConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/lccclient?id=lccclientModuleConfig");
	}

	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) lookupServiceBD(CryptoConstants.MODULE_NAME, CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME, PaymentBrokerBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static LCCReservationBD getLCCReservationBD() {
		return (LCCReservationBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCReservationBD.SERVICE_NAME);
	}

	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getConfig(), "lccclient",
				"lccclient.config.dependencymap.invalid");
	}

	private static Object lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getConfig(), "lccclient",
				"lccclient.config.dependencymap.invalid");
	}
}
