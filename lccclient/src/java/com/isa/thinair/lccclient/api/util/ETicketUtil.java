package com.isa.thinair.lccclient.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Eticket;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.SurfaceSegmentUtil;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author eric
 */
public class ETicketUtil {

	private static final Comparator<LCCClientReservationSegment> LCC_SEGEMNT_SEQUNCE_COMPARATOR = new Comparator<LCCClientReservationSegment>() {
		public int compare(LCCClientReservationSegment seg1, LCCClientReservationSegment seg2) {
			return seg1.getSegmentSeq().compareTo(seg2.getSegmentSeq());
		}
	};

	/**
	 * @param passengers
	 * @param lccSegments
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(
			Collection<LCCClientReservationPax> passengers, Collection<LCCClientReservationSegment> lccSegments,
			CreateTicketInfoDTO ticketInfoDto) throws ModuleException {
		List<LCCClientReservationSegment> segmentList = new ArrayList<LCCClientReservationSegment>(lccSegments);
		// sort segment list by departure date
		Collections.sort(segmentList);

		return LCCClientModuleUtil.getReservationBD().generateIATAETicketNumbers(ETicketUtil.getPassengerSequnceList(passengers),
				ETicketUtil.getSegmentSequnceList(segmentList), ticketInfoDto);

	}

	/**
	 * @param passengers
	 * @return
	 */
	private static Collection<Integer> getPassengerSequnceList(Collection<LCCClientReservationPax> passengers) {
		Collection<Integer> passengerSequnceCollection = new ArrayList<Integer>();
		Iterator<LCCClientReservationPax> paxIterator = passengers.iterator();
		while (paxIterator.hasNext()) {
			passengerSequnceCollection.add(paxIterator.next().getPaxSequence());
		}

		return passengerSequnceCollection;

	}

	/**
	 * @param segmentList
	 * @return
	 */
	private static List<Integer> getSegmentSequnceList(List<LCCClientReservationSegment> segmentList) {
		List<Integer> segmentSequnceCollection = new ArrayList<Integer>();

		Iterator<LCCClientReservationSegment> segmentIterator = segmentList.iterator();
		while (segmentIterator.hasNext()) {
			segmentSequnceCollection.add(segmentIterator.next().getSegmentSeq());
		}

		return segmentSequnceCollection;

	}

	/**
	 * @param reservationSegments
	 * @param paxSequnce
	 * @param ticketNumbers
	 * @return
	 * @throws ModuleException
	 */
	public static List<Eticket> createPaxEticket(Collection<LCCClientReservationSegment> reservationSegments, int paxSequnce,
			Map<Integer, EticketDTO> ticketNumbers) throws ModuleException {

		List<LCCClientReservationSegment> sortedReservationSegments = new ArrayList<LCCClientReservationSegment>(
				reservationSegments);

		Collections.sort(sortedReservationSegments, LCC_SEGEMNT_SEQUNCE_COMPARATOR);
		List<Eticket> travelerEtickets = new ArrayList<Eticket>();

		Iterator<Integer> eTicketIterator = ticketNumbers.keySet().iterator();
		while (eTicketIterator.hasNext()) {
			Integer lccSegmentSequnce = eTicketIterator.next();
			EticketDTO eticketDTO = ticketNumbers.get(lccSegmentSequnce);
			Eticket eticket = new Eticket();
			eticket.setLccSegmentSequence(lccSegmentSequnce);
			eticket.setEticketNumber(eticketDTO.getEticketNo());
			eticket.setCouponNo(eticketDTO.getCouponNo());
			travelerEtickets.add(eticket);
		}

		updateEticketWithSegmentDetail(sortedReservationSegments, travelerEtickets);

		return travelerEtickets;
	}

	private static void updateEticketWithSegmentDetail(Collection<LCCClientReservationSegment> colLCCClientReservationSegment,
			List<Eticket> travelerEtickets) throws ModuleException {

		Map<String, CachedAirportDTO> mapSegCodeAndCachedAirportDTO = SurfaceSegmentUtil
				.getCachedAirportMap(colLCCClientReservationSegment);
		Map<String, Integer> airlineWiseSegmentSequence = new HashMap<String, Integer>();
		
		boolean isInterline = false;
		Set<String> carrierCodes = new HashSet<String>();
		for (LCCClientReservationSegment segment : colLCCClientReservationSegment) {
			carrierCodes.add(AppSysParamsUtil.extractCarrierCode(segment.getFlightNo()));
		}
		if (!carrierCodes.isEmpty() && carrierCodes.size() > 1) {
			isInterline = true;
		}

		for (LCCClientReservationSegment segment : colLCCClientReservationSegment) {
			CachedAirportDTO cachedAirportDTO = mapSegCodeAndCachedAirportDTO.get(segment.getSegmentCode());
			String carrierCode = AppSysParamsUtil.extractCarrierCode(segment.getFlightNo());

			if (cachedAirportDTO.isSurfaceSegment()) {
				// Nili Feb 17, 2012
				// For Surface Segments flight carrier comes as surface carrier code. If we use this for the segment
				// sequence generation for eticket it's causing problems. There fore we need to locate the actual
				// operating carrier for surface segments.
				// Ideally this should have been better if we have passed the operating by with the segment information.
				// This is practically impossible, since need to pass it via many places for many operations. There for
				// a fix was done to carry out from the CachedAirportMap. However this logic fails when same route is
				// being operated by 2 or more surface carriers. practically this can not happen. Just adding the
				// following validation to prevent this operation if it's fails in the future.

				if (cachedAirportDTO.getOperatedBy().length() != 2) {
					throw new ModuleException("lccclient.eticket.update.bus.operating.carrier.ambigous");
				}

				// Nili May 30, 2012
				// This is for an immediate fix for AARESAA-8760. 
				// FIXME We need a proper solution for this. It's raised as AARESAA-8765
				// Assumptions made: 1. Currently only one bus service exists for an airline
				//                   2. 01 belongs to E5 and 02 belongs to G9.
				//                   3. Only 01 and 02 exists in Airarabia group.
				if ("01".equals(carrierCode)) {
					carrierCode = "E5";
				} else if ("02".equals(carrierCode)) {
					carrierCode = "G9";
				} else {
					carrierCode = cachedAirportDTO.getOperatedBy();
				}
			}

			if (airlineWiseSegmentSequence.get(carrierCode) == null) {
				if (isInterline) {
					airlineWiseSegmentSequence.put(carrierCode, 1);						
				} else {
					airlineWiseSegmentSequence.put(carrierCode, segment.getSegmentSeq());
				}
			} else {
				airlineWiseSegmentSequence.put(carrierCode, airlineWiseSegmentSequence.get(carrierCode) + 1);
			}
			for (Eticket eTicket : travelerEtickets) {
				if (eTicket.getLccSegmentSequence().equals(segment.getSegmentSeq())) {
					eTicket.setAirlineCode(carrierCode);
					eTicket.setSegmentSequence(airlineWiseSegmentSequence.get(carrierCode));
					break;
				}
			}
		}
	}

	/**
	 * @param lccReservationSegments
	 * @return
	 */
	public static List<LCCClientReservationSegment> getUnflownSegments(
			Collection<LCCClientReservationSegment> lccReservationSegments) throws ModuleException {
		List<LCCClientReservationSegment> unflownLccSegments = new ArrayList<LCCClientReservationSegment>();

		Iterator<LCCClientReservationSegment> segemnetIdIterator = lccReservationSegments.iterator();
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		while (segemnetIdIterator.hasNext()) {

			LCCClientReservationSegment lccResSegment = segemnetIdIterator.next();
			if (!lccResSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
					&& lccResSegment.getDepartureDateZulu().compareTo(currentDate) > 0) {
				unflownLccSegments.add(lccResSegment);
			}
		}
		Collections.sort(unflownLccSegments);
		return unflownLccSegments;
	}

	/**
	 * @param lccReservationSegments
	 */
	@Deprecated
	public static Collection<LCCClientReservationSegment> getFlightSegments(
			Collection<LCCClientReservationSegment> lccReservationSegments) throws ModuleException {
		Collection<LCCClientReservationSegment> flightSegments = new ArrayList<LCCClientReservationSegment>();

		for (LCCClientReservationSegment lccclientReservationSegment : lccReservationSegments) {
			if (!SurfaceSegmentUtil.isBusSegment(lccclientReservationSegment)) {
				flightSegments.add(lccclientReservationSegment);
			}
		}
		return flightSegments;

	}

	/**
	 * @param trackInfoDTO
	 * @return
	 */
	public static CreateTicketInfoDTO createTicketingInfoDto(TrackInfoDTO trackInfoDTO) {

		CreateTicketInfoDTO tktInfoDto = new CreateTicketInfoDTO();
		if (trackInfoDTO != null) {
			tktInfoDto.setAppIndicator(trackInfoDTO.getAppIndicator());
			tktInfoDto.setPaymentAgent(trackInfoDTO.getPaymentAgent());
		}
		return tktInfoDto;
	}
	
	public static List<LCCClientReservationPax> getETGenerationEligiblePax(Collection<LCCClientReservationPax> passengers,
			Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers, boolean isFirstPayment, boolean isInfantPaymentSeparated) {
		List<LCCClientReservationPax> etGenerationEligiblePax = new ArrayList<LCCClientReservationPax>();

		for (LCCClientReservationPax resPax : passengers) {
			if (!ReservationInternalConstants.PassengerType.INFANT.equals(resPax.getPaxType()) || isInfantPaymentSeparated) {
				if ((resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1
						&& paxSeqWisePayAssemblers.get(resPax.getPaxSequence()) != null && paxSeqWisePayAssemblers
						.get(resPax.getPaxSequence()).getTotalPayAmount().compareTo(resPax.getTotalAvailableBalance()) >= 0)
						|| (isFirstPayment && resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 0)) {
					etGenerationEligiblePax.add(resPax);
					if (!isInfantPaymentSeparated && !resPax.getInfants().isEmpty()) {
						for (LCCClientReservationPax infant : resPax.getInfants()) {
							etGenerationEligiblePax.add(infant);
						}
					}
				}
			}
		}
		return etGenerationEligiblePax;
	}
	
	public static List<LCCClientReservationPax> getETGenerationEligiblePaxFromPayment(
			Collection<LCCClientReservationPax> passengers, Map<Integer, LCCClientPaymentAssembler> paxSeqWisePayAssemblers) {
		List<LCCClientReservationPax> etGenerationEligiblePax = new ArrayList<LCCClientReservationPax>();

		for (LCCClientReservationPax resPax : passengers) {
			if (!ReservationInternalConstants.PassengerType.INFANT.equals(resPax.getPaxType())) {
				if (paxSeqWisePayAssemblers.get(resPax.getPaxSequence()) != null
						&& paxSeqWisePayAssemblers.get(resPax.getPaxSequence()).getTotalPayAmount().compareTo(BigDecimal.ZERO) == 1) {
					etGenerationEligiblePax.add(resPax);
					if (!resPax.getInfants().isEmpty()) {
						for (LCCClientReservationPax infant : resPax.getInfants()) {
							etGenerationEligiblePax.add(infant);
						}
					}
				}
			}
		}
		return etGenerationEligiblePax;
	}
}
