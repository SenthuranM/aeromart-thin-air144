package com.isa.thinair.lccclient.api.util;

import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Country;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 */
public class LCCClientApiUtils {

	public static int getLccPaymentBrokerRefNo(Map temporyPaymentMap) throws ModuleException {

		if (temporyPaymentMap.size() == 1) {
			CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
					.getFirstElement(temporyPaymentMap.values());

			return lccClientCreditCardPaymentInfo.getPaymentBrokerId();
		}

		throw new ModuleException("airreservations.temporyPayment.multipleCCTmpPayNotSupported");
	}

	public static String getPNRFromLccTmpPayMap(Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap) throws ModuleException {
		if (mapTempPayMap.size() == 1) {
			CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) BeanUtils
					.getFirstElement(mapTempPayMap.values());

			return lccClientCreditCardPaymentInfo.getGroupPNR();
		}

		throw new ModuleException("airreservations.temporyPayment.multipleCCTmpPayNotSupported");
	}

	public static Map updateLccTemporyPaymentReferences(int temporyPayId, int paymentBrokerRefNo,
			Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap) {
		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = mapTempPayMap.get(temporyPayId);
		lccClientCreditCardPaymentInfo.setPaymentBrokerId(paymentBrokerRefNo);
		return mapTempPayMap;
	}

	public static int getTmpPayIdFromTmpPayMap(Map mapTempPayMap) throws ModuleException {
		if (mapTempPayMap.size() == 1) {
			return (Integer) BeanUtils.getFirstElement(mapTempPayMap.keySet());
		}

		throw new ModuleException("airreservations.temporyPayment.multipleCCTmpPayNotSupported");
	}

	public static ContactInfo transform(CommonReservationContactInfo lcclientReservationContactInfo) throws ModuleException {
		if (lcclientReservationContactInfo == null) {
			return null;
		}
		ContactInfo contactInfo = new ContactInfo();
		contactInfo.setEmail(lcclientReservationContactInfo.getEmail());
		contactInfo.setMarketingCarrierCustomerId(lcclientReservationContactInfo.getCustomerId());

		contactInfo.setPersonName(transform(lcclientReservationContactInfo.getTitle(),
				lcclientReservationContactInfo.getFirstName(), lcclientReservationContactInfo.getLastName()));

		// TODO : Must take CountryCode + AreaCode into account (by splitting dashes in number)
		Telephone telephone = new Telephone();
		telephone.setPhoneNumber(lcclientReservationContactInfo.getPhoneNo());
		contactInfo.setTelephone(telephone);

		// TODO : Must take CountryCode + AreaCode into account (by splitting dashes in number)
		Telephone mobile = new Telephone();
		mobile.setPhoneNumber(lcclientReservationContactInfo.getMobileNo());
		contactInfo.setMobile(mobile);

		Address address = new Address();
		address.setCityName(lcclientReservationContactInfo.getCity());

		String addressLine1 = BeanUtils.nullHandler(lcclientReservationContactInfo.getStreetAddress1());
		if (!addressLine1.isEmpty()) {
			address.getAddressLine().add(addressLine1);
		}

		String addressLine2 = BeanUtils.nullHandler(lcclientReservationContactInfo.getStreetAddress2());
		if (!addressLine2.isEmpty()) {
			address.getAddressLine().add(addressLine2);
		}
		address.setState(lcclientReservationContactInfo.getState());
		Country country = new Country();
		address.setCountry(country);
		country.setCountryCode(lcclientReservationContactInfo.getCountryCode());
		country.setCountryName(LCCUtils.getCountryName(lcclientReservationContactInfo.getCountryCode()));
		contactInfo.setAddress(address);

		// TODO : Must take CountryCode + AreaCode into account (by splitting dashes in number)
		Telephone fax = new Telephone();
		contactInfo.setFax(fax);
		fax.setPhoneNumber(lcclientReservationContactInfo.getFax());

		contactInfo.setZipCode(lcclientReservationContactInfo.getZipCode());

		// Nationality Code
		Integer nationalityCode = lcclientReservationContactInfo.getNationalityCode();
		if (nationalityCode != null) {
			contactInfo.setNationalityCode(LCCUtils.getNationalityIsoCode(nationalityCode));
		}

		contactInfo.setPreferredLanguage(lcclientReservationContactInfo.getPreferredLanguage());
		contactInfo.setTaxRegNo(lcclientReservationContactInfo.getTaxRegNo());

		// set emergency contact
		contactInfo.setEmgnPersonName(transform(lcclientReservationContactInfo.getEmgnTitle(),
				lcclientReservationContactInfo.getEmgnFirstName(), lcclientReservationContactInfo.getEmgnLastName()));

		Telephone emgnPhone = new Telephone();
		contactInfo.setEmgnTelephone(emgnPhone);
		emgnPhone.setPhoneNumber(lcclientReservationContactInfo.getEmgnPhoneNo());

		contactInfo.setEmgnEmail(lcclientReservationContactInfo.getEmgnEmail());
		contactInfo.setSendPromoEmail(lcclientReservationContactInfo.getSendPromoEmail());

		return contactInfo;
	}

	public static PersonName transform(String title, String firstName, String lastName) {
		PersonName personName = new PersonName();
		personName.setTitle(title);
		personName.setFirstName(firstName);
		personName.setSurName(lastName);

		return personName;
	}

}
