package com.isa.thinair.lccclient.api.service;

import java.util.Map;

import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.InterlineBlockSeatRQInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

public interface LCCSearchAndQuoteBD {

	public static final String SERVICE_NAME = "LCCSearchAndQuoteService";

	public FlightAvailRS searchAvailableFlights(FlightAvailRQ flightAvailRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public FlightPriceRS quoteFlightPrice(FlightPriceRQ priceQuoteRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public ScheduleAvailRS getAvailableSchedules(ScheduleAvailRQ scheduleAvailRQ, BasicTrackInfo trackInfo)
			throws ModuleException;

	public FlightPriceRS changeFlightPrice(ChangeFareRQ changeFareRQ, BasicTrackInfo trackInfo) throws ModuleException;

	public boolean priviledgeUserBlockSeat(InterlineBlockSeatRQInfoTO infoTO, BasicTrackInfo trackInfo) throws ModuleException;

	public void removeBlockSeats(String trxID, BasicTrackInfo basicTrackInfo) throws ModuleException;

	public boolean isReservationOnholdable(Map<String, ResOnholdValidationDTO> carrierWiseOnholdDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public FareAvailRS searchFares(FareAvailRQ fareAvailRQ, UserPrincipal userPrinciple, BasicTrackInfo trackInfo)
			throws ModuleException;

	public ApplicablePromotionDetailsTO pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria,
			BasicTrackInfo trackInfo) throws ModuleException;
	
	public Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal)
			throws ModuleException;
}
