package com.isa.thinair.lccclient.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;

public interface LCCReservationBD {

	public static final String SERVICE_NAME = "LCCReservationService";

	public Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public LCCClientReservation searchReservationByPNR(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public LCCClientReservation book(CommonReservationAssembler lcclientReservationAssembler, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public ReservationBalanceTO getResAlterationBalanceSummary(Set<LCCClientReservationPax> passengers,
			LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public LCCClientReservation
			addSegments(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO, TrackInfoDTO trackInfoDTO)
					throws ModuleException;

	public List[] getGroundSegment(GroundSegmentTO groundSegmentTO, BasicTrackInfo trackInfo) throws ModuleException;

	public void modifyContactInfo(String pnr, String version, CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, String appIndicator, TrackInfoDTO trackInfo)
			throws ModuleException;

	public void modifyPassengerInfo(LCCClientReservation lccClientReservation, LCCClientReservation oldLccClientReservation,
	                                Collection<String> allowedOperations, String appIndicator, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version,
			BasicTrackInfo trackInfo, boolean isInfantPaymentSeparated) throws ModuleException;

	public LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version, BasicTrackInfo trackInfo)
			throws ModuleException;

	public LCCClientReservation adjustCharge(String groupPNr, String version, List<LCCClientChargeAdustment> chargeAdjustments,
			ClientCommonInfoDTO clientInfoDto, BasicTrackInfo trackInfo, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS,
			Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax) throws ModuleException;

	public LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version,
			BasicTrackInfo trackInfo) throws ModuleException;

	public List<UserNoteTO> searchReservationAudit(String pnr, String carrierCode, BasicTrackInfo trackInfo)
			throws ModuleException;

	public List<UserNoteTO> searchUserNoteHistory(String pnr, String carrierCode, BasicTrackInfo trackInfo, boolean isClassifyUN)
			throws ModuleException;

	public Map<Integer, CommonCreditCardPaymentInfo> makeTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo,
			Collection<LCCClientPaymentInfo> colLCCClientPaymentInfo, boolean isCredit, String originatorCarrierCode,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public Map<Integer, LCCClientPaymentAssembler> makePayment(CommonReservationContactInfo contactInfo, String pnr,
			String transactionIdentifier, Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void refundPayment(CommonReservationContactInfo contactInfo, String pnr, String transactionIdentifier,
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void clearSegmentAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public void transferSegments(LCCClientTransferSegmentTO clientTransferSegmentTO, BasicTrackInfo trackInfo, String salesCahnnelKey)
			throws ModuleException;

	public LCCClientReservation addInfant(LCCClientReservation lccClientReservation, TrackInfoDTO trackInfoDTO,
			Map<Integer, LCCClientPaymentAssembler> payments, boolean autoCancellationEnabled, boolean mcETGenerationEligible)
			throws ModuleException;

	public boolean refundPassengers(CommonReservationAssembler reservationAssembler, String userNotes, TrackInfoDTO trackInfoDTO,
			Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds, boolean isManualRefund)
			throws ModuleException;

	public LCCClientReservation removePassenger(String groupPnr, String version, List<LCCClientReservationPax> reservationPaxs,
			CustomChargesTO customChargesTO, TrackInfoDTO trackInfoDTO,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException;

	public PaxContactConfigDTO loadPaxContactConfig(String appName, List<String> carrierList, BasicTrackInfo trackInfo)
			throws ModuleException;

	public Collection<String> reprotect(LCCClientTransferSegmentTO transferSegmentTO, Map<Integer, String> fltSegToPnrSegMap)
			throws ModuleException;

	public void reconcileDummyCarrierReservation(LCCClientPnrModesDTO pnrModesDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public Map<String, Collection<ChargeAdjustmentTypeDTO>> getCarrierWiseChargeAdjustmentTypes(Set<String> carriercodes,
			BasicTrackInfo trackInfo) throws ModuleException;

	public Map<Pair<String, Integer>, Set<ETicketInfoTO>> getETicketInformation(List<String> lccUniqueIDs,
			BasicTrackInfo trackInfo) throws ModuleException;

	public boolean executeAutoCancellation() throws ModuleException;

	public BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public ServiceResponce lccRequoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfo)
			throws ModuleException;

	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ,  TrackInfoDTO trackInfo) throws ModuleException;
	
	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> paxCouponUpdateRQ,  TrackInfoDTO trackInfo) throws ModuleException;

	public ReservationDiscountDTO calculateDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;
	
	public ServiceResponce calculatePaxLoyaltyRedeemableAmounts(RedeemCalculateReq redeemCalculateReqTo,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException;

	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO) throws ModuleException;
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<LCCClientReservationPax> paxList, boolean getOnlyFirstElement, List<String> participatingOperatingCarriers, TrackInfoDTO trackInfoDTO)throws ModuleException;
	
	public LCCClientReservation reverseRefundableCharges(String groupPNr, String version, boolean isNoShowTaxReverse,
			List<LCCClientChargeReverse> chargeReverseList, ClientCommonInfoDTO clientInfoDto, BasicTrackInfo trackInfo)
			throws ModuleException;
	
	public String generateNewPnr(String originatingCarrier, BasicTrackInfo trackInfo)
			throws ModuleException;
	
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo, String operatingCarrier) throws ModuleException;

}
