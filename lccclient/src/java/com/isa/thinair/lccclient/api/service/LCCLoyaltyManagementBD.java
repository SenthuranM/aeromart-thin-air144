package com.isa.thinair.lccclient.api.service;


import com.isa.thinair.aircustomer.api.dto.lms.LMSMemberDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author rumesh
 * 
 */
public interface LCCLoyaltyManagementBD {
	public static final String SERVICE_NAME = "LoyaltyManagementService";

	/**
	 * Get loyalty member account to be merged
	 * 
	 * @param memberFFID
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public LMSMemberDTO getMergeLoyaltyManagement(String memberFFID, BasicTrackInfo trackInfo) throws ModuleException;

	/**
	 * Update loyalty customer name to be merged
	 * 
	 * @param ffid
	 * @param title TODO
	 * @param firstName TODO
	 * @param lastName TODO
	 * @return
	 * @throws ModuleException
	 */
	public int customerNameUpdate(String ffid, BasicTrackInfo trackInfo, String title, String firstName, String lastName) throws ModuleException;
}
