package com.isa.thinair.lccclient.api.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationRequest;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public interface LCCAncillaryBD {

	public static final String SERVICE_NAME = "LCCAncillaryService";

	public LCCSeatMapDTO getSeatMap(LCCSeatMapDTO lccSeatMapDTO, String selectedLanguage, BasicTrackInfo trackInfo)
			throws ModuleException;

	public AnciAvailabilityRS getAncillaryAvailability(LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO,
			BasicTrackInfo trackInfo) throws ModuleException;

	public LCCMealResponceDTO getAvailableMeals(List<LCCMealRequestDTO> lccMealRequestDTOs, String transactionIdentifier,
			String selectedLanguage, BasicTrackInfo trackInfo, Map<String, Set<String>> flightRefWiseSelectedMeals, String pnr)
			throws ModuleException;

	public LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, String selectedLanguage, LCCReservationBaggageSummaryTo baggageSummaryTo, String pnr,
			BasicTrackInfo trackInfo, boolean isRequote, Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID)
			throws ModuleException;

	public List<LCCFlightSegmentSSRDTO> getSpecialServiceRequests(List<FlightSegmentTO> flightSegmentTOs,
			String transactionIdentifier, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> flightRefWiseSelectedSSRs, boolean isModifyAnci) throws ModuleException;

	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAirportServiceRequests(
			List<FlightSegmentTO> flightSegmentTOs, Map<String, Integer> serviceCount, int serviceCategory,
			AppIndicatorEnum appIndicator, String transactionIdentifier, String selectedLanguage, BasicTrackInfo trackInfo,
			boolean isModifyAnci, String pnr) throws ModuleException;

	public boolean blockSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, BasicTrackInfo trackInfo)
			throws ModuleException;

	public boolean releaseSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, BasicTrackInfo trackInfo)
			throws ModuleException;

	public List<LCCInsuranceQuotationDTO> getInsuranceQuotations(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCInsuredPassengerDTO> insuredPassengerDTOs, LCCInsuredJourneyDTO insuredJourneyDTO,
			String transactionIdentifier, UserPrincipal userPrincipal, BasicTrackInfo trackInfo,
			LCCInsuredContactInfoDTO contactInfo) throws ModuleException;

	public LCCAnciModificationResponse modifyAncillary(LCCAnciModificationRequest anciModificationRequest) throws ModuleException;

	public AnciAvailabilityRS getTaxApplicabilityForAncillaries(String carrierCode, String segmentCode, EXTERNAL_CHARGES extChg,
			BasicTrackInfo trackInfo) throws ModuleException;

	public LCCAncillaryQuotation getSelectedAncillaryDetails(LCCAncillaryQuotation selectedAncillary, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
			throws ModuleException;
}
