package com.isa.thinair.lccclient.api.service;

import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRSDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRSDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.userInfoPublish.UserInfoPublishRSDataTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface LCCMasterDataPublisherBD {

	public static final String SERVICE_NAME = "LCCMasterDataPublisherSevice";

	public RouteInfoPublishRSDataTO publishRouteInfoToLCC(RouteInfoPublishRQDataTO routeInfoRS) throws ModuleException;

	public MasterDataPublishRSDataTO publishMasterDataToLCC(MasterDataPublishRQDataTO airportPublishRQDataTO)
			throws ModuleException;

	public Boolean publishOndCombinationsViaLCC(String invokingCarrierCode) throws ModuleException;

	public UserInfoPublishRSDataTO publishUserInfoToLCC(UserInfoPublishRQDataTO userInfoPublishRQDataTO) throws ModuleException;

}
