package com.isa.thinair.lccclient.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AutomaticCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAutomaticCheckins;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedAutoCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRS;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class LccAncillaryUtils {
    public static LCCSelectedAncillaryRQ populateLCCSelectedAncillaryRQ (LCCAncillaryQuotation ancillaryQuotation, UserPrincipal userPrincipal, BasicTrackInfo trackInfo)
            throws ModuleException {

        List<BookingFlightSegment> flightSegmentsList = (List<BookingFlightSegment>) getBookingFlightSegments(ancillaryQuotation);
        Map<String, BookingFlightSegment> flightSegments = new HashMap<>();

        for (BookingFlightSegment flightSegment : flightSegmentsList) {
            flightSegments.put(flightSegment.getFlightRefNumber(), flightSegment);
        }

        LCCSelectedAncillaryRQ selectedAncillaryRQ = new LCCSelectedAncillaryRQ();

        FlightSegmentTO flightSegmentTO;
        SelectedBaggage selectedBaggage;
        SelectedSeat selectedSeat;
        SelectedMeal selectedMeal;
        SelectedSSR selectedSSR;
        SelectedSSR selectedInflightService;
		SelectedAutoCheckin selectedAutoCheckin;

        for (LCCSelectedSegmentAncillaryDTO segmentAnci : ancillaryQuotation.getSegmentQuotations()) {
            flightSegmentTO = segmentAnci.getFlightSegmentTO();

            selectedBaggage = new SelectedBaggage();
            selectedBaggage.setFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
            if(segmentAnci.getBaggageDTOs() != null){
                for (LCCBaggageDTO baggage : segmentAnci.getBaggageDTOs()) {
                    selectedBaggage.getBaggageName().add(baggage.getBaggageName());
                    selectedBaggage.getBaggageTemplateId().add(Integer.parseInt(baggage.getOndBaggageChargeId()));
                }
                selectedAncillaryRQ.getSelectedBaggages().add(selectedBaggage);
            }


            selectedSeat = new SelectedSeat();
            selectedSeat.setFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));

            if (segmentAnci.getExtraSeatDTOs() != null) {
                for(LCCAirSeatDTO lccAirSeatDTO : segmentAnci.getExtraSeatDTOs()){
                    selectedSeat.getSeatNumber().add(lccAirSeatDTO.getSeatNumber());
                }
                selectedAncillaryRQ.getSelectedSeats().add(selectedSeat);
            }

            selectedMeal = new SelectedMeal();
            selectedMeal.setFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
            if(segmentAnci.getMealDTOs() != null){
                for(LCCMealDTO lccMealDTO : segmentAnci.getMealDTOs()){
                    selectedMeal.getMealCode().add(lccMealDTO.getMealCode());
                }
                selectedAncillaryRQ.getSelectedMeals().add(selectedMeal);
            }

            Map<String,SelectedSSR> airportWiseSelectedSSR = new HashMap<String, SelectedSSR>();

            if(segmentAnci.getAirportServiceDTOs() != null){
                for(LCCAirportServiceDTO lccAirportServiceDTO : segmentAnci.getAirportServiceDTOs()){
                    if(!airportWiseSelectedSSR.containsKey(lccAirportServiceDTO.getAirportCode())){
                        selectedSSR = new SelectedSSR();
                        BookingFlightSegment flightSegment = cloneBookingFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
                        flightSegment.setAirportCode(lccAirportServiceDTO.getAirportCode());
                        selectedSSR.setFlightSegment(flightSegment);
                        airportWiseSelectedSSR.put(lccAirportServiceDTO.getAirportCode(), selectedSSR);
                    }
                    selectedSSR = airportWiseSelectedSSR.get(lccAirportServiceDTO.getAirportCode());
                    selectedSSR.getSsrCode().add(lccAirportServiceDTO.getSsrCode());
                }
            }

            if(airportWiseSelectedSSR.size() > 0){
                for(String airport : airportWiseSelectedSSR.keySet()){
                    selectedAncillaryRQ.getSelectedAirportServices().add(airportWiseSelectedSSR.get(airport));
                }
            }

            selectedInflightService = new SelectedSSR();
            selectedInflightService.setFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
            if(segmentAnci.getSpecialServiceRequestDTOs() != null){
                for(LCCSpecialServiceRequestDTO lccSpecialServiceRequestDTO : segmentAnci.getSpecialServiceRequestDTOs()){
                    selectedInflightService.getSsrCode().add(lccSpecialServiceRequestDTO.getSsrCode());
                }
                selectedAncillaryRQ.getSelectedInflightServices().add(selectedInflightService);
            }
            
            Map<String,SelectedSSR> airportWiseSelectedAPT = new HashMap<String, SelectedSSR>();            
            if(segmentAnci.getAirportTransferDTOs() != null){
                for(LCCAirportServiceDTO lccAirportTransferDTO : segmentAnci.getAirportTransferDTOs()){
                	
                	 if(!airportWiseSelectedAPT.containsKey(lccAirportTransferDTO.getAirportCode())){
                         selectedSSR = new SelectedSSR();
                         BookingFlightSegment flightSegment = cloneBookingFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
                         flightSegment.setAirportCode(lccAirportTransferDTO.getAirportCode());
                         flightSegment.setAirportType(lccAirportTransferDTO.getApplyOn());
                         selectedSSR.setFlightSegment(flightSegment);
                         airportWiseSelectedAPT.put(lccAirportTransferDTO.getAirportCode(), selectedSSR);
                     }
                     selectedSSR = airportWiseSelectedAPT.get(lccAirportTransferDTO.getAirportCode());
                     selectedSSR.getSsrCode().add(lccAirportTransferDTO.getAirportTransferId().toString());
                }
            }
			if (segmentAnci.getAutomaticCheckinDTOs() != null) {
				selectedAutoCheckin = new SelectedAutoCheckin();
				selectedAutoCheckin.setFlightSegment(flightSegments.get(flightSegmentTO.getFlightRefNumber()));
				for (LCCAutomaticCheckinDTO lccAutomaticCheckinDTO : segmentAnci.getAutomaticCheckinDTOs()) {
					if (!airportWiseSelectedAPT.containsKey(lccAutomaticCheckinDTO.getAutoCheckinId())) {
						//Below check is to avoid duplicate template ids
						if (!selectedAutoCheckin.getAutoCheckinId().contains(lccAutomaticCheckinDTO.getAutoCheckinId())) {
							selectedAutoCheckin.getAutoCheckinId().add(lccAutomaticCheckinDTO.getAutoCheckinId());
						}
					}
				}
				selectedAncillaryRQ.getSelectedAutoCheckins().add(selectedAutoCheckin);

			}
            
            if(airportWiseSelectedAPT.size() > 0){
                for(String airport : airportWiseSelectedAPT.keySet()){
                    selectedAncillaryRQ.getSelectedAirportTransfer().add(airportWiseSelectedAPT.get(airport));
                }
            }

        }

        return selectedAncillaryRQ;
    }

    public static LCCAncillaryQuotation populateLCCSelectedAncillaryRS(LCCSelectedAncillaryRS lccSelectedAncillaryRS)
            throws ModuleException {
        LCCAncillaryQuotation lccAncillaryQuotation = new LCCAncillaryQuotation();

        LCCSelectedSegmentAncillaryDTO lccSelectedSegmentAncillaryDTO;
        Map<String , LCCSelectedSegmentAncillaryDTO> flightSegmentWiseAncillaryRSMap = new HashMap<String, LCCSelectedSegmentAncillaryDTO>();

        List<FlightSegmentAirportServiceRequests> airportServices = lccSelectedAncillaryRS.getAirportServices();
        List<FlightSegmentBaggages> baggages = lccSelectedAncillaryRS.getBaggages();
        List<FlightSegmentSpecialServiceRequests> inflightServices = lccSelectedAncillaryRS.getInflightServices();
        List<FlightSegmentMeals> meals = lccSelectedAncillaryRS.getMeals();
        List<FlightSegmentSeats> seats = lccSelectedAncillaryRS.getSeats();
        List<FlightSegmentAirportServiceRequests> airportTransfers = lccSelectedAncillaryRS.getAirportTransfers();
		List<FlightSegmentAutomaticCheckins> automaticCheckins = lccSelectedAncillaryRS.getAutomaticCheckins();

        List<LCCAirportServiceDTO> airportServiceDTOs;
        LCCAirportServiceDTO lccAirportServiceDTO;
        List<LCCAirSeatDTO> seatDTOs;
        LCCAirSeatDTO lccAirSeatDTO;
        List<LCCBaggageDTO> baggageDTOs;
        LCCBaggageDTO lccBaggageDTO;
        List<LCCMealDTO> mealDTOs;
        LCCMealDTO lccMealDTO;
        List<LCCSpecialServiceRequestDTO> specialServiceRequestDTOs;
        LCCSpecialServiceRequestDTO lccSpecialServiceRequestDTO;
        List<LCCAirportServiceDTO> airportTransferDTOs;
        LCCAirportServiceDTO airportTransferDTO;
		List<LCCAutomaticCheckinDTO> lccAutomaticCheckinDTOs;
		LCCAutomaticCheckinDTO lccAutomaticCheckinDTO;
        String fltSegRefNumber;


        for(FlightSegmentAirportServiceRequests airportService : airportServices){

            airportServiceDTOs = new ArrayList<LCCAirportServiceDTO>();
            fltSegRefNumber = airportService.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(airportService.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(AirportServiceRequest airPortServiceRequest : airportService.getAirportServices()){
                lccAirportServiceDTO = new LCCAirportServiceDTO();
                lccAirportServiceDTO.setAdultAmount(airPortServiceRequest.getAdultAmount());
                lccAirportServiceDTO.setAirportCode(airPortServiceRequest.getAirportCode());
                lccAirportServiceDTO.setApplicabilityType(airPortServiceRequest.getApplicabilityType());
                lccAirportServiceDTO.setApplyOn(airPortServiceRequest.getApplyOn());
                lccAirportServiceDTO.setCarrierCode(airPortServiceRequest.getCarrierCode());
                lccAirportServiceDTO.setChildAmount(airPortServiceRequest.getChildAmount());
                lccAirportServiceDTO.setSsrDescription(airPortServiceRequest.getDescription());
                lccAirportServiceDTO.setReservationAmount(airPortServiceRequest.getReservationAmount());
                lccAirportServiceDTO.setInfantAmount(airPortServiceRequest.getInfantAmount());
                lccAirportServiceDTO.setServiceCharge(airPortServiceRequest.getServiceCharge());
                lccAirportServiceDTO.setSsrCode(airPortServiceRequest.getSsrCode());
                lccAirportServiceDTO.setSsrImagePath(airPortServiceRequest.getSsrImagePath());
                lccAirportServiceDTO.setSsrName(airPortServiceRequest.getSsrName());
                lccAirportServiceDTO.setSsrThumbnailImagePath(airPortServiceRequest.getSsrThumbnailImagePath());
                lccAirportServiceDTO.setStatus(airPortServiceRequest.getStatus());
                airportServiceDTOs.add(lccAirportServiceDTO);
            }

            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportServiceDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportServiceDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportServiceDTOs().addAll(airportServiceDTOs);
            } else {
            	 flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setAirportServiceDTOs(airportServiceDTOs);
            }

        }

        for(FlightSegmentBaggages segmentBaggage : baggages){
            baggageDTOs = new ArrayList<LCCBaggageDTO>();
            fltSegRefNumber = segmentBaggage.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(segmentBaggage.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(Baggage baggage : segmentBaggage.getBaggage()){
                lccBaggageDTO = new LCCBaggageDTO();
                lccBaggageDTO.setBaggageCharge(baggage.getBaggageCharge());
                lccBaggageDTO.setBaggageDescription(baggage.getBaggageDescription());
                lccBaggageDTO.setBaggageName(baggage.getBaggageName());
                lccBaggageDTO.setDefaultBaggage(baggage.getDefaultBaggage());
                lccBaggageDTO.setOndBaggageChargeId(baggage.getOndChargeId());
                lccBaggageDTO.setOndBaggageGroupId(baggage.getOndGroupId());
                baggageDTOs.add(lccBaggageDTO);
            }
            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getBaggageDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getBaggageDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getBaggageDTOs().addAll(baggageDTOs);
            } else {
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setBaggageDTOs(baggageDTOs);
            }
        }

        for(FlightSegmentSeats segmentSeat : seats){
            seatDTOs = new ArrayList<LCCAirSeatDTO>();
            fltSegRefNumber = segmentSeat.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(segmentSeat.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(Seat seat : segmentSeat.getSeat()){
                lccAirSeatDTO = new LCCAirSeatDTO();
                lccAirSeatDTO.setLogicalCabinClassCode(seat.getLogicalCabinClass());
                if(seat.getNotAllowedPassengerType() != null){
                    for(PassengerType passengertType : seat.getNotAllowedPassengerType()){
                        if(lccAirSeatDTO.getNotAllowedPassengerType() == null){
                            lccAirSeatDTO.setNotAllowedPassengerType(new ArrayList<String>());
                        }
                        lccAirSeatDTO.getNotAllowedPassengerType().add(passengertType.name());
                    }
                }
                lccAirSeatDTO.setSeatAvailability(seat.getSeatAvailability());
                lccAirSeatDTO.setSeatCharge(seat.getSeatCharge());
                lccAirSeatDTO.setSeatMessage(seat.getSeatMessage());
                lccAirSeatDTO.setSeatNumber(seat.getSeatNumber());
                seatDTOs.add(lccAirSeatDTO);
            }
            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getExtraSeatDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getExtraSeatDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getExtraSeatDTOs().addAll(seatDTOs);
            } else {
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setExtraSeatDTOs(seatDTOs);
            }
        }

        for(FlightSegmentMeals segmentMeal : meals){
            mealDTOs = new ArrayList<LCCMealDTO>();
            fltSegRefNumber = segmentMeal.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(segmentMeal.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(Meal meal : segmentMeal.getMeal()){
                lccMealDTO = new LCCMealDTO();
                lccMealDTO.setAllocatedMeals(meal.getAllocatedMeals());
                lccMealDTO.setAvailableMeals(meal.getAvailableMeals());
                lccMealDTO.setMealCategoryCode(meal.getMealCategoryCode());
                lccMealDTO.setMealCategoryID(meal.getMealCategoryID());
                lccMealDTO.setMealCharge(meal.getMealCharge());
                lccMealDTO.setMealCode(meal.getMealCode());
                lccMealDTO.setMealDescription(meal.getMealDescription());
                lccMealDTO.setMealImagePath(meal.getMealImageLink());
                lccMealDTO.setMealName(meal.getMealName());
                lccMealDTO.setMealStatus(meal.getMealStatus());
				lccMealDTO.setPopularity(meal.getPopularity());
                lccMealDTO.setOfferedTemplateID(meal.getOfferedAnciTemplateID());
                lccMealDTO.setSoldMeals(meal.getSoldMeals());
                lccMealDTO.setTranslatedMealDescription(meal.getTranslatedMealDescription());
                lccMealDTO.setTranslatedMealTitle(meal.getTranslatedMealTitle());
                mealDTOs.add(lccMealDTO);
            }
            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getMealDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getMealDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getMealDTOs().addAll(mealDTOs);
            } else {
            	  flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setMealDTOs(mealDTOs);
            }
          
        }

        for(FlightSegmentSpecialServiceRequests segmentInflightService : inflightServices){
            specialServiceRequestDTOs = new ArrayList<LCCSpecialServiceRequestDTO>();
            fltSegRefNumber = segmentInflightService.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(segmentInflightService.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(SpecialServiceRequest inflightService : segmentInflightService.getSpecialServiceRequest()){
                lccSpecialServiceRequestDTO = new LCCSpecialServiceRequestDTO();
                lccSpecialServiceRequestDTO.setAvailableQty(inflightService.getAvailableQty());
                lccSpecialServiceRequestDTO.setCarrierCode(inflightService.getCarrierCode());
                lccSpecialServiceRequestDTO.setCharge(inflightService.getCharge());
                lccSpecialServiceRequestDTO.setDescription(inflightService.getDescription());
                lccSpecialServiceRequestDTO.setServiceQuantity(inflightService.getServiceQuantity());
                lccSpecialServiceRequestDTO.setSsrCode(inflightService.getSsrCode());
                lccSpecialServiceRequestDTO.setSsrName(inflightService.getSsrName());
                lccSpecialServiceRequestDTO.setStatus(inflightService.getStatus());
                lccSpecialServiceRequestDTO.setText(inflightService.getText());
                specialServiceRequestDTOs.add(lccSpecialServiceRequestDTO);
            }
            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getSpecialServiceRequestDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getSpecialServiceRequestDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getSpecialServiceRequestDTOs().addAll(specialServiceRequestDTOs);
            } else {
                flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setSpecialServiceRequestDTOs(specialServiceRequestDTOs);
            }
      
        }
        
        for(FlightSegmentAirportServiceRequests airportTransfer : airportTransfers){

        	airportTransferDTOs = new ArrayList<LCCAirportServiceDTO>();
            fltSegRefNumber = airportTransfer.getFlightSegment().getFlightRefNumber();
            if(!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)){
                lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
                lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(airportTransfer.getFlightSegment()));
                flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
            }

            for(AirportServiceRequest airportTransferRequest : airportTransfer.getAirportServices()){
            	airportTransferDTO = new LCCAirportServiceDTO();
            	airportTransferDTO.setAdultAmount(airportTransferRequest.getAdultAmount());
            	airportTransferDTO.setAirportCode(airportTransferRequest.getAirportCode());
            	airportTransferDTO.setApplicabilityType(airportTransferRequest.getApplicabilityType());
            	airportTransferDTO.setApplyOn(airportTransferRequest.getApplyOn());
            	airportTransferDTO.setCarrierCode(airportTransferRequest.getCarrierCode());
            	airportTransferDTO.setChildAmount(airportTransferRequest.getChildAmount());
            	airportTransferDTO.setSsrDescription(airportTransferRequest.getDescription());
            	airportTransferDTO.setReservationAmount(airportTransferRequest.getReservationAmount());
            	airportTransferDTO.setInfantAmount(airportTransferRequest.getInfantAmount());
            	airportTransferDTO.setServiceCharge(airportTransferRequest.getServiceCharge());
            	airportTransferDTO.setSsrCode(airportTransferRequest.getSsrCode());
            	airportTransferDTO.setSsrImagePath(airportTransferRequest.getSsrImagePath());
            	airportTransferDTO.setSsrName(airportTransferRequest.getSsrName());
            	airportTransferDTO.setSsrThumbnailImagePath(airportTransferRequest.getSsrThumbnailImagePath());
            	airportTransferDTO.setStatus(airportTransferRequest.getStatus());
            	airportTransferDTO.setAirportTransferId(Integer.parseInt(airportTransferRequest.getSsrCode()));
            	airportTransferDTOs.add(airportTransferDTO);
            }

            if(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportTransferDTOs() != null && flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportTransferDTOs().size() > 0){
            	flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAirportTransferDTOs().addAll(airportTransferDTOs);
            } else {
            	 flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setAirportTransferDTOs(airportTransferDTOs);
            }

        }

		for (FlightSegmentAutomaticCheckins segmentAutomaticCheckin : automaticCheckins) {
			lccAutomaticCheckinDTOs = new ArrayList<LCCAutomaticCheckinDTO>();
			fltSegRefNumber = segmentAutomaticCheckin.getFlightSegment().getFlightRefNumber();
			if (!flightSegmentWiseAncillaryRSMap.containsKey(fltSegRefNumber)) {
				lccSelectedSegmentAncillaryDTO = new LCCSelectedSegmentAncillaryDTO();
				lccSelectedSegmentAncillaryDTO.setFlightSegmentTO(getFlightSegmentTO(segmentAutomaticCheckin.getFlightSegment()));
				flightSegmentWiseAncillaryRSMap.put(fltSegRefNumber, lccSelectedSegmentAncillaryDTO);
			}

			for (AutomaticCheckin automaticCheckin : segmentAutomaticCheckin.getAutomaticCheckin()) {
				lccAutomaticCheckinDTO = new LCCAutomaticCheckinDTO();
				lccAutomaticCheckinDTO.setAirportCode(automaticCheckin.getAirportCode());
				lccAutomaticCheckinDTO.setAutomaticCheckinCharge(automaticCheckin.getAutomaticCheckinCharge());
				lccAutomaticCheckinDTO.setStatus(automaticCheckin.getStatus());
				lccAutomaticCheckinDTO.setAutoCheckinId(automaticCheckin.getAutoCheckinTempId());
				lccAutomaticCheckinDTOs.add(lccAutomaticCheckinDTO);
			}
			if (flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAutomaticCheckinDTOs() != null
					&& flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAutomaticCheckinDTOs().size() > 0) {
				flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).getAutomaticCheckinDTOs().addAll(lccAutomaticCheckinDTOs);
			} else {
				flightSegmentWiseAncillaryRSMap.get(fltSegRefNumber).setAutomaticCheckinDTOs(lccAutomaticCheckinDTOs);
			}

		}

        if(flightSegmentWiseAncillaryRSMap.size() > 0 ){
            for(String fltSegRefNumberKey : flightSegmentWiseAncillaryRSMap.keySet()){
                if(lccAncillaryQuotation.getSegmentQuotations() == null){
                    lccAncillaryQuotation.setSegmentQuotations(new ArrayList<LCCSelectedSegmentAncillaryDTO>());
                }
                lccAncillaryQuotation.getSegmentQuotations().add(flightSegmentWiseAncillaryRSMap.get(fltSegRefNumberKey));
            }
        } else {
            lccAncillaryQuotation.setSegmentQuotations(new ArrayList<>());
        }

        lccAncillaryQuotation.setInsuranceQuotations(new ArrayList<>());

        return lccAncillaryQuotation;
    }



    public static List<? extends FlightSegment> getBookingFlightSegments(LCCAncillaryQuotation ancillaryQuotation)
            throws ModuleException {
        List<BookingFlightSegment> flightSegments = new ArrayList<>();
        BookingFlightSegment flightSegment;
        FlightSegmentTO flightSegmentTO;

        for (LCCSelectedSegmentAncillaryDTO segmentAncillary : ancillaryQuotation.getSegmentQuotations())  {
            flightSegmentTO = segmentAncillary.getFlightSegmentTO();

            flightSegment = new BookingFlightSegment();
            flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
            flightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
            flightSegment.setDepatureDateTime(flightSegmentTO.getDepartureDateTime());
            flightSegment.setDepatureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
            flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
            flightSegment.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
            flightSegment.setOperatingAirline(flightSegmentTO.getOperatingAirline());
            flightSegment.setReturnFlag(flightSegmentTO.isReturnFlag() ? "Y" : "N");
            flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
            flightSegment.setSegmentSequence(flightSegmentTO.getSegmentSequence());
            flightSegment.setOndSequence(flightSegmentTO.getOndSequence());
            flightSegment.setCabinClassCode(flightSegmentTO.getCabinClassCode());
            flightSegment.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
            flightSegment.setAirportCode(flightSegmentTO.getAirportCode());

            flightSegment.setBaggageONDGroupId(flightSegmentTO.getBaggageONDGroupId());
            flightSegments.add(flightSegment);
        }

        return flightSegments;
    }

    private static FlightSegmentTO getFlightSegmentTO(BookingFlightSegment bookingFlightSegment) {
        FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
        flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
        flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu());
        flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
        flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());
        flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
        flightSegmentTO.setFlightRefNumber(bookingFlightSegment.getFlightRefNumber());
        flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
        if(bookingFlightSegment.getReturnFlag().equals("Y")){
            flightSegmentTO.setReturnFlag(true);
        } else {
            flightSegmentTO.setReturnFlag(false);
        }
        flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
        flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
        flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
        flightSegmentTO.setBaggageONDGroupId(bookingFlightSegment.getBaggageONDGroupId());
        return flightSegmentTO;
    }
    
    private static BookingFlightSegment cloneBookingFlightSegment(BookingFlightSegment originalBookingFlightSegment){
    	BookingFlightSegment bookingFlightSegment =  new BookingFlightSegment();
    	bookingFlightSegment.setArrivalDateTime(originalBookingFlightSegment.getArrivalDateTime());
    	bookingFlightSegment.setArrivalDateTimeZulu(originalBookingFlightSegment.getArrivalDateTimeZulu());
    	bookingFlightSegment.setDepatureDateTime(originalBookingFlightSegment.getDepatureDateTime());
    	bookingFlightSegment.setDepatureDateTimeZulu(originalBookingFlightSegment.getDepatureDateTimeZulu());
    	bookingFlightSegment.setFlightNumber(originalBookingFlightSegment.getFlightNumber());
    	bookingFlightSegment.setFlightRefNumber(originalBookingFlightSegment.getFlightRefNumber());
    	bookingFlightSegment.setOperatingAirline(originalBookingFlightSegment.getOperatingAirline());
    	bookingFlightSegment.setReturnFlag(originalBookingFlightSegment.getReturnFlag());
    	bookingFlightSegment.setSegmentCode(originalBookingFlightSegment.getSegmentCode());
        bookingFlightSegment.setSegmentSequence(originalBookingFlightSegment.getSegmentSequence());
        bookingFlightSegment.setOndSequence(originalBookingFlightSegment.getOndSequence());
        bookingFlightSegment.setCabinClassCode(originalBookingFlightSegment.getCabinClassCode());
        bookingFlightSegment.setLogicalCabinClassCode(originalBookingFlightSegment.getLogicalCabinClassCode());
        bookingFlightSegment.setAirportCode(originalBookingFlightSegment.getAirportCode());
        bookingFlightSegment.setBaggageONDGroupId(originalBookingFlightSegment.getBaggageONDGroupId());
        
        return bookingFlightSegment;
    }
     
}
