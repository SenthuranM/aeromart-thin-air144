package com.isa.thinair.lccclient.api.service;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoCriteriaDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public interface LCCFlightServiceBD {
	public static final String SERVICE_NAME = "LCCFlightService";

	public Page searchFlightsForDisplay(FlightManifestSearchDTO searchDTO, BasicTrackInfo trackInfo) throws ModuleException;

	public Object getFlightManifestData(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public List<FlightLoadReportDataDTO> getFlightLoadData(ReportsSearchCriteria search, BasicTrackInfo trackInfo)
			throws ModuleException;

	public List<FlightSegmentInfoDTO> getFlightSegmentInfo(FlightSegmentInfoCriteriaDTO flightSegmentInfoCriteriaDTO, BasicTrackInfo trackInfo)
			throws ModuleException;
	
	public FlightSegement getFlightSegment(String flightNumber, Date departureDate)
			throws ModuleException;
}
