package com.isa.thinair.reporting.core.persistent.dao;

import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.reporting.core.persistent.dao.ReportsDAO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.util.InternalConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class TestReportsDAO extends PlatformTestCase {

	private ReportsDAO reportsDAO = null;
		
	protected void setUp() throws Exception {
		super.setUp();
		reportsDAO = 
			(ReportsDAO) ReportingUtils.getInstance().getLocalBean(
			InternalConstants.DAOProxyNames.REPORTS_DAO);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
