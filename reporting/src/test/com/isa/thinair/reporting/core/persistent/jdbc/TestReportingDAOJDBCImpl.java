package com.isa.thinair.reporting.core.persistent.jdbc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.commons.api.dto.Page;

import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;

public class TestReportingDAOJDBCImpl extends PlatformTestCase {
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void TestGetFlightsForInventoryUpdating(){
		FlightSearchCriteria criteria = new FlightSearchCriteria();
//		criteria.setOrigin("SHJ");
//		criteria.setDestination("LXR");
//		List viaPoints = new ArrayList();
//		viaPoints.add("SSH");
//		criteria.setViaPoints(viaPoints);
//        criteria.setCabinClassCode("Y");
		criteria.setFlightNumber("%");
        //FIXME
        //criteria.setFlightStatus(null);
        criteria.setMatchExactONDViaPointsCombinationOnly(false);
		GregorianCalendar gFrom = new GregorianCalendar(2008, Calendar.JANUARY, 1);
		GregorianCalendar gTo = new GregorianCalendar(2008, Calendar.JANUARY, 1);
		criteria.setDepartureDateFrom(gFrom.getTime());
		criteria.setDepartureDateTo(gTo.getTime());
		
		ReportingDAOJDBCImpl reportingJDBC = getReportingDAO();
		Page page = reportingJDBC.getFlightsForInventoryUpdating(criteria, 1, 20);
		assertNotNull(page.getPageData());
        assertEquals(page.getTotalNoOfRecords(), page.getPageData().size());
        
        System.out.println(page.getPageData().toString());
        Iterator fccInventoryDTOsIt = page.getPageData().iterator();
        while (fccInventoryDTOsIt.hasNext()){
            FlightInventorySummaryDTO fltSummaryDTO = (FlightInventorySummaryDTO)fccInventoryDTOsIt.next();
            System.out.println("flightId="+fltSummaryDTO.getFlightId()+" arrival="+fltSummaryDTO.getArrivalDateTime()+" depature="+fltSummaryDTO.getDepartureDateTime() + " viaPoints="+fltSummaryDTO.getViaPoints());
            Iterator fccInvDTOIt = fltSummaryDTO.getFccInventorySummaryDTO().iterator();
            while (fccInvDTOIt.hasNext()){
                FCCInventorySummaryDTO fccInvDTO = (FCCInventorySummaryDTO)fccInvDTOIt.next();
            
                System.out.println("fccaId="+fccInvDTO.getFccInvId() + " ccCode=" +fccInvDTO.getCabinClassCode());
                Iterator fccSegInvDTOsIT = fccInvDTO.getFccSegInventorySummary().iterator();
                while (fccSegInvDTOsIT.hasNext()){
                    FCCSegInvSummaryDTO fccSegInvDTO = (FCCSegInvSummaryDTO)fccSegInvDTOsIT.next();
                    System.out.println("fccsaId="+ fccSegInvDTO.getFccsInvId()+" segCode="+fccSegInvDTO.getSegmentCode());
         
                }
            }
        }
		System.out.println("Total no of records = " + page.getTotalNoOfRecords());
	}
    
    public void testGetFCCSeatMovementSummary(){
        ReportingDAOJDBCImpl reportingJDBC = getReportingDAO();
        FCCAgentsSeatMvDTO fccAgentsSeatMv = reportingJDBC.getFCCAgentsSeatMovementSummary(21429, "Y");
        assertNotNull(fccAgentsSeatMv);
    }
	
	public void TestGetFlightScheduleDetails(){
//		FlightScheduleDetailsSearchCriteria criteria = new FlightScheduleDetailsSearchCriteria();
//		List scheduleIds = new ArrayList();		
//		scheduleIds.add(new Integer(20021));
//		scheduleIds.add(new Integer(20023));
//		scheduleIds.add(new Integer(20024));
//		scheduleIds.add(new Integer(20025));
//		criteria.setSelectedScheduleIds(scheduleIds);
//		ReportingDAOJDBCImpl reportingJDBC = new ReportingDAOJDBCImpl();
//		Collection col = reportingJDBC.getFlightScheduleDetails(criteria);
//		System.out.println("Printing FlightScheduleDetails for selected schedule Ids... ");
//		printingFlightSchduleDetails(col);
//		
//		criteria = new FlightScheduleDetailsSearchCriteria();
//		criteria.setBuildStatus("BLT");
//		criteria.setFromAirport("CMB");
//		criteria.setOperationType(2);
//		criteria.setStartDate(new Date(107, 1, 1));
//		criteria.setStopDate(new Date(107, 3, 31));
//		Date date = new Date(107, 1, 1);		
//		criteria.setStatus("ACT");
//		col = reportingJDBC.getFlightScheduleDetails(criteria);
//		System.out.println("Printing FlightScheduleDetails for selected options... ");
//		printingFlightSchduleDetails(col);
	}
	
	private void printingFlightSchduleDetails(Collection col){
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			FlightScheduleDetailDTO flightScheduleDetailDTO = (FlightScheduleDetailDTO) iter.next();
//			System.out.println("Flight number : " + flightScheduleDetailDTO.getFlightNumber() + 
//					"     Schdule Id : " + String.valueOf(flightScheduleDetailDTO.getScheduleId()));
//			System.out.println("Printing FlightScheduleDetails... Segments");
//			
//			for (Iterator iterator = flightScheduleDetailDTO.getFlightScheduleDetailSegmentDTOs().iterator(); iterator.hasNext();) {
//				FlightScheduleDetailSegmentDTO flightScheduleDetailSegmentDTO = (FlightScheduleDetailSegmentDTO) iterator.next();
//				System.out.println("  Segment: " + flightScheduleDetailSegmentDTO.getSegmentCode() + 
//						" PNRs : " + String.valueOf(flightScheduleDetailSegmentDTO.getNumberOfPNRs()) );
//			}			
//		}
	}
	
	
	public void TestGetRetreiveFligtScheduleCapacityDetails(){
//		FlightScheduleCapacityVarianceSearchCriteria criteria = new FlightScheduleCapacityVarianceSearchCriteria();
//		criteria.setStartDate(new Date(107, 0, 1));
//		criteria.setStopDate(new Date(107, 2, 31));
//		
//		
//		ReportingDAOJDBCImpl reportingJDBC = new ReportingDAOJDBCImpl();
//		Collection col = reportingJDBC.getRetreiveFligtScheduleCapacityDetails(criteria);
//		System.out.println("Printing FligtScheduleCapacityDetails... " + String.valueOf(col.size()));
//		printingFligtScheduleCapacityDetails(col);		
		
	}
	
	private void printingFligtScheduleCapacityDetails(Collection col){
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			FlightScheduleCapacityVarianceDTO dto = (FlightScheduleCapacityVarianceDTO) iter.next();
//			System.out.println("Origin: " + dto.getOrigin() +
//					" Destination: " + dto.getDestination() +
//					" Via Points: " + dto.getViaPoints() +
//					" No. of Flights: " + dto.getCurrentYearNumberOfFlights().toString() +
//					" ASKs: " + dto.getCurrentYearASK().toString() +
//					" No. of PAX: " + dto.getCurrentYearNumberOfPAX().toString() +
//					" Var ASK: " + dto.getASKVariance().toString() +
//					" Var No. of Flights: " + dto.getNumberOfFlightsVariance().toString() + 
//					" Var No. of PAX: " + dto.getNumberOfPAXVariance().toString());			
//		}
	}
	
//	public void TestGetInboundOutboundFlightSchedule(){
//		InboundOutboundFlightScheduleValidationDetailsSearchCriteria criteria = new InboundOutboundFlightScheduleValidationDetailsSearchCriteria();
//		criteria.setStartDate(new Date(106, 10, 1));
//		criteria.setStopDate(new Date(106, 10, 30));		
//		
//		ReportingDAOJDBCImpl reportingJDBC = new ReportingDAOJDBCImpl();
//		Collection col = reportingJDBC.getInboundOutboundFlightSchedule(criteria);
//		System.out.println("Printing InboundOutboundFlightScheduleValidationDetails... " + String.valueOf(col.size()));
//		printingInboundOutboundFlightSchedule(col);		
//		
//	}
//	
//	private void printingInboundOutboundFlightSchedule(Collection col){
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			InboundOutboundFlightScheduleValidationDetailsDTO dto = (InboundOutboundFlightScheduleValidationDetailsDTO) iter.next();
//			System.out.println("Out - Segment: " + dto.getOutboundSegment() +
//					" Dparture Time Local: " + dto.getOutboundDepartureTimeLocal().toString());
//			System.out.println("In - Segment: " + dto.getInboundSegment() +
//					" Dparture Time Local: " + dto.getInboundDepartureTimeLocal().toString());
//		}
//	}
//	
//	
//	public void TestGetSeatInventoryFareMovementData(){
//		SeatInventoryFareSearchCriteria criteria = new SeatInventoryFareSearchCriteria();		
////		criteria.setOrigin("CMB");
////		criteria.setDestination("KHI");
////		List viaPoints = new ArrayList();
////		viaPoints.add("DEL");
////		criteria.setViaPoints(viaPoints);
////		criteria.setFlightNumber("%");
////		GregorianCalendar gFrom = new GregorianCalendar(2005, 12, 1);
////		GregorianCalendar gTo = new GregorianCalendar(2005, 12, 5);
////		criteria.setDepartureDateFrom(gFrom.getTime());
////		criteria.setDepartureDateTo(gTo.getTime());
//		
//		criteria.setDepartureDateFrom(new Date(105, 11, 1));
//		criteria.setDepartureDateTo(new Date(105, 11, 5));
//		criteria.setShowAllCombinations(true);
//		
//		ReportingDAOJDBCImpl reportingJDBC = new ReportingDAOJDBCImpl();
//		Page page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertNotNull("Retrieved Page object is null",page);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",8,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",5,page.getPageData().size());
//		
//		criteria.setDepartureAirport("SHJ");
//		criteria.setArrivalAirport("LXR");
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",2,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",2,page.getPageData().size());
//		
//		criteria.setFlightNumber("G9500 ");
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",2,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",2,page.getPageData().size());
//		
//		List viaPointsList = new ArrayList();
//		viaPointsList.add("SSH");
//		criteria.setViaPoints(viaPointsList);
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",2,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",2,page.getPageData().size());
//		
//		viaPointsList = new ArrayList();
//		viaPointsList.add(null);
//		viaPointsList.add("SSH");
//		criteria.setViaPoints(viaPointsList);
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",0,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",0,page.getPageData().size());
//		
//		viaPointsList = new ArrayList();
//		viaPointsList.add(null);
//		viaPointsList.add("SSH");
//		viaPointsList.add(null);		
//		criteria.setViaPoints(viaPointsList);
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",0,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",0,page.getPageData().size());
//		
//		viaPointsList = new ArrayList();
//		viaPointsList.add(null);
//		viaPointsList.add("SSH");
//		viaPointsList.add(null);
//		viaPointsList.add(null);
//		criteria.setViaPoints(viaPointsList);
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",0,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",0,page.getPageData().size());
//		
//		criteria.setDepartureAirport("SHJ");
//		criteria.setArrivalAirport("LXR");
//		criteria.setViaPoints(null);
//		criteria.setShowAllCombinations(false);
//		page = reportingJDBC.getSeatInventoryFareMovementData(criteria, 1, 5);
//		assertEquals("SeatInventoryFareMovementData returned invalid number of records",0,page.getTotalNoOfRecords());
//		assertEquals("SeatInventoryFareMovementData returned invalid number of page records",0,page.getPageData().size());
//	}
//	
	protected void tearDown() throws Exception{
		super.tearDown();
	}
    
    public ReportingDAOJDBCImpl getReportingDAO(){
        return (ReportingDAOJDBCImpl)  ReportingUtils.getInstance().getLocalBean("reportingDAOJDBCImpl");
    }
}
