/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.reporting.core.remoting.ejb;

import java.util.ArrayList;

import javax.sql.RowSet;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.service.DataExtractionBD;

/**
 * @author byorn
 *
 */
public class TestReportingServiceRemote extends PlatformTestCase{
	
	ReportsSearchCriteria  searchCriteria = null;
	private RowSet rowSet = null;
	
	protected void setUp() throws Exception{
		super.setUp();
	}

	public void testGetPerformanceOfSalesStaffSummaryData() throws Exception {
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList user = new ArrayList();
		user.add("T1");
		user.add("T2");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setUsers(user);
		rowSet = getReportingService().getPerformanceOfSalesStaffSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetPerformanceOfSalesStaffDetailData() throws Exception {
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setUserId("T2");
		rowSet = getReportingService().getPerformanceOfSalesStaffDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}

	public void testGetCallCentreSalesData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
        //FIXME
		//rowSet = getReportingService().getCallCentreSalesData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}		
	}
	
	public void testGetCallCentreModeOfPaymentsData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList payments = new ArrayList();
		payments.add("CA");
		payments.add("CC");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setPaymentTypes(payments);
		rowSet = getReportingService().getCallCentreModeOfPaymentsData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCallCentreRefundData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		rowSet = getReportingService().getCallCentreRefundData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCustomerTravelHistorySummaryData() throws Exception {
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setByCustomer(true);
		searchCriteria.setCustomerFirstName("");
		searchCriteria.setCustomerLastName("");
		searchCriteria.setBySector(true);
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
        //FIXME
		//rowSet = getReportingService().getCustomerTravelHistorySummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCustomerTravelHistoryDetailData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setCustomerId("45");
		searchCriteria.setBySector(true);
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
		rowSet = getReportingService().getCustomerTravelHistoryDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCustomerProfileSummaryData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setByNationality(true);
		searchCriteria.setNationality("1");
		searchCriteria.setByCountryOfResidence(true);
		searchCriteria.setCountryOfResidence("12");
		rowSet = getReportingService().getCustomerProfileSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCustomerProfileDetailData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setCustomerId("45");
		rowSet = getReportingService().getCustomerProfileDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	/*public void testGetTravelAgentsProductivityByFlightOrSegmentData() throws Exception {
		searchCriteria = new ReportsSearchCriteria();
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");		
		searchCriteria.setBySector(true);
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
		searchCriteria.setByFlight(true);
		searchCriteria.setFlightNumber("G9500");
		rowSet = getReportingService().getTravelAgentsProductivityByFlightOrSegmentData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetTravelAgentsPerformanceOfGSAData() throws Exception {
		searchCriteria = new ReportsSearchCriteria();
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");		
		searchCriteria.setAgentCode("CMB031");
		rowSet = getReportingService().getTravelAgentsPerformanceOfGSAData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}*/
	
	public void testGetEnplanementData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAirportCode("CMB");
		rowSet = getReportingService().getEnplanementData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetTopAgentsData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");		
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
		searchCriteria.setNoOfTops(10);
		rowSet = getReportingService().getTopAgentsData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetTopSegmentsData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setNoOfTops(15);
		rowSet = getReportingService().getTopSegmentsData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetSegmentContributionByAgentsData() throws Exception {
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
		rowSet = getReportingService().getSegmentContributionByAgentsData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetCountryContributionPerFlightSegmentData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");		
		searchCriteria.setBySector(true);
		searchCriteria.setSectorFrom("CMB");
		searchCriteria.setSectorTo("DOH");
		searchCriteria.setByFlight(true);
		searchCriteria.setFlightNumber("G9500");
		rowSet = getReportingService().getCountryContributionPerFlightSegmentData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetOnholdPassengersData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setByFlight(true);
		searchCriteria.setFlightNumber("G9500");
		rowSet = getReportingService().getOnholdPassengersData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetReservationBreakdownSummaryData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList agents = new ArrayList();
		agents.add("CMB070");
		agents.add("CMB055");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgents(agents);
		//searchCriteria.setReportOption(reportsSearchCriteria.REPORT_OPTION_RESERVATION);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FLOWN);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FORWARD);		
		rowSet = getReportingService().getReservationBreakdownSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetReservationBreakdownDetailData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgentCode("CMB070");
		//searchCriteria.setReportOption(reportsSearchCriteria.REPORT_OPTION_RESERVATION);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FLOWN);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FORWARD);		
		rowSet = getReportingService().getReservationBreakdownDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetReservationBreakdownTaxesData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList agents = new ArrayList();
		agents.add("CMB070");
		agents.add("CMB055");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgents(agents);
		//searchCriteria.setReportOption(reportsSearchCriteria.REPORT_OPTION_TAXES);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FLOWN);
		searchCriteria.setRevenueType(ReportsSearchCriteria.REVENUE_TYPE_FORWARD);	
		rowSet = getReportingService().getReservationBreakdownTaxesData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetPaymentsSummaryData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList payments = new ArrayList();
		payments.add("CA");
		payments.add("CC");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setPaymentTypes(payments);
        //FIXME
		//rowSet = getReportingService().getPaymentsSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetPaymentsDetailData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgentCode("CMB070");
		searchCriteria.setPaymentCode("CA");
        //FIXME
		//rowSet = getReportingService().getPaymentsDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetOutstandingBalanceSummaryData() throws Exception {
		searchCriteria = new ReportsSearchCriteria(); 
		ArrayList agentTypes = new ArrayList();
		agentTypes.add("SO");
		agentTypes.add("TA");
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgencyTypes(agentTypes);
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT);
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY);		
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY);		
		rowSet = getReportingService().getOutstandingBalanceSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetOutstandingBalanceDetailData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		searchCriteria.setAgentCode("CMB070");
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT);
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY);		
		searchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY);
		rowSet = getReportingService().getOutstandingBalanceDetailData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetTravelAgentProductivityData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
        //FIXME
		//rowSet = getReportingService().getTravelAgentProductivityData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	public void testGetInvoiceSummaryData() throws Exception { 
		searchCriteria = new ReportsSearchCriteria(); 
		searchCriteria.setDateRangeFrom("01-JAN-2005");
		searchCriteria.setDateRangeTo("01-NOV-2005");
		rowSet = getReportingService().getInvoiceSummaryData(searchCriteria);
		while (rowSet.next()) {
			for (int i = 1; i <= rowSet.getMetaData().getColumnCount(); i++) {
				System.out.println("Column "+ (i) + " value is ........ = " 
					+ rowSet.getString(i));					
			}
		}
	}
	
	private DataExtractionBD getReportingService()
	{ 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule reportingModule = lookup.getModule("reporting");
		System.out.println("Lookup successful...");
		DataExtractionBD delegate = null;
		try {
			delegate = (DataExtractionBD)reportingModule.getServiceBD("reporting.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}

}
