<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MealsSummaryReport" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="822" leftMargin="10" rightMargin="10" topMargin="0" bottomMargin="0" uuid="6126296f-d30e-4935-b486-a9518afe6b00">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="ID" class="java.lang.String" isForPrompting="false"/>
	<parameter name="OPTION" class="java.lang.String" isForPrompting="false"/>
	<parameter name="IMG" class="java.lang.String" isForPrompting="false"/>
	<parameter name="CARRIER" class="java.lang.String" isForPrompting="false"/>
	<parameter name="REPORT_MEDIUM" class="java.lang.String" isForPrompting="false"/>
	<parameter name="FLIGHT_NUMBER" class="java.lang.String" isForPrompting="false"/>
	<parameter name="FROM_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="TO_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="LOCALE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="FOC_MEAL_ENABLED" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT   a.meal_name, a.meal_price, COUNT (a.pas_id) AS number_meals,
         SUM (a.meal_price) AS tot_amt,SUM(COUNT (a.pas_id)) OVER() as total

    FROM (SELECT   mel.meal_name AS meal_name, mel.amount AS meal_price,
                   pp.pnr_pax_id AS pas_id

              FROM ml_t_meal mel,
                   ml_t_pnr_pax_seg_meal pp,
                   t_pnr_segment ps,
                   t_flight_segment fs,
                   t_flight ft,
                   t_flight_schedule fsh
             WHERE mel.meal_id = pp.meal_id
               AND pp.pnr_seg_id = ps.pnr_seg_id
               AND ps.flt_seg_id = fs.flt_seg_id
               AND fs.flight_id = ft.flight_id
               AND ft.schedule_id = fsh.schedule_id
               AND fs.est_time_departure_zulu
                      BETWEEN TO_DATE ('01-Sep-2009 00:00:00',
                                       'dd-mon-yyyy HH24:mi:ss'
                                      )
                          AND TO_DATE ('14-Oct-2009 23:59:59',
                                       'dd-mon-yyyy HH24:mi:ss'
                                      )
          ORDER BY meal_name) a
GROUP BY a.meal_name, a.meal_price]]>
	</queryString>
	<field name="MEAL_NAME" class="java.lang.String"/>
	<field name="MEAL_PRICE" class="java.math.BigDecimal"/>
	<field name="NUMBER_MEALS" class="java.math.BigDecimal"/>
	<field name="TOT_AMT" class="java.lang.Double"/>
	<field name="TOTAL" class="java.math.BigDecimal"/>
	<variable name="MEALS_TOTAL_AMT" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{TOT_AMT}]]></variableExpression>
	</variable>
	<variable name="numFormat" class="java.text.NumberFormat">
		<variableExpression><![CDATA[NumberFormat.getInstance(new Locale($P{LOCALE}));
((NumberFormat)value).setMinimumFractionDigits(2)]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="163" splitType="Stretch">
			<staticText>
				<reportElement uuid="28b571f1-3ef9-4d79-ae9d-c086946940bc" key="staticText-3" mode="Transparent" x="219" y="51" width="340" height="16" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Summary Report]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d23053c3-7664-4015-ae35-e83a42bf9d7f" key="staticText-6" mode="Transparent" x="503" y="72" width="152" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Report ID :]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b16fcb16-1b46-4a8b-98e0-bd87dbdd7ef9" key="staticText-6" mode="Transparent" x="503" y="87" width="152" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Date :]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b9756300-0b34-4bd5-89ed-274cf0807f74" key="staticText-7" mode="Transparent" x="503" y="101" width="152" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Time :]]></text>
			</staticText>
			<textField pattern="HH.mm.ss" isBlankWhenNull="false">
				<reportElement uuid="78230131-f995-451f-8bd7-47e3ef1638d8" key="textField-1" mode="Transparent" x="655" y="101" width="71" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement uuid="80eb7c2b-ce86-43a5-9437-7af6c9413286" key="textField-1" mode="Transparent" x="655" y="87" width="98" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="97bc22d6-dc8a-4887-8d66-68c723c24950" key="textField" mode="Transparent" x="655" y="72" width="98" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{ID}]]></textFieldExpression>
			</textField>
			<image scaleImage="Clip" hAlign="Left" vAlign="Top" isUsingCache="false" isLazy="true">
				<reportElement uuid="2732fdb1-454d-453e-8bf4-3b3b4dbee617" key="image-1" mode="Opaque" x="32" y="11" width="187" height="56" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
				<imageExpression><![CDATA[$P{IMG}]]></imageExpression>
			</image>
			<staticText>
				<reportElement uuid="d2ebbfe4-c34f-4936-b4db-56179caba4fb" key="staticText-37" mode="Transparent" x="219" y="24" width="340" height="27" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Meal Details for Finance]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="93888444-7275-4c00-8c58-b4ab62b7eb41" key="staticText-8" mode="Transparent" x="32" y="124" width="187" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Meals Name



]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="64468b6e-3004-408b-92fb-7a3be7ec5ebc" key="staticText-25" mode="Transparent" x="219" y="124" width="142" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Price]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="bdc01561-4f68-4fb3-9a21-bdaab7a36ec3" key="staticText-27" mode="Transparent" x="655" y="124" width="98" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[PAX Details]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement uuid="1da20194-3a5c-49ef-a79c-afa0d02f2c70" key="line-6" mode="Opaque" x="18" y="118" width="760" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="6cb9220e-1303-44d0-bdf5-ae138ac407f7" key="staticText-21" mode="Transparent" x="726" y="101" width="27" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Zulu]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b0fbb9e0-0eb9-48c7-ad56-51a386119a41" key="staticText-38" mode="Transparent" x="503" y="124" width="152" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total Amount]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3d515689-ad46-4ec7-9f0c-40b18dcd4c36" key="staticText-40" mode="Transparent" x="363" y="124" width="138" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No. Of Meals]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement uuid="1db47213-2cc6-4016-8e68-79342d5c988a" key="line-8" mode="Opaque" x="18" y="149" width="760" height="5" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="9c84384b-e3a8-4d1a-ab6b-d1ec9e93faca" key="staticText-46" mode="Transparent" x="32" y="87" width="100" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[From Date]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="f4cc464f-8a3d-46d7-8435-1230d1f2928d" key="textField-10" mode="Transparent" x="219" y="87" width="135" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{FROM_DATE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="47ee835e-ea4e-4596-a531-860080237aa4" key="staticText-47" mode="Transparent" x="32" y="101" width="100" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3186bf97-ed83-4101-93a4-87319f591292" key="staticText-48" mode="Transparent" x="32" y="72" width="100" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Flight No]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="29bd8116-4501-4aab-9f39-85ebe89829e0" key="textField-11" mode="Transparent" x="219" y="101" width="135" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{TO_DATE}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="9a7d22b4-6fd0-4593-a08a-53d90f4ec97d" key="textField-12" mode="Transparent" x="219" y="72" width="135" height="12" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{FLIGHT_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="30" splitType="Prevent">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="23d99215-e277-4706-b337-2b22eb558e8b" key="textField-2" mode="Transparent" x="32" y="2" width="187" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{MEAL_NAME}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement uuid="3eb254db-84f4-4c41-b039-91ad435ec345" key="textField-9" mode="Transparent" x="219" y="2" width="144" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{MEAL_PRICE})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true" hyperlinkType="Reference" hyperlinkTarget="Blank">
				<reportElement uuid="932319fc-145d-42f1-97e5-deaa1401ed4d" key="textField-8" mode="Opaque" x="655" y="2" width="98" height="22" forecolor="#0033FF" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="true" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Detail"]]></textFieldExpression>
				<anchorNameExpression><![CDATA[""]]></anchorNameExpression>
				<hyperlinkReferenceExpression><![CDATA["showMealsDetailReport.action?hdnRptType=DETAIL&hdnMealsName="+$F{MEAL_NAME}+"&hdnFltNo=+"+$P{FLIGHT_NUMBER}+"&hdnFromDt=+"+$P{FROM_DATE}+"&hdnToDt=+"+$P{TO_DATE}+"&radReportOption=CSV&hdnMode=VIEW&hdnLive="+$P{REPORT_MEDIUM}+"&radRptNumFormat="+$P{LOCALE}]]></hyperlinkReferenceExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="7bf6f16b-4562-4587-98d2-92ce40d3a99a" key="textField" mode="Transparent" x="503" y="2" width="152" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{TOT_AMT})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0" isBlankWhenNull="true">
				<reportElement uuid="f18dfc2c-fb90-4297-b23e-6400fb9c7ac3" key="textField" mode="Transparent" x="363" y="2" width="140" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{NUMBER_MEALS}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="62" splitType="Stretch">
			<line direction="BottomUp">
				<reportElement uuid="ed5625d0-dd1b-4ee4-a0f0-6051dd36a227" key="line-7" mode="Opaque" x="30" y="33" width="750" height="2" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="29359a32-f154-4772-8d96-2c7ff8e98f21" key="textField" mode="Transparent" x="655" y="40" width="98" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="cf3257b9-89d4-4a01-9bda-2e690b95c2a5" key="textField" mode="Transparent" x="32" y="40" width="100" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{CARRIER}]]></textFieldExpression>
			</textField>
			<line direction="BottomUp">
				<reportElement uuid="2f7b99b9-abb6-4e13-b28f-98d132f15d92" key="line-9" mode="Opaque" x="30" y="3" width="750" height="3" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="b0595fa1-d38a-4aad-9b25-7aa132f85bb3" key="staticText-42" mode="Transparent" x="32" y="9" width="187" height="22" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="d2ff9279-36a7-47cb-a6c6-2496bd50a508" key="textField" mode="Transparent" x="503" y="9" width="152" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($V{MEALS_TOTAL_AMT})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="d030617f-f309-477a-9fdd-f0310b034ba1" key="staticText-44" mode="Transparent" x="219" y="9" width="144" height="22" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean($P{FOC_MEAL_ENABLED}.equals("false"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="###0" isBlankWhenNull="true">
				<reportElement uuid="69220321-4cde-4736-89e7-ba86ccaa6855" key="textField" x="363" y="9" width="140" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{TOTAL})]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band height="23" splitType="Stretch"/>
	</summary>
</jasperReport>
