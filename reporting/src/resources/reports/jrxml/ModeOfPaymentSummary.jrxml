<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ModeOfPaymentSummary" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="822" leftMargin="10" rightMargin="10" topMargin="0" bottomMargin="0" uuid="8fc1b76c-d6be-4de9-adc4-8bbd6199a00d">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.1"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="ID" class="java.lang.String" isForPrompting="false"/>
	<parameter name="IMG" class="java.lang.String" isForPrompting="false"/>
	<parameter name="FROM_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="TO_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="OPTION" class="java.lang.String" isForPrompting="false"/>
	<parameter name="TIME_MODE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="CARRIER" class="java.lang.String" isForPrompting="false"/>
	<parameter name="REPORT_MEDIUM" class="java.lang.String" isForPrompting="false"/>
	<parameter name="REPORT_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["PAYMENT_SUMMARY"]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_CURRENCY" class="java.lang.String" isForPrompting="false">
		<parameterDescription><![CDATA[Base Currency]]></parameterDescription>
	</parameter>
	<parameter name="LOCALE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="TIME_ZONE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="ZULU_TIME" class="java.lang.String" isForPrompting="false"/>
	<parameter name="rnd" class="java.lang.String" isForPrompting="false">
		<parameterDescription><![CDATA[$V{numFormat}.applyPattern("#0.00")]]></parameterDescription>
	</parameter>
	<parameter name="LOCAL_TIME" class="java.lang.String" isForPrompting="false"/>
	<parameter name="VIEW" class="java.lang.String" isForPrompting="false"/>
	<parameter name="ENTITY_NAME" class="java.lang.String"/>
	<field name="PAY" class="java.lang.Double"/>
	<field name="PAYMENT_MODE" class="java.lang.String"/>
	<field name="AMT" class="java.math.BigDecimal"/>
	<field name="GW" class="java.lang.String"/>
	<field name="ENTITY" class="java.lang.String"/>
	<variable name="Grand_total" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{AMT}]]></variableExpression>
	</variable>
	<variable name="RPT_TYPE" class="java.lang.String">
		<variableExpression><![CDATA[( $P{REPORT_NAME}.equals("PAYMENT_SUMMARY") ? "PM" : "BC" )]]></variableExpression>
	</variable>
	<variable name="numFormat" class="java.text.NumberFormat">
		<variableExpression><![CDATA[NumberFormat.getInstance(new Locale($P{LOCALE}));
((NumberFormat)value).setMinimumFractionDigits(2)]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="136" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-36" mode="Transparent" x="124" y="1" width="0" height="2" forecolor="#000000" backcolor="#FFFFFF" uuid="c8da8fcb-5677-4d0f-94f2-c90f727ec0fb"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Static text]]></text>
			</staticText>
			<image scaleImage="Clip" hAlign="Left" vAlign="Top" isUsingCache="false" isLazy="true">
				<reportElement key="image-1" mode="Opaque" x="48" y="7" width="145" height="44" forecolor="#000000" backcolor="#FFFFFF" uuid="a84f78ce-7d44-487c-b002-98ca92d1ff17"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
				<imageExpression><![CDATA[$P{IMG}]]></imageExpression>
			</image>
			<staticText>
				<reportElement key="staticText-27" mode="Transparent" x="576" y="110" width="87" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="e39a9133-c17e-4616-ad96-60c8d118b4bc"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Detail]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement key="line-6" mode="Opaque" x="34" y="99" width="700" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="d8faa6d9-c918-4f79-b694-f5ab010b52c9"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="48" y="110" width="87" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="5eba1128-db8a-4837-9639-68648a7091d4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[( $P{REPORT_NAME}.equals("PAYMENT_SUMMARY") ? "Payment Mode" : "Currency" )]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-45" mode="Transparent" x="148" y="110" width="151" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="be30a54e-a6f3-4ece-a090-d1495c235d49"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Payment Gate Way]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="371" y="110" width="151" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="8c8ca144-540e-4865-afb4-69aacd839398"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Amount ("+$P{BASE_CURRENCY}+")"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-46" mode="Transparent" x="48" y="69" width="56" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="e9d45451-db09-40b6-bb50-31c601e04e1d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[From Date]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-10" mode="Transparent" x="148" y="69" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="244dd2a2-8341-4099-b26f-d115a0d9b274"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{FROM_DATE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-47" mode="Transparent" x="48" y="85" width="56" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="e7fcf6bb-d7ab-4900-9c50-f39d7d080b77"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Date]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-11" mode="Transparent" x="148" y="85" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="b5422bba-99c5-416b-b890-c32c65603949"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{TO_DATE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-48" mode="Transparent" x="576" y="53" width="48" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="a4f313e5-72c8-418a-b1d3-11e96cd7cc34"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Report ID :]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-12" mode="Transparent" x="634" y="53" width="62" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="616171d6-41a5-45dd-a6d7-6722ed990444"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{ID}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-49" mode="Transparent" x="576" y="69" width="48" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="af383035-801d-47bf-ad26-ccd9716601a2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Date :]]></text>
			</staticText>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement key="textField-13" mode="Transparent" x="634" y="69" width="62" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="58a7a235-b84f-424e-af09-c7bf9c252a48"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-50" mode="Transparent" x="576" y="85" width="48" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="2fa07bb0-019e-4492-a6b4-58f5d6e441de"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Time :]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-14" mode="Transparent" x="634" y="85" width="80" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="cb91e688-7500-434e-bc6e-b8606f1d6b1c">
					<printWhenExpression><![CDATA[new Boolean(!$P{ZULU_TIME}.equals( "" ))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{ZULU_TIME}]]></textFieldExpression>
			</textField>
			<textField pattern="HH.mm.ss" isBlankWhenNull="true">
				<reportElement key="textField-15" mode="Transparent" x="634" y="85" width="80" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="cc30ebbc-12b7-4453-abbc-0c59f0648425">
					<printWhenExpression><![CDATA[new Boolean($P{ZULU_TIME}.equals( "" ))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{LOCAL_TIME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-16" mode="Transparent" x="716" y="85" width="70" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="4c78c5cf-a3d6-4bdd-9012-2553824f73eb"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{TIME_ZONE}]]></textFieldExpression>
			</textField>
			<line direction="BottomUp">
				<reportElement key="line-6" mode="Opaque" x="42" y="135" width="700" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="462b65b8-7ed4-45e8-96b9-0186220892e0"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-55" mode="Transparent" x="148" y="30" width="428" height="21" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF" uuid="c25a0dc3-44af-46b5-adf4-5266b0999077"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Summary Report]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-20" mode="Transparent" x="148" y="12" width="428" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="90771ab5-4143-4fad-be28-e5c82d2fc122"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica-Bold" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Mode 0f Payment - "+ $P{CARRIER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-46" mode="Transparent" x="48" y="55" width="56" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="2d63dda8-c985-4078-9e8e-da6cf913d792"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Entity]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-10" mode="Transparent" x="148" y="55" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="2f2a48f6-5ea1-45fb-ad0e-a005e87180d5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{ENTITY_NAME}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="1" splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="29" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="48" y="7" width="87" height="21" forecolor="#000000" backcolor="#FFFFFF" uuid="c4e22e2a-216e-4d54-a947-b3adf23d239c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PAYMENT_MODE}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="371" y="7" width="151" height="21" forecolor="#000000" backcolor="#FFFFFF" uuid="8dd42112-9421-4c24-b4cd-35d3efb89c14"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{AMT})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true" hyperlinkType="Reference" hyperlinkTarget="Blank">
				<reportElement key="textField-8" mode="Opaque" x="576" y="7" width="87" height="21" forecolor="#0033FF" backcolor="#FFFFFF" uuid="07cefb79-6cee-4af1-9c5b-5481164a8c9b">
					<printWhenExpression><![CDATA[new Boolean(($F{PAY}.equals(new Double("42.00")) || $F{PAY}.equals(new Double("43.00")) || $F{PAY}.equals(new Double("44.00")) || $F{PAY}.equals(new Double("45.00")))? false : true)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="true" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Detail"]]></textFieldExpression>
				<anchorNameExpression><![CDATA[""]]></anchorNameExpression>
				<hyperlinkReferenceExpression><![CDATA["showModeOfPayments.action?hdnRptType=DETAIL&hdnMode=VIEW&hdnPayments="+$F{PAY}+"&chkOption="+$V{RPT_TYPE}+"&txtFromDate="+$P{FROM_DATE}+"&txtToDate="+$P{TO_DATE}+"&txtFromDate="+$P{FROM_DATE}+"&txtToDate="+$P{TO_DATE}+"&hdnTime="+$P{TIME_MODE}+"&radReportOption=CSV&hdnLive="+$P{REPORT_MEDIUM}+"&hdnPaymentGw="+$F{GW}+"&hdnPaymentDesc="+$F{PAYMENT_MODE}+"&radRptNumFormat="+$P{LOCALE}+"&chkNewview="+$P{VIEW}+"&selEntity="+$F{ENTITY}+"&hdnEntity="+$P{ENTITY_NAME}]]></hyperlinkReferenceExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="148" y="7" width="151" height="21" forecolor="#000000" backcolor="#FFFFFF" uuid="aa64cbe4-29e1-44e9-a93b-995bc3d68e3a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{GW}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="3" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="1" splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="80" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-14" mode="Transparent" x="48" y="61" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="475a9fbe-5854-4332-ad4e-5fff3ba1b96e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[*** End of Report ***]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="576" y="46" width="54" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="3eb84fe9-c5d0-43ac-be1e-360294b64de1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<line direction="BottomUp">
				<reportElement key="line-6" mode="Opaque" x="42" y="8" width="700" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="89d027e3-382c-46e6-9163-0799cb79e3cc"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<line direction="BottomUp">
				<reportElement key="line-6" mode="Opaque" x="42" y="34" width="700" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="77c26170-bd96-4920-a6c7-bc62748c6539"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="48" y="46" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="3876d329-8fa1-4f70-b3bf-3890fe68e0c9"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{CARRIER}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-19" mode="Transparent" x="371" y="12" width="151" height="16" forecolor="#000000" backcolor="#FFFFFF" uuid="d8c76830-75c1-4d7b-be6e-16ccd01f9570"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[( $P{REPORT_NAME}.equals("PAYMENT_SUMMARY") ? $V{numFormat}.format($V{Grand_total}) : null )]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-53" mode="Transparent" x="148" y="12" width="151" height="16" forecolor="#000000" backcolor="#FFFFFF" uuid="ba03f01f-68e1-43f3-aef3-c8f15a7c5c3e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Grand Total :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-54" mode="Transparent" x="48" y="12" width="87" height="16" forecolor="#000000" backcolor="#FFFFFF" uuid="5d237899-ff4e-45be-afd9-6b149fbc9038"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</lastPageFooter>
	<summary>
		<band height="23" splitType="Stretch"/>
	</summary>
</jasperReport>
