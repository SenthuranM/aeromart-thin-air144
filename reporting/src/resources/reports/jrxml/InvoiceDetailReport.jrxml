<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="InvoiceDetailReport" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="0" bottomMargin="0" uuid="0b46f91a-a4d4-42b7-a395-3fb2291ea513">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="AGENT_CODE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="AGENT_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="INVOICE_NO" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="INVOICE_PERIOD" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="ID" class="java.lang.String" isForPrompting="false"/>
	<parameter name="IMG" class="java.lang.String" isForPrompting="false"/>
	<parameter name="CARRIER" class="java.lang.String" isForPrompting="false"/>
	<parameter name="SHOW_PAYREF" class="java.lang.String" isForPrompting="false"/>
	<parameter name="PAYREF" class="java.lang.String" isForPrompting="false"/>
	<parameter name="LOCALE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="INVOICE_ENTITY" class="java.lang.String"/>
	<queryString>
		<![CDATA[/* Formatted on 2009/11/13 14:42 (Formatter Plus v4.8.7) */
SELECT   a.pnr_pax_id, a.pnr,
         (a.title || ' ' || a.first_name || ' ' || a.last_name
         ) AS pnr_pax_name,
         (b.amount * -1) amount, b.agent_code, b.ext_reference,
         (  (b.amount * -1)
          / (SELECT NVL (exrate.exrate_base_to_cur, 1) AS exchange_rate
               FROM t_currency_exchange_rate exrate
              WHERE TO_DATE ('15-Nov-2009 00:00:00', 'dd-mon-yyyy hh24:mi:ss')
                       BETWEEN exrate.effective_from
                           AND exrate.effective_to
                AND exrate.status = 'ACT'
                AND exrate.currency_code =
                       (SELECT MAX (ag2.currency_code)
                          FROM t_agent ag2
                         WHERE ag2.agent_code = agent_code
                           AND ag2.agent_code = 'DXB168'))
         ) TOTAL_AMOUNT_PAYCUR,
         TO_CHAR (b.tnx_date, 'DD/mm/YYYY') AS tnx_date, c.display_name
    FROM t_pnr_passenger a, t_pax_transaction b, t_user c
   WHERE a.pnr_pax_id = b.pnr_pax_id
     AND b.user_id = c.user_id
     AND b.agent_code = 'DXB168'
     AND b.nominal_code IN (19, 25)
     AND (b.sales_channel_code IS NULL OR b.sales_channel_code <> 11)
     AND b.tnx_date BETWEEN TO_DATE ('16-11-2009 00:00:00',
                                     'dd-mm-yyyy HH24:mi:ss'
                                    )
                        AND TO_DATE ('30-11-2009  23:59:59',
                                     'dd-mm-yyyy HH24:mi:ss'
                                    )
ORDER BY a.pnr, b.tnx_date]]>
	</queryString>
	<field name="PNR_PAX_ID" class="java.math.BigDecimal"/>
	<field name="PNR" class="java.lang.String"/>
	<field name="PNR_PAX_NAME" class="java.lang.String"/>
	<field name="AMOUNT" class="java.math.BigDecimal"/>
	<field name="AGENT_CODE" class="java.lang.String"/>
	<field name="EXT_REFERENCE" class="java.lang.String"/>
	<field name="TOTAL_AMOUNT_PAYCUR" class="java.math.BigDecimal"/>
	<field name="TNX_DATE" class="java.lang.String"/>
	<field name="DISPLAY_NAME" class="java.lang.String"/>
	<variable name="TOT_AMOUNT" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{AMOUNT}]]></variableExpression>
	</variable>
	<variable name="numFormat" class="java.text.NumberFormat">
		<variableExpression><![CDATA[NumberFormat.getInstance(new Locale($P{LOCALE}));
((NumberFormat)value).setMinimumFractionDigits(2)]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="161" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" mode="Transparent" x="345" y="14" width="235" height="18" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF" uuid="e6b5b62e-24d1-4e90-af4b-913600677f18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[INVOICE DETAILS]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Transparent" x="345" y="34" width="235" height="15" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF" uuid="2f0f202a-7f26-4070-94f4-2f831e8f24e7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Detail Report]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Transparent" x="0" y="68" width="101" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="d14cc3c0-16b8-456d-927c-5c396cad04f0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Agent Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" mode="Transparent" x="0" y="84" width="101" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="20762ec1-c9af-48d7-ad88-773b5188151d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Agent Name
]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" stretchType="RelativeToTallestObject" mode="Transparent" x="101" y="84" width="339" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="f24fb137-3268-4250-b4ca-8cb534437fa1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[" : " + $P{AGENT_NAME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="101" y="68" width="133" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="7314f3ca-4cc8-4982-914d-c9dc68330d4a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[" : " + $P{AGENT_CODE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-24" mode="Transparent" x="0" y="100" width="101" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="4251d310-7433-451f-8c16-89076372276f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Invoice No]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-26" mode="Transparent" x="0" y="115" width="101" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="41aa7add-408c-43a7-a5ed-fe3011fced8b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Invoice Period]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="101" y="100" width="133" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="f0b1f7ec-38e5-46ab-b6a5-3e2a31a8af0e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[" : " + $P{INVOICE_NO}]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" isUsingCache="false" isLazy="true">
				<reportElement key="image-1" mode="Opaque" x="4" y="2" width="95" height="47" forecolor="#000000" backcolor="#FFFFFF" uuid="dca22f0b-c0cc-47b8-be6f-b9326bba2cee"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
				<imageExpression><![CDATA[$P{IMG}]]></imageExpression>
			</image>
			<staticText>
				<reportElement key="staticText-9" mode="Transparent" x="0" y="136" width="140" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="3e99b817-9fdd-4f88-a589-c407d2c0f757"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Reservation No
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Transparent" x="275" y="136" width="78" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="eaf01a50-ae38-4420-b798-e907729ede50"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Payment Amount
]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement key="line-7" mode="Opaque" x="3" y="153" width="773" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="d7105991-23d3-4d56-8325-df5962a0c51e"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-23" mode="Transparent" x="502" y="136" width="103" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="4ad22f64-e6ff-4156-ac68-7f83b35c66bc"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[User Name]]></text>
			</staticText>
			<line direction="BottomUp">
				<reportElement key="line-7" mode="Opaque" x="3" y="130" width="773" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="c41c8333-703a-4e11-b269-286b08b9fc9a"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-20" mode="Transparent" x="580" y="68" width="69" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="dd66b02a-b5a8-43c9-8b34-e44937fd7f9b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Report ID :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Transparent" x="580" y="84" width="69" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="f0b13e48-4b26-4a7c-a84c-c966cdb34c71"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Date :]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Transparent" x="580" y="100" width="69" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="cf513a04-7ec8-4ea3-b18c-b64bcdb2b79d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Time :]]></text>
			</staticText>
			<textField pattern="HH.mm.ss" isBlankWhenNull="false">
				<reportElement key="textField-1" mode="Transparent" x="655" y="100" width="59" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="3a35a750-70cc-4121-9867-6a5480d728b0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement key="textField-2" mode="Transparent" x="655" y="84" width="60" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="0346f786-8f08-4def-b625-6339729f489b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="655" y="68" width="89" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="30f40714-3956-44cd-af63-cacc71a78f69"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{ID}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-38" mode="Transparent" x="717" y="100" width="34" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="5ec1e90f-3000-4684-a6c1-eb8b0bf37117"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Zulu]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="101" y="115" width="133" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="51946c00-96a9-40c7-b9ea-ba89724ab772"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[" : " + $P{INVOICE_PERIOD}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-43" mode="Transparent" x="142" y="136" width="133" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="0f318771-6ed3-4b08-99c9-d788c3393887"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Pax Name
]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-6" x="605" y="136" width="109" height="14" uuid="70fc81c2-663b-40df-a2f0-3050847a1b0e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8" isBold="true" pdfFontName="Helvetica"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{PAYREF}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-44" mode="Transparent" x="361" y="136" width="92" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="37174579-7d06-4d15-aa99-70447c4ea6d9"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Payment Date
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-45" mode="Transparent" x="580" y="115" width="69" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="f3fdc699-c4a1-4866-9da7-e6707f67ae6e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Entity : ]]></text>
			</staticText>
			<textField pattern="HH.mm.ss" isBlankWhenNull="true">
				<reportElement key="textField-7" mode="Transparent" x="655" y="115" width="59" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="5887aa6b-d8ce-475a-92ee-fbea3043ca34"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{INVOICE_ENTITY}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="20" splitType="Prevent">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="0" y="3" width="140" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="cfdd648f-2fed-4deb-912b-e13d7079100d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PNR}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="142" y="3" width="133" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="8d514be2-af8d-4feb-8156-b64bca797454"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PNR_PAX_NAME}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="275" y="3" width="78" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="86317e5f-6848-4c96-87b3-dbb46c92326d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{AMOUNT})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Transparent" x="361" y="3" width="92" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="20fe569f-8d7b-4808-b837-7e36804e9a87"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{TNX_DATE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-3" mode="Transparent" x="502" y="3" width="103" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="6ea07e73-c1e3-4061-b6a4-4512b1cfdb36"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{DISPLAY_NAME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-5" x="605" y="3" width="109" height="14" uuid="5c842013-32e7-4a95-a830-338391722b6c">
					<printWhenExpression><![CDATA[new Boolean($P{SHOW_PAYREF}.equals("Y"))]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{EXT_REFERENCE}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="61" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-14" mode="Transparent" x="0" y="46" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="d8769361-3222-408c-a459-8eee9966b52d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[*** End of Report ***]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="549" y="28" width="100" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="065e74d2-f32d-4064-87f0-6ff6127cf125"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" mode="Transparent" x="0" y="28" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="5fe8aa23-5e5e-448e-a0c4-c16c3c757fdb"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{CARRIER}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="275" y="9" width="78" height="18" uuid="50eae59b-2615-4f9c-895f-e8d899602bcd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($V{TOT_AMOUNT})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-41" mode="Transparent" x="0" y="9" width="140" height="18" forecolor="#000000" backcolor="#FFFFFF" uuid="64e3231a-bf77-4c8e-881d-017a62668ffb"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-42" x="142" y="9" width="133" height="18" uuid="42f99544-2cac-4d81-90e2-aa592c6bef92"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left"/>
				<text><![CDATA[Total]]></text>
			</staticText>
		</band>
	</lastPageFooter>
	<summary>
		<band height="39" splitType="Stretch"/>
	</summary>
</jasperReport>
