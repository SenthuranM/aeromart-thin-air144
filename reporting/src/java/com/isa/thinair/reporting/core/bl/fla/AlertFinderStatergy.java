package com.isa.thinair.reporting.core.bl.fla;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;

public interface AlertFinderStatergy {

	public List<FlaAlertDTO> composeAlerts(List<Integer> lstFlightIDs) throws ModuleException;

}
