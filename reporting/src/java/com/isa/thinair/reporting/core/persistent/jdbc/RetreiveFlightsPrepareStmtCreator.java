package com.isa.thinair.reporting.core.persistent.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;

/**
 * Prepared statment creator for flight and inventory summary retrieval
 * 
 * @author MN
 */
public class RetreiveFlightsPrepareStmtCreator implements PreparedStatementCreator {

	private String queryString;

	private FlightSearchCriteria criteria;

	private int startIndex;

	private int pageSize;

	private boolean isCountOnly;

	public RetreiveFlightsPrepareStmtCreator(FlightSearchCriteria criteria, int startIndex, int pageSize, boolean isCountOnly,
			String query) {
		this.criteria = criteria;
		this.startIndex = startIndex;
		this.pageSize = pageSize;
		this.isCountOnly = isCountOnly;
		this.queryString = query;
		// if (isCountOnly)
		// queryString = getQuery("FLIGHTS_SEARCH_FOR_INV_MGMT_COUNT");
		// else
		// queryString = getQuery("FLIGHTS_SEARCH_FOR_INV_MGMT");
	}

	// private String getQuery(String queryID) {
	// return ((QueryRetriever)ReportingUtils.getInstance()
	// .getLocalBean("queryRetriever")).getQueries().getProperty(queryID);
	// }

	public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
		PreparedStatement prepStmt = con.prepareStatement(queryString);
		String segmentCode = PlatformUtiltiies.prepareSegmentCode(criteria.getOrigin(), criteria.getDestination(),
				criteria.getViaPoints(), criteria.isMatchExactONDViaPointsCombinationOnly());
		int index = 0;
		if (!isCountOnly) {
			// prepStmt.setString(++index, segmentCode);
			prepStmt.setString(++index, criteria.getCabinClassCode() == null ? "%" : criteria.getCabinClassCode());
			prepStmt.setString(++index, criteria.isIncludeInvalidSegments() ? "%" : "Y");
			int[] arrSeatFactor = criteria.getSeatFactor();
			if (arrSeatFactor != null) {
				prepStmt.setInt(++index, arrSeatFactor[0]);
				prepStmt.setInt(++index, arrSeatFactor[1]);
			} else {
				prepStmt.setInt(++index, 0);
				prepStmt.setInt(++index, 100);
			}
		}
		prepStmt.setString(++index, criteria.getFlightNumber() == null ? "%" : criteria.getFlightNumber());

		Calendar fromDate = GregorianCalendar.getInstance();
		fromDate.setTime(criteria.getDepartureDateFrom());
		fromDate.set(Calendar.HOUR_OF_DAY, 0);
		fromDate.set(Calendar.MINUTE, 0);
		fromDate.set(Calendar.SECOND, 0);
		fromDate.set(Calendar.MILLISECOND, 0);

		Calendar toDate = GregorianCalendar.getInstance();
		toDate.setTime(criteria.getDepartureDateTo());
		toDate.set(Calendar.HOUR_OF_DAY, 23);
		toDate.set(Calendar.MINUTE, 59);
		toDate.set(Calendar.SECOND, 59);
		toDate.set(Calendar.MILLISECOND, 999);

		prepStmt.setTimestamp(++index, new Timestamp(fromDate.getTimeInMillis()));
		prepStmt.setTimestamp(++index, new Timestamp(toDate.getTimeInMillis()));

		prepStmt.setString(++index, criteria.getOrigin() != null && !criteria.getOrigin().equals("") ? criteria.getOrigin() : "%");
		prepStmt.setString(++index,
				criteria.getDestination() != null && !criteria.getDestination().equals("") ? criteria.getDestination() : "%");

		// prepStmt.setString(++index, (criteria.getFlightStatus()!=null && !criteria.getFlightStatus().equals(""))?
		// criteria.getFlightStatus():"%");
		prepStmt.setString(++index, segmentCode);
		prepStmt.setString(++index, criteria.getCabinClassCode() == null ? "%" : criteria.getCabinClassCode());
		prepStmt.setString(++index, criteria.isIncludeInvalidSegments() ? "%" : "Y");

		int[] arrSeatFactor = criteria.getSeatFactor();
		if (isCountOnly) {
			if (arrSeatFactor != null) {
				prepStmt.setInt(++index, arrSeatFactor[0]);
				prepStmt.setInt(++index, arrSeatFactor[1]);
			} else {
				prepStmt.setInt(++index, 0);
				prepStmt.setInt(++index, 100);
			}
		}
		if (!isCountOnly) {

			prepStmt.setInt(++index, (startIndex + 1));
			prepStmt.setInt(++index, startIndex + pageSize);
		}

		return prepStmt;
	}
}
