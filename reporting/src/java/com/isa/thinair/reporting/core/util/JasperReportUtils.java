package com.isa.thinair.reporting.core.util;

/**
 * @author M.Rikaz Utility Classes used by Jasper Report to define user define function
 */

public class JasperReportUtils {

	/**
	 * @param orgValue
	 *            - source string to format
	 * @param charPerLine
	 *            - number of characters per line
	 * @param breakChar
	 *            - insert string to break [ex:"\n","<br>
	 *            ",etc..]
	 * */

	public static String breakFullString(String orgValue, int charPerLine, String breakChar) {
		String fromatStr = "";

		String partA = "";
		String partB = "";
		if (breakChar == null) {
			breakChar = " ";
		}
		int tempSplit = -1;

		try {
			if (orgValue != null && orgValue.length() > charPerLine) {

				String tempStr = orgValue;
				while (tempStr.length() > charPerLine) {

					if (tempStr.length() < charPerLine) {
						charPerLine = tempStr.length();
					}

					partA = tempStr.substring(0, charPerLine);
					int lastIndex = partA.lastIndexOf(",");
					if ((lastIndex > 1) && ((tempStr.substring(charPerLine)).length() > charPerLine)) {
						tempSplit = lastIndex + 1;
						partA = tempStr.substring(0, tempSplit);
						partB = tempStr.substring(tempSplit);
					} else {
						partA = tempStr.substring(0, charPerLine);
						partB = tempStr.substring(charPerLine);
					}
					fromatStr += partA + "" + breakChar;
					tempStr = partB;
				}

				fromatStr += partB;

			} else {
				fromatStr = orgValue;
			}
		} catch (Exception e) {
			fromatStr = orgValue;
		}

		return fromatStr;
	}
}
