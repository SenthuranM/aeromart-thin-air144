/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.core.util;

/**
 * Class to keep constants related to Reporting. The Constants defined in this class should only use within the module
 * 
 * @author Byorn
 */
public abstract class InternalConstants {

	public static String MODULE_NAME = "reporting";

	public static interface DAOProxyNames {
		public static String DataExtractionDAO = "FlightsForInventoryDAO";
		public static String REPORTS_DAO = "reportsDAO";
		public static String MISDataProviderDAO = "misDataProviderDAO";
		public static String FLADataProviderDAO = "flaDataProviderDAO";
	}

	/** Scheduled reports related constants */
	public static interface ScheduledReportsConst {

		public static final String SCHEDULED_REPORT_JOB_BEAN_NAME = "scheduledReportGenJob";

		public static final String SCHEDULED_REPORT_JOB_PRIFIX = "scheduedReport:";
	}
}
