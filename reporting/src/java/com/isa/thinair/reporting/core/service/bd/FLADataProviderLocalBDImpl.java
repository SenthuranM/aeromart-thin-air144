package com.isa.thinair.reporting.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.reporting.api.service.FLADataProviderBD;

/**
 * @author harsha
 */
@Local
public interface FLADataProviderLocalBDImpl extends FLADataProviderBD {

}
