package com.isa.thinair.reporting.core.util;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

/*
 * class for looking up the module.
 * @author Byorn
 */

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reporting.core.config.ReportingConfig;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;

public class LookupUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(InternalConstants.MODULE_NAME);
	}

	public static Object getBean(String beanName) {
		return LookupServiceFactory.getInstance().getModule(InternalConstants.MODULE_NAME).getLocalBean(beanName);
	}

	public static ReportingConfig getReportingConfig() {
		return (ReportingConfig) getInstance().getModuleConfig();
	}

	public static SchedulerBD getSchedularBD() {
		return (SchedulerBD) GenericLookupUtils.lookupEJB3Service(SchedulerConstants.MODULE_NAME, SchedulerBD.SERVICE_NAME,
				getInstance().getModuleConfig(), ReportingConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) GenericLookupUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME,
				getInstance().getModuleConfig(), ReportingConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) GenericLookupUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME,
				getInstance().getModuleConfig(), ReportingConstants.MODULE_NAME, "module.dependencymap.invalid");
	}
	
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}
	
	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) GenericLookupUtils.lookupEJB3Service(AirschedulesConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME, getInstance().getModuleConfig(), ReportingConstants.MODULE_NAME,
				"module.dependencymap.invalid");
	}

}
