package com.isa.thinair.reporting.core.persistent.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.model.BCDetailsDTO;
import com.isa.thinair.reporting.api.model.ClassofServiceDTO;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.model.FlaGraphBarBreakDwnDTO;
import com.isa.thinair.reporting.api.model.FlightInvBkngDTO;
import com.isa.thinair.reporting.api.model.FlightInventroyDTO;
import com.isa.thinair.reporting.api.model.OnDDetailsDTO;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.util.ReportUtils;

public class FLADataProviderDAOJDBCImpl extends PlatformBaseJdbcDAOSupport implements FLADataProviderDAOJDBC {

	public FlightInventroyDTO getFlightInventoryDetails(Integer flightID, String lastSevenDay, String LastThirtyDay,
			String departDate) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Date dtCurrDate = Calendar.getInstance().getTime();
		String strCurrDate = DateToString(dtCurrDate);
		StringBuffer sb = new StringBuffer();

		String sql = null;
		sb.append(" SELECT B.SEGMENT_CODE,B.BOOKING_CODE,B.CABIN_CLASS_CODE, ");
		sb.append(" B.allocated_seats,B.onhold_seats,B.sold_seats, ");
		sb.append(" A.first_booking_date,A.last_booking_date,A.thirty_days_count, ");
		sb.append(" A.seven_days_count, B.est_time_departure_local, ");
		sb.append(" B.est_time_departure_zulu,B.flight_number ");
		sb.append(" FROM (SELECT   F.FLIGHT_ID||fs.segment_code|| bc.booking_code JOIN_KEY, ");
		sb.append(" F.FLIGHT_ID,fs.segment_code, bc.booking_code, bc.cabin_class_code, ");
		sb.append(" MIN (status_mod_date) first_booking_date,  MAX (status_mod_date) ");
		sb.append(" last_booking_date, ");
		sb.append("  ");

		sb.append(" COUNT (DISTINCT (CASE WHEN status_mod_date > ");
		sb.append(" to_date('");
		sb.append(LastThirtyDay);
		sb.append(" 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" AND status_mod_date < ");
		sb.append(" to_date('");
		sb.append(departDate);
		sb.append(" 23:59:59', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" AND (p.status = 'CNF' OR p.status = 'OHD') AND ps.status = 'CNF' THEN p.pnr_pax_id  ");
		sb.append("  ELSE NULL END)) thirty_days_count, ");
		sb.append(" COUNT (DISTINCT (CASE WHEN status_mod_date > ");
		sb.append(" to_date('");
		sb.append(lastSevenDay);
		sb.append(" 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append("  AND status_mod_date < ");
		sb.append(" to_date('");
		sb.append(departDate);
		sb.append(" 23:59:59', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" AND (p.status = 'CNF' OR p.status = 'OHD') AND ps.status = 'CNF' THEN p.pnr_pax_id  ");
		sb.append(" ELSE NULL END)) seven_days_count ");
		sb.append(" FROM t_pnr_segment ps,t_flight_segment fs,t_pnr_pax_fare_segment pfs, ");
		sb.append(" t_pnr_pax_fare ppf, t_booking_class bc,  t_flight f,t_pnr_passenger p  ");
		sb.append("  WHERE ps.flt_seg_id = fs.flt_seg_id  AND fs.flight_id = ");
		sb.append(flightID);
		sb.append(" AND ps.pnr_seg_id = pfs.pnr_seg_id  ");
		sb.append(" AND ppf.ppf_id = pfs.ppf_id AND pfs.booking_code = bc.booking_code ");
		sb.append(" AND p.pnr_pax_id = ppf.pnr_pax_id ");
		sb.append(" AND p.pax_type_code IN ('AD', 'CH') AND fs.flight_id = f.flight_id ");
		sb.append(" AND bc.bc_type <> 'OPENRT'  ");
		sb.append(" GROUP BY F.FLIGHT_ID||fs.segment_code|| bc.booking_code, F.FLIGHT_ID, ");
		sb.append(" fs.segment_code, bc.booking_code, bc.cabin_class_code )A, ");
		sb.append(" (SELECT   F.FLIGHT_ID||FS.SEGMENT_CODE||bc.booking_code JOIN_KEY, ");
		sb.append(" F.FLIGHT_ID,FS.SEGMENT_CODE,bc.booking_code, bc.cabin_class_code, ");
		sb.append(" allocated_seats,(FSBC.onhold_seats-nvl((SELECT sum(onhold_seats) ");
		sb.append(" FROM t_fcc_seg_bc_nesting where ");
		sb.append(" from_fccsba_id=FSBC.fccsba_id ),0)+  nvl((SELECT sum(onhold_seats) FROM ");
		sb.append(" t_fcc_seg_bc_nesting  where to_fccsba_id=FSBC.fccsba_id ),0))onhold_seats, ");
		sb.append(" (FSBC.sold_seats-nvl((SELECT sum(sold_seats) FROM t_fcc_seg_bc_nesting ");
		sb.append(" where from_fccsba_id=FSBC.fccsba_id ),0)+ nvl((SELECT sum(sold_seats) ");
		sb.append(" FROM t_fcc_seg_bc_nesting where to_fccsba_id=FSBC.fccsba_id ),0)) ");
		sb.append(" sold_seats, fs.est_time_departure_local, fs.est_time_departure_zulu, ");
		sb.append(" f.flight_number FROM  t_booking_class bc,t_fcc_seg_bc_alloc fsbc,t_flight f, ");
		sb.append(" T_FLIGHT_SEGMENT FS ");
		sb.append(" WHERE  f.flight_id = ");
		sb.append(flightID);
		sb.append(" AND F.flight_id=FS.flight_id ");
		sb.append(" AND fsbc.booking_code = bc.booking_code ");
		sb.append(" AND fs.segment_code = fsbc.segment_code ");
		sb.append(" AND fs.flight_id = fsbc.flight_id AND bc.bc_type <> 'OPENRT' ");
		sb.append(" )B ");
		sb.append(" WHERE A.JOIN_KEY(+)=B.JOIN_KEY and (B.onhold_seats > 0 or B.sold_seats > 0) ");

		sql = sb.toString();

		FlightInventroyDTO flightinvDetDTO = (FlightInventroyDTO) template.query(sql, new ResultSetExtractor() {

			String segCode = null;
			String cos = null;
			Date firstBkngDate = null;
			Date lastBkngDate = null;
			Date depTime = null;
			ArrayList lstExistBCDet = null;
			FlightInventroyDTO flightInvDTO = null;

			HashMap<String, HashMap<String, ArrayList<BCDetailsDTO>>> mapSegvsCos = new HashMap<String, HashMap<String, ArrayList<BCDetailsDTO>>>();

			BCDetailsDTO bcDetailDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					flightInvDTO = new FlightInventroyDTO();
					while (rs.next()) {
						bcDetailDTO = new BCDetailsDTO();

						segCode = rs.getString("SEGMENT_CODE");
						cos = rs.getString("CABIN_CLASS_CODE");
						depTime = new java.util.Date(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL").getTime());
						flightInvDTO.setDepartDate(depTime);
						flightInvDTO.setFlightNo(rs.getString("FLIGHT_NUMBER"));
						flightInvDTO.setOnd(segCode);
						if (rs.getTimestamp("FIRST_BOOKING_DATE") != null) {
							firstBkngDate = new java.util.Date(rs.getTimestamp("FIRST_BOOKING_DATE").getTime());
						} else {
							firstBkngDate = new java.util.Date();
						}
						if (rs.getTimestamp("LAST_BOOKING_DATE") != null) {
							lastBkngDate = new java.util.Date(rs.getTimestamp("LAST_BOOKING_DATE").getTime());
						} else {
							lastBkngDate = new java.util.Date();
						}
						bcDetailDTO.setBkgClass(rs.getString("BOOKING_CODE"));
						bcDetailDTO.setAloc(rs.getInt("ALLOCATED_SEATS"));
						bcDetailDTO.setHold(rs.getInt("ONHOLD_SEATS"));
						bcDetailDTO.setLast(lastBkngDate);
						bcDetailDTO.setFirst(firstBkngDate);
						bcDetailDTO.setLast30(rs.getInt("THIRTY_DAYS_COUNT") - rs.getInt("SEVEN_DAYS_COUNT"));
						bcDetailDTO.setLast7(rs.getInt("SEVEN_DAYS_COUNT"));
						bcDetailDTO.setSold(rs.getInt("SOLD_SEATS"));
						bcDetailDTO.setOver30((rs.getInt("SOLD_SEATS") + rs.getInt("ONHOLD_SEATS"))
								- rs.getInt("THIRTY_DAYS_COUNT"));
						// Arrange data in hash maps segment wise
						if (mapSegvsCos.containsKey(segCode)) {
							HashMap<String, ArrayList<BCDetailsDTO>> exstCosVsBCMap = (HashMap<String, ArrayList<BCDetailsDTO>>) mapSegvsCos
									.get(segCode);
							if (exstCosVsBCMap.containsKey(cos)) {
								exstCosVsBCMap.get(cos).add(bcDetailDTO);
							} else {
								ArrayList<BCDetailsDTO> lstBCDtail = new ArrayList<BCDetailsDTO>();
								lstBCDtail.add(bcDetailDTO);
								exstCosVsBCMap.put(cos, lstBCDtail);
							}
						} else {
							HashMap<String, ArrayList<BCDetailsDTO>> mapCosvsBC = new HashMap<String, ArrayList<BCDetailsDTO>>();
							ArrayList<BCDetailsDTO> lstBCDtail = new ArrayList<BCDetailsDTO>();
							lstBCDtail.add(bcDetailDTO);
							mapCosvsBC.put(cos, lstBCDtail);
							mapSegvsCos.put(segCode, mapCosvsBC);
						}
					}

					ClassofServiceDTO cosDTO = null;
					OnDDetailsDTO ondDTO = null;
					// Fill up data to flightInvDTO from maps
					if (mapSegvsCos.size() != 0) {
						ArrayList<OnDDetailsDTO> listOndDTO = new ArrayList<OnDDetailsDTO>();
						Set<String> keySetSeg = mapSegvsCos.keySet();
						for (String keySeg : keySetSeg) {
							ArrayList<ClassofServiceDTO> lstCosDTO = new ArrayList<ClassofServiceDTO>();
							ondDTO = new OnDDetailsDTO();
							ondDTO.setOnd(keySeg);
							HashMap<String, ArrayList<BCDetailsDTO>> mapCosVBc = mapSegvsCos.get(keySeg);
							Set<String> keySetCos = mapCosVBc.keySet();
							for (String keyCos : keySetCos) {
								cosDTO = new ClassofServiceDTO();
								cosDTO.setCosClass(keyCos);
								cosDTO.setBcDetailLst(mapCosVBc.get(keyCos));
								lstCosDTO.add(cosDTO);
							}
							ondDTO.setCosLst(lstCosDTO);
							listOndDTO.add(ondDTO);
						}
						flightInvDTO.setOndDetailsLst(listOndDTO);
					}

				}
				return flightInvDTO;
			}

		});

		return flightinvDetDTO;

	}

	public String DateToString(Date date) {
		String formattedDate = null;
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}

	@Override
	public ArrayList getFlightInvGraphData(Integer flightID, String lastSeventhDay, String lastThirtyDay, String departDate,
			ArrayList<String> lstChannel) {
		ArrayList lstGraphData = null;
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String sql = null;
		Date dtCurrDate = Calendar.getInstance().getTime();

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT  sc.description channel, sc.sales_channel_code, one_month_cnt, seven_days_count ");
		sb.append(" FROM t_sales_channel sc  LEFT JOIN (SELECT   res.origin_channel_code AS channelcode, ");
		sb.append(" COUNT (DISTINCT p.pnr_pax_id) one_month_cnt, ");
		sb.append("  COUNT ((CASE WHEN status_mod_date >  ");
		sb.append(" to_date('");
		sb.append(lastSeventhDay);
		sb.append(" 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" AND status_mod_date <");
		sb.append(" to_date('");
		sb.append(departDate);
		sb.append(" 23:59:59', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" THEN p.pnr_pax_id  ELSE NULL END)) seven_days_count ");
		sb.append(" FROM t_reservation res,t_pnr_segment ps,t_flight_segment fs, ");
		sb.append(" t_pnr_passenger p,  t_pnr_pax_fare_segment ppfs, t_booking_class bc,");
		sb.append(" t_pnr_pax_fare ppf  ");
		sb.append(" WHERE res.pnr = ps.pnr AND ps.flt_seg_id = fs.flt_seg_id  AND (p.status = 'CNF' OR p.status = 'OHD') ");
		sb.append("  AND res.pnr = p.pnr AND ps.status = 'CNF' AND fs.status <> 'CNX' AND fs.flight_id = '");
		sb.append(flightID + "' ");
		sb.append(" AND status_mod_date BETWEEN ");
		sb.append(" to_date('");
		sb.append(lastThirtyDay);
		sb.append(" 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sb.append(" AND ");
		sb.append(" to_date('");
		sb.append(departDate);
		sb.append(" 23:59:59', 'dd/mm/yyyy hh24:mi:ss') AND ps.pnr_seg_id = ppfs.pnr_seg_id  AND bc.bc_type_id <> 3 ");
		sb.append("  AND ppf.ppf_id = ppfs.ppf_id AND p.pax_type_code IN ('AD', 'CH') ");
		sb.append(" AND ppfs.booking_code = bc.booking_code ");
		sb.append(" AND ppf.pnr_pax_id = p.pnr_pax_id ");
		sb.append(" GROUP BY res.origin_channel_code) aaa ");
		sb.append(" ON sc.sales_channel_code = aaa.channelcode ");
		if (lstChannel != null && !lstChannel.isEmpty()) {
			sb.append(" where ");
			sb.append(ReportUtils.getReplaceStringForIN("sc.description", lstChannel, false));
		}
		sql = sb.toString();

		lstGraphData = (ArrayList) template.query(sql, new ResultSetExtractor() {
			FlightInvBkngDTO flightInvBkngDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<FlightInvBkngDTO> lstGraphData = new ArrayList<FlightInvBkngDTO>();
				if (rs != null) {
					while (rs.next()) {
						flightInvBkngDTO = new FlightInvBkngDTO();
						flightInvBkngDTO.setChannel(rs.getString("CHANNEL"));
						flightInvBkngDTO.setLast7(rs.getInt("SEVEN_DAYS_COUNT"));
						flightInvBkngDTO.setLast30(rs.getInt("one_month_cnt"));
						lstGraphData.add(flightInvBkngDTO);
					}
				}
				return lstGraphData;
			}

		});

		return lstGraphData;
	}

	public ArrayList getGraphDataBreakDown(Integer flightID, String channel, ArrayList<Integer> nominalCodes) {
		ArrayList lstGraphData = null;
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String sql = null;
		Date dtCurrDate = Calendar.getInstance().getTime();

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT res.pnr, res.status, (res.total_pax_count + res.total_pax_child_count) total_pax_count , res.total_pax_infant_count , ");
		sb.append(" decode(res.owner_channel_code,4,'Web',res.owner_agent_code) owner_agent_code, ");
		sb.append(" decode(res.owner_channel_code,4,'Web Agent',ag.agent_name) agent_name, ");
		sb.append(" sc.description channel, (SELECT  -1*sum(pt.amount)  FROM t_pnr_passenger g,t_pax_transaction pt ");
		sb.append(" WHERE g.pnr=res.pnr and pt.pnr_pax_id = g.pnr_pax_id ");
		sb.append(" and pt.nominal_code in (");
		sb.append(Util.buildIntegerInClauseContent(nominalCodes));
		sb.append(") and g.pnr in(select pnr from t_pnr_segment ps where ps.flt_seg_id IN ");
		sb.append(" (SELECT fs.flt_seg_id FROM t_flight_segment fs WHERE fs.flight_id = ");
		sb.append(flightID.toString() + " ))");
		sb.append(" ) pay, (res.total_fare + res.total_cnx + res.total_mod + res.total_adj+ res.total_surcharges+ res.total_tax) charges ");
		sb.append(" FROM  t_reservation res, t_agent ag, t_sales_channel sc ");
		sb.append(" WHERE res.owner_agent_code = ag.agent_code(+) and res.origin_channel_code = sc.sales_channel_code ");
		sb.append(" and sc.description = '" + channel + "' ");
		sb.append(" and res.pnr in(select pnr from t_pnr_segment ps where ps.status = 'CNF' and ps.flt_seg_id IN ");
		sb.append(" (SELECT fs.flt_seg_id FROM t_flight_segment fs WHERE fs.flight_id = ");
		sb.append(flightID.toString() + ")) ");

		sql = sb.toString();

		lstGraphData = (ArrayList) template.query(sql, new ResultSetExtractor() {
			FlaGraphBarBreakDwnDTO graphBrkDwnDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<FlaGraphBarBreakDwnDTO> lstGraphData = new ArrayList<FlaGraphBarBreakDwnDTO>();
				if (rs != null) {
					while (rs.next()) {
						BigDecimal pay = rs.getBigDecimal("pay");
						BigDecimal charges = rs.getBigDecimal("charges");
						graphBrkDwnDTO = new FlaGraphBarBreakDwnDTO();
						graphBrkDwnDTO.setAgentCode(rs.getString("owner_agent_code"));
						graphBrkDwnDTO.setAgentName(rs.getString("agent_name"));
						graphBrkDwnDTO.setPnr(rs.getString("pnr"));
						graphBrkDwnDTO.setStatus(rs.getString("status"));
						graphBrkDwnDTO.setTotPaxCnt(rs.getInt("total_pax_count"));
						graphBrkDwnDTO.setTotInfCnt(rs.getInt("total_pax_infant_count"));
						if (pay != null) {
							graphBrkDwnDTO.setTotCharges(pay.toString());
						} else {
							graphBrkDwnDTO.setTotCharges("&nbsp;");
						}
						if (pay != null) {
							graphBrkDwnDTO.setTotPayments(charges.toString());
						} else {
							graphBrkDwnDTO.setTotPayments("&nbsp;");
						}
						lstGraphData.add(graphBrkDwnDTO);
					}
				}
				return lstGraphData;
			}
		});

		return lstGraphData;
	}

	@Override
	public HashMap<String, Date> getGraphDataModificationDates(ArrayList<String> lstPnr) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String sql = null;
		HashMap<String, Date> mapGraphData = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" select ps.pnr, max(ps.status_mod_date) as mod_date from t_pnr_segment ps ");
		sb.append(" where ps.status = 'CNF' and  ps.pnr in (");
		sb.append(Util.buildStringInClauseContent(lstPnr));
		sb.append(") group by ps.pnr order by ps.pnr ");

		sql = sb.toString();
		mapGraphData = (HashMap<String, Date>) template.query(sql, new ResultSetExtractor() {
			HashMap<String, Date> mapGraphData = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					mapGraphData = new HashMap<String, Date>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						Date lastModDate = new java.util.Date(rs.getTimestamp("mod_date").getTime());
						mapGraphData.put(pnr, lastModDate);
					}
				}
				return mapGraphData;
			}
		});

		return mapGraphData;
	}

	@Override
	public List getFLAReservationAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List reservationAlertLst = null;
		String sql = null;
		StringBuffer sb = new StringBuffer();

		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}

		sb.append(" SELECT alt.alert_id, alt.content, to_char(alt.TIMESTAMP, 'DD-Mon-YY hh24:mi') TIMESTAMP, alt.status, ");
		sb.append(" alt.original_dep_date, ps.pnr, fs.flight_id,res.booking_timestamp,  ");
		sb.append(" (INITCAP (rc.c_title) ||' '||INITCAP (rc.c_first_name) || ' ' ||    ");
		sb.append(" INITCAP (rc.c_last_name)) pax_name,res.pnr, sc.description channel, ");
		sb.append(" decode(ps.alert_flag,0,'ACTIONED','NOT ACTIONED') action, atyp.description, ppfs.booking_code, ");
		sb.append(" rc.C_PHONE_NO , rc.C_MOBILE_NO ");
		sb.append(" FROM t_alert alt, t_pnr_segment ps, t_flight_segment fs, t_reservation res, t_reservation_contact rc , ");
		sb.append(" t_alert_type atyp, t_sales_channel sc,t_pnr_passenger pp, t_pnr_pax_fare ppf");
		sb.append(" , t_pnr_pax_fare_segment ppfs  WHERE alt.pnr_seg_id = ps.pnr_seg_id  ");
		sb.append(" AND res.pnr=rc.pnr AND res.pnr = ps.pnr and res.owner_channel_code = sc.sales_channel_code ");
		sb.append(" and atyp.alert_type_id = alt.alert_type_id and pp.pnr = res.pnr ");
		sb.append(" AND ps.flt_seg_id = fs.flt_seg_id  and pp.pnr_pax_id = ppf.pnr_pax_id ");
		sb.append(" and ppfs.pnr_seg_id = ps.pnr_seg_id ");
		sb.append(" and ppf.ppf_id = ppfs.ppf_id AND fs.flight_id = '");
		sb.append(flightID.toString());
		sb.append("' ");

		sql = sb.toString();
		reservationAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> lstResAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					lstResAlert = new ArrayList<FlaAlertDTO>();

					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(rs.getInt("alert_id")));
						flaAlertDTO.setContent(rs.getString("content"));
						flaAlertDTO.setAlertDate((rs.getString("TIMESTAMP")));
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("description"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						if (rs.getString("booking_code") != null)
							flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						else
							flaAlertDTO.setBkngClass("");
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						lstResAlert.add(flaAlertDTO);
					}

				}
				return lstResAlert;
			}

		});
		return reservationAlertLst;
	}

	@Override
	public List getFLADuplicateNameAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List dupNameAlertLst = null;

		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}

		String sql = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT distinct (UPPER (rc.c_title)|| ' ' || INITCAP (rc.c_first_name)  || ' '|| ");
		sb.append(" INITCAP (rc.c_last_name) ) pax_name,pp.pnr, to_char(res.booking_timestamp, ");
		sb.append(" 'DD-Mon-YY hh24:mi') booking_timestamp,  sc.description channel,  ");
		sb.append(" ppfs.booking_code,'Dup Name' alert_type, 'NOT ACTIONED' action ");
		sb.append(" , rc.C_PHONE_NO , rc.C_MOBILE_NO ");
		sb.append(" FROM t_pnr_passenger pp,t_reservation res, t_reservation_contact rc, t_sales_channel sc,  ");
		sb.append(" t_pnr_pax_fare ppf,t_pnr_pax_fare_segment ppfs ");
		sb.append(" WHERE ppfs.pnr_SEG_ID IN ( SELECT PS.pnr_SEG_ID FROM t_flight_segment fs, t_pnr_segment ps ");
		sb.append(" WHERE fs.flight_id = '");
		sb.append(flightID.toString());
		sb.append("' AND ps.flt_seg_id = fs.flt_seg_id and ps.status= 'CNF') ");
		sb.append(" AND pp.pax_type_code IN ('AD', 'CH') AND pp.pnr = res.pnr AND res.pnr=rc.pnr ");
		sb.append(" AND res.status = 'CNF' and pp.status = 'CNF'  ");
		sb.append(" AND sc.sales_channel_code = res.owner_channel_code ");
		sb.append(" AND pp.pnr_pax_id = ppf.pnr_pax_id ");
		sb.append(" AND ppfs.ppf_id = ppf.ppf_id and (pp.title,pp.first_name,pp.last_name) in ( ");
		sb.append(" SELECT a.title, a.first_name, a.last_name ");
		sb.append(" FROM t_pnr_passenger a where FIRST_NAME<>'T B A' and LAST_NAME<>'T B A' ");
		sb.append(" and status='CNF' AND PNR IN	(SELECT PNR FROM T_PNR_SEGMENT pnrseg, ");
		sb.append(" t_flight_segment fs WHERE pnrseg.flt_seg_id = fs.flt_seg_id ");
		sb.append(" and pnrseg.status= 'CNF' and fs.flight_id = '");
		sb.append(flightID.toString());
		sb.append("') GROUP BY a.title, a.first_name, a.last_name ");
		sb.append(" HAVING COUNT('X')>1 ) ");

		sql = sb.toString();
		dupNameAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> lstDupNameAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					lstDupNameAlert = new ArrayList<FlaAlertDTO>();
					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(0));
						flaAlertDTO.setContent("");
						// flaAlertDTO.setAlertDate(new java.util.Date());
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("alert_type"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						lstDupNameAlert.add(flaAlertDTO);
					}

				}
				return lstDupNameAlert;
			}
		});
		return dupNameAlertLst;
	}

	@Override
	public List getFLAPartialCreditAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List partialPayAlertLst = null;
		String sql = null;
		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}
		StringBuffer sb = new StringBuffer();
		sb.append("select * from (SELECT pp.pnr_pax_id, res.pnr, ");
		sb.append(" (rc.c_title || ' ' || INITCAP (rc.c_first_name)|| ' '|| INITCAP (rc.c_last_name)) pax_name, ");
		sb.append("TO_CHAR (res.booking_timestamp,'DD-Mon-YY hh24:mi') booking_timestamp, ");
		sb.append("sc.description channel, ppfs.booking_code, ");
		sb.append("  rc.C_PHONE_NO , rc.C_MOBILE_NO, ");
		sb.append("	 (select sum(amount) from t_pax_transaction pt where pt.pnr_pax_id=pp.pnr_pax_id)amount, ");
		sb.append("	'Partial Payments' alert_type, 'NOT ACTIONED' action ");
		sb.append("FROM t_pnr_passenger pp,t_reservation res, t_reservation_contact rc, t_sales_channel sc,t_pnr_pax_fare ppf,t_pnr_pax_fare_segment ppfs,t_pnr_segment ps ");
		sb.append("WHERE res.pnr=rc.pnr AND pp.pnr = res.pnr AND res.status = 'CNF' AND PS.status='CNF' ");
		sb.append("AND pp.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id ");
		sb.append("AND res.owner_channel_code = sc.sales_channel_code ");
		sb.append("AND pp.pnr =ps.pnr ");
		sb.append("AND ps.pnr_seg_id=ppfs.pnr_seg_id ");
		sb.append("AND flt_seg_id in (SELECT flt_seg_id  FROM t_flight_segment ");
		sb.append("       WHERE flight_id = ");
		sb.append(flightID.toString());
		sb.append("))where amount>0 ");

		sql = sb.toString();
		partialPayAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> lstPartialPayAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					lstPartialPayAlert = new ArrayList<FlaAlertDTO>();
					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(0));
						flaAlertDTO.setContent("");
						// flaAlertDTO.setAlertDate(new java.util.Date());
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("alert_type"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						lstPartialPayAlert.add(flaAlertDTO);
					}

				}
				return lstPartialPayAlert;
			}

		});
		return partialPayAlertLst;
	}

	@Override
	public List getFLATbaNamesAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List tbaNameAlertLst = null;
		String sql = null;
		StringBuffer sb = new StringBuffer();

		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}

		sb.append(" select distinct (rc.c_title||' '||rc.c_first_name|| ' '|| rc.c_last_name) pax_name, ");
		sb.append("  r.pnr, to_char(r.booking_timestamp, 'DD-Mon-YY hh24:mi') booking_timestamp, ");
		sb.append(" sc.description channel, ppfs.booking_code, ");
		sb.append(" 'TBA Name' alert_type, 'NOT ACTIONED' action ");
		sb.append(" , rc.C_PHONE_NO , rc.C_MOBILE_NO ");
		sb.append(" from t_pnr_passenger p,T_RESERVATION r, T_RESERVATION_CONTACT rc, t_pnr_pax_fare ppf, ");
		sb.append(" t_pnr_pax_fare_segment ppfs,t_sales_channel sc,t_pnr_segment PS ");
		sb.append(" where   flt_seg_id in(Select FS.flt_seg_id from t_flight_segment FS where FS.flight_id =");
		sb.append(flightID.toString());
		sb.append(") and first_name = 'T B A'  and last_name = 'T B A' and r.pnr=rc.pnr AND p.pnr=r.pnr and r.status='CNF' ");
		sb.append(" AND P.PNR_PAX_ID=PPF.PNR_PAX_ID AND PPF.PPF_ID=PPFS.PPF_ID AND ");
		sb.append(" sc.sales_channel_code = r.owner_channel_code   ");
		sb.append(" AND PS.PNR_SEG_ID=PPFS.PNR_SEG_ID AND PS.status='CNF' ");
		sb.append(" AND p.pax_type_code IN ('AD', 'CH') ");

		sql = sb.toString();
		tbaNameAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> lstTBANameAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					lstTBANameAlert = new ArrayList<FlaAlertDTO>();
					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(0));
						flaAlertDTO.setContent("");
						// flaAlertDTO.setAlertDate(new java.util.Date());
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("alert_type"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						lstTBANameAlert.add(flaAlertDTO);
					}

				}
				return lstTBANameAlert;
			}

		});
		return tbaNameAlertLst;
	}

	@Override
	public List getGroupBookingsAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List groupBkngAlertLst = null;
		String sql = null;
		StringBuffer sb = new StringBuffer();

		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}

		sb.append("SELECT res.total_pax_count + res.total_pax_child_count pax_count, res.pnr, ");
		sb.append("  ( rc.c_title || ' '||INITCAP(rc.c_first_name) || ' ' || INITCAP(rc.c_last_name)) pax_name, ");
		sb.append("   TO_CHAR (res.booking_timestamp, 'DD-Mon-YY hh24:mi') booking_timestamp, ");
		sb.append("   sc.description channel, ");
		sb.append("  rc.C_PHONE_NO , rc.C_MOBILE_NO ,");
		sb.append("   (SELECT DISTINCT ppfs.booking_code ");
		sb.append("              FROM t_pnr_pax_fare_segment ppfs ");
		sb.append("              WHERE ppfs.pnr_seg_id IN ( ");
		sb.append("                       SELECT ps.pnr_seg_id ");
		sb.append("                         FROM t_pnr_segment ps ");
		sb.append("                        WHERE ps.pnr = res.pnr ");
		sb.append("AND flt_seg_id IN (SELECT flt_seg_id ");
		sb.append("                                              FROM t_flight_segment ");
		sb.append("                                             WHERE flight_id = " + flightID.toString() + ")) ");
		sb.append("                AND booking_code IS NOT NULL ");
		sb.append("                AND ROWNUM = 1) booking_code, ");
		sb.append("  'Group Booking' alert_type, 'NOT ACTIONED' action ");
		sb.append(" , rc.C_PHONE_NO , rc.C_MOBILE_NO ");
		sb.append("FROM t_reservation res, t_reservation_contact rc, t_sales_channel sc ");
		sb.append("WHERE res.pnr=rc.pnr AND res.owner_channel_code = sc.sales_channel_code ");
		sb.append("AND res.status = 'CNF' ");
		sb.append("AND res.pnr IN (SELECT ps.pnr ");
		sb.append("                 FROM t_pnr_segment ps ");
		sb.append("                WHERE flt_seg_id IN (SELECT flt_seg_id ");
		sb.append("                                       FROM t_flight_segment ");
		sb.append("                                      WHERE flight_id = " + flightID.toString() + ")) ");
		sb.append("AND (res.total_pax_count + res.total_pax_child_count) > ");
		sb.append("                                          (SELECT ap.param_value ");
		sb.append("                                             FROM t_app_parameter ap ");
		sb.append("                                            WHERE ap.param_key = 'RES_7') ");

		sql = sb.toString();
		groupBkngAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> groupBkngAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					groupBkngAlert = new ArrayList<FlaAlertDTO>();

					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(0));
						flaAlertDTO.setContent("");
						// flaAlertDTO.setAlertDate(new java.util.Date());
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("alert_type"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						groupBkngAlert.add(flaAlertDTO);
					}

				}
				return groupBkngAlert;
			}

		});
		return groupBkngAlertLst;

	}

	@Override
	public List getFLADupPassportAlert(List<Integer> flightIDLst) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List groupBkngAlertLst = null;
		String sql = null;
		StringBuffer sb = new StringBuffer();

		Integer flightID = null;
		if (flightIDLst != null) {
			flightID = flightIDLst.get(0);// current impl has only one flight ID
		}

		sb.append(" SELECT  ppa.PASSPORT_NUMBER,(UPPER (rc.c_title)|| ' ' || UPPER (rc.c_first_name)   ");
		sb.append(" || ' '|| UPPER (rc.c_last_name) ) pax_name,pp.pnr,to_char(res.booking_timestamp, ");
		sb.append(" 'DD-Mon-YY hh24:mi') booking_timestamp,  ");
		sb.append("  rc.C_PHONE_NO , rc.C_MOBILE_NO , ");
		sb.append(" sc.description channel, ppfs.booking_code,'Dup passport' alert_type, 'NOT ACTIONED' ");
		sb.append(" action  FROM t_pnr_passenger pp, T_PNR_PAX_ADDITIONAL_INFO ppa, t_reservation res, t_reservation_contact rc, t_sales_channel sc, ");
		sb.append(" t_pnr_pax_fare ppf,t_pnr_pax_fare_segment ppfs ");
		sb.append("  WHERE pp.pnr IN ( SELECT DISTINCT ps.pnr FROM t_flight_segment fs, t_pnr_segment ps ");
		sb.append(" WHERE fs.flight_id = ");
		sb.append(flightID.toString());
		sb.append(" AND ps.flt_seg_id = fs.flt_seg_id and ps.status= 'CNF') ");
		sb.append(" AND pp.pax_type_code IN ('AD', 'CH') AND res.pnr=rc.pnr AND pp.pnr = res.pnr  ");
		sb.append(" AND res.status = 'CNF' and pp.status = 'CNF' AND sc.sales_channel_code = res.owner_channel_code  ");
		sb.append(" AND pp.pnr_pax_id = ppf.pnr_pax_id AND ppfs.ppf_id = ppf.ppf_id AND pp.pnr = ppa.pnr_pax_id  ");
		sb.append(" group by ppa.PASSPORT_NUMBER,rc.c_title,rc.c_first_name,rc.c_last_name ,rc.C_PHONE_NO , rc.C_MOBILE_NO,pp.pnr, ");
		sb.append(" res.booking_timestamp,  sc.description, ppfs.booking_code ");
		sb.append(" having count(ppa.PASSPORT_NUMBER) > 1 ");
		sb.append("  ");

		sql = sb.toString();
		groupBkngAlertLst = (List) template.query(sql, new ResultSetExtractor() {
			ArrayList<FlaAlertDTO> groupBkngAlert = null;
			FlaAlertDTO flaAlertDTO = null;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					groupBkngAlert = new ArrayList<FlaAlertDTO>();

					while (rs.next()) {
						flaAlertDTO = new FlaAlertDTO();
						flaAlertDTO.setAlertId(new Integer(0));
						flaAlertDTO.setContent("");
						// flaAlertDTO.setAlertDate(new java.util.Date());
						flaAlertDTO.setPnr(rs.getString("pnr"));
						flaAlertDTO.setAlertType(rs.getString("alert_type"));
						flaAlertDTO.setIsActioned(rs.getString("action"));
						flaAlertDTO.setPassengerName(rs.getString("pax_name"));
						flaAlertDTO.setBkngMode(rs.getString("channel"));
						flaAlertDTO.setBkngClass(rs.getString("booking_code"));
						flaAlertDTO.setSystemRemarks("");
						flaAlertDTO.setBkngDate(rs.getString("booking_timestamp"));
						flaAlertDTO.setMobileNo(rs.getString("C_MOBILE_NO"));
						flaAlertDTO.setPhoneNo(rs.getString("C_PHONE_NO"));
						groupBkngAlert.add(flaAlertDTO);
					}

				}
				return groupBkngAlert;
			}

		});
		return groupBkngAlertLst;

	}

}
