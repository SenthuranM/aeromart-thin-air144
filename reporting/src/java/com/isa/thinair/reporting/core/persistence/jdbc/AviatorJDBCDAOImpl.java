package com.isa.thinair.reporting.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCInvDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCinfoDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightLegDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightSegInventoryDTO;
import com.isa.thinair.reporting.core.persistence.dao.AviatorDAO;
import com.isa.thinair.reporting.core.util.LookupUtils;


/**
* @author Manoj Dhanushka
* 
*/
public class AviatorJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements AviatorDAO {
	
	public static final String FIXED_FLAG_Y = "Y";
	
	@SuppressWarnings("unchecked")
	public List<AviatorFlightLegDTO> getFlightLegDTOs() throws ModuleException {

		String query = " SELECT f.flight_id          ,"
				+ "  fl.flight_leg_id           ,"
				+ "  f.flight_number            ,"
				+ "  f.departure_date           ,"
				+ "  f.model_number             ,"
				+ "  fl.leg_number              ,"
				+ "  fl.origin                  ,"
				+ "  fl.est_time_departure_zulu ,"
				+ "  fl.est_departure_day_offset,"
				+ "  fl.destination             ,"
				+ "  fl.est_arrival_day_offset  ,"
				+ "  fl.est_time_arrival_zulu	"
				+ "  FROM t_flight f,	"
				+ "  t_flight_leg fl	"
				+ " WHERE f.flight_id = fl.flight_id	"
				//Comment this
				//+ " AND f.flight_id     in ('267507')	"
				+ " AND ((f.departure_date             > sysdate	"
				//+ " AND f.departure_date < (TRUNC(sysdate)    + 90 ) + 0/24 "
				+ " AND f.status                      IN ('ACT','CRE','CLS'))	"
				+ " OR ((TRUNC(sysdate)    - 2) + 0/24 < f.departure_date	"
				+ " AND f.departure_date               < sysdate	"
				+ " AND f.status                      IN ('ACT','CRE','CLS')))	"
				+ " ORDER BY f.departure_date,	"
				+ "  f.flight_id            ,	"
				+ "  fl.leg_number";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<AviatorFlightLegDTO> aviatorFlightLegDTOs = (List<AviatorFlightLegDTO>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AviatorFlightLegDTO> colFlightLegDTOs = new ArrayList<AviatorFlightLegDTO>();

				while (rs.next()) {
					AviatorFlightLegDTO flightLegDTO = new AviatorFlightLegDTO();
					flightLegDTO.setFlightId(rs.getInt("flight_id"));
					flightLegDTO.setFlightLegId(rs.getInt("flight_leg_id"));
					flightLegDTO.setFlightNumber(rs.getString("flight_number"));
					flightLegDTO.setDepartureDate(rs.getDate("departure_date"));
					flightLegDTO.setModelNumber(rs.getString("model_number"));
					flightLegDTO.setLegNumber(rs.getInt("leg_number"));
					flightLegDTO.setOrigin(rs.getString("origin"));
					flightLegDTO.setEstDepartureTime(rs.getTime("est_time_departure_zulu"));
					flightLegDTO.setEstDepartureDayOffset(rs.getInt("est_departure_day_offset"));
					flightLegDTO.setDestination(rs.getString("destination"));
					flightLegDTO.setEstArrivalTime(rs.getTime("est_time_arrival_zulu"));
					flightLegDTO.setEstArrivalDayOffset(rs.getInt("est_arrival_day_offset"));					

					colFlightLegDTOs.add(flightLegDTO);
				}
				return colFlightLegDTOs;
			}
		});
		
		addLocalTimeDetailsToFlightLeg(aviatorFlightLegDTOs);
		return aviatorFlightLegDTOs;
	}
	
	/**
	 * private method to add time details to the flight leg
	 */
	private void addLocalTimeDetailsToFlightLeg(List<AviatorFlightLegDTO> colFlightLegDTOs) throws ModuleException {

		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(LookupUtils.getAirportBD());
		Iterator<AviatorFlightLegDTO> itLegs = colFlightLegDTOs.iterator();

		while (itLegs.hasNext()) {

			AviatorFlightLegDTO leg = (AviatorFlightLegDTO) itLegs.next();

			// fields to find out
			Date estDepartureTimeLocal = null;
			Date estArrivalTimeLocal = null;
			int estArrivalDayOffsetLocal = 0;

			String origin = leg.getOrigin();
			String destination = leg.getDestination();
			Date startDateZulu = leg.getDepartureDate();
			// EFFECTIVE DATE for the time conversion
			Date effectiveDate = startDateZulu;

			Date estDepartureTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(leg.getDepartureDate(), leg.getEstDepartureTime(),
					leg.getEstDepartureDayOffset());
			Date estArrivalTimeZulu = CalendarUtil.getOfssetAndTimeAddedDate(leg.getDepartureDate(), leg.getEstArrivalTime(),
					leg.getEstArrivalDayOffset());

			// here effective date for estDepartureTimeZulu is the same date
			estDepartureTimeLocal = timeAdder.getLocalDateTime(origin, estDepartureTimeZulu);

			// here effective date for estArrivalTimeZulu is the same date
			estArrivalTimeLocal = timeAdder.getLocalDateTime(destination, estArrivalTimeZulu);

			Date startDateLocal = timeAdder.getLocalDateTimeAsEffective(origin, effectiveDate, startDateZulu);
			estArrivalDayOffsetLocal = CalendarUtil.daysUntil(startDateLocal, estArrivalTimeLocal);

			// setting found out fields
			leg.setEstDepartureTimeLocal(CalendarUtil.getOnlyTime(estDepartureTimeLocal));
			leg.setEstArrivalTimeLocal(CalendarUtil.getOnlyTime(estArrivalTimeLocal));
			leg.setEstArrivalDayOffsetLocal(estArrivalDayOffsetLocal);
			leg.setDepartureDateLocal(startDateLocal);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<Integer, List<AviatorFlightSegInventoryDTO>> getFlightLegInventory() throws ModuleException {

		String query = "   SELECT sa.flight_id         ,"
				  + "  sa.segment_code            ,"
				  + "  sa.logical_cabin_class_code,"
				  + "  sa.allocated_seats, "
				  + "  sa.oversell_seats, "
				  + "  sa.curtailed_seats, "
				  + "  sa.allocated_waitlist_seats,"
				  + "  sa.sold_seats + sa.on_hold_seats as total_bookings,"
				  + "  sa.available_seats, "
				  + "  sa.waitlisted_seats, "
				  + "  fs.status "
				  + "   FROM t_fcc_seg_alloc sa,"
				  + "   t_flight_segment fs , "
				  + "   t_flight f "
				  + "  WHERE "
				  //Comment this
				  //+ "  sa.flight_id in ('267507') AND "
				  + "  sa.flight_id = f.flight_id "
				  + "  AND ((f.departure_date             > sysdate "
				  //+ "  AND f.departure_date < (TRUNC(sysdate)    + 90 ) + 0/24 "
				  + "  AND f.status                      IN ('ACT','CRE','CLS')) "
				  + "  OR ((TRUNC(sysdate)    - 2) + 0/24 < f.departure_date "
				  + "  AND f.departure_date               < sysdate "
				  + "  AND f.status                      IN ('ACT','CRE','CLS'))) "
				  + "  and sa.flt_seg_id = fs.flt_seg_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Map<Integer, List<AviatorFlightSegInventoryDTO>> aviatorFlightLegInvDTOsMap = (Map<Integer, List<AviatorFlightSegInventoryDTO>>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, List<AviatorFlightSegInventoryDTO>> colFlightLegInvDTOs = new HashMap<Integer, List<AviatorFlightSegInventoryDTO>>();

				while (rs.next()) {
					
					AviatorFlightSegInventoryDTO flightLegInvDTO = new AviatorFlightSegInventoryDTO();
					Integer flightId = rs.getInt("flight_id");
					flightLegInvDTO.setFlightId(flightId);
					flightLegInvDTO.setSegmentCode(rs.getString("segment_code"));
					flightLegInvDTO.setCabinClassCode(rs.getString("logical_cabin_class_code"));
					flightLegInvDTO.setCapacity(rs.getInt("allocated_seats"));
					flightLegInvDTO.setOverSellAdult(rs.getInt("oversell_seats"));
					flightLegInvDTO.setCurtail(rs.getInt("curtailed_seats"));
					flightLegInvDTO.setWlAlloc(rs.getInt("allocated_waitlist_seats"));
					flightLegInvDTO.setTotalBookings(rs.getInt("total_bookings"));
					flightLegInvDTO.setAvailableAdult(rs.getInt("available_seats"));
					flightLegInvDTO.setWaitlistAdult(rs.getInt("waitlisted_seats"));
					flightLegInvDTO.setFltSegStatus(rs.getString("status"));
					
					int overbookCount = (flightLegInvDTO.getTotalBookings() - flightLegInvDTO.getOverSellAdult()
									- flightLegInvDTO.getCapacity() + flightLegInvDTO.getCurtail());
					flightLegInvDTO.setOverbookAdult(overbookCount < 0 ? 0 : overbookCount);
	
					if(colFlightLegInvDTOs.containsKey(flightId)) {
						colFlightLegInvDTOs.get(flightId).add(flightLegInvDTO);
					} else {
						List<AviatorFlightSegInventoryDTO> fltLegInvList = new ArrayList<AviatorFlightSegInventoryDTO>();
						fltLegInvList.add(flightLegInvDTO);
						colFlightLegInvDTOs.put(flightId, fltLegInvList);
					}
				}
				return colFlightLegInvDTOs;
			}
		});
		return aviatorFlightLegInvDTOsMap;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Integer, List<AviatorFlightBCInvDTO>> getFlightSegmentDetails() throws ModuleException {

		String query = "  SELECT f.flight_id                              ,"
				  + "  fs.flt_seg_id                                      ,"
				  + "  f.flight_number                                    ,"
				  + "  f.departure_date                                   ,"
				  + "  sa.segment_code                                    ,"
				  + "  sa.logical_cabin_class_code                        ,"
				  + "  sba.booking_code                                   ,"
				  + "  bc.nest_rank                                       ,"
				  + "  bc.fixed_flag                                      ,"
				  + "  sba.onhold_seats 								  ,"
				  + "  sba.waitlisted_seats                               ,"
				  + "  sba.sold_seats                                     ,"
				  + "  sba.allocated_seats                                ,"
				  + "  sba.available_seats                                ,"
				  + "  sba.status                                 		  ,"
				  + "  sba.cancelled_seats                                ,"
				  + "  sba.acquired_seats                                 ,"
				  + "  (SELECT NVL(SUM(sold_seats),0) "
				  + "  		FROM t_fcc_seg_bc_nesting "
	  			  + "  		where from_fccsba_id=sba.fccsba_id) as nested_sold_seats,"
  				  + "  	(SELECT NVL(SUM(onhold_seats),0) "
  				  + "  	FROM t_fcc_seg_bc_nesting "
  				  + "  	where from_fccsba_id=sba.fccsba_id) as nested_onhold_seats,"
  				  + "  	(SELECT  NVL(SUM(sold_seats),0) "
  				  + "  		FROM t_fcc_seg_bc_nesting "
  				  + "  		WHERE to_fccsba_id=sba.fccsba_id) as nested_aquired_sold_seats,"
  				  + "  	(SELECT NVL(SUM(onhold_seats),0) "
  				  + "  		FROM t_fcc_seg_bc_nesting "
  				  + "  		WHERE to_fccsba_id=sba.fccsba_id) as nested_acquired_onhold_seats,"
				  + "  ("
				  + "  (SELECT MIN(FARE_AMOUNT)"
				  + "     FROM t_ond_fare ofa"
				  + "    WHERE ofa.booking_code=bc.booking_code"
				  + "  AND ofa.ond_code        =sa.SEGMENT_CODE"
				  + "  AND f.DEPARTURE_DATE BETWEEN EFFECTIVE_FROM_DATE AND EFFECTIVE_TO_DATE"
				  + "  AND sysdate BETWEEN SALES_VALID_FROM AND SALES_VALID_TO"
				  + "  AND RT_EFFECTIVE_FROM_DATE IS NULL"
				  + "  ))min_ow_fare,"
				  + "(CASE WHEN EXISTS "
				  + "	(SELECT 1 "
				  + "		FROM t_ond_fare ofa "
				  + "		WHERE ofa.booking_code = bc.booking_code "
				  + "			AND ofa.ond_code != sa.SEGMENT_CODE "
				  + "			AND f.DEPARTURE_DATE BETWEEN EFFECTIVE_FROM_DATE AND EFFECTIVE_TO_DATE "
				  + "			AND sysdate BETWEEN SALES_VALID_FROM AND SALES_VALID_TO "
				  + "			AND (ofa.ond_code LIKE '%' || sa.SEGMENT_CODE || '%' )) "
				  + "THEN 1 ELSE 0 END) AS fare_cn,"
				  + "  ("
				  + "  (SELECT MIN(FARE_AMOUNT)"
				  + "     FROM t_ond_fare ofa"
				  + "    WHERE ofa.booking_code=bc.booking_code"
				  + "  AND ofa.ond_code        =sa.SEGMENT_CODE"
				  + "  AND f.DEPARTURE_DATE BETWEEN EFFECTIVE_FROM_DATE AND EFFECTIVE_TO_DATE"
				  + "  AND sysdate BETWEEN SALES_VALID_FROM AND SALES_VALID_TO"
				  + "  AND RT_EFFECTIVE_FROM_DATE IS NOT NULL"
				  + "  ))min_rt_fare,"
				  + "  (CASE WHEN bc.standard_code='Y' AND sba.status = 'OPN' AND bc.nest_rank > 0 THEN"
				  + "  (SELECT SUM(available_seats) FROM t_fcc_seg_bc_alloc a,t_booking_class b"
			      + "  where fccsa_id    = sa.fccsa_id and b.booking_code=a.booking_code"
			      + "  AND a.status      = 'OPN' AND b.nest_rank  <= bc.nest_rank  and ("       
			      + "      ( bc.allocation_type = 'SEG' and b.allocation_type in ('SEG','COM')) or"
			      + "      ( bc.allocation_type = 'RET' and b.allocation_type in ('RET','COM')) or"
			      + "      ( bc.allocation_type = 'CON' and b.allocation_type in ('CON','COM')) or"
			      + "      ( bc.allocation_type = 'COM' and b.allocation_type in ('SEG','RET','CON','COM'))"
			      + "  ))"
			      + "  ELSE sba.available_seats  END) AS nested_available_seats"
				  + "   FROM t_flight f      ,"
				  + "  t_flight_segment fs   ,"
				  + "  t_fcc_seg_alloc sa    ,"
				  + "  t_fcc_seg_bc_alloc sba,"
				  + "  t_booking_class bc"
				  + "  WHERE f.flight_id            = fs.flight_id"
				  + "  AND fs.flt_seg_id              = sa.flt_seg_id"
				  + "  AND sa.fccsa_id                = sba.fccsa_id"
				  + "  AND sba.booking_code           = bc.booking_code"
				  + "  AND sa.logical_cabin_class_code=bc.logical_cabin_class_code"
				  //Comment this
				  //+ "  AND f.flight_id                in ('267507')"
				  + "  AND ((f.departure_date             > sysdate "
				  //+ "  AND f.departure_date < (TRUNC(sysdate)    + 90 ) + 0/24 "
				  + "  AND f.status                      IN ('ACT','CRE','CLS')) "
				  + "  OR ((TRUNC(sysdate)    - 2) + 0/24 < f.departure_date "
				  + "  AND f.departure_date               < sysdate "
				  + "  AND f.status                      IN ('ACT','CRE','CLS'))) "
				  + "  ORDER BY sa.segment_code";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Map<Integer, List<AviatorFlightBCInvDTO>> aviatorFlightSegDTOsMap = (Map<Integer, List<AviatorFlightBCInvDTO>>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, List<AviatorFlightBCInvDTO>> colFlightSegDTOs = new HashMap<Integer, List<AviatorFlightBCInvDTO>>();

				while (rs.next()) {
					
					AviatorFlightBCInvDTO flightSegDTO = new AviatorFlightBCInvDTO();
					Integer flightId = rs.getInt("flight_id");
					flightSegDTO.setFlightId(flightId);
					flightSegDTO.setFlightSegId(rs.getInt("flt_seg_id"));
					flightSegDTO.setSegmentCode(rs.getString("segment_code"));
					flightSegDTO.setLogicalCabinClass(rs.getString("logical_cabin_class_code"));
					flightSegDTO.setBookingCode(rs.getString("booking_code"));
					flightSegDTO.setFixedFlag(rs.getString("fixed_flag").equals(FIXED_FLAG_Y)? 1 : 0);
					flightSegDTO.setStatus(rs.getString("status").equals("OPN")? 1 : 0);
					flightSegDTO.setNestRank(rs.getInt("nest_rank"));
					flightSegDTO.setOnholdSeats(rs.getInt("onhold_seats"));
					flightSegDTO.setWaitlistedSeats(rs.getInt("waitlisted_seats"));
					flightSegDTO.setCancelledSeats(rs.getInt("cancelled_seats"));
					flightSegDTO.setAcquiredSeats(rs.getInt("acquired_seats"));
					flightSegDTO.setIsConnectionFareAvailable(rs.getBoolean("fare_cn"));
					flightSegDTO.setSoldSeats(rs.getInt("sold_seats"));
					flightSegDTO.setAllocatedSeats(rs.getInt("allocated_seats"));
					flightSegDTO.setAvailableSeats(rs.getInt("available_seats"));
					flightSegDTO.setAvailableNestedSeats(rs.getInt("nested_available_seats"));
					flightSegDTO.setSeatsSoldNested(rs.getInt("nested_sold_seats"));
					flightSegDTO.setSeatsSoldAquiredByNesting(rs.getInt("nested_aquired_sold_seats"));
					flightSegDTO.setSeatsOnHoldNested(rs.getInt("nested_onhold_seats"));
					flightSegDTO.setSeatsOnHoldAquiredByNesting(rs.getInt("nested_acquired_onhold_seats"));
					flightSegDTO.setOwFare(rs.getDouble("min_ow_fare"));
					flightSegDTO.setRtFare(rs.getDouble("min_rt_fare"));

					if(colFlightSegDTOs.containsKey(flightId)) {
						colFlightSegDTOs.get(flightId).add(flightSegDTO);
					} else {
						List<AviatorFlightBCInvDTO> fltSegList = new ArrayList<AviatorFlightBCInvDTO>();
						fltSegList.add(flightSegDTO);
						colFlightSegDTOs.put(flightId, fltSegList);
					}
				}
				return colFlightSegDTOs;
			}
		});
		return aviatorFlightSegDTOsMap;
	}
	
	@SuppressWarnings("unchecked")
	public  Map<Integer, List<AviatorFlightBCinfoDTO>> getFlightBCinfoDTOs() throws ModuleException {

		String query = "SELECT ps.flt_seg_id," 
				+"   BC.booking_code," 
				+"   SUM(DECODE(ppoc.charge_group_code, 'FAR', ppsc.amount, 0)) FARE," 
				+"   SUM(ppsc.amount ) REVENUE," 
				+"   COUNT(DISTINCT (" 
				+"   CASE" 
				+"     WHEN BC.PAX_TYPE = 'GOSHOW'" 
				+"     THEN P.PNR_PAX_ID" 
				+"     ELSE NULL" 
				+"   END ))GOSHOW_SEATS," 
				+"   COUNT(DISTINCT (" 
				+"   CASE" 
				+"     WHEN ppfs.PAX_STATUS = 'N'" 
				+"     THEN P.PNR_PAX_ID" 
				+"     ELSE NULL" 
				+"   END ))NOSHOW_SEATS" 
				+" FROM t_pnr_segment ps," 
				+"   t_pnr_pax_fare_segment ppfs," 
				+"   t_pnr_pax_seg_charges ppsc," 
				+"   t_pnr_pax_ond_charges ppoc," 
				+"   T_PNR_PAX_FARE PPF," 
				+"   t_pnr_passenger p," 
				+"   T_BOOKING_CLASS BC ," 
				+"   T_FLIGHT_SEGMENT fs," 
				+"   t_flight f" 
				+" WHERE p.pnr_pax_id          =ppf.pnr_pax_id" 
				+" AND p.pnr                   =ps.pnr" 
				+" AND ps.pnr_seg_id           = ppfs.pnr_seg_id" 
				+" AND ppfs.ppfs_id            = ppsc.ppfs_id" 
				+" AND ppoc.pft_id             = ppsc.pft_id" 
				+" AND ppf.ppf_id              =ppfs.ppf_id" 
				+" AND ppfs.ppfs_id            =ppsc.ppfs_id" 
				+" AND PPFS.BOOKING_CODE       =BC.BOOKING_CODE" 
				+" AND BC.BC_TYPE NOT         IN('STANDBY','OPENRT' )" 
				+" AND ps.FLT_SEG_ID           =fs.FLT_SEG_ID" 
				+" AND fs.flight_id            = f.flight_id" 
				 //Comment this
				//+ "  AND f.flight_id                in ('267507')"
				+" AND f.departure_date        > (TRUNC(sysdate) - 2)" 
				+" AND f.status                                 IN ('ACT','CRE','CLS')" 
				+" AND PPOC.CHARGE_GROUP_CODE                   IN('FAR','SUR')" 
				+" AND ((P.STATUS ='CNF' AND PS.STATUS ='CNF') or ppfs.PAX_STATUS = 'N') "
				+" GROUP BY ps.flt_seg_id," 
				+"   BC.booking_code ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		 Map<Integer, List<AviatorFlightBCinfoDTO>> aviatorFlightBCinfoDTOsMap = ( Map<Integer, List<AviatorFlightBCinfoDTO>>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				 Map<Integer, List<AviatorFlightBCinfoDTO>> colFlightBCinfoDTOsMap = new HashMap<Integer, List<AviatorFlightBCinfoDTO>>();

				while (rs.next()) {
					AviatorFlightBCinfoDTO flightBCinfoDTO = new AviatorFlightBCinfoDTO();
					Integer flightSegId = rs.getInt("flt_seg_id");
					flightBCinfoDTO.setFlightSegId(flightSegId);
					flightBCinfoDTO.setBookingCode(rs.getString("booking_code"));
					flightBCinfoDTO.setRevenue(rs.getDouble("REVENUE"));
					flightBCinfoDTO.setGoshowCount(rs.getInt("GOSHOW_SEATS"));
					flightBCinfoDTO.setNoshowCount(rs.getInt("NOSHOW_SEATS"));

					if(colFlightBCinfoDTOsMap.containsKey(flightSegId)) {
						colFlightBCinfoDTOsMap.get(flightSegId).add(flightBCinfoDTO);
					} else {
						List<AviatorFlightBCinfoDTO> fltBCinfoList = new ArrayList<AviatorFlightBCinfoDTO>();
						fltBCinfoList.add(flightBCinfoDTO);
						colFlightBCinfoDTOsMap.put(flightSegId, fltBCinfoList);
					}
				}
				return colFlightBCinfoDTOsMap;
			}
		});

		return aviatorFlightBCinfoDTOsMap;
	}
}
