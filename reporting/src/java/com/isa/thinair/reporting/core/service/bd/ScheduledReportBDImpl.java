package com.isa.thinair.reporting.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.reporting.api.service.ScheduledReportBD;

@Remote
public interface ScheduledReportBDImpl extends ScheduledReportBD {

}
