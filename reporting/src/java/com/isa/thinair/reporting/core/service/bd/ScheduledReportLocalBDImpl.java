package com.isa.thinair.reporting.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.reporting.api.service.ScheduledReportBD;

@Local
public interface ScheduledReportLocalBDImpl extends ScheduledReportBD {

}
