package com.isa.thinair.reporting.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.reporting.api.criteria.SchduledReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ReportMetaData;
import com.isa.thinair.reporting.api.model.SRCountDTO;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;

public interface ScheduledReportDAO {

	public void saveScheduledReport(ScheduledReport scheduledReport);

	public ScheduledReport getScheduledReport(Integer scheduledReportId);

	public Collection<ScheduledReport> getNotActivatedScheduleReports();

	public Collection<ScheduledReport> getCompletionRequiredScheduleReports();

	public Collection<Integer> getStartedScheduledReportIdsWithNoTriggers();

	public void saveSRSentItem(SRSentItem sentItem);

	public ReportMetaData getReportMetaData(String reportName);

	public SRCountDTO getScheduledReportCount(String reportName, String userName, Date startDate, Date endDate);

	/** Interface method to getCharges...with paging and criteria **/
	public Page getAllScheduledReports(SchduledReportsSearchCriteria searchCriteria, int startIndex, int pageSize);

	public Page getAllSRSentItemsForScheduledReport(Long scheduledReportID, int startIndex, int pageSize);
}
