/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.reporting.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author byorn
 * @isa.module.config-bean
 */
public class ReportingConfig extends DefaultModuleConfig {
	private boolean setTransactionReadOnly = false;

	public ReportingConfig() {
		super();

	}

	/**
	 * @return Returns the setTransactionReadOnly.
	 */
	public boolean isSetTransactionReadOnly() {
		return setTransactionReadOnly;
	}

	/**
	 * @param setTransactionReadOnly
	 *            The setTransactionReadOnly to set.
	 */
	public void setSetTransactionReadOnly(boolean setTransactionReadOnly) {
		this.setTransactionReadOnly = setTransactionReadOnly;
	}

}
