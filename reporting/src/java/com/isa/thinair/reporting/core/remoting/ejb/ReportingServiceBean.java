/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.core.remoting.ejb;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.sql.RowSet;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.HalaServiceDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.AgentUserRolesDTO;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.bl.AviatorFileGenerator;
import com.isa.thinair.reporting.core.persistent.dao.ReportingDAOJDBC;
import com.isa.thinair.reporting.core.persistent.dao.ReportsDAO;
import com.isa.thinair.reporting.core.service.bd.DataExtractionBDImpl;
import com.isa.thinair.reporting.core.service.bd.DataExtractionLocalBDImpl;
import com.isa.thinair.reporting.core.util.InternalConstants;

/**
 * @author Byorn, Vinothini
 */
@Stateless
@RemoteBinding(jndiBinding = "ReportingService.remote")
@LocalBinding(jndiBinding = "ReportingService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ReportingServiceBean extends PlatformBaseSessionBean implements DataExtractionBDImpl, DataExtractionLocalBDImpl {

	private static final long serialVersionUID = -4563909788067380460L;
	
	AviatorFileGenerator aviatorFileGenerator = null;

	@Override
	public Page<FlightInventorySummaryDTO> searchFlightsForInventoryUpdation(FlightSearchCriteria criteria, int startindex,
			int pagesize) throws ModuleException {
		criteria.setApplicapableCarrierCodes(getUserPrincipal().getCarrierCodes());
		return getReportingDAOJDBC().getFlightsForInventoryUpdating(criteria, startindex, pagesize);
	}

	/**
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public FCCAgentsSeatMvDTO getFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList)
			throws ModuleException {
		return getReportingDAOJDBC().getFCCAgentsSeatMovementSummary(flightId, cabinClassCode, loadPNRList);
	}

	/**
	 * 
	 * @param flightId
	 * @param logicalCCCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public FCCAgentsSeatMvDTO getFCCSeatMovementSummaryForLogicalCC(int flightId, String logicalCCCode, boolean loadPNRList)
			throws ModuleException {
		return getReportingDAOJDBC().getFCCAgentsSeatMovementSummaryForLogicalCC(flightId, logicalCCCode, loadPNRList);
	}

	@Override
	public FCCAgentsSeatMvDTO getOwnerAgentWiseFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList)
			throws ModuleException {
		return getReportingDAOJDBC().getOwnerAgentWiseFCCSeatMovementSummary(flightId, cabinClassCode, loadPNRList);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAvsSeat(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAvsSeat(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getFlightDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getInboundOutboundFlightSchedule(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInboundOutboundFlightScheduleData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getScheduleDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getScheduleDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getCnxReservationDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCnxReservationDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAcquiredCreditDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAcquiredCreditDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAvailableCreditDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAvailableCreditDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public void getROPRevenue(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		getReportsDAO().getROPRevenueData(reportsSearchCriteria);
	}

	@Override
	public RowSet getROPRevenueDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getROPRevenueReportData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public void getForwardSales(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		getReportsDAO().getForwardSalesData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	@TransactionTimeout(7200)
	// Increased time out for this OND Report Only Make it 2hours
			public
			void getONDData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		getReportsDAO().getONDData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getScheduleCapasityVarience(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getScheduleCapacityVarienceData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAgentGSAData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgetGSAData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getMealsSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getMealsSummaryData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getExtPmtReconcilSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getExtPmtReconcilSummaryData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAgentUserData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentUserData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getUserPrivilegeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getUserPrivilegeData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public Map getAgentUserRoleData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentUserRoleData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getPerformanceOfSalesStaffSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPerformanceOfSalesStaffSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getPerformanceOfSalesStaffDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPerformanceOfSalesStaffDetailData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getCallCentreModeOfPaymentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCallCentreModeOfPaymentsData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getCallCentreRefundData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCallCentreRefundData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getCustomerTravelHistoryDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportsDAO().getCustomerTravelHistoryDetailData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getCustomerProfileSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCustomerProfileSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getWebProfilesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getWebProfilesData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getCustomerProfileDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCustomerProfileDetailData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getEnplanementData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getEnplanementData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getTopAgentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getTopAgentsData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getTopSegmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getTopSegmentsData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getSegmentContributionByAgentsByPOSData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getSegmentContributionByAgentsByPOSData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getCountryContributionPerFlightSegmentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCountryContributionPerFlightSegmentData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getOnholdPassengersData(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentChargeAdjustmentsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getIBEOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getIBEOnholdPassengersData(reportsSearchCriteria);

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getPaxContactDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPaxContactDetailsData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getReservationBreakdownSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getReservationBreakdownSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getCustomerExistingCreditDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCustomerExistingCreditData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getReservationBreakdownDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getReservationBreakdownDetailData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAirportTaxData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAirportTaxData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getFareDiscountsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFareDiscountsData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentCommisionData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentCommisionData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getReservationBreakdownTaxesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getReservationBreakdownTaxesData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getOutstandingBalanceSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getOutstandingBalanceSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getOutstandingBalanceDetailData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentProductivityData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportsDAO().getAgentProductivityData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getInvoiceSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getInvoiceSummaryDataByCurrency(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSummaryDataByCurrency(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentTransactionData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentTransactionData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getViewSeatInventoryFareMovements(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getViewSeatInventoryFareMovements(reportsSearchCriteria);
	}

	public ReportingDAOJDBC getReportingDAOJDBC() {
		return (ReportingDAOJDBC) ReportingUtils.getInstance().getLocalBean("reportingDAOJDBCImpl");
	}

	public ReportsDAO getReportsDAO() {
		return (ReportsDAO) ReportingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.REPORTS_DAO);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getInbDatas(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInbDatas(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 */
	@Override
	public Collection getPaymentsMode() throws ModuleException {
		return getReportsDAO().getPaymentsMode();
	}

	/**
	 * 
	 * @param ReportsSearchCriteria
	 * @throws ModuleException
	 * @return
	 */
	@Override
	public Collection getRoleForPrivileges(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getRoleForPrivileges(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getCompanyPaymentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCompanyPaymentQuery(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getCompanyPaymentCurrencyData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCompanyPaymentCurrencyQuery(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getCompanyPaymentPOSData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCompanyPaymentPOSQuery(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getInvoiceSummaryAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSummaryAttachment(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getInvoiceSummaryAttachmentByCurrency(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSummaryAttachmentByCurrency(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getInvoiceSummaryAttachmentByTransaction(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSummaryAttachmentByTransaction(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getCCTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCCTransactionDetails(reportsSearchCriteria);
	}
    
	/* (non-Javadoc)
	 * @see com.isa.thinair.reporting.api.service.DataExtractionBD#getCCTOPTransactionDetails(com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria)
	 */
	@Override
	public RowSet getCCTOPTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCCTopUpTransactionDetails(reportsSearchCriteria);
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @throws ModuleException
	 * @return
	 */
	@Override
	public RowSet getPerDayCCFraudDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPerDayCCFraudDetails(reportsSearchCriteria);
	}
	
	
	/**
	 *  
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public RowSet getPerDayCCTopUpFraudDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPerDayCCTopUpFraudDetails(reportsSearchCriteria);
	}


	/**
	 * 
	 * @throws ModuleException
	 * 
	 */
	@Override
	public RowSet getCCTransactionRefundDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCCTransactionRefundDetails(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * 
	 */
	@Override
	public RowSet getCCTransactionPendingRefundDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getCCTransactionPendingRefundDetails(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getFreightDetailsReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFreightDetailsReport(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getAdministrationAuditData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAdministrationAuditData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getReservationAuditData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getReservationAuditData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPaxStatusReport(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public Map<String, Integer> getPaxCountForPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPaxCountForPaxStatusReport(reportsSearchCriteria);
	}

	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getInterlineSalesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInterlineSalesData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public void getInterlineRevenue(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		getReportsDAO().getInterlineRevenueData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getFlightLoad(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightLoadData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentStatementSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentStatementSummaryData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentStatementDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentStatementDetailData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getEmailOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getEmailOutstandingBalanceDetailData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getInsurenceDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInsurenceDetails(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getPrivilegeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPrivilegeDetailsData(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentSalesReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentSalesPerformanceReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentSalesPerformanceReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentWiseForwardSalesReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getTicketWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getTicketWiseForwardSalesReport(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAgentSalesStatusData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentSalesStatusData(reportsSearchCriteria);

	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 */
	@Override
	public RowSet getBCReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBCReport(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getInvoiceSettlemenyHistoryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInvoiceSettlemenyHistoryReport(reportsSearchCriteria);
	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getNameChangeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getNameChangeDetailsReport(reportsSearchCriteria);
	}

	@Override
	/**Retrieves booked hala services summary for a given criteria.
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	public RowSet getBookedHalaServiceData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedHalaServicesSummaryReport(reportsSearchCriteria);
	}

	@Override
	/**
	 * Retrieves booked hala services details for a given criteria(flight no)
	 */
	public RowSet getBookedHalaServiceDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedHalaServicesDetailReport(reportsSearchCriteria);
	}

	/**
	 * @author Navod Ediriweera
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getSegmentTotalPaxByStatus(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getSegmentTotalPaxByStatus(reportsSearchCriteria);
	}

	/**
	 * @author Harsha
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getMealSummaryReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getMealSummaryReport(reportsSearchCriteria);
	}

	/**
	 * @author Harsha
	 * @return int
	 * @throws ModuleException
	 */
	@Override
	public Map<String, Map<String, Integer>> getCountOfPassengers(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException {
		return getReportsDAO().getCountOfPassengers(reportsSearchCriteria);
	}

	/**
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getMealDetailReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getMealDetailReport(reportsSearchCriteria);
	}

	public RowSet getFlightAncillaryDetailsReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightAncillaryDetailsReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getHalaServiceCommissionData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getHalaServiceCommissions(reportsSearchCriteria);
	}

	@Override
	public RowSet getPnrInvoiceReceipt(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportsDAO().getPnrInvoiceReceiptReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getLoyaltyPointsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getLoyaltyPointsData(reportsSearchCriteria);
	}

	@Override
	public Collection<HalaServiceDTO> getBookedHalaServiceSummaryForNotification(ReportsSearchCriteria search)
			throws ModuleException {
		return getReportsDAO().getBookedHalaServicesSummary(search);
	}

	@Override
	public Collection<PaxSSRDTO> getBookedHalaServiceDetailForNotification(ReportsSearchCriteria search) throws ModuleException {
		return getReportsDAO().getBookedHalaServicePaxDetails(search);
	}

	@Override
	public RowSet getAncillaryRevenueData(ReportsSearchCriteria search) throws ModuleException {
		return getReportsDAO().getAncillaryRevenueReport(search);
	}

	@Override
	public RowSet getAdvancedAncillaryRevenueReportData(ReportsSearchCriteria search) throws ModuleException {
		return getReportsDAO().getAdvancedAncillaryRevenueReport(search);
	}

	@Override
	public RowSet getCurrencyConversionData(ReportsSearchCriteria search) throws ModuleException {
		return getReportsDAO().getCurrencyConversionDetailReport(search);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getAirportTaxReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAirportTaxReportData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAdjustmentAuditReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAdjustmentAuditReportData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getVoidReservationDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getVoidReservationDetailsReportData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getBookingCountDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookingCountDetailsReportData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return Integer
	 * @throws ModuleException
	 */
	@Override
	public Integer getBookingCountDetailsReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookingCountDetailsReportPaxCount(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getBookingCountDetailAgentReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookingCountDetailAgentReportData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return Integer
	 * @throws ModuleException
	 */
	@Override
	public Integer getBookingCountDetailAgentReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookingCountDetailAgentReportPaxCount(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getLccAndDryCollection(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getLccAndDryCollectionData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getFareRuleDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFareRuleDetailsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getFareRuleVisibilityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFareRuleVisibilityData(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentsForFareRuleData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentsForFareRuleData(reportsSearchCriteria);
	}

	@Override
	public Page<String> getFlightsForFlightLoadAnalysis(FlightSearchCriteria criteria, int startindex, int pagesize)
			throws ModuleException {
		criteria.setApplicapableCarrierCodes(getUserPrincipal().getCarrierCodes());
		return getReportingDAOJDBC().getFlightsForFlightLoadAnalysis(criteria, startindex, pagesize);
	}

	@Override
	public RowSet getMisProductSalesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getMisProductSalesData(reportsSearchCriteria);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getBookedPAXSegmentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedPAXSegmentData(reportsSearchCriteria);
	}

	@Override public RowSet getOriginCountryOfCallData(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException {
		return getReportsDAO().getOriginCountryOfCallData(reportsSearchCriteria);
	}

	@Override public RowSet getOriginCountryOfCallDetail(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException {
		return getReportsDAO().getOriginCountryOfCallDetailData(reportsSearchCriteria);
	}


	@Override
	public RowSet getIncentiveSchemeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getIncentiveSchemeData(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentIncentiveSchemeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentIncentiveSchemeData(reportsSearchCriteria);
	}

	/**
	 * Returns the list of airports matching the given search criteria.
	 * 
	 * @param reportsSearchCriteria
	 *            The search criteria.
	 * @return RowSet The result set
	 * @throws ModuleException
	 */
	@Override
	public RowSet getAirportListData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAirportListData(reportsSearchCriteria);
	}

	@Override
	public RowSet getRouteDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getRouteDetailData(reportsSearchCriteria);
	}

	@Override
	public void getBookedSSRDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 */
	@Override
	public RowSet getBookedSSRDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedSSRDetailsReportData(reportsSearchCriteria);

	}

	/**
	 * @param reportsSearchCriteria
	 * @return RowSet
	 */
	@Override
	public RowSet getFareDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFareDetailsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getAppParameterDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAppParameterListData(reportsSearchCriteria);
	}

	@Override
	public RowSet getAgentHandlingFeeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentHandlingFeeData(reportsSearchCriteria);
	}

	public RowSet getFlownPassengerListData(ReportsSearchCriteria reportsSearchCriteria, String viewMode) throws ModuleException {
		return getReportsDAO().getFlownPassengerListData(reportsSearchCriteria, viewMode);
	}
	
	public RowSet getFlownPassengerData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlownPassengerData(reportsSearchCriteria);
	}

	@Override
	public RowSet getChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getChargeAdjustmentsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getBookedSSRSummaryDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedSSRSummaryReportData(reportsSearchCriteria);

	}

	@Override
	public RowSet getBookedSSRDetailDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBookedSSRDetailReportData(reportsSearchCriteria);

	}

	@Override
	public RowSet getAirportTransferDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAirportTransferDataforReports(reportsSearchCriteria);
	}

	@Override
	public void getAgentSalesModifyRefundReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		getReportsDAO().getAgentSalesModifyRefundReportData(reportsSearchCriteria);

	}

	@Override
	public RowSet getUserIncentiveDetailsReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getUserIncentiveDetailsReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getPromotionRequestsSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromotionRequestsSummary(reportsSearchCriteria);
	}

	@Override
	public RowSet getPromotionNextSeatFreeSummary(ReportsSearchCriteria reportSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromotionNextSeatFreeSummary(reportSearchCriteria);
	}

	@Override
	public RowSet getPromotionNextSeatFreeSummaryForCrew(ReportsSearchCriteria reportSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromotionNextSeatFreeSummaryForCrew(reportSearchCriteria);
	}

	@Override
	public RowSet getPromotionFlexiDateSummary(ReportsSearchCriteria reportSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromotionFlexiDateSummary(reportSearchCriteria);
	}

	public RowSet getFlightHistoryDetailsReport(String flightId, String flightNo, String flightDate, String reverseDate,
			Integer scheduleId, String fromDate, String toDate, boolean showResHistory) throws ModuleException {
		return getReportsDAO().getFlightHistoryDetailsReport(flightId, flightNo, flightDate, reverseDate, scheduleId, fromDate,
				toDate, showResHistory);
	}

	@Override
	public RowSet getTaxHistoryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getTaxHistoryData(reportsSearchCriteria);
	}

	@Override
	public RowSet getGDSReservationsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getGDSReservationsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getFlightConnectivityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightConnectivityData(reportsSearchCriteria);
	}

	@Override
	public RowSet getPromoCodeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromoCodeDetailsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getJNTaxReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getJNTaxReportData(reportsSearchCriteria);
	}

	public String getAgentStationCode() throws ModuleException {
		return getUserPrincipal().getAgentStation();
	}

	@Override
	public RowSet getFlightOverBookSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightOverBookSummary(reportsSearchCriteria);
	}

	@Override
	public RowSet getSalesChannelCodes(String salesChannelIDs) throws ModuleException {
		return getReportsDAO().getSalesChannelCodes(salesChannelIDs);
	}

	@Override
	public RowSet getIbeExitDataForReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getIbeExitDetails(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getInternationalFlightDepartureArrivalDataForReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getInternationalFlightDepartureArrivalDetails(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getPassengerInternationalFlightDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException{
		return getReportsDAO().getPassengerInternationalFlightDetailsData(reportsSearchCriteria);
	}

	@Override
	public RowSet getFlightSeatMapChangesSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getSeatMapChangesDetails(reportsSearchCriteria);

	}

	@Override
	public RowSet getIbeExitDataForReports(String exitDetailId) throws ModuleException {
		return getReportsDAO().getIbeExitDetails(exitDetailId);
	}

	@Override
	public RowSet getBundledFareReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBundledFareReportData(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getPassengerStatusAndRevenueData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPassengerStatusAndRevenueQuery(reportsSearchCriteria);
	}
		
	/**
	 * Returns the list of member who has loyalty for a given date range, flight id , segment code.
	 * 
	 * @param reportsSearchCriteria
	 *            The search criteria.
	 * @return RowSet The result set
	 * @throws ModuleException
	 */
	@Override
	public RowSet getFlightLoyaltyMembers(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getFlightLoyaltyMembers(reportsSearchCriteria);
	}
	
	
	/**
	 * Returns point redemption with detailed breakdown report.
	 * 
	 * @param reportsSearchCriteria
	 *            The search criteria.
	 * @return RowSet The result set
	 * @throws ModuleException
	 */
	@Override
	public RowSet getPointRedemptionDetailedReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPointRedemptionDetailedReport(reportsSearchCriteria);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(7200)
	public void generateAviatorFile() throws ModuleException {
		try {
			getAviatorFileGenerator().generateAviatorFile();
		} catch (ModuleException ex) {
			log.error(" #####AVIATOR##### ((o)) ModuleException::publishAviatorFile ", ex);
			//throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" #####AVIATOR##### ((o)) CommonsDataAccessException::searchAviatorFileData ", cdaex);
			//throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			log.error("#####AVIATOR##### generateAviatorFile", ex);
			this.sessionContext.setRollbackOnly();
		} 
	}
	
	private AviatorFileGenerator getAviatorFileGenerator() {
		if (aviatorFileGenerator != null) {
			return aviatorFileGenerator;
		}

		aviatorFileGenerator = new AviatorFileGenerator();
		return aviatorFileGenerator;
	}
	/**
	 * 
	 * @throws ModuleException
	 * @return
	 * 
	 */
	@Override
	public RowSet getNILTransactionsAgentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getNILTransactionsAgentsQuery(reportsSearchCriteria);
	}
	
	/**
	 * 
	 * @param ReportsSearchCriteria
	 * @throws ModuleException
	 * @return
	 */
	@Override
	public Page getRolesForPrivilege(AgentUserRolesDTO agentUserRolesDTO, int startIndex, int pageLength) throws ModuleException {
		return getReportingDAOJDBC().getRolesForPrivilege(agentUserRolesDTO, startIndex, pageLength);
	}

	@Override
	public Map getAgentUserRoleDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAgentUserRoleDetailData(reportsSearchCriteria);
	}

	@Override
	public RowSet getNameChangeChargeReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getNameChangeChargeReportData(reportsSearchCriteria);
	}

	@Override
	public RowSet getIssuedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getIssuedVoucherDetails(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getRedeemedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getRedeemedVoucherDetails(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getPFSDetailedExport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPFSDetailedExportData(reportsSearchCriteria);
	}

	@Override
	public RowSet getFlightMessageDetailsReport(String flightId, Integer scheduleId, String fromDate, String toDate)
			throws ModuleException {
		return getReportsDAO().getFlightMessageDetailsReport(flightId, scheduleId, fromDate, toDate);
	}

	@Override
	public RowSet getScheduleMessageDetailsReport(String scheduleId, String fromDate, String toDate) throws ModuleException {
		return getReportsDAO().getScheduleMessageDetailsReport(scheduleId, fromDate, toDate);
	}

	@Override
	public RowSet getPublishedMessageDetailsReport(String flightId, String strFromDate, String strToDate) throws ModuleException {
		return getReportsDAO().getPublishedMessageDetailsReport(flightId, strFromDate, strToDate);
	}

	@Override
	public RowSet getMCODetailReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
			return getReportsDAO().getMCODetailReport(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getMCOSummaryReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
			return getReportsDAO().getMCOSummaryReport(reportsSearchCriteria);
	}

	@Override
	public RowSet getForceConfirmedBookingReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportsDAO().getForceConfirmedBookingReport(reportsSearchCriteria);
	}

	public Map<String, ResultSet> getGstr1(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getGstr1(reportsSearchCriteria);

	}

	public Map<String, ResultSet> getGstr3(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getGstr3(reportsSearchCriteria);
	}

	@Override
	public RowSet getGSTAdditionalReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getGSTAdditionalReportData(reportsSearchCriteria);
	}

	@Override
	public RowSet getPassengersMealSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPassengersMealSummary(reportsSearchCriteria);
	}
	
	@Override
	public RowSet getBlockedLMSPointsData(ReportsSearchCriteria search) throws ModuleException{
		return getReportsDAO().getBlockedLoyaltyPointsData(search);
	}
	
	@Override
	public RowSet getCCTransactionsLMSPointsData(ReportsSearchCriteria search) throws ModuleException{
		return getReportsDAO().getCCTransactionsLMSPointsData(search);
	}
	
	@Override
	public RowSet getCCTransactionsLMSPointsDetailQuery(ReportsSearchCriteria search) throws ModuleException{
		return getReportsDAO().getCCTransactionsLMSPointsDetailQuery(search);
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	public RowSet getBlacklistedPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getBlacklistedPassengersData(reportsSearchCriteria);
	}

	@Override
	public RowSet getPromotionCriteriaDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getPromotionCriteraDetails(reportsSearchCriteria);
	}

	@Override
	public ResultSet getAutomaticCheckinDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportsDAO().getAutomaticCheckinDetails(reportsSearchCriteria);
	}
	
}