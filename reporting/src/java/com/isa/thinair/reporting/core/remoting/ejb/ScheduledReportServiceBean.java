/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.criteria.SchduledReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.scheduledRpts.ScheduledReportsViewTO;
import com.isa.thinair.reporting.api.model.ReportMetaData;
import com.isa.thinair.reporting.api.model.SRCountDTO;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistence.dao.ScheduledReportDAO;
import com.isa.thinair.reporting.core.service.bd.ScheduledReportBDImpl;
import com.isa.thinair.reporting.core.service.bd.ScheduledReportLocalBDImpl;
import com.isa.thinair.reporting.core.util.InternalConstants;
import com.isa.thinair.reporting.core.util.LookupUtils;

@Stateless
@RemoteBinding(jndiBinding = "ScheduledReport.remote")
@LocalBinding(jndiBinding = "ScheduledReport.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ScheduledReportServiceBean extends PlatformBaseSessionBean implements ScheduledReportBDImpl,
		ScheduledReportLocalBDImpl {

	@Override
	public void scheduleAReport(ScheduledReport scheduledReport) throws ModuleException {

		ReportMetaData reportMetaData = getScheduleReportDAO().getReportMetaData(scheduledReport.getReportName());
		if (reportMetaData == null) {
			throw new ModuleException("reporting.invalid.report");
		}

		SRCountDTO srCount = getScheduleReportDAO().getScheduledReportCount(scheduledReport.getReportName(),
				scheduledReport.getScheduledBy(), scheduledReport.getStartDate(), scheduledReport.getEndDate());

		if (reportMetaData.getMaxGlobalSchedCount() <= srCount.getCurrentGlobalCount()) {
			throw new ModuleException("reporting.schedreports.global.count.exceeded");
		}

		if (reportMetaData.getMaxUserSchedCount() <= srCount.getCurrentUserCount()) {
			throw new ModuleException("reporting.schedreports.user.count.exceeded");
		}

		if (CalendarUtil.daysUntil(new Date(), scheduledReport.getStartDate()) <= 1) {

			scheduledReport.setStatus(ScheduledReport.STATUS_NOT_STARTED);
			getScheduleReportDAO().saveScheduledReport(scheduledReport);

			String quartsJobName = getQuartzJobName(scheduledReport);
			LookupUtils.getSchedularBD()
					.addJob("scheduledReportGenJob", quartsJobName, null, scheduledReport.getCronExpression());

			// re save the scheduled report with new parameters
			scheduledReport.setStatus(ScheduledReport.STATUS_STARTED);
			scheduledReport.setQuartsJobName(quartsJobName);
			getScheduleReportDAO().saveScheduledReport(scheduledReport);

		} else {

			scheduledReport.setStatus(ScheduledReport.STATUS_NOT_STARTED);
			getScheduleReportDAO().saveScheduledReport(scheduledReport);
		}
	}

	@Override
	public ScheduledReport getScheduledReport(Integer scheduledReportId) {
		return getScheduleReportDAO().getScheduledReport(scheduledReportId);
	}

	@Override
	public Collection<ScheduledReport> getNotActivatedScheduleReports() {
		return getScheduleReportDAO().getNotActivatedScheduleReports();
	}

	@Override
	public Collection<ScheduledReport> getCompletionRequiredScheduleReports() {
		return getScheduleReportDAO().getCompletionRequiredScheduleReports();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void activateScheduledReport(ScheduledReport scheduledReport) throws ModuleException {

		String quartsJobName = getQuartzJobName(scheduledReport);
		LookupUtils.getSchedularBD().addJob(InternalConstants.ScheduledReportsConst.SCHEDULED_REPORT_JOB_BEAN_NAME,
				quartsJobName, null, scheduledReport.getCronExpression());

		scheduledReport.setStatus(ScheduledReport.STATUS_STARTED);
		scheduledReport.setQuartsJobName(quartsJobName);
		getScheduleReportDAO().saveScheduledReport(scheduledReport);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void finishScheduledReport(ScheduledReport scheduledReport) throws ModuleException {

		String quartsJobName = getQuartzJobName(scheduledReport);
		LookupUtils.getSchedularBD().removeJob(quartsJobName, null);

		scheduledReport.setStatus(ScheduledReport.STATUS_COMPLETED);
		getScheduleReportDAO().saveScheduledReport(scheduledReport);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void recreateScheduledReportTrigger(Integer scheduledReportId) throws ModuleException {

		ScheduledReport scheduledReport = getScheduleReportDAO().getScheduledReport(scheduledReportId);

		String quartsJobName = getQuartzJobName(scheduledReport);
		LookupUtils.getSchedularBD().addJob(InternalConstants.ScheduledReportsConst.SCHEDULED_REPORT_JOB_BEAN_NAME,
				quartsJobName, null, scheduledReport.getCronExpression());

		scheduledReport.setStatus(ScheduledReport.STATUS_STARTED);
		scheduledReport.setQuartsJobName(quartsJobName);
		getScheduleReportDAO().saveScheduledReport(scheduledReport);
	}

	public void saveSRSentItem(SRSentItem sentItem) {
		getScheduleReportDAO().saveSRSentItem(sentItem);
	}

	public ReportMetaData getReportMetaData(String reportName) {
		return getScheduleReportDAO().getReportMetaData(reportName);
	}

	@Override
	public Collection<Integer> getStartedScheduledReportIdsWithNoTriggers() {
		return getScheduleReportDAO().getStartedScheduledReportIdsWithNoTriggers();
	}

	private String getQuartzJobName(ScheduledReport scheduledReport) {
		return InternalConstants.ScheduledReportsConst.SCHEDULED_REPORT_JOB_PRIFIX + scheduledReport.getScheduledReportId();
	}

	private ScheduledReportDAO getScheduleReportDAO() {
		return (ScheduledReportDAO) ReportingUtils.getInstance().getLocalBean("ScheduledReportDAOProxy");
	}

	@Override
	public Page getAllScheduleReports(SchduledReportsSearchCriteria reportsSearchTO, int startIndex, int pageSize) {
		return getScheduleReportDAO().getAllScheduledReports(reportsSearchTO, startIndex, pageSize);
	}

	@Override
	public ScheduledReport modifyScheduleReport(ScheduledReportsViewTO reportsViewTO) {
		ScheduledReport existing = getScheduleReportDAO().getScheduledReport(
				Integer.parseInt(reportsViewTO.getScheduledReportID()));
		getScheduleReportDAO().saveScheduledReport(reportsViewTO.mergeDataForDomainObject(existing));
		return existing;
	}

	@Override
	public ScheduledReport activateDeactivteScheduled(ScheduledReportsViewTO reportsViewTO) throws ModuleException {
		ScheduledReport existing = getScheduleReportDAO().getScheduledReport(
				Integer.parseInt(reportsViewTO.getScheduledReportID()));
		if (ScheduledReport.STATUS_STARTED.equals(existing.getStatus())
				|| ScheduledReport.STATUS_NOT_STARTED.equals(existing.getStatus())) {
			existing.setStatus(ScheduledReport.STATUS_SUSPENDED);
		} else if (ScheduledReport.STATUS_SUSPENDED.equals(existing.getStatus())) {
			existing.setStatus(ScheduledReport.STATUS_NOT_STARTED);
		} else {
			throw new ModuleException("reporting.schedreports.activation.deactivatin.not.allowed");
		}
		getScheduleReportDAO().saveScheduledReport(existing);
		// deleting trigger
		String quartsJobName = getQuartzJobName(existing);
		LookupUtils.getSchedularBD().removeJob(quartsJobName, null);

		return existing;
	}

	@Override
	public Page getSRSentItemsForAScheduledReport(Long scheduledReportID, int startIndex, int pageSize) throws ModuleException {
		return getScheduleReportDAO().getAllSRSentItemsForScheduledReport(scheduledReportID, startIndex, pageSize);
	}
}