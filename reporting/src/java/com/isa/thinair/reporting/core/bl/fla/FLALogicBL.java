/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.reporting.core.bl.fla;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.reporting.api.model.FlaGraphBarBreakDwnDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.util.InternalConstants;

public class FLALogicBL {

	private static final Log log = LogFactory.getLog(FLALogicBL.class);
	private static String DDMMYYYY = "dd/MM/yyyy";

	public static ArrayList<FlaGraphBarBreakDwnDTO> getGraphDataBreakDown(Integer flightID, String channel,
			ArrayList<Integer> nominalCodes) {

		ArrayList<FlaGraphBarBreakDwnDTO> lstGraphData = getFLADataProviderDAOJDBC().getGraphDataBreakDown(flightID, channel,
				nominalCodes);
		HashMap<String, FlaGraphBarBreakDwnDTO> mapBrkDwnGraphData = new HashMap<String, FlaGraphBarBreakDwnDTO>();

		ArrayList<String> lstPnr = new ArrayList<String>();
		HashMap<String, Date> mapGraphData = new HashMap<String, Date>();
		if (lstGraphData != null & !lstGraphData.isEmpty()) {
			for (FlaGraphBarBreakDwnDTO flaBrkDwn : lstGraphData) {
				lstPnr.add(flaBrkDwn.getPnr());
			}
			if (!lstPnr.isEmpty()) {
				mapGraphData = getFLADataProviderDAOJDBC().getGraphDataModificationDates(lstPnr);
			}

		}
		if (!mapGraphData.isEmpty()) {

			for (FlaGraphBarBreakDwnDTO brkDwnDto : lstGraphData) {
				String pnr = brkDwnDto.getPnr();
				if (mapGraphData.containsKey(pnr)) {
					Date modDate = mapGraphData.get(pnr);
					String strModDate = dateToString(modDate, DDMMYYYY);
					brkDwnDto.setLastModDate(strModDate);
					mapBrkDwnGraphData.put(pnr, brkDwnDto);
				}
			}
		}

		return new ArrayList<FlaGraphBarBreakDwnDTO>(mapBrkDwnGraphData.values());
	}

	public static FLADataProviderDAOJDBC getFLADataProviderDAOJDBC() {
		return (FLADataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLADataProviderDAO);
	}

	private static String dateToString(Date date, String format) {
		String formattedDate = null;
		if (date != null && format != null && !format.equals("")) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}
}