package com.isa.thinair.reporting.core.bl.fla;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.util.InternalConstants;

public class GroupBookingFindingStrategyImpl implements AlertFinderStatergy {

	@Override
	public List<FlaAlertDTO> composeAlerts(List<Integer> lstFlightIDs) throws ModuleException {

		return ((FLADataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLADataProviderDAO)).getGroupBookingsAlert(lstFlightIDs);

	}

}
