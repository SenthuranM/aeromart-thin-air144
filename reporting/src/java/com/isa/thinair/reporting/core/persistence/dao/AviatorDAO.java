package com.isa.thinair.reporting.core.persistence.dao;

import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCInvDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCinfoDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightLegDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightSegInventoryDTO;

/**
* @author Manoj Dhanushka
* 
*/
public interface AviatorDAO {
	
	public List<AviatorFlightLegDTO> getFlightLegDTOs() throws ModuleException;
	
	public Map<Integer, List<AviatorFlightSegInventoryDTO>> getFlightLegInventory() throws ModuleException;
	
	public Map<Integer, List<AviatorFlightBCInvDTO>> getFlightSegmentDetails() throws ModuleException;
	
	public Map<Integer, List<AviatorFlightBCinfoDTO>> getFlightBCinfoDTOs() throws ModuleException;

}
