package com.isa.thinair.reporting.core.bl.fla;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;

public class AlertFinder {

	private static Log log = LogFactory.getLog(AlertFinder.class);
	
	private List<AlertFinderStatergy> alertFindingStrategyList = new ArrayList();

	public List<FlaAlertDTO> findAlerts(List<Integer> lstFlightID) throws ModuleException {

		List<FlaAlertDTO> flaAlertLst = new ArrayList<FlaAlertDTO>();
		if (alertFindingStrategyList != null && !alertFindingStrategyList.isEmpty()) {
			for (AlertFinderStatergy alertComposeStrategy : alertFindingStrategyList) {
				try {
					flaAlertLst.addAll(alertComposeStrategy.composeAlerts(lstFlightID));
				} catch (ModuleException e) {
					log.error(e);
				}

			}
		}

		return flaAlertLst;
	}

	public List<AlertFinderStatergy> getAlertFindingStrategyList() {
		return alertFindingStrategyList;
	}

	public void setAlertFindingStrategyList(List<AlertFinderStatergy> alertFindingStrategyList) {
		this.alertFindingStrategyList = alertFindingStrategyList;
	}

}
