package com.isa.thinair.reporting.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.reporting.api.service.MISDataProviderBD;

@Remote
public interface MISDataProviderBDImpl extends MISDataProviderBD {

}
