package com.isa.thinair.reporting.core.persistent.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.reporting.api.model.FlightInventroyDTO;

public interface FLADataProviderDAOJDBC {

	public FlightInventroyDTO getFlightInventoryDetails(Integer flightID, String lastSevenDay, String LastThirtyDay,
			String departDate);

	public ArrayList getFlightInvGraphData(Integer flightID, String lastSeventhDay, String lastThirtyDay, String departDate,
			ArrayList<String> lstChannel);

	public ArrayList getGraphDataBreakDown(Integer flightID, String channel, ArrayList<Integer> nominalCodes);

	public List getFLAReservationAlert(List<Integer> flightIDLst);

	public List getFLADuplicateNameAlert(List<Integer> flightIDLst);

	public List getFLAPartialCreditAlert(List<Integer> flightIDLst);

	public List getFLATbaNamesAlert(List<Integer> flightIDLst);

	public List getGroupBookingsAlert(List<Integer> flightIDLst);

	public List getFLADupPassportAlert(List<Integer> flightIDLst);

	public HashMap<String, Date> getGraphDataModificationDates(ArrayList<String> lstPnr);

}
