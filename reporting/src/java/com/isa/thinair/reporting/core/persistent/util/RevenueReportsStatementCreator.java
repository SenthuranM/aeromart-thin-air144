package com.isa.thinair.reporting.core.persistent.util;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.core.util.ReportUtils;

public class RevenueReportsStatementCreator {

	/**
	 * Returns invoice summary by currency query from the new view created for OND charges breakdown.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public static String getNewInvoiceSummaryQueryByCurrency(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();
		String startDate = null;
		String endDate = null;
		String endPeriod = null;
		int agentPrefixLength = AppSysParamsUtil.getDefaultAirlineIdentifierCode().trim().length() + 1;
		boolean showAirlineIdentifierInReports = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			startDate = reportsSearchCriteria.getFromDate();
			endDate = reportsSearchCriteria.getToDate();
			endPeriod = reportsSearchCriteria.getToDate();
		} else {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
			endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		}

		if (showAirlineIdentifierInReports) {
			sb.append("SELECT   substr(agent_code, " + agentPrefixLength
					+ ") as agent_code, SUM (total_amount) AS invoice_amount, ");
		} else {
			sb.append("SELECT   agent_code, SUM (total_amount) AS invoice_amount, ");
		}
		sb.append("         SUM (total_amount_paycur) AS invoice_amount_paycurr, ");
		sb.append(" SUM (total_amount) AS invoice_amount_basecurr, ");
		sb.append("         SUM (total_fare_amount) AS total_fare, ");
		sb.append("         SUM (total_tax_amount) AS total_tax, ");
		sb.append("         SUM (total_sur_amount) AS total_sur, ");
		sb.append("         SUM (total_mod_amount) AS total_mod, ");
		sb.append("         SUM (total_fare_amount_paycur) AS total_fare_amount_paycurr, ");
		sb.append("         SUM (total_tax_amount_paycur) AS total_tax_paycurr, ");
		sb.append("         SUM (total_sur_amount_paycur) AS total_sur_paycurr, ");
		sb.append("         SUM (total_mod_amount_paycur) AS total_mod_paycurr, ");
		sb.append("         payment_currency_code AS payment_currency_code, ");
		sb.append("         total_air_amount AS total_air_amount, ");
		sb.append("         total_land_amount AS total_land_amount, ");
		sb.append("         total_air_amount_paycurr AS total_air_amount_paycurr, ");
		sb.append("        total_land_amount_paycurr AS total_land_amount_paycurr, ");
		sb.append("         agent_name AS agent_name, station_code AS station_code, ");
		sb.append("         invoice_number AS invoice_number, station_name AS station_name, ");
		sb.append("         territory_code AS territory_code, country_name AS country_name, ");
		sb.append("         emails_sent AS emails_sent, invoice_amount_actual as invoice_amount_actual ");
		sb.append("    FROM (SELECT xxx.agent_code AS agent_code, ");
		sb.append("                 xxx.total_amount AS total_amount, ");
		sb.append("                xxx.total_amount_paycur AS total_amount_paycur, ");
		sb.append("                 xxx.total_fare_amount AS total_fare_amount, ");
		sb.append("                 xxx.total_tax_amount AS total_tax_amount, ");
		sb.append("                 xxx.total_sur_amount AS total_sur_amount, ");
		sb.append("                 xxx.total_mod_amount AS total_mod_amount, ");
		sb.append("                 xxx.total_fare_amount_paycur AS total_fare_amount_paycur, ");
		sb.append("                 xxx.total_tax_amount_paycur AS total_tax_amount_paycur, ");
		sb.append("                 xxx.total_sur_amount_paycur AS total_sur_amount_paycur, ");
		sb.append("                 xxx.total_mod_amount_paycur AS total_mod_amount_paycur, ");
		sb.append("                 xxx.payment_currency_code AS payment_currency_code, ");
		sb.append("                 xxx.total_air_amount AS total_air_amount, ");
		sb.append("                 xxx.total_land_amount AS total_land_amount, ");
		sb.append("                 xxx.total_air_amount_paycurr AS total_air_amount_paycurr, ");
		sb.append("                 xxx.total_land_amount_paycurr AS total_land_amount_paycurr, ");
		sb.append("                 c.agent_name AS agent_name, c.station_code AS station_code, ");
		sb.append("                 inv.invoice_number AS invoice_number, ");
		sb.append("                 sta.station_name AS station_name, ");
		sb.append("                 c.territory_code AS territory_code, ");
		sb.append("                 cou.country_name AS country_name, ");
		sb.append("                 inv.emails_sent AS emails_sent,");
		sb.append("					inv.invoice_amount as invoice_amount_actual ");
		sb.append("            FROM (SELECT v.agent_code AS agent_code, ");
		sb.append("                         v.total_amount AS total_amount, ");
		sb.append("                         v.total_amount_paycur AS total_amount_paycur, ");
		sb.append("                         v.total_fare_amount AS total_fare_amount, ");
		sb.append("                         v.total_tax_amount AS total_tax_amount, ");
		sb.append("                         v.total_sur_amount AS total_sur_amount, ");
		sb.append("                         v.total_mod_amount AS total_mod_amount, ");
		sb.append("                         v.total_fare_amount_paycur ");
		sb.append("                                                  AS total_fare_amount_paycur, ");
		sb.append("                         v.total_tax_amount_paycur AS total_tax_amount_paycur, ");
		sb.append("                         v.total_sur_amount_paycur AS total_sur_amount_paycur, ");
		sb.append("                         v.total_mod_amount_paycur AS total_mod_amount_paycur, ");
		sb.append("                         v.currency_code AS payment_currency_code, ");
		sb.append("                         v.total_amount AS total_air_amount, ");
		sb.append("                         0 AS total_land_amount, ");
		sb.append("                         v.total_amount_paycur AS total_air_amount_paycurr, ");
		sb.append("                         0 AS total_land_amount_paycurr ");
		sb.append("                    FROM v_pax_ond_txn_summary v ");
		sb.append("                   WHERE v.nominal_code IN (19, 25) ");

		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" and v.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and v.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
			sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
		}
		if (reportsSearchCriteria.getAgents() != null) { // if all agents selected, reportsSearchCriteria.getAgents() ==
															// null
			sb.append(" AND ").append(
					ReportUtils.getReplaceStringForIN("v.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		}
		sb.append("                    AND v.sales_channel <> 11) xxx, ");
		sb.append("                 t_agent c, ");
		sb.append("                 t_invoice inv, ");
		sb.append("                 t_station sta, ");
		sb.append("                 t_country cou ");
		sb.append("           WHERE xxx.agent_code = c.agent_code ");
		sb.append("             AND c.agent_code = inv.agent_code ");
		sb.append("             AND sta.station_code = c.station_code ");
		sb.append("             AND cou.country_code = sta.country_code ");
		sb.append(" AND TO_DATE(INV.INVIOCE_PERIOD_FROM,'dd-mm-yy') = TO_DATE('" + startDate + "','dd-mm-yyyy hh24:mi:ss'))");
		sb.append("GROUP BY agent_code, agent_name, station_code, territory_code,invoice_number,  station_name, country_name,invoice_amount_actual, ");
		sb.append("      emails_sent, payment_currency_code,   total_air_amount, total_land_amount, total_land_amount_paycurr,total_air_amount_paycurr ");

		return sb.toString();
	}

	/***
	 * Returns invoice details by currency query from the new view created for OND charges breakdown.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public static String getNewInvoiceDetailsQueryByCurrency(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();
		String startDate = null;
		String endDate = null;
		String endPeriod = null;
		int agentPrefixLength = AppSysParamsUtil.getDefaultAirlineIdentifierCode().trim().length() + 1;
		boolean showAirlineIdentifierInReports = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();
		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			startDate = reportsSearchCriteria.getFromDate();
			endDate = reportsSearchCriteria.getToDate();
			endPeriod = reportsSearchCriteria.getToDate();
		} else {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
			endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		}

		sb.append("SELECT   a.pnr_pax_id, a.pnr,(a.title || ' ' || a.first_name || ' ' || a.last_name  ) AS pnr_pax_name, ");
		if (showAirlineIdentifierInReports) {
			sb.append("substr(a.agent_code, " + agentPrefixLength
					+ ") as agent_code, a.ext_reference, TO_CHAR (a.txn_date, 'DD/mm/YYYY') AS tnx_date, c.display_name, ");
		} else {
			sb.append("a.agent_code, a.ext_reference, TO_CHAR (a.txn_date, 'DD/mm/YYYY') AS tnx_date, c.display_name, ");
		}
		sb.append("a.total_amount AS amount, a.total_amount_paycur, a.total_fare_amount, ");
		sb.append("a.total_fare_amount_paycur, a.total_tax_amount,a.total_tax_amount_paycur, a.total_sur_amount, ");
		sb.append("a.total_sur_amount_paycur, a.total_mod_amount, a.total_mod_amount_paycur, a.currency_code AS pay_currency, ");
		sb.append(" 'Normal Air Reservation' AS pay_type  FROM v_pax_ond_txn_summary a, t_user c ");
		sb.append("WHERE a.nominal_code IN (19, 25) ");
		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" and a.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and a.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
			sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
		}
		// if(reportsSearchCriteria.getAgents() != null){ // if all agents selected, reportsSearchCriteria.getAgents()
		// == null
		// sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("a.agent_code",
		// reportsSearchCriteria.getAgents(), false) + "  ");
		//
		// }
		sb.append(String.format(" AND( a.AGENT_CODE ='%s' )", reportsSearchCriteria.getAgentCode()));
		sb.append(" AND a.sales_channel <> 11 AND c.user_id = a.user_id ORDER BY a.pnr, a.txn_date ");

		return sb.toString();
	}

	/**
	 * Returns Invoice Summary Attachment By CurrencyQuery from the new view created for OND charges breakdown.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public static String getNewInvoiceSummaryAttachmentByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuffer sb = new StringBuffer();
		String startDate = null;
		String endDate = null;
		String endPeriod = null;
		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			startDate = reportsSearchCriteria.getFromDate();
			endDate = reportsSearchCriteria.getToDate();
			endPeriod = reportsSearchCriteria.getToDate();
		} else {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
			endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		}

		sb.append("SELECT invoice.*, NVL (holiday.holi_amount, 0) holi_amount,   NVL (holiday.holi_amount_local, 0) holi_amount_local ");
		sb.append("FROM (SELECT *   FROM (SELECT invoice_number, invoice_date,  TO_CHAR (invioce_period_from, 'DD/mm/YYYY' ) invioce_period_from, ");
		sb.append(" TO_CHAR (invoice_period_to,  'DD/mm/YYYY' ) invoice_period_to  FROM t_invoice ");
		sb.append(" WHERE agent_code  = '" + reportsSearchCriteria.getAgentCode() + "' ");
		sb.append(" and to_date(invioce_period_from,'dd-mm-yy') = to_date('" + startDate + "','dd-mm-yyyy hh24:mi:ss') ) inv, ");
		sb.append(" (SELECT    SUM (v.total_amount) as invoice_amount,  SUM (v.total_amount_paycur)  as invoice_amount_paycurr,  ");
		sb.append(" SUM (v.total_fare_amount)as total_fare,   SUM (v.total_fare_amount_paycur)  as total_fare_amount_paycurr, ");
		sb.append(" SUM (v.total_tax_amount)as total_tax, SUM (v.total_tax_amount_paycur)  as total_tax_paycurr,  ");
		sb.append(" SUM (v.total_sur_amount)as total_sur,  SUM (v.total_sur_amount_paycur)   as total_sur_paycurr, ");
		sb.append(" SUM (v.total_mod_amount)as total_mod, SUM (v.total_mod_amount_paycur) as total_mod_paycurr, ");
		sb.append(" b.agent_code, b.agent_name, b.address_line_1,  b.address_line_2, b.address_state_province, v.ext_reference ");
		sb.append(" FROM (SELECT a.agent_code agent_code,  a.nominal_code AS nominal_code,   a.user_id AS user_id, ");
		sb.append(" a.ext_reference AS ext_reference,  a.txn_date AS tnx_date,  a.total_amount AS total_amount, ");
		sb.append(" a.total_amount_paycur AS total_amount_paycur,  a.total_fare_amount AS total_fare_amount, ");
		sb.append(" a.total_fare_amount_paycur  AS total_fare_amount_paycur,  a.total_tax_amount AS total_tax_amount, ");
		sb.append(" a.total_tax_amount_paycur AS total_tax_amount_paycur,a.total_sur_amount AS total_sur_amount, ");
		sb.append(" a.total_sur_amount_paycur  AS total_sur_amount_paycur,  a.total_mod_amount AS total_mod_amount, ");
		sb.append(" a.total_mod_amount_paycur   AS total_mod_amount_paycur    FROM v_pax_ond_txn_summary a ");
		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" WHERE a.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and v.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
		}
		sb.append(" AND( a.agent_code ='" + reportsSearchCriteria.getAgentCode() + "' )");
		sb.append(" AND a.sales_channel <> 11  ) v,  t_agent b  WHERE v.agent_code = b.agent_code ");
		sb.append(" AND v.nominal_code IN (19, 25) ");
		sb.append(" GROUP BY b.agent_code,   b.agent_name, b.address_line_1, b.address_line_2,  b.address_state_province, ");
		sb.append(" v.ext_reference) BREAK) invoice, (SELECT   agent_code, NVL (SUM (amount), 0) holi_amount, ");
		sb.append(" NVL (SUM (amount_local), 0) holi_amount_local  FROM t_agent_transaction ");
		sb.append("    where agent_code  = '" + reportsSearchCriteria.getAgentCode() + "'");
		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
		}
		sb.append("       and nominal_code in (10,11) ");
		sb.append("       GROUP by agent_code ");
		sb.append(" ) holiday ");
		sb.append(" where invoice.agent_code = holiday.agent_code(+) ");

		return sb.toString();
	}

	/**
	 * TODO: check where is the detail attachment is calling
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public static String getNewInvoiceDetailsAttachmentByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();
		String startDate = null;
		String endDate = null;
		String endPeriod = null;
		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			startDate = reportsSearchCriteria.getFromDate();
			endDate = reportsSearchCriteria.getToDate();
			endPeriod = reportsSearchCriteria.getToDate();
		} else {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
			endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		}

		sb.append("SELECT invoice.*, NVL (holiday.holi_amount, 0) holi_amount,   NVL (holiday.holi_amount_local, 0) holi_amount_local ");
		sb.append("FROM (SELECT *   FROM (SELECT invoice_number, invoice_date,  TO_CHAR (invioce_period_from, 'DD/mm/YYYY' ) invioce_period_from, ");
		sb.append(" TO_CHAR (invoice_period_to,  'DD/mm/YYYY' ) invoice_period_to  FROM t_invoice ");
		sb.append(" WHERE agent_code  = '" + reportsSearchCriteria.getAgentCode() + "' ");
		sb.append(" and to_date(invioce_period_from,'dd-mm-yy') = to_date('" + startDate + "','dd-mm-yyyy hh24:mi:ss') ) inv, ");
		sb.append(" (SELECT    SUM (v.total_amount) as invoice_amount,  SUM (v.total_amount_paycur)  as invoice_amount_paycurr,  ");
		sb.append(" SUM (v.total_fare_amount)as total_fare,   SUM (v.total_fare_amount_paycur)  as total_fare_amount_paycurr, ");
		sb.append(" SUM (v.total_tax_amount)as total_tax, SUM (v.total_tax_amount_paycur)  as total_tax_paycurr,  ");
		sb.append(" SUM (v.total_sur_amount)as total_sur,  SUM (v.total_sur_amount_paycur)   as total_sur_paycurr, ");
		sb.append(" SUM (v.total_mod_amount)as total_mod, SUM (v.total_mod_amount_paycur) as total_mod_paycurr, ");
		sb.append(" b.agent_code, b.agent_name, b.address_line_1,  b.address_line_2, b.address_state_province, v.ext_reference ");
		sb.append(" FROM (SELECT a.agent_code agent_code,  a.nominal_code AS nominal_code,   a.user_id AS user_id, ");
		sb.append(" a.ext_reference AS ext_reference,  a.txn_date AS tnx_date,  a.total_amount AS total_amount, ");
		sb.append(" a.total_amount_paycur AS total_amount_paycur,  a.total_fare_amount AS total_fare_amount, ");
		sb.append(" a.total_fare_amount_paycur  AS total_fare_amount_paycur,  a.total_tax_amount AS total_tax_amount, ");
		sb.append(" a.total_tax_amount_paycur AS total_tax_amount_paycur,a.total_sur_amount AS total_sur_amount, ");
		sb.append(" a.total_sur_amount_paycur  AS total_sur_amount_paycur,  a.total_mod_amount AS total_mod_amount, ");
		sb.append(" a.total_mod_amount_paycur   AS total_mod_amount_paycur    FROM v_pax_ond_txn_summary a ");
		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" WHERE a.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and v.txn_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
		}
		sb.append(" AND( a.agent_code ='" + reportsSearchCriteria.getAgentCode() + "' )");
		sb.append(" AND a.sales_channel <> 11  ) v,  t_agent b  WHERE v.agent_code = b.agent_code ");
		sb.append(" AND v.nominal_code IN (19, 25) ");
		sb.append(" GROUP BY b.agent_code,   b.agent_name, b.address_line_1, b.address_line_2,  b.address_state_province, ");
		sb.append(" v.ext_reference) BREAK) invoice, (SELECT   agent_code, NVL (SUM (amount), 0) holi_amount, ");
		sb.append(" NVL (SUM (amount_local), 0) holi_amount_local  FROM t_agent_transaction ");
		sb.append("    where agent_code  = '" + reportsSearchCriteria.getAgentCode() + "'");
		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
			sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
		}
		sb.append("       and nominal_code in (10,11) ");
		sb.append("       GROUP by agent_code ");
		sb.append(" ) holiday ");
		sb.append(" where invoice.agent_code = holiday.agent_code(+) ");

		return sb.toString();
	}

	public static String getNewAgentCommissionQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		sb.append(" SELECT   t.agent_code, a.agent_name, SUM (t.amount) AS SUM, SUM (t.localamt) AS sum_local, ");
		sb.append(" SUM (t.total_fare_amount) AS total_fare,SUM (t.tot_charges) AS total_charges, ");
		sb.append(" SUM (t.commission) AS commission, a.account_code, a.currency_code,(t.rate / 100) AS rate ");
		sb.append(" FROM (SELECT v.agent_code AS agent_code, v.pnr, v.total_amount AS amount, ");
		sb.append(" v.total_amount_paycur AS localamt, v.total_fare_amount, ");
		sb.append("  (  v.total_fare_amount + v.total_tax_amount + v.total_sur_amount + v.total_mod_amount ) AS tot_charges, ");
		sb.append("   DECODE (acr.value_percentage_flag, NULL, 0, 'P', (v.total_fare_amount * acr.commission) / 100, 'V', acr.commission) AS commission, ");
		sb.append(" acr.commission AS rate FROM v_pax_ond_txn_summary v, ");
		sb.append(" (SELECT agc.agent_commission_rate_id,   agc.agent_commission_code, ag.agent_code, agc.value_percentage_flag, agc.commission ");
		sb.append("  FROM t_agent_commission_rate agc, t_agent ag    WHERE ag.agent_commission_code = agc.agent_commission_code ");
		sb.append("  AND agc.effective_from_date < SYSDATE    AND agc.effective_to_date > SYSDATE  AND agc.status = 'ACT') acr ");
		sb.append(" WHERE v.agent_code = acr.agent_code(+)  AND v.nominal_code IN ('19', '25') ");
		sb.append("	AND v.txn_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "', 'DD-MON-YYYY HH24:mi:ss' )  AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
				+ "', 'DD-MON-YYYY HH24:mi:ss') ");
		sb.append("	) T, T_AGENT A ");
		sb.append("	where A.AGENT_CODE = T.AGENT_CODE ");
		sb.append("   AND  " + ReportUtils.getReplaceStringForIN("A.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
		sb.append("	AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
		sb.append("group by t.agent_code,A.agent_name, a.account_code, a.currency_code , t.rate  ");

		return sb.toString();
	}

	/**
	 * Returns CompanyPaymentSummaryByCurrencyQuery from the new database view created from charges/payments breakdown
	 * 
	 * @param reportsSearchCriteria
	 * @param singleDataValuePayments
	 * @return
	 */
	public static String getNewCompanyPaymentSummaryByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria,
			String singleDataValuePayments) {
		StringBuffer sb = new StringBuffer();

		String gmtFromOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtFromOffsetInMinutes() + "/1440 ";
		String gmtToOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtToOffsetInMinutes() + "/1440 ";

		sb.append("SELECT   agent_code, agent_name, payment_currency_code, total_amount, ");
		sb.append("total_amount_paycur, total_fare_amount, total_fare_amount_paycur, ");
		sb.append("total_tax_amount, total_tax_amount_paycur, total_sur_amount, ");
		sb.append(" total_sur_amount_paycur, total_modify, total_refund, ");
		;
		sb.append("total_modify_local, total_refund_local, ");
		sb.append("(total_amount + total_refund) pmt_amt, ");
		sb.append("(total_amount_paycur + total_refund_local) pmt_amt_local ");
		sb.append("FROM (SELECT   a.agent_code, a.agent_name, t.payment_currency_code, ");
		sb.append("          SUM (t.total_amount) AS total_amount, ");
		sb.append("          SUM (t.total_amount_paycur) AS total_amount_paycur, ");
		sb.append("          SUM (t.total_fare_amount) AS total_fare_amount, ");
		sb.append("          SUM (total_fare_amount_paycur) AS total_fare_amount_paycur, ");
		sb.append("          SUM (total_tax_amount) AS total_tax_amount, ");
		sb.append("          SUM (total_tax_amount_paycur) AS total_tax_amount_paycur, ");
		sb.append("          SUM (total_sur_amount) AS total_sur_amount, ");
		sb.append("          SUM (total_sur_amount_paycur) AS total_sur_amount_paycur, ");
		sb.append("          SUM (DECODE (t.nominal_code, ");
		sb.append("                       30, total_mod_amount, ");
		sb.append("                       32, total_mod_amount, ");
		sb.append("                       6, total_mod_amount, ");
		sb.append("                       28, total_mod_amount, ");
		sb.append("                      18, total_mod_amount, ");
		sb.append("                       17, total_mod_amount, ");
		sb.append("                       16, total_mod_amount, ");
		sb.append("                       19, total_mod_amount, ");
		sb.append("                       15, total_mod_amount, ");
		sb.append("                       0  )) AS total_modify, ");
		sb.append("                      SUM (DECODE (t.nominal_code, ");
		sb.append("                       31, total_mod_amount,  33, total_mod_amount,5, total_mod_amount,29, total_mod_amount, ");
		sb.append("                       26, total_mod_amount, 24, total_mod_amount, 23, total_mod_amount,25, total_mod_amount,22, total_mod_amount, 0 ) ");
		sb.append("                       ) AS total_refund,  SUM (DECODE (t.nominal_code, 30, total_mod_amount_paycur, 32, total_mod_amount_paycur, ");
		sb.append("   						6, total_mod_amount_paycur, 28, total_mod_amount_paycur,18, total_mod_amount_paycur,17, total_mod_amount_paycur, ");
		sb.append("                     16, total_mod_amount_paycur, 19, total_mod_amount_paycur, 15, total_mod_amount_paycur,  0) ) AS total_modify_local, ");
		sb.append(" SUM (DECODE (t.nominal_code, 31, total_mod_amount_paycur,33, total_mod_amount_paycur,5, total_mod_amount_paycur,29, total_mod_amount_paycur, ");
		sb.append(" 26, total_mod_amount_paycur,  24, total_mod_amount_paycur, 23, total_mod_amount_paycur, 25, total_mod_amount_paycur,22, total_mod_amount_paycur,0) ");
		sb.append("  ) total_refund_local  FROM (SELECT v.agent_code agent_code, ");
		sb.append("     v.nominal_code AS nominal_code,  v.total_amount AS total_amount,    v.total_amount_paycur AS total_amount_paycur, ");
		sb.append("    v.total_fare_amount AS total_fare_amount,  v.total_fare_amount_paycur  AS total_fare_amount_paycur, ");
		sb.append(" v.total_tax_amount AS total_tax_amount,   v.total_tax_amount_paycur AS total_tax_amount_paycur, ");
		sb.append("  v.total_sur_amount AS total_sur_amount,  v.total_sur_amount_paycur    AS total_sur_amount_paycur, ");
		sb.append("   v.total_mod_amount AS total_mod_amount,   v.total_mod_amount_paycur AS total_mod_amount_paycur, ");
		if (reportsSearchCriteria.isInbaseCurr()) {
			sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "'    AS PAYMENT_CURRENCY_CODE");
		} else {
			sb.append(" v.currency_code    AS PAYMENT_CURRENCY_CODE");
		}
		sb.append("             FROM v_pax_ond_txn_summary v  WHERE v.txn_date BETWEEN TO_DATE ");
		sb.append("('" + reportsSearchCriteria.getDateRangeFrom() + "'  ||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
		sb.append(gmtFromOffsetInMinutesStr);
		sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "'   ||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
		sb.append(gmtToOffsetInMinutesStr);
		if(!reportsSearchCriteria.isByStation()){
			sb.append("		AND" + ReportUtils.getReplaceStringForIN("v.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}
		if (reportsSearchCriteria.isByStation() && reportsSearchCriteria.getAgents() != null
				&& !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append("		AND" + ReportUtils.getReplaceStringForIN("v.agent_code", reportsSearchCriteria.getAgents(), false)
					+ " ");
		}
		sb.append("              AND v.sales_channel <> 11) t, ");
		sb.append("          t_agent a ");
		sb.append(" WHERE T.NOMINAL_CODE IN(" + singleDataValuePayments + ")");
		sb.append("       AND a.agent_code = t.agent_code ");
		sb.append("       AND a.is_lcc_integration_agent = 'N' ");
		if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
			sb.append(" AND a.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
		}
		sb.append("  GROUP BY a.agent_code, a.agent_name, t.payment_currency_code) ");
		sb.append(" ORDER BY agent_code, payment_currency_code ");

		return sb.toString();
	}

	/***
	 * Returns Company Payment details By Currency Query from the new database view created from charges/payments
	 * breakdown
	 * 
	 * @param reportsSearchCriteria
	 * @param singleDataValuePayments
	 * @return
	 */
	public static String getNewCompanyPaymentDetailsByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria,
			String singleDataValuePayments) {
		StringBuffer sb = new StringBuffer();

		sb.append("select agent_code,agent_name,decode(nominal_code,19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',"
				+ "30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT') description,to_char(payment_date,'dd-MON-yyyy')payment_date,");
		sb.append("		pnr,passenger,routing,username,");
		sb.append("		 total_amount,  total_amount_paycur,");
		sb.append("		 total_fare_amount,  total_fare_amount_paycur, ");
		sb.append("		 total_tax_amount,  total_tax_amount_paycur,");
		sb.append("		 total_sur_amount,  total_sur_amount_paycur, ");
		sb.append("		 total_modify,  total_refund,");
		sb.append("		 total_modify_local,  total_refund_local, payment_currency_code, ");
		sb.append(" (total_amount + total_refund) pay_amt,");
		sb.append(" (total_amount_paycur + total_refund_local) pay_amt_local");
		sb.append(",eticket ");
		sb.append(" ,ACTUAL_PAY as ACTUAL_PAY, PAY_REF as PAY_REF ");
		sb.append("FROM ");
		sb.append("     (SELECT   c.agent_code, c.agent_name, v.nominal_code, ");
		sb.append("                   v.tnx_date payment_date, pnr pnr, ");
		sb.append("                   v.first_name || ' ' || v.last_name passenger, ");
		sb.append("                  u.display_name username, NVL (v.eticket_id, ");
		sb.append("                                                 ' ') AS eticket, ");
		sb.append("                    SUM (total_amount) total_amount, ");
		sb.append("                   SUM (total_amount_paycur) total_amount_paycur, ");
		sb.append("                    SUM (total_fare_amount) total_fare_amount, ");
		sb.append("                    SUM (total_fare_amount_paycur) ");
		sb.append("                    total_fare_amount_paycur, ");
		sb.append("                    SUM (total_tax_amount) total_tax_amount, ");
		sb.append("                   SUM (total_tax_amount_paycur) ");
		sb.append("                    total_tax_amount_paycur, ");
		sb.append("                     SUM (total_sur_amount) total_sur_amount, ");
		sb.append("                    SUM (total_sur_amount_paycur) ");
		sb.append("                    total_sur_amount_paycur, ");
		sb.append("                     SUM (DECODE (v.nominal_code, 30, total_mod_amount, 32, total_mod_amount, 6, total_mod_amount,28, total_mod_amount, ");
		sb.append("                      18, total_mod_amount,   17, total_mod_amount,  16, total_mod_amount,  19, total_mod_amount,  15, total_mod_amount, 0) ");
		sb.append("                        )  total_modify, ");
		sb.append("                        SUM (DECODE (v.nominal_code,  31, total_mod_amount, 33, total_mod_amount, 5, total_mod_amount,  29, total_mod_amount, ");
		sb.append("                      26, total_mod_amount,  24, total_mod_amount, 23, total_mod_amount,    ");
		sb.append("                       25, total_mod_amount,  22, total_mod_amount, 0   )   ) total_refund, ");
		sb.append(" 					SUM (DECODE (v.nominal_code, 30, total_mod_amount_paycur, ");
		sb.append("                       32, total_mod_amount_paycur, 6, total_mod_amount_paycur,28, total_mod_amount_paycur,  18, total_mod_amount_paycur, ");
		sb.append("                       17, total_mod_amount_paycur,  16, total_mod_amount_paycur, 19, total_mod_amount_paycur, ");
		sb.append("                         15, total_mod_amount_paycur,   0  ))  total_modify_local, ");
		sb.append("  				SUM (DECODE (v.nominal_code,  31, total_mod_amount_paycur, 33, total_mod_amount_paycur,5, total_mod_amount_paycur, ");
		sb.append(" 				29, total_mod_amount_paycur,26, total_mod_amount_paycur,24, total_mod_amount_paycur, 23, total_mod_amount_paycur, ");
		sb.append("                  25, total_mod_amount_paycur, 22, total_mod_amount_paycur,0 )  ) total_refund_local, ");
		sb.append("                     payment_currency_code, v.routing,   v.actual_pay AS actual_pay, v.pay_ref AS pay_ref  ");
		sb.append("  FROM (SELECT p.pnr AS pnr, b.agent_code AS agent_code,a.nominal_code AS nominal_code, ");
		sb.append("   a.user_id AS user_id, a.pnr_pax_id AS pnr_pax_id, a.first_name AS first_name,  a.last_name AS last_name, a.txn_date AS tnx_date, ");
		sb.append(" a.total_amount AS total_amount,a.total_amount_paycur AS total_amount_paycur,  a.total_fare_amount AS total_fare_amount, ");
		sb.append("   a.total_fare_amount_paycur AS total_fare_amount_paycur,  a.total_tax_amount AS total_tax_amount, ");
		sb.append("     a.total_tax_amount_paycur AS total_tax_amount_paycur, a.total_sur_amount AS total_sur_amount, ");
		sb.append("    a.total_sur_amount_paycur  AS total_sur_amount_paycur,  a.total_mod_amount AS total_mod_amount, ");
		sb.append("   a.total_mod_amount_paycur   AS total_mod_amount_paycur, ");
		if (reportsSearchCriteria.isInbaseCurr()) {
			sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE, ");
		} else {
			sb.append(" a.currency_code    AS PAYMENT_CURRENCY_CODE, ");
		}
		sb.append("  f_company_pymt_report (a.pnr) AS routing,actual_p.payment_mode AS actual_pay,a.ext_reference AS pay_ref ");
		sb.append(" FROM v_pax_ond_txn_summary a, t_pax_transaction b,t_pnr_passenger p, t_actual_payment_method actual_p ");
		sb.append("   WHERE a.txn_id = b.txn_id  AND a.pnr_pax_id = p.pnr_pax_id  ");
		sb.append(" AND a.txn_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
		if (!reportsSearchCriteria.isByStation()) {
			sb.append("		AND" + ReportUtils.getReplaceStringForIN("a.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}
		if (reportsSearchCriteria.isByStation() && reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append("		AND" + ReportUtils.getReplaceStringForIN("a.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}
		sb.append("  AND actual_p.payment_mode_id(+) = b.payment_mode_id  AND a.sales_channel <> 11 ) v, ");
		sb.append("   t_agent c, t_pax_trnx_nominal_code n,t_user u  ");
		sb.append(" WHERE v.nominal_code IN(" + singleDataValuePayments + ")");
		sb.append("   AND v.payment_currency_code = '" + reportsSearchCriteria.getCurrencies() + "'");
		sb.append("     AND v.agent_code = c.agent_code   AND v.user_id = u.user_id   AND v.nominal_code = n.nominal_code ");
		if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
			sb.append(" AND c.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
		}
		sb.append("   AND c.is_lcc_integration_agent = 'N'   ");
		if (reportsSearchCriteria.isOnlyReportingAgents()) {

			sb.append(" AND( c.AGENT_CODE IN (SELECT AGENT_CODE");
			sb.append(" FROM T_AGENT");
			sb.append(" WHERE STATUS = 'ACT'");
			sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
			sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"'))");

		}	
		sb.append(" GROUP BY v.pnr_pax_id, v.nominal_code, c.agent_code, c.agent_name, v.payment_currency_code,v.pnr, ");
		sb.append("  v.first_name || ' ' || v.last_name,  u.display_name,  v.tnx_date,  v.eticket_id,  v.routing,  v.pay_ref,   v.actual_pay) ");
		sb.append(" ORDER BY agent_code, description, payment_currency_code, payment_date, pnr ");

		return sb.toString();
	}

	/**
	 * Returns getNewModeOfPaymentSummaryByCurrencyQuery from the new database view created from charges/payments
	 * breakdown
	 * 
	 * @param reportsSearchCriteria
	 * @param singleDataValuePayments
	 * @return
	 */
	public static String getNewModeOfPaymentSummaryByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria,
			String singleDataValuePayments) {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT   DECODE (pay, 15, 1, 16, 2, 28, 3, 17, 4, 19, 5, 18, 6, 30, 7, 32), ");
		sb.append("DECODE (pay, 19, 'On Account', 18, 'Cash', 15, 'Visa', 16, 'Master', 28, 'Amex', 17, 'Diners', 30, 'Generic', 32, 'CMI') AS payment_mode,  ");
		sb.append("payment_currency_code, amt, amt_base, pay ");
		sb.append("FROM (SELECT   payment_currency_code, ");
		sb.append("          ( SUM (total_amount_paycur)) amt, ");
		sb.append("          ( SUM (total_amount)) amt_base, ");
		sb.append("          DECODE (nominal_code, 25, 19,22, 15, 23, 16, 24, 17,26, 18, 29, 28,6, 5,31, 30, 33, 32, nominal_code) pay ");
		sb.append("     FROM (SELECT a.currency_code as payment_currency_code, a.total_amount_paycur as total_amount_paycur, ");
		sb.append("       a.total_amount as total_amount, a.nominal_code as nominal_code  FROM v_pax_ond_txn_summary a,  t_agent g ");
		sb.append(" WHERE a.txn_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "', 'dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "' , 'dd-mon-yyyy hh24:mi:ss')");
		sb.append("  AND a.agent_code = g.agent_code(+) AND (   g.is_lcc_integration_agent = 'N'   OR g.is_lcc_integration_agent IS NULL) ");
		sb.append(" AND a.nominal_code IN (" + singleDataValuePayments + ") )");
		sb.append(" GROUP BY DECODE (nominal_code, 25, 19, 22, 15, 23, 16,  24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32,nominal_code), ");
		sb.append("          payment_currency_code) ORDER BY 1, 2 ");

		return sb.toString();

	}

	/***
	 * Returns getNewModeOfPayment details By Currency Query from the new database view created from charges/payments
	 * breakdown
	 * 
	 * @param reportsSearchCriteria
	 * @param singleDataValuePayments
	 * @return
	 */
	public static String getNewModeOfPaymentDetailsByCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria,
			String singleDataValuePayments) {
		StringBuffer sb = new StringBuffer();

		sb.append("SELECT v.pnr, ( v.title|| ' ' || v.first_name  || ' '|| v.last_name ) AS pnr_pax_name, ");
		sb.append(" DECODE (V.sales_channel, 4, 'Web',display_name ) display_name, TO_CHAR (V.txn_date, 'DD/mm/YYYY') AS tnx_date, ");
		sb.append(" (V.total_amount_paycur) AS amount,  actual_p.payment_mode AS actual_pay,  V.ext_reference AS pay_ref ");
		sb.append(" FROM v_pax_ond_txn_summary V, t_user u, t_agent g, t_actual_payment_method actual_p,t_pax_transaction pt ");
		sb.append(" WHERE V.user_id = u.user_id(+)  AND V.agent_code = g.agent_code(+) ");
		sb.append(" AND actual_p.payment_mode_id(+) = pt.payment_mode_id ");
		sb.append(" AND pt.txn_id = V.txn_id ");
		sb.append(" AND (   g.is_lcc_integration_agent = 'N' OR g.is_lcc_integration_agent IS NULL ) ");
		sb.append("AND V.txn_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom() + "', 'dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'dd-mon-yyyy hh24:mi:ss')");
		sb.append("AND V.nominal_code IN (" + singleDataValuePayments + ") ");
		if (reportsSearchCriteria.getPaymentCode() != null && !reportsSearchCriteria.getPaymentCode().equalsIgnoreCase("null")) {
			sb.append("AND V.currency_code = '" + reportsSearchCriteria.getPaymentCode() + "'  ");
		}
		sb.append(" ORDER BY 1 ");

		return sb.toString();
	}

}
