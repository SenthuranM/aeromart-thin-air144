package com.isa.thinair.reporting.core.bl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCInvDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightBCinfoDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightLegDTO;
import com.isa.thinair.reporting.api.dto.aviator.AviatorFlightSegInventoryDTO;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistence.dao.AviatorDAO;
import com.isa.thinair.reporting.core.util.LookupUtils;
import com.isaaviation.thinair.webservices.api.aviator.AType;
import com.isaaviation.thinair.webservices.api.aviator.AirlineType;
import com.isaaviation.thinair.webservices.api.aviator.AviatorInventoryFile;
import com.isaaviation.thinair.webservices.api.aviator.FType;
import com.isaaviation.thinair.webservices.api.aviator.FlightType;
import com.isaaviation.thinair.webservices.api.aviator.FlightsType;
import com.isaaviation.thinair.webservices.api.aviator.LType;
import com.isaaviation.thinair.webservices.api.aviator.LegType;
import com.isaaviation.thinair.webservices.api.aviator.ObjectFactory;
import com.isaaviation.thinair.webservices.api.aviator.SCType;
import com.isaaviation.thinair.webservices.api.aviator.SType;
import com.isaaviation.thinair.webservices.api.aviator.SegCabinType;
import com.isaaviation.thinair.webservices.api.aviator.SegType;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

/**
* @author Manoj Dhanushka
* 
* This class will generate the Inventory file for Mahan RM system Aviator
*/
public class AviatorFileGenerator {
	
	AviatorDAO aviatorDAO = null;
	SimpleDateFormat sdFmt = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdFmt1 = new SimpleDateFormat("HH:mm");
	SimpleDateFormat sdFmt2 = new SimpleDateFormat("HH:mm:ss");
	
	private static final Log log = LogFactory.getLog(AviatorFileGenerator.class);
	
	private static GlobalConfig globalConfig;
	
	private static MessagingServiceBD messagingServiceBD;
	
	public AviatorFileGenerator() {
		if (aviatorDAO == null) {
			this.aviatorDAO = getAviatorJDBCDAO();
		}
		messagingServiceBD = LookupUtils.getMessagingServiceBD();
		globalConfig = LookupUtils.getGlobalConfig();;
	}
	
	public void generateAviatorFile() throws ModuleException {
		
		List<AviatorFlightLegDTO> flightLegDTOs = aviatorDAO.getFlightLegDTOs();
		log.info("AVIATOR file getFlightLegDTOs Done" );
		Map<Integer, List<AviatorFlightSegInventoryDTO>> flightSegInvDTOs = aviatorDAO.getFlightLegInventory();
		Map<Integer, List<AviatorFlightBCInvDTO>> flightSegmentDTOs = aviatorDAO.getFlightSegmentDetails();	
		log.info("AVIATOR file getFlightBCinfoDTOs Start" );
		Map<Integer, List<AviatorFlightBCinfoDTO>> flightBCinfoDTOs = aviatorDAO.getFlightBCinfoDTOs();
		log.info("AVIATOR file getFlightBCinfoDTOs Done" );
		
		Map<Integer,FlightType> flightTypeMap = new HashMap<Integer, FlightType>();
		
		AviatorInventoryFile aviatorInventoryFile = new AviatorInventoryFile();	
		FlightsType flightsType = new FlightsType();	
		
		for (AviatorFlightLegDTO flightLegDTO : flightLegDTOs) {
			Integer flightId = flightLegDTO.getFlightId();
			ObjectFactory fact = new ObjectFactory(); 
			
			if (!flightTypeMap.containsKey(flightId)) {
				FlightType flightType = new FlightType();

				FType fType = new FType();
				fType.setFlightNumber(flightLegDTO.getFlightNumber());
				fType.setFlightDate(sdFmt.format(flightLegDTO.getDepartureDateLocal()));
				fType.setAircraft(flightLegDTO.getModelNumber());
				flightType.setF(fType);
				
				AType aType = new AType();
				aType.setDepartureAirport(flightLegDTO.getOrigin());
				JAXBElement<String> departureTime = fact.createATypeDepartureTime(sdFmt1.format(flightLegDTO.getEstDepartureTimeLocal()));   
				JAXBElement<String> arrivalAirport = fact.createATypeArrivalAirport(flightLegDTO.getDestination()); 
				JAXBElement<String> arrivalTime = fact.createATypeArrivalTime(sdFmt1.format(flightLegDTO.getEstArrivalTimeLocal())); 
				JAXBElement<Integer> dateOffset = fact.createATypeDateOffset(flightLegDTO.getEstArrivalDayOffsetLocal());  

				aType.getDepartureTimeAndArrivalAirportAndArrivalTime().add(departureTime);
				aType.getDepartureTimeAndArrivalAirportAndArrivalTime().add(arrivalAirport);
				aType.getDepartureTimeAndArrivalAirportAndArrivalTime().add(arrivalTime);
				aType.getDepartureTimeAndArrivalAirportAndArrivalTime().add(dateOffset);
				flightType.setA(aType);
				
				LegType legType = new LegType();
				LType lType = new LType();
				lType.setBoardPoint(flightLegDTO.getOrigin());
				lType.setOffPoint(flightLegDTO.getDestination());
				legType.getL().add(lType);	
				flightType.setLeg(legType);
				
				List<AviatorFlightSegInventoryDTO> flightSegInvList = flightSegInvDTOs.get(flightId);
				if (flightSegInvList != null) {
					SegCabinType segCabinType = new SegCabinType();
					for (AviatorFlightSegInventoryDTO flightSegInvDTO : flightSegInvList) {
						SCType scType = new SCType();
						scType.setBoardPoint(flightSegInvDTO.getSegmentCode().substring(0, 3));
						int length = flightSegInvDTO.getSegmentCode().length();
						scType.setOffPoint(flightSegInvDTO.getSegmentCode().substring(length - 3, length));
						scType.setCabinCode(flightSegInvDTO.getCabinClassCode());
						scType.setAllocAdult(flightSegInvDTO.getCapacity());
						scType.setOsoldAdult(flightSegInvDTO.getOverSellAdult());
						scType.setCurtail(flightSegInvDTO.getCurtail());
						scType.setWLAlloc(flightSegInvDTO.getWlAlloc());
						scType.setTB(flightSegInvDTO.getTotalBookings());
						scType.setAvailAdult(flightSegInvDTO.getAvailableAdult());
						scType.setOverAdult(flightSegInvDTO.getOverbookAdult());
						scType.setWLAdult(flightSegInvDTO.getWaitlistAdult());
						scType.setFltSegStatus(flightSegInvDTO.getFltSegStatus());							
						segCabinType.getSC().add(scType);
					}
					flightType.setSegCabin(segCabinType);
				}
				
				List<AviatorFlightBCInvDTO> flightSegDTOs = flightSegmentDTOs.get(flightId);
				if (flightSegDTOs != null) {
					SegType segType = new SegType();
					for (AviatorFlightBCInvDTO flightSegDTO : flightSegDTOs) {
						
						SType sType = new SType();
						String[] values = flightSegDTO.getSegmentCode().split("/");	
						int length = values.length;
						sType.setBoardPoint(values[0]);	
						sType.setOffPoint(values[length - 1]);
						sType.setCabinCode(flightSegDTO.getLogicalCabinClass());
						sType.setClazz(flightSegDTO.getBookingCode());
						sType.setRank(flightSegDTO.getNestRank());
						sType.setFix(flightSegDTO.getFixedFlag());
						sType.setInd(flightSegDTO.getStatus());
						sType.setBkd(flightSegDTO.getActualSeatsSold() + flightSegDTO.getActualSeatsOnHold());
						sType.setAcquire(flightSegDTO.getAcquiredSeats());
						sType.setCnxd(flightSegDTO.getCancelledSeats());
						sType.setFareCN(flightSegDTO.isConnectionFareAvailable() ? "Y" : "N");
						sType.setWt(flightSegDTO.getWaitlistedSeats());
						sType.setTkt(flightSegDTO.getActualSeatsSold());
						sType.setAllocation(flightSegDTO.getAllocatedSeats());
						sType.setAvailability(flightSegDTO.getAvailableSeats());
						sType.setNestedAvailability(flightSegDTO.getAvailableNestedSeats());
						sType.setSoldIn(flightSegDTO.getSeatsSoldAquiredByNesting());
						sType.setSoldOut(flightSegDTO.getSeatsSoldNested());
						sType.setOhdIn(flightSegDTO.getSeatsOnHoldAquiredByNesting());
						sType.setOhdOut(flightSegDTO.getSeatsOnHoldNested());
						sType.setFareOW(flightSegDTO.getOwFare());
						sType.setFareRT(flightSegDTO.getRtFare());	
						
						List<AviatorFlightBCinfoDTO> flightBCinfoDTOsList = flightBCinfoDTOs.get(flightSegDTO.getFlightSegId());
						if (flightBCinfoDTOsList != null) {
							for (AviatorFlightBCinfoDTO flightBCinfoDTO : flightBCinfoDTOsList) {
								if(flightBCinfoDTO.getBookingCode().equals(flightSegDTO.getBookingCode())) {
									sType.setGoshow(flightBCinfoDTO.getGoshowCount());
									sType.setNoshow(flightBCinfoDTO.getNoshowCount());
									sType.setRev(flightBCinfoDTO.getRevenue());
								}
							}
						}
						segType.getS().add(sType);
					}
					flightType.setSeg(segType);	
				}
				
				flightsType.getFlight().add(flightType);
				flightTypeMap.put(flightId, flightType);
				
			} else {
				FlightType flightType = flightTypeMap.get(flightLegDTO.getFlightId());
		
				JAXBElement<String> departureTime = fact.createATypeDepartureTime(sdFmt1.format(flightLegDTO.getEstDepartureTimeLocal()));   
				JAXBElement<String> arrivalAirport = fact.createATypeArrivalAirport(flightLegDTO.getDestination()); 
				JAXBElement<String> arrivalTime = fact.createATypeArrivalTime(sdFmt1.format(flightLegDTO.getEstArrivalTimeLocal())); 
				JAXBElement<Integer> dateOffset = fact.createATypeDateOffset(flightLegDTO.getEstArrivalDayOffsetLocal());  

				flightType.getA().getDepartureTimeAndArrivalAirportAndArrivalTime().add(departureTime);
				flightType.getA().getDepartureTimeAndArrivalAirportAndArrivalTime().add(arrivalAirport);
				flightType.getA().getDepartureTimeAndArrivalAirportAndArrivalTime().add(arrivalTime);
				flightType.getA().getDepartureTimeAndArrivalAirportAndArrivalTime().add(dateOffset);	
				
				LType lType = new LType();
				lType.setBoardPoint(flightLegDTO.getOrigin());
				lType.setOffPoint(flightLegDTO.getDestination());
				flightType.getLeg().getL().add(lType);	
			}
		}
		AirlineType h = new AirlineType();
		h.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		h.setCaptureDate(sdFmt.format(new Date()));
		String time = sdFmt2.format(new Date());
		h.setCaptureTime(time);
		h.setFlights(flightsType);
		aviatorInventoryFile.setH(h);
		flightLegDTOs = null;
		flightSegInvDTOs = null;
		flightSegmentDTOs = null;	
		flightBCinfoDTOs = null;
		//jaxbObjectToXML(aviatorInventoryFile);
		convertJaxbObjectToXMLZip(aviatorInventoryFile);
	}
	
	private static void jaxbObjectToXML(AviatorInventoryFile aviatorInvFile) {

		try {
			JAXBContext context = JAXBContext.newInstance(AviatorInventoryFile.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// Write to System.out for debugging
//			m.marshal(
//					new JAXBElement<AviatorInventoryFile>(new QName("uri", "local"), AviatorInventoryFile.class, aviatorInvFile),
//					System.out);

			// Write to File
			StringWriter sw = new StringWriter();
			m.marshal(
					new JAXBElement<AviatorInventoryFile>(new QName("uri", "local"), AviatorInventoryFile.class, aviatorInvFile),
					sw);
			String aviatorFileContent = sw.toString();
			//ftpFile(aviatorFileContent);
			log.info( "Aviator File Charater length : " +aviatorFileContent.length());
			//sendMail(aviatorFileContent);
			writeTextFile(aviatorFileContent);
		} catch (JAXBException e) {
			log.error("Error generating aviator file", e);
		}
	}
	
	private static void sendMail(String diu) {
		String FROM_ADDRESS = "reservations@isa.ae";
		String TO_ADDRESS = "mdhanushka@isa.ae";
		String SUBJECT = "Daily Schedule Update";
		try {
			SimpleEmailDTO emailDTO = new SimpleEmailDTO();
			emailDTO.setToEmailAddress(TO_ADDRESS);
			emailDTO.setSubject(SUBJECT);
			emailDTO.setContent(diu);
			emailDTO.setFromAddress(FROM_ADDRESS);
			messagingServiceBD.sendSimpleTextEmail(emailDTO);
		} catch (Exception me) {
			log.error("Error sending aviator email.", me);
		}
	}

	private static void writeTextFile(String diu) {

		String FILE_NAME = getAviatorAbsFilePath() + getAviatorFileName();		
		File file = new File(FILE_NAME+ ".xml");
		File zipFile = new File(FILE_NAME+ ".zip");
		try {
			FileWriter writer = new FileWriter(file);
			writer.write(diu);
			writer.close();
			zip(zipFile, file);
			sftpFile(zipFile);
		} catch (IOException e) {
			log.error("Error occurred in writing aviator file", e);
		}
	}

	private static void sftpFile(File zipFile) {

		String host = globalConfig.getAviatorFtpServerName();
		String user = "";
		String password = "";
		int port = 22;
		String remotePath = "";
		if (globalConfig.getAviatorFtpServerUserName() != null) {
			user = globalConfig.getAviatorFtpServerUserName();
		}
		if (globalConfig.getAviatorFtpServerPassword() != null) {
			password = globalConfig.getAviatorFtpServerPassword();
		}
		if (globalConfig.getAviatorFtpPort() != null) {
			port = globalConfig.getAviatorFtpPort();
		}
		if (globalConfig.getAviatorFtpRemotePath() != null) {
			remotePath = globalConfig.getAviatorFtpRemotePath();
		}
		try {
			//SFTPUtil sftpUtil = new SFTPUtil("10.20.2.16", 22, "aviator", "aviator17");
			//remotePath = "/home/aviator";
			SFTPUtil sftpUtil = new SFTPUtil(host, port, user, password);
			
			log.info("connectAndLogin for Aviator file upload -start");
			sftpUtil.connectAndLogin();
			log.info("connectAndLogin for Aviator file upload - end");
			log.info("uploadFile for Aviator file upload - start");
			sftpUtil.uploadFile(zipFile.getAbsolutePath(), remotePath + "/" + zipFile.getName());
			log.info("uploadFile for Aviator file upload - end");
			sftpUtil.closeAndLogout();
		} catch (JSchException e) {
			log.error("Error in copying aviator file to sftp location.", e);
		} catch (SftpException e) {
			log.error("Error in copying aviator file to sftp location.", e);
		}
	}
	
	private static void convertJaxbObjectToXMLZip(AviatorInventoryFile aviatorInvFile) {

		String aviatorAbsFilePath = getAviatorAbsFilePath();
		String aviatorFileName = getAviatorFileName();
		//String aviatorFileNameWithAbsPath = aviatorAbsFilePath + aviatorFileName;
		String aviatorZipFileName = aviatorFileName + ".zip";
		String aviatorZipFileNameWithAbsPath = aviatorAbsFilePath + aviatorZipFileName;

		try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(aviatorZipFileNameWithAbsPath));) {
			JAXBContext context = JAXBContext.newInstance(AviatorInventoryFile.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			ZipEntry ze = new ZipEntry(aviatorFileName + ".xml");
			zos.putNextEntry(ze);
			m.marshal(
					new JAXBElement<AviatorInventoryFile>(new QName("uri", "local"), AviatorInventoryFile.class, aviatorInvFile),
					zos);
			zos.flush();
			zos.closeEntry();
			zos.close();
			log.info("Aviator File converted to Zip xml");
			sftpAviatorFile(aviatorZipFileNameWithAbsPath, aviatorZipFileName);
		} catch (Exception e) {
			log.error("Error generating aviator file", e);
		}
	}
	
	private static void sftpAviatorFile(String absolutePath, String fileName) {

		String host = globalConfig.getAviatorFtpServerName();
		String user = "";
		String password = "";
		int port = 22;
		String remotePath = "";
		if (globalConfig.getAviatorFtpServerUserName() != null) {
			user = globalConfig.getAviatorFtpServerUserName();
		}
		if (globalConfig.getAviatorFtpServerPassword() != null) {
			password = globalConfig.getAviatorFtpServerPassword();
		}
		if (globalConfig.getAviatorFtpPort() != null) {
			port = globalConfig.getAviatorFtpPort();
		}
		if (globalConfig.getAviatorFtpRemotePath() != null) {
			remotePath = globalConfig.getAviatorFtpRemotePath();
		}
		try {
			// SFTPUtil sftpUtil = new SFTPUtil("10.20.2.16", 22, "aviator", "aviator17");
			// remotePath = "/home/aviator";
			SFTPUtil sftpUtil = new SFTPUtil(host, port, user, password);

			log.info("connectAndLogin for Aviator file upload -start");
			sftpUtil.connectAndLogin();
			log.info("connectAndLogin for Aviator file upload - end");
			log.info("uploadFile for Aviator file upload - start");
			sftpUtil.uploadFile(absolutePath, remotePath + "/" + fileName);
			log.info("uploadFile for Aviator file upload - end");
			sftpUtil.closeAndLogout();
		} catch (JSchException e) {
			log.error("Error in copying aviator file to sftp location.", e);
		} catch (SftpException e) {
			log.error("Error in copying aviator file to sftp location.", e);
		}
	}

	private static String getAviatorFileName() {
		StringBuilder localFileName = new StringBuilder();
		localFileName.append("INVW5").append("_").append(CalendarUtil.getDateInFormattedString("yyyyMMMddHHmmss", new Date()));
		return localFileName.toString();
	}
	
	private static String getAviatorAbsFilePath() {
		StringBuilder localFileName = new StringBuilder();
		localFileName.append(PlatformConstants.getAbsDIUSavePath()).append(File.separatorChar);
		return localFileName.toString();
	}
	
	public static void zip(File zip, File file) throws IOException {
		ZipOutputStream zos = null;
		try {
			
			log.info("Aviator file zipping - start");
			String name = file.getName();
			zos = new ZipOutputStream(new FileOutputStream(zip));

			ZipEntry entry = new ZipEntry(name);
			zos.putNextEntry(entry);

			FileInputStream fis = null;

			fis = new FileInputStream(file);
			byte[] byteBuffer = new byte[1024];
			int bytesRead = -1;
			while ((bytesRead = fis.read(byteBuffer)) != -1) {
				zos.write(byteBuffer, 0, bytesRead);
			}
			zos.flush();
			fis.close();

			zos.closeEntry();
			zos.close();
			log.info("Aviator file zipping - end");
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException Error creating zip file", e);
		} catch (IOException e) {
			log.error("IOExceptionError creating zip file", e);
		}
	}
	
	private static void ftpFile(String dsu, String fileName) {
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();
		byte[] byteStream = dsu.getBytes();

		// Code to FTP
		serverProperty.setServerName(globalConfig.getAviatorFtpServerName());
		if (globalConfig.getAmadeusFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(globalConfig.getAviatorFtpServerUserName());
		}
		if (globalConfig.getAmadeusFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(globalConfig.getAviatorFtpServerPassword());
		}
		ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);
	}
	
	private AviatorDAO getAviatorJDBCDAO() {
		return (AviatorDAO) ReportingUtils.getInstance().getLocalBean("aviatorDAO");
	}
}
