package com.isa.thinair.reporting.core.persistent.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;

/**
 * Extracts flight and inventory summary
 * 
 * @author MN
 */
public class RetreiveFligtsResultSetExtractor implements ResultSetExtractor {

	private boolean isCountOnly = false;

	private final FlightSearchCriteria criteria;

	public RetreiveFligtsResultSetExtractor(boolean isCountOnly, FlightSearchCriteria criteria) {
		this.isCountOnly = isCountOnly;
		this.criteria = criteria;
	}

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		if (isCountOnly) {
			rs.next();
			return new Integer(rs.getInt("rec_count"));
		}

		List<FlightInventorySummaryDTO> flightInventorySummaries = new ArrayList<FlightInventorySummaryDTO>();
		while (rs.next()) {
			int flightId = rs.getInt("flight_id");
			String fsSegmentCode = rs.getString("fs_segment_code");
			if (flightInventorySummaries.size() != 0
					&& (flightInventorySummaries.get(flightInventorySummaries.size() - 1)).getFlightId() == flightId) {
				FlightInventorySummaryDTO fltInvSummaryDTO = flightInventorySummaries.get(flightInventorySummaries.size() - 1);

				if (fltInvSummaryDTO.getLongestSegmentCode() == null
						|| (fltInvSummaryDTO.getLongestSegmentCode() != null && fsSegmentCode != null && fltInvSummaryDTO
								.getLongestSegmentCode().length() < fsSegmentCode.length())) {
					fltInvSummaryDTO.setLongestSegmentCode(fsSegmentCode);
					if (criteria.isTimeInLocal()) {
						fltInvSummaryDTO.setDepartureDateTime(rs.getTimestamp("fs_departure_local"));
						fltInvSummaryDTO.setArrivalDateTime(rs.getTimestamp("fs_arrival_local"));
					} else {
						fltInvSummaryDTO.setDepartureDateTime(rs.getTimestamp("fs_departure_zulu"));
						fltInvSummaryDTO.setArrivalDateTime(rs.getTimestamp("fs_arrival_zulu"));
					}

					fltInvSummaryDTO.setDepartureDateTimeLocal(rs.getTimestamp("fs_departure_local"));
					fltInvSummaryDTO.setArrivalDateTimeLocal(rs.getTimestamp("fs_arrival_local"));
					fltInvSummaryDTO.setDepartureDateTimeZulu(rs.getTimestamp("fs_departure_zulu"));
					fltInvSummaryDTO.setArrivalDateTimeZulu(rs.getTimestamp("fs_arrival_zulu"));
				}
			}

			if (flightInventorySummaries.size() == 0
					|| (flightInventorySummaries.get(flightInventorySummaries.size() - 1)).getFlightId() != flightId) {

				FlightInventorySummaryDTO flightInvSummaryDTO = new FlightInventorySummaryDTO();
				flightInvSummaryDTO.setFlightId(flightId);
				flightInvSummaryDTO.setCsOcCarrierCode(rs.getString("cs_oc_carrier_code"));
				flightInvSummaryDTO.setArrival(rs.getString("destination"));
				flightInvSummaryDTO.setDeparture(rs.getString("origin"));
				flightInvSummaryDTO.setFlightNumber(rs.getString("flight_number"));
				flightInvSummaryDTO.setModelNumber(rs.getString("model_number"));
				Object olFltId = rs.getObject("overlap_flight_instance_id");
				flightInvSummaryDTO.setOverlappingFlightId((olFltId != null) ? new Integer(olFltId.toString()) : null);
				flightInvSummaryDTO.setAvailableSeatKilometers(rs.getString("available_seat_kilometers"));
				Object objScheduleId = rs.getObject("schedule_id");
				flightInvSummaryDTO.setScheduleId(objScheduleId != null ? new Integer(objScheduleId.toString()) : null);

				flightInvSummaryDTO.setLongestSegmentCode(fsSegmentCode);// first segment
				if (criteria.isTimeInLocal()) {
					flightInvSummaryDTO.setDepartureDateTime(rs.getTimestamp("fs_departure_local"));
					flightInvSummaryDTO.setArrivalDateTime(rs.getTimestamp("fs_arrival_local"));
				} else {
					flightInvSummaryDTO.setDepartureDateTime(rs.getTimestamp("fs_departure_zulu"));
					flightInvSummaryDTO.setArrivalDateTime(rs.getTimestamp("fs_arrival_zulu"));
				}

				flightInvSummaryDTO.setDepartureDateTimeLocal(rs.getTimestamp("fs_departure_local"));
				flightInvSummaryDTO.setArrivalDateTimeLocal(rs.getTimestamp("fs_arrival_local"));
				flightInvSummaryDTO.setDepartureDateTimeZulu(rs.getTimestamp("fs_departure_zulu"));
				flightInvSummaryDTO.setArrivalDateTimeZulu(rs.getTimestamp("fs_arrival_zulu"));

				flightInvSummaryDTO.setFlightStatus(rs.getString("status"));
				flightInvSummaryDTO.setTailNumber(rs.getString("tail_number"));

				// flightInvSummaryDTO.setSegements(new ArrayList());

				List<FCCInventorySummaryDTO> fccInvSummaryDTOs = new ArrayList<FCCInventorySummaryDTO>();
				flightInvSummaryDTO.setFccInventorySummaryDTO(fccInvSummaryDTOs);

				flightInventorySummaries.add(flightInvSummaryDTO);
			}

			// setting segments in a flight
			Integer flightSegmentId = new Integer(rs.getInt("flt_seg_id"));
			Integer fccSegInvId = new Integer(rs.getInt("fccsa_id"));

			if (!flightInventorySummaries.get(flightInventorySummaries.size() - 1).getSegmentMap().containsKey(flightSegmentId)) {
				flightInventorySummaries.get(flightInventorySummaries.size() - 1).addSegment(flightSegmentId,
						rs.getString("fs_segment_code"));
			}

			String cabinClassCode = rs.getString("cabin_class_code");
			List<FCCInventorySummaryDTO> fccInvSummaryDTOs = flightInventorySummaries.get(flightInventorySummaries.size() - 1)
					.getFccInventorySummaryDTO();
			if (fccInvSummaryDTOs.size() == 0
					|| !fccInvSummaryDTOs.get(fccInvSummaryDTOs.size() - 1).getCabinClassCode().equals(cabinClassCode)) {
				FCCInventorySummaryDTO fccInvSummaryDTO = new FCCInventorySummaryDTO();
				fccInvSummaryDTO.setFccInvId(rs.getInt("fcca_id"));
				fccInvSummaryDTO.setCabinClassCode(cabinClassCode);
				fccInvSummaryDTO.setAdultBaseCapacity(rs.getInt("actual_capacity"));
				fccInvSummaryDTO.setInfantBaseCapacity(rs.getInt("infant_capacity"));
				int effectiveAdultBaseCapacity = rs.getInt("allocated_seats") + rs.getInt("oversell_seats")
						- rs.getInt("curtailed_seats");
				fccInvSummaryDTO.setEffectiveAdultBaseCapacity(effectiveAdultBaseCapacity);

				List<FCCSegInvSummaryDTO> fccSegInvSummaryDTOs = new ArrayList<FCCSegInvSummaryDTO>();
				fccInvSummaryDTO.setFccSegInventorySummary(fccSegInvSummaryDTOs);
				fccInvSummaryDTOs.add(fccInvSummaryDTO);
			}

			List<FCCSegInvSummaryDTO> fccSegInvSummaryDTOs = fccInvSummaryDTOs.get(fccInvSummaryDTOs.size() - 1)
					.getFccSegInventorySummary();

			if (fccSegInvSummaryDTOs.size() == 0
					|| (fccSegInvSummaryDTOs.get(fccSegInvSummaryDTOs.size() - 1).getFlightSegmentId() == flightSegmentId
							.intValue() && fccSegInvSummaryDTOs.get(fccSegInvSummaryDTOs.size() - 1).getFccsInvId() != fccSegInvId
							.intValue())
					|| fccSegInvSummaryDTOs.get(fccSegInvSummaryDTOs.size() - 1).getFlightSegmentId() != flightSegmentId
							.intValue()) {
				FCCSegInvSummaryDTO fccSegInvetorySummaryDTO = new FCCSegInvSummaryDTO();
				fccSegInvetorySummaryDTO.setFccsInvId(rs.getInt("fccsa_id"));
				fccSegInvetorySummaryDTO.setSegmentCode(rs.getString("segment_code"));
				fccSegInvetorySummaryDTO.setFlightSegmentId(flightSegmentId.intValue());
				fccSegInvetorySummaryDTO.setAdultAllocation(rs.getInt("allocated_seats"));
				fccSegInvetorySummaryDTO.setAdultSold(rs.getInt("sold_seats"));
				fccSegInvetorySummaryDTO.setAdultOnhold(rs.getInt("on_hold_seats"));
				fccSegInvetorySummaryDTO.setInfantAllocation(rs.getInt("infant_allocation"));
				fccSegInvetorySummaryDTO.setInfantSold(rs.getInt("sold_infant_seats"));
				fccSegInvetorySummaryDTO.setInfantOnhold(rs.getInt("onhold_infant_seats"));
				fccSegInvetorySummaryDTO.setAvailableSeats(rs.getInt("available_seats"));
				fccSegInvetorySummaryDTO.setAvailableInfantSeats(rs.getInt("available_infant_seats"));
				fccSegInvetorySummaryDTO.setSeatFactor(rs.getInt("seat_factor"));
				fccSegInvetorySummaryDTO.setLogicalCabinClassCode(rs.getString("logical_cabin_class_code"));
				fccSegInvSummaryDTOs.add(fccSegInvetorySummaryDTO);
			}
		}
		return flightInventorySummaries;
	}
}
