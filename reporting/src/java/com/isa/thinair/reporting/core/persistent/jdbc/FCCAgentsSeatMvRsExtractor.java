package com.isa.thinair.reporting.core.persistent.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.reporting.api.model.AgentSeatMvDTO;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;

/**
 * @author Nasly
 */
public class FCCAgentsSeatMvRsExtractor implements ResultSetExtractor {

	public static final String NON_AGENT_KEY = "NON_AGENT";
	public static final String NON_AGENT_NAME = "IBE";
	public static final String NON_AGENT_TYPE = "";
	private static boolean loadPNRList = false;

	FCCAgentsSeatMvRsExtractor(boolean loadPNRList) {
		FCCAgentsSeatMvRsExtractor.loadPNRList = loadPNRList;
	}

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		FCCAgentsSeatMvDTO fccAgentsSeatMvDTO = new FCCAgentsSeatMvDTO();
		HashMap<Integer, LinkedHashMap<String, AgentSeatMvDTO>> segmentsAgentsSeatMvs = new HashMap<Integer, LinkedHashMap<String, AgentSeatMvDTO>>();
		LinkedHashMap<String, AgentSeatMvDTO> fccAgentsSeatMvs = new LinkedHashMap<String, AgentSeatMvDTO>();

		fccAgentsSeatMvDTO.setFccAgentSeatMvDTOs(fccAgentsSeatMvs);
		fccAgentsSeatMvDTO.setFccSegAgentSeatMvDTOs(segmentsAgentsSeatMvs);

		Integer fltSegmentId = null;
		String agentCode = null;
		String agentName = null;
		String agentType = null;
		String stationCode = null;
		String sstatus = null;
		String rstatus = null;
		String pnr = null;
		int adultSeats = 0;
		int childSeats = 0;
		int infantSeats = 0;

		LinkedHashMap<String, AgentSeatMvDTO> segmentAgentsSeatMv = null;
		AgentSeatMvDTO segAgentSeatMvDTO = null;
		AgentSeatMvDTO fccAgentSeatMvDTO = null;

		while (rs.next()) {
			fltSegmentId = new Integer(rs.getInt("flt_seg_id"));

			segmentAgentsSeatMv = segmentsAgentsSeatMvs.get(fltSegmentId);
			if (segmentAgentsSeatMv == null) {
				segmentAgentsSeatMv = new LinkedHashMap<String, AgentSeatMvDTO>();
				segmentsAgentsSeatMvs.put(fltSegmentId, segmentAgentsSeatMv);
			}

			agentCode = rs.getString("agent_code");
			agentName = rs.getString("agent_name");
			agentType = rs.getString("agent_type_code");
			stationCode = rs.getString("station_code");
			if (loadPNRList) {
				pnr = rs.getString("pnr");
			}
			if (agentCode == null || agentCode.equals("")) {
				// for seats sold through ibe, agent code is empty
				agentCode = NON_AGENT_KEY;
				agentName = NON_AGENT_NAME;
				agentType = NON_AGENT_TYPE;
			}

			// fcc
			fccAgentSeatMvDTO = (AgentSeatMvDTO) fccAgentsSeatMvs.get(agentCode);
			if (fccAgentSeatMvDTO == null) {
				fccAgentSeatMvDTO = new AgentSeatMvDTO();
				fccAgentsSeatMvs.put(agentCode, fccAgentSeatMvDTO);
				fccAgentSeatMvDTO.setAgentCode(agentCode);
				fccAgentSeatMvDTO.setAgentName(agentName);
				fccAgentSeatMvDTO.setAgentTypeCode(agentType);
				fccAgentSeatMvDTO.setStationCode(stationCode);

			}

			// fcc seg
			segAgentSeatMvDTO = (AgentSeatMvDTO) segmentAgentsSeatMv.get(agentCode);
			if (segAgentSeatMvDTO == null) {
				segAgentSeatMvDTO = new AgentSeatMvDTO();
				segmentAgentsSeatMv.put(agentCode, segAgentSeatMvDTO);
				segAgentSeatMvDTO.setAgentCode(agentCode);
				segAgentSeatMvDTO.setAgentName(agentName);
				segAgentSeatMvDTO.setAgentTypeCode(agentType);
				segAgentSeatMvDTO.setStationCode(stationCode);
				fccAgentSeatMvDTO.setStationCode(stationCode);

			}

			sstatus = rs.getString("sstatus");
			rstatus = rs.getString("rstatus");
			adultSeats = rs.getInt("adult_seats");
			childSeats = rs.getInt("child_seats");
			infantSeats = rs.getInt("infant_seats");

			if (rstatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)
					&& sstatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				// fcc
				fccAgentSeatMvDTO.addSoldSeats(adultSeats);
				fccAgentSeatMvDTO.addSoldChildSeats(childSeats);
				fccAgentSeatMvDTO.addSoldInfantSeats(infantSeats);
				fccAgentSeatMvDTO.getConfirmedPNRs().add(pnr);
				// fcc seg
				segAgentSeatMvDTO.addSoldSeats(adultSeats);
				segAgentSeatMvDTO.addSoldChildSeats(childSeats);
				segAgentSeatMvDTO.addSoldInfantSeats(infantSeats);
				segAgentSeatMvDTO.getConfirmedPNRs().add(pnr);

			} else if (rstatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					&& sstatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				// fcc
				fccAgentSeatMvDTO.addOnholdSeats(adultSeats);
				fccAgentSeatMvDTO.addOnHoldChildSeats(childSeats);
				fccAgentSeatMvDTO.addOnholdInfantSeats(infantSeats);
				fccAgentSeatMvDTO.getOnHoldPNRs().add(pnr);
				// fcc seg
				segAgentSeatMvDTO.addOnholdSeats(adultSeats);
				segAgentSeatMvDTO.addOnHoldChildSeats(childSeats);
				segAgentSeatMvDTO.addOnholdInfantSeats(infantSeats);
				segAgentSeatMvDTO.getOnHoldPNRs().add(pnr);
			} else if (sstatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				// fcc
				fccAgentSeatMvDTO.addCancelledSeats(adultSeats);
				fccAgentSeatMvDTO.addCancelledChildSeats(childSeats);
				fccAgentSeatMvDTO.addCancelledInfantSeats(infantSeats);
				fccAgentSeatMvDTO.getCancelledPNRs().add(pnr);
				// fcc seg
				segAgentSeatMvDTO.addCancelledSeats(adultSeats);
				segAgentSeatMvDTO.addCancelledChildSeats(childSeats);
				segAgentSeatMvDTO.addCancelledInfantSeats(infantSeats);
				segAgentSeatMvDTO.getCancelledPNRs().add(pnr);
			}
		}
		return fccAgentsSeatMvDTO;
	}
}
