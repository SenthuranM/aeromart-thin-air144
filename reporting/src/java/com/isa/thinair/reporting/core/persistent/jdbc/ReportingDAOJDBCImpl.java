package com.isa.thinair.reporting.core.persistent.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.dto.AgentUserRolesDTO;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.reporting.core.persistent.dao.ReportingDAOJDBC;

/**
 * JDBC Data retrievals
 * 
 * @author Duminda
 */
public class ReportingDAOJDBCImpl extends PlatformBaseJdbcDAOSupport implements ReportingDAOJDBC {

	@Override
	public Page<FlightInventorySummaryDTO> getFlightsForInventoryUpdating(FlightSearchCriteria criteria, int startIndex,
			int pageSize) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String flightStatusSQL = "";
		if (criteria.getFlightStatuses() != null && criteria.getFlightStatuses().size() > 0) {
			flightStatusSQL = " AND f.status IN (" + getSqlInValuesStr(criteria.getFlightStatuses()) + ") ";
		}

		String localZuluStr = "zulu";
		if (criteria.isTimeInLocal()) {
			localZuluStr = "local";
		}

		if (criteria.getApplicapableCarrierCodes() != null && criteria.getApplicapableCarrierCodes().size() > 0) {
			flightStatusSQL += " AND substr(f.flight_number,0,2) IN ("
					+ Util.constructINStringForCharactors(criteria.getApplicapableCarrierCodes()) + " ) ";
		}

		String flightNumbersSQL = "";
		if (criteria.getFlightNumbers() != null && criteria.getFlightNumbers().size() > 0) {
			flightNumbersSQL = " AND f.flight_number IN (" + getSqlInValuesStr(criteria.getFlightNumbers()) + ") ";
		}
		int[] arrSeatFactor = criteria.getSeatFactor();
		String flightNumberWithseatfactorSql = "";
		if (arrSeatFactor != null) {
			flightNumberWithseatfactorSql = " AND round(((fccsa.sold_seats+fccsa.on_hold_seats)/fccsa.allocated_seats)*100,0) between "
					+ arrSeatFactor[0] + " and " + arrSeatFactor[1] + " ";
		}
		flightNumberWithseatfactorSql += flightNumbersSQL;

		String flightDayNums = "";
		if (criteria.getDayNumber() != null) {
			if (criteria.isTimeInLocal()) {
				flightDayNums = " AND F_GET_DAY_NUMBR_LOCAL_DEP_DATE(f.flight_id) IN (" + criteria.getDayNumber() + ") ";
			} else {
				flightDayNums = "AND f.day_number in (" + criteria.getDayNumber() + ")";
			}
		}

		@SuppressWarnings("unchecked")
		Collection<FlightInventorySummaryDTO> data = (Collection<FlightInventorySummaryDTO>) jdbcTemplate.query(
				new RetreiveFlightsPrepareStmtCreator(criteria, startIndex, pageSize, false, getQuery(
						"FLIGHTS_SEARCH_FOR_INV_MGMT", new String[] { flightDayNums, localZuluStr, flightStatusSQL,
								flightNumberWithseatfactorSql })), new RetreiveFligtsResultSetExtractor(false, criteria));
		int totalRecCount = ((Integer) jdbcTemplate.query(
				new RetreiveFlightsPrepareStmtCreator(criteria, startIndex, pageSize, true, getQuery(
						"FLIGHTS_SEARCH_FOR_INV_MGMT_COUNT", new String[] { flightDayNums, localZuluStr, flightStatusSQL,
								flightNumbersSQL })), new RetreiveFligtsResultSetExtractor(true, criteria))).intValue();

		return new Page<FlightInventorySummaryDTO>(totalRecCount, startIndex, pageSize, data);
	}

	@Override
	public FCCAgentsSeatMvDTO getFCCAgentsSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] params = { new Integer(flightId), cabinClassCode };
		String enableSqlForPnrList = " ";
		if (loadPNRList) {
			enableSqlForPnrList = ",t.pnr";
		}

		return (FCCAgentsSeatMvDTO) jdbcTemplate.query(
				getQuery("FCC_SEG_AGENTWISE_SEAT_MOVEMENT_INFO", new String[] { enableSqlForPnrList, enableSqlForPnrList }),
				params, new FCCAgentsSeatMvRsExtractor(loadPNRList));
	}

	@Override
	public FCCAgentsSeatMvDTO
			getFCCAgentsSeatMovementSummaryForLogicalCC(int flightId, String logicalCCCode, boolean loadPNRList) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] params = { new Integer(flightId), logicalCCCode };
		String enableSqlForPnrList = " ";
		if (loadPNRList) {
			enableSqlForPnrList = ",t.pnr";
		}

		return (FCCAgentsSeatMvDTO) jdbcTemplate.query(
				getQuery("FCC_SEG_AGENTWISE_SEAT_MOVEMENT_INFO_FOR_LOGICAL_CC", new String[] { enableSqlForPnrList,
						enableSqlForPnrList }), params, new FCCAgentsSeatMvRsExtractor(loadPNRList));
	}

	@Override
	public FCCAgentsSeatMvDTO getOwnerAgentWiseFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] params = { new Integer(flightId), cabinClassCode };
		String enableSqlForPnrList = " ";
		if (loadPNRList) {
			enableSqlForPnrList = ",t.pnr";
		}

		return (FCCAgentsSeatMvDTO) jdbcTemplate
				.query(getQuery("FCC_SEG_OWNER_AGENTWISE_SEAT_MOVEMENT_INFO", new String[] { enableSqlForPnrList,
						enableSqlForPnrList }), params, new FCCAgentsSeatMvRsExtractor(loadPNRList));
	}

	@Override
	public Page<String> getFlightsForFlightLoadAnalysis(FlightSearchCriteria criteria, int startIndex, int pageSize) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String flightStatusSQL = "";
		if (criteria.getFlightStatuses() != null && criteria.getFlightStatuses().size() > 0) {
			flightStatusSQL = " AND f.status IN (" + getSqlInValuesStr(criteria.getFlightStatuses()) + ") ";
		}

		String localZuluStr = "zulu";
		if (criteria.isTimeInLocal()) {
			localZuluStr = "local";
		}

		if (criteria.getApplicapableCarrierCodes() != null && criteria.getApplicapableCarrierCodes().size() > 0) {
			flightStatusSQL += " AND substr(f.flight_number,0,2) IN ("
					+ Util.constructINStringForCharactors(criteria.getApplicapableCarrierCodes()) + " ) ";
		}

		String flightNumbersSQL = "";
		if (criteria.getFlightNumbers() != null && criteria.getFlightNumbers().size() > 0) {
			flightNumbersSQL = " AND f.flight_number IN (" + getSqlInValuesStr(criteria.getFlightNumbers()) + ") ";
		}

		@SuppressWarnings("unchecked")
		Collection<String> data = (Collection<String>) jdbcTemplate.query(
				new RetreiveFlightsPrepareStmtCreator(criteria, startIndex, pageSize, false, getQuery("FLIGHTS_SEARCH_FOR_FLA",
						new String[] { localZuluStr, flightStatusSQL, flightNumbersSQL })), new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<String> flightNumbers = new ArrayList<String>();
						if (rs != null) {
							while (rs.next()) {
								String flightNo = PlatformUtiltiies.nullHandler(rs.getString("flight_number"));
								flightNumbers.add(flightNo);
							}
						}
						return flightNumbers;
					}
				});
		int totalRecCount = ((Integer) jdbcTemplate.query(
				new RetreiveFlightsPrepareStmtCreator(criteria, startIndex, pageSize, true, getQuery(
						"FLIGHTS_SEARCH_FOR_FLA_COUNT", new String[] { localZuluStr, flightStatusSQL, flightNumbersSQL })),
				new RetreiveFligtsResultSetExtractor(true, criteria))).intValue();

		return new Page<String>(totalRecCount, startIndex, pageSize, data);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Page getRolesForPrivilege(AgentUserRolesDTO agentUserRolesDTO, int startIndex, int pageLength) {

		// Selecting data for a page
		JdbcTemplate jt = new JdbcTemplate(getDataSource());

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM (SELECT DISTINCT rownum seq,  x.* FROM ( ");
		sb.append(" SELECT DISTINCT rl.role_name , rp.privilege_id ");
		sb.append(" FROM t_role_privilege rp , t_role rl");
		sb.append(" WHERE ( ");

		String privilege = agentUserRolesDTO.getPrivilege();

		if (!privilege.isEmpty()) {
			sb.append(" rp.privilege_id IN('" + privilege + "')) ");
		}
		sb.append(" and  rp.role_id=rl.role_id  ORDER BY rl.role_name  ");
		sb.append("  ) x  )  WHERE seq BETWEEN " + (startIndex + 1) + " AND " + (startIndex + pageLength));

		@SuppressWarnings("unchecked")
		Collection<AgentUserRolesDTO> rolesForPrivilege = (Collection<AgentUserRolesDTO>) jt.query(sb.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<AgentUserRolesDTO> roles = new ArrayList<AgentUserRolesDTO>();

						if (rs != null) {
							while (rs.next()) {
								AgentUserRolesDTO agentUserRolesDTO = new AgentUserRolesDTO();
								agentUserRolesDTO.setRole(rs.getString("ROLE_NAME"));
								agentUserRolesDTO.setPrivilege(rs.getString("PRIVILEGE_ID"));
								roles.add(agentUserRolesDTO);
							}
						}
						return roles;
					}
				});

		// Total count of data
		JdbcTemplate jtCount = new JdbcTemplate(getDataSource());

		StringBuilder sbCount = new StringBuilder();

		sbCount.append(" SELECT DISTINCT COUNT( rl.role_name ) AS REC_COUNT");
		sbCount.append(" FROM t_role_privilege rp , t_role rl");
		sbCount.append(" WHERE ( rp.privilege_id IN('" + privilege + "'))");
		sbCount.append(" AND rp.role_id = rl.role_id");
		sbCount.append(" ORDER BY rl.role_name");

		Integer resultCount = (Integer) jtCount.query(sbCount.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int resultCount = 0;
				while (rs.next()) {
					resultCount = rs.getInt("REC_COUNT");

				}
				return resultCount;
			}
		});

		int totalRecCount = resultCount;
		return new Page(totalRecCount, startIndex, startIndex + pageLength, rolesForPrivilege);
	}

}
