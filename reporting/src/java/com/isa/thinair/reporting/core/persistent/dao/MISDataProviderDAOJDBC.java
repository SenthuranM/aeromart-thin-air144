package com.isa.thinair.reporting.core.persistent.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.mis.OutstandingInvoiceInfoTO;
import com.isa.thinair.reporting.api.model.AgentPerformanceTO;
import com.isa.thinair.reporting.api.model.BookingsTO;
import com.isa.thinair.reporting.api.model.DistributionFlightInfoTO;
import com.isa.thinair.reporting.api.model.MISAccountSummaryTO;
import com.isa.thinair.reporting.api.model.MISOutstandingInvoicesTO;
import com.isa.thinair.reporting.api.model.SalesRevenueTO;

public interface MISDataProviderDAOJDBC {

	public long getMISDashboardGoal(MISReportsSearchCriteria searchCriteria);

	public long getMISYtdBookings(MISReportsSearchCriteria searchCriteria);

	public Map<String, BigDecimal> getMISDashSalesByChannel(MISReportsSearchCriteria searchCriteria);

	public Map<String, Integer> getMISDashSalesByChannelPaxCount(MISReportsSearchCriteria searchCriteria);

	public Collection<SalesRevenueTO> getMISDashYearlySales(MISReportsSearchCriteria searchCriteria);

	public Collection<BigDecimal> getMISDashRevenueBreakdown(MISReportsSearchCriteria searchCriteria);

	public Collection<DistributionFlightInfoTO> getMISDistCountryWiseFlights(MISReportsSearchCriteria searchCriteria);

	public Map<String, Collection<BookingsTO>> getMISDistChannelwiseSalesForCountry(MISReportsSearchCriteria searchCriteria);

	public Map<String, BigDecimal> getMISDistRevenueBreakDownChannelwise(MISReportsSearchCriteria searchCriteria);

	public Collection getMISDistPerformanceOfPortals(MISReportsSearchCriteria searchCriteria);

	public Map<String, Long> getMisRegionYearlySales(MISReportsSearchCriteria searchCriteria);

	public Map<String, Map<String, String>> getMISRegionRollingSalesTrend(MISReportsSearchCriteria searchCriteria);

	public Long getMISRegionalAvgAgentSales(MISReportsSearchCriteria searchCriteria);

	public Collection<AgentPerformanceTO> getBestPerformingAgent(MISReportsSearchCriteria searchCriteria);

	public Collection<AgentPerformanceTO> getLowPerformingAgent(MISReportsSearchCriteria searchCriteria);

	public Collection<AgentPerformanceTO> getRegionalProductSalesData(MISReportsSearchCriteria searchCriteria);

	public Map<String, BigDecimal> getAncillarySalesSummary(MISReportsSearchCriteria searchCriteria);

	public Map<String, Map<String, String>> getAncillarySalesByRoute(MISReportsSearchCriteria searchCriteria);

	public Map<String, Map<String, BigDecimal>> getAncillarySalesBychannel(MISReportsSearchCriteria searchCriteria);

	public Collection<MISAccountSummaryTO> getMISAccountSales(MISReportsSearchCriteria searchCriteria);

	public Collection<MISAccountSummaryTO> getMISAccountRefunds(MISReportsSearchCriteria searchCriteria);

	public Collection<MISOutstandingInvoicesTO> getMISAccountOutstandingInvoices(MISReportsSearchCriteria searchCriteria);

	public Map<String, BigDecimal> getMISOutstandingByRegion(MISReportsSearchCriteria searchCriteria);

	public BigDecimal getMISAccountTotalOutstandingCredits(MISReportsSearchCriteria searchCriteria);

	public HashMap<String, BigDecimal> getGSATurnover(MISReportsSearchCriteria searchCriteria, List<String> agentCodes);

	public Collection<OutstandingInvoiceInfoTO> getOutstandingInvoiceInfo(MISReportsSearchCriteria searchCriteria,
			String agentCode);

	public Integer getNoOfEmployees(String agentCode);

	public Collection<HashMap<String, String>>
			getRouteWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode);

	public Collection<HashMap<String, String>>
			getStaffWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode);

	public Map<String, Map<String, Map<String, BigDecimal>>> getMISSeatFactorReportData(
			ReportsSearchCriteria reportsSearchCriteria);

	public Map<String, List<Map<String, List<BigDecimal>>>> getYieldTrendAnalysisReport(
			ReportsSearchCriteria reportsSearchCriteria);
}
