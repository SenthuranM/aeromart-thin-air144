package com.isa.thinair.reporting.core.persistent.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class MISStatementCreator {

	/** ------------------------------ Agents related sql statements ----------------------------------- */
	public static String getNoOfEmployeesForAgentQuery(String agentCode) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(*) ");
		sql.append("FROM t_user u ");
		sql.append("WHERE u.agent_code='");
		sql.append(agentCode);
		sql.append("' and u.status='ACT' and u.service_channel_code in ('AA', 'AH')");

		return sql.toString();
	}

	/** ---------------------------- Ancillary related sql statements ---------------------------------- */

	/**
	 * Query for ancillary sales data as a count
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getAncillarySummaryDataByCount(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT SUM (a.meal_count) AS meal, SUM (a.seat_count) AS seat, "
				+ "SUM (a.insurance_count) AS ins, SUM (a.hala_count) AS hala "
				+ "FROM t_mis_ancillary_collection a, t_station st, t_country cn, t_region rg "
				+ "WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00',  'dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND a.station_code = st.station_code "
				+ "AND st.country_code = cn.country_code " + "AND cn.region_code = rg.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		return sql.toString();
	}

	/**
	 * Query for ancillary sales data as sales amounts
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getAncillarySummaryDataBySales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT SUM (a.meal_collection) AS meal, SUM (a.seat_collection) AS seat, "
				+ "SUM (a.insurance_collection) AS ins, SUM (a.hala_collection) AS hala "
				+ "FROM t_mis_ancillary_collection a, t_station st, t_country cn, t_region rg "
				+ "WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59','dd/mm/yyyy HH24:mi:ss' ) " + "AND a.station_code = st.station_code "
				+ "AND st.country_code = cn.country_code " + " AND cn.region_code = rg.region_code ");

		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}

		return sql.toString();
	}

	/**
	 * Query for ancillary sales data by route as sales amount
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getAncillarySummaryDataForRouteBySales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT   SUM (a.meal_collection) AS meal, SUM (a.seat_collection) AS seat, "
				+ "SUM (a.insurance_collection) AS ins, SUM (a.hala_collection) AS hala, " + "a.segment_code "
				+ "FROM t_mis_ancillary_collection a, " + "t_station st, " + "t_country cn, " + "t_region rg "
				+ "WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59', 'dd/mm/yyyy HH24:mi:ss') " + "AND a.station_code = st.station_code "
				+ "AND st.country_code = cn.country_code " + "AND cn.region_code = rg.region_code ");

		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}

		sql.append("GROUP BY a.segment_code");
		return sql.toString();
	}

	/**
	 * Query for ancillary sales data by route as a count
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getAncillarySummaryDataForRouteByCount(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT   SUM (a.meal_count) AS meal, SUM (a.seat_count) AS seat, "
				+ "SUM (a.insurance_count) AS ins, SUM (a.hala_count) AS hala, " + "a.segment_code "
				+ "FROM t_mis_ancillary_collection a, " + "t_station st, " + "t_country cn, " + "t_region rg "
				+ "WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND a.station_code = st.station_code "
				+ "AND st.country_code = cn.country_code " + "AND cn.region_code = rg.region_code ");

		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}

		sql.append("GROUP BY a.segment_code");
		return sql.toString();
	}

	public static String getAncillaryChannelWiseSales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT SUM (a.meal_collection) AS meal, SUM (a.seat_collection) AS seat, "
				+ "SUM (a.insurance_collection) AS ins, SUM (a.hala_collection) AS hala, ch.description as channel "
				+ "FROM t_mis_ancillary_collection a,t_sales_channel ch, t_station st, t_country cn, t_region rg "
				+ "WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss' ) " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59','dd/mm/yyyy HH24:mi:ss' ) " + "   AND a.sales_channel_code = ch.sales_channel_code "
				+ " AND a.station_code = st.station_code " + " AND st.country_code = cn.country_code "
				+ "  AND cn.region_code = rg.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		sql.append("GROUP BY ch.description");

		return sql.toString();
	}

	public static String getAncillaryChannelWiseSalesCount(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("    SELECT SUM (a.meal_count) AS meal, SUM (a.seat_count) AS seat, "
				+ "SUM (a.insurance_count) AS ins, SUM (a.hala_count) AS hala, ch.description as channel "
				+ " FROM t_mis_ancillary_collection a,t_sales_channel ch, t_station st, t_country cn, t_region rg "
				+ " WHERE a.created_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND a.sales_channel_code = ch.sales_channel_code "
				+ "AND a.station_code = st.station_code " + "AND st.country_code = cn.country_code "
				+ "AND cn.region_code = rg.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append(" AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		sql.append("GROUP BY ch.description");

		return sql.toString();
	}

	public static String getRouteWiseAgentRevenueQuery(MISReportsSearchCriteria searchCriteria, String agentCode) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM (");
		sql.append("	SELECT  c.segment_code, SUM(f.amount) AS amount ");
		sql.append(" 		FROM t_reservation a, ");
		sql.append("            t_pnr_segment b, ");
		sql.append("            t_flight_segment c, ");
		sql.append("            t_pnr_pax_fare_segment d, ");
		sql.append("            t_pnr_pax_ond_charges e, ");
		sql.append("            t_pnr_pax_seg_charges f, ");
		sql.append("            t_pnr_passenger g, ");
		sql.append("            t_pnr_pax_fare h, ");
		sql.append("            t_flight i ");
		sql.append("        WHERE a.pnr = b.pnr ");
		sql.append("        AND a.pnr = g.pnr ");
		sql.append("        AND b.flt_seg_id = c.flt_seg_id ");
		sql.append("        AND d.pnr_seg_id = b.pnr_seg_id ");
		sql.append("        AND h.ppf_id = e.ppf_id ");
		sql.append("        AND f.pft_id = e.pft_id ");
		sql.append("        AND f.ppfs_id = d.ppfs_id ");
		sql.append("        AND g.pnr_pax_id = h.pnr_pax_id ");
		sql.append("        AND h.ppf_id = d.ppf_id ");
		sql.append("        AND c.flight_id = i.flight_id ");
		sql.append("        AND a.origin_agent_code = '" + agentCode + "' ");
		sql.append("        AND e.charge_date between TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sql.append(" 					      and     TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59', 'dd/mm/yyyy hh24:mi:ss') ");
		sql.append("        GROUP BY c.segment_code ) ORDER BY amount DESC ");

		return sql.toString();
	}

	public static String getStaffWiseAgentRevenueQuery(MISReportsSearchCriteria searchCriteria, String agentCode) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM (");
		sql.append("	SELECT  u.display_name, SUM(f.amount) AS amount ");
		sql.append(" 		FROM t_reservation a, ");
		sql.append("            t_pnr_segment b, ");
		sql.append("            t_flight_segment c, ");
		sql.append("            t_pnr_pax_fare_segment d, ");
		sql.append("            t_pnr_pax_ond_charges e, ");
		sql.append("            t_pnr_pax_seg_charges f, ");
		sql.append("            t_pnr_passenger g, ");
		sql.append("            t_pnr_pax_fare h, ");
		sql.append("            t_flight i, ");
		sql.append("            t_user u ");
		sql.append("        WHERE a.pnr = b.pnr ");
		sql.append("        AND a.pnr = g.pnr ");
		sql.append("        AND b.flt_seg_id = c.flt_seg_id ");
		sql.append("        AND d.pnr_seg_id = b.pnr_seg_id ");
		sql.append("        AND h.ppf_id = e.ppf_id ");
		sql.append("        AND f.pft_id = e.pft_id ");
		sql.append("        AND f.ppfs_id = d.ppfs_id ");
		sql.append("        AND g.pnr_pax_id = h.pnr_pax_id ");
		sql.append("        AND h.ppf_id = d.ppf_id ");
		sql.append("        AND c.flight_id = i.flight_id ");
		sql.append("        AND a.origin_user_id = u.user_id ");
		sql.append("        AND a.origin_agent_code = '" + agentCode + "' ");
		sql.append("        AND e.charge_date between TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') ");
		sql.append(" 					      and     TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59', 'dd/mm/yyyy hh24:mi:ss') ");
		sql.append("        GROUP BY u.display_name ) ORDER BY amount DESC ");

		return sql.toString();
	}

	/**
	 * Query for MIS dash board sales by channel (pax count) graph
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISDashSalesByChannelPaxCount(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		if (searchCriteria.getRegion() == null && searchCriteria.getPos() == null) {
			sql.append("SELECT   SUM (r.total_pax_count + r.total_pax_child_count) AS sales, " + " ch.description  as channel "
					+ "FROM t_reservation r, t_pnr_segment ps, t_sales_channel ch " + "WHERE ps.status = 'CNF' "
					+ "AND r.status = 'CNF' " + "AND r.pnr = ps.pnr " + "AND r.origin_channel_code = ch.sales_channel_code "
					+ " AND ch.IS_BOOKINGS_ORIGINATED = '" + SalesChannelsUtil.IS_BOOKING_ORIGINATED_FLAG + "' "
					+ "AND ps.status_mod_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate() + "', 'dd/mm/yyyy') "
					+ "    AND TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') "
					+ "GROUP BY ch.description ");
		} else {
			if (searchCriteria.getPos() != null) {
				sql.append("SELECT   SUM (r.total_pax_count + r.total_pax_child_count) AS sales,ch.description  as channel "
						+ "FROM t_reservation r, t_pnr_segment ps, t_sales_channel ch " + "WHERE ps.status = 'CNF' "
						+ " AND r.status = 'CNF' " + "   AND r.pnr = ps.pnr " + "AND r.pnr IN (SELECT pnr "
						+ "   FROM t_reservation res " + "  WHERE res.origin_pos IN ('" + searchCriteria.getPos() + "')) "
						+ " AND r.origin_channel_code = ch.sales_channel_code " + " AND ch.IS_BOOKINGS_ORIGINATED = '"
						+ SalesChannelsUtil.IS_BOOKING_ORIGINATED_FLAG + "' " + "AND ps.status_mod_date BETWEEN TO_DATE ('"
						+ searchCriteria.getFromDate() + "', 'dd/mm/yyyy') " + "    AND TO_DATE ('" + searchCriteria.getToDate()
						+ " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "GROUP BY ch.description ");

			} else {
				if (searchCriteria != null && searchCriteria.equals("NET")) {
					sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) as sales, ch.description as channel "
							+ "FROM t_reservation r, t_pnr_segment ps, t_sales_channel ch " + "WHERE ps.status = 'CNF' "
							+ " AND r.status = 'CNF' " + " AND r.pnr = ps.pnr " + " AND ch.IS_BOOKINGS_ORIGINATED = '"
							+ SalesChannelsUtil.IS_BOOKING_ORIGINATED_FLAG + "' " + "AND ps.status_mod_date BETWEEN TO_DATE ('"
							+ searchCriteria.getFromDate() + "', 'dd/mm/yyyy') " + "    AND TO_DATE ('"
							+ searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND r.origin_pos = 'NET' "
							+ "AND r.origin_channel_code = ch.sales_channel_code " + " GROUP BY ch.description");
				} else {
					sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) as sales, ch.description as channel "
							+ "FROM t_reservation r, t_pnr_segment ps, t_sales_channel ch " + "WHERE ps.status = 'CNF' "
							+ " AND r.status = 'CNF' " + " AND r.pnr = ps.pnr " + " AND ch.IS_BOOKINGS_ORIGINATED = '"
							+ SalesChannelsUtil.IS_BOOKING_ORIGINATED_FLAG + "' " + "AND ps.status_mod_date BETWEEN TO_DATE ('"
							+ searchCriteria.getFromDate() + "', 'dd/mm/yyyy') " + "    AND TO_DATE ('"
							+ searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND r.pnr IN ( "
							+ "       SELECT pnr " + "       FROM t_reservation res " + "      WHERE res.origin_pos IN ( "
							+ "            SELECT st.station_code " + "            FROM t_station st, t_country cn "
							+ "          WHERE st.country_code = cn.country_code " + "           AND cn.region_code = '"
							+ searchCriteria.getRegion() + "')) " + " AND r.origin_channel_code = ch.sales_channel_code "
							+ " GROUP BY ch.description");
				}
			}
		}
		return sql.toString();
	}

	/**
	 * SQL to retrieve revenue breakdown
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISDashRevBreakdownSQL(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		if (searchCriteria.getRegion() == null && searchCriteria.getPos() == null) {
			sql.append("SELECT sum(su.far_amt) + sum(su.tax_amt) + sum(su.cnx_amt) + sum(su.mod_amt)+ "
					+ "sum(su.adj_amt) + sum(su.sur_amt) AS revenue, sum(su.far_amt) AS fare, "
					+ "sum(su.tax_amt) AS tax, sum(su.cnx_amt) + sum(su.mod_amt) + sum(su.adj_amt) "
					+ "+ sum(su.sur_amt) AS surcharges " + "FROM t_mis_txn_breakdown_summary su "
					+ "WHERE su.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
					+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss' ) " + "AND TO_DATE ('" + searchCriteria.getToDate()
					+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
		} else {
			if (searchCriteria.getPos() != null) {
				sql.append("SELECT sum(su.far_amt) + sum(su.tax_amt) + sum(su.cnx_amt) + sum(su.mod_amt) + "
						+ "sum(su.adj_amt) + sum(su.sur_amt) AS revenue, sum(su.far_amt) AS fare, "
						+ "sum(su.tax_amt) AS tax, sum(su.cnx_amt) + sum(su.mod_amt) + sum(su.adj_amt) + "
						+ "sum(su.sur_amt) AS surcharges " + "FROM t_mis_txn_breakdown_summary su "
						+ "WHERE su.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
						+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss' ) " + "AND TO_DATE ('" + searchCriteria.getToDate()
						+ "23:59:59','dd/mm/yyyy hh24:mi:ss') "
						+ "and pnr_pax_id in(select pnr_pax_id from t_pnr_passenger where pnr in "
						+ "(select pnr from t_reservation where origin_pos='" + searchCriteria.getPos() + "')) ");

			} else {
				if (searchCriteria.getRegion() != null && searchCriteria.getRegion().equals("NET")) {
					sql.append("SELECT sum(su.far_amt) + sum(su.tax_amt) + sum(su.cnx_amt) + sum(su.mod_amt) + "
							+ "sum(su.adj_amt) + sum(su.sur_amt) AS revenue, sum(su.far_amt) AS fare, "
							+ "sum(su.tax_amt) AS tax, sum(su.cnx_amt) + sum(su.mod_amt) + sum( "
							+ "su.adj_amt) + sum(su.sur_amt) AS surcharges " + "FROM t_mis_txn_breakdown_summary su "
							+ "WHERE su.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
							+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss' ) " + "AND TO_DATE ('" + searchCriteria.getToDate()
							+ "23:59:59','dd/mm/yyyy hh24:mi:ss') "
							+ "AND pnr_pax_id in(select pnr_pax_id from t_pnr_passenger where pnr in "
							+ "(select pnr from t_reservation where origin_pos = 'NET' ) )");
				} else {
					sql.append("SELECT sum(su.far_amt) + sum(su.tax_amt) + sum(su.cnx_amt) + sum(su.mod_amt) + "
							+ "sum(su.adj_amt) + sum(su.sur_amt) AS revenue, sum(su.far_amt) AS fare, "
							+ "sum(su.tax_amt) AS tax, sum(su.cnx_amt) + sum(su.mod_amt) + sum( "
							+ "su.adj_amt) + sum(su.sur_amt) AS surcharges " + "FROM t_mis_txn_breakdown_summary su "
							+ "WHERE su.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
							+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss' ) " + "AND TO_DATE ('" + searchCriteria.getToDate()
							+ "23:59:59','dd/mm/yyyy hh24:mi:ss') "
							+ "AND pnr_pax_id in(select pnr_pax_id from t_pnr_passenger where pnr in "
							+ "(select pnr from t_reservation where origin_pos in "
							+ "(select st.station_code from t_station st, t_country cn where st.country_code=cn.country_code "
							+ "AND cn.region_code ='" + searchCriteria.getRegion() + "' ))) ");
				}

			}

		}

		return sql.toString();
	}

	/**
	 * SQL to retrieve revenue breakdown monthly
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISDashRevenueByMonth(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		Calendar cal = GregorianCalendar.getInstance();
		boolean exceedsSixMons = false;
		try {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date toDate = df.parse(searchCriteria.getToDate() + " 23:59:59");
			Date fromDate = df.parse(searchCriteria.getFromDate() + " 00:00:00");
			cal.setTime(toDate);
			cal.add(GregorianCalendar.MONTH, -6);
			if (cal.getTime().compareTo(fromDate) > 0) {
				exceedsSixMons = true;
			}
		} catch (Exception e) {

		}

		if (searchCriteria.getRegion() == null && searchCriteria.getPos() == null) {
			sql.append("SELECT   SUM (su.far_amt) AS fare, "
					+ " SUM (su.sur_amt)+ SUM (su.cnx_amt)+ SUM (su.adj_amt)+SUM (su.mod_amt)  AS charges, "
					+ "TO_CHAR (tx.tnx_date, 'mm') AS MONTH, " + "TO_CHAR (tx.tnx_date, 'yyyy') AS YEAR "
					+ "FROM t_pax_transaction tx, " + "t_pnr_passenger pax, " + "t_reservation res, "
					+ "t_mis_txn_breakdown_summary su " + "WHERE res.pnr = pax.pnr " +
					// "AND res.status = 'CNF' "+
					"AND pax.pnr_pax_id = tx.pnr_pax_id " + "AND tx.txn_id = su.pax_txn_id ");
			if (exceedsSixMons) {
				sql.append("AND tx.tnx_date BETWEEN ADD_MONTHS( TO_DATE ('" + searchCriteria.getToDate()
						+ "23:59:59', 'dd/mm/yyyy hh24:mi:ss'),-5) " + "AND TO_DATE ('" + searchCriteria.getToDate()
						+ "23:59:59','dd/mm/yyyy hh24:mi:ss') ");
			} else {
				sql.append("AND tx.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
						+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
						+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
			}
			sql.append("GROUP BY TO_CHAR (tx.tnx_date, 'mm'),TO_CHAR (tx.tnx_date, 'yyyy') " + "ORDER BY YEAR, MONTH");
		} else {
			if (searchCriteria.getPos() != null) {
				sql.append("SELECT   SUM (su.far_amt) AS fare, "
						+ " SUM (su.sur_amt)+ SUM (su.cnx_amt)+ SUM (su.adj_amt)+SUM (su.mod_amt)  AS charges, "
						+ "TO_CHAR (tx.tnx_date, 'mm') AS MONTH, " + "TO_CHAR (tx.tnx_date, 'yyyy') AS YEAR "
						+ "FROM t_pax_transaction tx, " + "t_pnr_passenger pax, " + "t_reservation res, "
						+ "t_mis_txn_breakdown_summary su " + "WHERE res.pnr = pax.pnr "
						+
						// "AND res.status = 'CNF' "+
						"AND pax.pnr_pax_id = tx.pnr_pax_id " + "AND tx.txn_id = su.pax_txn_id AND res.origin_pos='"
						+ searchCriteria.getPos() + "' ");
				if (exceedsSixMons) {
					sql.append("AND tx.tnx_date BETWEEN ADD_MONTHS( TO_DATE ('" + searchCriteria.getToDate()
							+ "23:59:59', 'dd/mm/yyyy hh24:mi:ss'),-5) " + "AND TO_DATE ('" + searchCriteria.getToDate()
							+ "23:59:59','dd/mm/yyyy hh24:mi:ss') ");
				} else {
					sql.append("AND tx.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
							+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
							+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
				}
				sql.append("GROUP BY TO_CHAR (tx.tnx_date, 'mm'),TO_CHAR (tx.tnx_date, 'yyyy') " + "ORDER BY YEAR, MONTH");

			} else {
				if (searchCriteria.getRegion() != null && searchCriteria.equals("NET")) {
					sql.append("SELECT   SUM (su.far_amt) AS fare, "
							+ " SUM (su.sur_amt)+ SUM (su.cnx_amt)+ SUM (su.adj_amt)+SUM (su.mod_amt)  AS charges, "
							+ "TO_CHAR (tx.tnx_date, 'mm') AS MONTH, " + "TO_CHAR (tx.tnx_date, 'yyyy') AS YEAR "
							+ "FROM t_pax_transaction tx, " + "t_pnr_passenger pax, " + "t_reservation res, "
							+ "t_mis_txn_breakdown_summary su" + "WHERE res.pnr = pax.pnr " +
							// "AND res.status = 'CNF' "+
							"AND pax.pnr_pax_id = tx.pnr_pax_id " + "AND tx.txn_id = su.pax_txn_id ");
					if (exceedsSixMons) {
						sql.append("AND tx.tnx_date BETWEEN ADD_MONTHS( TO_DATE ('" + searchCriteria.getToDate()
								+ "23:59:59', 'dd/mm/yyyy hh24:mi:ss'),-5) " + "AND TO_DATE ('" + searchCriteria.getToDate()
								+ "23:59:59','dd/mm/yyyy hh24:mi:ss') ");
					} else {
						sql.append("AND tx.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
								+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
								+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
					}
					sql.append("AND res.origin_pos = 'NET' ");
					sql.append("GROUP BY TO_CHAR (tx.tnx_date, 'mm'),TO_CHAR (tx.tnx_date, 'yyyy') " + "ORDER BY YEAR, MONTH");
				} else {
					sql.append("SELECT   SUM (su.far_amt) AS fare, "
							+ " SUM (su.sur_amt)+ SUM (su.cnx_amt)+ SUM (su.adj_amt)+SUM (su.mod_amt)  AS charges, "
							+ "TO_CHAR (tx.tnx_date, 'mm') AS MONTH, " + "TO_CHAR (tx.tnx_date, 'yyyy') AS YEAR "
							+ "FROM t_pax_transaction tx, " + "t_pnr_passenger pax, " + "t_reservation res, "
							+ "t_mis_txn_breakdown_summary su, " + "t_station st, " + "t_country cn, " + "t_region rg "
							+ "WHERE res.pnr = pax.pnr " +
							// "AND res.status = 'CNF' "+
							"AND pax.pnr_pax_id = tx.pnr_pax_id " + "AND tx.txn_id = su.pax_txn_id ");
					if (exceedsSixMons) {
						sql.append("AND tx.tnx_date BETWEEN ADD_MONTHS( TO_DATE ('" + searchCriteria.getToDate()
								+ "23:59:59', 'dd/mm/yyyy hh24:mi:ss'),-5) " + "AND TO_DATE ('" + searchCriteria.getToDate()
								+ "23:59:59','dd/mm/yyyy hh24:mi:ss') ");
					} else {
						sql.append("AND tx.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
								+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') " + "AND TO_DATE ('" + searchCriteria.getToDate()
								+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
					}
					sql.append("AND res.origin_pos = st.station_code " + "AND st.country_code = cn.country_code "
							+ "AND cn.region_code = rg.region_code ");
					if (searchCriteria.getRegion() != null) {
						sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
					}
					sql.append("GROUP BY TO_CHAR (tx.tnx_date, 'mm'),TO_CHAR (tx.tnx_date, 'yyyy') " + "ORDER BY YEAR, MONTH");
				}
			}
		}
		return sql.toString();
	}

	public static String getMISDashRevenueByChannel(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		if (searchCriteria.getRegion() == null && searchCriteria.getPos() == null) {

			sql.append("SELECT   SUM (su.far_amt) + SUM (su.tax_amt)+SUM (su.cnx_amt)+SUM (su.mod_amt)+SUM (su.adj_amt)+ SUM (su.sur_amt) AS revenue, "
					+ "ch.description AS channel "
					+ "FROM t_pax_transaction tx, "
					+ "t_pnr_passenger pax, "
					+ "t_reservation res, "
					+ "t_sales_channel ch, "
					+ "t_mis_txn_breakdown_summary su "
					+ "WHERE res.pnr = pax.pnr "
					+ "AND res.origin_channel_code = ch.sales_channel_code "
					+
					// "AND res.status = 'CNF' "+
					"AND pax.pnr_pax_id = tx.pnr_pax_id "
					+ "AND tx.txn_id = su.pax_txn_id "
					+ "AND tx.tnx_date BETWEEN TO_DATE ('"
					+ searchCriteria.getFromDate()
					+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
					+ "AND TO_DATE ('"
					+ searchCriteria.getToDate()
					+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') GROUP BY ch.description");
		} else {

			if (searchCriteria.getPos() != null) {
				sql.append("SELECT   SUM (su.far_amt) + SUM (su.tax_amt)+SUM (su.cnx_amt)+SUM (su.mod_amt)+SUM (su.adj_amt)+ SUM (su.sur_amt) AS revenue, "
						+ "ch.description AS channel "
						+ "FROM t_pax_transaction tx, "
						+ "t_pnr_passenger pax, "
						+ "t_reservation res, "
						+ "t_sales_channel ch, "
						+ "t_mis_txn_breakdown_summary su "
						+ "WHERE res.pnr = pax.pnr "
						+
						// "AND res.status = 'CNF' "+
						"AND pax.pnr_pax_id = tx.pnr_pax_id "
						+ "AND tx.txn_id = su.pax_txn_id "
						+ "AND res.origin_channel_code = ch.sales_channel_code "
						+ "AND tx.tnx_date BETWEEN TO_DATE ('"
						+ searchCriteria.getFromDate()
						+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
						+ "AND TO_DATE ('"
						+ searchCriteria.getToDate()
						+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') "
						+ "AND res.origin_pos='"
						+ searchCriteria.getPos() + "'  GROUP BY ch.description");

			} else {
				sql.append("SELECT   SUM (su.far_amt) + SUM (su.tax_amt)+SUM (su.cnx_amt)+SUM (su.mod_amt)+SUM (su.adj_amt)+ SUM (su.sur_amt) AS revenue, "
						+ "ch.description AS channel "
						+ "FROM t_pax_transaction tx, "
						+ "t_pnr_passenger pax, "
						+ "t_reservation res, "
						+ "t_sales_channel ch, "
						+ "t_mis_txn_breakdown_summary su, "
						+ "t_station st, "
						+ "t_country cn, "
						+ "t_region rg "
						+ "WHERE res.pnr = pax.pnr "
						+
						// "AND res.status = 'CNF' "+
						"AND pax.pnr_pax_id = tx.pnr_pax_id "
						+ "AND tx.txn_id = su.pax_txn_id "
						+ "AND tx.tnx_date BETWEEN TO_DATE ('"
						+ searchCriteria.getFromDate()
						+ " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
						+ "AND TO_DATE ('"
						+ searchCriteria.getToDate()
						+ " 23:59:59','dd/mm/yyyy hh24:mi:ss') "
						+ "AND res.origin_pos = st.station_code "
						+ "AND res.origin_channel_code = ch.sales_channel_code "
						+ "AND st.country_code = cn.country_code "
						+ "AND cn.region_code = rg.region_code ");
				if (searchCriteria.getRegion() != null) {
					sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
				}
				sql.append("GROUP BY ch.description ");
			}
		}
		return sql.toString();
	}

	/**
	 * The query for ytd bookings count (this is the CNF pnr pax segment count)
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISDashYtdPaxSegmentCount(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		if (searchCriteria.getRegion() == null && searchCriteria.getPos() == null) {

			sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) as sales "
					+ "FROM t_reservation r, t_pnr_segment ps " + " WHERE ps.status = 'CNF' " + " AND r.status = 'CNF' "
					+ " AND r.pnr = ps.pnr " + " AND ps.status_mod_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
					+ " 00:00:00', 'dd,mm,yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
					+ " 23:59:59', 'dd,mm,yyyy HH24:mi:ss')");
		} else {

			if (searchCriteria.getPos() != null) {
				sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) AS sales "
						+ "  FROM t_reservation r, t_pnr_segment ps " + " WHERE ps.status = 'CNF' " + "  AND r.status = 'CNF' "
						+ "  AND r.pnr = ps.pnr " + "  AND r.pnr IN (SELECT pnr " + "                 FROM t_reservation res "
						+ "               WHERE res.origin_pos IN ('" + searchCriteria.getPos() + "')) "
						+ " AND ps.status_mod_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
						+ " 00:00:00', 'dd,mm,yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
						+ " 23:59:59', 'dd,mm,yyyy HH24:mi:ss')");
			}

			else {// if(searchCriteria.getRegion()!=null)
				if (searchCriteria.getRegion() != null && searchCriteria.getRegion().equals("NET")) {
					sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) AS sales "
							+ "  FROM t_reservation r, t_pnr_segment ps " + " WHERE ps.status = 'CNF' "
							+ "  AND r.status = 'CNF' " + "  AND r.pnr = ps.pnr " + "  AND r.origin_pos = 'NET' "
							+ " AND ps.status_mod_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
							+ " 00:00:00', 'dd,mm,yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
							+ " 23:59:59', 'dd,mm,yyyy HH24:mi:ss')");

				} else {
					sql.append("SELECT SUM (r.total_pax_count + r.total_pax_child_count) AS sales "
							+ "  FROM t_reservation r, t_pnr_segment ps " + " WHERE ps.status = 'CNF' "
							+ "  AND r.status = 'CNF' " + "  AND r.pnr = ps.pnr " + "  AND r.pnr IN (SELECT pnr "
							+ "                 FROM t_reservation res "
							+ "               WHERE res.origin_pos IN ( SELECT st.station_code "
							+ "  FROM t_station st, t_country cn " + " WHERE st.country_code = cn.country_code "
							+ "   AND cn.region_code = '" + searchCriteria.getRegion() + "')) "
							+ " AND ps.status_mod_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
							+ " 00:00:00', 'dd,mm,yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
							+ " 23:59:59', 'dd,mm,yyyy HH24:mi:ss')");
				}
			}
		}
		return sql.toString();
	}

	/**
	 * Returns MIS- Accounts outstanding invoice details
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISAccountsOutStandingInvQuery(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		sql.append("select * from(SELECT   agn.agent_name,invoiceno,sum(amount) as amount,invdate, rownum "
				+ "FROM t_agent agn, t_station st, t_country cn, t_region r, "
				+ "(select * from(SELECT   a.invoice_number AS invoiceno, a.invoice_date AS invdate, "
				+ "  a.agent_code AS agent_code, " + "  SUM (a.invoice_amount - a.settled_amount) AS amount "
				+ "FROM t_invoice a, t_agent ag " + "WHERE     a.agent_code = ag.agent_code "
				+ "AND a.invoice_date between to_date('" + searchCriteria.getFromDate() + " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
				+ "and to_date('" + searchCriteria.getToDate() + "23:59:59','dd/mm/yyyy hh24:mi:ss') "
				+ "GROUP BY a.agent_code, a.invoice_number, a.invoice_date ORDER BY amount DESC) ) aaa "
				+ "WHERE agn.station_code = st.station_code " + "AND  agn.agent_code = aaa.agent_code "
				+ "AND st.country_code = cn.country_code " + "AND cn.region_code = r.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND r.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		sql.append("GROUP BY invoiceno, invdate, agn.agent_name, rownum ORDER BY 3 DESC ) where rownum<=10 ");
		return sql.toString();
	}

	/**
	 * Retrieves sales for past 3 years
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISRegYearlySales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from(SELECT a.year, a.sales, rownum FROM t_mis_yearly_sales a order by year desc ) where rownum<=3");

		return sql.toString();
	}

	/**
	 * Retrieves all outstanding invoice amount total for each region for given search criteria.
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISAccountsOutStandingByRegion(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT   r.region_name,  decode(SUM(amount),0,null,SUM(amount)) as amount "
				+ "FROM t_agent agn, t_station st, t_country cn, t_region r, " + "(select * from(SELECT "
				+ "   a.agent_code AS agent_code, " + "   SUM (a.invoice_amount - a.settled_amount) AS amount "
				+ "FROM t_invoice a, t_agent ag " + "WHERE     ag.agent_code = a.agent_code "
				+ "AND a.invoice_date between TO_DATE ('" + searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy hh24:mi:ss') "
				+ "AND TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy hh24:mi:ss') "
				+ "GROUP BY a.agent_code ORDER BY amount DESC) ) aaa " + "WHERE agn.station_code = st.station_code "
				+ "AND  agn.agent_code = aaa.agent_code " + "AND st.country_code = cn.country_code "
				+ "AND cn.region_code = r.region_code " + "GROUP BY r.region_name ORDER BY amount DESC ");

		return sql.toString();
	}

	/**
	 * Returns query for all countries, cities in each country, flights departing from each country and agents belong to
	 * each country for a given search criteria
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISDistributionFlightsCitiesNAgents(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT   country_code, country, COUNT (DISTINCT station_code) cities, "
				+ "SUM (agents) agents, SUM (flights) flights "
				+ "FROM (SELECT   c.country_code, c.country_name as country, s.station_code, "
				+ " COUNT (DISTINCT a.agent_code) as agents, " + "MAX " + "((SELECT COUNT (*) " + "  FROM t_flight "
				+ " WHERE status <> 'CNX' " + "AND flight_id in (select flight_id from  t_flight_leg lg where  lg.origin IN ( "
				+ "       SELECT airport_code " + "         FROM t_airport  WHERE station_code =s.station_code)) "
				+ "AND departure_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy hh24:mi:ss') "
				+ "AND TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy hh24:mi:ss')) " + " ) flights "
				+ "FROM t_country c, t_station s, t_agent a, t_region rg " + "WHERE s.status = 'ACT' "
				+ "AND nvl(a.status,'ACT') = 'ACT' " + "AND c.region_code = rg.region_code "
				+ "AND c.country_code = s.country_code " + "AND s.station_code = a.station_code(+) ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		if (searchCriteria.getPos() != null) {
			sql.append("AND s.station_code = '" + searchCriteria.getPos() + "' ");
		}

		sql.append("GROUP BY c.country_code, c.country_name, s.station_code) " + "GROUP BY country_code, country ORDER BY 2 ");
		return sql.toString();
	}

	/**
	 * Returns the sql for Regional Product sales data
	 * 
	 * @param searchCriteria
	 * @return
	 */
	public static String getMISRegionalProductSalesDataQuery(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT   SUM (txn.amount) * -1 AS sales, ag.agent_name as agent, st.station_name as location, "
				+ "rg.region_name as region " + "FROM  t_pax_transaction txn, " + "t_agent ag, " + "t_station st, "
				+ " t_country cn, " + "t_region rg " + "WHERE txn.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59', 'dd/mm/yyyy HH24:mi:ss' ) " + "AND txn.agent_code = ag.agent_code "
				+ "AND ag.station_code = st.station_code " + "and st.country_code = cn.country_code "
				+ " and cn.region_code = rg.region_code " + "AND txn.nominal_code IN("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()) + ") "
				+ "AND ag.station_code = st.station_code " + "AND st.country_code = cn.country_code "
				+ "AND cn.region_code = rg.region_code " + "GROUP BY ag.agent_name, " + " st.station_name, "
				+ "cn.country_name, " + " rg.region_name ORDER BY rg.region_name");

		return sql.toString();
	}

	public static String getMISRegionalBestPerformingAgents(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * " + "FROM (SELECT SUM(txn.amount)*-1 AS revenue," + "txn.agent_code     AS agentcode,"
				+ "ag.agent_name             AS agentname," + "ag.address_line_1         AS address1,"
				+ "ag.address_line_2         AS address2," + "ag.address_city           AS city,"
				+ "ag.address_state_province AS state," + "ag.postal_code            AS postalcode,"
				+ "cn.country_name           AS country," + "st.station_name           AS stationname," + "ag.telephone,"
				+ "ag.email_id," + "ag.contact_person_name AS contactperson " + "FROM t_pax_transaction txn," + "t_agent ag,"
				+ "t_station st," + "t_country cn," + "t_region rg " + "WHERE txn.agent_code = ag.agent_code "
				+ "AND ag.station_code       = st.station_code " + "AND st.country_code       = cn.country_code "
				+ "AND cn.region_code        = rg.region_code " + "AND txn.tnx_date BETWEEN TO_DATE ('"
				+ searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('"
				+ searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND txn.nominal_code in ("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()) + ") ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}

		sql.append("GROUP BY txn.agent_code, " + "ag.agent_name, " + "st.station_name, " + "ag.address_line_1, "
				+ "ag.address_line_2, " + "ag.address_city, " + "ag.address_state_province, " + "ag.postal_code, "
				+ "cn.country_name, " + "ag.telephone, " + "ag.email_id, "
				+ "ag.contact_person_name ORDER BY revenue DESC) WHERE ROWNUM <= 10");
		return sql.toString();

	}

	public static String getMISRegionalLowPerformingAgents(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * " + "FROM (SELECT SUM(txn.amount)*-1 AS revenue," + "txn.agent_code     AS agentcode,"
				+ "ag.agent_name             AS agentname," + "ag.address_line_1         AS address1,"
				+ "ag.address_line_2         AS address2," + "ag.address_city           AS city,"
				+ "ag.address_state_province AS state," + "ag.postal_code            AS postalcode,"
				+ "cn.country_name           AS country," + "st.station_name           AS stationname," + "ag.telephone,"
				+ "ag.email_id," + "ag.contact_person_name AS contactperson " + "FROM t_pax_transaction txn," + "t_agent ag,"
				+ "t_station st," + "t_country cn," + "t_region rg " + "WHERE txn.agent_code = ag.agent_code "
				+ "AND ag.station_code       = st.station_code " + "AND st.country_code       = cn.country_code "
				+ "AND cn.region_code        = rg.region_code " + "AND txn.tnx_date BETWEEN TO_DATE ('"
				+ searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('"
				+ searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND txn.nominal_code in ("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()) + ") ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}

		sql.append("GROUP BY txn.agent_code, " + "ag.agent_name, " + "st.station_name, " + "ag.address_line_1, "
				+ "ag.address_line_2, " + "ag.address_city, " + "ag.address_state_province, " + "ag.postal_code, "
				+ "cn.country_name, " + "ag.telephone, " + "ag.email_id, "
				+ "ag.contact_person_name ORDER BY revenue) WHERE ROWNUM <= 10");
		return sql.toString();

	}

	public static String getMISRegionalAVGAgentSales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT ROUND (SUM (sales) / COUNT (*), 2) AS AVG "
				+ "FROM (SELECT   SUM (txn.amount) * -1 AS sales, txn.agent_code AS agentcode "
				+ "   FROM t_pax_transaction txn, " + "     t_agent ag, " + "     t_station st, " + "     t_country cn, "
				+ "     t_region rg " + " WHERE txn.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate()
				+ " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + " AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59', 'dd/mm/yyyy HH24:mi:ss' ) " + "  AND txn.agent_code = ag.agent_code "
				+ "  AND ag.station_code = st.station_code " + "  AND st.country_code = cn.country_code "
				+ "  AND cn.region_code = rg.region_code " + "  AND txn.nominal_code IN("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()) + ") ");

		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		if (searchCriteria.getRegion() != null) {
			sql.append("AND rg.region_code = '" + searchCriteria.getRegion() + "' ");
		}

		sql.append("GROUP BY txn.agent_code)");

		return sql.toString();
	}

	public static String getMISRegionalSixMonsRollingTrend(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT (seg.pnr_seg_id) AS sales, TO_CHAR (seg.status_mod_date, 'mm') AS MONTH, "
				+ "TO_CHAR (seg.status_mod_date, 'yy') AS YEAR, rg.region_name as region " + "FROM t_reservation res, "
				+ "t_pnr_passenger pax, " + " t_pnr_segment seg, " + " t_station st, " + "t_country cn, " + "t_region rg "
				+ "WHERE res.pnr = pax.pnr " + "AND pax.pnr = seg.pnr " + "AND pax.pax_type_code IN ('AD', 'CH') "
				+ "AND res.status = 'CNF' " + "AND seg.status = 'CNF' "
				+ "AND seg.status_mod_date  BETWEEN ADD_MONTHS (TO_DATE ('" + searchCriteria.getToDate()
				+ " 00:00:00', 'dd/mm/yyyy HH24:mi:ss'),-5) " + "AND TO_DATE ('" + searchCriteria.getToDate()
				+ " 23:59:59', 'dd/mm/yyyy HH24:mi:ss') " + "AND res.origin_pos = st.station_code "
				+ "AND st.country_code = cn.country_code " + " AND cn.region_code = rg.region_code "
				+ "GROUP BY TO_CHAR (seg.status_mod_date, 'mm'), " + " TO_CHAR (seg.status_mod_date, 'yy'), "
				+ "  rg.region_name ORDER BY YEAR, MONTH ");

		return sql.toString();
	}

	public static String getMISDistSalesByChannelPerCountry(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT sum(res.total_pax_count+res.total_pax_child_count) AS sales, cn.country_code country, "
				+ "ch.sales_channel_code channel, ch.sales_channel_colour channelcolor "
				+ "FROM t_reservation res, t_sales_channel ch, " + "t_pnr_segment seg, t_station st, t_country cn "
				+ "WHERE res.pnr = seg.pnr " + "AND res.status = 'CNF' " + "AND seg.status_mod_date BETWEEN TO_DATE ('"
				+ searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + "AND TO_DATE ('"
				+ searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') " + "AND seg.status = 'CNF' "
				+ "AND res.origin_pos = st.station_code " + "AND res.origin_channel_code = ch.sales_channel_code "
				+ "AND st.country_code = cn.country_code " + "AND cn.country_code IN (" + searchCriteria.getCountryCodes() + ") ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}

		sql.append("GROUP BY cn.country_code, ch.sales_channel_code, ch.sales_channel_colour ");

		return sql.toString();
	}

	public static String getMISAgentsYearySales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.DAY_OF_YEAR, 1);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String fromDate = df.format(cal.getTime());

		sql.append("SELECT SUM(txn.amount)*-1 AS sales, " + "txn.agent_code AS agentcode " + "FROM t_pax_transaction txn, "
				+ "t_agent ag " + "WHERE txn.tnx_date BETWEEN TO_DATE ('" + fromDate + " 00:00:00', 'dd/mm/yyyy HH24:mi:ss') "
				+ "AND TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy HH24:mi:ss') "
				+ " AND txn.agent_code = ag.agent_code " + " AND ag.agent_code in(" + searchCriteria.getAgentCodes() + ")"
				+ "AND txn.nominal_code IN(" + searchCriteria.getPaymentNominalCodes() + ") " + "GROUP BY txn.agent_code ");

		return sql.toString();
	}

	public static String getSeatFactorSummaryInfo(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		if (!reportsSearchCriteria.isByStation()) {
			sb.append(" select 's' station, departure_date, flight_number, status, segment_code, cabin_class_code, ");
			sb.append(" allocated_seats, oversell_seats, curtailed_seats, fixed_seats, sum(sold_seats) sold_seats, sum(on_hold_seats) on_hold_seats, available_seats ,  ");
			sb.append(" SUM(amount) amount, SUM(surCharge) surCharge  ");
			sb.append(" from ( ");
		}

		/*
		 * sb.append(" select station, departure_date, flight_number, flight_id, status, segment_code, cabin_class_code, "
		 * ); sb.append(
		 * " allocated_seats, oversell_seats, curtailed_seats, fixed_seats, sum(sold_seats) sold_seats, sum(on_hold_seats) on_hold_seats, available_seats ,  "
		 * ); //sb.append(" calc_seat_factor, SUM(amount) amount, SUM(surCharge) surCharge  ");
		 * sb.append(" SUM(amount) amount, SUM(surCharge) surCharge  "); sb.append(" from ( ");
		 */

		sb.append(" select NVL(d.station_code,'WEB') station, b.est_time_departure_local as departure_date, a.flight_number, a.status, ");
		sb.append(" b.segment_code, lcc.cabin_class_code, ");
		sb.append(" c.allocated_seats, c.oversell_seats, c.curtailed_seats, c.fixed_seats, ");
		sb.append(" NVL(d.SOLD_SEATS,0) SOLD_SEATS, NVL(d.onhold_seats,0) on_hold_seats, c.available_seats , ");
		// sb.append(" round(((NVL((select sum(x.SOLD_SEATS) from t_rep_fwd_bkg x where x.flt_seg_id=d.flt_seg_id),0) ");
		// sb.append(" +NVL((select sum(x.onhold_seats) from t_rep_fwd_bkg x where x.flt_seg_id=d.flt_seg_id),0))/c.allocated_seats),2) calc_seat_factor, ");
		sb.append(" round(nvl(d.far_amt,0),2) amount, ");
		sb.append(" round(nvl(d.sur_amt,0),2) surCharge ");
		sb.append(" from t_flight a, t_flight_segment b, t_fcc_seg_alloc c ,t_rep_fwd_bkg d, t_logical_cabin_class lcc ");
		sb.append(" where a.flight_id = b.flight_id ");
		sb.append(" and b.flt_seg_id = c.flt_seg_id ");
		sb.append(" and c.flt_seg_id = d.flt_seg_id (+) ");
		//TODO : Logical CC Change : check cabin_class_code
		sb.append(" and d.logical_cabin_class_code (+) = c.logical_cabin_class_code");
		sb.append(" AND lcc.logical_cabin_class_code = c.logical_cabin_class_code ");
		sb.append(" and ");

		if (reportsSearchCriteria.getDateRangeArray() != null && reportsSearchCriteria.getDateRangeArray().length > 1) {
			sb.append(" ( ");
			sb.append(" (b.est_time_departure_local between to_date('@startDate1  00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("	and to_date('@EndDate1  23:59:59', 'DD-MON-YYYY HH24:mi:ss')) or ");
			sb.append(" (b.est_time_departure_local between to_date('@startDate2  00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("	and to_date('@EndDate2  23:59:59', 'DD-MON-YYYY HH24:mi:ss')) or ");
			sb.append(" (b.est_time_departure_local between to_date('@startDate3  00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("	and to_date('@EndDate3  23:59:59', 'DD-MON-YYYY HH24:mi:ss')) or ");
			sb.append(" (b.est_time_departure_local between to_date('@startDate4  00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("	and to_date('@EndDate4  23:59:59', 'DD-MON-YYYY HH24:mi:ss')) ");
			sb.append(" ) ");
		} else {
			sb.append(" (b.est_time_departure_local between to_date('@startDate1  00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("	and to_date('@EndDate1  23:59:59', 'DD-MON-YYYY HH24:mi:ss')) ");
		}

		if (isNotEmptyOrNull(reportsSearchCriteria.getCarrierCode()))
			sb.append(" and SUBSTR(A.flight_number,1,2) IN('" + reportsSearchCriteria.getCarrierCode() + "')  ");

		if (!reportsSearchCriteria.getFlightStatus().equalsIgnoreCase("All"))
			sb.append(" and a.status in('" + reportsSearchCriteria.getFlightStatus() + "') ");
		else
			sb.append(" and a.status IN ('" + FlightStatusEnum.ACTIVE + "','" + FlightStatusEnum.CLOSED + "','"
					+ FlightStatusEnum.CANCELLED + "') ");

		if (reportsSearchCriteria.isByStation()) {
			sb.append("	order by b.est_time_departure_local, a.flight_number, b.segment_code ");
		} else {
			sb.append(" ) group by departure_date, flight_number, status, segment_code, cabin_class_code, allocated_seats, oversell_seats,  ");
			sb.append(" curtailed_seats, fixed_seats, available_seats ");
			sb.append(" order by departure_date, flight_number, segment_code ");
		}

		return sb.toString();
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("Select") || str.trim().equals("-1"));
	}

	private static String getSingleDataValue(Collection<String> parameters) {
		StringBuffer sbData = new StringBuffer();
		Iterator<String> it = null;
		String data = null;

		if ((parameters != null) && (!parameters.isEmpty())) {
			it = parameters.iterator();
			while (it.hasNext()) {
				data = it.next();
				if (data != null) {
					sbData.append("'");
					sbData.append(data);
					sbData.append("'");
					if (it.hasNext()) {
						sbData.append(",");
					}
				}
			}
		}
		return sbData.toString();
	}

}
