package com.isa.thinair.reporting.core.remoting.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.model.FlaGraphBarBreakDwnDTO;
import com.isa.thinair.reporting.api.model.FlightInventroyDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.bl.fla.AlertFinder;
import com.isa.thinair.reporting.core.bl.fla.FLALogicBL;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.service.bd.FLADataProviderBDImpl;
import com.isa.thinair.reporting.core.service.bd.FLADataProviderLocalBDImpl;
import com.isa.thinair.reporting.core.util.InternalConstants;
import com.isa.thinair.reporting.core.util.LookupUtils;

@Stateless
@RemoteBinding(jndiBinding = "FLADataService.remote")
@LocalBinding(jndiBinding = "FLADataService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FLADataServiceBean extends PlatformBaseSessionBean implements FLADataProviderBDImpl, FLADataProviderLocalBDImpl {

	public FlightInventroyDTO getFlightInventoryDetails(Integer flightID, String lastSevenDay, String LastThirtyDay,
			String departDate) throws ModuleException {
		return getFLADataProviderDAOJDBC().getFlightInventoryDetails(flightID, lastSevenDay, LastThirtyDay, departDate);

	}

	public ArrayList getFlightInvGraphData(Integer flightID, String lastSeventhDay, String lastThirtyDay, String departDate,
			ArrayList<String> lstChannel) throws ModuleException {
		return (ArrayList) getFLADataProviderDAOJDBC().getFlightInvGraphData(flightID, lastSeventhDay, lastThirtyDay, departDate,
				lstChannel);
	}

	public ArrayList<FlaGraphBarBreakDwnDTO> getGraphDataBreakDown(Integer flightID, String channel,
			ArrayList<Integer> nominalCodes) throws ModuleException {
		return (ArrayList<FlaGraphBarBreakDwnDTO>) FLALogicBL.getGraphDataBreakDown(flightID, channel, nominalCodes);
	}

	public List getFLAReservationAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getFLAReservationAlert(flightIDLst);
	}

	public List getFLADuplicateNameAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getFLADuplicateNameAlert(flightIDLst);
	}

	public List getFLAPartialCreditAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getFLAPartialCreditAlert(flightIDLst);
	}

	public List getFLATbaNamesAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getFLATbaNamesAlert(flightIDLst);
	}

	public List getGroupBookingsAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getGroupBookingsAlert(flightIDLst);
	}

	public List getFLADupPassportAlert(List<Integer> flightIDLst) throws ModuleException {
		return (List) getFLADataProviderDAOJDBC().getFLADupPassportAlert(flightIDLst);
	}

	public List<FlaAlertDTO> getFlaAlerts(List<Integer> lstFlightID) throws ModuleException {
		return ((AlertFinder) LookupUtils.getBean("flaAlertFinder")).findAlerts(lstFlightID);
	}

	public FLADataProviderDAOJDBC getFLADataProviderDAOJDBC() {
		return (FLADataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLADataProviderDAO);
	}

}
