package com.isa.thinair.reporting.core.bl.fla;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.util.InternalConstants;

public class DupNameAlertFindingStrategyImpl implements AlertFinderStatergy {

	@Override
	public List<FlaAlertDTO> composeAlerts(List<Integer> lstFlightIDs) throws ModuleException {
		HashMap<String, FlaAlertDTO> lstAlertRearrange = new HashMap<String, FlaAlertDTO>();

		List<FlaAlertDTO> lstDupNameAlert = ((FLADataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLADataProviderDAO)).getFLADuplicateNameAlert(lstFlightIDs);

		if (lstDupNameAlert != null && !lstDupNameAlert.isEmpty()) {
			for (FlaAlertDTO tbaAlertDto : lstDupNameAlert) {
				String tmpPNR = tbaAlertDto.getPnr();
				if (!lstAlertRearrange.containsKey(tmpPNR)) {
					lstAlertRearrange.put(tmpPNR, tbaAlertDto);
				}
			}
		}
		return new ArrayList(lstAlertRearrange.values());

	}

}
