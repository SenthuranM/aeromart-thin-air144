package com.isa.thinair.reporting.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.mis.OutstandingInvoiceInfoTO;
import com.isa.thinair.reporting.api.model.AgentPerformanceTO;
import com.isa.thinair.reporting.api.model.BookingsTO;
import com.isa.thinair.reporting.api.model.DistributionFlightInfoTO;
import com.isa.thinair.reporting.api.model.MISAccountSummaryTO;
import com.isa.thinair.reporting.api.model.MISOutstandingInvoicesTO;
import com.isa.thinair.reporting.api.model.SalesRevenueTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistent.dao.MISDataProviderDAOJDBC;
import com.isa.thinair.reporting.core.service.bd.MISDataProviderBDImpl;
import com.isa.thinair.reporting.core.service.bd.MISDataProviderLocalBDImpl;
import com.isa.thinair.reporting.core.util.InternalConstants;

@Stateless
@RemoteBinding(jndiBinding = "MISDataService.remote")
@LocalBinding(jndiBinding = "MISDataService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class MISDataServiceBean extends PlatformBaseSessionBean implements MISDataProviderBDImpl, MISDataProviderLocalBDImpl {

	@Override
	public long getMISDashboardGoal(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDashboardGoal(searchCriteria);
	}

	public MISDataProviderDAOJDBC getMISDataProviderDAOJDBC() {
		return (MISDataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.MISDataProviderDAO);
	}

	@Override
	public Collection<BigDecimal> getMISDashRevenueBreakdown(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDashRevenueBreakdown(searchCriteria);
	}

	@Override
	public Map<String, BigDecimal> getMISDashSalesByChannel(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDashSalesByChannel(searchCriteria);
	}

	@Override
	public Map<String, Map<String, Map<String, BigDecimal>>> getMISSeatFactorReportData(
			ReportsSearchCriteria reportsSearchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISSeatFactorReportData(reportsSearchCriteria);
	}

	@Override
	public Map<String, List<Map<String, List<BigDecimal>>>> getYieldTrendAnalysisReport(
			ReportsSearchCriteria reportsSearchCriteria) {
		return this.getMISDataProviderDAOJDBC().getYieldTrendAnalysisReport(reportsSearchCriteria);
	}

	@Override
	public Collection<SalesRevenueTO> getMISDashYearlySales(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDashYearlySales(searchCriteria);
	}

	@Override
	public long getMISDashboardYtdBookings(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISYtdBookings(searchCriteria);
	}

	@Override
	public Collection<DistributionFlightInfoTO> getMISDistFlightsNCitiesByCountry(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDistCountryWiseFlights(searchCriteria);
	}

	@Override
	public Map<String, Collection<BookingsTO>> getMISDistChannelwiseSalesForCountry(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDistChannelwiseSalesForCountry(searchCriteria);
	}

	@Override
	public Map<String, BigDecimal> getMISDistRevenueByChannel(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDistRevenueBreakDownChannelwise(searchCriteria);
	}

	@Override
	public Map<String, Map<String, String>> getMISRegionRollingSalesTrend(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISRegionRollingSalesTrend(searchCriteria);
	}

	@Override
	public Map<String, Long> getMisRegionYearlySales(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMisRegionYearlySales(searchCriteria);
	}

	@Override
	public Collection<AgentPerformanceTO> getMISRegionalBestPerformingAgents(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getBestPerformingAgent(searchCriteria);
	}

	@Override
	public Collection<AgentPerformanceTO> getMISRegionalLowPerformingAgents(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getLowPerformingAgent(searchCriteria);
	}

	@Override
	public Collection<AgentPerformanceTO> getRegionalProductSalesData(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getRegionalProductSalesData(searchCriteria);
	}

	@Override
	public Long getAgentAverageSales(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISRegionalAvgAgentSales(searchCriteria);
	}

	@Override
	public Map<String, BigDecimal> getAncillarySalesSummary(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getAncillarySalesSummary(searchCriteria);
	}

	@Override
	public Map<String, Map<String, String>> getAncillarySalesByRoute(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getAncillarySalesByRoute(searchCriteria);
	}

	@Override
	public Map<String, Map<String, BigDecimal>> getAncillarySalesBychannel(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getAncillarySalesBychannel(searchCriteria);
	}

	@Override
	public Collection<MISAccountSummaryTO> getMISAccountRefunds(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISAccountRefunds(searchCriteria);
	}

	@Override
	public Collection<MISAccountSummaryTO> getMISAccountSales(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISAccountSales(searchCriteria);
	}

	@Override
	public Collection<MISOutstandingInvoicesTO> getMISAccountOutstandingInvoices(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISAccountOutstandingInvoices(searchCriteria);
	}

	@Override
	public Map<String, BigDecimal> getMISOutstandingByRegion(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISOutstandingByRegion(searchCriteria);
	}

	@Override
	public BigDecimal getMISAccountTotalOutstandingCredits(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISAccountTotalOutstandingCredits(searchCriteria);
	}

	@Override
	public HashMap<String, BigDecimal> getGSATurnover(MISReportsSearchCriteria searchCriteria, List<String> agentCodes) {
		return this.getMISDataProviderDAOJDBC().getGSATurnover(searchCriteria, agentCodes);
	}

	@Override
	public Integer getNoOfEmployees(String agentCode) {
		return this.getMISDataProviderDAOJDBC().getNoOfEmployees(agentCode);
	}

	@Override
	public Collection<OutstandingInvoiceInfoTO> getOutstandingInvoiceInfo(MISReportsSearchCriteria searchCriteria,
			String agentCode) {
		return this.getMISDataProviderDAOJDBC().getOutstandingInvoiceInfo(searchCriteria, agentCode);
	}

	@Override
	public Map<String, Integer> getMISDashSalesByChannelPaxCount(MISReportsSearchCriteria searchCriteria) {
		return this.getMISDataProviderDAOJDBC().getMISDashSalesByChannelPaxCount(searchCriteria);
	}

	@Override
	public Collection<HashMap<String, String>>
			getRouteWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode) {
		return this.getMISDataProviderDAOJDBC().getRouteWiseAgentRevenue(searchCriteria, agentCode);
	}

	@Override
	public Collection<HashMap<String, String>>
			getStaffWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode) {
		return this.getMISDataProviderDAOJDBC().getStaffWiseAgentRevenue(searchCriteria, agentCode);
	}

}
