package com.isa.thinair.reporting.api.service;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.model.FlaGraphBarBreakDwnDTO;
import com.isa.thinair.reporting.api.model.FlightInventroyDTO;

/**
 * @author harsha
 * 
 */
public interface FLADataProviderBD {
	public static final String SERVICE_NAME = "FLADataService";

	public FlightInventroyDTO getFlightInventoryDetails(Integer flightID, String lastSevenDay, String LastThirtyDay,
			String departDate) throws ModuleException;

	public ArrayList getFlightInvGraphData(Integer flightID, String lastSevenDay, String LastThirtyDay, String departDate,
			ArrayList<String> lstChannel) throws ModuleException;

	public ArrayList<FlaGraphBarBreakDwnDTO> getGraphDataBreakDown(Integer flightID, String channel,
			ArrayList<Integer> nominalCodes) throws ModuleException;

	public List getFLAReservationAlert(List<Integer> flightIDLst) throws ModuleException;

	public List getFLADuplicateNameAlert(List<Integer> flightIDLst) throws ModuleException;

	public List getFLAPartialCreditAlert(List<Integer> flightIDLst) throws ModuleException;

	public List getFLATbaNamesAlert(List<Integer> flightIDLst) throws ModuleException;

	public List getGroupBookingsAlert(List<Integer> flightIDLst) throws ModuleException;

	public List getFLADupPassportAlert(List<Integer> flightIDLst) throws ModuleException;

	public List<FlaAlertDTO> getFlaAlerts(List<Integer> lstFlightID) throws ModuleException;
}