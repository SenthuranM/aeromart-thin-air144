package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_REPORT_META_DATA"
 */
public class ReportMetaData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String reportName;

	private String displayName;

	private String dataSource;

	private Integer maxGlobalSchedCount;

	private Integer maxUserSchedCount;

	/**
	 * @hibernate.id column = "REPORT_NAME" generator-class = "assigned"
	 */
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @hibernate.property column = "REPORT_DISPLAY_NAME"
	 */
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @hibernate.property column = "REPORT_DATA_SOURCE"
	 */
	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @hibernate.property column = "MAX_GLOBLE_SCHED_COUNT"
	 */
	public Integer getMaxGlobalSchedCount() {
		return maxGlobalSchedCount;
	}

	public void setMaxGlobalSchedCount(Integer maxGlobalSchedCount) {
		this.maxGlobalSchedCount = maxGlobalSchedCount;
	}

	/**
	 * @hibernate.property column = "MAX_USER_SCHED_COUNT"
	 */
	public Integer getMaxUserSchedCount() {
		return maxUserSchedCount;
	}

	public void setMaxUserSchedCount(Integer maxUserSchedCount) {
		this.maxUserSchedCount = maxUserSchedCount;
	}
}
