package com.isa.thinair.reporting.api.service;

import java.util.Collection;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.SchduledReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.scheduledRpts.ScheduledReportsViewTO;
import com.isa.thinair.reporting.api.model.ReportMetaData;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;

public interface ScheduledReportBD {

	public static final String SERVICE_NAME = "ScheduledReport";

	public void scheduleAReport(ScheduledReport scheduledReport) throws ModuleException;

	public ScheduledReport getScheduledReport(Integer scheduledReportId);

	public Collection<ScheduledReport> getNotActivatedScheduleReports();

	public Collection<ScheduledReport> getCompletionRequiredScheduleReports();

	public void activateScheduledReport(ScheduledReport scheduledReport) throws ModuleException;

	public void finishScheduledReport(ScheduledReport scheduledReport) throws ModuleException;

	public void recreateScheduledReportTrigger(Integer scheduledReportId) throws ModuleException;

	public Collection<Integer> getStartedScheduledReportIdsWithNoTriggers();

	public void saveSRSentItem(SRSentItem sentItem);

	public ReportMetaData getReportMetaData(String reportName);

	public Page getAllScheduleReports(SchduledReportsSearchCriteria reportsSearchTO, int startIndex, int pageSize)
			throws ModuleException;

	public ScheduledReport modifyScheduleReport(ScheduledReportsViewTO reportsViewTO);

	public ScheduledReport activateDeactivteScheduled(ScheduledReportsViewTO reportsViewTO) throws ModuleException;

	public Page getSRSentItemsForAScheduledReport(Long scheduledReportID, int startIndex, int pageSize) throws ModuleException;
}
