package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MISSeatFactorSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4182793212117603112L;
	private Integer misSFSId;
	private String station;
	private Integer flightId;
	private Integer totalCapacity;
	private Integer totalSeatSold;
	private Integer totalSeatOnHold;
	private BigDecimal seatFactor;
	private BigDecimal totalFareAmount;
	private BigDecimal totalSurChargeAmount;
	private BigDecimal totalModificationAmount;
	private BigDecimal totalCancelAmount;
	private Date createdDate;
	private BigDecimal yield;
	private Date departureDateLocal;
	private String estTimeDepartureLocal;

	/**
	 * @return the departureDateLocal
	 */
	public Date getDepartureDateLocal() {
		return departureDateLocal;
	}

	/**
	 * @param departureDateLocal
	 *            the departureDateLocal to set
	 */
	public void setDepartureDateLocal(Date departureDateLocal) {
		this.departureDateLocal = departureDateLocal;
	}

	/**
	 * @return the yield
	 */
	public BigDecimal getYield() {
		return yield;
	}

	/**
	 * @param yield
	 *            the yield to set
	 */
	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}

	/**
	 * @return the misSFSId
	 */
	public Integer getMisSFSId() {
		return misSFSId;
	}

	/**
	 * @param misSFSId
	 *            the misSFSId to set
	 */
	public void setMisSFSId(Integer misSFSId) {
		this.misSFSId = misSFSId;
	}

	/**
	 * @return the station
	 */
	public String getStation() {
		return station;
	}

	/**
	 * @param station
	 *            the station to set
	 */
	public void setStation(String station) {
		this.station = station;
	}

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the totalCapacity
	 */
	public Integer getTotalCapacity() {
		return totalCapacity;
	}

	/**
	 * @param totalCapacity
	 *            the totalCapacity to set
	 */
	public void setTotalCapacity(Integer totalCapacity) {
		this.totalCapacity = totalCapacity;
	}

	/**
	 * @return the totalSeatSold
	 */
	public Integer getTotalSeatSold() {
		return totalSeatSold;
	}

	/**
	 * @param totalSeatSold
	 *            the totalSeatSold to set
	 */
	public void setTotalSeatSold(Integer totalSeatSold) {
		this.totalSeatSold = totalSeatSold;
	}

	/**
	 * @return the totalSeatOnHold
	 */
	public Integer getTotalSeatOnHold() {
		return totalSeatOnHold;
	}

	/**
	 * @param totalSeatOnHold
	 *            the totalSeatOnHold to set
	 */
	public void setTotalSeatOnHold(Integer totalSeatOnHold) {
		this.totalSeatOnHold = totalSeatOnHold;
	}

	/**
	 * @return the seatFactor
	 */
	public BigDecimal getSeatFactor() {
		return seatFactor;
	}

	/**
	 * @param seatFactor
	 *            the seatFactor to set
	 */
	public void setSeatFactor(BigDecimal seatFactor) {
		this.seatFactor = seatFactor;
	}

	/**
	 * @return the totalFareAmount
	 */
	public BigDecimal getTotalFareAmount() {
		return totalFareAmount;
	}

	/**
	 * @param totalFareAmount
	 *            the totalFareAmount to set
	 */
	public void setTotalFareAmount(BigDecimal totalFareAmount) {
		this.totalFareAmount = totalFareAmount;
	}

	/**
	 * @return the totalSurChargeAmount
	 */
	public BigDecimal getTotalSurChargeAmount() {
		return totalSurChargeAmount;
	}

	/**
	 * @param totalSurChargeAmount
	 *            the totalSurChargeAmount to set
	 */
	public void setTotalSurChargeAmount(BigDecimal totalSurChargeAmount) {
		this.totalSurChargeAmount = totalSurChargeAmount;
	}

	/**
	 * @return the totalModificationAmount
	 */
	public BigDecimal getTotalModificationAmount() {
		return totalModificationAmount;
	}

	/**
	 * @param totalModificationAmount
	 *            the totalModificationAmount to set
	 */
	public void setTotalModificationAmount(BigDecimal totalModificationAmount) {
		this.totalModificationAmount = totalModificationAmount;
	}

	/**
	 * @return the totalCancelAmount
	 */
	public BigDecimal getTotalCancelAmount() {
		return totalCancelAmount;
	}

	/**
	 * @param totalCancelAmount
	 *            the totalCancelAmount to set
	 */
	public void setTotalCancelAmount(BigDecimal totalCancelAmount) {
		this.totalCancelAmount = totalCancelAmount;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the estTimeDepartureLocal
	 */
	public String getEstTimeDepartureLocal() {
		return estTimeDepartureLocal;
	}

	/**
	 * @param estTimeDepartureLocal
	 *            the estTimeDepartureLocal to set
	 */
	public void setEstTimeDepartureLocal(String estTimeDepartureLocal) {
		this.estTimeDepartureLocal = estTimeDepartureLocal;
	}

}
