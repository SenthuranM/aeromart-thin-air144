/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;

/**
 * DTO for capturing flight and inventory summary information
 * 
 * @author Byorn
 * @author MN
 */
public class FlightInventorySummaryDTO implements Serializable {

	private static final long serialVersionUID = -9018902704666494748L;

	private int flightId;

	private List<FCCInventorySummaryDTO> fccInventorySummaryDTO;

	private String departure;

	private Date departureDateTime;

	private String arrival;

	private Date arrivalDateTime;

	// private List segments;

	private final Map<Integer, String> segmentMap = new HashMap<Integer, String>();

	private String tailNumber;

	private String modelNumber;

	private String flightNumber;

	private int numberOfLegs = -1;

	private List<String> viaPoints;

	private String flightStatus;

	private Integer overlappingFlightId;

	private String longestSegmentCode;

	private String availableSeatKilometers;

	private Integer scheduleId;

	private Date departureDateTimeLocal;

	private Date arrivalDateTimeLocal;

	private Date departureDateTimeZulu;

	private Date arrivalDateTimeZulu;
	
	private String csOcCarrierCode;

	/**
	 * @return Returns the fccInventorySummaryDTO.
	 */
	public List<FCCInventorySummaryDTO> getFccInventorySummaryDTO() {
		return fccInventorySummaryDTO;
	}

	/**
	 * @param fccInventorySummaryDTO
	 *            The fccInventorySummaryDTO to set.
	 */
	public void setFccInventorySummaryDTO(List<FCCInventorySummaryDTO> fccInventorySummaryDTO) {
		this.fccInventorySummaryDTO = fccInventorySummaryDTO;
	}

	/**
	 * @return Returns the flightId.
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getTailNumber() {
		return tailNumber;
	}

	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

	public Integer getOverlappingFlightId() {
		return overlappingFlightId;
	}

	public void setOverlappingFlightId(Integer overlappingFlightId) {
		this.overlappingFlightId = overlappingFlightId;
	}

	/**
	 * 
	 * @return List having segment codes
	 */
	public List getSegments() {
		if (getSegmentMap() != null) {
			List<Object> segments = new ArrayList<Object>();
			Iterator segCodes = getSegmentMap().values().iterator();
			while (segCodes.hasNext()) {
				segments.add(segCodes.next());
			}
			return segments;
		}
		return null;
	}

	// /**
	// *
	// * @param segments
	// */
	// public void setSegements(List segments) {
	// this.segments = segments;
	// }

	public List<String> getViaPoints() {
		if (viaPoints == null) {
			viaPoints = new ArrayList<String>();
			if (getLongestSegmentCode() != null) {
				String[] stationCodes = StringUtils.split(getLongestSegmentCode(), "/");
				if (stationCodes != null && stationCodes.length > 2) {

					for (int i = 1; i < stationCodes.length; i++) {
						if (i != (stationCodes.length - 1)) {
							viaPoints.add(stationCodes[i]);
						}
					}
				}
			}
		}

		return viaPoints;
	}

	public int getNumberOfLegs() {
		if (numberOfLegs == -1 && getLongestSegmentCode() != null) {
			String[] stationCodes = StringUtils.split(getLongestSegmentCode(), "/");
			if (stationCodes != null) {
				numberOfLegs = stationCodes.length - 1;
			}
		}
		return numberOfLegs;// error
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	public String getLongestSegmentCode() {
		return longestSegmentCode;
	}

	public void setLongestSegmentCode(String longestSegmentCode) {
		this.longestSegmentCode = longestSegmentCode;
	}

	public String getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	public void setAvailableSeatKilometers(String availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	public Collection getFCCSegSummaryDTO(String cabinClassCode) {
		if (getFccInventorySummaryDTO() != null) {
			Iterator fccInvIt = getFccInventorySummaryDTO().iterator();
			while (fccInvIt.hasNext()) {
				FCCInventorySummaryDTO fccInv = (FCCInventorySummaryDTO) fccInvIt.next();
				if (fccInv.getCabinClassCode().equals(cabinClassCode)) {
					return fccInv.getFccSegInventorySummary();
				}
			}
		}
		return null;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		String t = "t";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
		summary.append(nl + "==========================Flight Information==========================" + nl);
		summary.append(t + "FlightId			= " + getFlightId() + nl);
		summary.append(t + "Flight Number		= " + getFlightNumber() + nl);
		summary.append(t + "Flight Status		= " + getFlightStatus() + nl);
		summary.append(t + "Origin				= " + getDeparture() + nl);
		summary.append(t + "Destination			= " + getArrival() + nl);
		summary.append(t + "Depature time       = "
				+ ((getDepartureDateTime() != null) ? dateFormat.format(getDepartureDateTime()) : "") + nl);
		summary.append(t + "Arrival time        = "
				+ ((getArrivalDateTime() != null) ? dateFormat.format(getArrivalDateTime()) : "") + nl);
		summary.append(t + "Via points 			= ");
		if (getViaPoints() != null) {
			List vias = getViaPoints();
			for (int i = 0; i < vias.size(); i++) {
				summary.append(vias.get(i)).append(" ");
			}
		}
		summary.append(nl);
		summary.append(t + "Number of Legs		= " + getNumberOfLegs() + nl);
		summary.append(nl + "======================================================================" + nl);
		Iterator fccInvIt = getFccInventorySummaryDTO().iterator();
		while (fccInvIt.hasNext()) {
			summary.append(((FCCInventorySummaryDTO) fccInvIt.next()).getSummary());
		}
		return summary;
	}

	// public Map getSegmentMap() {
	// Map segmentsMap = new HashMap();
	// if (getSegments() != null){
	// Iterator segIt = getSegments().iterator();
	// while (segIt.hasNext()){
	// FCCSegInventoryDTO segDTO = (FCCSegInventoryDTO) segIt.next();
	// segmentsMap.put(new Integer(segDTO.getFlightId()), segDTO.getSegmentCode());
	// }
	// }
	// return segmentsMap;
	// }

	public Map getSegmentMap() {
		return this.segmentMap;
	}

	public void addSegment(Integer flightSegId, String segmentCode) {
		this.segmentMap.put(flightSegId, segmentCode);
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}
}