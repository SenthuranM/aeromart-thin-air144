package com.isa.thinair.reporting.api.service;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.reporting.core.util.InternalConstants;

public class ReportingUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(InternalConstants.MODULE_NAME);
	}
}
