package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.List;

public class ClassofServiceDTO implements Serializable {

	private static final long serialVersionUID = -8499107818676114224L;

	private String cosClass;

	private List<BCDetailsDTO> bcDetailLst;

	public String getCosClass() {
		return cosClass;
	}

	public void setCosClass(String cosClass) {
		this.cosClass = cosClass;
	}

	public List<BCDetailsDTO> getBcDetailLst() {
		return bcDetailLst;
	}

	public void setBcDetailLst(List<BCDetailsDTO> bcDetailLst) {
		this.bcDetailLst = bcDetailLst;
	}

}
