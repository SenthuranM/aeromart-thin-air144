package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class SalesRevenueTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1762554232946538259L;
	public final static String REV_TYPE_FARE = "fare";
	public final static String REV_TYPE_CHARGE = "charge";

	private String sourceOfRevenue;
	private String amount;
	private String period;
	private String month;

	public SalesRevenueTO() {
		super();
	}

	private String year;

	public String getSourceOfRevenue() {
		return sourceOfRevenue;
	}

	public void setSourceOfRevenue(String sourceOfRevenue) {
		this.sourceOfRevenue = sourceOfRevenue;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}
