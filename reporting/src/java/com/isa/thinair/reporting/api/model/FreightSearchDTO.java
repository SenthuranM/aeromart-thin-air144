package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * Criteria for searching flights for inventory management
 * 
 * @author Duminda
 */
public class FreightSearchDTO implements Serializable {

	private static final long serialVersionUID = -4762035802278437676L;

	private String flightdate;

	private String flightnumber;

	private String segmentcode;

	private Double fare;

	private Double taxsurcharges;

	private Double excessWeight;

	private Double excessCollection;

	private Double cargoWeight;

	private Double cargoCollection;

	/**
	 * @return Returns the cargoCollection.
	 */
	public Double getCargoCollection() {
		return cargoCollection;
	}

	/**
	 * @param cargoCollection
	 *            The cargoCollection to set.
	 */
	public void setCargoCollection(Double cargoCollection) {
		this.cargoCollection = cargoCollection;
	}

	/**
	 * @return Returns the cargoWeight.
	 */
	public Double getCargoWeight() {
		return cargoWeight;
	}

	/**
	 * @param cargoWeight
	 *            The cargoWeight to set.
	 */
	public void setCargoWeight(Double cargoWeight) {
		this.cargoWeight = cargoWeight;
	}

	/**
	 * @return Returns the excessCollection.
	 */
	public Double getExcessCollection() {
		return excessCollection;
	}

	/**
	 * @param excessCollection
	 *            The excessCollection to set.
	 */
	public void setExcessCollection(Double excessCollection) {
		this.excessCollection = excessCollection;
	}

	/**
	 * @return Returns the excessWeight.
	 */
	public Double getExcessWeight() {
		return excessWeight;
	}

	/**
	 * @param excessWeight
	 *            The excessWeight to set.
	 */
	public void setExcessWeight(Double excessWeight) {
		this.excessWeight = excessWeight;
	}

	/**
	 * @return Returns the fare.
	 */
	public Double getFare() {
		return fare;
	}

	/**
	 * @param fare
	 *            The fare to set.
	 */
	public void setFare(Double fare) {
		this.fare = fare;
	}

	/**
	 * @return Returns the flightdate.
	 */
	public String getFlightdate() {
		return flightdate;
	}

	/**
	 * @param flightdate
	 *            The flightdate to set.
	 */
	public void setFlightdate(String flightdate) {
		this.flightdate = flightdate;
	}

	/**
	 * @return Returns the flightnumber.
	 */
	public String getFlightnumber() {
		return flightnumber;
	}

	/**
	 * @param flightnumber
	 *            The flightnumber to set.
	 */
	public void setFlightnumber(String flightnumber) {
		this.flightnumber = flightnumber;
	}

	/**
	 * @return Returns the segmentcode.
	 */
	public String getSegmentcode() {
		return segmentcode;
	}

	/**
	 * @param segmentcode
	 *            The segmentcode to set.
	 */
	public void setSegmentcode(String segmentcode) {
		this.segmentcode = segmentcode;
	}

	/**
	 * @return Returns the taxsurcharges.
	 */
	public Double getTaxsurcharges() {
		return taxsurcharges;
	}

	/**
	 * @param taxsurcharges
	 *            The taxsurcharges to set.
	 */
	public void setTaxsurcharges(Double taxsurcharges) {
		this.taxsurcharges = taxsurcharges;
	}

}
