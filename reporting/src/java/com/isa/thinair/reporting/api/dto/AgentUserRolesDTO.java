package com.isa.thinair.reporting.api.dto;

import java.io.Serializable;

public class AgentUserRolesDTO implements Serializable {

	private static final long serialVersionUID = -1L;
	
	String privilege;
	String role;


	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPrivilege() {
		return privilege;
	}

	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	
	
	
}