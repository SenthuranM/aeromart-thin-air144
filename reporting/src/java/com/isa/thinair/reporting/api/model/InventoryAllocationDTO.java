package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * @author Ruckman Colins S.
 * @since Feb 15, 2008 , 1:28:18 PM.
 */
public class InventoryAllocationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6518678431522781410L;

	private String markettingCarrierCode;

	private int allocation;

	private int sold_seat;

	private int onHoldSeat;

	private int availableSeat;

	public int getAllocation() {
		return allocation;
	}

	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}

	public int getAvailableSeat() {
		return availableSeat;
	}

	public void setAvailableSeat(int availableSeat) {
		this.availableSeat = availableSeat;
	}

	public String getMarkettingCarrierCode() {
		return markettingCarrierCode;
	}

	public void setMarkettingCarrierCode(String markettingCarrierCode) {
		this.markettingCarrierCode = markettingCarrierCode;
	}

	public int getOnHoldSeat() {
		return onHoldSeat;
	}

	public void setOnHoldSeat(int onHoldSeat) {
		this.onHoldSeat = onHoldSeat;
	}

	public int getSold_seat() {
		return sold_seat;
	}

	public void setSold_seat(int sold_seat) {
		this.sold_seat = sold_seat;
	}

}
