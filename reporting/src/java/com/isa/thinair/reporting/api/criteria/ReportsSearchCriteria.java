/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.api.criteria;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vinothini
 */
public class ReportsSearchCriteria implements Serializable {

	private static final long serialVersionUID = -7517461667068550138L;

	public static final String REVENUE_TYPE_FLOWN = "flown";
	public static final String REVENUE_TYPE_FORWARD = "forward";
	public static final String REPORT_OPTION_RESERVATION = "reservation";
	public static final String REPORT_OPTION_TAXES = "taxes";
	public static final String REPORT_OPTION_CREDIT_AND_DEBIT = "creditAndDebit";

	public static final String REPORT_OPTION_DEBIT_ONLY = "debitOnly";
	public static final String REPORT_OPTION_CREDIT_ONLY = "creditOnly";

	public static final String REVENUE_TAX_REPORT_OPTION_DETAIL = "RTREOPRT_DETAIL"; // CSV
																						// only
	public static final String REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY = "RTREOPRT_SEGMENT_SUMMARY";
	public static final String REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY = "RTREOPRT_CHARGES_SUMMARY";
	public static final String REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY = "RTREOPRT_AGENT_SUMMARY"; // CSV
																									// only

	public static final String BOOKED_SSR_DETAILS = "BSDREPORT";
	public static final String REVENUE_TAX_REPORT = "RTREPORT";
	public static final String FORWARD_BOOOKING_REPORT = "FBREPORT";
	public static final String ORIGIN_DESTINATION_REPORT = "ODREPORT";
	public static final String INBOUND_CONNECTION_REPORT = "ICREPORT";

	public static final String MODE_OF_PAYMENT_REPORT_BYAGENT = "modeOfPaymentByAgent";
	public static final String MODE_OF_PAYMENT_REPORT_BYPAYMENT_SUMMARY = "modeOfPaymentByPaymentSummary";
	public static final String MODE_OF_PAYMENT_REPORT_BYPAYMENT_DETAILS = "modeOfPaymentByPaymentDetails";

	public static final String MODE_OF_PAYMENT_WEB = "WEB";
	public static final String MODE_OF_PAYMENT_ALL = "ALL";
	public static final String MODE_OF_PAYMENT_AGENT = "AGENT";

	public static final String MODE_OF_PAYMENT_SUMMARY = "PAYMENT_SUMMARY";
	public static final String MODE_OF_PAYMENT_DETAIL = "PAYMENT_DETAIL";
	public static final String MODE_OF_PAYMENT_ONACCOUNT_DETAIL = "PAYMENT_ONACCOUNT_DETAIL";
	public static final String MODE_OF_PAYMENT_CREDIT_DETAIL = "PAYMENT_CREDIT_DETAIL";
	public static final String MODE_OF_PAYMENT_LOYALTY_DETAIL = "PAYMENT_LOYALTY_DETAIL";
	public static final String MODE_OF_PAYMENT_AGENT_SUMMARY = "PAYMENT_AGENT_SUMMARY";
	public static final String MODE_OF_PAYMENT_AGENT_DETAILS = "PAYMENT_AGENT_DETAILS";
	public static final String MODE_OF_PAYMENT_AGENT_NAME_SUMMARY = "PAYMENT_AGENT_NAME_SUMMARY"; // agent
																									// name
	public static final String MODE_OF_PAYMENT_EXTERNAL_SUMMARY = "PAYMENT_EXTERNAL_SUMMARY";
	public static final String MODE_OF_PAYMENT_CURRENCY_SUMMARY = "CURRENCY_SUMMARY";
	public static final String MODE_OF_PAYMENT_CURRENCY_DETAIL = "CURRENCY_DETAIL";

	public static final String AGENT_GSA_DETAILS = "agetGsaDetails";
	public static final String AGENT_GSA_SUMMARY = "agetGsaSummary";

	public static final String POINT_REDEMPTION_DETAILS = "pointRedemptionDetails";

	public static final String MEALS_DETAILS = "mealsDetails";
	public static final String MEALS_SUMMARY = "mealsSummary";
	public static final String EXT_PMT_RECONCIL_SUMMARY = "extPmtReconcilSummary";

	public static final String USER_LIST_DETAILS = "UserListDetails";
	public static final String USER_LIST_SUMMARY = "UserListSummary";
	public static final String USER_LIST_PRIVI = "UserListPrivi";
	public static final String PRIVILEGE_DETAILS = "PrivilegeDetails";
	public static final String USER_PRIVILEGE_DETAILS = "UserPrivilegeDetails";

	public static final String COMPANY_PAYMENT_DETAILS = "COMPANY_PAYMENT_DETAIL";
	public static final String COMPANY_PAYMENT_SUMMARY = "COMPANY_PAYMENT_SUMMARY";
	public static final String COMPANY_PAYMENT_POS_SUMMARY = "COMPANY_PAYMENT_POS_SUMMARY";

	public static final String BOOKED_PAX_SEGMENT_SUMMARY = "BOOKED_PAX_SEGMENT_SUMMARY";
	public static final String BOOKED_PAX_SEGMENT_DETAIL = "BOOKED_PAX_SEGMENT_DETAIL";

	public static final String AGENT_INVOICE_DETAILS = "invoiceDetails";
	public static final String PNR_INVOICE_DETAILS = "pnrInvoiceDetails";
	public static final String AGENT_INVOICE_SUMMARY = "invoiceSummary";
	public static final String AGENT_INVOICE_EXT_DETAILS = "invoiceExtDetails";
	public static final String AGENT_TRANS_SUMMARY = "AGENT_TRANS_SUMMARY";
	public static final String AGENT_TRANS_DETAILS = "AGENT_TRANS_DETAILS";
	public static final String AGENT_TRANS_AGENT_DETAIL = "AGENT_TRANS_AGENT_DETAIL";

	public static final String FORWARD_BOOKING = "FORWARD_BOOKING";
	public static final String FORWARD_BOOKING_LASTYEAR = "FORWARD_BOOKING_LASTYEAR";
	public static final String OND_REPORT = "OND_REPORT";

	public static final String FORWARD_BOOKING_OPTION_BY_FLIGHT = "BY_FLIGHT";
	public static final String FORWARD_BOOKING_OPTION_BY_STATION = "BY_STATION";
	public static final String FORWARD_BOOKING_OPTION_BY_AGENT = "BY_AGENT";

	public static final String DATASOURCE_TYPE_DEFAULT = "DEFAULT";
	public static final String DATASOURCE_TYPE_RPT = "RPT";

	public static final String IS_CUR_REQUIRED_N = "N";
	public static final String IS_CUR_REQUIRED_Y = "Y";

	public static final String SSR_DETAIL_REPORT = "DETAIL";
	public static final String SSR_SUMMARY_REPORT = "SUMMARY";

	public static final String AGENT_SALES_REPORT = "SALE";
	public static final String AGENT_REFUND_REPORT = "REFUND";
	public static final String AGENT_MODIFY_REPORT = "MODIFY";
	public static final String AGENT_REPORT_ALL = "ALL";

	public static final String SSR_SERVICE_CAT = "1";
	public static final String AIRPORT_SERVICE_CAT = "2";
	public static final String AIRPORT_TRANSFER_CAT = "3";

	public static final String NAME_CHANGE_REPORT_TYPE_AGENT = "NCCRPT_AGENT";
	public static final String NAME_CHANGE_REPORT_TYPE_CHANNEL = "NCCRPT_CHANNEL";
	public static final String NAME_CHANGE_REPORT_TYPE_SEGMENT = "NCCRPT_SEGMENT";

	public static final String NIL_AGENT_TRANS_SUMMARY = "NIL_AGENT_TRANS_SUMMARY";
	
	public static final String BASED_ON_ORIGIN_AGENT = "ORIGIN_AGENT";
	
	public static final String BASED_ON_OWNER_AGENT = "OWNER_AGENT";
	
	public static final String BLACKLIST_PAX_REPORT = "BLACKLIST_PAX_REPORT";

	private String isCurSerRequired = IS_CUR_REQUIRED_N;

	private boolean byCustomer = false;
	private boolean bySector = false;
	private boolean byNationality = false;
	private boolean byCountryOfResidence = false;
	private boolean byFlight = false;
	private boolean byAgent = false;
	private boolean byStation = false;
	private boolean byRegion = false;
	private boolean matchCombination = false;
	private boolean byCredit = false;
	private boolean inbaseCurr = false;
	private boolean includeOnhold = false;

	private boolean detailReport = false;
	private boolean excludeZeroBalance = false;


	private String agencey;

	private Timestamp dateFrom;
	private Timestamp dateTo;

	private String dateRangeFrom;
	private String dateRangeTo;
	private String dateRange2From;
	private String dateRange2To;
	private String userId;
	private String revenueType;
	private String reportOption;
	private String customerFirstName;
	private String customerLastName;
	private String sectorFrom;
	private String sectorTo;
	private String nationality;
	private String countryOfResidence;
	private String customerId;
	private String flightNumber;
	private boolean exactFlightNumber;
	private String reportYear;
	private String reportMonth;
	private String invoicePeriod;
	private String agentCode;
	private String paymentCode;
	private String airportCode;
	private String carrierCode;
	private String countryCode;
	private String gdsCode;
	private String bcCode;
	private String eventCode;
	private String depTimeLocal;
	private String segmentCodeFrom;
	private String segmentCodeTo;
	private String cos;
	private String logicalCC;
	private String bcType;
	private String allocationType;
	private String interlineCode;
	private int gmtFromOffsetInMinutes = 0;
	private int gmtToOffsetInMinutes = 0;

	private String sortByColumnName;
	private String sortByOrder;

	private int noOfTops;
	private double noOfTopsForReport;

	private Collection<String> users;
	private Collection<String> agents;
	private Collection<String> roles;
	private Collection<String> privileges;
	private Collection<String> creditConsumeAgents;
	private Collection<String> paymentTypes;
	private Collection<String> agencyTypes;
	private Collection<String> carrierCodes;
	private Collection<String> bookingCodes;
	private Collection<String> bcCategory;
	private Collection<String> segmentCodes;
	private List<String> viaPoints;
	private List<String> stations;

	private Collection<String> salesChannels;
	private Collection<String> schemeCodes;

	private Collection<String> ssrCodes;
	private Collection<String> paxStatusList;

	private String operationType;
	private String flightStatus;
	private String scheduleStatus;
	private String buildStatus;
	private String aircraftModel;
	private String timeZone;

	private String agentType;
	private String agentName;
	private String station;
	private String charge;
	private String territory;
	private String state;
	private String reportType;
	private String currencies;
	private String chargeGrooup;
	private String originCountry;

	private String[] agentCodes;

	private String year;
	private String month;

	private int billingPeriod;
	private String entity;

	private String pnr;

	private String segment;
	private String from;
	private String to;
	private String model;

	private String conTime;

	private String status;
	private String pfsStatus;
	private String bookinClass;

	private String taskGroup;
	private String task;
	private String contentSearch;

	private String paymentSource;
	private String printDetailsOf;

	private String departureDateRangeFrom;
	private String departureDateRangeTo;
	private String arrivalDateRangeFrom;
	private String arrivalDateRangeTo;

	private Set<String> paxStatus;

	private String paymentGW;

	private String mealsName;
	private String paxCategory;

	private String ssrCategory;
	private String ssrChargeId;
	private Integer noOfAttempts;

	private String invoiceNumber;
	private String accountNumber;
	private String iBEVisibililty;
	private String xBEVisibililty;

	private String reqReportFormat;

	private String fareRuleCode;
	private Integer fareRuleId;

	private String fromDate;
	private String toDate;
	private String date;
	private String flightType;

	private String fromTktNumber;
	private String toTktNumber;
	private String blacklistType;

	private boolean paymentTypeEnhancedView;
	private boolean displayAgentAdditionalInfo;
	private boolean modificationDetails;
	private boolean isSales;
	private boolean isRefund;
	private boolean isMealSelected;
	private boolean isSeatSelected;
	private boolean isBaggageSeleted;
	private boolean isFlexiSelected;
	private boolean isHalaSerivceSelected;
	private boolean isInsuranceSelected;
	private boolean isBusSelected;
	private boolean overriddenDiscountDataOnly;
	private boolean normalDiscountDataOnly;
	private boolean filterByConfirmed;
	private boolean includeCancelledDataToReport;
	private String bookingDateFrom;
	private String bookingDateTo;

	private String showExternal;

	private Collection<Integer> nominalCodes;

	private Collection<Integer> extPaymentNominalCodes;

	// for pax credit settle when utilize other carrier credit
	private Collection<Integer> paxCreditNominalCodes;
	private Collection<String> externalProducts;
	private Collection<Integer> externalProductNominalCodes;

	private boolean reportViewNew;

	private String dateRangeArray[][] = null;

	private Map<String, String[][]> dateRangeMap = null;

	private String region;

	//
	private String originAirport;
	private String destinationAirport;

	private String originMinConncetTime;
	private String originMaxConncetTime;
	private String destinationMinConncetTime;
	private String destinationMaxConncetTime;

	private String journeyType;

	private List<String> selectedChargeCodes;

	private Collection<String> flightNoCollection;
	// Fare Details Report,Flight no Collection
	private Collection<String> bcTypeCollection;
	// Fare Details Report,Booking class Type Collection
	private Collection<String> allocTypeCollection;
	// Fare Details Report,Allocation Type Collection

	private String paramSelectionType;
	private List<String> paramDataList;

	private String searchFlightType;

	private boolean extendOnhold = false;

	private String fareBasisCode;

	private String addressCity;

	private String quantifier;

	private String ffpNumber;

	private int count;

	private String chargeCode;

	private String category;

	private boolean onlyChargesWithoutRate;

	private List<String> selectedCountries = new ArrayList<String>();

	private List<String> selectedSegments = new ArrayList<String>();

	private List<String> selectedPOSs = new ArrayList<String>();

	private String fareDiscountCode = "All";

	private String salesAmount;

	private boolean requestAllPassengers = false;

	private List<String> lstmealCodes;

	private List<String> lstSeatMapCodes;

	private List<String> lstInsuranceCodes;

	private List<String> lstBaggageCodes;

	private List<String> lstSSRCodes;

	private List<String> lstHALACodes;

	private List<String> lstAptTransferCodes;

	private boolean showInLocalTime;

	private Set<String> promotionTypes;

	private boolean byBookingStatus;

	private String bookingStatus;

	private boolean includeAdvanceCCDetails;

	private Set<String> bspTnxTypes;

	private Integer salesChannelCode;

	private boolean consideringFixedSeats;

	private Set<Integer> bundledFarePeriodIds;

	private boolean isSelectedAllAgents;

	private String name;

	private String mobileNumber;

	private String ffid;

	private String productCode;

	private boolean baseCurrencySelected;

	private String flightDateRangeFrom;

	private String flightDateRangeTo;

	private String processType;

	// for issued voucher detail reports
	private String voucherName;

	private String firstName;

	private String lastName;

	private String voucherStatus;
	
	// Mahan force cnf rept criteria	
	private boolean onlyTotalAmountToPay = false;
	private int stateId;
	
	private boolean includePaidStatusFilter = false;
	private String stateCode;
	
	private String gsaCode;
	
	private boolean isOnlyReportingAgents;
		
	private String promoCriteriaID ; 
	
	private String basedOnAgent = BASED_ON_ORIGIN_AGENT;
	
	private int reservationFlow;

	public List<String> getSelectedPOSs() {
		return selectedPOSs;
	}

	public void setSelectedPOSs(List<String> selectedPOSs) {
		this.selectedPOSs = selectedPOSs;
	}

	public List<String> getSelectedSegments() {
		return selectedSegments;
	}

	public void setSelectedSegments(List<String> selectedSegments) {
		this.selectedSegments = selectedSegments;
	}

	public List<String> getSelectedCountries() {
		return selectedCountries;
	}

	public void setSelectedCountries(List<String> selectedCountries) {
		this.selectedCountries = selectedCountries;
	}

	public String getParamSelectionType() {
		return paramSelectionType;
	}

	public void setParamSelectionType(String paramSelectionType) {
		this.paramSelectionType = paramSelectionType;
	}

	public List<String> getParamDataList() {
		return paramDataList;
	}

	public void setParamDataList(List<String> paramDataList) {
		this.paramDataList = paramDataList;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the fareRuleId
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            the fareRuleId to set
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getSsrChargeId() {
		return ssrChargeId;
	}

	public void setSsrChargeId(String ssrChargeId) {
		this.ssrChargeId = ssrChargeId;
	}

	public String getSsrCategory() {
		return ssrCategory;
	}

	public void setSsrCategory(String ssrCategory) {
		this.ssrCategory = ssrCategory;
	}

	public String getPaxCategory() {
		return paxCategory;
	}

	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}

	public Set<String> getPaxStatus() {
		if (paxStatus == null) {
			paxStatus = new HashSet<String>();
		}
		return paxStatus;
	}

	public String getPaymentGW() {
		return paymentGW;
	}

	public void setPaymentGW(String paymentGW) {
		this.paymentGW = paymentGW;
	}

	/** Choose DEFAULT or RPT datasource */
	private String dataSoureType = DATASOURCE_TYPE_DEFAULT;

	/**
	 * @return - Returns the dateRangeFrom.
	 */
	public String getDateRangeFrom() {
		return this.dateRangeFrom;
	}

	/**
	 * @param dateRangeFrom
	 *            - The dateRangeFrom to set.
	 */
	public void setDateRangeFrom(String dateRangeFrom) {
		this.dateRangeFrom = dateRangeFrom;
	}

	/**
	 * additional date range form getter
	 * 
	 * @return
	 */
	public String getDateRange2From() {
		return dateRange2From;
	}

	/**
	 * additional date range form setter
	 * 
	 * @param dateRange2From
	 */
	public void setDateRange2From(String dateRange2From) {
		this.dateRange2From = dateRange2From;
	}

	/**
	 * additional date range to getter
	 * 
	 * @return
	 */
	public String getDateRange2To() {
		return dateRange2To;
	}

	/**
	 * additional date range to setter
	 * 
	 * @param dateRange2To
	 */
	public void setDateRange2To(String dateRange2To) {
		this.dateRange2To = dateRange2To;
	}

	/**
	 * @return - Returns the dateRangeTo.
	 */
	public String getDateRangeTo() {
		return this.dateRangeTo;
	}

	/**
	 * @param dateRangeTo
	 *            - The dateRangeTo to set.
	 */
	public void setDateRangeTo(String dateRangeTo) {
		this.dateRangeTo = dateRangeTo;
	}

	/**
	 * @return - Returns the users collection.
	 */
	public Collection<String> getUsers() {
		return this.users;
	}

	/**
	 * @param users
	 *            - The users to set.
	 */
	public void setUsers(Collection<String> users) {
		this.users = users;
	}

	/**
	 * @return - Returns the userId
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * @param userId
	 *            - The userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return - Returns the revenueType
	 */
	public String getRevenueType() {
		return this.revenueType;
	}

	/**
	 * @param revenueType
	 *            - The revenueType to set
	 */
	public void setRevenueType(String revenueType) {
		this.revenueType = revenueType;
	}

	/**
	 * @return - Returns the reportOption
	 */
	public String getReportOption() {
		return this.reportOption;
	}

	/**
	 * @param reportOption
	 *            - The reportOption to set
	 */
	public void setReportOption(String reportOption) {
		this.reportOption = reportOption;
	}

	/**
	 * @return - Returns the agents collection.
	 */
	public Collection<String> getAgents() {
		return this.agents;
	}

	/**
	 * @param agents
	 *            - The agents to set.
	 */
	public void setAgents(Collection<String> agents) {
		this.agents = agents;
	}

	/**
	 * @return the creditConsumeAgents
	 */
	public Collection<String> getCreditConsumeAgents() {
		return creditConsumeAgents;
	}

	/**
	 * @param creditConsumeAgents
	 *            the creditConsumeAgents to set
	 */
	public void setCreditConsumeAgents(Collection<String> creditConsumeAgents) {
		this.creditConsumeAgents = creditConsumeAgents;
	}

	/**
	 * @return - Returns the paymentType collection.
	 */
	public Collection<String> getPaymentTypes() {
		return this.paymentTypes;
	}

	/**
	 * @param paymentTypes
	 *            - The paymentTypes to set.
	 */
	public void setPaymentTypes(Collection<String> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	/**
	 * @return - Returns true if the customer option is selected
	 */
	public boolean isByCustomer() {
		return this.byCustomer;
	}

	/**
	 * @param byCustomer
	 *            - Customer option to set
	 */
	public void setByCustomer(boolean byCustomer) {
		this.byCustomer = byCustomer;
	}

	/**
	 * @return - Returns true if the sector option is selected
	 */
	public boolean isBySector() {
		return this.bySector;
	}

	/**
	 * @param bySector
	 *            - Sector option to set
	 */
	public void setBySector(boolean bySector) {
		this.bySector = bySector;
	}

	/**
	 * @return - Returns the customerFirstName
	 */
	public String getCustomerFirstName() {
		return this.customerFirstName;
	}

	/**
	 * @param customerFirstName
	 *            - The customerFirstName to set
	 */
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	/**
	 * @return - Returns the customerLastName
	 */
	public String getCustomerLastName() {
		return this.customerLastName;
	}

	/**
	 * @param customerLastName
	 *            - The customerLastName to set
	 */
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	/**
	 * @return - Returns the sectorFrom
	 */
	public String getSectorFrom() {
		return this.sectorFrom;
	}

	/**
	 * @param sectorFrom
	 *            - The sectorFrom to set
	 */
	public void setSectorFrom(String sectorFrom) {
		this.sectorFrom = sectorFrom;
	}

	/**
	 * @return - Returns the sectorTo
	 */
	public String getSectorTo() {
		return this.sectorTo;
	}

	/**
	 * @param sectorTo
	 *            - The sectorTo to set
	 */
	public void setSectorTo(String sectorTo) {
		this.sectorTo = sectorTo;
	}

	/**
	 * @return - Returns the nationality
	 */
	public String getNationality() {
		return this.nationality;
	}

	/**
	 * @param nationality
	 *            - The nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return - Returns the countryOfResidence
	 */
	public String getCountryOfResidence() {
		return this.countryOfResidence;
	}

	/**
	 * @param countryOfResidence
	 *            - The countryOfResidence to set
	 */
	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	/**
	 * @return - Returns the customerId
	 */
	public String getCustomerId() {
		return this.customerId;
	}

	/**
	 * @param customerId
	 *            - The customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return - Returns the flightNumber
	 */
	public String getFlightNumber() {
		return this.flightNumber;
	}

	/**
	 * @param flightNumber
	 *            - The flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return - Returns true if the nationality option is selected
	 */
	public boolean isByNationality() {
		return this.byNationality;
	}

	/**
	 * @param byNationality
	 *            - Nationality option to set
	 */
	public void setByNationality(boolean byNationality) {
		this.byNationality = byNationality;
	}

	/**
	 * @return - Returns true if the country of residence option is selected
	 */
	public boolean isByCountryOfResidence() {
		return this.byCountryOfResidence;
	}

	/**
	 * @param byCountryOfResidence
	 *            - Country of residence option to set
	 */
	public void setByCountryOfResidence(boolean byCountryOfResidence) {
		this.byCountryOfResidence = byCountryOfResidence;
	}

	/**
	 * @return - Returns true if the flight option is selected
	 */
	public boolean isByFlight() {
		return this.byFlight;
	}

	/**
	 * @param byFlight
	 *            - Flight option to set
	 */
	public void setByFlight(boolean byFlight) {
		this.byFlight = byFlight;
	}

	/**
	 * 
	 * @param byAgent
	 *            - Return true if the agent option is selected
	 */
	public boolean isByAgent() {
		return byAgent;
	}

	/**
	 * 
	 * @param byAgent
	 *            - Agent option to set
	 */
	public void setByAgent(boolean byAgent) {
		this.byAgent = byAgent;
	}

	/**
	 * 
	 * @param - Return true if the station option is selected
	 */
	public boolean isByStation() {
		return byStation;
	}

	/**
	 * 
	 * @param byStation
	 *            - station option to set
	 */
	public void setByStation(boolean byStation) {
		this.byStation = byStation;
	}

	/**
	 * @return - Returns the noOfTops
	 */
	public int getNoOfTops() {
		return this.noOfTops;
	}

	/**
	 * @param noOfTops
	 *            - The noOfTops to set
	 */
	public void setNoOfTops(int noOfTops) {
		this.noOfTops = noOfTops;
	}

	public double getNoOfTopsForReport() {
		return this.noOfTopsForReport;
	}

	/**
	 * @param noOfTops
	 *            - The noOfTops to set
	 */
	public void setNoOfTopsForReport(double noOfTops) {
		this.noOfTopsForReport = noOfTops;
	}

	/**
	 * @return - Returns the reportMonth
	 */
	public String getReportMonth() {
		return this.reportMonth;
	}

	/**
	 * @param reportMonth
	 *            - The reportMonth to set
	 */
	public void setReportMonth(String reportMonth) {
		this.reportMonth = reportMonth;
	}

	/**
	 * @return - Returns the reportYear
	 */
	public String getReportYear() {
		return this.reportYear;
	}

	/**
	 * @param reportYear
	 *            - The reportYear to set
	 */
	public void setReportYear(String reportYear) {
		this.reportYear = reportYear;
	}

	/**
	 * @return - Returns the invoicePeriod
	 */
	public String getInvoicePeriod() {
		return this.invoicePeriod;
	}

	/**
	 * @param invoicePeriod
	 *            - The invoicePeriod to set
	 */
	public void setInvoicePeriod(String invoicePeriod) {
		this.invoicePeriod = invoicePeriod;
	}

	/**
	 * @return - Returns the agentCode
	 */
	public String getAgentCode() {
		return this.agentCode;
	}

	/**
	 * @param agentCode
	 *            - The agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return - Returns the paymentCode
	 */
	public String getPaymentCode() {
		return this.paymentCode;
	}

	/**
	 * @param paymentCode
	 *            - The paymentCode to set
	 */
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	/**
	 * @return - Returns the airportCode
	 */
	public String getAirportCode() {
		return this.airportCode;
	}

	/**
	 * @param airportCode
	 *            - The airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return - Returns the agencyTypes collection.
	 */
	public Collection<String> getAgencyTypes() {
		return this.agencyTypes;
	}

	/**
	 * @param agencyTypes
	 *            - The agencyTypes to set.
	 */
	public void setAgencyTypes(Collection<String> agencyTypes) {
		this.agencyTypes = agencyTypes;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return - Returns the operationType
	 */
	public String getOperationType() {
		return this.operationType;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	/**
	 * @return - Returns the flightStatus
	 */
	public String getFlightStatus() {
		return this.flightStatus;
	}

	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	/**
	 * @return - Returns the scheduleStatus
	 */
	public String getScheduleStatus() {
		return this.scheduleStatus;
	}

	public void setBuildStatus(String buildStatus) {
		this.buildStatus = buildStatus;
	}

	/**
	 * @return - Returns the buildStatus
	 */
	public String getBuildStatus() {
		return this.buildStatus;
	}

	public void setAircraftModel(String aircraftModel) {
		this.aircraftModel = aircraftModel;
	}

	/**
	 * @return - Returns the aircraftModel
	 */
	public String getAircraftModel() {
		return this.aircraftModel;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return - Returns the timeZone
	 */
	public String getTimeZone() {
		return this.timeZone;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	/**
	 * @return - Returns the agentType
	 */
	public String getAgentType() {
		return this.agentType;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return - Returns the agentName
	 */
	public String getAgentName() {
		return this.agentName;
	}

	public void setStation(String station) {
		this.station = station;
	}

	/**
	 * @return - Returns the station
	 */
	public String getStation() {
		return this.station;
	}

	/**
	 * @return the charge
	 */
	public String getCharge() {
		return charge;
	}

	/**
	 * @param charge
	 *            the charge to set
	 */
	public void setCharge(String charge) {
		this.charge = charge;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	/**
	 * @return - Returns the territory
	 */
	public String getTerritory() {
		return this.territory;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return - Returns the territory
	 */
	public String getReportType() {
		return this.reportType;
	}

	public void setAgentCodes(String[] agentCodes) {
		this.agentCodes = agentCodes;
	}

	/**
	 * @return - Returns the agent codes
	 */
	public String[] getAgentCodes() {
		return this.agentCodes;
	}

	/**
	 * @return - Returns the year
	 */
	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return - Returns the month
	 */
	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return - Returns the month
	 */
	public int getBillingPeriod() {
		return this.billingPeriod;
	}

	public void setBillingPeriod(int billingPeriod) {
		this.billingPeriod = billingPeriod;
	}
	
	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	/**
	 * @return - Returns the segment
	 */
	public String getSegment() {
		return this.segment;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return - Returns the from
	 */
	public String getFrom() {
		return this.from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return - Returns the to
	 */
	public String getTo() {
		return this.to;
	}

	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return - Returns the model
	 */
	public String getModel() {
		return this.model;
	}

	/**
	 * @return Returns the viaPoints.
	 */
	public List<String> getViaPoints() {
		return viaPoints;
	}

	/**
	 * @param viaPoints
	 *            The viaPoints to set.
	 */
	public void setViaPoints(List<String> viaPoints) {
		this.viaPoints = viaPoints;
	}

	/**
	 * @return Returns the matchCombination.
	 */
	public boolean isMatchCombination() {
		return matchCombination;
	}

	/**
	 * @param matchCombination
	 *            The matchCombination to set.
	 */
	public void setMatchCombination(boolean matchCombination) {
		this.matchCombination = matchCombination;
	}

	/**
	 * @return Returns the dateFrom.
	 */
	public Timestamp getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom
	 *            The dateFrom to set.
	 */
	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return Returns the dateTo.
	 */
	public Timestamp getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo
	 *            The dateTo to set.
	 */
	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}

	public void setAgencey(String agencey) {
		this.agencey = agencey;
	}

	public String getAgencey() {
		return agencey;
	}

	/**
	 * @return Returns the conTime.
	 */
	public String getConTime() {
		return conTime;
	}

	/**
	 * @param conTime
	 *            The conTime to set.
	 */
	public void setConTime(String conTime) {
		this.conTime = conTime;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	/**
	 * @param bc
	 *            The booking class to set.
	 */
	public void setBookinClass(String bc) {
		this.bookinClass = bc;
	}

	/**
	 * @return Returns bookingClass the Booking Class.
	 */
	public String getBookingClass() {
		return bookinClass;
	}

	/**
	 * @return Returns the dataSoureType.
	 */
	public String getDataSoureType() {
		return dataSoureType;
	}

	/**
	 * @param dataSoureType
	 *            The dataSoureType to set.
	 */
	public void setDataSoureType(String dataSoureType) {
		this.dataSoureType = dataSoureType;
	}

	public List<String> getStations() {
		return stations;
	}

	public void setStations(List<String> stations) {
		this.stations = stations;
	}

	public String getContentSearch() {
		return contentSearch;
	}

	public void setContentSearch(String contentSearch) {
		this.contentSearch = contentSearch;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getTaskGroup() {
		return taskGroup;
	}

	public void setTaskGroup(String taskGroup) {
		this.taskGroup = taskGroup;
	}

	public String getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(String paymentSource) {
		this.paymentSource = paymentSource;
	}

	/**
	 * @return Returns the carrierCodes.
	 */
	public Collection<String> getCarrierCodes() {
		return carrierCodes;
	}

	/**
	 * @param carrierCodes
	 *            The carrierCodes to set.
	 */
	public void setCarrierCodes(Collection<String> carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	/**
	 * @return Returns the carrierCode.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @param countryCode
	 *            The country Code.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return The country code.
	 */
	public String getCountryCode() {
		return countryCode;
	}

	public String getPrintDetailsOf() {
		return printDetailsOf;
	}

	public void setPrintDetailsOf(String printDetailsOf) {
		this.printDetailsOf = printDetailsOf;
	}

	/**
	 * @return Returns the excludeZeroBalance.
	 */
	public boolean isExcludeZeroBalance() {
		return excludeZeroBalance;
	}

	/**
	 * @param excludeZeroBalance
	 *            The excludeZeroBalance to set.
	 */
	public void setExcludeZeroBalance(boolean excludeZeroBalance) {
		this.excludeZeroBalance = excludeZeroBalance;
	}

	/**
	 * @return - Returns the gdsCode
	 */
	public String getGdsCode() {
		return gdsCode;
	}

	/**
	 * @param gdsCode
	 *            - The gdsCode to set
	 */
	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	public String getBcCode() {
		return bcCode;
	}

	public void setBcCode(String bcCode) {
		this.bcCode = bcCode;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getDepTimeLocal() {
		return depTimeLocal;
	}

	public void setDepTimeLocal(String depTimeLocal) {
		this.depTimeLocal = depTimeLocal;
	}

	public String getSegmentCodeFrom() {
		return segmentCodeFrom;
	}

	public void setSegmentCodeFrom(String segmentCodeFrom) {
		this.segmentCodeFrom = segmentCodeFrom;
	}

	public String getSegmentCodeTo() {
		return segmentCodeTo;
	}

	public void setSegmentCodeTo(String segmentCodeTo) {
		this.segmentCodeTo = segmentCodeTo;
	}

	public String getDepartureDateRangeFrom() {
		return departureDateRangeFrom;
	}

	public void setDepartureDateRangeFrom(String departureDateRangeFrom) {
		this.departureDateRangeFrom = departureDateRangeFrom;
	}

	public String getDepartureDateRangeTo() {
		return departureDateRangeTo;
	}

	public void setDepartureDateRangeTo(String departureDateRangeTo) {
		this.departureDateRangeTo = departureDateRangeTo;
	}

	public String getAllocationType() {
		return allocationType;
	}

	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	public Collection<String> getBcCategory() {
		return bcCategory;
	}

	public void setBcCategory(Collection<String> bcCategory) {
		this.bcCategory = bcCategory;
	}

	public String getBcType() {
		return bcType;
	}

	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	public Collection<String> getBookingCodes() {
		return bookingCodes;
	}

	public void setBookingCodes(Collection<String> bookingCodes) {
		this.bookingCodes = bookingCodes;
	}

	public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the mealsName
	 */
	public String getMealsName() {
		return mealsName;
	}

	/**
	 * @param mealsName
	 *            the mealsName to set
	 */
	public void setMealsName(String mealsName) {
		this.mealsName = mealsName;
	}

	public boolean isByCredit() {
		return byCredit;
	}

	public void setByCredit(boolean byCredit) {
		this.byCredit = byCredit;
	}

	public Integer getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(Integer noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

	public String getCurrencies() {
		return currencies;
	}

	public void setCurrencies(String currencies) {
		this.currencies = currencies;
	}

	public String getiBEVisibililty() {
		return iBEVisibililty;
	}

	public void setiBEVisibililty(String iBEVisibililty) {
		this.iBEVisibililty = iBEVisibililty;
	}

	public String getxBEVisibililty() {
		return xBEVisibililty;
	}

	public void setxBEVisibililty(String xBEVisibililty) {
		this.xBEVisibililty = xBEVisibililty;
	}

	public Collection<String> getSegmentCodes() {
		return segmentCodes;
	}

	public void setSegmentCodes(Collection<String> segmentCodes) {
		this.segmentCodes = segmentCodes;
	}

	public String getReqReportFormat() {
		return reqReportFormat;
	}

	public void setReqReportFormat(String reqReportFormat) {
		this.reqReportFormat = reqReportFormat;
	}

	public boolean isInbaseCurr() {
		return inbaseCurr;
	}

	public void setInbaseCurr(boolean inbaseCurr) {
		this.inbaseCurr = inbaseCurr;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public boolean isPaymentTypeEnhancedView() {
		return paymentTypeEnhancedView;
	}

	public void setPaymentTypeEnhancedView(boolean paymentTypeEnhancedView) {
		this.paymentTypeEnhancedView = paymentTypeEnhancedView;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getShowExternal() {
		return showExternal;
	}

	public void setShowExternal(String showExternal) {
		this.showExternal = showExternal;
	}

	public Collection<Integer> getNominalCodes() {
		return nominalCodes;
	}

	public void setNominalCodes(Collection<Integer> nominalCodes) {
		this.nominalCodes = nominalCodes;
	}

	public Collection<Integer> getExtPaymentNominalCodes() {
		return extPaymentNominalCodes;
	}

	public void setExtPaymentNominalCodes(Collection<Integer> extPaymentNominalCodes) {
		this.extPaymentNominalCodes = extPaymentNominalCodes;
	}

	/**
	 * @return the paxCreditNominalCodes
	 */
	public Collection<Integer> getPaxCreditNominalCodes() {
		return paxCreditNominalCodes;
	}

	/**
	 * @param paxCreditNominalCodes
	 *            the paxCreditNominalCodes to set
	 */
	public void setPaxCreditNominalCodes(Collection<Integer> paxCreditNominalCodes) {
		this.paxCreditNominalCodes = paxCreditNominalCodes;
	}

	/**
	 * @return Returns the salesChannels.
	 */
	public Collection<String> getSalesChannels() {
		return salesChannels;
	}

	/**
	 * @param salesChannels
	 *            The salesChannels to set.
	 */
	public void setSalesChannels(Collection<String> salesChannels) {
		this.salesChannels = salesChannels;
	}

	public boolean getIncldeOnhold() {
		return includeOnhold;
	}

	public void setIncldeOnhold(boolean incldeOnhold) {
		this.includeOnhold = incldeOnhold;
	}

	public boolean isDetailReport() {
		return detailReport;
	}

	public void setDetailReport(boolean detailReport) {
		this.detailReport = detailReport;
	}

	public String getIsCurSerRequired() {
		return isCurSerRequired;
	}

	public void setIsCurSerRequired(String isCurSerRequired) {
		this.isCurSerRequired = isCurSerRequired;
	}

	public Collection<String> getSchemeCodes() {
		return schemeCodes;
	}

	public void setSchemeCodes(Collection<String> schemeCodes) {
		this.schemeCodes = schemeCodes;
	}

	public String getChargeGrooup() {
		return chargeGrooup;
	}

	public void setChargeGrooup(String chargeGrooup) {
		this.chargeGrooup = chargeGrooup;
	}

	/**
	 * @return the displayAgentAdditionalInfo
	 */
	public boolean isDisplayAgentAdditionalInfo() {
		return displayAgentAdditionalInfo;
	}

	/**
	 * @param displayAgentAdditionalInfo
	 *            the displayAgentAdditionalInfo to set
	 */
	public void setDisplayAgentAdditionalInfo(boolean displayAgentAdditionalInfo) {
		this.displayAgentAdditionalInfo = displayAgentAdditionalInfo;
	}

	public boolean isReportViewNew() {
		return reportViewNew;
	}

	public void setReportViewNew(boolean reportViewNew) {
		this.reportViewNew = reportViewNew;
	}

	public boolean isByRegion() {
		return byRegion;
	}

	public void setByRegion(boolean byRegion) {
		this.byRegion = byRegion;
	}

	public String[][] getDateRangeArray() {
		return dateRangeArray;
	}

	public void setDateRangeArray(String[][] dateRangeArray) {
		this.dateRangeArray = dateRangeArray;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * @param sortByColumnName
	 *            Sets the sort by column name.
	 */
	public void setSortByColumnName(String sortByColumnName) {
		this.sortByColumnName = sortByColumnName;
	}

	/**
	 * @return the sort by column name.
	 */
	public String getSortByColumnName() {
		return sortByColumnName;
	}

	/**
	 * @param sortByOrder
	 *            The sort order
	 */
	public void setSortByOrder(String sortByOrder) {
		this.sortByOrder = sortByOrder;
	}

	/**
	 * @return The sort order.
	 */
	public String getSortByOrder() {
		return sortByOrder;
	}

	public Map<String, String[][]> getDateRangeMap() {
		return dateRangeMap;
	}

	/**
	 * @param dateRangeMap
	 *            the dateRangeMap to set
	 */
	public void setDateRangeMap(Map<String, String[][]> dateRangeMap) {
		this.dateRangeMap = dateRangeMap;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	/**
	 * @return - Returns the ssrCodes collection.
	 */
	public Collection<String> getSsrCodes() {
		return this.ssrCodes;
	}

	/**
	 * @param ssrCodes
	 *            - The ssrCodes to set.
	 */
	public void setSsrCodes(Collection<String> ssrCodes) {
		this.ssrCodes = ssrCodes;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public Collection<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Collection<String> privileges) {
		this.privileges = privileges;
	}

	public void setFlightNoCollection(Collection<String> flightNoCollection) {
		this.flightNoCollection = flightNoCollection;
	}

	public Collection<String> getFlightNoCollection() {
		return flightNoCollection;
	}

	public Collection<String> getBcTypeCollection() {
		return bcTypeCollection;
	}

	public void setBcTypeCollection(Collection<String> bcTypeCollection) {
		this.bcTypeCollection = bcTypeCollection;
	}

	public Collection<String> getAllocTypeCollection() {
		return allocTypeCollection;
	}

	public void setAllocTypeCollection(Collection<String> allocTypeCollection) {
		this.allocTypeCollection = allocTypeCollection;
	}

	public String getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public List<String> getSelectedChargeCodes() {
		return selectedChargeCodes;
	}

	public void setSelectedChargeCodes(List<String> selectedChargeCodes) {
		this.selectedChargeCodes = selectedChargeCodes;
	}

	/**
	 * @return the interlineCode
	 */
	public String getInterlineCode() {
		return interlineCode;
	}

	/**
	 * @param interlineCode
	 *            the interlineCode to set
	 */
	public void setInterlineCode(String interlineCode) {
		this.interlineCode = interlineCode;
	}

	/**
	 * @return the fromTktNumber
	 */
	public String getFromTktNumber() {
		return fromTktNumber;
	}

	/**
	 * @param fromTktNumber
	 *            the fromTktNumber to set
	 */
	public void setFromTktNumber(String fromTktNumber) {
		this.fromTktNumber = fromTktNumber;
	}

	/**
	 * @return the toTktNumber
	 */
	public String getToTktNumber() {
		return toTktNumber;
	}

	/**
	 * @param toTktNumber
	 *            the toTktNumber to set
	 */
	public void setToTktNumber(String toTktNumber) {
		this.toTktNumber = toTktNumber;
	}

	public String getSearchFlightType() {
		return searchFlightType;
	}

	public void setSearchFlightType(String searchFlightType) {
		this.searchFlightType = searchFlightType;
	}

	public boolean isModificationDetails() {
		return modificationDetails;
	}

	public void setModificationDetails(boolean modificationDetails) {
		this.modificationDetails = modificationDetails;
	}

	public boolean isSales() {
		return isSales;
	}

	public void setSales(boolean isSales) {
		this.isSales = isSales;
	}

	public boolean isExtendOnhold() {
		return extendOnhold;
	}

	public void setExtendOnhold(boolean extendOnhold) {
		this.extendOnhold = extendOnhold;
	}

	public boolean isRefund() {
		return isRefund;
	}

	public void setRefund(boolean isRefund) {
		this.isRefund = isRefund;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return the logicalCC
	 */
	public String getLogicalCC() {
		return logicalCC;
	}

	/**
	 * @param logicalCC
	 *            the logicalCC to set
	 */
	public void setLogicalCC(String logicalCC) {
		this.logicalCC = logicalCC;
	}

	public String getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * @return the quantifier
	 */
	public String getQuantifier() {
		return quantifier;
	}

	/**
	 * @param quantifier
	 *            the quantifier to set
	 */
	public void setQuantifier(String quantifier) {
		this.quantifier = quantifier;
	}

	/**
	 * @return the ffpNumber
	 */
	public String getFfpNumber() {
		return ffpNumber;
	}

	/**
	 * @param ffpNumber
	 *            the ffpNumber to set
	 */
	public void setFfpNumber(String ffpNumber) {
		this.ffpNumber = ffpNumber;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the chargeCode
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            the chargeCode to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * 
	 * @return the onlyChargesWithoutRate
	 */
	public boolean isOnlyChargesWithoutRate() {
		return onlyChargesWithoutRate;
	}

	/**
	 * 
	 * @param onlyChargesWithoutRate
	 */
	public void setOnlyChargesWithoutRate(boolean onlyChargesWithoutRate) {
		this.onlyChargesWithoutRate = onlyChargesWithoutRate;
	}

	public String getFareDiscountCode() {
		return fareDiscountCode;
	}

	public void setFareDiscountCode(String fareDiscountCode) {
		this.fareDiscountCode = fareDiscountCode;
	}

	public boolean isMealSelected() {
		return isMealSelected;
	}

	public void setMealSelected(boolean isMealSelected) {
		this.isMealSelected = isMealSelected;
	}

	public boolean isSeatSelected() {
		return isSeatSelected;
	}

	public void setSeatSelected(boolean isSeatSelected) {
		this.isSeatSelected = isSeatSelected;
	}

	public boolean isBaggageSeleted() {
		return isBaggageSeleted;
	}

	public void setBaggageSeleted(boolean isBaggageSeleted) {
		this.isBaggageSeleted = isBaggageSeleted;
	}

	public boolean isFlexiSelected() {
		return isFlexiSelected;
	}

	public void setFlexiSelected(boolean isFlexiSelected) {
		this.isFlexiSelected = isFlexiSelected;
	}

	public boolean isHalaSerivceSelected() {
		return isHalaSerivceSelected;
	}

	public void setHalaSerivceSelected(boolean isSsrSelected) {
		this.isHalaSerivceSelected = isSsrSelected;
	}

	public boolean isInsuranceSelected() {
		return isInsuranceSelected;
	}

	public void setInsuranceSelected(boolean isInsuranceSelected) {
		this.isInsuranceSelected = isInsuranceSelected;
	}

	public boolean isBusSelected() {
		return isBusSelected;
	}

	public void setBusSelected(boolean isBusSelected) {
		this.isBusSelected = isBusSelected;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getFlightType() {
		return flightType;
	}

	public String getOriginMinConncetTime() {
		return originMinConncetTime;
	}

	public void setOriginMinConncetTime(String originMinConncetTime) {
		this.originMinConncetTime = originMinConncetTime;
	}

	public String getOriginMaxConncetTime() {
		return originMaxConncetTime;
	}

	public void setOriginMaxConncetTime(String originMaxConncetTime) {
		this.originMaxConncetTime = originMaxConncetTime;
	}

	public String getDestinationMinConncetTime() {
		return destinationMinConncetTime;
	}

	public void setDestinationMinConncetTime(String destinationMinConncetTime) {
		this.destinationMinConncetTime = destinationMinConncetTime;
	}

	public String getDestinationMaxConncetTime() {
		return destinationMaxConncetTime;
	}

	public void setDestinationMaxConncetTime(String destinationMaxConncetTime) {
		this.destinationMaxConncetTime = destinationMaxConncetTime;
	}

	public void setSalesAmount(String salesAmount) {
		this.salesAmount = salesAmount;
	}

	public String getSalesAmount() {
		return salesAmount;
	}

	/**
	 * @return the requestAllPaxData
	 */
	public boolean isRequestAllPassengers() {
		return requestAllPassengers;
	}

	/**
	 * @param requestAllPaxData
	 *            the requestAllPaxData to set
	 */
	public void setRequestAllPassengers(boolean requestAllPassengers) {
		this.requestAllPassengers = requestAllPassengers;
	}

	/**
	 * @return the lstmealCodes
	 */
	public List<String> getLstmealCodes() {
		return lstmealCodes;
	}

	/**
	 * @param lstmealCodes
	 *            the lstmealCodes to set
	 */
	public void setLstmealCodes(List<String> lstmealCodes) {
		this.lstmealCodes = lstmealCodes;
	}

	/**
	 * @return the lstSeatMapCodes
	 */
	public List<String> getLstSeatMapCodes() {
		return lstSeatMapCodes;
	}

	/**
	 * @param lstSeatMapCodes
	 *            the lstSeatMapCodes to set
	 */
	public void setLstSeatMapCodes(List<String> lstSeatMapCodes) {
		this.lstSeatMapCodes = lstSeatMapCodes;
	}

	/**
	 * @return the lstInsuranceCodes
	 */
	public List<String> getLstInsuranceCodes() {
		return lstInsuranceCodes;
	}

	/**
	 * @param lstInsuranceCodes
	 *            the lstInsuranceCodes to set
	 */
	public void setLstInsuranceCodes(List<String> lstInsuranceCodes) {
		this.lstInsuranceCodes = lstInsuranceCodes;
	}

	/**
	 * @return the lstBaggageCodes
	 */
	public List<String> getLstBaggageCodes() {
		return lstBaggageCodes;
	}

	/**
	 * @param lstBaggageCodes
	 *            the lstBaggageCodes to set
	 */
	public void setLstBaggageCodes(List<String> lstBaggageCodes) {
		this.lstBaggageCodes = lstBaggageCodes;
	}

	/**
	 * @return the lstSSRCodes
	 */
	public List<String> getLstSSRCodes() {
		return lstSSRCodes;
	}

	/**
	 * @param lstSSRCodes
	 *            the lstSSRCodes to set
	 */
	public void setLstSSRCodes(List<String> lstSSRCodes) {
		this.lstSSRCodes = lstSSRCodes;
	}

	/**
	 * @return the lstHALACodes
	 */
	public List<String> getLstHALACodes() {
		return lstHALACodes;
	}

	/**
	 * @param lstHALACodes
	 *            the lstHALACodes to set
	 */
	public void setLstHALACodes(List<String> lstHALACodes) {
		this.lstHALACodes = lstHALACodes;
	}

	public List<String> getLstAptTransferCodes() {
		return lstAptTransferCodes;
	}

	public void setLstAptTransferCodes(List<String> lstAptTransferCodes) {
		this.lstAptTransferCodes = lstAptTransferCodes;
	}

	public boolean isExactFlightNumber() {
		return exactFlightNumber;
	}

	public void setExactFlightNumber(boolean exactFlightNumber) {
		this.exactFlightNumber = exactFlightNumber;
	}

	public int getGmtFromOffsetInMinutes() {
		return gmtFromOffsetInMinutes;
	}

	public void setGmtFromOffsetInMinutes(int gmtFromOffsetInMinutes) {
		this.gmtFromOffsetInMinutes = gmtFromOffsetInMinutes;
	}

	public int getGmtToOffsetInMinutes() {
		return gmtToOffsetInMinutes;
	}

	public void setGmtToOffsetInMinutes(int gmtToOffsetInMinutes) {
		this.gmtToOffsetInMinutes = gmtToOffsetInMinutes;
	}

	/**
	 * @return the overriddenDiscountDataOnly
	 */
	public boolean isOverriddenDiscountDataOnly() {
		return overriddenDiscountDataOnly;
	}

	/**
	 * @param overriddenDiscountDataOnly
	 *            the overriddenDiscountDataOnly to set
	 */
	public void setOverriddenDiscountDataOnly(boolean overriddenDiscountDataOnly) {
		this.overriddenDiscountDataOnly = overriddenDiscountDataOnly;
	}

	/**
	 * @return the normalDiscountDataOnly
	 */
	public boolean isNormalDiscountDataOnly() {
		return normalDiscountDataOnly;
	}

	/**
	 * @param normalDiscountDataOnly
	 *            the normalDiscountDataOnly to set
	 */
	public void setNormalDiscountDataOnly(boolean normalDiscountDataOnly) {
		this.normalDiscountDataOnly = normalDiscountDataOnly;
	}

	public boolean isShowInLocalTime() {
		return showInLocalTime;
	}

	public void setShowInLocalTime(boolean showInLocalTime) {
		this.showInLocalTime = showInLocalTime;
	}

	public Set<String> getPromotionTypes() {
		return promotionTypes;
	}

	public void setPromotionTypes(Set<String> promotionTypes) {
		this.promotionTypes = promotionTypes;
	}

	/**
	 * @return the bookingStatus
	 */
	public String getBookingStatus() {
		return bookingStatus;
	}

	/**
	 * @param bookingStatus
	 *            the bookingStatus to set
	 */
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	/**
	 * @return the byBookingStatus
	 */
	public boolean isByBookingStatus() {
		return byBookingStatus;
	}

	/**
	 * @param byBookingStatus
	 *            the byBookingStatus to set
	 */
	public void setByBookingStatus(boolean byBookingStatus) {
		this.byBookingStatus = byBookingStatus;
	}

	/**
	 * @return the includeAdvanceCCDetails
	 */
	public boolean isIncludeAdvanceCCDetails() {
		return includeAdvanceCCDetails;
	}

	/**
	 * @param includeAdvanceCCDetails
	 *            the includeAdvanceCCDetails to set
	 */
	public void setIncludeAdvanceCCDetails(boolean includeAdvanceCCDetails) {
		this.includeAdvanceCCDetails = includeAdvanceCCDetails;
	}

	public String getPfsStatus() {
		return pfsStatus;
	}

	public void setPfsStatus(String pfsStatus) {
		this.pfsStatus = pfsStatus;
	}

	public Set<String> getBspTnxTypes() {
		return bspTnxTypes;
	}

	public void setBspTnxTypes(Set<String> bspTnxTypes) {
		this.bspTnxTypes = bspTnxTypes;
	}

	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public boolean isFilterByConfirmed() {
		return filterByConfirmed;
	}

	public void setFilterByConfirmed(boolean filterByConfirmed) {
		this.filterByConfirmed = filterByConfirmed;
	}

	public boolean isConsideringFixedSeats() {
		return consideringFixedSeats;
	}

	public void setConsideringFixedSeats(boolean consideringFixedSeats) {
		this.consideringFixedSeats = consideringFixedSeats;
	}

	public Set<Integer> getBundledFarePeriodIds() {
		return bundledFarePeriodIds;
	}

	public void setBundledFarePeriodIds(Set<Integer> bundledFarePeriodIds) {
		this.bundledFarePeriodIds = bundledFarePeriodIds;
	}

	public String getArrivalDateRangeFrom() {
		return arrivalDateRangeFrom;
	}

	public void setArrivalDateRangeFrom(String arrivalDateRangeFrom) {
		this.arrivalDateRangeFrom = arrivalDateRangeFrom;
	}

	public String getArrivalDateRangeTo() {
		return arrivalDateRangeTo;
	}

	public void setArrivalDateRangeTo(String arrivalDateRangeTo) {
		this.arrivalDateRangeTo = arrivalDateRangeTo;
	}

	public boolean isSelectedAllAgents() {
		return isSelectedAllAgents;
	}

	public void setSelectedAllAgents(boolean isSelectedAllAgents) {
		this.isSelectedAllAgents = isSelectedAllAgents;
	}

	public Collection<String> getPaxStatusList() {
		return paxStatusList;
	}

	public void setPaxStatusList(Collection<String> paxStatusList) {
		this.paxStatusList = paxStatusList;
	}

	public boolean isIncludeCancelledDataToReport() {
		return includeCancelledDataToReport;
	}

	public void setIncludeCancelledDataToReport(boolean includeCancelledDataToReport) {
		this.includeCancelledDataToReport = includeCancelledDataToReport;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the ffid
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid
	 *            the ffid to set
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	/**
	 * @return the product
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProductCode(String product) {
		this.productCode = product;
	}

	/**
	 * @return the baseCurrencySelected
	 */
	public boolean isBaseCurrencySelected() {
		return baseCurrencySelected;
	}

	/**
	 * @param baseCurrencySelected
	 *            the baseCurrencySelected to set
	 */
	public void setBaseCurrencySelected(boolean baseCurrencySelected) {
		this.baseCurrencySelected = baseCurrencySelected;
	}

	public String getBlacklistType() {
		return blacklistType;
	}

	public void setBlacklistType(String blacklistType) {
		this.blacklistType = blacklistType;
	}

	public String getPromoCriteriaID() {
		return promoCriteriaID;
	}

	public void setPromoCriteriaID(String promoCriteriaID) {
		this.promoCriteriaID = promoCriteriaID;
	}

	/**
	 * @return - Returns the flightDateRangeFrom.
	 */
	public String getFlightDateRangeFrom() {
		return this.flightDateRangeFrom;
	}

	/**
	 * @param flightDateRangeFrom
	 *            - The flightDateRangeFrom to set.
	 */
	public void setFlightDateRangeFrom(String flightDateRangeFrom) {
		this.flightDateRangeFrom = flightDateRangeFrom;
	}

	/**
	 * @return - Returns the flightDateRangeTo.
	 */
	public String getFlightDateRangeTo() {
		return this.flightDateRangeTo;
	}

	/**
	 * @param flightDateRangeTo
	 *            - The flightDateRangeTo to set.
	 */
	public void setFlightDateRangeTo(String flightDateRangeTo) {
		this.flightDateRangeTo = flightDateRangeTo;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getBookingDateFrom() {
		return bookingDateFrom;
	}

	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}

	public String getBookingDateTo() {
		return bookingDateTo;
	}

	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getVoucherStatus() {
		return voucherStatus;
	}

	public void setVoucherStatus(String voucherStatus) {
		this.voucherStatus = voucherStatus;
	}

	public boolean isOnlyTotalAmountToPay() {
		return onlyTotalAmountToPay;
	}
	

	public void setOnlyTotalAmountToPay(boolean onlyTotalAmountToPay) {
		this.onlyTotalAmountToPay = onlyTotalAmountToPay;
	}
	
	public boolean isIncludePaidStatusFilter() {
		return includePaidStatusFilter;
	}
	

	public void setIncludePaidStatusFilter(boolean includePaidStatusFilter) {
		this.includePaidStatusFilter = includePaidStatusFilter;
	}
	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getGsaCode() {
		return gsaCode;
	}

	public void setGsaCode(String gsaCode) {
		this.gsaCode = gsaCode;
	}

	public boolean isOnlyReportingAgents() {
		return isOnlyReportingAgents;
	}

	public void setOnlyReportingAgents(boolean isOnlyReportingAgents) {
		this.isOnlyReportingAgents = isOnlyReportingAgents;
	}

	public String getBasedOnAgent() {
		return basedOnAgent;
	}

	public void setBasedOnAgent(String basedOnAgent) {
		this.basedOnAgent = basedOnAgent;
	}

	public int getReservationFlow() {
		return reservationFlow;
	}

	public void setReservationFlow(int reservationFlow) {
		this.reservationFlow = reservationFlow;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}



}
