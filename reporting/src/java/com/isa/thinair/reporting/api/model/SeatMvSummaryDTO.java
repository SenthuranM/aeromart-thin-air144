package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class SeatMvSummaryDTO implements Serializable {

	private static final long serialVersionUID = -2762446350098649092L;

	private String agentName = "";

	private String agentCode = "";

	private String agentTypeCode = "";

	private String stationCode = "";
	private int soldSeats;

	private int soldChildSeats;

	private int soldInfantSeats;

	private int onholdSeats;

	private int onHoldChildSeats;

	private int onholdInfantSeats;

	private int cancelledSeats;

	private int cancelledChildSeats;

	private int cancelledInfantSeats;

	// sum values
	private int soldSeatsSum;

	private int soldChildSeatsSum;

	private int soldInfantSeatsSum;

	private int onholdSeatsSum;

	private int onHoldChildSeatsSum;

	private int onholdInfantSeatsSum;

	private int cancelledSeatsSum;

	private int cancelledChildSeatsSum;

	private int cancelledInfantSeatsSum;

	private Collection<String> confirmedPNRs;

	private Collection<String> cancelledPNRs;

	private Collection<String> onHoldPNRs;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	public int getCancelledInfantSeats() {
		return cancelledInfantSeats;
	}

	public void setCancelledInfantSeats(int cancelledInfantSeats) {
		this.cancelledInfantSeats = cancelledInfantSeats;
	}

	public int getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(int cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	public void setOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats = onholdInfantSeats;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	// --------- sum values

	public int getCancelledInfantSeatsSum() {
		return cancelledInfantSeatsSum;
	}

	public void setCancelledInfantSeatsSum(int cancelledInfantSeatsSum) {
		this.cancelledInfantSeatsSum = cancelledInfantSeatsSum;
	}

	public int getCancelledSeatsSum() {
		return cancelledSeatsSum;
	}

	public void setCancelledSeatsSum(int cancelledSeatsSum) {
		this.cancelledSeatsSum = cancelledSeatsSum;
	}

	public int getOnholdInfantSeatsSum() {
		return onholdInfantSeatsSum;
	}

	public void setOnholdInfantSeatsSum(int onholdInfantSeatsSum) {
		this.onholdInfantSeatsSum = onholdInfantSeatsSum;
	}

	public int getOnholdSeatsSum() {
		return onholdSeatsSum;
	}

	public void setOnholdSeatsSum(int onholdSeatsSum) {
		this.onholdSeatsSum = onholdSeatsSum;
	}

	public int getSoldInfantSeatsSum() {
		return soldInfantSeatsSum;
	}

	public void setSoldInfantSeatsSum(int soldInfantSeatsSum) {
		this.soldInfantSeatsSum = soldInfantSeatsSum;
	}

	public int getSoldSeatsSum() {
		return soldSeatsSum;
	}

	public void setSoldSeatsSum(int soldSeatsSum) {
		this.soldSeatsSum = soldSeatsSum;
	}

	public int getCancelledChildSeats() {
		return cancelledChildSeats;
	}

	public void setCancelledChildSeats(int cancelledChildSeats) {
		this.cancelledChildSeats = cancelledChildSeats;
	}

	public int getCancelledChildSeatsSum() {
		return cancelledChildSeatsSum;
	}

	public void setCancelledChildSeatsSum(int cancelledChildSeatsSum) {
		this.cancelledChildSeatsSum = cancelledChildSeatsSum;
	}

	public int getOnHoldChildSeats() {
		return onHoldChildSeats;
	}

	public void setOnHoldChildSeats(int onHoldChildSeats) {
		this.onHoldChildSeats = onHoldChildSeats;
	}

	public int getOnHoldChildSeatsSum() {
		return onHoldChildSeatsSum;
	}

	public void setOnHoldChildSeatsSum(int onHoldChildSeatsSum) {
		this.onHoldChildSeatsSum = onHoldChildSeatsSum;
	}

	public int getSoldChildSeats() {
		return soldChildSeats;
	}

	public void setSoldChildSeats(int soldChildSeats) {
		this.soldChildSeats = soldChildSeats;
	}

	public int getSoldChildSeatsSum() {
		return soldChildSeatsSum;
	}

	public void setSoldChildSeatsSum(int soldChildSeatsSum) {
		this.soldChildSeatsSum = soldChildSeatsSum;
	}

	/**
	 * @return the stationCode
	 */
	public String getStationCode() {
		return stationCode;
	}

	/**
	 * @param stationCode
	 *            the stationCode to set
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * @return the confirmedPNRs
	 */
	public Collection<String> getConfirmedPNRs() {
		if (confirmedPNRs == null) {
			confirmedPNRs = new ArrayList<String>();
		}
		return confirmedPNRs;
	}

	/**
	 * @param confirmedPNRs
	 *            the confirmedPNRs to set
	 */
	public void setConfirmedPNRs(Collection<String> confirmedPNRs) {
		this.confirmedPNRs = confirmedPNRs;
	}

	/**
	 * @return the cancelledPNRs
	 */
	public Collection<String> getCancelledPNRs() {
		if (cancelledPNRs == null) {
			cancelledPNRs = new ArrayList<String>();
		}
		return cancelledPNRs;
	}

	/**
	 * @param cancelledPNRs
	 *            the cancelledPNRs to set
	 */
	public void setCancelledPNRs(Collection<String> cancelledPNRs) {
		this.cancelledPNRs = cancelledPNRs;
	}

	/**
	 * @return the onHoldPNRs
	 */
	public Collection<String> getOnHoldPNRs() {
		if (onHoldPNRs == null) {
			onHoldPNRs = new ArrayList<String>();
		}
		return onHoldPNRs;
	}

	/**
	 * @param onHoldPNRs
	 *            the onHoldPNRs to set
	 */
	public void setOnHoldPNRs(Collection<String> onHoldPNRs) {
		this.onHoldPNRs = onHoldPNRs;
	}

}
