package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * @author harsha
 * 
 */
public class FlightInvBkngDTO implements Serializable {

	private static final long serialVersionUID = -572841187991901094L;

	private String channel;

	private Integer last30;

	private Integer last7;

	public String getChannel() {
		return channel;
	}

	public Integer getLast30() {
		return last30;
	}

	public Integer getLast7() {
		return last7;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setLast30(Integer last30) {
		this.last30 = last30;
	}

	public void setLast7(Integer last7) {
		this.last7 = last7;
	}

}
