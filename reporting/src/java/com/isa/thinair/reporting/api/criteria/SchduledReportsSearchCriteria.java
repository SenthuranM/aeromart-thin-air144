package com.isa.thinair.reporting.api.criteria;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * TO to hold Schedule Report Search Related Variables
 * 
 * @author Navod Ediriweera
 * @since 05 July 2010
 */
public class SchduledReportsSearchCriteria implements Serializable {
	private static Log log = LogFactory.getLog(SchduledReportsSearchCriteria.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String reportName;
	private String reportUser;
	private String fromDate;
	private String toDate;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportUser() {
		return reportUser;
	}

	public void setReportUser(String reportUser) {
		this.reportUser = reportUser;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Date getFromDateAsDateTime() {
		if (this.fromDate == null)
			return null;
		if ("".equals(this.fromDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.fromDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			if (log.isErrorEnabled())
				log.error("Error Parsing Date");
			return null;
		}
	}

	public Date getToDateAsDateTime() {
		if (this.toDate == null)
			return null;
		if ("".equals(this.toDate))
			return null;
		try {
			return CalendarUtil.getParsedTime(this.toDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss");
		} catch (Exception e) {
			if (log.isErrorEnabled())
				log.error("Error Parsing Date");
			return null;
		}
	}

}
