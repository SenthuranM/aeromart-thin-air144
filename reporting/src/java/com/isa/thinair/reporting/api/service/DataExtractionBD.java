/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.api.service;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Map;

import javax.sql.RowSet;

import com.isa.thinair.airreservation.api.dto.HalaServiceDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.AgentUserRolesDTO;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;

/**
 * @author Byorn
 */
public interface DataExtractionBD {

	public static final String SERVICE_NAME = "ReportingService";

	public Page<FlightInventorySummaryDTO> searchFlightsForInventoryUpdation(FlightSearchCriteria criteria, int startindex,
			int pagesize) throws ModuleException;

	public FCCAgentsSeatMvDTO getFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList)
			throws ModuleException;

	public FCCAgentsSeatMvDTO getFCCSeatMovementSummaryForLogicalCC(int flightId, String logicalCCCode, boolean loadPNRList)
			throws ModuleException;

	public FCCAgentsSeatMvDTO getOwnerAgentWiseFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList)
			throws ModuleException;

	public RowSet getPerformanceOfSalesStaffSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPerformanceOfSalesStaffDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCallCentreModeOfPaymentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCallCentreRefundData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCustomerTravelHistoryDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCustomerProfileSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getWebProfilesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCustomerProfileDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getEnplanementData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getTopAgentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getTopSegmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getSegmentContributionByAgentsByPOSData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCountryContributionPerFlightSegmentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPaxContactDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Map<String, Integer> getPaxCountForPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getIBEOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getReservationBreakdownSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getReservationBreakdownDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCustomerExistingCreditDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAirportTaxData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentCommisionData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getReservationBreakdownTaxesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getOutstandingBalanceSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSummaryDataByCurrency(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentTransactionData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAvsSeat(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInboundOutboundFlightSchedule(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getScheduleDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public void getROPRevenue(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getROPRevenueDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public void getForwardSales(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public void getONDData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
;
	public void getBookedSSRDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBookedSSRDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getScheduleCapasityVarience(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentGSAData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getMealsSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getExtPmtReconcilSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentUserData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getUserPrivilegeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Map getAgentUserRoleData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentProductivityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getViewSeatInventoryFareMovements(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Collection getPaymentsMode() throws ModuleException;

	public RowSet getInbDatas(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCompanyPaymentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBookedPAXSegmentData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getOriginCountryOfCallData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getOriginCountryOfCallDetail(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCompanyPaymentCurrencyData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCompanyPaymentPOSData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCnxReservationDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAcquiredCreditDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAvailableCreditDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSummaryAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSummaryAttachmentByCurrency(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSummaryAttachmentByTransaction(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getCCTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Collection getRoleForPrivileges(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlownPassengerListData(ReportsSearchCriteria reportsSearchCriteria, String viewMode) throws ModuleException;
	
	public RowSet getFlownPassengerData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentHandlingFeeData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getGDSReservationsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getIssuedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getPromotionCriteriaDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getRedeemedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	/**
	 * Returns per day CC transaction fraud status.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	public RowSet getPerDayCCFraudDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	/**
	 * Returns CCTransactionRefundDetails
	 * 
	 * @param reportsSearchCriteria
	 *            which contain the search options
	 * @return RowSet CCTransactionRefundDetails
	 * @throws ModuleException
	 */
	public RowSet getCCTransactionRefundDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	/**
	 * Returns CCTransactionPendingRefundDetails
	 * 
	 * @param reportsSearchCriteria
	 *            which contain the search options
	 * @return RowSet CCTransactionRefundDetails
	 * @throws ModuleException
	 */
	public RowSet getCCTransactionPendingRefundDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFreightDetailsReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAdministrationAuditData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getReservationAuditData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInterlineSalesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public void getInterlineRevenue(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightLoad(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	/**
	 * Agent Statement Summary Report
	 * 
	 * @since 07-08-2008
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	public RowSet getAgentStatementSummaryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	/**
	 * Agent Statement Detail Report
	 * 
	 * @since 07-08-2008
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	public RowSet getAgentStatementDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getEmailOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInsurenceDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPrivilegeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentSalesPerformanceReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getTicketWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentSalesStatusData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBCReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getInvoiceSettlemenyHistoryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getNameChangeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	/**
	 * Retrives The Total Pax of Top Segments for Confirmation
	 * 
	 * @author Navod Ediriweera
	 * @param reportsSearchCriteria
	 * @return
	 * @throws ModuleException
	 */
	public RowSet getSegmentTotalPaxByStatus(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Map<String, Map<String, Integer>> getCountOfPassengers(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException;

	public RowSet getMealSummaryReport(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getMealDetailReport(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getFlightAncillaryDetailsReport(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getBookedHalaServiceData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBookedHalaServiceDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getHalaServiceCommissionData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getPnrInvoiceReceipt(ReportsSearchCriteria search);

	public RowSet getLoyaltyPointsData(ReportsSearchCriteria search) throws ModuleException;

	public Collection<HalaServiceDTO> getBookedHalaServiceSummaryForNotification(ReportsSearchCriteria search)
			throws ModuleException;

	public Collection<PaxSSRDTO> getBookedHalaServiceDetailForNotification(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getAncillaryRevenueData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getAdvancedAncillaryRevenueReportData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getCurrencyConversionData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getAirportTaxReportData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getVoidReservationDetailsReportData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getBookingCountDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Integer getBookingCountDetailsReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBookingCountDetailAgentReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Integer getBookingCountDetailAgentReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAdjustmentAuditReportData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getLccAndDryCollection(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getFareRuleDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFareRuleVisibilityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAgentsForFareRuleData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Page<String> getFlightsForFlightLoadAnalysis(FlightSearchCriteria criteria, int startindex, int pagesize)
			throws ModuleException;

	public RowSet getMisProductSalesData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getIncentiveSchemeData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getAgentIncentiveSchemeData(ReportsSearchCriteria search) throws ModuleException;
	
	

	/**
	 * Gets the list of airports matching the search criteria provided.
	 * 
	 * @param search
	 *            The search criteria for the report.
	 * 
	 * @return The list of airports matching the provided criteria.
	 * @throws ModuleException
	 */
	public RowSet getAirportListData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getRouteDetailData(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getFareDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAppParameterDataforReports(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getBookedSSRSummaryDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getBookedSSRDetailDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getAirportTransferDataforReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public void getAgentSalesModifyRefundReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getUserIncentiveDetailsReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPromotionRequestsSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPromotionNextSeatFreeSummary(ReportsSearchCriteria reportSearchCriteria) throws ModuleException;

	public RowSet getPromotionNextSeatFreeSummaryForCrew(ReportsSearchCriteria reportSearchCriteria) throws ModuleException;

	public RowSet getPromotionFlexiDateSummary(ReportsSearchCriteria reportSearchCriteria) throws ModuleException;

	public RowSet getFlightHistoryDetailsReport(String flightId, String flightNo, String flightDate, String reverseDate,
			Integer scheduleId, String fromDate, String toDate, boolean showResHistory) throws ModuleException;

	public RowSet getTaxHistoryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFareDiscountsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightConnectivityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPromoCodeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getJNTaxReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public String getAgentStationCode() throws ModuleException;

	public RowSet getSalesChannelCodes(String salesChannelIDs) throws ModuleException;

	public RowSet getIbeExitDataForReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightOverBookSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightSeatMapChangesSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getIbeExitDataForReports(String exitDetailId) throws ModuleException;
	
	public RowSet getBundledFareReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getInternationalFlightDepartureArrivalDataForReports(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getPassengerInternationalFlightDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightLoyaltyMembers(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getPointRedemptionDetailedReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public void generateAviatorFile() throws ModuleException;

	public RowSet getNameChangeChargeReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getBlacklistedPassengersData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getNILTransactionsAgentsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public Page getRolesForPrivilege(AgentUserRolesDTO agentUserRolesDTO,int startIndex, int pageLength)throws ModuleException;

	public Map getAgentUserRoleDetailData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getPFSDetailedExport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getPassengerStatusAndRevenueData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getFlightMessageDetailsReport(String flightId, Integer scheduleId, String fromDate, String toDate)
			throws ModuleException;

	public RowSet getScheduleMessageDetailsReport(String scheduleId, String fromDate, String toDate) throws ModuleException;

	public RowSet getPublishedMessageDetailsReport(String scheduleFlightId, String strFromDate, String strToDate)
			throws ModuleException;
	
	public RowSet getMCODetailReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getMCOSummaryReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Map<String, ResultSet> getGstr1(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public Map<String, ResultSet> getGstr3(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getGSTAdditionalReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getForceConfirmedBookingReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public RowSet getPassengersMealSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;
	
	public RowSet getBlockedLMSPointsData(ReportsSearchCriteria search) throws ModuleException;
	
	public RowSet getCCTransactionsLMSPointsData(ReportsSearchCriteria search) throws ModuleException;
	
	public RowSet getCCTransactionsLMSPointsDetailQuery(ReportsSearchCriteria search) throws ModuleException;

	public RowSet getCCTOPTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	public ResultSet getAutomaticCheckinDetails(ReportsSearchCriteria search) throws ModuleException;
	
	public ResultSet getPerDayCCTopUpFraudDetails(ReportsSearchCriteria search) throws ModuleException;

}