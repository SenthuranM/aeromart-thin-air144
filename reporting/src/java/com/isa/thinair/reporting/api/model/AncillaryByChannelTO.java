package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class AncillaryByChannelTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1647031341686399916L;
	private String chargeCode;
	private String totalAmount;
	private Map<String, BigDecimal> salesByChannel;
	private Map<String, String> displaySalesByChannel;
	private String amount;

	public AncillaryByChannelTO() {
		super();

	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Map<String, BigDecimal> getSalesByChannel() {
		return salesByChannel;
	}

	public void setSalesByChannel(Map<String, BigDecimal> salesByChannel) {
		this.salesByChannel = salesByChannel;
	}

	public Map<String, String> getDisplaySalesByChannel() {
		return displaySalesByChannel;
	}

	public void setDisplaySalesByChannel(Map<String, String> displaySalesByChannel) {
		this.displaySalesByChannel = displaySalesByChannel;
	}

}
