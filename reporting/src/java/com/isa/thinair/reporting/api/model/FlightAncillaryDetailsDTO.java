package com.isa.thinair.reporting.api.model;

public class FlightAncillaryDetailsDTO {

	private String pnr;

	private String paxName;

	private String insuranceSelected;

	private String contactPerson;

	private String mobileNumber;

	private String phoneNumber;

	private String eMail;

	private String segmentCode;

	private String departureTimeLocal;

	private String mealSelected;

	private String baggageSelected;

	private String seatSelected;

	private String ssrSelected;

	private String flexiSelected;

	private String busSelected;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getInsuranceSelected() {
		return insuranceSelected;
	}

	public void setInsuranceSelected(String insuranceSelected) {
		this.insuranceSelected = insuranceSelected;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getMealSelected() {
		return mealSelected;
	}

	public void setMealSelected(String mealSelected) {
		this.mealSelected = mealSelected;
	}

	public String getBaggageSelected() {
		return baggageSelected;
	}

	public void setBaggageSelected(String baggageSelected) {
		this.baggageSelected = baggageSelected;
	}

	public String getSeatSelected() {
		return seatSelected;
	}

	public void setSeatSelected(String seatSelected) {
		this.seatSelected = seatSelected;
	}

	public String getSsrSelected() {
		return ssrSelected;
	}

	public void setSsrSelected(String ssrSelected) {
		this.ssrSelected = ssrSelected;
	}

	public String getFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public String getBusSelected() {
		return busSelected;
	}

	public void setBusSelected(String busSelected) {
		this.busSelected = busSelected;
	}

	public String getDepartureTimeLocal() {
		return departureTimeLocal;
	}

	public void setDepartureTimeLocal(String departureTimeLocal) {
		this.departureTimeLocal = departureTimeLocal;
	}

}
