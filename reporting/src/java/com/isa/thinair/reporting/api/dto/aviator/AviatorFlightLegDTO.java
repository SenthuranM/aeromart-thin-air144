package com.isa.thinair.reporting.api.dto.aviator;

import java.util.Date;

/**
* @author Manoj Dhanushka
* 
*/
public class AviatorFlightLegDTO {
	
	private Integer flightId;
	
	private Integer flightLegId;	

	private String flightNumber;
	
	private Date departureDate;
	
	private Date departureDateLocal;
	
	private String modelNumber;

	private String origin;

	private Date estDepartureTime;
	
	private int estDepartureDayOffset;
	
	private String destination;

	private Date estArrivalTime;

	private int estArrivalDayOffset;
	
	private int legNumber;
	
	private Date estDepartureTimeLocal;
	
	private Date estArrivalTimeLocal;

	private int estArrivalDayOffsetLocal;
	
	private int estDepartureDayOffsetLocal;

	public Integer getFlightId() {
		return flightId;
	}

	public Integer getFlightLegId() {
		return flightLegId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public String getOrigin() {
		return origin;
	}

	public Date getEstDepartureTime() {
		return estDepartureTime;
	}

	public String getDestination() {
		return destination;
	}

	public Date getEstArrivalTime() {
		return estArrivalTime;
	}

	public int getEstArrivalDayOffset() {
		return estArrivalDayOffset;
	}

	public int getLegNumber() {
		return legNumber;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public void setFlightLegId(Integer flightLegId) {
		this.flightLegId = flightLegId;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setEstDepartureTime(Date estDepartureTime) {
		this.estDepartureTime = estDepartureTime;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setEstArrivalTime(Date estArrivalTime) {
		this.estArrivalTime = estArrivalTime;
	}

	public void setEstArrivalDayOffset(int estArrivalDayOffset) {
		this.estArrivalDayOffset = estArrivalDayOffset;
	}

	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	public Date getEstDepartureTimeLocal() {
		return estDepartureTimeLocal;
	}

	public Date getEstArrivalTimeLocal() {
		return estArrivalTimeLocal;
	}

	public int getEstArrivalDayOffsetLocal() {
		return estArrivalDayOffsetLocal;
	}

	public void setEstDepartureTimeLocal(Date estDepartureTimeLocal) {
		this.estDepartureTimeLocal = estDepartureTimeLocal;
	}

	public void setEstArrivalTimeLocal(Date estArrivalTimeLocal) {
		this.estArrivalTimeLocal = estArrivalTimeLocal;
	}

	public void setEstArrivalDayOffsetLocal(int estArrivalDayOffsetLocal) {
		this.estArrivalDayOffsetLocal = estArrivalDayOffsetLocal;
	}

	public int getEstDepartureDayOffsetLocal() {
		return estDepartureDayOffsetLocal;
	}

	public void setEstDepartureDayOffsetLocal(int estDepartureDayOffsetLocal) {
		this.estDepartureDayOffsetLocal = estDepartureDayOffsetLocal;
	}

	public int getEstDepartureDayOffset() {
		return estDepartureDayOffset;
	}

	public void setEstDepartureDayOffset(int estDepartureDayOffset) {
		this.estDepartureDayOffset = estDepartureDayOffset;
	}

	public Date getDepartureDateLocal() {
		return departureDateLocal;
	}

	public void setDepartureDateLocal(Date departureDateLocal) {
		this.departureDateLocal = departureDateLocal;
	}	
}
