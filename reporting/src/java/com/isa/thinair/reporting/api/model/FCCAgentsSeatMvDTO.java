package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class FCCAgentsSeatMvDTO implements Serializable {

	private static final long serialVersionUID = 1215391317416006332L;

	private HashMap fccSegsAgentsSeatMvDTOs;

	private LinkedHashMap fccAgentsSeatMvDTOs;

	public void setFccAgentSeatMvDTOs(LinkedHashMap fccAgentSeatMvDTOs) {
		this.fccAgentsSeatMvDTOs = fccAgentSeatMvDTOs;
	}

	public void setFccSegAgentSeatMvDTOs(HashMap fccSegAgentSeatMvDTOs) {
		this.fccSegsAgentsSeatMvDTOs = fccSegAgentSeatMvDTOs;
	}

	public Collection<SeatMvSummaryDTO> getSegAgentsSeatMvWithFCCAgentsSeatMv(Integer flightSegId) {
		Collection<SeatMvSummaryDTO> fccSegAgentSeatSummaryDTOs = new ArrayList<SeatMvSummaryDTO>();
		if (fccAgentsSeatMvDTOs != null) {
			Iterator fccAgentSeatMvDTOsKeysIt = fccAgentsSeatMvDTOs.keySet().iterator();
			while (fccAgentSeatMvDTOsKeysIt.hasNext()) {
				String agentCode = (String) fccAgentSeatMvDTOsKeysIt.next();
				AgentSeatMvDTO fccAgentSeatMv = (AgentSeatMvDTO) fccAgentsSeatMvDTOs.get(agentCode);

				SeatMvSummaryDTO seatMv = new SeatMvSummaryDTO();
				fccSegAgentSeatSummaryDTOs.add(seatMv);

				seatMv.setAgentCode(agentCode);
				seatMv.setAgentName(fccAgentSeatMv.getAgentName());
				seatMv.setAgentTypeCode(fccAgentSeatMv.getAgentTypeCode());
				seatMv.setStationCode(fccAgentSeatMv.getStationCode());
				seatMv.setSoldSeatsSum(fccAgentSeatMv.getSoldSeats());
				seatMv.setSoldChildSeatsSum(fccAgentSeatMv.getSoldChildSeats());
				seatMv.setSoldInfantSeatsSum(fccAgentSeatMv.getSoldInfantSeats());
				seatMv.setOnholdSeatsSum(fccAgentSeatMv.getOnholdSeats());
				seatMv.setOnHoldChildSeatsSum(fccAgentSeatMv.getOnHoldChildSeats());
				seatMv.setOnholdInfantSeatsSum(fccAgentSeatMv.getOnholdInfantSeats());
				seatMv.setCancelledSeatsSum(fccAgentSeatMv.getCancelledSeats());
				seatMv.setCancelledChildSeatsSum(fccAgentSeatMv.getCancelledChildSeats());
				seatMv.setCancelledInfantSeatsSum(fccAgentSeatMv.getCancelledInfantSeats());
				seatMv.setCancelledPNRs(fccAgentSeatMv.getCancelledPNRs());
				seatMv.setConfirmedPNRs(fccAgentSeatMv.getConfirmedPNRs());
				seatMv.setOnHoldPNRs(fccAgentSeatMv.getOnHoldPNRs());

				AgentSeatMvDTO fccSegAgentSeatMv = null;
				if (fccSegsAgentsSeatMvDTOs != null) {
					LinkedHashMap fccSegAgentsSeatMv = (LinkedHashMap) fccSegsAgentsSeatMvDTOs.get(flightSegId);
					if (fccSegAgentsSeatMv != null) {
						fccSegAgentSeatMv = (AgentSeatMvDTO) fccSegAgentsSeatMv.get(agentCode);
					}
				}

				if (fccSegAgentSeatMv != null) {
					seatMv.setSoldSeats(fccSegAgentSeatMv.getSoldSeats());
					seatMv.setSoldChildSeats(fccSegAgentSeatMv.getSoldChildSeats());
					seatMv.setSoldInfantSeats(fccSegAgentSeatMv.getSoldInfantSeats());
					seatMv.setOnholdSeats(fccSegAgentSeatMv.getOnholdSeats());
					seatMv.setOnHoldChildSeats(fccSegAgentSeatMv.getOnHoldChildSeats());
					seatMv.setOnholdInfantSeats(fccSegAgentSeatMv.getOnholdInfantSeats());
					seatMv.setCancelledSeats(fccSegAgentSeatMv.getCancelledSeats());
					seatMv.setCancelledChildSeats(fccSegAgentSeatMv.getCancelledChildSeats());
					seatMv.setCancelledInfantSeats(fccSegAgentSeatMv.getCancelledInfantSeats());
				} else {
					seatMv.setSoldSeats(0);
					seatMv.setSoldChildSeats(0);
					seatMv.setSoldInfantSeats(0);
					seatMv.setOnholdSeats(0);
					seatMv.setOnHoldChildSeats(0);
					seatMv.setOnholdInfantSeats(0);
					seatMv.setCancelledSeats(0);
					seatMv.setCancelledChildSeats(0);
					seatMv.setCancelledInfantSeats(0);
				}
			}
		}
		return fccSegAgentSeatSummaryDTOs;
	}
}
