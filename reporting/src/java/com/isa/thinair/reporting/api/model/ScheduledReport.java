package com.isa.thinair.reporting.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_SCHED_REPT_TEMPLETE"
 */
public class ScheduledReport extends Persistent {

	private static final long serialVersionUID = 1L;

	public static String STATUS_NOT_STARTED = "NOT_STARTED";
	public static String STATUS_STARTED = "STARTED";
	public static String STATUS_COMPLETED = "COMPLETED";
	public static String STATUS_SUSPENDED = "SUSPENDED";

	public static String PERIOD_ADJUSTED_RANGE = "ADJUSTED_RANGE";
	public static String PERIOD_LAST_WEEK = "LAST_WEEK";
	public static String PERIOD_LAST_HALF_MONTH = "LAST_HALF_MONTH";
	public static String PERIOD_LAST_MONTH = "LAST_MONTH";
	public static String PERIOD_END_OF_THIS_MONTH = "TILL_END_OF_MONTH";
	public static String PERIOD_NEXT_MONTH = "NEXT_MONTH";
	public static String PERIOD_NEXT_2_MONTHS = "NEXT_2_MONTHS";
	public static String PERIOD_NEXT_3_MONTHS = "NEXT_3_MONTHS";
	public static String PERIOD_NEXT_6_MONTHS = "NEXT_6_MONTHS";

	private int scheduledReportId;

	private String cronExpression;

	private String paramTemplete;

	private String criteriaTemplete;

	private Date createdTime;

	private String scheduledBy;

	private String reportName;

	private String sendReportTo;

	private String reportFormat;

	private String reportTempleteRelativePath;

	private Date startDate;

	private Date endDate;

	private String status;

	private String quartsJobName;

	private String dateCalculationMechanism;

	/**
	 * @hibernate.id column = "SR_TEMPLETE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SCHED_REPT_TEMPLETE"
	 */
	public int getScheduledReportId() {
		return scheduledReportId;
	}

	public void setScheduledReportId(int scheduledReportId) {
		this.scheduledReportId = scheduledReportId;
	}

	/**
	 * @hibernate.property column = "CRON_EXPRESSION"
	 */
	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	/**
	 * @hibernate.property column = "PARAM_TEMPLETE"
	 */
	public String getParamTemplete() {
		return paramTemplete;
	}

	public void setParamTemplete(String paramTemplete) {
		this.paramTemplete = paramTemplete;
	}

	/**
	 * @hibernate.property column = "CREATED_TIME"
	 */
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @hibernate.property column = "SHEDULED_BY"
	 */
	public String getScheduledBy() {
		return scheduledBy;
	}

	public void setScheduledBy(String scheduledBy) {
		this.scheduledBy = scheduledBy;
	}

	/**
	 * @hibernate.property column = "REPORT_NAME"
	 */
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @hibernate.property column = "SEND_REPORT_TO"
	 */
	public String getSendReportTo() {
		return sendReportTo;
	}

	public void setSendReportTo(String sendReportTo) {
		this.sendReportTo = sendReportTo;
	}

	/**
	 * @hibernate.property column = "CRITERIA_TEMPLETE"
	 */
	public String getCriteriaTemplete() {
		return criteriaTemplete;
	}

	public void setCriteriaTemplete(String criteriaTemplete) {
		this.criteriaTemplete = criteriaTemplete;
	}

	/**
	 * @hibernate.property column = "REPORT_FORMAT"
	 */
	public String getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	/**
	 * @hibernate.property column = "REPORT_TEMPL_REL_PATH"
	 */
	public String getReportTempleteRelativePath() {
		return reportTempleteRelativePath;
	}

	public void setReportTempleteRelativePath(String reportTempleteRelativePath) {
		this.reportTempleteRelativePath = reportTempleteRelativePath;
	}

	/**
	 * @hibernate.property column = "START_DATE"
	 */
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @hibernate.property column = "END_DATE"
	 */
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "QUARTZ_JOB_NAME"
	 */
	public String getQuartsJobName() {
		return quartsJobName;
	}

	public void setQuartsJobName(String quartsJobName) {
		this.quartsJobName = quartsJobName;
	}

	/**
	 * @hibernate.property column = "DATE_CALC_MECHANISM"
	 */
	public String getDateCalculationMechanism() {
		return dateCalculationMechanism;
	}

	public void setDateCalculationMechanism(String dateCalculationMechanism) {
		this.dateCalculationMechanism = dateCalculationMechanism;
	}
}
