package com.isa.thinair.reporting.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_SCHED_REPT_SENT_ITEMS"
 */
public class SRSentItem extends Persistent {

	private static final long serialVersionUID = 1L;

	public static String STATUS_SENT = "S";
	public static String STATUS_FAILED = "F";

	private int srSentItemId;

	private int scheduledReportId;

	private Date sentTime;

	private String notes;

	private String status;

	/**
	 * @hibernate.id column = "SR_SENT_ITEMS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SCHED_REPT_SENT_ITEMS"
	 */
	public int getSrSentItemId() {
		return srSentItemId;
	}

	public void setSrSentItemId(int srSentItemId) {
		this.srSentItemId = srSentItemId;
	}

	/**
	 * @hibernate.property column = "SR_TEMPLETE_ID"
	 */
	public int getScheduledReportId() {
		return scheduledReportId;
	}

	public void setScheduledReportId(int scheduledReportId) {
		this.scheduledReportId = scheduledReportId;
	}

	/**
	 * @hibernate.property column = "SENT_TIME"
	 */
	public Date getSentTime() {
		return sentTime;
	}

	public void setSentTime(Date sentTime) {
		this.sentTime = sentTime;
	}

	/**
	 * @hibernate.property column = "NOTES"
	 */
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
