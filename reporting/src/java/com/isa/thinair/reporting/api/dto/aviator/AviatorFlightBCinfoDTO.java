package com.isa.thinair.reporting.api.dto.aviator;

/**
* @author Manoj Dhanushka
* 
*/
public class AviatorFlightBCinfoDTO {
	
	private Integer flightSegId;
	
	private String bookingCode;
	
	private double revenue;
	
	private int noshowCount;
	
	private int goshowCount;

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public double getRevenue() {
		return revenue;
	}

	public int getNoshowCount() {
		return noshowCount;
	}

	public int getGoshowCount() {
		return goshowCount;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public void setNoshowCount(int noshowCount) {
		this.noshowCount = noshowCount;
	}

	public void setGoshowCount(int goshowCount) {
		this.goshowCount = goshowCount;
	}

}
