package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class AgentSeatMvDTO implements Serializable {

	private static final long serialVersionUID = 3415165619608760321L;

	private String agentName = "";

	private String agentCode = "";

	private String agentTypeCode = "";

	private String stationCode = "";

	private int soldSeats;

	private int soldChildSeats;

	private int soldInfantSeats;

	private int onholdSeats;

	private int onHoldChildSeats;

	private int onholdInfantSeats;

	private int cancelledSeats;

	private int cancelledChildSeats;

	private int cancelledInfantSeats;

	private Collection<String> confirmedPNRs;

	private Collection<String> cancelledPNRs;

	private Collection<String> onHoldPNRs;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	public int getCancelledInfantSeats() {
		return cancelledInfantSeats;
	}

	public void addCancelledInfantSeats(int cancelledInfantSeats) {
		this.cancelledInfantSeats += cancelledInfantSeats;
	}

	public int getCancelledSeats() {
		return cancelledSeats;
	}

	public void addCancelledSeats(int cancelledSeats) {
		this.cancelledSeats += cancelledSeats;
	}

	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	public int getCancelledChildSeats() {
		return cancelledChildSeats;
	}

	public void addCancelledChildSeats(int cancelledChildSeats) {
		this.cancelledChildSeats += cancelledChildSeats;
	}

	public int getOnHoldChildSeats() {
		return onHoldChildSeats;
	}

	public void addOnHoldChildSeats(int onHoldChildSeats) {
		this.onHoldChildSeats += onHoldChildSeats;
	}

	public int getSoldChildSeats() {
		return soldChildSeats;
	}

	public void addSoldChildSeats(int soldChildSeats) {
		this.soldChildSeats += soldChildSeats;
	}

	public void addOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats += onholdInfantSeats;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void addOnholdSeats(int onholdSeats) {
		this.onholdSeats += onholdSeats;
	}

	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	public void addSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats += soldInfantSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void addSoldSeats(int soldSeats) {
		this.soldSeats += soldSeats;
	}

	/**
	 * @return the stationCode
	 */
	public String getStationCode() {
		return stationCode;
	}

	/**
	 * @param stationCode
	 *            the stationCode to set
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * @param soldSeats
	 *            the soldSeats to set
	 */
	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	/**
	 * @param soldChildSeats
	 *            the soldChildSeats to set
	 */
	public void setSoldChildSeats(int soldChildSeats) {
		this.soldChildSeats = soldChildSeats;
	}

	/**
	 * @param soldInfantSeats
	 *            the soldInfantSeats to set
	 */
	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	/**
	 * @param onholdSeats
	 *            the onholdSeats to set
	 */
	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	/**
	 * @return the confirmedPNRs
	 */
	public Collection<String> getConfirmedPNRs() {
		if (confirmedPNRs == null) {
			confirmedPNRs = new ArrayList<String>();
		}
		return confirmedPNRs;
	}

	/**
	 * @param confirmedPNRs
	 *            the confirmedPNRs to set
	 */
	public void setConfirmedPNRs(Collection<String> confirmedPNRs) {
		this.confirmedPNRs = confirmedPNRs;
	}

	/**
	 * @return the cancelledPNRs
	 */
	public Collection<String> getCancelledPNRs() {
		if (cancelledPNRs == null) {
			cancelledPNRs = new ArrayList<String>();
		}
		return cancelledPNRs;
	}

	/**
	 * @param cancelledPNRs
	 *            the cancelledPNRs to set
	 */
	public void setCancelledPNRs(Collection<String> cancelledPNRs) {
		this.cancelledPNRs = cancelledPNRs;
	}

	/**
	 * @return the onHoldPNRs
	 */
	public Collection<String> getOnHoldPNRs() {
		if (onHoldPNRs == null) {
			onHoldPNRs = new ArrayList<String>();
		}
		return onHoldPNRs;
	}

	/**
	 * @param onHoldPNRs
	 *            the onHoldPNRs to set
	 */
	public void setOnHoldPNRs(Collection<String> onHoldPNRs) {
		this.onHoldPNRs = onHoldPNRs;
	}

}
