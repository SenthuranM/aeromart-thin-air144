package com.isa.thinair.reporting.api.criteria;

import java.io.Serializable;

public class MISReportsSearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1174799635428000078L;
	private String fromDate;
	private String toDate;
	private String region;
	private String pos;
	private String countryCodes;
	private String ancillaryChargeCodes;
	private String paymentNominalCodes;
	private String refundNominalCodes;
	private boolean anciByCount;
	private String agentCodes;

	private String dateRangeArray[][] = null;

	public String[][] getDateRangeArray() {
		return dateRangeArray;
	}

	public void setDateRangeArray(String[][] dateRangeArray) {
		this.dateRangeArray = dateRangeArray;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getCountryCodes() {
		return countryCodes;
	}

	public void setCountryCodes(String countryCodes) {
		this.countryCodes = countryCodes;
	}

	public String getAncillaryChargeCodes() {
		return ancillaryChargeCodes;
	}

	public void setAncillaryChargeCodes(String ancillaryChargeCodes) {
		this.ancillaryChargeCodes = ancillaryChargeCodes;
	}

	public String getPaymentNominalCodes() {
		return paymentNominalCodes;
	}

	public void setPaymentNominalCodes(String paymentNominalCodes) {
		this.paymentNominalCodes = paymentNominalCodes;
	}

	public String getRefundNominalCodes() {
		return refundNominalCodes;
	}

	public void setRefundNominalCodes(String refundNominalCodes) {
		this.refundNominalCodes = refundNominalCodes;
	}

	public boolean isAnciByCount() {
		return anciByCount;
	}

	public void setAnciByCount(boolean anciByCount) {
		this.anciByCount = anciByCount;
	}

	public String getAgentCodes() {
		return agentCodes;
	}

	public void setAgentCodes(String agentCodes) {
		this.agentCodes = agentCodes;
	}

}
