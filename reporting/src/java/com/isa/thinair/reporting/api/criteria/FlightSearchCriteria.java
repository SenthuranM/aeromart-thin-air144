package com.isa.thinair.reporting.api.criteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Criteria for searching flights for inventory management
 * 
 * @author MN
 */
public class FlightSearchCriteria implements Serializable {

	private static final long serialVersionUID = -4762035802278437676L;

	private String destination;

	private String origin;

	private String flightNumber;

	private Date departureDateFrom;

	private Date departureDateTo;

	private Collection<String> flightStatuses;

	private List viaPoints;

	private String cabinClassCode;

	private boolean timeInLocal = true;

	private boolean matchExactONDViaPointsCombinationOnly = false;

	private boolean includeInvalidSegments = false;

	/** @todo take from User Principal **/
	private Collection<String> applicapableCarrierCodes;

	private Collection<String> flightNumbers;

	/** [min, max] seat factor values */
	private int[] seatFactor;

	// Set the default to get all day numbers
	private String dayNumber = null;

	/**
	 * @return Returns the destination.
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            The destination to set.
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the origin.
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            The origin to set.
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return Returns the departureDateFrom.
	 */
	public Date getDepartureDateFrom() {
		return departureDateFrom;
	}

	/**
	 * @param departureDateFrom
	 *            The departureDateFrom to set.
	 */
	public void setDepartureDateFrom(Date departureDateFrom) {
		this.departureDateFrom = departureDateFrom;
	}

	/**
	 * @return Returns the departureDateTo.
	 */
	public Date getDepartureDateTo() {
		return departureDateTo;
	}

	/**
	 * @param departureDateTo
	 *            The departureDateTo to set.
	 */
	public void setDepartureDateTo(Date departureDateTo) {
		this.departureDateTo = departureDateTo;
	}

	public List getViaPoints() {
		return viaPoints;
	}

	public void setViaPoints(List viaPoints) {
		this.viaPoints = viaPoints;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public boolean isTimeInLocal() {
		return timeInLocal;
	}

	public void setTimeInLocal(boolean timeInLocal) {
		this.timeInLocal = timeInLocal;
	}

	public Collection getFlightStatuses() {
		return flightStatuses;
	}

	public void addFlightStatus(String flightStatus) {
		if (this.flightStatuses == null) {
			this.flightStatuses = new ArrayList<String>();
		}
		this.flightStatuses.add(flightStatus);
	}

	public boolean isMatchExactONDViaPointsCombinationOnly() {
		return matchExactONDViaPointsCombinationOnly;
	}

	public void setMatchExactONDViaPointsCombinationOnly(boolean matchExactONDViaPointsCombinationOnly) {
		this.matchExactONDViaPointsCombinationOnly = matchExactONDViaPointsCombinationOnly;
	}

	public boolean isIncludeInvalidSegments() {
		return includeInvalidSegments;
	}

	public void setIncludeInvalidSegments(boolean includeInvalidSegments) {
		this.includeInvalidSegments = includeInvalidSegments;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append("\n\r-------------------------Criteria Summary-------------------------------\n\r");
		summary.append("\tFrom Date				: " + getDepartureDateFrom() + "\n\r");
		summary.append("\tTo Date				: " + getDepartureDateTo() + "\n\r");
		summary.append("\tOrigin				: " + getOrigin() + "\n\r");
		summary.append("\tDestination			: " + getDestination() + "\n\r");
		summary.append("\tMatched Segment Code	: "
				+ PlatformUtiltiies.prepareSegmentCode(getOrigin(), getDestination(), getViaPoints(),
						isMatchExactONDViaPointsCombinationOnly()) + "\n\r");
		summary.append("\tFlight Number 		: " + getFlightNumber() + "\n\r");
		summary.append("\tFlight Status 		: " + getFlightStatuses() + "\n\r");
		summary.append("\tCabinClass Code 		: " + getCabinClassCode() + "\n\r");
		summary.append("\tIs time in local 		: " + isTimeInLocal() + "\n\r");
		return summary;
	}

	public Collection<String> getApplicapableCarrierCodes() {

		return applicapableCarrierCodes;
	}

	public void setApplicapableCarrierCodes(Collection<String> applicapableCarrierCodes) {
		this.applicapableCarrierCodes = applicapableCarrierCodes;
	}

	public Collection<String> getFlightNumbers() {
		return flightNumbers;
	}

	public void setFlightNumbers(Collection<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	/**
	 * @return the seatFactor values [min, max]
	 */
	public int[] getSeatFactor() {
		return seatFactor;
	}

	/**
	 * @param seatFactor
	 *            the seatFactor [min, max] to set
	 */
	public void setSeatFactor(int[] seatFactor) {
		this.seatFactor = seatFactor;
	}

	public String getDayNumber() {
		return dayNumber;
	}

	public void setDayNumber(String dayNumber) {
		this.dayNumber = dayNumber;
	}

}
