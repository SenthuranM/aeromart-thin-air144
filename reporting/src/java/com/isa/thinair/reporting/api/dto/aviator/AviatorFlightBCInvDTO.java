package com.isa.thinair.reporting.api.dto.aviator;

/**
* @author Manoj Dhanushka
* 
*/
public class AviatorFlightBCInvDTO {
	
	private Integer flightId;
	
	private Integer flightSegId;
	
	private String segmentCode;
	
	private String logicalCabinClass;
	
	private String bookingCode;

	private int nestRank;

	private int fixedFlag;

	private int onholdSeats;

	private int waitlistedSeats;

	private int soldSeats;

	private int allocatedSeats;

	private int cancelledSeats;

	private int acquiredSeats;

	private boolean isConnectionFareAvailable;

	private int availableSeats;	
	
	private int availableNestedSeats;	
	
	private int seatsSoldNested;
	
	private int seatsSoldAquiredByNesting;
	
	private int seatsOnHoldNested;
	
	private int seatsOnHoldAquiredByNesting;
	
	private double owFare;
	
	private double rtFare;
	
	private double revenue;
	
	private int status;

	public Integer getFlightId() {
		return flightId;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public int getNestRank() {
		return nestRank;
	}

	public int getWaitlistedSeats() {
		return waitlistedSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public int getAvailableNestedSeats() {
		return availableNestedSeats;
	}

	public void setAvailableNestedSeats(int availableNestedSeats) {
		this.availableNestedSeats = availableNestedSeats;
	}

	public double getOwFare() {
		return owFare;
	}

	public double getRtFare() {
		return rtFare;
	}

	public int getFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(int fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	public double getRevenue() {
		return revenue;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public void setNestRank(int nestRank) {
		this.nestRank = nestRank;
	}

	public void setWaitlistedSeats(int waitlistedSeats) {
		this.waitlistedSeats = waitlistedSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public void setOwFare(double owFare) {
		this.owFare = owFare;
	}

	public void setRtFare(double rtFare) {
		this.rtFare = rtFare;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public int getSeatsSoldNested() {
		return seatsSoldNested;
	}

	public int getSeatsSoldAquiredByNesting() {
		return seatsSoldAquiredByNesting;
	}

	public int getSeatsOnHoldNested() {
		return seatsOnHoldNested;
	}

	public int getSeatsOnHoldAquiredByNesting() {
		return seatsOnHoldAquiredByNesting;
	}

	public void setSeatsSoldNested(int seatsSoldNested) {
		this.seatsSoldNested = seatsSoldNested;
	}

	public void setSeatsSoldAquiredByNesting(int seatsSoldAquiredByNesting) {
		this.seatsSoldAquiredByNesting = seatsSoldAquiredByNesting;
	}

	public void setSeatsOnHoldNested(int seatsOnHoldNested) {
		this.seatsOnHoldNested = seatsOnHoldNested;
	}

	public void setSeatsOnHoldAquiredByNesting(int seatsOnHoldAquiredByNesting) {
		this.seatsOnHoldAquiredByNesting = seatsOnHoldAquiredByNesting;
	}

	public int getActualSeatsSold() {
		return getSoldSeats() + getSeatsSoldAquiredByNesting() - getSeatsSoldNested();
	}

	public int getActualSeatsOnHold() {
		return getOnholdSeats() + getSeatsOnHoldAquiredByNesting() - getSeatsOnHoldNested();
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getAcquiredSeats() {
		return acquiredSeats;
	}

	public void setAcquiredSeats(int acquiredSeats) {
		this.acquiredSeats = acquiredSeats;
	}

	public int getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(int cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public boolean isConnectionFareAvailable() {
		return isConnectionFareAvailable;
	}

	public void setIsConnectionFareAvailable(boolean isConnectionFareAvailable) {
		this.isConnectionFareAvailable = isConnectionFareAvailable;
	}
}
