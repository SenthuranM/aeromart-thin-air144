package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

public class RegionalProductSalesTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -810354350879783400L;
	private String regionName;
	private String location;
	private BigDecimal actualSales;
	private String target;
	private String ytdSales;
	private String ytdVariance;
	private String strActualSales;

	private Map<String, Collection<AgentPerformanceTO>> agentsRegionalSalesLocationWise;

	public RegionalProductSalesTO() {
		super();

	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public BigDecimal getActualSales() {
		return actualSales;
	}

	public void setActualSales(BigDecimal actualSales) {
		this.actualSales = actualSales;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getYtdSales() {
		return ytdSales;
	}

	public void setYtdSales(String ytdSales) {
		this.ytdSales = ytdSales;
	}

	public String getYtdVariance() {
		return ytdVariance;
	}

	public void setYtdVariance(String ytdVariance) {
		this.ytdVariance = ytdVariance;
	}

	public Map<String, Collection<AgentPerformanceTO>> getAgentsRegionalSalesLocationWise() {
		return agentsRegionalSalesLocationWise;
	}

	public void setAgentsRegionalSalesLocationWise(Map<String, Collection<AgentPerformanceTO>> agentsRegionalSalesLocationWise) {
		this.agentsRegionalSalesLocationWise = agentsRegionalSalesLocationWise;
	}

	public String getStrActualSales() {
		return strActualSales;
	}

	public void setStrActualSales(String strActualSales) {
		this.strActualSales = strActualSales;
	}

}
