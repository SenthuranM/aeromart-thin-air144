package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class SRCountDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6898687796232877497L;

	private int currentGlobalCount = 0;

	private int currentUserCount = 0;

	public SRCountDTO(int globleCount, int userCount) {
		this.currentGlobalCount = globleCount;
		this.currentUserCount = userCount;
	}

	public int getCurrentGlobalCount() {
		return currentGlobalCount;
	}

	public void setCurrentGlobalCount(int currentGlobalCount) {
		this.currentGlobalCount = currentGlobalCount;
	}

	public int getCurrentUserCount() {
		return currentUserCount;
	}

	public void setCurrentUserCount(int currentUserCount) {
		this.currentUserCount = currentUserCount;
	}
}
