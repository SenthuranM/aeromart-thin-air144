package com.isa.thinair.reporting.api.dto.scheduledRpts;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.api.model.ScheduledReport;

/**
 * TO to hold Schedule Report Search Result Related Variables
 * 
 * @author Navod Ediriweera
 * @since 05 July 2010
 */
public class ScheduledReportsViewTO implements Serializable {
	private static Log log = LogFactory.getLog(ScheduledReportsViewTO.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String reportName;
	private String reportDisplayName;
	private String schduledBy;
	private String startDate;
	private String endDate;
	private String version;
	private String scheduledReportID;
	private String status;
	private String emailIDs;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDisplayName() {
		return reportDisplayName;
	}

	public void setReportDisplayName(String reportDisplayName) {
		this.reportDisplayName = reportDisplayName;
	}

	public String getSchduledBy() {
		return schduledBy;
	}

	public void setSchduledBy(String schduledBy) {
		this.schduledBy = schduledBy;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getScheduledReportID() {
		return scheduledReportID;
	}

	public void setScheduledReportID(String scheduledReportID) {
		this.scheduledReportID = scheduledReportID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * Mergers the Data of the Current TO To the Domain Object, if the ID is the same
	 * 
	 * @param sr
	 * @return
	 * @throws ModuleException
	 */
	public ScheduledReport mergeDataForDomainObject(ScheduledReport sr) {
		if (sr == null) {
			sr = new ScheduledReport();
		}
		if (PlatformUtiltiies.isNotEmptyString(scheduledReportID)) {
			sr.setScheduledReportId(Integer.parseInt(this.scheduledReportID));
		}
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			if (PlatformUtiltiies.isNotEmptyString(this.startDate)) {
				sr.setStartDate(format.parse(this.startDate + " 00:00:00"));
			}
			if (PlatformUtiltiies.isNotEmptyString(this.endDate)) {
				sr.setEndDate(format.parse(this.endDate + " 23:59:59"));
			}
		} catch (ParseException e) {
			log.error("Exception occured while parsing Date");
			throw new RuntimeException("Exception occured while parsing Date");
		}
		if (PlatformUtiltiies.isNotEmptyString(this.status))
			sr.setStatus(this.status);
		if (PlatformUtiltiies.isNotEmptyString(this.emailIDs))
			sr.setSendReportTo(this.emailIDs);
		if (PlatformUtiltiies.isNotEmptyString(this.version))
			sr.setVersion(new Long(this.version));

		return sr;
	}

	public String getEmailIDs() {
		return emailIDs;
	}

	public void setEmailIDs(String emailIDs) {
		this.emailIDs = emailIDs;
	}

}
