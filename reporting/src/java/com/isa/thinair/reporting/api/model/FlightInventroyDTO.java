package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author harsha
 * 
 */
public class FlightInventroyDTO implements Serializable {

	private static final long serialVersionUID = -1211173605837223646L;

	private Date departDate;

	private String flightNo;

	private String ond;

	private List<OnDDetailsDTO> ondDetailsLst;

	public String getDepartDate() {
		return dateToString(departDate);
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getOnd() {
		return ond;
	}

	public void setOnd(String ond) {
		this.ond = ond;
	}

	public List<OnDDetailsDTO> getOndDetailsLst() {
		return ondDetailsLst;
	}

	public void setOndDetailsLst(List<OnDDetailsDTO> ondDetailsLst) {
		this.ondDetailsLst = ondDetailsLst;
	}

	public String dateToString(Date date) {
		String formattedDate = null;
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}

}
