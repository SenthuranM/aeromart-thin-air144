package com.isa.thinair.reporting.api.model;

import java.util.Date;

public class SeatInventoryFareMovementsDTO {
	private Date flightDate;
	private String flightNumber;
	private String segmentCode;
	private String cos;
	private int seatsAllocated;
	private int overSell;
	private int curtailed;
	private int seatsSold;
	private int onHold;
	private int fixed;
	private int seatsAvailable;
	private int fareCollected;

	public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

	public int getCurtailed() {
		return curtailed;
	}

	public void setCurtailed(int curtailed) {
		this.curtailed = curtailed;
	}

	public int getFareCollected() {
		return fareCollected;
	}

	public void setFareCollected(int fareCollected) {
		this.fareCollected = fareCollected;
	}

	public int getFixed() {
		return fixed;
	}

	public void setFixed(int fixed) {
		this.fixed = fixed;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public int getOnHold() {
		return onHold;
	}

	public void setOnHold(int onHold) {
		this.onHold = onHold;
	}

	public int getOverSell() {
		return overSell;
	}

	public void setOverSell(int overSell) {
		this.overSell = overSell;
	}

	public int getSeatsAllocated() {
		return seatsAllocated;
	}

	public void setSeatsAllocated(int seatsAllocated) {
		this.seatsAllocated = seatsAllocated;
	}

	public int getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(int seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public int getSeatsSold() {
		return seatsSold;
	}

	public void setSeatsSold(int seatsSold) {
		this.seatsSold = seatsSold;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

}
