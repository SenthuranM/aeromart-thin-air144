package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class FlightMealDetailDTO implements Serializable {

	private static final long serialVersionUID = 4794175385737905439L;

	private String pnr;

	private String paxFirstName;

	private String paxLastName;

	private String mealCode;

	private String status;

	private String seatCode;

	private String depTimeLocal;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaxFirstName() {
		return paxFirstName;
	}

	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	public String getPaxLastName() {
		return paxLastName;
	}

	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public String getDepTimeLocal() {
		return depTimeLocal;
	}

	public void setDepTimeLocal(String depTimeLocal) {
		this.depTimeLocal = depTimeLocal;
	}
}
