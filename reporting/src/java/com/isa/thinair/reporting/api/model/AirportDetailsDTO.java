package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * DTO to hold airport details to generate the airport list report.
 * 
 * @author sanjaya
 */
public class AirportDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8066616493354017384L;
	private String airportName;
	private String countryName;
	private String stationName;
	private String gmtOffset;
	private String effectiveDSTOffset;
	private String minStopoverTime;
	private String onlineOfflineStatus;
	private String status;
	private String goShowAgent;
	private String lccPublishStatus;
	private String anciReminderStartEnd;
	private String busStation;

	/**
	 * @return The airport name.
	 */
	public String getAirportName() {
		return airportName;
	}

	/**
	 * @param airportName
	 *            to set.
	 */
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	/**
	 * 
	 * @return
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * 
	 * @param countryName
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * 
	 * @return
	 */
	public String getStationName() {
		return stationName;
	}

	/**
	 * 
	 * @param stationName
	 */
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	/**
	 * 
	 * @return
	 */
	public String getGmtOffset() {
		return gmtOffset;
	}

	/**
	 * 
	 * @param gmtOffset
	 */
	public void setGmtOffset(String gmtOffset) {
		this.gmtOffset = gmtOffset;
	}

	/**
	 * 
	 * @return
	 */
	public String getEffectiveDSTOffset() {
		return effectiveDSTOffset;
	}

	/**
	 * 
	 * @param effectiveDSTOffset
	 */
	public void setEffectiveDSTOffset(String effectiveDSTOffset) {
		this.effectiveDSTOffset = effectiveDSTOffset;
	}

	/**
	 * 
	 * @return
	 */
	public String getMinStopoverTime() {
		return minStopoverTime;
	}

	/**
	 * 
	 * @param minStopoverTime
	 */
	public void setMinStopoverTime(String minStopoverTime) {
		this.minStopoverTime = minStopoverTime;
	}

	/**
	 * 
	 * @return
	 */
	public String getOnlineOfflineStatus() {
		return onlineOfflineStatus;
	}

	/**
	 * 
	 * @param onlineOfflineStatus
	 */
	public void setOnlineOfflineStatus(String onlineOfflineStatus) {
		this.onlineOfflineStatus = onlineOfflineStatus;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public String getGoShowAgent() {
		return goShowAgent;
	}

	/**
	 * 
	 * @param goShowAgent
	 */
	public void setGoShowAgent(String goShowAgent) {
		this.goShowAgent = goShowAgent;
	}

	/**
	 * 
	 * @return
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * 
	 * @param lccPublishStatus
	 */
	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus;
	}

	/**
	 * 
	 * @return
	 */
	public String getAnciReminderStartEnd() {
		return anciReminderStartEnd;
	}

	/**
	 * 
	 * @param anciReminderStartEnd
	 */
	public void setAnciReminderStartEnd(String anciReminderStartEnd) {
		this.anciReminderStartEnd = anciReminderStartEnd;
	}

	/**
	 * 
	 * @return
	 */
	public String getBusStation() {
		return busStation;
	}

	/**
	 * 
	 * @param busStation
	 */
	public void setBusStation(String busStation) {
		this.busStation = busStation;
	}
}
