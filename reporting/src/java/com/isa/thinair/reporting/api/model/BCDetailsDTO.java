package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author harsha
 * 
 */
public class BCDetailsDTO implements Serializable {

	private static final long serialVersionUID = 7461725646113999039L;

	private String bkgClass;

	private Integer aloc;

	private Date first;

	private Integer hold;

	private Date last;

	private Integer last30;

	private Integer last7;

	private Integer sold;

	private Integer over30;

	public String getBkgClass() {
		return bkgClass;
	}

	public void setBkgClass(String bkgClass) {
		this.bkgClass = bkgClass;
	}

	public Integer getAloc() {
		return aloc;
	}

	public void setAloc(Integer aloc) {
		this.aloc = aloc;
	}

	public String getFirst() {
		return DateToString(first);
	}

	public void setFirst(Date first) {
		this.first = first;
	}

	public Integer getHold() {
		return hold;
	}

	public void setHold(Integer hold) {
		this.hold = hold;
	}

	public String getLast() {
		return DateToString(last);
	}

	public void setLast(Date last) {
		this.last = last;
	}

	public Integer getLast30() {
		return last30;
	}

	public void setLast30(Integer last30) {
		this.last30 = last30;
	}

	public Integer getLast7() {
		return last7;
	}

	public void setLast7(Integer last7) {
		this.last7 = last7;
	}

	public Integer getSold() {
		return sold;
	}

	public void setSold(Integer sold) {
		this.sold = sold;
	}

	public String DateToString(Date date) {
		String formattedDate = null;
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}

	public Integer getOver30() {
		return over30;
	}

	public void setOver30(Integer over30) {
		this.over30 = over30;
	}
}
