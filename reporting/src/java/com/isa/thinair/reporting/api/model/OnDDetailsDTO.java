package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author harsha
 * 
 */
public class OnDDetailsDTO implements Serializable {

	private static final long serialVersionUID = 5084775258335275302L;

	private String ond;

	private List<ClassofServiceDTO> cosLst;

	public String getOnd() {
		return ond;
	}

	public void setOnd(String ond) {
		this.ond = ond;
	}

	public List<ClassofServiceDTO> getCosLst() {
		return cosLst;
	}

	public void setCosLst(List<ClassofServiceDTO> cosLst) {
		this.cosLst = cosLst;
	}

}
