package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class BookingsTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4005318345750542635L;
	private String salesChannel;
	private String noOfBookings;
	private String countryName;
	private String salesChannelName;
	private String salesChannelColor;

	public BookingsTO() {
		super();
	}

	public BookingsTO(String salesChannel, String noOfBookings, String countryName, String salesChannelName,
			String salesChannelColor) {
		super();
		this.salesChannel = salesChannel;
		this.noOfBookings = noOfBookings;
		this.countryName = countryName;
		this.salesChannelName = salesChannelName;
		this.salesChannelColor = salesChannelColor;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getNoOfBookings() {
		return noOfBookings;
	}

	public void setNoOfBookings(String noOfBookings) {
		this.noOfBookings = noOfBookings;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getSalesChannelName() {
		return salesChannelName;
	}

	public void setSalesChannelName(String salesChannelName) {
		this.salesChannelName = salesChannelName;
	}

	public String getSalesChannelColor() {
		return salesChannelColor;
	}

	public void setSalesChannelColor(String salesChannelColor) {
		this.salesChannelColor = salesChannelColor;
	}

}
