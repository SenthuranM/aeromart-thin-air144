package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class MISAccountSummaryTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2828970250633394483L;
	public final static String SALES_TYPE_REFUND = "refund";
	public final static String SALES_TYPE_REVENUE = "revenue";

	private String salesType;
	private BigDecimal amount;
	private Integer nominalCode;
	private String paymentType;

	public String getSalesType() {
		return salesType;
	}

	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getNominalCode() {
		return nominalCode;
	}

	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

}
