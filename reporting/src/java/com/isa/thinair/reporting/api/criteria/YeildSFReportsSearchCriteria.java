package com.isa.thinair.reporting.api.criteria;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class YeildSFReportsSearchCriteria implements Serializable {
	private static Log log = LogFactory.getLog(YeildSFReportsSearchCriteria.class);

	/**
	 * @return the selAirlineCode
	 */
	public String getSelAirlineCode() {
		return selAirlineCode;
	}

	/**
	 * @param selAirlineCode
	 *            the selAirlineCode to set
	 */
	public void setSelAirlineCode(String selAirlineCode) {
		this.selAirlineCode = selAirlineCode;
	}

	/**
	 * @return the viewOption
	 */
	public String getViewOption() {
		return viewOption;
	}

	/**
	 * @param viewOption
	 *            the viewOption to set
	 */
	public void setViewOption(String viewOption) {
		this.viewOption = viewOption;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType
	 *            the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	private static final long serialVersionUID = 1L;
	private String selAirlineCode;
	private String viewOption;
	private String reportType;

}
