package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author nakhtar
 * 
 */
public class FareRuleDetailsDTO implements Serializable {

	private static final long serialVersionUID = -1094399379499515980L;

	private String fareRule;
	private String description;
	private String basis;
	private String flightWay;
	private String status;
	private String agentFareRuleCode;
	private String agents;
	private String visibilityFareRuleCode;
	private String visibilityCode;
	private String chargeDescription;
	private String printExpiryDate;
	private BigDecimal modificationCharges;
	private BigDecimal cancelCharges;
	private String advBookingDate;
	private String minMonths;
	private String minDays;
	private String minHours;
	private String minMins;
	private String maxMonths;
	private String maxDays;
	private String maxHours;
	private String maxMins;
	private String pExpDate;
	private String inTravelTimeFrom;
	private String inTravelTimeTo;
	private String dTravelTimeto;
	private String dTravelTimeFrom;
	private String paxCat;
	private String fareCat;
	private String modBufferTime;
	private String ruleComments;
	private String mFRules;
	private String childPayApplicable;
	private String childRefund;
	private String childMode;
	private String childCnx;
	private BigDecimal childNoShow;
	private String infantPay;
	private String infantRefund;
	private String infantMode;
	private String infantCnx;
	private BigDecimal infantNoShow;
	private String adultPay;
	private String adultRefund;
	private String adultMode;
	private String adultCnx;
	private BigDecimal adultNoShow;
	private String openReturn;
	private String orMonth;
	private String orDays;
	private String orHours;
	private String orMins;
	private String minStayOverTime;
	private String maxStayOverTime;
	private String openReturnLimit;
	private String inBound;
	private String outBound;
	private String modificationBuffer;

	public FareRuleDetailsDTO() {
		// nothing
	}

	/**
	 * @return the fareRule
	 */
	public String getFareRule() {
		return fareRule;
	}

	/**
	 * @param fareRule
	 *            the fareRule to set
	 */
	public void setFareRule(String fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the basis
	 */
	public String getBasis() {
		return basis;
	}

	/**
	 * @param basis
	 *            the basis to set
	 */
	public void setBasis(String basis) {
		this.basis = basis;
	}

	/**
	 * @return the flightWay
	 */
	public String getFlightWay() {
		return flightWay;
	}

	/**
	 * @param flightWay
	 *            the flightWay to set
	 */
	public void setFlightWay(String flightWay) {
		this.flightWay = flightWay;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the agentFareRuleCode
	 */
	public String getAgentFareRuleCode() {
		return agentFareRuleCode;
	}

	/**
	 * @param agentFareRuleCode
	 *            the agentFareRuleCode to set
	 */
	public void setAgentFareRuleCode(String agentFareRuleCode) {
		this.agentFareRuleCode = agentFareRuleCode;
	}

	/**
	 * @return the agents
	 */
	public String getAgents() {
		return agents;
	}

	/**
	 * @param agents
	 *            the agents to set
	 */
	public void setAgents(String agents) {
		this.agents = agents;
	}

	/**
	 * @return the visibilityFareRuleCode
	 */
	public String getVisibilityFareRuleCode() {
		return visibilityFareRuleCode;
	}

	/**
	 * @param visibilityFareRuleCode
	 *            the visibilityFareRuleCode to set
	 */
	public void setVisibilityFareRuleCode(String visibilityFareRuleCode) {
		this.visibilityFareRuleCode = visibilityFareRuleCode;
	}

	/**
	 * @return the visibilityCode
	 */
	public String getVisibilityCode() {
		return visibilityCode;
	}

	/**
	 * @param visibilityCode
	 *            the visibilityCode to set
	 */
	public void setVisibilityCode(String visibilityCode) {
		this.visibilityCode = visibilityCode;
	}

	/**
	 * @return the chargeDescription
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            the chargeDescription to set
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return the printExpiryDate
	 */
	public String getPrintExpiryDate() {
		return printExpiryDate;
	}

	/**
	 * @param printExpiryDate
	 *            the printExpiryDate to set
	 */
	public void setPrintExpiryDate(String printExpiryDate) {
		this.printExpiryDate = printExpiryDate;
	}

	/**
	 * @return the modificationCharges
	 */
	public BigDecimal getModificationCharges() {
		return modificationCharges;
	}

	/**
	 * @param modificationCharges
	 *            the modificationCharges to set
	 */
	public void setModificationCharges(BigDecimal modificationCharges) {
		this.modificationCharges = modificationCharges;
	}

	/**
	 * @return the cancelCharges
	 */
	public BigDecimal getCancelCharges() {
		return cancelCharges;
	}

	/**
	 * @param cancelCharges
	 *            the cancelCharges to set
	 */
	public void setCancelCharges(BigDecimal cancelCharges) {
		this.cancelCharges = cancelCharges;
	}

	/**
	 * @return the advBookingDate
	 */
	public String getAdvBookingDate() {
		return advBookingDate;
	}

	/**
	 * @param advBookingDate
	 *            the advBookingDate to set
	 */
	public void setAdvBookingDate(String advBookingDate) {
		this.advBookingDate = advBookingDate;
	}

	/**
	 * @return the minMonth
	 */
	public String getMinMonths() {
		return minMonths;
	}

	/**
	 * @param minMonth
	 *            the minMonth to set
	 */
	public void setMinMonths(String minMonths) {
		this.minMonths = minMonths;
	}

	/**
	 * @return the minDays
	 */
	public String getMinDays() {
		return minDays;
	}

	/**
	 * @param minDays
	 *            the minDays to set
	 */
	public void setMinDays(String minDays) {
		this.minDays = minDays;
	}

	/**
	 * @return the minHours
	 */
	public String getMinHours() {
		return minHours;
	}

	/**
	 * @param minHours
	 *            the minHours to set
	 */
	public void setMinHours(String minHours) {
		this.minHours = minHours;
	}

	/**
	 * @return the minMins
	 */
	public String getMinMins() {
		return minMins;
	}

	/**
	 * @param minMins
	 *            the minMins to set
	 */
	public void setMinMins(String minMins) {
		this.minMins = minMins;
	}

	/**
	 * @return the masMonths
	 */
	public String getMaxMonths() {
		return maxMonths;
	}

	/**
	 * @param masMonths
	 *            the masMonths to set
	 */
	public void setMaxMonths(String maxMonths) {
		this.maxMonths = maxMonths;
	}

	/**
	 * @return the maxDays
	 */
	public String getMaxDays() {
		return maxDays;
	}

	/**
	 * @param maxDays
	 *            the maxDays to set
	 */
	public void setMaxDays(String maxDays) {
		this.maxDays = maxDays;
	}

	/**
	 * @return the maxHours
	 */
	public String getMaxHours() {
		return maxHours;
	}

	/**
	 * @param maxHours
	 *            the maxHours to set
	 */
	public void setMaxHours(String maxHours) {
		this.maxHours = maxHours;
	}

	/**
	 * @return the maxMins
	 */
	public String getMaxMins() {
		return maxMins;
	}

	/**
	 * @param maxMins
	 *            the maxMins to set
	 */
	public void setMaxMins(String maxMins) {
		this.maxMins = maxMins;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the pExpDate
	 */
	public String getpExpDate() {
		return pExpDate;
	}

	/**
	 * @param pExpDate
	 *            the pExpDate to set
	 */
	public void setpExpDate(String pExpDate) {
		this.pExpDate = pExpDate;
	}

	/**
	 * @return the inTravelTimeFrom
	 */
	public String getInTravelTimeFrom() {
		return inTravelTimeFrom;
	}

	/**
	 * @param inTravelTimeFrom
	 *            the inTravelTimeFrom to set
	 */
	public void setInTravelTimeFrom(String inTravelTimeFrom) {
		this.inTravelTimeFrom = inTravelTimeFrom;
	}

	/**
	 * @return the inTravelTimeTo
	 */
	public String getInTravelTimeTo() {
		return inTravelTimeTo;
	}

	/**
	 * @param inTravelTimeTo
	 *            the inTravelTimeTo to set
	 */
	public void setInTravelTimeTo(String inTravelTimeTo) {
		this.inTravelTimeTo = inTravelTimeTo;
	}

	/**
	 * @return the dTravelTimeto
	 */
	public String getdTravelTimeto() {
		return dTravelTimeto;
	}

	/**
	 * @param dTravelTimeto
	 *            the dTravelTimeto to set
	 */
	public void setdTravelTimeto(String dTravelTimeto) {
		this.dTravelTimeto = dTravelTimeto;
	}

	/**
	 * @return the dTravelTimeFrom
	 */
	public String getdTravelTimeFrom() {
		return dTravelTimeFrom;
	}

	/**
	 * @param dTravelTimeFrom
	 *            the dTravelTimeFrom to set
	 */
	public void setdTravelTimeFrom(String dTravelTimeFrom) {
		this.dTravelTimeFrom = dTravelTimeFrom;
	}

	/**
	 * @return the paxCat
	 */
	public String getPaxCat() {
		return paxCat;
	}

	/**
	 * @param paxCat
	 *            the paxCat to set
	 */
	public void setPaxCat(String paxCat) {
		this.paxCat = paxCat;
	}

	/**
	 * @return the fareCat
	 */
	public String getFareCat() {
		return fareCat;
	}

	/**
	 * @param fareCat
	 *            the fareCat to set
	 */
	public void setFareCat(String fareCat) {
		this.fareCat = fareCat;
	}

	/**
	 * @return the modBufferTime
	 */
	public String getModBufferTime() {
		return modBufferTime;
	}

	/**
	 * @param modBufferTime
	 *            the modBufferTime to set
	 */
	public void setModBufferTime(String modBufferTime) {
		this.modBufferTime = modBufferTime;
	}

	/**
	 * @return the ruleComments
	 */
	public String getRuleComments() {
		return ruleComments;
	}

	/**
	 * @param ruleComments
	 *            the ruleComments to set
	 */
	public void setRuleComments(String ruleComments) {
		this.ruleComments = ruleComments;
	}

	/**
	 * @return the mFRules
	 */
	public String getmFRules() {
		return mFRules;
	}

	/**
	 * @param mFRules
	 *            the mFRules to set
	 */
	public void setmFRules(String mFRules) {
		this.mFRules = mFRules;
	}

	/**
	 * @return the childPayApplicable
	 */
	public String getChildPayApplicable() {
		return childPayApplicable;
	}

	/**
	 * @param childPayApplicable
	 *            the childPayApplicable to set
	 */
	public void setChildPayApplicable(String childPayApplicable) {
		this.childPayApplicable = childPayApplicable;
	}

	/**
	 * @return the childRefund
	 */
	public String getChildRefund() {
		return childRefund;
	}

	/**
	 * @param childRefund
	 *            the childRefund to set
	 */
	public void setChildRefund(String childRefund) {
		this.childRefund = childRefund;
	}

	/**
	 * @return the childMode
	 */
	public String getChildMode() {
		return childMode;
	}

	/**
	 * @param childMode
	 *            the childMode to set
	 */
	public void setChildMode(String childMode) {
		this.childMode = childMode;
	}

	/**
	 * @return the childCnx
	 */
	public String getChildCnx() {
		return childCnx;
	}

	/**
	 * @param childCnx
	 *            the childCnx to set
	 */
	public void setChildCnx(String childCnx) {
		this.childCnx = childCnx;
	}

	/**
	 * @return the childNoShow
	 */
	public BigDecimal getChildNoShow() {
		return childNoShow;
	}

	/**
	 * @param childNoShow
	 *            the childNoShow to set
	 */
	public void setChildNoShow(BigDecimal childNoShow) {
		this.childNoShow = childNoShow;
	}

	/**
	 * @return the infantPay
	 */
	public String getInfantPay() {
		return infantPay;
	}

	/**
	 * @param infantPay
	 *            the infantPay to set
	 */
	public void setInfantPay(String infantPay) {
		this.infantPay = infantPay;
	}

	/**
	 * @return the infantRefund
	 */
	public String getInfantRefund() {
		return infantRefund;
	}

	/**
	 * @param infantRefund
	 *            the infantRefund to set
	 */
	public void setInfantRefund(String infantRefund) {
		this.infantRefund = infantRefund;
	}

	/**
	 * @return the infantMode
	 */
	public String getInfantMode() {
		return infantMode;
	}

	/**
	 * @param infantMode
	 *            the infantMode to set
	 */
	public void setInfantMode(String infantMode) {
		this.infantMode = infantMode;
	}

	/**
	 * @return the infantCnx
	 */
	public String getInfantCnx() {
		return infantCnx;
	}

	/**
	 * @param infantCnx
	 *            the infantCnx to set
	 */
	public void setInfantCnx(String infantCnx) {
		this.infantCnx = infantCnx;
	}

	/**
	 * @return the infantNoShow
	 */
	public BigDecimal getInfantNoShow() {
		return infantNoShow;
	}

	/**
	 * @param infantNoShow
	 *            the infantNoShow to set
	 */
	public void setInfantNoShow(BigDecimal infantNoShow) {
		this.infantNoShow = infantNoShow;
	}

	/**
	 * @return the adultPay
	 */
	public String getAdultPay() {
		return adultPay;
	}

	/**
	 * @param adultPay
	 *            the adultPay to set
	 */
	public void setAdultPay(String adultPay) {
		this.adultPay = adultPay;
	}

	/**
	 * @return the adultRefund
	 */
	public String getAdultRefund() {
		return adultRefund;
	}

	/**
	 * @param adultRefund
	 *            the adultRefund to set
	 */
	public void setAdultRefund(String adultRefund) {
		this.adultRefund = adultRefund;
	}

	/**
	 * @return the adultMode
	 */
	public String getAdultMode() {
		return adultMode;
	}

	/**
	 * @param adultMode
	 *            the adultMode to set
	 */
	public void setAdultMode(String adultMode) {
		this.adultMode = adultMode;
	}

	/**
	 * @return the adultCnx
	 */
	public String getAdultCnx() {
		return adultCnx;
	}

	/**
	 * @param adultCnx
	 *            the adultCnx to set
	 */
	public void setAdultCnx(String adultCnx) {
		this.adultCnx = adultCnx;
	}

	/**
	 * @return the adultNoShow
	 */
	public BigDecimal getAdultNoShow() {
		return adultNoShow;
	}

	/**
	 * @param adultNoShow
	 *            the adultNoShow to set
	 */
	public void setAdultNoShow(BigDecimal adultNoShow) {
		this.adultNoShow = adultNoShow;
	}

	/**
	 * @return the openReturn
	 */
	public String getOpenReturn() {
		return openReturn;
	}

	/**
	 * @param openReturn
	 *            the openReturn to set
	 */
	public void setOpenReturn(String openReturn) {
		this.openReturn = openReturn;
	}

	/**
	 * @return the orMonth
	 */
	public String getOrMonth() {
		return orMonth;
	}

	/**
	 * @param orMonth
	 *            the orMonth to set
	 */
	public void setOrMonth(String orMonth) {
		this.orMonth = orMonth;
	}

	/**
	 * @return the orDays
	 */
	public String getOrDays() {
		return orDays;
	}

	/**
	 * @param orDays
	 *            the orDays to set
	 */
	public void setOrDays(String orDays) {
		this.orDays = orDays;
	}

	/**
	 * @return the orHours
	 */
	public String getOrHours() {
		return orHours;
	}

	/**
	 * @param orHours
	 *            the orHours to set
	 */
	public void setOrHours(String orHours) {
		this.orHours = orHours;
	}

	/**
	 * @return the orMins
	 */
	public String getOrMins() {
		return orMins;
	}

	/**
	 * @param orMins
	 *            the orMins to set
	 */
	public void setOrMins(String orMins) {
		this.orMins = orMins;
	}

	/**
	 * @return the minStayOverTime
	 */
	public String getMinStayOverTime() {
		return minStayOverTime;
	}

	/**
	 * @param minStayOverTime
	 *            the minStayOverTime to set
	 */
	public void setMinStayOverTime(String minStayOverTime) {
		this.minStayOverTime = minStayOverTime;
	}

	/**
	 * @return the maxStayOverTime
	 */
	public String getMaxStayOverTime() {
		return maxStayOverTime;
	}

	/**
	 * @param maxStayOverTime
	 *            the maxStayOverTime to set
	 */
	public void setMaxStayOverTime(String maxStayOverTime) {
		this.maxStayOverTime = maxStayOverTime;
	}

	/**
	 * @return the openReturnLimit
	 */
	public String getOpenReturnLimit() {
		return openReturnLimit;
	}

	/**
	 * @param openReturnLimit
	 *            the openReturnLimit to set
	 */
	public void setOpenReturnLimit(String openReturnLimit) {
		this.openReturnLimit = openReturnLimit;
	}

	/**
	 * @return the inBound
	 */
	public String getInBound() {
		return inBound;
	}

	/**
	 * @param inBound
	 *            the inBound to set
	 */
	public void setInBound(String inBound) {
		this.inBound = inBound;
	}

	/**
	 * @return the outBound
	 */
	public String getOutBound() {
		return outBound;
	}

	/**
	 * @param outBound
	 *            the outBound to set
	 */
	public void setOutBound(String outBound) {
		this.outBound = outBound;
	}

	/**
	 * @return the modificationBuffer
	 */
	public String getModificationBuffer() {
		return modificationBuffer;
	}

	/**
	 * @param modificationBuffer
	 *            the modificationBuffer to set
	 */
	public void setModificationBuffer(String modificationBuffer) {
		this.modificationBuffer = modificationBuffer;
	}

}
