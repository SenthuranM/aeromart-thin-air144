package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * DTO to hold IBEOnHoldPassengersReport Resultset details
 * 
 * @author asiri
 */

public class IBEOnHoldPassengersDTO implements Serializable {

	private static final long serialVersionUID = 8066616493354017384L;
	private String reservationDate;
	private String releaseDate;
	private String flightDate;
	private String flightNumber;
	private String pnr;
	private String noOfPaxOnHold;
	private String contactName;
	private String sector;
	private String resTel;
	private String mobile;
	private String segStatus;
	private String mxTime;
	private String modDate;

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getNoOfPaxOnHold() {
		return noOfPaxOnHold;
	}

	public void setNoOfPaxOnHold(String noOfPaxOnHold) {
		this.noOfPaxOnHold = noOfPaxOnHold;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getResTel() {
		return resTel;
	}

	public void setResTel(String resTel) {
		this.resTel = resTel;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSegStatus() {
		return segStatus;
	}

	public void setSegStatus(String segStatus) {
		this.segStatus = segStatus;
	}

	public String getMxTime() {
		return mxTime;
	}

	public void setMxTime(String mxTime) {
		this.mxTime = mxTime;
	}

	public String getModDate() {
		return modDate;
	}

	public void setModDate(String modDate) {
		this.modDate = modDate;
	}

}
