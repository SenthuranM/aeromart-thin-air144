package com.isa.thinair.reporting.api.dto.aviator;

/**
* @author Manoj Dhanushka
* 
*/
public class AviatorFlightSegInventoryDTO {
	
	private Integer flightId;
	
	private String segmentCode;

	private String cabinClassCode;
	
	private int capacity;
	
	private int totalBookings;
	
	private int overSellAdult;
	
	private int curtail;
	
	private int wlAlloc;
	
	private int availableAdult;
	
	private int overbookAdult;
	
	private int waitlistAdult;
	
	private String FltSegStatus;		

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getTotalBookings() {
		return totalBookings;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setTotalBookings(int totalBookings) {
		this.totalBookings = totalBookings;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getOverSellAdult() {
		return overSellAdult;
	}

	public int getCurtail() {
		return curtail;
	}

	public int getWlAlloc() {
		return wlAlloc;
	}

	public int getAvailableAdult() {
		return availableAdult;
	}

	public int getOverbookAdult() {
		return overbookAdult;
	}

	public int getWaitlistAdult() {
		return waitlistAdult;
	}

	public String getFltSegStatus() {
		return FltSegStatus;
	}

	public void setOverSellAdult(int overSellAdult) {
		this.overSellAdult = overSellAdult;
	}

	public void setCurtail(int curtail) {
		this.curtail = curtail;
	}

	public void setWlAlloc(int wlAlloc) {
		this.wlAlloc = wlAlloc;
	}

	public void setAvailableAdult(int availableAdult) {
		this.availableAdult = availableAdult;
	}

	public void setOverbookAdult(int overbookAdult) {
		this.overbookAdult = overbookAdult;
	}

	public void setWaitlistAdult(int waitlistAdult) {
		this.waitlistAdult = waitlistAdult;
	}

	public void setFltSegStatus(String fltSegStatus) {
		FltSegStatus = fltSegStatus;
	}
}
