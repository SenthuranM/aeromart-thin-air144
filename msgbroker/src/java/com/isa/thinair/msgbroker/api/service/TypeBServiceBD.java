package com.isa.thinair.msgbroker.api.service;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRS;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;

/**
 * @author Manoj Dhanushka
 */
public interface TypeBServiceBD {
	
	public static final String SERVICE_NAME = "TypeBService";

	/**
	 * @throws ModuleException
	 */
	public TypeBRS processTypeBMessage(TypeBRQ typeBRQ) throws ModuleException;
	
	/**
	 * @throws ModuleException
	 */
	public TypeBRS composeAndSendTypeBMessage(TypeBRQ typeBRQ) throws ModuleException;
	
	/**
	 * @throws ModuleException
	 */
	public String composeAndSendTypeBMessages(List<TypeBRQ> typeBRQs) throws ModuleException;

	public TypeBAnalyseRS analyseTypeBMessage(TypeBAnalyseRQ typeBAnalyseRQ) throws ModuleException;

}
