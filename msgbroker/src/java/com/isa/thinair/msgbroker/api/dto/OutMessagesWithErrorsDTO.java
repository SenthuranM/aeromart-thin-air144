package com.isa.thinair.msgbroker.api.dto;

import java.util.Collection;

import com.isa.thinair.msgbroker.api.model.OutMessage;

public class OutMessagesWithErrorsDTO {
	OutMessage outMessage;
	Collection outMessageProcessingStatuses;

	public OutMessage getOutMessage() {
		return outMessage;
	}

	public void setOutMessage(OutMessage outMessage) {
		this.outMessage = outMessage;
	}

	public Collection getOutMessageProcessingStatuses() {
		return outMessageProcessingStatuses;
	}

	public void setOutMessageProcessingStatuses(Collection outMessageProcessingStatuses) {
		this.outMessageProcessingStatuses = outMessageProcessingStatuses;
	}

}
