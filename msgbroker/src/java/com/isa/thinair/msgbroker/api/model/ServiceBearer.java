package com.isa.thinair.msgbroker.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_SERVICE_BEARER"
 */
public class ServiceBearer extends Persistent {

	private String airlineCode;
	
	private String gdsCode;

	private String airlineIATACode;

	private String airlineDescripton;

	private boolean recordStatus;

	private String sitaAddress;

	private String emailAddress;

	private String incommingEmailServerIP;

	private String incommingEmailSvrUserID;

	private String incommingEmailSvrUserPassword;

	private Integer incommingMailSvrListeningPort;

	private String outgoingEmailServerIP;

	private String outgoingEmailSvrUserID;

	private String outgoingEmailSvrUserPassword;

	private Integer outgoingEmailSvrListeningPort;

	private String outgoingEmailFromAddress;

	private String outgoingTypeBMessagingSvrName;
	
	private String mailAccessProtocol;

	private String messagingMode;

	private String messageInLocation;

	private String messageInBackupLocation;

	private String messageOutLocation;

	private String messageType;
	
	private String outgoingMailAccessProtocol;

	public enum MessagingMode {
		MAIL, FILE
	}

	/**
	 * @return Returns the mailAccessProtocol.
	 * 
	 * @hibernate.property column = "MAIL_ACCESS_PROTOCOL" 
	 */
	public String getMailAccessProtocol() {
		return mailAccessProtocol;
	}

	public void setMailAccessProtocol(String mailAccessProtocol) {
		this.mailAccessProtocol = mailAccessProtocol;
	}

	/**
	 * @return Returns the EmailAddress.
	 * 
	 * @hibernate.property column = "EMAIL_ADDRESS"
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The EmailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	/**
	 * @return the gdsCode
	 * 
	 * @hibernate.id column = "GDS_CODE" generator-class = "assigned"
	 */
	public String getGdsCode() {
		return gdsCode;
	}

	/**
	 * @param gdsCode the gdsCode to set
	 */
	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	/**
	 * @return Returns the SitaAddress.
	 * 
	 * @hibernate.property column = "SITA_ADDRESS"
	 */
	public String getSitaAddress() {
		return sitaAddress;
	}

	/**
	 * @param sitaAddress
	 *            The SitaAddress to set.
	 */
	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	// ----------------------------------------

	/**
	 * @return Returns the airlineCode.
	 * 
	 * @hibernate.property column = "AIRLINE_CODE" 
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            The airlineCode to set.
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode.trim();
	}

	/**
	 * @return Returns the airlineDescripton.
	 * 
	 * @hibernate.property column = "AIRLINE_DESCRIPTION"
	 */
	public String getAirlineDescripton() {
		return airlineDescripton;
	}

	/**
	 * @param airlineDescripton
	 *            The airlineDescripton to set.
	 */
	public void setAirlineDescripton(String airlineDescripton) {
		this.airlineDescripton = airlineDescripton;
	}

	/**
	 * @return Returns the airlineIATACode.
	 * 
	 * @hibernate.property column = "AIRLINE_IATA_CODE"
	 */
	public String getAirlineIATACode() {
		return airlineIATACode;
	}

	/**
	 * @param airlineIATACode
	 *            The airlineIATACode to set.
	 */
	public void setAirlineIATACode(String airlineIATACode) {
		this.airlineIATACode = airlineIATACode;
	}

	/**
	 * @return Returns the recordStatus.
	 * 
	 * @hibernate.property column = "RECORD_STATUS" type="yes_no"
	 */
	public boolean getRecordStatus() {
		return recordStatus;
	}

	/**
	 * @param recordStatus
	 *            The recordStatus to set.
	 */
	public void setRecordStatus(boolean recordStatus) {
		this.recordStatus = recordStatus;
	}

	/**
	 * @hibernate.property column = "in_emailsvrip"
	 */
	public String getIncommingEmailServerIP() {
		return incommingEmailServerIP;
	}

	public void setIncommingEmailServerIP(String incommingEmailServerIP) {
		this.incommingEmailServerIP = incommingEmailServerIP;
	}

	/**
	 * @hibernate.property column = "in_emailusrid"
	 */
	public String getIncommingEmailSvrUserID() {
		return incommingEmailSvrUserID;
	}

	public void setIncommingEmailSvrUserID(String incommingEmailSvrUserID) {
		this.incommingEmailSvrUserID = incommingEmailSvrUserID;
	}

	/**
	 * @hibernate.property column = "in_emailusrpswd"
	 */
	public String getIncommingEmailSvrUserPassword() {
		return incommingEmailSvrUserPassword;
	}

	public void setIncommingEmailSvrUserPassword(String incommingEmailSvrUserPassword) {
		this.incommingEmailSvrUserPassword = incommingEmailSvrUserPassword;
	}

	/**
	 * @hibernate.property column = "in_listeningport"
	 */
	public Integer getIncommingMailSvrListeningPort() {
		return incommingMailSvrListeningPort;
	}

	public void setIncommingMailSvrListeningPort(Integer incommingMailSvrListeningPort) {
		this.incommingMailSvrListeningPort = incommingMailSvrListeningPort;
	}

	/**
	 * @hibernate.property column = "out_from_mail_address"
	 */
	public String getOutgoingEmailFromAddress() {
		return outgoingEmailFromAddress;
	}

	public void setOutgoingEmailFromAddress(String outgoingEmailFromAddress) {
		this.outgoingEmailFromAddress = outgoingEmailFromAddress;
	}

	/**
	 * @hibernate.property column = "out_emailsvrip"
	 */
	public String getOutgoingEmailServerIP() {
		return outgoingEmailServerIP;
	}

	public void setOutgoingEmailServerIP(String outgoingEmailServerIP) {
		this.outgoingEmailServerIP = outgoingEmailServerIP;
	}

	/**
	 * @hibernate.property column = "out_listeningport"
	 */
	public Integer getOutgoingEmailSvrListeningPort() {
		return outgoingEmailSvrListeningPort;
	}

	public void setOutgoingEmailSvrListeningPort(Integer outgoingEmailSvrListeningPort) {
		this.outgoingEmailSvrListeningPort = outgoingEmailSvrListeningPort;
	}

	/**
	 * @hibernate.property column = "out_emailusrid"
	 */
	public String getOutgoingEmailSvrUserID() {
		return outgoingEmailSvrUserID;
	}

	public void setOutgoingEmailSvrUserID(String outgoingEmailSvrUserID) {
		this.outgoingEmailSvrUserID = outgoingEmailSvrUserID;
	}

	/**
	 * @hibernate.property column = "out_emailusrpswd"
	 */
	public String getOutgoingEmailSvrUserPassword() {
		return outgoingEmailSvrUserPassword;
	}

	public void setOutgoingEmailSvrUserPassword(String outgoingEmailSvrUserPassword) {
		this.outgoingEmailSvrUserPassword = outgoingEmailSvrUserPassword;
	}

	/**
	 * @hibernate.property column = "MESSAGING_MODE"
	 */
	public String getMessagingMode() {
		return messagingMode;
	}

	public void setMessagingMode(String messagingMode) {
		this.messagingMode = messagingMode;
	}

	/**
	 * @hibernate.property column = "MSG_IN_LOCATION"
	 */
	public String getMessageInLocation() {
		return messageInLocation;
	}

	public void setMessageInLocation(String messageInLocation) {
		this.messageInLocation = messageInLocation;
	}

	/**
	 * @hibernate.property column = "MSG_IN_BACKUP_LOCATION"
	 */
	public String getMessageInBackupLocation() {
		return messageInBackupLocation;
	}

	public void setMessageInBackupLocation(String messageInBackupLocation) {
		this.messageInBackupLocation = messageInBackupLocation;
	}

	/**
	 * @hibernate.property column = "MSG_OUT_LOCATION"
	 */
	public String getMessageOutLocation() {
		return messageOutLocation;
	}

	public void setMessageOutLocation(String messageOutLocation) {
		this.messageOutLocation = messageOutLocation;
	}

	/**
	 * @hibernate.property column = "MSG_TYPE"
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * Convert the airline object contents to an string. This follows the following format. Entity = entity_name;
	 * field_name_1 = field_1_value; field_name_2 = field_2_value;
	 */
	public String toString() {
		// TODO : add new fields.
		return super.toString() + " AirlineCode = " + (this.getAirlineCode() == null ? "" : this.getAirlineCode())
				+ "; AirlineDescripton = " + (this.getAirlineDescripton() == null ? "" : this.getAirlineDescripton())
				+ "; AirlineIATACode = " + (this.getAirlineIATACode() == null ? "" : this.getAirlineIATACode())
				+ "; RecordStatus = " + (this.getRecordStatus() ? "" : "");
	}

	/**
	 * @hibernate.property column = "OUT_MAIL_ACCESS_PROTOCOL"
	 */
	public String getOutgoingMailAccessProtocol() {
		return outgoingMailAccessProtocol;
	}

	public void setOutgoingMailAccessProtocol(String outgoingMailAccessProtocol) {
		this.outgoingMailAccessProtocol = outgoingMailAccessProtocol;
	}
}
