package com.isa.thinair.msgbroker.api.util;

import java.io.Serializable;

public class AVSConstants implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum AVSRbdEventCode {
		AVS_AVAIL_BELOW_TRESH,  
	    AVS_SEG_AVAILABILITY_CHANGE,
	    AVS_RBD_AVAILABILITY_CHANGE,
	    AVS_RBD_SEG_AVAILABILITY_CHANGE,
	    AVS_RBD_SEG_AVAILABILITY_RESET,
	    AVS_RBD_CLOSED,
	    AVS_RBD_REQUEST,
	    AVS_RBD_REOPEN, 
	    AVS_AVAIL_ABOVE_THRESHOLD,
		// TODO review: even though these are segment level (flight, schedule actually)
		// could be said as above BC level operations, these have to be published BC wise.
	    AVS_CREATE_FLIGHT,
	    AVS_PUBLISH_FLIGHT
	
	}
	
	public enum AVSSegmentEventCode {   
	    AVS_CREATE_FLIGHT,
        AVS_PUBLISH_FLIGHT,
	    AVS_UNPUBLISED_FLIGHT,
	    AVS_FLIGHT_CLOSED,
	    AVS_CANCEL_FLIGHT,
	    AVS_FLIGHT_REOPEN;
	}
	
	
}
