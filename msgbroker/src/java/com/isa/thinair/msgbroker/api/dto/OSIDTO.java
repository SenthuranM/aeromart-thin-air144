package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/*
 * OtherServiceInfoDTO.java
 *
 * Created on October 11, 2007, 4:01 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 * 
 * @author Administrator
 */
public class OSIDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of OtherServiceInfoDTO
     */
    public OSIDTO() {
    }

    private String codeOSI = null;

    private String carrierCode = null;

    private String osiValue = null;

    private String fullElement = null;

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCodeOSI() {
        return codeOSI;
    }

    public void setCodeOSI(String codeOSI) {
        this.codeOSI = codeOSI;
    }

    public String getFullElement() {
        return fullElement;
    }

    public void setFullElement(String fullElement) {
        this.fullElement = fullElement;
    }

    public String getOsiValue() {
        return osiValue;
    }

    public void setOsiValue(String osiValue) {
        this.osiValue = osiValue;
    }

}
