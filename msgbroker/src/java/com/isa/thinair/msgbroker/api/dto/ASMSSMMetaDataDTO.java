package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


public class ASMSSMMetaDataDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String messageType;
	private Date dateDownloaded;
	private String timeMode = "UTC";
	private Integer dateOfMessage;
	private String monthOfMessage;
	private Integer messageGroupSerialNumber;
	private String endCode;
	private String messageSerialNumber;
	private String messageSequenceReference;
	private String creatorReference;
	private String supplementaryInfo;
	private Integer inMessegeID;
	private Collection<ASMSSMSubMsgsDTO> asmSSMSubMsgsList;


	public String getMessageSequenceReference() {
		return messageSequenceReference;
	}

	public void setMessageSequenceReference(String messageSequenceReference) {
		if(messageSequenceReference!=null){
			messageSequenceReference = messageSequenceReference.replaceAll("/", "");
			setDateOfMessage(Integer.parseInt(messageSequenceReference.substring(0, 2)));
			setMonthOfMessage(messageSequenceReference.substring(2, 5));
			setMessageGroupSerialNumber(Integer.parseInt(messageSequenceReference.substring(5, 10)));
			setEndCode(messageSequenceReference.substring(10, 11));
			setMessageSerialNumber(messageSequenceReference.substring(11, messageSequenceReference.length()));
		}
		
		this.messageSequenceReference = messageSequenceReference;
	}

	public Integer getDateOfMessage() {
		return dateOfMessage;
	}

	public void setDateOfMessage(Integer dateOfMessage) {
		this.dateOfMessage = dateOfMessage;
	}

	public String getMonthOfMessage() {
		return monthOfMessage;
	}

	public void setMonthOfMessage(String monthOfMessage) {
		this.monthOfMessage = monthOfMessage;
	}

	public Integer getMessageGroupSerialNumber() {
		return messageGroupSerialNumber;
	}

	public void setMessageGroupSerialNumber(Integer messageGroupSerialNumber) {
		this.messageGroupSerialNumber = messageGroupSerialNumber;
	}

	public String getEndCode() {
		return endCode;
	}

	public void setEndCode(String endCode) {
		this.endCode = endCode;
	}

	public String getMessageSerialNumber() {
		return messageSerialNumber;
	}

	public void setMessageSerialNumber(String messageSerialNumber) {
		this.messageSerialNumber = messageSerialNumber;
	}

	public String getCreatorReference() {
		return creatorReference;
	}

	public void setCreatorReference(String creatorReference) {
		
		if(creatorReference!=null){
			this.creatorReference =  creatorReference.replaceAll("/", " ");
		}else{
			this.creatorReference = "";
		}
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}


	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	public Collection<ASMSSMSubMsgsDTO> getAsmSSMSubMsgsList() {
		return asmSSMSubMsgsList;
	}

	public void setAsmSSMSubMsgsList(Collection<ASMSSMSubMsgsDTO> asmSSMSubMsgsList) {
		this.asmSSMSubMsgsList = asmSSMSubMsgsList;
	}

	@Override
	public String toString() {
		String nextLine = "\n";
		String space = " ";
		StringBuilder sb = new StringBuilder();
		sb.append(messageType);
		sb.append(nextLine);
		sb.append(timeMode);
		sb.append(nextLine);
		sb.append(messageSequenceReference);
		sb.append(space);
		sb.append(creatorReference);
		sb.append(nextLine);
		if (supplementaryInfo != null) {
		sb.append(supplementaryInfo);
		sb.append(nextLine);
		}
		return sb.toString();
	}

	public Integer getInMessegeID() {
		return inMessegeID;
	}

	public void setInMessegeID(Integer inMessegeID) {
		this.inMessegeID = inMessegeID;
	}
}
