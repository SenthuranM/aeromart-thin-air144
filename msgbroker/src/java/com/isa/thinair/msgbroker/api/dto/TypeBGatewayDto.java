package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

public class TypeBGatewayDto implements Serializable {
	public enum MessageType {
		UNKNOWN, RESERVATION
	}

	private String gdsCarrierCode;
	private MessageType messageType;
	private String message;

	public TypeBGatewayDto() {
		messageType = MessageType.UNKNOWN;
	}

	public String getGdsCarrierCode() {
		return gdsCarrierCode;
	}

	public void setGdsCarrierCode(String gdsCarrierCode) {
		this.gdsCarrierCode = gdsCarrierCode;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
