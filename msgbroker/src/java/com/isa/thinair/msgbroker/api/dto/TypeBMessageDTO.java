package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Date;

public class TypeBMessageDTO implements Serializable, Comparable<TypeBMessageDTO> {

	public String messageDirection;

	public Date dateTime;

	public String rowMessage;

	public String processStatus;

	public TypeBMessageDTO(String messageDirection, Date dateTime, String rowMessage, String processStatus) {
		this.messageDirection = Direction.getMessageDirection(messageDirection);
		this.dateTime = dateTime;
		this.rowMessage = rowMessage;
		if (messageDirection.equals(Direction.I)) {
			this.processStatus = InMessageProcessStatus.getInMessageProcessStatus(processStatus);
		} else {
			this.processStatus = OutMessageProcessStatus.getOutMessageProcessStatus(processStatus);
		}
	}

	public enum Direction {

		I("I", "Received"), O("O", "Sent");
		private Direction(String code, String name) {
			this.code = code;
			this.name = name;
		}

		private final String code;
		private final String name;

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public static String getMessageDirection(String code) {
			for (Direction direction : Direction.values()) {
				if (direction.getCode().equals(code)) {
					return direction.getName();
				}
			}
			return null;
		}
	}

	public enum InMessageProcessStatus {
		U("U", "Unprocessed"), E("E", "Unparsable"), P("P", "Processed"), T("T", "TimedOut"), X("X", "ErroInProcessing");
		private InMessageProcessStatus(String code, String name) {
			this.code = code;
			this.name = name;
		}

		private final String code;
		private final String name;

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public static String getInMessageProcessStatus(String code) {
			for (InMessageProcessStatus processStatus : InMessageProcessStatus.values()) {
				if (processStatus.getCode().equals(code)) {
					return processStatus.getName();
				}
			}
			return null;
		}
	}

	public enum OutMessageProcessStatus {
		I("I", "InvalidResponseMessage"), N("N", "Untransmitted"), S("S", "MailServerAccepted"), D("D", "TimedOut"), F("F",
				"TransmissionFailure"), T("T", "Sent");
		private OutMessageProcessStatus(String code, String name) {
			this.code = code;
			this.name = name;
		}

		private final String code;
		private final String name;

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public static String getOutMessageProcessStatus(String code) {
			for (OutMessageProcessStatus processStatus : OutMessageProcessStatus.values()) {
				if (processStatus.getCode().equals(code)) {
					return processStatus.getName();
				}
			}
			return null;
		}
	}

	public String getMessageDirection() {
		return messageDirection;
	}

	public void setMessageDirection(String messageDirection) {
		this.messageDirection = messageDirection;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getRowMessage() {
		return rowMessage;
	}

	public void setRowMessage(String rowMessage) {
		this.rowMessage = rowMessage;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	@Override
	public int compareTo(TypeBMessageDTO o) {
		return (o.getDateTime()).compareTo(this.getDateTime());
	}

}
