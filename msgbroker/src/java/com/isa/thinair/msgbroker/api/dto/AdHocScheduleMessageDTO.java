package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.msgbroker.api.util.ReservationMessageProcessingConstants.ScheduleMessage.ActionIdentifier;
import com.isa.thinair.msgbroker.api.util.ReservationMessageProcessingConstants.ScheduleMessage.TimeMode;

//import org.opentravel.ota._2003._05.GlobalIndicatorType;

public class AdHocScheduleMessageDTO implements Serializable {

	// private static final long serialVersionUID = 1152261372021229733L;

	private TimeMode timeMode;
	private ActionIdentifier actionIdentifier;
	private String creatorReference;
	private String flightDesignator;
	private Date flightDate;
	private List<FlightLegDTO> changedFlightLegs; // Only applicable to 'CNL', 'RIN', 'EQT' and 'FLT')
	private String newFlightDesignator; // Used only for 'FLT' action identifier
	private Date newFlightDate;
	private String serviceType;
	private String aircraftType;
	private Collection RBDs;
	private Date fromDateOfExistingPeriod; // Used only for 'REV' action identifier to specify effective period's from
											// date.
	private Date toDateOfExistingPeriod; // Used only for 'REV' action identifier to specify effective period's to date.
	private List<FlightScheduleLegDTO> legs;
	private List<FlightScheduleSegmentDTO> segments;

	/**
	 * Returns sub message action identifier Example: NEW Format: aaa
	 * 
	 * @return
	 */
	public ActionIdentifier getActionIdentifier() {
		return actionIdentifier;
	}

	/**
	 * Sets sub message action identifier Format: aaa Example: NEW
	 * 
	 * @param actionIdentifier
	 */
	public void setActionIdentifier(ActionIdentifier actionIdentifier) {
		this.actionIdentifier = actionIdentifier;
	}

	/**
	 * Returns new flight designator (new flight number with airline code and operational suffix) Used only with 'FLT'
	 * action identifier
	 * 
	 * @return
	 */
	public String getNewFlightDesignator() {
		return newFlightDesignator;
	}

	/**
	 * Sets new flight designator (flight number with airline code and operational suffix) Used only with 'FLT' action
	 * identifier Format: XX(a)nnn(n)(a) Example: LX544
	 * 
	 * @param flightDesignator
	 */
	public void setNewFlightDesignator(String newFlightDesignator) {
		this.newFlightDesignator = newFlightDesignator;
	}

	/**
	 * Returns service type Format: a Example: G
	 * 
	 * @return
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * Sets service type Format: a Example: G
	 * 
	 * @param serviceType
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * Returns aircraft type (aircraft model) Format: xxx Example: M80
	 * 
	 * @return
	 */
	public String getAircraftType() {
		return aircraftType;
	}

	/**
	 * Sets aircraft type (aircraft model) Format: xxx Example: M80
	 * 
	 * @param aircraftType
	 */
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	/**
	 * Returns creator reference. This is used for keeping reference to the originator's records Format: x(x[-34])
	 * Example: REF 123/449
	 * 
	 * @return
	 */
	public String getCreatorReference() {
		return creatorReference;
	}

	public void setCreatorReference(String creatorReference) {
		this.creatorReference = creatorReference;
	}

	/**
	 * Returns flight designator (flight number with airline code and operational suffix)
	 * 
	 * @return
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	/**
	 * Sets flight designator (flight number with airline code and operational suffix) Format: XX(a)nnn(n)(a) Example:
	 * LX544
	 * 
	 * @param flightDesignator
	 */
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public Date getFromDateOfExistingPeriod() {
		return fromDateOfExistingPeriod;
	}

	public void setFromDateOfExistingPeriod(Date fromDateOfExistingPeriod) {
		this.fromDateOfExistingPeriod = fromDateOfExistingPeriod;
	}

	public Collection getRBDs() {
		return RBDs;
	}

	public void setRBDs(Collection ds) {
		RBDs = ds;
	}

	public TimeMode getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(TimeMode timeMode) {
		this.timeMode = timeMode;
	}

	public Date getToDateOfExistingPeriod() {
		return toDateOfExistingPeriod;
	}

	public void setToDateOfExistingPeriod(Date toDateOfExistingPeriod) {
		this.toDateOfExistingPeriod = toDateOfExistingPeriod;
	}

	public List<FlightLegDTO> getChangedFlightLegs() {
		return changedFlightLegs;
	}

	public void setChangedFlightLegs(List<FlightLegDTO> changedFlightLegs) {
		this.changedFlightLegs = changedFlightLegs;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public List<FlightScheduleLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<FlightScheduleLegDTO> legs) {
		this.legs = legs;
	}

	public Date getNewFlightDate() {
		return newFlightDate;
	}

	public void setNewFlightDate(Date newFlightDate) {
		this.newFlightDate = newFlightDate;
	}

	public List<FlightScheduleSegmentDTO> getSegments() {
		return segments;
	}

	public void setSegments(List<FlightScheduleSegmentDTO> segments) {
		this.segments = segments;
	}

}
