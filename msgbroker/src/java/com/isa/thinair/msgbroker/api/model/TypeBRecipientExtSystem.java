package com.isa.thinair.msgbroker.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_TYPE_B_RECIPIENT_EXT_SYSTEM"
 */

public class TypeBRecipientExtSystem implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer typeBRecipientExtSystemID;
	private String recipientAddress;
	private String messagingMode;
	private String processState;
	private Date processedDate;

	/**
	 * @return the typeBRecipientExtSystemID
	 * @hibernate.id column = "TYPE_B_RECIPIENT_EXT_SYSTEM_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_TYPE_B_RECIPIENT_EXT_SYSTEM"
	 */
	public Integer getTypeBRecipientExtSystemID() {
		return typeBRecipientExtSystemID;
	}

	/**
	 * @param typeBRecipientExtSystemID
	 *            the typeBRecipientExtSystemID to set
	 */
	public void setTypeBRecipientExtSystemID(Integer typeBRecipientExtSystemID) {
		this.typeBRecipientExtSystemID = typeBRecipientExtSystemID;
	}

	/**
	 * @return the recipientAddress
	 * @hibernate.property column = "RECIPIENT_ADDRESS"
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress
	 *            the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the messagingMode
	 * @hibernate.property column = "MESSAGING_MODE"
	 */
	public String getMessagingMode() {
		return messagingMode;
	}

	/**
	 * @param messagingMode
	 *            the messagingMode to set
	 */
	public void setMessagingMode(String messagingMode) {
		this.messagingMode = messagingMode;
	}

	/**
	 * @return the processState
	 * @hibernate.property column = "STATE"
	 */
	public String getProcessState() {
		return processState;
	}

	/**
	 * @param processState
	 *            the processState to set
	 */
	public void setProcessState(String processState) {
		this.processState = processState;
	}

	/**
	 * @return the processedDate
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	/**
	 * @param processedDate
	 *            the processedDate to set
	 */
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}	

}
