package com.isa.thinair.msgbroker.api.model;

import java.sql.Timestamp;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_IN_MSG_SEQUENCE_QUEUE"
 */

public class MessageSequenceQueue extends Persistent {

	private static final long serialVersionUID = 1L;
	private Integer messageSequenceId;
	private String processedStatus;
	private Date receievedDate;
	private Timestamp lastBatchReceived;

	/**
	 * 
	 * @hibernate.id column = "MSG_SEQ_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "MB_S_IN_MSG_SEQUENCE_QUEUE"
	 */
	public Integer getMessageSequenceId() {
		return messageSequenceId;
	}

	public void setMessageSequenceId(Integer messageSequenceId) {
		this.messageSequenceId = messageSequenceId;
	}

	/**
	 * returns the processedStatus
	 * 
	 * @return Returns the processedStatus.
	 * 
	 * @hibernate.property column = "PROCESSED_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * 
	 * @hibernate.property column = "RECEIVED_DATE"
	 */
	public Date getReceievedDate() {
		return receievedDate;
	}

	public void setReceievedDate(Date receievedDate) {
		this.receievedDate = receievedDate;
	}

	/**
	 * 
	 * @hibernate.property column = "LAST_BATCH_RECEIVED_AT"
	 */
	public Timestamp getLastBatchReceived() {
		return lastBatchReceived;
	}

	public void setLastBatchReceived(Timestamp lastBatchReceived) {
		this.lastBatchReceived = lastBatchReceived;
	}

}