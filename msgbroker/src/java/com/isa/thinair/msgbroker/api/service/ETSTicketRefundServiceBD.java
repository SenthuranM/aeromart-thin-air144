package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.commons.api.dto.ets.RefundTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.RefundTKTRESResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ETSTicketRefundServiceBD {
	
	public static final String SERVICE_NAME = "TicketRefundService";
	
	public RefundTKTRESResponseDTO refund(RefundTKTREQDTO ticketRefundRequest) throws ModuleException;
	
	
}
