/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/**
 * @author sanjeewaf
 */
public class SSRDTO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -2209912338012885430L;

	private String codeSSR = null;

	private String carrierCode = null;

	private String ssrValue = null;

	private String fullElement = null;

	private String actionOrStatusCode = null;

	private String adviceOrStatusCode = null;

	private String freeText = null;

	private boolean includeInResponse;

	public SSRDTO() {
		includeInResponse = true;
	}

	/**
	 * @return the actionOrStatusCode
	 */
	public String getActionOrStatusCode() {
		return actionOrStatusCode;
	}

	/**
	 * @param actionOrStatusCode
	 *            the actionOrStatusCode to set
	 */
	public void setActionOrStatusCode(String action_status_Code) {
		this.actionOrStatusCode = action_status_Code;
	}

	/**
	 * @return the adviceOrStatusCode
	 */
	public String getAdviceOrStatusCode() {
		return adviceOrStatusCode;
	}

	/**
	 * @param adviceOrStatusCode
	 *            the adviceOrStatusCode to set
	 */
	public void setAdviceOrStatusCode(String advice_status_Code) {
		this.adviceOrStatusCode = advice_status_Code;
	}

	/**
	 * @return the ssrValue
	 */
	public String getSsrValue() {
		return ssrValue;
	}

	/**
	 * @param ssrValue
	 *            the ssrValue to set
	 */
	public void setSsrValue(String ssrValue) {
		this.ssrValue = ssrValue;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the codeSSR
	 */
	public String getCodeSSR() {
		return codeSSR;
	}

	/**
	 * @param codeSSR
	 *            the codeSSR to set
	 */
	public void setCodeSSR(String codeSSR) {
		this.codeSSR = codeSSR;
	}

	/**
	 * @return the fullElement
	 */
	public String getFullElement() {
		return fullElement;
	}

	/**
	 * @param fullElement
	 *            the fullElement to set
	 */
	public void setFullElement(String fullElement) {
		this.fullElement = fullElement;
	}

	/**
	 * @return the freeText
	 */
	public String getFreeText() {
		return freeText;
	}

	/**
	 * @param freeText
	 *            the freeText to set
	 */
	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}

	public boolean isIncludeInResponse() {
		return includeInResponse;
	}

	public void setIncludeInResponse(boolean includeInResponse) {
		this.includeInResponse = includeInResponse;
	}
}
