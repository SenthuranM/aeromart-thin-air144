package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class _StandardScheduleMessageDTO implements Serializable {
	String timeMode; // Optional
	String creatorReference;
	String actionIdentifier;
	String asmWithdrawalIndicator;
	String flightDesignator;
	Date scheduleStartDate;
	Date scheduleEndDate;
	String datesOfOperation;
	String frequencyRate; // Optional
	String serviceType;
	String aircraftType;
	String paxReservationBookingDesgnators;

	// Collection of FlightScheduleLeg objects
	List flightScheduleLegs;

	private class FlightScheduleLeg {
		String departureStation;
		String scheduledTimeOfDeparture;
		int dateVariationForSTD; // Conditional
		String arrivalStation;
		String scheduledTimeOfArrival;
		int dateVariationForSTA; // Conditional
		String mealServiceNote;
	}

	// Collection of FlightScheduleSegment objects
	List flightScheduleSegments;

	private class FlightScheduleSegment {
		String departureStation;
		String arrivalStation;
		boolean isValid;
	}

	// public _StandardScheduleMessageDTO() {
	// this.frequencyRate = ReservationMessageProcessingConstants.ScheduleMessage.FrequencyRate.W1;
	// this.timeMode = ReservationMessageProcessingConstants.ScheduleMessage.TimeModes.LT;
	// // this.dateVariationForSTD = 0;
	// // this.dateVariationForSTA = 0;
	// }

	// public String validate(){
	// String errorText = "";
	// errorText = validateCreatorReference();
	// return errorText;
	// }

}
