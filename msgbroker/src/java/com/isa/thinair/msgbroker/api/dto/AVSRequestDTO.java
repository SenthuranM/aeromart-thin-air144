package com.isa.thinair.msgbroker.api.dto;

import java.util.ArrayList;
import java.util.List;

public class AVSRequestDTO extends RequestDTO {

	private static final long serialVersionUID = 7306190649955322553L;
	private String timestamp = null;
	private String messageType = null;
	private String recipeintAddress = null;
	private String senderAddress = null;
	private String communicationReference = null;

	private List<AVSRequestSegmentDTO> avsSegmentDTOs = null;

	/**
	 * Gets AVSSegmentDTOs colloection.
	 * 
	 * @return
	 */
	public List<AVSRequestSegmentDTO> getAVSSegmentDTOs() {
		return avsSegmentDTOs;
	}

	public void addAVSSegmentDTOs(AVSRequestSegmentDTO avsSegmentDTO) {
		if (avsSegmentDTOs == null)
			avsSegmentDTOs = new ArrayList<AVSRequestSegmentDTO>();

		avsSegmentDTOs.add(avsSegmentDTO);

	}

	/**
	 * Sets AVSSegmentDTOs colloection. Only 10 segments could be presented in the list.
	 * 
	 * @param segmentDTOs
	 */
	public void setAVSSegmentDTOs(List<AVSRequestSegmentDTO> avsSegmentDTOs) {
		this.avsSegmentDTOs = avsSegmentDTOs;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getRecipeintAddress() {
		return recipeintAddress;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public String getCommunicationReference() {
		return communicationReference;
	}

	public void setRecipeintAddress(String recipeintAddress) {
		this.recipeintAddress = recipeintAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public void setCommunicationReference(String communicationReference) {
		this.communicationReference = communicationReference;
	}
}
