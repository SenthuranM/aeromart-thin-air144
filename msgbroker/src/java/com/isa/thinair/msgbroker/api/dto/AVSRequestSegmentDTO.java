package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/**
 * 
 * @author M.Rikaz
 * 
 */
public class AVSRequestSegmentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flightDesignator;

	private String departureDate;

	private String departureStation;

	private String destinationStation;

	private String seatsAvailable;

	private String rbd;

	private String statusCode;

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	/**
	 * Returns flight designator (flight number with airline code)
	 * 
	 * @return
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	/**
	 * Sets flight designator (flight number with airline code) Format: XX(a)nnn(n) Example: LX544
	 * 
	 * @param flightDesignator
	 */
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	/**
	 * @return the seatsAvailable
	 */
	public String getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * @param seatsAvailable
	 *            the seatsAvailable to set
	 */
	public void setSeatsAvailable(String seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public String getRbd() {
		return rbd;
	}

	public void setRbd(String rbd) {
		this.rbd = rbd;
	}

	/**
	 * Status_NumericAvailability_Code is one of the following flight level status codes CR, CL, CC, CN, LR, LL, LC, LN
	 * 
	 * @return
	 */
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

}
