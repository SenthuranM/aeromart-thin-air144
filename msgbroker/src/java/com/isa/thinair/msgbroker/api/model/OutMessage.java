
package com.isa.thinair.msgbroker.api.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.DataConverterUtil;

/**
 * @hibernate.class table = "MB_T_OUT_MESSAGE"
 */

public class OutMessage extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final char NON_GDS_MESSAGE = 'Y';
	private static final char GDS_MESSAGE = 'N';

	private Integer outMessageID;
	private String airLineCode;
	private String messageType;
	private String sentTTYAddress;
	private String recipientTTYAddress;
	private String recordLocator;
	private Blob outMessage;
	private String outMessageText;
	private Date receivedDate;
	private String statusIndicator;
	private Date processedDate;
	private String lastErrorMessageCode;
	private Integer refInMessageID;
	private String sentEmailAddress;
	private String recipientEmailAddress;
	private String modeOfCommunication;
	private String gdsMessageID;
	private String pnr;
	private boolean processSuccess;
	private String wsServerUrl;
	private char nonGDSMessageFlag = 'N';
	private String responseStatus;
	
	/* TODO remove in production */
	// private String parsedXML;

	// public static final String Unprocessed = "U";
	// public static final String Unparsable = "E";
	// public static final String Processed = "P";

	/**
	 * returns the refInMessageID
	 * 
	 * @return Returns the refInMessageID.
	 * 
	 * @hibernate.property column = "REF_IN_MESSAGE_ID"
	 */
	public Integer getRefInMessageID() {
		return refInMessageID;
	}

	public void setRefInMessageID(Integer refInMessageID) {
		this.refInMessageID = refInMessageID;
	}

	/**
	 * returns the LastErrorMessageCode
	 * 
	 * @return Returns the LastErrorMessageCode.
	 * 
	 * @hibernate.property column = "LAST_ERROR_MESSAGE_CODE"
	 */
	public String getLastErrorMessageCode() {
		return lastErrorMessageCode;
	}

	public void setLastErrorMessageCode(String lastErrorMessageCode) {
		this.lastErrorMessageCode = lastErrorMessageCode;
	}

	/**
	 * returns the airLineCode
	 * 
	 * @return Returns the airLineCode.
	 * 
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirLineCode() {
		return airLineCode;
	}

	public void setAirLineCode(String airLineCode) {
		this.airLineCode = airLineCode;
	}

	/**
	 * returns the outMessageID
	 * 
	 * @return Returns the outMessageID.
	 * 
	 * @hibernate.id column = "MESSAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "MB_S_OUT_MESSAGE"
	 */
	public Integer getOutMessageID() {
		return outMessageID;
	}

	public void setOutMessageID(Integer outMessageID) {
		this.outMessageID = outMessageID;
	}

	/**
	 * returns the messageType
	 * 
	 * @return Returns the messageType.
	 * 
	 * @hibernate.property column = "REQUEST_TYPE_CODE"
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * returns the processedDate
	 * 
	 * @return Returns the processedDate.
	 * 
	 * @hibernate.property column = "PROCESSED_DATE"
	 */
	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	/**
	 * returns the receivedDate
	 * 
	 * @return Returns the receivedDate.
	 * 
	 * @hibernate.property column = "RECEIVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * returns the recordLocator
	 * 
	 * @return Returns the recordLocator.
	 * 
	 * @hibernate.property column = "RECORD_LOCATOR"
	 */
	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	/**
	 * returns the sentTTYAddress
	 * 
	 * @return Returns the sentTTYAddress.
	 * 
	 * @hibernate.property column = "SENDER_TTY_ADDRESS"
	 */
	public String getSentTTYAddress() {
		return sentTTYAddress;
	}

	public void setSentTTYAddress(String sentTTYAddress) {
		this.sentTTYAddress = sentTTYAddress;
	}

	/**
	 * returns the recipientTTYAddress
	 * 
	 * @return Returns the recipientTTYAddress.
	 * 
	 * @hibernate.property column = "RECIPIENT_TTY_ADDRESS"
	 */
	public String getRecipientTTYAddress() {
		return recipientTTYAddress;
	}

	public void setRecipientTTYAddress(String recipientTTYAddress) {
		this.recipientTTYAddress = recipientTTYAddress;
	}

	/**
	 * returns the sentEmailAddress
	 * 
	 * @return Returns the sentEmailAddress.
	 * 
	 * @hibernate.property column = "SENDER_EMAIL_ADDRESS"
	 */
	public String getSentEmailAddress() {
		return sentEmailAddress;
	}

	public void setSentEmailAddress(String sentEmailAddress) {
		this.sentEmailAddress = sentEmailAddress;
	}

	/**
	 * returns the recipientEmailAddress
	 * 
	 * @return Returns the recipientEmailAddress.
	 * 
	 * @hibernate.property column = "RECIPIENT_EMAIL_ADDRESS"
	 */
	public String getRecipientEmailAddress() {
		return recipientEmailAddress;
	}

	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}

	/**
	 * returns the statusIndicator
	 * 
	 * @return Returns the statusIndicator.
	 * 
	 * @hibernate.property column = "PROCESS_STATUS"
	 */
	public String getStatusIndicator() {
		return statusIndicator;
	}

	public void setStatusIndicator(String statusIndicator) {
		this.statusIndicator = statusIndicator;
	}

	/**
	 * returns the modeOfCommunication
	 * 
	 * @return Returns the modeOfCommunication.
	 * 
	 * @hibernate.property column = "MODE_OF_COMMUNICATION"
	 */
	public String getModeOfCommunication() {
		return modeOfCommunication;
	}

	public void setModeOfCommunication(String modeOfCommunication) {
		this.modeOfCommunication = modeOfCommunication;
	}

	/**
	 * returns the GDSMessageID
	 * 
	 * @return Returns the GDSMessageID.
	 * 
	 * @hibernate.property column = "GDS_MESSAGE_ID"
	 */
	public String getGdsMessageID() {
		return gdsMessageID;
	}

	public void setGdsMessageID(String gdsMessageID) {
		this.gdsMessageID = gdsMessageID;
	}

	/**
	 * returns the outMessageText
	 * 
	 * @return Returns the outMessageText.
	 * 
	 * @hibernate.property column = "ROW_MESSAGE"
	 */
	public Blob getOutMessage() {
		return outMessage;
	}

	public void setOutMessage(Blob outMessage) {
		this.outMessage = outMessage;
	}


	public String getOutMessageText() {
		String value = null;

		try {
			if (outMessageText == null) {
				value = new String(getOutMessage().getBytes(1, (int)getOutMessage().length()));
				outMessageText = value;
			}
		} catch (Exception e) {
		}

		return outMessageText;
	}
	
	public void setOutMessageText(String outMessageText) {
		Blob blob = null;

		this.outMessageText = outMessageText;
		blob = DataConverterUtil.createBlob((outMessageText.getBytes()));
		setOutMessage(blob);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isProcessSuccess() {
		return processSuccess;
	}

	public void setProcessSuccess(boolean processSuccess) {
		this.processSuccess = processSuccess;
	}
	
	public String getWsServerUrl() {
		return wsServerUrl;
	}

	public void setWsServerUrl(String wsServerUrl) {
		this.wsServerUrl = wsServerUrl;
	}
	
	/**
	 * returns the nonGDSMessageFlag
	 * 
	 * @return Returns the nonGDSMessageFlag.
	 * 
	 * @hibernate.property column = "NON_GDS_MESSAGE"
	 */
	public char getNonGDSMessageFlag() {
		return nonGDSMessageFlag;
	}

	public void setNonGDSMessageFlag(Character nonGDSMessageFlag) {
		if (nonGDSMessageFlag != null) {
			this.nonGDSMessageFlag = nonGDSMessageFlag;
		} else {
			this.nonGDSMessageFlag = GDS_MESSAGE;
		}
	}

	public boolean isNonGDSMessage() {
		if (getNonGDSMessageFlag() == NON_GDS_MESSAGE) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * returns the responseStatus
	 * 
	 * @return Returns the responseStatus.
	 * 
	 * @hibernate.property column = "RESPONSE_STATUS"
	 */

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	/**
	 * returns the parsedXML
	 * 
	 * @return Returns the parsedXML.
	 * 
	 * @hibernate.property column = "PARSED_XML"
	 */
	// public String getParsedXML() {
	// return parsedXML;
	// }
	// public void setParsedXML(String parsedXML) {
	// this.parsedXML = parsedXML;
	// }
	//

}