package com.isa.thinair.msgbroker.api.dto;

/*
 * DTOPaxBabbage.java
 *
 * Created on August 9, 2006, 10:38 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Chan
 */
public class SegmentDTO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 2303937224508251193L;

	/**
	 * Creates a new instance of DTOPaxBabbage
	 */

	private String flightNumber;
	
	private String csFlightNumber;

	private Date departureDate;

	private String departureStation;

	private String destinationStation;

	private String bookingCode;
	
	private String csBookingCode;

	public SegmentDTO() {
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {

		this.departureDate = departureDate;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getCsFlightNumber() {
		return csFlightNumber;
	}

	public String getCsBookingCode() {
		return csBookingCode;
	}

	public void setCsFlightNumber(String csFlightNumber) {
		this.csFlightNumber = csFlightNumber;
	}

	public void setCsBookingCode(String csBookingCode) {
		this.csBookingCode = csBookingCode;
	}
}
