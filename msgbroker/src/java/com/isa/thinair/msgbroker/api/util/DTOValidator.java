package com.isa.thinair.msgbroker.api.util;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.AVSMessageDTO;
import com.isa.thinair.msgbroker.api.dto.AVSSegmentDTO;
import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSSegmentEventCode;

public class DTOValidator {

	public static boolean validateAVSMessageDTO(AVSMessageDTO avsMessageDTO) throws ModuleException{
		/*
		 * Only 10 segments could be presented in AVSSegmentDTOs colloection.
		 */
		if (!avsMessageDTO.getAVSSegmentDTOs().isEmpty() && avsMessageDTO.getAVSSegmentDTOs().size() > 10) {
			throw new ModuleException("gdsmessagecomposer.avsmessage.exceeded.segmentslimit");
		}else {
            List<AVSSegmentDTO> avsSegmentList  =avsMessageDTO.getAVSSegmentDTOs();
          //  List nAvailCodeList = new ArrayList ();
           // String nAvailCodeS = "AVS_FLIGHT_REOPEN,AVS_RBD_REOPEN,CC,CN,LR,LL,LC,LN";

            for (Iterator iter = avsSegmentList.iterator(); iter.hasNext();) {
                AVSSegmentDTO element = (AVSSegmentDTO) iter.next();
                    String dpStation = element.getDepartureStation();
                    String dsStation = element.getDestinationStation();
                    
                    AVSSegmentEventCode airLineEvent = element.getAVSSegmentEventCode();
                    if ((airLineEvent == null || airLineEvent.name().equals("")) &&
                    		(element.getAvsRbdDTOs() == null || element.getAvsRbdDTOs().size() == 0)) {
                        throw new ModuleException("gdsmessagecomposer.avsmessage.missing.airline.event");
                    } if (element.getFlightDesignator() == null || element.getFlightDesignator().isEmpty()) {
                        throw new ModuleException("gdsmessagecomposer.avsmessage.missing.flightdesignator");
                    } if (element.getDepartureDate() == null ) {
                        throw new ModuleException("gdsmessagecomposer.avsmessage.missing.departuredate");
                    }                  
//                if (dpStation == null || dsStation == null) {
//                     
//                    if (nAvailCodeS.contains(airLineEvent.name())) {
//                        throw new ModuleException("gdsmessagecomposer.avsmessage.Invalid.levelstatuscodes");
//                    }
//                    
//                }
                
            }
        }
        
        //TODO Validate length of creator reference, 
		
        
		return true;
	}
}
