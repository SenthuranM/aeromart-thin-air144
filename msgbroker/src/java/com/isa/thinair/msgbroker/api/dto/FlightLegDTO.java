package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

public class FlightLegDTO implements Serializable {
	String departureStation;
	String arrivalStation;

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

}
