package com.isa.thinair.msgbroker.api.dto;

/**
 * 
 * @author Manoj Dhanushka
 */
public class OSIEmailDTO extends OSIDTO {
	
    private static final long serialVersionUID = -6957469125257929410L;

    private String email;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
