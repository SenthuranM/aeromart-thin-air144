package com.isa.thinair.msgbroker.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_IN_MSG_PROCESSING_STATUS"
 */

public class InMessageProcessingStatus extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7215001744915512145L;
	private Integer inMessageProcessingStatusID;
	private Integer inMessageID;
	private String errorMessageText;
	private String errorMessageCode;
	private Date processingDate;
	private String processingStatus;

	/**
	 * returns the inMessageProcessingStatusID
	 * 
	 * @return Returns the inMessageProcessingStatusID.
	 * 
	 * @hibernate.id column = "IN_MSG_PROCESSING_STATUS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "MB_S_IN_MSG_PROCESSING_STATUS"
	 */
	public Integer getInMessageProcessingStatusID() {
		return inMessageProcessingStatusID;
	}

	public void setInMessageProcessingStatusID(Integer inMessagePassingErrorID) {
		this.inMessageProcessingStatusID = inMessagePassingErrorID;
	}

	/**
	 * returns the inMessageID
	 * 
	 * @return Returns the inMessageID.
	 * 
	 * @hibernate.property column = "IN_MESSAGE_ID"
	 */
	public Integer getInMessageID() {
		return inMessageID;
	}

	public void setInMessageID(Integer inMessageID) {
		this.inMessageID = inMessageID;
	}

	/**
	 * returns the ErrorMessageText
	 * 
	 * @return Returns the ErrorMessageText.
	 * 
	 * @hibernate.property column = "ERROR_MESSAGE"
	 */
	public String getErrorMessageText() {
		return errorMessageText;
	}

	public void setErrorMessageText(String errorMessageText) {
		this.errorMessageText = errorMessageText;
	}

	/**
	 * returns the ErrorMessageCode
	 * 
	 * @return Returns the ErrorMessageCode.
	 * 
	 * @hibernate.property column = "ERROR_MESSAGE_CODE"
	 */
	public String getErrorMessageCode() {
		return errorMessageCode;
	}

	public void setErrorMessageCode(String errorMessageCode) {
		this.errorMessageCode = errorMessageCode;
	}

	/**
	 * returns the ProcessingStatus
	 * 
	 * @return Returns the ProcessingStatus.
	 * 
	 * @hibernate.property column = "PROCESSING_STATUS"
	 */
	public String getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	/**
	 * returns the ProcessingDate
	 * 
	 * @return Returns the ProcessingDate.
	 * 
	 * @hibernate.property column = "PROCESSING_DATE"
	 */
	public Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

}