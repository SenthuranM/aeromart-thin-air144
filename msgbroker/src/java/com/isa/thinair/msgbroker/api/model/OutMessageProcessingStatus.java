package com.isa.thinair.msgbroker.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_OUT_MSG_PROCESSING_STATUS"
 */

public class OutMessageProcessingStatus extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7215001744915512145L;
	private Integer outMessageProcessingStatusID;
	private Integer outMessageID;
	private String errorMessageText;
	private String errorMessageCode;
	private Date processingDate;
	private String processingStatus;

	/**
	 * returns the outMessageProcessingStatusID
	 * 
	 * @return Returns the outMessageProcessingStatusID.
	 * 
	 * @hibernate.id column = "OUT_MSG_PROCESSING_STATUS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "MB_S_OUT_MSG_PROCESSING_STATUS"
	 */
	public Integer getOutMessageProcessingStatusID() {
		return outMessageProcessingStatusID;
	}

	public void setOutMessageProcessingStatusID(Integer outMessageProcessingStatusID) {
		this.outMessageProcessingStatusID = outMessageProcessingStatusID;
	}

	/**
	 * returns the outMessageID
	 * 
	 * @return Returns the outMessageID.
	 * 
	 * @hibernate.property column = "OUT_MESSAGE_ID"
	 */
	public Integer getOutMessageID() {
		return outMessageID;
	}

	public void setOutMessageID(Integer outMessageID) {
		this.outMessageID = outMessageID;
	}

	/**
	 * returns the ErrorMessageText
	 * 
	 * @return Returns the ErrorMessageText.
	 * 
	 * @hibernate.property column = "ERROR_MESSAGE"
	 */
	public String getErrorMessageText() {
		return errorMessageText;
	}

	public void setErrorMessageText(String errorMessageText) {
		this.errorMessageText = errorMessageText;
	}

	/**
	 * returns the ErrorMessageCode
	 * 
	 * @return Returns the ErrorMessageCode.
	 * 
	 * @hibernate.property column = "ERROR_MESSAGE_CODE"
	 */
	public String getErrorMessageCode() {
		return errorMessageCode;
	}

	public void setErrorMessageCode(String errorMessageCode) {
		this.errorMessageCode = errorMessageCode;
	}

	/**
	 * returns the ProcessingStatus
	 * 
	 * @return Returns the ProcessingStatus.
	 * 
	 * @hibernate.property column = "PROCESSING_STATUS"
	 */
	public String getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	/**
	 * returns the ProcessingDate
	 * 
	 * @return Returns the ProcessingDate.
	 * 
	 * @hibernate.property column = "PROCESSING_DATE"
	 */
	public Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

}