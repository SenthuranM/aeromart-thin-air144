package com.isa.thinair.msgbroker.api.dto;

/*
 * DTOPaxBabbage.java
 *
 * Created on August 9, 2006, 10:38 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
import java.io.Serializable;

/**
 * 
 * @author Chan
 */
public class NameDTO implements Serializable {

	/**
	 * Creates a new instance of DTOPaxBabbage
	 */
	private String paxTitle = null;
	private String lastName = null;
	private String firstName = null;
	private String paxType = null;
	private int groupMagnitude;
	private int familyID;
	private boolean isInfant;

	public NameDTO() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPaxTitle() {
		return paxTitle;
	}

	public void setPaxTitle(String paxTitle) {
		this.paxTitle = paxTitle;
	}

	/**
	 * @return the groupMagnitude
	 */
	public int getGroupMagnitude() {
		return groupMagnitude;
	}

	/**
	 * @param groupMagnitude
	 *            the groupMagnitude to set
	 */
	public void setGroupMagnitude(int groupMagnitude) {
		this.groupMagnitude = groupMagnitude;
	}

	public int getFamilyID() {
		return familyID;
	}

	public void setFamilyID(int familyID) {
		this.familyID = familyID;
	}

	public boolean isInfant() {
		return isInfant;
	}

	public void setInfant(boolean isInfant) {
		this.isInfant = isInfant;
	}

}
