package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

public class FlightScheduleSegmentDTO implements Serializable {
	String departureStation;
	String arrivalStation;
	String trafficRestrictionNote;
	String otherSegmentInfo;

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getOtherSegmentInfo() {
		return otherSegmentInfo;
	}

	public void setOtherSegmentInfo(String otherSegmentInfo) {
		this.otherSegmentInfo = otherSegmentInfo;
	}

	public String getTrafficRestrictionNote() {
		return trafficRestrictionNote;
	}

	public void setTrafficRestrictionNote(String trafficRestrictionNote) {
		this.trafficRestrictionNote = trafficRestrictionNote;
	}

}
