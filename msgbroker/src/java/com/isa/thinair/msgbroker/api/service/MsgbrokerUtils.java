package com.isa.thinair.msgbroker.api.service;


import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.wsclient.api.service.DCSClientBD;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

import javax.security.auth.login.LoginException;


public class MsgbrokerUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("msgbroker");
	}

	public static MsgBrokerModuleConfig getMsgbrokerConfigs() {
		return (MsgBrokerModuleConfig) getInstance().getModuleConfig();
	}	

	/**
	 * Return transparent airport class delegate
	 * 
	 * @return
	 */
	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public static LocationBD getLocationBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight business delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static TypeBServiceBD getTypeBServiceBD() {
		return (TypeBServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeBServiceBD.SERVICE_NAME);
	}

	/**
	 * @return DCSClientBD
	 */
	public static DCSClientBD getDCSClientBD() {
		return (DCSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, DCSClientBD.SERVICE_NAME);
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getMsgbrokerConfigs(), "msgbroker",
				"msgbroker.config.dependencymap.invalid");
	}
	
	/**
	 * @return : The GDS Services Business Delegate.
	 */
	public static GDSServicesBD getGDSServicesBD() throws ModuleException {
		if (getMsgbrokerConfigs().isInvokeCredentials()) {
			try {
				ForceLoginInvoker.webserviceLogin(getMsgbrokerConfigs().getEjbAccessUsername(), getMsgbrokerConfigs().getEjbAccessPassword(),
						SalesChannelsUtil.SALES_CHANNEL_GDS);
			} catch (LoginException e) {
				throw new ModuleException(e, "module.invalid.user");
			}
		}
		return (GDSServicesBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSServicesBD.SERVICE_NAME);
	}
	
	public static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		if (getMsgbrokerConfigs().isInvokeCredentials()) {
			ForceLoginInvoker.login(getMsgbrokerConfigs().getEjbAccessUsername(), getMsgbrokerConfigs().getEjbAccessPassword());
		}
		return (MessageBrokerServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, MessageBrokerServiceBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
	
	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	public static ETLBD getETLBD() {
		return (ETLBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
	}
	
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}
}
