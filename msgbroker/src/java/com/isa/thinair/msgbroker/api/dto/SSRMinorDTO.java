package com.isa.thinair.msgbroker.api.dto;

public class SSRMinorDTO extends SSRDTO{
    
    /**
     * 
     */
    private static final long serialVersionUID = -5095921224639526160L;

    private String firstName;

    private String lastName;

    private String title;
    
    /** Age in years.*/
    private int minorAge;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the minorAge
     */
    public int getMinorAge() {
        return minorAge;
    }

    /**
     * @param minorAge the minorAge to set
     */
    public void setMinorAge(int minorAge) {
        this.minorAge = minorAge;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

}
