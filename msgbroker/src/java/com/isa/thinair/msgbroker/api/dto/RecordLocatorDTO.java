/*
 * RecordLocatorDTO.java
 *
 * Created on October 14, 2007, 12:28 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class RecordLocatorDTO implements Serializable {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 5332847513233518703L;

    private String bookingOffice = null;
    
    private String pnrReference = null;
    
    private String taOfficeCode = null;
    
    private String userID = null;
    
    private String closestCityAirportCode = null;
    
    private String cariierCRSCode= null;

    private String userType= null;
    
    private String countryCode= null;
    
    private String currencyCode= null;
    
    private String agentDutyCode= null;
    
    private String erspId= null;
    
    private String firstDeparturefrom= null;
    
    private String pointOfSales = null;
    
    /** Creates a new instance of RecordLocatorDTO */
    public RecordLocatorDTO() {
    }
    
    
    public String getBookingOffice(){
        return this.bookingOffice;
    }
    
    public void setBookingOffice(String bookingOffice){
        this.bookingOffice = bookingOffice;
    }
        
    public String getPnrReference(){
        return this.pnrReference;
    }
    
    public void setPnrReference(String pnrReference){
        this.pnrReference = pnrReference;
    }
        
    public String getTaOfficeCode(){
        return this.taOfficeCode;
    }
    
    public void setTaOfficeCode(String taOfficeCode){
        this.taOfficeCode = taOfficeCode;
    }
    public String getUserID(){
        return this.userID;
    }
    
    public void setUserID(String userID){
        this.userID = userID;
    }


	public String getClosestCityAirportCode() {
		return closestCityAirportCode;
	}


	public void setClosestCityAirportCode(String pseudoCityCode) {
		this.closestCityAirportCode = pseudoCityCode;
	}


    /**
     * @return the agentCode
     */
    public String getAgentDutyCode() {
        return agentDutyCode;
    }


    /**
     * @param agentCode the agentCode to set
     */
    public void setAgentDutyCode(String agentCode) {
        this.agentDutyCode = agentCode;
    }


    /**
     * @return the cariierCode
     */
    public String getCariierCRSCode() {
        return cariierCRSCode;
    }


    /**
     * @param cariierCode the cariierCode to set
     */
    public void setCariierCRSCode(String cariierCode) {
        this.cariierCRSCode = cariierCode;
    }


    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }


    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * @return the erspId
     */
    public String getErspId() {
        return erspId;
    }


    /**
     * @param erspId the erspId to set
     */
    public void setErspId(String erspId) {
        this.erspId = erspId;
    }


    /**
     * @return the firstDeparturefrom
     */
    public String getFirstDeparturefrom() {
        return firstDeparturefrom;
    }


    /**
     * @param firstDeparturefrom the firstDeparturefrom to set
     */
    public void setFirstDeparturefrom(String firstDeparturefrom) {
        this.firstDeparturefrom = firstDeparturefrom;
    }


    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }


    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }


	public String getPointOfSales() {
		return pointOfSales;
	}


	public void setPointOfSales(String pointOfSales) {
		this.pointOfSales = pointOfSales;
	}
    
    
                               
}
