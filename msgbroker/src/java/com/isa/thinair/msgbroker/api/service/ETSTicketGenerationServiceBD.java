package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.commons.api.dto.ets.ETicketGenerationReqDTO;
import com.isa.thinair.commons.api.dto.ets.ETicketGenerationResDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Manoj Dhanushka
 */
public interface ETSTicketGenerationServiceBD {
	
	public static final String SERVICE_NAME = "TicketGenerationService";
	
	public ETicketGenerationResDTO generateEtickets(ETicketGenerationReqDTO eTicketGenerationReq) throws ModuleException;
}
