package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

public class PNLADLResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<String> pnrs;
	private ArrayList<String> fileNames;
	private String lastGroupId;
	private HashMap<String, String> pnrPaxSegIDGroupCode;
	private HashMap<String, Integer> ccPaxMap;
	private boolean wsAdlSuccess;
	private Collection<PassengerInformation> addedPassengerList;

	public Set<String> getPnrs() {
		return pnrs;
	}

	public void setPnrs(Set<String> pnrs) {
		this.pnrs = pnrs;
	}

	public ArrayList<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(ArrayList<String> fileNames) {
		this.fileNames = fileNames;
	}

	public String getLastGroupId() {
		return lastGroupId;
	}

	public void setLastGroupId(String lastGroupId) {
		this.lastGroupId = lastGroupId;
	}

	public HashMap<String, String> getPnrPaxSegIDGroupCode() {
		return pnrPaxSegIDGroupCode;
	}

	public void setPnrPaxSegIDGroupCode(HashMap<String, String> pnrPaxSegIDGroupCode) {
		this.pnrPaxSegIDGroupCode = pnrPaxSegIDGroupCode;
	}

	public HashMap<String, Integer> getCcPaxMap() {
		return ccPaxMap;
	}

	public void setCcPaxMap(HashMap<String, Integer> ccPaxMap) {
		this.ccPaxMap = ccPaxMap;
	}

	public boolean isWsAdlSuccess() {
		return wsAdlSuccess;
	}

	public void setWsAdlSuccess(boolean wsAdlSuccess) {
		this.wsAdlSuccess = wsAdlSuccess;
	}

	public Collection<PassengerInformation> getAddedPassengerList() {
		return addedPassengerList;
	}

	public void setAddedPassengerList(Collection<PassengerInformation> addedPassengerList) {
		this.addedPassengerList = addedPassengerList;
	}

}
