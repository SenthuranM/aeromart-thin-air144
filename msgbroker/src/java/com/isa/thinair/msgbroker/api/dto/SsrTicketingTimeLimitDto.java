package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;

public class SsrTicketingTimeLimitDto extends SSRDTO {
	private Date ticketingTime;

	public Date getTicketingTime() {
		return ticketingTime;
	}

	public void setTicketingTime(Date ticketingTime) {
		this.ticketingTime = ticketingTime;
	}
}
