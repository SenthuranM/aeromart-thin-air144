package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Transfer object used to transfer out message processing details
 */

public class OutMessageProcessingStatusTO implements Serializable {

	private static final long serialVersionUID = 7215001744915512145L;

	private Long outMessageProcessingStatusID;
	private Integer outMessageID;
	private String errorMessageText;
	private String errorMessageCode;
	private Date processingDate;
	private String processingStatus;

	public String getErrorMessageCode() {
		return errorMessageCode;
	}

	public void setErrorMessageCode(String errorMessageCode) {
		this.errorMessageCode = errorMessageCode;
	}

	public String getErrorMessageText() {
		return errorMessageText;
	}

	public void setErrorMessageText(String errorMessageText) {
		this.errorMessageText = errorMessageText;
	}

	public Integer getOutMessageID() {
		return outMessageID;
	}

	public void setOutMessageID(Integer outMessageID) {
		this.outMessageID = outMessageID;
	}

	public Long getOutMessageProcessingStatusID() {
		return outMessageProcessingStatusID;
	}

	public void setOutMessageProcessingStatusID(Long outMessageProcessingStatusID) {
		this.outMessageProcessingStatusID = outMessageProcessingStatusID;
	}

	public Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public String getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

}