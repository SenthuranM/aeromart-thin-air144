package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSSegmentEventCode;



/**
 * 
 * @author thejaka
 *
 */
public class AVSSegmentDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String flightDesignator;
	
	private Date departureDate;

	private String departureStation;

	private String destinationStation;
	
	private Integer seatsAvailable;

	private AVSSegmentEventCode avsSegmentEventCode;
	
	Collection<AVSRbdDTO> avsRbdDTOs;

	/**
	 * Returns departure date of the segment
	 * @return
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * Sets the departure date fo the segment
	 * @param departureDate
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * Returns departure station (3 letter airport code) of the segment
	 * This is optional when Status_NumericAvailability_Code is one of the following flight level status codes
	 *  CR, CL, CC, CN, LR, LL, LC, LN
	 * @return
	 */	
	public String getDepartureStation() {
		return departureStation;
	}

	/**
	 * Sets departure station (3 letter airport code) of the segment
	 * This is optional when Status_NumericAvailability_Code is one of the following flight level status codes
	 *  CR, CL, CC, CN, LR, LL, LC, LN
	 * @param departureStation
	 */
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	/**
	 * Returns destination station (3 letter airport code) of the segment
	 * This is optional when Status_NumericAvailability_Code is one of the following flight level status codes
	 *  CR, CL, CC, CN, LR, LL, LC, LN
	 * @return
	 */
	public String getDestinationStation() {
		return destinationStation;
	}

	/**
	 * Sets destination station (3 letter airport code) of the segment
	 * This is optional when Status_NumericAvailability_Code is one of the following flight level status codes
	 *  CR, CL, CC, CN, LR, LL, LC, LN
	 * @param destinationStation
	 */
	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	/**
	 * Returns flight designator (flight number with airline code)
	 * @return
	 */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	/**
	 * Sets flight designator (flight number with airline code)
	 * Format: XX(a)nnn(n)
	 * Example: LX544
	 * @param flightDesignator
	 */
	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

    /**
     * @return the event
     */
    public AVSSegmentEventCode getAVSSegmentEventCode() {
        return avsSegmentEventCode;
    }

    /**
     * @param event the event to set
     */
    public void setAVSSegmentEventCode(AVSSegmentEventCode event) {
        this.avsSegmentEventCode = event;
    }

	/**
	 * @return the avsRbdDTOs
	 */
	public Collection<AVSRbdDTO> getAvsRbdDTOs() {
		return avsRbdDTOs;
	}

	/**
	 * @param avsRbdDTOs the avsRbdDTOs to set
	 */
	public void setAvsRbdDTOs(Collection<AVSRbdDTO> avsRbdDTOs) {
		this.avsRbdDTOs = avsRbdDTOs;
	}

	/**
	 * @return the seatsAvailable
	 */
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * @param seatsAvailable the seatsAvailable to set
	 */
	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}	
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(flightDesignator).append(departureDate).append(departureStation).append(destinationStation);
		return stringBuilder.toString();
	}
}
