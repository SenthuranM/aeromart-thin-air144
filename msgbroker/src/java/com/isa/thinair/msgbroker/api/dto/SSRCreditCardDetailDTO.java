/**
 * 
 */
package com.isa.thinair.msgbroker.api.dto;

/**
 * @author sanjeewaf
 */
public class SSRCreditCardDetailDTO extends SSRDTO {

	/**
	 * SSR OTHS YY CC AX371010374072700EXP 11 11 SSR OTHS YY CH NHNHN CC (([A-Z]{2})([0-9]+)EXP) (([0-9]{2}) ([0-9]{2}))
	 */
	/**
	 * CCD VENDORS
	 * 
	 * AA AMERICAN AIR CR CARD AB AUSTRALIAN BANKCARD AQ ALOHA AIR CR CARD AS ALASKA AIR CR CARD AX AMERICAN EXPRESS BA
	 * VISA BB BARCLAYS BANK/VISA BC BANKCARD BE BE IHG BUSINESS ACCOUNT BF C B B FRANCAIS/VISA BH BANK OF HAWAII BI
	 * BRANIFF FASTCHARGE BJ BANK CARD JAPAN BP BANK CARD PACIFIC BZ NEW ZEALAND BANKCARD
	 */

	private String cardType;

	private String cardNo;

	private String cardHolderName;

	private String expiryMonth;

	private String expiryYear;

	private String pinNumber;

	/**
	 * @return the cardNo
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * @param cardNo
	 *            the cardNo to set
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the expiryMonth
	 */
	public String getExpiryMonth() {
		return expiryMonth;
	}

	/**
	 * @param expiryMonth
	 *            the expiryMonth to set
	 */
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	/**
	 * @return the expiryYear
	 */
	public String getExpiryYear() {
		return expiryYear;
	}

	/**
	 * @param expiryYear
	 *            the expiryYear to set
	 */
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	/**
	 * @return the pinNumber
	 */
	public String getPinNumber() {
		return pinNumber;
	}

	/**
	 * @param pinNumber
	 *            the pinNumber to set
	 */
	public void setPinNumber(String pinNumber) {
		this.pinNumber = pinNumber;
	}

	/**
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName
	 *            the cardHolderName to set
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

}
