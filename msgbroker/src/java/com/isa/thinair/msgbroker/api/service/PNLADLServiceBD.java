package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.PNLADLResponseDTO;

public interface PNLADLServiceBD {

	public static final String SERVICE_NAME = "PNLADLService";

	public PNLADLResponseDTO sendADL(ADLDTO adlElements) throws ModuleException;
	public boolean sendADLToDcs(ADLDTO adlElements) throws ModuleException;

}
