package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO used to transfer Out message data
 */

public class OutMessageTO implements Serializable {

	private static final long serialVersionUID = 4740787069649970157L;

	private Long outMessageID;
	private String airLineCode;
	private String messageType;
	private String sentTTYAddress;
	private String recipientTTYAddress;
	private Integer recordLocator;
	private String outMessageText;
	private Date receivedDate;
	private String statusIndicator;
	private Date processedDate;
	private String lastErrorMessageCode;
	private Integer refInMessageID;
	private String sentEmailAddress;
	private String recipientEmailAddress;
	private String modeOfCommunication;

	public String getAirLineCode() {
		return airLineCode;
	}

	public void setAirLineCode(String airLineCode) {
		this.airLineCode = airLineCode;
	}

	public String getLastErrorMessageCode() {
		return lastErrorMessageCode;
	}

	public void setLastErrorMessageCode(String lastErrorMessageCode) {
		this.lastErrorMessageCode = lastErrorMessageCode;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getModeOfCommunication() {
		return modeOfCommunication;
	}

	public void setModeOfCommunication(String modeOfCommunication) {
		this.modeOfCommunication = modeOfCommunication;
	}

	public Long getOutMessageID() {
		return outMessageID;
	}

	public void setOutMessageID(Long outMessageID) {
		this.outMessageID = outMessageID;
	}

	public String getOutMessageText() {
		return outMessageText;
	}

	public void setOutMessageText(String outMessageText) {
		this.outMessageText = outMessageText;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getRecipientEmailAddress() {
		return recipientEmailAddress;
	}

	public void setRecipientEmailAddress(String recipientEmailAddress) {
		this.recipientEmailAddress = recipientEmailAddress;
	}

	public String getRecipientTTYAddress() {
		return recipientTTYAddress;
	}

	public void setRecipientTTYAddress(String recipientTTYAddress) {
		this.recipientTTYAddress = recipientTTYAddress;
	}

	public Integer getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(Integer recordLocator) {
		this.recordLocator = recordLocator;
	}

	public Integer getRefInMessageID() {
		return refInMessageID;
	}

	public void setRefInMessageID(Integer refInMessageID) {
		this.refInMessageID = refInMessageID;
	}

	public String getSentEmailAddress() {
		return sentEmailAddress;
	}

	public void setSentEmailAddress(String sentEmailAddress) {
		this.sentEmailAddress = sentEmailAddress;
	}

	public String getSentTTYAddress() {
		return sentTTYAddress;
	}

	public void setSentTTYAddress(String sentTTYAddress) {
		this.sentTTYAddress = sentTTYAddress;
	}

	public String getStatusIndicator() {
		return statusIndicator;
	}

	public void setStatusIndicator(String statusIndicator) {
		this.statusIndicator = statusIndicator;
	}

}