package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.List;

public class AVSMessageDTO implements Serializable {
	
	private static final long serialVersionUID = 7306190649955322553L;
	
//	private String messageIdentifier = null;
    private String senderAddress = null;
    private String recipientAddress = null;
    private String timestamp = null;

	private List<AVSSegmentDTO> AVSSegmentDTOs = null;

	/**
	 * Gets AVSSegmentDTOs colloection.
	 * @return
	 */
	public List<AVSSegmentDTO> getAVSSegmentDTOs() {
		return AVSSegmentDTOs;
	}
	

	/**
	 * Sets AVSSegmentDTOs colloection.
	 * Only 10 segments could be presented in the list.
	 * @param segmentDTOs
	 */
	public void setAVSSegmentDTOs(List<AVSSegmentDTO> avsSegmentDTOs) {
		AVSSegmentDTOs = avsSegmentDTOs;
	}


	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}


	/**
	 * @param senderAddress the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}


	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}


	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}


	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}


	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}



//	/**
//	 * Appends AVSSegmentDto into the AVSSegmentDTOs colloection.
//	 * @param avsSegmentDTO
//	 * @return
//	 */
//	public void addAVSSegmentDTO(AVSSegmentDTO avsSegmentDTO)  {
//		if (AVSSegmentDTOs==null){
//			AVSSegmentDTOs = new ArrayList<AVSSegmentDTO>();
//		}				
//		AVSSegmentDTOs.add(avsSegmentDTO);
//	}
//	
//		
//	/**
//	 * Removes AVSSegmentDto placed at listIndex's position in AVSSegmentDTOs colloection.
//	 * @param listIndex
//	 * @return
//	 */
//	public void removeAVSSegmentDTO(int listIndex)  {
//		if (AVSSegmentDTOs!=null && !AVSSegmentDTOs.isEmpty()){
//			AVSSegmentDTOs.remove(listIndex);
//		}
//	}
    
	 
}
