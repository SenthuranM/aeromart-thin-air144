package com.isa.thinair.msgbroker.api.dto;

public class SSROthersDTO extends SSRDTO {
	
    private static final long serialVersionUID = -7412147173689650386L;
    // SSR OTHS YY OSAG1245365D
    private String userNote;
	/**
	 * @return the userNote
	 */
	public String getUserNote() {
		return userNote;
	}
	/**
	 * @param userNote the userNote to set
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

}
