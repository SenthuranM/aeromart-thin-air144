package com.isa.thinair.msgbroker.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "MB_T_SERVICE_CLIENT"
 */
public class ServiceClient extends Persistent {

	private String serviceClientCode;

	private String serviceClientName;

	private String ttyAddress;

	private String emailAddress;

	private boolean recordStatus;
	
	private boolean addAddressToMessageBody;

	private String typeAAddress;
	
	private String emailDomain;
	
	private String apiUrl;
	
	private String typeASubAddress;

	/**
	 * @return Returns the serviceClientCode.
	 * 
	 * @hibernate.id column = "SERVICE_CLIENT_CODE" generator-class = "assigned"
	 */
	public String getServiceClientCode() {
		return serviceClientCode;
	}

	/**
	 * 
	 * @param serviceClientCode
	 *            To set the ServiceClient Code
	 */
	public void setServiceClientCode(String serviceClientCode) {
		this.serviceClientCode = serviceClientCode;
	}

	/**
	 * 
	 * @return the ServiceClient Name
	 * 
	 * @hibernate.property column = "SERVICE_CLIENT_NAME"
	 */
	public String getServiceClientName() {
		return serviceClientName;
	}

	/**
	 * 
	 * @param gdsName
	 *            to Set the ServiceClient name
	 */
	public void setServiceClientName(String serviceClientName) {
		this.serviceClientName = serviceClientName;
	}

	/**
	 * 
	 * @return the TTYAddress
	 * 
	 * @hibernate.property column = "TTY_ADDRESS"
	 */
	@Deprecated
	//since we can use mb_t_service_client_req_tty to get the TTYAddress, this method is no longer needed
	public String getTTYAddress() {
		return ttyAddress;
	}

	/**
	 * 
	 * @param gdsName
	 *            to Set the ServiceClient name
	 */
	@Deprecated
	public void setTTYAddress(String ttyAddress) {
		this.ttyAddress = ttyAddress;
	}

	/**
	 * 
	 * @return the TTYAddress
	 * 
	 * @hibernate.property column = "EMAIL_ADDRESS"
	 */
	@Deprecated
	//since we can use mb_t_service_client_req_tty to get the TTYAddress, this method is no longer needed
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * 
	 * @param gdsName
	 *            to Set the ServiceClient name
	 */
	@Deprecated
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * 
	 * @return the Status
	 * 
	 * @hibernate.property column = "RECORD_STATUS" type="yes_no"
	 */
	public boolean getRecordStatus() {
		return recordStatus;
	}

	/**
	 * 
	 * @param recordStatus
	 *            to set the ServiceClient Status
	 */
	public void setRecordStatus(boolean recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	/**
	 * @return the addAddressToMessageBody
	 * 
	 * @hibernate.property column = "ADD_ADDRESS_TO_MSG_BODY" type="yes_no"
	 */
	public boolean getAddAddressToMessageBody() {
		return addAddressToMessageBody;
	}

	/**
	 * @param addAddressToMessageBody the addAddressToMessageBody to set
	 */
	public void setAddAddressToMessageBody(boolean addAddressToMessageBody) {
		this.addAddressToMessageBody = addAddressToMessageBody;
	}

	/**
	 * @hibernate.property column = "TYPE_A_ADDRESS"
	 */
	public String getTypeAAddress() {
		return typeAAddress;
	}

	public void setTypeAAddress(String typeAAddress) {
		this.typeAAddress = typeAAddress;
	}

	/**
	 * 
	 * @hibernate.property column = "EMAIL_DOMAIN"
	 */
	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}

	/**
	 * @hibernate.property column = "API_URL"
	 */
	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	/**
	 * Convert the ServiceClient object contents to an string. This follows the following format. Entity = entity_name;
	 * field_name_1 = field_1_value; field_name_2 = field_2_value;
	 */
	public String toString() {
		return super.toString() + " GDSCode = " + (this.getServiceClientCode() == null ? "" : this.getServiceClientCode())
				+ "; GDSName = " + (this.getServiceClientName() == null ? "" : this.getServiceClientName()) + "; RecordStatus = "
				+ (this.getRecordStatus() ? "" : "");
	}

	/**
	 * @return the typeASubAddress
	 * @hibernate.property column = "TYPE_A_SUB_ADDRESS" 
	 */
	public String getTypeASubAddress() {
		return typeASubAddress;
	}
	

	public void setTypeASubAddress(String typeASubAddress) {
		this.typeASubAddress = typeASubAddress;
	}
	
}