package com.isa.thinair.msgbroker.api.dto;

import java.util.List;

/*
 * OtherServiceInfoDTO.java
 * Created on October 11, 2007, 4:01 AM
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 * @author Administrator
 */
public class OtherServiceInfoDTO extends OSIDTO {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of OtherServiceInfoDTO
     */
    public OtherServiceInfoDTO() {
    }

    private String serviceInfo;

    
    private List osiNameDTOs = null;

    public String getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(String serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * @return the osiNameDTOs
     */
    public List getOsiNameDTOs() {
        return osiNameDTOs;
    }

    /**
     * @param osiNameDTOs the osiNameDTOs to set
     */
    public void setOsiNameDTOs(List osiNameDTOs) {
        this.osiNameDTOs = osiNameDTOs;
    }


}
