package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.List;

public class ReservationOperationsDTO implements Serializable {
	String MainReservationOperation;
//	List<String> SubReservationOperations;
	List SubReservationOperations;
	public String getMainReservationOperation() {
		return MainReservationOperation;
	}
	public void setMainReservationOperation(String mainReservationOperation) {
		MainReservationOperation = mainReservationOperation;
	}
	public List getSubReservationOperations() {
		return SubReservationOperations;
	}
	public void setSubReservationOperations(List subReservationOperations) {
		SubReservationOperations = subReservationOperations;
	}
	
		
}
