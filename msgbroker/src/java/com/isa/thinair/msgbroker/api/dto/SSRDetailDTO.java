package com.isa.thinair.msgbroker.api.dto;

import java.util.List;

/*
 * SpecialServiceRequestDTO.java
 *
 * Created on October 11, 2007, 4:01 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 * 
 * @author Administrator
 */
public class SSRDetailDTO extends SSRDTO {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	private int noOfPax;

	private SegmentDTO segmentDTO = null;

	private List<NameDTO> ssrNameDTOs = null;

	/**
	 * Creates a new instance of SpecialServiceRequestDTO
	 */
	public SSRDetailDTO() {
	}

	public int getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public SegmentDTO getSegmentDTO() {
		return segmentDTO;
	}

	public void setSegmentDTO(SegmentDTO segmentDTO) {
		this.segmentDTO = segmentDTO;
	}

	/**
	 * @return the ssrNameDTOs
	 */
	public List<NameDTO> getSsrNameDTOs() {
		return ssrNameDTOs;
	}

	/**
	 * @param ssrNameDTOs the ssrNameDTOs to set
	 */
	public void setSsrNameDTOs(List<NameDTO> ssrNameDTOs) {
		this.ssrNameDTOs = ssrNameDTOs;
	}
}
