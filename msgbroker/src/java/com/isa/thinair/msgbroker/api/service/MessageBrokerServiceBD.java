package com.isa.thinair.msgbroker.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageSearchDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;


public interface MessageBrokerServiceBD {
	public static final String SERVICE_NAME = "MessageBrokerService";

	public void sendMessages() throws ModuleException;
	
	public void sendAVSMessages() throws ModuleException;

	public void sendPNLADLViaSitaTex() throws ModuleException;

	public void receiveMessage(boolean isUsedGDSConfiguration) throws ModuleException;

	public InMessage saveInMessage(InMessage inMessage) throws ModuleException;

	public void updateInMessage(InMessage inMessage) throws ModuleException;

	public boolean aquireInMessageLock(Integer inMessageID) throws ModuleException;

	public void saveOutMessage(OutMessage outMessage) throws ModuleException;

	public void saveInMessageProcessingStatus(InMessageProcessingStatus inMessageProcessingStatus);

	public void sendETLMessages() throws ModuleException;
	
	public void sendPFSMessages() throws ModuleException;

	public Collection<TypeBMessageDTO> searchTypeBMessageByPnr(TypeBMessageSearchDTO typeBMessageSearchDTO,
			List<String> messageTypes) throws ModuleException;

	public void saveTypeBRecipientExtSystem(TypeBRecipientExtSystem typeBRecipientExtSystem) throws ModuleException;
	
	public void sendPALCALViaSitaTex() throws ModuleException;

	public ServiceClient getServiceClient(String gdsCode);
	public SSMASMResponse updateOutMessagesResponseStatus(SSMASMMessegeDTO ssmAsmMessageDTO) throws ModuleException;

	public void sendACKNACMessages() throws ModuleException;

	public void clearInMsgProcessingQueue() throws ModuleException;
	
	public void postMessageMdbInvoker(Integer nextInMsgToProcess);
	
	public Collection<Integer> allLockedMsgQueues();
	
	public Integer getNextMessageIdFromWaitingQueue(Integer inMsgProcessingQueueId);
	
	public void updateNextInMsgId(Integer inMsgProcessingQueueId, Integer nextMsgIdToProcess);
	
	public void removeMsgFromQueue(Integer inMessageId,Integer inMsgProcessingQueueId);
	
	public void releaseLockedScheduleFlight(String msgStatus, Integer inMsgProcessingQueueId);
	
	public void processMessagesSequentially() throws ModuleException;
	
}
