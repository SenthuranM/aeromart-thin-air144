package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;

public class BookingSegmentDTO extends SegmentDTO {
    /**
     * 
     */
    private static final long serialVersionUID = 8725585831020155409L;
    private Date departureTime;
    private Date arrivalTime;
    private String dayOffSetSign;
    private String actionOrStatusCode;
    private int noofPax = 0;
    private String adviceOrStatusCode;
    private int dayOffSet;
    private String carrierCode;
    private String csCarrierCode;
    private String membersBlockIdentifier;
    private boolean changeInScheduleExist;
	private boolean primeFlight;


	/**
     * @return the membersBlockIdentifier
     */
    public String getMembersBlockIdentifier() {
        return membersBlockIdentifier;
    }
    /**
     * @param membersBlockIdentifier the membersBlockIdentifier to set
     */
    public void setMembersBlockIdentifier(String membersBlockIdentifier) {
        this.membersBlockIdentifier = membersBlockIdentifier;
    }
    /**
     * @return the carrierCode
     */
    public String getCarrierCode() {
        return carrierCode;
    }
    /**
     * @param carrierCode the carrierCode to set
     */
    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }
    /**
     * @return the dayChangeFlag
     */
    public int getDayOffSet() {
        return dayOffSet;
    }
    /**
     * @param dayChangeFlag the dayChangeFlag to set
     */
    public void setDayOffSet(int dayChangeFlag) {
        this.dayOffSet = dayChangeFlag;
    }
    public String getActionOrStatusCode() {
		return actionOrStatusCode;
	}
	public void setActionOrStatusCode(String action_status_Code) {
		this.actionOrStatusCode = action_status_Code;
	}
	public String getAdviceOrStatusCode() {
		return adviceOrStatusCode;
	}
	public void setAdviceOrStatusCode(String advice_status_Code) {
		this.adviceOrStatusCode = advice_status_Code;
	}
	/**
     * @return the arrivalTime
     */
    public Date getArrivalTime() {
        return arrivalTime;
    }
    /**
     * @param arrivalTime the arrivalTime to set
     */
    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
    /**
     * @return the departureTime
     */
    public Date getDepartureTime() {
        return departureTime;
    }
    /**
     * @param departureTime the departureTime to set
     */
    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }
    public String getDayOffSetSign() {
		return dayOffSetSign;
	}
	public void setDayOffSetSign(String dayOffSetSign) {
		this.dayOffSetSign = dayOffSetSign;
	}
	public int getNoofPax() {
		return noofPax;
	}
	public void setNoofPax(int noofPax) {
		this.noofPax = noofPax;
	}
	public boolean isChangeInScheduleExist() {
		return changeInScheduleExist;
	}
	public void setChangeInScheduleExist(boolean changeInScheduleExist) {
		this.changeInScheduleExist = changeInScheduleExist;
	}
	public String getCsCarrierCode() {
		return csCarrierCode;
	}
	public void setCsCarrierCode(String csCarrierCode) {
		this.csCarrierCode = csCarrierCode;
	}

	public boolean isPrimeFlight() {
		return primeFlight;
	}

	public void setPrimeFlight(boolean primeFlight) {
		this.primeFlight = primeFlight;
	}
}
