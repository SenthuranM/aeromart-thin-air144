package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

public class ASMSSMLegInfoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int legId;
	private String departureStation;
	private String scheduledTimeDeparture;
	private String dateVariationSTD;
	private String arrivalStation;
	private String scheduledTimeArrival;
	private String dateVariationSTA; 
	private String passengerSTA; 
	private String passengerSTD;
	private String mealServiceNote;
	
	public int getLegId() {
		return legId;
	}
	public void setLegId(int legId) {
		this.legId = legId;
	}
	public String getDepartureStation() {
		return departureStation;
	}
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}
	public String getScheduledTimeDeparture() {
		return scheduledTimeDeparture;
	}
	public void setScheduledTimeDeparture(String scheduledTimeDeparture) {
		this.scheduledTimeDeparture = scheduledTimeDeparture;
	}
	public String getDateVariationSTD() {
		return dateVariationSTD;
	}
	public void setDateVariationSTD(String dateVariationSTD) {
		this.dateVariationSTD = dateVariationSTD;
	}
	public String getArrivalStation() {
		return arrivalStation;
	}
	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}
	public String getScheduledTimeArrival() {
		return scheduledTimeArrival;
	}
	public void setScheduledTimeArrival(String scheduledTimeArrival) {
		this.scheduledTimeArrival = scheduledTimeArrival;
	}
	public String getDateVariationSTA() {
		return dateVariationSTA;
	}
	public void setDateVariationSTA(String dateVariationSTA) {
		this.dateVariationSTA = dateVariationSTA;
	}
	public String getPassengerSTA() {
		return passengerSTA;
	}
	public void setPassengerSTA(String passengerSTA) {
		this.passengerSTA = passengerSTA;
	}
	public String getMealServiceNote() {
		return mealServiceNote;
	}
	public void setMealServiceNote(String mealServiceNote) {
		this.mealServiceNote = mealServiceNote;
	}
	public String getPassengerSTD() {
		return passengerSTD;
	}
	public void setPassengerSTD(String passengerSTD) {
		this.passengerSTD = passengerSTD;
	}

}
