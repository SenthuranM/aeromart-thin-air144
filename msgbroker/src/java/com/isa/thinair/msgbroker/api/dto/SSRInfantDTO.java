package com.isa.thinair.msgbroker.api.dto;

import java.util.Date;


public class SSRInfantDTO extends SSRDTO {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	private String infantLastName;

	private String infantFirstName;

	private String infantTitle;

	private String guardianFirstName;

	private String guardianLastName;

	private String guardianTitle;

	private SegmentDTO segmentDTO;

	private boolean seatRequired;

	private int infantAge;
	
	private Date infantDateofBirth;

	public String getGuardianFirstName() {
		return guardianFirstName;
	}

	public void setGuardianFirstName(String guardianFirstName) {
		this.guardianFirstName = guardianFirstName;
	}

	public String getGuardianLastName() {
		return guardianLastName;
	}

	public void setGuardianLastName(String guardianLastName) {
		this.guardianLastName = guardianLastName;
	}

	public String getInfantFirstName() {
		return infantFirstName;
	}

	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	public String getInfantLastName() {
		return infantLastName;
	}

	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return the segmentDTO
	 */
	public SegmentDTO getSegmentDTO() {
		return segmentDTO;
	}

	/**
	 * @param segmentDTO
	 *            the segmentDTO to set
	 */
	public void setSegmentDTO(SegmentDTO segmentDTO) {
		this.segmentDTO = segmentDTO;
	}

	/**
	 * @return the seatRequired
	 */
	public boolean isSeatRequired() {
		return seatRequired;
	}

	/**
	 * @param seatRequired
	 *            the seatRequired to set
	 */
	public void setSeatRequired(boolean seatRequired) {
		this.seatRequired = seatRequired;
	}

	/**
	 * @return the guardiantTitle
	 */
	public String getGuardianTitle() {
		return guardianTitle;
	}

	/**
	 * @param guardiantTitle
	 *            the guardiantTitle to set
	 */
	public void setGuardianTitle(String guardianTitle) {
		this.guardianTitle = guardianTitle;
	}

	/**
	 * @return the infantTitle
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            the infantTitle to set
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return the infantAge
	 */
	public int getInfantAge() {
		return infantAge;
	}

	/**
	 * @param infantAge
	 *            the infantAge to set
	 */
	public void setInfantAge(int infantAge) {
		this.infantAge = infantAge;
	}

	public Date getInfantDateofBirth() {
		return infantDateofBirth;
	}

	public void setInfantDateofBirth(Date infantDateofBirth) {
		this.infantDateofBirth = infantDateofBirth;
	}

}
