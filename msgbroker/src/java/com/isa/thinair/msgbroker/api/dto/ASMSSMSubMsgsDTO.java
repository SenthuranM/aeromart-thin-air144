package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ASMSSMSubMsgsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String actionIdentifier;
	private String asmWithdrawalIndicator;
	private String flightNumberWithSuffix;
	private String flightNumber;
	private String additionalFlightNumbers;
	private String flightDesignatorOperationalSuffix;
	private String flightIdentifierDate;
	private String flightLegChangeIdentifier;
	private String jointOperationAirline;
	private String aircraftOwner;
	private String cockpitCrewEmployer;
	private String cabinCrewEmployer;
	private String wetLeaseAirline;
	private String periodOfOperation;
	private String periodOfOperationFrom;
	private String periodOfOperationTo;
	private String daysOfOperation;
	private String frequencyRate;
	private String onwardFlight;
	private String serviceType;
	private String aircraftType;
	private String bookingDesignator;
	private String bookingModifier;
	private String aircraftConfiguration;
	private String aircraftRegistration;
	private String trafficRestrictionNote;
	private String otherSegmentInfo;
	private List<ASMSSMLegInfoDTO> legInList;
	private String oldPeriodFrom;
	private String oldPeriodTo;
	private String oldDaysOfOperation;
	private String eTicketInfo;
	private int msgOrderSequence;
	private String newFlightNumberWithSuffix;
	private String newFlightIdentifierDate;
	private String fltChangeEffectedLeg;
	private String supplementaryInfo;
	private String mealServiceNote;
	private List<String> otherSegmentInfoList;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightDesignatorOperationalSuffix() {
		return flightDesignatorOperationalSuffix;
	}

	public void setFlightDesignatorOperationalSuffix(String flightDesignatorOperationalSuffix) {
		this.flightDesignatorOperationalSuffix = flightDesignatorOperationalSuffix;
	}

	public String getActionIdentifier() {
		return actionIdentifier;
	}

	public void setActionIdentifier(String actionIdentifier) {
		this.actionIdentifier = actionIdentifier;
	}

	public String getAsmWithdrawalIndicator() {
		return asmWithdrawalIndicator;
	}

	public void setAsmWithdrawalIndicator(String asmWithdrawalIndicator) {
		this.asmWithdrawalIndicator = asmWithdrawalIndicator;
	}

	public String getFlightNumberWithSuffix() {
		return flightNumberWithSuffix;
	}

	public void setFlightNumberWithSuffix(String flightNumberWithSuffix) {
		// if(flightNumberWithSuffix!=null){
		// char charAtLast = flightNumberWithSuffix.charAt(flightNumberWithSuffix.length()-1);
		// if (Character.isLetter(charAtLast)) {
		// setFlightNumber(flightNumberWithSuffix.substring(0, flightNumberWithSuffix.length()-1));
		// setFlightDesignatorOperationalSuffix(Character.toString(charAtLast));
		// }else{
		// setFlightNumber(flightNumberWithSuffix);
		// setFlightDesignatorOperationalSuffix("");
		// }
		//
		// }
		setFlightNumber(flightNumberWithSuffix);
		setFlightDesignatorOperationalSuffix("");
		this.flightNumberWithSuffix = flightNumberWithSuffix;
	}

	public String getAdditionalFlightNumbers() {
		return additionalFlightNumbers;
	}

	public void setAdditionalFlightNumbers(String additionalFlightNumbers) {

		if (getAdditionalFlightNumbers() != null) {
			this.additionalFlightNumbers = getAdditionalFlightNumbers() + "/" + additionalFlightNumbers;
		} else {
			this.additionalFlightNumbers = additionalFlightNumbers;
		}
	}

	public String getJointOperationAirline() {
		return jointOperationAirline;
	}

	public void setJointOperationAirline(String jointOperationAirline) {
		this.jointOperationAirline = jointOperationAirline;
	}

	public String getAircraftOwner() {
		return aircraftOwner;
	}

	public void setAircraftOwner(String aircraftOwner) {
		this.aircraftOwner = aircraftOwner;
	}

	public String getCockpitCrewEmployer() {
		return cockpitCrewEmployer;
	}

	public void setCockpitCrewEmployer(String cockpitCrewEmployer) {
		this.cockpitCrewEmployer = cockpitCrewEmployer;
	}

	public String getCabinCrewEmployer() {
		return cabinCrewEmployer;
	}

	public void setCabinCrewEmployer(String cabinCrewEmployer) {
		this.cabinCrewEmployer = cabinCrewEmployer;
	}

	public String getWetLeaseAirline() {
		return wetLeaseAirline;
	}

	public void setWetLeaseAirline(String wetLeaseAirline) {
		this.wetLeaseAirline = wetLeaseAirline;
	}

	public String getPeriodOfOperation() {
		return periodOfOperation;
	}

	public void setPeriodOfOperation(String periodOfOperation) {
		if (periodOfOperation != null) {
			setPeriodOfOperationFrom(periodOfOperation.split(" ")[0]);
			setPeriodOfOperationTo(periodOfOperation.split(" ")[1]);
		} else {
			this.periodOfOperation = null;
			setPeriodOfOperationFrom(null);
			setPeriodOfOperationTo(null);
		}
	}

	public String getDaysOfOperation() {
		return daysOfOperation;
	}

	public void setDaysOfOperation(String daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	public String getFrequencyRate() {
		return frequencyRate;
	}

	public void setFrequencyRate(String frequencyRate) {
		this.frequencyRate = frequencyRate;
	}

	public String getOnwardFlight() {
		return onwardFlight;
	}

	public void setOnwardFlight(String onwardFlight) {
		this.onwardFlight = onwardFlight;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getBookingDesignator() {
		return bookingDesignator;
	}

	public void setBookingDesignator(String bookingDesignator) {
		this.bookingDesignator = bookingDesignator;
	}

	public String getFlightIdentifierDate() {
		return flightIdentifierDate;
	}

	public void setFlightIdentifierDate(String flightIdentifierDate) {
		this.flightIdentifierDate = flightIdentifierDate;
	}

	public String getBookingModifier() {
		return bookingModifier;
	}

	public void setBookingModifier(String bookingModifier) {
		this.bookingModifier = bookingModifier;
	}

	public String getAircraftConfiguration() {
		return aircraftConfiguration;
	}

	public void setAircraftConfiguration(String aircraftConfiguration) {
		this.aircraftConfiguration = aircraftConfiguration;
	}

	public String getAircraftRegistration() {
		return aircraftRegistration;
	}

	public void setAircraftRegistration(String aircraftRegistration) {
		this.aircraftRegistration = aircraftRegistration;
	}

	public String getTrafficRestrictionNote() {
		return trafficRestrictionNote;
	}

	public void setTrafficRestrictionNote(String trafficRestrictionNote) {
		this.trafficRestrictionNote = trafficRestrictionNote;
	}

	public String getOtherSegmentInfo() {
		return otherSegmentInfo;
	}

	public void setOtherSegmentInfo(String otherSegmentInfo) {
		this.otherSegmentInfo = otherSegmentInfo;
	}

	public String getPeriodOfOperationFrom() {
		return periodOfOperationFrom;
	}

	public void setPeriodOfOperationFrom(String periodOfOperationFrom) {
		this.periodOfOperationFrom = periodOfOperationFrom;
	}

	public String getPeriodOfOperationTo() {
		return periodOfOperationTo;
	}

	public void setPeriodOfOperationTo(String periodOfOperationTo) {
		this.periodOfOperationTo = periodOfOperationTo;
	}

	public List<ASMSSMLegInfoDTO> getLegInList() {
		return legInList;
	}

	public void setLegInList(List<ASMSSMLegInfoDTO> legInList) {
		this.legInList = legInList;
	}

	public void addLegInList(ASMSSMLegInfoDTO legInfo) {
		if (this.legInList == null) {
			this.legInList = new ArrayList<ASMSSMLegInfoDTO>();
		}

		this.legInList.add(legInfo);

	}

	public String getFlightLegChangeIdentifier() {
		return flightLegChangeIdentifier;
	}

	public void setFlightLegChangeIdentifier(String flightLegChangeIdentifier) {
		this.flightLegChangeIdentifier = flightLegChangeIdentifier;
	}

	public String getOldPeriodFrom() {
		return oldPeriodFrom;
	}

	public void setOldPeriodFrom(String oldPeriodFrom) {
		this.oldPeriodFrom = oldPeriodFrom;
	}

	public String getOldPeriodTo() {
		return oldPeriodTo;
	}

	public void setOldPeriodTo(String oldPeriodTo) {
		this.oldPeriodTo = oldPeriodTo;
	}

	public String getOldDaysOfOperation() {
		return oldDaysOfOperation;
	}

	public void setOldDaysOfOperation(String oldDaysOfOperation) {
		this.oldDaysOfOperation = oldDaysOfOperation;
	}

	public String geteTicketInfo() {
		return eTicketInfo;
	}

	public void seteTicketInfo(String eTicketInfo) {
		this.eTicketInfo = eTicketInfo;
	}

	public int getMsgOrderSequence() {
		return msgOrderSequence;
	}

	public void setMsgOrderSequence(int msgOrderSequence) {
		this.msgOrderSequence = msgOrderSequence;
	}

	public String getNewFlightNumberWithSuffix() {
		return newFlightNumberWithSuffix;
	}

	public void setNewFlightNumberWithSuffix(String newFlightNumberWithSuffix) {
		this.newFlightNumberWithSuffix = newFlightNumberWithSuffix;
	}

	public String getNewFlightIdentifierDate() {
		return newFlightIdentifierDate;
	}

	public void setNewFlightIdentifierDate(String newFlightIdentifierDate) {
		this.newFlightIdentifierDate = newFlightIdentifierDate;
	}

	public String getFltChangeEffectedLeg() {
		return fltChangeEffectedLeg;
	}

	public void setFltChangeEffectedLeg(String fltChangeEffectedLeg) {
		this.fltChangeEffectedLeg = fltChangeEffectedLeg;
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	@Override
	public String toString() {
		String nextLine = "\n";
		String space = " ";
		StringBuilder sb = new StringBuilder();
		sb.append(msgOrderSequence);
		sb.append(nextLine);
		sb.append(actionIdentifier);
		if (asmWithdrawalIndicator != null)
			sb.append(asmWithdrawalIndicator);
		sb.append(nextLine);
		sb.append(flightNumber);
		if (flightIdentifierDate != null) {
			sb.append("/");
			sb.append(flightIdentifierDate);
		}
		if (periodOfOperationFrom != null | periodOfOperationTo != null | daysOfOperation != null) {
			sb.append(nextLine);
			sb.append(periodOfOperationFrom);
			sb.append(periodOfOperationTo);
			sb.append(daysOfOperation);
		}
		sb.append(nextLine);
		sb.append(serviceType);
		sb.append(aircraftType);
		sb.append(bookingDesignator);
		sb.append(nextLine);
		if (legInList != null && !legInList.isEmpty()) {
			for (ASMSSMLegInfoDTO leg : legInList) {
				sb.append(leg.getDepartureStation());
				sb.append(leg.getScheduledTimeDeparture());
				sb.append(leg.getArrivalStation());
				sb.append(leg.getScheduledTimeArrival());
				sb.append(nextLine);
			}
		}
		if (otherSegmentInfo != null) {
		sb.append(otherSegmentInfo);
		sb.append(nextLine);
		}
		if (eTicketInfo != null) {
		sb.append(eTicketInfo);
		sb.append(nextLine);
		}
		if (supplementaryInfo != null) {
			sb.append(supplementaryInfo);
			sb.append(nextLine);
		}
		return sb.toString();
	}

	public String getMealServiceNote() {
		return mealServiceNote;
	}

	public void setMealServiceNote(String mealServiceNote) {
		this.mealServiceNote = mealServiceNote;
	}

	public List<String> getOtherSegmentInfoList() {
		return otherSegmentInfoList;
	}

	public void setOtherSegmentInfoList(List<String> otherSegmentInfoList) {
		this.otherSegmentInfoList = otherSegmentInfoList;
	}

	public void addOtherSegmentInfo(String otherSegmentInfo) {
		if (this.otherSegmentInfoList == null) {
			this.otherSegmentInfoList = new ArrayList<String>();
		}
		this.otherSegmentInfoList.add(otherSegmentInfo);
	}

}
