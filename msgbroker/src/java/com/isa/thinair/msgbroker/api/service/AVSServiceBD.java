package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.msgbroker.api.dto.AVSMessageDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AVSServiceBD {

	public static final String SERVICE_NAME = "AVSService";

	public void sendAVSMessages(String gdsCode, Integer gdsId, String airLineCode, AVSMessageDTO avsMessageDTO)
			throws ModuleException;
	
}
