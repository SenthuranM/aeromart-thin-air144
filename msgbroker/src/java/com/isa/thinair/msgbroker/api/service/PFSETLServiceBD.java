package com.isa.thinair.msgbroker.api.service;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface PFSETLServiceBD {

	public static final String SERVICE_NAME = "PFSETLService";
	
	public void sendPFSMessages(List<PFSDTO> pfsDTOList, String airlineCode, boolean isEmptyPfs) throws ModuleException;
	
	public void  sendETLMessages(List<ETLDTO> pfsDTOList, String airlineCode)	throws ModuleException ;
	
}
