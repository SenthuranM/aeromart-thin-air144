package com.isa.thinair.msgbroker.api.dto;


public class OSIPhoneNoDTO extends OSIDTO {
    
    /**
     * 
     */
    private static final long serialVersionUID = -6957469125257929410L;

    private String phoneNo;
    
    private String  phone_Code;

      /**
     * @return the phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * @param phoneNo the phoneNo to set
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * @return the phone_Code
     */
    public String getPhone_Code() {
        return phone_Code;
    }

    /**
     * @param phone_Code the phone_Code to set
     */
    public void setPhone_Code(String phone_Code) {
        this.phone_Code = phone_Code;
    }

}
