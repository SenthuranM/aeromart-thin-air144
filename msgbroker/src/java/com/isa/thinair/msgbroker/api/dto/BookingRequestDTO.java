package com.isa.thinair.msgbroker.api.dto;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingRequestDTO extends RequestDTO {
    
    private static final long serialVersionUID = 1082096819633858785L;

    /** Creates a new instance of SegmentSellMessageDTO */
    public BookingRequestDTO() {
        ssrDTOs = new ArrayList<SSRDTO>();
        osiDTOs = new ArrayList<OSIDTO>();
    }

 //   private String messageIdentifier;

    //private String originatorAddress;

    //private String responderAddress;

    private RecordLocatorDTO originatorRecordLocator;

    private RecordLocatorDTO responderRecordLocator;

    private String communicationReference;

    private Date messageReceivedDateStamp;

    private List changedNameDTOs;

    private List newNameDTOs;
    
    private List<UnchangedNamesDTO> unChangedNameDTOs;

    private List bookingSegmentDTOs;
    
    private List<BookingSegmentDTO> otherAirlineSegmentDTOs;

    private List<SSRDTO> ssrDTOs;

    private List<OSIDTO> osiDTOs;
    
    private List<SSRDTO> unsupportedSSRDTOs;

    private boolean groupBooking;

	private boolean addBaggage;

	private GDSInternalCodes.TypeBMessageCategory messageCategory;

    /**
     * Returns carrier code of the responder TTY address.
     */
    // TODO verify and remove
    /*
     * public String getResponderCarrierCode(){ if (responderAddress != null) {
     * return originatorAddress.substring(originatorAddress.length()-2); }else {
     * return null; } }
     */

    public String getCommunicationReference() {
        return communicationReference;
    }

    public void setCommunicationReference(String communicationReference) {
        this.communicationReference = communicationReference;
    }

/*    public String getMessageIdentifier() {
        return messageIdentifier;
    }

    public void setMessageIdentifier(String messageIdentifier) {
        this.messageIdentifier = messageIdentifier;
    }*/

    public Date getMessageReceivedDateStamp() {
        return messageReceivedDateStamp;
    }

    public void setMessageReceivedDateStamp(Date messageReceivedDateStamp) {
        this.messageReceivedDateStamp = messageReceivedDateStamp;
    }

/*    public String getOriginatorAddress() {
        return originatorAddress;
    }

    public void setOriginatorAddress(String originatorAddress) {
        this.originatorAddress = originatorAddress;
    }*/

    public RecordLocatorDTO getOriginatorRecordLocator() {
        return originatorRecordLocator;
    }

    public void setOriginatorRecordLocator(RecordLocatorDTO originatorRecordLocator) {
        this.originatorRecordLocator = originatorRecordLocator;
    }

    public RecordLocatorDTO getResponderRecordLocator() {
        return responderRecordLocator;
    }

    public void setResponderRecordLocator(RecordLocatorDTO responderRecordLocator) {
        this.responderRecordLocator = responderRecordLocator;
    }

    public List getBookingSegmentDTOs() {
        return bookingSegmentDTOs;
    }

    public void setBookingSegmentDTOs(List bookingSegmentDTOs) {
        this.bookingSegmentDTOs = bookingSegmentDTOs;
    }

    public List getChangedNameDTOs() {
        return changedNameDTOs;
    }

    public void setChangedNameDTOs(List changedNameDTOs) {
        this.changedNameDTOs = changedNameDTOs;
    }

    /**
     * @return the newNameDTOs
     */
    public List getNewNameDTOs() {
        return newNameDTOs;
    }

    /**
     * @param newNameDTOs
     *            the newNameDTOs to set
     */
    public void setNewNameDTOs(List newNameDTOs) {
        this.newNameDTOs = newNameDTOs;
    }

    /**
     * @return the groupBooking
     */
    public boolean isGroupBooking() {
        return groupBooking;
    }

    /**
     * @param groupBooking
     *            the groupBooking to set
     */
    public void setGroupBooking(boolean groupBooking) {
        this.groupBooking = groupBooking;
    }

    public List<SSRDTO> getSsrDTOs() {
        return ssrDTOs;
    }

    public void setSsrDTOs(List<SSRDTO> ssrDTOs) {
        this.ssrDTOs = ssrDTOs;
    }

    public void addSsrDTOs(List<SSRDTO> ssrDTOs) {
        this.ssrDTOs.addAll(ssrDTOs);
    }
    
    public void addSsrDTO(SSRDTO ssrDTO) {
        this.ssrDTOs.add(ssrDTO);
    }

    public List<OSIDTO> getOsiDTOs() {
        return osiDTOs;
    }

    public void setOsiDTOs(List<OSIDTO> osiDTOs) {
        this.osiDTOs = osiDTOs;
    }

    /**
     * @return the unChangedNameDTOs
     */
    public List<UnchangedNamesDTO> getUnChangedNameDTOs() {
        return unChangedNameDTOs;
    }

    /**
     * @param unChangedNameDTOs the unChangedNameDTOs to set
     */
    public void setUnChangedNameDTOs(List<UnchangedNamesDTO> unChangedNameDTOs) {
        this.unChangedNameDTOs = unChangedNameDTOs;
    }

	public List<SSRDTO> getUnsupportedSSRDTOs() {
		return unsupportedSSRDTOs;
	}

	public void setUnsupportedSSRDTOs(List<SSRDTO> unsupportedSSRDTOs) {
		this.unsupportedSSRDTOs = unsupportedSSRDTOs;
	}

    /**
    public String getResponderAddress() {
        return responderAddress;
    }

    public void setResponderAddress(String responderAddress) {
        this.responderAddress = responderAddress;
    }*/

	public GDSInternalCodes.TypeBMessageCategory getMessageCategory() {
		return messageCategory;
	}

	public void setMessageCategory(GDSInternalCodes.TypeBMessageCategory messageCategory) {
		this.messageCategory = messageCategory;
	}

	public boolean isAddBaggage() {
		return addBaggage;
	}

	public void setAddBaggage(boolean addBaggage) {
		this.addBaggage = addBaggage;
	}

	public List<BookingSegmentDTO> getOtherAirlineSegmentDTOs() {
		return otherAirlineSegmentDTOs;
	}

	public void setOtherAirlineSegmentDTOs(List<BookingSegmentDTO> otherAirlineSegmentDTOs) {
		this.otherAirlineSegmentDTOs = otherAirlineSegmentDTOs;
	}
}
