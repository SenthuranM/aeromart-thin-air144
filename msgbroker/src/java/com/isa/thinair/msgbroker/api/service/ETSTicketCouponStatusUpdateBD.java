package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateReqDTO;
import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateResDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author rajitha
 *
 */
public interface ETSTicketCouponStatusUpdateBD {

	public static final String SERVICE_NAME = "TicketCouponStatusUpdateService";

	public FlightStatusUpdateResDTO updateETicketCoupnStatus(FlightStatusUpdateReqDTO eTicketCouponStatusUpdateReq)
			throws ModuleException;

}
