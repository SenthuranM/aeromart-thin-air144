package com.isa.thinair.msgbroker.api.service;

import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CouponStatusTKCREQServiceBD {

	public static final String SERVICE_NAME = "CouponStatusTKCREQService";

	public void updateCouponStatus(ETLDTO etlDTO) throws ModuleException;

}
