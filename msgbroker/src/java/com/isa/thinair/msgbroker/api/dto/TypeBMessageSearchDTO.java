package com.isa.thinair.msgbroker.api.dto;

import java.io.Serializable;

public class TypeBMessageSearchDTO implements Serializable {

	private String pnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
