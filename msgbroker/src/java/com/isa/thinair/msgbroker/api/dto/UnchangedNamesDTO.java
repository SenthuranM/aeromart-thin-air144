package com.isa.thinair.msgbroker.api.dto;
/*
 * DTOPaxBabbage.java
 *
 * Created on August 9, 2006, 10:38 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
import java.io.Serializable;
import java.util.List;
/**
 *
 * @author Chan
 */
public class UnchangedNamesDTO implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -8647219080340399107L;

    /** Previous values of names**/
    private List<NameDTO> unChangedNames;
    
    /** no of pax included in the party*/
    private  int partyTotal;

    /**
     * @return the oldNameDTOs
     */
    public List<NameDTO> getUnChangedNames() {
        return unChangedNames;
    }

    /**
     * @param oldNameDTOs the oldNameDTOs to set
     */
    public void setUnChangedNames(List<NameDTO> oldNameDTOs) {
        this.unChangedNames = oldNameDTOs;
    }

    /**
     * @return the partyTotal
     */
    public int getPartyTotal() {
        return partyTotal;
    }

    /**
     * @param partyTotal the partyTotal to set
     */
    public void setPartyTotal(int partyTotal) {
        this.partyTotal = partyTotal;
    }
  
}
