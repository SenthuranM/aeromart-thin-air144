package com.isa.thinair.msgbroker.core.util;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SMDataElementRequirmentCategory;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SSMDataElement;

public class SSMSubMessageFormat {

	private Map<SSMDataElement, SSMDataElementUseage> ssmSubMessageElementsMap = new HashMap<SSMDataElement, SSMDataElementUseage>();

	public SSMSubMessageFormat(String code) {
		if (code.equals("NEW")) {
			buildNEWSSMSubMessageType();
		}
		if (code.equals("CNL")) {
			buildCNLSSMSubMessageType();
		}
		if (code.equals("RPL")) {
			buildRPLSSMSubMessageType();
		}
		if (code.equals("EQT")) {
			buildEQTSSMSubMessageType();
		}
		if (code.equals("TIM")) {
			buildTIMSSMSubMessageType();
		}		
		if (code.equals("REV")) {
			buildREVSSMSubMessageType();
		}
		if (code.equals("FLT")) {
			buildFLTSSMSubMessageType();
		}
	}

	private void buildNEWSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));

		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SERVICE_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.SERVICE_TYPE, new SSMDataElementUseage(SSMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.AIRCRAFT_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.AIRCRAFT_TYPE, new SSMDataElementUseage(SSMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new SSMDataElementUseage(
				SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.LEG_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.DEPARTURE_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPARTURE_AIRPORT, new SSMDataElementUseage(SSMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DEPATURE_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPATURE_TIME, new SSMDataElementUseage(SSMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STD));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STD, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STD, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ARRIVAL_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_AIRPORT, new SSMDataElementUseage(SSMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ARRIVAL_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_TIME, new SSMDataElementUseage(SSMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STA));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STA, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STA, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.LEG_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, SSMDataElement.SEGMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.OTHER_SEGMENT_INFO));
		ssmSubMessageElementsMap.put(SSMDataElement.OTHER_SEGMENT_INFO, new SSMDataElementUseage(SSMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.SEGMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildCNLSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,
 SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));


		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildRPLSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SERVICE_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.SERVICE_TYPE, new SSMDataElementUseage(SSMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.AIRCRAFT_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.AIRCRAFT_TYPE, new SSMDataElementUseage(SSMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new SSMDataElementUseage(
				SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.LEG_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.DEPARTURE_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPARTURE_AIRPORT, new SSMDataElementUseage(SSMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DEPATURE_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPATURE_TIME, new SSMDataElementUseage(SSMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STD));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STD, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STD, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ARRIVAL_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_AIRPORT, new SSMDataElementUseage(SSMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ARRIVAL_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_TIME, new SSMDataElementUseage(SSMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STA));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STA, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STA, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.LEG_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, SSMDataElement.SEGMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.OTHER_SEGMENT_INFO));
		ssmSubMessageElementsMap.put(SSMDataElement.OTHER_SEGMENT_INFO, new SSMDataElementUseage(SSMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.SEGMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,
 SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildEQTSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SERVICE_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.SERVICE_TYPE, new SSMDataElementUseage(SSMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.AIRCRAFT_TYPE));
		ssmSubMessageElementsMap.put(SSMDataElement.AIRCRAFT_TYPE, new SSMDataElementUseage(SSMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new SSMDataElementUseage(
				SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.EQUIPMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.EQUIPMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, SSMDataElement.SEGMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.OTHER_SEGMENT_INFO));
		ssmSubMessageElementsMap.put(SSMDataElement.OTHER_SEGMENT_INFO, new SSMDataElementUseage(SSMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.SEGMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,
 SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildTIMSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.LEG_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.DEPARTURE_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPARTURE_AIRPORT, new SSMDataElementUseage(SSMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DEPATURE_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.DEPATURE_TIME, new SSMDataElementUseage(SSMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STD));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STD, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STD, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ARRIVAL_AIRPORT));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_AIRPORT, new SSMDataElementUseage(SSMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ARRIVAL_TIME));
		ssmSubMessageElementsMap.put(SSMDataElement.ARRIVAL_TIME, new SSMDataElementUseage(SSMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.DATE_VARIATION_FOR_STA));
		ssmSubMessageElementsMap.put(SSMDataElement.DATE_VARIATION_FOR_STA, new SSMDataElementUseage(
				SSMDataElement.DATE_VARIATION_FOR_STA, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.LEG_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.LEG_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, SSMDataElement.SEGMENT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.OTHER_SEGMENT_INFO));
		ssmSubMessageElementsMap.put(SSMDataElement.OTHER_SEGMENT_INFO, new SSMDataElementUseage(SSMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.SEGMENT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,
 SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}
	
	
	private void buildREVSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		
		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_BOL));
		
		
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.NEW_PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.NEW_PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.NEW_PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.NEW_PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.NEW_DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.NEW_DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(
				SSMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,
 SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}
	
	private void buildFLTSSMSubMessageType() {

		ssmSubMessageElementsMap.put(SSMDataElement.BEGIN_SSM, new SSMDataElementUseage(SSMDataElement.BEGIN_SSM,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.MESSAGE_HEADING_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		// ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// new SSMDataElementUseage(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_HEADING_EOL));
		// ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_HEADING_EOL,
		// new SSMDataElementUseage(SSMDataElement.MESSAGE_HEADING_EOL,
		// SMDataElementRequirmentCategory.Mandatory,SSMDataElement.MESSAGE_REFERENCE_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		ssmSubMessageElementsMap.put(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new SSMDataElementUseage(
				SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.TIME_MODE));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE, new SSMDataElementUseage(SSMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.TIME_MODE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.TIME_MODE_EOL, new SSMDataElementUseage(SSMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.MESSAGE_REFERENCE_BOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_BOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.MESSAGE_REFERENCE_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.MESSAGE_REFERENCE_EOL, new SSMDataElementUseage(
				SSMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.ACTION_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.ACTION_IDENTIFIER));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_IDENTIFIER, new SSMDataElementUseage(SSMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.ASM_WITHDRAWAL_INDICATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.ASM_WITHDRAWAL_INDICATOR, new SSMDataElementUseage(
				SSMDataElement.ASM_WITHDRAWAL_INDICATOR, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.ACTION_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.ACTION_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_DESIGNATOR, new SSMDataElementUseage(SSMDataElement.FLIGHT_DESIGNATOR,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, new SSMDataElementUseage(
				SSMDataElement.PERIOD_OF_OPERATION_TO_DATE, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.DAYS_OF_OPERATION));
		ssmSubMessageElementsMap.put(SSMDataElement.DAYS_OF_OPERATION, new SSMDataElementUseage(SSMDataElement.DAYS_OF_OPERATION,
				SMDataElementRequirmentCategory.Mandatory, SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, new SSMDataElementUseage(
				SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.NEW_FLIGHT_INFORMATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.NEW_FLIGHT_INFORMATION_BOL, new SSMDataElementUseage(
				SSMDataElement.NEW_FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.NEW_FLIGHT_DESIGNATOR));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_FLIGHT_DESIGNATOR, new SSMDataElementUseage(
				SSMDataElement.NEW_FLIGHT_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.NEW_FLIGHT_INFORMATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.NEW_FLIGHT_INFORMATION_EOL, new SSMDataElementUseage(
SSMDataElement.NEW_FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				SSMDataElement.COMMENTS_BOL));

		/* Other Segment Information. Have to configure generate method also : Conditional */

		// ssmSubMessageElementsMap.put(SSMDataElement.SEGMENT_INFORMATION_BOL, new SSMDataElementUseage(
		// SSMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
		// SSMDataElement.OTHER_SEGMENT_INFO));
		// ssmSubMessageElementsMap.put(SSMDataElement.OTHER_SEGMENT_INFO, new SSMDataElementUseage(
		// SSMDataElement.OTHER_SEGMENT_INFO, SMDataElementRequirmentCategory.Conditional,
		// SSMDataElement.SEGMENT_INFORMATION_EOL));
		// ssmSubMessageElementsMap
		// .put(SSMDataElement.SEGMENT_INFORMATION_EOL, new SSMDataElementUseage(SSMDataElement.SEGMENT_INFORMATION_EOL,
		// SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_BOL, new SSMDataElementUseage(SSMDataElement.COMMENTS_BOL,
				SMDataElementRequirmentCategory.NotApplicable, SSMDataElement.COMMENTS));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS, new SSMDataElementUseage(SSMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.COMMENTS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.COMMENTS_EOL, new SSMDataElementUseage(SSMDataElement.COMMENTS_EOL,
				SMDataElementRequirmentCategory.Conditional, SSMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUB­MESSAGE_SEPARATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, new SSMDataElementUseage(
				SSMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				SSMDataElement.SUPPLEMENTARY_INFORMATION));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATION, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		ssmSubMessageElementsMap.put(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new SSMDataElementUseage(
				SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				SSMDataElement.END_SSM));

		ssmSubMessageElementsMap.put(SSMDataElement.END_SSM, new SSMDataElementUseage(SSMDataElement.END_SSM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	public boolean isMandatoryDataElement(SSMDataElementUseage ssmDataElementUseage) {
		return ssmDataElementUseage.getSsmDataElementRequirmentCategory().equals(SMDataElementRequirmentCategory.Mandatory);
	}

	public boolean isNotApplicableDataElement(SSMDataElementUseage ssmDataElementUseage) {
		return ssmDataElementUseage.getSsmDataElementRequirmentCategory().equals(SMDataElementRequirmentCategory.NotApplicable);
	}

	public Map<SSMDataElement, SSMDataElementUseage> getSSMSubMessageElementsMap() {
		return ssmSubMessageElementsMap;
	}

	public SSMDataElementUseage getNextSSMDataElementUsage(SSMDataElementUseage currentSSMDataElementUseage) {
		if (currentSSMDataElementUseage == null) {
			return ssmSubMessageElementsMap.get(SSMDataElement.BEGIN_SSM);
		} else {
			return ssmSubMessageElementsMap.get(currentSSMDataElementUseage.getNextSSMDataElement());
		}
	}

	public SSMDataElementUseage getNextSSMDataElementUsage(SSMDataElement currentSSMDataElement) {
		if (currentSSMDataElement == null) {
			return ssmSubMessageElementsMap.get(SSMDataElement.BEGIN_SSM);
		} else {
			return ssmSubMessageElementsMap.get(ssmSubMessageElementsMap.get(currentSSMDataElement).getNextSSMDataElement());
		}
	}
}
