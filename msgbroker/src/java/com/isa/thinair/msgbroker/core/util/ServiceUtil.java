package com.isa.thinair.msgbroker.core.util;

import java.util.Date;
import java.util.Map;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;

public class ServiceUtil {
	MessageDAO messageDAO;
	private static Map requestTypes;

	private static int LOCK_TIMEOUT_TIME_IN_MINUTES = 10;

	public ServiceUtil() {
		if (requestTypes == null)
			requestTypes = getMessageDAO().getActiveMessageTypeTimeToLives();
	}

	public boolean isMessageTimedOut(OutMessage outMessage) {
		if (outMessage.getMessageType() == null) {
			return false;
		}
		if (((Double) requestTypes.get(outMessage.getMessageType())).doubleValue() == -1) {
			return false;
		}
		return !CalendarUtil.isGreaterThan(
				CalendarUtil.getOfssetInMilisecondAddedDate(outMessage.getReceivedDate(),
						new Double(((Double) requestTypes.get(outMessage.getMessageType())).doubleValue() * 60000).intValue()),
				new Date());
	}

	public boolean isMessageTimedOut(InMessage inMessage) {
		if (inMessage.getMessageType() == null) {
			return false;
		}
		if (((Double) requestTypes.get(inMessage.getMessageType())).doubleValue() == -1) {
			return false;
		}
		return !CalendarUtil.isGreaterThan(
				CalendarUtil.getOfssetInMilisecondAddedDate(inMessage.getReceivedDate(),
						new Double(((Double) requestTypes.get(inMessage.getMessageType())).doubleValue() * 60000).intValue()),
				new Date());
	}

	public boolean isMessageUnlockedOrLockTimedOut(InMessage inMessage) {
		if (inMessage.getLockStatus().equals(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED)) {
			return true;
		} else {
			return !CalendarUtil.isGreaterThan(
					CalendarUtil.getOfssetInMilisecondAddedDate(inMessage.getLastLockedTime(), LOCK_TIMEOUT_TIME_IN_MINUTES),
					new Date());
		}
	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}
	
	public static boolean isCodeShareGDSCode(String gdsCode) {
		GlobalConfig globalConfig = LookupUtil.getGlobalConfig();
		if (gdsCode != null && !"".equals(gdsCode.trim())) {
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				for (String gdsId : gdsStatusMap.keySet()) {
					GDSStatusTO gdsStatusTO = gdsStatusMap.get(gdsId);
					if (gdsCode.equalsIgnoreCase(gdsStatusTO.getGdsCode()) && gdsStatusTO.isCodeShareCarrier()) {
						return true;
					}
				}

			}
		}

		return false;
	}
}
