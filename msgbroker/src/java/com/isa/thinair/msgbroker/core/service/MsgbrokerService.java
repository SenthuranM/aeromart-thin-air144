/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.msgbroker.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * 
 *         MsgBroker's service interface
 * @isa.module.service-interface module-name="msgbroker" description="msgbroker module"
 */
public class MsgbrokerService extends DefaultModule {
}
