package com.isa.thinair.msgbroker.core.bl;

/*
 * RegularExpressions.java
 *
 * Created on October 10, 2007, 1:22 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * 
 * @author chan
 */
public class RegularExpressionLookup {

	/** Creates a new instance of RegularExpressions */
	public RegularExpressionLookup() {
		loadRegexHashMap();
	}

	private LinkedHashMap elementRegexHashMap;
	private HashMap messageSequenceRegexHashMap;
	private HashMap subElementReagexHashMap;

	public void loadRegexHashMap() {

		/*************************************
		 * DONOT Change the insertion order.
		 *************************************/

		// TODO remove cmmnt
		// linked hash map used inorder to make sure the key set maintain the insertion order.
		elementRegexHashMap = new LinkedHashMap();
		/*
		 * Groups for Message Header line 1 group1 = recipientAddress
		 */

		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS,
				MesageParserConstants.ElementRegularExpressions.RECIPIENT_ADDRESS);
		/*
		 * Groups for Message Header line 2 group1 = senderAddress group2 = communicationReference
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SENDER_ADDRESS,
				MesageParserConstants.ElementRegularExpressions.SENDER_ADDRESS);
		/*
		 * Groups for Message Identifier Element group1 = messageIdentifier
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER,
				MesageParserConstants.ElementRegularExpressions.MESSAGE_IDENTIFIER);
		/*
		 * Groups for Record Loactor Element group1 = recordLocator
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.RECORD_LOCATOR,
				MesageParserConstants.ElementRegularExpressions.RECORD_LOCATOR);
		/*
		 * Groups for Name Array group1 = nameList
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.NAME,
				MesageParserConstants.ElementRegularExpressions.NAME);
		// elementRegexHashMap.put( "NAM","(([1-9]+[0-9]?)([A-Z ]+)(/?)([A-Z ]*)(/?[A-Z/ ]*)[ ]?+)+");

		/*
		 * Groups for Segment Element group1 = carrierCode group2 = flightNumber group3 = reservationBookingDesignator
		 * group4 = flightDate group5 = origin group6 = destination group7 = actionCode group8 = noofPaxes group10 =
		 * departureTimr group11 = arrivaltime
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SEGMENT,
				MesageParserConstants.ElementRegularExpressions.SEGMENT);
		
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.CS_SEGMENT,
				MesageParserConstants.ElementRegularExpressions.CS_SEGMENT);

		/** Handle SSR comes with SAET code ssr */
		// elementRegexHashMap.put(
		// MesageParserConstants.GDSMessageElements.SSR_SEAT,MesageParserConstants.ElementRegularExpressions.SSR_SEATS);

		/** Handle SSR comes with one of OTHS|CKIN|GRPF|GRPS|MEDA code ssr */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_OTHER,
				MesageParserConstants.ElementRegularExpressions.SSR_OTHERS);

		/** SSR Child information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_CHLD,
				MesageParserConstants.ElementRegularExpressions.SSR_CHILD);

		/** SSR passport information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_PST1,
				MesageParserConstants.ElementRegularExpressions.SSR_PSPT);

		/** SSR passport continuation information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_PST2,
				MesageParserConstants.ElementRegularExpressions.SSR_PSPT_CONT);

		/** SSR FOID information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_FID1,
				MesageParserConstants.ElementRegularExpressions.SSR_FOID);

		/** SSR FOID continuation information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_FID2,
				MesageParserConstants.ElementRegularExpressions.SSR_FOID_CONT);

		/** SSR immergence contact no provide by the passenger */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_PCT,
				MesageParserConstants.ElementRegularExpressions.SSR_PCTC);

		/** SSR travel document non automated information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_DCS1,
				MesageParserConstants.ElementRegularExpressions.SSR_DOCS1);

		/** SSR travel document information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_DCS2,
				MesageParserConstants.ElementRegularExpressions.SSR_DOCS2);

		/** SSR travel document continuation information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_DCS_CONT,
				MesageParserConstants.ElementRegularExpressions.SSR_DOCS_CONT);

		/** SSR travel document (Visa) non automated information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_DOCO,
				MesageParserConstants.ElementRegularExpressions.SSR_DOCO1);
		
		/** SSR travel document (Visa) continuation information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_DOCO_CONT,
				MesageParserConstants.ElementRegularExpressions.SSR_DOCO_CONT);
		
		/** SSR travel document continuation information */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_LANG,
				MesageParserConstants.ElementRegularExpressions.SSR_LANG);

		/*
		 * Groups for SSR Element group1 = CodeSSR group2 = reservationBookingDesignator group3 = airLineIndentifier
		 * group4 = actionCode group5 = noofPaxes group6 = cityPair group7 = flightNumber group8 =
		 * reservationBookingDesignator group9 = flightDate
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_BEGIN,
				MesageParserConstants.ElementRegularExpressions.SSR_BEGIN);
		// elementRegexHashMap.put(
		// "SS","(SSR) ([A-Z]{4}) ([A-Z]{2}) ([A-Z]{2})([0-9]{1,2}) ([A-Z]{6})([0-9]{3,4})([A-Z]{1})([0-9]{2}[A-Z]{3})-((([1-9]?[0-9]?)([A-Z ]+)(/?)([A-Z ]*)(/?[A-Z/ ]*)[ ]?\\.]+)+)([A-Z 0-9]*)");

		/*
		 * Groups for SSR Element group1 = CodeSSR group2 = airLineIndentifier group3 = actionCode group4 = noofPaxes
		 * group5 = segment group6 = UNUSED group7 = nameString group8 = UNUSED group9 = freeText
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.SSR_CONT,
				MesageParserConstants.ElementRegularExpressions.SSR_CONT);
		// elementRegexHashMap.put(
		// "SS","(SSR) ([A-Z]{4}) ([A-Z]{2}) ([A-Z]{2})([0-9]{1,2}) ([A-Z]{6})([0-9]{3,4})([A-Z]{1})([0-9]{2}[A-Z]{3})-((([1-9]?[0-9]?)([A-Z ]+)(/?)([A-Z ]*)(/?[A-Z/ ]*)[ ]?\\.]+)+)([A-Z 0-9]*)");

		/** handle OSI TCP informations */
		// elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_PARTY_INFO,MesageParserConstants.ElementRegularExpressions.OSI_TCP);

		/*
		 * Groups for OSI Element group1 = airLineIndentifier group2 = codeOSi group3 = serviceInfo
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_INFO_CONT,
				MesageParserConstants.ElementRegularExpressions.OTHER_SERVICE_INFO_CONT);

		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_INFO_BEGIN,
				MesageParserConstants.ElementRegularExpressions.OTHER_SERVICE_INFO_BEGIN);

		/*
		 * Groups for OSI Element group1 = airLineIndentifier group2 = codeOSi group3 = serviceInfo
		 */

		// TODO remove if

		/*
		 * Groups for Message abbreviation
		 */
		elementRegexHashMap.put(MesageParserConstants.GDSMessageElements.MISCELLANEOUS_ABBREVIATION,
				MesageParserConstants.ElementRegularExpressions.MISCELLANEOUS_ABBREVIATION);

		/*
		 * Regular expressions for checking message element sequences
		 */
		messageSequenceRegexHashMap = new HashMap();
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.BOOKING,
				MesageParserConstants.ElementSequenceRegularExpressions.BOOKING_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.AMEND,
				MesageParserConstants.ElementSequenceRegularExpressions.AMEND_CANCEL_MESSAGE);
		// messageSequenceRegexHashMap.put( "CA","H1H2RLRL(NE)*(SE)+(SS)*(OS)*");
		// messageSequenceRegexHashMap.put( "RB","H1H2RLRL(NE)*(SE)+(SS)*(OS)*");
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.DIVIDE,
				MesageParserConstants.ElementSequenceRegularExpressions.DIVIDE_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.RECORD_LOACTOR_REQUEST,
				MesageParserConstants.ElementSequenceRegularExpressions.RLR_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.PDM,
				MesageParserConstants.ElementSequenceRegularExpressions.PDM_MESSAGE);
		// TODO added
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.NCO,
				MesageParserConstants.ElementSequenceRegularExpressions.NCO_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.SCHEDULE_CHANGE,
				MesageParserConstants.ElementSequenceRegularExpressions.SCHEDULE_CHANGE_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.CHNM,
				MesageParserConstants.ElementSequenceRegularExpressions.CHNM_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.NAR,
				MesageParserConstants.ElementSequenceRegularExpressions.NAR_MESSAGE);
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.DIVIDE_RESP,
				MesageParserConstants.ElementSequenceRegularExpressions.DIVIDE_MESSAGE1);
		
		messageSequenceRegexHashMap.put(MesageParserConstants.MessageTypes.TRL_RESP,
				MesageParserConstants.ElementSequenceRegularExpressions.TRL_MESSAGE);

		/* Regular expression for sub element in element sequence */
		subElementReagexHashMap = new HashMap();
		subElementReagexHashMap.put(MesageParserConstants.SubElementTypes.SSR_SEGMENT,
				MesageParserConstants.SubElementRegularExpressions.SSR_SEGMENT_ELEMENT);

	}

	public LinkedHashMap getElementRegexHashMap() {
		return this.elementRegexHashMap;
	}

	public HashMap getMessageSequenceRegexHashMap() {
		return this.messageSequenceRegexHashMap;
	}

	/**
	 * @return the subElementReagexHashMap
	 */
	public HashMap getSubElementReagexHashMap() {
		return subElementReagexHashMap;
	}

}
