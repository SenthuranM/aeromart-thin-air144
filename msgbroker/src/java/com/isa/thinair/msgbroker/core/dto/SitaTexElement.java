package com.isa.thinair.msgbroker.core.dto;

public interface SitaTexElement {

	String EOL = "\n";
	String DATA_DELIMITER = ",";
	String ELEMENT_ID_PREFIX = "=";
	String HEADER = "HEADER";
	String PRIORITY = "PRIORITY";
	String DESTINATION_TYPE_B = "DESTINATION TYPE B";
	String ORIGIN = "ORIGIN";
	String DOUBLE_SIGNATURE = "DBLSIG";
	String MESSAGE_ID = "MSGID";
	String SUBJECT = "SUBJECT";
	String STANDARD_MSG_ID = "SMI";
	String FAX_HEADER = "FAX HEADER";
	String TEXT = "TEXT";
	String ATTACHMENTS = "ATTACHMENTS";

	public interface SitaTexHeader {
		String SENT = "SND";
		String ERROR_NOT_SENT = "SER";
		String RECEIVED = "RCV";
		String RECIEVED_ERROR = "RER";

		String DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm";
	}

	public interface SitaTexPriority {
		String HIGHEST = "QU";
		String NORMAL = "QN";
		String LOWEST = "QD";
	}

	public interface SitaTexDestinations {
		String NATIVE = "STX";
		String RTB_SERVER = "RTB";
		String BFAX_SERVER = "FAX";
		String TELEX_SERVER = "TELEX";
		String LAN = "LAN";
		String INTERNET = "INTERNET";
		String FAX_DELIVERY_SERVICE_SERVER = "FDS";
		String EXTENDED = "INTERNET";
	}
}
