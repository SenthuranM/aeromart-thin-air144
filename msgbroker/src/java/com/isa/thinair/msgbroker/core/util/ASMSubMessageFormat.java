package com.isa.thinair.msgbroker.core.util;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.ASMDataElement;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SMDataElementRequirmentCategory;

public class ASMSubMessageFormat {

	private Map<ASMDataElement, ASMDataElementUseage> asmSubMessageElementsMap = new HashMap<ASMDataElement, ASMDataElementUseage>();

	public ASMSubMessageFormat(String code) {
		if (code.equals("NEW")) {
			buildNEWASMSubMessageType();
		}
		if (code.equals("CNL")) {
			buildCNLASMSubMessageType();
		}
		if (code.equals("RPL")) {
			buildRPLASMSubMessageType();
		}
		if (code.equals("EQT")) {
			buildEQTASMSubMessageType();
		}
		if (code.equals("TIM")) {
			buildTIMASMSubMessageType();
		}
		if (code.equals("FLT")) {
			buildFLTASMSubMessageType();
		}
	}

	private void buildNEWASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SERVICE_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.SERVICE_TYPE, new ASMDataElementUseage(ASMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.AIRCRAFT_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.AIRCRAFT_TYPE, new ASMDataElementUseage(ASMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		asmSubMessageElementsMap.put(ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new ASMDataElementUseage(
				ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.LEG_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.DEPARTURE_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.DEPARTURE_AIRPORT, new ASMDataElementUseage(ASMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.DEPATURE_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.DEPATURE_TIME, new ASMDataElementUseage(ASMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_AIRPORT, new ASMDataElementUseage(ASMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_TIME, new ASMDataElementUseage(ASMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.LEG_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, ASMDataElement.SEGMENT_INFORMATION_BOL));		
		
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.OTHER_SEGMENT_INFO));
		asmSubMessageElementsMap.put(ASMDataElement.OTHER_SEGMENT_INFO, new ASMDataElementUseage(ASMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.SEGMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.COMMENTS_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_BOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.COMMENTS));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS, new ASMDataElementUseage(ASMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.COMMENTS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_EOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildCNLASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildRPLASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SERVICE_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.SERVICE_TYPE, new ASMDataElementUseage(ASMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.AIRCRAFT_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.AIRCRAFT_TYPE, new ASMDataElementUseage(ASMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		asmSubMessageElementsMap.put(ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new ASMDataElementUseage(
				ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.LEG_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.DEPARTURE_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.DEPARTURE_AIRPORT, new ASMDataElementUseage(ASMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.DEPATURE_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.DEPATURE_TIME, new ASMDataElementUseage(ASMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_AIRPORT, new ASMDataElementUseage(ASMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_TIME, new ASMDataElementUseage(ASMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.LEG_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, ASMDataElement.SEGMENT_INFORMATION_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.OTHER_SEGMENT_INFO));
		asmSubMessageElementsMap.put(ASMDataElement.OTHER_SEGMENT_INFO, new ASMDataElementUseage(ASMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.SEGMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.COMMENTS_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_BOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.COMMENTS));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS, new ASMDataElementUseage(ASMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.COMMENTS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_EOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildEQTASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SERVICE_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.SERVICE_TYPE, new ASMDataElementUseage(ASMDataElement.SERVICE_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.AIRCRAFT_TYPE));
		asmSubMessageElementsMap.put(ASMDataElement.AIRCRAFT_TYPE, new ASMDataElementUseage(ASMDataElement.AIRCRAFT_TYPE,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR));
		asmSubMessageElementsMap.put(ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, new ASMDataElementUseage(
				ASMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.EQUIPMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.EQUIPMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.EQUIPMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, ASMDataElement.SEGMENT_INFORMATION_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.OTHER_SEGMENT_INFO));
		asmSubMessageElementsMap.put(ASMDataElement.OTHER_SEGMENT_INFO, new ASMDataElementUseage(ASMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.SEGMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.COMMENTS_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_BOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.COMMENTS));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS, new ASMDataElementUseage(ASMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.COMMENTS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_EOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildTIMASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.LEG_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.DEPARTURE_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.DEPARTURE_AIRPORT, new ASMDataElementUseage(ASMDataElement.DEPARTURE_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.DEPATURE_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.DEPATURE_TIME, new ASMDataElementUseage(ASMDataElement.DEPATURE_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_AIRPORT));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_AIRPORT, new ASMDataElementUseage(ASMDataElement.ARRIVAL_AIRPORT,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ARRIVAL_TIME));
		asmSubMessageElementsMap.put(ASMDataElement.ARRIVAL_TIME, new ASMDataElementUseage(ASMDataElement.ARRIVAL_TIME,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.LEG_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.LEG_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.LEG_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory, ASMDataElement.SEGMENT_INFORMATION_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.OTHER_SEGMENT_INFO));
		asmSubMessageElementsMap.put(ASMDataElement.OTHER_SEGMENT_INFO, new ASMDataElementUseage(ASMDataElement.OTHER_SEGMENT_INFO,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.SEGMENT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SEGMENT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SEGMENT_INFORMATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.COMMENTS_BOL));
		
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_BOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.COMMENTS));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS, new ASMDataElementUseage(ASMDataElement.COMMENTS,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.COMMENTS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.COMMENTS_EOL, new ASMDataElementUseage(
				ASMDataElement.COMMENTS_EOL, SMDataElementRequirmentCategory.Conditional,ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	private void buildFLTASMSubMessageType() {

		asmSubMessageElementsMap.put(ASMDataElement.BEGIN_ASM, new ASMDataElementUseage(ASMDataElement.BEGIN_ASM,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.MESSAGE_HEADING_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_HEADING_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_HEADING_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL));

		asmSubMessageElementsMap.put(ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, new ASMDataElementUseage(
				ASMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.TIME_MODE));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE, new ASMDataElementUseage(ASMDataElement.TIME_MODE,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.TIME_MODE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.TIME_MODE_EOL, new ASMDataElementUseage(ASMDataElement.TIME_MODE_EOL,
				SMDataElementRequirmentCategory.Conditional, ASMDataElement.MESSAGE_REFERENCE_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_BOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_SEQUENCE_REFERENCE, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.MESSAGE_REFERENCE_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.MESSAGE_REFERENCE_EOL, new ASMDataElementUseage(
				ASMDataElement.MESSAGE_REFERENCE_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.ACTION_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.ACTION_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.ACTION_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.ACTION_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.ACTION_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.ACTION_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_BOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_IDENTIFIER, new ASMDataElementUseage(ASMDataElement.FLIGHT_IDENTIFIER,
				SMDataElementRequirmentCategory.Mandatory, ASMDataElement.NEW_FLIGHT_IDENTIFIER));
		asmSubMessageElementsMap.put(ASMDataElement.NEW_FLIGHT_IDENTIFIER, new ASMDataElementUseage(
				ASMDataElement.NEW_FLIGHT_IDENTIFIER, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.FLIGHT_INFORMATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.FLIGHT_INFORMATION_EOL, new ASMDataElementUseage(
				ASMDataElement.FLIGHT_INFORMATION_EOL, SMDataElementRequirmentCategory.Mandatory,
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUB­MESSAGE_SEPARATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, new ASMDataElementUseage(
				ASMDataElement.SUB­MESSAGE_SEPARATION_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL));

		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL, SMDataElementRequirmentCategory.NotApplicable,
				ASMDataElement.SUPPLEMENTARY_INFORMATION));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATION, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATION, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL));
		asmSubMessageElementsMap.put(ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, new ASMDataElementUseage(
				ASMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL, SMDataElementRequirmentCategory.Conditional,
				ASMDataElement.END_ASM));

		asmSubMessageElementsMap.put(ASMDataElement.END_ASM, new ASMDataElementUseage(ASMDataElement.END_ASM,
				SMDataElementRequirmentCategory.Mandatory, null));

	}

	public boolean isMandatoryDataElement(ASMDataElementUseage asmDataElementUseage) {
		return asmDataElementUseage.getSsmDataElementRequirmentCategory().equals(SMDataElementRequirmentCategory.Mandatory);
	}

	public boolean isNotApplicableDataElement(ASMDataElementUseage asmDataElementUseage) {
		return asmDataElementUseage.getSsmDataElementRequirmentCategory().equals(SMDataElementRequirmentCategory.NotApplicable);
	}

	public Map<ASMDataElement, ASMDataElementUseage> getASMSubMessageElementsMap() {
		return asmSubMessageElementsMap;
	}

	public ASMDataElementUseage getNextASMDataElementUsage(ASMDataElementUseage currentASMDataElementUseage) {
		if (currentASMDataElementUseage == null) {
			return asmSubMessageElementsMap.get(ASMDataElement.BEGIN_ASM);
		} else {
			return asmSubMessageElementsMap.get(currentASMDataElementUseage.getNextASMDataElement());
		}
	}

	public ASMDataElementUseage getNextASMDataElementUsage(ASMDataElement currentASMDataElement) {
		if (currentASMDataElement == null) {
			return asmSubMessageElementsMap.get(ASMDataElement.BEGIN_ASM);
		} else {
			return asmSubMessageElementsMap.get(asmSubMessageElementsMap.get(currentASMDataElement).getNextASMDataElement());
		}
	}
}
