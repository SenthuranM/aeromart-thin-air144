package com.isa.thinair.msgbroker.core.bl;

public class MesageParserConstants {

	/** Denotes the operating system */
	public static interface ElementRegularExpressions {
		/*
		 * Groups for Recipient Address Element group1 = recipientAddress
		 */
		public static final String RECIPIENT_ADDRESS = "([A-Z1-9]{6,8})+(( )([A-Z1-9]{7,8}))*";
		/*
		 * Groups for Sender Address Element group1 = senderAddress group2 = communicationReference
		 */
		public static final String SENDER_ADDRESS = "\\.([A-Z1-9]{6,8})+ ?([0-9]{6,8})?(//[0-9]*)?";
		/*
		 * Groups for Record Loactor Element group1 = bookingOffice group2 = pnrLocator group3 = ta_of_cityCode group4 =
		 * completeRL
		 */
		public static final String RECORD_LOCATOR = "([A-Z][A-Z1-9]{4} |[A-Z][A-Z1-9]{4})([A-Z0-9]{4,11} |[A-Z0-9]{4,11})?([A-Z0-9]{6})?(/?)([A-Z0-9]{2,9})?(/?)(([0-9]{7,8})?)?(/?)([A-Z]{3})?(/?)([A-Z0-9]{2})?(/?)([A-Z])?(/?)([A-Z]{2,3})?(/?)([A-Z]{2,3})?(/?)([A-Z]{2})?(/?)([A-Z0-9]{9})?(/?)([A-Z]{3})?";

		public static final String MESSAGE_IDENTIFIER = "([A-Z]{3})";

		public static final String MISCELLANEOUS_ABBREVIATION = "([A-Z]{3,4})";

		public static final String NAME = "(([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+";

		public static final String SEGMENT = "([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1})([0-9]{3,4})([A-Z]{1})([0-9]{2}[A-Z]{3}([0-9]{2})?) ([A-Z]{3})([A-Z]{3}) ([A-Z]{2})([0-9]{1,2})(/([0-9]{4}) ([0-9]{4})|/([0-9]{4})([0-9]{4}))?(/(M?[1-2]{1}))?( \\.[0-9]{1,2}[A-Z]?/?[0-9]*[A-Z]?\\.)?( \\*[A-Z0-9/]*)?( [0-9A-Z]{2,3})?";
		
		public static final String CS_SEGMENT = "([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1})([0-9]{3,4})([A-Z]{1})/([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1})([0-9]{3,4})([A-Z]{1})([0-9]{2}[A-Z]{3}([0-9]{2})?) ([A-Z]{3})([A-Z]{3}) ([A-Z]{2})([0-9]{1,2})(/([0-9]{4}) ([0-9]{4})|/([0-9]{4})([0-9]{4}))?(/(M?[1-2]{1}))?( \\.[0-9]{1,2}[A-Z]?/?[0-9]*[A-Z]?\\.)?( \\*[A-Z0-9/]*)?( [0-9A-Z]{2,3})?";

		// public static final String SSR_BEGIN =
		// "SSR ([A-Z]{4}) ([A-Z]{2}) ([A-Z]{2})([1-9]{1,2})* ([A-Z0-9]{15,16})*(-(.*))?(\\.([A-Z0-9 ]*))?";
		// TODO Review changes made to ssr
		// orginal public static final String SSR_BEGIN =
		// "SSR ([A-Z]{4}) ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}) ([A-Z]{2})([1-9]{1,2})* ([A-Z0-9]{15,16})*(-(.*))?(\\.([A-Z0-9 ]*))?";

		public static final String SSR_CHILD = "SSR CHLD ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([1-9])((/)([0-9A-Z]{1,16}))?((-)(([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+)?" ;

		public static final String SSR_PSPT = "SSR PSPT ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) HK1(/)?([A-Z0-9]{8,25})?(/)?([A-Z]{2,3})?(/)?([0-9]{1,2}[A-Z]{3}[0-9]{2})?(/)?([A-Z]*)?(/)?([A-Z]*)?(/)?((F|M|I|FI|MI|/F/H|/M/H|/I/H|/FI/H|/MI/H)-([0-9]?[A-Z]*/[A-Z]*))?(\\.)?([A-Z0-9]*)?";

		public static final String SSR_PSPT_CONT = "SSR PSPT ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3})( )?(///)(.*)?";

		public static final String SSR_FOID = "SSR FOID ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) HK([0-9]{1,2})*(/)?([A-z]{2})([A-Z0-9]{0,25})(-[^\\.]*)?";

		public static final String SSR_FOID_CONT = "SSR FOID ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3})( )?(///)(.*)?";

		public static final String SSR_PCTC = "SSR PCTC ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) HK(//|/)([A-Z ]*)?(/)?([A-Z0-9 ]*)?(-[^\\.]*)?(\\.(.*))?";

		public static final String SSR_LANG = "SSR LANG ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) HK([0-9]{1,2})*(/| )([0-9A-Z]{1,16})?(-[^\\.]*)?(\\.(.*))?";

		public static final String SSR_BEGIN = "SSR ([A-Z]{4}) ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([1-9]{1,2})* ([A-Z0-9 ]{15,17})*(-[^\\.]*)?(\\.(.*))?";

		public static final String SSR_CONT = "SSR ([A-Z]{4}) ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3})( )?(///)([A-Z/ 0-9\\.]*)?(\\.([A-Z0-9]*))?";

		public static final String SSR_OTHERS = "SSR (OTHS|CKIN|GRPF|GRPS|MEDA|GPST|TKTL) ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) (.*)?";

		public static final String OSI_TCP = "OSI ([A-Z]{2}|[A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|/[A-Z]{3}) TCP([0-9]{1,2}) (([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+"; 

		public static final String OTHER_SERVICE_INFO_BEGIN = "OSI ([A-Z]{2}|[A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{4}( ))?(.*)"; 
		
		public static final String OTHER_SERVICE_INFO_CONT = "OSI ([A-Z]{2}|[A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|/[A-Z]{3})( )?(///) ([A-Z]{4})?(.*)";

		public static final String SSR_DOCS1 = "SSR DOCS ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([0-9]{1,2})?(/)?([A-Z]{1,2})?(/)?([A-Z]{1,3})?(/)?([A-Z0-9]{1,15})?(/)?([A-Z]{1,3})?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?(F|M|FI|MI|U)?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?([A-Z]{2,30})?(/)?([A-Z ]{2,30})?(/)?([A-Z ]{2,30})?(/)?(H)?(-(.*))?";

		public static final String SSR_DOCS2 = "SSR DOCS ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([0-9]{1,2})? ([A-Z]{6}( )?[0-9]{4}[A-Z][0-9]{2}[A-Z]{3})(/)?([A-Z]{1,2})?(/)?([A-Z]{1,3})?(/)?([A-Z0-9]{7,15})?(/)?([A-Z]{1,3})?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?(F|M|FI|MI|U)?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?([A-Z]{2,30})?(/)?([A-Z ]{2,30})?(/)?([A-Z ]{2,30})?(/)?(H)?(-(.*))?";

		public static final String SSR_DOCS_CONT = "SSR DOCS ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3})( )?(///)(.*)?";

		public static final String SSR_CC_VALUE = "(([A-Z]{2})([0-9]+)EXP) (([0-9]{2}) ([0-9]{2}))";
		
		public static final String SSR_SEATS ="SSR SEAT ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}) ((NN|IN|XX|IX|HX|KK|LK|UC|UN|NO|HS|OX))([1-9]{1,2})+( )?([A-Z0-9]{15,16})((-)?(.*))?(\\.([A-Z0-9 ]*))?";

		public static final String SSR_DOCO1 = "SSR DOCO ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([0-9]{1,2})?(/)?([A-Z]{1,35})?(/)?([A-Z])?(/)?([A-Z0-9]{1,25})?(/)?([A-Z]{1,35})?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?([A-Z]{1,3})?(/)?(I)?(-(.*))?";
		
		public static final String SSR_DOCO_CONT = "SSR DOCO ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3})( )?(///)(.*)?";
		// public static final String SSR_DOCS2_CONT =
		// "SSR DOCS ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}|/[A-Z]{3}) ([A-Z]{2})([0-9]{1,2})? ([A-Z]{6}[0-9]{4}[A-Z][0-9]{2}[A-Z]{3})(/)?([A-Z]{1,2})?(/)?([A-Z]{1,3})?(/)?([A-Z0-9]{7,15})?(/)?([A-Z]{1,3})?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?(F|M|FI|MI|U)?(/)?([0-9]{2}[A-Z]{3}[0-9]{2})?(/)?([A-Z]{2,30})?(/)?([A-Z]{2,30})?(/)?([A-Z]{2,30})?(/)?(H)?(-)?(.*)?";
				
		public static final String MESSAGE_REFERENCE = "(([0-9]{2}[A-Za-z]{3}[0-9]{5}[A-Za-z]{1}[0-9]{3})([0-9A-Za-z -\\/.])*)";

	}

	// TODO check the seat message with saber weather actioncodeandnofseats comesimmidiatebeforesegment without space ?
	// if come with soace then remove this.
	// public static final String SSR_SEATS =
	// "SSR SEAT ([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1}) ((NN|IN|XX|IX|HX|KK|LK|UC|UN|NO|HS|OX))([1-9]{1,2})+( )?([A-Z0-9]{15,16})((-)?(.*))?(\\.([A-Z0-9 ]*))?";

	/** Record Status */
	public static interface ElementSequenceRegularExpressions {

		public static final String BOOKING_MESSAGE = "(REC\\|SEN\\|)?(BPR\\|)?RLO\\|(NAM\\|)+((MAB\\|){0,1}(SEG\\|)|(MAB\\|){0,1}(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_DOCO\\|)*(SS_DOCO_CNT\\|)*(SS_LANG\\|)*(SS_CHLD\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String AMEND_CANCEL_MESSAGE = "(REC\\|SEN\\|)?(BPR\\|)?RLO\\|RLO\\|+(NAM\\|)*((MAB\\|){0,1}(SEG\\|)|(MAB\\|){0,1}(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SR_LANG\\|)*(SS_CHLD\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String DIVIDE_MESSAGE = "(REC\\|SEN\\|)?DVD\\|(RLO\\|){1,2}(NAM\\|)*((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String RLR_MESSAGE = "(REC\\|SEN\\|)?RLR\\|RLO\\|+(NAM\\|)*((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SSR_CHLD\\|)*(SR_LANG\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";
		
		public static final String PDM_MESSAGE = "(REC\\|SEN\\|)?PDM\\|RLO\\|+(NAM\\|)*((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SSR_CHLD\\|)*(SR_LANG\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String NCO_MESSAGE = "REC\\|SEN\\|NCO\\|RLO\\|+(NAM\\|)*((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SSR_CHLD\\|)*(SR_LANG\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String SCHEDULE_CHANGE_MESSAGE = "(REC\\|SEN\\|)?ASC\\|(RLO\\|){1,2}(NAM\\|)+((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String CHNM_MESSAGE = "(REC\\|SEN\\|)?(RLO\\|){1,2}(NAM\\|)+MAB\\|(NAM\\|)+((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String NAR_MESSAGE = "REC\\|SEN\\|NAR\\|RLO\\|(NAM\\|)+((MAB\\|){0,1}(SEG\\|)|(MAB\\|){0,1}(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

		public static final String DIVIDE_MESSAGE1 = "(REC\\|SEN\\|)?DVD\\|(RLO\\|){1,2}(NAM\\|)(MAB\\|)(NAM\\|)((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";
		
		public static final String TRL_MESSAGE = "(REC\\|SEN\\|)?TRL\\|(RLO\\|){1,2}(NAM\\|)*((SEG\\|)|(CS_SEG\\|))+((SS_PST1\\|)*(SS_PST2\\|)*(SS_FOID1\\|)*(SS_FOID2\\|)*(SS_PCT\\|)*(SS_DS1\\|)*(SS_DS2\\|)*(SS_DS_CNT\\|)*(SS_DOCO\\|)*(SS_DOCO_CNT\\|)*(SS_CHLD\\|)*(SR_LANG\\|)*(SS_OTHS\\|)*(SS1\\|)*(SS2\\|)*)*(OS1\\|)*(OS2\\|)*";

	}

	/** Denotes the System Parameter Keys */
	public static interface MessageTypes {

		// primary message identifiers
		public static final String BOOKING = "BKG"; //Create reservation
		public static final String AMEND = "AMD";   //Modify reservation
		public static final String DIVIDE = "DVD";  //Split reservation
		public static final String RECORD_LOACTOR_REQUEST = "RLR"; //Load reservation
		public static final String CAN = "CAN"; // Cancel reservation
		public static final String STR = "STR"; // Status reply - extend ohd
		public static final String CHNM = "CHNT"; // Name changes 
		public static final String AVAILABILITY_STATUS_NONNUMERIC = "AVS";
		public static final String AVAILABILITY_STATUS_UMERIC = "AVN";
		public static final String NCO = "NCO";
		public static final String SCHEDULE_CHANGE = "ASC";
		public static final String NAR = "NAR";
		public static final String PDM = "PDM";

		// Response message identifiers
		public static final String DIVIDE_RESP = "DVR";
		public static final String TRL_RESP = "TRL";
		public static final String RESERVATION_RESP = "RRP";
		
	}

	/** Denotes the device types */
	public interface GDSMessageElements {

		public static final String RECIPIENT_ADDRESS = "REC";

		public static final String SENDER_ADDRESS = "SEN";

		public static final String RECORD_LOCATOR = "RLO";

		public static final String MESSAGE_IDENTIFIER = "MID";

		public static final String MISCELLANEOUS_ABBREVIATION = "MAB";

		public static final String NAME = "NAM";

		public static final String SEGMENT = "SEG";
		
		public static final String CS_SEGMENT = "CS_SEG";

		public static final String SSR_BEGIN = "SS1";

		public static final String SSR_CONT = "SS2";

		public static final String SSR_CHLD = "SS_CHLD";

		/** SSR passport information */
		public static final String SSR_PST1 = "SS_PST1";

		/** SSR passport continuation information */
		public static final String SSR_PST2 = "SS_PST2";

		/** Passenger form of identification information - SSR FOID */
		public static final String SSR_FID1 = "SS_FOID1";

		/** Passenger form of identification continuation information - SSR FOID */
		public static final String SSR_FID2 = "SS_FOID2";

		/** SSR emergency contact no provide by the passenger */
		public static final String SSR_PCT = "SS_PCT";

		/** Passenger Travel Document information - SSR DOCS */
		public static final String SSR_DCS1 = "SS_DS1";

		/** Passenger Travel Document continuation information - SSR DOCS */
		public static final String SSR_DCS2 = "SS_DS2";

		/** Passenger Travel Document continuation information - SSR DOCS */
		public static final String SSR_DCS_CONT = "SS_DS_CNT";

		public static final String SSR_OTHER = "SS_OTHS";

		public static final String SSR_LANG = "SS_LANG";

		public static final String SSR_SEAT = "SS_SEAT";

		public static final String OTHER_SERVICE_PARTY_INFO = "OSTCP";

		public static final String OTHER_SERVICE_INFO_BEGIN = "OS1";

		public static final String OTHER_SERVICE_INFO_CONT = "OS2";
		
		/** Passenger Travel Document information - SSR DOCO */
		public static final String SSR_DOCO = "SS_DOCO";
		/** Passenger Travel Document continuation information - SSR DOCO */
		public static final String SSR_DOCO_CONT = "SS_DOCO_CNT";
		
		public static final String MESSAGE_REFERENCE = "MSG_REF";
	}

	/**
	 * Subelement types
	 * 
	 */
	public static interface SubElementTypes {

		public static final String SSR_SEGMENT = "SSR_SEG";

	}

	/**
	 * Subelement Regular expression.
	 * 
	 * @author sanjeewaf
	 */
	public static interface SubElementRegularExpressions {

		public static final String SSR_SEGMENT_ELEMENT = "([A-Z]{3})([A-Z]{3})([ ]?)([0-9]{3,4})([A-Z]{1})([0-9]{2}[A-Z]{3})";
		
		public static final String OSI_CONTACT_DETAILS = "([A-Z]{3})?( )?([A-Z]*)( )?([0-9 -]*[X]?[0-9]*)([ ])?([A-Z ]+/?[A-Z ]*)?(.*)?";
		
		public static final String OSI_CONTACT_DETAILS_EMAIL = "([A-Z]{3})?( )?([A-Z0-9.(..)%-]+//[A-Z0-9.-]+.[A-Z]{2,4})";

		public static final String SSR_TICKETING_TIME_LIMIT = "([A-Z]{2})\\/([A-Z]{3}) ([0-9]{4})\\/([0-9]{2}[A-Z]{3})";
	}

}
