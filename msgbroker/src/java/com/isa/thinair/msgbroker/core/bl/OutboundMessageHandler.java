package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.service.MessagingAuthenticator;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.ServiceUtil;

public class OutboundMessageHandler extends GdsTypeBMessageGateway {

	private final Log log = LogFactory.getLog(OutboundMessageHandler.class);

	public static boolean START_STOP_FLAG = false;

	private static final String CONTENT_TYPE = "text/plain";
	private static final String DEFAULT_MAILSERVER_ID = "D";
	private static final String SECONDARY_MAILSERVER_ID = "S";
	private static final String MAIL_PROTOCOL = "smtp";
	private final String MESSAGE_ID = "X-SITA-Message-ID";

	private static String SMTP_PROTOCOL = "mail.transport.protocol";

	private static String SMTP_HOST = "mail.smtp.host";

	private static String SMTP_PARTIAL_SENDING = "mail.smtp.sendpartial";

	private static String SMTP_AUTHENITCATE = "mail.smtp.auth";

	private static String SMTP_PORT = "mail.smtp.port";
	
	private static String  SMTPS_PROTOCOL ="smtps";

	// MailServerConfig defaultMailServerConfig;
	//
	// MailServerConfig secondaryMailServerConfig;

	Map airlines = new HashMap();

	private AdminDAO adminDAO;

	private MessageDAO messageDAO;

	OutMessage outMessage = null;

	Map requestTypes;

	Object[] arrAirlineDet;

	String toAddress;

	Message emailMessage;

	ServiceBearer airline;

	OutMessageProcessingStatus outMessageProcessingStatus;

	InternetAddress[] addresses = new InternetAddress[1];

	Session defaultMailSession = null;

	Session secondaryMailSession = null;

	// String currentMailServerID;

	InternetAddress internetAddress;

	ServiceUtil serviceUtil;

	int numberOfMailConnectTries = 0;

	int i = 0;

	public OutboundMessageHandler() {
		// defaultMailServerConfig = (MailServerConfig) LookupUtil.lookupMessagingConfig().
		// getMailServersConfigurationMap().get(
		// MessagingConstants.MailServerConfigurationKeys.
		// MAIL_SERVER_CONFIG_GDS_DEFAULT);
		// secondaryMailServerConfig = (MailServerConfig) LookupUtil.lookupMessagingConfig().
		// getMailServersConfigurationMap().get(
		// MessagingConstants.MailServerConfigurationKeys.
		// MAIL_SERVER_CONFIG_GDS_SECONDARY);
	}

	public void execute(List<String> messageTypes) {
		log.info("OutboundMessageHandler Started .........");
		List<ServiceBearer> airlineDomainObjects;
		// createMailSessions(DEFAULT_MAILSERVER_ID);
		// createMailSessions(SECONDARY_MAILSERVER_ID);
		serviceUtil = new ServiceUtil();
		// while(!START_STOP_FLAG) {
		try {
			// mailServerConfig = (MailServerConfig) LookupUtil.lookupMessagingConfig().
			// getMailServersConfigurationMap().get(
			// MessagingConstants.MailServerConfigurationKeys.
			// MAIL_SERVER_CONFIG_GDS);

			requestTypes = getMessageDAO().getActiveMessageTypeTimeToLives();

			// while(!START_STOP_FLAG) {
			try {
				// currentMailServerID = DEFAULT_MAILSERVER_ID;
				/*
				 * Get airlines that contains airline_gds_mail records.
				 */
				airlineDomainObjects = getAdminDAO().getServiceBearerAirlines();

				/*
				 * Put found active airlines to airlines array contains following format {ServiceBearer Domain Rec,
				 * session}
				 */
				for (Iterator<ServiceBearer> iterAirlineDomainObjs = airlineDomainObjects.iterator(); iterAirlineDomainObjs
						.hasNext();) {
					ServiceBearer airline = iterAirlineDomainObjs.next();
					if (airline.getRecordStatus()) {
						if (!airlines.containsKey(airline.getGdsCode())) {
							Object[] arrAirlineDet1 = { airline, null };
							airlines.put(airline.getGdsCode(), arrAirlineDet1);
							createEmailServerConnection(airline.getGdsCode());
						} else if (((ServiceBearer) ((Object[]) airlines.get(airline.getGdsCode()))[0]).getVersion() != airline
								.getVersion()) {
							((Object[]) airlines.get(airline.getGdsCode()))[0] = null;
							((Object[]) airlines.get(airline.getGdsCode()))[1] = null;
							Object[] arrAirlineDet1 = { airline, null };
							airlines.put(airline.getGdsCode(), arrAirlineDet1);
							createEmailServerConnection(airline.getGdsCode());
						}
					} else if (airlines.containsKey(airline.getGdsCode())) {
						airlines.remove(airline.getGdsCode());
					}
				}
				send(messageTypes);
				// Thread.sleep(
				// Long.parseLong(LookupUtil.getGlobalConfig().getSysParam(
				// Constants.SystemParamKeys.OUTGOING_MESSAGE_HANDLING_DELAY_IN_SECONDS)) * 1000);
				// Thread.sleep(Integer.parseInt("6000"));
			} catch (Exception ex) {
				log.error("OutboundMessageHandler()" + ex.getMessage(), ex);
			}
			// }
		} catch (Exception ex) {
			log.error("OutboundMessageHandler()" + ex.getMessage(), ex);
		}
		// }

		//
		// while(!START_STOP_FLAG) {
		// getMail();
		// try {
		// Thread.sleep(Integer.parseInt(mailServerConfig.getPollingDelay()));
		// } catch (Exception ex) {
		// log.error("POP3EMailRetriever.run()" + ex.getMessage());
		// }
		// }
	}

	public void receiveMessages(boolean useGDSConfiguration,Collection<Integer> unprocessedMsgIds) {
		// Dummy Method
	}

	public void sendMessages(List<String> messageTypes, boolean useGDSConfiguration) {

		log.info("OutboundMessageHandler : sending typeB messages emails started");
		Object[] arrAirlineDet;
		if (serviceUtil == null) {
			serviceUtil = new ServiceUtil();
		}

		for (ServiceBearer serviceBearer : applicableServiceBearers) {
			arrAirlineDet = new Object[]{ serviceBearer, null};
			airlines.put(serviceBearer.getGdsCode(), arrAirlineDet);
			createEmailServerConnection(serviceBearer.getGdsCode());
		}

		send(messageTypes);
	}

	private boolean checkEmailServerConnection(String gdsCode) {
		// {ServiceBearer Domain Rec, session}
		Object[] arrAirlineDet = (Object[]) airlines.get(gdsCode);
		if (arrAirlineDet == null || arrAirlineDet.length == 0) {
			return false;
		}
		// Session session = (Session) arrAirlineDet[1];
		if ((Session) arrAirlineDet[1] == null) {
			// Create session
			((Object[]) airlines.get(gdsCode))[1] = createSession(arrAirlineDet);
		}
		return true;
	}

	private Session createSession(Object[] arrAirlineDet) {

		airline = (ServiceBearer) arrAirlineDet[0];
		boolean isSecureEnabled = false;
		Properties props = new Properties();
		props.put(SMTP_PROTOCOL, "smtp");
		props.put(SMTP_HOST, airline.getOutgoingEmailServerIP());
		if (airline.getOutgoingEmailSvrListeningPort() != null) {
			props.put(SMTP_PORT, airline.getOutgoingEmailSvrListeningPort().toString());
		}

		if (SMTPS_PROTOCOL.equalsIgnoreCase(airline.getOutgoingMailAccessProtocol())) {
			isSecureEnabled = true;
			props.put("mail.smtp.socketFactory.port", airline.getOutgoingEmailSvrListeningPort().toString());
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.debug", "true");
		}
		props.put(SMTP_PARTIAL_SENDING, "true");

		Authenticator authenticator = null;
		if (airline.getOutgoingEmailSvrUserID() != null && !("").equals(airline.getOutgoingEmailSvrUserID())
				&& airline.getOutgoingEmailSvrUserPassword() != null && !("").equals(airline.getOutgoingEmailSvrUserPassword())) {
			props.put(SMTP_AUTHENITCATE, "true");
			authenticator = new MessagingAuthenticator(airline.getOutgoingEmailSvrUserID(),
					airline.getOutgoingEmailSvrUserPassword());
		}

		try {
			if(isSecureEnabled){
				defaultMailSession = Session.getInstance(props, authenticator);	
			}else{
				defaultMailSession = Session.getDefaultInstance(props, authenticator);	
			}			
		} catch (Exception ex) {
			defaultMailSession = Session.getInstance(props, authenticator);
		}
		return defaultMailSession;
	}

	// private void createMailSessions(String mailServerID){
	// // Properties props = System.getProperties();
	// // return Session.getInstance(props, null);
	// MailServerConfig mailServerConfig;
	//
	// if (mailServerID.equals(DEFAULT_MAILSERVER_ID)) {
	// mailServerConfig = defaultMailServerConfig;
	// }else {
	// mailServerConfig = secondaryMailServerConfig;
	// }
	//
	// Properties props = new Properties();
	// props.put(SMTP_PROTOCOL, mailServerConfig.getProvider());
	// props.put(SMTP_HOST, mailServerConfig.getHostAddress());
	// props.put(SMTP_PARTIAL_SENDING, "true");
	//
	// Authenticator authenticator = null;
	// if (!("").equals(mailServerConfig.getUserName())
	// && !("").equals(mailServerConfig.getPassword())) {
	// props.put(SMTP_AUTHENITCATE, "true");
	// authenticator = new MessagingAuthenticator(mailServerConfig
	// .getUserName(), mailServerConfig.getPassword());
	// }
	//
	// if (mailServerID.equals(DEFAULT_MAILSERVER_ID)) {
	// defaultMailSession = Session.getDefaultInstance(props,
	// authenticator);
	// }else{
	// secondaryMailSession = Session.getDefaultInstance(props,
	// authenticator);
	// }
	// }

	private boolean createEmailServerConnection(String gdsCode) {
		// {ServiceBearer Domain Rec, session}
		Object[] arrAirlineDet = (Object[]) airlines.get(gdsCode);
		if (arrAirlineDet == null || arrAirlineDet.length == 0) {
			return false;
		}
		((Object[]) airlines.get(gdsCode))[1] = null;
		return checkEmailServerConnection(gdsCode);
	}

	private void recordFailure(OutMessage outMessage, String messageStatus, String errorCode) {
		if (outMessage.getLastErrorMessageCode() == null || !outMessage.getLastErrorMessageCode().equals(errorCode)) {
			outMessageProcessingStatus = new OutMessageProcessingStatus();
			outMessageProcessingStatus.setErrorMessageCode(errorCode);
			outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
			outMessageProcessingStatus.setProcessingDate(new Date());
			outMessageProcessingStatus.setProcessingStatus(messageStatus);
			getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
			outMessage.setLastErrorMessageCode(errorCode);
		}
		outMessage.setStatusIndicator(messageStatus);
		outMessage.setProcessedDate(new Date());
		getMessageDAO().saveOutMessage(outMessage);
	}

	private void send(List<String> messageTypes) {

		/*
		 * Get out messages of type Untransmitted ('N')
		 */
		List<String> gdsCodes = new ArrayList<String>();
		Set<String> extRecipientAddresses = new HashSet<String>();

		for (ServiceBearer serviceBearer : applicableServiceBearers) {
			gdsCodes.add(serviceBearer.getGdsCode());
		}

		List outMessages = (List) getMessageDAO().getOutMessagesInFifoOrderForGds(
				MessageProcessingConstants.OutMessageProcessStatus.Untransmitted, messageTypes, gdsCodes);
		/*
		 * For each message, Get the relevant in message If not timed out, Get relevant mail box and send. -If message
		 * is for an inactive airline, update process status to 'F' (Transmission Failure) and record error. if failed,
		 * update process status to 'F' (Transmission Failure) and record error. if not failed, update process status to
		 * 'S' (MailServerAccepted / Sent). else update process status to 'D' (Timed out / Delayed) and record error to
		 * mark as Timed Out.
		 */
		// For each message,
		for (Iterator iterOutMessages = outMessages.iterator(); iterOutMessages.hasNext();) {
			outMessage = (OutMessage) iterOutMessages.next();
			// Get the relevant in message

			// If not timed out,
			if (!serviceUtil.isMessageTimedOut(outMessage) && !StringUtil.isNullOrEmpty(outMessage.getOutMessageText())) {
				// Get relevant mail box and send.

				// If message is for an inactive airline,
				// update process status to 'F' (Transmission Failure) and record error.
				// Note: arrAirlineDet format :- {ServiceBearer Domain Rec, session}
				boolean sendToExternalSystem = outMessage.isNonGDSMessage();
				ServiceBearer serviceBearer = null;
				toAddress = outMessage.getRecipientEmailAddress();
				if (!sendToExternalSystem) {
					if (outMessage.getRecipientEmailAddress() == null) {
						recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
								"outmessagesending.no.recipient.emailaddress");
						continue;
					}
					serviceBearer =  getAdminDAO().getServiceBearerAirlineForTTYAddress(outMessage.getSentTTYAddress());
					if (serviceBearer == null) {
						serviceBearer =  getAdminDAO().getServiceBearerAirlineForEmailAddress(outMessage.getSentEmailAddress());
					}
					arrAirlineDet = (Object[]) airlines.get(serviceBearer.getGdsCode());
					airline = (ServiceBearer) arrAirlineDet[0];
					try {
						addresses[0] = new InternetAddress(toAddress);
						internetAddress = new InternetAddress(airline.getEmailAddress());
					} catch (AddressException e) {
						// if failed, update process status to 'F' (Transmission Failure) and record error.
						recordAddressFailure(e);
					}
					if (arrAirlineDet == null) {
						recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
								"outmessagesending.inactive.airline");
						continue;
					}
					emailMessage = new MimeMessage(defaultMailSession);
				} else {
					if (outMessage.getRecipientEmailAddress() == null) {
						if (outMessage.getRecipientTTYAddress() == null) {
							recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
									"outmessagesending.no.recipient.emailaddress");
						}
						continue;
					}
					try {
						addresses[0] = new InternetAddress(toAddress);
						internetAddress = new InternetAddress(outMessage.getSentEmailAddress());
					} catch (AddressException e) {
						recordAddressFailure(e);;
					}
					emailMessage = new MimeMessage(getDefaultMailSessionForExternalSSMSending());
					extRecipientAddresses.add(outMessage.getRecipientEmailAddress());
				}	
				
				numberOfMailConnectTries = 0;
				while (numberOfMailConnectTries < 5) {
					try {
						numberOfMailConnectTries++;
						// Message emailMessage = new MimeMessage((Session) arrAirlineDet[1] );
						// if (currentMailServerID.equals(DEFAULT_MAILSERVER_ID)) {
						// emailMessage = new MimeMessage(defaultMailSession);
						// }else{
						// emailMessage = new MimeMessage(secondaryMailSession);
						// }
						if (outMessage.getGdsMessageID() != null && outMessage.getGdsMessageID() != "") {
							emailMessage.setHeader(MESSAGE_ID, outMessage.getGdsMessageID());
						}
						emailMessage.setFrom(internetAddress);
						emailMessage.setRecipients(javax.mail.Message.RecipientType.TO, addresses);
						emailMessage.setSubject("Message Reference: " + outMessage.getOutMessageID().toString());
						emailMessage.setContent(outMessage.getOutMessageText(), CONTENT_TYPE);
						emailMessage.saveChanges();
						log.info("Sending message " + outMessage.getOutMessageText());
						Transport.send(emailMessage);
						numberOfMailConnectTries = 0;
						break;
					} catch (MessagingException e) {
						log.error("Sending email failed for " + airline.getOutgoingEmailServerIP() + " mailserver. "
								+ "\ntoAddress(es)=" + toAddress + "\nSubject=" + "Message Reference: "
								+ outMessage.getOutMessageID().toString() + "\nContent=" + outMessage.getOutMessageText(), e);
						if (numberOfMailConnectTries < 5) {
							if (serviceBearer != null) {
								createEmailServerConnection(serviceBearer.getGdsCode());
							} 
							continue;
						} else {
							recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
									"outmessagesending.send.failed.messagingexception");
						}
						// if (numberOfMailConnectTries > 4 ){
						// log.error("Sending email failed for " +
						// (currentMailServerID.equals(DEFAULT_MAILSERVER_ID)?"default": "secondary") +
						// " mailserver. "
						// + "\ntoAddress(es)="
						// + toAddress + "\nSubject="
						// + "Message Reference: " + outMessage.getOutMessageID().toString() + "\nContent="
						// + outMessage.getOutMessageText(), e);
						// if (currentMailServerID.equals(DEFAULT_MAILSERVER_ID)) {
						// currentMailServerID = SECONDARY_MAILSERVER_ID;
						// numberOfMailConnectTries = 0;
						// continue;
						// }else {
						// currentMailServerID = DEFAULT_MAILSERVER_ID;
						// // if failed, update process status to 'F' (Transmission Failure) and record error.
						// recordFailure(outMessage,
						// MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
						// "outmessagesending.send.failed.messagingexception");
						// }
						// }
						// createMailSessions(currentMailServerID);
					}
				}
				if (numberOfMailConnectTries == 0) {
					// if not failed, update process status to 'S' (MailServerAccepted / Sent).
					try {
						outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.MailServerAccepted);
						outMessage.setProcessedDate(new Date());
						outMessage.setSentTTYAddress(airline.getSitaAddress());
						outMessage.setSentEmailAddress(airline.getEmailAddress());
						getMessageDAO().saveOutMessage(outMessage);

						outMessageProcessingStatus = new OutMessageProcessingStatus();
						outMessageProcessingStatus
								.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.MESSAGE_ACCEPTEDBYMAILSERVER);
						outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
						outMessageProcessingStatus.setProcessingDate(new Date());
						outMessageProcessingStatus
								.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.MailServerAccepted);
						getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
					} catch (Exception e) {
						// if failed, update process status to 'F' (Transmission Failure) and record error.
						log.error("Email message failed", e);
						recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
								"outmessagesending.send.failed.exception");
						// createEmailServerConnection(airline.getAirlineCode());
					}
				}
			} else {
				// else update process status to 'D' (Timed out / Delayed) and record error to mark as Timed Out.
				recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TimedOut,
						"outmessagesending.request.timedout");
			}
		}
	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * return the message DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}
	
	private Session getDefaultMailSessionForExternalSSMSending() {
		
		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
		Properties props = new Properties();
		props.put(SMTP_PROTOCOL, MAIL_PROTOCOL);
		props.put(SMTP_HOST,airReservationConfig.getDefaultMailServer());
		props.put(SMTP_PARTIAL_SENDING, "true");
		Authenticator authenticator = null;
		if (!("").equals(airReservationConfig.getDefaultMailServerUsername()) && !("").equals(airReservationConfig.getDefaultMailServerPassword())) {
			props.put(SMTP_AUTHENITCATE, "true");
			authenticator = new MessagingAuthenticator(airReservationConfig.getDefaultMailServerUsername(),
					airReservationConfig.getDefaultMailServerPassword());
		}
		defaultMailSession = Session.getInstance(props, authenticator);
		return defaultMailSession;

	}
	
	private void recordAddressFailure(AddressException e) {
		log.error("Sending email failed." + "\ntoAddress(es)=" + toAddress + "\nSubject=" + "Message Reference: "
				+ outMessage.getOutMessageID().toString() + "\nContent=" + outMessage.getOutMessageText(), e);
		recordFailure(outMessage, MessageProcessingConstants.OutMessageProcessStatus.TransmissionFailure,
				"outmessagesending.send.failed.addressexception");
	}

}
