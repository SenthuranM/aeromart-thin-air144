package com.isa.thinair.msgbroker.core.bl;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.MessageRequestProcessingUtil;

public class GDSEMailRetriever extends GdsTypeBMessageGateway {

	private final Log log = LogFactory.getLog(GDSEMailRetriever.class);

	public static boolean START_STOP_FLAG = false;

	private static final String CONTENT_TYPE = "text/plain";
	
	private final String MESSAGE_ID = "X-SITA-Message-ID";

	String undeliveredSubject = "Undeliverable: Message Reference: ";

	Map<String, Object> airlines = new HashMap<String, Object>();

	// MailServerConfig mailServerConfig;

	private AdminDAO adminDAO;

	private MessageDAO messageDAO;

	// Store store = null;

	// Folder folderInbox = null;

	InMessage inMessage = null;

	int i = 0;

	Folder folderInbox = null;

	Folder folder;

	InMessageProcessingStatus inMessagePassingErrors;

	boolean hasNewMessages = false;

	public GDSEMailRetriever() {

	}

	public void execute() {
		log.debug("GDSEMailRetriever started....");
		Object[] arrAirlineDet;
		List<ServiceBearer> airlineDomainObjects;
		/*
		 * undeliveredSubject = LookupUtil.getGlobalConfig().getSysParam(
		 * Constants.SystemParamKeys.UNDELIVERED_EMAIL_MESSAGE_SUBJECT_PREFIX);
		 */
		try {
			/*
			 * Get airlines that contains airline_gds_mail records.
			 */
			airlineDomainObjects = getAdminDAO().getServiceBearerAirlines();

			/*
			 * Put found active airlines to airlines array contains following format {ServiceBearer Domain Rec, session,
			 * inbox folder, Try to connect, store}
			 */
			for (Iterator<ServiceBearer> iterAirlineDomainObjs = airlineDomainObjects.iterator(); iterAirlineDomainObjs.hasNext();) {
				ServiceBearer airline = (ServiceBearer) iterAirlineDomainObjs.next();
				if (airline.getRecordStatus()) {
					if (!airlines.containsKey(airline.getGdsCode())) {
						Object[] arrAirlineDet1 = { airline, null, null, new Boolean(true), null };
						airlines.put(airline.getGdsCode(), arrAirlineDet1);
					} else if (((ServiceBearer) ((Object[]) airlines.get(airline.getGdsCode()))[0]).getVersion() != airline
							.getVersion()) {
						closeMailbox((Object[]) airlines.get(airline.getGdsCode()));
						Object[] arrAirlineDet1 = { airline, null, null, new Boolean(true), null };
						airlines.put(airline.getGdsCode(), arrAirlineDet1);
					}
				} else if (airlines.containsKey(airline.getGdsCode())) {
					airlines.remove(airline.getGdsCode());
				}
			}
			getMail();

			if (hasNewMessages) {
				InboundMessageHandler inboundMessageHandler = new InboundMessageHandler();
				inboundMessageHandler.execute(null);
			}

		} catch (Exception ex) {
			log.error("GDSEMailRetriever.run()", ex);
		}

		// Close opened mail boxes
		for (Iterator<String> iterAirlineCodes = airlines.keySet().iterator(); iterAirlineCodes.hasNext();) {
			arrAirlineDet = (Object[]) airlines.get((String) iterAirlineCodes.next());
			if (arrAirlineDet != null && arrAirlineDet.length != 0) {
				closeMailbox(arrAirlineDet);
			}
		}
	}

	public void sendMessages(List<String> messageTypes, boolean isUsedGDSConfiguration) {
		// Dummy method
	}

	public void receiveMessages(boolean useGDSConfiguration, Collection<Integer> unprocessedMsgIds) {

		Object[] arrAirlineDet;

		for (ServiceBearer serviceBearer : applicableServiceBearers) {
			arrAirlineDet = new Object[]{ serviceBearer, null, null, new Boolean(true), null };
			airlines.put(serviceBearer.getGdsCode(), arrAirlineDet);
		}

		getMail();

		if (hasNewMessages) {
			InboundMessageHandler inboundMessageHandler = new InboundMessageHandler();
			inboundMessageHandler.execute(unprocessedMsgIds);
		}

		for (String iterAirlineCode : airlines.keySet()) {
			arrAirlineDet = (Object[]) airlines.get(iterAirlineCode);
			if (arrAirlineDet != null && arrAirlineDet.length != 0) {
				closeMailbox(arrAirlineDet);
			}
		}

	}

	public ServiceBearer.MessagingMode getMessagingMode() {
		return ServiceBearer.MessagingMode.MAIL;
	}

	private boolean checkEmailServerConnection(String gdsCode) {
		// {ServiceBearer Domain Rec, session, inbox folder, Try to connect, store}
		Object[] arrAirlineDet = (Object[]) airlines.get(gdsCode);
		if (arrAirlineDet == null || arrAirlineDet.length == 0) {
			return false;
		}
		// Session session = (Session) arrAirlineDet[1];
		if ((Session) arrAirlineDet[1] == null) {
			// Create session
			((Object[]) airlines.get(gdsCode))[1] = createSession();
		}
		// Folder inboxFolder = (Folder) arrAirlineDet[2];
		if ((Folder) arrAirlineDet[2] == null) {
			// Create session
			return createStore(gdsCode, (Session) ((Object[]) airlines.get(gdsCode))[1]);
			// readInboxFolder(airlineCode);
		}
		return true;
	}

	private boolean readInboxFolder(String gdsCode) {
		folderInbox = null;

		try {
			folder = ((Store) ((Object[]) airlines.get(gdsCode))[4]).getDefaultFolder();
			if (folder == null) {
				log.error("No default folder");
				return false;
			}
			folderInbox = (Folder) ((Object[]) airlines.get(gdsCode))[2];
			if (folderInbox != null && folderInbox.isOpen()) {
				try {
					folderInbox.close(true);
				} catch (Exception e) {
					log.error(e);
				}
				folderInbox = null;
			}
			folderInbox = folder.getFolder("INBOX");
			if (folderInbox == null) {
				log.error("Folder INBOX not found");
				return false;
			}
			folderInbox.open(Folder.READ_WRITE);
			((Object[]) airlines.get(gdsCode))[2] = folderInbox;
		} catch (NoSuchProviderException ex) {
			log.error("GDSEMailRetriever.getMail() ", ex);
			return false;
		} catch (MessagingException ex) {
			log.error("GDSEMailRetriever.getMail() ", ex);
			return false;
		}
		return true;
	}

	private boolean createStore(String gdsCode, Session session) {
		// {ServiceBearer Domain Rec, session, inbox folder, Try to connect, store}
		// Folder folderInbox = null;
		ServiceBearer airline = null;
		String mailAccessProtocol = getMailAcceProtocolForGDS(gdsCode);
		if (mailAccessProtocol == null) {
			log.error("Mail access protocol has not been defined for the GDS: " + gdsCode);
			return false;
		}
		try {
			Store store;
			store = session.getStore(mailAccessProtocol);
			//store = session.getStore("pop3");
			airline = (ServiceBearer) ((Object[]) airlines.get(gdsCode))[0];
			// store.connect(mailServerConfig.getHostAddress(),
			// mailServerConfig.getUserName(), mailServerConfig.getPassword());
			if (airline.getIncommingMailSvrListeningPort() == null) {
				store.connect(airline.getIncommingEmailServerIP(), airline.getIncommingEmailSvrUserID(),
						airline.getIncommingEmailSvrUserPassword());
			} else {
				store.connect(airline.getIncommingEmailServerIP(), airline.getIncommingMailSvrListeningPort().intValue(),
						airline.getIncommingEmailSvrUserID(), airline.getIncommingEmailSvrUserPassword());
			}
			((Object[]) airlines.get(gdsCode))[4] = store;
		} catch (NoSuchProviderException ex) {
			if (airline != null) {
				log.error("No provider :-  Server IP: " + airline.getIncommingEmailServerIP() + " UserID : "
						+ airline.getIncommingEmailSvrUserID());
			}
			log.error("GDSEMailRetriever.getMail() ", ex);
			return false;
		} catch (MessagingException ex) {
			if (airline != null) {
				log.error("Messaging Exception :-  Server IP: " + airline.getIncommingEmailServerIP() + " UserID : "
						+ airline.getIncommingEmailSvrUserID());
			}
			log.error("GDSEMailRetriever.getMail() " + ex.getMessage(), ex);
			return false;
		}
		return true;
	}

	private Session createSession() {
		Properties props = System.getProperties();
		return Session.getInstance(props, null);
	}

	private void closeMailbox(Object[] airlineDet) {
		// {ServiceBearer Domain Rec, session, inbox folder, Try to connect, store}
		if (airlineDet[2] != null && ((Folder) airlineDet[2]).isOpen()) {
			try {
				((Folder) airlineDet[2]).close(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		airlineDet[2] = null;

		if (airlineDet[4] != null) {
			try {
				((Store) airlineDet[4]).close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		airlineDet[4] = null;
	}

	private boolean createEmailServerConnection(String gdsCode) {
		// {ServiceBearer Domain Rec, session, inbox folder, Try to connect, store}
		Object[] arrAirlineDet = (Object[]) airlines.get(gdsCode);
		if (arrAirlineDet == null || arrAirlineDet.length == 0) {
			return false;
		}
		closeMailbox((Object[]) airlines.get(gdsCode));
		((Object[]) airlines.get(gdsCode))[1] = null;
		((Object[]) airlines.get(gdsCode))[2] = null;
		((Object[]) airlines.get(gdsCode))[3] = null;
		return checkEmailServerConnection(gdsCode);
	}

	// private boolean checkValidFromAddress(Address[] messageAddresses, Set fromSITAAddresses){
	// if (messageAddresses == null || fromSITAAddresses == null){
	// return false;
	// }
	// for (Iterator iterFromSitaAddress = fromSITAAddresses.iterator(); iterFromSitaAddress.hasNext();) {
	// GDSSitaAddress gdsSitaAddress = (GDSSitaAddress) iterFromSitaAddress.next();
	// // gdsSitaAddress.
	// for (int i = 0; i < messageAddresses.length; i++) {
	// if ( getEmailAddress ((Address) messageAddresses[i]).equals(gdsSitaAddress.getEmailAddress())) {
	// return true;
	// }
	// }
	// }
	// return false;
	// }

	private String getEmailAddress(Address address) {
		if (address.toString().indexOf("<") != -1) {
			return address.toString().substring(address.toString().indexOf("<") + 1, address.toString().indexOf(">"));
		} else {
			return address.toString();
		}
	}

	// FIXME DILAN to skip normal messages from my inbox
	private boolean isRelevantMessageSubject(String subject) {
		if (subject != null && "GDS".equalsIgnoreCase(subject)) {
			return true;
		}
		return false;
	}

	private boolean handleMailUnrelayedMessages(String subject) {
		String strOutMessageId;
		Integer intOutMessageId;
		OutMessage outMessage;
		OutMessageProcessingStatus outMessageProcessingStatus;
		if (subject != null && subject.length() > undeliveredSubject.length()
				&& subject.substring(0, undeliveredSubject.length()).equals(undeliveredSubject)) {
			// Undelivered message found
			log.info("GDSEMailRetriever --> handleMailUnrelayedMessages --> Undelivered message found !");
			strOutMessageId = subject.substring(undeliveredSubject.length());
			try {
				intOutMessageId = Integer.valueOf(strOutMessageId);
			} catch (Exception e) {
				log.error("GDSEMailRetriever --> handleMailUnrelayedMessages --> out message id cannot be obtained! /n"
						+ e.getMessage());
				// Return as handled.
				return true;
			}
			outMessage = getMessageDAO().getOutMessage(intOutMessageId);
			if (outMessage == null) {
				log.error("GDSEMailRetriever --> handleMailUnrelayedMessages --> There is no out message for the retrieved id! ");
				// Return as handled.
				return true;
			}
			if (outMessage.getLastErrorMessageCode() == null
					|| !outMessage.getLastErrorMessageCode().equals("outmessagesending.mailserver.couldnotrelay")) {
				outMessageProcessingStatus = new OutMessageProcessingStatus();
				outMessageProcessingStatus.setErrorMessageCode("outmessagesending.mailserver.couldnotrelay");
				outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
				outMessageProcessingStatus.setProcessingDate(new Date());
				outMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
				outMessage.setLastErrorMessageCode("outmessagesending.mailserver.couldnotrelay");
			}
			outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
			outMessage.setProcessedDate(new Date());
			getMessageDAO().saveOutMessage(outMessage);

			return true;
		}
		return false;
	}

	/**
	 * Method to read emails from the pop3 server
	 * 
	 */
	public void getMail() {
		// Session session = null;
		// Folder folder = null;
		// QueueWriter queueWriter = new QueueWriter();
		String bodyText = null;
		int messageCount = 0;
		String gdsCode;
		Object[] arrAirlineDet;

		// Properties props = System.getProperties();
		// session = Session.getInstance(props, null);

		// try {
		/*
		 * Loop through the airlines list and for each airline do following
		 */

		log.info("############################################################################");
		log.info("################# R e a d    E m a i l    M e s s a g e s ##################");
		log.info("############################################################################");

		for (Iterator<String> iterGdsCodes = airlines.keySet().iterator(); iterGdsCodes.hasNext();) {
			gdsCode = iterGdsCodes.next();
			if (!checkEmailServerConnection(gdsCode)) {
				continue;
			}
			arrAirlineDet = (Object[]) airlines.get(gdsCode);
			// 1. Get messages
			try {
				if (!readInboxFolder(gdsCode)) {
					continue;
				}
				messageCount = ((Folder) ((Object[]) airlines.get(gdsCode))[2]).getUnreadMessageCount();
			} catch (MessagingException e) {
				log.error("Error in reading inbox folder message count for gds code " + gdsCode + ".  Error:"
						+ e.getMessage());
				if (!createEmailServerConnection(gdsCode)) {
					continue;
				}
			}
			if (messageCount > 0) {
				// log.info("Message Count is " + messageCount);
				// System.out.println("Message count is " + messageCount);
				Message[] messages = null;
				try {
					messages = ((Folder) ((Object[]) airlines.get(gdsCode))[2]).getMessages();
					i = 0;
					while (messages.length > i) {
						// for (int i = 0; i < messages.length; i++) {
						// 2. Check whether the mail is from one of the given senders of the current airline.
						if ((messages[i] != null)) {
							if (handleMailUnrelayedMessages(messages[i].getSubject())) {
								messages[i].setFlag(Flags.Flag.DELETED, true);
								i++;
								continue;
							}
//							if (!isRelevantMessageSubject(messages[i].getSubject())) {
//								i++;
//								continue;
//							}
							// if ((messages[i] != null)
							// && checkValidFromAddress(messages[i].getFrom(),
							// ((ServiceBearer)arrAirlineDet[0]).getFromSITAAddresses() )
							// ){
							// log.info("Message found " + i);
							if (messages[i].getContent() instanceof Multipart) {
								Multipart mp = (Multipart) messages[i].getContent();
								for (int j = 0; j < mp.getCount(); j++) {
									if (mp.getBodyPart(j).getContentType().startsWith(CONTENT_TYPE)) {
										bodyText = (String) mp.getBodyPart(j).getContent();
									}
								}
							} else {
								bodyText = (String) messages[i].getContent();
							}
							if (!bodyText.trim().equals("")) {
								log.info("Reading new message | Message text : " + bodyText);
								inMessage = new InMessage();
								inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);
								//Assumption: GDS provide different type B addresses when connecting with different Airlines. 
								inMessage.setAirLineCode(AppSysParamsUtil.getDefaultCarrierCode());
								inMessage.setInMessageText(bodyText);
								inMessage.setReceivedDate(new Date());
								//TODO: Uncomment this
								String[] recordLocater = messages[i].getHeader(this.MESSAGE_ID);
								//String[] recordLocater = {"1"};
								if (recordLocater !=null && recordLocater.length!=0) {
									inMessage.setGdsMessageID(recordLocater[0]);
								}
								
								// if received message is [SSM/ASM/AVS] then need to identify the message type before
								// parsing
								String[] resultStr = MessageRequestProcessingUtil.getMessageIdentifier(bodyText, true);

								log.info("Message service type : " + resultStr[0]);

								if (Constants.ServiceTypes.SSM.equals(resultStr[0])) {
									inMessage.setMessageType(Constants.ServiceTypes.SSM);
									inMessage.setInMessageText(resultStr[1]);
									inMessage.setMessageSequence(resultStr[4]);									
								} else if (Constants.ServiceTypes.ASM.equals(resultStr[0])) {
									inMessage.setMessageType(Constants.ServiceTypes.ASM);
									inMessage.setInMessageText(resultStr[1]);
									inMessage.setMessageSequence(resultStr[4]);
								} else if (Constants.ServiceTypes.AVS.equals(resultStr[0])) {
									inMessage.setMessageType(Constants.ServiceTypes.AVS);
								} else {
									inMessage.setMessageType(Constants.ServiceTypes.ReservationService);
								}
								
								 String sentAddress = getEmailAddress(messages[i].getFrom()[0]).split("@")[0];
								 if(sentAddress != null && !sentAddress.equals("")){
									 inMessage.setSentTTYAddress(sentAddress.toUpperCase());
								 }
								 inMessage.setSentEmailAddress(getEmailAddress(messages[i].getFrom()[0]));
								 String reciepients = "";
								 for (int j = 0; j < messages[i].getAllRecipients().length; j++) {
									 reciepients += ((Address)messages[i].getAllRecipients()[j]).toString().
											 split("@")[0] + ",";
								 }
							 	inMessage.setRecipientTTYAddress(reciepients.substring(0, reciepients.length()-1));
								inMessage.setRecipientEmailAddress(((ServiceBearer) arrAirlineDet[0]).getEmailAddress());
								inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
								inMessage.setEditableStatus(true);
								inMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);

								log.info("Saving message in to the database");
								inMessage = MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessage(inMessage);
								log.info("Message Saved | Message ID : " + inMessage.getInMessageID());

								inMessagePassingErrors = new InMessageProcessingStatus();
								inMessagePassingErrors.setInMessageID(inMessage.getInMessageID());
								inMessagePassingErrors
										.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_MESSAGE_RECEIVED);
								inMessagePassingErrors.setProcessingDate(new Date());
								inMessagePassingErrors
										.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
								MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessagePassingErrors);
								hasNewMessages = true;
							}
							messages[i].setFlag(Flags.Flag.DELETED, true);
							i++;
							//break;
							// if
							// (queueWriter.writeToBSMQueue(bodyText,MessagingConstants.QueueJndINames.GDS_INCOMING_MESSAGE_QUEUE_JNDI_NAME
							// )) {
							// messages[i].setFlag(Flags.Flag.DELETED, true);
							// }
						} else if (messages[i] != null) {
							i++;
						}
					}
				} catch (MessagingException e) {
					log.error("Error in reading inbox folder messages for gds code " + gdsCode + ".  Error:", e);
					if (messages != null && messages.length > 0) {
						try {
							messages[i].setFlag(Flags.Flag.DELETED, true);
							log.error("The errorneous message is deleted from inbox folder of gds code " + gdsCode + ".");
						} catch (MessagingException e1) {
							log.error(e1);
						}
					}
					if (!createEmailServerConnection(gdsCode)) {
						continue;
					}
				} catch (IOException e) {
					log.error("IO Error in reading inbox folder messages for gds code " + gdsCode + ".  Error:", e);
					if (!createEmailServerConnection(gdsCode)) {
						continue;
					}
				} catch (ModuleException ex) {
					log.error("Error in saving message in to the database", ex);
				}

			} else {
				// log.info("Message not found ");
			}
		}

		log.info("############################################################################");
		log.info("############ R e a d i n g    E m a i l s    C o m p l e t e d #############");
		log.info("############################################################################");

	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * return the message DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}
	
	private String getMailAcceProtocolForGDS(String gdsCode){
	 return	getAdminDAO().getMailAccesProtocol(gdsCode);
	}
}
