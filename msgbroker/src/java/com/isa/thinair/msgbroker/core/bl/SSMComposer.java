package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.msgbroker.api.dto.SSIFlightLegDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO.Action;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO.DayOfWeek;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SSMDataElement;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SSMSubMessageType;
import com.isa.thinair.msgbroker.core.util.MessageComposerUtil;
import com.isa.thinair.msgbroker.core.util.SSMDataElementUseage;
import com.isa.thinair.msgbroker.core.util.SSMSubMessageFormat;

/**
 * 
 * @author thejaka
 * 
 */
public class SSMComposer {
	private static Log logger = LogFactory.getLog(SSMComposer.class);
	private List<String> ssmElements;
	private SSMDataElementUseage currentSSMDataElementUseage;
	private SimpleDateFormat sdfDDMMMYY = new SimpleDateFormat("ddMMMyy");
	Iterator iterSSIFlightLegDTO;
	private boolean isSkipHeader = false;

	private List<String> generateMessageElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageType ssmSubMessageType)
			throws ModuleException {
		ssmElements = new ArrayList<String>();
		SSMSubMessageFormat ssmSubMessageFormat = ssmSubMessageType.getSSMSubMessageFormat();
		currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(SSMDataElement.BEGIN_SSM);
		while (!currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.END_SSM)) {

			// MESSAGE_HEADING
			generateMessageHeadingElements(ssiMessegeDTO, ssmSubMessageFormat);

			// MESSAGE_REFERENCE
			generateMessageReferenceElements(ssiMessegeDTO, ssmSubMessageFormat);

			// ACTION_INFORMATION
			generateActionInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// FLIGHT_INFORMATION
			generateFlightInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// PERIOD_FREQUENCY_INFORMATION
			generatePeriodFreqInformationElements(ssiMessegeDTO, ssmSubMessageFormat);
			
			// RREVISED PERIOD_FREQUENCY_INFORMATION
			generateRevisedPeriodFreqInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// EQUIPMENT_INFORMATION
			generateEquipmentInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// LEG_INFORMATION
			generateLegInformationElements(ssiMessegeDTO, ssmSubMessageFormat);
			
			// NEW_FLIGHT_INFORMATION
			generateNewFlightInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// SEGMENT_INFORMATION
			generateSegmentInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

			// COMMENTS
			generateCommentElements(ssiMessegeDTO, ssmSubMessageFormat);
			
			// SUPPLEMENTARY_INFORMATIONS
			generateSupplementaryInformationElements(ssiMessegeDTO, ssmSubMessageFormat);

		}
		return ssmElements;
	}

	private void generateMessageHeadingElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat) {
		// MESSAGE_HEADING
		List<String> ssmElements = new ArrayList<String>();
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.MESSAGE_HEADING_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER)
				&& ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage)) {
			ssmElements.add("SSM");
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		// if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.MESSAGE_HEADING_EOL)){
		// ssmElements.add(MessageComposerConstants.EOL);
		// currentSSMDataElementUseage=
		// ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		// }
		//

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.STANDARD_MESSAGE_IDENTIFIER_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.TIME_MODE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			ssmElements.add(MessageComposerUtil.getMessagingTimeMode(ssiMessegeDTO.getTimeMode()));
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.TIME_MODE_EOL)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (!isSkipHeader) {
			this.ssmElements.addAll(ssmElements);
		}

	}

	private void generateMessageReferenceElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		List<String> ssmElements = new ArrayList<String>();
		// MESSAGE_REFERENCE
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.MESSAGE_REFERENCE_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.MESSAGE_SEQUENCE_REFERENCE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getReferenceNumber() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.messagesequencereference.invalid.null");
			} else {
				ssmElements.add(ssiMessegeDTO.getReferenceNumber().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.MESSAGE_REFERENCE_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (!isSkipHeader) {
			this.ssmElements.addAll(ssmElements);
		}
	}

	private void generateActionInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// ACTION_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ACTION_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ACTION_IDENTIFIER)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getAction() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.actionidentifier.invalid.null");
			} else {
				ssmElements.add(ssiMessegeDTO.getAction().toString());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ASM_WITHDRAWAL_INDICATOR)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getAction() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.asmwithdrawalindicator.invalid.null");
			} else {
				if (ssiMessegeDTO.isWithdrawal()) {
					ssmElements.add(" " + MessageComposerConstants.AsmWithdrawalIndicator);
				}
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ACTION_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}

	private void generateFlightInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// FLIGHT_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.FLIGHT_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.FLIGHT_DESIGNATOR)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getFlightDesignator() == null || ssiMessegeDTO.getFlightDesignator().trim().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.flightdesignator.invalid.null");
			} else {
				ssmElements.add(ssiMessegeDTO.getFlightDesignator().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.FLIGHT_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}

	private void generatePeriodFreqInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// PERIOD_FREQUENCY_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.PERIOD_OF_OPERATION_FROM_DATE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getStartDate() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.periodofoperationfromdate.invalid.null");
			} else {
				if (Action.REV == ssiMessegeDTO.getAction()) {
					ssmElements.add(MessageComposerConstants.SP);
				}
				ssmElements.add(sdfDDMMMYY.format(ssiMessegeDTO.getStartDate()).toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.PERIOD_OF_OPERATION_TO_DATE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getEndDate() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.periodofoperationtodate.invalid.null");
			} else {
				ssmElements.add(" " + sdfDDMMMYY.format(ssiMessegeDTO.getEndDate()).toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.DAYS_OF_OPERATION)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getDaysOfOperation() == null || ssiMessegeDTO.getDaysOfOperation().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.daysofoperation.invalid.null");
			} else {
				ssmElements.add(" ");
				for (Iterator iter = ssiMessegeDTO.getDaysOfOperation().iterator(); iter.hasNext();) {
					DayOfWeek dayOfWeek = (DayOfWeek) iter.next();
					ssmElements.add(String.valueOf(dayOfWeek.dayNumber()));
				}
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.PERIOD_FREQUENCY_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}		
	}
	
	private void generateRevisedPeriodFreqInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// NEW_NEW_PERIOD_FREQUENCY_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_PERIOD_OF_OPERATION_FROM_DATE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getNewStartDate() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.periodofoperationfromdate.invalid.null");
			} else {
				ssmElements.add(sdfDDMMMYY.format(ssiMessegeDTO.getNewStartDate()).toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_PERIOD_OF_OPERATION_TO_DATE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getNewEndDate() == null) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.periodofoperationtodate.invalid.null");
			} else {
				ssmElements.add(" " + sdfDDMMMYY.format(ssiMessegeDTO.getNewEndDate()).toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_DAYS_OF_OPERATION)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getNewDaysOfOperation() == null || ssiMessegeDTO.getNewDaysOfOperation().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.daysofoperation.invalid.null");
			} else {
				ssmElements.add(" ");
				for (Iterator iter = ssiMessegeDTO.getNewDaysOfOperation().iterator(); iter.hasNext();) {
					DayOfWeek dayOfWeek = (DayOfWeek) iter.next();
					ssmElements.add(String.valueOf(dayOfWeek.dayNumber()));
				}
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_PERIOD_FREQUENCY_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}

	private void generateEquipmentInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// EQUIPMENT_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.EQUIPMENT_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SERVICE_TYPE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getServiceType() == null || ssiMessegeDTO.getServiceType().trim().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.servicetype.invalid.null");
			} else {
				ssmElements.add(ssiMessegeDTO.getServiceType().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.AIRCRAFT_TYPE)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getAircraftType() == null || ssiMessegeDTO.getAircraftType().trim().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.aircrafttype.invalid.null");
			} else {
				ssmElements.add(" " + ssiMessegeDTO.getAircraftType().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.PASSENGER_RES_BOOKING_DESIGNATOR)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getPassengerResBookingDesignator() == null
					|| ssiMessegeDTO.getPassengerResBookingDesignator().trim().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.passengerreservationbookingdesignator.invalid.null");
			} else {
				ssmElements.add(" " + ssiMessegeDTO.getPassengerResBookingDesignator().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.EQUIPMENT_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}

	private void generateLegInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// LEG_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.LEG_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			// Iterate for all available legs
			iterSSIFlightLegDTO = ssiMessegeDTO.getLegs().iterator();
			while (iterSSIFlightLegDTO.hasNext()) {
				SSIFlightLegDTO ssiFlightLegDTO = (SSIFlightLegDTO) iterSSIFlightLegDTO.next();
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.LEG_INFORMATION_EOL)) {
					ssmElements.add(MessageComposerConstants.EOL);
					currentSSMDataElementUseage = ssmSubMessageFormat
							.getNextSSMDataElementUsage(SSMDataElement.LEG_INFORMATION_BOL);
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.DEPARTURE_AIRPORT)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getDepatureAirport() == null || ssiFlightLegDTO.getDepatureAirport().trim().isEmpty()) {
						if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
							throw new ModuleException("reservation.ssm.composer.departureairport.invalid.null");
					} else {
						ssmElements.add(ssiFlightLegDTO.getDepatureAirport().toUpperCase());
						currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
					}
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.DEPATURE_TIME)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getDepatureTime() == null || ssiFlightLegDTO.getDepatureTime().trim().isEmpty()) {
						if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
							throw new ModuleException("reservation.ssm.composer.departuretime.invalid.null");
					} else {
						ssmElements.add(ssiFlightLegDTO.getDepatureTime().toUpperCase());
						currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
					}
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.DATE_VARIATION_FOR_STD)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getDepatureOffiset() != 0) {
						ssmElements.add("/" + ssiFlightLegDTO.getDepatureOffiset());
					}
					currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ARRIVAL_AIRPORT)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getArrivalAirport() == null || ssiFlightLegDTO.getArrivalAirport().trim().isEmpty()) {
						if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
							throw new ModuleException("reservation.ssm.composer.arrivalairport.invalid.null");
					} else {
						ssmElements.add(" " + ssiFlightLegDTO.getArrivalAirport().toUpperCase());
						currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
					}
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.ARRIVAL_TIME)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getArrivalTime() == null || ssiFlightLegDTO.getArrivalTime().trim().isEmpty()) {
						if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
							throw new ModuleException("reservation.ssm.composer.arrivaltime.invalid.null");
					} else {
						ssmElements.add(ssiFlightLegDTO.getArrivalTime().toUpperCase());
						currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
					}
				}
				if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.DATE_VARIATION_FOR_STA)
						&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
					if (ssiFlightLegDTO.getArrivalOffset() != 0) {
						ssmElements.add("/" + ssiFlightLegDTO.getArrivalOffset());
					}
					currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
				}
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.LEG_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}
	
	private void generateNewFlightInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// NEW_FLIGHT_INFORMATION
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_FLIGHT_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_FLIGHT_DESIGNATOR)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (ssiMessegeDTO.getNewFlightDesignator() == null || ssiMessegeDTO.getNewFlightDesignator().trim().isEmpty()) {
				if (ssmSubMessageFormat.isMandatoryDataElement(currentSSMDataElementUseage))
					throw new ModuleException("reservation.ssm.composer.flightdesignator.invalid.null");
			} else {
				ssmElements.add(ssiMessegeDTO.getNewFlightDesignator().toUpperCase());
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			}
		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.NEW_FLIGHT_INFORMATION_EOL)) {
			ssmElements.add(MessageComposerConstants.EOL);
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}
	
	private void generateSegmentInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		// SEGMENT_INFORMATION
		boolean isExternalFltFound = false;
		boolean isCSCarrier = ssiMessegeDTO.isCodeShareCarrier();
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SEGMENT_INFORMATION_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
			// Iterate for all external flights
			if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.OTHER_SEGMENT_INFO)
					&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
				currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
				List<String> externalFlightNos = ssiMessegeDTO.getExternalFlightNos();
				if (externalFlightNos != null && !externalFlightNos.isEmpty()) {
					Iterator<String> externalFlightNoItr = externalFlightNos.iterator();
					StringBuilder cfInfo = new StringBuilder();
					while (externalFlightNoItr.hasNext()) {
						String externalFlightNo = externalFlightNoItr.next();
						if (externalFlightNo != null && !"".equals(externalFlightNo.trim())) {
							cfInfo.append("/" + externalFlightNo.toUpperCase());
						}
					}

					if (isCSCarrier && !StringUtil.isNullOrEmpty(cfInfo.toString())
							&& !StringUtil.isNullOrEmpty(ssiMessegeDTO.getSegmentCode())) {
						ssmElements.add(ssiMessegeDTO.getSegmentCode());
						ssmElements.add(MessageComposerConstants.SP);
						ssmElements.add(MessageComposerConstants.OTHER_SEG_INFO);
						ssmElements.add(cfInfo.toString().toUpperCase());
						isExternalFltFound = true;
					}
				}

			}

		}
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SEGMENT_INFORMATION_EOL)) {
			if (isExternalFltFound) {
				ssmElements.add(MessageComposerConstants.EOL);
			}
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}
	

	private void generateCommentElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat)
			throws ModuleException {
		List<String> tempElements = new ArrayList<String>();
		boolean isCommentElementAvailable = false;
		// COMMENTS
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.COMMENTS_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);

			if (ssiMessegeDTO.getLegs() != null && !ssiMessegeDTO.getLegs().isEmpty()) {
				// Iterate for all available legs
				iterSSIFlightLegDTO = ssiMessegeDTO.getLegs().iterator();
				while (iterSSIFlightLegDTO.hasNext()) {
					SSIFlightLegDTO ssiFlightLegDTO = (SSIFlightLegDTO) iterSSIFlightLegDTO.next();

					if (ssiFlightLegDTO.isEticketCandidate()
							&& !(ssiMessegeDTO.getAction() == Action.CNL || ssiMessegeDTO.getAction() == Action.TIM)) {
						tempElements.add(" ");
						tempElements.add(MessageComposerConstants.MsgDataElementIdentifier.E_TICKETING);
						tempElements.add("/");
						tempElements.add(MessageComposerConstants.MsgConstants.E_TICKET_CANDIDATE);
					}

					if (!tempElements.isEmpty()) {
						isCommentElementAvailable = true;
						if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.COMMENTS_EOL)) {
							ssmElements.add(MessageComposerConstants.EOL);
							currentSSMDataElementUseage = ssmSubMessageFormat
									.getNextSSMDataElementUsage(SSMDataElement.COMMENTS_BOL);
						}

						if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.COMMENTS)
								&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
							ssmElements.add(ssiFlightLegDTO.getDepatureAirport().toUpperCase());
							ssmElements.add(ssiFlightLegDTO.getArrivalAirport().toUpperCase());

							ssmElements.addAll(tempElements);

							currentSSMDataElementUseage = ssmSubMessageFormat
									.getNextSSMDataElementUsage(currentSSMDataElementUseage);
						}
					}
					tempElements.clear();
				}
			}
		}

		// TODO: temp fix above populating logic should be revisited
		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.COMMENTS)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.COMMENTS_EOL)) {
			if (isCommentElementAvailable)
			ssmElements.add(MessageComposerConstants.EOL);

			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

	}

	public String composeSSMessage(SSIMessegeDTO ssiMessegeDTO, boolean includeAddressInMsgBody) throws ModuleException {

		SSMSubMessageType ssmSubMessageType = MessageComposerUtil.getSSMSubMessageType(ssiMessegeDTO.getAction());
		String outMessage = null;

		List<String> ssmElements = generateMessageElements(ssiMessegeDTO, ssmSubMessageType);

		if (!ssmElements.isEmpty()) {

			try {
				String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.SSM
						+ MessageComposerConstants.TEMPLATE_SUFFIX;
				StringWriter writer = new StringWriter();
				HashMap<String, Object> printDataMap = new HashMap<String, Object>();

				if (includeAddressInMsgBody) {
					printDataMap.put("res", ssiMessegeDTO.getRecipientAddress());
					printDataMap.put("sen", ssiMessegeDTO.getSenderAddress());
					SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
			        Date dt = new Date();
			        String timestamp = sdf.format(dt).toUpperCase();
					printDataMap.put("timestamp", timestamp);
				}
				printDataMap.put("ssmElements", ssmElements.iterator());
				
				new TemplateEngine().writeTemplate(printDataMap, templateName, writer);
				outMessage = writer.toString();

			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.ssmessage.error\n", e);
				}
				throw new ModuleException("gdsmessagecomposer.ssmessage.error");

			}

		} else {

			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.ssmessage.noelements\n");
			}

			throw new ModuleException("gdsmessagecomposer.ssmessage.noelementsr");

		}

		logger.info(outMessage);
		return outMessage;

	}

	private void generateSupplementaryInformationElements(SSIMessegeDTO ssiMessegeDTO, SSMSubMessageFormat ssmSubMessageFormat) {

		boolean isSupplementaryInfoFound = false;

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUB­MESSAGE_SEPARATION_BOL)
				&& ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (!StringUtil.isNullOrEmpty(ssiMessegeDTO.getSupplementaryInfo())) {
			isSupplementaryInfoFound = true;
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUB­MESSAGE_SEPARATION)) {
			if (isSupplementaryInfoFound)
				ssmElements.add("//");

			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUB­MESSAGE_SEPARATION_EOL)) {
			if (isSupplementaryInfoFound)
				ssmElements.add(MessageComposerConstants.EOL);

			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_BOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUPPLEMENTARY_INFORMATION)
				&& !ssmSubMessageFormat.isNotApplicableDataElement(currentSSMDataElementUseage)) {
			if (isSupplementaryInfoFound) {
				ssmElements.add(MessageComposerConstants.SupplementaryInformation.SUPPLEMENTARY_INFORMATION_INDICATOR);
				ssmElements.add(MessageComposerConstants.SP);
				ssmElements.add(ssiMessegeDTO.getSupplementaryInfo().toUpperCase());
			}
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}

		if (currentSSMDataElementUseage.getSsmDataElement().equals(SSMDataElement.SUPPLEMENTARY_INFORMATIONS_EOL)) {
			currentSSMDataElementUseage = ssmSubMessageFormat.getNextSSMDataElementUsage(currentSSMDataElementUseage);
		}
	}

	public String composeSSMSubMessages(Map<Integer, SSIMessegeDTO> ssmMessegeDTOMap, boolean includeAddressInMsgBody,
			Collection bookingClassList, String recipientAddress, String senderAddress, boolean isCodeShareCarrier)
			throws ModuleException {

		StringBuilder message = new StringBuilder();
		boolean isFirstMessage = true;
		String outMessage = null;
		if (ssmMessegeDTOMap != null && !ssmMessegeDTOMap.isEmpty()) {
			for (Integer key : ssmMessegeDTOMap.keySet()) {
				SSIMessegeDTO ssiMessegeDTO = ssmMessegeDTOMap.get(key);

				if (bookingClassList != null && !bookingClassList.isEmpty()) {
					String rbds = "";

					Iterator itRbd = bookingClassList.iterator();
					while (itRbd.hasNext()) {
						String code = (String) itRbd.next();
						rbds += code;
					}

					ssiMessegeDTO.setPassengerResBookingDesignator(rbds);
				}
				ssiMessegeDTO.setCodeShareCarrier(isCodeShareCarrier);
				SSMSubMessageType ssmSubMessageType = MessageComposerUtil.getSSMSubMessageType(ssiMessegeDTO.getAction());

				List<String> ssmElements = generateMessageElements(ssiMessegeDTO, ssmSubMessageType);

				if (!ssmElements.isEmpty()) {

					try {
						String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.SSM
								+ MessageComposerConstants.TEMPLATE_SUFFIX;
						StringWriter writer = new StringWriter();
						HashMap<String, Object> printDataMap = new HashMap<String, Object>();

						if (isFirstMessage && includeAddressInMsgBody) {
							printDataMap.put("res", recipientAddress);
							printDataMap.put("sen", senderAddress);
							SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
							Date dt = new Date();
							String timestamp = sdf.format(dt).toUpperCase();
							printDataMap.put("timestamp", timestamp);
						}
						printDataMap.put("ssmElements", ssmElements.iterator());

						new TemplateEngine().writeTemplate(printDataMap, templateName, writer);

						if (!isFirstMessage)
							message.append("\n//\n");

						message.append(writer.toString());
						isFirstMessage = false;
						this.isSkipHeader = true;
					} catch (Exception e) {

						if (logger.isDebugEnabled()) {
							logger.debug("gdsmessagecomposer.ssmessage.error\n", e);
						}
						throw new ModuleException("gdsmessagecomposer.ssmessage.error");

					}

				} else {
					 logger.debug("gdsmessagecomposer.ssmessage.noelements\n");
				}

			}

			outMessage = message.toString();

			if (!StringUtil.isNullOrEmpty(outMessage)) {
				outMessage = outMessage.replaceAll("[\n\r]+", "\n");
				outMessage = outMessage.replaceAll("\n[ \t]*\n", "\n");
			}
		}

		logger.info(outMessage);
		return outMessage;

	}

	public static void main(String[] args) {

		SSMComposer composer = new SSMComposer();

		SSIMessegeDTO ssiMessegeDTO = new SSIMessegeDTO();
		/*
		 * SSM 29SEP00002E001 LT TIM G9525 01OCT 30OCT 1234567 SHJ1945 CMB0015/1 CMB0115/1 MLE0300/1
		 */
		ssiMessegeDTO.setAction(Action.TIM);
		ssiMessegeDTO.setReferenceNumber("29SEP00002E001");
		ssiMessegeDTO.setFlightDesignator("G9525");
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(2008, 12, 1);
		ssiMessegeDTO.setStartDate(gc.getTime());
		gc.set(2008, 12, 2);
		ssiMessegeDTO.setEndDate(gc.getTime());
		List<DayOfWeek> daysOfOperation = new ArrayList<DayOfWeek>();
		daysOfOperation.add(DayOfWeek.SUNDAY);
		daysOfOperation.add(DayOfWeek.MONDAY);
		daysOfOperation.add(DayOfWeek.TUESDAY);
		daysOfOperation.add(DayOfWeek.WEDNESDAY);
		daysOfOperation.add(DayOfWeek.THURSDAY);
		daysOfOperation.add(DayOfWeek.FRIDAY);
		daysOfOperation.add(DayOfWeek.SATURDAY);
		ssiMessegeDTO.setDaysOfOperation(daysOfOperation);
		List<SSIFlightLegDTO> legs = new ArrayList<SSIFlightLegDTO>();
		SSIFlightLegDTO leg = new SSIFlightLegDTO();
		leg.setDepatureAirport("SHJ");
		leg.setDepatureTime("1945");
		leg.setDepatureOffiset(0);
		leg.setArrivalAirport("CMB");
		leg.setArrivalTime("0015");
		leg.setArrivalOffset(1);
		legs.add(leg);
		leg = new SSIFlightLegDTO();
		leg.setDepatureAirport("CMB");
		leg.setDepatureTime("0115");
		leg.setDepatureOffiset(1);
		leg.setArrivalAirport("MLE");
		leg.setArrivalTime("0300");
		leg.setArrivalOffset(1);
		legs.add(leg);
		ssiMessegeDTO.setLegs(legs);
		ssiMessegeDTO.setWithdrawal(true);
		
		List<String> externalFlightNos = new ArrayList<String>();
		externalFlightNos.add(new String("UF1123"));
		externalFlightNos.add(new String("MH0023"));
		
		ssiMessegeDTO.setExternalFlightNos(externalFlightNos);
		/* SSM Check */
		try {
			String composedText = composer.composeSSMessage(ssiMessegeDTO, true);
			logger.info(composedText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
