package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

public abstract class GdsTypeBMessageGateway {

	protected Collection<ServiceBearer> applicableServiceBearers;
	protected AdminDAO adminDAO;
	protected MessageDAO messageDAO;

	protected GdsTypeBMessageGateway() {
		applicableServiceBearers = new ArrayList<ServiceBearer>();
		adminDAO = LookupUtil.lookupAdminDAO();
		messageDAO = LookupUtil.lookupMessageDAO();
	}

	public abstract void receiveMessages(boolean useGDSConfiguration, Collection<Integer> unprocessedMsgIds);

	public abstract void sendMessages(List<String> messageTypes, boolean isUsedGDSConfiguration);

	public Collection<ServiceBearer> getApplicableServiceBearers() {
		return applicableServiceBearers;
	}

	public void setApplicableServiceBearers(Collection<ServiceBearer> applicableServiceBearers) {
		this.applicableServiceBearers = applicableServiceBearers;
	}

}

