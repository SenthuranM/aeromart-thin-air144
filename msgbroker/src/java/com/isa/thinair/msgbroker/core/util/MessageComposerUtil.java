/**
 * 
 */
package com.isa.thinair.msgbroker.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.isa.thinair.commons.api.constants.CommonsConstants.TimeMode;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.msgbroker.api.dto.NameDTO;
import com.isa.thinair.msgbroker.api.dto.OSIAddressDTO;
import com.isa.thinair.msgbroker.api.dto.OSIDTO;
import com.isa.thinair.msgbroker.api.dto.OSIEmailDTO;
import com.isa.thinair.msgbroker.api.dto.OSIPhoneNoDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO.Action;
import com.isa.thinair.msgbroker.api.dto.SSRChildDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocoDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocsDTO;
import com.isa.thinair.msgbroker.api.dto.SSREmergencyContactDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRFoidDTO;
import com.isa.thinair.msgbroker.api.dto.SSRInfantDTO;
import com.isa.thinair.msgbroker.api.dto.SSRPassportDTO;
import com.isa.thinair.msgbroker.api.dto.SegmentDTO;
import com.isa.thinair.msgbroker.api.dto.UnchangedNamesDTO;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.bl.TypeBMessageConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.config.TypeBMessageStructure;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.ASMSubMessageType;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SSMSubMessageType;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * utility class for message composing.
 * 
 * @author sanjeewaf
 */
public class MessageComposerUtil {

	/** Constant string for String "SSR" . */
	private static final String SSR = "SSR";

	/** Constant string postfix for SSR infant which occupies code . */
	private static final String OCCUPY_SEAT_POSTFIX = "OCCUPYING SEAT";

	/** Constant string for under ".":. */
	private static final String POINT = ".";

	/** Constant string for under score:. */
	private static final String HYPHEN = "-";

	/** Constant string for SSR infant age in months "MTHS". */
	private static final String MTHS = "MTHS";

	/** Constant string stores message template path. */
	private static String MessageTemplatePath = null;

	public static String getMessageTemplatePath() {
		if (MessageTemplatePath == null) {
			MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
					MsgbrokerConstants.MODULE_NAME);
			MessageTemplatePath = moduleConfig.getTemplatesroot();
		}
		return MessageTemplatePath;
	}

	public static SSMSubMessageType getSSMSubMessageType(Action action) {
		if (action.equals(Action.NEW)) {
			return SSMSubMessageType.NEW;
		} else if (action.equals(Action.CNL)) {
			return SSMSubMessageType.CNL;
		} else if (action.equals(Action.RPL)) {
			return SSMSubMessageType.RPL;
		} else if (action.equals(Action.EQT)) {
			return SSMSubMessageType.EQT;
		} else if (action.equals(Action.TIM)) {
			return SSMSubMessageType.TIM;
		} else if (action.equals(Action.REV)) {
			return SSMSubMessageType.REV;
		} else if (action.equals(Action.FLT)) {
			return SSMSubMessageType.FLT;
		}
		return null;
	}

	public static ASMSubMessageType getASMSubMessageType(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action action) {
		if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.NEW)) {
			return ASMSubMessageType.NEW;
		} else if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.CNL)) {
			return ASMSubMessageType.CNL;
		} else if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.RPL)) {
			return ASMSubMessageType.RPL;
		} else if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.EQT)) {
			return ASMSubMessageType.EQT;
		} else if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.TIM)) {
			return ASMSubMessageType.TIM;
		} else if (action.equals(com.isa.thinair.msgbroker.api.dto.ASMessegeDTO.Action.FLT)) {
			return ASMSubMessageType.FLT;
		}
		return null;
	}

	/**
	 * Method will process each SSR DTO and will construct possible Elements.
	 * 
	 * @param specialServiceRequestDTOs
	 * @return
	 */
	public static List composeSSRElements(List<SSRDetailDTO> ssrDetailDTOs) {

		// TODO change ssr.getAction_status_Code to ADVICE CODE****************
		List<String> ssrElementList = new ArrayList<String>();
		String ssrElement = "";
		String ssrNameString = "";
		for (Iterator iter = ssrDetailDTOs.iterator(); iter.hasNext();) {

			ssrElement = "";
			ssrNameString = "";

			SSRDetailDTO ssr = (SSRDetailDTO) iter.next();
			if (ssr.getCodeSSR().trim().equals(TypeBMessageConstants.SSRCodes.CHLD)) {

				ssrElement = SSR + MessageComposerConstants.SP + ssr.getCarrierCode().trim() + MessageComposerConstants.SP
						+ ssr.getAdviceOrStatusCode().trim();
				if (ssr.getFreeText() != null && !ssr.getFreeText().equals("")) {
					ssrElement += MessageComposerConstants.FWD + ssr.getFreeText().trim();
				}
				if (ssr.getSsrNameDTOs() != null) {
					ssrNameString = MessageComposerUtil.buildNameElement(ssr.getSsrNameDTOs());
					ssrElement += HYPHEN + ssrNameString;
				}
				ssrElementList.add(ssrElement.trim());

			} else { // assumed that except for chld - (sofar) all other ssr
						// need segment element
				String ssr1 = "";
				String ssr2 = "";
				SegmentDTO ssrSegment = ssr.getSegmentDTO();
				String codeSSR = ssr.getCodeSSR().trim();
				if (ssrSegment != null) {
					List ssrNameList = ssr.getSsrNameDTOs();
					ssr1 = SSR + MessageComposerConstants.SP + codeSSR + MessageComposerConstants.SP
							+ ssr.getCarrierCode().trim() + MessageComposerConstants.SP;
					if (codeSSR.equals(TypeBMessageConstants.SSRCodes.TKNE)
							|| codeSSR.equals(TypeBMessageConstants.SSRCodes.TKNR)) {
						ssr2 = ssr.getAdviceOrStatusCode().trim() + ssr.getNoOfPax() + MessageComposerConstants.SP
								+ ssrSegment.getDepartureStation().trim() + ssrSegment.getDestinationStation().trim()
								+ MessageComposerConstants.SP + ssrSegment.getFlightNumber().trim() + ssrSegment.getBookingCode().substring(0,1)
								+ CalendarUtil.getSegmentDayMonth(ssrSegment.getDepartureDate());
					} else {
						ssr2 = ssr.getAdviceOrStatusCode().trim() + ssr.getNoOfPax() + MessageComposerConstants.SP
								+ ssrSegment.getDepartureStation().trim() + ssrSegment.getDestinationStation().trim()
								+ ssrSegment.getFlightNumber().trim() + ssrSegment.getBookingCode().substring(0,1)
								+ CalendarUtil.getSegmentDayMonth(ssrSegment.getDepartureDate());						
					}
					if (ssrNameList != null && !ssrNameList.isEmpty()) {
						ssr2 += HYPHEN + MessageComposerUtil.buildNameElement(ssrNameList);
					}
					if (ssr.getFreeText() != null && ssr.getFreeText() != "") {
						ssr2 += POINT + ssr.getFreeText().trim();
					}
					// check for length
					if ((ssr1 + ssr2).trim().length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

						List slist = MessageComposerUtil.getSSRElemenets(codeSSR, ssr.getCarrierCode().trim(), ssr2);
						for (Iterator iterator = slist.iterator(); iterator.hasNext();) {
							ssrElementList.add(((String) iterator.next()).trim());
						}
					} else { // element fit in normal type B lentgh
						ssrElementList.add((ssr1 + ssr2).trim());
					}

				} else if (codeSSR.equals(TypeBMessageConstants.SSRCodes.OTHS)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.CKIN)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.GRPF)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.GRPS)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.MEDA)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.GPST)
						|| codeSSR.equals(TypeBMessageConstants.SSRCodes.TICKETING_TIME_LIMIT)) { // OTHS|CKIN|GRPF|GRPS|MEDA|GPST
																					// special handlle

					ssr1 = SSR + MessageComposerConstants.SP + codeSSR + MessageComposerConstants.SP
							+ ssr.getCarrierCode().trim() + MessageComposerConstants.SP;
					ssr2 = ssr.getSsrValue();

					if ((ssr1 + ssr2).trim().length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

						List slist = MessageComposerUtil.getSSRElemenets(codeSSR, ssr.getCarrierCode().trim(), ssr2);
						for (Iterator iterator = slist.iterator(); iterator.hasNext();) {
							ssrElementList.add(((String) iterator.next()).trim());
						}
					} else { // element fit in normal type B lentgh
						ssrElementList.add((ssr1 + ssr2).trim());
					}

				} else {
					// TODO throw exception
					// SSR segmenet null
					return null;
				}

			}

		}

		return ssrElementList;
	}

	/**
	 * Method will process each SSR DTO and will construct possible Elements.
	 * 
	 * @param ssrDocsDTOs
	 * @return
	 */
	public static List<String> composeSSRDocsElements(List<SSRDocsDTO> ssrDocsDTOs) {

		List<String> ssrElementList = new ArrayList<String>();
		for (Iterator<SSRDocsDTO> iter = ssrDocsDTOs.iterator(); iter.hasNext();) {
			SSRDocsDTO ssr = iter.next();
			String ssr1 = "";
			String ssr2 = "";
			String codeSSR = ssr.getCodeSSR().trim();

			List<NameDTO> ssrNameList = ssr.getPnrNameDTOs();
			ssr1 = SSR + MessageComposerConstants.SP + codeSSR + MessageComposerConstants.SP + ssr.getCarrierCode().trim()
					+ MessageComposerConstants.SP;
			ssr2 = ssr.getAdviceOrStatusCode().trim() + ssr.getNoOfPax() + MessageComposerConstants.FWD + ssr.getDocType()
					+ MessageComposerConstants.FWD + nullHandler(ssr.getDocCountryCode()) + MessageComposerConstants.FWD
					+ nullHandler(ssr.getDocNumber()) + MessageComposerConstants.FWD + nullHandler(ssr.getPassengerNationality())
					+ MessageComposerConstants.FWD
					+ (ssr.getPassengerDateOfBirth() == null ? "" : CalendarUtil.getAge(ssr.getPassengerDateOfBirth()))
					+ MessageComposerConstants.FWD + nullHandler(ssr.getPassengerGender()) + MessageComposerConstants.FWD
					+ (ssr.getDocExpiryDate() == null ? "" : CalendarUtil.getAge(ssr.getDocExpiryDate()))
					+ MessageComposerConstants.FWD + nullHandler(ssr.getDocFamilyName()) + MessageComposerConstants.FWD
					+ nullHandler(ssr.getDocGivenName());
			if (ssrNameList != null && !ssrNameList.isEmpty()) {
				ssr2 += HYPHEN + MessageComposerUtil.buildNameElement(ssrNameList);
			}
			if (ssr.getFreeText() != null && ssr.getFreeText() != "") {
				ssr2 += POINT + ssr.getFreeText().trim();
			}
			// check for length
			if ((ssr1 + ssr2).trim().length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List<String> slist = MessageComposerUtil.getSSRElemenets(codeSSR, ssr.getCarrierCode().trim(), ssr2);
				for (Iterator<String> iterator = slist.iterator(); iterator.hasNext();) {
					ssrElementList.add(iterator.next().trim());
				}
			} else { // element fit in normal type B lentgh
				ssrElementList.add((ssr1 + ssr2).trim());
			}
		}
		return ssrElementList;
	}
	
	/**
	 * Method will process each SSR DTO and will construct possible Elements.
	 * 
	 * @param ssrDocoDTOs
	 * @return
	 */
	public static List<String> composeSSRDocoElements(List<SSRDocoDTO> ssrDocoDTOs) {

		List<String> ssrElementList = new ArrayList<String>();
		for (Iterator<SSRDocoDTO> iter = ssrDocoDTOs.iterator(); iter.hasNext();) {
			SSRDocoDTO ssr = iter.next();
			String ssr1 = "";
			String ssr2 = "";
			String codeSSR = ssr.getCodeSSR().trim();

			List<NameDTO> ssrNameList = ssr.getPnrNameDTOs();
			ssr1 = SSR + MessageComposerConstants.SP + codeSSR + MessageComposerConstants.SP + ssr.getCarrierCode().trim()
					+ MessageComposerConstants.SP;
			ssr2 = ssr.getAdviceOrStatusCode().trim() + ssr.getNoOfPax() + MessageComposerConstants.FWD + nullHandler(ssr.getPlaceOfBirth()) 
					+ MessageComposerConstants.FWD + ssr.getTravelDocType() + MessageComposerConstants.FWD
					+ nullHandler(ssr.getVisaDocNumber()) + MessageComposerConstants.FWD + nullHandler(ssr.getVisaDocPlaceOfIssue())
					+ MessageComposerConstants.FWD + (ssr.getVisaDocIssueDate() == null ? "" : CalendarUtil.getAge(ssr.getVisaDocIssueDate()))
					+ MessageComposerConstants.FWD +  nullHandler(ssr.getVisaApplicableCountry())
					+ MessageComposerConstants.FWD + (ssr.isInfant() == true ? "I" : "");
			if (ssrNameList != null && !ssrNameList.isEmpty()) {
				ssr2 += HYPHEN + MessageComposerUtil.buildNameElement(ssrNameList);
			}
			if (ssr.getFreeText() != null && ssr.getFreeText() != "") {
				ssr2 += POINT + ssr.getFreeText().trim();
			}
			// check for length
			if ((ssr1 + ssr2).trim().length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List<String> slist = MessageComposerUtil.getSSRElemenets(codeSSR, ssr.getCarrierCode().trim(), ssr2);
				for (Iterator<String> iterator = slist.iterator(); iterator.hasNext();) {
					ssrElementList.add(iterator.next().trim());
				}
			} else { // element fit in normal type B lentgh
				ssrElementList.add((ssr1 + ssr2).trim());
			}
		}
		return ssrElementList;
	}

	public static List composeOSIElements(List<OSIDTO> osiDTOs, List unchangedNameDTOs) {

		List<OSIDTO> osiTOs = new ArrayList<OSIDTO>();
		for (OSIDTO osiDTO : osiDTOs) {
			if (osiDTO.getCodeOSI() != null && osiDTO.getCodeOSI().length() > 3
					&& osiDTO.getCodeOSI().substring(0, 3).equals(GDSExternalCodes.OSICodes.REMAINING_PASSENGERS.getCode())) {

				if (unchangedNameDTOs != null && !unchangedNameDTOs.isEmpty()) {
					List unchangedNameList = ((UnchangedNamesDTO) unchangedNameDTOs.get(0)).getUnChangedNames();
					String unChangedNames = MessageComposerUtil.buildNameElement(unchangedNameList);
					if (unChangedNames != null && !unChangedNames.equals("")) {
						if (unChangedNames.length() + 8 > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
							List<String> slist = MessageComposerUtil.getOSIElemenets(unChangedNames);
							for (Iterator<String> iterator = slist.iterator(); iterator.hasNext();) {
								OSIDTO osiTO = cloneOSIDTO(osiDTO);
								osiTO.setOsiValue(iterator.next().trim());
								osiTOs.add(osiTO);
							}
						} else {
							osiDTO.setOsiValue(unChangedNames);
							osiTOs.add(osiDTO);
						}
					}
				}
			} else if (osiDTO instanceof OSIPhoneNoDTO) {
				osiDTO.setOsiValue(((OSIPhoneNoDTO) osiDTO).getPhoneNo());
				osiTOs.add(osiDTO);
			} else if (osiDTO instanceof OSIEmailDTO) {
				osiDTO.setOsiValue(((OSIEmailDTO) osiDTO).getEmail());
				osiTOs.add(osiDTO);
			} else if (osiDTO instanceof OSIAddressDTO) {
				String osiValue = "";
				OSIAddressDTO addressDto = (OSIAddressDTO) osiDTO;
				if (addressDto.getCity() != null) {
					osiValue = osiValue + addressDto.getCity() + " ";
				}
				if (addressDto.getAddressLineOne() != null) {
					osiValue = osiValue + addressDto.getAddressLineOne() + " ";
				}
				if (addressDto.getAddressLineTwo() != null) {
					osiValue = osiValue + addressDto.getAddressLineTwo();
				}
				osiDTO.setOsiValue(osiValue);
				osiTOs.add(osiDTO);
			} else {
				osiTOs.add(osiDTO);
			}
		}
		return osiTOs;
	}
	
	public static OSIDTO cloneOSIDTO(OSIDTO osiDTO) {
		OSIDTO osiDTOCopy = new OSIDTO();
		osiDTOCopy.setCarrierCode(osiDTO.getCarrierCode());
		osiDTOCopy.setCodeOSI(osiDTO.getCodeOSI());
		osiDTOCopy.setFullElement(osiDTO.getFullElement());
		osiDTOCopy.setOsiValue(osiDTO.getOsiValue());
		return osiDTOCopy;
	}

	public static List composeEmergencyDetail(List<SSREmergencyContactDetailDTO> emergencyContactDetailDTOs) {

		// TODO change getAction_status_Code to ADVICE CODE****************
		List<String> emElementList = new ArrayList<String>();
		String em1 = "";
		String em2 = "";
		for (Iterator iter = emergencyContactDetailDTOs.iterator(); iter.hasNext();) {

			SSREmergencyContactDetailDTO em = (SSREmergencyContactDetailDTO) iter.next();
			em1 = "";
			em2 = "";
			// TODO complete this
			em1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.PCTC + MessageComposerConstants.SP
					+ em.getCarrierCode() + MessageComposerConstants.SP;
			em2 = em.getAdviceOrStatusCode();
			if (!em.isRefused()) {

				em2 += MessageComposerConstants.FWD + em.getEmContactName() + MessageComposerConstants.FWD
						+ em.getEmContactNumberwithCountry();
				if (em.getPassengerFirstName() != null && em.getPassengerLastName() != null) {
					em2 += HYPHEN + "1" + em.getPassengerLastName() + MessageComposerConstants.FWD + em.getPassengerFirstName();
				}
				if (em.getFreeText() != null && em.getFreeText() != "") {
					em2 += POINT + em.getFreeText();
				}
			} else {
				em2 += MessageComposerConstants.FWD + MessageComposerConstants.FWD;
				if (em.getPassengerFirstName() != null && em.getPassengerLastName() != null) {
					em2 += HYPHEN + "1" + em.getPassengerLastName() + MessageComposerConstants.FWD + em.getPassengerFirstName();
				}
				if (em.getFreeText() != null && !em.getFreeText().equals("")) {
					em2 += POINT + em.getFreeText();
				}
			}

			if ((em1 + em2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List elist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.PCTC, em.getCarrierCode().trim(),
						em2);
				for (Iterator iterator = elist.iterator(); iterator.hasNext();) {
					emElementList.add((String) iterator.next());
				}
			} else { // element fit in normal type B lentgh
				emElementList.add((em1 + em2));

			}

		}
		return emElementList;
	}

	/**
	 * compose infantDTO's into elements.
	 * 
	 * @param infantDTOs
	 * @return
	 */
	public static List composeInfantElements(List<SSRInfantDTO> infantDTOs) {

		List<String> infantElementList = new ArrayList<String>();
		String inf1 = "";
		String inf2 = "";
		String iFName;
		String iLNmae;
		String iTitle;
		String iPFName;
		String iPLName;
		String iPTitle;
		Date iDob;

		for (Iterator iter = infantDTOs.iterator(); iter.hasNext();) {

			inf1 = "";
			inf2 = "";
			SSRInfantDTO inf = (SSRInfantDTO) iter.next();
			iFName = inf.getInfantFirstName();
			iLNmae = inf.getInfantLastName();
			iTitle = inf.getInfantTitle();
			iPFName = inf.getGuardianFirstName();
			iPLName = inf.getGuardianLastName();
			iPTitle = inf.getGuardianTitle();
			iDob = inf.getInfantDateofBirth();
			SegmentDTO infSegment = (SegmentDTO) inf.getSegmentDTO();

			if (infSegment != null) {

				if (inf.isSeatRequired()) {

					if (iPLName != null && !iPLName.equals("")) {

						inf1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.INFT
								+ MessageComposerConstants.SP + inf.getCarrierCode() + MessageComposerConstants.SP;
						inf2 = inf.getAdviceOrStatusCode() + "1" + MessageComposerConstants.SP
								+ infSegment.getDepartureStation().trim() + infSegment.getDestinationStation().trim()
								+ infSegment.getFlightNumber().trim() + infSegment.getBookingCode().trim()
								+ CalendarUtil.getSegmentDayMonth(infSegment.getDepartureDate()) + "-1" + iLNmae
								+ MessageComposerConstants.FWD + iFName;
						if (iTitle != null && !iTitle.equals("")) {
							inf2 += iTitle.trim();
						}
						inf2 += POINT + iPFName + MessageComposerConstants.FWD + iPLName;
						if (iPTitle != null && !iPTitle.equals("")) {
							inf2 += iPTitle.trim();
						}
						inf2 += MessageComposerConstants.SP + OCCUPY_SEAT_POSTFIX;
						if ((inf1 + inf2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

							List ilist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.INFT,
									inf.getCarrierCode(), inf2);
							for (Iterator iterator = ilist.iterator(); iterator.hasNext();) {
								infantElementList.add((String) iterator.next());
							}
						} else { // element fit in normal type B length
							infantElementList.add((inf1 + inf2));

						}

					} else {

						inf1 = SSR + TypeBMessageConstants.SSRCodes.INFT + inf.getCarrierCode() + MessageComposerConstants.SP;

						inf2 = inf.getAdviceOrStatusCode() + "1" + infSegment.getDepartureStation().trim()
								+ infSegment.getDestinationStation().trim() + infSegment.getFlightNumber().trim()
								+ infSegment.getBookingCode().trim()
								+ CalendarUtil.getSegmentDayMonth(infSegment.getDepartureDate()) + "-1"
								+ MessageComposerConstants.FWD + iLNmae + iFName;
						if (iTitle != null && !iTitle.equals("")) {
							inf2 += iTitle.trim();
						}
						if (iDob != null) {
							SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyy");
							String infantDOB = formatter.format(iDob);
							inf2 += infantDOB.toUpperCase() + MessageComposerConstants.SP + OCCUPY_SEAT_POSTFIX;
						} else {
							inf2 += inf.getInfantAge() + MTHS + MessageComposerConstants.SP + OCCUPY_SEAT_POSTFIX;
						}

						if ((inf1 + inf2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

							List ilist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.INFT,
									inf.getCarrierCode(), inf2);
							for (Iterator iterator = ilist.iterator(); iterator.hasNext();) {
								infantElementList.add((String) iterator.next());
							}
						} else { // element fit in normal type B length
							infantElementList.add((inf1 + inf2));

						}

					}

				} else { // infant without seats

					inf1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.INFT + MessageComposerConstants.SP
							+ inf.getCarrierCode() + MessageComposerConstants.SP;
					inf2 = inf.getAdviceOrStatusCode() + "1" + MessageComposerConstants.SP
							+ infSegment.getDepartureStation().trim() + infSegment.getDestinationStation().trim()
							+ MessageComposerConstants.SP + infSegment.getFlightNumber().trim()
							+ infSegment.getBookingCode().trim() + CalendarUtil.getSegmentDayMonth(infSegment.getDepartureDate());

					if (iPLName != null && iPFName != null) {
						inf2 += HYPHEN + "1" + iPLName + MessageComposerConstants.FWD + iPFName;
					}

					if (iPTitle != null && !iPTitle.equals("")) {
						inf2 += iPTitle.trim();
					}
					inf2 += POINT + iLNmae + MessageComposerConstants.FWD + iFName;
					if (iTitle != null && !iTitle.equals("")) {
						inf2 += iTitle.trim();
					}
					if (iDob != null) {
						SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyy");
						String infantDOB = formatter.format(iDob);
						inf2 = inf2 + MessageComposerConstants.SP + infantDOB.toUpperCase();
					} else {
						inf2 = inf2 + MessageComposerConstants.SP + inf.getInfantAge() + MTHS;
					}

					if (inf.getFreeText() != null && !inf.getFreeText().equals("")) {
						inf2 += inf.getFreeText();
					}
					if ((inf1 + inf2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

						List ilist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.INFT,
								inf.getCarrierCode(), inf2);
						for (Iterator iterator = ilist.iterator(); iterator.hasNext();) {
							infantElementList.add((String) iterator.next());
						}
					} else { // element fit in normal type B length
						infantElementList.add((inf1 + inf2));

					}

				}

			}
		}
		return infantElementList;
	}

	/**
	 * @return foid elements
	 */
	public static List composeFoidElements(List<SSRFoidDTO> foidDTOs) {

		// TODO change getAction_status_Code to ADVICE CODE****************
		List<String> foidElementList = new ArrayList<String>();
		String foid1 = "";
		String foid2 = "";
		for (Iterator iter = foidDTOs.iterator(); iter.hasNext();) {

			SSRFoidDTO foid = (SSRFoidDTO) iter.next();
			foid1 = "";
			foid2 = "";

			foid1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.FOID + MessageComposerConstants.SP
					+ foid.getCarrierCode() + MessageComposerConstants.SP;
			foid2 = foid.getAdviceOrStatusCode().trim() + "1" + MessageComposerConstants.FWD
					+ foid.getPassengerIdentificationType() + foid.getPassengerIdentificationNo();
			if (foid.getPnrNameDTOs() != null && !foid.getPnrNameDTOs().isEmpty()) {
				foid2 += HYPHEN + MessageComposerUtil.buildNameElement(foid.getPnrNameDTOs());
			}

			if ((foid1 + foid2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List flist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.FOID, foid.getCarrierCode()
						.trim(), foid2);
				for (Iterator iterator = flist.iterator(); iterator.hasNext();) {
					foidElementList.add((String) iterator.next());
				}
			} else { // element fit in normal type B lentgh
				foidElementList.add((foid1 + foid2));
			}

		}

		return foidElementList;
	}

	/**
	 * create pssport elements
	 * 
	 * @param passportDTOs
	 * @return
	 */
	public static List composePassportElements(List<SSRPassportDTO> passportDTOs) {

		// TODO change getAction_status_Code to ADVICE CODE****************
		List<String> passportElementList = new ArrayList<String>();
		String pspt1 = "";
		String pspt2 = "";
		for (Iterator iter = passportDTOs.iterator(); iter.hasNext();) {

			SSRPassportDTO pspt = (SSRPassportDTO) iter.next();
			pspt1 = "";
			pspt2 = "";

			pspt1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.PSPT + MessageComposerConstants.SP;
			pspt2 = pspt.getAdviceOrStatusCode().trim() + "1" + MessageComposerConstants.FWD
					+ pspt.getPassengerPassportNo().trim() + MessageComposerConstants.FWD + pspt.getPassportCountryCode().trim()
					+ MessageComposerConstants.FWD + CalendarUtil.getAge(pspt.getPassengerDateOfBirth())
					+ MessageComposerConstants.FWD + pspt.getPassengerFamilyName().trim() + MessageComposerConstants.FWD
					+ pspt.getPassengerGivenName().trim() + MessageComposerConstants.FWD + pspt.getPassengerGender().trim();
			if (pspt.isMultiPassengerPassport()) {
				pspt2 += MessageComposerConstants.FWD + "H";
			}
			if (pspt.getPnrNameDTOs() != null && !pspt.getPnrNameDTOs().isEmpty()) {
				pspt2 += HYPHEN + MessageComposerUtil.buildNameElement(pspt.getPnrNameDTOs());
			}
			if (pspt.getFreeText() != null && !pspt.getFreeText().equals("")) {
				pspt2 += POINT + pspt.getFreeText();
			}

			if ((pspt1 + pspt2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List plist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.PSPT, pspt.getCarrierCode()
						.trim(), pspt2);
				for (Iterator iterator = plist.iterator(); iterator.hasNext();) {
					passportElementList.add((String) iterator.next());
				}
			} else { // element fit in normal type B lentgh
				passportElementList.add((pspt1 + pspt2));
			}

		}

		return passportElementList;
	}

	/**
	 * @param NameDTO
	 *            List
	 * @return String Possible name string Correspond to NameDTO List received
	 */
	public static String buildNameElement(List nameList) {

		List<String> arrList = new ArrayList<String>();
		String possibleNameString = "";
		try {
			for (int i = 0; i < nameList.size(); i++) {

				NameDTO nameDTO = (NameDTO) nameList.get(i);
				String lastName = nameDTO.getLastName();
				String firstName = nameDTO.getFirstName();
				String title = nameDTO.getPaxTitle();

				if (title == null) {
					title = "";
				}
				String possibleName = "";
				if (lastName == null || lastName.equals("")) {
					if (firstName != null && !firstName.equals("") && nameDTO.getGroupMagnitude() > 9) {
						// TODO saber grouplimit so check it before remove , and check with
						// actual message if group has been reduce to less than 9 due to changes - eg delete names
						possibleName = "";
						possibleNameString += nameDTO.getGroupMagnitude() + firstName + " ";

					}

				} else {

					possibleName = lastName + "/" + firstName + title;
				}

				if (possibleName != null && !possibleName.equals("")) {
					arrList.add(possibleName.trim());
				}

			}

			LinkedHashMap hashMap = new LinkedHashMap();
			for (int i = 0; i < arrList.size(); i++) {

				String str = (String) arrList.get(i);
				String name_Title = str.substring(str.indexOf("/") + 1);
				String lastName = str.substring(0, str.indexOf("/"));

				if (hashMap.containsKey(lastName)) {

					ArrayList arr = (ArrayList) hashMap.get(lastName);
					arr.add(name_Title);
					hashMap.put(lastName, arr);

				} else {

					ArrayList arrayList = new ArrayList();
					arrayList.add(name_Title);
					hashMap.put(lastName, arrayList);
				}

			}

			Set set = hashMap.keySet();

			String element = "";
			for (Iterator iter = set.iterator(); iter.hasNext();) {

				String lastname = (String) iter.next();
				ArrayList firstNameList = (ArrayList) hashMap.get(lastname);
				element = "";
				for (Iterator iterator = firstNameList.iterator(); iterator.hasNext();) {
					element += "/" + (String) iterator.next();

				}
				possibleNameString = possibleNameString + firstNameList.size() + lastname + element + " ";

			}
			return possibleNameString.trim();
		} catch (Exception e) {
			// TODO: handle exception
			// Incorrect NameDto's Received
			return "";
		}

	}

	/**
	 * SSRChildDTO handle.
	 * 
	 * @return child element list
	 */
	public static List composeChildElements(List<SSRChildDTO> childDTOs) {

		// TODO change getAction_status_Code to ADVICE CODE****************
		List<String> childElementList = new ArrayList<String>();
		String child1;
		String child2;
		for (Iterator iter = childDTOs.iterator(); iter.hasNext();) {

			SSRChildDTO child = (SSRChildDTO) iter.next();
			child1 = "";
			child2 = "";

			child1 = SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.CHLD + MessageComposerConstants.SP
					+ child.getCarrierCode() + MessageComposerConstants.SP;
			child2 = child.getAdviceOrStatusCode().trim() + "1";

			if (child.getDateOfBirth() != null) {
				child2 += MessageComposerConstants.FWD + CalendarUtil.getAge(child.getDateOfBirth());
			}

			child2 += HYPHEN + "1" + child.getLastName() + MessageComposerConstants.FWD + child.getFirstName();

			if (child.getTitle() != null && !child.getTitle().equals("")) {
				child2 += child.getTitle().trim();
			}

			if ((child1 + child2).length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {

				List flist = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.CHLD, child.getCarrierCode()
						.trim(), child2);
				for (Iterator iterator = flist.iterator(); iterator.hasNext();) {
					childElementList.add((String) iterator.next());
				}
			} else { // element fit in normal type B lentgh
				childElementList.add((child1 + child2));
			}

		}

		return childElementList;
	}

	/**
	 * compose ssr elements.
	 * 
	 * @return
	 */
	public static List composeSSRs(List<SSRDTO> ssrDTOs, String bookingOffice, String CarrierCode) {

		List<String> ssrElementList = new ArrayList<String>();
		String ssr1;
		String ssr2;
		String airlineDesignator = "";
		if (bookingOffice != null && bookingOffice != "") {
			airlineDesignator = bookingOffice.substring(3);
		} else if (CarrierCode != null && CarrierCode != "") {
			airlineDesignator = CarrierCode;
		}

		// TODO continuation does not handled here since saber does not allow to exceed max length
		// !ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.OTHS
		for (Iterator iter = ssrDTOs.iterator(); iter.hasNext();) {
			SSRDTO ssrDTO = (SSRDTO) iter.next();
			if (ssrDTO.getCodeSSR() != null && !ssrDTO.getCodeSSR().equals("")) {

				ssr1 = SSR + MessageComposerConstants.SP + ssrDTO.getCodeSSR() + MessageComposerConstants.SP + airlineDesignator
						+ MessageComposerConstants.SP + ssrDTO.getSsrValue();
				if (ssr1 != null && ssr1.length() > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
					List ssrList;
					if (ssrDTO.getCarrierCode() == null) {
						ssrList = getSSRElemenets(ssrDTO.getCodeSSR(), airlineDesignator, ssrDTO.getSsrValue().toUpperCase());
					} else {
						ssrList = getSSRElemenets(ssrDTO.getCodeSSR(), airlineDesignator, ssrDTO.getSsrValue().toUpperCase());

					}

					for (Iterator iterator = ssrList.iterator(); iterator.hasNext();) {
						ssrElementList.add((String) iterator.next());
					}

					// ssrElementList.addAll(aa);
				} else {

					ssrElementList.add(ssr1);
				}
			}
		}

		return ssrElementList;
	}

	/**
	 * Method will generate SSR elements reply to the receiving parameters . method invokes only if no of characters
	 * exceeds 69.
	 * 
	 * @param codeSSR
	 * @param carrierCode
	 * @param restString
	 * @return list contains SSR elements .
	 */
	public static List<String> getSSRElemenets(String codeSSR, String carrierCode, String restString) {

		int inialLength = codeSSR.length() + carrierCode.length() + 6;
		String prefix = "SSR " + codeSSR + " " + carrierCode + " ";
		String conPrefix = prefix + "///";

		List<String> arrayList = new ArrayList<String>();
		int strLength = restString.length();

		int length = inialLength + strLength;

		if (length > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
			boolean flag = false;
			int maxTextLenght = MessageComposerConstants.LINE_CHARACTER_LENGTH - inialLength;
			int contTextLenght = maxTextLenght - 3;
			int index1 = maxTextLenght;
			String substing1 = restString.substring(0, index1);

			index1 = getContinuationIndex(substing1);
			if (index1 == 0) {
				index1 = maxTextLenght;
			}

			String st1 = prefix + restString.substring(0, index1);
			arrayList.add(st1);
			restString = restString.substring(index1);

			if (restString.length() > 0) {
				flag = true;
				int index;
				while (flag) {
					if (restString.length() >= contTextLenght) {
						index = contTextLenght;
						String subString = restString.substring(0, index);
						index = getContinuationIndex(subString);
						if (index == 0) {
							index = contTextLenght;
						}

						String str2 = conPrefix + restString.substring(0, index);
						arrayList.add(str2);
						restString = restString.substring(index);
					} else {
						String str2 = conPrefix + restString;
						arrayList.add(str2);
						flag = false;
					}

				}
			}

		}
		return arrayList;

	}
	
	public static List<String> getOSIElemenets(String restString) {

		int inialLength = 12; //OSI W5 TCP12
		String prefix = " ";
		String conPrefix = prefix + "///";

		List<String> arrayList = new ArrayList<String>();
		int strLength = restString.length();

		int length = inialLength + strLength;

		if (length > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
			boolean flag = false;
			int maxTextLenght = MessageComposerConstants.LINE_CHARACTER_LENGTH - inialLength;
			int contTextLenght = maxTextLenght - 3;
			int index1 = maxTextLenght;
			String substing1 = restString.substring(0, index1);

			index1 = getContinuationIndex(substing1);
			if (index1 == 0) {
				index1 = maxTextLenght;
			}

			String st1 = prefix + restString.substring(0, index1);
			arrayList.add(st1);
			restString = restString.substring(index1);

			if (restString.length() > 0) {
				flag = true;
				int index;
				while (flag) {
					if (restString.length() >= contTextLenght) {
						index = contTextLenght;
						String subString = restString.substring(0, index);
						index = getContinuationIndex(subString);
						if (index == 0) {
							index = contTextLenght;
						}

						String str2 = conPrefix + restString.substring(0, index);
						arrayList.add(str2);
						restString = restString.substring(index);
					} else {
						String str2 = conPrefix + restString;
						arrayList.add(str2);
						flag = false;
					}

				}
			}

		}
		return arrayList;

	}

	private static int getContinuationIndex(String substing) {

		int index = 0;
		if (!substing.endsWith("/") || !substing.endsWith(" ") || !substing.endsWith(".")) {
			int a = substing.lastIndexOf("/");
			int b = substing.lastIndexOf(" ");
			int c = substing.lastIndexOf(".");

			if (a > b) {
				if (a > c) {
					index = a;
				} else {
					index = c;
				}
			} else {
				if (b > c) {
					index = b;
				} else {
					index = c;
				}
			}

		}
		return index;
	}

	/**
	 * Method build Pax Name element lines
	 * 
	 * @param oldNameList
	 * @return List contains String of names.
	 */
	public static List buildPaxNameElement(List nameList) {

		// List<List<String>> listOfGroups = new ArrayList<List<String>>();
		LinkedHashMap tempMap = new LinkedHashMap();
		List<String> arrList = new ArrayList<String>();
		String possibleNameString = "";
		try {
			for (int i = 0; i < nameList.size(); i++) {

				NameDTO nameDTO = (NameDTO) nameList.get(i);
				String lastName = nameDTO.getLastName();
				String firstName = nameDTO.getFirstName();
				String title = nameDTO.getPaxTitle();
				int familyID = nameDTO.getFamilyID();

				if (title == null) {
					title = "";
				}
				String possibleName = "";
				if (lastName == null || lastName.equals("")) {
					if (firstName != null && !firstName.equals("") && nameDTO.getGroupMagnitude() > 9) {
						// TODO saber grouplimit so check it before remove , and check with
						// actual message if group has been reduce to less than 9 due to changes - eg delete names
						possibleName = "";
						possibleNameString += nameDTO.getGroupMagnitude() + firstName + " ";

					}

				} else {

					possibleName = lastName + "/" + firstName + title;
				}

				if (tempMap.containsKey(familyID)) {
					ArrayList<String> arrayList = (ArrayList<String>) tempMap.get(familyID);
					arrayList.add(possibleName);
				} else {
					ArrayList<String> newList = new ArrayList<String>();
					newList.add(possibleName);
					tempMap.put(familyID, newList);
				}

			}

			Set tempMapKeySet = tempMap.keySet();

			LinkedHashMap hashMap = new LinkedHashMap();

			for (Iterator iter = tempMapKeySet.iterator(); iter.hasNext();) {
				Integer tempFamilyID = (Integer) iter.next();
				ArrayList<String> temparrList = (ArrayList<String>) tempMap.get(tempFamilyID);
				if (temparrList != null) {
					for (int j = 0; j < temparrList.size(); j++) {
						String str = (String) temparrList.get(j);
						String name_Title = str.substring(str.indexOf("/") + 1);
						String lastName = str.substring(0, str.indexOf("/"));

						if (hashMap.containsKey(tempFamilyID)) {

							if (tempFamilyID.intValue() > 0) {
								ArrayList arr = (ArrayList) hashMap.get(tempFamilyID);
								arr.add(name_Title);
								hashMap.put(tempFamilyID, arr);
							} else {
								ArrayList arr = (ArrayList) hashMap.get(tempFamilyID);
								arr.add(str);
								hashMap.put(tempFamilyID, arr);
							}
						} else {

							if (tempFamilyID.intValue() > 0) {
								ArrayList arrayList = new ArrayList();
								arrayList.add(lastName);
								arrayList.add(name_Title);
								hashMap.put(tempFamilyID, arrayList);
							} else {
								ArrayList arrayList = new ArrayList();
								arrayList.add(str);
								hashMap.put(tempFamilyID, arrayList);
							}

						}
					}
				}
			}

			Set set = hashMap.keySet();
			int keySetSize = set.size();
			int loopNumber = 0;
			ArrayList<String> arrayList = new ArrayList<String>();

			String element = "";
			for (Iterator iter = set.iterator(); iter.hasNext();) {

				loopNumber++;
				Integer familyID = (Integer) iter.next();
				ArrayList namesList = (ArrayList) hashMap.get(familyID);
				element = "";
				String newlyAddingNameElement = "";
				if (familyID.intValue() > 0) {
					String familyName = (String) namesList.get(0);
					for (int i = 1; i < namesList.size(); i++) {
						element += "/" + namesList.get(i);
					}
					newlyAddingNameElement = (namesList.size() - 1) + familyName + element + " ";
					int nameStringLength = possibleNameString.length() + newlyAddingNameElement.length();
					if (nameStringLength > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
						arrayList.add(possibleNameString.trim());
						possibleNameString = "";
						possibleNameString = newlyAddingNameElement;
					} else {
						possibleNameString = possibleNameString + newlyAddingNameElement;
					}

				} else {
					for (int j = 0; j < namesList.size(); j++) {
						newlyAddingNameElement = "1" + namesList.get(j) + " ";
						int nameStringLength = possibleNameString.length() + newlyAddingNameElement.length();
						if (nameStringLength > MessageComposerConstants.LINE_CHARACTER_LENGTH) {
							arrayList.add(possibleNameString.trim());
							possibleNameString = "";
							possibleNameString = newlyAddingNameElement;
						} else {
							possibleNameString = possibleNameString + newlyAddingNameElement;
						}
					}
				}
				if (keySetSize == loopNumber && possibleNameString != null && possibleNameString.trim() != "") {
					arrayList.add(possibleNameString.trim());
				}
			}

			return arrayList;

		} catch (Exception e) {
			// TODO: handle exception
			// Incorrect NameDto's Received
			return null;
		}

	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param string
	 * @return
	 */
	private static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	public static TypeBMessageStructure.MessageConfig getSyncMessageType(int typeBSyncType,
	                                                                     GDSExternalCodes.SyncMessageIdentifier syncMessageIdentifier) {
		TypeBMessageStructure.MessageConfig messageConfig = null;

		MsgBrokerModuleConfig msgBrokerModuleConfig = MsgbrokerUtils.getMsgbrokerConfigs();
		TypeBMessageStructure typeBMessageStructure = msgBrokerModuleConfig.getTypeBMessageStructure();
		GDSInternalCodes.TypeBSyncType syncType = GDSInternalCodes.TypeBSyncType.getTypeBSyncType(typeBSyncType);

		l_1:
		for (TypeBMessageStructure.SyncMessageType syncMessageType : typeBMessageStructure.getSyncMessageTypes()) {
			if (syncMessageType.getSyncType() == syncType) {

				for (TypeBMessageStructure.SyncMessageConfig config : syncMessageType.getSyncMsgConfigs()) {
					if (config.getSyncMessageIdentifier() == syncMessageIdentifier) {
						messageConfig = config.getMessageConfig();
						break  l_1;
					}
				}
			}
		}

		return messageConfig;
	}
	
	public static TypeBMessageStructure.MessageConfig getMessageConfig(int typeBSyncType, String messageIdentifier) {
		TypeBMessageStructure.MessageConfig messageConfig = null;

		MsgBrokerModuleConfig msgBrokerModuleConfig = MsgbrokerUtils.getMsgbrokerConfigs();
		TypeBMessageStructure typeBMessageStructure = msgBrokerModuleConfig.getTypeBMessageStructure();

		l_1: for (TypeBMessageStructure.ResMessageType resMessageType : typeBMessageStructure.getResMessageTypes()) {
			for (TypeBMessageStructure.ResMessageConfig config : resMessageType.getResMsgConfigs()) {
				messageConfig = config.getMessageConfig();
				break l_1;
			}
		}

		return messageConfig;
	}
	
	public static TypeBMessageStructure.MessageConfig getCodeshareMessageConfig(String messageIdentifier) {
		TypeBMessageStructure.MessageConfig messageConfig = null;

		MsgBrokerModuleConfig msgBrokerModuleConfig = MsgbrokerUtils.getMsgbrokerConfigs();
		TypeBMessageStructure typeBMessageStructure = msgBrokerModuleConfig.getTypeBMessageStructure();

		l_1: for (TypeBMessageStructure.CodeshareMessageType codeshareMessageType : typeBMessageStructure.getCodeshareMessageTypes()) {
			for (TypeBMessageStructure.ResMessageConfig config : codeshareMessageType.getResMsgConfigs()) {
				messageConfig = config.getMessageConfig();
				break l_1;
			}
		}

		return messageConfig;
	}

	public static String getMessagingTimeMode(String timeMode) {
		if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(timeMode)) {
			return TimeMode.LOCAL_TIME_MODE;
		} else {
			return TimeMode.ZULU_TIME_MODE;
		}
	}
}
