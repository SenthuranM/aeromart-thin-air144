/**
 * 
 */
package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.etl.ETLBookingClassList;
import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.airreservation.api.dto.etl.ETLPaxInfo;
import com.isa.thinair.airreservation.api.dto.etl.ETLPaxTO;
import com.isa.thinair.airreservation.api.dto.etl.EtlCabinClassList;
import com.isa.thinair.airreservation.api.dto.etl.PassengerBookingToETL;
import com.isa.thinair.airreservation.api.dto.etl.ETLMessageDataDTO;
import com.isa.thinair.airreservation.api.dto.etl.EtlTotalDestinationElementTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author Rikaz
 * 
 */
public class ETLComposer {
	private static Log logger = LogFactory.getLog(ETLComposer.class);
	public static final String JUMP_SEAT_PREFIX = "JS";
	public static final String CREW_PREFIX = "CRW";
	public static final String SEC_PREFIX = "SEC";
	public static final String ETL_MESSAGE_IDENTIFIER = "ETL";
	public static final String ETL_MESSAGE_END = "ENDETL";
	private static final int MAX_LINE_LENGTH_IATA = 50;
	private static final String ETL_MESSAGE_PART_IDENTIFIER = "PART";
	private static final String ETL_MESSAGE_END_PART_IDENTIFIER = "ENDPART";
	private static final String DECIMAL_FORMAT_THREE = "%03d";

	public String composeETLMessage(String addressElement, String communicationElement, ETLMessageDataDTO etlMetaDTO)
			throws ModuleException {

		String outMessage = "";
		if (etlMetaDTO != null) {
			HashMap<String, Object> etlTemplate = new HashMap<String, Object>();
			etlTemplate.put("etlMetaDTO", etlMetaDTO);
			try {
				String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.ETL
						+ MessageComposerConstants.TEMPLATE_SUFFIX;
				StringWriter writer = new StringWriter();
				new TemplateEngine().writeTemplate(etlTemplate, templateName, writer);
				outMessage = writer.toString();

			} catch (Exception e) {
				if (logger.isDebugEnabled()) {
					logger.debug("etlcomposer.etlmessage.error\n");
				}
				logger.info("etlcomposer.etlmessage.error\n" + e.getMessage());
				throw new ModuleException("etlcomposer.etlmessage.error");
			}

		} else {

			if (logger.isDebugEnabled()) {
				logger.debug("etlcomposer.etlmessage.noelements\n");
			}
			logger.info("etlcomposer.etlmessage.noelements\n");
			throw new ModuleException("etlcomposer.etlmessage.noelementsr");
		}
		logger.info(outMessage);
		return outMessage;

	}

	public List<ETLMessageDataDTO> getETLMessageParts(ETLDTO etlDTOOrg, String addressElement, String communicationElement) {

		Map<Integer, ETLDTO> etlDTOParts = this.groupPassangersForEtlParts(etlDTOOrg);
		List<ETLMessageDataDTO> etlMetaDTOParts = new ArrayList<ETLMessageDataDTO>();
		if (!etlDTOParts.isEmpty()) {
			for (Integer part : etlDTOParts.keySet()) {
				ETLDTO etlDTO = etlDTOParts.get(part);
				String originAirport = etlDTO.getDepartureAirport();
				ETLMessageDataDTO etlMetaDTO = new ETLMessageDataDTO();
				etlMetaDTO.setPartElement(ETL_MESSAGE_PART_IDENTIFIER + part);
				etlMetaDTO.setFromAddress(addressElement);
				etlMetaDTO.setCommunicationElement(communicationElement);
				etlMetaDTO.setMsgType(ETL_MESSAGE_IDENTIFIER);
				etlMetaDTO = buildFlightData(etlMetaDTO, etlDTO, originAirport);
				etlMetaDTO = buildSeatConfigurationData(etlMetaDTO, etlDTO);
				etlMetaDTO = buildTotalByDestination(etlMetaDTO, etlDTO, originAirport);
				// etlMetaDTO.setEndElement(ETL_MESSAGE_END);

				if (part == etlDTOParts.size()) {
					etlMetaDTO.setEndElement(ETL_MESSAGE_END);
				} else {
					etlMetaDTO.setEndElement(ETL_MESSAGE_END_PART_IDENTIFIER + part);
				}
				etlMetaDTOParts.add(etlMetaDTO);
			}
		}

		return etlMetaDTOParts;
	}

	private ETLMessageDataDTO buildSeatConfigurationData(ETLMessageDataDTO etlMetaDataDTO, ETLDTO etlDTO) {
		// TODO: current ThinAir ETL parser not supporting all types of CFG therefore later ETL parser needs to be
		// modified to support any CFG formats

		// Map<String, Integer> cabinCapacityMap = etlDTO.getCabinCapacityMap();
		// StringBuilder seatConfigStr = new StringBuilder("");
		// if (cabinCapacityMap != null && !cabinCapacityMap.isEmpty()) {
		// for (String cabinClass : cabinCapacityMap.keySet()) {
		// Integer capacity = cabinCapacityMap.get(cabinClass);
		// seatConfigStr.append(formatThreeDigitNumber(capacity)).append(cabinClass);
		// }
		// }
		// etlMetaDataDTO.setSeatConfiguration(seatConfigStr.toString());
		etlMetaDataDTO.setSeatConfiguration("");
		return etlMetaDataDTO;
	}

	private ETLMessageDataDTO buildFlightData(ETLMessageDataDTO pfsMetaDataDTO, ETLDTO etlDTO, String originAirport) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
		Date depdate = etlDTO.getFlightDate();
		pfsMetaDataDTO.setFlightNumber(etlDTO.getFlightNumber());
		pfsMetaDataDTO.setDayMonth(sdf.format(depdate).toUpperCase());
		pfsMetaDataDTO.setBoardingAirport(originAirport);
		return pfsMetaDataDTO;
	}

	private ETLMessageDataDTO buildTotalByDestination(ETLMessageDataDTO etlMetaDTO, ETLDTO etlDTO, String originAirport) {
		Map<String, Map<String, Map<String, Set<ETLBookingClassList>>>> destinationCabinPaxMap = new HashMap<String, Map<String, Map<String, Set<ETLBookingClassList>>>>();
		Map<String, Set<ETLBookingClassList>> bookingClassMap = new HashMap<String, Set<ETLBookingClassList>>();
		Map<String, Map<String, Set<ETLBookingClassList>>> cabinClassMap = new HashMap<String, Map<String, Set<ETLBookingClassList>>>();

		if (etlDTO.getPassengerNameList() != null && !etlDTO.getPassengerNameList().isEmpty()) {
			for (ETLPaxInfo pax : etlDTO.getPassengerNameList()) {
				Set<ETLBookingClassList> paxBookingCol = new HashSet<ETLBookingClassList>();
				ETLBookingClassList paxBooking = new ETLBookingClassList();

				String paxEticket = "";
				String paxEticketCoupen = "";
				String infantEticket = "";
				String infantEticketCoupen = "";

				paxEticket = pax.getEticketNumber();
				if (pax.getCoupNumber() != null) {
					paxEticketCoupen = pax.getCoupNumber().toString();
				}

				infantEticket = pax.getInfEticketNumber();
				if (!StringUtil.isNullOrEmpty(infantEticket)) {
					if (pax.getInfCoupNumber() != null) {
						infantEticketCoupen = pax.getInfCoupNumber().toString();
					}
					infantEticket = "INF" + infantEticket + MessageComposerConstants.FWD + infantEticketCoupen;
				}

				String cabinClass = pax.getCabinClass();
				String paxFirstName = pax.getFirstName();
				String paxLastName = pax.getLastName();
				String bookingClass = pax.getBookingClass();
				String paxTitle = pax.getTitle();
				String pnr = pax.getPnr();
				String status = pax.getPaxStatus();

				paxBooking.setPaxFirstName(paxFirstName);
				paxBooking.setPaxLastName(paxLastName);
				paxBooking.setPaxTitle(paxTitle);
				paxBooking.setPnr(pnr);
				paxBooking.setPaxEticket(paxEticket);
				paxBooking.setPaxCoupenNo(paxEticketCoupen);
				paxBooking.setBookingClass(bookingClass);
				paxBooking.setPaxStatus(status);
				paxBooking.setInfantEticket(infantEticket);

				String destinationAirport = pax.getArrivalAirport();

				if (!destinationCabinPaxMap.containsKey(destinationAirport)) {
					bookingClassMap = new HashMap<String, Set<ETLBookingClassList>>();
					cabinClassMap = new HashMap<String, Map<String, Set<ETLBookingClassList>>>();
					paxBookingCol.add(paxBooking);
					bookingClassMap.put(bookingClass, paxBookingCol);
					cabinClassMap.put(cabinClass, bookingClassMap);
					destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
				} else {
					cabinClassMap = destinationCabinPaxMap.get(destinationAirport);
					if (!cabinClassMap.containsKey(cabinClass)) {
						bookingClassMap = new HashMap<String, Set<ETLBookingClassList>>();
						paxBookingCol.add(paxBooking);
						bookingClassMap.put(bookingClass, paxBookingCol);
						cabinClassMap.put(cabinClass, bookingClassMap);
						destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
					} else {
						bookingClassMap = cabinClassMap.get(cabinClass);
						if (!bookingClassMap.containsKey(bookingClass)) {
							paxBookingCol.add(paxBooking);
							bookingClassMap.put(bookingClass, paxBookingCol);
							cabinClassMap.put(cabinClass, bookingClassMap);
							destinationCabinPaxMap.put(destinationAirport, cabinClassMap);
						} else {
							paxBookingCol = bookingClassMap.get(bookingClass);
							paxBookingCol.add(paxBooking);
							bookingClassMap.put(bookingClass, paxBookingCol);
							cabinClassMap.put(cabinClass, bookingClassMap);
							destinationCabinPaxMap.put(destinationAirport, cabinClassMap);

						}
					}
				}

			}
		}

		for (Map.Entry<String, Map<String, Map<String, Set<ETLBookingClassList>>>> entry : destinationCabinPaxMap.entrySet()) {
			String destinationAirport = entry.getKey();
			Map<String, Map<String, Set<ETLBookingClassList>>> cabinClassDataMap = entry.getValue();
			EtlTotalDestinationElementTO etlTotalDestinationElement = new EtlTotalDestinationElementTO();
			etlTotalDestinationElement.setDestinationAirPort(destinationAirport);
			List<EtlCabinClassList> cabinClassWiseList = new ArrayList<EtlCabinClassList>();
			for (Map.Entry<String, Map<String, Set<ETLBookingClassList>>> cabinClassMapEntry : cabinClassDataMap.entrySet()) {
				Integer cabinClassCount = 0;
				String cabinClass = cabinClassMapEntry.getKey();
				Map<String, Set<ETLBookingClassList>> bookingClassDataMap = cabinClassMapEntry.getValue();
				List<PassengerBookingToETL> bookingClassPaxLst = new ArrayList<PassengerBookingToETL>();
				EtlCabinClassList cabinClassPaxData = new EtlCabinClassList();
				cabinClassPaxData.setCabinClass(cabinClass);
				for (Map.Entry<String, Set<ETLBookingClassList>> bookingClassClassMapEntry : bookingClassDataMap.entrySet()) {
					Integer bookingClassCount = 0;
					PassengerBookingToETL bookingClassPax = new PassengerBookingToETL();
					String bookingClass = bookingClassClassMapEntry.getKey();
					bookingClassPax.setBookingClass(bookingClass);
					Set<ETLBookingClassList> paxListBookingClass = bookingClassClassMapEntry.getValue();
					List<ETLPaxTO> paxInfoList = new ArrayList<ETLPaxTO>();
					for (ETLBookingClassList ETLBookingClassList : paxListBookingClass) {
						ETLPaxTO paxInfo = new ETLPaxTO();
						paxInfo.setPaxFirstName(ETLBookingClassList.getPaxFirstName());
						paxInfo.setPaxLastName(ETLBookingClassList.getPaxLastName());
						paxInfo.setPaxTitle(ETLBookingClassList.getPaxTitle());
						paxInfo.setPaxEticket(ETLBookingClassList.getPaxEticket());
						paxInfo.setPaxCoupenNo(ETLBookingClassList.getPaxCoupenNo());
						paxInfo.setPnr(ETLBookingClassList.getPnr());
						paxInfo.setInfantETicket(ETLBookingClassList.getInfantEticket());

						paxInfo.setPaxStatus(ETLBookingClassList.getPaxStatus());
						if (ETLBookingClassList.getSeatCode() != null) {
							paxInfo.setSeatCode(ETLBookingClassList.getSeatCode());
						} else {
							paxInfo.setSeatCode(JUMP_SEAT_PREFIX);
						}
						paxInfo.setSsrDiscription(ETLBookingClassList.getSsrDescription());
						paxInfoList.add(paxInfo);
						if ("F".equals(paxInfo.getPaxStatus())) {
							cabinClassCount = cabinClassCount + 1;
							bookingClassCount = bookingClassCount + 1;
						}
					}
					bookingClassPax.setCount(bookingClassCount);
					bookingClassPax.setCountDisplay(String.format(DECIMAL_FORMAT_THREE, bookingClassCount));

					bookingClassPax.setPaxList(paxInfoList);
					bookingClassPaxLst.add(bookingClassPax);
				}
				cabinClassPaxData.setPaxBookingList(bookingClassPaxLst);
				cabinClassPaxData.setCount(cabinClassCount);
				cabinClassPaxData.setCountDisplay(String.format(DECIMAL_FORMAT_THREE, cabinClassCount));

				cabinClassWiseList.add(cabinClassPaxData);
			}
			etlTotalDestinationElement.setCabinClassWiseList(cabinClassWiseList);
			etlMetaDTO.getTotalByDestination().add(etlTotalDestinationElement);
		}

		return etlMetaDTO;
	}

	private Map<Integer, ETLDTO> groupPassangersForEtlParts(ETLDTO etlDTO) {
		Map<Integer, ETLDTO> etlDTOParts = new HashMap<Integer, ETLDTO>();
		List<List<ETLPaxInfo>> partitionedList = getPaxInfoListPartition(etlDTO.getPassengerNameList(), MAX_LINE_LENGTH_IATA);
		if (partitionedList != null) {
			Integer partNo = 1;
			for (List<ETLPaxInfo> list : partitionedList) {
				ETLDTO copyEtlDto = this.copyETLDTO(etlDTO);
				copyEtlDto.getPassengerNameList().addAll(list);
				etlDTOParts.put(partNo, copyEtlDto);
				partNo++;
			}
		}

		return etlDTOParts;
	}

	private <T> List<List<T>> getPaxInfoListPartition(List<T> list, final int L) {
		List<List<T>> parts = new ArrayList<List<T>>();
		final int N = list.size();
		for (int i = 0; i < N; i += L) {
			parts.add(new ArrayList<T>(list.subList(i, Math.min(N, i + L))));
		}
		return parts;
	}

	private ETLDTO copyETLDTO(ETLDTO src) {
		ETLDTO etlDTO = new ETLDTO();
		etlDTO.setFlightNumber(src.getFlightNumber());
		etlDTO.setFlightDate(src.getFlightDate());
		etlDTO.setDepartureAirport(src.getDepartureAirport());
		etlDTO.setArrivalAirport(src.getArrivalAirport());
		etlDTO.setGdsCode(src.getGdsCode());
		etlDTO.setCarrierCode(src.getCarrierCode());
		// etlDTO.setCabinCapacityMap(cabinCapacityMap);

		return etlDTO;
	}
}
