package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.accelaero.msgadaptor.OutboundTypeAMessageServiceGrpc;
import com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCREQ;
import com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCRESResponse;
import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.airreservation.api.dto.etl.ETLPaxInfo;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCREQMessage;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCREQRequestDTO;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCRESResponseDTO;
import com.isa.thinair.commons.api.dto.ets.Header;
import com.isa.thinair.commons.api.dto.ets.InterchangeHeader;
import com.isa.thinair.commons.api.dto.ets.InterchangeInfo;
import com.isa.thinair.commons.api.dto.ets.MessageFunction;
import com.isa.thinair.commons.api.dto.ets.MessageHeader;
import com.isa.thinair.commons.api.dto.ets.MessageIdentifier;
import com.isa.thinair.commons.api.dto.ets.RequestIdentity;
import com.isa.thinair.commons.api.dto.ets.TicketControlTicketNumberDetails;
import com.isa.thinair.commons.api.dto.ets.TicketCoupon;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.adaptor.CouponStatusTKCREQReqAdaptor;
import com.isa.thinair.msgbroker.core.adaptor.CouponStatusTKCREQResAdaptor;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.service.bd.CouponStatusTKCREQServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.CouponStatusTKCREQServiceBeanRemote;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

/**
 * Bean for invoking msg-adaptor service
 * 
 * @author rajitha
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "CouponStatusTKCREQService.remote")
@LocalBinding(jndiBinding = "CouponStatusTKCREQService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class CouponStatusTKCREQServiceBean extends PlatformBaseSessionBean
		implements CouponStatusTKCREQServiceBeanLocal, CouponStatusTKCREQServiceBeanRemote {

	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

	@Override
	public void updateCouponStatus(ETLDTO etlDTO) throws ModuleException {

		String gdsCode = etlDTO.getGdsCode();
		String msgType = MessageComposerConstants.Message_Identifier.ETL.toString();

		for (ETLPaxInfo etlPax : etlDTO.getPassengerNameList()) {

			CouponStatusTKCREQRequestDTO couponStatusTKCREQDTO = new CouponStatusTKCREQRequestDTO();

			try {

				setRequestIdentityCouponStatusTKCREQRequest(couponStatusTKCREQDTO);
				setRequestHeaderCouponStatusTKCREQRequest(couponStatusTKCREQDTO, gdsCode);
				setMessageCouponStatusTKCREQRequest(couponStatusTKCREQDTO, etlPax);
				updateCouponStatus(couponStatusTKCREQDTO);

			} catch (Exception ex) {

				log.error("Error while updating message adaptor" + ex.getCause());
				throw new ModuleException(ex,"Updating message adaptor failed");
			}

		}
	}

	private void setRequestIdentityCouponStatusTKCREQRequest(CouponStatusTKCREQRequestDTO couponStatusTKCREQDTO) {

		RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
				TypeAConstants.RequestIdentitySystem.TYPEA_MSG_ADAPTOR);
		couponStatusTKCREQDTO.setRequestIdentity(requestIdentity);
	}

	private void setRequestHeaderCouponStatusTKCREQRequest(CouponStatusTKCREQRequestDTO couponStatusTKCREQDTO, String gdsCode) {

		Map<String, String> headerInfoMap = getServiceClientReqTypeDAO().getInterchangeHeadderInfo(gdsCode);

		Header header = new Header();
		InterchangeHeader interchangeHeader = new InterchangeHeader();

		// interchangeControlRef need to set

		InterchangeInfo senderInfo = new InterchangeInfo();
		InterchangeInfo receiverInfo = new InterchangeInfo();

		senderInfo.setInterchangeId(headerInfoMap.get("AA_INTERCHANGE_ID"));
		receiverInfo.setInterchangeCode(headerInfoMap.get("GDS_INTERCHANGE_ID"));

		interchangeHeader.setSenderInfo(senderInfo);
		interchangeHeader.setReceiverInfo(receiverInfo);

		interchangeHeader.setSyntaxId(headerInfoMap.get("TYPE_A_SYNTAX_ID"));
		interchangeHeader.setVersionNo(headerInfoMap.get("TYPE_A_VERSION"));

		header.setInterchangeHeader(interchangeHeader);
		couponStatusTKCREQDTO.setHeader(header);

	}

	private void setMessageCouponStatusTKCREQRequest(CouponStatusTKCREQRequestDTO couponStatusTKCREQDTO, ETLPaxInfo etlPax) {

		CouponStatusTKCREQMessage statusUpdateTKCREQMessage = new CouponStatusTKCREQMessage();

		MessageFunction messageFunction = new MessageFunction();
		// OriginatorInformation originatorInformation = new OriginatorInformation();

		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.CHANGE_OF_STATUS);
		statusUpdateTKCREQMessage.setMessageFunction(messageFunction);

		setTicketControllUpdateDetails(statusUpdateTKCREQMessage, etlPax);
		setMessageHeaderInfo(couponStatusTKCREQDTO, statusUpdateTKCREQMessage);

	}

	private void setTicketControllUpdateDetails(CouponStatusTKCREQMessage statusUpdateTKCREQMessage, ETLPaxInfo etlPax) {

		TicketControlTicketNumberDetails ticket = new TicketControlTicketNumberDetails();
		ticket.setDocumentType(TypeAConstants.DocumentType.TICKET);
		ticket.setTicketNumber(etlPax.getEticketNumber());

		TicketCoupon coupon = new TicketCoupon();
		coupon.setCouponNumber(etlPax.getCoupNumber().toString());
		coupon.setStatus(TypeAConstants.CouponStatus.ORIGINAL_ISSUE);
		coupon.setPreviousStatus(TypeAConstants.CouponStatus.AIRPORT_CONTROL);

		ticket.setTicketCoupon(coupon);

		statusUpdateTKCREQMessage.setTickets(ticket);
	}

	private void setMessageHeaderInfo(CouponStatusTKCREQRequestDTO couponStatusTKCREQDTO,
			CouponStatusTKCREQMessage statusUpdateTKCREQMessage) {

		MessageHeader messageHeader = new MessageHeader();
		MessageIdentifier msgIdentifier = new MessageIdentifier();

		String messageVersion = couponStatusTKCREQDTO.getHeader().getInterchangeHeader().getVersionNo();

		msgIdentifier.setVersion(messageVersion.split("\\.")[0]);
		msgIdentifier.setReleaseNumber(messageVersion.split("\\.")[1]);
		msgIdentifier.setMessageType("TKCREQ");

		messageHeader.setMessageId(msgIdentifier);

		statusUpdateTKCREQMessage.setMessageHeader(messageHeader);

	}

	private CouponStatusTKCRESResponseDTO updateCouponStatus(CouponStatusTKCREQRequestDTO couponStatusTKCREQRequest)
			throws ModuleException {

		CouponStatusTKCRESResponseDTO couponStatusTKCRES = null;

		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance()
				.getModuleConfig(MsgbrokerConstants.MODULE_NAME);

		String host = moduleConfig.getMsgAdaptorHost();
		int port = moduleConfig.getMsgAdaptorPort();

		try {

			ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
			OutboundTypeAMessageServiceGrpc.OutboundTypeAMessageServiceBlockingStub blockingStub = OutboundTypeAMessageServiceGrpc
					.newBlockingStub(channel);
			CouponStatusTKCREQReqAdaptor rqAdaptor = new CouponStatusTKCREQReqAdaptor();
			CouponStatusTKCREQResAdaptor rsAdaptor = new CouponStatusTKCREQResAdaptor();

			CouponStatusTKCREQ input = rqAdaptor.fromDTO(couponStatusTKCREQRequest);
			CouponStatusTKCRESResponse output = callMsgAdaptorForTKCREQ(blockingStub, input);
			couponStatusTKCRES = rsAdaptor.fromGRPC(output);

			shutdown(channel);

		} catch (Exception e) {
			log.error("ERROR :: Error while updating message adaptor" + e.getCause());
		}
		return couponStatusTKCRES;

	}

	private CouponStatusTKCRESResponse callMsgAdaptorForTKCREQ(
			OutboundTypeAMessageServiceGrpc.OutboundTypeAMessageServiceBlockingStub blockingStub, CouponStatusTKCREQ input) {

		CouponStatusTKCRESResponse output = null;
		try {
			output = blockingStub.updateCouponStatus(input);
		} catch (StatusRuntimeException ex) {
			log.error("ERROR :: Error while comunicating with message adaptor" + ex.getCause());
		}

		return output;
	}

	private void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

}
