/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.msgbroker.api.service.PNLADLServiceBD;

/**
 * @author Ishan
 */

@Local
public interface PNLADLServiceLocalDelegateImpl extends PNLADLServiceBD {

}
