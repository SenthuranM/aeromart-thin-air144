package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;

public interface IBMsgProcessorBD {
	
	public InMessageProcessResultsDTO processAIRIMPMessage(InMessage inMessage);
	
	public InMessageProcessResultsDTO processAIRIMPMessage(TypeBRQ typeBRQ);	
}
