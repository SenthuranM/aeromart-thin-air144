package com.isa.thinair.msgbroker.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.dto.ets.InterchangeHeader;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;

public class ServiceClientReqDaoJDBCImpl extends PlatformBaseJdbcDAOSupport implements ServiceClientReqDaoJDBC {
	
	private static final org.apache.commons.logging.Log log = LogFactory.getLog(ServiceClientReqDaoJDBCImpl.class);
	
	@Override
	public String getTTYForServiceClientReqType(String serviceClientCode, String requestType) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String ttyAddres = null;
		try {
			ttyAddres = (String) template.queryForObject("SELECT OUT_TTY_ADDRESS " + "FROM MB_T_SERVICE_CLIENT_REQ_TTY "
					+ "WHERE REQUEST_TYPE_CODE='" + requestType + "'" + " AND SERVICE_CLIENT_CODE = '" + serviceClientCode + "'",
					String.class);
		} catch (Exception e) {
			log.error("No entry can be found for the Service Client : " + serviceClientCode + " Request Type " + requestType
					+ "in MB_T_SERVICE_CLIENT_REQ_TTY .");
		}
		return ttyAddres;
	}
	
	@Override
	public String getServiceClientForReqTypeTTY(String tty, String requestType) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String serviceClientCode = null;
		try {
			serviceClientCode = (String) template.queryForObject("SELECT SERVICE_CLIENT_CODE "
					+ "FROM MB_T_SERVICE_CLIENT_REQ_TTY " + "WHERE REQUEST_TYPE_CODE='" + requestType + "'"
					+ " AND IN_TTY_ADDRESS = '" + tty + "'", String.class);
		} catch (Exception e) {
			log.error("No entry can be found for the tty_address : " + tty + " Request Type " + requestType
					+ "in MB_T_SERVICE_CLIENT_REQ_TTY .");
		}

		return serviceClientCode;
	}

	@Override
	public void clearInMsgProcessingQueue() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = null;
		Date thresholdTime = CalendarUtil.addMinutes(new Date(), -30);
		String strThresholdTime = CalendarUtil.formatForSQL_toDate(new Timestamp(thresholdTime.getTime()));
		try {
			query = "DELETE FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq WHERE mpq.TIME_STAMP < TO_TIMESTAMP(" + strThresholdTime + ")";
			template.execute(query);
		} catch (Exception e) {
			log.error("Message processing queue clearing error");
		}

	}

	@Override
	public Collection<Integer> retrieveLockedQueueIds() {
		
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Date thresholdTime = CalendarUtil.addMinutes(new Date(), -45);
		String strThresholdTime = CalendarUtil.formatForSQL_toDate(new Timestamp(thresholdTime.getTime()));

		List<Integer> lockedFlightQueueIds = new ArrayList<>();
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT MSG_PROCESSING_QUEUE_ID");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.LOCK_STATUS ='L' and ( ( (TO_DATE(" + strThresholdTime + ")) - mpq.TIME_STAMP)>0 ) ");

		List<Integer> lockedQueueIdList = (List<Integer>) template.query(sql.toString(), new Object[] {},
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							lockedFlightQueueIds.add(new Integer(rs.getInt("MSG_PROCESSING_QUEUE_ID")));
						}

						return lockedFlightQueueIds;

					}
				});

		return lockedQueueIdList;
	}

	@Override
	public Map<String, String> getInterchangeHeadderInfo(String gdsCode) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Map<String, String> keyValues = new HashMap<String, String>();
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT TYPE_A_VERSION,TYPE_A_SYNTAX_ID, AA_INTERCHANGE_ID,GDS_INTERCHANGE_ID,TYPE_A_SENDER_ID");
		sql.append(" FROM T_GDS WHERE GDS_CODE='"+gdsCode+"'");

		Map<String, String> values = (HashMap<String, String>) template.query(sql.toString(), new Object[] {},
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							keyValues.put("TYPE_A_VERSION",rs.getString("TYPE_A_VERSION"));
							keyValues.put("TYPE_A_SYNTAX_ID",rs.getString("TYPE_A_SYNTAX_ID"));
							keyValues.put("AA_INTERCHANGE_ID",rs.getString("AA_INTERCHANGE_ID"));
							keyValues.put("GDS_INTERCHANGE_ID",rs.getString("GDS_INTERCHANGE_ID"));
							keyValues.put("TYPE_A_SENDER_ID",rs.getString("TYPE_A_SENDER_ID"));
						}

						return keyValues;

					}
				});

		return values;

	}
	
}
