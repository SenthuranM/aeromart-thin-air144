package com.isa.thinair.msgbroker.core.persistence.dao;

public interface AdminJdbcDAO {

	public String getPnrByExtRecordLocator(String extRecordLocator);
	
	public boolean isReservationExists(String pnr);
	
	public String getTypeBElementResFlag(String airimpVersion,
			String primaryElement, String secondaryElement);
	
	public Integer getOtherAirlineConfirmedSegmentCount(String pnr);

}
