package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.accelaero.ets.SimplifiedTicketServiceGrpc;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.VoidTKTREQ;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.VoidTKTRESResponse;
import com.isa.thinair.commons.api.dto.ets.VoidTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.VoidTKTRESResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.adaptor.ETSTicketVoidRequestAdaptor;
import com.isa.thinair.msgbroker.core.adaptor.ETSTicketVoidResponseAdaptor;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketVoidServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketVoidServiceBeanRemote;
import com.isa.thinair.platform.api.LookupServiceFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

/**
 * 
 * @author rajitha
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "TicketVoidService.remote")
@LocalBinding(jndiBinding = "TicketVoidService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETSTicketVoidServiceBean implements ETSTicketVoidServiceBeanLocal, ETSTicketVoidServiceBeanRemote {

	private Log log = LogFactory.getLog(ETSTicketGenerationServiceBean.class);

	@Override
	public VoidTKTRESResponseDTO voidTicket(VoidTKTREQDTO ticketVoidRequest) throws ModuleException {

		VoidTKTRESResponseDTO response = null;

		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance()
				.getModuleConfig(MsgbrokerConstants.MODULE_NAME);
		String etsHost = moduleConfig.getEtsHost();
		int etsPort = moduleConfig.getEtsPort();

		try {

			ManagedChannel channel = ManagedChannelBuilder.forAddress(etsHost, etsPort).usePlaintext(true).build();
			SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub = SimplifiedTicketServiceGrpc
					.newBlockingStub(channel);

			ETSTicketVoidRequestAdaptor requestAdaptor = new ETSTicketVoidRequestAdaptor();
			ETSTicketVoidResponseAdaptor responseAdaptor = new ETSTicketVoidResponseAdaptor();

			VoidTKTREQ input = requestAdaptor.fromDTO(ticketVoidRequest);
			VoidTKTRESResponse output = callVoid(blockingStub, input);
			response = responseAdaptor.fromGRPC(output);
			shutdown(channel);

		} catch (Exception ex) {

			log.error("ERROR :: Error while processing the request for void" + ex);
		}

		return response;
	}

	private VoidTKTRESResponse callVoid(SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub,
			VoidTKTREQ ticketVoidRequest) {

		VoidTKTRESResponse voidTicketResponse = null;
		try {
			voidTicketResponse = blockingStub.voidTicket(ticketVoidRequest);
		} catch (StatusRuntimeException ex) {
			log.error("ERROR :: Error while calling ETS for void" + ex);
		}

		return voidTicketResponse;
	}

	private void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}
}
