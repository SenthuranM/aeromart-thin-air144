package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.accelaero.ets.SimplifiedTicketServiceGrpc;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.RefundTKTREQ;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.RefundTKTRESResponse;
import com.isa.thinair.commons.api.dto.ets.RefundTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.RefundTKTRESResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.adaptor.ETSTicketRefundRequestAdaptor;
import com.isa.thinair.msgbroker.core.adaptor.ETSTicketRefundResponseAdaptor;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketRefundServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketRefundServiceBeanRemote;
import com.isa.thinair.platform.api.LookupServiceFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

/**
 * 
 * @author rajitha
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "TicketRefundService.remote")
@LocalBinding(jndiBinding = "TicketRefundService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETSTicketRefundServiceBean extends PlatformBaseSessionBean
		implements ETSTicketRefundServiceBeanLocal, ETSTicketRefundServiceBeanRemote {

	@Override
	public RefundTKTRESResponseDTO refund(RefundTKTREQDTO ticketRefundRequest) throws ModuleException {

		RefundTKTRESResponseDTO ticketRefundResponse = null;

		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance()
				.getModuleConfig(MsgbrokerConstants.MODULE_NAME);
		String etsHost = moduleConfig.getEtsHost();
		int etsPort = moduleConfig.getEtsPort();

		try {
			ManagedChannel channel = ManagedChannelBuilder.forAddress(etsHost, etsPort).usePlaintext(true).build();
			SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub = SimplifiedTicketServiceGrpc
					.newBlockingStub(channel);

			ETSTicketRefundRequestAdaptor requestAdaptor = new ETSTicketRefundRequestAdaptor();
			ETSTicketRefundResponseAdaptor responseAdaptor = new ETSTicketRefundResponseAdaptor();

			RefundTKTREQ input = requestAdaptor.fromDTO(ticketRefundRequest);
			RefundTKTRESResponse output = callRefund(blockingStub, input);
			ticketRefundResponse = responseAdaptor.fromGRPC(output);
			shutdown(channel);

		} catch (Exception e) {
			log.error("ERROR :: Error ETS refund update" + e.getCause());
		}

		return ticketRefundResponse;
	}

	private RefundTKTRESResponse callRefund(SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub,
			RefundTKTREQ input) {
		RefundTKTRESResponse output = null;
		try {
			output = blockingStub.refund(input);
		} catch (StatusRuntimeException e) {
			log.error("ERROR :: ERROR REFUND" + e.getCause());
		}
		return output;
	}

	private void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

}
