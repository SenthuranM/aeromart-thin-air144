package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.ets.SimplifiedTicketServiceWrapper;
import com.isa.cloud.support.generic.adaptor.AbstractRequestGrpcDtoAdapter;
import com.isa.thinair.commons.api.dto.ets.VoidTKTRESResponseDTO;

public class ETSTicketVoidResponseAdaptor
		extends AbstractRequestGrpcDtoAdapter<SimplifiedTicketServiceWrapper.VoidTKTRESResponse, VoidTKTRESResponseDTO> {

	public ETSTicketVoidResponseAdaptor() {
		super(SimplifiedTicketServiceWrapper.VoidTKTRESResponse.class, VoidTKTRESResponseDTO.class);
	}
}
