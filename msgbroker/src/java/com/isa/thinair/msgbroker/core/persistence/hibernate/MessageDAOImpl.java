package com.isa.thinair.msgbroker.core.persistence.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.Action;
import com.isa.thinair.msgbroker.api.dto.OutMessagesWithErrorsDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageSearchDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.MessageSequenceQueue;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.RequestType;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;
import com.isa.thinair.msgbroker.core.bl.MesageConstants;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.Util;

/**
 * @isa.module.dao-impl dao-name="messageDAO"
 * @author chan
 */
public class MessageDAOImpl extends PlatformBaseHibernateDaoSupport implements MessageDAO {

	private Log log = LogFactory.getLog(MessageDAOImpl.class);

	public void saveOutMessage(OutMessage oOutMessage) {
		hibernateSaveOrUpdate(oOutMessage);
	}

	public void saveOutMessageProcessingStatus(OutMessageProcessingStatus outMessageProcessingStatus) {
		hibernateSaveOrUpdate(outMessageProcessingStatus);
	}

	public Collection<OutMessage> getOutMessagesInFifoOrderForGds(String outMessageProcessStatus, List<String> messageTypes,
			List<String> gdsCodes) {
		String messageTypeSql = "";
		StringBuilder queryContent = new StringBuilder();
		queryContent.append("SELECT * FROM MB_T_OUT_MESSAGE WHERE PROCESS_STATUS = :prc_status ")
				.append("AND REQUEST_TYPE_CODE IN ( :msg_types_1 ) AND ( RECIPIENT_TTY_ADDRESS IN ")
				.append("  ( SELECT DISTINCT OUT_TTY_ADDRESS FROM MB_T_SERVICE_CLIENT_REQ_TTY ")
				.append("  WHERE SERVICE_CLIENT_CODE IN ( :gds_codes ) ")
				.append("  AND REQUEST_TYPE_CODE     IN ( :msg_types_2 ) ) ")
				.append( "  OR (NON_GDS_MESSAGE = 'Y')) ORDER BY RECEIVED_DATE ");

		Session session = getSessionFactory().getCurrentSession();
		Query query = session.createSQLQuery(queryContent.toString()).addEntity(OutMessage.class)
				.setString("prc_status", outMessageProcessStatus).setParameterList("msg_types_1", messageTypes)
				.setParameterList("gds_codes", gdsCodes).setParameterList("msg_types_2", messageTypes);

		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Integer> getOutMessageIdsInFifoOrderForGds(String outMessageProcessStatus, List<String> messageTypes,
			List<String> gdsCodes) {
		String messageTypeSql = "";
		List<Integer> outMessageIds = new ArrayList<Integer>();
		StringBuilder queryContent = new StringBuilder();
		queryContent.append("SELECT MESSAGE_ID FROM MB_T_OUT_MESSAGE WHERE PROCESS_STATUS = :prc_status ")
				.append("AND REQUEST_TYPE_CODE IN ( :msg_types_1 ) AND ( RECIPIENT_TTY_ADDRESS IN ")
				.append("  ( SELECT DISTINCT OUT_TTY_ADDRESS FROM MB_T_SERVICE_CLIENT_REQ_TTY ")
				.append("  WHERE SERVICE_CLIENT_CODE IN ( :gds_codes ) ")
				.append("  AND REQUEST_TYPE_CODE     IN ( :msg_types_2 ) ) ")
				.append( "  OR (NON_GDS_MESSAGE = 'Y')) ORDER BY MESSAGE_ID ");

		Session session = getSessionFactory().getCurrentSession();
		Query query = session.createSQLQuery(queryContent.toString())
				.setString("prc_status", outMessageProcessStatus).setParameterList("msg_types_1", messageTypes)
				.setParameterList("gds_codes", gdsCodes).setParameterList("msg_types_2", messageTypes);
		
		List <BigDecimal> outMessageIdsRes = query.list();
		for(BigDecimal outMessageId:outMessageIdsRes){
			outMessageIds.add((outMessageId.intValueExact()));
			}
		return outMessageIds;
	}

	public Collection<OutMessage> getOutMessagesInFifoOrder(String outMessageProcessStatus, List<String> messageTypes) {
		String messageTypeSql = "";
		if (messageTypes != null && messageTypes.size() > 0) {
			messageTypeSql = "'" + messageTypes.get(0) + "'";
			for (int i = 1; i < messageTypes.size(); i++) {
				messageTypeSql = messageTypeSql + "," + "'" + messageTypes.get(i) + "'";
			}
		} else {
			messageTypeSql = "''";
		}

		String hql = "SELECT o " + " FROM  OutMessage AS o " + " WHERE o.statusIndicator = '" + outMessageProcessStatus + "' "
				+ "AND o.messageType in (" + messageTypeSql + ")" + " ORDER BY o.receivedDate ";

		return find(hql, OutMessage.class);
	}
	
	public Collection<Integer> getOutMessageIdsInFifoOrder(String outMessageProcessStatus, List<String> messageTypes) {
		String messageTypeSql = "";
		if (messageTypes != null && messageTypes.size() > 0) {
			messageTypeSql = "'" + messageTypes.get(0) + "'";
			for (int i = 1; i < messageTypes.size(); i++) {
				messageTypeSql = messageTypeSql + "," + "'" + messageTypes.get(i) + "'";
			}
		} else {
			messageTypeSql = "''";
		}

		String hql = "SELECT o.outMessageID " + " FROM  OutMessage AS o " + " WHERE o.statusIndicator = '"
				+ outMessageProcessStatus + "' " + "AND o.messageType in (" + messageTypeSql + ")" + " ORDER BY o.receivedDate ";

		return find(hql, Integer.class);
	}

	public Collection<InMessage> getInMessagesInFifoOrder(String inMessageProcessStatus) {

		String hql = "SELECT i " + " FROM  InMessage AS i " + " WHERE i.statusIndicator = '" + inMessageProcessStatus + "' "
				+ " ORDER BY i.receivedDate ";

		return find(hql, InMessage.class);
	}

	public Collection<Integer> getInMessageIDsInFifoOrder(String inMessageProcessStatus) {

		String hql = "SELECT i.inMessageID " + " FROM  InMessage AS i " + " WHERE i.statusIndicator = '" + inMessageProcessStatus
				+ "' " + " ORDER BY i.receivedDate ";

		return find(hql, Integer.class);
	}

	public List<Integer> getLockableInMessageIDsInFifoOrder(String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing) {

		String msgTypes = "";
		if(skipMsgTypesForSeqProcessing!=null && !skipMsgTypesForSeqProcessing.isEmpty()){
			msgTypes =	" AND i.messageType not in ("+Util.buildStringInClauseContent(skipMsgTypesForSeqProcessing)+") ";
		}		
		
		String hql = "SELECT i.inMessageID" + " FROM  InMessage AS i " + " WHERE i.statusIndicator = '" + inMessageProcessStatus
				+ "' " 
				+ msgTypes
				+ "   and (i.lockStatus = '" + MessageProcessingConstants.InMessageLockStatuses.UNLOCKED + "' "
				+ "    	or (TO_DATE('" + CalendarUtil.getFormattedDate(new Date(), "dd-MM-yyyy HH:mm:ss")
				+ "','DD-MM-YY HH24:MI:SS') - i.lastLockedTime " + " - " + 1 + " ) > 0)" + " ORDER BY i.inMessageID ";
		return find(hql, Integer.class);
	}

	@Override
	public List<InMessage> getAllLockableInMessagesInFifoOrder(String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing) {

		String msgTypes = "";
		if(skipMsgTypesForSeqProcessing!=null && !skipMsgTypesForSeqProcessing.isEmpty()){
			msgTypes =	" AND i.messageType not in ("+Util.buildStringInClauseContent(skipMsgTypesForSeqProcessing)+") ";
		}
		
		String hql = "SELECT i" + " FROM  InMessage AS i " + " WHERE i.statusIndicator = '" + inMessageProcessStatus
				+ "' " 
				+ msgTypes
				+ "   and (i.lockStatus = '" + MessageProcessingConstants.InMessageLockStatuses.UNLOCKED + "' "
				+ "    	or (TO_DATE('" + CalendarUtil.getFormattedDate(new Date(), "dd-MM-yyyy HH:mm:ss")
				+ "','DD-MM-YY HH24:MI:SS') - i.lastLockedTime " + " - " + 1 + " ) > 0)" + " ORDER BY i.inMessageID ";
		return find(hql, InMessage.class);
	}
	
	public Map<String, Double> getActiveMessageTypeTimeToLives() {
		List<RequestType> messageTypes = find("from RequestType where recordStatus = 'Y'", RequestType.class);
		Map<String, Double> messageTypesMap = new HashMap<String, Double>();
		for (Iterator<RequestType> iterMessageTypes = messageTypes.iterator(); iterMessageTypes.hasNext();) {
			RequestType messageType = (RequestType) iterMessageTypes.next();
			messageTypesMap.put(messageType.getRequestTypeCode(), messageType.getTimeToLive());
		}
		return messageTypesMap;
	}

	public Collection<OutMessagesWithErrorsDTO> getOutMessagesWithErrors(Date receiveDate, Date processedDate,
			String messageType, String processingStatus, String senderEmailAddress, String recipientEmailAddress)
			throws ModuleException {
		Object[] params = new Object[1];
		int numberOfParams = 0;
		String hql = "SELECT m " + " FROM  OutMessage AS m " + " WHERE m.statusIndicator = '" + processingStatus.trim() + "' ";

		if (receiveDate != null) {
			hql += " and TRUNC(m.receivedDate) = ? ";
			params[++numberOfParams - 1] = CalendarUtil.getStartTimeOfDate(receiveDate);
		}
		if (processedDate != null) {
			hql += " and TRUNC(m.processedDate) = ? ";
			params[++numberOfParams - 1] = CalendarUtil.getStartTimeOfDate(processedDate);
		}
		if (senderEmailAddress != null) {
			hql += " and m.sentBy = ? ";
			params[++numberOfParams - 1] = senderEmailAddress.trim();
		}
		if (recipientEmailAddress != null) {
			hql += " and m.sentTo = ? ";
			params[++numberOfParams - 1] = recipientEmailAddress.trim();
		}
		if (messageType != null) {
			hql += " and m.messageType = ? ";
			params[++numberOfParams - 1] = messageType.trim();
		}

		hql += " ORDER BY m.receivedDate ";

		List<OutMessage> outMessages;
		if (numberOfParams == 0) {
			outMessages = find(hql, OutMessage.class);
		} else {
			outMessages = find(hql, params, OutMessage.class);
		}

		List<OutMessagesWithErrorsDTO> outMessagesWithErrorsDTOs = new ArrayList<OutMessagesWithErrorsDTO>();
		OutMessagesWithErrorsDTO outMessagesWithErrorsDTO;
		for (Iterator<OutMessage> iterInMesgs = outMessages.iterator(); iterInMesgs.hasNext();) {
			OutMessage outMessage = iterInMesgs.next();
			outMessagesWithErrorsDTO = new OutMessagesWithErrorsDTO();
			outMessagesWithErrorsDTO.setOutMessage(outMessage);
			hql = "SELECT s " + " FROM  OutMessageProcessingStatus AS s " + " WHERE s.outMessageID = "
					+ outMessage.getOutMessageID();
			outMessagesWithErrorsDTO.setOutMessageProcessingStatuses(find(hql, OutMessageProcessingStatus.class));
			outMessagesWithErrorsDTOs.add(outMessagesWithErrorsDTO);
		}

		return outMessagesWithErrorsDTOs;
	}

	public void updateOutMessagesResponseStatus(String messageSequenceReference, Action action) {

		JdbcTemplate templete = new JdbcTemplate(LookUpUtils.getDatasource());
		String sql = "UPDATE MB_T_OUT_MESSAGE SET" + " RESPONSE_STATUS = '" + action + "', VERSION = VERSION + 1 "
				+ "WHERE  UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(ROW_MESSAGE, 2000)) LIKE '%" + messageSequenceReference
				+ "%' AND PROCESS_STATUS IN('T','S') AND REQUEST_TYPE_CODE IN('SSM','ASM')";

		templete.execute(sql);
	}

	/**
	 * Method to return the OutMessage detail for a specified messageID.
	 * 
	 * @param outMessageID
	 * @return
	 */
	public OutMessage getOutMessage(Integer outMessageID) {
		return (OutMessage) get(OutMessage.class, outMessageID);
	}

	/**
	 * Method to return the request type for a specified request type code.
	 * 
	 * @param requestTypeCode
	 * @return
	 */
	public RequestType getRequestType(String requestTypeCode) {
		return (RequestType) get(RequestType.class, requestTypeCode);
	}

	@Override
	public void saveInMessage(InMessage oInMessage) {
		hibernateSaveOrUpdate(oInMessage);
	}

	@Override
	public boolean aquireInMessageLock(Integer inMessageID) {
		StringBuffer hql = new StringBuffer(
				"UPDATE InMessage SET lockStatus=:lockedStatus, lastLockedTime=:lockedTime, version = version + 1 WHERE "
						+ "lockStatus=:unlockedStatus AND inMessageID=:inMessageID");

		int lockedMessagesCount = getSession().createQuery(hql.toString())
				.setString("lockedStatus", MessageProcessingConstants.InMessageLockStatuses.LOCKED)
				.setDate("lockedTime", new Date())
				.setString("unlockedStatus", MessageProcessingConstants.InMessageLockStatuses.UNLOCKED)
				.setInteger("inMessageID", inMessageID).executeUpdate();

		return lockedMessagesCount >= 1;

	}

	public InMessage saveAndReLoad(InMessage in) {
		Serializable key = hibernateSave(in);

		return (InMessage)get(InMessage.class, key);
	}

	@Override
	public void saveInMessageProcessingStatus(InMessageProcessingStatus inMessageProcessingStatus) {
		hibernateSaveOrUpdate(inMessageProcessingStatus);
	}

	@Override
	public InMessage getInMessage(Integer messageID) {
		return (InMessage) get(InMessage.class, messageID);
	}

	@Override
	public List<TypeBMessageDTO> searchTypeBMessageByPnr(TypeBMessageSearchDTO typeBMessageSearchDTO, List<String> messageTypes) {
		/**
		 * TODO : Need to match using regex(Ex: THRW5 15AK31 ==> AgentStationCarrierCode Pnr)
		 * 		  Check for exact match since tables are not indexing
		 */
		String sql = " SELECT o.PROCESSED_DATE                                                         AS PROCESSED_DATE, "
				+ " o.PROCESS_STATUS                                                        AS PROCESS_STATUS, "
				+ " SYS.UTL_RAW.CAST_TO_VARCHAR2(SYS.DBMS_LOB.SUBSTR(o.row_message,2000,1)) AS MESSAGE, " + " '"
				+ MessageProcessingConstants.MessageDirection.OUT_MESSAGE
				+ "'                                                                     AS DIRECTION "
				+ " FROM MB_T_OUT_MESSAGE o " + "WHERE o.REQUEST_TYPE_CODE IN ( " + Util.buildStringInClauseContent(messageTypes)
				+ ") ";
		if (typeBMessageSearchDTO.getPnr() != null) {
			sql += " AND (SYS.UTL_RAW.CAST_TO_VARCHAR2(SYS.DBMS_LOB.SUBSTR(o.row_message,2000,1)) LIKE '%"
					+ typeBMessageSearchDTO.getPnr() + "%') ";
		}
		sql += " UNION " + "SELECT i.RECEIVED_DATE AS PROCESSED_DATE, i.PROCESS_STATUS     AS PROCESS_STATUS, "
				+ "  i.ROW_MESSAGE        AS MESSAGE," + "'" + MessageProcessingConstants.MessageDirection.IN_MESSAGE
				+ "'                  AS DIRECTION " + " FROM MB_T_IN_MESSAGE i " + " WHERE i.MESSAGE_TYPE_CODE IN ( "
				+ Util.buildStringInClauseContent(messageTypes) + ") ";
		if(typeBMessageSearchDTO.getPnr() !=null){
		sql += " AND i.ROW_MESSAGE LIKE '%" + typeBMessageSearchDTO.getPnr() + "%'";
		}

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		@SuppressWarnings("unchecked")
		List<TypeBMessageDTO> typeBMessageList = (List<TypeBMessageDTO>) templete.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<TypeBMessageDTO> typeBMessages = new ArrayList<TypeBMessageDTO>();
				while (rs.next()) {
					TypeBMessageDTO message = new TypeBMessageDTO(rs.getString("DIRECTION"), rs
							.getTimestamp("PROCESSED_DATE"), rs.getString("MESSAGE"), rs.getString("PROCESS_STATUS"));
					typeBMessages.add(message);
				}
				return typeBMessages;
			}
		});
		return typeBMessageList;
	}

	@Override
	public void saveTypeBRecipientExtSystem(TypeBRecipientExtSystem typeBRecipientExtSystem) {
		hibernateSaveOrUpdate(typeBRecipientExtSystem);		
	}



	@Override
	public boolean isScheduleLockForProcessing(Integer inMsgProcessingId) {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT LOCK_STATUS as status");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID = ?");

		Boolean isScheduleLock = (Boolean) template.query(sql.toString(), new Object[] { inMsgProcessingId },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							return new Boolean(
									(rs.getString("status")).equals(MesageConstants.InMessageQueueStatus.LOCK));
						}
						return null;
					}
				});

		return isScheduleLock;

	} 
	

	@Override
	public void updateScheduleForLockStatus(Integer msgProcessingQueueId, String msgStatus, Integer inMsgId) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { msgStatus, inMsgId,msgProcessingQueueId};
		int[] types = new int[] {java.sql.Types.CHAR,java.sql.Types.INTEGER,java.sql.Types.INTEGER};
		sql.append("UPDATE MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" SET mpq.LOCK_STATUS =? , mpq.IN_MESSAGE_ID= ?" );
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID =?");
		
		template.update(sql.toString(),params,types );
	}

	@Override
	public List<Integer> allInMessageIdsInqueue() {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		List<Integer> allInMessegeIds = new ArrayList<Integer>();

		sql.append(" SELECT IN_MESSAGE_ID mID");
		sql.append(" FROM MB_T_IN_MESSAGE_WAITING_QUEUE mwq");

		List<Integer> allInMsgIds = (List<Integer>) template.query(sql.toString(), new Object[] {},
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							allInMessegeIds.add(new Integer(rs.getInt("mID")));
						}
						return allInMessegeIds;
					}
				});

		return allInMsgIds;
	}

	@Override
	public void updateFlightDesignatorDependencyTable(Integer inMessageProcessingQueueId, String newFlightDesignator) {
	
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { inMessageProcessingQueueId,newFlightDesignator};
		int[] types = new int[] {java.sql.Types.INTEGER,java.sql.Types.VARCHAR};

		sql.append("INSERT INTO mb_t_in_msg_queue_flt_ref");
		sql.append(" (MSG_PROCESSING_QUEUE_ID,OTHER_FLIGHT_DESIGNATOR)");
		sql.append(" VALUES (?,?)");

		template.update(sql.toString(),params,types );
		
	}
	
	public void addMessageForWaitingQueue(Integer msgProcessingQueueId, Integer inMessageId){
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO MB_T_IN_MESSAGE_WAITING_QUEUE");
		sql.append(
				"(IN_MESSAGE_WAITING_QUEUE_ID, MSG_PROCESSING_QUEUE_ID,IN_MESSAGE_ID)");
		sql.append(" VALUES (MB_S_IN_MESSAGE_WAITING_QUEUE.NEXTVAL,?,?)");


		Object[] params = new Object[] { msgProcessingQueueId,inMessageId};
		int[] types = new int[] { java.sql.Types.INTEGER, java.sql.Types.INTEGER};

		template.update(sql.toString(), params, types);
		
		
	}

	@Override
	public void updateScheduleForLockStatus(String msgStatus, Integer inMsgId, Integer inMessageProcessingQueueId) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { msgStatus, inMsgId,inMessageProcessingQueueId};
		int[] types = new int[] {java.sql.Types.CHAR,java.sql.Types.INTEGER,java.sql.Types.INTEGER};
		sql.append("UPDATE MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" SET mpq.LOCK_STATUS =? , mpq.IN_MESSAGE_ID= ?" );
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID =?");
		
		template.update(sql.toString(),params,types );
	}
	
	@Override
	public void setMessageStatusForProcessing(String flightDesignator, Integer inMsgId, String msgStatus){
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Date time = new Date();
		String updatedTime = CalendarUtil.formatForSQL_toDate(new Timestamp(time.getTime()));

		Object[] params = new Object[] {msgStatus, new Date(), inMsgId, flightDesignator };
		int[] types = new int[] {java.sql.Types.CHAR, java.sql.Types.DATE,java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR };
		sql.append("INSERT INTO MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(
				" (mpq.MSG_PROCESSING_QUEUE_ID, mpq.LOCK_STATUS, mpq.TIME_STAMP, mpq.IN_MESSAGE_ID, mpq.FLIGHT_DESIGNATOR)");
		sql.append(" VALUES (MB_S_IN_MSG_PROCESSING_QUEUE.nextval,?,?,?,?)");

		template.update(sql.toString(), params, types);
		
	}

	@Override
	public Integer getFlightDesignatorForPostProcessing(Integer inMsgId, String status) {
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT MSG_PROCESSING_QUEUE_ID as qId");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.LOCK_STATUS = ? and mpq.IN_MESSAGE_ID = ?");

		Integer inMsgQueueId = (Integer) template.query(sql.toString(), new Object[] { status, inMsgId },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							return new Integer(rs.getInt("qId"));
						}
						return null;
					}
				});

		return inMsgQueueId;
	}

	@Override
	public Integer getNextInMsgIdInQueue(Integer msgProcessingQueueId) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
	
		sql.append("SELECT * FROM ( SELECT IN_MESSAGE_ID as mId");
		sql.append(" FROM MB_T_IN_MESSAGE_WAITING_QUEUE mwq");
		sql.append(" WHERE mwq.MSG_PROCESSING_QUEUE_ID =? ORDER BY mwq.IN_MESSAGE_ID) WHERE ROWNUM = 1 ");

		Integer inMsgId = (Integer) template.query(sql.toString(), new Object[] { msgProcessingQueueId },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							return new Integer(rs.getInt("mId"));
						}
						return null;
					}
				});

		return inMsgId;
	}

	
	@Override
	public void updateNextMsgIdToProcess(Integer inMessageProcessingQueueId, Integer msgId) {
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { msgId,inMessageProcessingQueueId};
		int[] types = new int[] {java.sql.Types.INTEGER,java.sql.Types.INTEGER};
		sql.append("UPDATE MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" SET mpq.IN_MESSAGE_ID=?");
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID=?");

		
		template.update(sql.toString(),params,types );
		
	}

	@Override
	public void removeMessageFromWaitingQueue(Integer msgId,Integer inMessageProcessingQueueId) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { msgId, inMessageProcessingQueueId};
		int[] types = new int[] {java.sql.Types.INTEGER, java.sql.Types.INTEGER};
		
		sql.append("DELETE FROM MB_T_IN_MESSAGE_WAITING_QUEUE mwq");
		sql.append(" WHERE mwq.IN_MESSAGE_ID=? AND mwq.MSG_PROCESSING_QUEUE_ID =?");

		template.update(sql.toString(),params,types);
		
		
	}

	@Override
	public void updateLastMessageProcessingTime(Integer inMessageProcessingQueueId) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		java.sql.Date lastUpdatedTime =  new java.sql.Date(new Date().getTime());
		
		Object[] params = new Object[] {lastUpdatedTime,inMessageProcessingQueueId};
		int[] types = new int[] {java.sql.Types.DATE,java.sql.Types.INTEGER};
		sql.append("UPDATE MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" SET mpq.TIME_STAMP=?");
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID=?");
		
		template.update(sql.toString(),params,types );
		
	}

	@Override
	public void removeFlightDependancies(Integer inMessageProcessingQueueId) {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		Object[] params = new Object[] { inMessageProcessingQueueId };
		int[] types = new int[] { java.sql.Types.INTEGER };
		sql.append("DELETE FROM mb_t_in_msg_queue_flt_ref mbt_FLT");
		sql.append(" WHERE mbt_FLT.MSG_PROCESSING_QUEUE_ID=?");

		template.update(sql.toString(), params, types);

	}

	@Override
	public Integer retrieveInMsgProcessingQueueId(String flightDesignator) {
		
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		sql.append("((SELECT MSG_PROCESSING_QUEUE_ID as qID");
		sql.append(" FROM mb_t_in_msg_queue_flt_ref mbt_flt");
		sql.append(" WHERE mbt_flt.OTHER_FLIGHT_DESIGNATOR = ?)");
		sql.append(" UNION ");
		sql.append("(SELECT MSG_PROCESSING_QUEUE_ID as qID");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.FLIGHT_DESIGNATOR = ?))");
		
		Integer inMessageProcessingId  = (Integer) template.query(sql.toString(), new Object[] { flightDesignator, flightDesignator },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							return new Integer(rs.getInt("qID"));
						}
						return null;
					}
				});

		return inMessageProcessingId;

	}

	@Override
	public boolean isMessagesLockedInProcessingQueue() {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		List<Integer> allMessegeProcessingIds = new ArrayList<Integer>();

		sql.append("SELECT MSG_PROCESSING_QUEUE_ID as qID");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.LOCK_STATUS='L' ");

		List<Integer> processingIds = (List<Integer>) template.query(sql.toString(), new Object[] {},
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							allMessegeProcessingIds.add(new Integer(rs.getInt("qID")));
						}
						return allMessegeProcessingIds;
					}
				});

		if (processingIds.size() > 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public void unlockMsgProcessingQueue(Integer inMessageProcessingQueueId, String msgStatus) {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		
		Object[] params = new Object[] { msgStatus,inMessageProcessingQueueId};
		int[] types = new int[] {java.sql.Types.CHAR,java.sql.Types.INTEGER};
		sql.append("UPDATE MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" SET mpq.LOCK_STATUS =?" );
		sql.append(" WHERE mpq.MSG_PROCESSING_QUEUE_ID =?");
		
		template.update(sql.toString(),params,types );
		
	}

	@Override
	public List<InMessage> getAllLockableInMessagesByMessageSequence(String inMessageProcessStatus,
			List<String> skipMsgTypesForSeqProcessing, Date updatedDateTime) {

		String msgTypes = "";
		if (skipMsgTypesForSeqProcessing != null && !skipMsgTypesForSeqProcessing.isEmpty()) {
			msgTypes = " AND i.messageType in (" + Util.buildStringInClauseContent(skipMsgTypesForSeqProcessing) + ") ";
			msgTypes += " AND i.receivedDate <:updatedDateTime ";
		}

		String hql = "SELECT i  FROM  InMessage AS i  WHERE i.statusIndicator = '" + inMessageProcessStatus + "' " + msgTypes
				+ "   and (i.lockStatus = '" + MessageProcessingConstants.InMessageLockStatuses.UNLOCKED + "' "
				+ "    	or (TO_DATE('" + CalendarUtil.getFormattedDate(new Date(), "dd-MM-yyyy HH:mm:ss")
				+ "','DD-MM-YY HH24:MI:SS') - i.lastLockedTime " + " - " + 1 + " ) > 0)"
				+ " ORDER BY i.messageSequence,i.inMessageID ";

		Query q = getSession().createQuery(hql);
		if (updatedDateTime != null) {
			q.setTimestamp("updatedDateTime", updatedDateTime);
		}

		return q.list();

	}

	
	public List<Integer> getMessagesOrderByMessageSequence(Collection<Integer> messageIds, String inMessageProcessStatus,
			List<String> skipMsgTypesForSeqProcessing) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT i.inMessageID FROM  InMessage AS i WHERE i.statusIndicator = '" + inMessageProcessStatus + "' ");
		if (skipMsgTypesForSeqProcessing != null && !skipMsgTypesForSeqProcessing.isEmpty()) {
			sql.append(" AND i.messageType in (" + Util.buildStringInClauseContent(skipMsgTypesForSeqProcessing) + ") ");
		}

		if (messageIds != null && !messageIds.isEmpty()) {
			sql.append(" AND i.inMessageID in (" + Util.buildIntegerInClauseContent(messageIds) + ") ");
		}

		sql.append("   AND (i.lockStatus = '" + MessageProcessingConstants.InMessageLockStatuses.UNLOCKED + "' ");
		sql.append("    	or (TO_DATE('" + CalendarUtil.getFormattedDate(new Date(), "dd-MM-yyyy HH:mm:ss")
				+ "','DD-MM-YY HH24:MI:SS') - i.lastLockedTime " + " - " + 1 + " ) > 0) ");
		sql.append(" ORDER BY i.messageSequence,i.inMessageID ");

		return find(sql.toString(), Integer.class);
	}

	@Override
	public void addBatchForMessageSequenceProcessingQueue() {
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO MB_T_IN_MSG_SEQUENCE_QUEUE(MSG_SEQ_ID,");
		sql.append("RECEIVED_DATE,PROCESSED_STATUS,LAST_BATCH_RECEIVED_AT) ");
		sql.append("VALUES(MB_S_IN_MSG_SEQUENCE_QUEUE.NEXTVAL,?,?,?) ");

		java.sql.Date lastUpdatedTime = new java.sql.Date(new Date().getTime());
		java.sql.Timestamp lastUpdatedTimeSec = new java.sql.Timestamp(new Date().getTime());

		Object[] params = new Object[] { lastUpdatedTime,
				MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.WAITING, lastUpdatedTimeSec };
		int[] types = new int[] { java.sql.Types.DATE, java.sql.Types.CHAR, java.sql.Types.TIMESTAMP };

		template.update(sql.toString(), params, types);

	}

	@Override
	public void updateMessageSequenceProcessingQueue(Integer msgSequenceId, String status) {
		try {

			String hqlUpdate = "UPDATE MessageSequenceQueue set processedStatus =:status  where "
					+ "messageSequenceId =:msgSeqId)";

			getSession().createQuery(hqlUpdate).setParameter("status", status).setParameter("msgSeqId", msgSequenceId)
					.executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	@Override
	public boolean isPreviousBatchStillProcessing() {

		int count = 0;
		try {

			count = ((Long) getSession().createQuery("SELECT count(*) FROM MessageSequenceQueue i WHERE i.processedStatus ='"
					+ MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.PROCESSING + "' ").uniqueResult())
							.intValue();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		if (count > 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<MessageSequenceQueue> loadWaitingBatchesForSequentialProcessing() {

		String hql = "SELECT i FROM  MessageSequenceQueue AS i " + " WHERE i.processedStatus = '"
				+ MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.WAITING + "' "
				+ " ORDER BY i.lastBatchReceived desc ";

		return find(hql, MessageSequenceQueue.class);

	}

	@Override
	public void clearCompletedMessageSequenceBatches() {

		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM MB_T_IN_MSG_SEQUENCE_QUEUE WHERE ");
		sql.append("PROCESSED_STATUS='" + MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.COMPLETED + "' ");

		template.update(sql.toString());
	}

	@Override
	public boolean isUnProcessedMsgWaitingForSequentialProcessing(List<String> msgTypesForSeqProcessing) {

		if (msgTypesForSeqProcessing != null && !msgTypesForSeqProcessing.isEmpty()) {
			String msgTypes = " AND i.messageType in (" + Util.buildStringInClauseContent(msgTypesForSeqProcessing) + ") ";

			String hql = "SELECT i" + " FROM  InMessage AS i " + " WHERE i.statusIndicator = '"
					+ MessageProcessingConstants.InMessageProcessStatus.Unprocessed + "' " + msgTypes + "   and (i.lockStatus = '"
					+ MessageProcessingConstants.InMessageLockStatuses.UNLOCKED + "' " + "    	or (TO_DATE('"
					+ CalendarUtil.getFormattedDate(new Date(), "dd-MM-yyyy HH:mm:ss")
					+ "','DD-MM-YY HH24:MI:SS') - i.lastLockedTime " + " - " + 1 + " ) > 0)" + " ORDER BY i.inMessageID ";

			List<InMessage> inMessages = find(hql, InMessage.class);

			if (inMessages != null && inMessages.size() > 0) {
				return true;
			}

		}

		return false;
	}
	
}
