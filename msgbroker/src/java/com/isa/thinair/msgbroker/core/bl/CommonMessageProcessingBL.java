package com.isa.thinair.msgbroker.core.bl;

public class CommonMessageProcessingBL {

	public static String buildEmailAddressForTTYAddress(String ttyAddress, String outgoingTypeBMessagingSvrName) {
		if (ttyAddress != null && outgoingTypeBMessagingSvrName != null) {
			return ttyAddress + "@" + outgoingTypeBMessagingSvrName;
		}
		return null;
	}

}
