package com.isa.thinair.msgbroker.core.bl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPassengerData;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.adl.comparator.CompPassengerFirstName;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.dto.PNLADLResponseDTO;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.PNLADLComposerUtil;

public class ADLComposer {

	// TODO impment this in Template method design pattern. need to do it after getting PNL logic also to msgbroker and
	// make it separate from airreservation

	private static Log logger = LogFactory.getLog(ADLComposer.class);
	private Map<Integer, List<String>> contents;
	private String flightElement;
	private String classCodesElement;
	Map<Integer, List<String>> passengerDataPerPart;
	int adlPartNumber;
	List<String> passengerData;
	private final int MAX_LINE_LENGTH_IATA = 64;
	private final int MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_AND_TOURID_LENGTH = 55;
	SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
	private String[] groupIds;
	String lastGroupId;
	private HashMap<String, String> pnrPaxSegIDGroupCode = new HashMap<String, String>();
	private HashMap<String, String> pnrGroupCodes = new HashMap<String, String>();
	private HashMap<String, String> pnrGroupCodesPerPart = new HashMap<String, String>();
	private HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();
	private HashMap<String, String> validCos = new HashMap<String, String>();
	private Collection<PassengerInformation> addedPassengerList = new ArrayList<PassengerInformation>();

	public ADLComposer() {
		contents = new HashMap<Integer, List<String>>();
		passengerDataPerPart = new HashMap<Integer, List<String>>();
		passengerData = new ArrayList<String>();
		adlPartNumber = 1;
		groupIds = PNLADLComposerUtil.populateGroupIds();
	}

	public PNLADLResponseDTO composeADL(ADLDTO adlElements) throws ModuleException {
		this.initiateCommonADLElements(adlElements);
		this.composePassengerData(adlElements);
		for (Integer partNumber : passengerDataPerPart.keySet()) {
			boolean isLastPart = (partNumber == passengerDataPerPart.size());
			this.composeSingleADLPart(partNumber, isLastPart, passengerDataPerPart.get(partNumber));
		}
		return this.composeFinalADL(adlElements.getFlightInfo());
	}

	private PNLADLResponseDTO composeFinalADL(FlightInformation flightInfoElement) throws ModuleException {
		try {
			PNLADLResponseDTO response = new PNLADLResponseDTO();
			ArrayList<String> fileNames = new ArrayList<String>();
			for (Integer partNumber : contents.keySet()) {
				String path = MessageComposerApiUtils.generateFileName(flightInfoElement,
						MessageComposerConstants.PNLADLMessageConstants.ADL, partNumber);
				fileNames.add(path);
				FileWriter fw = new FileWriter(path);
				BufferedWriter bw = new BufferedWriter(fw);
				for (String data : contents.get(partNumber)) {
					bw.write(data);
					bw.newLine();
				}
				bw.close();
				fw.close();
			}
			response.setFileNames(fileNames);
			response.setLastGroupId(this.lastGroupId);
			response.setPnrPaxSegIDGroupCode(this.pnrPaxSegIDGroupCode);
			response.setCcPaxMap(this.ccPaxMap);
			response.setAddedPassengerList(this.addedPassengerList);
			return response;
		} catch (IOException e) {
			throw new ModuleException("ADL file write error");
		}
	}

	private void initiateCommonADLElements(ADLDTO adlElements) {
		this.composeFlightElement(adlElements.getFlightInfo());
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			this.composeClassCodesElement(adlElements.getClassCodes());
		}
		lastGroupId = adlElements.getLastGroupCode();
		ccPaxMap = adlElements.getDestinationWisePaxCount();

	}

	private void composePassengerData(ADLDTO adlElements) {

		for (String destination : adlElements.getPassengerData().keySet()) {
			Map<String, ADLPassengerData> cosWisePassengerMap = adlElements.getPassengerData().get(destination);
			for (String cos : cosWisePassengerMap.keySet()) {

				// adding destination and pax count element
				String destinationPaxCount = this.composeDestinationElement(cosWisePassengerMap.get(cos), destination, cos,
						adlElements.getDestinationWisePaxCount());
				composePassengerAddDelChgData(cosWisePassengerMap.get(cos), destinationPaxCount);

			}

		}
	}

	private void composePassengerAddDelChgData(ADLPassengerData adlPassengerData, String destinationPaxCountElement) {
		// int partNumber = 1;
		boolean isMultipartADL = false;

		passengerData.add(destinationPaxCountElement);

		// Iterating delete list
		Map<String, List<PassengerInformation>> pnrWiseDeletedPassengers = PNLADLComposerUtil
				.generatePnrWisePassengers(adlPassengerData.getDeletedPassengers());
		if (adlPassengerData.getDeletedPassengers() != null && adlPassengerData.getDeletedPassengers().size() > 0) {
			boolean isFirstGroup = true;
			pnrGroupCodes.clear();
			passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_DEL);
			SortedMap<String, List<PassengerInformation>> lastNameWisePassengers = this
					.groupAllPassengersFromLastName(adlPassengerData.getDeletedPassengers());
			for (Entry<String, List<PassengerInformation>> entry : lastNameWisePassengers.entrySet()) {
				Map<String, List<PassengerInformation>> lastnamePnrWisePassengers = PNLADLComposerUtil
						.generatePnrWisePassengers(entry.getValue());
				for (Entry<String, List<PassengerInformation>> entryPax : lastnamePnrWisePassengers.entrySet()) {
					// Check the total pax count for pnr
					List<PassengerInformation> pnrWisePassengers = pnrWiseDeletedPassengers.get(entryPax.getValue().get(0)
							.getPnr());
					boolean addGroupId = false;
					if (pnrWisePassengers.size() != entryPax.getValue().size()) {
						addGroupId = true;
					}
					if (!pnrGroupCodesPerPart.containsKey(entryPax.getValue().get(0).getPnr())) {
						isFirstGroup = true;
					}
					List<String> passengerElements = generatePassengerElement(entryPax.getValue(), false, false, addGroupId,
							pnrWisePassengers.size(), isFirstGroup);
					isFirstGroup = false;
					// Current length and new to be added length should be less than max length
					if (passengerData.size() + passengerElements.size() < 59) {
						passengerData.addAll(passengerElements);
					} else {
						passengerDataPerPart.put(adlPartNumber, passengerData);
						passengerData = new ArrayList<String>();
						passengerData.add(destinationPaxCountElement);
						passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_DEL);
						passengerData.addAll(passengerElements);
						adlPartNumber = adlPartNumber + 1;
						pnrGroupCodesPerPart.clear();
						isFirstGroup = true;
					}
				}
			}
		}

		// Iterating add list
		Map<String, List<PassengerInformation>> pnrWiseAddedPassengers = PNLADLComposerUtil
				.generatePnrWisePassengers(adlPassengerData.getAddedPassengers());
		if (adlPassengerData.getAddedPassengers() != null && adlPassengerData.getAddedPassengers().size() > 0) {
			boolean isFirstGroup = true;
			pnrGroupCodes.clear();
			pnrGroupCodesPerPart.clear();
			passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_ADD);
			SortedMap<String, List<PassengerInformation>> lastNameWisePassengers = this
					.groupAllPassengersFromLastName(adlPassengerData.getAddedPassengers());
			for (Entry<String, List<PassengerInformation>> entry : lastNameWisePassengers.entrySet()) {
				Map<String, List<PassengerInformation>> lastnamePnrWisePassengers = PNLADLComposerUtil
						.generatePnrWisePassengers(entry.getValue());

				addedPassengerList.addAll(entry.getValue());

				for (Entry<String, List<PassengerInformation>> entryPax : lastnamePnrWisePassengers.entrySet()) {
					// Check the total pax count for pnr
					List<PassengerInformation> pnrWisePassengers = pnrWiseAddedPassengers
							.get(entryPax.getValue().get(0).getPnr());
					boolean addGroupId = false;
					if (pnrWisePassengers.size() != entryPax.getValue().size()) {
						addGroupId = true;
					}
					if (!pnrGroupCodesPerPart.containsKey(entryPax.getValue().get(0).getPnr())) {
						isFirstGroup = true;
					}
					List<String> passengerElements = generatePassengerElement(entryPax.getValue(), true, true, addGroupId,
							pnrWisePassengers.size(), isFirstGroup);
					isFirstGroup = false;

					// Current length and new to be added length should be less than max length
					if (passengerData.size() + passengerElements.size() < 59) {
						passengerData.addAll(passengerElements);
					} else {
						passengerDataPerPart.put(adlPartNumber, passengerData);
						passengerData = new ArrayList<String>();
						passengerData.add(destinationPaxCountElement);
						passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_ADD);
						passengerData.addAll(passengerElements);
						adlPartNumber = adlPartNumber + 1;
						pnrGroupCodesPerPart.clear();
						isFirstGroup = true;
					}
				}
			}
		}
		// Iterating change list
		Map<String, List<PassengerInformation>> pnrWiseChangedPassengers = PNLADLComposerUtil
				.generatePnrWisePassengers(adlPassengerData.getChangedPassengers());
		if (adlPassengerData.getChangedPassengers() != null && adlPassengerData.getChangedPassengers().size() > 0) {
			boolean isFirstGroup = true;
			pnrGroupCodes.clear();
			passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_CHG);
			SortedMap<String, List<PassengerInformation>> lastNameWisePassengers = this
					.groupAllPassengersFromLastName(adlPassengerData.getChangedPassengers());
			for (Entry<String, List<PassengerInformation>> entry : lastNameWisePassengers.entrySet()) {
				Map<String, List<PassengerInformation>> lastnamePnrWisePassengers = PNLADLComposerUtil
						.generatePnrWisePassengers(entry.getValue());

				for (Entry<String, List<PassengerInformation>> entryPax : lastnamePnrWisePassengers.entrySet()) {
					// Check the total pax count for pnr
					List<PassengerInformation> pnrWisePassengers = pnrWiseChangedPassengers.get(entryPax.getValue().get(0)
							.getPnr());
					boolean addGroupId = false;
					if (pnrWisePassengers.size() != entryPax.getValue().size()) {
						addGroupId = true;
					}
					if (!pnrGroupCodesPerPart.containsKey(entryPax.getValue().get(0).getPnr())) {
						isFirstGroup = true;
					}
					List<String> passengerElements = generatePassengerElement(entryPax.getValue(), true, false, addGroupId,
							pnrWisePassengers.size(), isFirstGroup);
					isFirstGroup = false;
					// Current length and new to be added length should be less than max length
					if (passengerData.size() + passengerElements.size() < 59) {
						passengerData.addAll(passengerElements);
					} else {
						passengerDataPerPart.put(adlPartNumber, passengerData);
						passengerData = new ArrayList<String>();
						passengerData.add(destinationPaxCountElement);
						passengerData.add(MessageComposerConstants.PNLADLMessageConstants.ADL_CHG);
						passengerData.addAll(passengerElements);
						adlPartNumber = adlPartNumber + 1;
						pnrGroupCodesPerPart.clear();
						isFirstGroup = true;
					}
				}
			}
		}
		if (!isMultipartADL) {
			passengerDataPerPart.put(adlPartNumber, passengerData);
		}
	}

	// TODO change this method, refactor
	private List<String> generatePassengerElement(List<PassengerInformation> passengers, boolean addRemarks,
			boolean isAddedRecord, boolean addGroupId, int groupPnrPaxCount, boolean isFirstGroup) {
		List<String> passengerElement = new ArrayList<String>();
		// To append the last name
		boolean isFirstPassenger = true;
		// To find the pax count in each group
		int passengerCount = 0;
		int totalIteratedPassengerCount = 0;
		int paxCountWithEXST = 0;
		StringBuffer passengerData = new StringBuffer();
		List<PassengerInformation> groupedPassengers = new ArrayList<PassengerInformation>();
		// Generate the groupId
		String groupId = this.determineGroupId(passengers, isAddedRecord);
		boolean isProcessedPnr = false;

		if (pnrGroupCodes.get(passengers.get(0).getPnr()) != null) {
			groupId = pnrGroupCodes.get(passengers.get(0).getPnr());
			isProcessedPnr = true;
		} else {
			pnrGroupCodes.put(passengers.get(0).getPnr(), groupId);
		}
		if (pnrGroupCodesPerPart.get(passengers.get(0).getPnr()) == null) {
			pnrGroupCodesPerPart.put(passengers.get(0).getPnr(), groupId);
		}

		for (PassengerInformation passengerInfo : passengers) {
			this.generatePnrPaxSegIDGroupCodeMap(passengerInfo, groupId);
			totalIteratedPassengerCount = totalIteratedPassengerCount + 1;
			String firstNameElement = this.composePassengerFirstNameElement(passengerInfo);
			String pnr = passengerInfo.getPnr();
			if(passengerInfo.isCodeShareFlight()){
				pnr = passengerInfo.getExternalPnr();
			}
			if ((passengerData.toString().length() + firstNameElement.length()) < MAX_SINGLE_PAX_FULLNAME_WITH_TITLE_AND_TOURID_LENGTH) {
				groupedPassengers.add(passengerInfo);
				// appending last name only to first passenger in group reservation
				if (isFirstPassenger) {
					passengerData.append(passengerInfo.getLastName());
					isFirstPassenger = false;
				}
				paxCountWithEXST = paxCountWithEXST + 1;
				if (passengerInfo.getSeats() != null && passengerInfo.getSeats().size() > 1) {
					paxCountWithEXST = paxCountWithEXST + 1;
				}
				passengerData.append(firstNameElement);
				passengerCount = passengerCount + 1;
				// If last passenger, inject passenger count and PNR (i.e not multi group element)
				if (passengerCount == passengers.size() && !isProcessedPnr) {
					// insert passenger count to beginning
					passengerData.insert(0, paxCountWithEXST);

					if (addGroupId) {
						// injecting group id
						passengerData.append(MessageComposerConstants.HYPHEN);
						passengerData.append(groupId);
						passengerData.append(groupPnrPaxCount);
					}
					if (isFirstGroup) {
						passengerData.append(MessageComposerConstants.SP);
						passengerData.append(this.composePNRElement(pnr));
						passengerData.append(this.composeMarketingFlightElement(passengerInfo));
						passengerData.append(this.composeInboundElement(passengerInfo.getInboundInfo()));
						passengerData.append(this.composeOnwardConnectionElement(passengerInfo.getOnwardconnectionlist()));
						if (passengerInfo.isWaitListedPassenger()) {
							passengerData.append(this.composeWaitListedPassengerElement(passengerInfo.getWaitListedInfo()));
						}
						if (passengerInfo.isStandByPassenger()) {
							passengerData.append(this.composeStandByPassengerElement());
						}
						isFirstGroup = false;
					}
					passengerElement.add(passengerData.toString());
					this.addRemarksElements(passengerElement, groupedPassengers, addRemarks);
				}
				// If multi group element and all the passengers are iterated
				else if (totalIteratedPassengerCount == passengers.size()) {
					passengerData.insert(0, passengerCount);
					passengerData.append(MessageComposerConstants.HYPHEN);
					passengerData.append(groupId);
					passengerData.append(groupPnrPaxCount);
					
					if (isFirstGroup) {
						passengerData.append(MessageComposerConstants.SP);
						passengerData.append(this.composePNRElement(pnr));
						isFirstGroup = false;
					}
					passengerElement.add(passengerData.toString());
					this.addRemarksElements(passengerElement, groupedPassengers, addRemarks);
				}

			} else {
				// Breaking group reservation in to several parts due to length restrictions
				// insert passenger count to beginning
				passengerData.insert(0, passengerCount);
				passengerData.append(MessageComposerConstants.HYPHEN);
				passengerData.append(groupId);
				passengerData.append(groupPnrPaxCount);
				// Adding pnr only to first group
				if (isFirstGroup) {
					passengerData.append(MessageComposerConstants.SP);
					passengerData.append(this.composePNRElement(pnr));
					passengerData.append(this.composeMarketingFlightElement(passengerInfo));
					passengerData.append(this.composeInboundElement(passengerInfo.getInboundInfo()));
					passengerData.append(this.composeOnwardConnectionElement(passengerInfo.getOnwardconnectionlist()));
					if (passengerInfo.isWaitListedPassenger()) {
						passengerData.append(this.composeWaitListedPassengerElement(passengerInfo.getWaitListedInfo()));
					}
					if (passengerInfo.isStandByPassenger()) {
						passengerData.append(this.composeStandByPassengerElement());
					}
					isFirstGroup = false;
				}
				passengerElement.add(passengerData.toString());
				this.addRemarksElements(passengerElement, groupedPassengers, addRemarks);
				groupedPassengers.clear();
				groupedPassengers.add(passengerInfo);
				// Creating new passenger group name element
				passengerData = new StringBuffer();
				passengerData.append(passengerInfo.getLastName());
				passengerData.append(firstNameElement);
				// re-initialize passengerCount variable to create next passenger element for group bookings
				passengerCount = 1;

				if (totalIteratedPassengerCount == passengers.size()) {
					passengerData.insert(0, passengerCount);
					passengerData.append(MessageComposerConstants.HYPHEN);
					passengerData.append(groupId);
					passengerData.append(groupPnrPaxCount);
					if (isFirstGroup) {
						passengerData.append(MessageComposerConstants.SP);
						passengerData.append(this.composePNRElement(pnr));
						isFirstGroup = false;
					}
					passengerElement.add(passengerData.toString());
					this.addRemarksElements(passengerElement, groupedPassengers, addRemarks);
				}

			}
		}

		return passengerElement;
	}

	private SortedMap<String, List<PassengerInformation>> groupAllPassengersFromLastName(List<PassengerInformation> passengers) {

		// Assume all the pnr's have same length. Need to be parameterized this
		final int pnrLength = passengers.get(0).getPnr().length();

		Comparator<String> lastNameComparator = new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				if (s1.equals(s2)) {
					return s1.substring(pnrLength).compareTo(s2.substring(pnrLength));
				} else {
					return s1.compareTo(s2);
				}				
			}
		};

		SortedMap<String, List<PassengerInformation>> lastNameWisePassengers = new TreeMap<String, List<PassengerInformation>>(
				lastNameComparator);
		// sort passengers from first name
		Collections.sort(passengers,new CompPassengerFirstName());
		for (PassengerInformation passenger : passengers) {
			// If passenger has extra seats, separate them as single pax from the group
			List<AncillaryDTO> paxSeats = passenger.getSeats();
			String lastName = passenger.getPnr() + passenger.getLastName();
			if (paxSeats != null && !paxSeats.isEmpty() && paxSeats.size() > 1) {
				List<PassengerInformation> passengerGroup = new ArrayList<PassengerInformation>();
				passengerGroup.add(passenger);
				String lastNameTmp = lastName + paxSeats.get(0).getDescription();
				lastNameWisePassengers.put(lastNameTmp, passengerGroup);
			} else if (!lastNameWisePassengers.containsKey(lastName)) {
				List<PassengerInformation> passengerGroup = new ArrayList<PassengerInformation>();
				passengerGroup.add(passenger);
				lastNameWisePassengers.put(lastName, passengerGroup);
			} else {
				lastNameWisePassengers.get(lastName).add(passenger);
			}
		}

		return lastNameWisePassengers;
	}

	private String composePassengerFirstNameElement(PassengerInformation passengerInfo) {
		StringBuffer data = new StringBuffer();
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getFirstName());
		data.append(passengerInfo.getTitle());
		// kind of Hack to add extra seats details
		if (passengerInfo.getSeats() != null && passengerInfo.getSeats().size() > 1) {
			data.append(MessageComposerConstants.FWD);
			data.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
		}
		return data.toString();
	}

	private String composePNRElement(String pnr) {
		StringBuffer data = new StringBuffer();
		data.append(MessageComposerConstants.POINT);
		data.append(MessageComposerConstants.PNLADLMessageConstants.L);
		data.append(MessageComposerConstants.FWD);
		data.append(pnr);
		return data.toString();
	}

	private String composeInboundElement(InboundConnectionDTO inboundInfo) {
		StringBuffer data = new StringBuffer();
		if (inboundInfo != null) {
			data.append(MessageComposerConstants.SP);
			data.append(MessageComposerConstants.PNLADLMessageConstants.INBOUND);
			data.append((inboundInfo.getFlightNumber() != null) ? inboundInfo.getFlightNumber() : "");
			data.append((inboundInfo.getFareClass() != null) ? inboundInfo.getFareClass() : "");
			data.append((inboundInfo.getDate() != null) ? inboundInfo.getDate() : "");
			data.append((inboundInfo.getDepartureStation() != null) ? inboundInfo.getDepartureStation() : "");
		}
		return data.toString();
	}

	private String composeOnwardConnectionElement(ArrayList<OnWardConnectionDTO> onwardConnections) {
		StringBuffer data = new StringBuffer();
		if (onwardConnections != null) {
			int index = 1;
			for (OnWardConnectionDTO owcDTO : onwardConnections) {
				data.append(MessageComposerConstants.SP);
				data.append(MessageComposerConstants.PNLADLMessageConstants.ONWARD);
				if (index != 1) {
					data.append(index);
				}
				data.append(MessageComposerConstants.FWD);
				data.append(owcDTO.getFlight());
				data.append(owcDTO.getFareclass());
				data.append((owcDTO.getDay() < 10) ? "0" + owcDTO.getDay() : owcDTO.getDay());
				data.append(owcDTO.getArrivalpoint());
				data.append(owcDTO.getDeparturetime());
				data.append(owcDTO.getReservationstatus());
				index++;
			}

		}
		return data.toString();
	}

	private String composeWaitListedPassengerElement(String waitListingDetails) {
		StringBuffer data = new StringBuffer();
		if (waitListingDetails != null) {
			data.append(MessageComposerConstants.SP);
			data.append(MessageComposerConstants.PNLADLMessageConstants.WAITLIST);
			data.append(waitListingDetails);
		}
		return data.toString();
	}

	private String composeStandByPassengerElement() {
		StringBuffer data = new StringBuffer();
		data.append(MessageComposerConstants.SP);
		data.append(MessageComposerConstants.PNLADLMessageConstants.ID2N2);

		return data.toString();
	}

	private void addRemarksElements(List<String> passengerElement, List<PassengerInformation> groupedPassengers,
			boolean addRemarks) {
		if (addRemarks && !groupedPassengers.isEmpty()) {
			passengerElement.addAll(this.composeRemarksElement(groupedPassengers));
		}
	}

	private List<String> composeRemarksElement(List<PassengerInformation> passengers) {
		List<String> remarks = new ArrayList<String>();
		StringBuffer data = new StringBuffer();
		boolean isCCDetailsAdded = false;
		for (PassengerInformation passengerInfo : passengers) {
			// Eticket
			if (passengerInfo.getEticketNumber() != null) {
				remarks.addAll(this.generatePassengerEticket(passengerInfo, false, null));
			}

			// Credit Card, only generates once per group
			if (AppSysParamsUtil.sendCCDetailsWithPNL() && passengerInfo.getCcDigits() != null && !isCCDetailsAdded) {
				remarks.addAll(this.generatePassengerCreditCardDetails(passengerInfo, passengers.size()));
				isCCDetailsAdded = true;
			}

			// Passport
			if (passengerInfo.getFoidNumber() != null && !("").equals(passengerInfo.getFoidNumber())) {
				remarks.addAll(this.generatePassengerPassportDetails(passengerInfo));
			}

			// Visa
			if (passengerInfo.getVisaDocNumber() != null) {
				remarks.addAll(this.generatePassengerVisaDetails(passengerInfo, false));
			}

			// Child information
			if (passengerInfo.getPaxType() != null
					&& passengerInfo.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				remarks.addAll(generatePassengerChildDetails(passengerInfo));
			}
		}

		// Infants
		remarks.addAll(this.generatePassengerInfantDetails(passengers));
		List<String> anciRemarks = null;
		// Baggage
		if (AppSysParamsUtil.isShowBaggages()) {
			anciRemarks = this.generatePassengerBaggageElement(passengers);
			if (anciRemarks != null) {
				remarks.addAll(anciRemarks);
			}
		}
		// SSR
		anciRemarks = this.generatePassengerSsrElement(passengers);
		if (anciRemarks != null) {
			remarks.addAll(anciRemarks);
		}

		// MEAL
		if (AppSysParamsUtil.isShowMeals()) {
			anciRemarks = this.generatePassengerMealElement(passengers);
			if (anciRemarks != null) {
				remarks.addAll(anciRemarks);
			}
		}
		// SEAT
		if (AppSysParamsUtil.isShowSeatMap()) {
			anciRemarks = this.generatePassengerSeatDetails(passengers);
			if (anciRemarks != null) {
				remarks.addAll(anciRemarks);
			}
		}

		if (data.toString().length() > 0) {
			remarks.add(data.toString());
		}
		return this.generateFinalRemarksData(remarks);
	}

	private List<String> generateFinalRemarksData(List<String> remarks) {
		StringBuffer data = new StringBuffer();
		List<String> finalRemarks = new ArrayList<String>();
		for (String remarksElement : remarks) {
			if (remarksElement.length() < MAX_LINE_LENGTH_IATA) {
				if ((data.length() + remarksElement.length()) < MAX_LINE_LENGTH_IATA) {
					data.append(remarksElement);
				} else {
					finalRemarks.add(data.toString());
					data = new StringBuffer();
					data.append(remarksElement);
				}
			} else {
				// Inject remarks continuation element
				this.addRemarksContinuationElement(finalRemarks, remarksElement);
			}

		}
		if (data.toString().length() > 0) {
			finalRemarks.add(data.toString());
		}
		return finalRemarks;
	}

	private void addRemarksContinuationElement(List<String> remarks, String remarkElement) {
		int strLength = 0;
		boolean isFirstPart = true;
		while (strLength < remarkElement.length()) {
			if (isFirstPart) {
				remarks.add(remarkElement.substring(strLength, MAX_LINE_LENGTH_IATA));
				isFirstPart = false;
			} else {
				StringBuffer data = new StringBuffer();
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS_CONT);
				int length = ((strLength + MAX_LINE_LENGTH_IATA) > remarkElement.length())
						? remarkElement.length()
						: (strLength + MAX_LINE_LENGTH_IATA);
				data.append(remarkElement.substring(strLength, length));
				remarks.add(data.toString());
			}
			strLength = strLength + MAX_LINE_LENGTH_IATA;
		}

	}

	private List<String> generatePassengerEticket(PassengerInformation passengerInfo, boolean isInfant,
			PassengerInformation parentInfo) {
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		if (AppSysParamsUtil.isShowTicketDetailsInPnlAdl()) {
			data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			data.append(MessageComposerConstants.PNLADLMessageConstants.TKNE);
			data.append(MessageComposerConstants.SP);
			// Hard coded passenger count as it is single eticket for single passenger
			data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
			data.append(MessageComposerConstants.SP);
			if (isInfant) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.INF);
				data.append(passengerInfo.getEticketNumber());
				data.append(MessageComposerConstants.FWD);
				data.append(passengerInfo.getCoupon());
				data.append(MessageComposerConstants.HYPHEN);
				data.append(1);
				data.append(parentInfo.getLastName());
				data.append(MessageComposerConstants.FWD);
				data.append(parentInfo.getFirstName());
				if (parentInfo.getTitle() != null) {
					data.append(parentInfo.getTitle());
				}
			} else {
				data.append(passengerInfo.getEticketNumber());
				data.append(MessageComposerConstants.FWD);
				data.append(passengerInfo.getCoupon());
				data.append(MessageComposerConstants.HYPHEN);
				data.append(1);
				data.append(passengerInfo.getLastName());
				data.append(MessageComposerConstants.FWD);
				data.append(passengerInfo.getFirstName());
				if (passengerInfo.getTitle() != null) {
					data.append(passengerInfo.getTitle());
				}
			}
			data.append(MessageComposerConstants.SP);
			remarks.add(data.toString());
		}
		return remarks;
	}

	private List<String> generatePassengerChildDetails(PassengerInformation passengerInfo) {
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		data.append(MessageComposerConstants.PNLADLMessageConstants.CHLD);
		data.append(MessageComposerConstants.SP);
		// Hard coded passenger count as it is single eticket for single passenger
		data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
		data.append(MessageComposerConstants.SP);

		Date chdDOB = passengerInfo.getDob();
		String dobChd = "";
		if (chdDOB != null) {
			dobChd = sdf.format(chdDOB).toString().toUpperCase();
			data.append(dobChd);
		}

		data.append("-1");
		data.append(passengerInfo.getLastName());
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getFirstName());
		// Infants dont have title
		if (passengerInfo.getTitle() != null) {
			data.append(passengerInfo.getTitle());
		}
		data.append(MessageComposerConstants.SP);
		remarks.add(data.toString());
		return remarks;
	}

	private List<String> generatePassengerPassportDetails(PassengerInformation passengerInfo) {
		StringBuffer data = new StringBuffer();
		String foidIssuedCountry = "";
		String foidExpiryDate = "";
		if (!passengerInfo.isNICSentInPNLADL()) {
			foidIssuedCountry = passengerInfo.getFoidIssuedCountry() != null ? passengerInfo.getFoidIssuedCountry() : "";
			foidExpiryDate = passengerInfo.getFoidExpiry() != null ? sdf.format(passengerInfo.getFoidExpiry()).toUpperCase() : "";
		}
		List<String> remarks = new ArrayList<String>();
		data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		data.append(MessageComposerConstants.PNLADLMessageConstants.DOCS);
		data.append(MessageComposerConstants.SP);
		// Hard coded passenger count as it is single pp details for single passenger
		data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
		data.append(MessageComposerConstants.SP);
		data.append(passengerInfo.isNICSentInPNLADL() ? "/I/" : "/P/");
		data.append(foidIssuedCountry);
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getFoidNumber());
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getNationality() != null ? passengerInfo.getNationality() : "");
		data.append(MessageComposerConstants.FWD);
		if (passengerInfo.getDob() != null) {
			data.append(sdf.format(passengerInfo.getDob()).toUpperCase());
		}
		data.append(MessageComposerConstants.FWD);
		if (passengerInfo.getGender() != null) {
			data.append(passengerInfo.getGender());
		}
		data.append(MessageComposerConstants.FWD);
		data.append(foidExpiryDate);
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getLastName());
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getFirstName());
		data.append("-1");
		data.append(passengerInfo.getLastName());
		data.append(MessageComposerConstants.FWD);
		data.append(passengerInfo.getFirstName());
		data.append(passengerInfo.getTitle());
		remarks.add(data.toString());
		return remarks;
	}

	private List<String> generatePassengerVisaDetails(PassengerInformation passengerInfo, boolean isInfant) {
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		if (passengerInfo.getVisaDocNumber() != null) {
			data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
			data.append(MessageComposerConstants.PNLADLMessageConstants.DOCO);
			data.append(MessageComposerConstants.SP);
			// Hard coded passenger count as it is single pp details for single passenger
			data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getPlaceOfBirth() != null ? passengerInfo.getPlaceOfBirth().toUpperCase() : "");
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getTravelDocumentType());
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getVisaDocNumber() != null ? passengerInfo.getVisaDocNumber() : "");
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getVisaDocPlaceOfIssue() != null
					? passengerInfo.getVisaDocPlaceOfIssue().toUpperCase()
					: "");
			data.append(MessageComposerConstants.FWD);
			if (passengerInfo.getVisaDocIssueDate() != null) {
				data.append(sdf.format(passengerInfo.getVisaDocIssueDate()).toUpperCase());
			}
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getVisaApplicableCountry());
			if (isInfant) {
				data.append("/I");
			}
			data.append("-1");
			data.append(passengerInfo.getLastName());
			data.append(MessageComposerConstants.FWD);
			data.append(passengerInfo.getFirstName());
			if (!isInfant) {
				data.append(passengerInfo.getTitle());
			}
			remarks.add(data.toString());
		}
		return remarks;
	}

	private List<String> generatePassengerCreditCardDetails(PassengerInformation passengerInfo, int passengerCount) {
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		data.append(MessageComposerConstants.PNLADLMessageConstants.EPAY);
		data.append(MessageComposerConstants.SP);
		data.append(MessageComposerConstants.PNLADLMessageConstants.KK + passengerCount);
		data.append(MessageComposerConstants.SP);
		data.append(" CC/XXXX-XXXX-XXXX-");
		data.append(passengerInfo.getCcDigits());
		remarks.add(data.toString());
		return remarks;
	}

	private List<String> generatePassengerSeatDetails(List<PassengerInformation> groupedPassengers) {
		// TODO handle multiple seats with EXST - 20MAR13 Done need to test
		StringBuffer data = new StringBuffer();
		StringBuffer exstData = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		int seatsCount = 0;
		if (groupedPassengers.size() == 1) {
			List<AncillaryDTO> seats = groupedPassengers.get(0).getSeats();

			if (seats != null) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				data.append(MessageComposerConstants.PNLADLMessageConstants.RQST);
				data.append(MessageComposerConstants.SP);
				// One seat per passenger
				data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
				data.append(MessageComposerConstants.SP);
				for (AncillaryDTO anci : seats) {
					seatsCount = seatsCount + 1;
					if (seatsCount == 1) {
						data.append(anci.getDescription());
						data.append(MessageComposerConstants.SP);
					}
					// if (seatsCount != seats.size()) {
					// data.append(MessageComposerConstants.SP);
					// }
					if (seatsCount > 1) {
						exstData.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
						exstData.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
						exstData.append(MessageComposerConstants.SP);
						exstData.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
						exstData.append(MessageComposerConstants.SP);
						exstData.append(anci.getDescription());
						exstData.append("-1");
						exstData.append(groupedPassengers.get(0).getLastName());
						exstData.append(MessageComposerConstants.FWD);
						exstData.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
					}
				}
			}
			remarks.add(data.toString());
			remarks.add(exstData.toString());
		} else {
			for (PassengerInformation passenger : groupedPassengers) {

				List<AncillaryDTO> seats = passenger.getSeats();
				StringBuffer singleSeat = null;
				if (seats != null) {
					singleSeat = new StringBuffer();
					exstData = new StringBuffer();
					singleSeat.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
					singleSeat.append(MessageComposerConstants.PNLADLMessageConstants.RQST);
					singleSeat.append(MessageComposerConstants.SP);
					// One seat per passenger
					singleSeat.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
					singleSeat.append(MessageComposerConstants.SP);
					seatsCount = 0;
					for (AncillaryDTO anci : seats) {
						seatsCount = seatsCount + 1;
						if (seatsCount == 1) {
							singleSeat.append(anci.getDescription());
							singleSeat.append(MessageComposerConstants.HYPHEN);
							singleSeat.append(1);
							singleSeat.append(passenger.getLastName());
							singleSeat.append(MessageComposerConstants.FWD);
							singleSeat.append(passenger.getFirstName());
							singleSeat.append(passenger.getTitle());
						} else {
							exstData.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
							exstData.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
							exstData.append(MessageComposerConstants.SP);
							exstData.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
							exstData.append(MessageComposerConstants.SP);
							exstData.append(anci.getDescription());
							exstData.append("-1");
							exstData.append(passenger.getLastName());
							exstData.append(MessageComposerConstants.FWD);
							exstData.append(passenger.getFirstName());
							exstData.append(passenger.getTitle());
							exstData.append(MessageComposerConstants.FWD);
							exstData.append(MessageComposerConstants.PNLADLMessageConstants.EXST);
						}
					}
					remarks.add(singleSeat.toString());
					remarks.add(exstData.toString());
				}
			}
		}
		if (seatsCount != 0) {
			return remarks;
		} else {
			return null;
		}
	}

	private List<String> generatePassengerBaggageElement(List<PassengerInformation> passengers) {
		// TODO get correct baggage ssr code for airport
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		int count = 0;
		if (passengers.size() == 1) {
			AncillaryDTO baggage = passengers.get(0).getBaggages();
			if (baggage != null) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				// data.append(MessageComposerConstants.PNLADLMessageConstants.SPBG);
				data.append(baggage.getCode() != null ? baggage.getCode() : MessageComposerConstants.PNLADLMessageConstants.SPBG);
				data.append(MessageComposerConstants.SP);
				// One baggage per passenger
				data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
				data.append(MessageComposerConstants.SP);

				count = count + 1;
				data.append(baggage.getDescription());
				data.append(MessageComposerConstants.SP);
			}
			remarks.add(data.toString());
		} else {
			boolean hasSameBaggage = true;
			String baggageDes = null;
			String bagCode = null;
			for (PassengerInformation passenger : passengers) {

				AncillaryDTO baggage = passenger.getBaggages();
				if (baggage != null) {
					bagCode = baggage.getCode();
					count = count + 1;
					if (baggageDes != null && !baggageDes.equals(baggage.getDescription())) {
						hasSameBaggage = false;
					}
					baggageDes = baggage.getDescription();

				}
			}

			bagCode = (bagCode != null ? bagCode : MessageComposerConstants.PNLADLMessageConstants.SPBG);
			if (hasSameBaggage && count == passengers.size()) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				data.append(bagCode);
				data.append(MessageComposerConstants.SP);
				data.append(MessageComposerConstants.PNLADLMessageConstants.HK + count);
				data.append(MessageComposerConstants.SP);
				data.append(baggageDes);
				data.append(MessageComposerConstants.SP);
				remarks.add(data.toString());
			} else {
				for (PassengerInformation passenger : passengers) {
					StringBuffer singleBaggage = null;
					AncillaryDTO baggage = passenger.getBaggages();
					if (baggage != null) {
						singleBaggage = new StringBuffer();
						singleBaggage.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
						singleBaggage.append(bagCode);
						singleBaggage.append(MessageComposerConstants.SP);
						singleBaggage.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
						singleBaggage.append(MessageComposerConstants.SP);
						count = count + 1;
						singleBaggage.append(baggage.getDescription());
						singleBaggage.append(MessageComposerConstants.HYPHEN);
						singleBaggage.append(1);
						singleBaggage.append(passenger.getLastName());
						singleBaggage.append(MessageComposerConstants.FWD);
						singleBaggage.append(passenger.getFirstName());
						singleBaggage.append(passenger.getTitle());
						singleBaggage.append(MessageComposerConstants.SP);
						remarks.add(singleBaggage.toString());

					}
				}
			}
		}

		if (count != 0) {
			return remarks;
		} else {
			return null;
		}
	}

	private List<String> generatePassengerMealElement(List<PassengerInformation> passengers) {
		// TODO implement multimeal support
		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		int count = 0;
		if (passengers.size() == 1) {
			List<AncillaryDTO> meals = passengers.get(0).getMeals();
			if (meals != null) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				data.append(MessageComposerConstants.PNLADLMessageConstants.SPML);
				data.append(MessageComposerConstants.SP);
				data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
				data.append(MessageComposerConstants.SP);
				for (AncillaryDTO anci : meals) {
					count = count + 1;
					data.append(anci.getDescription());
					data.append(MessageComposerConstants.SP);

				}
			}
			remarks.add(data.toString());
		} else {
			boolean hasSameMeal = true;
			String mealDes = null;
			for (PassengerInformation passenger : passengers) {
				List<AncillaryDTO> meals = passenger.getMeals();
				if (meals != null) {
					count = count + 1;
					for (AncillaryDTO anci : meals) {

						if (mealDes != null && !mealDes.equals(anci.getDescription())) {
							hasSameMeal = false;
						}
						mealDes = anci.getDescription();
					}

				}
			}
			if (hasSameMeal && count == passengers.size()) {
				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				data.append(MessageComposerConstants.PNLADLMessageConstants.SPML);
				data.append(MessageComposerConstants.SP);
				data.append(MessageComposerConstants.PNLADLMessageConstants.HK + count);
				data.append(MessageComposerConstants.SP);
				data.append(mealDes);
				remarks.add(data.toString());
			} else {
				for (PassengerInformation passenger : passengers) {
					StringBuffer singleMeal = null;
					List<AncillaryDTO> meals = passenger.getMeals();

					if (meals != null) {
						singleMeal = new StringBuffer();
						singleMeal.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
						singleMeal.append(MessageComposerConstants.PNLADLMessageConstants.SPML);
						singleMeal.append(MessageComposerConstants.SP);
						singleMeal.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
						singleMeal.append(MessageComposerConstants.SP);
						for (AncillaryDTO anci : meals) {
							count = count + 1;
							singleMeal.append(anci.getDescription());
						}
						singleMeal.append(MessageComposerConstants.HYPHEN);
						singleMeal.append(1);
						singleMeal.append(passenger.getLastName());
						singleMeal.append(MessageComposerConstants.FWD);
						singleMeal.append(passenger.getFirstName());
						singleMeal.append(passenger.getTitle());
						singleMeal.append(MessageComposerConstants.SP);
						remarks.add(singleMeal.toString());

					}
				}
			}
		}
		if (count != 0) {
			return remarks;
		} else {
			return null;
		}
	}

	private List<String> generatePassengerSsrElement(List<PassengerInformation> passengers) {

		StringBuffer data = new StringBuffer();
		List<String> remarks = new ArrayList<String>();
		int count = 0;
		if (passengers.size() == 1) {
			List<AncillaryDTO> ssrs = passengers.get(0).getSsrs();
			if (ssrs != null) {
				for (AncillaryDTO anci : ssrs) {
					data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
					data.append(anci.getCode());
					data.append(MessageComposerConstants.SP);

					data.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
					data.append(MessageComposerConstants.SP);
					count = count + 1;
					data.append(anci.getDescription());

					data.append(MessageComposerConstants.SP);

				}
			}
			remarks.add(data.toString());
		} else {

			for (PassengerInformation passenger : passengers) {
				StringBuffer singleSsr = null;
				List<AncillaryDTO> ssrs = passenger.getSsrs();
				if (ssrs != null) {
					for (AncillaryDTO anci : ssrs) {
						singleSsr = new StringBuffer();
						singleSsr.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
						singleSsr.append(anci.getCode());
						singleSsr.append(MessageComposerConstants.SP);

						singleSsr.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
						singleSsr.append(MessageComposerConstants.SP);
						count = count + 1;
						singleSsr.append(anci.getDescription());

						singleSsr.append(MessageComposerConstants.HYPHEN);
						singleSsr.append(1);
						singleSsr.append(passenger.getLastName());
						singleSsr.append(MessageComposerConstants.FWD);
						singleSsr.append(passenger.getFirstName());
						singleSsr.append(passenger.getTitle());
						singleSsr.append(MessageComposerConstants.SP);
						remarks.add(singleSsr.toString());
					}
				}
			}
		}

		if (count != 0) {
			return remarks;
		} else {
			return null;
		}
	}

	private List<String> generatePassengerInfantDetails(List<PassengerInformation> groupedPassengers) {
		List<String> remarks = new ArrayList<String>();
		List<String> visa = new ArrayList<String>();
		List<String> etickets = null;
		PassengerInformation infant = null;
		int infantCount = 0;
		for (PassengerInformation passenger : groupedPassengers) {
			infant = passenger.getInfant();
			if (infant != null) {
				etickets = new ArrayList<String>();
				StringBuffer data = new StringBuffer();

				data.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
				data.append(MessageComposerConstants.PNLADLMessageConstants.INFT);
				data.append(MessageComposerConstants.SP);
				data.append(MessageComposerConstants.HK + 1);
				data.append(MessageComposerConstants.SP);
				if (infant.getDob() != null) {
					data.append(sdf.format(infant.getDob()).toUpperCase());
					data.append(MessageComposerConstants.SP);
				}
				data.append(infant.getLastName());
				data.append(MessageComposerConstants.FWD);
				data.append(infant.getFirstName());
				if (infant.getFirstName() != null) {
					data.append(infant.getTitle());
				}

				remarks.add(data.toString());
				infantCount = infantCount + 1;
				// generate infant eticket
				etickets.addAll(generatePassengerEticket(infant, true, passenger));
				remarks.addAll(etickets);
				if (infant.getFoidNumber() != null && !("").equals(infant.getFoidNumber())) {
					remarks.addAll(this.generatePassengerPassportDetails(infant));
				}
				// visa
				visa.addAll(generatePassengerVisaDetails(infant, true));
			}
		}
		if (infantCount > 0) {
			remarks.addAll(visa);
		}

		return remarks;

	}

	private String determineGroupId(List<PassengerInformation> passengers, boolean isAddedRecord) {
		if (isAddedRecord) {
			this.lastGroupId = PNLADLComposerUtil.determineNextGroupId(lastGroupId, groupIds);
			return this.lastGroupId;
		} else {
			for (PassengerInformation passenger : passengers) {
				if (passenger.getGroupId() != null) {
					return passenger.getGroupId();
				}
			}
		}
		// This can not be happened, but returning new groupId just in case
		this.lastGroupId = PNLADLComposerUtil.determineNextGroupId(lastGroupId, groupIds);
		return this.lastGroupId;
	}

	private void composeSingleADLPart(Integer partNumber, boolean isLastPart, List<String> passengerData) {
		List<String> data = new ArrayList<String>();
		data.add(this.composeMessageIdentifier());
		data.add(this.composeFlightElementWithPartInformation(partNumber));
		if (partNumber == 1 && AppSysParamsUtil.isRBDPNLADLEnabled()) {
			data.add(this.getClassCodeElements());
		}
		// passenger elements
		data.addAll(passengerData);
		// End ADL
		data.add(this.composeEndElement(isLastPart, partNumber));
		contents.put(partNumber, data);

	}

	private String composeMessageIdentifier() {
		return MessageComposerConstants.PNLADLMessageConstants.ADL;
	}

	private String composeEndElement(boolean isLastPart, Integer partNumber) {
		if (isLastPart) {
			return MessageComposerConstants.PNLADLMessageConstants.ENDADL;
		} else {
			return MessageComposerConstants.PNLADLMessageConstants.ENDPART + partNumber;
		}
	}

	private String composeDestinationElement(ADLPassengerData adlPassengerData, String destination, String cos,
			Map<String, Integer> destinationWisePaxCount) {
		StringBuffer data = new StringBuffer();
		data.append(MessageComposerConstants.HYPHEN);
		data.append(destination);
		int addedPassengerCount = (adlPassengerData.getAddedPassengers() != null)
				? adlPassengerData.getAddedPassengers().size()
				: 0;
		int deletedPassengerCount = (adlPassengerData.getDeletedPassengers() != null) ? adlPassengerData.getDeletedPassengers()
				.size() : 0;

		int previousPaxCount = 0;
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			previousPaxCount = getValidDestinationWisePaxCount(cos, destinationWisePaxCount);
		} else {
			previousPaxCount = (destinationWisePaxCount.get(cos) != null) ? destinationWisePaxCount.get(cos) : 0;
		}

		int newPassengerCount = Math.max((previousPaxCount + addedPassengerCount) - deletedPassengerCount, 0);
		if (newPassengerCount < 10) {
			data.append(0);
		}
		data.append(newPassengerCount);
		data.append(cos);
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			ccPaxMap.put(this.getValidCos(cos), newPassengerCount);
		} else {
			ccPaxMap.put(cos, newPassengerCount);
		}
		return data.toString();
	}

	// FIXME Ugly hack to send rbd adl as sometimes we are having more than one char booking classes. need to come up
	// with a solution for that in BC level
	private int getValidDestinationWisePaxCount(String cos, Map<String, Integer> destinationWisePaxCount) {

		int total = 0;
		for (String val : destinationWisePaxCount.keySet()) {
			if (cos != null && cos.equals(String.valueOf(val.charAt(0)))) {
				total = total + (destinationWisePaxCount.get(val) != null ? destinationWisePaxCount.get(val) : 0);
			}
		}
		return total;
	}

	// FIXME Ugly hack to send rbd adl as sometimes we are having more than one char booking classes. need to come up
	// with a solution for that in BC level
	private String getValidCos(String cos) {
		if (validCos.get(cos) != null) {
			return validCos.get(cos);
		}
		return cos;
	}

	private String composeFlightElementWithPartInformation(Integer partNumber) {
		StringBuffer data = new StringBuffer();
		data.append(flightElement);
		data.append(partNumber);
		return data.toString();
	}

	private void composeFlightElement(FlightInformation flightInfoElement) {
		StringBuffer data = new StringBuffer();
		data.append(flightInfoElement.getFlightNumber());
		data.append(MessageComposerConstants.FWD);
		data.append(MessageComposerApiUtils.compileDepartureDate(flightInfoElement.getFlightDate()));
		data.append(MessageComposerConstants.SP);
		data.append(flightInfoElement.getBoardingAirport());
		data.append(MessageComposerConstants.SP);
		data.append(MessageComposerConstants.PNLADLMessageConstants.PART);
		flightElement = data.toString();
	}

	private void composeClassCodesElement(Map<String, List<String>> classCodes) {
		StringBuilder data = new StringBuilder(MessageComposerConstants.PNLADLMessageConstants.RBD);
		data.append(MessageComposerConstants.SP);
		Set<String> addedCos;
		boolean isNotFirstValue = false;
		for (String cabinClass : classCodes.keySet()) {
			addedCos = new HashSet<String>();
			if (isNotFirstValue) {
				data.append(MessageComposerConstants.SP);
			}
			data.append(cabinClass);
			data.append(MessageComposerConstants.FWD);
			List<String> allLcc = classCodes.get(cabinClass);
			for (String lcc : allLcc) {
				String rbdCos = String.valueOf(lcc.charAt(0));
				if (!addedCos.contains(rbdCos)) {
					data.append(rbdCos);
				}
				addedCos.add(rbdCos);
				validCos.put(rbdCos, lcc);
			}
			isNotFirstValue = true;
		}
		classCodesElement = data.toString();
	}

	private String getClassCodeElements() {
		return classCodesElement;
	}

	private void generatePnrPaxSegIDGroupCodeMap(PassengerInformation passenger, String groupId) {
		pnrPaxSegIDGroupCode.put(passenger.getPnrPaxId() + "|" + passenger.getPnrSegId(), groupId);
	}
	
	private String composeMarketingFlightElement(PassengerInformation pax) {
		StringBuffer data = new StringBuffer();
		if (pax != null && pax.getCsFlightNo() != null && pax.getCsBookingClass() != null && pax.getCsOrginDestination() != null
				&& pax.getCsDepatureDateTime() != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd");
			SimpleDateFormat tf = new SimpleDateFormat("HHmm");
			data.append(MessageComposerConstants.SP);
			data.append(MessageComposerConstants.POINT);
			data.append(MessageComposerConstants.PNLADLMessageConstants.M);
			data.append(MessageComposerConstants.FWD);
			data.append(pax.getCsFlightNo());
			data.append(pax.getCsBookingClass());
			data.append(df.format(pax.getCsDepatureDateTime()));			
			data.append(PnlAdlUtil.getOndCode(pax.getCsOrginDestination()));
			data.append(tf.format(pax.getCsDepatureDateTime()));
			data.append(MessageComposerConstants.HK);
		}
		return data.toString();
	}
}
