package com.isa.thinair.msgbroker.core.parser;

import com.isa.thinair.msgbroker.core.dto.SitaTexElement;
import com.isa.thinair.msgbroker.core.dto.SitaTexMessage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import static com.isa.thinair.msgbroker.core.dto.SitaTexElement.*;

public abstract class SitaTexParser {

	public static boolean parseSitaTex(SitaTexMessage.MessageHeader messageHeader, String elementId,
	                                      List<String> elementData) throws Exception {

		boolean parsed = true;
		if (elementId.equals(HEADER)) {

			String[] rawData = elementData.get(0).split(DATA_DELIMITER);
			messageHeader.setMessageType(rawData[0].trim());

			DateFormat format = new SimpleDateFormat(SitaTexElement.SitaTexHeader.DATE_TIME_FORMAT);
			messageHeader.setDate(format.parse(rawData[1].trim()));

		} else if (elementId.equals(PRIORITY)) {

			messageHeader.setPriority(elementData.get(0).trim());

		} else if (elementId.equals(DESTINATION_TYPE_B)) {

			SitaTexMessage.MessageHeader.SitaTexDestination destination;

			for(String dest : elementData) {
				destination = new SitaTexMessage.MessageHeader.SitaTexDestination();
				String[] rawData = dest.split(DATA_DELIMITER);

				destination.setAddressType(rawData[0].trim());
				if (rawData.length > 1) {
					destination.setAddress(rawData[1].trim());
				}
				if (rawData.length > 2) {
					destination.setExtension(rawData[2].trim());
				}

				messageHeader.getDestinations().add(destination);
			}

		} else if (elementId.equals(ORIGIN)) {

			messageHeader.setOriginAddress(elementData.get(0).trim());

		} else if (elementId.equals(DOUBLE_SIGNATURE)) {

			messageHeader.setAirlineDesignator(elementData.get(0).trim());

		} else if (elementId.equals(MESSAGE_ID)) {

			messageHeader.setMessageId(elementData.get(0).trim());

		} else if (elementId.equals(SUBJECT)) {

			messageHeader.setSubject(elementData.get(0).trim());

		} else if (elementId.equals(STANDARD_MSG_ID)) {

			messageHeader.setSmi(elementData.get(0).trim());

		}  else if (elementId.equals(FAX_HEADER)) {

			// TODO -- seems not required for the moment

		} else {
			parsed = false;
		}

		return parsed;
	}

	public static boolean parseSitaTex(SitaTexMessage.MessageBody messageBody, String elementId,
	                                    List<String> elementData) {


		boolean parsed = true;
		if (elementId.equals(TEXT)) {
			StringBuilder text = new StringBuilder();
			Iterator<String> iterator = elementData.iterator();

			while (iterator.hasNext()) {
				text.append(iterator.next());

				if (iterator.hasNext()) {
					text.append(EOL);
				}
			}
			messageBody.setMessageText(text.toString());

		} else if (elementId.equals(ATTACHMENTS)) {

			for (String attachment : elementData) {
				messageBody.getAttachments().add(attachment);
			}

		} else {
			parsed = false;
		}

		return parsed;
	}

	public static String getSitaTex(SitaTexMessage.MessageHeader messageHeader) {
		StringBuilder sitaTex = new StringBuilder();

		// HEADER
		String date = new SimpleDateFormat(SitaTexElement.SitaTexHeader.DATE_TIME_FORMAT)
				.format(messageHeader.getDate());
		sitaTex.append(ELEMENT_ID_PREFIX).append(HEADER).append(EOL);
		sitaTex.append(messageHeader.getMessageType()).append(DATA_DELIMITER).append(date).append(EOL);


		// PRIORITY
		if (messageHeader.getPriority() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(PRIORITY).append(EOL).append(messageHeader.getPriority()).append(EOL);
		}

		// DESTINATION TYPE B
		sitaTex.append(ELEMENT_ID_PREFIX).append(DESTINATION_TYPE_B).append(EOL);
		for (SitaTexMessage.MessageHeader.SitaTexDestination destination : messageHeader.getDestinations()) {
			sitaTex.append(destination.getAddressType()).append(DATA_DELIMITER)
					.append(destination.getAddress());

			if (destination.getExtension() != null) {
				sitaTex.append(DATA_DELIMITER).append(destination.getExtension());
			}
			sitaTex.append(EOL);
		}

		// ORIGIN
		if (messageHeader.getOriginAddress() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(ORIGIN).append(EOL).append(messageHeader.getOriginAddress()).append(EOL);
		}

		// DBLSIG
		if (messageHeader.getAirlineDesignator() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(DOUBLE_SIGNATURE).append(EOL).append(messageHeader.getAirlineDesignator()).append(EOL);
		}

		// MSGID
		if (messageHeader.getMessageId() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(MESSAGE_ID).append(EOL).append(messageHeader.getMessageId()).append(EOL);
		}

		// SUBJECT
		if (messageHeader.getSubject() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(SUBJECT).append(EOL).append(messageHeader.getSubject()).append(EOL);
		}

		// SMI
		if (messageHeader.getSmi() != null) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(STANDARD_MSG_ID).append(EOL).append(messageHeader.getSmi()).append(EOL);
		}

		// TODO ----  FAX HEADER

		return sitaTex.toString().trim();
	}

	public static String getSitaTex(SitaTexMessage.MessageBody messageBody) {
		StringBuilder sitaTex = new StringBuilder();

		// TEXT
		sitaTex.append(ELEMENT_ID_PREFIX).append(TEXT).append(EOL).append(messageBody.getMessageText()).append(EOL);

		if (!messageBody.getAttachments().isEmpty()) {
			sitaTex.append(ELEMENT_ID_PREFIX).append(TEXT).append(EOL);

			for (String attachment : messageBody.getAttachments()) {
				sitaTex.append(attachment).append(EOL);
			}
		}

		return sitaTex.toString().trim();
	}

}
