package com.isa.thinair.msgbroker.core.bl.ssm;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.msgbroker.api.dto.ASMSSMMetaDataDTO;
import com.isa.thinair.msgbroker.api.dto.SSMASMRequestDTO;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.util.Constants;

/**
 * Type B Message Parser.
 * 
 * @author M.Rikaz
 * 
 */
public class SSMASMessageParser {
	private String messageType;
	private String senderAddress;
	private String recipeintAddress;

	public SSMASMessageParser(String messageType) {
		this.messageType = messageType;
	}

	public MessageParserResponseDTO parseMessage(String messageBody) throws ModuleException {

		MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
		SSMASMRequestDTO scheduleRequestDTO = null;
		try {
			scheduleRequestDTO = getASMSSMMetaDataDTO(messageBody);
			scheduleRequestDTO.setOriginatorAddress(getSenderAddress());
			scheduleRequestDTO.setResponderAddress(getRecipeintAddress());
			oMessageParserResponseDTO.setRequestDTO(scheduleRequestDTO);
			oMessageParserResponseDTO.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
			oMessageParserResponseDTO.setSuccess(false);			
			oMessageParserResponseDTO.setReasonCode(StringUtil.trimStringMessage(e.getMessage(),90));
		}

		return oMessageParserResponseDTO;
	}

	private SSMASMRequestDTO getASMSSMMetaDataDTO(String messageBody) throws ModuleException, Exception {
		ASMSSMMetaDataDTO metaDataDTO = null;
		SSMASMRequestDTO scheduleRequestDTO = new SSMASMRequestDTO();

		if (Constants.ServiceTypes.SSM.equals(messageType)) {
			metaDataDTO = SSMParser.process(messageBody);
			scheduleRequestDTO.setAsmssmMetaDataDTO(metaDataDTO);
		} else if (Constants.ServiceTypes.ASM.equals(messageType)) {
			metaDataDTO = ASMParser.process(messageBody);
			scheduleRequestDTO.setAsmssmMetaDataDTO(metaDataDTO);
		}

		return scheduleRequestDTO;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getRecipeintAddress() {
		return recipeintAddress;
	}

	public void setRecipeintAddress(String recipeintAddress) {
		this.recipeintAddress = recipeintAddress;
	}
}
