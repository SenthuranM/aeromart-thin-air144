package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.ETSTicketServiceBD;

@Remote
public interface ETSTicketServiceBeanRemote extends ETSTicketServiceBD {

}
