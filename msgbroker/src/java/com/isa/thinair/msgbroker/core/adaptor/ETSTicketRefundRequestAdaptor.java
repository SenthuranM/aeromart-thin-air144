package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.ets.SimplifiedTicketServiceWrapper;
import com.isa.thinair.commons.api.dto.ets.RefundTKTREQDTO;

public class ETSTicketRefundRequestAdaptor
		extends AbstractGrpcRequestCreator<SimplifiedTicketServiceWrapper.RefundTKTREQ, RefundTKTREQDTO> {

	public ETSTicketRefundRequestAdaptor() {
		super(SimplifiedTicketServiceWrapper.RefundTKTREQ.class, RefundTKTREQDTO.class);
	}
}
