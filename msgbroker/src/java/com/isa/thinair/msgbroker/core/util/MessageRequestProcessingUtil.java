package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMFlightLegDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.Action;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.msgbroker.api.dto.ASMSSMLegInfoDTO;
import com.isa.thinair.msgbroker.api.dto.ASMSSMMetaDataDTO;
import com.isa.thinair.msgbroker.api.dto.ASMSSMSubMsgsDTO;
import com.isa.thinair.msgbroker.api.dto.AVSRequestDTO;
import com.isa.thinair.msgbroker.api.dto.SSMASMRequestDTO;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.MesageParserConstants;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageRequestProcessingUtil {

	public static final String OTHER_SEGMENT = "[a-zA-Z]{6}10(\\/[A-Z0-9]{3,7})*";
	private static String TIME_FORMAT = "HHmm";
	private static String LOCAL_TIME = "LT";

	public static SSMASMMessegeDTO convertMsgBrokerScheduleDTOToAccelaero(SSMASMRequestDTO scheduleRequestDTO)
			throws ModuleException {
		SSMASMMessegeDTO ssiMessegeDTO = new SSMASMMessegeDTO();
		boolean isACKNACMessage = false;
		try {
			String operatingCarrierCode = scheduleRequestDTO.getOperatingCarrier();
			boolean isCodeShareCarrier = scheduleRequestDTO.isCodeShareCarrier();

			MsgBrokerModuleConfig config = MsgbrokerUtils.getMsgbrokerConfigs();

			if (scheduleRequestDTO == null || scheduleRequestDTO.getAsmssmMetaDataDTO() == null) {
				throw new ModuleException("gdsservices.airschedules.logic.bl.message.invalid");
			}

			ASMSSMMetaDataDTO asmssmMetaDataDTO = scheduleRequestDTO.getAsmssmMetaDataDTO();

			ssiMessegeDTO.setProcessingUserID(config.getEjbAccessUsername());
			ssiMessegeDTO.setTimeMode(asmssmMetaDataDTO.getTimeMode());
			ssiMessegeDTO.setMessageSequenceReference(asmssmMetaDataDTO.getMessageSequenceReference());
			ssiMessegeDTO.setCreatorReference(asmssmMetaDataDTO.getCreatorReference());
			ssiMessegeDTO.setInMessegeID(asmssmMetaDataDTO.getInMessegeID());
			ssiMessegeDTO.setSenderAddress(scheduleRequestDTO.getOriginatorAddress());

			String messageType = asmssmMetaDataDTO.getMessageType();

			if (GDSExternalCodes.MessageType.SSM.toString().equals(messageType)) {
				ssiMessegeDTO.setMessageType(GDSExternalCodes.MessageType.SSM);
				ssiMessegeDTO.setEnableAutoScheduleSplit(scheduleRequestDTO.isEnableAutoScheduleSplit());
			} else if (GDSExternalCodes.MessageType.ASM.toString().equals(messageType)) {
				ssiMessegeDTO.setMessageType(GDSExternalCodes.MessageType.ASM);
			} else {
				throw new ModuleException("gdsservices.airschedules.message.type.invalid");
			}

			ssiMessegeDTO.setCodeShareCarrier(isCodeShareCarrier);

			if (AppSysParamsUtil.getDefaultCarrierCode().equals(operatingCarrierCode)) {
				ssiMessegeDTO.setOwnSchedule(true);
			}

			List<ASMSSMSubMsgsDTO> asmSSMSubMsgsList = (List<ASMSSMSubMsgsDTO>) asmssmMetaDataDTO.getAsmSSMSubMsgsList();
			if (asmSSMSubMsgsList != null && asmSSMSubMsgsList.size() > 0) {
				for (ASMSSMSubMsgsDTO asmSSMSubMsgsDTO : asmSSMSubMsgsList) {
					SSMASMSUBMessegeDTO ssmASMSUBMessegeDTO = new SSMASMSUBMessegeDTO();

					String actionIdent1 = asmSSMSubMsgsDTO.getActionIdentifier(); // CNL,NEW

					if (SSMASMSUBMessegeDTO.Action.NAC.toString().equals(actionIdent1)
							|| SSMASMSUBMessegeDTO.Action.ACK.toString().equals(actionIdent1)) {
						isACKNACMessage = true;
					}
					if (!isACKNACMessage) {
						if (StringUtil.isNullOrEmpty(asmSSMSubMsgsDTO.getFlightNumber())) {
							throw new ModuleException("gdsservices.response.message.flight.no.required");
						}
						ssmASMSUBMessegeDTO.setOperatingCarrier(operatingCarrierCode);
						ssmASMSUBMessegeDTO.setFlightDesignator(asmSSMSubMsgsDTO.getFlightNumber());
						ssmASMSUBMessegeDTO.setStartDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getPeriodOfOperationFrom(),
								false));
						ssmASMSUBMessegeDTO.setEndDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getPeriodOfOperationTo(), false));
						ssmASMSUBMessegeDTO.setServiceType(asmSSMSubMsgsDTO.getServiceType());
						ssmASMSUBMessegeDTO.setAircraftType(asmSSMSubMsgsDTO.getAircraftType());
						ssmASMSUBMessegeDTO.setPassengerResBookingDesignator(asmSSMSubMsgsDTO.getBookingDesignator());
						ssmASMSUBMessegeDTO.setNewFlightDesignator(asmSSMSubMsgsDTO.getNewFlightNumberWithSuffix());
						ssmASMSUBMessegeDTO.setSubSupplementaryInfo(asmSSMSubMsgsDTO.getSupplementaryInfo());
						ssmASMSUBMessegeDTO.setAircraftConfiguration(asmSSMSubMsgsDTO.getAircraftConfiguration());
					}
					ssmASMSUBMessegeDTO.setTimeMode(asmssmMetaDataDTO.getTimeMode());
					ssmASMSUBMessegeDTO.setMessageSequenceReference(asmssmMetaDataDTO.getMessageSequenceReference());
					ssmASMSUBMessegeDTO.setCreatorReference(asmssmMetaDataDTO.getCreatorReference());

					if (GDSExternalCodes.MessageType.SSM.toString().equals(messageType)) {
						ssmASMSUBMessegeDTO.setMessageType(GDSExternalCodes.MessageType.SSM);
					} else if (GDSExternalCodes.MessageType.ASM.toString().equals(messageType)) {
						ssmASMSUBMessegeDTO.setMessageType(GDSExternalCodes.MessageType.ASM);
						if (!StringUtil.isNullOrEmpty(asmSSMSubMsgsDTO.getFlightIdentifierDate())) {
							ssmASMSUBMessegeDTO.setStartDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getFlightIdentifierDate(),
									true));
						}
						if (!StringUtil.isNullOrEmpty(asmSSMSubMsgsDTO.getNewFlightIdentifierDate())) {
							ssmASMSUBMessegeDTO.setNewStartDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getNewFlightIdentifierDate(), true));
						}
					} else {
						throw new ModuleException("gdsservices.airschedules.message.type.invalid");
					}

					// past flight details allowed to update only for TIM & CNL
					GDSApiUtils.validateScheduleDate(ssmASMSUBMessegeDTO, actionIdent1);

					if (SSMASMSUBMessegeDTO.Action.NEW.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.NEW);
					} else if (SSMASMSUBMessegeDTO.Action.CNL.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.CNL);
					} else if (SSMASMSUBMessegeDTO.Action.RPL.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.RPL);
					} else if (SSMASMSUBMessegeDTO.Action.EQT.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.EQT);
					} else if (SSMASMSUBMessegeDTO.Action.TIM.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.TIM);
					} else if (SSMASMSUBMessegeDTO.Action.REV.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.REV);
					} else if (SSMASMSUBMessegeDTO.Action.SKD.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.SKD);
					} else if (SSMASMSUBMessegeDTO.Action.FLT.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.FLT);
					} else if (SSMASMSUBMessegeDTO.Action.NAC.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.NAC);
						ssiMessegeDTO.setUpdateOutResponseStatus(true);
					} else if (SSMASMSUBMessegeDTO.Action.ACK.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setAction(SSMASMSUBMessegeDTO.Action.ACK);
						ssiMessegeDTO.setUpdateOutResponseStatus(true);
					} else {
						throw new ModuleException("gdsservices.airschedules.action.type.invalid");
					}

					ssmASMSUBMessegeDTO.setCodeShareCarrier(isCodeShareCarrier);

					if (AppSysParamsUtil.getDefaultCarrierCode().equals(operatingCarrierCode)) {
						ssmASMSUBMessegeDTO.setOwnSchedule(true);
					}


						String allMarketingFlight = asmSSMSubMsgsDTO.getOtherSegmentInfo();
						String[] marketingFlights;

					if (!StringUtil.isNullOrEmpty(allMarketingFlight)) {
						allMarketingFlight = allMarketingFlight.replaceAll("\\s", "");
						if (allMarketingFlight.matches(OTHER_SEGMENT)) {
							allMarketingFlight = allMarketingFlight.substring(9);
							marketingFlights = allMarketingFlight.trim().split("/");
							String marketingFltNo = null;
							for (String marketingFlight : marketingFlights) {
								if (!StringUtil.isNullOrEmpty(marketingFlight)) {
									marketingFltNo = marketingFlight;
									if (!ssmASMSUBMessegeDTO.isOwnSchedule()
											&& AppSysParamsUtil.getDefaultCarrierCode().equals(marketingFlight.substring(0, 2))) {
										ssmASMSUBMessegeDTO.setExternalFlightNo(marketingFlight.trim());
									} else {
										ssmASMSUBMessegeDTO.addMarketingFlightNos(marketingFlight.trim());
									}
								}
							}

							if (!ssmASMSUBMessegeDTO.isOwnSchedule()
									&& StringUtil.isNullOrEmpty(ssmASMSUBMessegeDTO.getExternalFlightNo())) {
								ssmASMSUBMessegeDTO.setExternalFlightNo(marketingFltNo);
							}

							filterMarketingFlightNoForCodeshareCarrier(ssmASMSUBMessegeDTO);

						}
					}
					if (!ssmASMSUBMessegeDTO.isOwnSchedule()) {
						// when code share marketing element is not present marketing flightDesignator will be composed
						// using
						// MARKETING_FLIGHT_PREFIX from T_GDS table, if its updated by Marketing airline
						if (isMarketingElementRequired(ssmASMSUBMessegeDTO.getAction())
								&& StringUtil.isNullOrEmpty(ssmASMSUBMessegeDTO.getExternalFlightNo()) && !isACKNACMessage) {
							String marketingFlightDesignator = GDSApiUtils.composeMarketingFlightDesignator(
									ssmASMSUBMessegeDTO.getFlightDesignator(), scheduleRequestDTO.getMarketingFlightPrefix(),
									operatingCarrierCode);
							if (!StringUtil.isNullOrEmpty(marketingFlightDesignator)) {
								ssmASMSUBMessegeDTO.setExternalFlightNo(marketingFlightDesignator);
							} else {
								throw new ModuleException("gdsservices.airschedules.logic.bl.dei.data.required");
							}
						}
					}

					if (SSMASMSUBMessegeDTO.Action.REV.toString().equals(actionIdent1)) {
						ssmASMSUBMessegeDTO.setStartDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getOldPeriodFrom(), false));
						ssmASMSUBMessegeDTO.setEndDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getOldPeriodTo(), false));
						ssmASMSUBMessegeDTO.setDaysOfOperation(getOperationDays(asmSSMSubMsgsDTO.getOldDaysOfOperation()));

						ssmASMSUBMessegeDTO.setNewStartDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getPeriodOfOperationFrom(),
								false));
						ssmASMSUBMessegeDTO
								.setNewEndDate(CalendarUtil.parseDate(asmSSMSubMsgsDTO.getPeriodOfOperationTo(), false));
						ssmASMSUBMessegeDTO.setNewDaysOfOperation(getOperationDays(asmSSMSubMsgsDTO.getDaysOfOperation()));

					} else {
						ssmASMSUBMessegeDTO.setDaysOfOperation(getOperationDays(asmSSMSubMsgsDTO.getDaysOfOperation()));
					}

					List<ASMSSMLegInfoDTO> legInList1 = asmSSMSubMsgsDTO.getLegInList();
					if (legInList1 != null && legInList1.size() > 0 && !isACKNACMessage) {
						for (ASMSSMLegInfoDTO assAsmssmLegInfoDTO : legInList1) {
							SSMASMFlightLegDTO ssiFlightLegDTO = new SSMASMFlightLegDTO();
							ssiFlightLegDTO.setDepatureAirport(assAsmssmLegInfoDTO.getDepartureStation());
							ssiFlightLegDTO.setArrivalAirport(assAsmssmLegInfoDTO.getArrivalStation());
							ssiFlightLegDTO.setDepatureTime(assAsmssmLegInfoDTO.getScheduledTimeDeparture());
							ssiFlightLegDTO.setArrivalAirport(assAsmssmLegInfoDTO.getArrivalStation());
							ssiFlightLegDTO.setArrivalTime(assAsmssmLegInfoDTO.getScheduledTimeArrival());
							int offset = 0;
							String dateVariation = assAsmssmLegInfoDTO.getDateVariationSTD();
							if (dateVariation != null && !dateVariation.trim().equals("")) {
								offset = Integer.parseInt(dateVariation);
							}
							ssiFlightLegDTO.setDepatureOffiset(offset);

							dateVariation = assAsmssmLegInfoDTO.getDateVariationSTA();
							offset = 0;
							if (dateVariation != null && !dateVariation.trim().equals("")) {
								offset = Integer.parseInt(dateVariation);
							}
							Date depatureTime = CalendarUtil.getParsedTime(ssiFlightLegDTO.getDepatureTime(), TIME_FORMAT);
							Date arrivalTime = CalendarUtil.getParsedTime(ssiFlightLegDTO.getArrivalTime(), TIME_FORMAT);

							if (LOCAL_TIME.equalsIgnoreCase(asmssmMetaDataDTO.getTimeMode())) {

								if (GDSExternalCodes.MessageType.ASM.toString().equals(messageType)
										&& asmSSMSubMsgsDTO.getFlightIdentifierDate() != null
										&& !asmSSMSubMsgsDTO.getFlightIdentifierDate().trim().equals("")) {

									depatureTime = getZuluDateTime(assAsmssmLegInfoDTO.getDepartureStation(),
											asmSSMSubMsgsDTO.getFlightIdentifierDate(),
											assAsmssmLegInfoDTO.getScheduledTimeDeparture());
									arrivalTime = getZuluDateTime(assAsmssmLegInfoDTO.getArrivalStation(),
											asmSSMSubMsgsDTO.getFlightIdentifierDate(),
											assAsmssmLegInfoDTO.getScheduledTimeArrival());
								}

								if (GDSExternalCodes.MessageType.SSM.toString().equals(messageType)
										&& asmSSMSubMsgsDTO.getPeriodOfOperationFrom() != null
										&& !asmSSMSubMsgsDTO.getPeriodOfOperationFrom().trim().equals("")) {

									depatureTime = getZuluDateTime(assAsmssmLegInfoDTO.getDepartureStation(),
											asmSSMSubMsgsDTO.getPeriodOfOperationFrom(),
											assAsmssmLegInfoDTO.getScheduledTimeDeparture());
									arrivalTime = getZuluDateTime(assAsmssmLegInfoDTO.getArrivalStation(),
											asmSSMSubMsgsDTO.getPeriodOfOperationFrom(),
											assAsmssmLegInfoDTO.getScheduledTimeArrival());
								}
							}

							boolean arrivalNextDay = isArrivalNextDay(depatureTime, arrivalTime,
									ssiFlightLegDTO.getDepatureOffiset(), offset);

							if (arrivalNextDay) {
								offset = 1;
							}

							ssiFlightLegDTO.setArrivalOffset(offset);
							ssmASMSUBMessegeDTO.addLeg(ssiFlightLegDTO);
						}
					}
					ssiMessegeDTO.addSsmASMSUBMessegeList(ssmASMSUBMessegeDTO);
				}

			}

			ssiMessegeDTO.setSupplementaryInfo(asmssmMetaDataDTO.getSupplementaryInfo());
			ssiMessegeDTO.setRawMessage(scheduleRequestDTO.getRawMessage());

		} catch (ModuleException ex) {
			ssiMessegeDTO.setValidRequest(false);
			String messageCode = ex.getExceptionCode();
			if (!StringUtil.isNullOrEmpty(messageCode)) {
				String errorMessage = getErrorMessage(messageCode);
				if ("MESSAGE CODE SET IS NULL OR DOES NOT EXISTS".equalsIgnoreCase(errorMessage)) {
					errorMessage = messageCode;
				}
				ssiMessegeDTO.setErrorMessage(errorMessage);
			} else {
				ssiMessegeDTO.setErrorMessage("INTERNAL PROCESSING ERROR");
			}
		} catch (Exception e) {
			ssiMessegeDTO.setErrorMessage("INTERNAL PROCESSING ERROR");
			ssiMessegeDTO.setValidRequest(false);
		}

		return ssiMessegeDTO;
	}

	public static List<AVSRequestSegmentDTO> convertMsgBrokerAVSSegmentInfoToAccelaero(AVSRequestDTO avsRequestDTO)
			throws ModuleException {

		List<AVSRequestSegmentDTO> segmentList = new ArrayList<AVSRequestSegmentDTO>();

		if (avsRequestDTO != null && avsRequestDTO.getAVSSegmentDTOs() != null && !avsRequestDTO.getAVSSegmentDTOs().isEmpty()) {

			for (com.isa.thinair.msgbroker.api.dto.AVSRequestSegmentDTO avsSegment : avsRequestDTO.getAVSSegmentDTOs()) {
				AVSRequestSegmentDTO avsSegmentDTO = new AVSRequestSegmentDTO();
				avsSegmentDTO.setFlightDesignator(avsSegment.getFlightDesignator());
				avsSegmentDTO.setDepartureDate(CalendarUtil.parseDate(avsSegment.getDepartureDate(), true));
				avsSegmentDTO.setDepartureStation(avsSegment.getDepartureStation());
				avsSegmentDTO.setDestinationStation(avsSegment.getDestinationStation());
				avsSegmentDTO.setStatusCode(avsSegment.getStatusCode());
				avsSegmentDTO.setRbd(avsSegment.getRbd());

				if (!StringUtil.isNullOrEmpty(avsSegment.getSeatsAvailable())) {
					avsSegmentDTO.setSeatsAvailable(new Integer(avsSegment.getSeatsAvailable()));
					avsSegmentDTO.setNumericAvailability(true);
				} else {
					avsSegmentDTO.setSeatsAvailable(0);
				}

				segmentList.add(avsSegmentDTO);
			}

		} else {
			throw new ModuleException("gdsservices.airschedules.logic.bl.message.invalid");
		}
		return segmentList;
	}

	public static String getErrorMessage(String messageCode) {
		return GDSApiUtils.maskNull(new ModuleException(messageCode).getMessageString());
	}

	public static String[] getMessageIdentifier(String rawMessage, boolean removeAddress) {
		String[] resultStr = new String[5];
		String msgType = "";
		StringBuilder newMsgText = new StringBuilder();
		String senderAddress = "";
		String recipientAddress = "";
		String msgReference = "";
		if (!StringUtil.isNullOrEmpty(rawMessage)) {
			Pattern pattern;
			Matcher matcher;

			Map<String, String> keyTokensMap = new HashMap<String, String>();
			keyTokensMap.put(MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS,
					MesageParserConstants.ElementRegularExpressions.RECIPIENT_ADDRESS);
			keyTokensMap.put(MesageParserConstants.GDSMessageElements.SENDER_ADDRESS,
					MesageParserConstants.ElementRegularExpressions.SENDER_ADDRESS);
			keyTokensMap.put(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER,
					MesageParserConstants.ElementRegularExpressions.MESSAGE_IDENTIFIER);
			keyTokensMap.put(MesageParserConstants.GDSMessageElements.MESSAGE_REFERENCE,
					MesageParserConstants.ElementRegularExpressions.MESSAGE_REFERENCE);

			List<String> validMsgTypeList = new ArrayList<String>();
			validMsgTypeList.add(Constants.ServiceTypes.ASM);
			validMsgTypeList.add(Constants.ServiceTypes.SSM);
			validMsgTypeList.add(Constants.ServiceTypes.AVS);

			String[] messageTokens = rawMessage.split("\n");
			boolean isMsgTypeFound = false;
			boolean isElementsMatched = false;
			if (messageTokens != null && messageTokens.length > 0) {

				TOK: for (int i = 0; i < messageTokens.length; i++) {

					String sElement = messageTokens[i];
					if (sElement != null) {
						sElement = sElement.trim();
					}

					if (!isElementsMatched) {
						for (String tokenKey : keyTokensMap.keySet()) {
							String token = keyTokensMap.get(tokenKey);

							pattern = Pattern.compile(token);
							matcher = pattern.matcher(sElement);

							if (matcher.matches()
									&& MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS.equals(tokenKey)) {
								recipientAddress = matcher.toMatchResult().group(1);
							}

							if (matcher.matches() && MesageParserConstants.GDSMessageElements.SENDER_ADDRESS.equals(tokenKey)) {
								senderAddress = matcher.toMatchResult().group(1);
							}

							if (matcher.matches() && MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER.equals(tokenKey)
									&& validMsgTypeList.contains(sElement)) {

								msgType = sElement;
								isMsgTypeFound = true;

							}

							if (isMsgTypeFound && matcher.matches()
									&& MesageParserConstants.GDSMessageElements.MESSAGE_REFERENCE.equals(tokenKey)) {
								msgReference = matcher.toMatchResult().group(2);
								isElementsMatched = true;
							}

						}
					}

					if (isMsgTypeFound && removeAddress) {
						newMsgText.append(sElement + "\n");
					}

				}
			}
			if (newMsgText.toString().isEmpty()) {
				newMsgText.append(rawMessage);
			}
		}
		resultStr[0] = msgType;
		resultStr[1] = newMsgText.toString();
		resultStr[2] = senderAddress;
		resultStr[3] = recipientAddress;
		resultStr[4] = msgReference;
		return resultStr;

	}

	private static boolean isArrivalNextDay(Date startTime, Date endTime, int depOffSet, int arrOffSet) throws ParseException {
		boolean isNextDay = false;

		if (depOffSet == 0 && arrOffSet == 0) {
			// Start Time
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(startTime);

			// Current Time
			java.util.Date currentTime = new SimpleDateFormat("HH:mm:ss").parse("00:00:00");
			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.setTime(currentTime);

			// End Time
			Calendar endCalendar = Calendar.getInstance();
			endCalendar.setTime(endTime);

			//
			if (currentTime.compareTo(endTime) < 0) {

				currentCalendar.add(Calendar.DATE, 1);
				currentTime = currentCalendar.getTime();

			}

			if (startTime.compareTo(endTime) < 0) {

				startCalendar.add(Calendar.DATE, 1);
				startTime = startCalendar.getTime();

			}

			if (currentTime.before(startTime)) {
				// Time is Lesser
				isNextDay = false;
			} else {
				if (currentTime.after(endTime)) {
					// day +
					endCalendar.add(Calendar.DATE, 1);
					endTime = endCalendar.getTime();

				}

				if (currentTime.before(endTime)) {
					// RESULT, Time lies b/w
					isNextDay = true;
				} else {
					// RESULT, Time does not lies b/w
					isNextDay = false;

				}

			}
		}

		return isNextDay;
	}

	private static boolean isMarketingElementRequired(Action action) {
		List<SSMASMSUBMessegeDTO.Action> validMsgTypeList = new ArrayList<SSMASMSUBMessegeDTO.Action>();
		validMsgTypeList.add(SSMASMSUBMessegeDTO.Action.CNL);
		validMsgTypeList.add(SSMASMSUBMessegeDTO.Action.REV);
		validMsgTypeList.add(SSMASMSUBMessegeDTO.Action.FLT);

		if (validMsgTypeList.contains(action)) {
			return false;
		}

		return true;
	}

	private static Collection<DayOfWeek> getOperationDays(String daysOfOperation) {

		Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();
		int i = 0;
		if (daysOfOperation != null && !daysOfOperation.trim().equals("") && daysOfOperation.trim().length() > 0) {
			while (daysOfOperation.length() > i && !daysOfOperation.substring(i, i + 1).trim().equals("")) {
				int key = Integer.parseInt(daysOfOperation.substring(i, i + 1));
				switch (key) {
				case 1:
					dayList.add(DayOfWeek.MONDAY);
					break;
				case 2:
					dayList.add(DayOfWeek.TUESDAY);
					break;
				case 3:
					dayList.add(DayOfWeek.WEDNESDAY);
					break;
				case 4:
					dayList.add(DayOfWeek.THURSDAY);
					break;
				case 5:
					dayList.add(DayOfWeek.FRIDAY);
					break;
				case 6:
					dayList.add(DayOfWeek.SATURDAY);
					break;
				case 7:
					dayList.add(DayOfWeek.SUNDAY);
					break;

				}

				i++;
			}
		}
		return dayList;
	}

	public static Date getZuluDateTime(String airportCode, String flightDate, String departureTime) throws ModuleException,
			ParseException {
		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());

		return timeAdder.getZuluDateTime(
				airportCode,
				CalendarUtil.getTimeAddedDate(CalendarUtil.parseDate(flightDate, false),
						CalendarUtil.getParsedTime(departureTime, TIME_FORMAT)));
	}


	
	private static void filterMarketingFlightNoForCodeshareCarrier(SSMASMSUBMessegeDTO ssmASMSUBMessegeDTO) {
		if (ssmASMSUBMessegeDTO.isCodeShareCarrier() && !StringUtil.isNullOrEmpty(ssmASMSUBMessegeDTO.getFlightDesignator())
				&& ssmASMSUBMessegeDTO.getMarketingFlightNos() != null && ssmASMSUBMessegeDTO.getMarketingFlightNos().size() > 0
				&& ssmASMSUBMessegeDTO.getMarketingFlightNos().contains(ssmASMSUBMessegeDTO.getFlightDesignator())) {
			ssmASMSUBMessegeDTO.getMarketingFlightNos().remove(ssmASMSUBMessegeDTO.getFlightDesignator());

			if (!StringUtil.isNullOrEmpty(ssmASMSUBMessegeDTO.getExternalFlightNo())
					&& !ssmASMSUBMessegeDTO.getMarketingFlightNos().isEmpty()
					&& ssmASMSUBMessegeDTO.getMarketingFlightNos().contains(ssmASMSUBMessegeDTO.getExternalFlightNo())) {
				ssmASMSUBMessegeDTO.getMarketingFlightNos().remove(ssmASMSUBMessegeDTO.getExternalFlightNo());
			}
		}
	}
}
