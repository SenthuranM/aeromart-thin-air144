package com.isa.thinair.msgbroker.core.dto;

import com.isa.thinair.msgbroker.api.dto.RequestDTO;

public class MessageParserResponseDTO extends ServiceResponseDTO{
    
    private static final long serialVersionUID = -7138714134804677354L;
    private String status;
    private String formData;
    private RequestDTO requestDTO;
    private boolean skipProcessing;    

    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public String getFormData() {
        return formData;
    }
    
    public void setFormData(String formData) {
        this.formData = formData;
    }

    public RequestDTO getRequestDTO() {
        return requestDTO;
    }

    public void setRequestDTO(RequestDTO requestDTO) {
        this.requestDTO = requestDTO;
    }

	public boolean isSkipProcessing() {
		return skipProcessing;
	}

	public void setSkipProcessing(boolean skipProcessing) {
		this.skipProcessing = skipProcessing;
	}
}
