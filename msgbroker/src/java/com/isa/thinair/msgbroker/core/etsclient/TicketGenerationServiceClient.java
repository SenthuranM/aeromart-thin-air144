package com.isa.thinair.msgbroker.core.etsclient;

import java.util.concurrent.TimeUnit;

import com.accelaero.ets.SampleServiceGrpc;
import com.accelaero.ets.SampleServiceWrapper.TestInput;
import com.accelaero.ets.SampleServiceWrapper.TestOutput;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class TicketGenerationServiceClient {
	
	private ManagedChannel channel;
	
	private SampleServiceGrpc.SampleServiceBlockingStub blockingStub;
	
	public TicketGenerationServiceClient(String host, int port) {
		channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
		blockingStub = SampleServiceGrpc.newBlockingStub(channel);
	}

	public void callSampleServiceMethod() {
		TestInput input = TestInput.newBuilder().build();
		TestOutput output;
		try {
			output = blockingStub.sampleMethod(input);
			System.out.println(output.getMessage());
		} catch (StatusRuntimeException e) {
			e.printStackTrace();
		}
	}
	
	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}
}
