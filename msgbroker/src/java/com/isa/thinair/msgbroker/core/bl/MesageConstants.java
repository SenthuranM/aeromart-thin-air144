package com.isa.thinair.msgbroker.core.bl;

public class MesageConstants {

	public static final String MESSAGE_FROM_ADDRESS = "MESSAGE_FROM_ADDRESS";
	public static final String MESSAGE_TO_ADDRESS = "MESSAGE_FROM_ADDRESS";
	
	public static interface InMessageQueueStatus {
		public static final String LOCK = "L";
		public static final String UNLOCK = "U";
	}

	public static interface InMessageScheduleType {
		public static final String FLIGHT = "F";
		public static final String SCHEDULE = "S";
	}
}
