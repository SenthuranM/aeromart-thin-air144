package com.isa.thinair.msgbroker.core.util;

import java.io.Serializable;

public final class MessageProcessingConstants implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5875538756670056497L;

	public static interface InMessageProcessingStatusErrorCodes {
		public static final String SERVICE_UNAVAILABLE = "inmessageprocessing.status.service.unavailable";
		public static final String INTERNAL_EXCEPTION = "inmessageprocessing.internal.error";
		public static final String REQUEST_TIMED_OUT = "inmessageprocessing.request.timedout";
		public static final String WEB_SERVICE_RESPONCE_RECEIVED = "inmessageprocessing.status.webservice.responcereceived";
		public static final String HOST_NOTIFICATION_RECEIVED = "inmessageprocessing.status.hostnotificationreceived";
		public static final String REQUEST_MESSAGE_RECEIVED = "inmessageprocessing.status.requestmessagereceived";
		public static final String GOT_THE_RESPONSE_FOR_THE_REQUEST = "inmessageprocessing.status.responsereceivedforrequest";
		public static final String UNKNOWN_REQUESTER = "inmessageprocessing.status.unknown.requesterttyaddress";
		public static final String DUPLICATE_IN_MESSAGE = "inmessageprocessing.status.duplicate.inmessage";
		public static final String SCHEDULE_LOCKED_FOR_UPDATE = "schedule.locked.for.another.update";
		public static final String SKIPPED_IN_MESSAGE = "inmessageprocessing.skipped.waiting.sequential.processing";
		// public static final String AVS_RESPONSE_RECEIVED = "inmessageprocessing.status.avs.responsereceived";
	}

	public static interface InMessageLockStatuses {
		public static final String LOCKED = "L";
		public static final String UNLOCKED = "U";
	}

	public static interface OutMessageProcessingStatusErrorCodes {
		public static final String MESSAGE_ACCEPTEDBYMAILSERVER = "outmessagesending.status.acceptedbymailserver";
		public static final String RESPONSE_QUEUED_FOR_TRANSMISSION = "outmessagesending.status.responsequeuedfortransmission";
	}

	public static interface MessageDirection {
		public static final String IN_MESSAGE = "I";
		public static final String OUT_MESSAGE = "O";
	}

	public static interface ModeOfCommunication {
		public static final String Email = "E";
		public static final String TTYAddress = "T";
		public static final String MatIP = "M";
		public static final String API = "I";
		public static final String Queue = "Q";
		public static final String SitaTex = "S";
	}

	public static interface InMessageProcessStatus {
		public static final String Unprocessed = "U"; // Done
		public static final String Unparsable = "E"; // Done
		public static final String Processed = "P"; // Web Service Responce Received
		public static final String TimedOut = "T"; // Done
		public static final String ErroInProcessing = "X"; // Done
	}

	public static interface OutMessageProcessStatus {
		public static final String InvalidResponseMessage = "I"; // Done _/
		public static final String Untransmitted = "N"; // Not MailServerAccepted //Done
		public static final String MailServerAccepted = "S"; // Sent // Message MailServerAccepted
		public static final String TimedOut = "D"; // Delayed -- //Done
		public static final String TransmissionFailure = "F"; // Done
		public static final String Sent = "T"; // Done
		public static final String Processing = "P"; // Done
	}
	
	public static interface InMessageSequenceBatchProcessedStatus {
		public static final String WAITING = "W";
		public static final String PROCESSING = "P";
		public static final String COMPLETED = "C";
		public static final String ErroInProcessing = "E"; 
	}

}
