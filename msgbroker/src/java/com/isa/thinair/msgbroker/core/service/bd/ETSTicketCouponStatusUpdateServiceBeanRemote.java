package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.ETSTicketCouponStatusUpdateBD;

/**
 * 
 * @author rajitha
 *
 */

@Remote
public interface ETSTicketCouponStatusUpdateServiceBeanRemote extends ETSTicketCouponStatusUpdateBD {

}
