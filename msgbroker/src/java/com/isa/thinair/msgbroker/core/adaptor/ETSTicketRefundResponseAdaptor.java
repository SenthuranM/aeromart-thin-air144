package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.ets.SimplifiedTicketServiceWrapper;
import com.isa.cloud.support.generic.adaptor.AbstractRequestGrpcDtoAdapter;
import com.isa.thinair.commons.api.dto.ets.RefundTKTRESResponseDTO;

public class ETSTicketRefundResponseAdaptor
		extends AbstractRequestGrpcDtoAdapter<SimplifiedTicketServiceWrapper.RefundTKTRESResponse, RefundTKTRESResponseDTO> {

	public ETSTicketRefundResponseAdaptor() {
		super(SimplifiedTicketServiceWrapper.RefundTKTRESResponse.class, RefundTKTRESResponseDTO.class);
	}

}
