package com.isa.thinair.msgbroker.core.bl.avs;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.AVSRequestDTO;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;

/**
 * Type B Message Parser.
 * 
 * @author M.Rikaz
 * 
 */
public class AVSMessageParser {
	public MessageParserResponseDTO parseMessage(String messageBody) throws ModuleException {
		Throwable exception = null;

		MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
		AVSRequestDTO metaDataDTO = null;
		try {

			metaDataDTO = AvsParser.process(messageBody);
			oMessageParserResponseDTO.setRequestDTO(metaDataDTO);
			oMessageParserResponseDTO.setSuccess(true);

		} catch (Exception e) {
			 e.printStackTrace();
			oMessageParserResponseDTO.setRequestDTO(null);
			oMessageParserResponseDTO.setSuccess(false);
			oMessageParserResponseDTO.setStatus("ERROR PARSING FAILED");
		}

		return oMessageParserResponseDTO;
	}

}
