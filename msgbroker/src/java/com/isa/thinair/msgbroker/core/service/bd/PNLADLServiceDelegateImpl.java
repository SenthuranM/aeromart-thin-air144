/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.PNLADLServiceBD;

/**
 * @author Ishan
 */
@Remote
public interface PNLADLServiceDelegateImpl extends PNLADLServiceBD {

}
