package com.isa.thinair.msgbroker.core.adaptor;

import com.isa.cloud.support.generic.adaptor.GenericGRPCBuilderMapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;

public abstract class AbstractGrpcRequestCreator<GRPC,DTO> {

	private Class<GRPC> grpcClass;
	
	private Class<DTO> dtoClass;

	public AbstractGrpcRequestCreator(Class<GRPC> grpcClass, Class<DTO> dtoClass) {
		this.grpcClass = grpcClass;
		this.dtoClass = dtoClass;
	}

	public GRPC fromDTO(Object dto) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		Class[] emptyParams = new Class[0];
		if(dto != null) {
			Method e = this.grpcClass.getDeclaredMethod("newBuilder", emptyParams);
			Object grpcReqBuilder = e.invoke(GenericGRPCBuilderMapper.getDefaultGRPCObjectFromClassName(this.grpcClass.getName()), new Object[0]);
			Field grpcPayLoadField = this.getPayLoadFieldFromGRPC(grpcReqBuilder.getClass());
			String setterName = null;
			String replacedFieldName = grpcPayLoadField.getName().replace("_", "");
			if(Iterable.class.isAssignableFrom(dto.getClass())) {
				setterName = GenericGRPCBuilderMapper.getAllSetterMethodNameForIterable(replacedFieldName);
			} else if(Map.class.isAssignableFrom(dto.getClass())) {
				setterName = GenericGRPCBuilderMapper.getMutableMethodNameForMap(replacedFieldName);
			} else {
				setterName = GenericGRPCBuilderMapper.getSetterMethodName(replacedFieldName);
			}

			return this.setGRPCObjectByField(grpcReqBuilder, dto, grpcPayLoadField.getName(), setterName);
		}	
		return null;
	}

	private Field getPayLoadFieldFromGRPC(Class<?> grpcBuilderClass) {
		Field[] grpcResponseFields = grpcBuilderClass.getDeclaredFields();
		Field[] var3 = grpcResponseFields;
		int var4 = grpcResponseFields.length;

		for(int var5 = 0; var5 < var4; ++var5) {
			Field grpcField = var3[var5];
			if(!grpcField.getName().endsWith("Builder_") && !"error_".equals(grpcField.getName()) && !"warnings_".equals(grpcField.getName()) && !"requestIdentity_".equals(grpcField.getName()) && !"bitField0_".equals(grpcField.getName())) {
				return grpcField;
			}
		}

		return null;
	}

	private GRPC setGRPCObjectByField(Object grpcReqBuilder, Object object, String fieldName, String setterName) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		GenericGRPCBuilderMapper mapper = null;
		Object grpcObject = null;
		Field grpcField = null;
		Type grpcType = null;
		Method method;
		if(Iterable.class.isAssignableFrom(object.getClass())) {
			grpcField = GenericGRPCBuilderMapper.getFiledByName(grpcReqBuilder, fieldName);
			grpcType = GenericGRPCBuilderMapper.getTypeFromField(grpcField);
			grpcObject = this.getGrpcCollectionFromDtoCollection((Iterable)object, Class.forName(grpcType.getTypeName()));
		} else if(Map.class.isAssignableFrom(object.getClass())) {
			Map grpcObject1 = this.getGrpcMapFromDTOMap((Map)object, grpcReqBuilder, fieldName);
			if(grpcObject1 != null) {
				method = grpcReqBuilder.getClass().getDeclaredMethod(setterName, new Class[0]);
				Map builderMap = (Map)method.invoke(grpcReqBuilder, new Object[0]);
				builderMap.putAll((Map)grpcObject1);
			}

			grpcObject = null;
		} else if(object.getClass().isEnum()) {
			grpcObject = this.getGrpcEnumFromDTOEnum((Enum)object, setterName, grpcReqBuilder);
		} else {
			mapper = new GenericGRPCBuilderMapper(Class.forName(this.grpcClass.getName()));
			grpcObject = mapper.map(object);
		}
		return (GRPC)grpcObject;
	}

	private List getGrpcCollectionFromDtoCollection(Iterable<Object> dtoList, Class<?> grpcClass) {
		Class dtoClass = this.getGenericTypeOfList(dtoList);
		ArrayList grpcList = new ArrayList();
		if(dtoClass != null) {
			if(GenericGRPCBuilderMapper.isPrimitiveOrString(dtoClass)) {
				grpcList.addAll((Collection)dtoList);
			} else {
				GenericGRPCBuilderMapper mapper = new GenericGRPCBuilderMapper(grpcClass);
				Iterator var6 = dtoList.iterator();

				while(var6.hasNext()) {
					Object dtoObject = var6.next();
					grpcList.add(mapper.map(dtoObject));
				}
			}
		}

		return grpcList;
	}

	private Class<?> getGenericTypeOfList(Iterable<Object> objects) {
		if(objects != null) {
			Iterator var2 = objects.iterator();
			if(var2.hasNext()) {
				Object object = var2.next();
				return object.getClass();
			}
		}
		return null;
	}

	private Map getGrpcMapFromDTOMap(Map dtoMap, Object grpcReqBuilder, String fieldName) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if(dtoMap != null) {
			Object[] dtoMapGenericTypes = this.getMapKeyValueGenericType(dtoMap);
			Type[] grpcMapGenericTypes = GenericGRPCBuilderMapper.getMapKeyValueGenericTypeFromGetter(fieldName, grpcReqBuilder);
			boolean isKeyPrivimitive = GenericGRPCBuilderMapper.isPrimitiveOrString((Class)dtoMapGenericTypes[0]);
			boolean isValuePrivimitive = GenericGRPCBuilderMapper.isPrimitiveOrString((Class)dtoMapGenericTypes[1]);
			Class grpcKeyClass = (Class)grpcMapGenericTypes[0];
			Class grpcValueClass = (Class)grpcMapGenericTypes[1];
			GenericGRPCBuilderMapper keyMapper = null;
			GenericGRPCBuilderMapper valueMapper = null;
			if(!isKeyPrivimitive && !grpcKeyClass.isEnum()) {
				keyMapper = new GenericGRPCBuilderMapper(grpcKeyClass);
			}

			if(!isValuePrivimitive && !grpcValueClass.isEnum()) {
				valueMapper = new GenericGRPCBuilderMapper(grpcValueClass);
			}

			HashMap grpcMap = new HashMap();
			Object grpcKey = null;
			Object grpcValue = null;

			for(Iterator var15 = dtoMap.keySet().iterator(); var15.hasNext(); grpcMap.put(grpcKey, grpcValue)) {
				Object object = var15.next();
				if(isKeyPrivimitive) {
					grpcKey = object;
				} else if(grpcKeyClass.isEnum()) {
					grpcKey = GenericGRPCBuilderMapper.getGrpcEnumFromClass(grpcKeyClass, ((Enum)grpcKey).name());
				} else {
					grpcKey = keyMapper.map(object);
				}

				if(isValuePrivimitive) {
					grpcValue = dtoMap.get(object);
				} else if(grpcValueClass.isEnum()) {
					grpcValue = GenericGRPCBuilderMapper.getGrpcEnumFromClass(grpcValueClass, ((Enum)dtoMap.get(object)).name());
				} else {
					grpcValue = valueMapper.map(dtoMap.get(object));
				}
			}

			return grpcMap;
		} else {
			return null;
		}
	}

	private Object[] getMapKeyValueGenericType(Map map) {
		if(map != null) {
			ArrayList classList = new ArrayList();
			Iterator var3 = map.keySet().iterator();
			if(var3.hasNext()) {
				Object key = var3.next();
				classList.add(key.getClass());
				classList.add(map.get(key).getClass());
			}

			return classList.toArray();
		} else {
			return null;
		}
	}

	private Enum getGrpcEnumFromDTOEnum(Enum dtoEnum, String setterMethodName, Object grpcReqBuilder) throws NoSuchMethodException, SecurityException, ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method builderMethod = GenericGRPCBuilderMapper.getMethodByName(grpcReqBuilder.getClass().getDeclaredMethods(), setterMethodName);
		Type grpcEnumType = GenericGRPCBuilderMapper.getEnumTypeFromMethodParameters(builderMethod.getParameters());
		Method grpcEnumMethod = Class.forName(grpcEnumType.getTypeName()).getDeclaredMethod("valueOf", new Class[]{String.class});
		return (Enum)grpcEnumMethod.invoke((Object)null, new Object[]{dtoEnum.name()});
	}
}
