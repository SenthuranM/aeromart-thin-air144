package com.isa.thinair.msgbroker.core.dto;

import java.io.Serializable;

/**
 * responce interface to return responce form the service
 * @author Lasantha Pambagoda
 */
public class ServiceResponseDTO implements Serializable {

	String reasonCode; //Contains reason code about the operation success or failure.
	boolean isSuccess; //Contains whether the operation is success or fail
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}	
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

}
