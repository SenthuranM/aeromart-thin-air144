package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;

public class TypeBUnifier {
	private TypeBUnifier () { }

	public static void unifyInbound(BookingRequestDTO bookingRequestDTO) throws ModuleException {

		SSRDocsDTO ssrDocsDTO;
		SSRDocoDTO ssrDocoDTO;
		LocationBD locationBD = MsgbrokerUtils.getLocationBD();
		Country country;

		if (bookingRequestDTO.getSsrDTOs() != null) {
			for (SSRDTO ssrdto : bookingRequestDTO.getSsrDTOs()) {
				if (ssrdto.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.DOCS)) {
					ssrDocsDTO = (SSRDocsDTO) ssrdto;

					if (ssrDocsDTO.getPassengerNationality() != null && ssrDocsDTO.getPassengerNationality().length() == 3) {
						country = locationBD.getCountryByIsoCode(ssrDocsDTO.getPassengerNationality());
						if (country != null) {
							ssrDocsDTO.setPassengerNationality(country.getCountryCode());
						}
					}

					if (ssrDocsDTO.getDocCountryCode() != null && ssrDocsDTO.getDocCountryCode().length() == 3) {
						country = locationBD.getCountryByIsoCode(ssrDocsDTO.getDocCountryCode());
						if (country != null) {
							ssrDocsDTO.setDocCountryCode(country.getCountryCode());
						}
					}
				}
				if (ssrdto.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.DOCO)) {
					ssrDocoDTO = (SSRDocoDTO) ssrdto;

					if (ssrDocoDTO.getVisaApplicableCountry() != null && ssrDocoDTO.getVisaApplicableCountry().length() == 3) {
						country = locationBD.getCountryByIsoCode(ssrDocoDTO.getVisaApplicableCountry());
						if (country != null) {
							ssrDocoDTO.setVisaApplicableCountry(country.getCountryCode());
						}
					}

				}
			}
		}

	}
}
