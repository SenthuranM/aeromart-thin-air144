package com.isa.thinair.msgbroker.core.dto;


import java.util.List;

public abstract class TextMessage {

	public abstract void parseMessage(List<String> message);

	public abstract List<String> generateMessage();


}
