package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.ETSTicketGenerationServiceBD;

@Remote
public interface ETSTicketGenerationServiceBeanRemote extends ETSTicketGenerationServiceBD {

}
