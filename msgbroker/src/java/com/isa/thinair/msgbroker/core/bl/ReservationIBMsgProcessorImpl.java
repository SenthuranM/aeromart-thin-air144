package com.isa.thinair.msgbroker.core.bl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.msgbroker.core.util.GdsErrorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentTypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.MessageError;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GdsApplicationError;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.dto.InNacDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.dto.MessageComposerResponseDTO;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminJdbcDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.util.BookingRequestDTOConverter;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.GDSConfigAdaptor;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;

public class ReservationIBMsgProcessorImpl implements IBMsgProcessorBD {

	private final Log log = LogFactory.getLog(ReservationIBMsgProcessorImpl.class);

	private AdminDAO adminDAO;

	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	private OutMessageProcessingStatus outMessageProcessingStatus;

	private ServiceClientDTO serviceClientDTO;

	private ServiceBearerDTO serviceBearerDTO;

	private ServiceClientTTYDTO serviceClientTTYDTO;

	public InMessageProcessResultsDTO processAIRIMPMessage(InMessage inMessage) {
		MessageParserResponseDTO messageParserResponseDTO;
		InMessageProcessingStatus inMessageProcessingStatus;
		InMessageParser inMessageParser = new InMessageParser();
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();

		if (inMessage != null) {

			messageParserResponseDTO = inMessageParser.parseMessage(inMessage);

			if (messageParserResponseDTO.isSuccess() && !messageParserResponseDTO.isSkipProcessing()) {

				Object obj = messageParserResponseDTO.getRequestDTO();

				if (obj instanceof BookingRequestDTO) {

					BookingRequestDTO bookingRequestDTO = (BookingRequestDTO) obj;
					inMessage.setSentTTYAddress(bookingRequestDTO.getOriginatorAddress());
					inMessage.setMessageType(bookingRequestDTO.getMessageIdentifier());
					if (inMessage.getRecordLocator() == null || inMessage.getRecordLocator() == "") {
						inMessage.setRecordLocator(bookingRequestDTO.getOriginatorRecordLocator().getPnrReference());
					}
					inMessage.setProcessedDate(new Date());
					OutMessage outMessage = new OutMessage();
					outMessage.setAirLineCode(inMessage.getAirLineCode());
					outMessage.setRecordLocator(inMessage.getRecordLocator());
					outMessage.setGdsMessageID(inMessage.getGdsMessageID());
					outMessage.setMessageType(inMessage.getMessageType());
					outMessage.setRefInMessageID(inMessage.getInMessageID());
					outMessage.setModeOfCommunication(inMessage.getModeOfCommunication());
					outMessage.setProcessedDate(new Date());
					outMessage.setReceivedDate(new Date());

					try {
						String senderAddress = messageParserResponseDTO.getRequestDTO().getOriginatorAddress();
						String messageType = inMessage.getMessageType();
						setGDSConfigsForProcessingMessage(senderAddress, messageType);

						if (serviceClientDTO != null && serviceBearerDTO != null) {
							inMessage.setServiceClientCode(serviceClientDTO.getServiceClientCode());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
							outMessage.setRecipientTTYAddress(serviceClientTTYDTO.getOutTTYAddress());
							outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(
									serviceClientTTYDTO.getOutTTYAddress(), serviceClientDTO.getEmailDomain()));

							bookingRequestDTO.setMessageReceivedDateStamp(inMessage.getReceivedDate());
							com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO accelaeroBookingRequestDTO = BookingRequestDTOConverter
									.convertMsgBrokerBookingRequestDTOToAccelaero(bookingRequestDTO, inMessage.getInMessageID(),
											serviceClientDTO.getServiceClientCode());
							TypeBUnifier.unifyInbound(accelaeroBookingRequestDTO);

							GDSServicesBD gdsServicesBD = MsgbrokerUtils.getGDSServicesBD();
							Map<String, com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO> requestDetailMap = new HashMap<String, com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO>();
							requestDetailMap.put(accelaeroBookingRequestDTO.getUniqueAccelAeroRequestId(),
									accelaeroBookingRequestDTO);
							Map responceDetailMap = gdsServicesBD.processRequests(requestDetailMap);

							com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO bookingResponseDTO = (com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO) responceDetailMap
									.get(accelaeroBookingRequestDTO.getUniqueAccelAeroRequestId());

							if (serviceClientTTYDTO.getOutTTYAddress() != null
									&& serviceClientTTYDTO.getOutTTYAddress() != bookingRequestDTO.getOriginatorAddress()) {
								bookingRequestDTO.setOriginatorAddress(serviceClientTTYDTO.getOutTTYAddress());
							}
							
							if (bookingResponseDTO.getProcessStatus().getNotifyStatus().equals(GDSExternalCodes.NotifyStatus.PROCEED)) {
								if(!bookingResponseDTO.getMessageIdentifier().equals(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode())){
									int typeBSyncType = TTYMessageUtil.getTypeBSyncTypeOfGDS(serviceClientDTO.getServiceClientCode());
									composerResponseDTO = messageComposer.composeGDSBookingMessage(
											BookingRequestDTOConverter.syncronizeMsgBrokerBookingRequestDTOWithAccelaero(
													bookingRequestDTO, bookingResponseDTO), serviceBearerDTO.getAirlineCode(),
											typeBSyncType, serviceClientDTO.getAddAddressToMessageBody(), false);
	
									if (composerResponseDTO.isSuccess()) {
										outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
									} else {
										// TODO There should be a way of informing the failed state to the reservation
										// system
									}
								}
								outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessage(outMessage);
								inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

								inMessageProcessingStatus = new InMessageProcessingStatus();
								inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
								inMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.GOT_THE_RESPONSE_FOR_THE_REQUEST);
								inMessageProcessingStatus.setProcessingDate(new Date());
								inMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
								inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

								outMessageProcessingStatus = new OutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								outMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
								outMessageProcessingStatus.setProcessingDate(new Date());
								outMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
							} else if (bookingResponseDTO.getProcessStatus().getNotifyStatus()
									.equals(GDSExternalCodes.NotifyStatus.NAC)) {
								composerResponseDTO = messageComposer.composeNACMessage(inMessage.getInMessageText(),
										inMessage.getAirLineCode(), bookingResponseDTO.getResponseMessage(), inMessage.getModeOfCommunication());

								if (composerResponseDTO.isSuccess()) {
									outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
									outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
									outMessage
											.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
									inMessageProcessResultsDTO.setOutMessage(outMessage);
									inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
								} else {
									// TODO There should be a way of informing the failed state to the reservation
									// system
								}
							} else if (bookingResponseDTO.getProcessStatus().getNotifyStatus()
									.equals(GDSExternalCodes.NotifyStatus.IGNORE)) {
								inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

								inMessageProcessingStatus = new InMessageProcessingStatus();
								inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
								inMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.GOT_THE_RESPONSE_FOR_THE_REQUEST);
								inMessageProcessingStatus.setProcessingDate(new Date());
								inMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
								inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
							}
						} else {
							inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMessage.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
						}

					} catch (ModuleException e) {
						inMessageProcessingStatus = new InMessageProcessingStatus();
						inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
						if (e.getExceptionCode() != null) {
							inMessageProcessingStatus.setErrorMessageCode(e.getExceptionCode());
						} else {
							inMessageProcessingStatus.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
						}
						inMessageProcessingStatus.setErrorMessageText(e.getMessageString());
						inMessageProcessingStatus.setProcessingDate(new Date());
						inMessageProcessingStatus
								.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessage.setLastErrorMessageCode(e.getExceptionCode());

						// Send NAC
						composerResponseDTO = messageComposer.composeNACMessage(inMessage.getInMessageText(),
								inMessage.getAirLineCode(), "ERROR MESSAGE", inMessage.getModeOfCommunication());

						if (composerResponseDTO.isSuccess()) {
							outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
							inMessageProcessResultsDTO.setOutMessage(outMessage);
							inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMessageProcessingStatus
									.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
						}
					} catch (Exception e) {
						log.error(e);
						inMessageProcessingStatus = new InMessageProcessingStatus();
						inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
						inMessageProcessingStatus
								.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
						inMessageProcessingStatus.setErrorMessageText(e.getMessage());
						inMessageProcessingStatus.setProcessingDate(new Date());
						inMessageProcessingStatus
								.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessage
								.setLastErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);

						// Send NAC
						composerResponseDTO = messageComposer.composeNACMessage(inMessage.getInMessageText(),
								inMessage.getAirLineCode(), "ERROR MESSAGE", inMessage.getModeOfCommunication());

						if (composerResponseDTO.isSuccess()) {
							outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
							inMessageProcessResultsDTO.setOutMessage(outMessage);
							inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMessageProcessingStatus
									.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
						}
					}
				} else if (obj instanceof InNacDTO) {
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
				}

			} else if (!messageParserResponseDTO.isSuccess()) {

				String senderAddress = extractAddress(inMessage.getSentEmailAddress(), inMessage.getSentTTYAddress());
				String messageType = inMessage.getMessageType();
				setGDSConfigsForProcessingMessage(senderAddress, messageType);

				if (serviceClientDTO != null && serviceBearerDTO != null) {
					inMessageProcessingStatus = new InMessageProcessingStatus();
					inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
					inMessageProcessingStatus.setErrorMessageCode(messageParserResponseDTO.getReasonCode());
					inMessageProcessingStatus.setErrorMessageText(messageParserResponseDTO.getStatus());
					inMessageProcessingStatus.setProcessingDate(new Date());
					inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
					inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
					inMessage.setProcessedDate(new Date());
					inMessage.setLastErrorMessageCode(messageParserResponseDTO.getReasonCode());
					composerResponseDTO = messageComposer.composeNACMessage(inMessage.getInMessageText(),
							inMessage.getAirLineCode(), messageParserResponseDTO.getStatus(), inMessage.getModeOfCommunication());
					if (composerResponseDTO.isSuccess()) {
						OutMessage outMessage = new OutMessage();
						outMessage.setAirLineCode(inMessage.getAirLineCode());
						outMessage.setRecordLocator(inMessage.getRecordLocator());
						outMessage.setGdsMessageID(inMessage.getGdsMessageID());
						outMessage.setMessageType(inMessage.getMessageType());
						outMessage.setRefInMessageID(inMessage.getInMessageID());
						outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
						outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
						String receiverTTYAddress = null;
						if (AppSysParamsUtil.isGDSCsConfigsFromCache()) {
							receiverTTYAddress = TTYMessageUtil.getOutTTYForServiceClientReqType(
									serviceClientDTO.getServiceClientCode(), "NAC");
						} else {
							receiverTTYAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(
									serviceClientDTO.getServiceClientCode(), "NAC");
						}
						outMessage.setRecipientTTYAddress(receiverTTYAddress);
						outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(
								receiverTTYAddress, serviceClientDTO.getEmailDomain()));
						outMessage.setModeOfCommunication(inMessage.getModeOfCommunication());
						outMessage.setProcessedDate(new Date());
						outMessage.setReceivedDate(new Date());
						outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
						outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
						inMessageProcessResultsDTO.setOutMessage(outMessage);
						inMessage
								.setSentTTYAddress(extractAddress(inMessage.getSentEmailAddress(), inMessage.getSentTTYAddress()));

					}
				} else {
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
					inMessage.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
				}
			}
			inMessageProcessResultsDTO.setInMessage(inMessage);
			return inMessageProcessResultsDTO;
		}
		return null;
	}

	public InMessageProcessResultsDTO processAIRIMPMessage(TypeBRQ typeBRQ) {

		String message = typeBRQ.getTypeBMessage();
		MessageParserResponseDTO messageParserResponseDTO;
		InMessageProcessingStatus inMessageProcessingStatus;
		InMessageParser inMessageParser = new InMessageParser();
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();
		InMessage inMsg = new InMessage();
		MessageError messageError = null;
		List<String> errorCode = null;

		if (message != null) {

			messageParserResponseDTO = inMessageParser.parseMessage(message);

			if (messageParserResponseDTO.isSuccess()) {

				Object obj = messageParserResponseDTO.getRequestDTO();

				if (obj instanceof BookingRequestDTO) {

					BookingRequestDTO bookingRequestDTO = (BookingRequestDTO) obj;
					inMsg.setSentTTYAddress(bookingRequestDTO.getOriginatorAddress());
					inMsg.setMessageType(bookingRequestDTO.getMessageIdentifier());
					inMsg.setRecordLocator(bookingRequestDTO.getOriginatorRecordLocator().getPnrReference());
					inMsg.setProcessedDate(new Date());
					OutMessage outMessage = new OutMessage();
					outMessage.setProcessedDate(new Date());
					outMessage.setReceivedDate(new Date());

					try {
						String senderAddress = messageParserResponseDTO.getRequestDTO().getOriginatorAddress();
						String messageType = Constants.ServiceTypes.ReservationService;
						setGDSConfigsForProcessingMessage(senderAddress, messageType);

						if (serviceClientDTO != null && serviceBearerDTO != null) {

							inMsg.setServiceClientCode(serviceClientDTO.getServiceClientCode());
							bookingRequestDTO.setMessageReceivedDateStamp(new Date());
							com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO accelaeroBookingRequestDTO = BookingRequestDTOConverter
									.convertMsgBrokerBookingRequestDTOToAccelaero(bookingRequestDTO, null,
											serviceClientDTO.getServiceClientCode());
							TypeBUnifier.unifyInbound(accelaeroBookingRequestDTO);

							if (bookingRequestDTO.getMessageIdentifier() != null
									&& bookingRequestDTO.getMessageIdentifier()
											.equals(MesageParserConstants.MessageTypes.BOOKING)
									&& (typeBRQ instanceof InventoryAdjustmentTypeBRQ)) {
								accelaeroBookingRequestDTO.setBlockedSeats(((InventoryAdjustmentTypeBRQ) typeBRQ)
										.getBlockedSeats());
							}

							GDSServicesBD gdsServicesBD = MsgbrokerUtils.getGDSServicesBD();
							Map<String, com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO> requestDetailMap = new HashMap<String, com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO>();
							requestDetailMap.put(accelaeroBookingRequestDTO.getUniqueAccelAeroRequestId(),
									accelaeroBookingRequestDTO);
							Map responceDetailMap = gdsServicesBD.processRequests(requestDetailMap);

							com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO bookingResponseDTO = (com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO) responceDetailMap
									.get(accelaeroBookingRequestDTO.getUniqueAccelAeroRequestId());

							if (serviceClientTTYDTO.getOutTTYAddress() != null
									&& serviceClientTTYDTO.getOutTTYAddress() != bookingRequestDTO.getOriginatorAddress()) {
								bookingRequestDTO.setOriginatorAddress(serviceClientTTYDTO.getOutTTYAddress());
							}
							
							if (bookingResponseDTO.getProcessStatus().getNotifyStatus().equals(GDSExternalCodes.NotifyStatus.PROCEED)) {
								if((!bookingResponseDTO.getMessageIdentifier().equals(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode()))
										&& bookingResponseDTO.isSendResponse()){
									int typeBSyncType = TTYMessageUtil.getTypeBSyncTypeOfGDS(serviceClientDTO.getServiceClientCode());
									composerResponseDTO = messageComposer.composeGDSBookingMessage(
											BookingRequestDTOConverter.syncronizeMsgBrokerBookingRequestDTOWithAccelaero(
													bookingRequestDTO, bookingResponseDTO), serviceBearerDTO.getAirlineCode(),
											typeBSyncType, serviceClientDTO.getAddAddressToMessageBody(), false);
	
									if (composerResponseDTO.isSuccess()) {
										outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
										if (bookingResponseDTO.getProcessStatus().equals(
												GDSExternalCodes.ProcessStatus.INVOCATION_SUCCESS)) {
											outMessage.setProcessSuccess(true);
										}
										if (bookingRequestDTO.getResponderRecordLocator() != null
												&& bookingRequestDTO.getResponderRecordLocator().getPnrReference() != null) {
											outMessage.setPnr(bookingRequestDTO.getResponderRecordLocator().getPnrReference());
										}
									} else {
										// TODO There should be a way of informing the failed state to the reservation
										// system
									}
								}
								if(typeBRQ.isSaveInOutMsg()){
									outMessage.setMessageType(bookingRequestDTO.getResponseMessageIdentifier());
									if(bookingRequestDTO.getResponseMessageIdentifier() != null 
											&& bookingRequestDTO.getResponseMessageIdentifier().equals(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode())){
										outMessage.setRecipientTTYAddress(serviceClientTTYDTO.getInTTYAddress());
										outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(
												serviceClientTTYDTO.getOutTTYAddress(), serviceClientDTO.getEmailDomain()));
										outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
										outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
									}
								}
								
								outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessage(outMessage);
								inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

								inMessageProcessingStatus = new InMessageProcessingStatus();
								inMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.GOT_THE_RESPONSE_FOR_THE_REQUEST);
								inMessageProcessingStatus.setProcessingDate(new Date());
								inMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
								inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

								outMessageProcessingStatus = new OutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								outMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
								outMessageProcessingStatus.setProcessingDate(new Date());
								outMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);

								if (bookingResponseDTO.getProcessStatus() == GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE
										|| bookingResponseDTO.getProcessStatus() == GDSExternalCodes.ProcessStatus.GENERAL_FAILURE
										|| bookingResponseDTO.getProcessStatus() == GDSExternalCodes.ProcessStatus.INVOCATION_FAILURE) {

									messageError = new MessageError();
									messageError.addError(GdsApplicationError.SYSTEM_ERROR);
									inMessageProcessResultsDTO.setMessageError(messageError);
								}

							} else if (bookingResponseDTO.getProcessStatus().getNotifyStatus()
									.equals(GDSExternalCodes.NotifyStatus.NAC)) {

								composerResponseDTO = messageComposer.composeNACMessage(message,
										AppSysParamsUtil.getDefaultCarrierCode(), bookingResponseDTO.getResponseMessage(), null);

								if (composerResponseDTO.isSuccess()) {
									// outMessage.setRecipientTTYAddress(gds.getTTYAddress());
									outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
									outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
									outMessage
											.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
									inMessageProcessResultsDTO.setOutMessage(outMessage);
									inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
								}
							} else if (bookingResponseDTO.getProcessStatus().getNotifyStatus()
									.equals(GDSExternalCodes.NotifyStatus.IGNORE)) {
								inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

								inMessageProcessingStatus = new InMessageProcessingStatus();
								inMessageProcessingStatus.setInMessageID(inMsg.getInMessageID());
								inMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.GOT_THE_RESPONSE_FOR_THE_REQUEST);
								inMessageProcessingStatus.setProcessingDate(new Date());
								inMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
								inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
							}

 							errorCode = bookingResponseDTO.getErrors();

						} else {
							inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMsg.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
						}
					} catch (ModuleException e) {
						inMessageProcessingStatus = new InMessageProcessingStatus();
						inMessageProcessingStatus.setErrorMessageCode(e.getExceptionCode());
						inMessageProcessingStatus.setErrorMessageText(e.getMessageString());
						inMessageProcessingStatus.setProcessingDate(new Date());
						inMessageProcessingStatus
								.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

						inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMsg.setLastErrorMessageCode(e.getExceptionCode());

						// Send NAC
						composerResponseDTO = messageComposer.composeNACMessage(message,
								AppSysParamsUtil.getDefaultCarrierCode(), "ERROR MESSAGE", null);

						if (composerResponseDTO.isSuccess()) {
							outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
							inMessageProcessResultsDTO.setOutMessage(outMessage);
							inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMessageProcessingStatus
									.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
						}
					} catch (Exception e) {
						log.error(e);
						inMessageProcessingStatus = new InMessageProcessingStatus();
						inMessageProcessingStatus
								.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
						inMessageProcessingStatus.setErrorMessageText(e.getMessage());
						inMessageProcessingStatus.setProcessingDate(new Date());
						inMessageProcessingStatus
								.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

						inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMsg.setLastErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);

						// Send NAC
						composerResponseDTO = messageComposer.composeNACMessage(message,
								AppSysParamsUtil.getDefaultCarrierCode(), "ERROR MESSAGE", null);

						if (composerResponseDTO.isSuccess()) {
							outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
							outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
							outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
							inMessageProcessResultsDTO.setOutMessage(outMessage);
							inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
							inMessageProcessingStatus
									.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
						}
					}
				} else if (obj instanceof InNacDTO) {
					inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
				}
			} else {
				
				OutMessage outMessage = new OutMessage();
				outMessage.setProcessedDate(new Date());
				outMessage.setReceivedDate(new Date());
				
				composerResponseDTO = messageComposer.composeNACMessage(message,
						AppSysParamsUtil.getDefaultCarrierCode(), messageParserResponseDTO.getStatus(), null);

				if (composerResponseDTO.isSuccess()) {

					outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
					outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Sent);
					inMessageProcessResultsDTO.setOutMessage(outMessage);

				}
				
				inMessageProcessingStatus = new InMessageProcessingStatus();
				inMessageProcessingStatus.setErrorMessageCode(messageParserResponseDTO.getReasonCode());
				inMessageProcessingStatus.setErrorMessageText(messageParserResponseDTO.getStatus());
				inMessageProcessingStatus.setProcessingDate(new Date());
				inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
				inMsg.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMsg.setProcessedDate(new Date());
				inMsg.setLastErrorMessageCode(messageParserResponseDTO.getReasonCode());
			}

			if (errorCode != null) {
				if (messageError == null) {
					messageError = new MessageError();
				}

				// todo -- remove this and move to gds-services
				messageError.getErrors().clear();
				messageError.getErrors().addAll(GdsErrorUtil.toGdsApplicationErrors(errorCode));
				// todo - end

				messageError.getErrorCodes().clear();
				messageError.getErrorCodes().addAll(errorCode);
			}

			if (messageError != null) {
				inMessageProcessResultsDTO.setMessageError(messageError);
			}

			inMessageProcessResultsDTO.setInMessage(inMsg);
			return inMessageProcessResultsDTO;
		}
		return null;
	}

	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}
	
	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

	private String extractAddress(String emailID, String ttyAddress) {
		String address = "";
		if (emailID != null && !emailID.equals("")) {
			String[] splittedEmailAddress = emailID.split("@");
			address = splittedEmailAddress[0].trim().toUpperCase();
		} else if (ttyAddress != null && !ttyAddress.equals("")) {
			address = ttyAddress;
		}

		return address;
	}

	private void setGDSConfigsForProcessingMessage(String senderAddress, String messageType) {

		if (AppSysParamsUtil.isGDSCsConfigsFromCache()) {
			serviceClientTTYDTO = TTYMessageUtil.getServiceClientTTYForReqTypeTTY(senderAddress, messageType);
			if (serviceClientTTYDTO != null) {
				serviceClientDTO = TTYMessageUtil.getServiceClientByServiceClientCode(serviceClientTTYDTO.getServiceClientCode());
				serviceBearerDTO = TTYMessageUtil.getServiceBearerByServiceClientCode(serviceClientTTYDTO.getServiceClientCode());
			}
		} else {
			String serviceClientCode = getServiceClientReqTypeDAO().getServiceClientForReqTypeTTY(senderAddress, messageType);
			String receiverTTYAddress = getServiceClientReqTypeDAO()
					.getTTYForServiceClientReqType(serviceClientCode, messageType);
			if (serviceClientCode != null && receiverTTYAddress != null) {
				serviceClientTTYDTO = new ServiceClientTTYDTO(serviceClientCode, null, receiverTTYAddress);
				ServiceClient serviceClient = getAdminDAO().getServiceClient(serviceClientCode);
				if (serviceClient != null) {
					serviceClientDTO = GDSConfigAdaptor.adapt(serviceClient);
					ServiceBearer serviceBearerAirline = getAdminDAO().getServiceBearer(AppSysParamsUtil.getDefaultCarrierCode(),
							serviceClientCode);
					if (serviceBearerAirline != null) {
						serviceBearerDTO = GDSConfigAdaptor.adapt(serviceBearerAirline);
					}
				}
			}
		}
	}
}
