package com.isa.thinair.msgbroker.core.dto;

/**
 * 
 * @author rajitha
 *
 */
public class QueuedFlightDesignatorInfoDTO {
	
	private String flightDesignator;
	
	private String newFlightDesignator;
	
	private boolean hasFlightDesignatorChanged;
	

	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public String getNewFlightDesignator() {
		return newFlightDesignator;
	}

	public void setNewFlightDesignator(String newFlightDesignator) {
		this.newFlightDesignator = newFlightDesignator;
	}

	public boolean isFlightDesignatorChanged() {
		return hasFlightDesignatorChanged;
	}

	public void setHasFlightDesignatorChanged(boolean isFLTMessage) {
		this.hasFlightDesignatorChanged = isFLTMessage;
	}
	
}
