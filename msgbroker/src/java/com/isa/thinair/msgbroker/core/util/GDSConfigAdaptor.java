package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;

public class GDSConfigAdaptor {
	
	public static ServiceClientDTO adapt(ServiceClient serviceClient) {
		ServiceClientDTO serviceClientDTO = new ServiceClientDTO();
		serviceClientDTO.setServiceClientCode(serviceClient.getServiceClientCode());
		serviceClientDTO.setAddAddressToMessageBody(serviceClient.getAddAddressToMessageBody());
		serviceClientDTO.setEmailDomain(serviceClient.getEmailDomain());
		serviceClientDTO.setApiUrl(serviceClient.getApiUrl());
		return serviceClientDTO;		
	}
	
	public static ServiceBearerDTO adapt(ServiceBearer serviceBearer) {
		ServiceBearerDTO serviceBearerDTO = new ServiceBearerDTO();
		serviceBearerDTO.setAirlineCode(serviceBearer.getAirlineCode());
		serviceBearerDTO.setOutgoingEmailFromAddress(serviceBearer.getOutgoingEmailFromAddress());
		serviceBearerDTO.setSitaAddress(serviceBearer.getSitaAddress());
		return serviceBearerDTO;
	}
}
