/**
 * 
 */
package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sanjeewaf
 */
/**
 * Class Holds Type-B message codes and abbreviations. ref : Chapter 7 at AIRIMP .
 */
public class TypeBMessageConstants {

	/** AIRIMP30.pdf Chapter-7 page no 219: */

	/** Holds The SSR codes */

	public static interface SSRCodes {

		/** Constant string for SSR code "CHLD" (child info) . */
		public static final String CHLD = "CHLD";

		/** Constant string for SSR code "PSPT" (passport info). */
		public static final String PSPT = "PSPT";

		/** Constant string for SSR code "OTHS" (other remarks). */
		public static final String OTHS = "OTHS";

		/** Constant string for SSR code "FOID" . */
		public static final String FOID = "FOID";

		/** Constant string for SSR code "DOCS" (travel docs). */
		public static final String DOCS = "DOCS";
		
		/** Constant string for SSR code "DOCO" (travel docs Visa). */
		public static final String DOCO = "DOCO";

		/** Constant string for SSR code "PCTC" (emmergancy contact info). */
		public static final String PCTC = "PCTC";

		/** Constant string for SSR infant code INFT . */
		public static final String INFT = "INFT";

		/** Constant string for SSR code UMNR (unaccompanied minor). */
		public static final String UMNR = "UMNR";

		/** Constant string for SSR infant code LANG . */
		public static final String LANG = "LANG";

		/** Constant string for SSR code "CKIN" . */
		public static final String CKIN = "CKIN";

		/** Constant string for SSR code "GRPF" . */
		public static final String GRPF = "GRPF";

		/** Constant string for SSR code "GRPS" . */
		public static final String GRPS = "GRPS";

		/** Constant string for SSR code "MEDA" . */
		public static final String MEDA = "MEDA";

		/** Constant string for SSR code "GPST" . */
		public static final String GPST = "GPST";
		
		public static final String TKNE = "TKNE";
		
		public static final String TKNR = "TKNR";

		public static final String TICKETING_TIME_LIMIT = "TKTL";
		
		public static final String FQTV = "FQTV";

		/**
		 * following osi's fills OSIPhoneNoDTO Contact CTC[] Address (home or hotel) .......... CTCA Business phone
		 * ................... CTCB Home phone ....................... CTCH Y Mobile phone ................... CTCM
		 * Travel Agent phone ............... CTCT Phone nature not known . ......... CTCP Datafax phone
		 * .................... CTCF
		 */
		/** Constant string for OSI telephone no . */
		public static final String CTCP = "CTCP";

		/** Constant string for OSI telephone no . */
		public static final String CTCB = "CTCB";

		/** Constant string for OSI telephone no . */
		public static final String CTCH = "CTCH";

		/** Constant string for OSI telephone no . */
		public static final String CTCA = "CTCA";

		/** Constant string for OSI telephone no . */
		public static final String CTCM = "CTCM";

		/** Constant string for OSI telephone no . */
		public static final String CTCT = "CTCT";

		/** Constant string for OSI telephone no . */
		public static final String CTCF = "CTCF";
		
		/** Constant string for OSI telephone no . */
		public static final String CTCE = "CTCE";

	}
	
	public static interface PhoneContactInfoType {
		public static final String HOME_TELEPHONE = "H";
		
		public static final String BUSINESS_TELEPHONE = "B";
		
		public static final String MOBILE_PHONE = "M";
		
		public static final String TRAVEL_AGENT_PHONE = "T";
		
		public static final String UNDEFINED_TELEPHONE_NUMBER = "P";
	}
	
	public static interface AddressInfoType {
		public static final String HOME_ADDRESS = "A";
	}
	
	public static interface EmailInfoType {
		public static final String EMAIL_ADDRESS = "E";
	}

	/** Holds The Advice Codes */
	public static interface AdviceCodes {

	}

	/** Holds The Status Codes */
	public static interface StatusCodes {

		/** Constant string for status code "RV" . */
		public static final String NV = "NV";

		/** Constant string for status code "NU" . */
		public static final String NU = "NU";

		/** Constant string for status code "FC" . */
		public static final String FC = "FC";

		/** Constant string for status code "EX" . */
		public static final String EX = "EX";

		/** Constant string for status code "HQ" . */
		public static final String HQ = "HQ";

		/** Constant string for status code "HS" . */
		public static final String HS = "HS";

		/** Constant string for status code "RR" . */
		public static final String RR = "RR";

		/** Constant string for status code "HW" . */
		public static final String HW = "HW";

		/** Constant string for status code "HA" . */
		public static final String HA = "HA";

		/** Constant string for status code "HN" . */
		public static final String HN = "HN";

		/** Constant string for status code "HL" . */
		public static final String HL = "HL";

		/** Constant string for status code "HK" . */
		public static final String HK = "HK";
		
		public static final String CH = "CH";
		
		public static final String HX = "HX";

		/** Constant string for status code "RV" . */
		public static final String RV = null;

	}

	/** Holds The Message Identifiers */
	public static interface MessageIdentifiers {

		public static final String AVS = "AVS";
		public static final String SSM = "SSM";
		public static final String ASM = "ASM";
		public static final String PFS = "PFS";
		public static final String ETL = "ETL";

	}

	/** Holds PaxTitles */
	public static interface PaxTitles {

		/** Constant string for passenger title "MR" . */
		public static final String MR = "MR";

		/** Constant string for passenger title "MS" . */
		public static final String MS = "MS";

		/** Constant string for passenger title "DR" . */
		public static final String DR = "DR";

		/** Constant string for passenger title "JR" . */
		public static final String JR = "JR";

		/** Constant string for passenger title "CHD" . */
		public static final String CHD = "CHD";

		/** Constant string for passenger title "MRS" . */
		public static final String MRS = "MRS";

		/** Constant string for passenger title "REV" . */
		public static final String REV = "REV";

		/** Constant string for passenger title "MSTR" . */
		public static final String MSTR = "MSTR";
		
		/** Constant string for passenger title "MISS" . */
		public static final String MISS = "MISS";

	}

	private static List<String> contactInfoList = null;
	
	private static List<String> phoneContactInfoType = null;

	static {

		contactInfoList = new ArrayList<String>();
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCP);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCA);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCT);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCB);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCH);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCM);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCF);
		contactInfoList.add(TypeBMessageConstants.SSRCodes.CTCE);
	}

	/**
	 * @return the cONTACT_INFO_LIST
	 */
	public static List<String> getContactInfoList() {
		return contactInfoList;
	}

	private static List<String> statusCodesList = null;

	static {

		statusCodesList = new ArrayList<String>();
		statusCodesList.add(TypeBMessageConstants.StatusCodes.EX);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.FC);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HA);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HL);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HN);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HQ);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HS);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HW);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.NU);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.NV);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.RR);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HK);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.RV);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.CH);
		statusCodesList.add(TypeBMessageConstants.StatusCodes.HX);
	}

	/**
	 * @return the statusCodesList
	 */
	public static List<String> getStatusCodesList() {
		return statusCodesList;
	}
	
	static {
		phoneContactInfoType = new ArrayList<String>();
		phoneContactInfoType.add(TypeBMessageConstants.PhoneContactInfoType.BUSINESS_TELEPHONE);
		phoneContactInfoType.add(TypeBMessageConstants.PhoneContactInfoType.HOME_TELEPHONE);
		phoneContactInfoType.add(TypeBMessageConstants.PhoneContactInfoType.MOBILE_PHONE);
		phoneContactInfoType.add(TypeBMessageConstants.PhoneContactInfoType.TRAVEL_AGENT_PHONE);
		phoneContactInfoType.add(TypeBMessageConstants.PhoneContactInfoType.UNDEFINED_TELEPHONE_NUMBER);
	}
	
	public static List<String> getPhoneContactInfoType () {
		return phoneContactInfoType;
	}

	public static interface CommonCodes {
		String LIEU_AIRLINE_DESIGNATOR = "YY";
	}
}
