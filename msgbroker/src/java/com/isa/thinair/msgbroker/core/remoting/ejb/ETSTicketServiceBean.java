package com.isa.thinair.msgbroker.core.remoting.ejb;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTREQDTO;
import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTRESDTO;
import com.isa.thinair.commons.api.dto.ets.IssueTKTRESResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.msgbroker.core.adaptor.IssueExchangeTKTREQAdaptor;
import com.isa.thinair.msgbroker.core.adaptor.IssueTKTRESAdaptor;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.accelaero.ets.SimplifiedTicketServiceGrpc;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.IssueExchangeTKTREQ;
import com.accelaero.ets.SimplifiedTicketServiceWrapper.IssueTKTRESResponse;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.ETSTicketServiceBeanRemote;
import com.isa.thinair.platform.api.LookupServiceFactory;

@Stateless
@RemoteBinding(jndiBinding = "TicketService.remote")
@LocalBinding(jndiBinding = "TicketService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETSTicketServiceBean extends PlatformBaseSessionBean implements ETSTicketServiceBeanLocal,
ETSTicketServiceBeanRemote{
	
	private final Log log = LogFactory.getLog(ETSTicketServiceBean.class);

	@Override
	public IssueExchangeTKTRESDTO issue(IssueExchangeTKTREQDTO issueExchangeTKTREQDTO) throws ModuleException {
	
		MsgBrokerModuleConfig moduleConfig = (MsgBrokerModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MsgbrokerConstants.MODULE_NAME);
		IssueExchangeTKTRESDTO issueExchangeTKTRESDTO = null;
		try {
			String etsHost = moduleConfig.getEtsHost();
			int etsPort = moduleConfig.getEtsPort();
			ManagedChannel channel = ManagedChannelBuilder.forAddress(etsHost, etsPort).usePlaintext(true).build();
			SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub = SimplifiedTicketServiceGrpc
					.newBlockingStub(channel);
			IssueExchangeTKTREQAdaptor requestAdaptor = new IssueExchangeTKTREQAdaptor();
			IssueExchangeTKTREQ input = requestAdaptor.fromDTO(issueExchangeTKTREQDTO);
			IssueTKTRESResponse issueTKTRESResponse = callIssueMethod(blockingStub, input);
			IssueTKTRESAdaptor responseAdaptor = new IssueTKTRESAdaptor();
			IssueTKTRESResponseDTO issueTKTRESResponseDTO = responseAdaptor.fromGRPC(issueTKTRESResponse);
			shutdown(channel);		
		} catch (Exception e) {
			log.info(e);
		}
		return issueExchangeTKTRESDTO;
	}
	
	public IssueTKTRESResponse callIssueMethod(SimplifiedTicketServiceGrpc.SimplifiedTicketServiceBlockingStub blockingStub, IssueExchangeTKTREQ input) {
		IssueTKTRESResponse output = null;
		try {
			output = blockingStub.issue(input);	
			System.out.println(output.getError());
		} catch (StatusRuntimeException e) {
			e.printStackTrace();
		}
		return output;
	}
	
	public void shutdown(ManagedChannel channel) throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}	
}
