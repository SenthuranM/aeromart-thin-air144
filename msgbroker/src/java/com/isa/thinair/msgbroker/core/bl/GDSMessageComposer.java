package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.config.TypeBMessageStructure;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.ChangedNamesDTO;
import com.isa.thinair.msgbroker.api.dto.OSIDTO;
import com.isa.thinair.msgbroker.api.dto.RecordLocatorDTO;
import com.isa.thinair.msgbroker.api.dto.SSRChildDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocoDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocsDTO;
import com.isa.thinair.msgbroker.api.dto.SSREmergencyContactDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRFoidDTO;
import com.isa.thinair.msgbroker.api.dto.SSRInfantDTO;
import com.isa.thinair.msgbroker.api.dto.SSRPassportDTO;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;
import com.isa.thinair.msgbroker.core.config.TypeBMessageStructure;
import com.isa.thinair.msgbroker.core.dto.MessageComposerResponseDTO;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.Message_Identifier;
import com.isa.thinair.msgbroker.core.util.MessageComposerUtil;

/**
 * Method Will Compose the incoming request DTO into a outgoing text message using relevant text message.
 * 
 * @author chan, Sanjeewaf, Manoj 
 */
public class GDSMessageComposer {

	private static Log logger = LogFactory.getLog(GDSMessageComposer.class);

	private String sStatus;

	public Message_Identifier message_identifier;

	public String bookingCode;

	List<SSRDetailDTO> ssrDetailDTOs;
	
	List<SSRDocsDTO> ssrDocsDTOs;
	
	List<SSRDocoDTO> ssrDocoDTOs;

	List<SSRPassportDTO> passportDTOs;

	List<SSREmergencyContactDetailDTO> emergencyContactDetailDTOs;

	List<SSRFoidDTO> foidDTOs;

	List<SSRInfantDTO> infantDTOs;

	List<SSRChildDTO> childDTOs;

	List<SSRDTO> ssrDTOs;

	List<OSIDTO> osiDTOs;

	String carrierCode = null;

	/**
	 * Method will compose the simple booking response
	 * 
	 * @param bookingRequestDTO
	 * @return MessageComposerResponseDTO contain the composed message and status.
	 */
	public MessageComposerResponseDTO composeGDSBookingMessage(BookingRequestDTO bookingRequestDTO, String serviceBearerAirlineCode,
			int typeBSyncType, boolean includeAddressInMsgBody, boolean includeOSI) {

		MessageComposerResponseDTO msgComposerResponseDTO = new MessageComposerResponseDTO();

		GDSInternalCodes.TypeBMessageCategory messageCategory = bookingRequestDTO.getMessageCategory();
		TypeBMessageStructure.MessageConfig messageConfig = null;

		if (messageCategory != null) {
			switch (messageCategory) {
				case SYNC_OUT:
					GDSExternalCodes.SyncMessageIdentifier syncMessageIdentifier =
							GDSExternalCodes.SyncMessageIdentifier.getSyncMessageIdentifier(bookingRequestDTO.getResponseMessageIdentifier());
	
					messageConfig = MessageComposerUtil.getSyncMessageType(typeBSyncType, syncMessageIdentifier);	
					break;
					
				case RESERVATION:
					messageConfig = MessageComposerUtil.getMessageConfig(typeBSyncType, bookingRequestDTO.getResponseMessageIdentifier());	
					break;
					
				case CODESHARE_OUT:
					messageConfig = MessageComposerUtil.getCodeshareMessageConfig(bookingRequestDTO.getResponseMessageIdentifier());	
					break;
			}
		}


		try {
			String composedString = composeRawMessage(bookingRequestDTO, serviceBearerAirlineCode, messageConfig,
					includeAddressInMsgBody, includeOSI);
			msgComposerResponseDTO.setComposedMessage(composedString);
			msgComposerResponseDTO.setSuccess(true);
			return msgComposerResponseDTO;

		} catch (ModuleException me) {
			msgComposerResponseDTO.setSuccess(false);
			msgComposerResponseDTO.setReasonCode(me.getExceptionCode());
			msgComposerResponseDTO.setStatus(sStatus);
			return msgComposerResponseDTO;
		}

	}

	/**
	 * Method compose actual RawDTO
	 * 
	 * @param bookingRequestDTO
	 * @return composed string message.
	 * @throws ModuleException
	 *             error while composing .
	 */
	private String composeRawMessage(BookingRequestDTO bookingRequestDTO, String serviceBearerAirlineCode, TypeBMessageStructure.MessageConfig messageConfig,
	                                 boolean includeAddressInMsgBody, boolean includeOSI) throws ModuleException {

		String outMessage = null;
		String respAddress;

		try {

			HashMap <String,Object> context = new HashMap<String, Object>();
			Collection bookingSegmentCollection = bookingRequestDTO.getBookingSegmentDTOs();
			String messageIdentifier = bookingRequestDTO.getMessageIdentifier();
			String responseMessageIdentifier = bookingRequestDTO.getResponseMessageIdentifier();
			List changedNameDTOs = bookingRequestDTO.getChangedNameDTOs();
			List newNameDTOs = bookingRequestDTO.getNewNameDTOs();
			List unchangedNameDTOs = bookingRequestDTO.getUnChangedNameDTOs();
			RecordLocatorDTO originatorRecordLocator = bookingRequestDTO.getOriginatorRecordLocator();
			RecordLocatorDTO responderRecordLocator = bookingRequestDTO.getResponderRecordLocator();
			String communicationReference = bookingRequestDTO.getCommunicationReference();
			String senderBookingOffice = bookingRequestDTO.getOriginatorRecordLocator().getBookingOffice();

			ssrDetailDTOs = new ArrayList<SSRDetailDTO>();
			ssrDocsDTOs = new ArrayList<SSRDocsDTO>();
			ssrDocoDTOs = new ArrayList<SSRDocoDTO>();
			passportDTOs = new ArrayList<SSRPassportDTO>();
			emergencyContactDetailDTOs = new ArrayList<SSREmergencyContactDetailDTO>();
			foidDTOs = new ArrayList<SSRFoidDTO>();
			infantDTOs = new ArrayList<SSRInfantDTO>();
			childDTOs = new ArrayList<SSRChildDTO>();
			ssrDTOs = new ArrayList<SSRDTO>();			
			osiDTOs = bookingRequestDTO.getOsiDTOs();

			fillSpecialServiceDTOs(bookingRequestDTO.getSsrDTOs());

			String templateName = "email/" + "COMMON-RESPONSE" + MessageComposerConstants.TEMPLATE_SUFFIX;
			boolean incompleteResponseDTO = false;

			if (serviceBearerAirlineCode != null) {
				carrierCode = serviceBearerAirlineCode;
				String bookingOffice = MessageComposerApiUtils.generateBookingOffice(serviceBearerAirlineCode);
				context.put("bkOffice", bookingOffice);
			} else {
				incompleteResponseDTO = true;
			}

			if (carrierCode != null && !carrierCode.equals("")) {
				context.put("carrierCode", carrierCode);
			}

			if (messageIdentifier != null) {

				if (responseMessageIdentifier != null && responseMessageIdentifier != "") {
					context.put("mid", true);
					context.put("messageId", responseMessageIdentifier);
				} else {
					context.put("mid", false);
				}

			} else {
				incompleteResponseDTO = true;
			}

			if (changedNameDTOs != null && !changedNameDTOs.isEmpty()) {
				// names present
				List oldNameList = ((ChangedNamesDTO) changedNameDTOs.get(0)).getOldNameDTOs();
				// String oldNames = MessageComposerUtil.buildNameElement(oldNameList);
				List oldList = MessageComposerUtil.buildPaxNameElement(oldNameList);

				List newNameList = ((ChangedNamesDTO) changedNameDTOs.get(0)).getNewNameDTOs();
				List newList = MessageComposerUtil.buildPaxNameElement(newNameList);

				if (oldList != null && !oldList.isEmpty() && newList != null && !newList.isEmpty()) {

					context.put("cNames", true);
					context.put("changedOLDNames", oldList.iterator());
					context.put("changedNewNames", newList.iterator());
				} else {
					context.put("cNames", false);
				}

			}
			if (newNameDTOs != null && !newNameDTOs.isEmpty()) {
				List nList = MessageComposerUtil.buildPaxNameElement(newNameDTOs);
				if (nList != null && !nList.isEmpty()) {

					context.put("nNames", true);
					context.put("newNames", nList.iterator());
				} else {
					context.put("nNames", false);
				}
			}

			if (includeAddressInMsgBody) {
				context.put("res", bookingRequestDTO.getOriginatorAddress());
				context.put("sen", bookingRequestDTO.getResponderAddress());
				
				SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
		        Date dt = new Date();
		        String timestamp = sdf.format(dt).toUpperCase();
		        context.put("timestamp", timestamp);
			}

			if (originatorRecordLocator != null) {
				context.put("originatorRecordLocator", originatorRecordLocator);
			} 
			if (responderRecordLocator != null) {
				context.put("responderRecordLocator", responderRecordLocator);
			}
			if (communicationReference != null) {
				context.put("communicationReference", communicationReference);
			}
			if (bookingSegmentCollection != null) {
				context.put("bookingSegmentCollection", bookingSegmentCollection.iterator());
			} else {
				incompleteResponseDTO = true;
				sStatus = "Booking segment missing in the booking response";
			}
			if (osiDTOs != null && includeOSI) {
				List osiElements = MessageComposerUtil.composeOSIElements(osiDTOs, unchangedNameDTOs);
				if (osiElements != null && !osiElements.isEmpty()) {
					context.put("otherServiceInfoDTOs", osiElements.iterator());
				}
			}			 
			if (ssrDetailDTOs != null && !ssrDetailDTOs.isEmpty()) {
				List ssrElements = MessageComposerUtil.composeSSRElements(ssrDetailDTOs);
				if (ssrElements != null && !ssrElements.isEmpty()) {
					context.put("ssrDetailElements", ssrElements.iterator());
				}
			}
			if (ssrDocsDTOs != null && !ssrDocsDTOs.isEmpty()) {
				List<String> ssrElements = MessageComposerUtil.composeSSRDocsElements(ssrDocsDTOs);
				if (ssrElements != null && !ssrElements.isEmpty()) {
					context.put("ssrDocsElements", ssrElements.iterator());
				}
			}

			if (passportDTOs != null && !passportDTOs.isEmpty()) {

				List psptElements = MessageComposerUtil.composePassportElements(passportDTOs);
				if (psptElements != null && !psptElements.isEmpty()) {
					context.put("passportElements", psptElements.iterator());
				}
			}
			if (emergencyContactDetailDTOs != null && !emergencyContactDetailDTOs.isEmpty()) {

				List emElements = MessageComposerUtil.composeEmergencyDetail(emergencyContactDetailDTOs);
				if (emElements != null && !emElements.isEmpty()) {
					context.put("emElements", emElements.iterator());
				}
			}
			if (foidDTOs != null && !foidDTOs.isEmpty()) {

				List foidElements = MessageComposerUtil.composeFoidElements(foidDTOs);

				if (foidElements != null && !foidElements.isEmpty()) {
					context.put("foidElements", foidElements.iterator());
				}
			}
			if (infantDTOs != null && !infantDTOs.isEmpty()) {

				List infantElements = MessageComposerUtil.composeInfantElements(infantDTOs);
				if (infantElements != null && !infantElements.isEmpty()) {
					context.put("infantElements", infantElements.iterator());
				}

			}
			if (childDTOs != null && !childDTOs.isEmpty()) {

				List childElements = MessageComposerUtil.composeChildElements(childDTOs);
				if (childDTOs != null && !childDTOs.isEmpty()) {
					context.put("childElements", childElements.iterator());
				}
			}
			if (ssrDTOs != null && !ssrDTOs.isEmpty()) {

				List ssrS = MessageComposerUtil.composeSSRs(ssrDTOs, senderBookingOffice, bookingRequestDTO
						.getOriginatorRecordLocator().getCariierCRSCode());
				if (ssrDTOs != null && !ssrDTOs.isEmpty()) {
					context.put("ssrElements", ssrS.iterator());
				}

			}
			CalendarUtil calanderUtil = new CalendarUtil();
			if (calanderUtil != null) {
				context.put("calUtil", calanderUtil);
			}

			if (incompleteResponseDTO) {

				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.bookingresponse.error\n");
				}
				System.out.println(sStatus);
				System.out.println("Incomplete response");
				throw new ModuleException("gdsmessagecomposer.bookingresponse.error");
			}

			if (messageConfig != null) {
				context.put("messageConfig", messageConfig);
			}

			Template template = null;
			StringWriter writer = new StringWriter();

			new TemplateEngine().writeTemplate(context, templateName, writer);			
			outMessage = writer.toString().replace("\r", "").replace("\n", "").replace("&nbps", "\n");
			writer.close();

		} catch (Exception e) {

			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.composer.error\n");
			}
			System.out.println("gdsmessagecomposer.composer.error\n" + e.getMessage());
			sStatus = e.getMessage();
			throw new ModuleException("gdsmessagecomposer.composer.error");

		}

		if (logger.isDebugEnabled()) {
			logger.debug(":::::::::COMPOSED MESSAGE:::::::::::\n");
			logger.debug(outMessage);
			logger.debug("\n:::::::::COMPOSED MESSAGE:::::::::::\n");
		}

		System.out.println(":::::::::COMPOSED MESSAGE:::::::::::\n");
		System.out.println(outMessage);
		System.out.println("\n:::::::::COMPOSED MESSAGE:::::::::::\n");

		return outMessage.toUpperCase();
	}

	/**
	 * Method fill each Special Service Dto's from SSR dto.
	 * 
	 * @param ssrDTOs2
	 */
	private void fillSpecialServiceDTOs(List<SSRDTO> ssrDTOList) {

		for (SSRDTO obj : ssrDTOList) {

			if (obj.isIncludeInResponse()) {

				if (obj instanceof SSRDetailDTO) {
					ssrDetailDTOs.add((SSRDetailDTO) obj);
				} else if (obj instanceof SSRDocsDTO) {
					ssrDocsDTOs.add((SSRDocsDTO) obj);
				} else if (obj instanceof SSRDocoDTO) {
					ssrDocoDTOs.add((SSRDocoDTO) obj);
				} else if (obj instanceof SSRPassportDTO) {
					passportDTOs.add((SSRPassportDTO) obj);
				} else if (obj instanceof SSREmergencyContactDetailDTO) {
					emergencyContactDetailDTOs.add((SSREmergencyContactDetailDTO) obj);
				} else if (obj instanceof SSRFoidDTO) {
					foidDTOs.add((SSRFoidDTO) obj);
				} else if (obj instanceof SSRInfantDTO) {
					infantDTOs.add((SSRInfantDTO) obj);
				} else if (obj instanceof SSRChildDTO) {
					childDTOs.add((SSRChildDTO) obj);
				} else {
					ssrDTOs.add((SSRDTO) obj);
				}
			}
		}
	}

	/**
	 * Method invokes NAC message composing.
	 * @param modeOfCommunication 
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	public MessageComposerResponseDTO composeNACMessage(String inMessage, String carrierCode, String nacDetail, String modeOfCommunication) {

		MessageComposerResponseDTO msgComposerResponseDTO = new MessageComposerResponseDTO();

		try {
			String composedString = getNACMessagee(inMessage, carrierCode, nacDetail, modeOfCommunication);
			msgComposerResponseDTO.setComposedMessage(composedString);
			msgComposerResponseDTO.setSuccess(true);
			return msgComposerResponseDTO;

		} catch (ModuleException me) {
			msgComposerResponseDTO.setSuccess(false);
			msgComposerResponseDTO.setReasonCode(me.getExceptionCode());
			msgComposerResponseDTO.setStatus(sStatus);
			return msgComposerResponseDTO;
		}
	}

	/**
	 * Method converts bookingRequest Dto into NAC message.l
	 * @param modeOfCommunication 
	 * 
	 * @param bookingRequestDTO
	 * @return
	 * @throws ModuleException
	 */
	private String getNACMessagee(String inMessageString, String carrierCode, String nacDeailString, String modeOfCommunication) throws ModuleException {
		String outMessage = null;
		try {

			boolean incompleteResponse = false;

			if ((inMessageString == null || !inMessageString.isEmpty()) || (nacDeailString == null || nacDeailString.isEmpty())) {

				inMessageString = inMessageString.trim().toUpperCase();
				nacDeailString = nacDeailString.trim().toUpperCase();

				String ssrNACDeatilElement = MessageComposerConstants.SSR + MessageComposerConstants.SP + TypeBMessageConstants.SSRCodes.OTHS
						+ MessageComposerConstants.SP + nacDeailString;
				if (!(ssrNACDeatilElement.length() > MessageComposerConstants.LINE_CHARACTER_LENGTH)) {
					inMessageString += "\n" + ssrNACDeatilElement;
				} else {
					List ssrElements = MessageComposerUtil.getSSRElemenets(TypeBMessageConstants.SSRCodes.OTHS, carrierCode,
							nacDeailString.trim().toUpperCase());
					for (Iterator iter = ssrElements.iterator(); iter.hasNext();) {
						String element = (String) iter.next();
						inMessageString += "\n" + element;
					}
				}

				String[] elementStrings = inMessageString.split("\n");
				if(!MessageComposerConstants.SITA_COMMUNICATION.equalsIgnoreCase(modeOfCommunication)){
					elementStrings[0] = elementStrings[1] = "";
				}
				outMessage = StringUtils.join(elementStrings, "\n");
				outMessage = "NAC\n" + outMessage.trim();

			} else {
				incompleteResponse = true;
			}
			if (incompleteResponse) {

				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.bookingresponse.error\n");
				}
				System.out.println("NAC message without supplementary reason");
				throw new ModuleException("gdsmessagecomposer.nac.response.error");
			}

		} catch (Exception e) {

			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.composer.error\n");
			}
			System.out.println("gdsmessagecomposer.composer.error\n" + e.getMessage());
			sStatus = e.getMessage();
			throw new ModuleException("gdsmessagecomposer.composer.error");

		}

		return outMessage;
	}

	/**
	 * @return the ssrDocsDTOs
	 */
	public List<SSRDocsDTO> getSsrDocsDTOs() {
		return ssrDocsDTOs;
	}

	/**
	 * @param ssrDocsDTOs the ssrDocsDTOs to set
	 */
	public void setSsrDocsDTOs(List<SSRDocsDTO> ssrDocsDTOs) {
		this.ssrDocsDTOs = ssrDocsDTOs;
	}
	
	public MessageComposerResponseDTO composeNACMessage(String messageType, String timeMode, String sequenceReference,
			String creatorReference, String inMessage, String responseMsg) {

		MessageComposerResponseDTO msgComposerResponseDTO = new MessageComposerResponseDTO();

		try {
			String composedString = getNACMessage(messageType, timeMode, sequenceReference, creatorReference, inMessage,
					responseMsg);
			msgComposerResponseDTO.setComposedMessage(composedString);
			msgComposerResponseDTO.setSuccess(true);
			return msgComposerResponseDTO;

		} catch (ModuleException me) {
			msgComposerResponseDTO.setSuccess(false);
			msgComposerResponseDTO.setReasonCode(me.getExceptionCode());
			msgComposerResponseDTO.setStatus(sStatus);
			return msgComposerResponseDTO;
		}
	}
	
	private String getNACMessage(String messageType, String timeMode, String sequenceReference, String creatorReference,
			String inMessageString, String nacDeailString) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		try {

			boolean incompleteResponse = false;

			if (!StringUtil.isNullOrEmpty(inMessageString) && !StringUtil.isNullOrEmpty(nacDeailString)) {

				inMessageString = inMessageString.trim().toUpperCase();
				nacDeailString = nacDeailString.trim().toUpperCase();

				sb.append(messageType);
				sb.append("\n");
				if (!StringUtil.isNullOrEmpty(timeMode)) {
					sb.append(timeMode);
					sb.append("\n");
				}

				if (!StringUtil.isNullOrEmpty(sequenceReference)) {
					sb.append(sequenceReference);
					if (!StringUtil.isNullOrEmpty(creatorReference)) {
						sb.append("/" + creatorReference);
					}
					sb.append("\n");
				}

				sb.append("NAC");
				sb.append("\n");
				sb.append("000 "+ nacDeailString);
				sb.append("\n");
				sb.append(inMessageString);

			} else {
				incompleteResponse = true;
			}
			if (incompleteResponse) {
				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.bookingresponse.error\n");
				}
				throw new ModuleException("gdsmessagecomposer.nac.response.error");
			}

		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.composer.error\n");
			}
			sStatus = e.getMessage();
			throw new ModuleException("gdsmessagecomposer.composer.error");

		}

		return sb.toString();
	}

	public MessageComposerResponseDTO composeACKMessage(String msgType, String timeMode, String msgSequenceReference,
			String creatorReference) {

		MessageComposerResponseDTO msgComposerResponseDTO = new MessageComposerResponseDTO();

		try {
			String composedString = getACKMessage(msgType, timeMode, msgSequenceReference, creatorReference);
			msgComposerResponseDTO.setComposedMessage(composedString);
			msgComposerResponseDTO.setSuccess(true);
			return msgComposerResponseDTO;

		} catch (ModuleException me) {
			msgComposerResponseDTO.setSuccess(false);
			msgComposerResponseDTO.setReasonCode(me.getExceptionCode());
			msgComposerResponseDTO.setStatus(sStatus);
			return msgComposerResponseDTO;
		}
	}

	private String getACKMessage(String msgType, String timeMode, String msgSequenceReference, String creatorReference)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();
		try {

			boolean incompleteResponse = false;

			if (!StringUtil.isNullOrEmpty(msgType) && !StringUtil.isNullOrEmpty(msgSequenceReference)) {

				sb.append(msgType);
				sb.append("\n");
				if (!StringUtil.isNullOrEmpty(timeMode)) {
					sb.append(timeMode);
					sb.append("\n");
				}

				sb.append(msgSequenceReference);
				if (!StringUtil.isNullOrEmpty(creatorReference)) {
					sb.append("/" + creatorReference);
				}
				sb.append("\n");
				sb.append("ACK");
				sb.append("\n");

			} else {
				incompleteResponse = true;
			}
			if (incompleteResponse) {
				if (logger.isDebugEnabled()) {
					logger.debug("gdsmessagecomposer.bookingresponse.error\n");
				}
				throw new ModuleException("gdsmessagecomposer.nac.response.error");
			}

		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("gdsmessagecomposer.composer.error\n");
			}
			sStatus = e.getMessage();
			throw new ModuleException("gdsmessagecomposer.composer.error");

		}

		return sb.toString();

	}

}
