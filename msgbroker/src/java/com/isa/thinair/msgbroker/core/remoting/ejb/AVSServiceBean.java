package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.msgbroker.api.dto.AVSMessageDTO;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.core.bl.AVSMessageComposer;
import com.isa.thinair.msgbroker.core.bl.CommonMessageProcessingBL;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.service.bd.AVSServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.AVSServiceBeanRemote;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;

@Stateless
@RemoteBinding(jndiBinding = "AVSService.remote")
@LocalBinding(jndiBinding = "AVSService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AVSServiceBean extends PlatformBaseSessionBean implements AVSServiceBeanLocal, AVSServiceBeanRemote {

	private final Log log = LogFactory.getLog(AVSServiceBean.class);
	private MessageDAO messageDAO;
	private AdminDAO adminDAO;
	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendAVSMessages(String gdsCode, Integer gdsId, String airLineCode, AVSMessageDTO avsMessageDTO)
			throws ModuleException {

		AVSMessageComposer avsComposer = new AVSMessageComposer();
		ServiceClient gds;
		ServiceBearer airline;
		String ttyAddress;
		Gds activeGds = null;

		try {
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airLineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.AVS.toString());
			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			// Call composer
			gds = getAdminDAO().getServiceClient(gdsCode);
			activeGds = LookupUtil.getGdsServiceBD().getGds(gdsId);

			airline = getAdminDAO().getServiceBearer(airLineCode, gdsCode);
			ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode,
					MessageComposerConstants.Message_Identifier.AVS.toString());
			if (ttyAddress != null) {
				avsMessageDTO.setRecipientAddress(ttyAddress);
				avsMessageDTO.setSenderAddress(airline.getSitaAddress());
				outMessage.setOutMessageText(avsComposer.composeAVSBookingMessage(avsMessageDTO, gdsId,
						gds.getAddAddressToMessageBody()));
				outMessage.setReceivedDate(new Date());
				outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
						gds.getEmailDomain()));
				outMessage.setRecipientTTYAddress(ttyAddress);
				outMessage.setSentEmailAddress(airline.getOutgoingEmailFromAddress());
				outMessage.setSentTTYAddress(airline.getSitaAddress());
				outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				outMessage.setNonGDSMessageFlag('N');

				getMessageDAO().saveOutMessage(outMessage);
			} else {
				log.error("AVS will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
						+ gdsCode + ".");
			}

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}

	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * return the message DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

}
