package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.Action;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageSearchDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.msgbroker.core.bl.GDSMessageHandler;
import com.isa.thinair.msgbroker.core.bl.PostMessageMDBInvoker;
import com.isa.thinair.msgbroker.core.bl.SequentialMessageProcessor;
import com.isa.thinair.msgbroker.core.service.bd.MessagingBrokerServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.MessagingBrokerServiceBeanRemote;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

@Stateless
@RemoteBinding(jndiBinding = "MessageBrokerService.remote")
@LocalBinding(jndiBinding = "MessageBrokerService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class MessageBrokerServiceBean extends PlatformBaseSessionBean implements MessagingBrokerServiceBeanRemote,
		MessagingBrokerServiceBeanLocal {

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendMessages() throws ModuleException {

		List<String> messageTypes = new ArrayList<String> ();
		messageTypes.add(MessageComposerConstants.Message_Identifier.BKG.toString());	
		messageTypes.add(MessageComposerConstants.Message_Identifier.AMD.toString());	
		messageTypes.add(MessageComposerConstants.Message_Identifier.CAN.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.DVD.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.NCO.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.PDM.toString());
		messageTypes.add(Constants.ServiceTypes.ReservationService);

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, GDSMessageHandler.Category.RESERVATION, true);
	}
	
	public void sendPNLADLViaSitaTex() throws ModuleException{
		List<String> messageTypes = new ArrayList<String> ();
		messageTypes.add(MessageComposerConstants.Message_Identifier.PNL.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.ADL.toString());
		
		
		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, null, false);
	
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendAVSMessages() throws ModuleException {

		List<String> messageTypes = new ArrayList<String> ();
		messageTypes.add(MessageComposerConstants.Message_Identifier.AVS.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, GDSMessageHandler.Category.INVENTORY, true);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void receiveMessage(boolean isUsedGDSConfiguraion) throws ModuleException {
		GDSMessageHandler handler = new GDSMessageHandler();
		handler.execute(GDSMessageHandler.Direction.IN, GDSMessageHandler.Category.RESERVATION, isUsedGDSConfiguraion);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InMessage saveInMessage(InMessage inMessage) throws ModuleException {
		try {
			return LookupUtil.lookupMessageDAO().saveAndReLoad(inMessage);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateInMessage(InMessage inMessage) throws ModuleException {
		try {
			LookupUtil.lookupMessageDAO().saveInMessage(inMessage);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean aquireInMessageLock(Integer inMessageID) throws ModuleException {
		try {
			return LookupUtil.lookupMessageDAO().aquireInMessageLock(inMessageID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveOutMessage(OutMessage outMessage) throws ModuleException {
		try {
			LookupUtil.lookupMessageDAO().saveOutMessage(outMessage);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveInMessageProcessingStatus(InMessageProcessingStatus inMessageProcessingStatus) {
		LookupUtil.lookupMessageDAO().saveInMessageProcessingStatus(inMessageProcessingStatus);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendETLMessages() throws ModuleException {
		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.ETL.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, null, true);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendPFSMessages() throws ModuleException {
		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.PFS.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, null, true);
	}
	

	@Override
	public Collection<TypeBMessageDTO> searchTypeBMessageByPnr(TypeBMessageSearchDTO typeBMessageSearchDTO, List<String> messageTypes) throws ModuleException {
		List<TypeBMessageDTO> typeBMessages = LookupUtil.lookupMessageDAO().searchTypeBMessageByPnr(typeBMessageSearchDTO, messageTypes);
		Collections.sort(typeBMessages);
		return typeBMessages;
	}
	
	@Override
	public void saveTypeBRecipientExtSystem(TypeBRecipientExtSystem typeBRecipientExtSystem) throws ModuleException{
		try {
			LookupUtil.lookupMessageDAO().saveTypeBRecipientExtSystem(typeBRecipientExtSystem);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceClient getServiceClient(String gdsCode) {
		return LookupUtil.lookupAdminDAO().getServiceClient(gdsCode);
	}

	public SSMASMResponse updateOutMessagesResponseStatus(SSMASMMessegeDTO ssmAsmMessageDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		try {
			List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList = (List<SSMASMSUBMessegeDTO>) ssmAsmMessageDTO.getSsmASMSUBMessegeList();
			if (ssmASMSubMsgsList != null && ssmASMSubMsgsList.size() > 0) {
				for (SSMASMSUBMessegeDTO ssmASMSubMsgsDTO : ssmASMSubMsgsList) {
					if (Action.ACK.equals(ssmASMSubMsgsDTO.getAction())) {
						// ToDo: Available for further development
					} else {
						LookupUtil.lookupMessageDAO().updateOutMessagesResponseStatus(
								ssmAsmMessageDTO.getMessageSequenceReference(), ssmASMSubMsgsDTO.getAction());
					}
				}
			}
			ssimResponse.setACKNACMessage(true);
			ssimResponse.setSuccess(true);
			ssimResponse.setSsiMessegeDTO(ssmAsmMessageDTO);
			log.info("OUT SSM MESSAGE'S RESPONSE_STATUS UPDATED");
		} catch (CommonsDataAccessException e) {
			log.error(" Exception @ ProcessSSMAction :: processRequestForUpdateResponseStatus", e);
			ssimResponse.setSuccess(false);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
		}
		return ssimResponse;
	}

	public void sendACKNACMessages() throws ModuleException {
		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.ACK.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.NAC.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, GDSMessageHandler.Category.RESPONSE, true);

	}

	@Override
	public void clearInMsgProcessingQueue() throws ModuleException {
		try {
			LookupUtil.lookupServiceClientReqDAO().clearInMsgProcessingQueue();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), MsgbrokerConstants.MODULE_NAME);
		}

	}


	@Override
	public void postMessageMdbInvoker(Integer inMsgId) {
		PostMessageMDBInvoker mdbInvoker = new PostMessageMDBInvoker();
		mdbInvoker.invokeInboundMessageProcessorMDB(inMsgId);
	}
	
	@Override
	public Collection<Integer> allLockedMsgQueues(){
		return  LookupUtil.lookupServiceClientReqDAO().retrieveLockedQueueIds();
	}

	@Override
	public Integer getNextMessageIdFromWaitingQueue(Integer inMsgProcessingQueueId) {
		return LookupUtil.lookupMessageDAO().getNextInMsgIdInQueue(inMsgProcessingQueueId);
	}

	@Override
	public void updateNextInMsgId(Integer inMsgProcessingQueueId, Integer nextMsgIdToProcess) {
		LookupUtil.lookupMessageDAO().updateNextMsgIdToProcess(inMsgProcessingQueueId, nextMsgIdToProcess);
		
	}

	@Override
	public void removeMsgFromQueue(Integer inMessageId, Integer inMsgProcessingQueueId) {
		LookupUtil.lookupMessageDAO().removeMessageFromWaitingQueue(inMessageId, inMsgProcessingQueueId);	
	}

	@Override
	public void releaseLockedScheduleFlight(String msgStatus, Integer inMsgProcessingQueueId) {
		LookupUtil.lookupMessageDAO().unlockMsgProcessingQueue(inMsgProcessingQueueId, msgStatus);
		
	}

	@Override
	public void sendPALCALViaSitaTex() throws ModuleException {

		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.PAL.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.CAL.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, null, false);

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void processMessagesSequentially() throws ModuleException{
		SequentialMessageProcessor seqMessageProcessor = new SequentialMessageProcessor();
		seqMessageProcessor.execute();
		
	}
}
