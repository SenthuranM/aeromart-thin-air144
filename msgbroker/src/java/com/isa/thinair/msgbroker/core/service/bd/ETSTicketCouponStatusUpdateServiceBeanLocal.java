package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.msgbroker.api.service.ETSTicketCouponStatusUpdateBD;

/***
 * 
 * @author rajitha
 *
 */

@Local
public interface ETSTicketCouponStatusUpdateServiceBeanLocal extends ETSTicketCouponStatusUpdateBD {

}
