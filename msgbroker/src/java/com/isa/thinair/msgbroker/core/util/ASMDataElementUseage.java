package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.ASMDataElement;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SMDataElementRequirmentCategory;

public class ASMDataElementUseage {
	private ASMDataElement asmDataElement;
	private SMDataElementRequirmentCategory asmDataElementRequirmentCategory;
	private ASMDataElement nextASMDataElement;

	public ASMDataElementUseage(ASMDataElement element, SMDataElementRequirmentCategory category, ASMDataElement nextElement) {
		nextASMDataElement = nextElement;
		asmDataElement = element;
		asmDataElementRequirmentCategory = category;
	}

	public ASMDataElement getSsmDataElement() {
		return asmDataElement;
	}

	public void setSsmDataElement(ASMDataElement asmDataElement) {
		this.asmDataElement = asmDataElement;
	}

	public SMDataElementRequirmentCategory getSsmDataElementRequirmentCategory() {
		return asmDataElementRequirmentCategory;
	}

	public void setSsmDataElementRequirmentCategory(SMDataElementRequirmentCategory asmDataElementRequirmentCategory) {
		this.asmDataElementRequirmentCategory = asmDataElementRequirmentCategory;
	}

	public ASMDataElement getNextASMDataElement() {
		return nextASMDataElement;
	}

	public void setNextASMDataElement(ASMDataElement nextASMDataElement) {
		this.nextASMDataElement = nextASMDataElement;
	}
}
