package com.isa.thinair.msgbroker.core.adaptor;

import com.isa.thinair.commons.api.dto.ets.IssueExchangeTKTREQDTO;
import com.accelaero.ets.SimplifiedTicketServiceWrapper;

public class IssueExchangeTKTREQAdaptor extends AbstractGrpcRequestCreator<SimplifiedTicketServiceWrapper.IssueExchangeTKTREQ, IssueExchangeTKTREQDTO>{

	public IssueExchangeTKTREQAdaptor() {
		super(SimplifiedTicketServiceWrapper.IssueExchangeTKTREQ.class, IssueExchangeTKTREQDTO.class);
	}
}
