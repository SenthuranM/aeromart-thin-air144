package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.dto.InNacDTO;
import com.isa.thinair.msgbroker.api.dto.InNacDTO.MessageIdentifier;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;

public class NACMessageParser {
   
    private static String sStatus;
       
    public static MessageParserResponseDTO parseNacMessage(String inMessage)  {
        
        MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
        try {
            InNacDTO nacDTO = getNACMessage(inMessage);
            oMessageParserResponseDTO.setRequestDTO(nacDTO);
            oMessageParserResponseDTO.setSuccess(true);
            return oMessageParserResponseDTO;
            
        } catch (ModuleException me) {
            oMessageParserResponseDTO.setRequestDTO(null);
            oMessageParserResponseDTO.setSuccess(false);
            oMessageParserResponseDTO.setReasonCode(me.getExceptionCode());
            oMessageParserResponseDTO.setStatus(sStatus);
            return oMessageParserResponseDTO;
        }        
    }
    
	public static MessageParserResponseDTO parseNacResMessage(String inMessage) {

		MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
		InNacDTO nacDTO = new InNacDTO();
		oMessageParserResponseDTO.setRequestDTO(nacDTO);
		oMessageParserResponseDTO.setSuccess(true);
		return oMessageParserResponseDTO;
	}
    
    /**
     * @param inMessage
     * @return
     * @throws ModuleException
     */
    private static InNacDTO getNACMessage(String inMessage) throws ModuleException {
      
        /* Exit if message is empty */
        if (inMessage == null || inMessage.equals("")) {
            // TODO remove sysout
            System.out.println("Empty IN-NAC Message");
            throw new ModuleException("message.text.empty");

        }
        inMessage = inMessage.replaceAll("\r", "");
        
        InNacDTO nacDTO = new InNacDTO();
        
        String[] messageTokens = inMessage.split("\n");
        
        boolean completeDTO = true;

        String errorMsg = "";

        for (int i = 0; i < messageTokens.length && completeDTO; i++) {
            
            String element = messageTokens[i];

            if(element!= null) {
                element = element.trim();
                switch (i) {
                    case 0 :
                        if(element.matches("([A-Z]{2}( ))?([A-Z1-9]{7,8})+(( )([A-Z1-9]{7,8}))*")) {
                           if (element.indexOf(" ") == 2) {
                               nacDTO.setResponderAddress(element.substring(3));
                           }else {
                               nacDTO.setResponderAddress(element); 
                           }
                        } else {
                            completeDTO = false;
                        }
                        break;
                    case 1 :
                        if (element.matches("\\.([A-Z1-9]{7,8})+ ?([0-9A-Z]{6,7})?")) {
                            nacDTO.setOriginatorAddress(element);
                        } else {
                            completeDTO = false;   
                        }
                        break;
                    case 2 :
                        if(!element.startsWith("~NAC SSM/ASM REJECT")) {
                            completeDTO = false;    
                        }
                        break;
                    case 3 : 
                        errorMsg += element; 
                        break;
                    case 4 :
                        if (element.equals(MessageIdentifier.ASM.name()) || element.equals(MessageIdentifier.SSM.name()) 
                                || element.equals(MessageIdentifier.AVS.name())) {
                            nacDTO.setMessageIdentifier(element) ;
                            if(messageTokens[5].equals("LT")) {
                                
                                nacDTO.setReferredMessageSequenceReference(messageTokens[6]);
                            } else {
                                nacDTO.setReferredMessageSequenceReference(messageTokens[5]);
                            }
                        } 
                        //else {//errorMsg += element;}
                        
                        break;
                    case 5 : 
                        if (element.equals(MessageIdentifier.ASM.name()) || element.equals(MessageIdentifier.SSM.name()) 
                                || element.equals(MessageIdentifier.AVS.name())) {
                            nacDTO.setMessageIdentifier(element);
                            
                            if(messageTokens[6].equals("LT")) {
                                nacDTO.setReferredMessageSequenceReference(messageTokens[7]);
                            } else {
                                nacDTO.setReferredMessageSequenceReference(messageTokens[6]);
                            }
                            
                        }
                         
                        break;
                  
                }
            }
            
        } 
        if (completeDTO && nacDTO.getMessageIdentifier()!= null 
                && nacDTO.getReferredMessageSequenceReference()!= null)  {
            nacDTO.setErrorMessage(errorMsg);
            return nacDTO;
            
        } else {
            
            return null;
        }
    }
    public static void main(String[] args) {
  /*      QP SOFMSH1                        
        .HDQBB1S 010350Z                  
        ~NAC SSM/ASM REJECT               
        INVALID SUB-MSG ACTION CODE       
        ERROR PROGRAM: KSB0   NUMBER: 007 
        SSM                               
        01OCT21003E001                    
        LT                                
        NEW                               
        G9505                             
        G9505                             
        01JAN09 31JAN09 1234567           
        G 320 N                           
        CMB0800 SHJ1230                   
        END REJECT */     
        
        InNacDTO nacDTO = null;
        
        String msg1= "";
        msg1= "QP SOFMSH1\n";
        msg1 += ".HDQBB1S 010420Z\n";
        msg1 += "~NAC SSM/ASM REJECT\n";
        msg1 += "INVALID SUB-MSG ACTION CODE\n";
        msg1 += "ERROR PROGRAM: KSB0   NUMBER: 007\n";
        msg1 += "SSM\n";
        msg1 += "LT\n";
        msg1 += "01OCT22002E001\n";
        msg1 += "NEW\n";
        msg1 += "G9625\n";
        msg1 += "G9625\n";
        msg1 += "05NOV 06NOV 1234567\n";
        msg1 += "G 320 S\n";
        msg1 += "SHJ1945 CMB2255\n";
        msg1 += "END REJECT\n";
        
        MessageParserResponseDTO response = NACMessageParser.parseNacMessage(msg1);
        
        if (response.isSuccess()) {
            nacDTO = (InNacDTO) response.getRequestDTO();
        }
        
        if (nacDTO !=null) {
            System.out.println("########NAC MESSAGE###########");
            System.out.println(msg1);
            System.out.println("########NAC MESSAGE###########");
            System.out.println("OriginatorAddress : " + nacDTO.getOriginatorAddress());
            System.out.println("ResponderAddress : "+nacDTO.getResponderAddress());
            System.out.println("ReferredMessageIdentifier : "+ nacDTO.getMessageIdentifier());
            System.out.println("ReferredMessageSequenceReference : "+nacDTO.getReferredMessageSequenceReference());
            System.out.println("ErrorMessage : " +nacDTO.getErrorMessage());
        } else {
            System.out.println("NACDTO EMPTY");
        }
    }

}
