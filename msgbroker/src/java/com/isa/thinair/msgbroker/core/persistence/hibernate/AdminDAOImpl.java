package com.isa.thinair.msgbroker.core.persistence.hibernate;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.msgbroker.api.model.OutMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.Util;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * 
 * @isa.module.dao-impl dao-name="adminDAO"
 */
public class AdminDAOImpl extends PlatformBaseHibernateDaoSupport implements AdminDAO {

	private Log log = LogFactory.getLog(AdminDAOImpl.class);

	/**
	 * Method to return the ServiceBearer detail for a specified airline code and gds.
	 * 
	 * @param airlineCode
	 * @param gdsCode
	 * @return
	 */
	
	public ServiceBearer getServiceBearer(String airlineCode, String gdsCode) {
		String hql = "SELECT airline " + " FROM  ServiceBearer airline " + " WHERE airline.airlineCode = '" + airlineCode
				+ "' and airline.gdsCode = '" + gdsCode + "'";
		List airlines = find(hql, ServiceBearer.class);
		if (airlines != null && !airlines.isEmpty()) {
			return (ServiceBearer) airlines.get(0);
		} else {
			return null;
		}
	}

	public ServiceBearer getServiceBearer(String carrierCode) {
		String sql = "SELECT sb.* FROM t_gds g, mb_t_service_bearer sb " +
				"WHERE g.gds_code = sb.gds_code AND g.carrier_code = :carrier_code ";
		Session session = getSessionFactory().getCurrentSession();
		Query query = session.createSQLQuery(sql)
				.addEntity(ServiceBearer.class)
				.setString("carrier_code", carrierCode);
		List serviceBearers = query.list();
		if (serviceBearers != null && !serviceBearers.isEmpty()) {
			return (ServiceBearer) serviceBearers.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Method to return all airlines
	 * 
	 * @return
	 */
	public List<ServiceBearer> getServiceBearerAirlines() {
		return find("SELECT airline FROM  ServiceBearer airline", ServiceBearer.class);
	}

	/**
	 * Method to return all airlines
	 * 
	 * @return
	 */
	public List getServiceBearerAirlinesByDesc() {
		return find("from ServiceBearer order by UPPER(airlineDescripton) asc", ServiceBearer.class);
	}

	/**
	 * Method to update or add airline detail
	 * 
	 * @param airline
	 */
	public void saveServiceBearerAirline(ServiceBearer airline) {
		hibernateSaveOrUpdate(airline);
	}

	/**
	 * Method to delete an airline
	 * 
	 * @param airlineCode
	 */
	public void removeServiceBearerAirline(ServiceBearer airline) {
		delete(airline);
	}

	// public List getGDSSitaAddresses() {
	// String hql = "SELECT oGDSSitaAddress " +
	// " FROM  GDSSitaAddress oGDSSitaAddress " +
	// " WHERE oGDSSitaAddress.recordStatus = 'Y'";
	//
	// return find(hql);
	// }

	// public List getGDSSitaAddressesWithGDS(){
	// String hql = "SELECT oGDSSitaAddress, g " +
	// " FROM  GDSSitaAddress oGDSSitaAddress, ServiceClient g " +
	// " WHERE oGDSSitaAddress.recordStatus = 'Y' " +
	// "	AND oGDSSitaAddress.GDSCode = g.GDSCode";
	// return find(hql);
	// }

	// ServiceClient

	/**
	 * Method to return the ServiceClient detail for a specified gds code.
	 * 
	 * @param gdsCode
	 * @return
	 */
	public ServiceClient getServiceClient(String gdsCode) {
		return (ServiceClient) get(ServiceClient.class, gdsCode);
	}

	/**
	 * Method to return all GDSs
	 * 
	 * @return
	 */
	public List getServiceClients() {
		return find("from ServiceClient order by serviceClientCode asc", ServiceClient.class);
	}

	/**
	 * Method to update or add ServiceClient detail
	 * 
	 * @param gds
	 */
	public void saveServiceClient(ServiceClient gds) {
		hibernateSaveOrUpdate(gds);
	}

	/**
	 * Method to delete an ServiceClient
	 * 
	 * @param gdsCode
	 */
	public void removeServiceClient(ServiceClient gds) {
		delete(gds);
	}

	public List getServiceClients(Collection gdsCodes) {
		String hql = "SELECT gds " + " FROM  ServiceClient gds " + " WHERE gds.serviceClientCode IN ( "
				+ Util.buildStringInClauseContent(gdsCodes) + ") ";
		return find(hql, ServiceClient.class);
	}

	public ServiceClient getServiceClientForTTYAddress(String ttyAddress) {
		String hql = "SELECT gds " + " FROM  ServiceClient gds " + " WHERE gds.TTYAddress = '" + ttyAddress + "' ";
		List gdses = find(hql, ServiceClient.class);
		if (gdses != null && !gdses.isEmpty()) {
			return (ServiceClient) gdses.get(0);
		} else {
			return null;
		}
	}

	public ServiceBearer getServiceBearerAirlineForTTYAddress(String ttyAddress) {
		String hql = "SELECT airline " + " FROM  ServiceBearer airline " + " WHERE airline.sitaAddress = '" + ttyAddress + "' ";
		List airlines = find(hql, ServiceBearer.class);
		if (airlines != null && !airlines.isEmpty()) {
			return (ServiceBearer) airlines.get(0);
		} else {
			return null;
		}
	}
	
	public ServiceBearer getServiceBearerAirlineForEmailAddress(String emailAddress) {
		String hql = "SELECT airline " + " FROM  ServiceBearer airline " + " WHERE airline.emailAddress = '" + emailAddress + "' ";
		List airlines = find(hql, ServiceBearer.class);
		if (airlines != null && !airlines.isEmpty()) {
			return (ServiceBearer) airlines.get(0);
		} else {
			return null;
		}
	}
	
	public String getMailAccesProtocol(String gdsCode) {
		String hql = "SELECT bearer.mailAccessProtocol " + " FROM  ServiceBearer bearer " + " WHERE bearer.gdsCode = '" + gdsCode + "' ";
		List accesProtocols = find(hql, String.class);
		if (accesProtocols != null && !accesProtocols.isEmpty()) {
			return (String)accesProtocols.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<String> getPendingExternalSystemMessagingModes() {
		String hql = "SELECT distinct recipient.messagingMode " + " FROM  TypeBRecipientExtSystem recipient ";
		return find(hql, String.class);
	}	
	
}