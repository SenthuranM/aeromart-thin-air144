package com.isa.thinair.msgbroker.core.bl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;
import com.isa.thinair.msgbroker.api.dto.ChangedNamesDTO;
import com.isa.thinair.msgbroker.api.dto.NameDTO;
import com.isa.thinair.msgbroker.api.dto.OSIAddressDTO;
import com.isa.thinair.msgbroker.api.dto.OSIDTO;
import com.isa.thinair.msgbroker.api.dto.OSIEmailDTO;
import com.isa.thinair.msgbroker.api.dto.OSIPhoneNoDTO;
import com.isa.thinair.msgbroker.api.dto.OtherServiceInfoDTO;
import com.isa.thinair.msgbroker.api.dto.RecordLocatorDTO;
import com.isa.thinair.msgbroker.api.dto.SSRChildDTO;
import com.isa.thinair.msgbroker.api.dto.SSRCreditCardDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocoDTO;
import com.isa.thinair.msgbroker.api.dto.SSRDocsDTO;
import com.isa.thinair.msgbroker.api.dto.SSREmergencyContactDetailDTO;
import com.isa.thinair.msgbroker.api.dto.SSRFoidDTO;
import com.isa.thinair.msgbroker.api.dto.SSRInfantDTO;
import com.isa.thinair.msgbroker.api.dto.SSRMinorDTO;
import com.isa.thinair.msgbroker.api.dto.SSROSAGDTO;
import com.isa.thinair.msgbroker.api.dto.SSROthersDTO;
import com.isa.thinair.msgbroker.api.dto.SSRPassportDTO;
import com.isa.thinair.msgbroker.api.dto.SegmentDTO;
import com.isa.thinair.msgbroker.api.dto.SsrTicketingTimeLimitDto;
import com.isa.thinair.msgbroker.api.dto.UnchangedNamesDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

/**
 * Type B Message Parser.
 * 
 * @author Chan Gunawardena, sanjeewaf, Manoj 
 * 
 */
public class GDSMessageParser {

	private Pattern pattern;

	private Pattern subElementPattern;

	private Matcher matcher;

	private Matcher subElementMatcher;

	private LinkedHashMap elementRegexHashMap;

	private HashMap messageSequenceRegexHashMap;

	private HashMap subElementReagexHashMap;

	private BookingRequestDTO bookingRequestDTO;

	private List<NameDTO> nameDTOs;

	private List<BookingSegmentDTO> bookingSegmentDTOs;
	
	private List<BookingSegmentDTO> oalSegmentDTOs;

	private List<OSIDTO> osiDTOs;

	private List<SSRDTO> infantDTOs;

	private List<SSRDTO> ssrDTOs;
	
	private SSRCreditCardDetailDTO cardDetailDTO;

	private List<SSRDTO> psptDTOs;

	private List<SSRDTO> docsDTOs;

	private List<SSRDTO> foidDTOs;

	private List<SSRDTO> unSupportedSSRDTOs;

	private List<SSRDTO> emergencyContactDetailDTOs;
	
	private List<SSRDTO> docoDTOs;

	private List<UnchangedNamesDTO> unChangedNameDTOs;

	private String sElementSeq;

	private String sStatus;

	private AdminDAO adminDAO;

	/** List will hold the action codes recived in segnment element */
	private List<String> segmentActionCodeList;
	
	private List<String> oalSegmentActionCodeList;

	/** List will hold the changedNameDTO's */
	private List<ChangedNamesDTO> changedNameDTOs;

	/** logger initialization. */
	private Log log = LogFactory.getLog(getClass());

	/** Constant string for under score:. */
	private static final String HYPHEN = "-";

	/** Constant string for under ".":. */
	private static final String POINT = ".";

	/** Constant string for regular expression "\\". */
	private static final String REGEXP_POINT = "\\.";

	/** Constant string for regular expression "///". */
	private static final String ELEMENT_CONTINUATION_MARK = "///";

	/** Constant string for regular expression "//". */
	private static final String DOUBLE_FOWARD_SLASH = "//";
	
	/** Constant string for regular expression "//". */
	private static final String SINGLE_FOWARD_SLASH = "/";

	/** Constant string for Single Space String . */
	private static final String SINGLE_SPACE = " ";

	/** Constant string postfix for SSR infant which occupies code . */
	private static final String OCCUPY_SEAT_POSTFIX = "OCCUPYING SEAT";

	/** Constant string postfix for SSR infant which occupies code . */
	private static final String OCCUPY_SEAT_POSTFIX1 = "OCCUPYINGSEAT";

	/** Constant string for SSR infant age in months "MTHS". */
	private static final String MTHS = "MTHS";
	
	/** Constant string for SSR DOCS and DOCO date format **/
	private static final String DATE_FORMAT_STRING = "ddMMMyy";

	/** Creates a new instance of GDSMessageParser */
	public GDSMessageParser() {
		RegularExpressionLookup oRegularExpressions = new RegularExpressionLookup();
		elementRegexHashMap = oRegularExpressions.getElementRegexHashMap();
		messageSequenceRegexHashMap = oRegularExpressions.getMessageSequenceRegexHashMap();
		subElementReagexHashMap = oRegularExpressions.getSubElementReagexHashMap();
	}

	public MessageParserResponseDTO parseMessage(String messageText) {

		MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
		try {
			BookingRequestDTO bookingRequestDTO = parseRawMessage(messageText);
			oMessageParserResponseDTO.setRequestDTO(bookingRequestDTO);
			oMessageParserResponseDTO.setSuccess(true);
			return oMessageParserResponseDTO;
		} catch (ModuleException me) {
			oMessageParserResponseDTO.setRequestDTO(null);
			oMessageParserResponseDTO.setSuccess(false);
			oMessageParserResponseDTO.setReasonCode(me.getExceptionCode());
			oMessageParserResponseDTO.setStatus(sStatus);
			return oMessageParserResponseDTO;
		}

	}

	public MessageParserResponseDTO parseMessage(InMessage inMessage) {

		MessageParserResponseDTO oMessageParserResponseDTO = new MessageParserResponseDTO();
		try {
			String messageText = inMessage.getInMessageText();
			BookingRequestDTO bookingRequestDTO = parseRawMessage(messageText);
			delaySSRTKNEMessageProcessing(inMessage, bookingRequestDTO, oMessageParserResponseDTO);
			if (bookingRequestDTO.getResponderAddress() == null ) {
				bookingRequestDTO.setResponderAddress(extractAddress(inMessage.getRecipientEmailAddress(),
						inMessage.getRecipientTTYAddress()));
			}
			if (bookingRequestDTO.getOriginatorAddress() == null) {
				bookingRequestDTO.setOriginatorAddress(extractAddress(inMessage.getSentEmailAddress(),
						inMessage.getSentTTYAddress()));
			}
			if (inMessage.getGdsMessageID() != null) {
				bookingRequestDTO.setCommunicationReference(inMessage.getGdsMessageID());
			}
			oMessageParserResponseDTO.setRequestDTO(bookingRequestDTO);
			oMessageParserResponseDTO.setSuccess(true);
			return oMessageParserResponseDTO;
		} catch (ModuleException me) {
			oMessageParserResponseDTO.setRequestDTO(null);
			oMessageParserResponseDTO.setSuccess(false);
			oMessageParserResponseDTO.setReasonCode(me.getExceptionCode());
			oMessageParserResponseDTO.setStatus(sStatus);
			return oMessageParserResponseDTO;
		}

	}

	private void delaySSRTKNEMessageProcessing(InMessage inMessage, BookingRequestDTO bookingRequestDTO2,
			MessageParserResponseDTO oMessageParserResponseDTO) {
		if (bookingRequestDTO.getSsrDTOs() != null) {
			boolean noChangeRequestToETs = noChangeRequestToETs(bookingRequestDTO.getSsrDTOs());
			if (!noChangeRequestToETs && (inMessage.getRetryCount() == null || inMessage.getRetryCount() < 2)) {
				if (inMessage.getRetryCount() == null) {
					inMessage.setRetryCount(1);
				} else {
					inMessage.setRetryCount(inMessage.getRetryCount() + 1);
				}
				oMessageParserResponseDTO.setSkipProcessing(true);
			}
		}
	}

	/**
	 * Method will compose the raw text into a set of new lines and will combine all continuation elements into single
	 * element.
	 * 
	 * @deprecated
	 * @param rawMessageText
	 * @return break down the message to set of lines.
	 * @throws ModuleException
	 */
	private String[] rawMessageCompiler(String rawMessageText) throws ModuleException {

		ArrayList v1 = new ArrayList();
		if (rawMessageText == null || rawMessageText.equals("")) {
			if (log.isDebugEnabled()) {
				log.debug("Empty Message");
			}

			sStatus = "Empty text";
			throw new ModuleException("message.text.empty");

		}
		rawMessageText = rawMessageText.replaceAll("\r", "\n");

		String[] elementStrings = rawMessageText.split("\n");

		for (int i = 0; i < elementStrings.length; i++) {

			if (elementStrings[i].length() > 69) {

				System.out.println(i + "-:" + elementStrings[i]);
				System.out.println("Length of the element exceeds 69 characters");

				if (log.isDebugEnabled()) {
					log.debug("Length of the element exceeds 69 characters " + elementStrings[i]);
				}
				sStatus = "Length of the element exceeds 69 characters";
				throw new ModuleException("gdsmessageparser.element.excessivelength");

			}
			if (i + 1 < elementStrings.length && elementStrings[i + 1].contains("///")) {
				elementStrings[i] = elementStrings[i].concat(elementStrings[i + 1].substring(elementStrings[i + 1]
						.indexOf(ELEMENT_CONTINUATION_MARK) + 3));
				v1.add(elementStrings[i]);
				elementStrings[i + 1] = elementStrings[i];
				i++;

			} else {
				v1.add(elementStrings[i]);
			}

		}
		System.out.println("\n************** construct****************\n");
		for (int i = 0; i < v1.size(); i++) {
			System.out.println(v1.get(i));
		}

		System.out.println("\n************** construct****************\n");

		return null;
	}

	/**
	 * Method that Parse the raw message.
	 * 
	 * @throws ModuleException
	 *             if found a error while parsing.
	 */
	private BookingRequestDTO parseRawMessage(String sMessage) throws ModuleException {
		sStatus = "";

		if (sMessage == null || sMessage.equals("")) {
			if (log.isDebugEnabled()) {
				log.debug("Empty Message");
			}
			sStatus = "Empty text";
			throw new ModuleException("message.text.empty");
		}

		sElementSeq = "";
		bookingRequestDTO = new BookingRequestDTO();
		nameDTOs = new ArrayList<NameDTO>();
		bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		oalSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		osiDTOs = new ArrayList<OSIDTO>();
		infantDTOs = new ArrayList<SSRDTO>();
		ssrDTOs = new ArrayList<SSRDTO>();
		psptDTOs = new ArrayList<SSRDTO>();
		docsDTOs = new ArrayList<SSRDTO>();
		foidDTOs = new ArrayList<SSRDTO>();
		unSupportedSSRDTOs = new ArrayList<SSRDTO>();
		emergencyContactDetailDTOs = new ArrayList<SSRDTO>();
		unChangedNameDTOs = new ArrayList<UnchangedNamesDTO>();
		segmentActionCodeList = new ArrayList<String>();
		oalSegmentActionCodeList = new ArrayList<String>();
		changedNameDTOs = new ArrayList<ChangedNamesDTO>();
		cardDetailDTO = new SSRCreditCardDetailDTO();
		docoDTOs = new ArrayList<SSRDTO>();

		/*
		 * Replace all carriage return characters present probably due to direct copying from windows files, with a line
		 * feed
		 */
		sMessage = sMessage.replaceAll("\r", "");
		sElementSeq = getElementSequence(sMessage);
		HashMap sMessageKeyHashMap = getMessageType(sElementSeq);
		if (sMessageKeyHashMap != null) {
			if (cardDetailDTO != null && cardDetailDTO.getFullElement() != null) {
				ssrDTOs.add(cardDetailDTO);
			}

			bookingRequestDTO.setNewNameDTOs(nameDTOs);
			bookingRequestDTO.setBookingSegmentDTOs(bookingSegmentDTOs);
			bookingRequestDTO.setOsiDTOs(osiDTOs);
			bookingRequestDTO.addSsrDTOs(infantDTOs);
			bookingRequestDTO.addSsrDTOs(ssrDTOs);
			bookingRequestDTO.addSsrDTOs(psptDTOs);
			bookingRequestDTO.addSsrDTOs(docsDTOs);
			bookingRequestDTO.addSsrDTOs(foidDTOs);
			bookingRequestDTO.addSsrDTOs(emergencyContactDetailDTOs);
			bookingRequestDTO.addSsrDTOs(docoDTOs);
			bookingRequestDTO.setUnsupportedSSRDTOs(unSupportedSSRDTOs);
			bookingRequestDTO.setUnChangedNameDTOs(unChangedNameDTOs);
			bookingRequestDTO.setChangedNameDTOs(changedNameDTOs);
			bookingRequestDTO.setMessageReceivedDateStamp(new Date());
			bookingRequestDTO.setOtherAirlineSegmentDTOs(oalSegmentDTOs);

			if (bookingRequestDTO.getMessageIdentifier() == null) {
				extractMessageIdentifier();
				// Set set = sMessageKeyHashMap.keySet();
				// Object[] oArray = set.toArray();
				// bookingRequestDTO.setMessageIdentifier((String)oArray[0]);
			}
			updateSsrs();
			return bookingRequestDTO;
		} else {

			if (log.isDebugEnabled()) {
				log.debug("Unknown message, element Sequence = " + sElementSeq);
			}
			sStatus = "Unknown Message";
			throw new ModuleException("gdsmessageparser.sequence.unknown");
		}
	}

	private String extractAddress(String emailID, String ttyAddress) {
		String address = "";
		if (emailID != null && !emailID.equals("")) {
			String[] splittedEmailAddress = emailID.split("@");
			address = splittedEmailAddress[0].trim().toUpperCase();
		} else if (ttyAddress != null && !ttyAddress.equals("")) {
			address = ttyAddress;
		}

		return address;
	}

	/**
	 * Method will set the Message identifier when its not available in the raw message. * @throws ModuleException when
	 * raw message does not contain any action status code
	 */
	private void extractMessageIdentifier() throws ModuleException {
		List<String> c = new ArrayList<String>();
		c.add("XX");
		c.add("OX");
		c.add("XL");
		c.add("XR");
		c.add("IX");
		c.add("CX");

		int noOfActionCodes;
		String messageIdentifier = null;

		if (segmentActionCodeList.isEmpty()) {
			throw new ModuleException("gdsmessageparser.action.status.unknown");

		} else {
			noOfActionCodes = segmentActionCodeList.size();
			List<String> segmentList = new ArrayList<String>(segmentActionCodeList);
			String pnr = getPnrReference(bookingRequestDTO);

			if (pnr != null) {

				segmentActionCodeList.retainAll(c);
				segmentList.removeAll(TypeBMessageConstants.getStatusCodesList());
				if (segmentList.isEmpty()) {

					if (sElementSeq != null
							&& sElementSeq.contains(MesageParserConstants.GDSMessageElements.MISCELLANEOUS_ABBREVIATION)
							|| (ssrDTOs != null && !ssrDTOs.isEmpty()) || (osiDTOs != null && !osiDTOs.isEmpty())
							|| (docsDTOs != null && !docsDTOs.isEmpty()) || (infantDTOs != null && !infantDTOs.isEmpty())
							|| (docoDTOs != null && !docoDTOs.isEmpty())) {
						if (osiDTOs != null && !osiDTOs.isEmpty()) {
							for (OSIDTO osidto : osiDTOs) {
								if (osidto.getCodeOSI().equals(GDSExternalCodes.OSICodes.RECORD_LOCATOR.getCode())) {
									messageIdentifier = MesageParserConstants.MessageTypes.DIVIDE_RESP;
									break;
								}
							}
						} else if (!noChangeRequestToOALSegemnts(pnr)) {
							messageIdentifier = MesageParserConstants.MessageTypes.AMEND;
						} else if (!sElementSeq.contains(MesageParserConstants.GDSMessageElements.MISCELLANEOUS_ABBREVIATION)) {
							boolean noChangeRequestToSSRInfants = noChangeRequestToSSRInfants();
							boolean noChangeRequestToSSRDocs = noChangeRequestToSSRDocs();
							boolean noChangeRequestToSSRs = noChangeRequestToSSRs();
							boolean noChangeRequestToSSRDoco = noChangeRequestToSSRDoco();
							if (noChangeRequestToSSRs && noChangeRequestToSSRInfants && noChangeRequestToSSRDocs && noChangeRequestToSSRDoco) {
								messageIdentifier = MesageParserConstants.MessageTypes.RESERVATION_RESP;
							}
						}
						if (messageIdentifier == null) {
							messageIdentifier = MesageParserConstants.MessageTypes.AMEND;
						}
						bookingRequestDTO.setMessageIdentifier(messageIdentifier);
					} else if (!noChangeRequestToOALSegemnts(pnr)) {
						bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.AMEND);
					} else {
						bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.RESERVATION_RESP);
					}

				} else if (noOfActionCodes - segmentActionCodeList.size() == 0) {
					if (unChangedNameDTOs != null & unChangedNameDTOs.size() > 0) {
						bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.AMEND);
					} else {
						bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.CAN);
					}
				} else {
					bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.AMEND);
				}

			} else {
				segmentList.removeAll(c);
				if (segmentList.isEmpty()) {
					throw new ModuleException("gdsmessageparser.action.status.unknown");
				} else {
					bookingRequestDTO.setMessageIdentifier(MesageParserConstants.MessageTypes.BOOKING);
				}
			}
		}
	}
	
	private boolean noChangeRequestToSSRInfants() {
		if (!infantDTOs.isEmpty()) {
			for (SSRDTO infantDTO : infantDTOs) {
				if ((infantDTO.getAdviceOrStatusCode() != null && !infantDTO.getAdviceOrStatusCode().equals(
						GDSExternalCodes.AdviceCode.CONFIRMED))
						|| (infantDTO.getActionOrStatusCode() != null && !infantDTO.getActionOrStatusCode().equals(
								GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode()))) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean noChangeRequestToSSRDocs() {
		if (!docsDTOs.isEmpty()) {
			for (SSRDTO docsDTO : docsDTOs) {
				if ((docsDTO.getAdviceOrStatusCode() != null && !docsDTO.getAdviceOrStatusCode().equals(
						GDSExternalCodes.AdviceCode.CONFIRMED))
						|| (docsDTO.getActionOrStatusCode() != null && docsDTO.getActionOrStatusCode().equals(
								GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode()))) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean noChangeRequestToSSRDoco() {
		if (!docoDTOs.isEmpty()) {
			for (SSRDTO docoDTO : docoDTOs) {
				if ((docoDTO.getAdviceOrStatusCode() != null && !docoDTO.getAdviceOrStatusCode().equals(
						GDSExternalCodes.AdviceCode.CONFIRMED))
						|| (docoDTO.getActionOrStatusCode() != null && docoDTO.getActionOrStatusCode().equals(
								GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode()))) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean noChangeRequestToSSRs() {
		if (!ssrDTOs.isEmpty()) {
			for (SSRDTO ssrDTO : ssrDTOs) {
				if (!(ssrDTO instanceof SSROthersDTO)) {
					if ((ssrDTO.getAdviceOrStatusCode() != null && !ssrDTO.getAdviceOrStatusCode().equals(
							GDSExternalCodes.AdviceCode.CONFIRMED))
							|| (ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.ActionCode.NEED.getCode()))
							|| (ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.ActionCode.CANCEL.getCode()))
							|| ((ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.TKNE) || ssrDTO.getCodeSSR().equals(
									TypeBMessageConstants.SSRCodes.FQTV))
									&& ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode()))
							|| (ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.TICKETING_TIME_LIMIT)
									&& ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.ActionCode.SOLD.getCode()))
							|| (ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.TKNR)
									&& ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode()))) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean noChangeRequestToETs(List<SSRDTO> ssrDTOList) {
		if (!ssrDTOList.isEmpty()) {
			for (SSRDTO ssrDTO : ssrDTOList) {
				if (!(ssrDTO instanceof SSROthersDTO)) {
					if ((ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.TKNE)
							&& ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
							GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode()))
							|| (ssrDTO.getCodeSSR().equals(TypeBMessageConstants.SSRCodes.TKNR)
									&& ssrDTO.getActionOrStatusCode() != null && ssrDTO.getActionOrStatusCode().equals(
									GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode()))) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean noChangeRequestToOALSegemnts(String pnr) {
		if (!oalSegmentActionCodeList.isEmpty()) {
			Integer oalSegCount = LookupUtil.lookupAdminJdbcDAO().getOtherAirlineConfirmedSegmentCount(pnr);
			if (oalSegmentActionCodeList.contains(TypeBMessageConstants.StatusCodes.HX)
					|| !oalSegCount.equals(oalSegmentDTOs.size())) {
				return false;
			}
		}
		return true;
	}

	private String getPnrReference(BookingRequestDTO bookingRequestDTO) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getResponderRecordLocator();
		if (recordLocatorDTO == null) {
			String pnr = null;
			String originatorPnrReference = bookingRequestDTO.getOriginatorRecordLocator().getPnrReference();
			String bookingOffice = bookingRequestDTO.getOriginatorRecordLocator().getBookingOffice();
			try {
				if (bookingOffice != null
						&& bookingOffice.length() > 2
						&& AppSysParamsUtil.getDefaultCarrierCode().equals(
								bookingOffice.substring(bookingOffice.length() - 2, bookingOffice.length()))) {
					if (LookupUtil.lookupAdminJdbcDAO().isReservationExists(originatorPnrReference)) {
						return originatorPnrReference;
					}
				} else {
					pnr = LookupUtil.lookupAdminJdbcDAO().getPnrByExtRecordLocator(originatorPnrReference);
				}
			} catch (Exception ex) {
			}
			return pnr;
		} else {
			return recordLocatorDTO.getPnrReference();
		}
	}

	/**
	 * @deprecated from the one getElementSequence which use array instead of tokens.
	 */
	private String getElementSequence1(String sRawMessage) throws ModuleException {
		String sArrayMessLines[];
		int iMessageLineCount = 0;
		StringTokenizer messageTokenizer = new StringTokenizer(sRawMessage, "\n");
		iMessageLineCount = messageTokenizer.countTokens();
		sArrayMessLines = new String[iMessageLineCount];
		String sElementSequence = "";
		int iTokennumber = -1;
		while (messageTokenizer.hasMoreTokens()) {
			iTokennumber++;
			String sElement = messageTokenizer.nextToken();
			if (sElement.length() > 69) {

				if (log.isDebugEnabled()) {
					log.debug("Length of the element exceeds 69 characters " + sElement);
				}
				sStatus = "Length of the element exceeds 69 characters";
				throw new ModuleException("gdsmessageparser.element.excessivelength");
			}

			sArrayMessLines[iTokennumber] = sElement;
			Set set = elementRegexHashMap.keySet();
			Iterator iter = set.iterator();
			boolean elementFound = false;
			while (iter.hasNext()) {
				String sKey = (String) iter.next();
				String sRegExp = (String) elementRegexHashMap.get(sKey);
				pattern = Pattern.compile(sRegExp);
				matcher = pattern.matcher(sElement);
				if (matcher.matches()) {
					extractValues(matcher, sKey);
					elementFound = true;
					if (sKey.equals(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER))
						sElementSequence += sElement + "|";
					else
						sElementSequence += sKey + "|";
					break;
				} else {
					elementFound = false;
				}
			}
			if (!elementFound) {

				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + sElement);
				}
				sStatus = "Unknown element: " + sElement;
				throw new ModuleException("gdsmessageparser.element.unknown");
			}
		}
		return sElementSequence;
	}

	/**
	 * Method will break down the raw message in to element and find out the each element for known type .if the element
	 * identified as known element then will keep track of the element sequence.which helps to determine message type.
	 * 
	 * @param sRawMessage
	 * @return elements sequence that generated.
	 * @throws ModuleException
	 */
	private String getElementSequence(String sRawMessage) throws ModuleException {
		boolean chntFound = false;
		String sElementSequence = "";
		String[] messageTokens = sRawMessage.split("\n");

		for (int i = 0; i < messageTokens.length; i++) {
			String sElement = messageTokens[i];
			if (sElement != null) {
				sElement = sElement.trim();
			}

			if (i == 0) {
				if (sElement.substring(0, 3).matches("[A-Z]{2}( )")) {

					sElement = sElement.substring(3);
				}

			}
			// if (i == 2) {
			// if (sElement.equals("SPT")) {
			// continue;
			// }
			// }

			if (sElement.length() > 69) {
				System.out.println("Length of the element exceeds 69 characters");
				if (log.isDebugEnabled()) {
					log.debug("Length of the element exceeds 69 characters " + sElement);
				}
				throw new ModuleException("gdsmessageparser.element.excessivelength");
			}

			Set set = elementRegexHashMap.keySet();
			Iterator iter = set.iterator();
			boolean elementFound = false;

			while (iter.hasNext()) {
				String sKey = (String) iter.next();
				boolean isSkeyParsed = isParsedElemens(sElementSequence, sKey);
				if (isSkeyParsed) {
					continue;
				}
				String sRegExp = (String) elementRegexHashMap.get(sKey);
				pattern = Pattern.compile(sRegExp);
				matcher = pattern.matcher(sElement);
				if (matcher.matches()) {
					if (sKey.equals(MesageParserConstants.GDSMessageElements.NAME)
							&& messageTokens[i + 1].trim().equals(MesageParserConstants.MessageTypes.CHNM)) {
						setChangeNames(sElement, messageTokens[i + 2]);
						i = i + 2;
						elementFound = true;
						chntFound = true;
						sElementSequence += MesageParserConstants.GDSMessageElements.NAME + "|"
								+ MesageParserConstants.GDSMessageElements.MISCELLANEOUS_ABBREVIATION + "|"
								+ MesageParserConstants.GDSMessageElements.NAME + "|";
						break;

					} else if (sKey.equals(MesageParserConstants.GDSMessageElements.NAME) && sElementSequence.endsWith("NAM|")
							&& chntFound) {
						setChangeNames(null, sElement);
						elementFound = true;
						break;
					} else {
						extractValues(matcher, sKey);
					}

					elementFound = true;
					if (sKey.equals(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER))
						sElementSequence += sElement + "|";
					else
						sElementSequence += sKey + "|";
					break;
				} else {
					elementFound = false;
				}
			}

			if (!elementFound) {
				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + sElement);
				}
				sStatus = "Unknown element: " + sElement;
				throw new ModuleException("gdsmessageparser.element.unknown");
			}
		}
		return sElementSequence;
	}

	/**
	 * Method will identifies weather the current key has been already identified or key has passed the possible passion
	 * of that can occur. eg : if skey id (MID)Message identifier then we know that it cannot be come after RLO and
	 * should not search for MID if we found already.
	 * 
	 * @param elementSequence
	 *            represent the parsed element sequence.
	 * @param key
	 *            current search element been processed .
	 * @return
	 */
	private boolean isParsedElemens(String elementSequence, String skey) {

		// REC|SEN|RLR|RLO|NAM|SEG|SEG|OS1|OS1|OS1|OS1|OS1|
		boolean isParsed = false;
		if (elementSequence.contains(skey)) {
			if (skey.equals(MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS)) {
				isParsed = true;
			} else if (skey.equals(MesageParserConstants.GDSMessageElements.SENDER_ADDRESS)) {
				isParsed = true;
			} else if (skey.equals(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER)) {
				isParsed = true;
			} else if (skey.equals(MesageParserConstants.GDSMessageElements.RECORD_LOCATOR)) {
				if (elementSequence.lastIndexOf(skey) != elementSequence.lastIndexOf(skey)) {
					isParsed = true;
				} else if (elementSequence.contains(MesageParserConstants.GDSMessageElements.NAME)) {
					isParsed = true;
				} else {
					isParsed = false;
				}
			} else if (skey.equals(MesageParserConstants.GDSMessageElements.NAME)) {
				if (elementSequence.contains(MesageParserConstants.GDSMessageElements.SEGMENT)) {
					isParsed = true;
				}

			}

		} else {

			isParsed = false;
		}

		return isParsed;
	}

	/**
	 * Method will call upon that the element sequence found NAME CHNT NAME sequence this set the ChangedNameDto .
	 * Assume that there cannot be more than one CHNT's in single message.
	 * 
	 * @param oldNameElement
	 *            Name Element before change.
	 * @param newNameElement
	 *            New Name Element.
	 * @throws ModuleException
	 */
	private void setChangeNames(String oldNameElement, String newNameElement) throws ModuleException {

		ChangedNamesDTO changedNamesDTO = new ChangedNamesDTO();

		if (oldNameElement != null) {

			List<NameDTO> oldNameDTOList = nameElementcheck(oldNameElement);
			if (!nameDTOs.isEmpty()) {
				oldNameDTOList.addAll(nameDTOs);
			}
			changedNamesDTO.setOldNameDTOs(oldNameDTOList);
		}

		List<NameDTO> newNameDTOList = nameElementcheck(newNameElement);

		if (changedNameDTOs != null && changedNameDTOs.size() > 0) {
			((ChangedNamesDTO) changedNameDTOs.get(0)).getNewNameDTOs().addAll(newNameDTOList);

		} else {
			changedNamesDTO.setNewNameDTOs(newNameDTOList);
			changedNameDTOs.add(changedNamesDTO);
		}

	}

	/**
	 * @param sElementSeq
	 * @return HashMap contains Message type
	 */
	private HashMap getMessageType(String sElementSeq) {
		if (sElementSeq == null)
			return null;
		HashMap<String, String> keyAndSequence = new HashMap<String, String>();
		Set set = messageSequenceRegexHashMap.keySet();

		Iterator iter = set.iterator();
		String sKey = "";
		while (iter.hasNext()) {
			sKey = (String) iter.next();
			String sRegExp = (String) messageSequenceRegexHashMap.get(sKey);
			pattern = Pattern.compile(sRegExp);
			matcher = pattern.matcher(sElementSeq);
			if (matcher.matches()) {
				keyAndSequence.put(sKey, sElementSeq);
				return keyAndSequence;
			}
		}
		return null;
	}

	/**
	 * Method will extract each element strings into specific element DTO's.
	 * 
	 * @param matcher
	 *            matched regexp
	 * @param sKey
	 *            contains the element string.
	 * @throws ModuleException
	 */
	private void extractValues(Matcher matcher, String sKey) throws ModuleException {

		if (sKey.equals(MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS)) {
			bookingRequestDTO.setResponderAddress(matcher.toMatchResult().group(1));
		}
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SENDER_ADDRESS)) {
			bookingRequestDTO.setOriginatorAddress(matcher.toMatchResult().group(1));
			bookingRequestDTO.setCommunicationReference(matcher.toMatchResult().group(2));
		}
		if (sKey.equals(MesageParserConstants.GDSMessageElements.MESSAGE_IDENTIFIER)) {
			bookingRequestDTO.setMessageIdentifier(matcher.toMatchResult().group(1));
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.RECORD_LOCATOR)) {
			String bookingOffice = matcher.toMatchResult().group(1);
			RecordLocatorDTO oRecordLocatorDTO = new RecordLocatorDTO();
			if (bookingOffice != null) {
				bookingOffice = bookingOffice.trim();
			}
			oRecordLocatorDTO.setBookingOffice(bookingOffice);
			oRecordLocatorDTO.setPnrReference((matcher.toMatchResult().group(2)));
			oRecordLocatorDTO.setTaOfficeCode((matcher.toMatchResult().group(5)));
			oRecordLocatorDTO.setUserID((matcher.toMatchResult().group(7)));
			oRecordLocatorDTO.setClosestCityAirportCode((matcher.toMatchResult().group(10)));
			oRecordLocatorDTO.setCariierCRSCode((matcher.toMatchResult().group(12)));
			oRecordLocatorDTO.setUserType((matcher.toMatchResult().group(14)));
			oRecordLocatorDTO.setCountryCode((matcher.toMatchResult().group(16)));
			oRecordLocatorDTO.setCurrencyCode((matcher.toMatchResult().group(18)));
			oRecordLocatorDTO.setAgentDutyCode((matcher.toMatchResult().group(20)));
			oRecordLocatorDTO.setErspId((matcher.toMatchResult().group(22)));
			oRecordLocatorDTO.setFirstDeparturefrom((matcher.toMatchResult().group(24)));
			if (bookingOffice != null) {
				String pointOfSales = matcher.toMatchResult().group().substring(bookingOffice.length());
				oRecordLocatorDTO.setPointOfSales(pointOfSales.trim());
			}

			if (bookingRequestDTO.getOriginatorRecordLocator() != null) {
				// oRecordLocatorDTO.setOwner(MesageParserConstants.GDSMessageElements.RECIPIENT_ADDRESS);
				bookingRequestDTO.setResponderRecordLocator(oRecordLocatorDTO);

			} else {
				// oRecordLocatorDTO.setOwner(MesageParserConstants.GDSMessageElements.SENDER_ADDRESS);
				bookingRequestDTO.setOriginatorRecordLocator(oRecordLocatorDTO);
			}
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.NAME)) {

			String sNamesString = matcher.toMatchResult().group(1);
			List<NameDTO> nameDTOList = nameElementcheck(sNamesString);

			nameDTOs.addAll(nameDTOList);

		}

		/*
		 * Groups for Segment Element group1 = carrierCode group2 = flightNumber group3 = reservationBookingDesignator
		 * group4 = flightDate group5 = origin group6 = destination group7 = actionCode group8 = noofPaxes group10 =
		 * departureTimr group11 = arrivaltime
		 */

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SEGMENT)) {
			try {

				String airline = matcher.toMatchResult().group(1);
				BookingSegmentDTO bookingSegmentDTO = new BookingSegmentDTO();
				bookingSegmentDTO.setCarrierCode(matcher.toMatchResult().group(1));
				bookingSegmentDTO.setFlightNumber(matcher.toMatchResult().group(2));
				bookingSegmentDTO.setBookingCode(matcher.toMatchResult().group(3));

				Date d = CalendarUtil.getSegmentDate(matcher.toMatchResult().group(4).trim());
				if (d == null) {
					sStatus = "Unknown element: " + matcher.toMatchResult().group();
					throw new ModuleException("gdsmessageparser.element.unknown");
				} else {
					bookingSegmentDTO.setDepartureDate(d);
				}
				// SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
				// Date d = sdf.parse(matcher.toMatchResult().group(4));
				// bookingSegmentDTO.setDepatDate(depatDate);;
				bookingSegmentDTO.setDepartureStation(matcher.toMatchResult().group(6));
				bookingSegmentDTO.setDestinationStation(matcher.toMatchResult().group(7));
				bookingSegmentDTO.setActionOrStatusCode(matcher.toMatchResult().group(8));
				bookingSegmentDTO.setNoofPax(Integer.parseInt(matcher.toMatchResult().group(9)));

				String depDate = matcher.toMatchResult().group(11);
				String arrDate = matcher.toMatchResult().group(12);
				String depDate2 = matcher.toMatchResult().group(13);
				String arrDate2 = matcher.toMatchResult().group(14);
				if (!Util.isNull(depDate)) {

					Date d1 = CalendarUtil.getSegmentTime(depDate.trim());
					bookingSegmentDTO.setDepartureTime(d1);
				} else if (!Util.isNull(depDate2)) {
					Date d1 = CalendarUtil.getSegmentTime(depDate2.trim());
					bookingSegmentDTO.setDepartureTime(d1);
				}

				if (!Util.isNull(arrDate)) {
					Date d2 = CalendarUtil.getSegmentTime(arrDate.trim());
					bookingSegmentDTO.setArrivalTime(d2);
				} else if (!Util.isNull(arrDate2)) {
					Date d2 = CalendarUtil.getSegmentTime(arrDate2.trim());
					bookingSegmentDTO.setArrivalTime(d2);
				}

				if (matcher.toMatchResult().group(16) != null && !matcher.toMatchResult().group(16).isEmpty()) {
					bookingSegmentDTO.setDayOffSet(Integer.parseInt(matcher.toMatchResult().group(16)));
				}
				bookingSegmentDTO.setMembersBlockIdentifier(matcher.toMatchResult().group(18));

				if (airline.equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					segmentActionCodeList.add(matcher.toMatchResult().group(8));
					bookingSegmentDTOs.add(bookingSegmentDTO);
				} else {
					oalSegmentActionCodeList.add(matcher.toMatchResult().group(8));
					oalSegmentDTOs.add(bookingSegmentDTO);
				}

			} catch (Exception e) {
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");
			}
		}
		if (sKey.equals(MesageParserConstants.GDSMessageElements.CS_SEGMENT)) {
			try {

				String airline = matcher.toMatchResult().group(1);
				Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();
				Set<String> csCarriers = new HashSet<String>();
				for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
					if (gdsStatusTO.isCodeShareCarrier()) {
						csCarriers.add(gdsStatusTO.getCarrierCode());
					}
				}
				csCarriers.add(AppSysParamsUtil.getDefaultCarrierCode());

				if (csCarriers.contains(airline) || airline.equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					BookingSegmentDTO bookingSegmentDTO = new BookingSegmentDTO();
					String csCarrierCode = matcher.toMatchResult().group(4);

					bookingSegmentDTO.setCarrierCode(matcher.toMatchResult().group(1));
					bookingSegmentDTO.setFlightNumber(matcher.toMatchResult().group(2));
					bookingSegmentDTO.setBookingCode(matcher.toMatchResult().group(3));
					bookingSegmentDTO.setCsCarrierCode(csCarrierCode);
					bookingSegmentDTO.setCsFlightNumber(matcher.toMatchResult().group(5));
					bookingSegmentDTO.setCsBookingCode(matcher.toMatchResult().group(6));

					Date d = CalendarUtil.getSegmentDate(matcher.toMatchResult().group(7).trim());
					if (d == null) {
						sStatus = "Unknown element: " + matcher.toMatchResult().group();
						throw new ModuleException("gdsmessageparser.element.unknown");
					} else {
						bookingSegmentDTO.setDepartureDate(d);
					}
					// SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
					// Date d = sdf.parse(matcher.toMatchResult().group(4));
					// bookingSegmentDTO.setDepatDate(depatDate);;
					bookingSegmentDTO.setDepartureStation(matcher.toMatchResult().group(9));
					bookingSegmentDTO.setDestinationStation(matcher.toMatchResult().group(10));
					bookingSegmentDTO.setActionOrStatusCode(matcher.toMatchResult().group(11));
					bookingSegmentDTO.setNoofPax(Integer.parseInt(matcher.toMatchResult().group(12)));

					String depDate = matcher.toMatchResult().group(14);
					String arrDate = matcher.toMatchResult().group(15);
					String depDate2 = matcher.toMatchResult().group(16);
					String arrDate2 = matcher.toMatchResult().group(17);
					if (!Util.isNull(depDate)) {

						Date d1 = CalendarUtil.getSegmentTime(depDate.trim());
						bookingSegmentDTO.setDepartureTime(d1);
					} else if (!Util.isNull(depDate2)) {
						Date d1 = CalendarUtil.getSegmentTime(depDate2.trim());
						bookingSegmentDTO.setDepartureTime(d1);
					}

					if (!Util.isNull(arrDate)) {
						Date d2 = CalendarUtil.getSegmentTime(arrDate.trim());
						bookingSegmentDTO.setArrivalTime(d2);
					} else if (!Util.isNull(arrDate2)) {
						Date d2 = CalendarUtil.getSegmentTime(arrDate2.trim());
						bookingSegmentDTO.setArrivalTime(d2);
					}

					if (matcher.toMatchResult().group(19) != null && !matcher.toMatchResult().group(19).isEmpty()) {
						bookingSegmentDTO.setDayOffSet(Integer.parseInt(matcher.toMatchResult().group(19)));
					}
					bookingSegmentDTO.setMembersBlockIdentifier(matcher.toMatchResult().group(21));

					if (airline.equals(AppSysParamsUtil.getDefaultCarrierCode()) || airline.equals(csCarrierCode)) {
						bookingSegmentDTOs.add(bookingSegmentDTO);
						segmentActionCodeList.add(matcher.toMatchResult().group(11));
					} else {
						oalSegmentDTOs.add(bookingSegmentDTO);
						oalSegmentActionCodeList.add(matcher.toMatchResult().group(11));
					}
				}

			} catch (Exception e) {
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");
			}
		}
		
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_CHLD)) {
			setChildElementDTO(matcher);
//			setInfantElementDTO
//			try {
//				SSRChildDTO childDTO = new SSRChildDTO();
//				childDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.CHLD);
//				childDTO.setFullElement(matcher.group());
//				childDTO.setCarrierCode(matcher.toMatchResult().group(1));
//				childDTO.setActionOrStatusCode(matcher.toMatchResult().group(2));
//				childDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
//
//				/*
//				 * age of child in book : 05 yrs or 05 years but according to saber you have to enter DOB in following
//				 * format .
//				 */
//				String childAge = matcher.toMatchResult().group(6);
//				if (childAge != null && !childAge.equals("")) {
//
//					String dobFormatString = "ddMMMyy";
//					DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
//					Date dob = dobFormat.parse(childAge);
//					childDTO.setDateOfBirth(dob);
//
//				}
//
//				if (matcher.toMatchResult().group(9) != null) {
//					List ssrNameDTOList = setNameElementDTOs(matcher.toMatchResult().group(9).trim());
//
//					if (!ssrNameDTOList.isEmpty()) {
//						NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);
//						childDTO.setFirstName(nameDTO.getFirstName());
//						childDTO.setLastName(nameDTO.getLastName());
//						childDTO.setTitle(nameDTO.getPaxTitle());
//					}
//				} else if (nameDTOs.size() == 1) {
//					NameDTO nameDTO = (NameDTO) nameDTOs.get(0);
//					childDTO.setFirstName(nameDTO.getFirstName());
//					childDTO.setLastName(nameDTO.getLastName());
//					childDTO.setTitle(nameDTO.getPaxTitle());
//				} else {
//					throw new Exception();
//				}
//
//				ssrDTOs.add(childDTO);
//
//			} catch (Exception e) {
//
//				if (log.isDebugEnabled()) {
//					log.debug("Unknown element: " + matcher.toMatchResult().group());
//				}
//				sStatus = "Unknown element: " + matcher.toMatchResult().group();
//				throw new ModuleException("gdsmessageparser.element.unknown");
//			}
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_PST1)) {

			/*
			 * Saber Format -------------
			 * 
			 * EXAMPLE 1: PASSPORT FORMAT FOR INDIVIDUAL TRAVELER A B C D E F G I
			 * 3PSPT1/0123456789/US/02NOV68/MORRIS/WAYNETHOMAS/M-1.1
			 * 
			 * a use segment number or letter a or omit for all airlines b passengers passport number c one to three
			 * letter iso code of passport issuing country d passengers birth date, day month year of birth e family
			 * name as it appears on the passport f given and middle name as it appears on the passport g gender of
			 * passenger, use m-male,f-female,i-infant,
			 */

			/*
			 * start() = 0, end() = 69 group(0) = "SSR PSPT TW
			 * HK1/P132487219/US/15NOV61/PEERS/MARLAT/F/H-1PEERS/MARLAMS" group(1) = "TW" group(2) = "/" group(3) =
			 * "P132487219" passpor no group(4) = "/" group(5) = "US" issued country code group(6) = "/" group(7) =
			 * "15NOV61" date of birth group(8) = "/" group(9) = "PEERS" family name group(10) = "/" group(11) =
			 * "MARLAT" middle name group(12) = "null" group(13) = "/F/H-1PEERS/MARLAMS" group(14) = "/F/H" -- gender or
			 * gender/h - muslty passenger group(15) = "1PEERS/MARLAMS" -- pnr name group(16) = "null" group(17) = FREE
			 * TEXT
			 */
			try {

				SSRPassportDTO passportDTO = new SSRPassportDTO();
				passportDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.PSPT);
				passportDTO.setFullElement(matcher.toMatchResult().group());
				passportDTO.setCarrierCode(matcher.toMatchResult().group(1));
				passportDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
				passportDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
				passportDTO.setPassengerPassportNo(matcher.toMatchResult().group(3));
				passportDTO.setPassportCountryCode(matcher.toMatchResult().group(5));

				String dobString = matcher.toMatchResult().group(7);
				if (dobString != null && !dobString.equals("")) {

					String dobFormatString = "ddMMMyy";
					DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
					Date dob = dobFormat.parse(dobString);
					passportDTO.setPassengerDateOfBirth(dob);

				}

				passportDTO.setPassengerFamilyName(matcher.toMatchResult().group(9));
				passportDTO.setPassengerGivenName(matcher.toMatchResult().group(11));
				String genderInfo = matcher.toMatchResult().group(14);
				if (genderInfo != null) {
					if (genderInfo.contains("/H")) {
						passportDTO.setMultiPassengerPassport(true);
						passportDTO.setPassengerGender(genderInfo.substring(1, genderInfo.indexOf("/H")).trim());
					} else {
						passportDTO.setPassengerGender(genderInfo.trim());
					}
				}

				String pnrNameString = matcher.toMatchResult().group(15);
				if (pnrNameString != null) {
					List ssrNameDTOList = setNameElementDTOs(pnrNameString.trim());
					passportDTO.setPnrNameDTOs(ssrNameDTOList);
				}

				passportDTO.setFreeText(matcher.toMatchResult().group(17).trim());

				psptDTOs.add(passportDTO);

			} catch (Exception e) {

				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + matcher.toMatchResult().group());
				}
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");
			}

		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_PST2)) {

			try {

				Pattern psptContPattern = null;
				Matcher psptContMatcher = null;

				String childelement = matcher.toMatchResult().group();

				String passportParentElement = ((SSRPassportDTO) psptDTOs.get(psptDTOs.size() - 1)).getFullElement();
				psptDTOs.remove(psptDTOs.size() - 1);
				passportParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
				psptContPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.SSR_PST1));
				psptContMatcher = psptContPattern.matcher(passportParentElement);

				if (psptContMatcher.matches()) {

					SSRPassportDTO passportDTO = new SSRPassportDTO();
					passportDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.PSPT);
					passportDTO.setFullElement(psptContMatcher.toMatchResult().group());
					passportDTO.setCarrierCode(psptContMatcher.toMatchResult().group(1));
					passportDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
					passportDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
					passportDTO.setPassengerPassportNo(psptContMatcher.toMatchResult().group(3));
					passportDTO.setPassportCountryCode(psptContMatcher.toMatchResult().group(5));

					String dobString = matcher.toMatchResult().group(7);
					if (dobString != null && !dobString.equals("")) {

						String dobFormatString = "ddMMMyy";
						DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
						Date dob = dobFormat.parse(dobString);
						passportDTO.setPassengerDateOfBirth(dob);

					}

					passportDTO.setPassengerFamilyName(psptContMatcher.toMatchResult().group(9));
					passportDTO.setPassengerGivenName(psptContMatcher.toMatchResult().group(11));
					String genderInfo = psptContMatcher.toMatchResult().group(14);
					if (genderInfo != null) {
						if (genderInfo.contains("/H")) {
							passportDTO.setMultiPassengerPassport(true);
							passportDTO.setPassengerGender(genderInfo.substring(1, genderInfo.indexOf("/H")).trim());
						} else {
							passportDTO.setPassengerGender(genderInfo.trim());
						}
					}

					String pnrNameString = psptContMatcher.toMatchResult().group(15);
					if (pnrNameString != null) {
						List ssrNameDTOList = setNameElementDTOs(pnrNameString.trim());
						passportDTO.setPnrNameDTOs(ssrNameDTOList);
					}

					passportDTO.setFreeText(psptContMatcher.toMatchResult().group(17).trim());

					psptDTOs.add(passportDTO);

				}

			} catch (Exception e) {

				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + matcher.toMatchResult().group());
				}
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");
			}

		}

		/** Passenger form of identification information - SSR FOID */
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_FID1)) {

			SSRFoidDTO foidDTO = new SSRFoidDTO();
			foidDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.FOID);
			foidDTO.setFullElement(matcher.toMatchResult().group());
			foidDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
			foidDTO.setCarrierCode(matcher.toMatchResult().group(1));
			foidDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
			foidDTO.setPassengerIdentificationType(matcher.toMatchResult().group(4));
			foidDTO.setPassengerIdentificationNo(matcher.toMatchResult().group(5));
			String pnrNameString = matcher.toMatchResult().group(6);
			if (pnrNameString != null && pnrNameString.startsWith(HYPHEN)) {
				pnrNameString = pnrNameString.substring(1);
				List ssrNameDTOList = setNameElementDTOs(pnrNameString.trim());
				foidDTO.setPnrNameDTOs(ssrNameDTOList);
			}
			foidDTOs.add(foidDTO);

		}

		/** Passenger form of identification continuation information - SSR FOID */
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_FID2)) {

			Pattern foidContPattern = null;
			Matcher foidContMatcher = null;

			String childelement = matcher.toMatchResult().group();

			String foidParentElement = ((SSRFoidDTO) foidDTOs.get(foidDTOs.size() - 1)).getFullElement();
			foidDTOs.remove(foidDTOs.size() - 1);
			foidParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
			foidContPattern = Pattern
					.compile((String) elementRegexHashMap.get(MesageParserConstants.GDSMessageElements.SSR_FID1));
			foidContMatcher = foidContPattern.matcher(foidParentElement);

			if (foidContMatcher.matches()) {

				SSRFoidDTO foidDTO = new SSRFoidDTO();
				foidDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.FOID);
				foidDTO.setFullElement(foidContMatcher.toMatchResult().group());
				foidDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
				foidDTO.setCarrierCode(foidContMatcher.toMatchResult().group(1));
				foidDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
				foidDTO.setPassengerIdentificationType(foidContMatcher.toMatchResult().group(4));
				foidDTO.setPassengerIdentificationNo(foidContMatcher.toMatchResult().group(5));
				String pnrNameString = foidContMatcher.toMatchResult().group(6);
				if (pnrNameString != null && pnrNameString.startsWith(HYPHEN)) {
					pnrNameString = pnrNameString.substring(1);
					List ssrNameDTOList = setNameElementDTOs(pnrNameString.trim());
					foidDTO.setPnrNameDTOs(ssrNameDTOList);
				}
				foidDTOs.add(foidDTO);

			}

		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_OTHER)) {

			String stringElement = matcher.group(0);
			String codeSSR;
			boolean dtoFound = false;
			String ccType;
			String carrierCode;
			Pattern ccPattern = null;
			Matcher ccMatcher = null;
			SSROthersDTO ossrDTO = new SSROthersDTO();

			Pattern ptrn;
			Matcher mtchr;
			String text;

			codeSSR = matcher.toMatchResult().group(1);
			if (codeSSR.equals("GRPF") || codeSSR.equals("GRPS")) {
				bookingRequestDTO.setGroupBooking(true);
			}
			ossrDTO.setCodeSSR(codeSSR);
			carrierCode = matcher.toMatchResult().group(2);
			ossrDTO.setCarrierCode(carrierCode);

			text = matcher.toMatchResult().group(3);

			if (codeSSR.equals(TypeBMessageConstants.SSRCodes.TICKETING_TIME_LIMIT)) {
				ptrn = Pattern.compile(MesageParserConstants.SubElementRegularExpressions.SSR_TICKETING_TIME_LIMIT);
				mtchr = ptrn.matcher(text);

				if (mtchr.matches()) {
					Date ticketingTime = CalendarUtil.parseDateTime01(mtchr.group(4), mtchr.group(3), true);
					SsrTicketingTimeLimitDto tktl = new SsrTicketingTimeLimitDto();

					tktl.setActionOrStatusCode(mtchr.group(1));
					tktl.setCodeSSR(codeSSR);
					tktl.setCarrierCode(carrierCode);
					tktl.setTicketingTime(ticketingTime);
					ssrDTOs.add(tktl);
				}

				dtoFound = true;

			} else if (codeSSR.equals(TypeBMessageConstants.SSRCodes.OTHS)) {
				// above ssr will be ignored
				dtoFound = true;
			} else {
				ccType = stringElement.substring(matcher.start(3));
				if (ccType != null) {
					ccPattern = Pattern.compile(MesageParserConstants.ElementRegularExpressions.SSR_CC_VALUE);
					ccMatcher = ccPattern.matcher(ccType);

					if (ccType.matches("CH [A-Z ]+")) {
						cardDetailDTO.setCardHolderName(ccType.substring(3));
						dtoFound = true;
					} else if (ccType.matches("CSC[0-9]+")) {
						cardDetailDTO.setPinNumber(ccType.substring(3));
						dtoFound = true;
					} else if (ccType.matches("OSAG[0-9A-Z]{8,10}")) { // agencyno length is assumed.
						SSROSAGDTO osagDTO = new SSROSAGDTO();
						osagDTO.setCarrierCode(carrierCode);
						osagDTO.setCodeSSR(codeSSR);
						osagDTO.setFullElement(matcher.group());
						osagDTO.setSsrValue(ccType);
						osagDTO.setOsagCode(ccType.substring(4));
						ssrDTOs.add(osagDTO);
						dtoFound = true;
					}
				}
				if (ccMatcher.matches()) {

					cardDetailDTO.setCardNo(ccMatcher.toMatchResult().group(3));
					cardDetailDTO.setCardType(ccMatcher.toMatchResult().group(2));
					cardDetailDTO.setExpiryMonth(ccMatcher.toMatchResult().group(5));
					cardDetailDTO.setExpiryYear(ccMatcher.toMatchResult().group(6));
					cardDetailDTO.setCodeSSR(codeSSR);
					cardDetailDTO.setSsrValue(ccType);
					cardDetailDTO.setFullElement(stringElement);
					dtoFound = true;
				}
			}


			if (!dtoFound) {
				ossrDTO.setSsrValue(stringElement.substring(matcher.start(3)));
				ossrDTO.setFullElement(matcher.group());
				ssrDTOs.add(ossrDTO);
			}

		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_PCT)) {
			SSREmergencyContactDetailDTO emContactDetailDTO = new SSREmergencyContactDetailDTO();
			emContactDetailDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.PCTC);
			emContactDetailDTO.setFullElement(matcher.toMatchResult().group());
			emContactDetailDTO.setCarrierCode(matcher.toMatchResult().group(1));
			emContactDetailDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
			emContactDetailDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
			emContactDetailDTO.setEmContactName(matcher.toMatchResult().group(3));
			emContactDetailDTO.setEmContactNumberwithCountry(matcher.toMatchResult().group(5));
			emContactDetailDTO.setFreeText(matcher.toMatchResult().group(8));

			String nameString = matcher.toMatchResult().group(6);
			if (nameString != null && nameString.startsWith(HYPHEN)) {
				List ssrNameDTOList = setNameElementDTOs(nameString.substring(1));
				if (!ssrNameDTOList.isEmpty()) {
					NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);
					emContactDetailDTO.setPassengerFirstName(nameDTO.getFirstName());
					emContactDetailDTO.setPassengerLastName(nameDTO.getLastName());
					emContactDetailDTO.setPassengerTitle(nameDTO.getPaxTitle());
				}
			}

			if (matcher.toMatchResult().group(2).contains(DOUBLE_FOWARD_SLASH)) {
				emContactDetailDTO.setRefused(true);
			}
			emergencyContactDetailDTOs.add(emContactDetailDTO);

		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_DCS1)) {
			setDocsInfoElement(matcher);
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_DCS_CONT)) {

			Pattern ssrContinuationElementPattern = null;
			Matcher ssrContinuationElementMatcher = null;
			String childelement = matcher.group();

			String carrierCode = null;
			String codeSSR = TypeBMessageConstants.SSRCodes.DOCS;
			carrierCode = (matcher.toMatchResult().group(1));
			if (carrierCode.startsWith("/")) {
				carrierCode = carrierCode.substring(1);
			}
			int docSSRPosition = -1;

			for (int i = docsDTOs.size() - 1; i >= 0; i--) {
				SSRDocsDTO docsDTO = (SSRDocsDTO) docsDTOs.get(i);

				if (docsDTO.getCodeSSR().equals(codeSSR) && docsDTO.getCarrierCode().equals(carrierCode)) {
					docSSRPosition = i;
					break;
				}
			}
			if (docSSRPosition != -1) {
				// parent element extract and remove, use the full element for further implementation.
				String ssrParentElement = ((SSRDocsDTO) docsDTOs.get(docSSRPosition)).getFullElement();
				docsDTOs.remove(docSSRPosition);
				ssrParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
				ssrContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.SSR_DCS1));
				ssrContinuationElementMatcher = ssrContinuationElementPattern.matcher(ssrParentElement);
				if (ssrContinuationElementMatcher.matches()) {
					setDocsInfoElement(ssrContinuationElementMatcher);
				}
			}
		}
		
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_DOCO)) {
			setDocoInfoElement(matcher);
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_DOCO_CONT)) {

			Pattern ssrContinuationElementPattern = null;
			Matcher ssrContinuationElementMatcher = null;
			String childelement = matcher.group();

			String carrierCode = null;
			String codeSSR = TypeBMessageConstants.SSRCodes.DOCO;
			carrierCode = (matcher.toMatchResult().group(1));
			if (carrierCode.startsWith("/")) {
				carrierCode = carrierCode.substring(1);
			}
			int docSSRPosition = -1;

			for (int i = docoDTOs.size() - 1; i >= 0; i--) {
				SSRDocoDTO docoDTO = (SSRDocoDTO) docoDTOs.get(i);

				if (docoDTO.getCodeSSR().equals(codeSSR) && docoDTO.getCarrierCode().equals(carrierCode)) {
					docSSRPosition = i;
					break;
				}
			}
			if (docSSRPosition != -1) {
				// parent element extract and remove, use the full element for further implementation.
				String ssrParentElement = ((SSRDocoDTO) docoDTOs.get(docSSRPosition)).getFullElement();
				docoDTOs.remove(docSSRPosition);
				ssrParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
				ssrContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.SSR_DOCO));
				ssrContinuationElementMatcher = ssrContinuationElementPattern.matcher(ssrParentElement);
				if (ssrContinuationElementMatcher.matches()) {
					setDocoInfoElement(ssrContinuationElementMatcher);
				}
			}
		}
		
		/** This is not using */
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_DCS2)) {

			try {
				SSRDocsDTO ssrDocsDTO = new SSRDocsDTO();
				ssrDocsDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.DOCS);
				ssrDocsDTO.setFullElement(matcher.group());
				ssrDocsDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
				ssrDocsDTO.setCarrierCode(matcher.toMatchResult().group(1));
				ssrDocsDTO.setActionOrStatusCode(matcher.toMatchResult().group(2));
				ssrDocsDTO.setNoOfPax(Integer.parseInt(matcher.toMatchResult().group(3)));
				ssrDocsDTO.setDocType(matcher.toMatchResult().group(7));
				ssrDocsDTO.setDocCountryCode(matcher.toMatchResult().group(9));
				ssrDocsDTO.setDocNumber(matcher.toMatchResult().group(11));
				ssrDocsDTO.setPassengerNationality(matcher.toMatchResult().group(13));
				String dateFormatString = "ddMMMyy";
				DateFormat dateFormat = new SimpleDateFormat(dateFormatString);
				String dobString = matcher.toMatchResult().group(15);
				if (dobString != null && !dobString.equals("")) {
					Date dob = dateFormat.parse(dobString);
					ssrDocsDTO.setPassengerDateOfBirth(dob);
				}
				ssrDocsDTO.setPassengerGender(matcher.toMatchResult().group(17));
				String expDateString = matcher.toMatchResult().group(19);
				if (expDateString != null && !expDateString.equals("")) {
					Date expiryDate = dateFormat.parse(expDateString);
					ssrDocsDTO.setDocExpiryDate(expiryDate);
				}
				ssrDocsDTO.setDocFamilyName(matcher.toMatchResult().group(21));
				ssrDocsDTO.setDocGivenName(matcher.toMatchResult().group(23));
				// document second given name 23
				// is multy passanger passport ? h 25

				String possiblePNRString = matcher.toMatchResult().group(29);
				if (possiblePNRString != null) {

					List ssrNameDTOList = setNameElementDTOs(possiblePNRString);
					ssrDocsDTO.setPnrNameDTOs(ssrNameDTOList);
				}
				docsDTOs.add(ssrDocsDTO);

			} catch (Exception e) {
				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + matcher.toMatchResult().group());
				}
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");
			}
		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_LANG)) {

			SSRDetailDTO detailDTO = new SSRDetailDTO();
			detailDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.LANG);
			detailDTO.setActionOrStatusCode(TypeBMessageConstants.StatusCodes.HK);
			detailDTO.setCarrierCode(matcher.toMatchResult().group(1));
			detailDTO.setFullElement(matcher.group());
			detailDTO.setSsrValue(matcher.group().substring(10 + matcher.toMatchResult().group(1).length()).trim());
			detailDTO.setFreeText(matcher.toMatchResult().group(7));
			String ssrSegment = matcher.toMatchResult().group(4);
			String nopax = matcher.toMatchResult().group(2);
			if (nopax != null) {
				detailDTO.setNoOfPax(Integer.parseInt(nopax));
			} else {
				detailDTO.setNoOfPax(1);
			}
			if (ssrSegment != null && !ssrSegment.equals("")) {

				subElementPattern = Pattern.compile((String) subElementReagexHashMap
						.get(MesageParserConstants.SubElementTypes.SSR_SEGMENT));
				subElementMatcher = subElementPattern.matcher(ssrSegment);

				if (subElementMatcher.matches()) {
					SegmentDTO ssrLangSegmentDTO = new SegmentDTO();
					ssrLangSegmentDTO.setDepartureStation(subElementMatcher.toMatchResult().group(1));
					ssrLangSegmentDTO.setDestinationStation(subElementMatcher.toMatchResult().group(2));
					ssrLangSegmentDTO.setFlightNumber(subElementMatcher.toMatchResult().group(3));
					ssrLangSegmentDTO.setBookingCode(subElementMatcher.toMatchResult().group(4));

					Date d = CalendarUtil.getSegmentDate(subElementMatcher.toMatchResult().group(5).trim());
					if (d == null) {
						System.out.println("Unknown element: " + matcher.toMatchResult().group());
						sStatus = "Unknown element: " + matcher.toMatchResult().group();
						throw new ModuleException("gdsmessageparser.element.unknown");
					} else {
						ssrLangSegmentDTO.setDepartureDate(d);
					}
					detailDTO.setSegmentDTO(ssrLangSegmentDTO);

				}
			}
			String nameString = matcher.toMatchResult().group(5);
			if (nameString != null && nameString.startsWith(HYPHEN)) {
				List ssrNameDTOList = setNameElementDTOs(nameString.substring(1));
				detailDTO.setSsrNameDTOs(ssrNameDTOList);
			}

			ssrDTOs.add(detailDTO);

		}
		/*
		 * Groups for SSR Element group1 = CodeSSR group2 = airLineIndentifier group3 = actionCode group4 = noofPaxes
		 * group5 = segment group6 = UNUSED group7 = nameString group8 = UNUSED group9 = freeText
		 */
		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_BEGIN)) {

			String codeSSR = matcher.toMatchResult().group(1);

			if (codeSSR.trim().equals(TypeBMessageConstants.SSRCodes.UMNR)) {
				setMinorDTO(matcher);
			} else if (codeSSR.trim().equals(TypeBMessageConstants.SSRCodes.INFT)) {
				setInfantElementDTO(matcher);
			} else {
				SSRDetailDTO ossrDTO = setSSRElemntDTO(matcher);
				ssrDTOs.add(ossrDTO);
			}

		}

		/*
		 * Groups for SS2 Element group1 = CodeSSR group2 = airLineIndentifier GROUP3 = UNUSED (///) group4 = nameString
		 * group5 = UNUSED group6 = freeText
		 */

		if (sKey.equals(MesageParserConstants.GDSMessageElements.SSR_CONT)) {

			Pattern ssrContinuationElementPattern = null;
			Matcher ssrContinuationElementMatcher = null;
			String childelement = matcher.group();
			String carrierCode = null;
			String codeSSR = (matcher.toMatchResult().group(1));
			carrierCode = (matcher.toMatchResult().group(2));

			if (carrierCode.startsWith("/")) {
				carrierCode = carrierCode.substring(1);
			}

			if (codeSSR.trim().equals(TypeBMessageConstants.SSRCodes.INFT)) {

				// assumed that continuous information always appear in order

				String ssrParentElement = ((SSRInfantDTO) infantDTOs.get(infantDTOs.size() - 1)).getFullElement();
				infantDTOs.remove(infantDTOs.size() - 1);
				ssrParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
				ssrContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.SSR_BEGIN));
				ssrContinuationElementMatcher = ssrContinuationElementPattern.matcher(ssrParentElement);
				if (ssrContinuationElementMatcher.matches()) {

					setInfantElementDTO(ssrContinuationElementMatcher);
				}
			} else if (codeSSR.trim().equals(TypeBMessageConstants.SSRCodes.CHLD)) {

					// assumed that continuous information always appear in order

					String ssrChildElement = ((SSRChildDTO) ssrDTOs.get(ssrDTOs.size() - 1)).getFullElement();
					ssrDTOs.remove(ssrDTOs.size() - 1);
					ssrChildElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
					ssrContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
							.get(MesageParserConstants.GDSMessageElements.SSR_CHLD));
					ssrContinuationElementMatcher = ssrContinuationElementPattern.matcher(ssrChildElement);
					if (ssrContinuationElementMatcher.matches()) {
						setChildElementDTO(ssrContinuationElementMatcher);
					}

			} else {
				int ssrPosition = -1;
				for (int i = ssrDTOs.size() - 1; i >= 0; i--) {
					SSRDetailDTO specialServiceRequestDTO = (SSRDetailDTO) ssrDTOs.get(i);

					if (specialServiceRequestDTO.getCodeSSR().equals(codeSSR)
							&& specialServiceRequestDTO.getCarrierCode().equals(carrierCode)) {
						ssrPosition = i;
						break;
					}
				}
				if (ssrPosition != -1) {

					// parent element extract and remove, use the full element
					// for further implementation.
					String ssrParentElement = ((SSRDetailDTO) ssrDTOs.get(ssrPosition)).getFullElement();
					ssrDTOs.remove(ssrPosition);
					ssrParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
					ssrContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
							.get(MesageParserConstants.GDSMessageElements.SSR_BEGIN));
					ssrContinuationElementMatcher = ssrContinuationElementPattern.matcher(ssrParentElement);
					if (ssrContinuationElementMatcher.matches()) {

						SSRDetailDTO ossrDTO = setSSRElemntDTO(ssrContinuationElementMatcher);
						ssrDTOs.add(ossrDTO);
					}
				}
			}
		}

		/*
		 * Groups for OS1 Element group1 = ActionId group1 = codeOSI group1 = UNUSED group1 = ServiceInfo
		 */
		if (sKey.equals(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_INFO_BEGIN)) {

			try {

				Pattern tcpPattern = null;
				Matcher tcpMatcher = null;
				tcpPattern = Pattern.compile(MesageParserConstants.ElementRegularExpressions.OSI_TCP);
				tcpMatcher = tcpPattern.matcher(matcher.group());

				if (tcpMatcher.matches()) {

					if (nameDTOs != null && nameDTOs.size() > 0) {
						String unChangedNames = tcpMatcher.toMatchResult().group(3);

						UnchangedNamesDTO unchangedNamesDTO = new UnchangedNamesDTO();
						if (unChangedNames != null && !unChangedNames.isEmpty()) {

							List<NameDTO> storedNameList = setNameElementDTOs(unChangedNames);
							unchangedNamesDTO.setUnChangedNames(storedNameList);
							// unChangedNameDTOs.add((NameDTO) storedNameList);
						}
						if (tcpMatcher.toMatchResult().group(2) != null && !tcpMatcher.toMatchResult().group(2).isEmpty()) {
							unchangedNamesDTO.setPartyTotal(Integer.parseInt(tcpMatcher.toMatchResult().group(2)));
						}
						unChangedNameDTOs.add(unchangedNamesDTO);
					}
				} else {

					setOsiInfoElemnt(matcher);
				}
			} catch (Exception e) {

				if (log.isDebugEnabled()) {
					log.debug("Unknown element: " + matcher.toMatchResult().group());
				}
				sStatus = "Unknown element: " + matcher.toMatchResult().group();
				throw new ModuleException("gdsmessageparser.element.unknown");

			}

		}

		if (sKey.equals(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_INFO_CONT)) {

			Pattern osiContinuationElementPattern = null;
			Matcher osiContinuationElementMatcher = null;

			String codeOSI = null;
			String actionId = null;
			int osiPosition = -1;

			OtherServiceInfoDTO oOtherServiceInfoDTO = new OtherServiceInfoDTO();
			String childelement = matcher.group();
			actionId = matcher.toMatchResult().group(1);
			if (actionId.startsWith("/")) {
				actionId = actionId.substring(1);
			}
			oOtherServiceInfoDTO.setCarrierCode(actionId);
			codeOSI = matcher.toMatchResult().group(2);

			for (int i = osiDTOs.size() - 1; i >= 0; i--) {

				OtherServiceInfoDTO otherServiceInfoDTO = (OtherServiceInfoDTO) osiDTOs.get(i);
				if (otherServiceInfoDTO.getCarrierCode().equals(actionId)) {
					osiPosition = i;
					break;
				}
			}

			if (osiPosition != -1) {

				// parent element extract and remove, use the full element for
				// further implementation.
				String osiParentElement = ((OtherServiceInfoDTO) osiDTOs.get(osiPosition)).getFullElement();
				osiDTOs.remove(osiPosition);
				osiParentElement += childelement.substring(childelement.indexOf(ELEMENT_CONTINUATION_MARK) + 3);
				osiContinuationElementPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.OTHER_SERVICE_INFO_BEGIN));
				osiContinuationElementMatcher = osiContinuationElementPattern.matcher(osiParentElement);
				if (osiContinuationElementMatcher.matches()) {

					setOsiInfoElemnt(osiContinuationElementMatcher);
				}
			}

		}
	}

	/**
	 * Method will fill SSR UMNR, unaccompanied minor.
	 * 
	 * @param minorElementMatcher
	 * @throws ModuleException
	 */
	private void setMinorDTO(Matcher minorElementMatcher) throws ModuleException {

		List ssrNameDTOList = null;
		SSRMinorDTO minorDTO = new SSRMinorDTO();
		String fullElement = minorElementMatcher.group();
		minorDTO.setFullElement(fullElement);

		String carrierCode = minorElementMatcher.toMatchResult().group(2);
		if (carrierCode.startsWith("/")) {
			carrierCode = carrierCode.substring(1);
		}
		minorDTO.setCarrierCode(carrierCode);
		minorDTO.setActionOrStatusCode(minorElementMatcher.toMatchResult().group(3));
		minorDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.UMNR);
		minorDTO.setSsrValue(minorElementMatcher.group().substring(10 + carrierCode.length()).trim());

		String possibleNameString = minorElementMatcher.toMatchResult().group(6);
		if (possibleNameString != null && !possibleNameString.isEmpty()) {

			ssrNameDTOList = setNameElementDTOs(possibleNameString.trim().substring(1));
		}

		if (ssrNameDTOList != null && !ssrNameDTOList.isEmpty()) {
			NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);
			minorDTO.setFirstName(nameDTO.getFirstName());
			minorDTO.setLastName(nameDTO.getLastName());
			minorDTO.setTitle(nameDTO.getPaxTitle());
		}

		String freeText = minorElementMatcher.toMatchResult().group(8);
		if (freeText != null) {
			if (freeText.startsWith(POINT)) {
				freeText = freeText.substring(1);
			}
			if (freeText.matches("UM[0-9]{2}|UM [0-9]{2}")) {
				minorDTO.setMinorAge(Integer.parseInt(freeText.substring(freeText.length() - 2)));
			}
		}

		ssrDTOs.add(minorDTO);
	}

	/**
	 * Method will fill OtherServiceInfoDTO DTO's.
	 * 
	 * @param osiMatcher
	 *            matched pattern macher.
	 * @return
	 */
	private void setOsiInfoElemnt(Matcher osiMatcher) {
		Pattern osiNameElementPattern = null;
		String serviceInfo = null;
		String actionId = null;
		String codeOsi = null;

		String fullElemet = osiMatcher.group();

		actionId = osiMatcher.toMatchResult().group(1);
		if (actionId.startsWith("/")) {
			actionId = actionId.substring(1);
		}

		codeOsi = osiMatcher.toMatchResult().group(2);

		String osiValue = osiMatcher.group().substring(osiMatcher.group().indexOf(actionId) + actionId.length()).trim();
		if (codeOsi != null) {
			osiValue = osiValue.substring(codeOsi.length());
		}

		serviceInfo = osiMatcher.toMatchResult().group(4);
		if (codeOsi != null) {
			codeOsi = codeOsi.trim();
		}

		if (codeOsi != null && TypeBMessageConstants.getContactInfoList().contains(codeOsi)) {

			OSIPhoneNoDTO phoneNoDTO = new OSIPhoneNoDTO();
			OSIAddressDTO addressDTO = new OSIAddressDTO();
			OSIEmailDTO emailDTO = new OSIEmailDTO();
			String code;
			String contactInfoType;
			/*
			 * OSI G9 CTCT CMB232323223 A OSI G9 CTCB CMB234234234 B OSI G9 CTCP CMB234234234 C OSI G9 CTCP CMB234234234
			 * M OSI G9 CTCH CMB234234234 H OSI G9 CTCF CMB234234234 F OSI G9 CTCP CMB234234234 HTL OSI G9 CTCP
			 * CMB234234234 E OSI G9 CTCP CMB3232212 OSI G9 CTCP CMB234234234 T
			 */
			contactInfoType = codeOsi.substring(codeOsi.length() - 1);
			if (contactInfoType != null && TypeBMessageConstants.getPhoneContactInfoType().contains(contactInfoType)) {
				Pattern phoneNumberPattern = null;
				Matcher phoneNumberMatcher = null;

				phoneNumberPattern = Pattern.compile(MesageParserConstants.SubElementRegularExpressions.OSI_CONTACT_DETAILS);
				phoneNumberMatcher = phoneNumberPattern.matcher(osiValue);

				if (phoneNumberMatcher.matches()) {
					String phoneNumber = "";
					String tempPhoneNumber = (phoneNumberMatcher.toMatchResult().group(5)).trim();
					String name = phoneNumberMatcher.toMatchResult().group(7);
					if (phoneNumberMatcher.toMatchResult().group(6) == null && name != null && !name.equals("")
							&& tempPhoneNumber.length() > 2 && tempPhoneNumber.charAt(tempPhoneNumber.length()-2)== '-' ) {
						phoneNumber = tempPhoneNumber.substring(0, tempPhoneNumber.length() - 2);
					} else {
						phoneNumber = tempPhoneNumber;
					}
					
					if(phoneNumber.contains(" ")){
						phoneNumber = phoneNumber.replace(" ", "-");
					}

					if (phoneNumber.contains("-")) {
						String tempNumber = "";
						String[] phnNumbers = phoneNumber.split("-");
						if (phnNumbers.length > 3) {
							for (int i = 2; i < phnNumbers.length; i++) {
								tempNumber += phnNumbers[i];
							}
							if (tempNumber != null && !tempNumber.equals("")) {
								phoneNumber = phnNumbers[0] + "-" + phnNumbers[1] + "-" + tempNumber;
							}
						}
					}
					phoneNoDTO.setPhoneNo(phoneNumber);
					phoneNoDTO.setOsiValue(phoneNumber);
					phoneNoDTO.setCodeOSI(codeOsi);
					phoneNoDTO.setCarrierCode(actionId);
					phoneNoDTO.setFullElement(fullElemet);
					osiDTOs.add(phoneNoDTO);
					
					String address = "";
					String addressLineOne = phoneNumberMatcher.toMatchResult().group(8);
					String city = phoneNumberMatcher.toMatchResult().group(1);
					if (name != null && !name.equals("")) {
						address += name + ",";
					}
					if (addressLineOne != null && !addressLineOne.equals("")) {
						address += addressLineOne + ",";
					}
					if (city != null && !city.equals("")) {
						address += city;
					}
					addressDTO.setAddressLineOne(phoneNumberMatcher.toMatchResult().group(8));
					addressDTO.setOsiValue(address);
					addressDTO.setCity(phoneNumberMatcher.toMatchResult().group(1));
					addressDTO.setCodeOSI(TypeBMessageConstants.SSRCodes.CTCA);
					addressDTO.setCarrierCode(actionId);
					addressDTO.setFullElement(fullElemet);
					if(address.length() > 0) {
						osiDTOs.add(addressDTO);
					}
					
				} else {
					addressDTO.setFullElement(fullElemet);
					addressDTO.setAddressLineOne(osiValue);
					addressDTO.setOsiValue(osiValue);
					addressDTO.setCarrierCode(actionId);
					addressDTO.setCodeOSI(TypeBMessageConstants.AddressInfoType.HOME_ADDRESS);
					osiDTOs.add(addressDTO);
				}
			} else if (contactInfoType != null && contactInfoType.equals(TypeBMessageConstants.AddressInfoType.HOME_ADDRESS)) {
				addressDTO.setAddressLineOne(osiValue);
				addressDTO.setFullElement(fullElemet);
				addressDTO.setCarrierCode(actionId);
				addressDTO.setOsiValue(osiValue);
				addressDTO.setCodeOSI(codeOsi);
				osiDTOs.add(addressDTO);

			} else if (contactInfoType != null && contactInfoType.equals(TypeBMessageConstants.EmailInfoType.EMAIL_ADDRESS)) {
				Pattern emailPattern = null;
				Matcher emailMatcher = null;
				if(osiValue != null && osiValue.length() > 0) {
					osiValue = osiValue.toUpperCase();
				}

				emailPattern = Pattern.compile(MesageParserConstants.SubElementRegularExpressions.OSI_CONTACT_DETAILS_EMAIL);
				emailMatcher = emailPattern.matcher(osiValue);

				if (emailMatcher.matches()) {
					if(osiValue != null && osiValue.length() > 0) {
						osiValue = osiValue.toLowerCase();
					}					
					String rawEmail = osiValue;
					String email1 = rawEmail.replace("//", "@");
					String email = email1.replace("..", "_");
					emailDTO.setEmail(email);
					emailDTO.setFullElement(fullElemet);
					emailDTO.setCarrierCode(actionId);
					emailDTO.setOsiValue(email);
					emailDTO.setCodeOSI(codeOsi);
					osiDTOs.add(emailDTO);
				}
			}

		} else {

			OtherServiceInfoDTO oOtherServiceInfoDTO = new OtherServiceInfoDTO();
			oOtherServiceInfoDTO.setFullElement(fullElemet);
			oOtherServiceInfoDTO.setCarrierCode(actionId);
			oOtherServiceInfoDTO.setOsiValue(osiValue);
			oOtherServiceInfoDTO.setCodeOSI(codeOsi);

			if (serviceInfo != null && serviceInfo.lastIndexOf(HYPHEN) > 0) {
				String possibleNameString = serviceInfo.substring(serviceInfo.lastIndexOf(HYPHEN) + 1);
				osiNameElementPattern = Pattern.compile((String) elementRegexHashMap
						.get(MesageParserConstants.GDSMessageElements.NAME));
				if (osiNameElementPattern.matcher(possibleNameString).matches()) {
					try {
						oOtherServiceInfoDTO.setOsiNameDTOs(setNameElementDTOs(possibleNameString));
					} catch (Exception e) {
						System.out.println("Unknown element in OSI: " + possibleNameString);
						if (log.isDebugEnabled()) {
							log.debug("Unknown element in OSI: " + possibleNameString);
						}
					}
					serviceInfo = serviceInfo.substring(0, serviceInfo.lastIndexOf(HYPHEN));
				}
			}

			oOtherServiceInfoDTO.setServiceInfo(serviceInfo);

			osiDTOs.add(oOtherServiceInfoDTO);
		}

	}

	private void setDocsInfoElement(Matcher docsMatcher) throws ModuleException {

		try {
			SSRDocsDTO ssrDocsDTO = new SSRDocsDTO();
			ssrDocsDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.DOCS);
			ssrDocsDTO.setFullElement(docsMatcher.group());
			ssrDocsDTO.setSsrValue(docsMatcher.group().substring(10 + docsMatcher.toMatchResult().group(1).length()).trim());
			ssrDocsDTO.setCarrierCode(docsMatcher.toMatchResult().group(1));
			ssrDocsDTO.setActionOrStatusCode(docsMatcher.toMatchResult().group(2));
			ssrDocsDTO.setNoOfPax(Integer.parseInt(docsMatcher.toMatchResult().group(3)));
			ssrDocsDTO.setDocType(docsMatcher.toMatchResult().group(5));
			ssrDocsDTO.setDocCountryCode(docsMatcher.toMatchResult().group(7));
			ssrDocsDTO.setDocNumber(docsMatcher.toMatchResult().group(9));
			ssrDocsDTO.setPassengerNationality(docsMatcher.toMatchResult().group(11));
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
			String dobString = docsMatcher.toMatchResult().group(13);
			if (dobString != null && !dobString.equals("")) {
				Date dob = dateFormat.parse(dobString);
				ssrDocsDTO.setPassengerDateOfBirth(dob);
			}
			ssrDocsDTO.setPassengerGender(docsMatcher.toMatchResult().group(15));
			String expDateString = docsMatcher.toMatchResult().group(17);
			if (expDateString != null && !expDateString.equals("")) {
				Date expiryDate = dateFormat.parse(expDateString);
				ssrDocsDTO.setDocExpiryDate(expiryDate);
			}
			ssrDocsDTO.setDocFamilyName(docsMatcher.toMatchResult().group(19));
			ssrDocsDTO.setDocGivenName(docsMatcher.toMatchResult().group(21));
			// document second given name 23
			// is multy passanger passport ? h 25

			String possiblePNRString = docsMatcher.toMatchResult().group(27);
			if (possiblePNRString != null) {

				String regEx = "(([1-9]?[0-9]*)[A-Z ]+/[A-Z ]+)+";
				Pattern pattern = Pattern.compile(regEx);
				Matcher matcher = pattern.matcher(possiblePNRString);

				if (matcher.matches()) {
					List ssrNameDTOList = setNameElementDTOs(possiblePNRString);
					ssrDocsDTO.setPnrNameDTOs(ssrNameDTOList);
				}
			} else {
				ssrDocsDTO.setPnrNameDTOs(setNameElementDTOs(ssrDocsDTO.getDocGivenName(), ssrDocsDTO.getDocFamilyName()));
			}
			docsDTOs.add(ssrDocsDTO);

		} catch (Exception e) {

			System.out.println("Unknown element: " + docsMatcher.toMatchResult().group());
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + docsMatcher.toMatchResult().group());
			}
			sStatus = "Unknown element: " + docsMatcher.toMatchResult().group();
			throw new ModuleException("gdsmessageparser.element.unknown");
		}
	}

	private void setDocoInfoElement(Matcher docoMatcher) throws ModuleException {

		try {
			SSRDocoDTO ssrDocoDTO = new SSRDocoDTO();
			ssrDocoDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.DOCO);
			ssrDocoDTO.setFullElement(docoMatcher.group());
			ssrDocoDTO.setSsrValue(docoMatcher.group().substring(10 + docoMatcher.toMatchResult().group(1).length()).trim());
			ssrDocoDTO.setCarrierCode(docoMatcher.toMatchResult().group(1));
			ssrDocoDTO.setActionOrStatusCode(docoMatcher.toMatchResult().group(2));
			ssrDocoDTO.setNoOfPax(Integer.parseInt(docoMatcher.toMatchResult().group(3)));
			ssrDocoDTO.setPlaceOfBirth(docoMatcher.group(5));
			ssrDocoDTO.setTravelDocType(docoMatcher.toMatchResult().group(7));
			ssrDocoDTO.setVisaDocNumber(docoMatcher.toMatchResult().group(9));
			ssrDocoDTO.setVisaDocPlaceOfIssue(docoMatcher.group(11));
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
			String issueDateString = docoMatcher.toMatchResult().group(13);
			if (issueDateString != null && !issueDateString.equals("")) {
				Date issueDate = dateFormat.parse(issueDateString);
				ssrDocoDTO.setVisaDocIssueDate(issueDate);
			}
			ssrDocoDTO.setVisaApplicableCountry(docoMatcher.toMatchResult().group(15));
			
			String infantVal = docoMatcher.toMatchResult().group(17);
			if(infantVal != null){
				ssrDocoDTO.setInfant(infantVal.equals("I"));
			}else{
				ssrDocoDTO.setInfant(false);
			}
			

			String nameString = docoMatcher.toMatchResult().group(19);
			if (nameString != null) {

				String regEx = "(([1-9]?[0-9]*)[A-Z ]+/[A-Z ]+)+";
				Pattern pattern = Pattern.compile(regEx);
				Matcher matcher = pattern.matcher(nameString);

				if (matcher.matches()) {
					List ssrNameDTOList = setNameElementDTOs(nameString);
					ssrDocoDTO.setPnrNameDTOs(ssrNameDTOList);
				}
			}
			docoDTOs.add(ssrDocoDTO);

		} catch (Exception e) {

			System.out.println("Unknown element: " + docoMatcher.toMatchResult().group());
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + docoMatcher.toMatchResult().group());
			}
			sStatus = "Unknown element: " + docoMatcher.toMatchResult().group();
			throw new ModuleException("gdsmessageparser.element.unknown");
		}
	}
	
	/**
	 * Method will fill the SpecialServiceRequestDTO from the match result.
	 * 
	 * @param ssrElementmatcher
	 * @return SpecialServiceRequestDTO filled object.
	 * @throws ModuleException
	 */
	private SSRDetailDTO setSSRElemntDTO(Matcher ssrElementmatcher) throws ModuleException {
		String ssrSegment = null;
		String fullElement = null;
		String noOfPax = null;
		String codeSSR = null;
		String carrierCode = null;

		codeSSR = ssrElementmatcher.toMatchResult().group(1);
		SSRDetailDTO ossrDTO = new SSRDetailDTO();
		ossrDTO.setFullElement(ssrElementmatcher.group());

		ossrDTO.setCodeSSR(codeSSR);
		carrierCode = ssrElementmatcher.toMatchResult().group(2);
		if (carrierCode.startsWith("/")) {
			carrierCode = carrierCode.substring(1);
		}
		ossrDTO.setCarrierCode(carrierCode);
		ossrDTO.setSsrValue(ssrElementmatcher.group().substring(10 + carrierCode.length()).trim());
		ossrDTO.setActionOrStatusCode(ssrElementmatcher.toMatchResult().group(3));

		noOfPax = ssrElementmatcher.toMatchResult().group(4);
		if (noOfPax != null) {
			ossrDTO.setNoOfPax(Integer.parseInt(ssrElementmatcher.toMatchResult().group(4)));
		}
		ssrSegment = ssrElementmatcher.toMatchResult().group(5);
		
		if (ssrSegment != null) {

			subElementPattern = Pattern.compile((String) subElementReagexHashMap
					.get(MesageParserConstants.SubElementTypes.SSR_SEGMENT));
			subElementMatcher = subElementPattern.matcher(ssrSegment);

			if (subElementMatcher.matches()) {
				SegmentDTO ssrSegmentDTO = new SegmentDTO();
				ssrSegmentDTO.setDepartureStation(subElementMatcher.toMatchResult().group(1));
				ssrSegmentDTO.setDestinationStation(subElementMatcher.toMatchResult().group(2));
				ssrSegmentDTO.setFlightNumber(subElementMatcher.toMatchResult().group(4));
				ssrSegmentDTO.setBookingCode(subElementMatcher.toMatchResult().group(5));

				Date d = CalendarUtil.getSegmentDate(subElementMatcher.toMatchResult().group(6).trim());
				if (d == null) {
					sStatus = "Unknown element: " + ssrElementmatcher.toMatchResult().group();
					throw new ModuleException("gdsmessageparser.element.unknown");
				} else {
					ssrSegmentDTO.setDepartureDate(d);
				}
				ossrDTO.setSegmentDTO(ssrSegmentDTO);

			}

		}

		String nameString = ssrElementmatcher.toMatchResult().group(6);
		if (nameString != null && nameString.startsWith(HYPHEN) && nameString.contains(SINGLE_FOWARD_SLASH)) {
			List ssrNameDTOList = setNameElementDTOs(nameString.substring(1));
			ossrDTO.setSsrNameDTOs(ssrNameDTOList);
		}
		String freeText = ssrElementmatcher.toMatchResult().group(8);
		if (freeText != null) {
			if (freeText.startsWith(POINT)) {
				ossrDTO.setFreeText(freeText.substring(1));
			} else {
				ossrDTO.setFreeText(freeText);
			}
		}

		return ossrDTO;
	}

	/**
	 * Method which process the SSR INFT and fill the InfantDTO
	 * 
	 * @param ssrElementmatcher
	 *            element pattern macher.
	 * @throws ModuleException
	 */
	// TODO cover all possibilites eg : occuping seats + parents name+ age (dob
	// and months
	private void setInfantElementDTO(Matcher infantElementMatcher) throws ModuleException {
		// TODO complete this method
		SSRInfantDTO infantDTO = new SSRInfantDTO();
		String fullElement = infantElementMatcher.group();
		infantDTO.setFullElement(fullElement);
		boolean isSeatRequired = false;
		infantDTO.setSeatRequired(false);
		infantDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.INFT);

		if (fullElement.trim().endsWith(OCCUPY_SEAT_POSTFIX) || fullElement.trim().endsWith(OCCUPY_SEAT_POSTFIX1)) {
			isSeatRequired = true;
			infantDTO.setSeatRequired(true);
		}
		String carrierCode = infantElementMatcher.toMatchResult().group(2);
		if (carrierCode.startsWith("/")) {
			carrierCode = carrierCode.substring(1);
		}
		infantDTO.setCarrierCode(carrierCode);
		infantDTO.setActionOrStatusCode(infantElementMatcher.toMatchResult().group(3));
		String guardiantNameString = infantElementMatcher.toMatchResult().group(6);
		if (guardiantNameString != null && guardiantNameString.startsWith(HYPHEN)
				&& guardiantNameString.contains(SINGLE_FOWARD_SLASH)) {
			List ssrNameDTOList = setNameElementDTOs(guardiantNameString.substring(1));
			// usuaally one guardiant name appear here
			NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);
			if (isSeatRequired) {
				infantDTO.setInfantFirstName(nameDTO.getFirstName());
				infantDTO.setInfantLastName(nameDTO.getLastName());
				infantDTO.setInfantTitle(nameDTO.getPaxTitle());
			} else {
				infantDTO.setGuardianFirstName(nameDTO.getFirstName());
				infantDTO.setGuardianLastName(nameDTO.getLastName());
				infantDTO.setGuardianTitle(nameDTO.getPaxTitle());
			}

		}

		String inftName = infantElementMatcher.toMatchResult().group(7);
		if (inftName != null) {
			if (inftName.startsWith(POINT)) {
				inftName = inftName.substring(1);
			}
			List ssrNameDTOList = setNameElementDTOsInfant(inftName);
			NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);

			infantDTO.setInfantFirstName(nameDTO.getFirstName());
			infantDTO.setInfantLastName(nameDTO.getLastName());
			infantDTO.setInfantTitle(nameDTO.getPaxTitle());
		}


		String freeText = infantElementMatcher.toMatchResult().group(8);

		if (freeText != null && (freeText.startsWith(POINT) || freeText.startsWith(HYPHEN))) {
			freeText = freeText.substring(1);
		}
		try {

			if (freeText != null && !isSeatRequired) {
				/* ::::::: NEW TEXT */

				Pattern inftFreeTextPattern = Pattern
						.compile("([A-Z ]+/?[A-Z ]*)( )?([0-9]{1,2}MTHS|[0-9]{2}[A-Z]{3}[0-9]{2})?(/[A-Z]*)?");
				Matcher inftFreeTextMatcher = inftFreeTextPattern.matcher(freeText);

				if (inftFreeTextMatcher.matches()) {

					String infantName = inftFreeTextMatcher.toMatchResult().group(1).trim();
					if (infantName.contains("/")) {
						List ssrNameDTOList = setNameElementDTOsInfant(infantName);
						NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);

						infantDTO.setInfantFirstName(nameDTO.getFirstName());
						infantDTO.setInfantLastName(nameDTO.getLastName());
						infantDTO.setInfantTitle(nameDTO.getPaxTitle());

					} else {
						infantDTO.setInfantFirstName(infantName);
					}

					String infantAge = inftFreeTextMatcher.toMatchResult().group(3);
					if (infantAge != null) {
						if (infantAge.endsWith("MTHS")) {
							infantDTO.setInfantAge(Integer.parseInt(infantAge.substring(0, 2)));
						} else {

							String dobFormatString = "ddMMMyy";
							DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
							Date dob = dobFormat.parse(infantAge);
							Date date = new Date();
							long today = date.getTime();
							long bday = dob.getTime();
							infantDTO.setInfantAge((int) (((today - bday) / (24 * 60 * 60 * 1000)) / 30));
							infantDTO.setInfantDateofBirth(dob);

						}
					}
					String inftSSRfreeText = inftFreeTextMatcher.toMatchResult().group(4);
					if (inftSSRfreeText != null) {
						infantDTO.setFreeText(inftSSRfreeText.substring(1));

					}
				}

			} else {
				// infnt occupy a seat
				if (freeText != null && freeText.contains(MTHS)) {
					infantDTO.setInfantAge(Integer.parseInt(freeText.substring(0, 2)));
				}
			}
			if (freeText != null && freeText.length() >= 7) {
				String infantAage = freeText.substring(0, 7);
				if (infantAage != null && infantAage.matches("([0-9]{2}[A-Z]{3}[0-9]{2})")) {

					String dobFormatString = "ddMMMyy";
					DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
					Date dob = dobFormat.parse(infantAage);
					Date date = new Date();
					long today = date.getTime();
					long bday = dob.getTime();
					infantDTO.setInfantAge((int) (((today - bday) / (24 * 60 * 60 * 1000)) / 30));
					infantDTO.setInfantDateofBirth(dob);
				}

			}

		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + infantElementMatcher.group());
			}
			sStatus = "Unknown element: " + infantElementMatcher.group();
			throw new ModuleException("gdsmessageparser.element.unknown");
		}

		// segment information in ssr inft element
		String infantSegment = infantElementMatcher.toMatchResult().group(5);
		if (infantSegment != null) {

			subElementPattern = Pattern.compile((String) subElementReagexHashMap
					.get(MesageParserConstants.SubElementTypes.SSR_SEGMENT));
			subElementMatcher = subElementPattern.matcher(infantSegment);

			if (subElementMatcher.matches()) {
				SegmentDTO infantSegmentDTO = new SegmentDTO();
				infantSegmentDTO.setDepartureStation(subElementMatcher.toMatchResult().group(1));
				infantSegmentDTO.setDestinationStation(subElementMatcher.toMatchResult().group(2));
				infantSegmentDTO.setFlightNumber(subElementMatcher.toMatchResult().group(4));
				infantSegmentDTO.setBookingCode(subElementMatcher.toMatchResult().group(5));

				Date d = CalendarUtil.getSegmentDate(subElementMatcher.toMatchResult().group(6).trim());
				if (d == null) {
					if (log.isDebugEnabled()) {
						log.debug("Unknown element: " + infantElementMatcher.toMatchResult().group());
					}
					sStatus = "Unknown element: " + infantElementMatcher.toMatchResult().group();
					throw new ModuleException("gdsmessageparser.element.unknown");

				} else {

					infantSegmentDTO.setDepartureDate(d);
				}
				infantDTO.setSegmentDTO(infantSegmentDTO);

			}

		}

		infantDTOs.add(infantDTO);

		List<NameDTO> infants = new ArrayList<NameDTO>();
		for (NameDTO nameDTO : nameDTOs) {
			if (infantDTO.getInfantFirstName() != null && infantDTO.getInfantLastName() != null
					&& infantDTO.getInfantFirstName().equals(nameDTO.getFirstName())
					&& infantDTO.getInfantLastName().equals(nameDTO.getLastName())) {
				infants.add(nameDTO);
			}
		}
		nameDTOs.removeAll(infants);
	}

	/**
	 * Method will first check for the existence of possible group names else set the normal name element eg : 25AMEX
	 * this means its group group magnitude always between 10-99 except in a name change. when name change occurs group
	 * magnitude can be breakdown to single digit.
	 * 
	 * @param sNamesString
	 * @return nameList
	 * @throws ModuleException
	 */
	private List<NameDTO> nameElementcheck(String sNamesString) throws ModuleException {

		NameDTO nameDTO;
		List<NameDTO> nameDTOList = new ArrayList<NameDTO>();
		String groupName;

		try {
			if (sNamesString.matches("([1-9][0-9][A-Z]+)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1];
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);

			} else if (sNamesString.matches("([1-9][0-9][A-Z]+)( GRP)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1].replace(" GRP", "");
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);
				bookingRequestDTO.setGroupBooking(true);

			} else if (sNamesString.matches("([1-9][0-9][A-Z]+/[A-Z]+)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1].substring(0, sNamesString.indexOf("/") - 2);
				nameDTO.setFirstName(groupName);
				nameDTO.setLastName((sNamesString.split("[0-9]+"))[1].substring(sNamesString.indexOf("/") - 1));
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
				nameDTOList.add(nameDTO);

			} else if (sNamesString.matches("([1-9][0-9][A-Z]+/[A-Z]+)( GRP)")) {

				sNamesString = sNamesString.replace(" GRP", "");
				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1].substring(0, sNamesString.indexOf("/"));
				nameDTO.setFirstName(groupName);
				nameDTO.setLastName((sNamesString.split("[0-9]+"))[1].substring(sNamesString.indexOf("/")));
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
				nameDTOList.add(nameDTO);
				bookingRequestDTO.setGroupBooking(true);

			} else if (sNamesString.matches("([1-9][0-9][A-Z]+(/[A-Z]+)?) (([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+")) {
				// 21ROTARY 1IOANNOU/AMR
				String[] nameGropus = sNamesString.split(SINGLE_SPACE);
				if (!nameGropus[0].contains("/")) {
					nameDTO = new NameDTO();
					groupName = nameGropus[0].substring(2);
					nameDTO.setGroupMagnitude(Integer.parseInt(nameGropus[0].substring(0, 2)));
					nameDTO.setFirstName(groupName);
					nameDTOList.add(nameDTO);
				} else {

					nameDTO = new NameDTO();
					groupName = sNamesString.substring(2, sNamesString.indexOf("/"));
					nameDTO.setFirstName(groupName);
					nameDTO.setLastName((sNamesString.split("[0-9]+"))[1].substring(sNamesString.indexOf("/")));
					nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
					nameDTOList.add(nameDTO);
				}
				for (int iPos = 1; iPos < nameGropus.length; iPos++) {

					List<NameDTO> nameList = setNameElementDTOs(nameGropus[iPos]);
					nameDTOList.addAll(nameList);
				}

			} else if (sNamesString.matches("([1-9][0-9][A-Z]+/[A-Z]+)+ ([1-9][0-9][A-Z]+)+")) {
				// 1DODGE/HJH 26WATTSTRAVEL //TODO chaeck this type possible , if not, remove
				String[] nameGropus = sNamesString.trim().split(SINGLE_SPACE);
				if (!nameGropus[nameGropus.length - 1].contains("/")) {
					nameDTO = new NameDTO();
					groupName = nameGropus[nameGropus.length - 1].substring(2);
					nameDTO.setGroupMagnitude(Integer.parseInt(nameGropus[nameGropus.length - 1].substring(0, 2)));
					nameDTO.setFirstName(groupName);
					nameDTOList.add(nameDTO);
				} else {

					nameDTO = new NameDTO();
					groupName = sNamesString.substring(2, sNamesString.indexOf("/"));
					nameDTO.setFirstName(groupName);
					nameDTO.setLastName((sNamesString.split("[0-9]+"))[1].substring(sNamesString.indexOf("/")));
					nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
					nameDTOList.add(nameDTO);
				}

				for (int iPos = 0; iPos < nameGropus.length - 1; iPos++) {

					List<NameDTO> nameList = setNameElementDTOs(nameGropus[iPos]);
					nameDTOList.addAll(nameList);
				}

			} else if (sNamesString
					.matches("([1-9][0-9][A-Z]+(/[A-Z]+)?) (GRP) (([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+")) {

				sNamesString = sNamesString.replace(" GRP ", " ");
				bookingRequestDTO.setGroupBooking(true);
				String[] nameGropus = sNamesString.split(SINGLE_SPACE);

				if (!nameGropus[0].contains("/")) {
					nameDTO = new NameDTO();
					// groupName = (sNamesString.split("[0-9]+"))[1];
					groupName = nameGropus[0].substring(2);
					nameDTO.setGroupMagnitude(Integer.parseInt(nameGropus[0].substring(0, 2)));
					nameDTO.setFirstName(groupName);
					nameDTOList.add(nameDTO);
				} else {

					nameDTO = new NameDTO();
					groupName = sNamesString.substring(2, sNamesString.indexOf("/"));
					nameDTO.setFirstName(groupName);
					nameDTO.setLastName((sNamesString.split("[0-9]+"))[1].substring(sNamesString.indexOf("/")));
					nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 2)));
					nameDTOList.add(nameDTO);
				}
				for (int iPos = 1; iPos < nameGropus.length; iPos++) {

					List<NameDTO> nameList = setNameElementDTOs(nameGropus[iPos]);
					nameDTOList.addAll(nameList);
				}

			} else if (sNamesString.matches("([1-9][A-Z]+)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1];
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 1)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);

			} else if (sNamesString.matches("([1-9][A-Z]+)( GRP)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1].replace(" GRP", "");
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 1)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);
				bookingRequestDTO.setGroupBooking(true);
			} else if (sNamesString.matches("([1-9][A-Z]+) (([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+")) {

				String[] nameGropus = sNamesString.split(SINGLE_SPACE);
				nameDTO = new NameDTO();
				groupName = nameGropus[0].substring(1);
				nameDTO.setGroupMagnitude(Integer.parseInt(nameGropus[0].substring(0, 1)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);

				for (int iPos = 1; iPos < nameGropus.length; iPos++) {

					List<NameDTO> nameList = setNameElementDTOs(nameGropus[iPos]);
					nameDTOList.addAll(nameList);
				}

			} else if (sNamesString.matches("([1-9][A-Z])( GRP) (([1-9]+[0-9]?[A-Z ]+/?[A-Z ]*/?[A-Z/ ]*[ ]?)+)+")) {

				sNamesString = sNamesString.replace(" GRP ", " ");
				String[] nameGropus = sNamesString.split(SINGLE_SPACE);

				nameDTO = new NameDTO();
				groupName = nameGropus[0].substring(1);
				nameDTO.setGroupMagnitude(Integer.parseInt(nameGropus[0].substring(0, 1)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);
				bookingRequestDTO.setGroupBooking(true);
				for (int iPos = 1; iPos < nameGropus.length; iPos++) {

					List<NameDTO> nameList = setNameElementDTOs(nameGropus[iPos]);
					nameDTOList.addAll(nameList);
				}

			} else if (sNamesString.matches("([1-9][A-Z]+)")) {

				nameDTO = new NameDTO();
				groupName = (sNamesString.split("[0-9]+"))[1];
				nameDTO.setGroupMagnitude(Integer.parseInt(sNamesString.substring(0, 1)));
				nameDTO.setFirstName(groupName);
				nameDTOList.add(nameDTO);

			} else {
				nameDTOList = setNameElementDTOs(sNamesString);
			}

		} catch (Exception e) {

			System.out.println("Unknown Name element: " + sNamesString);
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + sNamesString);
			}
			sStatus = "Unknown element: " + sNamesString;
			throw new ModuleException("gdsmessageparser.element.unknown");
		}

		return nameDTOList;

		/*
		 * Groups for Name Array group1 = nameList
		 */
		// sNamesString = sNamesString.replace(" "+MR,MR);
		// sNamesString = sNamesString.replace(" "+MS,MS);
		// sNamesString = sNamesString.replace(" "+MRS,MRS);
		// sNamesString = sNamesString.replace(" "+CHD,CHD);
		// sNamesString = sNamesString.replace(" "+REV,REV);
	}

	/**
	 * Method will extract separateName elements from names string
	 * 
	 * @param namesString
	 *            contain name element string
	 * @return List of NameDTO's
	 * @throws ModuleException
	 */
	private List<NameDTO> setNameElementDTOs(String sNamesString) throws ModuleException {
		List<NameDTO> nameDTOList = new ArrayList<NameDTO>();
		NameDTO nameDTO;
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.MR, TypeBMessageConstants.PaxTitles.MR);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.MS, TypeBMessageConstants.PaxTitles.MS);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.MRS, TypeBMessageConstants.PaxTitles.MRS);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.CHD, TypeBMessageConstants.PaxTitles.CHD);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.REV, TypeBMessageConstants.PaxTitles.REV);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.JR, TypeBMessageConstants.PaxTitles.JR);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.MSTR, TypeBMessageConstants.PaxTitles.MSTR);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.MISS, TypeBMessageConstants.PaxTitles.MISS);
		sNamesString = sNamesString.replace(" " + TypeBMessageConstants.PaxTitles.DR, TypeBMessageConstants.PaxTitles.DR);
		String[] saNameGropus = sNamesString.split(SINGLE_SPACE);
		ArrayList<String> nameGroups = new ArrayList();
		String temporaryName = "";
		int familyID = 0;
		boolean isFamily = false;
		for (int namePos = 0; namePos < saNameGropus.length; namePos++) {
			if (saNameGropus[namePos].length() > 0 && Character.isDigit(saNameGropus[namePos].charAt(0))) {
				if (temporaryName != "") {
					nameGroups.add(temporaryName);
					temporaryName = "";
				}
				temporaryName = saNameGropus[namePos];
			} else if (temporaryName != null || temporaryName != "") {
				temporaryName = temporaryName + " " + saNameGropus[namePos];
			}
			if (namePos == saNameGropus.length - 1 && temporaryName != "") {
				nameGroups.add(temporaryName);
			}
		}
		String[] organizedSaNameGroups = new String[nameGroups.size()];
		organizedSaNameGroups = nameGroups.toArray(organizedSaNameGroups);

		try {
			for (int iPos = 0; iPos < organizedSaNameGroups.length; iPos++) {
				String[] saNamesAndNumber = organizedSaNameGroups[iPos].split("/");
				String sNoofSurNames = (saNamesAndNumber[0].split("[A-Z]+"))[0];
				if (Integer.parseInt(sNoofSurNames) > 1) {
					familyID++;
					isFamily = true;
				} else {
					isFamily = false;
				}

				String sLastName = saNamesAndNumber[0].substring(sNoofSurNames.length());
				for (int iPos1 = 1; iPos1 < saNamesAndNumber.length; iPos1++) {
					nameDTO = new NameDTO();
					// nameDTO = new NameDTO();
					// oPassengerDTO.setFirstName(saNamesAndNumber[iPos1]);
					String element = saNamesAndNumber[iPos1];
					int firstNameLength = element.length();
					if (firstNameLength >= 2) {
						element = element.trim();
						if ((element.substring(element.length() - 2).equals(TypeBMessageConstants.PaxTitles.MR))
								|| (element.substring(element.length() - 2).equals(TypeBMessageConstants.PaxTitles.MS))
								|| (element.substring(element.length() - 2).equals(TypeBMessageConstants.PaxTitles.DR))
								|| (element.substring(element.length() - 2).equals(TypeBMessageConstants.PaxTitles.JR))) {

							nameDTO.setPaxTitle(element.substring(element.length() - 2));
							nameDTO.setFirstName(element.substring(0, element.length() - 2));

						} else if (firstNameLength >= 3
								&& element.substring(element.length() - 3).equals(TypeBMessageConstants.PaxTitles.CHD)) {
							nameDTO.setFirstName(element.substring(0, element.length() - 3));

						} else if (firstNameLength >= 3
								&& ((element.substring(element.length() - 3).equals(TypeBMessageConstants.PaxTitles.MRS)) || (element
										.substring(element.length() - 3).equals(TypeBMessageConstants.PaxTitles.REV)))) {

							nameDTO.setPaxTitle(element.substring(element.length() - 3));
							nameDTO.setFirstName(element.substring(0, element.length() - 3));

						} else if (element.length() > 3
								&& (element.substring(element.length() - 4).equals(TypeBMessageConstants.PaxTitles.MSTR) || element
										.substring(element.length() - 4).equals(TypeBMessageConstants.PaxTitles.MISS))) {
							nameDTO.setPaxTitle(element.substring(element.length() - 4));
							nameDTO.setFirstName(element.substring(0, element.length() - 4));
						} else {
							nameDTO.setFirstName(element);
						}
					} else {
						nameDTO.setFirstName(element);
					}

					// nameDTO.setLastName(sLastName);
					// nameDTOList.add(nameDTO);
					if (isFamily) {
						nameDTO.setFamilyID(familyID);
					} else {
						nameDTO.setFamilyID(0);
					}
					nameDTO.setLastName(sLastName);
					nameDTOList.add(nameDTO);
				}
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + sNamesString);
			}
			sStatus = "Unknown element: " + sNamesString;
			throw new ModuleException("gdsmessageparser.element.unknown");
		}

		if (nameDTOList.isEmpty() || nameDTOList == null) {
			System.out.println("Unknown Name element: " + sNamesString);
			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + sNamesString);
			}
			sStatus = "Unknown element: " + sNamesString;
			throw new ModuleException("gdsmessageparser.element.unknown");
		}
		return nameDTOList;
	}


	private List<NameDTO> setNameElementDTOs(String givenName, String surname) throws ModuleException {
		List<NameDTO> nameDTOList = new ArrayList<NameDTO>();
		NameDTO nameDTO = new NameDTO();
		nameDTO.setFirstName(givenName);
		nameDTO.setLastName(surname);
		nameDTOList.add(nameDTO);

		return nameDTOList;
	}

	private List<NameDTO> setNameElementDTOsInfant(String names) throws ModuleException {
		names = names.replace(" " + TypeBMessageConstants.PaxTitles.MR, TypeBMessageConstants.PaxTitles.MR);
		names = names.replace(" " + TypeBMessageConstants.PaxTitles.MS, TypeBMessageConstants.PaxTitles.MS);
		names = names.replace(" " + TypeBMessageConstants.PaxTitles.MSTR, TypeBMessageConstants.PaxTitles.MSTR);
		names = names.replace(" " + TypeBMessageConstants.PaxTitles.MISS, TypeBMessageConstants.PaxTitles.MISS);

		List<NameDTO> nameDTOList = new ArrayList<NameDTO>();
		NameDTO nameDTO;
		String[] nameParts;

		nameDTO = new NameDTO();

		nameParts = names.split("/");
		if (nameParts.length >= 2) {
			if (nameParts[1].length() >= 2
					&& (nameParts[1].substring(nameParts[1].length() - 2).equals(TypeBMessageConstants.PaxTitles.MR) || nameParts[1]
							.substring(nameParts[1].length() - 2).equals(TypeBMessageConstants.PaxTitles.MS))) {
				nameDTO.setPaxTitle(nameParts[1].substring(nameParts[1].length() - 2));
				nameDTO.setFirstName(nameParts[1].substring(0, nameParts[1].length() - 2));
			} else if (nameParts[1].length() > 3
					&& (nameParts[1].substring(nameParts[1].length() - 4).equals(TypeBMessageConstants.PaxTitles.MSTR) || nameParts[1]
							.substring(nameParts[1].length() - 4).equals(TypeBMessageConstants.PaxTitles.MISS))) {
				nameDTO.setPaxTitle(nameParts[1].substring(nameParts[1].length() - 4));
				nameDTO.setFirstName(nameParts[1].substring(0, nameParts[1].length() - 4));
			} else {
				nameDTO.setFirstName(nameParts[1]);
			}
			nameDTO.setLastName(nameParts[0]);
		}

		nameDTOList.add(nameDTO);

		return nameDTOList;
	}

	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * Method which process the SSR CHLD and fill the SSRChildDTO
	 * 
	 * @param ssrElementmatcher
	 *            element pattern macher.
	 * @throws ModuleException
	 */

	private void setChildElementDTO(Matcher childElementMatcher) throws ModuleException {

		try {
			SSRChildDTO childDTO = new SSRChildDTO();
			childDTO.setCodeSSR(TypeBMessageConstants.SSRCodes.CHLD);
			childDTO.setFullElement(childElementMatcher.group());
			childDTO.setCarrierCode(childElementMatcher.toMatchResult().group(1));
			childDTO.setActionOrStatusCode(childElementMatcher.toMatchResult().group(2));
			childDTO.setSsrValue(childElementMatcher.group().substring(10 + childElementMatcher.toMatchResult().group(1).length()).trim());

			/*
			 * age of child in book : 05 yrs or 05 years but according to saber you have to enter DOB in following
			 * format .
			 */
			String childAge = childElementMatcher.toMatchResult().group(6);
			if (childAge != null && !childAge.equals("")) {

				String dobFormatString = "ddMMMyy";
				DateFormat dobFormat = new SimpleDateFormat(dobFormatString);
				Date dob = dobFormat.parse(childAge);
				childDTO.setDateOfBirth(dob);

			}
			String childNameString = childElementMatcher.toMatchResult().group(9);
			if (childNameString != null && childNameString.contains(SINGLE_FOWARD_SLASH)) {
				List ssrNameDTOList = setNameElementDTOs(childElementMatcher.toMatchResult().group(9).trim());

				if (!ssrNameDTOList.isEmpty()) {
					NameDTO nameDTO = (NameDTO) ssrNameDTOList.get(0);
					childDTO.setFirstName(nameDTO.getFirstName());
					childDTO.setLastName(nameDTO.getLastName());
					childDTO.setTitle(nameDTO.getPaxTitle());
				}
			} else if (nameDTOs.size() == 1) {
				NameDTO nameDTO = (NameDTO) nameDTOs.get(0);
				childDTO.setFirstName(nameDTO.getFirstName());
				childDTO.setLastName(nameDTO.getLastName());
				childDTO.setTitle(nameDTO.getPaxTitle());
//			} else {
//				throw new Exception();
			}

			ssrDTOs.add(childDTO);

		} catch (Exception e) {

			if (log.isDebugEnabled()) {
				log.debug("Unknown element: " + matcher.toMatchResult().group());
			}
			sStatus = "Unknown element: " + matcher.toMatchResult().group();
			throw new ModuleException("gdsmessageparser.element.unknown");
		}
	}

	public static void main(String[] args) throws Exception {
		String sMessage1 = "";
		sMessage1 = "HDQRMNW\n";
		sMessage1 += ".HDQRMAA 121844\n";
		sMessage1 += "HDQAA QHMYWM/F4X0/4510107\n";
		sMessage1 += "1SMITH/L MR 1HOOVER/A MR\n";
		sMessage1 += "NW282Y06MAR SEADTW SS1/1230 1934\n";
		sMessage1 += "NW260Y06MAR DTWLGA SS1/2045 2230\n";
		sMessage1 += "NW153Y12MAR LGASEA SS1/1400 1810\n";
		sMessage1 += "OSI NW CTCT NYC817 963 5572 A\n";
		sMessage1 += "OSI NW CTCB NYC817 571 2033 B\n";
		sMessage1 += "OSI NW CTCH NYC817 622 1956 H\n";

		// 4.1.2
		String sMessage2 = "";
		sMessage2 += "CLTRMUS\n";
		sMessage2 += ".HDQRMAA 192149\n";
		sMessage2 += "HDQAA QEKMVV/J4K0/4510107\n";
		sMessage2 += "2RICHARDS/MARTY/KAREN\n";
		sMessage2 += "US1478F22DEC CLTLAX SS2/1550 1749\n";
		sMessage2 += "US1002F29DEC LAXCLT SS2/1315 2041\n";
		sMessage2 += "OSI US CTCT NYC817 963 5572 A\n";
		sMessage2 += "OSI US CTCB NYC817 571 2033 B\n";
		sMessage2 += "OSI US CTCH NYC817 622 1956 H\n";

		String sMessage3 = "";
		sMessage3 = "HKGRMCX\n";
		sMessage3 += ".HDQRMAA 231859\n";
		sMessage3 += "HDQAA QEKMVV/J4K0/4510107\n";
		sMessage3 += "1MASUDA/YUICHI\n";
		sMessage3 += "CX838C18DEC YVRHKG SS1/1230 1934\n";
		sMessage3 += "CX839Y18DEC YVRHKG LL1/2045 2230\n";
		sMessage3 += "SSR NSST PA KK8 ORDCPH1102C12AUG-5VANDERMERWE/JOHAN/SEBATIAN/JUSTIN/\n";
		sMessage3 += "SSR NSST PA ///CHRISTIAN/THELMA 3STROBEAK/VLADIMIR/DJON/ALEX\n";
		sMessage3 += "SSR NSST PA ///.11A11B11C11D11E11F11G11H\n";
		// sMessage3 += "SSR NSST UA KK5 ORDSIN1135F21DEC-5JONES/MARTIN/JONATHAN/MARY/LEO/BOB/\n";
		// sMessage3 += "SSR NSST UA ///FRED.22C22D22E22F22G\n";
		// sMessage3 += "SSR GPST TW NN30JFKSTL0209Y11AUG\n";
		// sMessage3 += "SSR TKNA BA HK1 LHRJFK0175J18JAN-1VOCKING/JMR.12545081742210-2221\n";

		String sMessage67 = "";
		sMessage67 = "JFKRMTW\n";
		sMessage67 += ".PARRMPA 051355\n";
		sMessage67 += "PARPA 115Y10AUG\n";
		sMessage67 += "30SITA/TOUR\n";
		sMessage67 += "DL119Y10AUG ORYJFK HK30/1435 1700\n";
		sMessage67 += "TW209Y11AUG JFKSTL NN30/1550 1720\n";
		sMessage67 += "SSR GRPF TW YNO\n";
		sMessage67 += "SSR GRPF TW YNO PARNYCSTL FRF 3590\n";
		sMessage67 += "SSR GPST TW NN30JFKSTL0209Y11AUG\n";

		String sMessage68 = "";
		sMessage68 = "SHJMMG9\n";
		sMessage68 += ".HDQBB1S 160953\n";
		sMessage68 += "HDQ1S CKCAVB/PD5C/99999999\n";
		sMessage68 += "20ABC GRP\n";
		sMessage68 += "G9505Y22MAY SHJCMB SS1/2200 0315/1\n";
		sMessage68 += "SSR NSST G9 NN1 SHJCMB0505Y22MAY.FREETEXTSSR\n";
		sMessage68 += "OSI G9 NXTM RL-NONE\n";
		sMessage68 += "OSI G9 NXTM TL-TAW/\n";
		sMessage68 += "OSI G9 CTCP CMB12345678910\n";

		String sMessage70 = "";
		sMessage70 = "SHJMMG9\n";
		sMessage70 += ".HDQBB1S 160953\n";
		sMessage70 += "HDQ1S CKCAVB/PD5C/99999999\n";
		sMessage70 += "1SMITH/JHON\n";
		sMessage70 += "G9505Y22MAY SHJCMB SS1/2200 0315/1\n";
		sMessage70 += "SSR NSST G9 NN1 SHJCMB0505Y22MAY.FREETEXTSSR\n";
		sMessage70 += "OSI G9 NXTM RL-NONE\n";
		sMessage70 += "OSI G9 NXTM TL-TAW/\n";
		sMessage70 += "OSI G9 NXTM FC-NONE\n";
		sMessage70 += "OSI G9 NXTM TK-NONE\n";
		sMessage70 += "OSI G9 NXTM FP-NONE\n";
		sMessage70 += "OSI G9 CTCP CMB12345678910\n";

		String sMessage71 = "";
		sMessage71 = "SHJMMG9\n";
		sMessage71 += ".HDQBB1S 161026\n";
		sMessage71 += "HDQ1S IJJPJP/PD5C/99999999\n";
		sMessage71 += "20AMEX GRP\n";
		sMessage71 += "G9505Y17MAY SHJCMB NN20/2200 0315/1\n";
		sMessage71 += "SSR GRPF G9 IT FARE\n";
		sMessage71 += "OSI G9 NXTM RL-NONE\n";
		sMessage71 += "OSI G9 NXTM TL-TAW/\n";
		sMessage71 += "OSI G9 NXTM FC-NONE\n";
		sMessage71 += "OSI G9 CTCP CMB12345678910\n";

		String sMessage72 = "";
		sMessage72 = "QP SHJMMG9\n";
		sMessage72 += ".HDQBB1S 161055\n";
		sMessage72 += "HDQ1S MSXAUF/PD5C/99999999\n";
		sMessage72 += "20GROUP/AMEN\n";
		sMessage72 += "G9505Y17MAY SHJCMB NN20/2200 0315/1\n";
		sMessage72 += "SSR GRPF G9 IT FREE\n";
		sMessage72 += "OSI G9 NXTM RL-NONE\n";
		sMessage72 += "OSI G9 CTCP CMB1012314\n";

		String sMessage73 = "";
		sMessage73 = "LONRMBA\n";
		sMessage73 += ".OSLRMSK 101112\n";
		sMessage73 += "OSLSK AY714F19APR\n";
		sMessage73 += "1JOHNSON/JW 2BOHMER/T/G\n";
		sMessage73 += "AY714F19APR OSLLHR HK3/0600 0700\n";
		sMessage73 += "BA262T19APR LHRMAN NN3/0800 0845\n";
		sMessage73 += "BA073T27APR MANPIK NN3/1850 1935\n";
		sMessage73 += "SSR NSST BA NN3 LHRMAN0262T19APR\n";
		sMessage73 += "SSR NSST BA NN3 MANPIK0073T27APR\n";

		String sMessage74 = "";
		sMessage74 = "HDQRMLH\n";
		sMessage74 += ".HDQRMAA 220501\n";
		sMessage74 += "HDQTW YRFBES A731AH\n";
		sMessage74 += "1MONTI/RMS\n";
		sMessage74 += "ARNK\n";
		sMessage74 += "DI009J22DEC FRASYD NN1/2225 0725/2 LH\n";

		String sMessage75 = "";
		sMessage75 = "ROMRMTW MADRMIB\n";
		sMessage75 += ".BEYRMME 111021\n";
		sMessage75 += "BEYME 231F11MAY\n";
		sMessage75 += "1KHOURY\n";
		sMessage75 += "ME231F11MAY BEYFCO HK1/1320 1550\n";
		sMessage75 += "TW075F15MAY FCOMAD NN1/1250 1505\n";
		sMessage75 += "ARNK\n";
		sMessage75 += "IB218F20MAY BCNLHR NN1/1115 1220\n";

		String sMessage76 = "";
		sMessage76 = "JFKRMTW\n";
		sMessage76 += ".PARRMPA 051355\n";
		sMessage76 += "PARPA 115Y10AUG\n";
		sMessage76 += "5ARDMORE 3BATES 5DRUMMOND 3ENGLER 4HAYES\n";
		sMessage76 += "5ZIMMERMAN 5CLARK\n";
		sMessage76 += "DL119Y10AUG ORYJFK HK30/1435 1700\n";
		sMessage76 += "TW209Y11AUG JFKSTL NN30/1550 1720\n";
		sMessage76 += "SSR  TW TCP30 SITA/TOUR\n";
		sMessage76 += "SSR GRPF TW YNC\n";
		sMessage76 += "SSR GRPF TW YNC PARNYCSTL FRF 3590\n";
		sMessage76 += "OSI TW CTCA NYC HOLIDAY INN AGT ABC TRAVEL\n";

		String sMessage78 = "";
		sMessage78 = "SHJMMG9\n";
		sMessage78 += ".HDQBB1S 220900\n";
		sMessage78 += "HDQ1S IOGQED/PD5C/99999999\n";
		sMessage78 += "17CITA 5NAME/AB/CD/EF/GH/IJ 3NAMEE/X/Y/Z\n";
		sMessage78 += "G9505Y29MAY SHJCMB NN25/2200 0315/1\n";
		sMessage78 += "SSR GRPF G9 IT FARE\n";
		sMessage78 += "SSR  G9 TCP25 CITA\n";
		sMessage78 += "OSI G9 CTCP CMB123456789 \n";

		String sMessage62 = "";
		sMessage62 = "HDQRM1S\n";
		sMessage62 += ".LAXRMVS\n";
		sMessage62 += "MEXMX\n";
		sMessage62 += "10GDSEMPLOYEES\n";
		sMessage62 += "CHNT\n";
		sMessage62 += "1ERICKSON/JIM 1LITTER/KATHLEEN MS\n";
		sMessage62 += "8GDSEMPLOYEES\n";
		sMessage62 += "VS023J30SEP LHRLAX KK10\n";
		sMessage62 += "SSR  YY TCP10 GDS EMPLOYEE\n";

		String sMessage80 = "";
		sMessage80 = "SHJMMG9\n";
		sMessage80 += ".HDQBB1S 260556\n";
		sMessage80 += "HDQ1S MCFUVM/PD5C/99999999\n";
		sMessage80 += "1SANJ/FONS\n";
		sMessage80 += "G9505Y30MAY SHJCMB SS1/2200 0315/1\n";
		sMessage80 += "OSI G9 NXTM RL-NONE\n";
		sMessage80 += "OSI G9 CTCP CMB741852\n";

		String sMessage81 = "";
		sMessage81 = "LHRRMBA\n";
		sMessage81 += ".HDQRMAA 041016\n";
		sMessage81 += "HDQAA QRDOEJ/C6K2/9120054\n";
		sMessage81 += "LHRBA SWFX54\n";
		sMessage81 += "1FLOWER/SMR\n";
		sMessage81 += "BA076J29JAN ACCLGW XX1\n";
		sMessage81 += "BA077J22JAN LGWACC HK1\n";
		// sMessage81 += "BA076J27JAN ACCLGW NN1\n";
		// sMessage81 += "SSR TKNM BA HK1 LGWACC0077J22JAN.07980391477713\n";

		String sMessage83 = "";
		sMessage83 += "LHRRMBA\n";
		sMessage83 += ".HDQRM1P 041017\n";
		sMessage83 += "HDQ1P 6NPN28\n";
		sMessage83 += "1CAMERON/SMS 1WILSON/TMS\n";
		sMessage83 += "BA8072W20JAN NCLLGW HK2\n";
		sMessage83 += "BA237G20JAN LGWMCO HK2\n";
		sMessage83 += "BA236G24JAN MCOLGW HK2\n";
		sMessage83 += "BA8073W25JAN LGWNCL HK2\n";
		sMessage83 += "SSR TKNM BA HK1 NCLLGW8072W20JAN-1WILSON/TMS.00742374399012/\n";
		sMessage83 += "SSR TKNM BA ///00742320762971\n";
		sMessage83 += "SSR TKNM BA HK1 LGWMCO0237G20JAN-1WILSON/TMS.00742374399012/\n";
		sMessage83 += "SSR TKNM BA ///00742320762971\n";
		sMessage83 += "SSR TKNM BA HK1 MCOLGW0236G24JAN-1WILSON/TMS.00742374399012/\n";
		sMessage83 += "SSR TKNM BA ///00742320762971\n";
		sMessage83 += "SSR TKNM BA HK1 LGWNCL8073W25JAN-1WILSON/TMS.00742374399012/\n";
		sMessage83 += "SSR TKNM BA ///00742320762971\n";

		String sMessage79 = "";
		sMessage79 = "SWIRM1G\n";
		sMessage79 += ".AMSRMKL 121314\n";
		sMessage79 += "SWI1G ABC472\n";
		sMessage79 += "2VANDERGOOT/MR/MRS\n";
		sMessage79 += "KL652B14APR IADAMS HK2\n";
		sMessage79 += "KL341B15APR AMSGVA HK2\n";
		sMessage79 += "SSR ADTK 1G TO KL BY 10APR OTHERWISE WILL BE XLD\n";

		String sMessage85 = "";
		sMessage85 = "QVIRMSN\n";
		sMessage85 += ".PARRMAF 151020\n";
		sMessage85 += "PARAF 11E231\n";
		sMessage85 += "1SMITH/JOHN\n";
		sMessage85 += "SN352Y20MAY ORYBRU XX1\n";
		sMessage85 += "OSI SN TCP3 1GREEN/BOB 1PINK/MR 1ROSE/MRS\n";

		String sMessage86 = "";
		// Invalide message ,here only one RLO
		sMessage86 = "HDQRMDL\n";
		sMessage86 += ".HDQRMUA 131510\n";
		sMessage86 += "HDQUA 264330PRR\n";
		sMessage86 += "30SITA/TOUR\n";
		sMessage86 += "CHNT\n";
		sMessage86 += "20SITA/TOUR\n";
		sMessage86 += "DL453Y24MAY SYRROC XX10\n";
		sMessage86 += "AA328Y23MAY ORDSYR HK20/1300 1445\n";
		sMessage86 += "DL453Y24MAY SYRROC HK20\n";
		sMessage86 += "SSR  DL TCP20 SITA/TOUR\n";

		String sMessage87 = "";
		sMessage87 = "SHJMMG9\n";
		sMessage87 += ".HDQBB1S 270505\n";
		sMessage87 += "HDQ1S JSDRPV/PD5C/99999999\n";
		sMessage87 += "3ASDASDAJSDASKDA/ASDASDASDADADADADA/ASDASDASDASDASDASDAS/ASDADD\n";
		sMessage87 += "G9505Y07JUN SHJCMB SS3/2200 0315/1\n";
		sMessage87 += "OSI G9 NXTM RL-NONE\n";
		sMessage87 += "OSI G9 NXTM TL-TAW/\n";
		sMessage87 += "OSI G9 NXTM FC-NONE\n";
		sMessage87 += "OSI G9 CTCP CMB85274855\n";

		String sMessage88 = "";
		sMessage88 = "SHJMMG9\n";
		sMessage88 += ".HDQBB1S 270529\n";
		sMessage88 += "HDQ1S JSDRPV/PD5C/99999999\n";
		sMessage88 += "3ASDASDAJSDASKDA/ASDASDASDADADADADA/ASDASDASDASDASDASDAS/ASDADD\n";
		sMessage88 += "G9505Y07JUN SHJCMB XX3\n";

		String sMessage89 = "";
		sMessage89 = ".HDQRMDL 211555\n";
		sMessage89 += "HDQDL B4PT\n";
		sMessage89 += "45WATTSTRAVEL\n";
		sMessage89 += "CHNT\n";
		sMessage89 += "1SMITH 1BROWN 1JONES 27WATTS TRAVEL\n";
		sMessage89 += "BN076Y21DEC MSYDAL XX15\n";
		sMessage89 += "DL410Y18DEC ATLMSY HK30/0800 1033\n";
		sMessage89 += "BN076Y21DEC MSYDAL HK30\n";

		String sMessage90 = "";
		sMessage90 = "MUCRM1A\n";
		sMessage90 += ".HDQRM1P 200935\n";
		// sMessage90 += "BPR\n";
		sMessage90 += "HDQ1P YHY2N9/KK32/91295315/NCL/1P/T/GB//GS\n";
		sMessage90 += "1CORKING/STEPHENMR\n";
		sMessage90 += "IB6125Y30SEP MADMIA LK1/1810 2205 1A\n";
		sMessage90 += "IB5122C12OCT MIAMAD LK1/1745 0745/1 1A\n";
		sMessage90 += "SSR SEAT IB LK1" + " MADMIA6125Y30SEP.147AN\n";
		sMessage90 += "SSR SEAT IB LK1 MIAMAD6122C12OCT.008FN\n";

		String sMessage92 = "";
		sMessage92 = "LAXRMCO\n";
		sMessage92 += ".HDQRMNW 111525\n";
		// TODO check this sMessage92 += "HDQNW LSND7Z
		// 10861F/MS1/24000012/MSP/NW/A/US/USD/PD\n";
		sMessage92 += "HDQNW LSND7Z\n";
		sMessage92 += "2LARSON/TES 1KOURAJIAN/ART\n";
		sMessage92 += "NW1861Y22JUN MSPIAH HK3/0925 1214\n";
		sMessage92 += "CO3557H22JUN IAHBRO HK3/1435 1549\n";
		sMessage92 += "CO3098H24JUN BROIAH HK3/0805 0900\n";
		sMessage92 += "NW1682Y24JUN IAHMEM HK3/1039 1210\n";
		sMessage92 += "ARNK\n";
		// TODO check this sMessage92 += "CO3997H26JUN LBBIAH HK3 -NW7297Y\n";
		sMessage92 += "CO3997H26JUN LBBIAH HK3\n";
		sMessage92 += "NW1086Y26JUN IAHMEM HK3/1439 1610\n";
		sMessage92 += "SSR TKNE CO HK3 IAHBRO3557H22JUN-1LARSON/TES.0122164009165C2/165-166\n";
		sMessage92 += "SSR TKNE CO HK3 BROIAH3098H24JUN-1LARSON/TES.0122164009165C3/165-166\n";
		sMessage92 += "SSR TKNE CO HK3 LBBIAH3997H26JUN-1LARSON/TES.0122164009166C2/165-166\n";

		String sMessage93 = "";
		sMessage93 = "HDQRMDL\n";
		sMessage93 += ".HDQRM1P 022202\n";
		sMessage93 += "BPR\n";
		sMessage93 += "HDQ1P YC42YA/0578105\n";
		sMessage93 += "1SMITH/HENRY 1JONES/JENNY 1HARRIS/JIMMSTR\n";
		sMessage93 += "DL123B19AUG ATLMSY HK3\n";
		sMessage93 += "SSR SEAT DL NN2 ATLMSY0123B19AUG-1JONES/JENNY 1HARRIS/JIMMSTR.23A23B\n";

		String sMessage94 = "";
		sMessage94 = "HDQRMAA\n";
		sMessage94 += ".MUCRM1A\n";
		sMessage94 += "BPR\n";
		// TODO CHECK sMessage94 += "MUC1A
		// ZWWPQG/MADIB0500/7849037/MAD/IB/A/ES//GS\n";
		sMessage94 += "MUC1A ZWWPQG\n";
		sMessage94 += "HDQAA EPWDFH\n";
		sMessage94 += "1CORKING/STEPHENMR 1OKEEFE/MEGMRS 1DAMASALCALA/CAYETANOMR\n";
		sMessage94 += "AA069J30JUL MADMIA HK3/1145 1515\n";
		sMessage94 += "SSR SEAT AA XX1 MADMIA0069J30JUL-1OKEEFE/MEGMRS.03HN\n";
		sMessage94 += "SSR SEAT AA LK1 MADMIA0069J30JUL-1OKEEFE/MEGMRS.04KN\n";

		String sMessage95 = "";
		sMessage95 = "JFKRMTW\n";
		sMessage95 += ".PARRMPA 051355\n";
		sMessage95 += "PARPA 115Y10AUG\n";
		sMessage95 += "25SITA/TOUR 5ARDMORE/BOB/SUE/TIM/TOM/TONY\n";
		sMessage95 += "DL119Y10AUG ORYJFK HK30/1435 1700\n";
		sMessage95 += "TW209Y11AUG JFKSTL NN30/1550 1720\n";
		sMessage95 += "SSR GRPS TW TCP30 SITA/TOUR\n";
		sMessage95 += "SSR GRPF TW YLE13/GV30\n";
		sMessage95 += "SSR GRPF TW YLE13/GV30 PARNYCSTL FRF 3590\n";
		sMessage95 += "SSR GPST TW NN25JFKSTL0209Y11AUG\n";
		// sMessage95 += "SSR NSST TW NN5JFKSTL0209Y11AUG-5ARDMORE\n";
		// sMessage95 += "BOB/SUE/TIM/TOM/TONY\n";

		String sMessage96 = "";
		sMessage96 = "LHRRMBA\n";
		sMessage96 += ".HDQRMAA 041016\n";
		sMessage96 += "HDQAA QRDOEJ/C6K2/9120054\n";
		sMessage96 += "LHRBA SWFX54\n";
		sMessage96 += "1FLOWER/SMR\n";
		sMessage96 += "BA076J29JAN ACCLGW XX1\n";
		sMessage96 += "BA077J22JAN LGWACC HK1\n";
		sMessage96 += "BA076J27JAN ACCLGW NN1\n";
		sMessage96 += "SSR TKNM BA HK1 LGWACC0077J22JAN.07980391477713\n";
		sMessage96 += "SSR TKNM BA HK1 ACCLGW0076J27JAN.07980391477713\n";
		sMessage96 += "SSR WCHR US HK4 DNVORD0318S15FEB\n";
		sMessage96 += "SSR DOCS BA HK1 MADLHR0455Y28JUN/P/SIN/S78654091/SIN/12JUL64/\n";
		sMessage96 += "SSR DOCS BA ///M/23OCT05/STEVENSON/JOHN/RICHARD-1STEVENSON/JOHNMR\n";

		String sMessage77 = "";
		sMessage77 = "JFKRMTW\n";
		sMessage77 += ".PARRMPA 051355\n";
		sMessage77 += "PARPA 115Y10AUG\n";
		sMessage77 += "25SITA/TOUR 5ARDMORE/BOB/SUE/TIM/TOM/TONY 5FONSEKA/KUM/JAM\n";
		sMessage77 += "18AMEX 5AME/A/B/C/D/E\n";
		// sMessage77 += "25SITA\n";
		sMessage77 += "DL119Y10AUG ORYJFK HK30/1435 1700\n";
		sMessage77 += "TW209Y11AUG JFKSTL NN30/1550 1720\n";
		// sMessage77 += "SSR TW TCP30 SITA/TOUR\n";
		sMessage77 += "SSR GRPF TW YLE13/GV30\n";
		sMessage77 += "SSR GRPF TW YLE13/GV30 PARNYCSTL FRF 3590\n";
		sMessage77 += "SSR GPST TW NN25JFKSTL0209Y11AUG\n";
		sMessage77 += "SSR NSST TW NN5 JFKSTL0209Y11AUG-5ARDMORE/BOB/SUE/TIM/TOM/TONY.FRE\n";

		String sMessage84 = "";
		sMessage84 = "LHRRMBA\n";
		sMessage84 += ".ALLRMAZ 211135\n";
		sMessage84 += "ALLAZJK14TM\n";
		sMessage84 += "1MASON/JMR\n";
		sMessage84 += "BA643C09JAN OSLLHR HK1\n";
		sMessage84 += "SSR INFT IB NN1 MADMIA6123Y20JUN-1POPOV/ANNAMS\n";
		sMessage84 += "SSR INFT IB ///.POPOV/OLIVERMSTR 4MTHS\n";

		String sMessage98 = "";
		sMessage98 = "SHJMMG9\n";
		sMessage98 += ".HDQBB1S 130638\n";
		sMessage98 += "HDQ1S JLOUUT/PD5C/99999999\n";
		sMessage98 += "1SANJEEWA/FONSEKA\n";
		sMessage98 += "G9505Y25JUN SHJCMB NN1/2200 0315/1\n";
		// sMessage98 += "SSR INFT G9 NN1 SHJCMB0505Y25JUN.JON/CENA 07MTHS\n";
		sMessage98 += "SSR INFT IB NN1 MADMIA6123Y20JUN-1ANJ/JOLI.09JAN07 OCCUPYING SEAT\n";
		sMessage98 += "OSI G9 NXTM RL-NONE\n";
		sMessage98 += "OSI G9 NXTM TL-TAW/\n";
		sMessage98 += "OSI G9 CTCP CMB12345678\n";

		String sMessage99 = "";
		sMessage99 = "SHJMMG9\n";
		sMessage99 += ".HDQBB1S 130638\n";
		sMessage99 += "HDQ1S JLOUUT/PD5C/99999999\n";
		sMessage99 += "1SANJEEWA/FONSEKA\n";
		sMessage99 += "G9505Y25JUN SHJCMB NN1/2200 0315/1\n";
		// sMessage99 += "SSR INFT G9 NN1 SHJCMB0505Y26JUN.JON/CENA 09JAN07\n";
		sMessage99 += "SSR INFT G9 NN1 SHJCMB0505Y27JUN-1ANJALINA/JOLI.JERM/ROMIO 09JAN07\n";
		sMessage99 += "OSI G9 CTCP CMB123456789\n";

		/** * special message NAC should go here.. message without infant name */
		String sMessage100 = "";
		sMessage100 = "SHJMMG9\n";
		sMessage100 += ".HDQBBAA 170531\n";
		sMessage100 += "HDQAA DTAUKV/PD5C/99999999\n";
		sMessage100 += "1NAME/SANJEEWA\n";
		sMessage100 += "G9505Y01JUL SHJCMB NN1/2200 0315/1\n";
		sMessage100 += "SSR INFT G9 NN1 SHJCMB0505Y01JUL.09MTHS OCCUPYING SEAT\n";
		sMessage100 += "OSI G9 CTCP CMB123456\n";

		String sMessage101 = "";
		sMessage101 = "SHJMMG9\n";
		sMessage101 += ".HDQBBAA 171010\n";
		sMessage101 += "HDQAA KBWZQE/PD5C/99999999\n";
		sMessage101 += "1SANJEEWA/FONSEKA 1ANJALINA/JOLI\n";
		sMessage101 += "G9505Y19JUN SHJCMB NN2/2200 0315/1\n";
		sMessage101 += "SSR INFT G9 NN1 CMBSHJ0506Y19JUN-1SANJEEWA/BANDARANAYAKABAGAWATHMR\n";
		sMessage101 += "SSR INFT G9 ///.08JUN08 OCCUPYING SEAT\n";
		sMessage101 += "OSI G9 CTCP CMB123456789\n";

		String sMessage102 = "";
		sMessage102 = "SHJMMG9\n";
		sMessage102 += ".HDQBBAA 171010\n";
		sMessage102 += "HDQAA KBWZQE/PD5C/99999999\n";
		sMessage102 += "1SANJEEWA/FONSEKA 1ANJALINA/JOLI\n";
		sMessage102 += "G9505Y19JUN SHJCMB NN2/2200 0315/1\n";
		// sMessage102 += "SSR PCTC YY HK/HANS GIBSON/GB 001 0181 555
		// 0000.OFFICE NUMBER\n";
		sMessage102 += "SSR PSPT DL HK1/P9123432/NZ/27OCT52/HIGH/ANTHONYJAMES/M-1HIGH/ARNOMR\n";
		// sMessage102 += "SSR PSPT TW
		// HK1/P132487219/US/17APR89/PEERSHYJHHGJHGJJGF/\n";
		// sMessage102 += "SSR PSPT TW HK1 ///STEMAE/F/H-1PEERS/MARMS\n";

		String sMessage103 = "";
		sMessage103 = "AMSRMKL\n";
		sMessage103 += ".SWIRM1G 180907\n";
		sMessage103 += "NCO\n";
		sMessage103 += "SWI1G 123456/AAA/12378945/FRADET\n";
		sMessage103 += "1GAMANO/HIDEYUKIMR\n";
		sMessage103 += "JL555M20MAY NRTFRA HK1/1135 1250\n";
		sMessage103 += "KL603M21MAY FRAAMS HK1\n";
		sMessage103 += "NW055M21MAY AMSMSP HK1/1600 1655\n";
		sMessage103 += "AA126M22MAY MSPDFW HK1/1300 1710\n";
		sMessage103 += "SSR VGML SN HK3 ORYBRU0352Y20MAY-1SMITH/JOHN\n";
		sMessage103 += "SSR VGML SN ///1GREEN/BOB1ROSE/MRS\n";

		String sMessage104 = "";
		sMessage104 = "MSYRMBN\n";
		sMessage104 += ".HDQRMDL 231447\n";
		sMessage104 += "HDQDL B4PTDI A35IVL\n";
		sMessage104 += "27WATTSTRAVEL\n";
		sMessage104 += "CHNT\n";
		sMessage104 += "1DODGE/HJH 26WATTSTRAVEL\n";
		sMessage104 += "DL351Y21DEC ATLMSY HK27/0001 0112\n";
		sMessage104 += "BN076Y22DEC DALMSY NN27/1305 1420\n";
		sMessage104 += "SSR GRPS YY TCP27 WATTS TRAVEL\n";

		String sMessage105 = "";
		sMessage105 = "SHJMMG9\n";
		sMessage105 += ".HDQBBAA 170531\n";
		sMessage105 += "HDQAA DTAUKV/PD5C/99999999\n";
		sMessage105 += "1NAME/SANJEEWA\n";
		sMessage105 += "G9505Y01JUL SHJCMB NN1/2200 0315/1\n";
		sMessage105 += "SSR FOID IB HK4/FFIB135847361-1CORKING/STEPHEN \n";
		sMessage105 += "SSR FOID IB ///2GUBERT/LAURENT/NEOMSTR\n";

		/** ASC Message Testing */

		String sMessage106 = "";
		sMessage106 = "LHRRMBA\n";
		sMessage106 += ".MUCRM1A 042117\n";
		sMessage106 += "ASC\n";
		sMessage106 += "LHRBA3261\n";
		sMessage106 += "1FARNSWORTH/E\n";
		sMessage106 += "AF690Y16MAY CDGHKG TK1/1700 0800/1\n";

		String sMessage107 = "";
		sMessage107 = "MUCRM1A\n";
		sMessage107 += ".HDQRMTW 072142\n";
		sMessage107 += "ASC\n";
		sMessage107 += "MUC1A ABCDEF\n";
		sMessage107 += "1APPLEBY/R\n";
		sMessage107 += "TW110Y20MAY ORDJFK TL1/1730 2025\n";

		String sMessage108 = "";
		sMessage108 = "AKLRMNZ\n";
		sMessage108 += ".LHRRMBA 042117\n";
		sMessage108 += "ASC\n";
		sMessage108 += "AKLNZJ23GAS\n";
		sMessage108 += "1MARTIN/LMRS\n";
		sMessage108 += "BA009J22DEC LHRSYD TK1/2225 0725/2\n";

		String sMessage109 = "";
		sMessage109 = "LHRRMBA\n";
		sMessage109 += ".MUCRM1A 042117\n";
		sMessage109 += "ASC\n";
		sMessage109 += "LHRBA 3261\n";
		sMessage109 += "1DAVIS/CHARLES\n";
		sMessage109 += "AF692Y16MAY ORYZRH UN1\n";
		sMessage109 += "AF690Y16MAY ORYZRH TK1/1700 1800\n";
		sMessage109 += "DL375C30APR ATLSJO UN2 *TOUR/LITE\n";
		sMessage109 += "NW742K01MAR MSPDTW TK2/0742 0945 .1. *E5F6G7\n";
		sMessage109 += "SSR PCTC JK HK/SUSAN RORY/US12345678900-1RORY/DAVIDMR.WIFE\n";
		sMessage109 += "SSR NSST AF KK1 ORYZRH0690Y16MAY.14B\n";
		sMessage109 += "SSR INFT G9 NN1 SHJCMB0505Y27JUN-1ANJALINA/JOLI.JERM/ROMIO 09JAN07\n";
		sMessage109 += "SSR PSPT DL HK1/P9123432/NZ/27OCT52/HIGH/ANTHONYJAMES/M-1HIGH/ARNOMR\n";
		sMessage109 += "SSR FOID IB HK4/FFIB135847361-1CORKING/STEPHEN \n";
		sMessage109 += "SSR FOID IB ///2GUBERT/LAURENT/NEOMSTR\n";
		sMessage109 += "OSI YY RLOC DNVALABCDEF\n";

		String sMessage91 = "";
		sMessage91 = "NYCRMEA\n";
		sMessage91 += ".ZRHRMLX 292045\n";
		sMessage91 += "ZRHLX 219F01JUN\n";
		sMessage91 += "ZRHLX 219F01JUN\n";
		sMessage91 += "2MOORE/B/T\n";
		sMessage91 += "5JONES/MARTIN/JONATHAN/MARY/LEO/BOB\n";
		sMessage91 += "EA066F08JUN JFKMIA XX2\n";
		sMessage91 += "LX100F02JUN GVAJFK HK2/1030 1435\n";
		sMessage91 += "EA066F05JUN JFKMIA SS2/0900 1150\n";
		sMessage91 += "KL0606J20JAN SFOAMS UN2\n";
		sMessage91 += "SSR OTHS ESTRICTED 01APR10APR\n";
		sMessage91 += "SSR LANG UA HK/MIAJFK0026Y29OCT.PSGR SPEAKS ZULU ONLY";
		sMessage91 += "SSR CHLD KL HK1/05YRS-1MATTH/EUMSTR\n";
		sMessage91 += "SSR INFT IB NN1 MADMIA6123Y20JUN-1FITCH/CHANLERMSTR.14MT SEAT\n";
		sMessage91 += "SSR CHLD KL HK1/05YRS-1MATTHIEU/MSTR\n";
		sMessage91 += "SSR CHLD AA HK1/-1MATTHEWS/ROSSMSTR\n";
		sMessage91 += "SSR PSPT TW HK1/P132487219/US/15NOV61/PEERS/MARLAT/F/H-1PEERS/MARLAMS\n";

		String sMessage82 = "";
		sMessage82 = "LHRRMBA\n";
		sMessage82 += ".SWIRM1G 040958\n";
		sMessage82 += "SWI1G 31397O/1EHA/91216591\n";
		sMessage82 += "2VOCKING/JMR/SMRS 1ELLIS/AMRS 1NASH/DMR 1SPIZEWSKI/TMRS\n";
		sMessage82 += "2VOCKING/JMR/SMRS 1ELLIS/AMRS 1NASH/DMR 1SPIZEWSKI/TMRS\n";
		sMessage82 += "BA022J22JAN LGWIAH XX5\n";
		sMessage82 += "BA175J18JAN LHRJFK HK5\n";
		sMessage82 += "BA232J28JAN BDALGW SS5/1915 0630/1\n";
		sMessage82 += "SSR TKNA BA HK1 LHRJFK0175J18JAN-1VOCKING/JMR.12545081742210-2221\n";
		sMessage82 += "SSR TKNA BA HK1 BDALGW0232J28JAN-1VOCKING/JMR.12545081742210-2221\n";
		sMessage82 += "SSR TKNA BA HK1 LHRJFK0175J18JAN-1VOCKING/SMRS.12545081742232-2243\n";
		sMessage82 += "SSR TKNA BA HK1 BDALGW0232J28JAN-1VOCKING/SMRS.12545081742232-2243\n";
		sMessage82 += "SSR INFT BA NN1 0175J18JAN LHRJFK-1VOCKING/SMRS.VOCKING/ABBYMISS 4MTHS\n";
		sMessage82 += "SSR INFT AA NN1 1444Y26JAN JFKBDA-1VOCKING/SMRS.VOCKING/ABBYMISS 4MTHS\n";
		sMessage82 += "SSR INFT BA NN1 0232J28JAN BDALGW-1VOCKING/SMRS.VOCKING/ABBYMISS 4MTHS\n";

		String sMessage110 = "";
		sMessage110 = "QP SHJMMG9\n";
		sMessage110 += ".HDQBB1S 250408\n";
		sMessage110 += "HDQ1S JXORAJ/PD5C/99999999\n";
		sMessage110 += "1NAME/NAME\n";
		sMessage110 += "G9505Y26JUL SHJCMB NN1/2200 0315/1\n";
		sMessage110 += "G9505Y26JUL SHJCMB XX1/2200 0315/1\n";
		sMessage110 += "OSI G9 NXTM RL-NONE\n";
		sMessage110 += "OSI G9 NXTM TL-TAW/\n";
		sMessage110 += "OSI G9 CTCP CMB8\n";

		String sMessage111 = "";
		sMessage111 = "SHJMMG9\n";
		sMessage111 += ".HDQBB1S 281114\n";
		sMessage111 += "HDQ1S JZOWEN/PD5C/99999999\n";
		sMessage111 += "1JHON/TRAVOLTA 1HEATH/LEADGER\n";
		sMessage111 += "G9505Y29AUG SHJCMB NN2/2200 0315/1\n";
		sMessage111 += "G9506Y30AUG CMBSHJ NN2/0430 0740\n";
		sMessage111 += "SSR CHLD G9 HK1/12NOV02-1HEATH/LEADGER\n";
		sMessage111 += "OSI G9 CTCP CMB55555\n";

		String sMessage112 = "";
		sMessage112 = "SHJMMG9\n";
		sMessage112 += ".HDQBB1S 290653\n";
		sMessage112 += "HDQ1S BVIQTT/PD5C/99999999\n";
		sMessage112 += "22ROTARY GRP\n";
		sMessage112 += "CHNT\n";
		sMessage112 += "21ROTARY GRP 1IOANNOU/AMR\n";
		sMessage112 += "G9505Y01AUG SHJCMB HN22\n";
		sMessage112 += "SSR GRPS G9 TCP22 ROTARY GRP\n";
		sMessage112 += "OSI G9 CTCP CMB9999999\n";

		String sMessage113 = "";
		sMessage113 = "SHJMMG9\n";
		sMessage113 += ".HDQBB1S 290647\n";
		sMessage113 += "HDQ1S BVIQTT/PD5C/99999999\n";
		sMessage113 += "22ROTARY GRP\n";
		sMessage113 += "G9505Y01AUG SHJCMB NN22/2200 0315/1\n";
		sMessage113 += "OSI G9 CTCP CMB7778787\n";

		String sMessage115 = "";
		sMessage115 = "SHJMMG9\n";
		sMessage115 += ".HDQBB1S 300641\n";
		sMessage115 += "HDQ1S JICWYC/PD5C/99999999\n";
		sMessage115 += "1SANJEEWA/FONSEKA 1NONAME/A 1NONAME/B\n";
		sMessage115 += "G9505Y07AUG SHJCMB NN3/2200 0315/1\n";
		sMessage115 += "SSR LANG G9 HK2 SHJCMB0505Y07AUG-1SANJEEWA/FONSEKA 1NONAME/A.PISSU\n";
		sMessage115 += "OSI G9 CTCP CMB787878\n";

		String sMessage116 = "";
		sMessage116 = "SHJMMG9\n";
		sMessage116 += ".HDQBB1S 300641\n";
		sMessage116 += "HDQ1S DYLVOO/PD5C/99999999\n";
		sMessage116 += "1NAMEKTYUNTBJ/HJJGSFVGJ 1SGTSJNGGSSA/JIJHGYGFGS 1SSSDSDS/SDSDSD\n";
		sMessage116 += "1SDSDSUUU/ASHYUY 1SSWT/SJJJJUSNGBGTS 1OPOPOPOPOP/SHYHGGFFVV\n";
		sMessage116 += "G9505Y20AUG SHJCMB NN6/2200 0315/1\n";
		sMessage116 += "OSI G9 CTCP CMB12345678912\n";

		// String sMessage117 = "";
		// sMessage117 = "SHJMMG9\n";
		// sMessage117 += ".HDQBB1S 300641\n";
		// sMessage117 += "HDQ1S BBTZGK/PD5C/99999999\n";
		// sMessage117 += "1NAWEEN/FONSEKA 1JONATHAN/BLADE\n";
		// sMessage117 += "G9505Y20AUG SHJCMB NN2/2200 0315/1\n";
		// sMessage117 += "SSR UMNR G9 NN1 SHJCMB0505Y20AUG-1NAWEEN/FONSEKA.UM 07\n";
		// sMessage117 += "OSI G9 CTCP CMB45567891254-A\n";

		String sMessage18 = "";
		sMessage18 = "SHJMMG9\n";
		sMessage18 += ".HDQBB1S 310355\n";
		sMessage18 += "HDQ1S JJYPPO/PD5C/99999999\n";
		sMessage18 += "1HARRIES/JHON\n";
		sMessage18 += "G9505Y26AUG SHJCMB NN1/2200 0315/1\n";
		sMessage18 += "OSI G9 CTCP CMB4444\n";

		String sMessage19 = "";
		sMessage19 = "SHJMMG9\r\n";
		sMessage19 += ".HDQBB1S 120946\r\n";
		sMessage19 += "HDQ1S DAZRED/PD5C/99999999\r\n";
		sMessage19 += "1DOM/JOLU\r\n";
		sMessage19 += "G9505Y26AUG SHJCMB NN1/2200 0315/1\r\n";
		sMessage19 += "OSI G9 NXTM RL-NONE\r\n";
		sMessage19 += "OSI G9 NXTM TL-NONE\r\n";
		sMessage19 += "OSI G9 NXTM FC-NONE\r\n";
		sMessage19 += "OSI G9 NXTM TK-NONE\r\n";
		sMessage19 += "OSI G9 NXTM FP-NONE\r\n";
		sMessage19 += "OSI G9 CTCP CMB5445445\r\n";

		String sMessage4 = "";
		sMessage4 = "HKGRMCX\n";
		sMessage4 += ".HDQRMAA 242031\n";
		sMessage4 += "HDQAA QEKMVV/JK40/4510107\n";
		sMessage4 += "HKGCX ABCDEF\n";
		sMessage4 += "1MASUDA/YUICHI\n";
		sMessage4 += "CX839C18DEC YVRHKG HK1\n";
		sMessage4 += "CX839Y18DEC YVRHKG HK1\n";
		sMessage4 += "CX838C27DEC HKGYVR SS1/1730 1245\n";

		String sMessage15 = "";
		sMessage15 = "SHJMMG9\n";
		sMessage15 += ".HDQBB1S 200447\n";
		sMessage15 += "HDQ1S PSXQRW/PD5C/99999999\n";
		sMessage15 += "1SILVA/KAMAL\n";
		sMessage15 += "CHNT\n";
		sMessage15 += "1SILVA/BIMALMR\n";
		sMessage15 += "QR301Y21JAN CMBDOH HK1/0835 1110\n";
		sMessage15 += "G9134Y22JAN DOHSHJ HK1\n";
		sMessage15 += "G9505Y23JAN SHJCMB HK1\n";
		sMessage15 += "SQ467Y24JAN CMBSIN HK1/0930 1555\n";
		sMessage15 += "OSI G9 TCP2 1SOYSA/FREDMR\n";

		String sMessage114 = "";
		sMessage114 = "SHJMMG9\n";
		sMessage114 += ".HDQBB1S 300555\n";
		sMessage114 += "HDQ1S JIAUSC/PD5C/99999999\n";
		sMessage114 += "1NAME/NONAME\n";
		sMessage114 += "G9505Y07AUG SHJCMB KL1/2200 0315/1\n";
		sMessage114 += "SSR LANG G9 HK/SHJCMB0505Y07AUG.SPEAK SINGHALA ONLY\n";
		sMessage114 += "SSR OTHS G9 HI ENTER CC WHEN MODIFYING PNR\n";
		sMessage114 += "OSI G9 CTCP CMB8555\n";

		String sMessage118 = "";
		sMessage118 = "SHJMMG9\n";
		sMessage118 += ".HDQBB1S 211029\n";
		sMessage118 += "HDQ1S PSXQRW/PD5C/99999999\n";
		sMessage118 += "1SILVA/BIMALMR 1SOYSA/FREDMR\n";
		sMessage118 += "G9505Y23JAN SHJCMB SS2/2200 0315/1\n";
		sMessage118 += "OSI G9 NXTM FP-NONE\r\n";
		sMessage118 += "OSI G9 CTCT CMB232323223 A\n";
		sMessage118 += "OSI G9 CTCB CMB234234234 B\n";
		sMessage118 += "OSI G9 CTCP CMB234234234 C\n";
		sMessage118 += "OSI G9 CTCP CMB234234234 M\n";
		sMessage118 += "OSI G9 CTCH CMB234234234 H\n";
		sMessage118 += "OSI G9 CTCF CMB234234234 F\n";
		sMessage118 += "OSI G9 CTCP CMB234234234 HTL\n";
		sMessage118 += "OSI G9 CTCP CMB234234234 E\n";
		sMessage118 += "OSI G9 CTCP CMB3232212\n";
		sMessage118 += "OSI G9 CTCP CMB234234234 T\n";

		/**
         
         */

		String sMessage119 = "";
		sMessage119 = "SHJMMG9\n";
		sMessage119 += ".HDQBB1S 211029\n";
		sMessage119 += "HDQ1S EXPMSQ/PD5C/99999999\n";
		sMessage119 += "SHJG9 DAM085\n";
		sMessage119 += "1SDSD/SDSD\n";
		sMessage119 += "G9505Y23AUG SHJCMB HK1\n";
		sMessage119 += "OSI G9 NXTM RL-DAM085\n";
		sMessage119 += "OSI G9 NXTM TL-NONE\n";
		sMessage119 += "OSI G9 NXTM FC-NONE\n";

		String sMessage120 = "";
		sMessage120 = "SHJMMG9\n";
		sMessage120 += ".HDQBB1S 210502\n";
		sMessage120 += "HDQ1S KLHJGX/PD5C/99999999\n";
		sMessage120 += "SHJG9 DAM034\n";
		sMessage120 += "1SANJE/DONSEK\n";
		sMessage120 += "G9505Y26AUG SHJCMB HK1\n";
		sMessage120 += "G9505Y25AUG SHJCMB HK1\n";
		sMessage120 += "OSI G9 NXTM RL-DAM034\n";
		sMessage120 += "OSI G9 NXTM TL-NONE\n";
		sMessage120 += "OSI G9 NXTM FC-NONE\n";
		sMessage120 += "OSI G9 NXTM TK-NONE\n";

		String sMessage121 = "";
		sMessage121 = "SHJMMG9\n";
		sMessage121 += ".HDQBB1S 061046\n";
		sMessage121 += "HDQ1S BDQARK/PD5C/99999999\n";
		sMessage121 += "1NAME/NONAME\n";
		sMessage121 += "G9505Y25AUG SHJCMB NN1/2200 0315/1\n";
		sMessage121 += "SSR UMNR G9 NN1 SHJCMB0505Y25AUG.07\n";
		sMessage121 += "OSI G9 NXTM RL-NONE\n";
		sMessage121 += "OSI G9 CTCP CMB7874745\n";

		String sMessage122 = "";
		sMessage122 = "SHJMMG9\n";
		sMessage122 += ".HDQBB1S 190919\n";
		sMessage122 += "HDQ1S CZZKEW/PD5C/99999999\n";
		sMessage122 += "2SIRNAME/A/B\n";
		sMessage122 += "G9505Y23AUG SHJCMB NN2/2200 0315/1\n";
		sMessage122 += "SSR NSST G9 NN1 SHJCMB0505Y23AUG-1SIRNAME/B\n";
		sMessage122 += "OSI G9 NXTM RL-NONE\n";
		sMessage122 += "OSI G9 CTCP CMB4545\n";

		String sMessage123 = "";
		sMessage123 = "QP SHJMMG9\n";
		sMessage123 += ".HDQBB1S 220838\n";
		sMessage123 += "DVD\n";
		sMessage123 += "HDQ1S CGEZAO/PD5C/99999999\n";
		sMessage123 += "SHJG9 DOM123\n";
		sMessage123 += "1FONS/A\n";
		sMessage123 += "G9506Y26AUG CMBSHJ XX1\n";
		sMessage123 += "G9505Y25AUG SHJCMB HK1\n";
		sMessage123 += "OSI G9 NXTM SSS-1ADSE/GG\n";

		String msg14 = "";
		msg14 = "SHJMMG9\n";
		msg14 += ".HDQBB1S 281214\n";
		msg14 += "HDQ1S MHDGPD/PD5C/99999999\n";
		msg14 += "2PERERA/A/B 2UUU/SGSG/JKJK 2SDSDS/DS/SDS\n";
		msg14 += "G95005Y01SEP SHJCMB NN6/2200 0315/1\n";
		msg14 += "SSR NSST G9 NN2 SHJCMB0505Y01SEP-1UUU/SGSG 1UUU/JKJK\n";
		msg14 += "OSI G9 NXTM RL-NONE\n";
		msg14 += "OSI G9 NXTM TL-TAW\n";
		msg14 += "OSI G9 NXTM FC-NONE\n";
		msg14 += "OSI G9 NXTM TK-NONE\n";
		msg14 += "OSI G9 NXTM FP-NONE\n";
		msg14 += "OSI G9 CTCP CMB44444\n";

		String msg124 = "";
		msg124 = "SHJMMG9\n";
		msg124 += ".HDQBB1S 300641\n";
		msg124 += "HDQ1S JICWYC/PD5C/99999999\n";
		msg124 += "1SANJEEWA/FONSEKA 1NONAME/A 1NONAME/B\n";
		msg124 += "G9505Y07AUG SHJCMB NN3/2200 0315/1\n";
		msg124 += "SSR TKNE NW HK1 MSPORD0122Y22AUG-1MACKIN/L.0054922111221C3/220-222\n";
		// msg124 += "SSR INFT G9 NN1 SHJCMB0505Y23AUG-1SANJEEWA/FONSEKA.08MTHS OCCUPYING\n";
		// msg124 += "SSR INFT G9 ///SEAT\n";
		msg124 += "OSI G9 CTCP CMB787878\n";

		String sMessage69 = "";
		sMessage69 = "HDQRMNW\n";
		sMessage69 += ".AMSRMKL\n";
		sMessage69 += "AMSKL AAABBB\n";
		sMessage69 += "SWI1G 6CQFYG\n";
		sMessage69 += "1CLARK/PMRS\n";
		sMessage69 += "CHNT\n";
		sMessage69 += "1CLARKSON/PMRS\n";
		sMessage69 += "KL611C20DEC AMSORD HK1\n";
		sMessage69 += "DI023Y21MAY02 STRFDH NN1/2120 2145 LJK\n";

		String msg31 = "";
		msg31 = "SHJMMG9\n";
		msg31 += ".HDQBB1S 110456\n";
		msg31 += "HDQ1S LDQRVF/PD5C/99999999\n";
		msg31 += "SHJG9 10000296\n";
		msg31 += "1JOHN/BRADSHOWMR 1BOBY/LASHLY\n";
		msg31 += "G9505Y15SEP SHJCMB XX2\n";
		msg31 += "G9505Y15SEP SHJCMB XX2/2200 0315\n";
		msg31 += "OSI G9 NXTM TK-NONE\n";

		String msg125 = "";
		msg125 = "SHJMMG9\n";
		msg125 += ".HDQBB1S 150546\n";
		msg125 += "HDQ1S IQRWJR/PD5C/99999999\n";
		msg125 += "SHJG9 DOM666\n";
		msg125 += "1NORTIK/HJKA\n";
		msg125 += "G9585Y23SEP SHJCMB SS1\n";
		msg125 += "G9585Y23SEP SHJCMB HK1\n";
		// msg125 += "SSR NSST G9 NN1 SHJCMB0585Y23SEP\n";
		msg125 += "OSI G9 NXTM TK-NONE\n";

		String msg127 = "";
		msg127 = "SHJMMG9\n";
		msg127 += ".HDQBB1S 191115\n";
		msg127 += "HDQ1S LOMSHJ/PD5C/99999999\n";
		msg127 += "1FONSEKA/SANJEEWA\n";
		msg127 += "G9585Y24JAN SHJCMB SS1/0445 0755\n";
		// msg127 += "SSR INFT G9 NN1 SHJCMB0505Y27JUN-1ANJALINA/JOLI.JERM/ROMIO 09JAN07\n";
		msg127 += "SSR OTHS YY CC AX371010374072700EXP 12 11\n";
		msg127 += "SSR OTHS YY CH SANJEEWA FONSEKA\n";
		msg127 += "SSR OTHS YY CSC256\n";
		// msg127 += "SSR OTHS G9 HI ENTER CC WHEN MODIFYING PNR\n";
		// msg127 += "SSR NSST G9 NN2 SHJCMB0505Y01SEP-1UUU/SGSG 1UUU/JKJK\n";
		// // msg127 += "OSI G9 NXTM TK-NONE\n";
		// msg127 += "OSI G9 CTCH CMB0094252142414 H\n";
		String msg128 = "";
		msg128 = "SHJMMG9\n";
		msg128 += ".HDQBB1S 150546\n";
		msg128 += "SPT\n";
		msg128 += "HDQ1S BZYMFQ/PD5C/99999999\n";
		msg128 += "SHJG9 06N8QE\n";
		msg128 += "1RATHNAYALA/AMILAMR\n";
		msg128 += "G9111Y22OCT SHJMCT HK1\n";
		msg128 += "OSI G9 NXTM TK-NONE\n";
		msg128 += "OSI G9 CTCP CMB773921620\n";

		String msg126 = "";
		msg126 = "SHJMMG9\n";
		msg126 += ".HDQBB1S 150546\n";
		msg126 += "HDQ1S IQSRWY/PD5C/99999999\n";
		msg126 += "1FONSEKA/SANJEEWA MR\n";
		msg126 += "G9585Y23SEP SHJCMB HK1\n";
		// msg126 += "SSR OTHS YY OSAG1245365D\n";
		// msg126 += "OSI G9 NXTM TK-NONE\n";
		msg126 += "SSR OTHS YY BA4005550000000001EXP 05 13\n";
		msg126 += "SSR OTHS YY CH ALLAN JACKSON\n";
		msg126 += "SSR OTHS YY CSC1111\n";

		final String MESSAGE_TO_BE_PASSED = msg126;
		// TODO Chek for the message indentifier CHG
		GDSMessageParser oGDSMessageParser = new GDSMessageParser();
		// try{
		BookingRequestDTO bookingRequestDTO = new BookingRequestDTO();
		MessageParserResponseDTO oMessageParserResponseDTO = oGDSMessageParser.parseMessage(MESSAGE_TO_BE_PASSED);

		bookingRequestDTO = (BookingRequestDTO) oMessageParserResponseDTO.getRequestDTO();
		if (bookingRequestDTO != null && bookingRequestDTO.isGroupBooking()) {
			System.out.println("!!XXXXXXXXXXXXXXXXXXXXXXXX Group booking XXXXXXXXXXXXXXXXXXXXXXXX\n");
		}
		System.out.println("Raw message \n" + MESSAGE_TO_BE_PASSED + "\nEnd of msg\n ---------------\n");
		if (bookingRequestDTO != null) {
			System.out.println("Sender = " + bookingRequestDTO.getOriginatorAddress());
			// System.out.println("Reciver = " + bookingRequestDTO.getResponderAddress());
			System.out.println("CommunicationReference = " + bookingRequestDTO.getCommunicationReference());
			System.out.println("MessageIdentifier = " + bookingRequestDTO.getMessageIdentifier());

			if (bookingRequestDTO != null && bookingRequestDTO.getOriginatorRecordLocator() != null) {
				RecordLocatorDTO originatorRL = bookingRequestDTO.getOriginatorRecordLocator();

				System.out.println("---------ORiginator RecordLocator begin------------");
				System.out.println("Booking office :" + originatorRL.getBookingOffice());
				System.out.println("PnrReference :" + originatorRL.getPnrReference());
				System.out.println("Office Code  :" + originatorRL.getTaOfficeCode());
				System.out.println("user Id      :" + originatorRL.getUserID());
				System.out.println("getPseudoCityCode  :" + originatorRL.getClosestCityAirportCode());
				System.out.println("getCariierCode     :" + originatorRL.getCariierCRSCode());
				System.out.println("UserType           :" + originatorRL.getUserType());
				System.out.println("CountryCode        :" + originatorRL.getCountryCode());
				System.out.println("CurrencyCode       :" + originatorRL.getCurrencyCode());
				System.out.println("AgentCode          :" + originatorRL.getAgentDutyCode());
				System.out.println("ErspId             :" + originatorRL.getErspId());
				System.out.println("FirstDeparturefrom :" + originatorRL.getFirstDeparturefrom());
				System.out.println("---------RecordLocator end------------");
			}

			if (bookingRequestDTO != null && bookingRequestDTO.getResponderRecordLocator() != null) {
				RecordLocatorDTO ResponderRL = bookingRequestDTO.getResponderRecordLocator();
				System.out.println("---------Responder RecordLocator begin------------");
				System.out.println("Booking office :" + ResponderRL.getBookingOffice());
				System.out.println("PnrReference :" + ResponderRL.getPnrReference());
				System.out.println("Office Code  :" + ResponderRL.getTaOfficeCode());
				System.out.println("user Id      :" + ResponderRL.getUserID());
				System.out.println("getPseudoCityCode    :" + ResponderRL.getClosestCityAirportCode());
				System.out.println("getCariierCode       :" + ResponderRL.getCariierCRSCode());
				System.out.println("UserType             :" + ResponderRL.getUserType());
				System.out.println("CountryCode          :" + ResponderRL.getCountryCode());
				System.out.println("CurrencyCode         :" + ResponderRL.getCurrencyCode());
				System.out.println("AgentCode            :" + ResponderRL.getAgentDutyCode());
				System.out.println("ErspId               :" + ResponderRL.getErspId());
				System.out.println("FirstDeparturefrom   :" + ResponderRL.getFirstDeparturefrom());
				System.out.println("---------Responder RecordLocator end------------");
			}

			Collection pax = bookingRequestDTO.getNewNameDTOs();
			if (pax.isEmpty()) {
				System.out.println("NO Newwwwwwwwwwwwwwwwwwwwwwww Names");
			}
			Iterator iter1 = pax.iterator();
			while (iter1.hasNext()) {
				Object object = iter1.next();
				System.out.println("***Name*************\nTitle = " + ((NameDTO) object).getPaxTitle() + " First Name = "
						+ ((NameDTO) object).getFirstName() + " lastName : " + ((NameDTO) object).getLastName()
						+ " group Magnitude : " + ((NameDTO) object).getGroupMagnitude() + "\n***********");
			}

			Collection collection = bookingRequestDTO.getBookingSegmentDTOs();
			if (collection.size() < 0) {
				System.out.println("Booking Segment Dto's are null");
			} else {
				for (Iterator iter = collection.iterator(); iter.hasNext();) {
					BookingSegmentDTO element = (BookingSegmentDTO) iter.next();
					System.out.println("\n-- Booking Segment Dto :");
					System.out.println("getCarrierCode()  :" + element.getCarrierCode());
					System.out.println("getFlightNumber() :" + element.getFlightNumber());
					System.out.println("getBookingCode()  : " + element.getBookingCode());
					System.out.println("getDepartureDate():" + element.getDepartureDate());
					System.out.println("getDepartureStation():" + element.getDepartureStation());
					System.out.println("getDestinationStation() : " + element.getDestinationStation());
					System.out.println("getActionCode()   :" + element.getActionOrStatusCode());
					System.out.println("NoofPax()   :" + element.getNoofPax());
					System.out.println("getDepartureTime():" + element.getDepartureTime());
					System.out.println("getArrivalTime()  :" + element.getArrivalTime());
					System.out.println("getDayChangeFlag():" + element.getDayOffSet());
					System.out.println("getCarrierCode():" + element.getCarrierCode());
					System.out.println("getMembersBlockIdentifier():" + element.getMembersBlockIdentifier());
					System.out.println("-- Booking Segment Dto End :");
				}
			}

			Collection c = bookingRequestDTO.getOsiDTOs();
			if (c.size() == 0) {
				System.out.println("NO OSI INFORMATIONS");
			} else {
				for (Iterator iter = c.iterator(); iter.hasNext();) {
					Object obj = iter.next();

					if (obj instanceof OtherServiceInfoDTO) {
						OtherServiceInfoDTO element = (OtherServiceInfoDTO) obj;

						System.out.println("\n--OSI info Begin :");
						System.out.println("getCarrierCode    :" + element.getCarrierCode());
						System.out.println("getCodeOSI     :" + element.getCodeOSI());
						System.out.println("getFullElement : " + element.getFullElement());
						List osiNameList = element.getOsiNameDTOs();
						if (osiNameList != null && !osiNameList.isEmpty()) {
							for (Iterator itr = osiNameList.iterator(); itr.hasNext();) {
								NameDTO osiName = (NameDTO) itr.next();
								System.out.println("OSI Names: : " + "Title = " + osiName.getPaxTitle() + " First Name = "
										+ osiName.getFirstName() + " LastName : " + osiName.getLastName() + " Group Magnitude : "
										+ osiName.getGroupMagnitude());
							}
						}
						System.out.println("getServiceInfo :" + element.getServiceInfo());
						System.out.println("getOsiValue :" + element.getOsiValue());
						System.out.println("\n--OSI info End  :");

					} else if (obj instanceof OSIPhoneNoDTO) {
						OSIPhoneNoDTO phoNoDTO = (OSIPhoneNoDTO) obj;

						System.out.println("\n--OSI Phone info Begin :");
						System.out.println("getCarrierCode    :" + phoNoDTO.getCarrierCode());
						System.out.println("getCodeOSI     :" + phoNoDTO.getCodeOSI());
						System.out.println("getFullElement : " + phoNoDTO.getFullElement());
						System.out.println("getPhoneNo  :" + phoNoDTO.getPhoneNo());
						System.out.println("getPhone_Code :" + phoNoDTO.getPhone_Code());
						System.out.println("getOsiValue :" + phoNoDTO.getOsiValue());
						System.out.println("\n--OSI Phone info End  :");

					}
				}
			}

			List changeNameDtoList = bookingRequestDTO.getChangedNameDTOs();
			if (changeNameDtoList != null) {
				System.out.println("%%%%%%%%%%%%%%%%%%%%%Change Names%%%%%%%%%%%%%%%%%%%%%%%");
				for (Iterator iterator = changeNameDtoList.iterator(); iterator.hasNext();) {
					ChangedNamesDTO changedNamesDTO = (ChangedNamesDTO) iterator.next();
					List oldNameList = changedNamesDTO.getOldNameDTOs();
					for (Iterator itr = oldNameList.iterator(); itr.hasNext();) {
						NameDTO oldname = (NameDTO) itr.next();
						System.out.println("Old Names: : " + "Title = " + oldname.getPaxTitle() + " First Name = "
								+ oldname.getFirstName() + " LastName : " + oldname.getLastName() + " Group Magnitude : "
								+ oldname.getGroupMagnitude());
					}

					List newNameList = changedNamesDTO.getNewNameDTOs();
					for (Iterator itr1 = newNameList.iterator(); itr1.hasNext();) {
						NameDTO newName = (NameDTO) itr1.next();
						System.out.println("New Names: : " + "Title = " + newName.getPaxTitle() + " First Name = "
								+ newName.getFirstName() + " LastName : " + newName.getLastName() + " Group Magnitude : "
								+ newName.getGroupMagnitude());
					}

				}

			} else {
				System.out.println("%%%%%%%%%%%%%NO CHNT HERE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			}

			List unChangedNames = bookingRequestDTO.getUnChangedNameDTOs();
			if (unChangedNames != null) {

				for (Iterator iter = unChangedNames.iterator(); iter.hasNext();) {
					UnchangedNamesDTO unch = (UnchangedNamesDTO) iter.next();
					List names = unch.getUnChangedNames();
					for (Iterator itr = names.iterator(); itr.hasNext();) {
						NameDTO oldname = (NameDTO) itr.next();
						System.out.println("UnChanged Names : : " + "Title = " + oldname.getPaxTitle() + " First Name = "
								+ oldname.getFirstName() + " LastName : " + oldname.getLastName() + " Group Magnitude : "
								+ oldname.getGroupMagnitude());
					}
					System.out.println("Party Total :" + unch.getPartyTotal());

				}

			}

			Collection ssrs = bookingRequestDTO.getSsrDTOs();
			System.out.println(ssrs.size());
			// iter = ssrs.iterator();

			for (Iterator iterator = ssrs.iterator(); iterator.hasNext();) {
				Object obj = iterator.next();
				if (obj instanceof SSRDetailDTO) {

					SSRDetailDTO ssrDTO = (SSRDetailDTO) obj;
					System.out.println("\n--special service Requests--");
					System.out.println("code SSR : " + ssrDTO.getCodeSSR());
					System.out.println("Carriercode: " + ssrDTO.getCarrierCode());
					System.out.println("SSR Value : " + ssrDTO.getSsrValue());
					System.out.println("action status code : " + ssrDTO.getActionOrStatusCode());
					System.out.println("no of pax : " + ssrDTO.getNoOfPax());
					System.out.println("Free text : " + ssrDTO.getFreeText());
					System.out.println("Full Element : " + ssrDTO.getFullElement());

					System.out.println("End of -special service Requests--");
					SegmentDTO segmentDTO = ssrDTO.getSegmentDTO();
					if (segmentDTO != null) {
						System.out.println("Date :" + segmentDTO.getDepartureDate());
						System.out.println("Departure   : " + segmentDTO.getDepartureStation());
						System.out.println("Destination :" + segmentDTO.getDestinationStation());
						System.out.println("Flight No   :" + segmentDTO.getFlightNumber());
						System.out.println("Booking Code:" + segmentDTO.getBookingCode());

					} else {
						System.out.println("Segment Null " + ssrDTO.getSegmentDTO());
					}
					List ssrNameList = ssrDTO.getSsrNameDTOs();
					if (ssrNameList != null) {
						for (Iterator nameDTOIterator = ssrNameList.iterator(); nameDTOIterator.hasNext();) {
							NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

							System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
									+ nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
						}
					} else {
						System.out.println("SSR NAME NOT AVAILABLE");
					}

				} else if (obj instanceof SSRInfantDTO) {

					SSRInfantDTO infantDTO = (SSRInfantDTO) obj;
					System.out.println("\n--INFANT DTO--");
					System.out.println("CodeSSR : " + infantDTO.getCodeSSR());
					System.out.println("isSeatRequired : " + infantDTO.isSeatRequired());
					System.out.println("InfantFirstName: " + infantDTO.getInfantFirstName());
					System.out.println("InfantLastName : " + infantDTO.getInfantLastName());
					System.out.println("InfantTitle : " + infantDTO.getInfantTitle());
					System.out.println("GuardiantFirstName : " + infantDTO.getGuardianFirstName());
					System.out.println("GuardiantLastName  : " + infantDTO.getGuardianLastName());
					System.out.println("GuardiantTitle : " + infantDTO.getGuardianTitle());
					System.out.println("Action_status_Code : " + infantDTO.getActionOrStatusCode());
					System.out.println("Advice_status_Code : " + infantDTO.getAdviceOrStatusCode());
					System.out.println("FullElement: " + infantDTO.getFullElement());
					System.out.println("getFreetext: " + infantDTO.getFreeText());

					System.out.println("End of INFANT DTO--");
					SegmentDTO segmentDTO = infantDTO.getSegmentDTO();

					System.out.println("End of INFANT Segment DTO--");
					if (segmentDTO != null) {
						System.out.println("Date :" + segmentDTO.getDepartureDate());
						System.out.println("Departure   : " + segmentDTO.getDepartureStation());
						System.out.println("Destination :" + segmentDTO.getDestinationStation());
						System.out.println("Flight No   :" + segmentDTO.getFlightNumber());
						System.out.println("Booking Code:" + segmentDTO.getBookingCode());

					}
					System.out.println("End of INFANT Segment DTO--");
				} else if (obj instanceof SSREmergencyContactDetailDTO) {

					System.out.println("\n----------------EmergencyContactDetailDTO------- ");
					SSREmergencyContactDetailDTO emContactDetailDTO = (SSREmergencyContactDetailDTO) obj;
					System.out.println("isRefused : " + emContactDetailDTO.isRefused());
					System.out.println("Carriercode : " + emContactDetailDTO.getCarrierCode());
					System.out.println("actioncode : " + emContactDetailDTO.getActionOrStatusCode());
					System.out.println("getEmContactName : " + emContactDetailDTO.getEmContactName());
					System.out.println("getEmContactNumberwithCountry : " + emContactDetailDTO.getEmContactNumberwithCountry());
					System.out.println("getPassengerFirstName() : " + emContactDetailDTO.getPassengerFirstName());
					System.out.println("getPassengerLastName() : " + emContactDetailDTO.getPassengerLastName());
					System.out.println("getFreeText() : " + emContactDetailDTO.getFreeText());
					System.out.println("getFullElement() : " + emContactDetailDTO.getFullElement());
					System.out.println("----------------EmergencyContactDetailDTO------- ");

				} else if (obj instanceof SSRPassportDTO) {
					System.out.println("\n----------------Passport DTO------- ");
					SSRPassportDTO passportDTO = (SSRPassportDTO) obj;
					System.out.println(" getCarrierCode: " + passportDTO.getCarrierCode());
					System.out.println(" getAction_status_Code: " + passportDTO.getActionOrStatusCode());
					System.out.println(" getPassengerPassportNo: " + passportDTO.getPassengerPassportNo());
					System.out.println(" getPassengerFamilyName: " + passportDTO.getPassengerFamilyName());
					System.out.println(" getPassengerGivenName: " + passportDTO.getPassengerGivenName());
					System.out.println(" getPassportCountryCode: " + passportDTO.getPassportCountryCode());
					System.out.println(" getPassengerDateOfBirth: " + passportDTO.getPassengerDateOfBirth());
					System.out.println(" getPassengerGender: " + passportDTO.getPassengerGender());
					System.out.println(" isMultiPassengerPassport: " + passportDTO.isMultiPassengerPassport());
					System.out.println(" getFreeText: " + passportDTO.getFreeText());

					List ssrPsptNameList = passportDTO.getPnrNameDTOs();
					if (ssrPsptNameList != null) {
						for (Iterator nameDTOIterator = ssrPsptNameList.iterator(); nameDTOIterator.hasNext();) {
							NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

							System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
									+ nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
						}
					} else {
						System.out.println("SSR Passport NOT AVAILABLE");
					}

					System.out.println("----------------Passport DTO------- ");

				} else if (obj instanceof SSRFoidDTO) {

					System.out.println("\n----------------FOID DTO------- ");
					SSRFoidDTO foidDTO = (SSRFoidDTO) obj;
					System.out.println(" getCarrierCode: " + foidDTO.getCarrierCode());
					System.out.println(" getAction_status_Code: " + foidDTO.getActionOrStatusCode());
					System.out.println(" getPassengerIdentificationType: " + foidDTO.getPassengerIdentificationType());
					System.out.println(" getPassengerIdentificationNo: " + foidDTO.getPassengerIdentificationNo());
					System.out.println(" getFullElement: " + foidDTO.getFullElement());

					List ssrfoidNameList = foidDTO.getPnrNameDTOs();
					if (ssrfoidNameList != null) {
						for (Iterator nameDTOIterator = ssrfoidNameList.iterator(); nameDTOIterator.hasNext();) {
							NameDTO nameDTO = (NameDTO) nameDTOIterator.next();

							System.out.println("SSR : Title = " + nameDTO.getPaxTitle() + " FirstName = "
									+ nameDTO.getFirstName() + " LastName : " + nameDTO.getLastName());
						}
					} else {
						System.out.println("SSR FOID NOT AVAILABLE");
					}

				} else if (obj instanceof SSRChildDTO) {

					System.out.println("\n----------------Child DTO------- ");
					SSRChildDTO child = (SSRChildDTO) obj;
					System.out.println(" getCodeSSR: " + child.getCodeSSR());
					System.out.println(" getCarrierCode: " + child.getCarrierCode());
					System.out.println(" getAction_status_Code: " + child.getActionOrStatusCode());
					System.out.println(" getChildFirstName: " + child.getFirstName());
					System.out.println(" getChildLastName: " + child.getLastName());
					System.out.println(" getChildTitle: " + child.getTitle());
					System.out.println(" getChildAge(): " + child.getDateOfBirth());
					System.out.println(" getFullElement: " + child.getFullElement());

				} else if (obj instanceof SSRMinorDTO) {

					System.out.println("\n----------------Minor DTO------- ");
					SSRMinorDTO minor = (SSRMinorDTO) obj;
					System.out.println(" getCodeSSR: " + minor.getCodeSSR());
					System.out.println(" getCarrierCode: " + minor.getCarrierCode());
					System.out.println(" getAction_status_Code: " + minor.getActionOrStatusCode());
					System.out.println(" getChildFirstName: " + minor.getFirstName());
					System.out.println(" getChildLastName: " + minor.getLastName());
					System.out.println(" getChildTitle: " + minor.getTitle());
					System.out.println(" getChildAge(): " + minor.getMinorAge());
					System.out.println(" getFullElement: " + minor.getFullElement());

				} else if (obj instanceof SSRCreditCardDetailDTO) {

					System.out.println("\n----------------SSR CArD DTO------- ");
					SSRCreditCardDetailDTO card = (SSRCreditCardDetailDTO) obj;
					System.out.println(" CodeSSR: " + card.getCodeSSR());
					System.out.println(" CardNo: " + card.getCardNo());
					System.out.println(" PinNo: " + card.getPinNumber());
					System.out.println(" CardType: " + card.getCardType());
					System.out.println(" ExpiryMonth: " + card.getExpiryMonth());
					System.out.println(" ExpiryYear: " + card.getExpiryYear());
					System.out.println(" car holder name: " + card.getCardHolderName());
					System.out.println(" FullElement: " + card.getSsrValue());

				} else if (obj instanceof SSRDTO) {

					System.out.println("\n----------------SSR DTO------- ");
					SSRDTO ssr = (SSRDTO) obj;
					System.out.println(" getCodeSSR: " + ssr.getCodeSSR());
					System.out.println(" getCarrierCode: " + ssr.getCarrierCode());
					System.out.println(" getAction_status_Code: " + ssr.getActionOrStatusCode());
					System.out.println(" SsrValue: " + ssr.getSsrValue());
					System.out.println(" FreeText: " + ssr.getFreeText());
					System.out.println(" FullElement: " + ssr.getFullElement());

				} else {
					System.out.println("invalide or unprocessed SSR DTO");
					throw new Exception("invalide or unprocessed SSR DTO");
				}
			}
		}
	}

	private void updateSsrs() {
		List<SSRDTO> ssrs = bookingRequestDTO.getSsrDTOs();
		if (ssrs != null) {
			for (SSRDTO ssr : ssrs) {
				if (ssr.getActionOrStatusCode().equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())) {
					ssr.setIncludeInResponse(false);
				}
			}
		}
	}
}
