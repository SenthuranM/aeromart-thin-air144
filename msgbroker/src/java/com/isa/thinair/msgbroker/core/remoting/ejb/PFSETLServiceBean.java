package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.airreservation.api.dto.etl.ETLMessageDataDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PFSMessageDataDTO;
import com.isa.thinair.airreservation.api.model.CodeSharePfs;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.CommonMessageProcessingBL;
import com.isa.thinair.msgbroker.core.bl.ETLComposer;
import com.isa.thinair.msgbroker.core.bl.PFSComposer;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.service.bd.PFSETLServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.PFSETLServiceBeanRemote;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;

@Stateless
@RemoteBinding(jndiBinding = "PFSETLService.remote")
@LocalBinding(jndiBinding = "PFSETLService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PFSETLServiceBean extends PlatformBaseSessionBean implements PFSETLServiceBeanLocal, PFSETLServiceBeanRemote {

	private final Log log = LogFactory.getLog(PFSETLServiceBean.class);
	private MessageDAO messageDAO;
	private AdminDAO adminDAO;
	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendPFSMessages(List<PFSDTO> pfsDTOList, String airlineCode, boolean isEmptyPfs) throws ModuleException {
		for (PFSDTO pfsDTO : pfsDTOList) {
			ServiceBearer airline;
			ServiceClient serviceClient;
			String ttyAddress;
			CodeSharePfs pfs = getCodeSharePfsEntry(pfsDTO);
			try {
				// fill out message
				String gdsCode = pfsDTO.getGdsCode();
				String carrierCode = pfsDTO.getCarrierCode();
				String msgType = MessageComposerConstants.Message_Identifier.PFS.toString();

				airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
				serviceClient = getAdminDAO().getServiceClient(gdsCode);
				ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode, msgType);
				boolean isPfsAlreadySent = MsgbrokerUtils.getReservationBD().checkCodeSharePfsAlreadySent(pfsDTO.getFlightDate(),
						pfsDTO.getFlightNumber(), pfsDTO.getDepartureAirport(), carrierCode);
				StringBuilder content = new StringBuilder();
				if (!isPfsAlreadySent) {
					if (ttyAddress != null && airline != null) {
						PFSComposer pfsComposer = new PFSComposer(isEmptyPfs);
						List<PFSMessageDataDTO> pfsMetaDataDTOList = pfsComposer.preparePFSMessageDataDTO(pfsDTO, ttyAddress,
								airline.getSitaAddress());

						if (pfsMetaDataDTOList != null && !pfsMetaDataDTOList.isEmpty()) {
							Integer msgRefId = null;

							String toEmailAddress = CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
									serviceClient.getEmailDomain());
							for (PFSMessageDataDTO pfsMessageDataDTO : pfsMetaDataDTOList) {
								String pfsMessage = pfsComposer.composePFSMessage(ttyAddress, airline.getSitaAddress(),
										pfsMessageDataDTO);
								if (!StringUtil.isNullOrEmpty(pfsMessage)) {
									content.append(pfsMessage);
									msgRefId = saveOutMessageRecord(ttyAddress, pfsMessage, carrierCode, msgType,
											airline.getOutgoingEmailFromAddress(), airline.getSitaAddress(), toEmailAddress);
								}								
							}

							pfs.setToAddress(ttyAddress);
							pfs.setPfsContent(content.toString());
							// only the last part PFS message id will be linked
							if (msgRefId != null)
								pfs.setRefMessageID(msgRefId);
						}

					} else {
						log.info("PFS will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
								+ gdsCode + ".");
						pfs.setStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
					}

					saveCodeSharePfs(pfs);
				} else {
					log.info("PFS already sent for following code-share carrier");
					log.info("Flight Date :" + pfsDTO.getFlightDate() + " , " + pfsDTO.getFlightNumber() + " , "
							+ pfsDTO.getDepartureAirport() + " , " + carrierCode);
				}

			} catch (ModuleException e) {
				pfs.setStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
				saveCodeSharePfs(pfs);
				log.error(e);
				this.sessionContext.setRollbackOnly();
				throw new ModuleException(e.getCause(), e.getExceptionCode());
			} catch (CommonsDataAccessException daex) {
				pfs.setStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
				saveCodeSharePfs(pfs);
				this.sessionContext.setRollbackOnly();
				log.error(daex);
				throw new ModuleException(daex, daex.getExceptionCode());
			}

		}

	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * return the message DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendETLMessages(List<ETLDTO> etlDTOList, String airlineCode) throws ModuleException {

		for (ETLDTO etlDTO : etlDTOList) {
			ServiceBearer airline;
			ServiceClient serviceClient;
			String ttyAddress;
			CodeShareETL codeShareEtl = getCodeShareEtlEntry(etlDTO);
			try {
				// fill out message

				String gdsCode = etlDTO.getGdsCode();
				String msgType = MessageComposerConstants.Message_Identifier.ETL.toString();

				airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
				serviceClient = getAdminDAO().getServiceClient(gdsCode);
				ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode, msgType);
				if (ttyAddress != null && airline != null) {
					String toEmailAddress = CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
							serviceClient.getEmailDomain());
					codeShareEtl.setToAddress(ttyAddress);

					ETLComposer etlComposer = new ETLComposer();
					List<ETLMessageDataDTO> etlMetaDTOParts = etlComposer.getETLMessageParts(etlDTO, ttyAddress,
							airline.getSitaAddress());
					StringBuilder etlContent = new StringBuilder();
					Integer msgRefId = null;
					if (!etlMetaDTOParts.isEmpty()) {
						for (ETLMessageDataDTO etlMetaDTO : etlMetaDTOParts) {
							String etlMessage = etlComposer.composeETLMessage(ttyAddress, airline.getSitaAddress(), etlMetaDTO);
							if(!StringUtil.isNullOrEmpty(etlMessage)){
								etlContent.append(etlMessage);
								msgRefId = saveOutMessageRecord(ttyAddress, etlMessage, etlDTO.getCarrierCode(), msgType,
										airline.getOutgoingEmailFromAddress(), airline.getSitaAddress(), toEmailAddress);
							}							
						}
					}

					codeShareEtl.setEtlContent(etlContent.toString());

					if (msgRefId != null)
						codeShareEtl.setRefMessageID(msgRefId);
				} else {
					log.info("ETL will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
							+ gdsCode + ".");
					codeShareEtl.setStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				}
				saveCodeShareEtl(codeShareEtl);

			} catch (ModuleException e) {
				codeShareEtl.setStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				saveCodeShareEtl(codeShareEtl);
				log.error(e);
				this.sessionContext.setRollbackOnly();
				throw new ModuleException(e.getCause(), e.getExceptionCode());
			} catch (CommonsDataAccessException daex) {
				codeShareEtl.setStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				saveCodeShareEtl(codeShareEtl);
				this.sessionContext.setRollbackOnly();
				log.error(daex);
				throw new ModuleException(daex, daex.getExceptionCode());
			}

		}

	}

	private CodeSharePfs getCodeSharePfsEntry(PFSDTO pfsDTO) {
		CodeSharePfs pfs = new CodeSharePfs();
		pfs.setCsCarrierCode(pfsDTO.getCarrierCode());
		pfs.setCsFlightDesignator(pfsDTO.getFlightNumber());
		pfs.setDepartureDate(pfsDTO.getFlightDate());
		pfs.setFromAirport(pfsDTO.getDepartureAirport());
		pfs.setProcessedDate(new Date());
		pfs.setVersion(-1);
		pfs.setStatus(ReservationInternalConstants.PfsStatus.PARSED);

		return pfs;
	}

	private CodeShareETL getCodeShareEtlEntry(ETLDTO etlDTO) {

		CodeShareETL etl = new CodeShareETL();
		etl.setCarrierCode(etlDTO.getCarrierCode());
		etl.setFlightDesignator(etlDTO.getFlightNumber());
		etl.setDepartureDate(etlDTO.getFlightDate());
		etl.setFromAirport(etlDTO.getDepartureAirport());
		etl.setToAirport(etlDTO.getArrivalAirport());
		etl.setProcessedDate(new Date());
		etl.setVersion(-1);
		etl.setStatus(ParserConstants.ETLProcessStatus.PROCESSED);

		return etl;
	}

	private void saveCodeSharePfs(CodeSharePfs pfs) throws ModuleException {
		MsgbrokerUtils.getReservationBD().saveCodeSharePfsEntry(pfs);
	}

	private void saveCodeShareEtl(CodeShareETL codeShareEtl) throws ModuleException {
		MsgbrokerUtils.getETLBD().saveCodeShareETLEntry(codeShareEtl);
	}

	private Integer saveOutMessageRecord(String ttyAddress, String pfsMessage, String carrierCode, String msgType,
			String fromEmailAddress, String fromSitaAddress, String toEmailAddress) {
		if (!StringUtil.isNullOrEmpty(pfsMessage) && !StringUtil.isNullOrEmpty(msgType)
				&& (!StringUtil.isNullOrEmpty(toEmailAddress) || !StringUtil.isNullOrEmpty(ttyAddress))) {
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(carrierCode);
			outMessage.setMessageType(msgType);
			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			outMessage.setOutMessageText(pfsMessage);
			outMessage.setReceivedDate(new Date());
			outMessage.setRecipientEmailAddress(toEmailAddress);
			outMessage.setRecipientTTYAddress(ttyAddress);
			outMessage.setSentEmailAddress(fromEmailAddress);
			outMessage.setSentTTYAddress(fromSitaAddress);
			outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
			getMessageDAO().saveOutMessage(outMessage);
			return outMessage.getOutMessageID();
		}
		return null;
	}

}
