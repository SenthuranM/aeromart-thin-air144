package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceBearer.MessagingMode;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

public class GDSMessageHandler {

	private static Log log = LogFactory.getLog(GDSMessageHandler.class);

	private List<String> messageTypes;

	private String senderAddress;

	protected Map<ServiceBearer.MessagingMode, List<ServiceBearer>> serviceBearers;

	protected AdminDAO adminDAO;
	protected MessageDAO messageDAO;

	public enum Direction {
		IN, OUT
	}

	public enum Category {
		RESERVATION, SCHEDULE, INVENTORY, RESPONSE
	}

	public GDSMessageHandler() {
		serviceBearers = new HashMap<ServiceBearer.MessagingMode, List<ServiceBearer>>();
		adminDAO = LookupUtil.lookupAdminDAO();
		messageDAO = LookupUtil.lookupMessageDAO();
	}

	public void execute(Direction direction, Category category, boolean useGDSConfiguration) {
		List<ServiceBearer> airlineDomainObjects;
		Map<String, ServiceBearer> applicableBearers = new HashMap<String, ServiceBearer>();
		ServiceBearer.MessagingMode messagingMode;
		GdsTypeBMessageGateway gateway;
		Set<ServiceBearer.MessagingMode> externalMessagingModes = new HashSet<ServiceBearer.MessagingMode>();
		Set<ServiceBearer.MessagingMode> messagingModes = new HashSet<ServiceBearer.MessagingMode>();
		List<Integer> unprocessedMsgIds = new ArrayList<Integer>();
		
		if (useGDSConfiguration) {
			try {
				airlineDomainObjects = adminDAO.getServiceBearerAirlines();

				for (ServiceBearer airline : airlineDomainObjects) {
					if (airline.getRecordStatus()) {
						if (!applicableBearers.containsKey(airline.getGdsCode())) {
							applicableBearers.put(airline.getGdsCode(), airline);
						}
					} else if (applicableBearers.containsKey(airline.getGdsCode())) {
						applicableBearers.remove(airline.getGdsCode());
					}
				}

				for (ServiceBearer serviceBearer : applicableBearers.values()) {
					messagingMode = ServiceBearer.MessagingMode.valueOf(serviceBearer.getMessagingMode());

					if (!serviceBearers.containsKey(messagingMode)) {
						serviceBearers.put(messagingMode, new ArrayList<ServiceBearer>());
					}

					serviceBearers.get(messagingMode).add(serviceBearer);
				}			
				
				messagingModes.addAll(serviceBearers.keySet());					
				if (direction == Direction.OUT && category == GDSMessageHandler.Category.SCHEDULE) {
					List<String> externalMessagingModesList = new ArrayList<String>();
					externalMessagingModesList = adminDAO.getPendingExternalSystemMessagingModes();

					for (String externalSystemOutMessageMode : externalMessagingModesList) {
						externalMessagingModes.add(ServiceBearer.MessagingMode.valueOf(externalSystemOutMessageMode));
					}
					messagingModes.addAll(externalMessagingModes);
				}

				
				
				for (ServiceBearer.MessagingMode mode : messagingModes) {
					gateway = getTypeBMessageGateway(mode, direction);
					if(serviceBearers.get(mode) == null){
						continue;
					}
					gateway.setApplicableServiceBearers(serviceBearers.get(mode));

					if (direction == Direction.IN) {
						gateway.receiveMessages(true, unprocessedMsgIds);
					} else if (direction == Direction.OUT) {
						gateway.sendMessages(messageTypes, true);
					}
				}

			} catch (Exception ex) {
				log.error("GDSMessageHandler ---- ", ex);
			}
		} else {
			gateway = getTypeBMessageGateway(MessagingMode.FILE, Direction.OUT);
			
			log.info("####SITATEXT#### procesing message via " + gateway.getClass());
			if (direction == Direction.IN) {
				gateway.receiveMessages(false, unprocessedMsgIds);
			} else if (direction == Direction.OUT) {
				gateway.sendMessages(messageTypes, false);
			}
		}
		
		addBatchForMessageSequenceProcessing(direction);
	}

	private GdsTypeBMessageGateway getTypeBMessageGateway(ServiceBearer.MessagingMode messagingMode, Direction direction) {
		GdsTypeBMessageGateway gateway = null;
		switch (messagingMode) {
		case FILE:
			gateway = new GDSMessageGatewayFileImpl();
			break;
		case MAIL:
			switch (direction) {
			case IN:
				gateway = new GDSEMailRetriever();
				break;
			case OUT:
				gateway = new OutboundMessageHandler();
				break;
			}
			break;
		}

		return gateway;
	}

	public List<String> getMessageTypes() {
		return messageTypes;
	}

	public void setMessageTypes(List<String> messageTypes) {
		this.messageTypes = messageTypes;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}
	
	private void addBatchForMessageSequenceProcessing(Direction direction) {
		if (Direction.IN == direction && AppSysParamsUtil.isEnableSequentialMessageProcessing()) {
			List<String> msgTypesForSequentialProcessing = AppSysParamsUtil.getSequentialProcessingAllowedMessageTypes();
			if (msgTypesForSequentialProcessing != null && !msgTypesForSequentialProcessing.isEmpty()) {
				if (messageDAO.isUnProcessedMsgWaitingForSequentialProcessing(msgTypesForSequentialProcessing)) {
					LookupUtil.lookupMessageDAO().addBatchForMessageSequenceProcessingQueue();
				}
			}
		}

	}
}
