package com.isa.thinair.msgbroker.core.remoting.ejb;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBGatewayDto;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.IBMsgProcessorBD;
import com.isa.thinair.msgbroker.core.bl.MessageBeanFactory;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.dto.SitaTexMessage;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.Date;

@MessageDriven(name = "TypeBGatewayMdb", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/typeBGatewayQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class TypeBGatewayMdb implements MessageListener {

	private Log log = LogFactory.getLog(TypeBGatewayMdb.class);

	public void onMessage(Message message) {
		TypeBGatewayDto dto;
		SitaTexMessage msg;
		String messageCategory = null;
		ServiceBearer serviceBearer = null;

		MsgBrokerModuleConfig config = MsgbrokerUtils.getMsgbrokerConfigs();
		ForceLoginInvoker.login(config.getEjbAccessUsername(), config.getEjbAccessPassword());

		MessageDAO messageDAO = LookupUtil.lookupMessageDAO();

		ObjectMessage objMsg = (ObjectMessage) message;

		OutMessage outMessage = null;
		OutMessageProcessingStatus outMessageProcessingStatus = null;
		IBMsgProcessorBD ibMessageProcessor;

		try {
			Object object = objMsg.getObject();
			if (object instanceof TypeBGatewayDto) {
				dto  = (TypeBGatewayDto) object;

				switch (dto.getMessageType()) {
				case RESERVATION:
					messageCategory = Constants.ServiceTypes.ReservationService;
					break;
				}

				serviceBearer = LookupUtil.lookupAdminDAO().getServiceBearer(dto.getGdsCarrierCode());

				msg = new SitaTexMessage(dto.getMessage(), SitaTexMessage.MessageType.valueOf(serviceBearer.getMessageType()));

				// write to db
				InMessage inMessage = new InMessage();
				inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.LOCKED);
				inMessage.setAirLineCode(AppSysParamsUtil.getDefaultCarrierCode());
				inMessage.setInMessageText(dto.getMessage());
				inMessage.setReceivedDate(new Date());
				inMessage.setMessageType(messageCategory);
				inMessage.setSentTTYAddress(msg.getMessageHeader().getOriginAddress());
				inMessage.setRecipientTTYAddress(serviceBearer.getSitaAddress());
				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
				inMessage.setEditableStatus(true);
				inMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.SitaTex);
				inMessage = messageDAO.saveAndReLoad(inMessage);

				InMessageProcessingStatus inMessageProcessingStatus = new InMessageProcessingStatus();
				inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
				inMessageProcessingStatus
						.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_MESSAGE_RECEIVED);
				inMessageProcessingStatus.setProcessingDate(new Date());
				if (!msg.isParseFailed()) {
					inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
				} else {
					inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				}
				MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessageProcessingStatus);


				// invoke type-b + save out message
				ibMessageProcessor = MessageBeanFactory.getIBMsgProcessor(inMessage.getMessageType());
				if (ibMessageProcessor != null) {
					InMessageProcessResultsDTO inMessageProcessResultsDTO = ibMessageProcessor.processAIRIMPMessage(inMessage);
					if (inMessageProcessResultsDTO != null) {
						if (inMessageProcessResultsDTO.getInMessage() != null) {
							inMessage = inMessageProcessResultsDTO.getInMessage();
						}
						if (inMessageProcessResultsDTO.getInMessageProcessingStatus() != null) {
							inMessageProcessingStatus = inMessageProcessResultsDTO.getInMessageProcessingStatus();
							inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
							MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessageProcessingStatus);
						}
						if (inMessageProcessResultsDTO.getOutMessage() != null) {
							outMessage = inMessageProcessResultsDTO.getOutMessage();
							MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
							if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
								outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								messageDAO.saveOutMessageProcessingStatus(outMessageProcessingStatus);
							}
						}
					} else {
						log.error("No Implementation for the message type : " + inMessage.getMessageType());
					}
				} else {
					log.error("No Implementation for the message type : " + inMessage.getMessageType());
				}
			}
		} catch (JMSException e) {
			log.error("Messaging Error -- ", e);
		} catch (ModuleException e) {
			log.error("Error occurred -- ", e);
		}

	}
}
