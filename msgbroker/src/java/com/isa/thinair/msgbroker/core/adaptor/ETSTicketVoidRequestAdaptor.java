package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.ets.SimplifiedTicketServiceWrapper;
import com.isa.thinair.commons.api.dto.ets.VoidTKTREQDTO;

public class ETSTicketVoidRequestAdaptor
		extends AbstractGrpcRequestCreator<SimplifiedTicketServiceWrapper.VoidTKTREQ, VoidTKTREQDTO> {

	public ETSTicketVoidRequestAdaptor() {
		super(SimplifiedTicketServiceWrapper.VoidTKTREQ.class, VoidTKTREQDTO.class);
	}
}
