package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentTypeBRS;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRS;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.GdsDtoTransformer;
import com.isa.thinair.msgbroker.core.bl.IBMsgProcessorBD;
import com.isa.thinair.msgbroker.core.bl.InMessageParser;
import com.isa.thinair.msgbroker.core.bl.MessageBeanFactory;
import com.isa.thinair.msgbroker.core.bl.OutboundTypeBMessageAssembler;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.service.bd.TypeBServiceBeanLocal;
import com.isa.thinair.msgbroker.core.service.bd.TypeBServiceBeanRemote;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.MessageRequestProcessingUtil;

/**
 * @author Manoj Dhanushka
 */
@Stateless
@RemoteBinding(jndiBinding = "TypeBService.remote")
@LocalBinding(jndiBinding = "TypeBService.local")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TypeBServiceBean extends PlatformBaseSessionBean implements TypeBServiceBeanLocal, TypeBServiceBeanRemote {

	private static Log log = LogFactory.getLog(TypeBServiceBean.class);

	private MessageDAO messageDAO;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TypeBRS processTypeBMessage(TypeBRQ typeBRQ) throws ModuleException {
		
		log.info(" -------TypeBServiceBean : processTypeBMessage started------- ");
		TypeBRS typeBRS = new TypeBRS();
		try {
			IBMsgProcessorBD ibMessageProcessor = null;
			InMessage inMessage = null;
			InMessageProcessingStatus inMessageProcessingStatus = null;
			OutMessage outMessage = null;
			OutMessageProcessingStatus outMessageProcessingStatus = null;
			String pnr = null;
			InventoryAdjustmentTypeBRS invAdjTypeBRS;
			String msgType = null;
			String[] resultStr = MessageRequestProcessingUtil.getMessageIdentifier(typeBRQ.getTypeBMessage(), false);
			if (Constants.ServiceTypes.SSM.equals(resultStr[0])) {
				msgType = Constants.ServiceTypes.SSM;
			} else if (Constants.ServiceTypes.ASM.equals(resultStr[0])) {
				msgType = Constants.ServiceTypes.ASM;
			} else if (Constants.ServiceTypes.AVS.equals(resultStr[0])) {
				msgType = Constants.ServiceTypes.AVS;
			} else {
				msgType = Constants.ServiceTypes.ReservationService;
			}			
			
			log.info(" -------TypeBServiceBean : saveInMessage------- ");
			if (typeBRQ.isSaveInOutMsg()) {
				saveInMessage(typeBRQ, msgType);
			}
			ibMessageProcessor = MessageBeanFactory.getIBMsgProcessor(msgType);
			log.info(" -------TypeBServiceBean : processERIMPMessage------- ");
			InMessageProcessResultsDTO inMessageProcessResultsDTO = ibMessageProcessor.processAIRIMPMessage(typeBRQ);
			if (inMessageProcessResultsDTO != null) {
				if (inMessageProcessResultsDTO.getInMessage() != null) {
					inMessage = inMessageProcessResultsDTO.getInMessage();
				}
				if (inMessageProcessResultsDTO.getInMessageProcessingStatus() != null) {
					inMessageProcessingStatus = inMessageProcessResultsDTO.getInMessageProcessingStatus();
					if (inMessageProcessingStatus.getProcessingStatus().equals(
							MessageProcessingConstants.InMessageProcessStatus.Processed)) {
						if (inMessageProcessResultsDTO.getOutMessage() != null) {
							outMessage = inMessageProcessResultsDTO.getOutMessage();
							if (typeBRQ.isSaveInOutMsg()) {
								
								if(outMessage.getMessageType() != null && 
										!outMessage.getMessageType().equals(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode())){
									outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Sent);
								}
								
								outMessage.setRecordLocator(inMessage.getRecordLocator());
								outMessage.setMessageType(inMessage.getMessageType());
								log.info(" -------TypeBServiceBean : saveOutMessage------- ");
								MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
								if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
									outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									log.info(" -------TypeBServiceBean : saveOutMessageProcessingStatus------- ");
									getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
								}
							} else if (typeBRQ.getChannel() != null && typeBRQ.getChannel() == TypeBRQ.Channel.TYPE_A &&
									inMessageProcessResultsDTO.isSendNotifications()) {

								outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								outMessage.setRecordLocator(inMessage.getRecordLocator());
								outMessage.setMessageType(inMessage.getMessageType());
								log.info(" -------TypeBServiceBean : saveOutMessage------- ");
								MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
								if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
									outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									log.info(" -------TypeBServiceBean : saveOutMessageProcessingStatus------- ");
									getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
								}
							} else if (typeBRQ.getChannel() != null && typeBRQ.getChannel() == TypeBRQ.Channel.TYPE_A &&
									inMessageProcessResultsDTO.isSendNotifications()) {

								outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								outMessage.setRecordLocator(inMessage.getRecordLocator());
								outMessage.setMessageType(inMessage.getMessageType());
								log.info(" -------TypeBServiceBean : saveOutMessage------- ");
								MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
								if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
									outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									log.info(" -------TypeBServiceBean : saveOutMessageProcessingStatus------- ");
									getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
								}
							} else if (typeBRQ.getChannel() != null && typeBRQ.getChannel() == TypeBRQ.Channel.TYPE_A &&
									inMessageProcessResultsDTO.isSendNotifications()) {

								outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								outMessage.setRecordLocator(inMessage.getRecordLocator());
								outMessage.setMessageType(inMessage.getMessageType());
								log.info(" -------TypeBServiceBean : saveOutMessage------- ");
								MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
								if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
									outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									log.info(" -------TypeBServiceBean : saveOutMessageProcessingStatus------- ");
									getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
								}
							}
							pnr = outMessage.getPnr();
							typeBRS.setResponseMessage(outMessage.getOutMessageText());
							if (outMessage.isProcessSuccess()) {
								typeBRS.setResponseStatus(TypeBRS.ResponseStatus.SUCCESSFUL);
							} else {
								typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
							}
						}
					} else if(inMessageProcessingStatus.getProcessingStatus().equals(
							MessageProcessingConstants.InMessageProcessStatus.Unparsable)){
						
						if (inMessageProcessResultsDTO.getOutMessage() != null) {
							outMessage = inMessageProcessResultsDTO.getOutMessage();
							if (typeBRQ.isSaveInOutMsg()) {
								
								outMessage.setMessageType(msgType);
								log.info(" -------TypeBServiceBean : saveOutMessage------- ");
								MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
								if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
									outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									log.info(" -------TypeBServiceBean : saveOutMessageProcessingStatus------- ");
									getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
								}
							}
							typeBRS.setResponseMessage(outMessage.getOutMessageText());
						}
						
						typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
						
					} else {
						typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
						typeBRS.setResponseMessage("Parsing message failed");
					}
				}
				if (typeBRQ.getRequestType() == TypeBRQ.RequestType.INVENTORY_ADJUSTMENT) {
					invAdjTypeBRS = new InventoryAdjustmentTypeBRS();

					invAdjTypeBRS.setMessageError(inMessageProcessResultsDTO.getMessageError());
					invAdjTypeBRS.setPnr(pnr);

					typeBRS = invAdjTypeBRS;
				}
			} else {
				String err = "No Implementation for the message type " + inMessage != null ? inMessage.getMessageType() : "null";
				typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
				typeBRS.setResponseMessage(err);
				log.error(err);
			}
		} catch (Exception e) {
			typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
			typeBRS.setResponseMessage("Processing message failed");
			log.error(e);
		}
		return typeBRS;
	}

	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public TypeBRS composeAndSendTypeBMessage(TypeBRQ typeBRQ) throws ModuleException {

		log.debug("Inside sendTypeBMessage of TypeBServiceBean");
		TypeBRS typeBRS = new TypeBRS();

		try {
			InMessage inMessage = null;
			OutMessage outMessage = null;
			OutMessageProcessingStatus outMessageProcessingStatus = null;

			OutboundTypeBMessageAssembler outboundTypeBMessageAssembler = new OutboundTypeBMessageAssembler();
			InMessageProcessResultsDTO inMessageProcessResultsDTO = outboundTypeBMessageAssembler.assembleOutboundTypeBMessage(typeBRQ);
			if (inMessageProcessResultsDTO != null) {
				if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
					if (inMessageProcessResultsDTO.getOutMessage() != null) {
						outMessage = inMessageProcessResultsDTO.getOutMessage();
						MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
						if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
							outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
							outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
							getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
						}
						log.debug("Finished sendTypeBMessage successfully.");
						if (outMessage.getModeOfCommunication().equals(MessageProcessingConstants.ModeOfCommunication.API)) {
							typeBRQ.setTypeBMessage(outMessage.getOutMessageText());
							typeBRQ.setWsServerUrl(outMessage.getWsServerUrl());
							return MsgbrokerUtils.getWSClientBD().processTypeBResMessage(typeBRQ);
						}
						typeBRS.setResponseStatus(TypeBRS.ResponseStatus.SUCCESSFUL);
						typeBRS.setResponseMessage(outMessage.getOutMessageText());
						return typeBRS;
					}
				}
			} else {
				log.error("No Implementation for the message type " + inMessage != null ? inMessage.getMessageType() : "null");
			}
			log.debug("Exit sendTypeBMessage. OutMessage doesn't save.");
			typeBRS.setResponseStatus(TypeBRS.ResponseStatus.FAILED);
			return typeBRS;
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("Processing message failed", e);
		}
	}

	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String composeAndSendTypeBMessages(List<TypeBRQ> typeBRQs) throws ModuleException {

		try {
			InMessage inMessage = null;
			OutMessage outMessage = null;
			OutMessageProcessingStatus outMessageProcessingStatus = null;

			OutboundTypeBMessageAssembler outboundTypeBMessageAssembler = new OutboundTypeBMessageAssembler();
			List<InMessageProcessResultsDTO> inMessageProcessResultsDTOs = outboundTypeBMessageAssembler.prepareOutboundTypeBMessages(typeBRQs);
			if (inMessageProcessResultsDTOs != null) {
				for (InMessageProcessResultsDTO inMessageProcessResultsDTO : inMessageProcessResultsDTOs) {
					if (inMessageProcessResultsDTO.getInMessageProcessingStatus() != null) {
						if (inMessageProcessResultsDTO.getOutMessage() != null) {
							outMessage = inMessageProcessResultsDTO.getOutMessage();
							MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
							if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
								outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
								getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
							}
							// return outMessage.getOutMessageText();
						}
					}
				}
			} else {
				log.error("No Implementation for the message type " + inMessage != null ? inMessage.getMessageType() : "null");
			}
			return null;
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("Processing message failed", e);
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public TypeBAnalyseRS analyseTypeBMessage(TypeBAnalyseRQ typeBAnalyseRQ) throws ModuleException {
		TypeBAnalyseRS analyseRS = new TypeBAnalyseRS();

		InMessageParser inMessageParser = new InMessageParser();
		MessageParserResponseDTO responseDTO = inMessageParser.parseMessage(typeBAnalyseRQ.getTypeBMessage());

		BookingRequestDTO bookingRequestDTO;

		if (responseDTO != null) {
			if (responseDTO.getRequestDTO() != null && responseDTO.getRequestDTO() instanceof BookingRequestDTO) {
				bookingRequestDTO = (BookingRequestDTO) responseDTO.getRequestDTO();
				analyseRS = GdsDtoTransformer.toTypeBAnalyseRS(bookingRequestDTO);
			}
		}

		return analyseRS;
	}

	private void saveInMessage(TypeBRQ typeBRQ, String msgType) throws ModuleException {
		
		InMessage inMessage = new InMessage();
		inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.LOCKED);
		inMessage.setAirLineCode(AppSysParamsUtil.getDefaultCarrierCode());
		inMessage.setInMessageText(typeBRQ.getTypeBMessage());
		inMessage.setReceivedDate(new Date());
//		inMessage.setMessageType(Constants.ServiceTypes.ReservationService);
		inMessage.setMessageType(msgType);
		inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
		inMessage.setEditableStatus(true);
		inMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.API);
		inMessage = MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessage(inMessage);

		InMessageProcessingStatus inMessagePassingErrors = new InMessageProcessingStatus();
		inMessagePassingErrors.setInMessageID(inMessage.getInMessageID());
		inMessagePassingErrors
				.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_MESSAGE_RECEIVED);
		inMessagePassingErrors.setProcessingDate(new Date());
		inMessagePassingErrors.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
		MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessagePassingErrors);
	}

	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}
}
