package com.isa.thinair.msgbroker.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.Action;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageDTO;
import com.isa.thinair.msgbroker.api.dto.TypeBMessageSearchDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.MessageSequenceQueue;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.RequestType;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;

public interface MessageDAO {

	// Message

	// public String getEmailAddressForTTYAddress(String ttyAddress);

	public void saveOutMessage(OutMessage oOutMessage);

	public void saveInMessage(InMessage oInMessage);

	public InMessage saveAndReLoad(InMessage in);

	public void saveInMessageProcessingStatus(InMessageProcessingStatus inMessageProcessingStatus);

	public void saveOutMessageProcessingStatus(OutMessageProcessingStatus outMessageProcessingStatus);

	public Collection getOutMessagesInFifoOrderForGds(String outMessageProcessStatus, List<String> messageTypes, List<String> gdsCodes);
	
	public Collection getOutMessagesInFifoOrder(String outMessageProcessStatus, List<String> messageTypes);
	
	public Collection getOutMessageIdsInFifoOrderForGds(String outMessageProcessStatus, List<String> messageTypes, List<String> gdsCodes);
	
	public Collection getOutMessageIdsInFifoOrder(String outMessageProcessStatus, List<String> messageTypes);

	public Map getActiveMessageTypeTimeToLives();

	// public Collection getInMessages(Date receiveDate, Date processedDate, String messageType, String
	// processingStatus) throws ModuleException;
	// public Collection getInMessages(Date receiveDate, Date processedDate, String messageType, String
	// processingStatus,
	// String senderEmailAddress, String recipientEmailAddress, String messageDirection) throws ModuleException;

	public Collection getOutMessagesWithErrors(Date receiveDate, Date processedDate, String messageType, String processingStatus,
			String senderEmailAddress, String recipientEmailAddress) throws ModuleException;

	public OutMessage getOutMessage(Integer outMessageID);

	public Collection<Integer> getInMessageIDsInFifoOrder(String inMessageProcessStatus);

	public List<Integer> getLockableInMessageIDsInFifoOrder(String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing);
	
	public List<InMessage> getAllLockableInMessagesInFifoOrder(String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing);

	public RequestType getRequestType(String requestTypeCode);

	public InMessage getInMessage(Integer messageID);

	public List<TypeBMessageDTO> searchTypeBMessageByPnr(TypeBMessageSearchDTO typeBMessageSearchDTO, List<String> messageTypes);

	public void saveTypeBRecipientExtSystem(TypeBRecipientExtSystem typeBRecipientExtSystem);
	
	public boolean isScheduleLockForProcessing(Integer inMsgProcessingQueueId);

	public void updateOutMessagesResponseStatus(String messageSequenceReference, Action action);

	public Integer retrieveInMsgProcessingQueueId(String newFlightDesignator);
	
	public void updateScheduleForLockStatus(Integer msgProcessingQueueId, String msgStatus, Integer inMsgId);

	public void addMessageForWaitingQueue(Integer msgProcessingQueueId, Integer inMessageId);	
	
	public Integer getFlightDesignatorForPostProcessing(Integer inMsgId, String status);
	
	public Integer getNextInMsgIdInQueue(Integer msgProcessingQueueId);	
	
	public void removeMessageFromWaitingQueue(Integer msgId, Integer inMessageProcessingQueueId);
	
	public void updateScheduleForLockStatus(String msgStatus, Integer inMsgId, Integer inMessageProcessingQueueId);
	
	public void updateNextMsgIdToProcess(Integer inMessageProcessingQueueId,Integer msgId);	
		
	public void setMessageStatusForProcessing(String flightDesignator, Integer inMsgId, String msgStatus);
	
	public void updateLastMessageProcessingTime(Integer inMessageProcessingQueueId);
	
	public List<Integer> allInMessageIdsInqueue();

	public void removeFlightDependancies(Integer inMessageProcessingQueueId);
	
	public void updateFlightDesignatorDependencyTable(Integer inMessageProcessingQueueId, String newFlightDesignator);	
	
	public boolean isMessagesLockedInProcessingQueue();
	
	public void unlockMsgProcessingQueue(Integer inMessageProcessingQueueId, String msgStatus);
	/**
	 * Method to filter the gds codes which are referred
	 * 
	 * @param gdsCode
	 * @return
	 */
	// public boolean isGDSReferencedInGDSSitaAddresses(String strGDSCode);
	
	public List<InMessage> getAllLockableInMessagesByMessageSequence(String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing, Date updatedDateTime);
	
	public List<Integer> getMessagesOrderByMessageSequence(Collection<Integer> messageIds,String inMessageProcessStatus, List<String> skipMsgTypesForSeqProcessing);

	public void addBatchForMessageSequenceProcessingQueue();
	
	public boolean isPreviousBatchStillProcessing();
	
	public void updateMessageSequenceProcessingQueue(Integer msgSequenceId,String status);
	
	public List<MessageSequenceQueue> loadWaitingBatchesForSequentialProcessing();
	
	public void clearCompletedMessageSequenceBatches();

	public boolean aquireInMessageLock(Integer inMessageID);
	
	public boolean isUnProcessedMsgWaitingForSequentialProcessing(List<String> msgTypesForSeqProcessing);
	
}
