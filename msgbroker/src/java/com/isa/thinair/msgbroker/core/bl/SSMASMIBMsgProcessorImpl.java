package com.isa.thinair.msgbroker.core.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.AVSResponse;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.msgbroker.api.dto.AVSRequestDTO;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.dto.InNacDTO;
import com.isa.thinair.msgbroker.api.dto.SSMASMRequestDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.dto.MessageComposerResponseDTO;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.GDSConfigAdaptor;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants.InMessageProcessingStatusErrorCodes;
import com.isa.thinair.msgbroker.core.util.MessageRequestProcessingUtil;

public class SSMASMIBMsgProcessorImpl implements IBMsgProcessorBD {

	private final Log log = LogFactory.getLog(SSMASMIBMsgProcessorImpl.class);

	private AdminDAO adminDAO;

	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	OutMessageProcessingStatus outMessageProcessingStatus;

	private String receiverTTYAddress;

	private ServiceClientDTO serviceClientDTO;

	private ServiceBearerDTO serviceBearerDTO;

	private ServiceClientTTYDTO serviceClientTTYDTO;
	
	private String sentTTYAddress;

	public InMessageProcessResultsDTO processAIRIMPMessage(InMessage inMessage) {

		if (inMessage != null) {
			MessageParserResponseDTO messageParserResponseDTO;
			setGdsConfigs(inMessage);
			InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();

			messageParserResponseDTO = ssmAsmInMessageParser(inMessage);

			inMessageProcessResultsDTO = processInboundMsg(messageParserResponseDTO, inMessage);
			inMessageProcessResultsDTO.setInMessage(inMessage);
			return inMessageProcessResultsDTO;
		}

		return null;
	}

	private void setGdsConfigs(InMessage inMessage){
		sentTTYAddress = extractAddress(inMessage.getSentEmailAddress(), inMessage.getSentTTYAddress());
		setGDSConfigsForProcessingMessage(sentTTYAddress, inMessage.getMessageType());
	}
	
	private InMessageProcessResultsDTO processInboundMsg(MessageParserResponseDTO messageParserResponseDTO,
			InMessage inMessage) {
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();
		InMessageProcessingStatus inMessageProcessingStatus;
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		
		boolean isValidCarrier = false;

		Object obj = messageParserResponseDTO.getRequestDTO();
		if (obj instanceof SSMASMRequestDTO) {
			log.info("RECEIVED SSM/ASM from Operating Carrier");
			SSMASMRequestDTO scheduleRQDTO = (SSMASMRequestDTO) obj;

			String serviceClientCode = null;
			String msgType = inMessage.getMessageType();
			String timeMode = null;
			String msgSequenceReference = null;
			String creatorReference = null;

			try {
				setGdsConfigs(inMessage);
				if (serviceClientDTO != null && serviceBearerDTO != null) {

					isValidCarrier = setValidGDSDetails(scheduleRQDTO);
					serviceClientCode = serviceClientDTO.getServiceClientCode();

					SSMASMResponse ssiMResponse = new SSMASMResponse();
					if (isValidCarrier) {
						scheduleRQDTO.getAsmssmMetaDataDTO().setInMessegeID(inMessage.getInMessageID());
						scheduleRQDTO.setRawMessage(inMessage.getInMessageText());
						ssiMResponse = processSSMASMMessage(scheduleRQDTO);
					} else {
						ssiMResponse.setSuccess(false);
						ssiMResponse.setResponseMessage("UNKNOWN MESSAGE SENDER");
					}

					inMessage.setProcessedDate(new Date());
					inMessage.setServiceClientCode(serviceClientCode);
					inMessage.setSentTTYAddress(sentTTYAddress);
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

					if (!ssiMResponse.isSuccess()) {
						if (ssiMResponse.isScheduleAlreadyInSession()) {
							inMessageProcessingStatus = new InMessageProcessingStatus();
							inMessageProcessingStatus
									.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
							inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
							inMessage.setLastErrorMessageCode(ssiMResponse.getResponseMessage());
							inMessageProcessingStatus.setErrorMessageCode(
									InMessageProcessingStatusErrorCodes.SCHEDULE_LOCKED_FOR_UPDATE);
							inMessageProcessingStatus.setErrorMessageText(ssiMResponse.getResponseMessage());
							inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
							inMessageProcessResultsDTO.setInMessage(inMessage);

							return inMessageProcessResultsDTO;
						}
						boolean allowRetry = allowRetryProcessNextBatch(inMessage, ssiMResponse.getResponseMessage());

						if (isValidCarrier && !allowRetry) {
							SSMASMMessegeDTO ssiMessegeDTO = ssiMResponse.getSsiMessegeDTO();

							if (ssiMessegeDTO != null) {
								timeMode = ssiMessegeDTO.getTimeMode();
								msgSequenceReference = ssiMessegeDTO.getMessageSequenceReference();
								creatorReference = ssiMessegeDTO.getCreatorReference();
							}

							composerResponseDTO = messageComposer.composeNACMessage(msgType, timeMode,
									msgSequenceReference, creatorReference, inMessage.getInMessageText(),
									ssiMResponse.getResponseMessage());

							sendSheduleMsgProcessFailedEMail(composerResponseDTO.getComposedMessage(),
									inMessage.getInMessageText());

							if (composerResponseDTO.isSuccess()) {
								OutMessage outMessage = composeNacMessage(inMessage,
										composerResponseDTO.getComposedMessage());

								inMessageProcessResultsDTO.setOutMessage(outMessage);

								outMessageProcessingStatus = new OutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								outMessageProcessingStatus.setErrorMessageCode(
										MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
								outMessageProcessingStatus.setProcessingDate(new Date());
								outMessageProcessingStatus.setProcessingStatus(
										MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
							}
						}

						composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
								allowRetry);

					} else {
						if (!ssiMResponse.isACKNACMessage()) {
							inMessageProcessResultsDTO = composeACKMessage(ssiMResponse, inMessage, msgType);
						}
						composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);
						inMessage.setLastErrorMessageCode(null);
					}

				} else {
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
					inMessage.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
				}

			} catch (Exception e) {
				e.printStackTrace();
				log.error(e);
				composeInMessageProcessingErrorStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
						e.getMessage());

				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
				inMessage.setLastErrorMessageCode(
						MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);

				if (isValidCarrier) {
					// Send NAC
					composerResponseDTO = messageComposer.composeNACMessage(msgType, timeMode, msgSequenceReference,
							creatorReference, inMessage.getInMessageText(), "ERROR MESSAGE");

					if (composerResponseDTO.isSuccess() && serviceClientCode != null && serviceBearerDTO != null) {
						OutMessage outMessage = composeNacMessage(inMessage, composerResponseDTO.getComposedMessage());

						inMessageProcessResultsDTO.setOutMessage(outMessage);
						inMessage
								.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);

					}
				}

			}
		} else if (obj instanceof AVSRequestDTO) {
			
			log.info("RECEIVED AVS from Operating Carrier");
			AVSRequestDTO avsRequestDTO = (AVSRequestDTO) obj;
			String gdsCarrierCode = null;
			String serviceClientCode = null;
			String msgType = inMessage.getMessageType();
			String timeMode = null;
			String msgSequenceReference = null;
			String creatorReference = null;
			boolean processAvs = false;
			com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO avsDTO = new com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO();
			try {
				setGdsConfigs(inMessage);

				if (serviceClientDTO != null && serviceBearerDTO != null) {
					serviceClientCode = serviceClientDTO.getServiceClientCode();

					Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();

					if (serviceClientCode != null && gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
						for (String gdsId : gdsStatusMap.keySet()) {
							GDSStatusTO gdsStatusTO = gdsStatusMap.get(gdsId);
							if (gdsStatusTO != null && serviceClientCode.equalsIgnoreCase(gdsStatusTO.getGdsCode())) {
								gdsCarrierCode = gdsStatusTO.getCarrierCode();
								avsDTO.setGdsId(gdsId);
								avsDTO.setOperatingCarrier(gdsCarrierCode);
								isValidCarrier = true;
								processAvs = gdsStatusTO.isProcessAvs();
								break;
							}
						}

					}

					AVSResponse avsResponse = new AVSResponse();

					if (isValidCarrier) {
						avsResponse = processAVSMessage(avsRequestDTO, avsDTO, processAvs);
					} else {
						avsResponse.setSuccess(false);
						avsResponse.setResponseMessage("UNKNOWN MESSAGE SENDER");
					}

					inMessage.setProcessedDate(new Date());
					inMessage.setServiceClientCode(serviceClientCode);
					inMessage.setSentTTYAddress(sentTTYAddress);
					if (!avsResponse.isSuccess()) {

						boolean allowRetry = allowRetryProcessNextBatch(inMessage, avsResponse.getResponseMessage());

						if (isValidCarrier && !allowRetry) {
							composerResponseDTO = messageComposer.composeNACMessage(msgType, null, null, null,
									inMessage.getInMessageText(), avsResponse.getResponseMessage());

							if (composerResponseDTO.isSuccess()) {

								OutMessage outMessage = composeNacMessage(inMessage,
										composerResponseDTO.getComposedMessage());

								inMessageProcessResultsDTO.setOutMessage(outMessage);

								outMessageProcessingStatus = new OutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								outMessageProcessingStatus.setErrorMessageCode(
										MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
								outMessageProcessingStatus.setProcessingDate(new Date());
								outMessageProcessingStatus.setProcessingStatus(
										MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
							}
						}

						composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
								allowRetry);

					} else {
						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
						inMessage.setLastErrorMessageCode(null);

						composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);

					}

				} else {
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
					inMessage.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
				}

			} catch (Exception e) {
				log.error(e);

				composeInMessageProcessingErrorStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
						e.getMessage());

				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
				inMessage.setLastErrorMessageCode(
						MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);

				if (isValidCarrier) {
					// Send NAC
					composerResponseDTO = messageComposer.composeNACMessage(msgType, timeMode, msgSequenceReference,
							creatorReference, inMessage.getInMessageText(), "ERROR MESSAGE");

					if (composerResponseDTO.isSuccess()) {
						OutMessage outMessage = composeNacMessage(inMessage, composerResponseDTO.getComposedMessage());

						inMessageProcessResultsDTO.setOutMessage(outMessage);
						inMessage
								.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
					}
				}

			}

		} else if (obj instanceof InNacDTO) {
			inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

		}
		return inMessageProcessResultsDTO;

	}
	
	
	
	
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}

	private String extractAddress(String emailID, String ttyAddress) {
		String address = "";
		if (emailID != null && !emailID.equals("")) {
			String[] splittedEmailAddress = emailID.split("@");
			address = splittedEmailAddress[0].trim().toUpperCase();
		} else if (ttyAddress != null && !ttyAddress.equals("")) {
			address = ttyAddress;
		}

		return address;
	}

	// only for testing purpose
	@Override
	public InMessageProcessResultsDTO processAIRIMPMessage(TypeBRQ typeBRQ) {

		String message = typeBRQ.getTypeBMessage();
		MessageParserResponseDTO messageParserResponseDTO;
		InMessageProcessingStatus inMessageProcessingStatus;
		InMessageParser inMessageParser = new InMessageParser();
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();
		InMessage inMessage = new InMessage();

		if (message != null) {
			messageParserResponseDTO = inMessageParser.parseMessage(message);
			inMessage.setInMessageText(message);
			if (!messageParserResponseDTO.isSuccess()) {

				inMessageProcessingStatus = new InMessageProcessingStatus();
				inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
				inMessageProcessingStatus.setErrorMessageCode(messageParserResponseDTO.getReasonCode());
				inMessageProcessingStatus.setErrorMessageText(messageParserResponseDTO.getStatus());
				inMessageProcessingStatus.setProcessingDate(new Date());
				inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMessage.setProcessedDate(new Date());
				inMessage.setLastErrorMessageCode(messageParserResponseDTO.getReasonCode());

			} else {
				Object obj = messageParserResponseDTO.getRequestDTO();

				if (obj instanceof SSMASMRequestDTO) {
					log.info("RECEIVED SSM/ASM from Operating Carrier");
					SSMASMRequestDTO scheduleRQDTO = (SSMASMRequestDTO) obj;
					// received SSM/ASM
					String msgType = null;
					if (inMessage.getMessageType() != null) {
						msgType = inMessage.getMessageType();
					} else {
						msgType = scheduleRQDTO.getAsmssmMetaDataDTO().getMessageType();
					}
					String timeMode = null;
					String msgSequenceReference = null;
					String creatorReference = null;
					scheduleRQDTO.setOperatingCarrier(AppSysParamsUtil.getDefaultCarrierCode());

					setGDSConfigsForProcessingMessage(scheduleRQDTO.getOriginatorAddress(), scheduleRQDTO.getAsmssmMetaDataDTO()
							.getMessageType());

					try {
						SSMASMResponse ssiMResponse = new SSMASMResponse();
						try {
							scheduleRQDTO.getAsmssmMetaDataDTO().setInMessegeID(inMessage.getInMessageID());
							// even though GDS details are not valid we will allow processing since this flow is used
							// for testing only
							setValidGDSDetails(scheduleRQDTO);
							scheduleRQDTO.setRawMessage(inMessage.getInMessageText());
							ssiMResponse = processSSMASMMessage(scheduleRQDTO);
						} catch (Exception ex) {
							ssiMResponse.setSuccess(false);
							ssiMResponse.setResponseMessage("ERROR WHILE PROCESSING");
						}

						if (serviceClientDTO != null && serviceBearerDTO != null) {
							if (!ssiMResponse.isSuccess()) {
									SSMASMMessegeDTO ssiMessegeDTO = ssiMResponse.getSsiMessegeDTO();
								if (ssiMResponse.isScheduleAlreadyInSession()) {
									inMessageProcessingStatus = new InMessageProcessingStatus();
									inMessageProcessingStatus
											.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
									inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
									inMessage.setLastErrorMessageCode("Schedule message already locked for another update");
									inMessageProcessingStatus
											.setErrorMessageCode(InMessageProcessingStatusErrorCodes.SCHEDULE_LOCKED_FOR_UPDATE);
									inMessageProcessingStatus
											.setErrorMessageText("Schedule message already locked for another update");
									inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
									inMessageProcessResultsDTO.setInMessage(inMessage);

									return inMessageProcessResultsDTO;
								}
								if (ssiMessegeDTO != null) {
									timeMode = ssiMessegeDTO.getTimeMode();
									msgSequenceReference = ssiMessegeDTO.getMessageSequenceReference();
									creatorReference = ssiMessegeDTO.getCreatorReference();
								}

								composerResponseDTO = messageComposer.composeNACMessage(msgType, timeMode, msgSequenceReference,
										creatorReference, inMessage.getInMessageText(), ssiMResponse.getResponseMessage());

								sendSheduleMsgProcessFailedEMail(composerResponseDTO.getComposedMessage(), message);

								if (composerResponseDTO.isSuccess()) {

									OutMessage outMessage = composeNacMessage(inMessage, composerResponseDTO.getComposedMessage());

									inMessageProcessResultsDTO.setOutMessage(outMessage);
									inMessage.setSentTTYAddress(extractAddress(inMessage.getSentEmailAddress(),
											inMessage.getSentTTYAddress()));

									outMessageProcessingStatus = new OutMessageProcessingStatus();
									outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
									outMessageProcessingStatus
											.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
									outMessageProcessingStatus.setProcessingDate(new Date());
									outMessageProcessingStatus
											.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
									inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
								}

								composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);
							} else {
								if (!ssiMResponse.isACKNACMessage()) {
									inMessageProcessResultsDTO = composeACKMessage(ssiMResponse, inMessage, msgType);
								}
								composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);
							}
						}
						// inMessage.setProcessedDate(new Date());
						// inMessage.setServiceClientCode(serviceClientCode);
						// inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

					} catch (Exception e) {
						log.error(e);
						composeInMessageProcessingErrorStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
								e.getMessage());

						// inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						// inMessage
						// .setLastErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
					}
				} else if (obj instanceof AVSRequestDTO) {
					log.info("RECEIVED AVS from Operating Carrier");
					AVSRequestDTO avsRequestDTO = (AVSRequestDTO) obj;
					String msgType = inMessage.getMessageType();

					com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO avsDTO = new com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO();
					try {

						AVSResponse avsResponse = new AVSResponse();

						try {
							avsResponse = processAVSMessage(avsRequestDTO, avsDTO, true);
						} catch (Exception ex) {
							avsResponse.setSuccess(false);
							avsResponse.setResponseMessage("ERROR WHILE PROCESSING");
						}

						inMessage.setProcessedDate(new Date());
						// inMessage.setServiceClientCode(serviceClientCode);
						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
						if (!avsResponse.isSuccess()) {

							composerResponseDTO = messageComposer.composeNACMessage(msgType, null, null, null,
									inMessage.getInMessageText(), avsResponse.getResponseMessage());

							if (composerResponseDTO.isSuccess()) {

								OutMessage outMessage = new OutMessage();
								outMessage.setProcessedDate(new Date());
								outMessage.setReceivedDate(new Date());
								outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
								// outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessage(outMessage);
								inMessage.setSentTTYAddress(extractAddress(inMessage.getSentEmailAddress(),
										inMessage.getSentTTYAddress()));
								// inMessage.setSentTTYAddress(extractAddress(inMessage.getRecipientEmailAddress()));

								outMessageProcessingStatus = new OutMessageProcessingStatus();
								outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
								outMessageProcessingStatus
										.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
								outMessageProcessingStatus.setProcessingDate(new Date());
								outMessageProcessingStatus
										.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
								inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
							}

							composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);

						} else {

							composeInMessageProcessingStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(), false);
						}

					} catch (Exception e) {
						log.error(e);
						composeInMessageProcessingErrorStatus(inMessageProcessResultsDTO, inMessage.getInMessageID(),
								e.getMessage());

						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
						inMessage
								.setLastErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
					}

				} else if (obj instanceof InNacDTO) {
					// TODO implement for NAc sto's
					inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);

				}
			}
			inMessageProcessResultsDTO.setInMessage(inMessage);
			return inMessageProcessResultsDTO;
		}
		return null;

	}

	private synchronized SSMASMResponse processSSMASMMessage(SSMASMRequestDTO scheduleRequestDTO) throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();
		if (scheduleRequestDTO != null) {

			SSMASMMessegeDTO ssmAsmMessageDTO = MessageRequestProcessingUtil
					.convertMsgBrokerScheduleDTOToAccelaero(scheduleRequestDTO);

			if (ssmAsmMessageDTO.isValidRequest()) {
				if (ssmAsmMessageDTO.isUpdateOutResponseStatus()) {
					ssiMResponse = MsgbrokerUtils.getMessageBrokerServiceBD().updateOutMessagesResponseStatus(ssmAsmMessageDTO);
				} else {
					GDSServicesBD gdsServicesBD = MsgbrokerUtils.getGDSServicesBD();
					ssmAsmMessageDTO.setOperatingCarrier(scheduleRequestDTO.getOperatingCarrier());
					ssiMResponse = gdsServicesBD.processSSMASMRequests(ssmAsmMessageDTO);
				}
			} else {
				ssiMResponse.setSuccess(false);
				ssiMResponse.setResponseMessage(ssmAsmMessageDTO.getErrorMessage());
			}
		}
		return ssiMResponse;
	}

	private synchronized AVSResponse processAVSMessage(AVSRequestDTO avsRequestDTO,
			com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO avsDTO, boolean processAvs) throws ModuleException {
		AVSResponse avsResponse = new AVSResponse();
		if (avsRequestDTO != null && avsResponse != null && avsDTO != null) {
			if (processAvs) {
				List<AVSRequestSegmentDTO> avsEntryList = MessageRequestProcessingUtil
						.convertMsgBrokerAVSSegmentInfoToAccelaero(avsRequestDTO);

				if (avsEntryList != null && !avsEntryList.isEmpty()) {
					avsDTO.setAVSSegmentDTOs(avsEntryList);
					GDSServicesBD gdsServicesBD = MsgbrokerUtils.getGDSServicesBD();
					avsResponse = gdsServicesBD.processAVSRequests(avsDTO);
				} else {
					avsResponse.setSuccess(false);
					avsResponse.setResponseMessage("EMPTY MESSAGE");
				}
			} else {
				avsResponse.setSuccess(false);
				avsResponse.setResponseMessage("AVS PROCESS NOT ENABLED");
			}
		}
		return avsResponse;
	}

	private boolean allowRetryProcessNextBatch(InMessage inMessage, String responseMsg) {

		List<String> messages = new ArrayList<String>();
		messages.add("Cannot Process your Request. Please Try Later!");
		messages.add("ROW WAS UPDATED OR DELETED BY ANOTHER TRANSACTION");
		messages.add("SCHEDULE OR FLIGHT DOES NOT EXIST");
		messages.add("ERROR PROCESSING MESSAGE QUEUE");
		boolean allowRetry = false;
		if (inMessage != null) {
			if (!StringUtil.isNullOrEmpty(responseMsg)) {
				for (String msg : messages) {
					if (msg.equalsIgnoreCase(responseMsg) || responseMsg.toUpperCase().contains(msg.toUpperCase())) {
						allowRetry = true;
						break;
					}
				}

			}
			if (StringUtil.isNullOrEmpty(inMessage.getLastErrorMessageCode()) && allowRetry) {
				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
				inMessage.setLastErrorMessageCode("Allow Re-attempt processing once");
				allowRetry = true;
			} else {
				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Processed);
				inMessage.setLastErrorMessageCode(responseMsg);
				allowRetry = false;
			}
		}
		return allowRetry;
	}

	public void sendSheduleMsgProcessFailedEMail(String nacMessage, String inMessage) throws ModuleException {
		if (AppSysParamsUtil.isScheduleMsgProcessErrorEmailEnabled()) {
			List<String> receivers = AppSysParamsUtil.getScheduleMsgProcessErrorEmailRecipients();
			MessagingServiceBD messagingServiceBD = LookupUtil.getMessagingServiceBD();
			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			HashMap<String, String> topicParamMap = new HashMap<String, String>();
			String error = "";
			if (receivers == null || receivers.isEmpty()) {
				log.error("Schedule message process error mail receivers list is empty. Sending mail failed.");
				return;
			}
			for (String recipient : receivers) {
				UserMessaging userMessaging = new UserMessaging();
				userMessaging.setToAddres(recipient);
				messages.add(userMessaging);
			}
			Topic topic = new Topic();
			topic.setTopicName(Constants.ScheduleMessageEmailTemplates.SCHEDULE_PROCESS_FAILED_EMAIL);
			topic.setAttachMessageBody(false);
			error = (nacMessage != null && !nacMessage.isEmpty()) ? nacMessage : inMessage;
			topicParamMap.put("error", error);
			topic.setTopicParams(topicParamMap);

			MessageProfile messageProfile = new MessageProfile();
			messageProfile.setUserMessagings(messages);
			messageProfile.setTopic(topic);

			messagingServiceBD.sendMessage(messageProfile);
		}
	}

	public InMessageProcessResultsDTO composeACKMessage(SSMASMResponse ssiMResponse, InMessage inMessage, String msgType) {
		String timeMode = null;
		String msgSequenceReference = null;
		String creatorReference = null;
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();

		if (serviceBearerDTO != null && serviceClientDTO != null) {
			SSMASMMessegeDTO ssiMessegeDTO = ssiMResponse.getSsiMessegeDTO();

			if (ssiMessegeDTO != null) {
				timeMode = ssiMessegeDTO.getTimeMode();
				msgSequenceReference = ssiMessegeDTO.getMessageSequenceReference();
				creatorReference = ssiMessegeDTO.getCreatorReference();
			}

			composerResponseDTO = messageComposer.composeACKMessage(msgType, timeMode, msgSequenceReference, creatorReference);

			if (composerResponseDTO.isSuccess()) {

				OutMessage outMessage = new OutMessage();
				outMessage.setAirLineCode(inMessage.getAirLineCode());
				outMessage.setRecordLocator(inMessage.getRecordLocator());
				outMessage.setGdsMessageID(inMessage.getGdsMessageID());
				outMessage.setMessageType("ACK");
				outMessage.setRefInMessageID(inMessage.getInMessageID());
				outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
				outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
				outMessage.setRecipientTTYAddress(receiverTTYAddress);
				outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(receiverTTYAddress,
						serviceClientDTO.getEmailDomain()));
				outMessage.setModeOfCommunication(inMessage.getModeOfCommunication());
				outMessage.setProcessedDate(new Date());
				outMessage.setReceivedDate(new Date());
				outMessage.setOutMessageText(composerResponseDTO.getComposedMessage());
				outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				inMessageProcessResultsDTO.setOutMessage(outMessage);

				outMessageProcessingStatus = new OutMessageProcessingStatus();
				outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
				outMessageProcessingStatus
						.setErrorMessageCode(MessageProcessingConstants.OutMessageProcessingStatusErrorCodes.RESPONSE_QUEUED_FOR_TRANSMISSION);
				outMessageProcessingStatus.setProcessingDate(new Date());
				outMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				inMessageProcessResultsDTO.setOutMessageProcessingStatus(outMessageProcessingStatus);
			}
		}

		return inMessageProcessResultsDTO;

	}

	private void setGDSConfigsForProcessingMessage(String senderAddress, String messageType) {

		if (AppSysParamsUtil.isGDSCsConfigsFromCache()) {
			serviceClientTTYDTO = TTYMessageUtil.getServiceClientTTYForReqTypeTTY(senderAddress, messageType);
			if (serviceClientTTYDTO != null) {
				serviceClientDTO = TTYMessageUtil.getServiceClientByServiceClientCode(serviceClientTTYDTO.getServiceClientCode());
				serviceBearerDTO = TTYMessageUtil.getServiceBearerByServiceClientCode(serviceClientTTYDTO.getServiceClientCode());
				this.receiverTTYAddress = serviceClientTTYDTO.getInTTYAddress();
			}
		} else {
			String serviceClientCode = getServiceClientReqTypeDAO().getServiceClientForReqTypeTTY(senderAddress, messageType);
			String receiverTTYAddress = getServiceClientReqTypeDAO()
					.getTTYForServiceClientReqType(serviceClientCode, messageType);
			if (serviceClientCode != null && receiverTTYAddress != null) {
				serviceClientTTYDTO = new ServiceClientTTYDTO(serviceClientCode, null, receiverTTYAddress);
				this.receiverTTYAddress = serviceClientTTYDTO.getInTTYAddress();
				ServiceClient serviceClient = getAdminDAO().getServiceClient(serviceClientCode);
				if (serviceClient != null) {
					serviceClientDTO = GDSConfigAdaptor.adapt(serviceClient);
					ServiceBearer serviceBearerAirline = getAdminDAO().getServiceBearer(AppSysParamsUtil.getDefaultCarrierCode(),
							serviceClientCode);
					if (serviceBearerAirline != null) {
						serviceBearerDTO = GDSConfigAdaptor.adapt(serviceBearerAirline);
					}
				}
			}
		}
	}

	private OutMessage composeNacMessage(InMessage inMessage, String nacMessageText) {
		OutMessage outMessage = new OutMessage();
		outMessage.setAirLineCode(inMessage.getAirLineCode());
		outMessage.setRecordLocator(inMessage.getRecordLocator());
		outMessage.setGdsMessageID(inMessage.getGdsMessageID());
		outMessage.setMessageType("NAC");

		outMessage.setRefInMessageID(inMessage.getInMessageID());
		outMessage.setSentEmailAddress(serviceBearerDTO.getOutgoingEmailFromAddress());
		outMessage.setSentTTYAddress(serviceBearerDTO.getSitaAddress());
		outMessage.setRecipientTTYAddress(receiverTTYAddress);
		outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(receiverTTYAddress,
				serviceClientDTO.getEmailDomain()));
		outMessage.setModeOfCommunication(inMessage.getModeOfCommunication());
		outMessage.setProcessedDate(new Date());
		outMessage.setReceivedDate(new Date());
		outMessage.setOutMessageText(nacMessageText);
		outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);

		return outMessage;
	}

	private void composeInMessageProcessingStatus(InMessageProcessResultsDTO inMessageProcessResultsDTO, Integer inMessageID,
			boolean isAllowRetry) {
		InMessageProcessingStatus inMessageProcessingStatus = new InMessageProcessingStatus();
		inMessageProcessingStatus.setInMessageID(inMessageID);
		inMessageProcessingStatus
				.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.GOT_THE_RESPONSE_FOR_THE_REQUEST);
		inMessageProcessingStatus.setProcessingDate(new Date());
		if (isAllowRetry) {
			inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
		}else{
			inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Processed);
		}

		inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
	}

	private void composeInMessageProcessingErrorStatus(InMessageProcessResultsDTO inMessageProcessResultsDTO,
			Integer inMessageID, String errorMessage) {
		InMessageProcessingStatus inMessageProcessingStatus = new InMessageProcessingStatus();
		inMessageProcessingStatus
				.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.INTERNAL_EXCEPTION);
		inMessageProcessingStatus.setErrorMessageText(errorMessage);
		inMessageProcessingStatus.setProcessingDate(new Date());
		inMessageProcessingStatus.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unprocessed);
		inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);
	}

	private boolean setValidGDSDetails(SSMASMRequestDTO scheduleRQDTO) {
		if (serviceClientDTO != null) {
			Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();
			String gdsCode = serviceClientDTO.getServiceClientCode();

			if (gdsCode != null && gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				for (String gdsId : gdsStatusMap.keySet()) {
					GDSStatusTO gdsStatusTO = gdsStatusMap.get(gdsId);
					if (gdsStatusTO != null && gdsCode.equalsIgnoreCase(gdsStatusTO.getGdsCode())) {
						scheduleRQDTO.setOperatingCarrier(gdsStatusTO.getCarrierCode());
						scheduleRQDTO.setCodeShareCarrier(gdsStatusTO.isCodeShareCarrier());
						scheduleRQDTO.setMarketingFlightPrefix(gdsStatusTO.getMarketingFlightPrefix());
						scheduleRQDTO.setEnableAutoScheduleSplit(gdsStatusTO.isEnableAutoScheduleSplit());

						return true;
					}
				}

			}
		}
		return false;
	}
	
	/**
	 * Common parser for both flows
	 * @param inMessage
	 * @return
	 */
	public MessageParserResponseDTO ssmAsmInMessageParser(InMessage inMessage) {

		MessageParserResponseDTO messageParserResponseDTO = new MessageParserResponseDTO();
		InMessageProcessingStatus inMessageProcessingStatus;
		InMessageParser inMessageParser = new InMessageParser();
		GDSMessageComposer messageComposer = new GDSMessageComposer();
		MessageComposerResponseDTO composerResponseDTO;
		InMessageProcessResultsDTO inMessageProcessResultsDTO = new InMessageProcessResultsDTO();

		setGdsConfigs(inMessage);
		messageParserResponseDTO = inMessageParser.parseMessage(inMessage);

		if (!messageParserResponseDTO.isSuccess()) {
			if (serviceClientDTO != null && serviceBearerDTO != null) {
				inMessageProcessingStatus = new InMessageProcessingStatus();
				inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
				inMessageProcessingStatus.setErrorMessageCode(messageParserResponseDTO.getReasonCode());
				inMessageProcessingStatus.setErrorMessageText(messageParserResponseDTO.getStatus());
				inMessageProcessingStatus.setProcessingDate(new Date());
				inMessageProcessingStatus
						.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMessageProcessResultsDTO.setInMessageProcessingStatus(inMessageProcessingStatus);

				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.Unparsable);
				inMessage.setProcessedDate(new Date());
				inMessage.setLastErrorMessageCode(messageParserResponseDTO.getReasonCode());
				composerResponseDTO = messageComposer.composeNACMessage(inMessage.getInMessageText(),
						inMessage.getAirLineCode(), messageParserResponseDTO.getStatus(), null);
				if (composerResponseDTO.isSuccess()) {
					OutMessage outMessage = composeNacMessage(inMessage, composerResponseDTO.getComposedMessage());

					inMessageProcessResultsDTO.setOutMessage(outMessage);
					inMessage.setSentTTYAddress(sentTTYAddress);
				}
			} else {
				inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
				inMessage.setLastErrorMessageCode("UNKNOWN MESSAGE SENDER");
			}
		}

		return messageParserResponseDTO;

	}
	
	public String retrieveSenderAddress(String emailID, String ttyAddress){
		return extractAddress(emailID, ttyAddress);
	}
}
