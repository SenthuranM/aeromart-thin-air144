package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.ETSTicketRefundServiceBD;

@Remote
public interface ETSTicketRefundServiceBeanRemote extends ETSTicketRefundServiceBD {

}
