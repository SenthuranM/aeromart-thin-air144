package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.PFSETLServiceBD;

@Remote
public interface PFSETLServiceBeanRemote extends PFSETLServiceBD {

}
