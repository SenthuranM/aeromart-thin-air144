package com.isa.thinair.msgbroker.core.bl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.msgbroker.api.dto.InMessageProcessResultsDTO;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.api.model.InMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.model.MessageSequenceQueue;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.OutMessageProcessingStatus;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.config.MsgBrokerModuleConfig;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.ServiceUtil;

public class SequentialMessageProcessor {

	private static Log log = LogFactory.getLog(SequentialMessageProcessor.class);
	private MessageDAO messageDAO;
	private static final String DUPLICATE_MESSAGE = "DUPLICATE MESSAGE";
	private List<String> msgTypesForSequentialProcessing = null;
	private boolean isEnableSequentialMessageProcessing = false;

	public SequentialMessageProcessor() {
		isEnableSequentialMessageProcessing = AppSysParamsUtil.isEnableSequentialMessageProcessing();
		if (isEnableSequentialMessageProcessing) {
			msgTypesForSequentialProcessing = AppSysParamsUtil.getSequentialProcessingAllowedMessageTypes();
		}
	}

	public void execute() {
		log.info("In side SequentialMessageProcessor ");
		if (this.isEnableSequentialMessageProcessing) {
			if (!isPreviousBatchAlreadyInProgress()) {

				List<MessageSequenceQueue> waitingBatches = getMessageDAO().loadWaitingBatchesForSequentialProcessing();

				if (waitingBatches != null && !waitingBatches.isEmpty()) {

					log.info("Waiting messages batches to be processed " + waitingBatches.size());
					for (MessageSequenceQueue msgSequenceQueue : waitingBatches) {
						Integer messageSequenceId = msgSequenceQueue.getMessageSequenceId();

						try {

							processingMessageQueueStatus(messageSequenceId,
									MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.PROCESSING);
							
							Timestamp lastBatchReceivedTime = msgSequenceQueue.getLastBatchReceived();
							Calendar start = Calendar.getInstance();
							start.setTimeInMillis(lastBatchReceivedTime.getTime());
							
							log.info("Batch in progress  - " + messageSequenceId + " - Last Updated Time "
									+ CalendarUtil.formatDate(start.getTime(), CalendarUtil.PATTERN7));

							List<InMessage> unprocessedInMessages = getMessageDAO().getAllLockableInMessagesByMessageSequence(
									MessageProcessingConstants.InMessageProcessStatus.Unprocessed,
									this.msgTypesForSequentialProcessing, start.getTime());
							if (unprocessedInMessages != null && !unprocessedInMessages.isEmpty()) {
								log.info("Loaded Un-Processed Messages for Sequential Message Processing count - "
										+ unprocessedInMessages.size());
								List<Integer> messageIdsToProcess = checkAndOmitDuplicateMessages(unprocessedInMessages);
								processInMessages(messageIdsToProcess);
							} else {
								log.info("ZERO messages for Sequential processing ");
							}

							processingMessageQueueStatus(messageSequenceId,
									MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.COMPLETED);

						} catch (Exception e) {
							log.error(e.getMessage());
							processingMessageQueueStatus(messageSequenceId,
									MessageProcessingConstants.InMessageSequenceBatchProcessedStatus.ErroInProcessing);

						}

					}
					getMessageDAO().clearCompletedMessageSequenceBatches();
				} else {
					log.info("No latest received batches for Sequential processing ");
				}

			} else {
				log.info("SequentialMessageProcessor Still previous Batch in Execution ");
			}
		} else {
			log.info("SequentialMessageProcessor DISABLED");
		}

		log.info("Exit SequentialMessageProcessor ");
	}

	private void processInMessages(List<Integer> messageIdsToProcess) {
		if (messageIdsToProcess != null && !messageIdsToProcess.isEmpty()) {
			List<Integer> mesasgesByMessageSequence = getMessageDAO().getMessagesOrderByMessageSequence(messageIdsToProcess,
					MessageProcessingConstants.InMessageProcessStatus.Unprocessed, this.msgTypesForSequentialProcessing);
			if (mesasgesByMessageSequence != null && !mesasgesByMessageSequence.isEmpty()) {
				for (Integer messageId : mesasgesByMessageSequence) {
					log.info("Start processing Message Id " + messageId);
					startProcessMessage(messageId);
					log.info("End processing Message Id " + messageId);
				}
			}

		}

	}

	private void startProcessMessage(Integer messageId) {
		MsgBrokerModuleConfig config = MsgbrokerUtils.getMsgbrokerConfigs();
		ForceLoginInvoker.login(config.getEjbAccessUsername(), config.getEjbAccessPassword());

		InMessage inMessage = null;
		try {
			InMessageProcessingStatus inMessageProcessingStatus = null;
			OutMessage outMessage = null;
			OutMessageProcessingStatus outMessageProcessingStatus = null;
			// define service type per message
			IBMsgProcessorBD ibMessageProcessor = null;
			ServiceUtil serviceUtil = new ServiceUtil();
			inMessage = getMessageDAO().getInMessage(messageId);
			if (inMessage.getStatusIndicator().equals(MessageProcessingConstants.InMessageProcessStatus.Unprocessed)
					&& serviceUtil.isMessageUnlockedOrLockTimedOut(inMessage)) {

				log.info("Locking message | msg_id : " + inMessage.getInMessageID());
				boolean inMessageLocked = MsgbrokerUtils.getMessageBrokerServiceBD().aquireInMessageLock(inMessage.getInMessageID());

				if (inMessageLocked) {

					log.info("Message locked | msg_id : " + inMessage.getInMessageID());
					inMessage = getMessageDAO().getInMessage(messageId);

					if (!serviceUtil.isMessageTimedOut(inMessage)) {
						ibMessageProcessor = MessageBeanFactory.getIBMsgProcessor(inMessage.getMessageType());
						if (ibMessageProcessor != null) {
							InMessageProcessResultsDTO inMessageProcessResultsDTO = ibMessageProcessor
									.processAIRIMPMessage(inMessage);
							if (inMessageProcessResultsDTO != null) {
								if (inMessageProcessResultsDTO.getInMessage() != null) {
									inMessage = inMessageProcessResultsDTO.getInMessage();
								}
								if (inMessageProcessResultsDTO.getInMessageProcessingStatus() != null) {
									inMessageProcessingStatus = inMessageProcessResultsDTO.getInMessageProcessingStatus();
									inMessageProcessingStatus.setInMessageID(inMessage.getInMessageID());
									MsgbrokerUtils.getMessageBrokerServiceBD()
											.saveInMessageProcessingStatus(inMessageProcessingStatus);
								}
								if (inMessageProcessResultsDTO.getOutMessage() != null) {
									outMessage = inMessageProcessResultsDTO.getOutMessage();
									MsgbrokerUtils.getMessageBrokerServiceBD().saveOutMessage(outMessage);
									if (inMessageProcessResultsDTO.getOutMessageProcessingStatus() != null) {
										outMessageProcessingStatus = inMessageProcessResultsDTO.getOutMessageProcessingStatus();
										outMessageProcessingStatus.setOutMessageID(outMessage.getOutMessageID());
										getMessageDAO().saveOutMessageProcessingStatus(outMessageProcessingStatus);
									}
								}
							} else {
								log.error("No Implementation for the message type " + inMessage != null
										? inMessage.getMessageType()
										: "null");
							}
						} else {
							log.error("No Implementation for the message type " + inMessage != null
									? inMessage.getMessageType()
									: "null");
						}
					} else {
						// The request timed out, record the error message.
						InMessageProcessingStatus inMessagePassingErrors = new InMessageProcessingStatus();
						inMessagePassingErrors.setInMessageID(inMessage.getInMessageID());
						inMessagePassingErrors.setErrorMessageCode(
								MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_TIMED_OUT);
						inMessagePassingErrors.setProcessingDate(new Date());
						inMessagePassingErrors.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.TimedOut);
						MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(inMessagePassingErrors);

						inMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.TimedOut);
						inMessage.setProcessedDate(new Date());
						inMessage.setLastErrorMessageCode(
								MessageProcessingConstants.InMessageProcessingStatusErrorCodes.REQUEST_TIMED_OUT);
					}
					inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);

					MsgbrokerUtils.getMessageBrokerServiceBD().updateInMessage(inMessage);
					log.info("End processing msg " + inMessage.getInMessageID());

				} else {
					log.info("Message already on locked status | msg_id : " + inMessage.getInMessageID());
				}
			}

		} catch (ModuleException e) {
			log.error("Error in Saving to Database", e);

			try {
				if (inMessage != null) {
					inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);
					MsgbrokerUtils.getMessageBrokerServiceBD().updateInMessage(inMessage);
				}
			} catch (ModuleException me) {
				log.error("Error ---- ", me);
			}
		} catch (Exception e) {
			log.error("Error ---- ", e);

			try {
				if (inMessage != null) {
					inMessage.setLockStatus(MessageProcessingConstants.InMessageLockStatuses.UNLOCKED);
					MsgbrokerUtils.getMessageBrokerServiceBD().updateInMessage(inMessage);
				}
			} catch (ModuleException me) {
				log.error("Error ---- ", me);
			}
		}
	}

	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}

	private List<Integer> checkAndOmitDuplicateMessages(List<InMessage> allUnprocessedMessages) {

		List<InMessage> duplicateMessages = new ArrayList<InMessage>();
		List<InMessage> tempMessages = new ArrayList<InMessage>();
		ArrayList<Integer> processableMessageIDs = new ArrayList<Integer>();
		if (allUnprocessedMessages != null && !allUnprocessedMessages.isEmpty()) {
			for (InMessage message : allUnprocessedMessages) {
				boolean isDuplicateMessage = false;

				for (InMessage tempMessage : tempMessages) {
					if (message.getInMessageText().equals(tempMessage.getInMessageText())) {
						isDuplicateMessage = true;
						break;
					}
				}

				if (isDuplicateMessage) {
					duplicateMessages.add(message);
				} else {
					tempMessages.add(message);
				}
			}

			// tempMessages.sort((InMessage o1, InMessage o2) ->
			// o1.getMessageSequence().compareTo(o2.getMessageSequence()));

			for (InMessage message : tempMessages) {
				processableMessageIDs.add(message.getInMessageID());
			}

			updateDuplicateMessageProcessStatus(duplicateMessages);
		}

		return processableMessageIDs;
	}

	private void updateDuplicateMessageProcessStatus(Collection<InMessage> duplicateMessages) {
		for (InMessage duplicateMessage : duplicateMessages) {
			InMessageProcessingStatus duplicateMessageProcessingStatus = new InMessageProcessingStatus();
			duplicateMessageProcessingStatus.setInMessageID(duplicateMessage.getInMessageID());
			duplicateMessageProcessingStatus.setErrorMessageText(DUPLICATE_MESSAGE);
			duplicateMessageProcessingStatus
					.setErrorMessageCode(MessageProcessingConstants.InMessageProcessingStatusErrorCodes.DUPLICATE_IN_MESSAGE);
			duplicateMessageProcessingStatus.setProcessingDate(new Date());
			duplicateMessageProcessingStatus
					.setProcessingStatus(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
			MsgbrokerUtils.getMessageBrokerServiceBD().saveInMessageProcessingStatus(duplicateMessageProcessingStatus);

			duplicateMessage.setStatusIndicator(MessageProcessingConstants.InMessageProcessStatus.ErroInProcessing);
			duplicateMessage.setLastErrorMessageCode(DUPLICATE_MESSAGE);
			getMessageDAO().saveInMessage(duplicateMessage);
		}
	}

	private boolean isPreviousBatchAlreadyInProgress() {
		return getMessageDAO().isPreviousBatchStillProcessing();
	}

	private void processingMessageQueueStatus(Integer messageSequenceId, String status) {
		getMessageDAO().updateMessageSequenceProcessingQueue(messageSequenceId, status);
	}

}
