package com.isa.thinair.msgbroker.core.bl;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.msgbroker.api.dto.AVSMessageDTO;
import com.isa.thinair.msgbroker.api.dto.AVSRbdDTO;
import com.isa.thinair.msgbroker.api.dto.AVSSegmentDTO;
import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSRbdEventCode;
import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSSegmentEventCode;
import com.isa.thinair.msgbroker.api.util.DTOValidator;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author sanjeewaf
 */
public class AVSMessageComposer {

	String sStatus;

	private static Log logger = LogFactory.getLog(GDSMessageComposer.class);

	private final String ALL_RBDS = "/";

	/**
	 * Method compose outgoing AVS message.
	 * 
	 * @param avsMessageDTO
	 * @return
	 * @throws Exception
	 */

	public String composeAVSBookingMessage(AVSMessageDTO avsMessageDTO, int gdsId, boolean includeAddressInMsgBody)
			throws ModuleException {

		String outMessage = null;

		if (DTOValidator.validateAVSMessageDTO(avsMessageDTO)) {

			try {

				String templateName = "email/" + TypeBMessageConstants.MessageIdentifiers.AVS
						+ MessageComposerConstants.TEMPLATE_SUFFIX;
				List<String> avsElements = new ArrayList<String>();
				String avsEelement = "";
				List<AVSSegmentDTO> avsSegmentList = avsMessageDTO.getAVSSegmentDTOs();

				avsSegmentList=	prepareEffectiveAVSEntryList(avsSegmentList);

				for (Iterator iter = avsSegmentList.iterator(); iter.hasNext();) {
					avsEelement = "";
					AVSSegmentDTO avsSeg = (AVSSegmentDTO) iter.next();
					ArrayList<String> segElementList = buildAVSElement(avsSeg, gdsId);
					avsElements.addAll(segElementList);

					if (avsSeg.getDepartureStation() != null && avsSeg.getDestinationStation() != null) {
						avsEelement += MessageComposerConstants.SP + avsSeg.getDepartureStation()
								+ avsSeg.getDestinationStation();
					}
					if (logger.isDebugEnabled()) {
						logger.debug(avsEelement + "\n");
					}

				}

				if (!avsElements.isEmpty()) {
					StringWriter writer = new StringWriter();
					HashMap<String, Object> printDataMap = new HashMap<String, Object>();

					printDataMap.put("avsElements", avsElements.iterator());
					if (includeAddressInMsgBody) {
						printDataMap.put("res", avsMessageDTO.getRecipientAddress());
						printDataMap.put("sen", avsMessageDTO.getSenderAddress());

						SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
						Date dt = new Date();
						String timestamp = sdf.format(dt).toUpperCase();
						printDataMap.put("timestamp", timestamp);
					}
					new TemplateEngine().writeTemplate(printDataMap, templateName, writer);
					outMessage = writer.toString().replace("\r", "").replace("\n", "").replace("&nbps", "\n");

				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("gdsmessagecomposer.avsmessage.error\n");
					}
					logger.error("gdsmessagecomposer.avsmessage.error");
					throw new ModuleException("gdsmessagecomposer.avsmessage.error");
				}

			} catch (Exception e) {

				if (logger.isDebugEnabled()) {
					logger.error("gdsmessagecomposer.avsmessage.error", e);
				}
				sStatus = e.getMessage();
				throw new ModuleException("gdsmessagecomposer.avsmessage.error");

			}

		} else {

			logger.error("gdsmessagecomposer.avsmessage.error");

			throw new ModuleException("gdsmessagecomposer.avsmessage.error");
		}
		return outMessage;
	}

	private List<AVSSegmentDTO> prepareEffectiveAVSEntryList(List<AVSSegmentDTO> avsSegmentList) throws ModuleException {

		if (avsSegmentList.size() > 10) {
			throw new ModuleException("gdsmessagecomposer.avsmessage.error");
		}

		List<AVSSegmentDTO> effectiveAVSSegmentList = new ArrayList<AVSSegmentDTO>();
		List<AVSSegmentDTO> reversedAVSSegmentList = Lists.reverse(avsSegmentList);
		boolean skip;

		for (AVSSegmentDTO reversedAVSSegDTO : reversedAVSSegmentList) {

			skip = false;

			for (AVSSegmentDTO effectiveAvsSegDTO : effectiveAVSSegmentList) {

				if (!StringUtil.isNullOrEmpty(reversedAVSSegDTO.toString())
						&& reversedAVSSegDTO.toString().contentEquals(effectiveAvsSegDTO.toString())) {

					if (reversedAVSSegDTO.getAVSSegmentEventCode() != null) {

						if (effectiveAvsSegDTO.getAVSSegmentEventCode() != null
								&& effectiveAvsSegDTO.getAVSSegmentEventCode().equals(reversedAVSSegDTO.getAVSSegmentEventCode())) {
							skip = true;
						}
					}

					else if (effectiveAvsSegDTO.getAVSSegmentEventCode() == null) {

						if (isForSameBookingClass(effectiveAvsSegDTO.getAvsRbdDTOs(), reversedAVSSegDTO.getAvsRbdDTOs())) {
							skip = true;
						}
					}
				}
			}

			if (!skip) {
				effectiveAVSSegmentList.add(reversedAVSSegDTO);
			}
		}

		 return Lists.reverse(effectiveAVSSegmentList);
	}

	private boolean isForSameBookingClass(Collection<AVSRbdDTO> effectiveAVSBookingClasses,
			Collection<AVSRbdDTO> reversedAVSBookingClasses) throws ModuleException {
		boolean isMatchingBC = false;
		if (!(effectiveAVSBookingClasses.size() == 1 && reversedAVSBookingClasses.size() == 1)) {
			throw new ModuleException("gdsmessagecomposer.avsmessage.error");
		}

		if (!StringUtil.isNullOrEmpty(effectiveAVSBookingClasses.iterator().next().getBookingCode())
				&& effectiveAVSBookingClasses.iterator().next().getBookingCode()
						.contentEquals(reversedAVSBookingClasses.iterator().next().getBookingCode())) {
			isMatchingBC = true;
		}
		return isMatchingBC;
	}

	/**
	 * Method will build avs segment elemnt depend on accelero event
	 * 
	 * @param avsSeg
	 * @throws ModuleException
	 */
	private ArrayList<String> buildAVSElement(AVSSegmentDTO avsSeg, int gdsId) throws ModuleException {

		ArrayList<String> avsElements = new ArrayList<String>();
		String avsElementString;

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
		Date dt = avsSeg.getDepartureDate();
		String flightDesignator = avsSeg.getFlightDesignator();
		String date = sdf.format(dt).toUpperCase();
		String departureStation = avsSeg.getDepartureStation();
		String destinationStation = avsSeg.getDestinationStation();

		AVSSegmentEventCode avsSegmentEventCode = avsSeg.getAVSSegmentEventCode();
		AVSRbdEventCode avsRbdEventCode = null;

		// if publish or create then handle it at BC level
		// TODO: rectify avsSegmentEventCode setting as to eliminate this hack
		if (AVSSegmentEventCode.AVS_PUBLISH_FLIGHT.equals(avsSegmentEventCode)
				|| AVSSegmentEventCode.AVS_CREATE_FLIGHT.equals(avsSegmentEventCode)) {

			avsSegmentEventCode = null;
			avsRbdEventCode = AVSRbdEventCode.AVS_CREATE_FLIGHT;
		}

		if (avsSegmentEventCode != null) {

			if (AVSSegmentEventCode.AVS_UNPUBLISED_FLIGHT.equals(avsSegmentEventCode)
					|| AVSSegmentEventCode.AVS_FLIGHT_CLOSED.equals(avsSegmentEventCode)) {

				avsElementString = flightDesignator + ALL_RBDS + date + MessageComposerConstants.SP
						+ MessageComposerConstants.AVSStausCodes.LIMIT_SALE_CLOSED + MessageComposerConstants.SP
						+ departureStation + destinationStation;
				avsElements.add(avsElementString);

			} else if (AVSSegmentEventCode.AVS_CANCEL_FLIGHT.equals(avsSegmentEventCode)) {

				avsElementString = flightDesignator + ALL_RBDS + date + MessageComposerConstants.SP
						+ MessageComposerConstants.AVSStausCodes.NOOP_FLIGHT + MessageComposerConstants.SP + departureStation
						+ destinationStation;
				avsElements.add(avsElementString);
			} else {
				throw new ModuleException("gdsmessagecomposer.avsmessage.error");
			}
		}

		else if (avsSeg.getAvsRbdDTOs() != null) {
			for (AVSRbdDTO avsRbdDTO : avsSeg.getAvsRbdDTOs()) {
				avsRbdEventCode = avsRbdDTO.getAvsRbdEventCode();
				String rbd = avsRbdDTO.getBookingCode();

				if (avsRbdEventCode != null) {

					Integer availableSeats = getAvailabileSeats(avsSeg, avsRbdDTO);

					String customAvailability = getCustomAvailability(gdsId, availableSeats);

					switch (avsRbdEventCode) {
					case AVS_CREATE_FLIGHT:
					case AVS_AVAIL_BELOW_TRESH:
					case AVS_RBD_SEG_AVAILABILITY_RESET:
					case AVS_SEG_AVAILABILITY_CHANGE:
					case AVS_RBD_SEG_AVAILABILITY_CHANGE:
					case AVS_RBD_AVAILABILITY_CHANGE:
					case AVS_RBD_REOPEN:
					case AVS_AVAIL_ABOVE_THRESHOLD:

						avsElementString = flightDesignator + rbd + date + MessageComposerConstants.SP + customAvailability
								+ MessageComposerConstants.SP + departureStation + destinationStation;
						avsElements.add(avsElementString);

						break;

					case AVS_RBD_CLOSED:
						avsElementString = flightDesignator + rbd + date + MessageComposerConstants.SP
								+ MessageComposerConstants.AVSStausCodes.LIMIT_SALE_CLOSED + MessageComposerConstants.SP
								+ departureStation + destinationStation;
						avsElements.add(avsElementString);
						break;

					case AVS_RBD_REQUEST:
						avsElementString = flightDesignator + rbd + date + MessageComposerConstants.SP
								+ MessageComposerConstants.AVSStausCodes.LIMIT_SALE_REQUEST + MessageComposerConstants.SP
								+ departureStation + destinationStation;
						avsElements.add(avsElementString);
						break;

					default:
						logger.error("gdsmessagecomposer.avsmessage.error");
						throw new ModuleException("gdsmessagecomposer.avsmessage.error");
					}
				}
			}
		} else {
			throw new ModuleException("gdsmessagecomposer.avsmessage.error");
		}

		return avsElements;
	}

	private Integer getAvailabileSeats(AVSSegmentDTO avsSeg, AVSRbdDTO avsRbdDTO) {
		// TODO pass only the effective constraint/availability
		Integer availableSeats = (avsSeg.getSeatsAvailable().intValue() < avsRbdDTO.getNumericAvailability().intValue()) ? avsSeg
				.getSeatsAvailable() : avsRbdDTO.getNumericAvailability();
		return availableSeats;
	}

	private String getCustomAvailability(int gdsId, Integer availableSeats) {
		String customAvailability;

		Gds gds;

		boolean isNAVSEnabled = false;
		boolean isOnlyANumeric = true;

		Integer AVS_CLOSURE_THRESHOLD = null;
		Integer AVS_AVAIL_THRESHOLD = null;

		try {
			gds = LookupUtil.getGdsServiceBD().getGds(gdsId);
			if (gds != null) {
				isNAVSEnabled = gds.checkNAVSEnabled();
				isOnlyANumeric = gds.checkOnlyANumeric();
				AVS_CLOSURE_THRESHOLD = gds.getAvsClosureThreshold();
				AVS_AVAIL_THRESHOLD = gds.getAvsAvailThreshold();
			} else {

				setDefaultAvsThresholds(AVS_CLOSURE_THRESHOLD, AVS_AVAIL_THRESHOLD);
			}
		} catch (Exception e) {
			setDefaultAvsThresholds(AVS_CLOSURE_THRESHOLD, AVS_AVAIL_THRESHOLD);

		}

		if (availableSeats < AVS_CLOSURE_THRESHOLD) {
			customAvailability = MessageComposerConstants.AVSStausCodes.LIMIT_SALE_CLOSED;

		}

		else if (isNAVSEnabled && availableSeats<= AVS_AVAIL_THRESHOLD) {

			if (isOnlyANumeric) {
				// L<digit> (Sirena)
				customAvailability = MessageComposerConstants.AVSStausCodes.L_AVAIL + availableSeats;

			} else {
				// 0<digit> (Amadeus)
				customAvailability = MessageComposerConstants.AVSStausCodes.ZERO_AVAIL_PREFIX + availableSeats;
			}
		} else {
			// LA if not NAVS enabled
			customAvailability = MessageComposerConstants.AVSStausCodes.L_SALE_OPEN;
		}

		return customAvailability;
	}

	private void setDefaultAvsThresholds(Integer AVS_CLOSURE_THRESHOLD, Integer AVS_AVAIL_THRESHOLD) {

		logger.error("Gds loading failed, setting default AVS thresholds\nLower Threshold: "
				+ AirinventoryCustomConstants.AVS_CLOSURE_THRESHOLD + "\nUpper Threshold: "
				+ AirinventoryCustomConstants.AVS_AVAIL_THRESHOLD);

		AVS_CLOSURE_THRESHOLD = AirinventoryCustomConstants.AVS_CLOSURE_THRESHOLD;
		AVS_AVAIL_THRESHOLD = AirinventoryCustomConstants.AVS_AVAIL_THRESHOLD;
	}

	public static void main(String[] args) {

		AVSMessageComposer composer = new AVSMessageComposer();

		AVSMessageDTO avsDTO = new AVSMessageDTO();
		AVSSegmentDTO segmentDTO = new AVSSegmentDTO();

		Date dt = new Date();

		segmentDTO.setDepartureDate(dt);
		segmentDTO.setFlightDesignator("BA123");
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_FLIGHT_CLOSED);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_CREATE_FLIGHT);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_PUBLISH_FLIGHT);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_UNPUBLISED_FLIGHT);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_RBD_CLOSED);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_CANCEL_FLIGHT);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_RBD_CHANGE_ALLOCATION);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_AVAIL_BELOW_TRESH);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_RBD_REOPEN);
		// segmentDTO.setEvent(EventIdentifierCodes.AVS_AVAIL_ABOVE_THRESHOLD);
		// segmentDTO.setAVSSegmentEventCode(AVSSegmentEventCode.AVS_FLIGHT_REOPEN);
		segmentDTO.setDepartureStation("SHJ");
		segmentDTO.setDestinationStation("CMB");

		Collection<AVSRbdDTO> avsRbdDTOs = new ArrayList<AVSRbdDTO>();

		AVSRbdDTO avsRbdDTO1 = new AVSRbdDTO();
		avsRbdDTO1.setAvsRbdEventCode(AVSRbdEventCode.AVS_AVAIL_BELOW_TRESH);// for testing
		avsRbdDTO1.setBookingCode("Y");
		avsRbdDTO1.setNumericAvailability(5);

		avsRbdDTOs.add(avsRbdDTO1);

		AVSRbdDTO avsRbdDTO2 = new AVSRbdDTO();
		avsRbdDTO2.setAvsRbdEventCode(AVSRbdEventCode.AVS_RBD_CLOSED); // for testing
		avsRbdDTO2.setBookingCode("Z");
		avsRbdDTO2.setNumericAvailability(7);

		avsRbdDTOs.add(avsRbdDTO2);

		segmentDTO.setAvsRbdDTOs(avsRbdDTOs);

		/*
		 * AVSSegmentDTO segmentDTO1 = new AVSSegmentDTO(); segmentDTO1.setBookingCode("Y"); Date dt1 = new Date();
		 * segmentDTO1.setDepartureDate(dt1); segmentDTO1.setFlightDesignator("UL123");
		 * segmentDTO1.setStatus_numericAvailability_Code(segmentDTO.getStatus_numericAvailability_Code().A2);
		 * segmentDTO1.setDepartureStation("SHJ"); segmentDTO1.setDestinationStation("CMB");
		 */

		AVSSegmentDTO segmentDTO2 = new AVSSegmentDTO();
		Date dt2 = new Date();
		segmentDTO2.setDepartureDate(dt2);
		segmentDTO2.setFlightDesignator("UL123");
		segmentDTO2.setAVSSegmentEventCode(AVSSegmentEventCode.AVS_CANCEL_FLIGHT);
		segmentDTO2.setDepartureStation("CMD");
		segmentDTO2.setDestinationStation("DXB");

		List<AVSSegmentDTO> segList = new ArrayList<AVSSegmentDTO>();
		segList.add(segmentDTO);
		// segList.add(segmentDTO1);
		segList.add(segmentDTO2);
		avsDTO.setAVSSegmentDTOs(segList);

		/* AVS Check */
		try {
			System.out.println(composer.composeAVSBookingMessage(avsDTO, 1, true));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
