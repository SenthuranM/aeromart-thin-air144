package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;

public class MDBInvoker extends PlatformBaseServiceDelegate {

	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME_IN_MSG_PROCESSER = "queue/inMessageProceesorQueue";
	private static final java.lang.String DESTINATION_JNDI_NAME_TYPEB_GATEWAY = "queue/typeBGatewayQueue";

	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void invokeInboundMessageProcessorMDB(Integer messageId) {
		try {
			sendMessage(messageId, MsgbrokerUtils.getMsgbrokerConfigs().getJndiProperties(),
					DESTINATION_JNDI_NAME_IN_MSG_PROCESSER,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

	public void invokeTypeBGatewayMdb(Integer messageId) {
		try {
			sendMessage(messageId, MsgbrokerUtils.getMsgbrokerConfigs().getJndiProperties(),
					DESTINATION_JNDI_NAME_TYPEB_GATEWAY,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}
}
