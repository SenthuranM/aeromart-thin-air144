package com.isa.thinair.msgbroker.core.util;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Converts a collection to a string in the format of SQL in clause
 * 
 * @author Thejaka
 */

public class Util {

	public static final String CODE_TYPE_STRING = "STRING";
	public static final String CODE_TYPE_INTEGER = "INTEGER";
	public static final String CODE_TYPE_STRING_WITHOUT_QUOTE = "StringWithoutQuote";
	private static final String DEFAULT_PLACE_HOLDER_MARK = "{}";

	/**
	 * Replaces a given string place holders with given string
	 * 
	 * @param srcString
	 * @param placeHolderValue
	 * @param stringToInsert
	 * @return
	 */
	public static String setStringToPlaceHolderIndex(String srcString, String placeHolderValue, String stringToInsert) {
		return setStringToPlaceHolderIndex(srcString, placeHolderValue, stringToInsert, null);
	}

	public static String setStringToPlaceHolderIndex(String srcString, String placeHolderValue, String stringToInsert,
			String placeHolderMark) {
		String placeHolderMarkStart;
		String placeHolderMarkEnd;
		if (placeHolderMark == null) {
			placeHolderMark = DEFAULT_PLACE_HOLDER_MARK;
		}
		placeHolderMarkStart = placeHolderMark.substring(0, 1);
		placeHolderMarkEnd = placeHolderMark.substring(1, 2);

		StringBuffer queryBuff = new StringBuffer(srcString);

		String placeHolder = placeHolderMarkStart + placeHolderValue + placeHolderMarkEnd;
		int startIndex = queryBuff.toString().indexOf(placeHolder);
		while (startIndex != -1) {
			queryBuff.delete(startIndex, startIndex + placeHolder.length());
			queryBuff.insert(startIndex, stringToInsert);
			startIndex = queryBuff.toString().indexOf(placeHolder);
		}

		return queryBuff.toString();
	}

	/**
	 * Replaces a given string place holders with given values in the hash map for the hash map keys
	 * 
	 * @param srcString
	 * @param replaceKeyValuePairs
	 * @return
	 */
	public static String setStringToPlaceHolderIndex(String srcString, HashMap replaceKeyValuePairs) {
		return setStringToPlaceHolderIndex(srcString, replaceKeyValuePairs, null);
	}

	public static String setStringToPlaceHolderIndex(String srcString, HashMap replaceKeyValuePairs, String placeHolderMark) {
		for (Iterator iterKeyValue = replaceKeyValuePairs.keySet().iterator(); iterKeyValue.hasNext();) {
			String key = (String) iterKeyValue.next();
			Object keyValue = replaceKeyValuePairs.get(key);
			if (keyValue != null) {
				String strValue = "";
				if (keyValue instanceof String) {
					strValue = (String) keyValue;
				} else if (keyValue instanceof Integer) {
					strValue = ((Integer) keyValue).toString();
				} else if (keyValue instanceof Date) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
					strValue = dateFormat.format((Date) keyValue);
				}
				srcString = setStringToPlaceHolderIndex(srcString, key, strValue, placeHolderMark);
			}
		}
		return srcString;
	}

	/**
	 * Remove unused tags from the given string
	 * 
	 * @param srcString
	 * @param placeHolderMark
	 * @param selectionCrierionString
	 *            : If null, simply delete the tags with the placeHolderMark. If not null, check whether this string
	 *            exists in a tag identified by placeHolderMark. If so remove the tag.
	 * @return
	 */
	public static String removeUnUsedTagsFromString(String srcString, String placeHolderMark, String selectionCrierionString) {
		String placeHolderMarkStart;
		String placeHolderMarkEnd;
		if (placeHolderMark == null) {
			placeHolderMark = DEFAULT_PLACE_HOLDER_MARK;
		}
		placeHolderMarkStart = placeHolderMark.substring(0, 1);
		placeHolderMarkEnd = placeHolderMark.substring(1, 2);

		StringBuffer queryBuff = new StringBuffer(srcString);

		// String placeHolder = placeHolderMarkStart + placeHolderMarkEnd;
		int startIndex = queryBuff.toString().indexOf(placeHolderMarkStart);
		int endIndex = queryBuff.toString().indexOf(placeHolderMarkEnd);
		while ((endIndex - startIndex) > 0) {
			if (selectionCrierionString == null
					|| queryBuff.substring(startIndex, endIndex + 1).toString().indexOf(selectionCrierionString) > -1) {
				// If null or check whether selectionCrierionChar exists in a tag identified by placeHolderMark.
				// If so remove the tag.
				queryBuff.delete(startIndex, endIndex + 1);
				startIndex = -1;
				endIndex = -1;
			}
			// queryBuff.insert(startIndex, "");
			startIndex = queryBuff.toString().indexOf(placeHolderMarkStart, startIndex + 1);
			endIndex = queryBuff.toString().indexOf(placeHolderMarkEnd, endIndex + 1);
		}
		return queryBuff.toString();
	}

	public static String buildStringInClauseContent(Collection codes) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, null, null);
	}

	public static String buildStringInClauseContent(Collection codes, String WrappingString) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, WrappingString, null);
	}

	public static String buildStringInClauseContent(Collection codes, String WrappingString, String seperationString) {
		return buildInClauseContent(codes, CODE_TYPE_STRING, WrappingString, seperationString);
	}

	public static String buildIntegerInClauseContent(Collection codes) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, null, null);
	}

	public static String buildIntegerInClauseContent(Collection codes, String WrappingString) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, WrappingString, null);
	}

	public static String buildIntegerInClauseContent(Collection codes, String WrappingString, String seperationString) {
		return buildInClauseContent(codes, CODE_TYPE_INTEGER, WrappingString, seperationString);
	}

	public static String buildInClauseContent(String code, int numberOfTimes, String type, String WrappingString,
			String seperationString) {
		List codes = new ArrayList();
		for (int i = 0; i < numberOfTimes; i++) {
			codes.add(code);
		}
		return buildInClauseContent(codes, type, WrappingString, seperationString);
	}

	private static String buildInClauseContent(Collection codes, String type, String WrappingString, String seperationString) {
		String quateString;
		String codesString = "";
		if (seperationString == null)
			seperationString = ",";
		if (type.equals(CODE_TYPE_INTEGER)) {
			quateString = WrappingString == null ? "" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = ((Integer) iter.next()).toString();
				codesString += quateString + strCode + quateString + seperationString;
			}
		} else if (type.equals(CODE_TYPE_STRING_WITHOUT_QUOTE)) {
			quateString = WrappingString == null ? "" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (String) iter.next();
				codesString += quateString + strCode + quateString + seperationString;
			}
		} else {
			quateString = WrappingString == null ? "'" : WrappingString;
			for (Iterator iter = codes.iterator(); iter.hasNext();) {
				String strCode = (String) iter.next();
				codesString += quateString + strCode + quateString + seperationString;
			}
		}

		if (codesString.equals(""))
			return null;
		else
			codesString = codesString.substring(0, codesString.length() - seperationString.length());
		return codesString;
	}

	// public static boolean isModelCollectionEqual(Collection firstPersistents, Collection secondPersistents,
	// boolean compareVersion) throws ModuleException{
	//
	// if (firstPersistents==null){
	// if (secondPersistents==null)
	// return true;
	// else if (secondPersistents.isEmpty())
	// return true;
	// else
	// return false;
	// }
	// if (secondPersistents==null){
	// if (firstPersistents.isEmpty())
	// return true;
	// else
	// return false;
	// }
	// if (firstPersistents.size() != secondPersistents.size())
	// return false;
	// boolean found;
	// Map idMap = new HashMap();
	// for (Iterator iterFirstPersistents = firstPersistents.iterator(); iterFirstPersistents.hasNext();) {
	// Persistent firstPersistent = (Persistent) iterFirstPersistents.next();
	// found = false;
	// for (Iterator iterSecondPersistents = secondPersistents.iterator(); iterSecondPersistents.hasNext();) {
	// Persistent secondPersistent = (Persistent) iterSecondPersistents.next();
	// if ( !idMap.containsKey(new Integer(secondPersistent.hashCode())) &&
	// firstPersistent.isContentEqual(secondPersistent,compareVersion)){
	// found=true;
	// idMap.put(new Integer(secondPersistent.hashCode()),null);
	// break;
	// }
	// }
	// if (!found) {
	// return false;
	// }
	// }
	// return true;
	// }

	public static boolean allEqual(Object obj1, Object obj2) {
		if (obj1 != null && obj2 != null) {
			if (obj1 instanceof Date) {
				Date date1 = (Date) obj1;
				Date date2;
				if (!(obj2 instanceof Date)) {
					return false;
				} else {
					date2 = (Date) obj2;
					return CalendarUtil.isEqualStartTimeOfDate(date1, date2);
				}
			} else if (obj1.equals(obj2)) {
				return true;
			}
		} else if (allNullOrEmpty(obj1, obj2)) {
			return true;
		}
		return false;
	}

	public static boolean allNullOrEmpty(Object obj1, Object obj2) {
		if (allNull(obj1, obj2)) {
			return true;
		} else if (obj1 == null) {
			if (obj2 instanceof String)
				return ((String) obj2).trim().equals("");
			else if (obj2 instanceof Collection)
				return ((Collection) obj2).isEmpty();
		} else if (obj2 == null) {
			if (obj1 instanceof String)
				return ((String) obj1).trim().equals("");
			else if (obj1 instanceof Collection)
				return ((Collection) obj1).isEmpty();
		} else if (obj1 instanceof String && obj2 instanceof String) {
			return ((String) obj1).trim().equals("") && ((String) obj2).trim().equals("");
		} else if (obj1 instanceof Collection && obj2 instanceof Collection) {
			return ((Collection) obj1).isEmpty() && ((Collection) obj2).isEmpty();
		}
		return false;
	}

	public static boolean allNull(Object obj1, Object obj2) {
		return (obj1 == null && obj2 == null);
	}

	// public static boolean allEmpty(String obj1, String obj2){
	// return (obj1 == null && obj2 == null);
	// }

	// public static boolean allEmpty(Collection col1, Collection col2){
	// if (allNull( col1, col2)) {
	// return true;
	// }else if (allNotNull( col1, col2)) {
	// return (col1.isEmpty() && col2.isEmpty());
	// }else {
	// return false;
	// }
	//
	// }

	public static boolean allNotNull(Object obj1, Object obj2) {
		return (obj1 != null && obj2 != null);
	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param object
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	/**
	 * Find out whether the object passed is a null value
	 * 
	 * @param object
	 * @return
	 */
	public static Boolean isNullHandler(Object object) {
		if (object == null) {
			return new Boolean(true);
		} else {
			return new Boolean(false);
		}
	}

	/**
	 * Find out whether the object passed is a null value
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isNull(Object object) {
		if (object == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Find out whether the string passed is empty
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isEmpty(String object) {
		if ((!isNull(object)) && (object.trim().length() == 0)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create a SQL time stamp
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	/**
	 * Parse a date for a date format
	 * 
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String parseDateFormat(Date date, String dateFormat) {
		String formatedDate = null;
		if (date != null) {
			Format formatter = new SimpleDateFormat(dateFormat);
			formatedDate = formatter.format(date);
		}
		return formatedDate;
	}

	/**
	 * Convert a date string of the format "dd/mm/yy" to a Date object
	 * 
	 * @param strdate
	 * @return
	 */

	public static Date formatStringToDate(String strDate) throws ModuleException {
		Date dtReturn = null;
		if (strDate != null && !strDate.equals("")) {
			int date = Integer.parseInt(strDate.substring(0, 2));
			int month = Integer.parseInt(strDate.substring(3, 5));
			int year = Integer.parseInt(strDate.substring(6, 10));

			Calendar validDate = new GregorianCalendar(year, month - 1, date);
			dtReturn = new Date(validDate.getTime().getTime());
		}
		return dtReturn;
	}

	/**
	 * Method to get the operating system.
	 * 
	 * @return
	 */
	public static String getOperatingSystem() {
		if (System.getProperty("os.name").toLowerCase(Locale.US).indexOf("windows") > -1) {
			return Constants.OperatingSystem.WINDOWS;
		} else {
			return Constants.OperatingSystem.UNIX;
		}
	}

	/**
	 * Handles null value and replace with a "&nbsp;" value. This also trims if a String value exist
	 * 
	 * @param object
	 * @return
	 */
	public static String jspNullHandler(Object object) {
		if (object == null) {
			return "&nbsp;";
		} else if ((object instanceof Integer) && (object.equals(new Integer(0)))) {
			return "&nbsp;";
		} else {
			return object.toString().trim();
		}
	}

	public static String getSpaces(int begin, int end) {
		String spaces = "";
		if (end >= begin) {
			for (int i = begin; i <= end; i++) {
				spaces += " ";
			}
		}
		return spaces;
	}

	public static boolean isValidAirlineDesignator(String airlineDesignator) {
		String regex = "([A-Z]{1}[0-9]{1}|[0-9]{1}[A-Z]{1}|[A-Z]{1}[A-Z]{1})|[A-Z]{3}";
		if (airlineDesignator.matches(regex))
			return true;
		else
			return false;
	}

	public static boolean isValidDayMonthYear(String dayMonthYear) {
		String regex = "((0[1-9]|2[0-9]|3[0-1])(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(0[1-9]|[2-9][0-9]))";
		if ((dayMonthYear == null) || (dayMonthYear == "") || !dayMonthYear.matches(regex))
			return false;
		else
			return true;
	}

	public static boolean isValidTime(String time) {
		String regex = "(0[0-9]|1[0-9]|2[0-3])(0[0-9]|[15][0-9])";
		if ((time == null) || (time == "") || (!time.matches(regex)))
			return false;
		else
			return true;
	}

	public static boolean hasSixDigits(String sixDigits) {
		String regex = "[1-9]{6}";
		if ((sixDigits == null) || (sixDigits == "") || !sixDigits.matches(regex))
			return false;
		else
			return true;
	}

	/**
	 * 
	 * Convert Date time to String Format
	 * 
	 * @param dtDate
	 * @return
	 */
	public static String DateTimeToStringFormat(Date dtDate) {
		String strDate = "";
		if (dtDate != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			strDate = simpleDateFormat.format(dtDate);
		}
		return strDate;
	}

	/**
	 * 
	 * Convert Date time (with seconds) to String Format
	 * 
	 * @param dtDate
	 * @return
	 */
	public static String DateTimeWithSecsToStringFormat(Date dtDate) {
		String strDate = "";
		if (dtDate != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			strDate = simpleDateFormat.format(dtDate);
		}
		return strDate;
	}

}
