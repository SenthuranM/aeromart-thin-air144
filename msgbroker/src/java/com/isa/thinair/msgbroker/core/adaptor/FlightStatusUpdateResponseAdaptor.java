package com.isa.thinair.msgbroker.core.adaptor;

import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateResDTO;
import com.accelaero.ets.SimplifiedTicketServiceWrapper;
import com.isa.cloud.support.generic.adaptor.AbstractRequestGrpcDtoAdapter;

public class FlightStatusUpdateResponseAdaptor extends
		AbstractRequestGrpcDtoAdapter<SimplifiedTicketServiceWrapper.FlightStatusUpdateResponse, FlightStatusUpdateResDTO> {

	public FlightStatusUpdateResponseAdaptor() {
		super(SimplifiedTicketServiceWrapper.FlightStatusUpdateResponse.class, FlightStatusUpdateResDTO.class);

	}

}
