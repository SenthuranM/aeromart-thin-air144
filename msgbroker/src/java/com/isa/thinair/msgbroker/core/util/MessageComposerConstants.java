package com.isa.thinair.msgbroker.core.util;


public interface MessageComposerConstants {

	/** Constant string for under ".":. */
	public static final String POINT = ".";

	/** Constant string for under score:. */
	public static final String HYPHEN = "-";

	/** Constant string for forward slash:. */
	public static final String FWD = "/";

	/** Constant string for action status code "HK" . */
	public static final String HK = "HK";

	/** Constant string for String "SSR" . */
	public static final String SSR = "SSR";

	/** Constant string for passenger title "REV" . */
	public static final String TEMPLATE_SUFFIX = "_Template.xml.vm";

	/** Constant string for change name MAB "CHNT" . */
	public static final String CHNT = "CHNT";

	/** Constant string for SSR code "CHLD" . */
	public static final String CHLD = "CHLD";

	/** Constant string for OSI telephone no . */
	public static final String CTCP = "CTCP";

	/** Constant string for Single space character " " . */
	public static final String SP = " ";
	
	public static final int LINE_CHARACTER_LENGTH = 65;
	
	public final String EOL = "\n";
	
	public final String AsmWithdrawalIndicator = "XASM";
	
	public final String OTHER_SEG_INFO = "10";
	
	public final String SITA_COMMUNICATION = "S";
	

	public static interface AVSStausCodes {

		public static final String FLIGHT_OPEN = "AS";

		public static final String NOOP_FLIGHT = "CN";

		public static final String NOOP_SEGMENT = "CS";

		public static final String FLIGHT_CLOSED_REQUEST_ONLY = "CR";

		public static final String FLIGHT_CLOSED_WAITLIST_OPEN = "CL";

		public static final String FLIGHT_CLOSED = "CC";

		public static final String LIMIT_SALE_REQUEST = "LR";
		
		public static final String LIMIT_SALE_WAITLIST = "LL";		
			
		public static final String LIMIT_SALE_CLOSED = "LC";
		
		public static final String L_SALE_OPEN = "LA"; //intended to say segment specific opening
		
		public static final String LIMIT_SALE_LANDING_CANCEL = "LN";

		public static final String ZERO_AVAIL_PREFIX = "0";

		public static final String L_AVAIL = "L";
		
		public static final String A_AVAIL_S = "A";
		
		public static final String RE_OPENED_FOR_SALES = "LA";

	}

	public enum Message_Identifier {
		BKG, AVS, ASC, DVD, RLR, NAC, ACK, AVN, AMD, DVR, NAR, CAN, SSM, ASM, AKA, PNL, ADL,PFS,ETL, NCO, PDM, RES, PAL, CAL;
	}

	public enum SMDataElementRequirmentCategory {
		Mandatory, Conditional, Optional, NotApplicable;
	}

	public enum SSMDataElement {
		BEGIN_SSM,

		// MESSAGE_HEADING_BOL,STANDARD_MESSAGE_IDENTIFIER,MESSAGE_HEADING_EOL,
		MESSAGE_HEADING_BOL, STANDARD_MESSAGE_IDENTIFIER, STANDARD_MESSAGE_IDENTIFIER_EOL, TIME_MODE, TIME_MODE_EOL,

		MESSAGE_REFERENCE_BOL, MESSAGE_SEQUENCE_REFERENCE, MESSAGE_REFERENCE_EOL,

		ACTION_INFORMATION_BOL, ACTION_IDENTIFIER, ASM_WITHDRAWAL_INDICATOR, ACTION_INFORMATION_EOL,

		FLIGHT_INFORMATION_BOL, FLIGHT_DESIGNATOR, FLIGHT_INFORMATION_EOL,

		PERIOD_FREQUENCY_INFORMATION_BOL, PERIOD_OF_OPERATION_FROM_DATE, PERIOD_OF_OPERATION_TO_DATE, DAYS_OF_OPERATION, PERIOD_FREQUENCY_INFORMATION_EOL,
		
		NEW_PERIOD_FREQUENCY_INFORMATION_BOL, NEW_PERIOD_OF_OPERATION_FROM_DATE, NEW_PERIOD_OF_OPERATION_TO_DATE, NEW_DAYS_OF_OPERATION, NEW_PERIOD_FREQUENCY_INFORMATION_EOL,

		EQUIPMENT_INFORMATION_BOL, SERVICE_TYPE, AIRCRAFT_TYPE, PASSENGER_RES_BOOKING_DESIGNATOR, EQUIPMENT_INFORMATION_EOL,

		LEG_INFORMATION_BOL, DEPARTURE_AIRPORT, DEPATURE_TIME, DATE_VARIATION_FOR_STD, ARRIVAL_AIRPORT, ARRIVAL_TIME, DATE_VARIATION_FOR_STA, LEG_INFORMATION_EOL,
		
		NEW_FLIGHT_INFORMATION_BOL, NEW_FLIGHT_DESIGNATOR, NEW_FLIGHT_INFORMATION_EOL,

		SEGMENT_INFORMATION_BOL, OTHER_SEGMENT_INFO, SEGMENT_INFORMATION_EOL,

		COMMENTS_BOL, COMMENTS, COMMENTS_EOL,

		SUB­MESSAGE_SEPARATION_BOL ,SUB­MESSAGE_SEPARATION, SUB­MESSAGE_SEPARATION_EOL,
		
		SUPPLEMENTARY_INFORMATIONS_BOL, SUPPLEMENTARY_INFORMATION, SUPPLEMENTARY_INFORMATIONS_EOL,

		END_SSM
	}

	public enum SSMSubMessageType {
		NEW("NEW"), CNL("CNL"), RPL("RPL"), EQT("EQT"), TIM("TIM"), REV("REV"), FLT("FLT");

		private final SSMSubMessageFormat ssmSubMessageFormat;

		public SSMSubMessageFormat getSSMSubMessageFormat() {
			return ssmSubMessageFormat;
		}

		private SSMSubMessageType(String code) {
			ssmSubMessageFormat = new SSMSubMessageFormat(code);
		}
	}

	public enum ASMDataElement {
		BEGIN_ASM,

		MESSAGE_HEADING_BOL, STANDARD_MESSAGE_IDENTIFIER, STANDARD_MESSAGE_IDENTIFIER_EOL, TIME_MODE, TIME_MODE_EOL,

		MESSAGE_REFERENCE_BOL, MESSAGE_SEQUENCE_REFERENCE, MESSAGE_REFERENCE_EOL,

		ACTION_INFORMATION_BOL, ACTION_IDENTIFIER, ACTION_INFORMATION_EOL,

		FLIGHT_INFORMATION_BOL, FLIGHT_IDENTIFIER, NEW_FLIGHT_IDENTIFIER, FLIGHT_INFORMATION_EOL,

		EQUIPMENT_INFORMATION_BOL, SERVICE_TYPE, AIRCRAFT_TYPE, PASSENGER_RES_BOOKING_DESIGNATOR, EQUIPMENT_INFORMATION_EOL,

		LEG_INFORMATION_BOL, DEPARTURE_AIRPORT, DEPATURE_TIME, ARRIVAL_AIRPORT, ARRIVAL_TIME, LEG_INFORMATION_EOL,
		
		SEGMENT_INFORMATION_BOL, OTHER_SEGMENT_INFO, SEGMENT_INFORMATION_EOL,
		
		COMMENTS_BOL, COMMENTS, COMMENTS_EOL,

		SUB­MESSAGE_SEPARATION_BOL, SUB­MESSAGE_SEPARATION, SUB­MESSAGE_SEPARATION_EOL,

		SUPPLEMENTARY_INFORMATIONS_BOL, SUPPLEMENTARY_INFORMATION, SUPPLEMENTARY_INFORMATIONS_EOL,

		END_ASM;
	}

	public enum ASMSubMessageType {
		NEW("NEW"), CNL("CNL"), RPL("RPL"), EQT("EQT"), TIM("TIM"), FLT("FLT");

		private final ASMSubMessageFormat asmSubMessageFormat;

		public ASMSubMessageFormat getASMSubMessageFormat() {
			return asmSubMessageFormat;
		}

		private ASMSubMessageType(String code) {
			asmSubMessageFormat = new ASMSubMessageFormat(code);
		}
	}

	public static interface PNLADLMessageConstants {

		public static final String PNL = "PNL";

		public static final String ADL = "ADL";

		public static final String PART = "PART";

		public static final String RBD = "RBD";

		public static final String ENDADL = "ENDADL";

		public static final String ENDPNL = "ENDPNL";

		public static final String ENDPART = "ENDPART";
		
		public static final String END = "END";
		
		public static final String ADL_DEL = "DEL";
		
		public static final String ADL_ADD = "ADD";
		
		public static final String ADL_CHG = "CHG";

		public static final String L = "L";

		public static final String REMARKS = ".R/";

		public static final String REMARKS_CONT = ".RN/";

		public static final String TKNE = "TKNE";

		public static final String HK = "HK";

		public static final String KK = "KK";

		public static final String EPAY = "EPAY";

		public static final String DOCS = "DOCS";
		
		public static final String DOCO = "DOCO";

		public static final String SPML = "SPML";

		public static final String RQST = "RQST";

		public static final String EXST = "EXST";

		public static final String SPBG = "SPBG";

		public static final String INF = "INF";

		public static final String INFT = "INFT";

		public static final String INBOUND = ".I/";

		public static final String ONWARD = ".O";

		public static final String WAITLIST = ".WL/";

		public static final String ID2N2 = ".ID2/ID00N2";

		public static final String CHLD = "CHLD";
		
		public static final String M = "M";
		
		public static final String HYPHEN="-";
		
		public static final String FWD="/";
		
		public static final String SP=" ";
		
		public static final String DOT=".";
		
		public static final String NIL= "NIL";
		
		public static final String CFG= "CFG";

	}

	interface MsgDataElementIdentifier {
		String E_TICKETING = "505";
	}

	interface MsgConstants {
		String E_TICKET_CANDIDATE = "ET";
		String E_TICKET_NON_CANDIDATE = "EN";
	}

	interface SupplementaryInformation {
		String SUPPLEMENTARY_INFORMATION_INDICATOR = "SI";
	}

}
