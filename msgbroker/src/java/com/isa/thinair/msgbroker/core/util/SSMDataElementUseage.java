package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SMDataElementRequirmentCategory;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.SSMDataElement;

public class SSMDataElementUseage {
	private SSMDataElement ssmDataElement;
	private SMDataElementRequirmentCategory ssmDataElementRequirmentCategory;
	private SSMDataElement nextSSMDataElement;

	public SSMDataElementUseage(SSMDataElement element, SMDataElementRequirmentCategory category, SSMDataElement nextElement) {
		nextSSMDataElement = nextElement;
		ssmDataElement = element;
		ssmDataElementRequirmentCategory = category;
	}

	public SSMDataElement getSsmDataElement() {
		return ssmDataElement;
	}

	public void setSsmDataElement(SSMDataElement ssmDataElement) {
		this.ssmDataElement = ssmDataElement;
	}

	public SMDataElementRequirmentCategory getSsmDataElementRequirmentCategory() {
		return ssmDataElementRequirmentCategory;
	}

	public void setSsmDataElementRequirmentCategory(SMDataElementRequirmentCategory ssmDataElementRequirmentCategory) {
		this.ssmDataElementRequirmentCategory = ssmDataElementRequirmentCategory;
	}

	public SSMDataElement getNextSSMDataElement() {
		return nextSSMDataElement;
	}

	public void setNextSSMDataElement(SSMDataElement nextSSMDataElement) {
		this.nextSSMDataElement = nextSSMDataElement;
	}
}
