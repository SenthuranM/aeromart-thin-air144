package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.msgbroker.core.util.Constants;

public class MessageBeanFactory {
	public static IBMsgProcessorBD getIBMsgProcessor(String currentServiceType) {
		
		if(Constants.ServiceTypes.ReservationService.equals(currentServiceType) || Constants.ServiceTypes.DVD.equals(currentServiceType)
				|| Constants.ServiceTypes.ASC.equals(currentServiceType)){
			return (IBMsgProcessorBD) new ReservationIBMsgProcessorImpl();
		} else if (Constants.ServiceTypes.SSM.equals(currentServiceType) || Constants.ServiceTypes.ASM.equals(currentServiceType)
				|| Constants.ServiceTypes.AVS.equals(currentServiceType)) {
			return (IBMsgProcessorBD) new SSMASMIBMsgProcessorImpl();
		}
		return null;
	}
}
