package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCREQ;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCREQRequestDTO;

public class CouponStatusTKCREQReqAdaptor
		extends AbstractGrpcRequestCreator<com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCREQ, CouponStatusTKCREQRequestDTO> {

	public CouponStatusTKCREQReqAdaptor() {
		super(CouponStatusTKCREQ.class, CouponStatusTKCREQRequestDTO.class);
	}

}

