package com.isa.thinair.msgbroker.core.dto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.msgbroker.core.util.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.msgbroker.core.parser.SitaTexParser;

public class SitaTexMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Log log = LogFactory.getLog(SitaTexMessage.class);

	private boolean parseFailed;
	private MessageHeader messageHeader;
	private MessageBody messageBody;

	public enum MessageType {
		SITA_TEX, PLAIN
	}

	public SitaTexMessage() {
	}

	public SitaTexMessage(String message, MessageType messageType) {

		String elementName = null;
		String[] lines;
		Map<String, List<String>> elements = new HashMap<String, List<String>>();
		boolean parsed;

		messageHeader = new MessageHeader();
		messageBody = new MessageBody();

		try {
			lines = message.split(SitaTexElement.EOL);

			for (String currentLine : lines) {

				switch (messageType) {
				case SITA_TEX:
					if (currentLine.startsWith(SitaTexElement.ELEMENT_ID_PREFIX)) {
						elementName = currentLine.substring(SitaTexElement.ELEMENT_ID_PREFIX.length());
						elements.put(elementName, new ArrayList<String>());
					} else {
						elements.get(elementName).add(currentLine);
					}
					break;
				case PLAIN:
					if (!elements.containsKey(SitaTexElement.TEXT)) {
						elements.put(SitaTexElement.TEXT, new ArrayList<String>());
					}
					elements.get(SitaTexElement.TEXT).add(currentLine);
					break;
				}


			}

			for (String elementId : elements.keySet()) {
				parsed = false;
				try {
					parsed = SitaTexParser.parseSitaTex(messageHeader, elementId, elements.get(elementId));
				} catch (Exception e) {
					parseFailed = true;
					log.error("SITATEX Parse Failed - message : " + message);
				}
				if (!parsed) {
					try {
						parsed = SitaTexParser.parseSitaTex(messageBody, elementId, elements.get(elementId));
					} catch (Exception e) {
						parseFailed = true;
						log.error("SITATEX Parse Failed - message : " + message);
					}
				}

				log.info("SITATEX message : " + message + " ---- Parsing status : " + parsed);
			}

		} catch (Exception e) {
			log.error("SITATEX Parse Failed - message : " + message);
		}

	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

	public MessageBody getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(MessageBody messageBody) {
		this.messageBody = messageBody;
	}

	public String toSitaTexMessage(MessageType messageMode) {
		String message = null;

		switch (messageMode) {
		case SITA_TEX:
			message = SitaTexParser.getSitaTex(messageHeader) + SitaTexElement.EOL + SitaTexParser.getSitaTex(messageBody);
			break;
		case PLAIN:
			message = messageBody.getMessageText();
			break;
		}

		return message;
	}

	public boolean isParseFailed() {
		return parseFailed;
	}

	public void setParseFailed(boolean parseFailed) {
		this.parseFailed = parseFailed;
	}

	public boolean isAcknowledgement() {
		return messageHeader.getMessageType() != null
				&& messageHeader.getMessageType().equalsIgnoreCase(SitaTexElement.SitaTexHeader.SENT);
	}

	public static class MessageHeader {
		private String messageType;
		private Date date;
		private String priority;
		private List<SitaTexDestination> destinations;
		private String originAddress;
		private String airlineDesignator;
		private String messageId;
		private String subject;
		private String smi;

		public MessageHeader() {
			destinations = new ArrayList<SitaTexDestination>();
		}

		public static class SitaTexDestination {
			private String addressType;
			private String address;
			private String extension;

			public String getAddressType() {
				return addressType;
			}

			public void setAddressType(String addressType) {
				this.addressType = addressType;
			}

			public String getAddress() {
				return address;
			}

			public void setAddress(String address) {
				this.address = address;
			}

			public String getExtension() {
				return extension;
			}

			public void setExtension(String extension) {
				this.extension = extension;
			}
		}

		public String getMessageType() {
			return messageType;
		}

		public void setMessageType(String messageType) {
			this.messageType = messageType;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public String getPriority() {
			return priority;
		}

		public void setPriority(String priority) {
			this.priority = priority;
		}

		public List<SitaTexDestination> getDestinations() {
			return destinations;
		}

		public void setDestinations(List<SitaTexDestination> destinations) {
			this.destinations = destinations;
		}

		public String getOriginAddress() {
			return originAddress;
		}

		public void setOriginAddress(String originAddress) {
			this.originAddress = originAddress;
		}

		public String getAirlineDesignator() {
			return airlineDesignator;
		}

		public void setAirlineDesignator(String airlineDesignator) {
			this.airlineDesignator = airlineDesignator;
		}

		public String getMessageId() {
			return messageId;
		}

		public void setMessageId(String messageId) {
			this.messageId = messageId;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getSmi() {
			return smi;
		}

		public void setSmi(String smi) {
			this.smi = smi;
		}
	}

	public static class MessageBody {
		private String messageText;
		private List<String> attachments;

		public MessageBody() {
			attachments = new ArrayList<String>();
		}

		public String getMessageText() {
			return messageText;
		}

		public void setMessageText(String messageText) {
			this.messageText = messageText;
		}

		public List<String> getAttachments() {
			return attachments;
		}

		public void setAttachments(List<String> attachments) {
			this.attachments = attachments;
		}
	}

}
