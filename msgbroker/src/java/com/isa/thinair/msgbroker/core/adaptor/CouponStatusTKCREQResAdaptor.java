package com.isa.thinair.msgbroker.core.adaptor;

import com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCRESResponse;
import com.isa.cloud.support.generic.adaptor.AbstractRequestGrpcDtoAdapter;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCRESResponseDTO;

public class CouponStatusTKCREQResAdaptor extends AbstractRequestGrpcDtoAdapter<com.accelaero.msgadaptor.OutboundTypeAMessageServiceWrapper.CouponStatusTKCRESResponse, CouponStatusTKCRESResponseDTO> {

	public CouponStatusTKCREQResAdaptor() {
		super(CouponStatusTKCRESResponse.class, CouponStatusTKCRESResponseDTO.class);
	}

}
