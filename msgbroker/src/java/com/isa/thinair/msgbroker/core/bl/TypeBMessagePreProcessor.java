package com.isa.thinair.msgbroker.core.bl;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentTypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.MessageError;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.util.GdsApplicationError;
import com.isa.thinair.msgbroker.api.dto.BookingRequestDTO;
import com.isa.thinair.msgbroker.core.dto.MessageParserResponseDTO;
import com.isa.thinair.msgbroker.core.util.LookupUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TypeBMessagePreProcessor {

	public static MessageError handleResMessage(MessageParserResponseDTO parserResponseDTO, TypeBRQ typeBRequest)
		throws ModuleException {

		MessageError messageError = new MessageError();

		BookingRequestDTO bookingRequestDTO;
		InventoryAdjustmentTypeBRQ inventoryAdjustmentTypeBRQ;
		List<Integer> seatBlockIds;
		Integer seatsBlocked = 0;


		// TODO -- multi-leg + infant
		if (parserResponseDTO.getRequestDTO() instanceof BookingRequestDTO) {

			bookingRequestDTO = (BookingRequestDTO)parserResponseDTO.getRequestDTO();

			if (typeBRequest instanceof InventoryAdjustmentTypeBRQ) {
				inventoryAdjustmentTypeBRQ = (InventoryAdjustmentTypeBRQ) typeBRequest;
				seatBlockIds = new ArrayList<Integer>();

				for(TempSegBcAllocDTO blockedSeats : inventoryAdjustmentTypeBRQ.getBlockedSeats()) {

					if (seatsBlocked == null) {
						seatsBlocked = blockedSeats.getSeatsBlocked();
					} else if (seatsBlocked != null && !seatsBlocked.equals(blockedSeats.getSeatsBlocked())) {
						messageError.addError(GdsApplicationError.UNEQUAL_NUMBER_IN_PARTY);
						break;
					}
					seatBlockIds.add(blockedSeats.getId());
				}

				if (inventoryAdjustmentTypeBRQ.getPaxCount() != seatsBlocked) {
					messageError.addError(GdsApplicationError.UNEQUAL_NUMBER_IN_PARTY);
				}
			}
		}

		return messageError;

	}
}
