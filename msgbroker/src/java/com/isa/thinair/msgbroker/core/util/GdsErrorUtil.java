package com.isa.thinair.msgbroker.core.util;

import com.isa.thinair.gdsservices.api.util.GdsApplicationError;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class GdsErrorUtil {
	private static Log log = LogFactory.getLog(GdsErrorUtil.class);

	private GdsErrorUtil() {
	}

	public static List<GdsApplicationError> toGdsApplicationErrors(List<String> errorCode) {

		List<GdsApplicationError> gdsApplicationErrors = new ArrayList<>();

		for (String e : errorCode) {
			if (e.equals("airreservations.arg.flightInClosure")) {
				gdsApplicationErrors.add(GdsApplicationError.UNABLE_TO_SELL_TIME_LIMIT_REACHED);
			} else if (e.equals("gdsservices.validators.specified.ssr.codes.notsupported")
					|| e.equals("airreservations.anci.ssr.not.available")) {
				gdsApplicationErrors.add(GdsApplicationError.SSR_NOT_AVAILABLE);
			} else if (e.equals("gdsservices.response.message.invalidPnrCancel")) {
				gdsApplicationErrors.add(GdsApplicationError.OPERATION_NOT_AUTHORIZED_FOR_PNR);
			} else {
				log.error("un-categorized error ---- " + e);
				gdsApplicationErrors.add(GdsApplicationError.UNABLE_TO_PROCESS);
			}
		}


		return gdsApplicationErrors;
	}
}
