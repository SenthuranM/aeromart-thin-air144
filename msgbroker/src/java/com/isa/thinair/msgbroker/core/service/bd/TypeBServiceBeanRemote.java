package com.isa.thinair.msgbroker.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;

@Remote
public interface TypeBServiceBeanRemote extends TypeBServiceBD {
	
}
