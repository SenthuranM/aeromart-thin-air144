package com.isa.thinair.msgbroker.core.util;

import java.io.Serializable;

public final class MessagingConstants implements Serializable {

	/** Denotes mail server configuration keys */
	public static interface MailServerConfigurationKeys {
		public static final String MAIL_SERVER_CONFIG_BSM = "mailserver_bsm";
		public static final String MAIL_SERVER_CONFIG_BPM = "mailserver_bpm";
		public static final String MAIL_SERVER_CONFIG_GDS_DEFAULT = "mailserver_gds_default";
		public static final String MAIL_SERVER_CONFIG_GDS_SECONDARY = "mailserver_gds_secondary";
	}

	/** Denotes topic configuration keys */
	public static interface TopicConfigurationKeys {
		public static final String TOPIC_CONFIG_BPM = "topic_bpm";
	}

	/** Denotes queue configuration keys */
	public static interface QueueJndINames {
		public static final java.lang.String DESTINATION_JNDI_NAME = "queue/bsmmails";
		public static final java.lang.String GDS_INCOMING_MESSAGE_QUEUE_JNDI_NAME = "queue/incominggdsemails";
		public static final java.lang.String HOST_NOTIFICATIONS_QUEUE_JNDI_NAME = "queue/hostnotifications";
	}

}