package com.isa.thinair.msgbroker.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.core.config.ExternalSitaConfig;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.msgbroker.api.model.OutMessage;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;
import com.isa.thinair.msgbroker.core.bl.ASMComposer;
import com.isa.thinair.msgbroker.core.bl.CommonMessageProcessingBL;
import com.isa.thinair.msgbroker.core.bl.GDSMessageHandler;
import com.isa.thinair.msgbroker.core.bl.SSMComposer;
import com.isa.thinair.msgbroker.core.persistence.dao.AdminDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.MessageDAO;
import com.isa.thinair.msgbroker.core.persistence.dao.ServiceClientReqDaoJDBC;
import com.isa.thinair.msgbroker.core.service.bd.SSMASMServiceDelegateImpl;
import com.isa.thinair.msgbroker.core.service.bd.SSMASMServiceLocalDelegateImpl;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageProcessingConstants;
import com.isa.thinair.msgbroker.core.util.ServiceUtil;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Ishan
 */
@Stateless
@RemoteBinding(jndiBinding = "SSMASMService.remote")
@LocalBinding(jndiBinding = "SSMASMService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SSMASMServiceBean extends PlatformBaseSessionBean implements SSMASMServiceDelegateImpl,
		SSMASMServiceLocalDelegateImpl {

	private static Log log = LogFactory.getLog(SSMASMServiceBean.class);
	// private MessageBD messageBD = BusinessDelegateFactory.getMessageBusinessDelegate();
	private AdminDAO adminDAO;
	private MessageDAO messageDAO;
	private ServiceClientReqDaoJDBC serviceClientReqDaoJDBC;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendStandardScheduleMessage(String gdsCode, String airlineCode, SSIMessegeDTO ssmDTO) throws ModuleException {
		SSMComposer ssmComposer = new SSMComposer();
		ServiceClient gds;
		ServiceBearer airline;
		String ttyAddress;
		try {
			log.info("SSMASMServiceBean --> Received an SSM to send. ");
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.SSM.toString());

			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			outMessage.setNonGDSMessageFlag('N');
			gds = getAdminDAO().getServiceClient(gdsCode);
			
			boolean isCSCarrier = ServiceUtil.isCodeShareGDSCode(gdsCode);
			ssmDTO.setCodeShareCarrier(isCSCarrier);
			// Call composer
			log.debug(ssmComposer.composeSSMessage(ssmDTO, gds.getAddAddressToMessageBody()));
			log.debug("GDS Code: " + gdsCode + "   Airline Code:" + airlineCode);
			airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
			ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode, MessageComposerConstants.Message_Identifier.SSM.toString());	
			if (ttyAddress != null) {
				ssmDTO.setRecipientAddress(ttyAddress);
				ssmDTO.setSenderAddress(airline.getSitaAddress());
				outMessage.setOutMessageText(ssmComposer.composeSSMessage(ssmDTO, gds.getAddAddressToMessageBody()));
				outMessage.setReceivedDate(new Date());
				
				if (gds != null && airline != null) {
					outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
							gds.getEmailDomain()));
					outMessage.setRecipientTTYAddress(ttyAddress);
					outMessage.setSentEmailAddress(airline.getOutgoingEmailFromAddress());
					outMessage.setSentTTYAddress(airline.getSitaAddress());
					outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
					log.debug("Before SSM save out message");
					getMessageDAO().saveOutMessage(outMessage);
				}
			}  else {
				log.error("SSM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
						+ gdsCode + ".");
			}
			
		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendAdHocScheduleMessage(String gdsCode, String airlineCode, ASMessegeDTO asmDTO) throws ModuleException {
		ASMComposer asmComposer = new ASMComposer();
		ServiceClient gds;
		ServiceBearer airline;
		String ttyAddress;
		try {
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.ASM.toString());
			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			gds = getAdminDAO().getServiceClient(gdsCode);
			
			boolean isCSCarrier = ServiceUtil.isCodeShareGDSCode(gdsCode);
			asmDTO.setCodeShareCarrier(isCSCarrier);
			
			airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
			ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode,
					MessageComposerConstants.Message_Identifier.ASM.toString());
			if (ttyAddress != null) {
				asmDTO.setRecipientAddress(ttyAddress);
				asmDTO.setSenderAddress(airline.getSitaAddress());
				// Call composer
				outMessage.setOutMessageText(asmComposer.composeASMessage(asmDTO, gds.getAddAddressToMessageBody()));
				outMessage.setReceivedDate(new Date());
				outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
						gds.getEmailDomain()));
				outMessage.setRecipientTTYAddress(ttyAddress);
				outMessage.setSentEmailAddress(airline.getOutgoingEmailFromAddress());
				outMessage.setSentTTYAddress(airline.getSitaAddress());
				outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				getMessageDAO().saveOutMessage(outMessage);
			} else {
				log.info("ASM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
						+ gdsCode + ".");
			}
			
			

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	public void publishSSMASMMessages() throws ModuleException {
		List<String> messageTypes = new ArrayList<String>();
		messageTypes.add(MessageComposerConstants.Message_Identifier.SSM.toString());
		messageTypes.add(MessageComposerConstants.Message_Identifier.ASM.toString());

		GDSMessageHandler handler = new GDSMessageHandler();
		handler.setMessageTypes(messageTypes);
		handler.execute(GDSMessageHandler.Direction.OUT, GDSMessageHandler.Category.SCHEDULE, true);

	}

	/**
	 * return the admin DAO
	 * 
	 * @return
	 */
	private AdminDAO getAdminDAO() {
		if (adminDAO == null) {
			adminDAO = LookupUtil.lookupAdminDAO();
		}
		return adminDAO;
	}

	/**
	 * return the message DAO
	 * 
	 * @return
	 */
	private MessageDAO getMessageDAO() {
		if (messageDAO == null) {
			messageDAO = LookupUtil.lookupMessageDAO();
		}
		return messageDAO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendStandardScheduleMessageForExternalSystem(String airlineCode, SSIMessegeDTO ssmDTO) throws ModuleException {
		SSMComposer ssmComposer = new SSMComposer();
		String ttyAddress;
		String emailAddress;
		try {
			log.info("SSMASMServiceBean --> Received an SSM to send for External System. ");
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.SSM.toString());

			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);

			// Call composer
			log.debug(ssmComposer.composeSSMessage(ssmDTO, false));
			log.debug("External System: " + ssmDTO.getExternalEmailAddres() + ssmDTO.getExternalSitaAddress()
					+ "   Airline Code:" + airlineCode);
			ExternalSitaConfig sitaConfig = (ExternalSitaConfig) ((MessagingModuleConfig) LookupServiceFactory.getInstance()
					.getModuleConfig("messaging")).getExternalSitaLocationConfigurationMap().get(
					MessagingCustomConstants.TEST_EXT_SITA);
			AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
			String sendersEmailAddress = airReservationConfig.getSitaMessageSender();
			String sendersSITAAddress = sitaConfig.getSitaAddress();
			ttyAddress = ssmDTO.getExternalSitaAddress();
			emailAddress = ssmDTO.getExternalEmailAddres();
			if (ttyAddress != null && sendersSITAAddress != null) {
				ssmDTO.setRecipientAddress(ttyAddress);
				ssmDTO.setSenderAddress(sendersSITAAddress);
				outMessage.setOutMessageText(ssmComposer.composeSSMessage(ssmDTO, false));
				outMessage.setReceivedDate(new Date());
				outMessage.setRecipientEmailAddress(emailAddress);
				outMessage.setRecipientTTYAddress(ttyAddress);
				outMessage.setSentEmailAddress(sendersEmailAddress);
				outMessage.setSentTTYAddress(sendersSITAAddress);
				outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				outMessage.setNonGDSMessageFlag('Y');
				log.debug("Before SSM save out message");
				getMessageDAO().saveOutMessage(outMessage);

			} else {
				log.error("SSM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the external service client or Sender.");
			}

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	private ServiceClientReqDaoJDBC getServiceClientReqTypeDAO() {
		if (serviceClientReqDaoJDBC == null) {
			serviceClientReqDaoJDBC = LookupUtil.lookupServiceClientReqDAO();
		}
		return serviceClientReqDaoJDBC;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendAdHocScheduleMessageToExternalSystem(String airlineCode, ASMessegeDTO asmDTO) throws ModuleException {
		
		ASMComposer asmComposer = new ASMComposer();
		String ttyAddress;
		String emailAddress;
		try {
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.ASM.toString());
			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			ExternalSitaConfig sitaConfig = (ExternalSitaConfig) ((MessagingModuleConfig) LookupServiceFactory.getInstance()
					.getModuleConfig("messaging")).getExternalSitaLocationConfigurationMap().get(
					MessagingCustomConstants.TEST_EXT_SITA);
			AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();
			String sendersEmailAddress = airReservationConfig.getSitaMessageSender();
			String sendersSITAAddress = sitaConfig.getSitaAddress();
			ttyAddress = asmDTO.getExternalSitaAddress();
			emailAddress = asmDTO.getExternalEmailAddres();
			if (ttyAddress != null && emailAddress != null) {
				asmDTO.setRecipientAddress(ttyAddress);
				asmDTO.setSenderAddress(sendersSITAAddress);
				// Call composer
				outMessage.setOutMessageText(asmComposer.composeASMessage(asmDTO, true));
				outMessage.setReceivedDate(new Date());
				outMessage.setRecipientEmailAddress(emailAddress);
				outMessage.setRecipientTTYAddress(ttyAddress);
				outMessage.setSentEmailAddress(sendersEmailAddress);
				outMessage.setSentTTYAddress(sendersSITAAddress);
				outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
				outMessage.setNonGDSMessageFlag('Y');
				log.debug("Before ASM save out message");
				getMessageDAO().saveOutMessage(outMessage);
			} else {
				log.info("ASM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address or Email address cannot be found for the service client");
			}			

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	@Override
	public boolean isEnableAttachingDefaultAnciTemplate(String tty, String requestType) throws ModuleException {
		try {

			String gdsCode = LookupUtil.lookupServiceClientReqDAO().getServiceClientForReqTypeTTY(tty, requestType);
			Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();
			for (GDSStatusTO gdsStsTo : gdsStatusMap.values()) {
				if (gdsStsTo.getGdsCode().equals(gdsCode)) {
					return gdsStsTo.isEnableAttachDefaultAnciTemplate();
				}
			}
			return false;

		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}
	}

	@Override
	public void sendStandardScheduleSubMessages(String gdsCode, String airlineCode, Map<Integer, SSIMessegeDTO> ssmMessegeDTOMap,
			Collection<String> bookingClassList) throws ModuleException {

		ServiceClient gds;
		ServiceBearer airline;
		String ttyAddress;
		try {
			log.info("SSMASMServiceBean --> Received an SSM Sub-Messages to send.");
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.SSM.toString());

			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			outMessage.setNonGDSMessageFlag('N');
			gds = getAdminDAO().getServiceClient(gdsCode);

			boolean isCSCarrier = ServiceUtil.isCodeShareGDSCode(gdsCode);
			log.debug("GDS Code: " + gdsCode + "   Airline Code:" + airlineCode);
			airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
			ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode,
					MessageComposerConstants.Message_Identifier.SSM.toString());
			if (ttyAddress != null) {
				SSMComposer ssmComposer = new SSMComposer();
				String msgStr = ssmComposer.composeSSMSubMessages(ssmMessegeDTOMap, gds.getAddAddressToMessageBody(),
						bookingClassList, ttyAddress, airline.getSitaAddress(), isCSCarrier);
				if (gds != null && airline != null && !StringUtil.isNullOrEmpty(msgStr)) {
					outMessage.setOutMessageText(msgStr);
					outMessage.setReceivedDate(new Date());
					outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
							gds.getEmailDomain()));
					outMessage.setRecipientTTYAddress(ttyAddress);
					outMessage.setSentEmailAddress(airline.getOutgoingEmailFromAddress());
					outMessage.setSentTTYAddress(airline.getSitaAddress());
					outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
					log.debug("Before SSM save out message");
					getMessageDAO().saveOutMessage(outMessage);
				}
			} else {
				log.error("SSM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
						+ gdsCode + ".");
			}

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}

	}

	@Override
	public void sendAdHocScheduleSubMessages(String gdsCode, String airlineCode, Map<Integer, ASMessegeDTO> asmMessegeDTOMap,
			Collection<String> bookingClassList) throws ModuleException {

		ServiceClient gds;
		ServiceBearer airline;
		String ttyAddress;
		try {
			log.info("SSMASMServiceBean --> Received an ASM Sub-Messages to send.");
			// fill out message
			OutMessage outMessage = new OutMessage();
			outMessage.setAirLineCode(airlineCode);
			outMessage.setMessageType(MessageComposerConstants.Message_Identifier.ASM.toString());
			outMessage.setModeOfCommunication(MessageProcessingConstants.ModeOfCommunication.Email);
			gds = getAdminDAO().getServiceClient(gdsCode);

			boolean isCSCarrier = ServiceUtil.isCodeShareGDSCode(gdsCode);

			airline = getAdminDAO().getServiceBearer(airlineCode, gdsCode);
			ttyAddress = getServiceClientReqTypeDAO().getTTYForServiceClientReqType(gdsCode,
					MessageComposerConstants.Message_Identifier.ASM.toString());
			if (ttyAddress != null) {
				ASMComposer asmComposer = new ASMComposer();
				String msgStr = asmComposer.composeASMSubMessages(asmMessegeDTOMap, gds.getAddAddressToMessageBody(),
						bookingClassList, ttyAddress, airline.getSitaAddress(), isCSCarrier);
				if (gds != null && airline != null && !StringUtil.isNullOrEmpty(msgStr)) {
					outMessage.setOutMessageText(msgStr);
					outMessage.setReceivedDate(new Date());
					outMessage.setRecipientEmailAddress(CommonMessageProcessingBL.buildEmailAddressForTTYAddress(ttyAddress,
							gds.getEmailDomain()));
					outMessage.setRecipientTTYAddress(ttyAddress);
					outMessage.setSentEmailAddress(airline.getOutgoingEmailFromAddress());
					outMessage.setSentTTYAddress(airline.getSitaAddress());
					outMessage.setStatusIndicator(MessageProcessingConstants.OutMessageProcessStatus.Untransmitted);
					getMessageDAO().saveOutMessage(outMessage);
				}

			} else {
				log.info("ASM will not be saved in MB_T_OUT_MESSAGE beacuese TTY address cannot be found for the service client : "
						+ gdsCode + ".");
			}

		} catch (ModuleException e) {
			log.error(e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode());
		} catch (CommonsDataAccessException daex) {
			this.sessionContext.setRollbackOnly();
			log.error(daex);
			throw new ModuleException(daex, daex.getExceptionCode());
		}

	}

}
