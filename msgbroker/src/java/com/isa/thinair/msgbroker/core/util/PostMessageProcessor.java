package com.isa.thinair.msgbroker.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.model.InMessage;
import com.isa.thinair.msgbroker.core.bl.MesageConstants;
import com.isa.thinair.msgbroker.core.bl.PostMessageMDBInvoker;

/**
 * Class for adding messages from queue to jms
 * @author rajitha
 *
 */
public class PostMessageProcessor {

	private static final Log log = LogFactory.getLog(PostMessageProcessor.class);

	public void postMessageProcess(InMessage prevInMessage) throws ModuleException {

		Integer prevInMessageId = prevInMessage.getInMessageID();
		Integer inMessageProcessingQueueId = LookupUtil.lookupMessageDAO()
				.getFlightDesignatorForPostProcessing(prevInMessageId, MesageConstants.InMessageQueueStatus.LOCK);

		if (inMessageProcessingQueueId != null) {
			Integer nextMsgIdToProcess = getNextInMessageIdFromWaitingQueue(inMessageProcessingQueueId);
			if ((nextMsgIdToProcess != null)) {

				if (prevInMessageId.intValue() != nextMsgIdToProcess.intValue()) {
					LookupUtil.lookupMessageDAO().updateNextMsgIdToProcess(inMessageProcessingQueueId, nextMsgIdToProcess);
					removeMsgFromWaitingQueue(nextMsgIdToProcess, inMessageProcessingQueueId);
					nextMessageMDBInvoker(nextMsgIdToProcess);
				} else {
					removeMsgFromWaitingQueue(nextMsgIdToProcess, inMessageProcessingQueueId);
					releaseLockForFlight(inMessageProcessingQueueId, prevInMessageId);
				}

			} else {

				releaseLockForFlight(inMessageProcessingQueueId, prevInMessageId);
				removeFlightDesignatorDependancies(inMessageProcessingQueueId);
			}
			
			
			updateLastMessageProcessingTime(inMessageProcessingQueueId);
			log.info("post messages processor updated " + prevInMessageId);
			
			
		}

	}

	private void removeFlightDesignatorDependancies(Integer inMessageProcessingQueueId) {
		LookupUtil.lookupMessageDAO().removeFlightDependancies(inMessageProcessingQueueId);
		
	}

	private void nextMessageMDBInvoker(Integer inMsgId) {
		PostMessageMDBInvoker mdbInvoker = new PostMessageMDBInvoker();
		mdbInvoker.invokeInboundMessageProcessorMDB(inMsgId);
	}

	private Integer getNextInMessageIdFromWaitingQueue(Integer msgProcessingQueueId) {
		return LookupUtil.lookupMessageDAO().getNextInMsgIdInQueue(msgProcessingQueueId);
	}

	private void removeMsgFromWaitingQueue(int inMsgID, Integer msgProcessingQueueId){
		LookupUtil.lookupMessageDAO().removeMessageFromWaitingQueue(inMsgID, msgProcessingQueueId);
	}

	private void releaseLockForFlight(Integer inMessageProcessingQueueId, Integer inMsgId) {
		LookupUtil.lookupMessageDAO().updateScheduleForLockStatus(MesageConstants.InMessageQueueStatus.UNLOCK, inMsgId,
				inMessageProcessingQueueId);
	}

	private void updateLastMessageProcessingTime(Integer inMessageProcessingQueueId) {
		LookupUtil.lookupMessageDAO().updateLastMessageProcessingTime(inMessageProcessingQueueId);

	}

}
