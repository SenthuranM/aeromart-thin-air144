/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.reportingframework.clients;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.reportingframework.core.bl.ReportingManager;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;

/**
 * Sample PDF client
 *  
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PdfClient extends HttpServlet
{
	/**
	 * Handles client requests
	 */
	public void service(HttpServletRequest request, HttpServletResponse response
		) throws ServletException
   	{
		ServletContext context = this.getServletConfig().getServletContext();

		try {
			// Store the template name with the template reference path 
			// Note: this can be performed when application starts up automatically
			// Template name can be WebappReport
			// Template path can be D:/jboss-4.0.1sp1/server/default/deploy/webapp.war/reports/sample.jrxml
			ReportingManager.storeIreportTemplateRefs("WebappReport", context.getRealPath("/reports/sample.jrxml"));
			
			// Parameters that need to be passed to the report
			Map parameters = new HashMap();
			
			ReportingManager.createPDFReport("WebappReport", parameters, null, response);
		} catch (ReportingException e) {
			// TODO Ofcause do handle the error :-)
		}
	}
}
