/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.reportingframework.clients;

import java.util.Random;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;

/**
 * Sample framework local client calling reporting framework
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class FrameworkClient {
	
	/**
	 * Client start up
	 * @param args
	 */
	public static void main(String[] args) {
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule sampleModule = lookup.getModule(ReportingframeworkConstants.MODULE_NAME);

		System.out.println("Lookup successful...");

		ReportingFrameworkBD delegate = null;
		try {
			delegate = (ReportingFrameworkBD)sampleModule.getServiceBD("reportingframework.service.local");
			delegate.createHTMLReport(null,null,null,null,null,null);
			System.out.println("Delegate creation successful...");
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
	}
}
