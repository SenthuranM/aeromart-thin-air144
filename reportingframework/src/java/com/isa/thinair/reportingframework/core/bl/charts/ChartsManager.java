package com.isa.thinair.reportingframework.core.bl.charts;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;

import com.isa.thinair.reportingframework.api.dto.BarChartDTO;
import com.isa.thinair.reportingframework.api.dto.ChartCommonsDTO;
import com.isa.thinair.reportingframework.api.dto.LineChartDTO;
import com.isa.thinair.reportingframework.api.dto.PieChartDTO;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_TYPES;

/**
 * Chart Manager to create a chart according to the required type.
 * 
 * @author Navod Ediriweera
 * @since Jul 09, 2009
 */
public class ChartsManager {

	/************************ Charts *****************************************************/
	/**
	 * Creates a 3D or 2D Chart
	 * 
	 * @param chartDTO
	 * @param pieChartDataSet
	 * @return
	 */
	public static Chart createChart(ChartCommonsDTO chartDTO) throws ReportingException {
		try {
			Chart chart = null;
			// validating chart data
			if (chartDTO.getChartResultSet() == null || chartDTO.getChartType() == null) {
				throw new ReportingException("Invalid Chart Data");
			}

			if (chartDTO.getChartType().equals(CHART_TYPES.PIE_CHART_3D)) {
				PieChartDTO pieChartDTO = (PieChartDTO) chartDTO;
				chart = ChartsCreater.createPieChart3D(pieChartDTO);
			} else if (chartDTO.getChartType().equals(CHART_TYPES.PIE_CHART_2D)) {
				PieChartDTO pieChartDTO = (PieChartDTO) chartDTO;
				chart = ChartsCreater.createPieChart2D(pieChartDTO);
			} else if (chartDTO.getChartType().equals(CHART_TYPES.BAR_CHART_3D)) {
				BarChartDTO barChartDTO = (BarChartDTO) chartDTO;
				chart = ChartsCreater.createBarChart3D(barChartDTO);
			} else if (chartDTO.getChartType().equals(CHART_TYPES.BAR_CHART_2D)) {
				BarChartDTO barChartDTO = (BarChartDTO) chartDTO;
				chart = ChartsCreater.createBarChart2D(barChartDTO);
			} else if (chartDTO.getChartType().equals(CHART_TYPES.LINE_CHART_2D)) {
				LineChartDTO lineChartDTO = (LineChartDTO) chartDTO;
				chart = ChartsCreater.createLineChart2D(lineChartDTO);
			} else if (chartDTO.getChartType().equals(CHART_TYPES.LINE_CHART_3D)) {
				LineChartDTO lineChartDTO = (LineChartDTO) chartDTO;
				chart = ChartsCreater.createLineChart3D(lineChartDTO);
			} else {
				throw new ReportingException("Unsupported Chart Type");
			}
			chart.setHeight(chartDTO.getHeight());
			chart.setWidth(chartDTO.getWidth());
			return chart;

		} catch (Exception e) {
			throw new ReportingException("Cannot create chart", e.getCause());
		}
	}

	/*********************************** Chart Export ***********************************************/
	/**
	 * Exports a Chart as PNG
	 * 
	 * @param response
	 * @param jfreeChart
	 * @param width
	 * @param height
	 */
	public static void exportChartAsPNG(HttpServletResponse response, Chart chart) throws ReportingException {
		try {
			OutputStream outStream = response.getOutputStream();

			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
			byte[] byteStream = byteArray.toByteArray();
			response.setContentType("image/png");

			ChartUtilities.writeChartAsPNG(outStream, chart.getJFreeChart(), chart.getWidth(), chart.getHeight());

			response.setContentLength((int) byteStream.length);
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");

			outStream.flush();
			outStream.close();

		} catch (Exception e) {
			throw new ReportingException("Cannot export Chart", e.getCause());
		}
	}

}
