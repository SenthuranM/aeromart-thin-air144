/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * =============================================================================== 
 */
package com.isa.thinair.reportingframework.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.service-interface module-name="reportingframework" description="reporting framework"
 */
public class ReportingFrameworkService extends DefaultModule {

	/** Constructor */
	public ReportingFrameworkService() {
		super();
	}
}
