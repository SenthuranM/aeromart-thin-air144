/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reportingframework.core.service.bd;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.reportingframework.api.dto.ReportParam;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.reportingframework.api.dto.BarChartDTO;
import com.isa.thinair.reportingframework.api.dto.LineChartDTO;
import com.isa.thinair.reportingframework.api.dto.PieChartDTO;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;
import com.isa.thinair.reportingframework.core.bl.ReportingManager;
import com.isa.thinair.reportingframework.core.bl.charts.Chart;
import com.isa.thinair.reportingframework.core.bl.charts.ChartsManager;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;
import com.isa.thinair.reportingframework.core.config.ReportingFrameworkConfig;

/**
 * Implementation of all the methods required to access the reservation details <strong> Local Access </strong>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.bd-impl id="reportingframework.service.local"
 */
public class ReportingFrameworkBDLocalImpl extends PlatformBaseServiceDelegate implements ReportingFrameworkBD {

	/** Holds the logger instance */
	private final Log log = LogFactory.getLog(this.getClass());

	/**
	 * TODO Move this functionality to the module framework
	 */
	private void storeCompiledReportTemplates() throws ModuleException {
		log.debug("Inside storeCompiledReportTemplates");

		LookupService lookup = LookupServiceFactory.getInstance();
		IModule reportingModule = lookup.getModule(ReportingframeworkConstants.MODULE_NAME);
		ReportingFrameworkConfig reportingFrameworkConfig = (ReportingFrameworkConfig) reportingModule.getModuleConfig();

		Properties properties = reportingFrameworkConfig.getReportParameters();
		Iterator<Object> iterator = properties.keySet().iterator();
		String key = null;
		String value = null;

		try {
			while (iterator.hasNext()) {
				key = (String) iterator.next();
				value = (String) properties.get(key);

				ReportingManager.storeIreportTemplateRefs(key, value);
			}
		} catch (ReportingException e) {
			log.error("##################################################");
			log.error("The reports Initializing process failed...........");
			log.error("##################################################");
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit storeCompiledReportTemplates");
	}

	/**
	 * Creates HTML Report
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, Map<Object, Object> imagesMap, String imagesURI,
			ServletResponse servletResponse) throws ModuleException {
		log.debug("Inside createHTMLReport");
		byte[] byteStream = null;

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			byteStream = ReportingManager.createHTMLReport(templateName, parameters, resultSet, imagesMap, imagesURI,
					servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}catch (Exception e) {
			e.printStackTrace();
		}

		log.debug("Exit createHTMLReport");
		return byteStream;
	}

	/**
	 * Creates HTML Report from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param imagesMap
	 * @param imagesURI
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, Collection<?> colData, Map<Object, Object> imagesMap, String imagesURI,
			ServletResponse servletResponse) throws ModuleException {
		log.debug("Inside createHTMLReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createHTMLReport(templateName, parameters, colData, imagesMap, imagesURI,
					servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createHTMLReport");
		return byteStream;
	}

	/**
	 * Creates PDF Report
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			byteStream = ReportingManager.createPDFReport(templateName, parameters, resultSet, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			byteStream = ReportingManager.createPDFReport(templateName, parameters, colData, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, ResultSet resultSet) throws ModuleException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createPDFReport(templateName, parameters, resultSet);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	/**
	 * Creates Excel Report
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			byteStream = ReportingManager.createXLSReport(templateName, parameters, resultSet, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	/**
	 * Creates Excel Report from a collection of data
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			byteStream = ReportingManager.createXLSReport(templateName, parameters, colData, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	/**
	 * Store Jasper template references
	 * 
	 * @param templateReferenceName
	 * @param jasperTemplateName
	 * @throws ModuleException .
	 * @see com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD#storeJasperTemplateRefs(java.lang.String,
	 *      java.lang.String)
	 */
	public void storeJasperTemplateRefs(String templateReferenceName, String jasperTemplateName) throws ModuleException {
		log.debug("Inside storeJasperTemplateRefs");

		// TODO Remove this, once functionality to the module framework start up exist
		// storeCompiledReportTemplates();

		try {
			ReportingManager.storeJasperTemplateRefs(templateReferenceName, jasperTemplateName);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit storeJasperTemplateRefs");
	}

	/**
	 * Create csv file
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createCSVReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createCSVReport(templateName, parameters, resultSet, null, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createCSVReport");
		return byteStream;
	}

	/**
	 * Create csv file from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException {
		log.debug("Inside createCSVReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createCSVReport(templateName, parameters, null, colData, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createCSVReport");
		return byteStream;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD#createAndExportBarChart2D(com.isa.thinair
	 * .reportingframework.api.dto.BarChartDTO, javax.servlet.http.HttpServletResponse)
	 */
	public void createAndExportBarChart(BarChartDTO barChartDTO, HttpServletResponse response) throws ModuleException {
		try {
			Chart chart = ChartsManager.createChart(barChartDTO);
			ChartsManager.exportChartAsPNG(response, chart);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD#createAndExportLineChart2D(com.isa.thinair
	 * .reportingframework.api.dto.LineChartDTO, javax.servlet.http.HttpServletResponse)
	 */
	public void createAndExportLineChart(LineChartDTO lineChartDTO, HttpServletResponse response) throws ModuleException {
		try {
			Chart chart = ChartsManager.createChart(lineChartDTO);
			ChartsManager.exportChartAsPNG(response, chart);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD#createAndExportPieChart3D(com.isa.thinair
	 * .reportingframework.api.dto.PieChartDTO, javax.servlet.http.HttpServletResponse)
	 */
	public void createAndExportPieChart(PieChartDTO pieChartDTO, HttpServletResponse response) throws ModuleException {
		try {
			Chart chart = ChartsManager.createChart(pieChartDTO);
			ChartsManager.exportChartAsPNG(response, chart);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
	}

	@Override
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, ResultSet resultSet)
			throws ModuleException {
		try {
			return ReportingManager.createCSVReport(templateName, parameters, resultSet, null, null);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
	}

	@Override
	public void createCSVReportV2(Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException {
		try {
			ReportingManager.createCSVReportV2(parameters, resultSet, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
	}
	
	@Override
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, ResultSet resultSet,
			Map<Object, Object> imagesMap, String imagesURI) throws ModuleException {
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createHTMLReport(templateName, parameters, resultSet, imagesMap, imagesURI);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
		log.debug("Exit createXLSReport");
		return byteStream;
	}

	@Override
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, ResultSet resultSet)
			throws ModuleException {
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;
		try {
			byteStream = ReportingManager.createXLSReport(templateName, parameters, resultSet);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createXLSReport");
		return byteStream;
	}
	/**
	 * Creates an HTML Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colReportData
	 * @param imagesMap
	 * @param imagesURI
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, Collection<?> colData,Map<Object, Object> imagesMap, 
			String imagesURI) throws ModuleException{
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createHTMLReport(templateName, parameters, colData, imagesMap, imagesURI);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
		log.debug("Exit createXLSReport");
		return byteStream;
	}
	
	@Override
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, Collection<?>  colData)
			throws ModuleException {
		log.debug("Inside createXLSReport");
		byte[] byteStream = null;
		try {
			byteStream = ReportingManager.createXLSReport(templateName, parameters, colData);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createXLSReport");
		return byteStream;
	}

	@Override
	public byte[] createPDFReport(String templateName,
			Map<String, Object> parameters, Collection<?> colData)
			throws ModuleException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		try {
			byteStream = ReportingManager.createPDFReport(templateName, parameters, colData);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	@Override
	public byte[] createCSVReport(String templateName,
			Map<String, Object> parameters, Collection<?> colData)
			throws ModuleException {
		try {
			return ReportingManager.createCSVReport(templateName, parameters, null, colData, null);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}
	}


	public byte[] createReport(ReportParam.ReportType reportType, String templateName, Map<String, Object> parameters, Map<String, ResultSet> resultSets,
			ServletResponse servletResponse) throws ModuleException {

		log.debug("Inside createPDFReport");
		byte[] byteStream = null;


		try {
			byteStream = ReportingManager.createReport(reportType, templateName, parameters, resultSets, servletResponse);
		} catch (ReportingException e) {
			throw new ModuleException(e, "platform.module.lookup.failed", "reportingframework");
		}

		log.debug("Exit createPDFReport");
		return byteStream;
	}

	public byte[] createReport(ReportParam.ReportType reportType, String templateName, Map<String, Object> parameters, Map<String, ResultSet> resultSets) throws ModuleException {
		log.debug("Inside createPDFReport");
		byte[] byteStream = null;

		byteStream = createReport(reportType, templateName, parameters, resultSets, null);

		log.debug("Exit createPDFReport");
		return byteStream;
	}


}
