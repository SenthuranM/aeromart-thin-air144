/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.reportingframework.core.bl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;

/**
 * This class denotes abstract functionality for generating HTML, PDF and Excel reports.
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ExportManager {
	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(ExportManager.class);

	/**
	 * Performs HTML generation
	 * 
	 * @param writer
	 * @param jasperPrint
	 * @param imagesMap
	 * @param imagesURI
	 */
	public static byte[] htmlExport(ServletResponse servletResponse, JasperPrint jasperPrint, Map<Object, Object> imagesMap,
			String imagesURI) throws ReportingException {
		log.debug("Inside htmlExport");
		servletResponse.setContentType("text/html");
		byte[] byteStream = null;

		try {
			OutputStream outputStream = servletResponse.getOutputStream();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRHtmlExporter exporter = new JRHtmlExporter();

			exporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
			exporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, imagesURI);
			exporter.exportReport();

			byteStream = baos.toByteArray();

			outputStream.write(byteStream);
			outputStream.flush();
			outputStream.close();
		} catch (JRException e) {
			log.error("Error occured while creating Jasper HTML " + e.getMessage());
			ReportingException reportingException = new ReportingException("Jasper HTML generation failed...");
			throw reportingException;
		} catch (IOException e) {
			log.error("Error occured while creating file HTML " + e.getMessage());
			ReportingException reportingException = new ReportingException("File HTML generation failed...");
			throw reportingException;
		} catch (Exception e) {
			log.error("Error occured while creating file HTML " + e.getMessage());
		}

		log.debug("Exit htmlExport");
		return byteStream;
	}

	/**
	 * Performs PDF generation
	 * 
	 * @param writer
	 * @param jasperPrint
	 * @param outputStream
	 */
	public static byte[] pdfExport(ServletResponse servletResponse, JasperPrint jasperPrint) throws ReportingException {
		log.debug("Inside pdfExport");

		servletResponse.setContentType("application/pdf");
		byte[] byteStream = null;

		try {
			OutputStream outputStream = servletResponse.getOutputStream();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byteStream = baos.toByteArray();

			outputStream.write(byteStream);
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			log.error("Error occured while creating file PDF " + e.getMessage());
			e.printStackTrace();
		}

		log.debug("Exit pdfExport");
		return byteStream;
	}

	/**
	 * Performs PDF generation
	 * 
	 * @param jasperPrint
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] pdfExport(JasperPrint jasperPrint) throws ReportingException {
		log.debug("Inside pdfExport");

		byte[] byteStream = null;

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byteStream = baos.toByteArray();
		} catch (JRException e) {
			log.error("Error occured while creating Jasper PDF " + e.getMessage());
			ReportingException reportingException = new ReportingException("Jasper PDF generation failed...");
			throw reportingException;
		}

		log.debug("Exit pdfExport");
		return byteStream;
	}

	/**
	 * Performs Excel generation
	 * 
	 * @param writer
	 * @param jasperPrint
	 * @param outputStream
	 */
	public static byte[] excelExport(ServletResponse servletResponse, JasperPrint jasperPrint) throws ReportingException {

		log.debug("Inside excelExport");
		servletResponse.setContentType("application/vnd.ms-excel");
		byte[] byteStream;

		try {
			OutputStream outputStream = servletResponse.getOutputStream();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JExcelApiExporter exporter = new JExcelApiExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);

			// TODO fix : pass the sheet name as an argument
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);

//			exporter.setParameter(JRXlsExporterParameter.SHEET_NAMES, new String[] { "ISA" });
			exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 10000);
			exporter.exportReport();

			byteStream = baos.toByteArray();

			outputStream.write(byteStream);
			outputStream.flush();
			outputStream.close();
		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel ", e);
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		} catch (IOException e) {
			log.error("Error occured while creating file Excel ", e);
			ReportingException reportingException = new ReportingException("File Excel generation failed...");
			throw reportingException;
		} catch (Exception e) {
			log.error("Error occured while creating file Excel ", e);
			e.printStackTrace();
			ReportingException reportingException = new ReportingException("File Excel generation failed...");
			throw reportingException;
		}

		log.debug("Exit excelExport");
		return byteStream;
	}

	/**
	 * Performs CSV generation
	 * 
	 * @param servletResponse
	 * @param jasperPrint
	 * @throws ReportingException
	 */
	public static byte[] csvExport(ServletResponse servletResponse, JasperPrint jasperPrint) throws ReportingException {
		log.debug("Inside csvExport");
		servletResponse.setContentType("application/vnd.ms-excel");
		byte[] byteStream;

		try {
			OutputStream outputStream = servletResponse.getOutputStream();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRCsvExporter exporter = new JRCsvExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byteStream = baos.toByteArray();

			outputStream.write(byteStream);
			outputStream.flush();
			outputStream.close();
		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		} catch (IOException e) {
			log.error("Error occured while creating file Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("File Excel generation failed...");
			throw reportingException;
		}

		log.debug("Exit csvExport");
		return byteStream;
	}

	/**
	 * Performs CSV generation
	 * 
	 * @param servletResponse
	 * @param jasperPrint
	 * @throws ReportingException
	 */
	public static byte[] csvExport(ServletResponse servletResponse, JasperPrint jasperPrint, String reportNumDelimiter)
			throws ReportingException {
		log.debug("Inside csvExport");
		servletResponse.setContentType("application/vnd.ms-excel");
		byte[] byteStream;

		try {
			OutputStream outputStream = servletResponse.getOutputStream();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRCsvExporter exporter = new JRCsvExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, reportNumDelimiter);

			exporter.exportReport();

			byteStream = baos.toByteArray();

			outputStream.write(byteStream);
			outputStream.flush();
			outputStream.close();
		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		} catch (IOException e) {
			log.error("Error occured while creating file Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("File Excel generation failed...");
			throw reportingException;
		}

		log.debug("Exit csvExport");
		return byteStream;
	}

	/**
	 * Retrive a ByteArray of the CSV Report
	 * 
	 * @param jasperPrint
	 * @param reportNumDelimiter
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] csvExport(JasperPrint jasperPrint, String reportNumDelimiter) throws ReportingException {
		log.debug("Inside csvExport");
		byte[] byteStream;

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRCsvExporter exporter = new JRCsvExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, reportNumDelimiter);

			exporter.exportReport();

			byteStream = baos.toByteArray();

		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		}
		log.debug("Exit csvExport");
		return byteStream;
	}

	public static byte[] csvExport(JasperPrint jasperPrint) throws ReportingException {
		log.debug("Inside csvExport");
		byte[] byteStream;

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRCsvExporter exporter = new JRCsvExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byteStream = baos.toByteArray();

		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		}
		log.debug("Exit csvExport");
		return byteStream;
	}

	public static void csvExport(Map<String, Object> parameters, ResultSet resultSet, String reportNumDelimiter,
			ServletResponse servletResponse) throws ReportingException {
		log.debug("Inside csvExport");
		servletResponse.setContentType("application/vnd.ms-excel");

		try {
			reportNumDelimiter = PlatformUtiltiies.nullHandler(reportNumDelimiter);
			reportNumDelimiter = reportNumDelimiter.isEmpty() ? "," : reportNumDelimiter;

			PrintWriter writer = servletResponse.getWriter();
			composeResultSet(reportNumDelimiter, parameters, resultSet, writer);
			writer.flush();
			writer.close();
		} catch (SQLException e) {
			log.error("Error occured while SQL generating for the pure CSV file " + e.getMessage());
			throw new ReportingException("Pure CSV SQL generation failed...");
		} catch (IOException e) {
			log.error("Error occured while creating file pure CSV " + e.getMessage());
			throw new ReportingException("Pure CSV File generation failed...");
		}

		log.debug("Exit csvExport");
	}

	/**
	 * Compose the result set
	 * 
	 * @param rs
	 * @param writer
	 * @throws SQLException
	 */
	private static void composeResultSet(String seperator, Map<String, Object> parameters, ResultSet rs, PrintWriter writer)
			throws SQLException {

		if (rs == null) {
			writer.println();
		} else {
			if (parameters != null && parameters.size() > 0) {
				writer.println();
				Map<Object, Object> sortedElements = new TreeMap<Object, Object>(parameters);
				for (Entry<Object, Object> entry : sortedElements.entrySet()) {
					writer.println(PlatformUtiltiies.nullHandler(entry.getKey()).toUpperCase() + " : "
							+ PlatformUtiltiies.nullHandler(entry.getValue()).toUpperCase());
				}
				writer.println();
			}

			// Get result set meta data
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			Map<Integer, String> indexAndColmnNames = new HashMap<Integer, String>();
			Map<Integer, Object> indexAndColmnTypes = new HashMap<Integer, Object>();

			// Get the column names; column indices start from 1
			for (int i = 1; i < numColumns + 1; i++) {
				indexAndColmnNames.put(new Integer(i), rsmd.getColumnName(i));
				indexAndColmnTypes.put(new Integer(i), new Integer(rsmd.getColumnType(i)));
			}

			Integer index;
			String columnName;
			for (Iterator<Integer> itindexAndColmnNames = indexAndColmnNames.keySet().iterator(); itindexAndColmnNames.hasNext();) {
				index = (Integer) itindexAndColmnNames.next();
				columnName = (String) indexAndColmnNames.get(index);
				writer.print(columnName.toLowerCase() + seperator);
			}
			writer.println();

			int i;
			Object value;
			Integer columnType;
			while (rs.next()) {
				i = 1;
				while (i <= numColumns) {
					columnType = (Integer) indexAndColmnTypes.get(new Integer(i));

					if (columnType != null
							&& ((columnType.intValue() == Types.DATE) || (columnType.intValue() == Types.TIMESTAMP))) {
						value = rs.getTimestamp(i);
					} else {
						value = rs.getObject(i);
					}

					writer.print(value + seperator);
					i++;
				}
				writer.println();
			}
		}
	}

	/**
	 * Retrive a Byte Array of the HTML Report
	 * 
	 * @param jasperPrint
	 * @param imagesMap
	 * @param imagesURI
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] htmlExport(JasperPrint jasperPrint, Map<Object, Object> imagesMap, String imagesURI)
			throws ReportingException {
		log.debug("Inside htmlExport");
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRHtmlExporter exporter = new JRHtmlExporter();

			exporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, baos);
			exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
			exporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, imagesURI);
			exporter.exportReport();

			return baos.toByteArray();

		} catch (JRException e) {
			log.error("Error occured while creating Jasper HTML " + e.getMessage());
			ReportingException reportingException = new ReportingException("Jasper HTML generation failed...");
			throw reportingException;
		} finally {
			log.debug("Exit htmlExport");
		}
	}

	/**
	 * Retrive a Byte Array of the Excel Export
	 * 
	 * @param jasperPrint
	 * @return
	 * @throws ReportingException
	 */
	public static byte[] excelExport(JasperPrint jasperPrint) throws ReportingException {

		log.debug("Inside excelExport");
		byte[] byteStream;

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JExcelApiExporter exporter = new JExcelApiExporter();

			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			byteStream = baos.toByteArray();

		} catch (JRException e) {
			log.error("Error occured while creating Jasper Excel " + e.getMessage());
			ReportingException reportingException = new ReportingException("Excel generation failed...");
			throw reportingException;
		}
		log.debug("Exit excelExport");
		return byteStream;
	}
}
