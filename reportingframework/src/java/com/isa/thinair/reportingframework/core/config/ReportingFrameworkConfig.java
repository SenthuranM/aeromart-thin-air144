/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.reportingframework.core.config;

import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.config-bean
 */
public class ReportingFrameworkConfig extends DefaultModuleConfig {

	/**
	 * Holds the report reference names and report template names
	 */
	private Properties reportParameters;

	/** The constructure. */
	public ReportingFrameworkConfig() {
		super();
	}

	/**
	 * @return Returns the reportParameters.
	 */
	public Properties getReportParameters() {
		return reportParameters;
	}

	/**
	 * @param reportParameters
	 *            The reportParameters to set.
	 */
	public void setReportParameters(Properties reportParameters) {
		this.reportParameters = reportParameters;
	}

}
