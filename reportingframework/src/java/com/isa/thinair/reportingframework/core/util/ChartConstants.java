package com.isa.thinair.reportingframework.core.util;

/**
 * Chart Constants
 * 
 * @author Navod Ediriweera
 * @since Jul 08, 2009
 */
public class ChartConstants {

	/**
	 * Chart Types of specific charts. Used when creating the DTOs
	 */
	public static interface CHART_TYPES {
		public static String PIE_CHART_3D = "PIE_CHART_3D";
		public static String PIE_CHART_2D = "PIE_CHART_2D";

		public static String BAR_CHART_3D = "BAR_CHART_3D";
		public static String BAR_CHART_2D = "BAR_CHART_2D";

		public static String LINE_CHART_2D = "LINE_CHART_2D";
		public static String LINE_CHART_3D = "LINE_CHART_3D";

	}

	/**
	 * Available SQL result set variations
	 */
	public static interface CHART_DATA_SET_TYPES {
		public static String THREE_COLUMN_DATA = "THREE_COLUMN_DATA";
		public static String N_COLUMN_DATA = "N_COLUMN_DATA";
		public static String TWO_COLUMN_DATA = "TWO_COLUMN_DATA";
	}

	/**
	 * Suuper Chart type of Categoy Charts.Auto-set internally when creating a chart.
	 */
	public static interface MAIN_CHART_TYPES {
		public static String CATEGORY_CHART = "CATEGORY_CHART";
		public static String PIE_CHART = "PIE_CHART";

	}
}
