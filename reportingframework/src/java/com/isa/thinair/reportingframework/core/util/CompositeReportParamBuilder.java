package com.isa.thinair.reportingframework.core.util;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CompositeReportParamBuilder {

	public static JRDataSource buildDataSource(Map<String, DataSource> dataSourcesMap) {

		List<Map<String, ?>> dataSourcesList = new ArrayList<>();
		dataSourcesList.add(dataSourcesMap);
		JRDataSource dataSource = new JRMapCollectionDataSource(dataSourcesList);

		return dataSource;
	}


}
