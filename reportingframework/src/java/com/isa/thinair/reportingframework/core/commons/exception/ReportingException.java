/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.reportingframework.core.commons.exception;

/**
 * The main reporting exception
 * <p>
 * This is mainly a wrapper for the reporting module
 * </p>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReportingException extends Exception {
	/**
	 * Creates ReportingException
	 * 
	 * @param message
	 *            actual exception description
	 * @param cause
	 *            throwable object
	 */
	public ReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates ReportingException
	 * 
	 * @param message
	 *            actual exception description
	 */
	public ReportingException(String message) {
		super(message);
	}

	/**
	 * Creates ReportingException
	 * 
	 * @param cause
	 *            throwable object
	 */
	public ReportingException(Throwable cause) {
		super(cause);
	}

}