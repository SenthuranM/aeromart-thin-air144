package com.isa.thinair.reportingframework.core.bl.charts;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.sql.Types;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import com.isa.thinair.reportingframework.core.commons.exception.ReportingException;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_DATA_SET_TYPES;

/**
 * Converting ResultSet Data to JFreeChart Data Types
 * 
 * @author Navod Ediriweera
 * @since Jul 10, 2009
 */
public class ChartDataParser {

	/**
	 * ResultSet values have to be send in two columns.<br>
	 * Column 1 - Pie Piece Name (String/TimeStamp) - Key - Comparable<br>
	 * Column 2 - Pie Piece Vaule (Double/Number/BiggDecimal/Integer) - Value - Double<br>
	 * See Example http://www.java2s.com/Code/Java/Chart/JFreeChartPieChartDemo1.htm
	 * 
	 * @param rs
	 * @return
	 */
	public static DefaultPieDataset createPieChartDataSet(ResultSet rs) throws ReportingException {
		try {
			DefaultPieDataset pieDataSet = new DefaultPieDataset();
			ResultSetMetaData rmsd = rs.getMetaData();
			int colCount = rmsd.getColumnCount();
			int colType = 0;
			while (rs.next()) {
				String key = null;
				Double value = null;
				int i = 1;
				Timestamp timestamp = null;
				while (i < colCount + 1) {
					colType = rmsd.getColumnType(i);
					if (colType == Types.INTEGER) {
						value = Integer.valueOf(rs.getInt(i)).doubleValue();
					} else if (colType == Types.NUMERIC) {
						value = Integer.valueOf(rs.getInt(i)).doubleValue();
					} else if (colType == Types.DOUBLE) {
						value = rs.getDouble(i);
					} else {
						if (colType == Types.TIMESTAMP) {
							timestamp = rs.getTimestamp(i);
						} else {
							key = rs.getObject(i).toString();
						}
					}
					i++;
				}
				if (key == null || value == null) {
					throw new ReportingException("Invalid SQL Data: Null");
				}
				pieDataSet.setValue((timestamp == null ? key.trim() : timestamp), value.doubleValue());
			}
			return pieDataSet;

		} catch (Exception e) {
			throw new ReportingException("Error Accessing Chart SQL Data", e.getCause());
		}

	}

	/**
	 * Converts ResultSet to BarChart/Line Chart data<br>
	 * ResultSet should be as followes<br>
	 * 1st Column - Value (Double/Number/BigDecimal)<br>
	 * 2nd Column - Series (String/TimeStamp)<br>
	 * 3rd Column - Catagory(String/TimeStamp)<br>
	 * <br>
	 * Corrosponding to {@link CHART_DATA_SET_TYPES.THREE_COLUMN_DATA}
	 * 
	 * @param rs
	 * @return
	 * @throws ReportingException
	 */
	public static DefaultCategoryDataset createThreeColumnDCDataSet(ResultSet rs) throws ReportingException {
		DefaultCategoryDataset categoryDataset = new DefaultCategoryDataset();
		try {
			ResultSetMetaData rmsd = rs.getMetaData();
			int colCount = rmsd.getColumnCount();
			while (rs.next()) {
				Double value = null;
				String series = null;
				Timestamp timestampSeries = null;
				String category = null;
				Timestamp timestampCategory = null;
				String strTemp = null;
				Timestamp timestampTemp = null;
				int i = 1;
				int colType = 0;
				while (i < colCount + 1) {
					colType = rmsd.getColumnType(i);
					if (i == 1) {// First column is the value
						if (colType == Types.INTEGER) {
							value = Integer.valueOf(rs.getInt(i)).doubleValue();
						} else if (colType == Types.NUMERIC) {
							value = Integer.valueOf(rs.getInt(i)).doubleValue();
						} else if (colType == Types.DOUBLE) {
							value = rs.getDouble(i);
						}
					} else {
						if (colType == Types.TIMESTAMP) {
							timestampTemp = rs.getTimestamp(i);
						} else {
							strTemp = rs.getObject(i).toString();
						}
					}
					if (i == 2) {// Column 2
						series = strTemp;
						timestampSeries = timestampTemp;
					} else if (i == 3) {// Column 3
						category = strTemp;
						timestampCategory = timestampTemp;
					}
					i++;
				}
				if (value == null || (category == null && timestampCategory == null)
						|| (timestampSeries == null && series == null)) {
					throw new ReportingException("Error processing Chart SQL Data");
				}
				categoryDataset.addValue(value, timestampSeries == null ? series.trim() : timestampSeries,
						(timestampCategory == null ? category.trim() : timestampCategory));
			}
		} catch (Exception e) {
			throw new ReportingException("Error Accessing Chart SQL Data", e.getCause());
		}
		return categoryDataset;
	}

	/**
	 * Creates a CategoyDataSet for Bar and Line Charts
	 * 
	 * @param rs
	 *            SQL ResultSet<br>
	 *            1st Column - Category<br>
	 *            2nd Column - 1st Series<br>
	 *            3rd Colmun - 2nd Series<br>
	 *            ... ...<br>
	 *            nth Colunm - n-1 Series<br>
	 *            Series Names should be named in the SQL "AS" clause<br>
	 * <br>
	 *            Corrosponding to {@link CHART_DATA_SET_TYPES.N_COLUMN_DATA}
	 * @return
	 * @throws ReportingException
	 */
	public static DefaultCategoryDataset createDCDataSetFrom_N_Columns(ResultSet rs) throws ReportingException {

		DefaultCategoryDataset defaultcategorydataset = new DefaultCategoryDataset();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int colCount = rsmd.getColumnCount();
			while (rs.next()) {
				int colTypeCat = rsmd.getColumnType(1);
				String category = null;
				Timestamp catTime = null;
				Double value = null;
				// First Column is the Category
				if (colTypeCat == Types.TIMESTAMP) {
					catTime = rs.getTimestamp(1);
				} else {
					category = rs.getObject(1).toString();
				}
				for (int j = 2; j < colCount + 1; j++) {// start from 2. First col is the Category
					int colTypeSeries = rsmd.getColumnType(j);
					if (colTypeSeries == Types.INTEGER) {
						value = Integer.valueOf(rs.getInt(j)).doubleValue();
					} else if (colTypeSeries == Types.NUMERIC) {
						value = Integer.valueOf(rs.getInt(j)).doubleValue();
					} else if (colTypeSeries == Types.DOUBLE) {
						value = rs.getDouble(j);
					}
					if (value == null || (catTime == null && category == null)) {
						throw new ReportingException("Error Accessing Chart SQL Data");
					}
					defaultcategorydataset.addValue(value, rsmd.getColumnLabel(j), (category == null ? catTime : category));
				}
			}
			return defaultcategorydataset;
		} catch (Exception e) {
			throw new ReportingException("Error Accessing Chart SQL Data", e.getCause());
		}
	}
}
