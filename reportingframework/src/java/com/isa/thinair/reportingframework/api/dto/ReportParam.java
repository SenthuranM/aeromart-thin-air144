package com.isa.thinair.reportingframework.api.dto;

public interface ReportParam {

	enum ReportType {
		HTML, PDF, XSL, CSV
	}

}
