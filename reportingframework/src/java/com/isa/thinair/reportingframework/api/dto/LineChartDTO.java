package com.isa.thinair.reportingframework.api.dto;

import com.isa.thinair.reportingframework.core.util.ChartConstants;

/**
 * 
 * @author Navod Ediriweera
 * @since Jul 10, 2009
 */
public class LineChartDTO extends CategoryChartCommonDTO {

	private static final long serialVersionUID = -2327221374536532006L;

	/**
	 * 
	 * @param chartType
	 *            The name of the char. Corrosponding to the value in {@link ChartConstants.CHART_TYPES}
	 */
	public LineChartDTO(String chartType) {
		super(chartType);
	}

}
