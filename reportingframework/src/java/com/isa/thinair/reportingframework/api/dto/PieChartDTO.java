/**
 * 
 */
package com.isa.thinair.reportingframework.api.dto;

import java.awt.Color;

import com.isa.thinair.reportingframework.core.util.ChartConstants;
import com.isa.thinair.reportingframework.core.util.ChartConstants.MAIN_CHART_TYPES;

/**
 * Set the infomration you want to set. defualt behaviour otherwise. DataSet cannot be null
 * 
 * @author Navod Ediriweera
 * @since Jul 08, 2009
 */
public class PieChartDTO extends ChartCommonsDTO {

	private static final long serialVersionUID = -4766480629454684304L;
	protected int startAngle;
	protected double interiorGap; // orientation (<0.41)
	protected Color baseSectionOutLineColor;
	protected Boolean isSectionOutLine;

	/**
	 * @param chartType
	 *            The name of the char. Corrosponding to the value in {@link ChartConstants.CHART_TYPES}
	 */
	public PieChartDTO(String chartType) {// Default Behaviour
		super(chartType);

		this.startAngle = 270;
		this.interiorGap = 0.05;
		this.baseSectionOutLineColor = Color.BLACK;
		this.isSectionOutLine = true;
		this.chartMainType = MAIN_CHART_TYPES.PIE_CHART;
	}

	/**
	 * @return the baseSectionOutLineColor
	 */
	public Color getBaseSectionOutLineColor() {
		return baseSectionOutLineColor;
	}

	/**
	 * @param baseSectionOutLineColor
	 *            the baseSectionOutLineColor to set
	 */
	public void setBaseSectionOutLineColor(Color baseSectionOutLineColor) {
		this.baseSectionOutLineColor = baseSectionOutLineColor;
	}

	/**
	 * @return the isSectionOutLine
	 */
	public Boolean getIsSectionOutLine() {
		return isSectionOutLine;
	}

	/**
	 * @param isSectionOutLine
	 *            the isSectionOutLine to set
	 */
	public void setIsSectionOutLine(Boolean isSectionOutLine) {
		this.isSectionOutLine = isSectionOutLine;
	}

	public int getStartAngle() {
		return startAngle;
	}

	public void setStartAngle(int startAngle) {
		this.startAngle = startAngle;
	}

	public double getInteriorGap() {
		return interiorGap;
	}

	public void setInteriorGap(double interiorGap) {
		this.interiorGap = interiorGap;
	}

}
