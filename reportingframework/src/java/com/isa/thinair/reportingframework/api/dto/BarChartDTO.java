/**
 * 
 */
package com.isa.thinair.reportingframework.api.dto;

import com.isa.thinair.reportingframework.core.util.ChartConstants;

/**
 * Set the infomration you want to set. defualt behaviour otherwise. DataSet cannot be null
 * 
 * @author Navod Ediriweera
 * @since Jul 08, 2009
 */
public class BarChartDTO extends CategoryChartCommonDTO {

	private static final long serialVersionUID = -7311756736088638293L;

	/**
	 * 
	 * @param chartType
	 *            The name of the char. Corrosponding to the value in {@link ChartConstants.CHART_TYPES}
	 */
	public BarChartDTO(String chartType) {
		super(chartType);
	}
}
