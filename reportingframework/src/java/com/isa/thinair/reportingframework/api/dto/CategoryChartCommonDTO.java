package com.isa.thinair.reportingframework.api.dto;

import org.jfree.chart.renderer.category.AbstractCategoryItemRenderer;

import com.isa.thinair.reportingframework.core.util.ChartConstants.MAIN_CHART_TYPES;

/**
 * @author Navod Ediriweera
 * @since Jul 10, 2009
 */
public abstract class CategoryChartCommonDTO extends ChartCommonsDTO {

	private static final long serialVersionUID = 202811221453926110L;
	protected String xAxisName;
	protected String yAxisName;
	/**
	 * Rendere defines custom behaviour. <br>
	 * Category Chart Renderes are implementations of the {@link AbstractCategoryItemRenderer} <br>
	 * Set Chart Specific Rendere from the Front end when display needs to be customized.
	 */
	protected AbstractCategoryItemRenderer rendere;
	/**
	 * Shows the decimal point in the Range (Y) axis<br>
	 * Defualt - false.
	 */
	protected boolean showDecimalsYAxis;

	/**
	 * @param chartType
	 */
	public CategoryChartCommonDTO(String chartType) {
		super(chartType);
		this.showDecimalsYAxis = false;
		this.chartMainType = MAIN_CHART_TYPES.CATEGORY_CHART;
	}

	/**
	 * @return the showDecimalsYAxis
	 */
	public boolean isShowDecimalsYAxis() {
		return showDecimalsYAxis;
	}

	/**
	 * @param showDecimalsYAxis
	 *            the showDecimalsYAxis to set
	 */
	public void setShowDecimalsYAxis(boolean showDecimalsYAxis) {
		this.showDecimalsYAxis = showDecimalsYAxis;
	}

	/**
	 * @return the rendere
	 */
	public AbstractCategoryItemRenderer getRendere() {
		return rendere;
	}

	/**
	 * @param rendere
	 *            the rendere to set
	 */
	public void setRendere(AbstractCategoryItemRenderer rendere) {
		this.rendere = rendere;
	}

	/**
	 * @return the xAxisName
	 */
	public String getXAxisName() {
		return xAxisName;
	}

	/**
	 * @param axisName
	 *            the xAxisName to set
	 */
	public void setXAxisName(String axisName) {
		xAxisName = axisName;
	}

	/**
	 * @return the yAxisName
	 */
	public String getYAxisName() {
		return yAxisName;
	}

	/**
	 * @param axisName
	 *            the yAxisName to set
	 */
	public void setYAxisName(String axisName) {
		yAxisName = axisName;
	}
}
