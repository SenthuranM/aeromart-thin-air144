/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */

package com.isa.thinair.reportingframework.api.service;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reportingframework.api.dto.BarChartDTO;
import com.isa.thinair.reportingframework.api.dto.LineChartDTO;
import com.isa.thinair.reportingframework.api.dto.PieChartDTO;
import com.isa.thinair.reportingframework.api.dto.ReportParam;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_DATA_SET_TYPES;

/**
 * Interface to define all the methods required to access the reporting framework
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.bd-intf id="reportingframework.service"
 */
public interface ReportingFrameworkBD {
	/**
	 * Creates HTML Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param imagesMap
	 * @param imagesURI
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, Map<Object, Object> imagesMap, String imagesURI,
			ServletResponse servletResponse) throws ModuleException;

	/**
	 * Creates an HTML Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param imagesMap
	 * @param imagesURI
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, ResultSet resultSet,
			Map<Object, Object> imagesMap, String imagesURI) throws ModuleException;

	/**
	 * Creates HTML Report from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param imagesMap
	 * @param imagesURI
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, Collection<?> colData, Map<Object, Object> imagesMap, String imagesURI,
			ServletResponse servletResponse) throws ModuleException;

	/**
	 * Creates an HTML Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param imagesMap
	 * @param imagesURI
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createHTMLReport(String templateName, Map<String, Object> parameters, Collection<?> colData,Map<Object, Object> imagesMap, 
			String imagesURI) throws ModuleException;

	/**
	 * Creates PDF Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException;

	/**
	 * Creates a PDF Report from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException;

	/**
	 * Called from BL class in INVOICE BL.
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, ResultSet resultSet) throws ModuleException;

	/**
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param colData
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createPDFReport(String templateName, Map<String, Object> parameters, Collection<?>  colData) throws ModuleException;

	/**
	 * Creates Excel Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException;

	/**
	 * Creates a Excel Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, ResultSet resultSet)
			throws ModuleException;

	/**
	 * Creates Excel Report from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException;
	
	/**
	 * Creates a Excel Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createXLSReport(String templateName, Map<String, Object> parameters, Collection<?> colData)
			throws ModuleException;

	/**
	 * Create CSV Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse)
			throws ModuleException;

	/**
	 * Creates a CSV Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param resultSet
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, ResultSet resultSet)
			throws ModuleException;

	/**
	 * Create CSV Report from a collection of DTOs
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @param servletResponse
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, Collection<?> colData, ServletResponse servletResponse)
			throws ModuleException;
	
	/**
	 * Creates a CSV Report
	 * 
	 * @param templateName
	 * @param parameters
	 * @param colData
	 * @return
	 * @throws ModuleException
	 */
	public byte[] createCSVReport(String templateName, Map<String, Object> parameters, Collection<?> colData)
			throws ModuleException;

	/**
	 * Create CSV Report with out using Jasper engine. Implentation of this API was motivated due to JASPER CSV
	 * processing are highly error prone with regards to the alignments.
	 * 
	 * @param parameters
	 * @param resultSet
	 * @param servletResponse
	 * @throws ModuleException
	 */
	public void createCSVReportV2(Map<String, Object> parameters, ResultSet resultSet, ServletResponse servletResponse) throws ModuleException;

	/**
	 * Store Jasper template references
	 * 
	 * @param templateReferenceName
	 * @param jasperTemplateName
	 * @throws ModuleException
	 */
	public void storeJasperTemplateRefs(String templateReferenceName, String jasperTemplateName) throws ModuleException;

	/****************************************** Charts ******************************************/
	/**
	 * Creates a pie chart
	 * 
	 * @author Navod Ediriweera
	 * @param pieChartDTO
	 *            SQL ResultSet <br>
	 *            1st Column - Category <br>
	 *            2nd Column - 1st Series <br>
	 *            3rd Colmun - 2nd Series <br>
	 *            ... ... <br>
	 *            nth Colunm - n-1 Series <br>
	 *            Series Names should be named in the SQL "AS" clause <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.THREE_COLUMN_DATA} when setting the SQL ResultSet <br>
	 * <br>
	 *            OR <br>
	 *            1st Column - Value (Double/Number/BigDecimal) <br>
	 *            2nd Column - Series (String/TimeStamp) <br>
	 *            3rd Column - Catagory(String/TimeStamp) <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.TWO_COLUMN_DATA} when setting the SQL ResultSet
	 * @param response
	 * @return
	 * @throws ModuleException
	 */
	public void createAndExportPieChart(PieChartDTO pieChartDTO, HttpServletResponse response) throws ModuleException;

	/**
	 * Creates Bar chart
	 * 
	 * @author Navod Ediriweera
	 * @param barChartDTO
	 * <br>
	 *            SQL ResultSet <br>
	 *            1st Column - Category <br>
	 *            2nd Column - 1st Series <br>
	 *            3rd Colmun - 2nd Series <br>
	 *            ... ... <br>
	 *            nth Colunm - n-1 Series <br>
	 *            Series Names should be named in the SQL "AS" clause <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.THREE_COLUMN_DATA} when setting the SQL ResultSet <br>
	 * <br>
	 *            OR <br>
	 *            1st Column - Value (Double/Number/BigDecimal) <br>
	 *            2nd Column - Series (String/TimeStamp) <br>
	 *            3rd Column - Catagory(String/TimeStamp) <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.TWO_COLUMN_DATA} when setting the SQL ResultSet
	 * @param response
	 * @return
	 * @throws ModuleException
	 */
	public void createAndExportBarChart(BarChartDTO barChartDTO, HttpServletResponse response) throws ModuleException;

	/**
	 * Creates a Line Chart
	 * 
	 * @author Navod Ediriweera
	 * @param lineChartDTO
	 *            SQL ResultSet <br>
	 *            1st Column - Category <br>
	 *            2nd Column - 1st Series <br>
	 *            3rd Colmun - 2nd Series <br>
	 *            ... ... <br>
	 *            nth Colunm - n-1 Series <br>
	 *            Series Names should be named in the SQL "AS" clause <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.THREE_COLUMN_DATA} when setting the SQL ResultSet <br>
	 * <br>
	 *            OR <br>
	 *            1st Column - Value (Double/Number/BigDecimal) <br>
	 *            2nd Column - Series (String/TimeStamp) <br>
	 *            3rd Column - Catagory(String/TimeStamp) <br>
	 *            WITH {@link CHART_DATA_SET_TYPES.TWO_COLUMN_DATA} when setting the SQL ResultSet
	 * @param response
	 * @return
	 * @throws ModuleException
	 */
	public void createAndExportLineChart(LineChartDTO lineChartDTO, HttpServletResponse response) throws ModuleException;


	public byte[] createReport(ReportParam.ReportType reportType, String templateName, Map<String, Object> parameters, Map<String, ResultSet> resultSets,
			ServletResponse servletResponse) throws ModuleException;

	public byte[] createReport(ReportParam.ReportType reportType, String templateName, Map<String, Object> parameters, Map<String, ResultSet> resultSets)
			throws ModuleException;


}
