==============================================================================
ISA Software License, Version 1.0

Copyright (c) 2005 The Information Systems Associates. All rights reserved.

Redistribution and use of this code in source and binary forms, with or without
modification, is not permitted without prior approval from ISA.
 
Use is subjected to license terms. 
 
@version $Id $
=============================================================================== 

How to build the project
========================
Running dist-all target will generate the reportingframework-1.0.jar 
under repository.modules.api directory

Dependancies
============
This framework depends on the following jar files.

* commons-digester.jar
* itext-1.1.4.jar 
* jasperreports-1.0.2.jar
* jdt-compiler.jar
* poi-2.5.1-final-20040804.jar

Special Notes
=============
The user is advice to comiple the relavant jasper report from Ireport as a best practice.
The compiling jasper report version should match with the target jasper report version.

How to generate
===============
(1) HTML Reports  ---> Refer HtmlClient.java
(2) PDF Reports   ---> Refer PdfClient.java
(3) Excel Reports ---> Refer ExcelClient.java
(4) Framework Client ---> Refer FrameworkClient.java




