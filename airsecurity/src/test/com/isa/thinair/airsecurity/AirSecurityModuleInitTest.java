package com.isa.thinair.airsecurity;

import com.isa.thinair.platform.api.util.PlatformTestCase;

public class AirSecurityModuleInitTest extends PlatformTestCase {
	
	public void testModuleInitialization(){
		try {
			super.setUp();
		} catch ( Exception ex) {
			fail("Error occured in the module initialization, error = " + ex.getMessage());
		}
	}

}
