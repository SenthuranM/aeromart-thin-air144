/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestSecurityDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airsecurity.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import junit.framework.TestCase;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserDomain;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.core.persistence.dao.PrivilegeDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.RoleDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Thejaka
 *
 */
public class TestSecurityDAOImpl extends TestCase {

//	public TestSecurityDAOImpl() {
//		super();
//	}
//
//	public TestSecurityDAOImpl(String arg0) {
//		super(arg0);
//	}
//	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
//	public void testAll(){
////		checkPrivilegeDAO();
////		checkUserDAO();		
//	}
	
	public void testGettingUserPrivileges() throws ModuleException{
		IModule securityMod = AirSecurityUtil.getInstance();
		assertNotNull( "module was null" , securityMod );
		UserDAO dao = (UserDAO) securityMod.getLocalBean("UserDAOImplProxy");
		User user = dao.getUser("USER1");
		displayUserPrivs(user);
		displayUserPrivs(dao.getUser("USER2"));
		displayUserPrivs(dao.getUser("SYSTEM"));
		displayUserPrivs(dao.getUsers(35,5).getPageData());
	}
	
	private void displayUserPrivs(Collection users) throws ModuleException{
		for (Iterator iterUsers = users.iterator(); iterUsers.hasNext();) {
			User user = (User) iterUsers.next();
			displayUserPrivs(user);
		}		
	}
	
	private void displayUserPrivs(User user) throws ModuleException{
		Collection privs = user.getPrivitedgeIDs();
		System.out.println("Privilege ID List for user " + user.getUserId());
		for (Iterator iter = privs.iterator(); iter.hasNext();) {
			String privId = (String) iter.next();
			System.out.println("         " + privId);
		}
		System.out.println("-------------------");
	}
	
	
	
//	public void testTemp(){
//		System.out.println("Going to test Privilege DAO temp methods");
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );		
//		PrivilegeDAO privDAO = (PrivilegeDAO) securityMod.getLocalBean("PrivilegeDAOImplProxy");
//		
//		System.out.println("Printing all the temporary privileges.");
//		Iterator iterPriv = privDAO.getTempPrivileges().iterator();		
////		System.out.println("Temp class name : " + iterPriv.next().getClass().getName() );		
//		
//	    while (iterPriv.hasNext()){	    	
//	    	Collection priv = ((HashMap) iterPriv.next()).values();
//	    	System.out.println("Got hash map to a collection");
//	    	//.getPrivilegeId();
//	    	Iterator tmpIter =priv.iterator(); 
//	    	while (tmpIter.hasNext()){
//		    	System.out.println("Temporary Privilege Id: " + (String) tmpIter.next());	    	
//		    }	    	
//	    }
//	    System.out.println("Test finished for Privilege DAO temp methods");
//	}

	
	
//	public void testPrivilegeDAO(){
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		PrivilegeDAO dao = (PrivilegeDAO) securityMod.getLocalBean("PrivilegeDAOImplProxy");
//		assertEquals("Privilege description not equal",dao.getPrivilege("AIRCRAFT.UPD").getPrivilege(),"Edit any Aircraft");
//		assertEquals("Privilege'a privilege category is not equal",dao.getPrivilege("AIRPORT.DEL").getPrivilegeCategoryID(),"AIRPORT");
//		//Test privilege dependencies		
//		PrivilegeDependantsDTO privilegeDependantsDTO  = dao.getPrivilegeDependencies("MAKERES.PAYMNT");
//		assertTrue("privilege dependencies are not retrieved",privilegeDependantsDTO.getDependantPrevilegeIds().size()>0);
//		System.out.print("Privileges are retrieved -> Privilege : " + privilegeDependantsDTO.getPrivilegeId() + 
//				" a dipendent privilege : " + privilegeDependantsDTO.getDependantPrevilegeIds().iterator().next().toString());
//		//Test privilege categories
//		System.out.print("Going to retrieve privilege categories!");
//		Collection privCats = dao.getPrivilegeCategories();
//		assertTrue("No privilege categories are retrived.",privCats.size()>0);
//		System.out.print("One of the retrieved privilege category :" + ((PrivilegeCategory)privCats.iterator().next()).getPrivilegeCategoryDesc());
//		
//		//	Checking privilege category page
//		System.out.println("Checking privilege category pages in DAO...");
//		Page page = dao.getPrivilegeCategories(0,2);
//		assertTrue("Get privilege categories method dosn't work well", page.getPageData().size() > 1);
//		System.out.println("Retrieved page privilege categories : " +page.getPageData().size() );
//		System.out.println("One of the retrieved privilege category is " + ((PrivilegeCategory)page.getPageData().iterator().next()).getPrivilegeCategoryDesc() );
//			
//	}
//	
//	public void testUserSearch() throws ModuleException{
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		UserDAO dao = (UserDAO) securityMod.getLocalBean("UserDAOImplProxy");
//		
//		UserSearchDTO userSearchDTO = new UserSearchDTO("q","VVV","V%","0%"); 
//		
//		Page userPage = dao.searchUsers(userSearchDTO,1,5);
//		Collection userColl = userPage.getPageData();
//		for (Iterator iter = userColl.iterator(); iter.hasNext();) {
//			User user = (User) iter.next();
//			System.out.println("User " + user.getUserId());
//		}
//	}
//	
//	public void testUserSecurity() throws ModuleException{
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		UserDAO dao = (UserDAO) securityMod.getLocalBean("UserDAOImplProxy");
//		User user = dao.getUser("TEST_USER");
////		user.setPassword("passwordAB");
//		user.setPassword("password");
//		dao.saveUser(user);
//		
//		user = null;
//		dao = (UserDAO) securityMod.getLocalBean("UserDAOImplProxy");
//		User user1 = dao.getUser("TEST_USER");
////		user1.setLastName("YOOSUF");
//		user1.setPassword(null);
//		dao.saveUser(user1);
//		assertTrue("Password was not set ",dao.authenticate("TEST_USER","password")!=null);		
//	}

//	public void testUserDAO() throws ModuleException{
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		UserDAO dao = (UserDAO) securityMod.getLocalBean("UserDAOImplProxy");
//		RoleDAO roleDAO = (RoleDAO) securityMod.getLocalBean("RoleDAOImplProxy");
//		assertNotNull(dao);
//
//		//Test User
//		User obj1 = new User();
//		String key = "888";
//		System.out.print("Going to set User ID 888");
//		obj1.setUserId(key);
//		System.out.print("Going to set Email ID 888");
//		obj1.setEmailId("aa@aa.com");
//		System.out.print("Going to set First Name 888");
//		obj1.setDisplayName("Thejaka");
//		System.out.print("Going to set Last Name 888");
////		obj1.setLastName("Usgoda Arachchi");
//		System.out.print("Going to set password of 888");
//		obj1.setPassword("xx");
//		System.out.print("Going to set domain ID of 888");
////		obj1.setUserDomainID(1);
////		obj1.setStationCode("CMB");
//		obj1.setStatus(User.STATUS_ACTIVE);
//		System.out.print("Going to save User 888");
//		dao.saveUser(obj1);
//		System.out.print("User 888 saved");
//		
////		Update the user 
//		User objUser=dao.getUser(key);
//		objUser.setDisplayName("Amila");
////		objUser.setUserDomainID(1);
//		objUser.setStatus(User.STATUS_INVALID);
//		Set roleSet = new HashSet();
//		roleSet.add(roleDAO.getRole(1));
//		roleSet.add(roleDAO.getRole(222));
//		objUser.setRoles(roleSet);
//		System.out.print("Going to update User 888");
//		dao.saveUser(objUser);
//		System.out.print("User 888 updated");
//		
//		//assertNotSame(objP,dao.getPrivilege(keyP));
//		
//		//Check user
//		objUser=dao.getUser(key);
//		assertEquals("User's name is not correctly saved",objUser.getDisplayName(),"Amila");
//		System.out.print("User's name is correctly saved");
////		assertEquals("User's domain id is not correctly saved", objUser.getUserDomainID(),1);		
//		System.out.print("User's domain id is correctly saved");
//		Set priviledgeIDs= new HashSet();
//		Iterator iterRoles = roleSet.iterator();
//	    while (iterRoles.hasNext()){
//	    	Role role = (Role)iterRoles.next();
//	    	Iterator iterPrivileges = role.getPrivileges().iterator();
//		    while (iterPrivileges.hasNext()) {
//		    	priviledgeIDs.add(Integer.getInteger(String.valueOf(((Privilege) iterPrivileges.next()).getPrivilegeId())));
//		    }
//	    }
//	    assertEquals("User's privileges are not correctly saved",priviledgeIDs,objUser.getPrivitedgeIDs());
//	    System.out.print("User's privileges are correctly saved");
//		
//		//Remove User
//		dao.removeUser(key);
//		
//		System.out.println("Going to retrieved user 555");
//		User user = dao.getUser("555");
//		System.out.println("Retrieved user 555");
//		System.out.println("Retrieved user is " + user.getDisplayName());
//		
//		System.out.println("Going to retrieved user 222");
//		user = dao.getUser("222");
//		System.out.println("Retrieved user 222");
//		System.out.println("Retrieved user is " + user.getDisplayName());		
//		
//		System.out.println("Going to authonticate user 555");
//		user = dao.authenticate("555","xx");
//		if (user != null )
//			System.out.println("User " + user.getUserId() + "is authonticated");
//		
//		System.out.println("Going to check un-authorized access for user 555");
//		user = dao.authenticate("555","yy");
//		assertNull("User '555' with password 'yy' should not be authonticated.",user);
//		if (user == null )
//			System.out.println("Checking un-authorized access for user 555 is successful");
//		
//		//Search users		
//		System.out.println("Going to search users");
//		ModuleCriterion moduleCriterion = new ModuleCriterion();
//		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		moduleCriterion.setFieldName("userId");
//		List value = new ArrayList();
//		value.add("555");
//		moduleCriterion.setValue(value);
//		List critrian = new ArrayList();
//		critrian.add(moduleCriterion);	
//		
//		List usersList =  (List) dao.searchUsers(critrian);			
//		Iterator iter = usersList.iterator();
//	    while (iter.hasNext()){
//	    	String userId = ((User)iter.next()).getUserId();
//	    	System.out.println("Searched user Id " + userId);
//	    	assertEquals("Invalid User ID",userId,"555");
//	    }
//	    System.out.println("Searched user found!");
//	    
//	    //Search users for a page
//	    System.out.println("Going to search users for a page");
//		moduleCriterion = new ModuleCriterion();
//		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		moduleCriterion.setFieldName("status");
//		value = new ArrayList();
//		value.add(User.STATUS_ACTIVE);
//		moduleCriterion.setValue(value);
//		critrian = new ArrayList();
//		critrian.add(moduleCriterion);	
//
//
//		System.out.println("Called search users all users for critiria");
//		Collection usersCol =  dao.searchUsers(critrian);		
//		iter = usersCol.iterator();		
//	    while (iter.hasNext()){
//	    	String userId = ((User)iter.next()).getUserId();
//	    	System.out.println("Searched user Id " + userId);	    	
//	    }
//		
//		System.out.println("Called search users for a page");
//		Page usersPage =  dao.searchUsers(critrian,2,3);
//		assertEquals("Searched page data not found",usersPage.getPageData().size(),3);
//		System.out.println("Searched users page details listed below :");
//		iter = usersPage.getPageData().iterator();		
//	    while (iter.hasNext()){
//	    	String userId = ((User)iter.next()).getUserId();
//	    	System.out.println("Searched user Id " + userId);	    	
//	    }	    
//	    
//	    
//	    System.out.println("Get a user domain");
//	    UserDomain ud = dao.getUserDomain(1);
//	    assertEquals("User domain is not valid","Test Domain Entered by Thejaka",ud.getDomainName());
//	    System.out.println("User domain is valid");
//	    
//	    //	  Checking user page
//		System.out.println("Checking user pages in DAO...");
//		Page page = dao.getUsers(1,3);
//		assertTrue("Get users method dosn't work well", page.getPageData().size() > 1);
//		System.out.println("Retrieved page users : " +page.getPageData().size() );
//		System.out.println("One of the retrieved user is " + ((User)page.getPageData().iterator().next()).getDisplayName() );
//		
//		//Checking active users collection 
//		System.out.println("Checking active users collection");
//		Collection usersCollection = dao.getActiveUsers();
//		assertTrue("Get active users method dosn't work well", usersCollection.size() > 1);
//		System.out.println("Retrieved number of users : " +usersCollection.size() );
//		System.out.println("One of the retrieved user is " + ((User)usersCollection.iterator().next()).getDisplayName() );
//		
//		
//		user =  dao.getUser("TEST_USER");
//		System.out.println("Print rolls of user :" + user.getUserId());
//		
//		printRollsOfUser(user);
//		Collection roles = user.getRoles();
//		if (roles != null){			
//			Role role = (Role)roles.iterator().next();
//			System.out.println("Inactivating the Role :" + role.getRoleId());
//			role.setStatus(Role.STATUS_INACTIVE);
//			roleDAO.saveRole(role);
//			
//			user =  dao.getUser("TEST_USER");
//			System.out.println("Print rolls of user :" + user.getUserId() + " after inactive the role " + role.getRoleId() );			
//			printRollsOfUser(user);
//			
//			Set rolesSet= user.getRoles();
//			role = roleDAO.getRole(444);
//			role.setStatus(Role.STATUS_ACTIVE);
//			roleDAO.saveRole(role);
//			role = roleDAO.getRole(444);
//			rolesSet.add(role);
//			user.setRoles(rolesSet);
//			user.setEmailId("nasly@jkcs.slt.lk");
//			dao.saveUser(user);			
//			
////			System.out.println("Activating the Role :" + role.getRoleId());
////			role.setStatus(Role.STATUS_ACTIVE);
////			roleDAO.saveRole(role);			
//			
//		}
////		Check Search users order		
//		System.out.println("Going to search users for firstname contains T and display in order");
//		moduleCriterion = new ModuleCriterion();
//		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		moduleCriterion.setFieldName("firstName");
//		value = new ArrayList();
//		value.add("%T%");
//		moduleCriterion.setValue(value);
//		critrian = new ArrayList();
//		critrian.add(moduleCriterion);	
//		
//		usersList =  (List) dao.searchUsers(critrian);			
//		iter = usersList.iterator();
//	    while (iter.hasNext()){
//	    	String userId = ((User)iter.next()).getUserId();
//	    	System.out.println("Searched user " + userId + " " + ((User)iter.next()).getDisplayName());		    	
//	    }
//	    
//	    //Check load users order		
//		System.out.println("Going to load users and display in order by PK");		
//		
//		usersList =  (List) dao.getUsers();			
//		iter = usersList.iterator();
//	    while (iter.hasNext()){
//	    	objUser = (User)iter.next();
//	    	System.out.println("Loaded user " + objUser.getUserId() + " " + objUser.getDisplayName());		    	
//	    }
//	}
	
	private void printRollsOfUser(User user){		 
		if (user != null){
			Iterator iter = user.getRoles().iterator();		
		    while (iter.hasNext()){
		    	String roleId = ((Role)iter.next()).getRoleId();
		    	System.out.println("Granted role Id " + roleId);	    	
		    }			
		}
	}
	
//	public void testGetRoleCollection(){
//		IModule securityMod = AirSecurityUtil.getInstance();
//		RoleDAO roleDAO = (RoleDAO) securityMod.getLocalBean("RoleDAOImplProxy");
//		Set roles = new HashSet();
//		roles.add(new Integer(2023));
//		roles.add(new Integer(2024));
//		roles.add(new Integer(2027));
//		HashMap privsMAP = roleDAO.getPrivilegesForRoles(roles);
//		for (Iterator iter = privsMAP.keySet().iterator(); iter.hasNext();) {
//			Integer roleId = (Integer) iter.next();
//			System.out.println("Role Id : " + roleId.intValue() +">>>>>>>");
//			Collection privs = (Collection) privsMAP.get(roleId);
//			for (Iterator iterator = privs.iterator(); iterator.hasNext();) {
//				Privilege priv = (Privilege) iterator.next();
//				System.out.println("----Privi ID : " + priv.getPrivilegeId() );
//			}			
//		}
//		
//	}
	
//	public void testGetAnsPrivCollection(){
//		IModule securityMod = AirSecurityUtil.getInstance();
//		PrivilegeDAO privDAO = (PrivilegeDAO) securityMod.getLocalBean("PrivilegeDAOImplProxy");
//		Set privs = new HashSet();
//		privs.add("plan.fares");
//		privs.add("plan.flight");
//		privs.add("plan.flight.sch.seg.overlap");
//		HashMap privsMAP = privDAO.getPrivilegeDependencies((Collection)privs);
//		for (Iterator iter = privsMAP.keySet().iterator(); iter.hasNext();) {
//			String privId = (String) iter.next();
//			System.out.println("Priv Id : " + privId +">>>>>>>");
//			Collection ansPrivs = (Collection) privsMAP.get(privId);
//			for (Iterator iterator = ansPrivs.iterator(); iterator.hasNext();) {
//				String priv = (String) iterator.next();
//				System.out.println("----Ans Privi ID : " + priv );
//			}			
//		}
//		
//	}

//	public void testRoleDAO(){
//		System.out.println("Going to test Role DAO");
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		RoleDAO roleDAO = (RoleDAO) securityMod.getLocalBean("RoleDAOImplProxy");
//		PrivilegeDAO privDAO = (PrivilegeDAO) securityMod.getLocalBean("PrivilegeDAOImplProxy");
//		Role role = new Role();
//		role.setRoleId(1212);
//		role.setRoleName("xxxx");
//		role.setStatus(Role.STATUS_ACTIVE);
//		role.setRemarks("Test Role entered by Thejaka");
//		Set privileges = new HashSet();
//		
//	
//		privileges.add(privDAO.getPrivilege("AIRCRAFT.UPD"));
//		privileges.add(privDAO.getPrivilege("AIRPORT.DEL"));
//		role.setPrivileges(privileges);		
//		roleDAO.saveRole(role);
//		
//		//Check the saved role
//		role = roleDAO.getRole(1212);		
//		assertEquals(role.getRoleName(),"xxxx");
//		assertEquals(role.getStatus(),Role.STATUS_ACTIVE);
//		assertEquals(role.getRemarks(),"Test Role entered by Thejaka");
//			
//		System.out.println("Print roles privileges set >> ");
//		Iterator iterPriv = role.getPrivileges().iterator();
//		Iterator iterPrivLoc = privileges.iterator();		
//	    while (iterPriv.hasNext()){
//	    	String priv1 = ((Privilege)iterPrivLoc.next()).getPrivilegeId();
//	    	String priv2 = ((Privilege) iterPriv.next()).getPrivilegeId();
//	    	System.out.println("Privilege Ids Local: " + String.valueOf(priv1) + "  Saved: " + String.valueOf(priv2));
//	    	//assertEquals(priv1, priv2);
//	    }
//	    System.out.println("Roles privileges set is printed ");
//		//assertEquals(role.getPrivileges(),privileges);
//		
//	    //Update Role
//	    role.setRoleName("yyyy");
//	    role.setStatus(Role.STATUS_INACTIVE);
//	    roleDAO.saveRole(role);
//	    //Check the updated role
//		role = roleDAO.getRole(1212);
//		assertEquals("Role name is not updated",role.getRoleName(),"yyyy");
//		assertEquals("Role name is not updated",role.getStatus(),Role.STATUS_INACTIVE);
//				
//		System.out.println("Going to retrieved Role 1 " + role.getRoleName());
//		role = roleDAO.getRole(1);
//		System.out.println("Retrieved Role is " + role.getRoleName());
//		
//		//Tests privileges
//		System.out.println("Printing all the privileges.");
//		iterPriv = privDAO.getPrivileges().iterator();		
//	    while (iterPriv.hasNext()){	    	
//	    	String priv = ((Privilege) iterPriv.next()).getPrivilegeId();
//	    	System.out.println("Privilege Id: " + String.valueOf(priv));	    	
//	    }    
//	    
//		//Checking active roles collection 
//		System.out.println("Checking active roles collection");
//		Collection rolesCollection = roleDAO.getActiveRoles();
//		assertTrue("Get active roles method dosn't work well", rolesCollection.size() > 0);
//		System.out.println("Retrieved number of roles : " +rolesCollection.size() );
//		System.out.println("One of the retrieved role is " + ((Role)rolesCollection.iterator().next()).getRoleName() );	
//	    
//		//Search roles for a page
//	    System.out.println("Going to search roles for a page");
//	    ModuleCriterion moduleCriterion = new ModuleCriterion();
//		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		moduleCriterion.setFieldName("status");
//		List value = new ArrayList();
//		value.add(Role.STATUS_ACTIVE);
//		moduleCriterion.setValue(value);
//		List critrian = new ArrayList();
//		critrian.add(moduleCriterion);
//		
//		Page rolesPage =  roleDAO.searchRoles(critrian,0,5);
//		assertTrue("Searched page data not found",rolesPage.getPageData().size()>0);
//		System.out.println("Searched roles page details listed below :");
//		Iterator iter = rolesPage.getPageData().iterator();		
//	    while (iter.hasNext()){
//	    	int roleId = ((Role)iter.next()).getRoleId();
//	    	System.out.println("Searched role Id " + String.valueOf(roleId));	    	
//	    }		
//	    
//		//Remove the added records
//	    System.out.println("Remove the added records");
//		roleDAO.removeRole(1212);
//		System.out.println("Removed the added records");
//	}
//	
//	public void testSearchRole(){
////		Search roles		
//		IModule securityMod = AirSecurityUtil.getInstance();
//		assertNotNull( "module was null" , securityMod );
//		RoleDAO roleDAO = (RoleDAO) securityMod.getLocalBean("RoleDAOImplProxy");
//		
//		Role role = new Role();
//		role.setRoleId(1212);
//		role.setRoleName("xxxx");
//		role.setRemarks("Test Role entered by Thejaka");
//		
//		System.out.println("Going to search roles");
//		ModuleCriterion moduleCriterion = new ModuleCriterion();
//		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
//		moduleCriterion.setFieldName("roleName");
//		List value = new ArrayList();
//		//value.add(Integer.valueOf("121212"));
//		value.add("yyyy");
//		moduleCriterion.setValue(value);
//		List critrian = new ArrayList();
//		critrian.add(moduleCriterion);	
//		
//		List rolesList =  (List) roleDAO.searchRoles(critrian);			
//		Iterator iter = rolesList.iterator();
//	    while (iter.hasNext()){
//	    	int roleId = ((Role)iter.next()).getRoleId();
//	    	System.out.println("Searched role id " + roleId);
//	    	assertEquals("Invalid Role ID",roleId,1212);
//	    }
//	    System.out.println("Searched role found!");
//	    
//	    Collection roles = roleDAO.getRoles();
//	    if (roles==null)
//	    	System.out.println("Roles set is null");	    
//	    assertTrue("Roles set retrived is Null",roles!=null);
//	    //assertNull("Role set is not ritrieved ",roles);	    
//	    
//	    //Remove the added records
//		roleDAO.removeRole(1212);
//	}
	
	protected void tearDown(){	
	}

}

