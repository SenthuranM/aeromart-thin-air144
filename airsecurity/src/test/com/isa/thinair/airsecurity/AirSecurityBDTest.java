package com.isa.thinair.airsecurity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.PrivilegeDependantsDTO;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class AirSecurityBDTest extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testSecurityBD() throws Exception {
		IModule securityMod = AirSecurityUtil.getInstance();
		assertNotNull("module was null", securityMod);
		SecurityBD securityBD = (SecurityBD) securityMod.getServiceBD("security.service.remote");
		assertNotNull("returned BD was null", securityBD);
		Privilege privilege = securityBD.getPrivilege("AIRCRAFT.UPD");
		assertNotNull("privilege is null",privilege);
		System.out.println("Privilege name of AIRCRAFT.UPD is" + privilege.getPrivilege());
		
		User user = securityBD.getUser("222");
		System.out.println("Retrieved user is " + user.getDisplayName());
		Role role = securityBD.getRole(1);
		System.out.println("Retrieved Role is " + role.getRoleName());
		user = securityBD.getUser("555");
		System.out.println("Retrieved user 555's name is " + user.getDisplayName());
		System.out.println("Retrieved user 555's password is " + user.getPassword());
		user = securityBD.authenticate("555","xx");
		assertNotNull("User '555' with password 'xx' could not authonticate.",user);
		if (user != null)
			System.out.println("User " + user.getDisplayName() + " is authonticated.");
		user = securityBD.authenticate("555","yy");
		assertNull("User '555' with password 'yy' should not be authonticated.",user);

		PrivilegeDependantsDTO privilegeDependantsDTO;
		privilegeDependantsDTO = securityBD.getPrivilegeDependencies("MAKERES.PAYMNT");
		System.out.print("The given number of privilege dependencies are retrieved :" + privilegeDependantsDTO.getDependantPrevilegeIds().size());
		assertTrue("privilege dependencies are not retrieved",privilegeDependantsDTO.getDependantPrevilegeIds().size()>0);
		System.out.print("Privileges are retrieved -> Privilege : " + privilegeDependantsDTO.getPrivilegeId() + 
				" a dipendent privilege : " + privilegeDependantsDTO.getDependantPrevilegeIds().iterator().next().toString());
		//Checking user page
		System.out.println("Checking user pages...");
		Page page = securityBD.getUsers(1,3);
		assertTrue("Get users method dosn't work well", page.getPageData().size() > 1);
		System.out.println("Retrieved page users : " +page.getPageData().size() );
		System.out.println("One of the retrieved user is " + ((User)page.getPageData().iterator().next()).getDisplayName() );
		
		//Checking active users collection 
		System.out.println("Checking active users collection");
		Collection usersCollection = securityBD.getActiveUsers();
		assertTrue("Get active users method dosn't work well", usersCollection.size() > 1);
		System.out.println("Retrieved number of users : " +usersCollection.size() );
		System.out.println("One of the retrieved user is " + ((User)usersCollection.iterator().next()).getDisplayName() );
		
		//Checking active roles collection 
		System.out.println("Checking active roles collection");
		Collection rolesCollection = securityBD.getActiveRoles();
		assertTrue("Get active roles method dosn't work well", rolesCollection.size() > 1);
		System.out.println("Retrieved number of roles : " +rolesCollection.size() );
		System.out.println("One of the retrieved role is " + ((Role)rolesCollection.iterator().next()).getRoleName() );	
	    
		//Check duplicate Save for Roll
		System.out.println("Going to check for duplicate Role");
		boolean duplicate = false;
		role = new Role();
		role.setRoleId(1);
		role.setRoleName("xxxx");
		role.setStatus(Role.STATUS_ACTIVE);
		role.setRemarks("Duplicate Role entered by Thejaka");
		Set privileges = new HashSet();	
		privileges.add(securityBD.getPrivilege("AIRCRAFT.UPD"));
		privileges.add(securityBD.getPrivilege("AIRPORT.DEL"));
		role.setPrivileges(privileges);
		duplicate = false;
		try{
		securityBD.saveRole(role);
		}catch(ModuleException e){
			duplicate = true;
			System.out.println("Exception is:-" + e.getExceptionCode()); 
			//e.printStackTrace();
		}
		assertTrue("Duplicate role is not identified !",duplicate);
		
		//Check duplicate Save for User
		System.out.println("Going to check for duplicate User");
		User obj1 = new User();
		String key = "222";		
		obj1.setUserId(key);
		obj1.setEmailId("aa@aa.com");
		obj1.setDisplayName("Thejaka");
//		obj1.setLastName("Usgoda Arachchi");
		obj1.setPassword("xx");
//		obj1.setUserDomainID(1);
//		obj1.setStationCode("CMB");
		obj1.setStatus(User.STATUS_ACTIVE);
		duplicate = false;
		try{
			securityBD.saveUser(obj1);
		}catch(ModuleException e){
			duplicate = true;
			System.out.println("Duplication Found for User" + e.getExceptionCode());					
			//e.printStackTrace();
		}
		assertTrue("Duplicate user is not identified !",duplicate);
	}
}
