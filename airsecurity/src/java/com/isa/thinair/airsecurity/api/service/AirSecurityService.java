/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.api.service;

import com.isa.thinair.platform.api.IModule;

/**
 * @author Thejaka
 * 
 *         Security module's service interface
 */
public interface AirSecurityService extends IModule {
}
