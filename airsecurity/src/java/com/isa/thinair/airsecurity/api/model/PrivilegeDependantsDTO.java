/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:02
 * 
 * $Id$
 * 
 * ===============================================================================
 */
//package com.isa.thinair.security.api.model;

package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;
import java.util.Collection;

public class PrivilegeDependantsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1540033135115049442L;

	private String privilegeId;

	private Collection<String> dependantPrevilegeIds;

	public String getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(String privId) {
		privilegeId = privId;
	}

	public Collection<String> getDependantPrevilegeIds() {
		return dependantPrevilegeIds;
	}

	public void setDependantPrevilegeIds(Collection<String> depends) {
		dependantPrevilegeIds = depends;
	}

}
