/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0
 *
 * Copyright (c) 2005-07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:58
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airsecurity.api.dto.external.PrivilegeTO;
import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public interface SecurityBDExternal {

	public static final String SERVICE_NAME = "SecurityService";

	/**
	 * Return PrivilegeTO(s) corresponding to Role Id(s)
	 * 
	 * @param roleIdCollection
	 * @return
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Map<String, Collection<PrivilegeTO>> getPrivilegeTOsForRoleIDs(Collection<String> colRoleId) throws ModuleException;

	/**
	 * Return UserTO(s) corresponding to Privilege Id(s)
	 * 
	 * @param colPrivilege
	 * @param loadPrivileges
	 * @return
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Map<String, Collection<UserTO>> getUserTOs(Collection<String> colPrivilegeIds, boolean loadPrivileges) throws ModuleException;

	/**
	 * Retrieve specific UserTO by userId, with privileges set in User object Remember roles are set by default for the
	 * user
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 * @since 2.0
	 */
	public UserTO getUserTO(String userId, boolean loadPrivileges) throws ModuleException;

	/**
	 * Get paged Roles matching the criteria
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Page searchRoleTOs(List<ModuleCriterion> criteria, int sartIndex, int noRecs, List<String> orderByFieldList) throws ModuleException;

	/**
	 * Get paged User data which is matching the criteria, ordered by userId
	 * 
	 * @param userSearchDTO
	 * @param startIndex
	 * @param noRecs
	 * @return Paged User data
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Page searchUserTOs(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException;

	/**
	 * Get all the active RoleTOs
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Collection<RoleTO> getActiveRoleTOs() throws ModuleException;

	/**
	 * Get all the PrivilegeTOs
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Collection<PrivilegeTO> getPrivilegeTOs() throws ModuleException;

	/**
	 * Remove the User
	 * 
	 * @param userId
	 * @param version
	 * @throws ModuleException
	 * @since 2.0
	 */
	public void removeUser(String userId, long version) throws ModuleException;

	/**
	 * Save or update userTO
	 * 
	 * @param userTO
	 * @throws ModuleException
	 * @since 2.0
	 */
	public void saveOrUpdateUser(UserTO userTO) throws ModuleException;

	/**
	 * Delete Role by roleId
	 * 
	 * @param roleID
	 * @param version
	 * @throws ModuleException
	 * @since 2.0
	 */
	public void removeRole(String roleID, long version) throws ModuleException;

	/**
	 * Save or update a RoleTO
	 * 
	 * @param roleTO
	 * @throws ModuleException
	 * @since 2.0
	 */
	public void saveOrUpdateRole(RoleTO roleTO) throws ModuleException;
}
