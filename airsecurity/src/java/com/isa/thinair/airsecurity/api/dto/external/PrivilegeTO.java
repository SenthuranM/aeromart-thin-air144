/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:24:02
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.dto.external;

import java.io.Serializable;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * 
 *          Privilege is the entity class to repesent a Privilege model
 * 
 */
public class PrivilegeTO implements Serializable {

	private static final long serialVersionUID = 8436972144616459019L;

	private String privilegeId;

	private String privilege;

	private String categoryID;

	/**
	 * returns the privilegeId
	 * 
	 * @return Returns the privilegeId.
	 * 
	 */
	public String getPrivilegeId() {
		return privilegeId;
	}

	/**
	 * sets the privilegeId
	 * 
	 * @param privilegeId
	 *            The privilegeId to set.
	 */
	public void setPrivilegeId(String privilegeId) {
		this.privilegeId = privilegeId;
	}

	/**
	 * returns the privilege
	 * 
	 * @return Returns the privilege.
	 * 
	 */
	public String getPrivilege() {
		return privilege;
	}

	/**
	 * sets the privilege
	 * 
	 * @param privilege
	 *            The privilege to set.
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	/**
	 * returns the privilege
	 * 
	 * @return Returns the privilege category id.
	 * 
	 */
	public String getPrivilegeCategoryID() {
		return categoryID;
	}

	/**
	 * sets the privilege category id
	 * 
	 * @param privilege
	 *            The privilege to set.
	 */
	public void setPrivilegeCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}
}
