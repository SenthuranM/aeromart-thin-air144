/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:58
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.PrivilegeDependantsDTO;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Thejaka
 */
public interface SecurityBD {

	public static final String SERVICE_NAME = "SecurityService";

	/**
	 * Save or Updaate user
	 * 
	 * @param user
	 * @throws ModuleException
	 */
	public void saveUser(User user) throws ModuleException;

	/**
	 * Update dashboard preferences for user
	 * 
	 * @param userId
	 * @param pref
	 * @throws ModuleException
	 */
	public void saveDashboardPref(String userId, String pref) throws ModuleException;

	/**
	 * Get paged User data, ordered by userId
	 * 
	 * @param sartIndex
	 * @param noRecs
	 * @return Paged User data
	 * @throws ModuleException
	 */
	public Page getUsers(int sartIndex, int noRecs) throws ModuleException;

	/**
	 * Retrieve specific User by userId, with privileges set in User object
	 * 
	 * @param userId
	 * @return User
	 * @throws ModuleException
	 */
	public User getUser(String userId) throws ModuleException;

	/**
	 * Retrieve basic details of the User by userId in User object
	 * 
	 * @param userId
	 * @return User
	 * @throws ModuleException
	 */
	public User getUserBasicDetails(String userId) throws ModuleException;

	/**
	 * Retrieve specific User by userId, with the option to load privileges
	 * 
	 * @param userId
	 * @param loadPrivileges
	 * @return
	 * @throws ModuleException
	 */
	public User getUser(String userId, boolean loadPrivileges) throws ModuleException;

	/**
	 * Get paged User data which is matching the criteria, ordered by userId
	 * 
	 * @param userSearchDTO
	 * @param startIndex
	 * @param noRecs
	 * @return Paged User data
	 * @throws ModuleException
	 */
	public Page searchUsers(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException;

	/**
	 * Get paged User data which is matching the criteria, ordered by userId
	 * 
	 * @param userSearchDTO
	 * @param startIndex
	 * @param noRecs
	 * @return Paged User data
	 * @throws ModuleException
	 */
	public Page searchUsersWithAirlineSelection(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException;

	/**
	 * Get Users matching the criteria
	 * 
	 * @param criteria
	 * @return Collection of User
	 * @throws ModuleException
	 */
	public Collection<User> searchUsers(List<ModuleCriterion> criteria) throws ModuleException;

	/**
	 * Get paged Users matching the criteria
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @return Paged User collection
	 * @throws ModuleException
	 */
	public Page searchUsers(List<ModuleCriterion> criteria, int sartIndex, int noRecs) throws ModuleException;

	/**
	 * Get paged Users matching the criteria, ordered by orderByFieldList items
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return Paged User collection
	 * @throws ModuleException
	 */
	public Page searchUsers(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList)
			throws ModuleException;

	/**
	 * Get all the users
	 * 
	 * @return User collection
	 * @throws ModuleException
	 */
	public Collection<User> getUsers() throws ModuleException;

	/**
	 * Get all the usersIds for given agentCode
	 * 
	 * @param agentCode
	 * @return userId collection
	 * @throws ModuleException
	 */
	public Collection<String> getAgentUserIDs(String agentCode) throws ModuleException;

	/**
	 * Get all the users for given agentCode
	 * 
	 * @param agentCode
	 * @return User collection
	 * @throws ModuleException
	 */
	public Collection<User> getAgentUsers(String agentCode) throws ModuleException;

	/**
	 * Get all the active users
	 * 
	 * @return User collection
	 * @throws ModuleException
	 */
	public Collection<User> getActiveUsers() throws ModuleException;

	/**
	 * Get User collection corresponding to userID collection
	 * 
	 * @param userIDs
	 * @return User collection
	 * @throws ModuleException
	 */
	public Collection<User> getBasicUserInformation(Collection<String> userIDs) throws ModuleException;

	/**
	 * Delete user by userId
	 * 
	 * @param userId
	 * @throws ModuleException
	 */
	public void removeUser(String userId) throws ModuleException;

	/**
	 * Delete user
	 * 
	 * @param user
	 * @throws ModuleException
	 */
	public void removeUser(User user) throws ModuleException;

	/**
	 * Save or update a Role
	 * 
	 * @param role
	 * @throws ModuleException
	 */
	public Role saveRole(Role role, Map<String, String[]> mapCustomContents) throws ModuleException;

	/**
	 * Get all the Roles, order by RoleId
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 */
	public Collection<Role> getRolesByServiceChannels(Collection<String> channels) throws ModuleException;

	/**
	 * Get all the dry exclusion roles which will be added automatically when a user is created.
	 * 
	 * @param channels
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Role> getDryExcludeRolesByServiceChannels(Collection<String> channels) throws ModuleException;

	/**
	 * Get all the Roles, order by RoleId based on channel
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 */
	public Collection<Role> getRoles(String channel) throws ModuleException;

	/**
	 * Get all the editable roles
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 */
	public Collection<Role> getEditableRoles() throws ModuleException;

	/**
	 * Get all the active Roles
	 * 
	 * @return Role collection
	 * @throws ModuleException
	 */
	public Collection<Role> getActiveRoles() throws ModuleException;

	/**
	 * Get paged Roles, ordered by RoleId
	 * 
	 * @param sartIndex
	 * @param noRecs
	 * @return Paged Role collection
	 * @throws ModuleException
	 */
	public Page getRoles(int sartIndex, int noRecs) throws ModuleException;

	/**
	 * Get Roles matching the criteria
	 * 
	 * @param criteria
	 * @return Role collection
	 * @throws ModuleException
	 */
	public Collection<Role> searchRoles(List<ModuleCriterion> criteria) throws ModuleException;

	/**
	 * Get paged Roles matching the criteria
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @return Paged Role collection
	 * @throws ModuleException
	 */
	public Page searchRoles(List<ModuleCriterion> criteria, int sartIndex, int noRecs) throws ModuleException;

	/**
	 * Get Role
	 * 
	 * @param roleID
	 * @return Role
	 * @throws ModuleException
	 */
	public Role getRole(String roleID) throws ModuleException;

	/**
	 * Delete Role by roleId
	 * 
	 * @param roleID
	 * @throws ModuleException
	 */
	public void removeRole(String roleID, String serviceChannel) throws ModuleException;

	/**
	 * Delete Role
	 * 
	 * @param role
	 * @throws ModuleException
	 */
	public void removeRole(Role role) throws ModuleException;

	/**
	 * Get Privilege collection for roleId
	 * 
	 * @param roleId
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getPrivilegesForRole(String roleId) throws ModuleException;

	/**
	 * Get Privilege collection for roleId
	 * 
	 * @param roleId
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getPrivilegesForRole(String roleId, String serviceChannel) throws ModuleException;

	/**
	 * Returns the exclude privileges for given dry user and OC
	 * 
	 * @param userId
	 * @param airlineCode
	 * @return
	 * @throws ModuleException
	 */
	public Set<String> getExcludePrivilegesForDryUser(String userId, String opCarrierAirlineCode) throws ModuleException;

	/**
	 * Returns the include privileges for given dry user and OC
	 * 
	 * @param userId
	 * @param airlineCode
	 * @return
	 * @throws ModuleException
	 */
	public Set<String> getIncludePrivilegesForDryUser(String userId, String opCarrierAirlineCode) throws ModuleException;

	/**
	 * Get Privilege collections for roleIds
	 * 
	 * @param roleIdCollection
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Collection<Privilege>> getPrivilegesForRoleIds(Collection<String> roleIdCollection, String channel)
			throws ModuleException;

	/**
	 * Get distinct Privilege for selected role ids
	 * 
	 * @param roleIdCollection
	 * @param service
	 *            channel
	 * @return Collection
	 * @throws ModuleException
	 */
	public Collection<String[]> getDistinctPrivilegesForRoles(Collection<String> roleIdCollection, String channel)
			throws ModuleException;

	/**
	 * Get all the privileges based on service channel
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getPrivileges(String serviceChannel) throws ModuleException;

	/**
	 * Get all active privileges
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getActivePrivileges() throws ModuleException;

	/**
	 * Get all active privileges based on service channel
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getActivePrivileges(String serviceChannel) throws ModuleException;

	/**
	 * Get all active visible privileges
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getActiveVisiblePrivileges() throws ModuleException;

	/**
	 * Get all active visible privileges basedon service channel
	 * 
	 * @return Privilege collection
	 * @throws ModuleException
	 */
	public Collection<Privilege> getActiveVisiblePrivileges(String serviceChannel) throws ModuleException;

	/**
	 * Get privilege for privilegeId
	 * 
	 * @param privilegeId
	 * @return Privilege
	 * @throws ModuleException
	 */
	public Privilege getPrivilege(String privilegeId, String serviceChannel) throws ModuleException;

	/**
	 * Get dependent privileges ids for a given privilegeId
	 * 
	 * @param previlegeId
	 * @return PrivilegeDependantsDTO
	 * @throws ModuleException
	 */
	public PrivilegeDependantsDTO getPrivilegeDependencies(String previlegeId, String serviceChannel) throws ModuleException;

	/**
	 * Get dependent privilege id collections for privilege ids
	 * 
	 * @param previlegeIds
	 * @return Depedendent privilege id collections keyed by privilege id
	 * @throws ModuleException
	 */
	public HashMap<String, List<String>> getPrivilegeDependencies(Collection<String> previlegeIds, String serviceChannel)
			throws ModuleException;

	/**
	 * Get User with privileges set, if authentication successful
	 * 
	 * @param userId
	 * @param password
	 *            Plain-text password
	 * @return User, null if authentication fails
	 * @throws ModuleException
	 */
	public User authenticate(String userId, String password) throws ModuleException;

	/**
	 * Return Roles for Role Id(s)
	 * 
	 * @param colRoleIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Role> getRoles(Collection<String> colRoleIds) throws ModuleException;

	/**
	 * Return Privilege for Privilege Id(s)
	 * 
	 * @param colPrivilegeIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Privilege> getPrivileges(Collection<String> colPrivilegeIds, String serviceChannel) throws ModuleException;

	/**
	 * Return Collections of UserTO(s) corresponding to AgentCodes
	 * 
	 * @param colAgentCode
	 * @return
	 * @throws ModuleException
	 * @since 2.0
	 */
	public Map<String, Collection<UserTO>> getAgentUserSortedMap(Collection<String> colAgentCode) throws ModuleException;

	/**
	 * 
	 * @param colUserCodes
	 * @return
	 * @throws ModuleException
	 */
	public Collection<String[]> getUserRoleSortedMap(Collection<String> colUserCodes) throws ModuleException;

	public void blockUsers(Collection<String> agentCodes) throws ModuleException;

	/**
	 * Save or Update usernotes
	 * 
	 * @param uNotes
	 * @throws ModuleException
	 */
	public void saveUserNotes(UserNotes uNotes) throws ModuleException;

	/**
	 * Get usernotes
	 * 
	 * @param user
	 * @throws ModuleException
	 */
	public Collection<UserNotes> getUserNotes(String userNoteType, String user) throws ModuleException;

	/**
	 * Get checkin notes
	 * 
	 * @param strFlightSegId
	 * @throws ModuleException
	 */
	public Collection<UserNotes> getCheckinNotes(String userNoteType, String strFlightSegId) throws ModuleException;

	/**
	 * Return dry User Enable state for given carrier code
	 * 
	 * @param defCarrierCode
	 * @return
	 * @throws ModuleException
	 */
	public String getDryUserEnable(String defCarrierCode) throws ModuleException;

	public Collection<User> getUsersByID(Collection<String> userIDList) throws ModuleException;

	public String getNextSequenceForRole() throws ModuleException;

	public Map<String, Set<String>> getAssignedAgentTypesForRoles(Collection<String> roleIds) throws ModuleException;

	public void setUserNewMessageStatus(Set<String> userIds, Set<String> agentCodes, Set<String> agentTypeCodes,
			Set<String> agentPos) throws ModuleException;

	public void updateUserNewMessageStatus(String userId, String newMessageStatus) throws ModuleException;

	public boolean isNewDashBoardMsgsExists(String userId) throws ModuleException;

	public Privilege getPrivilege(String privilegeId) throws ModuleException;

	/**
	 * Gets the GMT time difference saved in the database for each airport that the given user is assigned to.
	 * 
	 * @param userName
	 *            User whose GMT time difference need to be retrieved.
	 * @return the GMT time difference in minutes, returning integer can be + or - as defined in the database entry.
	 * @throws ModuleException
	 *             If any underlying operations throw an exception
	 */
	public int getUserGMTTimeDifference(String userName) throws ModuleException;

	public String[] getMyIDUserForCarrier(String myIDCarrierCode) throws ModuleException;

	public List<User> getLCCUnpublishedUsers() throws ModuleException;

	public List<Role> getLCCUnpublishedRoles() throws ModuleException;

	public void updateUserVersion(String userId, long version) throws ModuleException;

	public void updateRoleVersion(String roleId, long version) throws ModuleException;

	public void updateRoleLCCPublishStatus(List<Role> unPublishedRoles, List<String> updatedRoleIds) throws ModuleException;

	public void updateUserLCCPublishStatus(List<User> unPublishedUsers, List<String> updatedUserIds) throws ModuleException;

	public void saveOrUpdateUser(UserTO userTO) throws ModuleException;
	
	public boolean isPasswordUsed(String userID, String password) throws ModuleException;
	
	public String getUserAccountLockStatus(String userID, boolean clearLock)  throws ModuleException;
	
	public Collection<String> getReportingAgentCodesIncludeSubAgentLevels(String agentCode, String agentStatus,
			boolean isPrivilegedAgent) throws ModuleException;

	public boolean isCaptchaEnableForUser(String userIP) throws ModuleException;
	/**
	 * This method will return the minimum of MAX_ADULT_COUNT corresponds to user and the agent 
	 * 
	 * @param userID user Id of the logged user
	 * @return Maximum value of adults in a reservation for the given user.
	 */
	public Integer getMaxAdultCount(String userID);

	public Collection<String> getAllReportingAgentCodes(String agentCode, boolean isRetrieveAllReportingAgents)
			throws ModuleException;
}
