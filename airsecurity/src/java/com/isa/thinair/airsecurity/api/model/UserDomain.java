/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:30:13
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.model;

import java.io.Serializable;

/**
 * 
 * @author : Code Generation Tool and Thejaka
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_USER_DOMAIN"
 * 
 *                  UserDomain is the entity class to repesent a UserDomain model
 * 
 */

public class UserDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1260765867605716737L;
	public static final int DOMAINID_INTERNAL = 1;
	public static final int DOMAINID_EXTERNAL = 2;

	private int userDomainId;

	private String domainName;

	/**
	 * returns the userDomainId
	 * 
	 * @return Returns the userDomainId.
	 * 
	 * @hibernate.id column = "USER_DOMAIN_ID" generator-class = "assigned"
	 */
	public int getUserDomainId() {
		return userDomainId;
	}

	/**
	 * sets the userDomainId
	 * 
	 * @param userDomainId
	 *            The userDomainId to set.
	 */
	public void setUserDomainId(int userDomainId) {
		this.userDomainId = userDomainId;
	}

	/**
	 * returns the domainName
	 * 
	 * @return Returns the domainName.
	 * 
	 * @hibernate.property column = "DOMAIN_NAME"
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * sets the domainName
	 * 
	 * @param domainName
	 *            The domainName to set.
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
