/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.api.service;

import java.util.Collection;

import com.isa.thinair.airsecurity.api.model.PrivilegeCategory;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Thejaka
 */
public interface SecurityUtilBD {
	public static final String SERVICE_NAME = "SecurityUtilService";

	//public Collection getUserDomains() throws ModuleException;

	//public UserDomain getUserDomain(int userDomainId) throws ModuleException;

	public Collection<PrivilegeCategory> getPrivilegeCategories() throws ModuleException;

	public PrivilegeCategory getPrivilegeCategory(String privilegeCategoryId) throws ModuleException;

	//public Collection getUsersForUserDomain(int domainId) throws ModuleException;
}
