package com.isa.thinair.airsecurity.api.utils;

import java.util.Date;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AirSecurityUtil {

	public static boolean isPasswordExpire(Date passwordExpiryDate) {
		boolean passExpire = false;

		if (AppSysParamsUtil.isEnablePasswordExpiry()) {
			if (passwordExpiryDate != null && passwordExpiryDate.compareTo(new Date()) <= 0) {
				passExpire = true;
			}
		}

		return passExpire;
	}

}
