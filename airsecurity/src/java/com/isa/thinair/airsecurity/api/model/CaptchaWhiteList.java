package com.isa.thinair.airsecurity.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Shiluka
 * @version : V 1.0.0
 * 
 * @hibernate.class table = "T_CAPTCHA_WHITELIST"
 * 
 *                  Contains WhiteListed IPs for the aeromart-agent's login page image captcha
 * 
 */
public class CaptchaWhiteList extends Tracking {
	
	private static final long serialVersionUID = -1254356985674524568L;
	
	private String ip_address;
	
	private String carrier;
	
	private String status;
	
	public CaptchaWhiteList() {
		
	}

	/**
	 * @return the ip_address
	 * @hibernate.id column = "IP_ADDRESS" generator-class = "assigned"
	 */
	public String getIp_address() {
		return ip_address;
	}

	/**
	 * @param ip_address the ip_address to set
	 */
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	/**
	 * @return the carrier
	 * @hibernate.property column = "CARRIER"
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
}
