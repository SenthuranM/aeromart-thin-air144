package com.isa.thinair.airsecurity.api.utils;

public class AirSecurityInternalConstants {

	/** Role change email templeate */
	public static final String ROLE_CHANGE_EMAIL_TEMPLEATE = "role_change";
	public static final String ROLE_PRIVILEGE_CHANGE_EMAIL_TEMPLATE = "role_privilege_change";

}
