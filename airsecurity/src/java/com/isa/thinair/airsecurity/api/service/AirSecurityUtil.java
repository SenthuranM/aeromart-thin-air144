package com.isa.thinair.airsecurity.api.service;

import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airsecurity.core.config.AirSecurityConfig;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class AirSecurityUtil {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("airsecurity");
	}

	/**
	 * Returns transparent CryptoServiceBD instance
	 * 
	 * @return
	 */
	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) lookupServiceBD(CryptoConstants.MODULE_NAME, CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Returns transparent SecurityBD instance
	 * 
	 * @return
	 */
	public static SecurityBD getSecurityBD() {
		return (SecurityBD) AirSecurityUtil.lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) AirSecurityUtil.lookupEJB3Service(MessagingConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) AirSecurityUtil
				.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static AirSecurityConfig getAirSecurityConfig() {
		return (AirSecurityConfig) LookupServiceFactory.getInstance().getModuleConfig("airsecurity");
	}

	/**
	 * For Looking the BookingClass BD IServiceDelegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirSecurityConfig(),
				"airsecurity", "airsecurity.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirSecurityConfig(), "airsecurity",
				"airsecurity.config.dependencymap.invalid");
	}

}
