package com.isa.thinair.airsecurity.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_USER_NOTES"
 * 
 *                  UserNotes is the entity class to represent a UserNote model
 * 
 */
public class UserNotes extends Persistent {

	private static final long serialVersionUID = 583905013935143740L;
	private int userNotesId;
	private String userNotesTypeCode;
	private String identificationCode;
	private String content;
	private String userId;
	private Date createdDate;

	/** default constructor */
	public UserNotes() {
	}

	/**
	 * 
	 * @hibernate.id column = "USER_NOTES_ID" type = "java.lang.Integer" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_USER_NOTES"
	 * 
	 */
	public int getUserNotesId() {
		return this.userNotesId;
	}

	public void setUserNotesId(int userNotesId) {
		this.userNotesId = userNotesId;
	}

	/**
	 * @hibernate.property column="USER_NOTES_TYPE_CODE"
	 */
	public String getUserNotesTypeCode() {
		return this.userNotesTypeCode;
	}

	public void setUserNotesTypeCode(String userNotesTypeCode) {
		this.userNotesTypeCode = userNotesTypeCode;
	}

	/**
	 * @hibernate.property column="IDENTIFICATION_CODE"
	 */
	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	/**
	 * @hibernate.property column="CONTENT"
	 */
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @hibernate.property column="USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the createdDate
	 * 
	 * @return createdDate
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the createdDate
	 * 
	 * @param createdDate
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
