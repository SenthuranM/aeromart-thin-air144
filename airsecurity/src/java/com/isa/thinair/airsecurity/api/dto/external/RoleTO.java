/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:27:01
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airsecurity.api.dto.external;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * Role is the entity class to repesent a Role Transfer Information
 */
public class RoleTO extends Tracking implements Serializable {	

	private static final long serialVersionUID = 8075213906659203805L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private final String STATUS_VISIBLE_TO_OTHERS = "Y";

	private final String STATUS_NOT_VISIBLE_TO_OTHERS = "N";	

	public String serviceChannel;

	private String roleId;

	private String roleName;

	private String remarks;

	private String status;

	private boolean visibleToOthers;

	private Collection<String> colPrivilegeIds;
	
	private boolean editable;

	public RoleTO() {
		visibleToOthers = false;
	}

	/**
	 * returns the roleId
	 * 
	 * @return Returns the roleId.
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * sets the roleId
	 * 
	 * @param roleId
	 *            The roleId to set.
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * returns the roleName
	 * 
	 * @return Returns the roleName.
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * sets the roleName
	 * 
	 * @param roleName
	 *            The roleName to set.
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the other's visibility status
	 * 
	 * @return Returns the other's visibility status.
	 */
	@SuppressWarnings("unused")
	private String getVisibilityStatus() {
		if (visibleToOthers)
			return STATUS_VISIBLE_TO_OTHERS;
		else
			return STATUS_NOT_VISIBLE_TO_OTHERS;
	}

	/**
	 * sets the other's visibility status
	 * 
	 * @param status
	 *            The other's visibility status to set.
	 */
	@SuppressWarnings("unused")
	private void setVisibilityStatus(String visibilityStatus) {
		if (STATUS_VISIBLE_TO_OTHERS.equals(visibilityStatus))
			this.visibleToOthers = true;
		else
			this.visibleToOthers = false;
	}

	public boolean isVisibleToOthers() {
		return visibleToOthers;
	}

	public void setVisibleToOthers(boolean visibleToOthers) {
		this.visibleToOthers = visibleToOthers;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return Returns the colPrivilegeIds.
	 */
	public Collection<String> getColPrivilegeIds() {
		return colPrivilegeIds;
	}

	/**
	 * Add Privilege Id(s)
	 * 
	 * @param privilegeId
	 */
	public void addPrivilegeId(String privilegeId) {
		if (this.getColPrivilegeIds() == null) {
			this.setColPrivilegeIds(new HashSet<String>());
		}

		this.getColPrivilegeIds().add(privilegeId);
	}

	/**
	 * @param colPrivilegeIds
	 *            The colPrivilegeIds to set.
	 */
	private void setColPrivilegeIds(Collection<String> colPrivilegeIds) {
		this.colPrivilegeIds = colPrivilegeIds;
	}

	public String getServiceChannel() {
		return serviceChannel;
	}

	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

}
