package com.isa.thinair.airsecurity.api.utils;

public class Constants {
	/**
	 * Service channel for AccelAero
	 */
	public static final String INTERNAL_CHANNEL = "AA";

	/**
	 * Service channel for Holidays
	 */
	public static final String EXTERNAL_CHANNEL = "HO";

	/**
	 * Service channel for both AccelAeor and Holidays
	 */
	public static final String BOTH_CHANNELS = "AH";

}
