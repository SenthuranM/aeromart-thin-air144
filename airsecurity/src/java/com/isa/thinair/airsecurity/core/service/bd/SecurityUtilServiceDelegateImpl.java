/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airsecurity.api.service.SecurityUtilBD;

/**
 * @author Thejaka
 */
@Remote
public interface SecurityUtilServiceDelegateImpl extends SecurityUtilBD {

}
