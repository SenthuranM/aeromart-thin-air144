package com.isa.thinair.airsecurity.core.audit;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public abstract class AuditAirSecurity {

	public static final String ROLE = "ROLE";
	public static final String USER = "USER";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(String code, String type, UserPrincipal principal) throws ModuleException {

		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (type.equals(ROLE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_ROLES));
		}
		if (type.equals(USER)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_USERS));
		}

		try {
			AirmasterUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (CommonsDataAccessException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (Exception e) {

			throw new ModuleException("airsecurity.technical.error", "airsecurity.desc");
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(Persistent persistant, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof User) {
			User user = (User) persistant;

			if (persistant.getVersion() <= 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_USERS));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_USERS));
			}

			contents.put("user_id", user.getUserId());
			contents.put("display_name", user.getDisplayName());
			contents.put("agent_code", user.getAgentCode());
			contents.put("create_agent_user", user.getNormalUserCreateStatus());
			contents.put("create_admin_user", user.getAdminUserCreateStatus());
			contents.put("role_ids", getRoleIdsAsString(user.getRoles()));
			contents.put("status", user.getStatus());
		}

		if (persistant instanceof Role) {
			Role obj = (Role) persistant;

			if (persistant.getVersion() <= 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_ROLES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_ROLES));
			}

			contents.put("role_id", String.valueOf(obj.getRoleId()));
			contents.put("role_name", obj.getRoleName());
			contents.put("status", obj.getStatus());
			contents.put("remarks", obj.getRemarks());
		}

		try {
			AirmasterUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (CommonsDataAccessException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (Exception e) {

			throw new ModuleException("airsecurity.technical.error", "airsecurity.desc");
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(Persistent persistant, UserPrincipal principal, Map<String, String[]> mapCustomContents)
			throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof Role) {
			Role obj = (Role) persistant;

			if (persistant.getVersion() <= 0) {

				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_ROLES));
			} else if ((persistant.getVersion() > 0) && (mapCustomContents.get("granted_privileges").length == 0)
					&& (mapCustomContents.get("revoked_privileges").length == 0)) {

				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_ROLES));
			} else {
				if ((mapCustomContents.get("granted_privileges") != null)
						&& (mapCustomContents.get("granted_privileges").length > 0)
						&& (mapCustomContents.get("revoked_privileges").length == 0)) {
					audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_PRIVILEGES));
				} else {
					audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_PRIVILEGES));
				}
			}

			contents.put("role_id", String.valueOf(obj.getRoleId()));
			contents.put("role_name", obj.getRoleName());
			contents.put("status", obj.getStatus());
			contents.put("remarks", obj.getRemarks());

		}

		if (persistant instanceof User) {
			User user = (User) persistant;

			if (persistant.getVersion() <= 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_USERS));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_USERS));
			}

			contents.put("user_id", user.getUserId());
			contents.put("display_name", user.getDisplayName());
			contents.put("agent_code", user.getAgentCode());
			contents.put("create_agent_user", user.getNormalUserCreateStatus());
			contents.put("create_admin_user", user.getAdminUserCreateStatus());
			contents.put("role_ids", getRoleIdsAsString(user.getRoles()));
			contents.put("status", user.getStatus());
		}

		if (mapCustomContents != null && !mapCustomContents.isEmpty()) {

			for (Entry<String, String[]> entry : mapCustomContents.entrySet()) {
				contents.put(entry.getKey(), entry.getValue());
			}
		}

		try {
			AirmasterUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (CommonsDataAccessException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (Exception e) {

			throw new ModuleException("airsecurity.technical.error", "airsecurity.desc");
		}

	}

	public static void doAudit(Persistent persistant, UserPrincipal principal, String oldPassword) throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof User) {
			User user = (User) persistant;

			if (persistant.getVersion() <= 0)
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_NEW_PASSWORD));
			else
				audit.setTaskCode(String.valueOf(TasksUtil.RESET_CHANGE_PASSWORD));

			contents.put("old_password", oldPassword);
			contents.put("new_password", user.getPassword());
			contents.put("user_id", user.getUserId());
			contents.put("display_name", user.getDisplayName());
			contents.put("agent_code", user.getAgentCode());
			contents.put("status", user.getStatus());

		}

		try {
			AirmasterUtils.getAuditorBD().audit(audit, contents);
		} catch (ModuleException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (CommonsDataAccessException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		} catch (Exception e) {

			throw new ModuleException("airsecurity.technical.error", "airsecurity.desc");
		}

	}

	private static String getRoleIdsAsString(Collection<Role> roles) {
		StringBuffer buf = new StringBuffer();

		if (roles != null && roles.size() > 0) {
			Role value = null;
			boolean isFirst = true;
			for (Iterator<Role> valuesIt = roles.iterator(); valuesIt.hasNext();) {
				value = (Role) valuesIt.next();
				if (value != null) {
					if (!isFirst) {
						buf.append(",");
					} else {
						isFirst = false;
					}
					buf.append(value.getRoleId());
				}
			}
		}

		return buf.toString();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAudit(Persistent persistant, UserPrincipal principal, Collection[] colRoleChanges)
			throws ModuleException {

		if (persistant instanceof User) {

			User user = (User) persistant;

			Audit audit = new Audit();
			audit.setTimestamp(new Date());
			audit.setTaskCode(TasksUtil.MASTER_CHANGE_ROLE);
			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			audit.setUserId(principal != null ? principal.getName() : "");

			if (colRoleChanges[0] != null && colRoleChanges[1] != null && persistant.getVersion() > 0) {

				contents.put("removedRoles", getRoleIdsAsString(colRoleChanges[0]));
				contents.put("addedRoles", getRoleIdsAsString(colRoleChanges[1]));
				contents.put("user_id", user.getUserId());
			}

			try {
				AirmasterUtils.getAuditorBD().audit(audit, contents);
			} catch (ModuleException e) {
				throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
			} catch (CommonsDataAccessException e) {
				throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
			} catch (Exception e) {
				throw new ModuleException("airsecurity.technical.error", "airsecurity.desc");
			}

		}
	}

}
