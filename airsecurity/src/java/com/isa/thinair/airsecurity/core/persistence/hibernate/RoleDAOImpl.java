/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:27:01
 * 
 * ===============================================================================
 */

package com.isa.thinair.airsecurity.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airsecurity.core.persistence.dao.RoleDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * BHive: Role module
 * 
 * @author Thejaka
 * @isa.module.dao-impl dao-name="RoleDAOImpl"
 * 
 *                      RoleDAOImpl is the RoletDAO implementatiion for hibernate
 * 
 */
public class RoleDAOImpl extends PlatformBaseHibernateDaoSupport implements RoleDAO {

	// private Log log = LogFactory.getLog(RoleDAO.class);

	private final String channelAA = Constants.INTERNAL_CHANNEL;
	private final String channelHO = Constants.EXTERNAL_CHANNEL;
	private final String channelHA = Constants.BOTH_CHANNELS;

	public Collection<Role> getEditableRoles() {
		return find(
				"select role from Role as role where role.roleId <> " + AppSysParamsUtil.getDefaultCarrierCode()
						+ Role.UNEDITABLE_ROLE_ID + " and role.serviceChannel in ('" + channelAA + "','" + channelHA
						+ "') order by role.roleId ", Role.class);
	}

	public Collection<Role> getRoles() {

		return find("from Role as r where r.serviceChannel = '" + channelHO + "' order by r.roleId ", Role.class);
	}

	public Collection<Role> getRolesByServiceChannels(Collection<String> channels) {
		return find("from Role as r where r.serviceChannel IN (" + Util.buildStringInClauseContent(channels)
						+ ") order by r.roleId ", Role.class);
	}

	@Override
	public Collection<Role> getDryExcludeRolesByServiceChannels(Collection<String> channels) {
		return find(
				"from Role as r where r.serviceChannel IN (" + Util.buildStringInClauseContent(channels)
						+ ")  and r.includeExcludeFlag =  '" + Role.EXCLUDE_RLOE + "' order by r.roleId ", Role.class);
	}

	public Collection<Role> getRoles(String channel) {

		return find("from Role as r where r.serviceChannel = '" + channel + "' order by r.roleId ", Role.class);
	}

	public Collection<Role> getRoles(Collection<String> colRoleIds) {
		return find(
				"from Role as role where role.roleId IN (" + Util.buildStringInClauseContent(colRoleIds)
						+ ") and role.serviceChannel = '" + channelAA + "' order by role.roleId ", Role.class);
	}

	public Collection<Role> getActiveRoles() {

		return (Collection<Role>) find(
				"select r from Role as r where r.status = '" + Role.STATUS_ACTIVE + "' and r.serviceChannel = '" + channelAA
						+ " ' " + " order by r.roleId ", Role.class);
	}

	public Collection<Role> getActiveRoles(String channel) {

		if (channel != null)
			return (Collection<Role>) find(
					"select r from Role as r where r.status = '" + Role.STATUS_ACTIVE + "' and r.serviceChannel = '" + channel
							+ " ' " + " order by r.roleId ", Role.class);
		else
			return null;
	}

	public Page getRoles(int startIndex, int noRecs) {
		return getRoles(startIndex, noRecs, null);
	}

	@SuppressWarnings("rawtypes")
	public Page getRoles(int startIndex, int noRecs, List<String> orderByFieldList) {
		try {
			Criteria criteria = getSession().createCriteria(Role.class);
			if (orderByFieldList == null)
				criteria.addOrder(Order.asc("roleId"));
			else {
				for (Iterator<String> iter = orderByFieldList.iterator(); iter.hasNext();) {
					String orderByField = (String) iter.next();
					criteria.addOrder(Order.asc(orderByField));
				}
			}
			return new Page(getRoles().size(), startIndex, startIndex + noRecs, (Collection) criteria.setFirstResult(startIndex)
					.setMaxResults(noRecs).list());
		} catch (HibernateException he) {
			throw new CommonsDataAccessException(he, "airsecurity.arg.dao.invalidrecordsrange");
		}
	}

	public Role getRole(String id) {
		List<Role> lisRole = null;
		Role role = null;

		lisRole = find("select r from Role as r where r.roleId = '" + id + "'", Role.class);// and
																										// r.serviceChannel
																										// = '"+
																										// channelAA +"'
		if (lisRole != null && lisRole.size() > 0) {
			role = (Role) lisRole.get(0);
		}
		return role;
	}

	public Role getRole(String id, String channel) {
		List<Role> lisRole = null;
		Role role = null;

		lisRole = find(
				"select r from Role as r where r.roleId = '" + id + "' and r.serviceChannel = '" + channel + "'", Role.class);
		if (lisRole != null && lisRole.size() > 0) {
			role = (Role) lisRole.get(0);
		}
		return role;
	}

	public Collection<Role> searchRoles(List<ModuleCriterion> criteria) {
		return searchRoles(criteria, null);
	}

	@SuppressWarnings("unchecked")
	public Collection<Role> searchRoles(List<ModuleCriterion> criteria, List<String> orderByFieldList) {
		try {
			return searchData(Role.class, criteria, orderByFieldList);
		} catch (HibernateException he) {
			throw new CommonsDataAccessException(he, "airsecurity.arg.dao.invalidcriteria");
		}
	}

	public Page searchRoles(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList) {
		try {
			// return new Page(searchRoles(criteria).size(), startIndex, startIndex+noRecs,
			// searchRoles(criteria));
			return searchData(Role.class, criteria, startIndex, noRecs, orderByFieldList);
			// return new Page(roleCollection.list().size(), startIndex, startIndex+noRecs,
			// (Collection)roleCollection.setFirstResult(startIndex).setMaxResults(startIndex+noRecs-1).list());
		} catch (HibernateException he) {
			throw new CommonsDataAccessException(he, "airsecurity.arg.dao.invalidcriteria");
		}
	}

	// private Page searchData(Class classOfSearch, List criteriaList, int startIndex, int noRecs) {
	// Criteria searchCriteria =
	// CriteriaParserForHibernate.parse(criteriaList,getSession().createCriteria(classOfSearch));
	// return new Page(searchCriteria.list().size(), startIndex, startIndex+noRecs,
	// (Collection)searchCriteria.setFirstResult(startIndex).setMaxResults(noRecs).list());
	// }

	@SuppressWarnings("rawtypes")
	private List searchData(Class classOfSearch, List<ModuleCriterion> criteriaList, List<String> orderByFieldList) {
		return CriteriaParserForHibernate.parse(criteriaList, getSession().createCriteria(classOfSearch), orderByFieldList)
				.list();
	}

	@SuppressWarnings("rawtypes")
	private Page searchData(Class classOfSearch, List<ModuleCriterion> criteriaList, int startIndex, int noRecs,
			List<String> orderByFieldList) {
		Criteria searchCriteria = CriteriaParserForHibernate.parse(criteriaList, getSession().createCriteria(classOfSearch),
				orderByFieldList);
		return new Page(searchCriteria.list().size(), startIndex, startIndex + noRecs, (Collection) searchCriteria
				.setFirstResult(startIndex).setMaxResults(noRecs).list());
	}

	public void saveRole(Role role) {
		hibernateSaveOrUpdate(role);

		// Comment updating user role part due to requirement of AARESAA-5019.Inactive role only can't assign for new
		// users.
		// existing user may have that role as same.
		/*
		 * if (role.getStatus().equals(Role.STATUS_INACTIVE)) {
		 * 
		 * JdbcTemplate jt = new JdbcTemplate( getDatasource()); jt.execute("DELETE FROM t_user_role WHERE role_id = " +
		 * String.valueOf(role.getRoleId()));
		 * 
		 * 
		 * // UserDAOImpl userDAO = new UserDAOImpl(); // List userList = find("from User" ); //
		 * Iterator userIter = userList.iterator(); // while (userIter.hasNext()) { // User user =
		 * (User)userIter.next(); // Set roles = user.getRoles(); // if (roles.remove(role)) { // user.setRoles(roles);
		 * // userDAO.saveUser(user); // } // } }
		 */
	}

	public void removeRole(String id) {
		Object role = load(Role.class, Integer.valueOf(String.valueOf(id)));
		delete(role);
	}

	public void removeRole(String id, String channel) {
		Object role = find(
				"select r from Role as r where r.roleId = '" + id + "' and r.serviceChannel = '" + channel + "'", Role.class);
		delete(role);
	}

	public void removeRole(Role role) {
		delete(role);
	}

	public Collection<Privilege> getPrivilegesForRole(String roleId) {
		Role role = (Role) get(Role.class, roleId);
		return role.getPrivileges();
	}

	public Collection<Privilege> getPrivilegesForRoleHolidays(String roleId) {
		Role role = (Role) find(
				"select r from Role as r where r.roleId = '" + roleId + "' and r.serviceChannel = '" + channelHO + "'", Role.class);
		return role.getPrivileges();
	}

	public Collection<Privilege> getPrivilegesForRole(String roleId, String channel) {
		List<Role> lstRoles = find(
				"select r from Role as r where r.roleId = '" + roleId + "' and r.serviceChannel = '" + channel + "'", Role.class);
		return lstRoles.get(0).getPrivileges();
	}

	@SuppressWarnings("unchecked")
	public Map<String, Collection<Privilege>> getPrivilegesForRoleIds(Collection<String> roleIdCollection) {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		String sql = " SELECT a.ROLE_ID, a.PRIVILEGE_ID, b.PRIVILEGE, b.PRIVILEGE_CATEGORY_ID "
				+ " FROM T_ROLE_PRIVILEGE a, T_PRIVILEGE b " + " WHERE a.PRIVILEGE_ID = b.PRIVILEGE_ID AND a.ROLE_ID in ("
				+ Util.buildIntegerInClauseContent(roleIdCollection) + ") ORDER BY a.ROLE_ID ";

		Map<String, Collection<Privilege>> privHashMap = (Map<String, Collection<Privilege>>) jt.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<Privilege>> privHashMap = new TreeMap<String, Collection<Privilege>>();
						Collection<Privilege> colPrivilegeTO;
						Privilege privilege;
						String roleId;

						if (rs != null) {
							while (rs.next()) {
								privilege = new Privilege();

								roleId = rs.getString("ROLE_ID");
								privilege.setPrivilegeId(rs.getString("PRIVILEGE_ID"));
								privilege.setPrivilege(rs.getString("PRIVILEGE"));
								privilege.setPrivilegeCategoryID(rs.getString("PRIVILEGE_CATEGORY_ID"));

								if (privHashMap.containsKey(roleId)) {
									colPrivilegeTO = (Collection<Privilege>) privHashMap.get(roleId);
									colPrivilegeTO.add(privilege);
								} else {
									colPrivilegeTO = new ArrayList<Privilege>();
									colPrivilegeTO.add(privilege);

									privHashMap.put(roleId, colPrivilegeTO);
								}
							}
						}

						return privHashMap;
					}
				});

		return privHashMap;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Collection<Privilege>> getPrivilegesForRoleIds(Collection<String> roleIdCollection, String channel) {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		String sql = " SELECT a.ROLE_ID, a.PRIVILEGE_ID, b.PRIVILEGE, b.PRIVILEGE_CATEGORY_ID "
				+ " FROM T_ROLE_PRIVILEGE a, T_PRIVILEGE b " + " WHERE a.PRIVILEGE_ID = b.PRIVILEGE_ID AND a.ROLE_ID in ("
				+ Util.buildStringInClauseContent(roleIdCollection) + ")AND b.SERVICE_CHANNEL_CODE = '" + channel
				+ "' ORDER BY a.ROLE_ID ";

		Map<String, Collection<Privilege>> privHashMap = (Map<String, Collection<Privilege>>) jt.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<Privilege>> privHashMap = new TreeMap<String, Collection<Privilege>>();
						Collection<Privilege> colPrivilegeTO;
						Privilege privilege;
						String roleId;

						if (rs != null) {
							while (rs.next()) {
								privilege = new Privilege();

								roleId = rs.getString("ROLE_ID");
								privilege.setPrivilegeId(rs.getString("PRIVILEGE_ID"));
								privilege.setPrivilege(rs.getString("PRIVILEGE"));
								privilege.setPrivilegeCategoryID(rs.getString("PRIVILEGE_CATEGORY_ID"));

								if (privHashMap.containsKey(roleId)) {
									colPrivilegeTO = (Collection<Privilege>) privHashMap.get(roleId);
									colPrivilegeTO.add(privilege);
								} else {
									colPrivilegeTO = new ArrayList<Privilege>();
									colPrivilegeTO.add(privilege);

									privHashMap.put(roleId, colPrivilegeTO);
								}
							}
						}

						return privHashMap;
					}
				});

		return privHashMap;
	}

	public String getNextSeqNumberRole() {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		String sql = " SELECT S_ROLE.NEXTVAL NEXTSEQ FROM DUAL ";

		String seqno = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String temp = "";

				if (rs != null) {
					if (rs.next()) {
						temp = rs.getString("NEXTSEQ");
					}
				}

				return temp;
			}
		});

		return seqno;
	}

	public Collection<String[]> getDistinctPrivilegesForRoles(Collection<String> roleIdCollection, String channel) {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		final Set<String[]> list = new LinkedHashSet<String[]>();
		final Set temporySet = new LinkedHashSet();

		String sql = " select distinct rp.privilege_id, p.privilege " + " from t_role_privilege rp, t_privilege p "
				+ " where rp.privilege_id = p.privilege_id " + " and rp.role_id in ("
				+ Util.buildStringInClauseContent(roleIdCollection) + ") and p.service_channel_code = '" + channel
				+ "' order by p.privilege ";

		jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				// if result set is not null
				if (rs != null) {

					// populate recordMap
					while (rs.next()) {
						String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
								rs.getObject(2) != null ? rs.getObject(2).toString() : "" };

						if (!temporySet.contains(rs.getObject(1))) {
							temporySet.add(rs.getObject(1));
							list.add(s);
						}
					}
				}
				return null;
			}
		});

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Set<String>> getAssignedAgentTypesForRoles(Collection<String> roleIds) {
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		Map<String, Set<String>> roleAgentTypeMap = new HashMap<String, Set<String>>();

		for (Object roleId : roleIds) {

			String role = (String) roleId;
			String sql = "SELECT DISTINCT TA.AGENT_TYPE_CODE FROM T_USER TU, T_USER_ROLE TUR ,T_AGENT TA "
					+ "WHERE TA.AGENT_CODE = TU.AGENT_CODE AND TUR.USER_ID = TU.USER_ID AND TUR.ROLE_ID ='" + role + "'";

			Object agentTypes = jt.query(sql, new ResultSetExtractor() {

				public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {
					Set<String> tempAgentTypes = null;
					if (rs != null) {
						tempAgentTypes = new HashSet<String>();
						while (rs.next()) {
							tempAgentTypes.add(rs.getString("AGENT_TYPE_CODE"));
						}
					}

					return tempAgentTypes;
				}
			});

			if (agentTypes != null) {
				roleAgentTypeMap.put(role, (Set<String>) agentTypes);
			} else {
				roleAgentTypeMap.put(role, null);
			}

		}

		return roleAgentTypeMap;

	}

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public List<Role> getLCCUnpublishedRoles() {
		return find(
				"from Role role where role.lccPublishStatus in ('" + Util.LCC_TOBE_PUBLISHED + "','" + Util.LCC_TOBE_REPUBLISHED
						+ "')", Role.class);
	}

	@Override
	public void updateRoleVersion(String roleId, long version) {
		String hql = "UPDATE Role SET version=:version WHERE roleId=:roleId";
		Query qry = getSession().createQuery(hql).setParameter("version", version).setParameter("roleId", roleId);
		qry.executeUpdate();
	}

	@Override
	public void updateRolePublishStatus(String roleId, String lccPublishStatus) {
		String hql = "UPDATE Role SET lccPublishStatus=:lccPublishStatus WHERE roleId=:roleId";
		Query qry = getSession().createQuery(hql).setParameter("lccPublishStatus", lccPublishStatus)
				.setParameter("roleId", roleId);
		qry.executeUpdate();
	}

}
