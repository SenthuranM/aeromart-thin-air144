/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.service.SecurityBDExternal;

/**
 * @author Thejaka
 * 
 */
@Remote
public interface SecurityServiceDelegateImpl extends SecurityBD, SecurityBDExternal {

}
