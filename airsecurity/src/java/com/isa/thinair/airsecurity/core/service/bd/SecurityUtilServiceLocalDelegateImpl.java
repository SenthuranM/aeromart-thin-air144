/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airsecurity.api.service.SecurityUtilBD;

/**
 * @author Thejaka
 */
@Local
public interface SecurityUtilServiceLocalDelegateImpl extends SecurityUtilBD {

}
