package com.isa.thinair.airsecurity.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airsecurity.api.dto.external.PrivilegeTO;
import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airsecurity.core.audit.AuditAirSecurity;
import com.isa.thinair.airsecurity.core.persistence.dao.PrivilegeDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.RoleDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;

/**
 * Core functionality wrappers for external clients
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class SecurityExternalBL {

	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(SecurityExternalBL.class);

	/**
	 * Returns PrivilegeDAO instance
	 * 
	 * @return
	 */
	private static PrivilegeDAO lookupPrivilegeDAO() {
		return (PrivilegeDAO) AirSecurityUtil.getInstance().getLocalBean("PrivilegeDAOImplProxy");
	}

	/**
	 * Returns RoleDAO instance
	 * 
	 * @return
	 */
	private static RoleDAO lookupRoleDAO() {
		return (RoleDAO) AirSecurityUtil.getInstance().getLocalBean("RoleDAOImplProxy");
	}

	/**
	 * Returns UserDAO instance
	 * 
	 * @return
	 */
	private static UserDAO lookupUserDAO() {
		return (UserDAO) AirSecurityUtil.getInstance().getLocalBean("UserDAOImplProxy");
	}

	/**
	 * Returns User For Save Or Update
	 * 
	 * @param userTO
	 * @return
	 * @throws ModuleException
	 */
	public static User getUserForSaveOrUpdate(UserTO userTO) throws ModuleException {
		return new User(userTO);
	}

	/**
	 * Returns User For Save Or Update
	 * 
	 * @param userTO
	 * @return
	 * @throws ModuleException
	 */
	public static User getUserForSaveOrUpdate(UserTO userTO, String channel) throws ModuleException {
		User user = new User(userTO);
		user.setServiceChannel(channel);
		return user;
	}

	/**
	 * Returns Role For Save Or Update
	 * 
	 * @param roleTO
	 * @return
	 * @throws ModuleException
	 */
	public static Role getRoleForSaveOrUpdate(RoleTO roleTO) throws ModuleException {
		return new Role(roleTO);
	}

	/**
	 * Returns UserTO
	 * 
	 * @param userId
	 * @param loadPrivileges
	 * @return
	 * @throws ModuleException
	 */
	public static UserTO getUserTO(String userId, boolean loadPrivileges) throws ModuleException {
		String channels[] = { Constants.EXTERNAL_CHANNEL, Constants.BOTH_CHANNELS };
		User user = lookupUserDAO().getUser(userId, loadPrivileges, channels);

		if (user != null) {
			CryptoServiceBD cryptoServiceBD = AirSecurityUtil.getCryptoServiceBD();
			user.setPassword(cryptoServiceBD.decrypt(user.getPassword()));

			return user.toUserTO();
		} else {
			throw new ModuleException("module.hibernate.objectnotfound", "airsecurity.desc");
		}
	}

	/**
	 * Removes User
	 * 
	 * @param userId
	 * @param version
	 * @throws ModuleException
	 */
	public static void removeUser(String userId, long version, UserPrincipal userPrincipal) throws ModuleException {
		String channels[] = { Constants.EXTERNAL_CHANNEL, Constants.BOTH_CHANNELS };
		User user = lookupUserDAO().getUser(userId, channels);
		if (user == null) {
			log.error("Removing user for user object is failed. User not found!");
			throw new ModuleException("module.delete.error", "airsecurity.desc");
		} else {
			if (user.getVersion() != version) {
				throw new ModuleException("module.hibernate.staledata", "airsecurity.desc");
			}

			for (Iterator<Role> itRole = user.getRoles().iterator(); itRole.hasNext();) {
				Role role = (Role) itRole.next();

				if (BeanUtils.nullHandler(role.getServiceChannel()).equals(Constants.INTERNAL_CHANNEL)) {
					throw new ModuleException("module.constraint.childrecord", "airsecurity.desc");
				}
			}

			lookupUserDAO().removeUser(user);
			AuditAirSecurity.doAudit(user.getUserId(), AuditAirSecurity.USER, userPrincipal);
		}
	}

	/**
	 * Remove Role
	 * 
	 * @param roleId
	 * @param version
	 * @param userPrincipal
	 * @throws ModuleException
	 */
	public static void removeRole(String roleId, long version, UserPrincipal userPrincipal) throws ModuleException {
		Role role = lookupRoleDAO().getRole(roleId, Constants.EXTERNAL_CHANNEL);
		if (role == null) {
			log.error("Removing role for role object is failed. Role not found!");
			throw new ModuleException("module.delete.error", "airsecurity.desc");
		} else {
			if (role.getVersion() != version) {
				throw new ModuleException("module.hibernate.staledata", "airsecurity.desc");
			} else {
				lookupRoleDAO().removeRole(role);
				AuditAirSecurity.doAudit(String.valueOf(roleId), AuditAirSecurity.ROLE, userPrincipal);
			}
		}
	}

	/**
	 * Return PrivilegeTOs for Role Id(s)
	 * 
	 * @param colRoleIds
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Collection<PrivilegeTO>> getPrivilegeTOsForRoleIDs(Collection<String> colRoleIds) throws ModuleException {
		String serviceChannel = Constants.EXTERNAL_CHANNEL;
		Map<String, Collection<PrivilegeTO>> finalExtPrivilegeMap = new HashMap<String, Collection<PrivilegeTO>>();
		Map<String, Collection<Privilege>> privilegeMap = lookupRoleDAO().getPrivilegesForRoleIds(colRoleIds, serviceChannel);
		Collection<Privilege> colPrivilege;
		Collection<PrivilegeTO> colPrivilegeTO;
		Privilege privilege;
		String roleId;

		for (Iterator<String> itr = privilegeMap.keySet().iterator(); itr.hasNext();) {
			roleId = (String) itr.next();
			colPrivilege = (Collection<Privilege>) privilegeMap.get(roleId);

			colPrivilegeTO = new ArrayList<PrivilegeTO>();

			for (Iterator<Privilege> iter = colPrivilege.iterator(); iter.hasNext();) {
				privilege = (Privilege) iter.next();
				colPrivilegeTO.add(privilege.toPrivilegeTO());
			}

			finalExtPrivilegeMap.put(roleId, colPrivilegeTO);
		}

		return finalExtPrivilegeMap;
	}

	/**
	 * Returns UserTO(s) for Privilege Id(s)
	 * 
	 * @param colPrivilegeIds
	 * @param loadPrivileges
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Collection<UserTO>> getUserTOs(Collection<String> colPrivilegeIds, boolean loadPrivileges) throws ModuleException {
		CryptoServiceBD cryptoServiceBD = AirSecurityUtil.getCryptoServiceBD();
		Map<String, Collection<String>> mapUserIdsForPrivilegeId = lookupUserDAO().getUserIds(colPrivilegeIds);
		Map<String, Collection<UserTO>> mapUserTOsForPrivilegeId = new HashMap<String, Collection<UserTO>>();
		Map<String, User> mapUserIdForUser = new HashMap<String, User>();
		Collection<String> colAllUserIds = new ArrayList<String>();

		String privilegeId;
		String userId;
		User user;
		Collection<String> colUserIds;
		Collection<UserTO> colUserTO;

		for (Iterator<String> iter = mapUserIdsForPrivilegeId.keySet().iterator(); iter.hasNext();) {
			privilegeId = (String) iter.next();
			colUserIds = (Collection<String>) mapUserIdsForPrivilegeId.get(privilegeId);
			colAllUserIds.addAll(colUserIds);
		}

		Collection<User> colUser = lookupUserDAO().getUsers(colAllUserIds, loadPrivileges);
		for (Iterator<User> iter = colUser.iterator(); iter.hasNext();) {
			user = (User) iter.next();
			user.setPassword(cryptoServiceBD.decrypt(user.getPassword()));
			mapUserIdForUser.put(user.getUserId(), user);
		}

		for (Iterator<String> iter = mapUserIdsForPrivilegeId.keySet().iterator(); iter.hasNext();) {
			privilegeId = (String) iter.next();
			colUserIds = (Collection<String>) mapUserIdsForPrivilegeId.get(privilegeId);

			colUserTO = new ArrayList<UserTO>();

			for (Iterator<String> iterator = colUserIds.iterator(); iterator.hasNext();) {
				userId = (String) iterator.next();
				user = (User) mapUserIdForUser.get(userId);
				colUserTO.add(user.toUserTO());
			}

			mapUserTOsForPrivilegeId.put(privilegeId, colUserTO);
		}

		return mapUserTOsForPrivilegeId;
	}

	/**
	 * Search RoleTO(s) for a given criteria
	 * 
	 * @param criteria
	 * @param sartIndex
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static Page searchRoleTOs(List<ModuleCriterion> criteria, int sartIndex, int noRecs, List<String> orderByFieldList) throws ModuleException {

		// Set criterian - Service channel
		ModuleCriterion MCchannel = new ModuleCriterion();
		MCchannel.setCondition(ModuleCriterion.CONDITION_EQUALS);
		MCchannel.setFieldName("serviceChannel");
		String servChannel = Constants.EXTERNAL_CHANNEL;
		List<String> value = new ArrayList<String>();
		value.add(servChannel);
		MCchannel.setValue(value);
		criteria.add(MCchannel);

		Page page = lookupRoleDAO().searchRoles(criteria, sartIndex, noRecs, orderByFieldList);
		Collection<RoleTO> colRoleTO = new ArrayList<RoleTO>();
		Role role;
		for (Iterator<Role> iter = page.getPageData().iterator(); iter.hasNext();) {
			role = (Role) iter.next();
			colRoleTO.add(role.toRoleTO());
		}

		page.setPageData(colRoleTO);

		return page;
	}

	/**
	 * Search UserTO(s) for a given criteria
	 * 
	 * @param userSearchDTO
	 * @param startIndex
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static Page searchUserTOs(UserSearchDTO userSearchDTO, int startIndex, int noRecs) throws ModuleException {
		CryptoServiceBD cryptoServiceBD = AirSecurityUtil.getCryptoServiceBD();
		String channel[] = { Constants.EXTERNAL_CHANNEL, Constants.BOTH_CHANNELS };
		userSearchDTO.setServiceChannel(channel);
		Page page = lookupUserDAO().searchUsers(userSearchDTO, startIndex, noRecs);
		Collection<UserTO> colUserTOs = new ArrayList<UserTO>();
		User user;

		for (Iterator<User> iter = page.getPageData().iterator(); iter.hasNext();) {
			user = (User) iter.next();
			user.setPassword(cryptoServiceBD.decrypt(user.getPassword()));
			colUserTOs.add(user.toUserTO());
		}

		page.setPageData(colUserTOs);
		return page;
	}

	/**
	 * Return Active RoleTO(s)
	 * 
	 * @param channel
	 *            TODO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<RoleTO> getActiveRoleTOs(String channel) throws ModuleException {
		Collection<Role> colRoles = lookupRoleDAO().getActiveRoles(channel);
		Collection<RoleTO> colRoleTOs = new ArrayList<RoleTO>();
		Role role;

		for (Iterator<Role> iter = colRoles.iterator(); iter.hasNext();) {
			role = (Role) iter.next();
			colRoleTOs.add(role.toRoleTO());
		}

		return colRoleTOs;
	}

	/**
	 * Return PrivilegeTO(s)
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<PrivilegeTO> getPrivilegeTOs() throws ModuleException {
		String servChannel = Constants.EXTERNAL_CHANNEL;
		Collection<Privilege> colPrivilege = lookupPrivilegeDAO().getPrivileges(servChannel);
		Collection<PrivilegeTO> colPrivilegeTO = new ArrayList<PrivilegeTO>();
		Privilege privilege;

		for (Iterator<Privilege> iter = colPrivilege.iterator(); iter.hasNext();) {
			privilege = (Privilege) iter.next();
			colPrivilegeTO.add(privilege.toPrivilegeTO());
		}

		return colPrivilegeTO;
	}

	public static String getNextSeqNoForRole() throws ModuleException {
		return lookupRoleDAO().getNextSeqNumberRole();
	}
}
