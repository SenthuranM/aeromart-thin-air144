package com.isa.thinair.airsecurity.core.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.api.utils.AirSecurityInternalConstants;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class CheckRolesBL {

	static GlobalConfig globalConfig = AirSecurityUtil.getGlobalConfig();

	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(CheckRolesBL.class);

	/**
	 * Method used to check user for existing user. If exist this will audit and
	 * track the role changes.
	 * 
	 * @param user
	 * @return
	 * @throws ModuleException
	 */
	public static HashMap<String, Object> checkUser(User user,
			String loggedInUser) throws ModuleException {

		User existingUser = (User) lookupUserDAO().getUser(user.getUserId());
		String oldPassword = "";
		boolean blnChangePass = false;
		String changedContent = "";

		// Existing editable user or new user
		if (existingUser != null) {
			// Start - Temp fix for setting CREATED_BY and CREATED_DATE
			if (user.getCreatedBy() == null || "".equals(user.getCreatedBy())) {
				user.setCreatedBy(existingUser.getCreatedBy());
			}
			if (user.getCreatedDate() == null
					|| "".equals(user.getCreatedDate())) {
				user.setCreatedDate(existingUser.getCreatedDate());
			}
			// End - Temp fix for setting CREATED_BY and CREATED_DATE

			// Existing editable user
			if (user.getVersion() != -1) {
				trackRoleChanges(existingUser, user, loggedInUser);
			}

			if (user.getPassword() != null) {
				blnChangePass = true;
				oldPassword = existingUser.getPassword();
				user.setPassword(getEncryptedString(user.getPassword()));
			} else {
				user.setPassword(existingUser.getPassword());
			}

		} else {
			// User Not Found
			if (user.getPassword() != null) {
				// New user
				blnChangePass = true;
				user.setPassword(getEncryptedString(user.getPassword()));
			}
		}

		lookupUserDAO().evictUser(existingUser);

		LinkedHashMap<String, Object> userData = new LinkedHashMap<String, Object>();
		userData.put("User", user);
		userData.put("oldpasswd", oldPassword);
		userData.put("blnChangePass", blnChangePass);

		if (existingUser != null) {
			changedContent = createChangedContent(existingUser, user, userData);
		}
		userData.put("changedContent", changedContent);

		return userData;
	}

	private static String createChangedContent(User existingUser, User user,
			LinkedHashMap<String, Object> userData) {

		String changedContent = "";
		String separator = ",";

		if (!existingUser.getDisplayName().equals(user.getDisplayName())) {
			changedContent += "display_name changed from "
					+ existingUser.getDisplayName() + " to "
					+ user.getDisplayName() + separator;
		}
		if (!existingUser.getEmailId().equals(user.getEmailId())) {
			changedContent += "email_id changed from "
					+ existingUser.getEmailId() + " to " + user.getEmailId()
					+ separator;
		}
		if (!existingUser.getThemeCode().equals(user.getThemeCode())) {
			changedContent += "theme_code changed from "
					+ existingUser.getThemeCode() + " to "
					+ user.getThemeCode() + separator;
		}
		if (!existingUser.getAgentCode().equals(user.getAgentCode())) {
			changedContent += "agent_code changed from "
					+ existingUser.getAgentCode() + " to "
					+ user.getAgentCode() + separator;
		}
		if (!existingUser.getStatus().equals(user.getStatus())) {
			changedContent += "status changed from " + existingUser.getStatus()
					+ " to " + user.getStatus() + separator;
		}
		if (!existingUser.getAdminUserCreateStatus().equals(
				user.getAdminUserCreateStatus())) {

			changedContent += "create_admin_users changed from "
					+ getRadioButtonSelectedVal(
							existingUser.getAdminUserCreateStatus(), 1)
					+ " to "
					+ getRadioButtonSelectedVal(
							user.getAdminUserCreateStatus(), 1) + separator;
		}
		if (!existingUser.getNormalUserCreateStatus().equals(
				user.getNormalUserCreateStatus())) {
			changedContent += "create_agent_user changed from "
					+ getRadioButtonSelectedVal(
							existingUser.getNormalUserCreateStatus(), 2)
					+ " to "
					+ getRadioButtonSelectedVal(
							user.getNormalUserCreateStatus(), 2) + separator;
		}
		if (user.getFfpNumber() == null) {
			user.setFfpNumber("");
		}
		if (existingUser.getFfpNumber() == null
				|| user.getFfpNumber().equals("")) {
			if (existingUser.getFfpNumber() == null
					&& (!user.getFfpNumber().equals(""))) {
				changedContent += "ffp_number changed from " + "none" + " to "
						+ user.getFfpNumber() + separator;
			} else if (user.getFfpNumber().equals("")
					&& existingUser.getFfpNumber() != null) {
				changedContent += "ffp_number changed from "
						+ existingUser.getFfpNumber() + " to " + "none"
						+ separator;
			}
		} else {
			if (!existingUser.getFfpNumber().equals(user.getFfpNumber())) {
				changedContent += "ffp_number changed from "
						+ existingUser.getFfpNumber() + " to "
						+ user.getFfpNumber() + separator;
			}
		}
		if (((Boolean) userData.get("blnChangePass")).booleanValue()) {
			changedContent += "password resetted" + separator;
		}

		return changedContent;
	}

	private static String getRadioButtonSelectedVal(String optionValue,
			int userOption) {
		String returnTask = "";
		String ownerStr = "";

		if (userOption == 1) {
			ownerStr = "CREATE ADMIN ";
		} else {
			ownerStr = "CREATE AGENT";
		}

		if (optionValue.equals("A")) {
			returnTask = "'" + ownerStr + " ANY'";
		}
		if (optionValue.equals("B")) {
			returnTask = "'" + ownerStr + " OWNER' & '" + ownerStr
					+ " REPORTING'";
		}
		if (optionValue.equals("O")) {
			returnTask = "'" + ownerStr + " OWNER'";
		}
		if (optionValue.equals("R")) {
			returnTask = "'" + ownerStr + " REPORTING'";
		}
		if (optionValue.equals("N")) {
			returnTask = "'DISABLE " + ownerStr + "'";
		}
		return returnTask;
	}

	/**
	 * Extract roles and process them for any changes. If change exist send
	 * email.
	 * 
	 * @param existingUser
	 * @param newUser
	 */
	private static void trackRoleChanges(User existingUser, User newUser,
			String loggedInUser) throws ModuleException {

		Set<String> exSet = breakroles(existingUser.getRoles());
		Set<String> newSet = breakroles(newUser.getRoles());
		Set<String> removedRoles = new TreeSet<String>();
		Set<String> addedRoles = new TreeSet<String>();

		// No roles assigned
		if (exSet.isEmpty() && newSet.isEmpty())
			log.debug("[CheckRolesBL::trackRoleChanges] No roles assigned");
		// If roles exist
		else {
			// check removed roles
			removedRoles = compare(exSet, newSet);
			// check added roles
			addedRoles = compare(newSet, exSet);

			// If no change in roles
			if (removedRoles.isEmpty() && addedRoles.isEmpty()) {
				log.debug("[CheckRolesBL::trackRoleChanges] No role change");
			} else {
				// Role changes exist
				log.debug("[CheckRolesBL::trackRoleChanges] Roles changed");
				// build role list
				Collection<Role> cRemovedRoles = getRoleSet(removedRoles,
						existingUser.getRoles());
				Collection<Role> cAddedRoles = getRoleSet(addedRoles,
						newUser.getRoles());
				alertChanges(cRemovedRoles, cAddedRoles, newUser, loggedInUser);
			}

		}

	}

	private static Collection<Role> getRoleSet(Set<String> changedRoles,
			Set<Role> roles) {
		ArrayList<Role> roleset = new ArrayList<Role>();
		Iterator<String> ItrChndRoles = changedRoles.iterator();
		while (ItrChndRoles.hasNext()) {
			String roleId = ItrChndRoles.next().toString();
			Iterator<Role> Itr = roles.iterator();
			while (Itr.hasNext()) {
				Role role = (Role) Itr.next();
				if (role.getRoleId().equals(roleId)) {
					roleset.add(role);
					break;
				}
			}
		}

		return roleset;
	}

	private static Set<String> breakroles(Collection<Role> roles) {
		Iterator<Role> Itr = roles.iterator();
		Set<String> set = new TreeSet<String>();
		while (Itr.hasNext()) {
			Role role = (Role) Itr.next();
			set.add(role.getRoleId());
		}
		return set;
	}

	private static void alertChanges(Collection<Role> removedRoles,
			Collection<Role> addedRoles, User newUser, String loggedInUser)
			throws ModuleException {
		String strEmail = null;
		String strEnable = globalConfig
				.getBizParam(SystemParamKeys.ALERT_ON_ROLE_CHANGE);
		if (strEnable.equals("Y")) {
			strEmail = globalConfig
					.getBizParam(SystemParamKeys.EMAIL_ADDRESS_FOR_ALERT_ON_ROLE_CHANGE);
			log.debug("Alerting for role changes to :" + strEmail);
			compileMail(removedRoles, addedRoles, strEmail, newUser,
					loggedInUser);

		}
	}

	public static void alertUserPrivilegeChanges(
			Map<String, String[]> mapCustomContents, UserPrincipal userPrinciple)
			throws ModuleException {
		String strEmail = null;
		String strEnable = globalConfig
				.getBizParam(SystemParamKeys.ALERT_ON_ROLE_CHANGE);
		if (strEnable.equals("Y")) {
			strEmail = globalConfig
					.getBizParam(SystemParamKeys.EMAIL_ADDRESS_FOR_ALERT_ON_ROLE_CHANGE);
			log.debug("Alerting privilege changes to :" + strEmail);
			compileMailToInformRolePrivilegeChanges(mapCustomContents,
					strEmail, userPrinciple);

		}
	}

	/**
	 * Return Set where all x's not found in y
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private static Set<String> compare(Set<String> x, Set<String> y) {
		Set<String> set = new TreeSet<String>();

		// No existing roles. Added new roles
		if (!(x.isEmpty())) {
			Iterator<String> xItr = x.iterator();
			while (xItr.hasNext()) {
				String temp = xItr.next().toString();
				if (!(y.contains(temp))) {
					set.add(temp);
				}
			}
		}

		return set;
	}

	/**
	 * Private method to generate a key.
	 * 
	 * @param toEncrypt
	 *            text to encrypt.
	 * @return the key.
	 * @throws ModuleException
	 */
	@SuppressWarnings("unused")
	private static String getDecryptedString(String toDecrypt)
			throws ModuleException {
		String key = null;
		CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) AirSecurityUtil
				.getCryptoServiceBD();

		key = cryptoServiceBD.decrypt(toDecrypt);

		return key;
	}

	/**
	 * Private method to generate a key.
	 * 
	 * @param toEncrypt
	 *            text to encrypt.
	 * @return the key.
	 * @throws ModuleException
	 */
	private static String getEncryptedString(String toEncrypt)
			throws ModuleException {
		String key = null;
		CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) AirSecurityUtil
				.getCryptoServiceBD();

		key = cryptoServiceBD.encrypt(toEncrypt);

		return key;
	}

	/**
	 * Returns UserDAO instance
	 * 
	 * @return
	 */
	private static UserDAO lookupUserDAO() {
		return (UserDAO) AirSecurityUtil.getInstance().getLocalBean(
				"UserDAOImplProxy");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void compileMail(Collection<Role> removedRoles,
			Collection<Role> addedRoles, String strEmail, User newUser,
			String loggedInUser) throws ModuleException {

		MessagingServiceBD messagingServiceBD = AirSecurityUtil
				.getMessagingServiceBD();

		// User Message
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(newUser.getDisplayName());
		userMessaging.setToAddres(strEmail);

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		TravelAgentBD travelAgentDelegate = null;

		travelAgentDelegate = (TravelAgentBD) AirSecurityUtil
				.getTravelAgentBD();
		log.debug("getTravelAgentBD is successfully executed "
				+ travelAgentDelegate);

		Agent oAgent = travelAgentDelegate.getAgent(newUser.getAgentCode());

		HashMap itineraryDataMap = new HashMap();
		itineraryDataMap.put("removedRoles", removedRoles);
		itineraryDataMap.put("addedRoles", addedRoles);
		itineraryDataMap.put("user", newUser);
		itineraryDataMap.put("agent", oAgent);
		itineraryDataMap.put("userLoggedIn", loggedInUser);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(itineraryDataMap);
		topic.setTopicName(AirSecurityInternalConstants.ROLE_CHANGE_EMAIL_TEMPLEATE);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void compileMailToInformRolePrivilegeChanges(
			Map<String, String[]> mapCustomContents, String strEmail,
			UserPrincipal userPrinciple) throws ModuleException {

		MessagingServiceBD messagingServiceBD = AirSecurityUtil
				.getMessagingServiceBD();

		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(userPrinciple.getUserId());
		userMessaging.setToAddres(strEmail);

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		TravelAgentBD travelAgentDelegate = null;
		travelAgentDelegate = (TravelAgentBD) AirSecurityUtil
				.getTravelAgentBD();
		Agent agent = travelAgentDelegate
				.getAgent(userPrinciple.getAgentCode());

		HashMap dataMap = new HashMap();

		Iterator keyIte = mapCustomContents.keySet().iterator();

		while (keyIte.hasNext()) {
			Object key = keyIte.next();
			dataMap.put(key.toString(),
					Arrays.asList((String[]) mapCustomContents.get(key)));
		}

		dataMap.put("user", userPrinciple.getUserId());
		dataMap.put("agent", agent);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(dataMap);
		topic.setTopicName(AirSecurityInternalConstants.ROLE_PRIVILEGE_CHANGE_EMAIL_TEMPLATE);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		messagingServiceBD.sendMessage(messageProfile);
	}

	@SuppressWarnings("rawtypes")
	public static Collection[] getRoleChanges(Set<Role> existingUserRoles,
			Set<Role> newUserRoles) throws ModuleException {
		Set<String> exSet = breakroles(existingUserRoles);
		Set<String> newSet = breakroles(newUserRoles);
		Set<String> removedRoles = new TreeSet<String>();
		Set<String> addedRoles = new TreeSet<String>();

		// No roles assigned
		if (exSet.isEmpty() && newSet.isEmpty())
			return new Collection[] { null, null };
		else {
			// check removed roles
			removedRoles = compare(exSet, newSet);
			// check added roles
			addedRoles = compare(newSet, exSet);

			// If no change in roles
			if (removedRoles.isEmpty() && addedRoles.isEmpty()) {
				return new Collection[] { null, null };
			} else {
				Collection<Role> cRemovedRoles = getRoleSet(removedRoles,
						existingUserRoles);
				Collection<Role> cAddedRoles = getRoleSet(addedRoles,
						newUserRoles);
				return new Collection[] { cRemovedRoles, cAddedRoles };
			}
		}
	}
}
