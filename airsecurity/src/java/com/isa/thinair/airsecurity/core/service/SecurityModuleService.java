/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.service;

import com.isa.thinair.airsecurity.api.service.AirSecurityService;
import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * 
 *         Security Module's service interface
 * @isa.module.service-interface module-name="airsecurity" description="security module"
 */
public class SecurityModuleService extends DefaultModule implements AirSecurityService {
}
