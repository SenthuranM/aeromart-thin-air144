/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airsecurity.core.remoting.ejb;

import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airsecurity.api.model.PrivilegeCategory;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.airsecurity.core.persistence.dao.PrivilegeDAO;
import com.isa.thinair.airsecurity.core.persistence.dao.UserDAO;
import com.isa.thinair.airsecurity.core.service.bd.SecurityUtilServiceDelegateImpl;
import com.isa.thinair.airsecurity.core.service.bd.SecurityUtilServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author Nasly
 */
@Stateless
@RemoteBinding(jndiBinding = "SecurityUtilService.remote")
@LocalBinding(jndiBinding = "SecurityUtilService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SecurityUtilServiceBean extends PlatformBaseSessionBean implements SecurityUtilServiceDelegateImpl,
		SecurityUtilServiceLocalDelegateImpl {
	private final Log log = LogFactory.getLog(getClass());

	public Collection<PrivilegeCategory> getPrivilegeCategories() throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeCategories();
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege categories is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	public PrivilegeCategory getPrivilegeCategory(String privilegeCategoryId) throws ModuleException {
		try {
			return lookupPrivilegeDAO().getPrivilegeCategory(privilegeCategoryId);
		} catch (CommonsDataAccessException e) {
			log.error("Getting privilege category is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), "airsecurity.desc");
		}
	}

	private PrivilegeDAO lookupPrivilegeDAO() {
		return (PrivilegeDAO) AirSecurityUtil.getInstance().getLocalBean("PrivilegeDAOImplProxy");
	}

	@SuppressWarnings("unused")
	private UserDAO lookupUserDAO() {
		return (UserDAO) AirSecurityUtil.getInstance().getLocalBean("UserDAOImplProxy");
	}
}
