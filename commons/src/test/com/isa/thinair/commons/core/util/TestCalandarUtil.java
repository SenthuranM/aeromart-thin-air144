package com.isa.thinair.commons.core.util;

import java.util.Date;

public class TestCalandarUtil {
	public static void main(String[] args) {
		testIsInEndBuffer();
	}

	private static void testIsInEndBuffer() {
		int start = 60;
		int end = 30;
		Date date = CalanderUtil.add(new Date(), 0, 0, 0, 0, 31, 0);
		
		System.out.println(CalanderUtil.isInEndBuffer(date, start, end));
	}
}
