package com.isa.thinair.commons.core.dao;


import java.util.Iterator;
import java.util.Map;


import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestCommonsDAO extends PlatformTestCase {
	public void testSSRApplicableAirports() {
		Map<String, Boolean> ssrAirpotMap = getSSRChargeJdbcDAO().getSSRApplicableAirportsMap();
		
		System.out.println("SSR ID AIRPORT AVAILABILITY");
		
		for (Iterator iterator = ssrAirpotMap.keySet().iterator(); iterator.hasNext();) {
			String ssrIdAprCOde = (String) iterator.next();
			
			System.out.println(ssrIdAprCOde + ":" + ssrAirpotMap.get(ssrIdAprCOde));
		}
		System.out.println();
	}
	
	private CommonsDAO getSSRChargeJdbcDAO() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO;
	}
}
