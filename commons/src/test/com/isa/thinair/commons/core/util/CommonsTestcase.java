package com.isa.thinair.commons.core.util;

import junit.framework.TestCase;

import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class CommonsTestcase extends TestCase {
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		System.setProperty("repository.home", "c:/isaconfig-test");
		ModuleFramework.startup();
	
		try {
			new ClientLoginModule().login("SYSTEM","password", "c:/isaconfig-test/client/config/client_jass_login.config", "JbossClientLogin");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
