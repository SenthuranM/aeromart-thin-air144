package com.isa.thinair.commons.core.util;

import java.io.Serializable;

/**
 * generic implementation of pair of values
 * 
 * @author malaka
 * 
 * @param <L>
 * @param <R>
 */
public class Pair<L, R> implements Serializable {

	/** Serialization version */
	private static final long serialVersionUID = 2385918890077093841L;

	private final L left;
	private final R right;

	private Pair(L left, R right) {
		super();
		this.left = left;
		this.right = right;
	}

	/**
	 * Static creation method for a Pair<L, R>.
	 * 
	 * @param <L>
	 * @param <R>
	 * @param left
	 * @param right
	 * @return Pair<L, R>(left, right)
	 */
	public static <L, R> Pair<L, R> of(L left, R right) {
		return new Pair<L, R>(left, right);
	}

	/**
	 * @return the left
	 */
	public L getLeft() {
		return left;
	}

	/**
	 * @return the right
	 */
	public R getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		int hashLeft = ((left == null) ? 0 : left.hashCode());
		int hashRight = ((right == null) ? 0 : right.hashCode());
		return (hashLeft + hashRight) * hashLeft + hashRight;
	}

	@Override
	public boolean equals(Object other) {

		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Pair) {
			Pair otherPair = (Pair) other;
			return ((this.left == otherPair.left || (this.left != null && otherPair.left != null && this.left
					.equals(otherPair.left))) && (this.right == otherPair.right || (this.right != null && otherPair.right != null && this.right
					.equals(otherPair.right))));
		}

		return false;

	}

	/**
	 * Returns a String representation of the Pair in the form: (L,R)
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(left);
		builder.append(",");
		builder.append(right);
		builder.append(")");
		return builder.toString();
	}

}
