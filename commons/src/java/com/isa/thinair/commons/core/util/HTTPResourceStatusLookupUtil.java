package com.isa.thinair.commons.core.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.constants.SystemParamKeys;

public class HTTPResourceStatusLookupUtil {

	private static final Log log = LogFactory.getLog(HTTPResourceStatusLookupUtil.class);
	private static HTTPResourceStatusLookupUtil instance = null;
	private static Map<String, ResourceLookupStatus> resourceStatus = new HashMap<String, ResourceLookupStatus>();
	private static Object lock = new Object();

	private static String CONTENT_PROP = "Content-Type";
	private static String CONTENT_PROP_VALUE = "image/gif";
	private static String RESOURCE_FOUND_STATUS_CODE = "200";

	public static HTTPResourceStatusLookupUtil getInstance() {
		if (instance == null) {
			synchronized (lock) {
				if (instance == null) {
					instance = new HTTPResourceStatusLookupUtil();
				}
			}
		}
		return instance;
	}

	public String availableResource(String resourceNamePrefix) {
		String resourceUrl = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ITINERARY_ADVERTISEMENT_IMAGE_URL);

		if (resourceUrl == null || "".equals(resourceUrl.trim())) {
			return "";
		}

		// String resourceUrl = "http://reservationsg9.isaaviations.com/ibe/images/$.jpg";
		String resourceFullURL = resourceUrl.replaceFirst("\\$", resourceNamePrefix);

		ResourceLookupStatus resourceLookupStatus = resourceStatus.get(resourceNamePrefix);

		if (resourceLookupStatus == null || resourceLookupStatus.isExpired()) {
			resourceLookupStatus = new ResourceLookupStatus(isHTTPResourceAvailable(resourceFullURL));
			resourceStatus.put(resourceNamePrefix, resourceLookupStatus);
		}

		if (resourceLookupStatus.isAvailable()) {
			return resourceFullURL;
		}

		return "";
	}

	private boolean isHTTPResourceAvailable(String URL) {

		HttpClient httpClient = new HttpClient();
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String contentType = "";

		if (globalConfig.getUseProxy()) {
			httpClient.getHostConfiguration().setProxy(globalConfig.getHttpProxy(), globalConfig.getHttpPort());
		}

		if (URL == null || URL.equals("")) {
			return false;
		}

		HttpMethod httpMethod = new HeadMethod(URL);
		boolean resourceAvailable = false;
		try {
			httpClient.executeMethod(httpMethod);
			Header header = httpMethod.getResponseHeader(CONTENT_PROP);
			int statusCode = httpMethod.getStatusCode();
			if (header != null) {
				contentType = header.getValue();
			}

			if (RESOURCE_FOUND_STATUS_CODE.equals(Integer.toString(statusCode))) {
				resourceAvailable = true;
			} else if (contentType.equals(CONTENT_PROP_VALUE)) {
				resourceAvailable = true;
			}

		} catch (HttpException e) {
			log.warn("HttpException Cannot connect to Resource Image" + e);
		} catch (IOException e) {
			log.warn("IOException Cannot connect to Resource Image" + e);
		}
		return resourceAvailable;
	}

	private static class ResourceLookupStatus {
		private boolean available = false;
		private long lastLookuTimestamp;
		private static long cacheExpInMillis = 60 * 60 * 100; // FIXME Take from global configuration file

		public ResourceLookupStatus(boolean isAvailable) {
			this.available = isAvailable;
			this.lastLookuTimestamp = System.currentTimeMillis();
		}

		public boolean isExpired() {
			return System.currentTimeMillis() - this.lastLookuTimestamp > cacheExpInMillis;
		}

		public boolean isAvailable() {
			return available;
		}
	}

}
