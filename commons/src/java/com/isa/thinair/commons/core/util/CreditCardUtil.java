package com.isa.thinair.commons.core.util;

/*
 *@author Chinthaka Weerakkody
 *
 */
public class CreditCardUtil {

	public static boolean validateCardNumberWithLuhnAlgorithm(String creditCardNumber) {
		int sumOdd = 0, sumEven = 0;
		String reverse = new StringBuffer(creditCardNumber).reverse().toString();
		for (int i = 0; i < reverse.length(); i++) {
			int digit = Character.digit(reverse.charAt(i), 10);
			if (i % 2 == 0) { // this is for odd digits, they are 1-indexed in the algorithm
				sumOdd += digit;
			} else { // add 2 * digit for 0-4, add 2 * digit - 9 for 5-9
				sumEven += 2 * digit;
				if (digit >= 5) {
					sumEven -= 9;
				}
			}
		}
		return (sumOdd + sumEven) % 10 == 0;
	}
}
