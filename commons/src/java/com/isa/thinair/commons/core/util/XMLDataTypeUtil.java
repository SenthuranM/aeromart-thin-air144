package com.isa.thinair.commons.core.util;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import com.isa.thinair.commons.api.exception.ModuleException;

public class XMLDataTypeUtil {

	public static XMLGregorianCalendar getXMLGregorianCalendar(Date date) throws ModuleException {
		if (date != null) {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeInMillis(date.getTime());

			try {
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				throw new ModuleException("commons.invalid.date");
			}
		} else {
			return null;
		}
	}

	public static Date getDate(XMLGregorianCalendar xmlCal) {
		return xmlCal.toGregorianCalendar().getTime();
	}

	public static boolean isLessThan(Date first, Date second) {
		return (first.compareTo(second) < 0) ? true : false;
	}

	public static boolean isLessThanOrEqual(Date first, Date second) {
		return (first.compareTo(second) <= 0) ? true : false;
	}

	public static Duration getDuration(long miliseconds) throws DatatypeConfigurationException {
		return DatatypeFactory.newInstance().newDuration(miliseconds);
	}
}
