package com.isa.thinair.commons.core.constants;

/**
 * Identify the AccelAero Client codes. Used to identify the client for clint specific processing.
 */
public interface AccelAeroClients {

	public static final String AIR_ARABIA = "AA";

	public static final String MIHIN_LANKA = "ML";

	public static final String TMA = "TM";

	public static final String FLYYETI = "FY";

	public static final String AIR_ARABIA_GROUP = "GA";

}
