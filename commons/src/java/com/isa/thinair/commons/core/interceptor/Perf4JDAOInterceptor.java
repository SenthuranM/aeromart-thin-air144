package com.isa.thinair.commons.core.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.perf4j.StopWatch;
import org.perf4j.log4j.Log4JStopWatch;

import com.isa.thinair.commons.core.util.SystemPropertyUtil;

/**
 * @author Nilindra Fernando
 */
public class Perf4JDAOInterceptor implements MethodInterceptor {

	private static final boolean isPer4JEnable = SystemPropertyUtil.isPerf4JEnable();

	public Object invoke(MethodInvocation context) throws Throwable {

		if (isPer4JEnable) {
			String className = Perf4JBaseUtils.getClassName(context.getThis());
			String methodName = context.getMethod().getName();
			StopWatch stopWatch = new Log4JStopWatch("DAO-" + className + "-" + methodName);
			Object result = context.proceed();
			stopWatch.stop();
			return result;
		} else {
			return context.proceed();
		}
	}
}
