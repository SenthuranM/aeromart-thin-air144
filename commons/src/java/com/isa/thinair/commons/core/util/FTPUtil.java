package com.isa.thinair.commons.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.isa.thinair.platform.api.constants.PlatformConstants;

public class FTPUtil extends FTPClient {
	private final static Log log = LogFactory.getLog(FTPUtil.class);

	/** A convenience method for connecting and logging in */
	public boolean connectAndLogin(String host, String userName, String password) throws IOException, UnknownHostException,
			FTPConnectionClosedException {
		boolean success = false;
		connect(host);
		int reply = getReplyCode();
		if (log.isDebugEnabled()) {
			log.debug("Reply code from FTP : " + reply);
		}
		if (FTPReply.isPositiveCompletion(reply)) {
			success = login(userName, password);

			if (log.isDebugEnabled()) {
				log.debug("Log in Reply from FTP : " + success);
			}
		}
		if (!success) {
			disconnect();
		}
		return success;
	}

	/**
	 * Turn passive transfer mode on or off. If Passive mode is active, a PASV command to be issued and interpreted
	 * before data transfers; otherwise, a PORT command will be used for data transfers. If you're unsure which one to
	 * use, you probably want Passive mode to be on.
	 */
	public void setPassiveMode(boolean setPassive) {
		if (setPassive) {
			enterLocalPassiveMode();
		} else {
			enterLocalActiveMode();
		}
	}

	/** Use ASCII mode for file transfers */
	public boolean ascii() throws IOException {
		return setFileType(FTP.ASCII_FILE_TYPE);
	}

	/** Use Binary mode for file transfers */
	public boolean binary() throws IOException {
		return setFileType(FTP.BINARY_FILE_TYPE);
	}

	/**
	 * Download a file from the server, and save it to the specified local file
	 */
	public boolean downloadFile(String serverFile, String localFile) throws IOException, FTPConnectionClosedException {
		boolean result = false;
		try{
		FileOutputStream out = new FileOutputStream(localFile);
		result = retrieveFile(serverFile, out);
		out.close();
		
		}catch (Exception e) {
				log.error("**************************************************");
				log.error("download attachment failed [" + e.getMessage() + "]");
				log.error("**************************************************");
			}
			
		return result;
	}

	/**
	 * Upload a file to the server
	 */
	public boolean uploadFile(String localFile, String serverFile) throws IOException, FTPConnectionClosedException {
		boolean result = false;
		try{
		log.info("=====Before Reading file " + localFile);
		FileInputStream in = new FileInputStream(localFile);
		log.info("=====After Reading file " + (in.available()));
		setFileTransferMode(BINARY_FILE_TYPE);
		log.info("=====Total Available Bytes: " + (in.available()));

		 result = storeFile(serverFile, in);
		 in.close();
		 log.info("===========getReplyCode():= " + getReplyCode() + "  --------- getReplyString():= " + getReplyString()
			+ "-------replyCode:=+replyCode+=========");
		 
		} catch (Exception e) {
			log.error(e);
			log.info("=========================FTP Storing error============================");
		}
		return result;
	}

	/**
	 * Get the list of files in the current directory as a Vector of Strings (excludes subdirectories)
	 */
	public Vector<String> listFileNames() throws IOException, FTPConnectionClosedException {
		FTPFile[] files = listFiles();
		Vector<String> v = new Vector<String>();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].isDirectory()) {
				v.addElement(files[i].getName());
			}
		}
		return v;
	}

	/**
	 * Get the list of files in the current directory as a single Strings, delimited by \n (char '10') (excludes
	 * subdirectories)
	 */
	public String listFileNamesString() throws IOException, FTPConnectionClosedException {
		return vectorToString(listFileNames(), "\n");
	}

	/**
	 * Get the list of subdirectories in the current directory as a Vector of Strings (excludes files)
	 */
	public Vector<String> listSubdirNames() throws IOException, FTPConnectionClosedException {
		FTPFile[] files = listFiles();
		Vector<String> v = new Vector<String>();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) {
				v.addElement(files[i].getName());
			}
		}
		return v;
	}

	/**
	 * Get the list of subdirectories in the current directory as a single Strings, delimited by \n (char '10')
	 * (excludes files)
	 */
	public String listSubdirNamesString() throws IOException, FTPConnectionClosedException {
		return vectorToString(listSubdirNames(), "\n");
	}

	/** Convert a Vector to a delimited String */
	private String vectorToString(Vector<String> v, String delim) {
		StringBuffer sb = new StringBuffer();
		String s = "";
		for (int i = 0; i < v.size(); i++) {
			sb.append(s).append((String) v.elementAt(i));
			s = delim;
		}
		return sb.toString();
	}

	public boolean uploadAttachmentWithOutFTP(String attachmentName, byte[] content) {
		log.info("================================================================");
		log.info("============Start Uploading Attachments Without FTP ========================");
		log.info("================================================================");
		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + attachmentName;
		File attachment = new File(path);
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(attachment);
			fileOutputStream.write(content);
			fileOutputStream.flush();
			fileOutputStream.close();
			log.info("==Write to local file path: " + path + " Size: " + content.length + "  ========================");
			return true;
		} catch (Exception e) {
			log.error("**************************************************");
			log.error("upload attachment failed [" + e.getMessage() + "]");
			log.error("**************************************************");
		}
		return false;
	}

	/**
	 * 
	 * @param attachmentName
	 * @param content
	 * @param serverProperty
	 * @return
	 */
	public boolean uploadAttachment(String attachmentName, byte[] content, FTPServerProperty serverProperty) {
		log.info("================================================================");
		log.info("============Start Uploading Attachments ========================");
		log.info("================================================================");
		boolean uploaded = false;
		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + attachmentName;
		
		File attachment = new File(path);

		try {
			FileOutputStream fileOutputStream = new FileOutputStream(attachment);
			fileOutputStream.write(content);
			fileOutputStream.flush();
			fileOutputStream.close();
			log.info("==Write to local file path: " + path + " Size: " + content.length + "  ========================");

			if (connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(), serverProperty.getPassword())) {
				binary();
				log.info("== Start uploading : localfile " + path + "     Server File: " + attachmentName + "=========");
				setPassiveMode(CommonsServices.getGlobalConfig().isFtpEnablePassiveMode());
				uploaded = uploadFile(path, attachmentName);
				log.debug("== Finished uploading status: " + uploaded + " =================================");
				if (uploaded) {
					if (!listFileNames().contains(attachmentName)) {
						uploaded = false;
					}
				}
			}
			log.info("================================================================");
			log.info("============Finished Uploading Attachments ========================");
			log.info("================================================================");

		} catch (Exception e) {
			log.error("**************************************************");
			log.error("upload attachment failed [" + e.getMessage() + "]");
			log.error("**************************************************");
		} finally {
			try {
				if (isConnected()) {
					disconnect();
				}
			} catch (Exception e) {
				log.error("Disconnecting FTP connection failed.", e);
			}
		}
		return uploaded;
	}

	/**
	 * 
	 * @param attachmentName
	 * @param serverProperty
	 * @return
	 */
	public boolean downloadAttachment(String attachmentName, FTPServerProperty serverProperty) {
		boolean downloaded = false;
		String path = PlatformConstants.getAbsAttachmentsPath() + "/" + attachmentName;
		File local  = new File(path);
		try {
			if(local.exists()) {
				downloaded = local.exists();
			}
			else if (connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(), serverProperty.getPassword())) {
				binary();
				downloaded = downloadFile(attachmentName, path);
				if (downloaded) {
					downloaded = local.exists();
				}
			}
			log.info("================================================================");
			log.info("============Downloaded Attachments for Email ========================");
			log.info("================================================================");
		} catch (Exception e) {
			log.error("download attachment failed [" + e.getMessage() + "]");
		} finally {
			try {
				if (isConnected()) {
					disconnect();
				}
			} catch (Exception e) {
				log.error("Disconnecting FTP connection failed.", e);
			}
		}
		return downloaded;
	}
	
	public boolean uploadZipFile(String remoteFile, String localFile, FTPServerProperty serverProperty ){
		
		boolean uploaded = false;
		
		try {
			if (connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(), serverProperty.getPassword())) {
				binary();
				log.info("== Start uploading : localfile " + localFile + "     Server File: " + remoteFile + "=========");
				setPassiveMode(CommonsServices.getGlobalConfig().isFtpEnablePassiveMode());
				uploaded = uploadFile(localFile, remoteFile);
				log.debug("== Finished uploading status: " + uploaded + " =================================");
				if (uploaded) {
					if (!listFileNames().contains(remoteFile)) {
						uploaded = false;
					}
				}
			} else {
			    uploaded = false;
				log.info("Could not connect to FTP server");
			}
		} catch (UnknownHostException e) {
			log.error("Could not connect to FTP server",e);
		} catch (FTPConnectionClosedException e) {
			log.error("Could not connect to FTP server",e);
		} catch (IOException e) {
			log.error("Could not connect to FTP server",e);
		} 
		
		return uploaded;
	}

	/**
	 * @param args
	 */
	/*
	 * public static void main(String[] args) { try { FTPUtil ftp = new FTPUtil(); String serverName = "KITHALAGAMA";
	 * String userName = "anonymous"; String password = ""; if (ftp.connectAndLogin(serverName, userName, password)) {
	 * System.out.println("Connected to " + serverName); try { System.out.println("Welcome message:\n" +
	 * ftp.getReplyString()); System.out.println("********************************************************");
	 * System.out.println("Current Directory:\n" + ftp.printWorkingDirectory());
	 * System.out.println("********************************************************"); ftp.setPassiveMode(true);
	 * System.out.println("Files in this directory:\n" + ftp.listFileNamesString());
	 * System.out.println("********************************************************");
	 * System.out.println("Subdirectories in this directory:\n" + ftp.listSubdirNamesString());
	 * System.out.println("********************************************************"); ftp.binary();
	 * 
	 * boolean downloaded = ftp.downloadFile("aavino.txt", "C:\\aavino.txt"); System.out.println("\nFile Downloaded : "
	 * + downloaded + "\n"); System.out.println("********************************************************");
	 * ftp.binary(); boolean uploaded = ftp.uploadFile("C:\\alertparams.txt", "testing_vino.txt");
	 * System.out.println("\nFile Uploaded : " + uploaded + "\n"); } catch (Exception ftpe) { ftpe.printStackTrace(); }
	 * finally { ftp.logout(); ftp.disconnect(); } } else { System.out.println("Unable to connect to" + serverName); }
	 * System.out.println("Finished"); } catch(Exception e) { e.printStackTrace(); } }
	 */
}
