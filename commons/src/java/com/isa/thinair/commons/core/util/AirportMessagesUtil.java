package com.isa.thinair.commons.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessage;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;

public class AirportMessagesUtil {

	public static final String INCLUDE = "INCLUDE";
	public static final String EXCLUDE = "EXCLUDE";
	public static final String ANY = "ANY";
	public static final String MATCH_ANY = "***";

	public static void extractAirportMessageDisplayDTO(AirportFlightDetail airportFlightDetail,
			Map<Integer, AirportMessageDisplayDTO> requestedAirportMessagesMap,
			Collection<AirportMessage> airportMessages) {

		for (AirportMessage airportMessage : airportMessages) {
			Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
			boolean includeThisFlight = airportMessage.getInclusionFligths().size() > 0 ? airportMessage
					.getInclusionFligths().contains(airportFlightDetail.getFlightNumber()) : true;
			boolean journeyType = airportMessage.getDepArrType().equals(AirportMessagesUtil.ANY)
					|| airportFlightDetail.getTravelWay().equals(airportMessage.getDepArrType());
			// FIXME : will need a change for segment length > 7
			String anyOnd = AirportMessagesUtil.MATCH_ANY + "/" + AirportMessagesUtil.MATCH_ANY;
			String anyOutboundOnd = AirportMessagesUtil.MATCH_ANY + "/" + airportFlightDetail.getOnd().substring(4);
			String anyInboundOnd = airportFlightDetail.getOnd().substring(0, 3) + "/" + AirportMessagesUtil.MATCH_ANY;
			boolean includeThisOnd = airportMessage.getInclusionONDs().size() > 0 ? (airportMessage.getInclusionONDs()
					.contains(anyOnd)
					|| airportMessage.getInclusionONDs().contains(anyOutboundOnd)
					|| airportMessage.getInclusionONDs().contains(anyInboundOnd) || airportMessage.getInclusionONDs()
					.contains(airportFlightDetail.getOnd())) : true;
			boolean excludeThisOnd = airportMessage.getExclusionONDs().size() > 0 ? (airportMessage.getExclusionONDs()
					.contains(anyOnd)
					|| airportMessage.getExclusionONDs().contains(anyOutboundOnd)
					|| airportMessage.getExclusionONDs().contains(anyInboundOnd) || airportMessage.getExclusionONDs()
					.contains(airportFlightDetail.getOnd())) : false;
			if (airportMessage.getSalesChannels().contains(airportFlightDetail.getSalesChannel())
					&& !airportMessage.getExclusionFlights().contains(airportFlightDetail.getFlightNumber())
					&& currentDate.after(airportMessage.getTimestampFrom())
					&& currentDate.before(airportMessage.getTimestampTo())
					&& airportFlightDetail.getFlightDepDate().after(airportMessage.getTravelValidityFrom())
					&& airportFlightDetail.getFlightDepDate().before(airportMessage.getTravelValidityTo())
					&& journeyType && includeThisFlight && includeThisOnd && !excludeThisOnd) {

				if (!requestedAirportMessagesMap.containsKey(airportMessage.getAirportMessageId())) {
					AirportMessageDisplayDTO airportMessageDisplayDTO = new AirportMessageDisplayDTO();
					airportMessageDisplayDTO.setAirportCode(airportMessage.getAirport());
					airportMessageDisplayDTO.setAirportMessage(airportMessage.getMessage());
					airportMessageDisplayDTO.setAirportMsgId(airportMessage.getAirportMessageId());
					airportMessageDisplayDTO.setI18MessageKey(airportMessage.getI18nMessageKey());
					requestedAirportMessagesMap.put(airportMessage.getAirportMessageId(), airportMessageDisplayDTO);
				}

			}
		}
	}
}
