package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * 
 * @author Navod Ediriweera TODO: Review and merge if functionality is present elsewhere
 * @since 27Sep2010
 */
public class SubStationUtil {

	public static final String GROUND_STATION_PARENT_CODE_IDENTIFIER = "!";

	public static final String SUB_STATION_CODE_SEPERATOR = ":";

	public static final String SUB_STATION_DISPLAY_SEPERATOR = " -- ";

	private static final String BUS_SERVICE_LEGEND = "(Bus Stop) "; // TODO
																	// load
																	// from
																	// property
																	// file

	public static GlobalConfig globalConfig = (GlobalConfig) LookupServiceFactory.getInstance().getBean(
			PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);

	public static Boolean isPrimaryStationBelongingToGroundStationPresent(String segmentCode, String groundCode) {
		//String mainAirport = globalConfig.getMapPrimaryStationBySubStation().get(groundCode);
		// if (mainAirport == null)
		// return Boolean.FALSE;
		String connectedToStation = globalConfig.getMapTopLevelStationByConnectedStation().get(groundCode);
		if (connectedToStation == null)
			return Boolean.FALSE;
		String[] semgnetCodes = segmentCode.split("/");
		List<String> arrList = Arrays.asList(semgnetCodes);
		if (arrList.contains(connectedToStation)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public static List<String[]>[] getGroundSegmentCombo(String selectedOnd, boolean isAddGroundSegment) {

		List<String[]> availableGroundStationsFrom = new ArrayList<String[]>();
		List<String[]> availableGroundStationsTo = new ArrayList<String[]>();
		if (isAddGroundSegment) {

			/**
			 * Airports = Splited OND Code If First Airport Or Last They should be present in From/To For First Airport
			 * -- If it has bus segment child airports they all should be present in "From Search" Because Bus Segment
			 * Should Be Child---> Parent For Last Airport -- If it has bus Segment Child airprots they all should be
			 * present in "To Seach" Because Bus Segment Should Be Parent---> Child
			 */

			if (selectedOnd != null) {
				String[] ondData = selectedOnd.split("/");
				availableGroundStationsFrom = new ArrayList<String[]>();
				availableGroundStationsTo = new ArrayList<String[]>();
				for (int i = 0; i < ondData.length; i++) {
					String[] code = CommonsServices.getGlobalConfig().getMapAllAirportCodes().get(ondData[i]);
					if (i == 0) {
						List<String[]> groundStationList = CommonsServices.getGlobalConfig().getMapParentStationByGroundStation()
								.get(ondData[i]);
						// If ground stations present it should be departing from that ground station
						if (groundStationList != null) {
							availableGroundStationsFrom.addAll(groundStationList);
							availableGroundStationsTo.add(code);
						}
					} else if (i == ondData.length - 1) {
						List<String[]> groundStationList = CommonsServices.getGlobalConfig().getMapParentStationByGroundStation()
								.get(ondData[i]);
						if (groundStationList != null) {
							availableGroundStationsTo.addAll(groundStationList);
							availableGroundStationsFrom.add(code);
						}
					}
				}
			}
		}
		return new List[] { availableGroundStationsFrom, availableGroundStationsTo };
	}

	@SuppressWarnings("rawtypes")
	public static List[] getGroundSegmentModifyCombo(String selectedOnd) {

		List<String[]> availableGroundStationsFrom = new ArrayList<String[]>();
		List<String[]> availableGroundStationsTo = new ArrayList<String[]>();

		/**
		 * settling ground station list
		 * 
		 * From is a ground Station -- > Get Parent Stations of
		 */

		if (selectedOnd != null) {
			String[] ondData = selectedOnd.split("/");
			for (int i = 0; i < ondData.length; i++) {
				if (i == 0) {
					// if ground segment parent -- add the "Connecting " Airport to list.
					if (CommonsServices.getGlobalConfig().getPrimaryStationCodeMap().containsKey(ondData[i])) {
						// get connecting airport
						String topLevelStation = CommonsServices.getGlobalConfig().getMapTopLevelStationByConnectedStation()
								.get(ondData[i]);
						List<String[]> subStationsForTopLevelStation = CommonsServices.getGlobalConfig()
								.getMapParentStationByGroundStation().get(topLevelStation);

						for (Iterator<String[]> iterator = subStationsForTopLevelStation.iterator(); iterator.hasNext();) {
							String[] strings = iterator.next();
							availableGroundStationsFrom.add(strings);
						}
					} else {
						String[] desc = CommonsServices.getGlobalConfig().getMapAllAirportCodes().get(ondData[i]);
						availableGroundStationsFrom.add(desc);
					}// else add the current airport/desc to list
				}
				if (i == ondData.length - 1) {
					if (CommonsServices.getGlobalConfig().getPrimaryStationCodeMap().containsKey(ondData[i])) {
						// get connecting airport

						String topLevelStation = CommonsServices.getGlobalConfig().getMapTopLevelStationByConnectedStation()
								.get(ondData[i]);
						List<String[]> subStationsForTopLevelStation = CommonsServices.getGlobalConfig()
								.getMapParentStationByGroundStation().get(topLevelStation);

						for (Iterator<String[]> iterator = subStationsForTopLevelStation.iterator(); iterator.hasNext();) {
							String[] strings = iterator.next();
							availableGroundStationsTo.add(strings);
						}
					} else {
						String[] desc = CommonsServices.getGlobalConfig().getMapAllAirportCodes().get(ondData[i]);
						availableGroundStationsTo.add(desc);
					}
				}
			}
		}
		return new List[] { availableGroundStationsFrom, availableGroundStationsTo };
	}

	/**
	 * Load substation description
	 * 
	 * @param segmentCode
	 * @param subStationShortName
	 * @param primaryAirportName
	 * @return
	 */
	public static String getSubStationName(String segmentCode, String subStationShortName, String primaryAirportName) {
		if (AppSysParamsUtil.isGroundServiceEnabled() && subStationShortName != null && !subStationShortName.isEmpty()) {
			if (CommonsServices.getGlobalConfig().getMapTopLevelStationByConnectedStation().containsKey(segmentCode)) {
				return BUS_SERVICE_LEGEND + CommonsServices.getGlobalConfig().getGroundStationNamesMap().get(subStationShortName);
			}
		}
		return primaryAirportName;
	}
}
