/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.io.Serializable;

/**
 * Get Supported Language Codes
 * 
 * @author dumindag
 * 
 */
public final class LanguageCodesEnum implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	public enum LANGUAGE {
		ENGLISH("en"), ARABIC("ar"), FRENCH("fr"), RUSSIAN("ru"), SPANISH("es"), ITALIANO("it");
		private String language;

		LANGUAGE(String lang) {
			language = lang;
		}

		public String getLanguage() {
			return language;
		}

		public boolean equals(String lang) {
			return language.equalsIgnoreCase(lang);
		}

	}
}
