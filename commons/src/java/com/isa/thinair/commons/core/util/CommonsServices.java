package com.isa.thinair.commons.core.util;

import com.isa.thinair.commons.core.service.AerospikeCachingService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class CommonsServices {

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static DevConfig getDevConfig() {
		return (DevConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.DEV_CONFIG_BEAN);
	}
	
	public static AerospikeCachingService getAerospikeCachingService() {
		return (AerospikeCachingService) LookupServiceFactory.getInstance().getBean(
				PlatformBeanUrls.Commons.AEROSPIKE_CONFIG_BEAN);
	}
}
