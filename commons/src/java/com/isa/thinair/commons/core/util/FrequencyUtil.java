/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import com.isa.thinair.commons.api.dto.Frequency;

/**
 * utility calss for manipulate frequncy related functions
 * 
 * @author Lasantha Pambagoda
 */
public class FrequencyUtil {

	/**
	 * check if sub frequncy
	 * 
	 * @param outer
	 * @param sub
	 * @return boolean
	 */
	public static boolean chackIfSubFrequency(Frequency outer, Frequency sub) {

		boolean isSub = false;

		if ((!outer.getDay0() && sub.getDay0()) || (!outer.getDay1() && sub.getDay1()) || (!outer.getDay2() && sub.getDay2())
				|| (!outer.getDay3() && sub.getDay3()) || (!outer.getDay4() && sub.getDay4())
				|| (!outer.getDay5() && sub.getDay5()) || (!outer.getDay6() && sub.getDay6())) {
			isSub = false;
		} else {
			isSub = true;
		}

		return isSub;
	}

	/**
	 * get the res of frequency
	 * 
	 * @param outer
	 * @param sub
	 * @return
	 */
	public static Frequency getRestOfTheFrequency(Frequency outer, Frequency sub) {

		Frequency rest = new Frequency();

		if (outer.getDay0() && !sub.getDay0())
			rest.setDay0(true);
		if (outer.getDay1() && !sub.getDay1())
			rest.setDay1(true);
		if (outer.getDay2() && !sub.getDay2())
			rest.setDay2(true);
		if (outer.getDay3() && !sub.getDay3())
			rest.setDay3(true);
		if (outer.getDay4() && !sub.getDay4())
			rest.setDay4(true);
		if (outer.getDay5() && !sub.getDay5())
			rest.setDay5(true);
		if (outer.getDay6() && !sub.getDay6())
			rest.setDay6(true);

		return rest;
	}

	/**
	 * get ovaer lap frequency for true
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static Frequency getTrueOverlapFrequency(Frequency first, Frequency second) {

		Frequency rest = new Frequency();

		if (first.getDay0() && second.getDay0())
			rest.setDay0(true);
		if (first.getDay1() && second.getDay1())
			rest.setDay1(true);
		if (first.getDay2() && second.getDay2())
			rest.setDay2(true);
		if (first.getDay3() && second.getDay3())
			rest.setDay3(true);
		if (first.getDay4() && second.getDay4())
			rest.setDay4(true);
		if (first.getDay5() && second.getDay5())
			rest.setDay5(true);
		if (first.getDay6() && second.getDay6())
			rest.setDay6(true);

		return rest;
	}

	/**
	 * is given two frequencys are equal
	 * 
	 * @param a
	 * @param b
	 * @return boolean
	 */
	public static boolean isEqualFrequency(Frequency a, Frequency b) {

		boolean isEqual = false;

		if (a.getDay0() == b.getDay0() && a.getDay1() == b.getDay1() && a.getDay2() == b.getDay2() && a.getDay3() == b.getDay3()
				&& a.getDay4() == b.getDay4() && a.getDay5() == b.getDay5() && a.getDay6() == b.getDay6())
			isEqual = true;

		return isEqual;
	}

	/**
	 * is all true in the frequncy
	 * 
	 * @param frequency
	 * @return
	 */
	public static boolean isAllTrue(Frequency frequency) {

		boolean isAllTrue = false;

		if (frequency.getDay0() && frequency.getDay1() && frequency.getDay2() && frequency.getDay3() && frequency.getDay4()
				&& frequency.getDay4() && frequency.getDay5() && frequency.getDay6())
			isAllTrue = true;

		return isAllTrue;
	}

	/**
	 * is all false in the frequency
	 * 
	 * @param frequency
	 * @return
	 */
	public static boolean isAllFalse(Frequency frequency) {

		boolean isAllFalse = false;

		if (!frequency.getDay0() && !frequency.getDay1() && !frequency.getDay2() && !frequency.getDay3() && !frequency.getDay4()
				&& !frequency.getDay4() && !frequency.getDay5() && !frequency.getDay6())
			isAllFalse = true;

		return isAllFalse;
	}

	public static Frequency shiftFrequencyBy(Frequency fqn, int shift) {

		boolean isForward = (shift > 0) ? true : false;
		int numOfIter = (shift > 0) ? shift : shift * -1;

		for (int i = 0; i < numOfIter; i++) {

			if (isForward) {

				fqn = shiftForwardByOne(fqn);

			} else {

				fqn = shiftBackwordByOne(fqn);
			}
		}

		return fqn;
	}

	public static Frequency getCopyOfFrequency(Frequency fqn) {

		Frequency newFqn = new Frequency();

		if (fqn != null) {
			newFqn.setDay0(fqn.getDay0());
			newFqn.setDay1(fqn.getDay1());
			newFqn.setDay2(fqn.getDay2());
			newFqn.setDay3(fqn.getDay3());
			newFqn.setDay4(fqn.getDay4());
			newFqn.setDay5(fqn.getDay5());
			newFqn.setDay6(fqn.getDay6());
		}

		return newFqn;
	}

	public static Frequency shiftForwardByOne(Frequency fqn) {

		boolean day6 = fqn.getDay6();

		fqn.setDay6(fqn.getDay5());
		fqn.setDay5(fqn.getDay4());
		fqn.setDay4(fqn.getDay3());
		fqn.setDay3(fqn.getDay2());
		fqn.setDay2(fqn.getDay1());
		fqn.setDay1(fqn.getDay0());
		fqn.setDay0(day6);

		return fqn;
	}

	public static Frequency shiftBackwordByOne(Frequency fqn) {

		boolean day6 = fqn.getDay6();

		fqn.setDay6(fqn.getDay0());
		fqn.setDay0(fqn.getDay1());
		fqn.setDay1(fqn.getDay2());
		fqn.setDay2(fqn.getDay3());
		fqn.setDay3(fqn.getDay4());
		fqn.setDay4(fqn.getDay5());
		fqn.setDay5(day6);

		return fqn;
	}

	public static Frequency setAllTrue(Frequency fqn) {

		fqn.setDay0(true);
		fqn.setDay1(true);
		fqn.setDay2(true);
		fqn.setDay3(true);
		fqn.setDay4(true);
		fqn.setDay5(true);
		fqn.setDay6(true);

		return fqn;
	}

}
