package com.isa.thinair.commons.core.util;

import java.awt.Insets;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;
import org.zefer.pd4ml.PD4PageMark;

import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * @author Nilindra Fernando
 */
public class PD4MLBridge {

	private static final Log log = LogFactory.getLog(PD4MLBridge.class);

	private static PD4MLBridge instance;

	private static PD4ML pd4ml;

	private static boolean isInitialized = false;

	private static Object initializeLock = new Object();

	private PD4MLBridge() {
	}

	private static void startup() throws FileNotFoundException {
		if (!isInitialized) {
			synchronized (initializeLock) {
				if (!isInitialized) {
					instance = new PD4MLBridge();
					instance.initialize();
				}
			}
		}
	}

	public static PD4MLBridge getInstance() throws FileNotFoundException {
		startup();
		return instance;
	}

	private void initialize() throws FileNotFoundException {

		pd4ml = new PD4ML();
		pd4ml.setPageInsets(new Insets(20, 10, 10, 10));
		pd4ml.setHtmlWidth(950);
		//pd4ml.setPageSize(pd4ml.changePageOrientation(PD4Constants.A4)); // landscape page orientation
		pd4ml.setPageSize(PD4Constants.A4); //Portrait orientation
		pd4ml.enableImgSplit(false);

		String fontsDir = PlatformConstants.getConfigRootAbsPath()
				+ File.separator + "templates" + File.separator + "fonts";

		if (fontsDir != null && fontsDir.length() > 0) {
			pd4ml.useTTF(fontsDir, true);
		}

		String headerBody = null;
		if (headerBody != null && headerBody.length() > 0) {
			PD4PageMark header = new PD4PageMark();
			header.setAreaHeight(-1); // autocompute
			header.setHtmlTemplate(headerBody); // autocompute
			pd4ml.setPageHeader(header);
		}

		if (log.isDebugEnabled()) {
			pd4ml.enableDebugInfo();
		}

		isInitialized = true;
	}

	public byte[] render(byte[] inputByteArray)
			throws InvalidParameterException, IOException {
		InputStreamReader inputStreamReader = new InputStreamReader(
				new ByteArrayInputStream(inputByteArray));
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		pd4ml.render(inputStreamReader, byteArrayOutputStream);
		return byteArrayOutputStream.toByteArray();
	}

}
