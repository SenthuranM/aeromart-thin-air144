package com.isa.thinair.commons.core.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author Nilindra Fernando
 */
public class UniqueIDGenerator {

	private static Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");

	public static String generate() {
		return formatter.format(new Date()) + "-" + UUID.randomUUID();
	}

}
