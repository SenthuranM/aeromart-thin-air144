package com.isa.thinair.commons.core.util;

import java.util.Map;

public class ExternalAccoutingSystemsConfig {
	
	private Map<String, Object> accountingSystemMap;

	public Map<String, Object> getAccountingSystemMap() {
		return accountingSystemMap;
	}

	public void setAccountingSystemMap(Map<String, Object> accountingSystemMap) {
		this.accountingSystemMap = accountingSystemMap;
	}

}
