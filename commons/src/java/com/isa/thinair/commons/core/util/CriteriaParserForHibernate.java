/*
 * Created on Jul 5, 2005
 *
 */
package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;

/**
 * @author Nasly sub editor Byorn sub editor Thejaka
 * 
 */
public class CriteriaParserForHibernate {

	private static int PARAMETER_LIMIT = 999;

	public static Criteria parse(List<ModuleCriterion> criteriaList, Criteria hibernateCriteria) {
		List<String> orderByFieldList = new ArrayList<String>();
		Iterator<ModuleCriterion> it = criteriaList.iterator();
		while (it.hasNext()) {
			ModuleCriterion criterion = (ModuleCriterion) it.next();
			orderByFieldList.add(criterion.getFieldName());
		}
		return parse(criteriaList, hibernateCriteria, orderByFieldList);
	}

	public static Criteria parse(List<ModuleCriterion> criteriaList, Criteria hibernateCriteria, List<String> orderByFieldList) {
		if (orderByFieldList == null)
			return parse(criteriaList, hibernateCriteria);

		if ((orderByFieldList != null) && (orderByFieldList.size() > 0)) {
			Iterator<String> iterOrderByFieldList = orderByFieldList.iterator();
			while (iterOrderByFieldList.hasNext()) {
				String orderByField = (String) iterOrderByFieldList.next();
				hibernateCriteria.addOrder(Order.asc(orderByField));

			}
		}

		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			Iterator<ModuleCriterion> it = criteriaList.iterator();

			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getCondition().equals(ModuleCriterion.CONDITION_EQUALS)) {

					// List fieldnames = criterion.getFieldName();
					// List fieldvalues = criterion.getValue();
					hibernateCriteria.add(Expression.eq(criterion.getFieldName(), criterion.getValue().get(0)));
					// hibernateCriteria.add(Expression.eq((String)fieldnames.get(0), fieldvalues.get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN)) {
					hibernateCriteria.add(Expression.gt(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {
					hibernateCriteria.add(Expression.ge(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN)) {
					hibernateCriteria.add(Expression.lt(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {
					hibernateCriteria.add(Expression.le(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_BETWEEN)) {
					hibernateCriteria.add(Expression.between(criterion.getFieldName(), criterion.getValue().get(0), criterion
							.getValue().get(1)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {
					hibernateCriteria.add(
							buildCriterionForList(criterion.getFieldName(), criterion.getValue(), ModuleCriterion.CONDITION_IN));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_NOT_IN)) {
					hibernateCriteria.add(buildCriterionForList(criterion.getFieldName(), criterion.getValue(),
							ModuleCriterion.CONDITION_NOT_IN));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE)) {
					hibernateCriteria.add(Expression.like(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_IGNORECASE)) {
					hibernateCriteria.add(Expression.like(criterion.getFieldName(), criterion.getValue().get(0)).ignoreCase());
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE)) {
					hibernateCriteria.add(Expression.ilike(criterion.getFieldName(), (String) criterion.getValue().get(0),
							MatchMode.ANYWHERE));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_NOT_EQUAL)) {
					hibernateCriteria.add(Expression.ne(criterion.getFieldName(), criterion.getValue().get(0)));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IS_NOT_NULL)) {
					hibernateCriteria.add(Expression.isNotNull(criterion.getFieldName()));
				}
			}
			return hibernateCriteria;
		}
		return hibernateCriteria;
	}

	// -------------------

	public static Criterion buildCriterionForList(String fieldName, List values, String moduleCriteria) {
		Criterion criterion = null;
		int listSize = values.size();
		for (int i = 0; i < listSize; i += PARAMETER_LIMIT) {
			List subList;
			if (listSize > i + PARAMETER_LIMIT) {
				subList = values.subList(i, (i + PARAMETER_LIMIT));
			} else {
				subList = values.subList(i, listSize);
			}
			if (ModuleCriterion.CONDITION_IN.equals(moduleCriteria)) {
				if (criterion != null) {
					criterion = Restrictions.or(criterion, Restrictions.in(fieldName, subList));
				} else {
					criterion = Restrictions.in(fieldName, subList);
				}
			} else if (ModuleCriterion.CONDITION_NOT_IN.equals(moduleCriteria)) {
				if (criterion != null) {
					criterion = Restrictions.and(criterion, Restrictions.not(Restrictions.in(fieldName, subList)));
				} else {
					criterion = Restrictions.not(Restrictions.in(fieldName, subList));
				}
			}
		}
		return criterion;
	}

	public static Criteria parseNew(List<ModuleCriterion> criteriaList, Criteria hibernateCriteria) {
		List<String> orderByFieldList = new ArrayList<String>();
		Iterator<ModuleCriterion> it = criteriaList.iterator();
		while (it.hasNext()) {
			ModuleCriterion criterion = (ModuleCriterion) it.next();
			orderByFieldList.add(criterion.getFieldName());
		}
		return parse(criteriaList, hibernateCriteria, orderByFieldList);
	}

	public static Criteria parseNew(List<ModuleCriterion> criteriaList, Criteria hibernateCriteria, List<String> orderByFieldList) {
		if (orderByFieldList == null)
			return parseNew(criteriaList, hibernateCriteria);

		if ((orderByFieldList != null) && (orderByFieldList.size() > 0)) {
			Iterator<String> iterOrderByFieldList = orderByFieldList.iterator();
			while (iterOrderByFieldList.hasNext()) {
				String orderByField = (String) iterOrderByFieldList.next();
				hibernateCriteria.addOrder(Order.asc(orderByField));

			}
		}

		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			Iterator<ModuleCriterion> it = criteriaList.iterator();

			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();
				hibernateCriteria.add(getExpression(criterion));
			}
			return hibernateCriteria;
		}
		return hibernateCriteria;
	}

	private static Criterion getExpression(ModuleCriterion criterion) {
		if (criterion != null) {
			if (criterion.isLeaf()) {
				if (criterion.getCondition().equals(ModuleCriterion.CONDITION_EQUALS)) {
					return Expression.eq(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN)) {
					return Expression.gt(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {
					return Expression.ge(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN)) {
					return Expression.lt(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {
					return Expression.le(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_BETWEEN)) {
					return Expression.between(criterion.getFieldName(), criterion.getValue().get(0), criterion.getValue().get(1));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {
					return Expression.in(criterion.getFieldName(), criterion.getValue());
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE)) {
					return Expression.like(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_IGNORECASE)) {
					return Expression.like(criterion.getFieldName(), criterion.getValue().get(0)).ignoreCase();
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_INCASE_SENSITIVE)) {
					return Expression.ilike(criterion.getFieldName(), (String) criterion.getValue().get(0), MatchMode.ANYWHERE);
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_NOT_EQUAL)) {
					return Expression.ne(criterion.getFieldName(), criterion.getValue().get(0));
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IS_NOT_NULL)) {
					return Expression.isNotNull(criterion.getFieldName());
				}
			} else {
				ModuleCriteria moduleCriteria = (ModuleCriteria) criterion;
				if (moduleCriteria.getModulecriterionList() != null && moduleCriteria.getModulecriterionList().size() > 1) {
					Criterion result = null;
					for (int i = 0; i < moduleCriteria.getModulecriterionList().size() - 1; i++) {
						if (result == null) {
							result = Expression.or(
									getExpression((ModuleCriterion) moduleCriteria.getModulecriterionList().get(i)),
									getExpression((ModuleCriterion) moduleCriteria.getModulecriterionList().get(i + 1)));
						} else {
							result = Expression.or(result, getExpression((ModuleCriterion) moduleCriteria
									.getModulecriterionList().get(i + 1)));
						}
					}
					return result;
				}
				if (moduleCriteria.getJoinCondition().equals(ModuleCriteria.JOIN_CONDITION_AND)) {
					return Expression.and(getExpression(moduleCriteria.getLeftCriterion()),
							getExpression(moduleCriteria.getRightCriterion()));
				} else if (moduleCriteria.getJoinCondition().equals(ModuleCriteria.JOIN_CONDITION_OR)) {
					return Expression.or(getExpression(moduleCriteria.getLeftCriterion()),
							getExpression(moduleCriteria.getRightCriterion()));
				}
			}
		}
		return null;
	}
}
