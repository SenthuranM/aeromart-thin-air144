package com.isa.thinair.commons.core.util;

public class ReportFormatTypes {

	public interface ReportTypes {
		static String HTML = "HTML";
		static String PDF = "PDF";
		static String EXCEL = "EXCEL";
		static String CSV = "CSV";
		static String CSV_AGENT = "CSV_AGENT";
	}

}
