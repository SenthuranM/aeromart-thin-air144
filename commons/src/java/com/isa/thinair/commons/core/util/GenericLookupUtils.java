package com.isa.thinair.commons.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.platform.api.DefaultModuleConfig;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Nilindra Fernando
 */
public class GenericLookupUtils {

	private static final Log log = LogFactory.getLog(GenericLookupUtils.class);

	/**
	 * Returns EJB2 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @param config
	 * @param currentModuleName
	 * @param errorCode
	 * @return
	 */
	public static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality,
			DefaultModuleConfig config, String currentModuleName, String errorCode) {
		String fullBDKey;

		String locality = (String) config.getModuleDependencyMap().get(targetModuleName);

		if (locality == null || !(locality.equals("local") || locality.equals("remote"))) {
			if (log.isErrorEnabled()) {
				log.error("Invalid module dependency map configuration found");
			}
			throw new ModuleRuntimeException(errorCode, currentModuleName + ".desc");
		}

		fullBDKey = BDKeyWithoutLocality + "." + locality;
		return LookupServiceFactory.getInstance().getServiceBD(targetModuleName, fullBDKey);

	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param serviceName
	 * @param config
	 * @param currentModuleName
	 * @param errorCode
	 * @return
	 */
	public static Object lookupEJB3Service(String targetModuleName, String serviceName, DefaultModuleConfig config,
			String currentModuleName, String errorCode) {
		String fullBDKey;

		String locality = (String) config.getModuleDependencyMap().get(targetModuleName);
		if (locality == null || !(locality.equals("local") || locality.equals("remote"))) {
			if (log.isErrorEnabled()) {
				log.error("Invalid module dependency map configuration found");
			}

			throw new ModuleRuntimeException(errorCode, currentModuleName + ".desc");
		}

		DefaultModuleConfig clientModuleConfig = LookupServiceFactory.getInstance().getModule(targetModuleName).getModuleConfig();
		fullBDKey = serviceName + "." + locality;

		Object reference = LookupServiceFactory.getInstance().getService(
				clientModuleConfig.getJndiProperties(), fullBDKey, CommonsServices.getGlobalConfig().isNoCaching());
		
		if (reference == null) {
			log.error("Can't locate the module reference for the service name : [" + serviceName + "] fullBDKey : [" + fullBDKey + "] ");
			throw new ModuleRuntimeException(errorCode, currentModuleName + ".desc");
		} else {
			return reference;
		}
	}
}
