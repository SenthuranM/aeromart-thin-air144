package com.isa.thinair.commons.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.IpToCountry;
import com.isa.thinair.commons.api.model.WhiteListedEmailDomains;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.persistence.dao.CommonsHibernateDAO;

/**
 * 
 * @author nafly
 * @isa.module.dao-impl dao-name="CommonsHibernateDAO"
 */
public class CommonsHibernateDAOImpl extends PlatformBaseHibernateDaoSupport implements CommonsHibernateDAO {

	@Override
	public List<String> getActiveDomainNames() {
		Object[] params = { WhiteListedEmailDomains.STATUS_ACT };

		List<String> domainNames = find(
				"select lower(d.domainName) from WhiteListedEmailDomains d where d.status=?", params, String.class);

		return domainNames;
	}

	/**
	 * Common method to save or update any entity, Did this because commons module can benefit from such method. To find
	 * the entityName use ClassName.class.getName() eg :-WhiteListedEmailDomains.class.getName()
	 */
	@Override
	public void saveOrUpdateEntity(String entityName, Object object) {
		saveEntity(entityName, object);
		
	}
	
	@Override
	public List<IpToCountry> getUnsynedIPToCountryData() throws ModuleException {
		List<IpToCountry> list = find(
				"from IpToCountry ipToCountry where ipToCountry.syncedWithCachingDB = ? ", new Object[] { false }, IpToCountry.class);
		return list;
	}

	@Override
	public void saveOrUpdateAllIPToCountries(List<IpToCountry> ipToCountries) {
		hibernateSaveOrUpdateAll(ipToCountries);
	};

}
