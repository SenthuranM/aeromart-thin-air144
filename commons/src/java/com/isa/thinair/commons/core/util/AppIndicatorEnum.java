/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.io.Serializable;

/**
 * @author kasun
 * 
 */
public class AppIndicatorEnum implements Serializable {

	private static final long serialVersionUID = -3842318224655968441L;

	private char code;

	private AppIndicatorEnum(char code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(AppIndicatorEnum allocateEnum) {
		return (allocateEnum.getCode() == this.getCode());
	}

	public char getCode() {
		return code;
	}
	
	/**
	 * @param String value of app Indicator
	 * */
	public static AppIndicatorEnum fromValue(String v) {
        return new AppIndicatorEnum(v.charAt(0));
    }

	public static final AppIndicatorEnum APP_IBE = new AppIndicatorEnum('W');

	public static final AppIndicatorEnum APP_KIOSK = new AppIndicatorEnum('K');

	public static final AppIndicatorEnum APP_XBE = new AppIndicatorEnum('C');

	public static final AppIndicatorEnum APP_WS = new AppIndicatorEnum('S');
	
	public static final AppIndicatorEnum APP_GDS = new AppIndicatorEnum('G');

	public static final AppIndicatorEnum APP_IBEPAYMENT = new AppIndicatorEnum('I');

	public static final AppIndicatorEnum APP_MYID = new AppIndicatorEnum('M');

	public static final AppIndicatorEnum APP_OFFLINEPAYMENT = new AppIndicatorEnum('F');

	public static final AppIndicatorEnum APP_LCC = new AppIndicatorEnum('L');

	public static final AppIndicatorEnum APP_GOQUO = new AppIndicatorEnum('Q');
	
	public static final AppIndicatorEnum APP_IOS = new AppIndicatorEnum('J');
	
	public static final AppIndicatorEnum APP_ANDROID = new AppIndicatorEnum('A');

}
