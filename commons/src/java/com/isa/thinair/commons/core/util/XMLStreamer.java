package com.isa.thinair.commons.core.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Streamer Utility for any Java Object Produces the XML and vice versa
 * 
 * @author Nilindra Fernando
 * 
 */
public class XMLStreamer {
	private static XStream xstream = new XStream(new DomDriver());

	public static String compose(Object object) {
		return xstream.toXML(object);
	}

	public static Object decompose(String xml) {
		return xstream.fromXML(xml);
	}

}
