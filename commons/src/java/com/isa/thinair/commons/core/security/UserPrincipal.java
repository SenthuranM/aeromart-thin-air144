/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.security;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collection;

import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;

/**
 * Holds the User Principal Data transfer information
 * 
 * @author Nilindra Fernando / Byorn John
 * @since 1.0
 */
public class UserPrincipal implements Principal, Serializable {

	private static final long serialVersionUID = 5062754184455116754L;

	/** Holds the name of the principal this can be userId or the customerEmailId ...etc */
	private String userText;

	/** Holds the corresponding customer id */
	private Integer customerId;

	/** Holds the corresponding user id */
	private String userId;

	/** Holds user password */
	private String password;

	/** Holds the corresponding sales channel */
	private int salesChannel;
	
	/** Holds the corresponding directbill id */
	private String directBillId;

	/** Holds the corresponding agent code */
	private String agentCode;

	/** Holds the agent station */
	private String agentStation;

	/** Holds the time difference with the agent location */
	private Collection<UserDST> colUserDST;

	/** Holds the accessable carrier codes in type [Object] */
	private Collection<String> carrierCodes;

	/** Holds user default Carrier Code */
	private String defaultCarrierCode;

	/**
	 * Holds the airline code . This is unique to a given airline so can be used to uniquely ID the airline which the
	 * user is belong to. Since an airline may operate multiple carrier codes, default carrier code cannot be used here.
	 */
	private String airlineCode;

	private String agentTypeCode;

	/** Holds user connecting remote ip address */
	private String ipAddress;

	/** Holds information about the client os, browser */
	private String clientInfo;

	/** Holds the agent currency code */
	private String agentCurrencyCode;

	/** Holds the height of the screen */
	private String screenHeight;

	private String callingInstanceId;

	private String sessionId;
	
	private ApplicationEngine applicationEngine;
	

	/**
	 * Create User Principal Identity
	 * 
	 * @param userText
	 * @return
	 */
	public static Principal createIdentity(String userText) {
		UserPrincipal userPrincipal = new UserPrincipal();
		userPrincipal.userText = userText;
		return userPrincipal;
	}

	/**
	 * Create Identity
	 * 
	 * @param userText
	 * @param salesChannelCode
	 * @param agentCode
	 * @param agentStation
	 * @param customerId
	 * @param userId
	 * @param colUserDST
	 * @param password
	 * @param carrierCodes
	 * @param userDefaultCarrier
	 * @param airlineCode
	 *            unique id to represent the airline id the user is belonges to
	 * @param agentTypeCode
	 * @param agentCurrencyCode
	 * @param ipAddress
	 * @param clientInfo
	 * @param sessionId
	 *            TODO
	 * @return
	 */
	public static Principal createIdentity(String userText, int salesChannelCode, String agentCode, String agentStation,
			Integer customerId, String userId, Collection<UserDST> colUserDST, String password, Collection<String> carrierCodes,
			String userDefaultCarrier, String airlineCode, String agentTypeCode, String agentCurrencyCode, String ipAddress,
			String clientInfo, String screenHeight, String sessionId) {
		UserPrincipal userPrincipal = new UserPrincipal();
		userPrincipal.userText = userText;
		userPrincipal.salesChannel = salesChannelCode;
		userPrincipal.agentCode = cacheIntern(agentCode);
		userPrincipal.agentStation = cacheIntern(agentStation);
		userPrincipal.customerId = customerId;
		userPrincipal.userId = userId;
		userPrincipal.password = password;
		userPrincipal.colUserDST = colUserDST;
		userPrincipal.carrierCodes = carrierCodes;
		userPrincipal.defaultCarrierCode = cacheIntern(userDefaultCarrier);
		userPrincipal.airlineCode = cacheIntern(airlineCode);
		userPrincipal.agentTypeCode = cacheIntern(agentTypeCode);
		userPrincipal.agentCurrencyCode = cacheIntern(agentCurrencyCode);
		userPrincipal.ipAddress = ipAddress;
		userPrincipal.clientInfo = clientInfo;
		userPrincipal.screenHeight = screenHeight;
		userPrincipal.callingInstanceId = cacheIntern(SystemPropertyUtil.getInstanceId());
		userPrincipal.sessionId = sessionId;

		return userPrincipal;
	}

	public static String cacheIntern(String in) {
		if (in != null) {
			return in.intern();
		}
		return in;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public int getSalesChannel() {
		return salesChannel;
	}

	public boolean equals(Object another) {
		if (!(another instanceof UserPrincipal)) {
			return false;
		}

		String userText = ((UserPrincipal) another).getName();
		int salesChannel = ((UserPrincipal) another).getSalesChannel();
		String agentCode = ((UserPrincipal) another).getAgentCode();
		String agentStation = ((UserPrincipal) another).getAgentStation();
		Integer customerId = ((UserPrincipal) another).getCustomerId();
		String userId = ((UserPrincipal) another).getUserId();
		String defaultCarrier = ((UserPrincipal) another).getDefaultCarrierCode();
		String airlineCode = ((UserPrincipal) another).getAirlineCode();

		boolean equals = false;

		// Checking the name
		if (this.userText == null) {
			equals = userText == null;
		} else {
			equals = this.userText.equals(userText);
		}

		// Checking the sales channel code
		equals = equals && this.salesChannel == salesChannel;

		// Checking the agent code
		if (this.agentCode == null) {
			equals = equals && agentCode == null;
		} else {
			equals = equals && this.agentCode.equals(agentCode);
		}

		// Checking the agent station
		if (this.agentStation == null) {
			equals = equals && agentStation == null;
		} else {
			equals = equals && this.agentStation.equals(agentStation);
		}

		// Checking the customer id
		if (this.customerId == null) {
			equals = equals && customerId == null;
		} else {
			equals = equals && this.customerId.equals(customerId);
		}

		// Checking the user Id
		if (this.userId == null) {
			equals = equals && userId == null;
		} else {
			equals = equals && this.userId.equals(userId);
		}

		// Checking the default carrier code
		if (this.defaultCarrierCode == null) {
			equals = equals && defaultCarrier == null;
		} else {
			equals = equals && this.defaultCarrierCode.equals(defaultCarrier);
		}

		// Checking the airline code
		if (this.airlineCode == null) {
			equals = equals && airlineCode == null;
		} else {
			equals = equals && this.airlineCode.equals(airlineCode);
		}

		return equals;
	}

	public int hashCode() {
		return (userText == null ? 0 : userText.hashCode());
	}

	public String toString() {
		return userText + "," + salesChannel + "," + customerId + "," + agentCode + "," + agentStation + "," + userId + ","
				+ defaultCarrierCode + "," + callingInstanceId;
	}

	/**
	 * @return Returns the customerId.
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @return Returns the colUserDST.
	 */
	public Collection<UserDST> getColUserDST() {
		return colUserDST;
	}

	/**
	 * @return Returns the agentStation.
	 */
	public String getAgentStation() {
		return agentStation;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Returns the name
	 */
	public String getName() {
		return userText;
	}

	/**
	 * @return Return the password
	 */
	public String getPassword() {
		return password;
	}

	public Collection<String> getCarrierCodes() {
		return carrierCodes;
	}

	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	/**
	 * @return the airlineCode
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * @return the agentCurrencyCode
	 */
	public String getAgentCurrencyCode() {
		return agentCurrencyCode;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the clientInfo
	 */
	public String getClientInfo() {
		return clientInfo;
	}

	/**
	 * @param screenHeight
	 *            the screenHeight to set
	 */
	public void setScreenHeight(String screenHeight) {
		this.screenHeight = screenHeight;
	}

	/**
	 * @return the screenHeight
	 */
	public String getScreenHeight() {
		return screenHeight;
	}

	public String getCallingInstanceId() {
		return callingInstanceId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public String getDirectBillId() {
		return directBillId;
	}

	public void setDirectBillId(String directBillId) {
		this.directBillId = directBillId;
	}

	/**
	 * @return the applicationEngine
	 */
	public ApplicationEngine getApplicationEngine() {

		switch (this.salesChannel) {
		case SalesChannelsUtil.SALES_CHANNEL_AGENT:
			this.applicationEngine = ApplicationEngine.XBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_PUBLIC:
			this.applicationEngine = ApplicationEngine.IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_WEB:
			this.applicationEngine = ApplicationEngine.IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
			this.applicationEngine = ApplicationEngine.WS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_LCC:
			this.applicationEngine = ApplicationEngine.LCC;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_IOS:
			this.applicationEngine = ApplicationEngine.IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
			this.applicationEngine = ApplicationEngine.IBE;
			break;
		default:
			break;
		}

		return this.applicationEngine;
	}
	
	
	
}
