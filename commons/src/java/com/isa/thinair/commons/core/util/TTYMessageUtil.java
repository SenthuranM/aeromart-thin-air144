package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;

/**
 * @author Manoj Dhanushka
 */
public class TTYMessageUtil {
	
	public static String getGdsMappedBCForActualBC(String gdsCarrierCode, String bookingCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		String gdsBookingClass = null;
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gdsBookingClass = getGdsMappedBCForActualBC(gdsStatusTO.getGdsId(), bookingCode);
			}
		}
		if(gdsBookingClass != null) {
			return gdsBookingClass;
		}
		return bookingCode;
	}

    public static String getGdsMappedBCForActualBC(Integer gdsId, String bookingCode) {
        Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();
        String gdsBookingClass = null;
        if (!gdsBookingClassMap.get(gdsId).isEmpty()) {
            gdsBookingClass = gdsBookingClassMap.get(gdsId).get(bookingCode);
        }
        if(gdsBookingClass != null) {
            return gdsBookingClass;
        }
        return bookingCode;
    }

	public static String getActualBCForGdsMappedBC(Integer gdsId, String gdsbookingCode) {
		Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();
		String bookingClass = null;
		if (!gdsBookingClassMap.get(gdsId).isEmpty()) {
			Map<String, String> bookingClassMap = gdsBookingClassMap.get(gdsId);
			for (Entry<String, String> bcEntry : bookingClassMap.entrySet()) {
				if (bcEntry.getValue().equals(gdsbookingCode)) {
					bookingClass = bcEntry.getKey();
				}
			}
		}
		if (bookingClass != null) {
			return bookingClass;
		}
		return gdsbookingCode;
	}
	
	public static String getExternalCsBCForGdsMappedBC(String gdsCarrierCode, String bookingCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				return getExternalCsBCForGdsMappedBC(gdsStatusTO.getGdsId(), bookingCode);
			}
		}
		return bookingCode;
	}
	
	public static String getExternalCsBCForGdsMappedBC(Integer gdsId, String bookingCode) {
		Map<Integer, Map<String, String>> csBookingClassMap = CommonsServices.getGlobalConfig().getCsBookingClassMap();
		String csBookingClass = null;
		if (!csBookingClassMap.get(gdsId).isEmpty()) {
			csBookingClass = csBookingClassMap.get(gdsId).get(bookingCode);
		}
		if (csBookingClass != null) {
			return csBookingClass;
		}
		return bookingCode;
	}
	
	public static String getExternalCsBCForActualBC(String gdsCarrierCode, String bookingCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();

		Integer gdsId = null;
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gdsId = gdsStatusTO.getGdsId();
			}
		}
		if (gdsId != null) {
			return getExternalCsBCForGdsMappedBC(gdsId, getGdsMappedBCForActualBC(gdsId, bookingCode));
		}
		return bookingCode;
	}

	public static String getActualBCForExternalCsBC(Integer gdsId, String gdsbookingCode) {

		Map<Integer, Map<String, String>> csBookingClassMap = CommonsServices.getGlobalConfig().getCsBookingClassMap();
		if (csBookingClassMap != null && csBookingClassMap.get(gdsId) != null && !csBookingClassMap.get(gdsId).isEmpty()) {
			Map<String, String> gdsBcByCsMappedBc = csBookingClassMap.get(gdsId);
			if (gdsBcByCsMappedBc != null && !gdsBcByCsMappedBc.isEmpty()) {
				for (String csMappedBc : gdsBcByCsMappedBc.keySet()) {
					String gdsBc = gdsBcByCsMappedBc.get(csMappedBc);
					if (gdsbookingCode.equalsIgnoreCase(gdsBc)) {
						return getActualBCForGdsMappedBC(gdsId, csMappedBc);
					}
				}
			}
		}
		return null;
	}

	
	public static List<String> getSingleEntryMappedActualBCList(Integer gdsId) {
		List<String> actualBcList = new ArrayList<String>();
		Map<Integer, List<String>> gdsMappedBookingClassList = CommonsServices.getGlobalConfig()
				.getSingleEntryMappedGDSBookingClasses();
		if (gdsMappedBookingClassList != null && gdsMappedBookingClassList.get(gdsId) != null
				&& !gdsMappedBookingClassList.get(gdsId).isEmpty()) {

			List<String> mapedBcList = gdsMappedBookingClassList.get(gdsId);

			if (mapedBcList != null && !mapedBcList.isEmpty()) {

				Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();

				if (gdsBookingClassMap != null && !gdsBookingClassMap.get(gdsId).isEmpty()) {
					Map<String, String> bookingClassMap = gdsBookingClassMap.get(gdsId);
					if (bookingClassMap != null && !bookingClassMap.isEmpty()) {
						for (String actualBc : bookingClassMap.keySet()) {

							if (bookingClassMap.get(actualBc) != null && mapedBcList.contains(bookingClassMap.get(actualBc))) {
								actualBcList.add(actualBc);
							}

						}
					}

				}
			}

		}

		return actualBcList;
	}
	
	public static String getLogicalCCByActualBookingCode(String bookingCode) {
		Map<String, List<String>> bookingClassByLogicalCCMap = CommonsServices.getGlobalConfig().getBookingClassByLogicalCCMap();

		if (bookingClassByLogicalCCMap != null && !bookingClassByLogicalCCMap.isEmpty()) {
			for (String logicalCabin : bookingClassByLogicalCCMap.keySet()) {
				if (bookingClassByLogicalCCMap.get(logicalCabin) != null
						&& !bookingClassByLogicalCCMap.get(logicalCabin).isEmpty()
						&& bookingClassByLogicalCCMap.get(logicalCabin).contains(bookingCode)) {
					return logicalCabin;
				}

			}
		}

		return null;
	}
	
	public static boolean getOALSegmentsSendingEligibility(String gdsCarrierCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				return gdsStatusTO.isSendOALSegments();
			}
		}
		return false;
	}
	
	public static boolean getOALSegmentsRecordingEligibility(Integer gdsId) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getGdsId().equals(gdsId)) {
				return gdsStatusTO.isRecordOALSegments();
			}
		}
		return false;
	}
	
	public static String getOutTTYForServiceClientReqType(String serviceClientCode, String requestType) {
		String ttyAddres = null;
		Map<String, List<ServiceClientTTYDTO>> serviceClientTTYbyRequestTypeMap = CommonsServices.getGlobalConfig()
				.getServiceClientTTYbyRequestType();
		if (!serviceClientTTYbyRequestTypeMap.isEmpty() && serviceClientTTYbyRequestTypeMap.get(requestType) != null) {
			for (ServiceClientTTYDTO serviceClientTTYDTO : serviceClientTTYbyRequestTypeMap.get(requestType)) {
				if (serviceClientCode.equals(serviceClientTTYDTO.getServiceClientCode())) {
					ttyAddres = serviceClientTTYDTO.getOutTTYAddress();
					break;
				}
			}
		}
		return ttyAddres;
	}
	
	public static String getServiceClientForReqTypeTTY(String tty, String requestType) {
		String serviceClientCode = null;
		Map<String, List<ServiceClientTTYDTO>> serviceClientTTYbyRequestTypeMap = CommonsServices.getGlobalConfig()
				.getServiceClientTTYbyRequestType();
		if (!serviceClientTTYbyRequestTypeMap.isEmpty() && serviceClientTTYbyRequestTypeMap.get(requestType) != null) {
			for (ServiceClientTTYDTO serviceClientTTYDTO : serviceClientTTYbyRequestTypeMap.get(requestType)) {
				if (tty.equals(serviceClientTTYDTO.getInTTYAddress())) {
					serviceClientCode = serviceClientTTYDTO.getServiceClientCode();
					break;
				}
			}
		}
		return serviceClientCode;
	}
	
	public static ServiceClientTTYDTO getServiceClientTTYForReqTypeTTY(String tty, String requestType) {
		Map<String, List<ServiceClientTTYDTO>> serviceClientTTYbyRequestTypeMap = CommonsServices.getGlobalConfig()
				.getServiceClientTTYbyRequestType();
		if (!serviceClientTTYbyRequestTypeMap.isEmpty() && serviceClientTTYbyRequestTypeMap.get(requestType) != null) {
			for (ServiceClientTTYDTO serviceClientTTYDTO : serviceClientTTYbyRequestTypeMap.get(requestType)) {
				if (tty.equals(serviceClientTTYDTO.getInTTYAddress())) {
					return serviceClientTTYDTO;
				}
			}
		}
		return null;
	}
	
	public static ServiceClientDTO getServiceClientByServiceClientCode(String serviceClientCode) {
		Map<String, ServiceClientDTO> serviceClients = CommonsServices.getGlobalConfig().getServiceClientsMap();
		if (!serviceClients.isEmpty() && serviceClients.get(serviceClientCode) != null) {
			return serviceClients.get(serviceClientCode);
		}
		return null;
	}
	
	public static ServiceBearerDTO getServiceBearerByServiceClientCode(String serviceClientCode) {
		Map<String, ServiceBearerDTO> serviceBearers = CommonsServices.getGlobalConfig().getServiceBearersMap();
		if (!serviceBearers.isEmpty() && serviceBearers.get(serviceClientCode) != null) {
			return serviceBearers.get(serviceClientCode);
		}
		return null;
	}
	
	public static int getTypeBSyncTypeOfGDS(String gdsCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getGdsCode().equals(gdsCode)) {
				return gdsStatusTO.getTypeBSyncType();
			}
		}
		return -1;
	}
}
