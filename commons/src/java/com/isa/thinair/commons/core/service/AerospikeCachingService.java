package com.isa.thinair.commons.core.service;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Key;
import com.aerospike.client.Language;
import com.aerospike.client.Record;
import com.aerospike.client.Value;
import com.aerospike.client.lua.LuaConfig;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.query.ResultSet;
import com.aerospike.client.query.Statement;
import com.aerospike.client.task.RegisterTask;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishToCachingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.AerospikeAdaptorBL;
import com.isa.thinair.commons.core.persistence.dao.CommonsDAO;
import com.isa.thinair.commons.core.util.AerospikeInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * @isa.module.config-bean
 */
public class AerospikeCachingService {

	private boolean systemEnabled;
	private String host;
	private int port;
	private String namespace;
	private int defaultWriteTimeout;
	private String username;
	private String password;
	private String ondNamespace;
	// UDFs
	private String udfExtension;
	private String ipToCountryUDF;
	private String ondsCacheUDF;
	private String appParamUDF;

	public enum SYSTEM {
		AA,
		INT,
		ALL,
		COND,
		NONE;
	}

	private Log log = LogFactory.getLog(AerospikeCachingService.class);

	private AerospikeClient aerospikeClient = null;

	private Map<String, Boolean> udfPopulated = null;

	public boolean isCachingDBEnabled() {
		return getSystemEnabled();
	}

	public void syncIpToCountryData() throws ModuleException {
		AerospikeAdaptorBL aerospikeAdaptorBL = new AerospikeAdaptorBL(getAerospikeClient(), getDefaultWriteTimeout(),
				getNamespace());
		aerospikeAdaptorBL.syncIpToCountryData();
	}

	public void syncAllAppParamsToCache() throws ModuleException {
		AerospikeAdaptorBL aerospikeAdaptorBL = new AerospikeAdaptorBL(getAerospikeClient(), getDefaultWriteTimeout(),
				getNamespace());
		aerospikeAdaptorBL.syncAllAppParamsToCache();
	}

	public void syncAppParameterToCache(String paramKey, String paramValue, String carrierCode) throws ModuleException {
		AerospikeAdaptorBL aerospikeAdaptorBL = new AerospikeAdaptorBL(getAerospikeClient(), getDefaultWriteTimeout(),
				getNamespace());
		aerospikeAdaptorBL.syncAppParameterToCache(paramKey, paramValue, carrierCode);
	}

	public void syncOndPublishToCache(List<OndPublishToCachingDTO> ondsForCaching) throws ModuleException {
		AerospikeAdaptorBL aerospikeAdaptorBL = new AerospikeAdaptorBL(getAerospikeClient(), getDefaultWriteTimeout(),
				getOndNamespace());
		aerospikeAdaptorBL.syncOndPublishToCache(ondsForCaching);
	}

	public String getAppParamValueByParamKey(String paramKey) {

		String paramValue = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = getAppParamUDF();
			aerospikeClient = getAerospikeClient();
			if (!getUdfPopulated().get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (getUdfPopulated().get(udf)) {
				Statement st = new Statement();
				st.setNamespace(getNamespace());
				st.setSetName(AerospikeInternalConstants.UDFAppParameter.SET_NAME);

				Key key = new Key(getNamespace(), AerospikeInternalConstants.UDFAppParameter.SET_NAME, paramKey);
				Record record = aerospikeClient.get(null, key);

				if (record != null && record.bins.get(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE) != null) {
					paramValue = record.bins.get(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE).toString();
				} else {
					log.info("App Param results is empty in Aerospike");
				}
			}
		} catch (Exception e) {
			log.error("Error in retriving app params from Aerospike", e);
		}
		// finally {
		// if (aerospikeClient != null) {
		// aerospikeClient.close();
		// }
		// }
		return paramValue;
	}

	public String getCountryById(Long ip) throws ModuleException {
		String countryCode = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = getIpToCountryUDF();
			aerospikeClient = getAerospikeClient();
			if (!getUdfPopulated().get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (getUdfPopulated().get(udf)) {
				Statement st = new Statement();
				st.setNamespace(getNamespace());
				st.setSetName(AerospikeInternalConstants.UDFIpToCountry.SET_NAME);

				ResultSet resultSet = aerospikeClient.queryAggregate(null, st, getIpToCountryUDF(),
						AerospikeInternalConstants.UDFIpToCountry.CALLING_METHOD, Value.get(ip));

				if (resultSet == null) {
					log.info("IP to country results is empty in Aerospike");
				} else {
					while (resultSet.next()) {
						countryCode = (String) resultSet.getObject();
						log.info("Aerospike data country for ip " + ip + " is " + countryCode);
						break;
					}
				}
			}

		} catch (Exception e) {
			log.error("Error in retriving country code by ip from Aerospike", e);
		}
		// finally {
		// if (aerospikeClient != null) {
		// aerospikeClient.close();
		// }
		// }
		return countryCode;
	}

	public SYSTEM getSearchSystem(List<String> originDestinations, String requestingCarrier) throws ModuleException {

		if (!AppSysParamsUtil.isLCCConnectivityEnabled()) {
			return SYSTEM.AA;
		}

		for (String ond : originDestinations) {

			String[] ondArray = ond.split("/");
			List<String> operatingCarrierList = getOperatingCarriers(ondArray[0].concat(ondArray[1]));
			Set<String> operatingCarrier = operatingCarrierList != null ? new HashSet<>(operatingCarrierList) : null;
			if (operatingCarrier != null && operatingCarrier.size() > 1) {
				return SYSTEM.INT;
			} else if (operatingCarrier != null && !operatingCarrier.contains(requestingCarrier)) {
				return SYSTEM.INT;
			} else if (operatingCarrier == null) {
				// set if not found result.

				boolean isValidOndRequest = isValidOndRequest(ondArray[0], ondArray[1]);
				if (!isValidOndRequest) {
					log.error("Error with airport codes");
					throw new ModuleException("ibe.flight.availability.ond.error");
				}

				return SYSTEM.INT;
			}
		}

		return SYSTEM.AA;
	}

	public boolean isValidOndRequest(String origin, String destination) {

		boolean isValidOndRequest = false;

		Collection<String> airportCodeSet = getAirportCodeSet();

		addCityCodesToAirport(airportCodeSet);
		
		if (airportCodeSet.contains(origin) && airportCodeSet.contains(destination)) {
			isValidOndRequest = true;
		}

		return isValidOndRequest;

	}

	private Collection<String> getAirportCodeSet() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return platformDAO.getAllAirportCodes();
	}

	/**
	 * Retrieving carrier by origin and destination
	 * 
	 * @param originAndDestination
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> getOperatingCarriers(String originAndDestination) {
		List<String> operatingCarriers = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = getOndsCacheUDF();
			aerospikeClient = getAerospikeClient();
			if (!getUdfPopulated().get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (getUdfPopulated().get(udf)) {

				Key key = new Key(getOndNamespace(), AerospikeInternalConstants.OndForCash.SET_NAME, originAndDestination);

				Record record = aerospikeClient.get(null, key);
				// TODO until ond cash udf generate for LCC
				if (record == null) {
					return null;
				}
				operatingCarriers = (List<String>) record.bins.get(AerospikeInternalConstants.SET_OndCashing.OPERATING_CARRIER);

				if (operatingCarriers.isEmpty()) {
					log.info("No results found");
				}
			}

		} catch (Exception ex) {
			// throw ex;
		}
		// finally {
		// if (aerospikeClient != null) {
		// aerospikeClient.close();
		// }
		// }

		return operatingCarriers;
	}

	public String getOndNamespace() {
		return ondNamespace;
	}

	public void setOndNamespace(String ondNamespace) {
		this.ondNamespace = ondNamespace;
	}

	public String getOndsCacheUDF() {
		return ondsCacheUDF;
	}

	public void setOndsCacheUDF(String ondsCacheUDF) {
		this.ondsCacheUDF = ondsCacheUDF;
	}

	/**
	 * @return the systemEnabled
	 */
	public boolean getSystemEnabled() {
		return systemEnabled;
	}

	/**
	 * @param systemEnabled
	 *            the systemEnabled to set
	 */
	public void setSystemEnabled(boolean systemEnabled) {
		this.systemEnabled = systemEnabled;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the defaultWriteTimeout
	 */
	public int getDefaultWriteTimeout() {
		return defaultWriteTimeout;
	}

	/**
	 * @param defaultWriteTimeout
	 *            the defaultWriteTimeout to set
	 */
	public void setDefaultWriteTimeout(int defaultWriteTimeout) {
		this.defaultWriteTimeout = defaultWriteTimeout;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the ipToCountryUDF
	 */
	public String getIpToCountryUDF() {
		return ipToCountryUDF;
	}

	/**
	 * @param ipToCountryUDF
	 *            the ipToCountryUDF to set
	 */
	public void setIpToCountryUDF(String ipToCountryUDF) {
		this.ipToCountryUDF = ipToCountryUDF;
	}

	/**
	 * @return the udfExtension
	 */
	public String getUdfExtension() {
		return udfExtension;
	}

	/**
	 * @param udfExtension
	 *            the udfExtension to set
	 */
	public void setUdfExtension(String udfExtension) {
		this.udfExtension = udfExtension;
	}

	public String getAppParamUDF() {
		return appParamUDF;
	}

	public void setAppParamUDF(String appParamUDF) {
		this.appParamUDF = appParamUDF;
	}

	private AerospikeClient getAerospikeClient() {
		if (aerospikeClient == null) {
			aerospikeClient = new AerospikeClient(getHost(), getPort());
			LuaConfig.SourceDirectory = getModuleConfigPath();
		}
		return aerospikeClient;
	}

	public void populateData(AerospikeClient aerospikeClient, String udf) {
		String localUDFLocatioc = getModuleConfigPath() + udf + getUdfExtension();
		String serverUDFLocation = udf + getUdfExtension();
		try {
			RegisterTask task = aerospikeClient.register(null, localUDFLocatioc, serverUDFLocation, Language.LUA);
			task.waitTillComplete();
			udfPopulated.put(udf, Boolean.TRUE);
		} catch (Exception e) {
			log.error("Error in registering " + udf + " UDF");
		}

	}

	private String getModuleConfigPath() {
		StringBuilder sb = new StringBuilder();
		sb.append(PlatformConstants.getConfigRootAbsPath()).append(File.separator).append("repository").append(File.separator)
				.append("modules").append(File.separator).append("commons").append(File.separator)
				.append(AerospikeInternalConstants.UDF_FOLDER).append(File.separator);
		return sb.toString();
	}

	private Map<String, Boolean> getUdfPopulated() {
		if (udfPopulated == null) {
			udfPopulated = new HashMap<String, Boolean>();
			this.udfPopulated.put(getIpToCountryUDF(), Boolean.FALSE);
			this.udfPopulated.put(getOndsCacheUDF(), Boolean.FALSE);
			this.udfPopulated.put(getAppParamUDF(), Boolean.FALSE);
		}
		return udfPopulated;
	}
	
	private Collection<String> getCityCodeSet() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return platformDAO.getAllAvailableCityCodes();
	}
	
	private void addCityCodesToAirport(Collection<String> airportCodeSet) {
		if (AppSysParamsUtil.enableCityBasesFunctionality() && airportCodeSet != null && !airportCodeSet.isEmpty()) {
			Collection<String> cityCodeSet = getCityCodeSet();
			if (cityCodeSet != null && !cityCodeSet.isEmpty()) {
				airportCodeSet.addAll(cityCodeSet);
			}
		}
	}

}
