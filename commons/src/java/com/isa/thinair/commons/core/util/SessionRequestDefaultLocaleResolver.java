package com.isa.thinair.commons.core.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

public class SessionRequestDefaultLocaleResolver extends SessionLocaleResolver {
	
	/**
	 * Determine the default locale for the given request,
	 * Called if no Locale session attribute has been found.
	 * <p>The default implementation returns the specified default locale,
	 * if any, else falls back to the request's accept-header locale.
	 * @param request the request to resolve the locale for
	 * @return the default locale (never {@code null})
	 * @see #setDefaultLocale
	 * @see javax.servlet.http.HttpServletRequest#getLocale()
	 * 
	 * The spring method has been overridden to get request
	 * locale first 
	 */
	protected Locale determineDefaultLocale(HttpServletRequest request) {
		Locale locale = request.getLocale();
		if (locale == null) {
			locale = getDefaultLocale();
		}
		return locale;
	}

}
