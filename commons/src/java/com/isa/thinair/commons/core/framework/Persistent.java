/*
 * Created on Jul 4, 2005
 *
 */
package com.isa.thinair.commons.core.framework;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 
 * @author kasun
 */

public abstract class Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6765219761980057897L;
	private long version = -1;

	public Persistent() {
		super();
	}

	/**
	 * @hibernate.version type = "long" unsaved-value = "negative"
	 * @return long
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            The version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
