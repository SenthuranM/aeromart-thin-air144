package com.isa.thinair.commons.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * 
 * @author nafly
 * @isa.module.service-interface module-name="commons" description="The commons module"
 */
public class CommonsService extends DefaultModule {

}
