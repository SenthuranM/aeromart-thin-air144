/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.io.File;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * @author Nasly
 */
public class TemplateEngine {
	private final Log log = LogFactory.getLog(getClass());

	private static String templatesRootDir = PlatformConstants.getConfigRootAbsPath() + File.separator + "templates";

	public TemplateEngine() {
		try {
			Properties p = new Properties();
			p.setProperty("resource.loader", "file");
			p.setProperty("file.resource.loader.path", templatesRootDir);
			p.put("input.encoding", "UTF-8");
			p.put("output.encoding", "UTF-8");
			Velocity.init(p);
		} catch (Exception ex) {
			throw new ModuleRuntimeException(ex, "module.runtime.error");
		}
	}

	@SuppressWarnings("rawtypes")
	public Writer writeTemplate(HashMap parameters, String templateFileName, Writer writer) {
		try {
			VelocityContext context = new VelocityContext();
			Iterator it = parameters.keySet().iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				context.put(key, parameters.get(key));
			}

			Template template = null;
			try {
				if (log.isDebugEnabled()) {
					log.debug("Templates root directory = " + templatesRootDir);
					log.debug("Template file = " + templateFileName);
				}
				template = Velocity.getTemplate(templateFileName, "UTF-8");
			} catch (ResourceNotFoundException rnfe) {
				log.error("Cannot find template " + templateFileName, rnfe);
			} catch (ParseErrorException pee) {
				log.error("Syntax error in template " + templateFileName, pee);
			}

			if (template != null)
				template.merge(context, writer);
			writer.flush();
		} catch (Exception e) {
			log.error("Exception in processing template ", e);
		}
		return writer;
	}
}