/**
 * 
 */
package com.isa.thinair.commons.core.util;

import com.isa.thinair.commons.core.persistence.dao.CommonsHibernateDAO;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author nafly
 *
 */
public class CommonsDAOUtil {
	private static final String COMMON_HIBERNATE_DAO_PROXY = "CommonsHibernateDAOProxy";
	private static final String MODULE_NAME = "commons";

	public static CommonsHibernateDAO getCommonsHibernateDAO() {
		return (CommonsHibernateDAO) ModuleFramework.getInstance().getBean(MODULE_NAME, COMMON_HIBERNATE_DAO_PROXY);
	}

}
