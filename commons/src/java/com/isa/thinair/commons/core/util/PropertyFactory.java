/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Feb 25, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author sudheera
 * 
 *         PropertyFactory is used to load property files
 * 
 */

public class PropertyFactory {
	private ResourceBundle rb;

	/**
	 * initializes the property factory
	 * 
	 * @param resourceBundleFile
	 */
	public PropertyFactory(String resourceBundleFile) {
		loadBunndle(resourceBundleFile);
	}

	/**
	 * gets the property
	 * 
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		String value = null;

		try {
			value = rb.getString(key);
		} catch (MissingResourceException mre) {
		}

		return value;
	}

	/**
	 * loads the resouce bundle
	 * 
	 * @param resourceBundleFile
	 */
	private void loadBunndle(String resourceBundleFile) {

		rb = ResourceBundle.getBundle(resourceBundleFile);
	}
}