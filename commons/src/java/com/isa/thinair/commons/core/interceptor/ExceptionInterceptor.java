package com.isa.thinair.commons.core.interceptor;

//port org.springframework.

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.StaleObjectStateException;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.MessagesUtil;

/**
 * 
 * Byorn.
 * 
 */
public class ExceptionInterceptor implements ThrowsAdvice {

	private final Log log = LogFactory.getLog(getClass());

	public void afterThrowing(Exception ex) throws Throwable {
		Throwable t = ex.getCause();

		while (t != null) {
			if (t instanceof SQLException) {
				SQLException sqlex = (SQLException) t;
				String errorcode = MessagesUtil.getErrorCode(Integer.toString(sqlex.getErrorCode()));
				log.error("MESSEGE ERRORKEY=  " + errorcode + ",DATABASE ERROR CODE :" + sqlex.getErrorCode(), ex);
				throw new CommonsDataAccessException(ex, errorcode);
			} else {
				t = t.getCause();
			}

		}

		// added this to check if stale data exception is caught.
		if (ex instanceof StaleObjectStateException) {
			log.error("MESSEGE ERRORKEY= module.hibernate.staledata", ex);
			throw new CommonsDataAccessException(ex, "module.hibernate.staledata");

		}

		// added this to check if stale data exception is caught.
		if (ex instanceof HibernateOptimisticLockingFailureException) {
			log.error("MESSEGE ERRORKEY= module.hibernate.staledata", ex);
			throw new CommonsDataAccessException(ex, "module.hibernate.staledata");

		}

		if (ex instanceof ObjectRetrievalFailureException) {
			log.error("MESSEGE ERRORKEY = module.hibernate.objectnotfound", ex);
			throw new CommonsDataAccessException(ex, "module.hibernate.objectnotfound");

		}

		if (ex instanceof ObjectNotFoundException) {
			log.error("MESSEGE ERRORKEY = module.hibernate.objectnotfound", ex);
			throw new CommonsDataAccessException(ex, "module.hibernate.objectnotfound");
		}

		/**
		 * when you manually throw CommonsDataAccessException , the value t is null because getCause() is null,
		 * therefore i suspect the code would never enter inside the following if condition
		 */
		if (t instanceof CommonsDataAccessException) {
			CommonsDataAccessException cde = (CommonsDataAccessException) t;
			if (cde.getExceptionCode() == null) {
				cde.setExceptionCode("module.unidentified.error");
				throw cde;
			} else {
				throw cde;
			}

		}
		/*** **************** ****/

		/*** CODE WILL ENTER THIS SECTION if commons data access exception is thrown in dao level **/
		if (ex instanceof CommonsDataAccessException) {
			CommonsDataAccessException cde = (CommonsDataAccessException) ex;
			if (cde.getExceptionCode() == null) {
				cde.setExceptionCode("module.unidentified.error");
				throw cde;
			} else {
				throw cde;
			}

		}

		if (ex instanceof SQLException) {

			String errorcode = MessagesUtil.getErrorCode(Integer.toString(((SQLException) ex).getErrorCode()));
			log.error("MESSEGE ERRORKEY=  " + errorcode + ",DATABASE ERROR CODE :" + ((SQLException) ex).getErrorCode(), ex);
			throw new CommonsDataAccessException(ex, errorcode);
		}
		if (ex.getClass().getName().equals("com.isa.thinair.commons.api.exception.ModuleException")) {

			throw ex;
		}

		// if the program comes here it means the exception is not able to be identified by the interceptor.
		throw new CommonsDataAccessException(ex, "module.unidentified.error");

	}

}
