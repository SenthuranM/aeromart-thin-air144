package com.isa.thinair.commons.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SystemPropertyUtil {
	private static final String DEV_MODE = "com.isa.thinair.devmode";
	private static final String DIABLE_PAYMENT_BROKER = "com.isa.thinair.disable.paymentbroker";
	private static final String REMOVE_CARD_PAYMENT_DECIMALS = "com.isa.thinair.remove.cardpayment.decimals";
	private static final String ENABLE_MANUAL_SEGMENT_TRANSFER = "com.isa.thinair.enable.manual.segment.transfer";
	private static final String RES_DEBUG_FRONTEND = "com.isa.thinair.debug.frontend";
	private static final String SET_SMS_TO_EMAIL = "com.isa.thinair.set.smstoemail";
	private static final String USE_XML_CONFIGS = "com.isa.thinair.use.xmlconfigs";
	private static final String FORCE_ENABLE_LCC_CONNECTIVITY = "com.isa.thinair.force.enable.lcc.connectivity";
	private static final String INSTANCE_ID = "com.isa.thinair.instanceid";
	private static final String DEV_OVERRIDE_APP_PARAM = "com.isa.thinair.override.app.param";
	private static final String CREATE_NEW_BUILD_ID_WITH_EVERY_RESTART = "com.isa.thinair.build.id.with.every.restart";
	private static final String IS_PERF4J_ENABLE = "com.isa.thinair.perf4j.enable";
	private static final String DISABLE_PROXY_GLOBALLY = "com.isa.thinair.disable.proxy.globally"; 
	
	private static Log log = LogFactory.getLog(SystemPropertyUtil.class);

	private static void logWarning(boolean status, String paramKey) {
		if (status) {
			log.warn("#### WARNING #### " + paramKey + " param is set to true");
		}
	}

	public static boolean isDevMode() {
		boolean status = "true".equals(System.getProperty(DEV_MODE));

		logWarning(status, DEV_MODE);

		return status;
	}

	public static boolean isDevOverrideAppParam() {
		boolean status = "true".equals(System.getProperty(DEV_OVERRIDE_APP_PARAM));
		
		logWarning(status, DEV_OVERRIDE_APP_PARAM);

		return status;
	}

	/**
	 * returns whether the payment broker is disabled.
	 * 
	 * @return
	 */
	public static boolean isPaymentBokerDisabled() {
		boolean status = "true".equals(System.getProperty(DIABLE_PAYMENT_BROKER));

		logWarning(status, DIABLE_PAYMENT_BROKER);

		return status;
	}

	/**
	 * returns whether the credit card amount to be rounded.
	 * 
	 * @return
	 */
	public static boolean checkRemoveCardPaymentDecimals() {
		boolean status = "true".equals(System.getProperty(REMOVE_CARD_PAYMENT_DECIMALS));

		logWarning(status, REMOVE_CARD_PAYMENT_DECIMALS);

		return status;
	}

	/**
	 * Returns whether the manual segment transfer is enabled or not
	 * 
	 * @return
	 */
	public static boolean isManualSegmentTransferEnabled() {
		boolean status = "true".equals(System.getProperty(ENABLE_MANUAL_SEGMENT_TRANSFER));
		logWarning(status, ENABLE_MANUAL_SEGMENT_TRANSFER);

		return status;
	}

	/**
	 * Returns whether to debug front end or not
	 * 
	 * @return
	 */
	public static String isDebugFrontend() {
		return System.getProperty(RES_DEBUG_FRONTEND);
	}

	/**
	 * Returns whether to send sms as emails for testing purpose
	 * 
	 * @return
	 */
	public static boolean isSmsToEmail() {
		boolean status = "true".equals(System.getProperty(SET_SMS_TO_EMAIL));
		logWarning(status, SET_SMS_TO_EMAIL);

		return status;
	}

	public static boolean isXMLConfigsEnabled() {
		boolean status = "true".equals(System.getProperty(USE_XML_CONFIGS));
		logWarning(status, USE_XML_CONFIGS);

		return status;
	}

	public static boolean isForceEnableLccConnectivity() {
		boolean status = "true".equals(System.getProperty(FORCE_ENABLE_LCC_CONNECTIVITY));
		logWarning(status, FORCE_ENABLE_LCC_CONNECTIVITY);

		return status;
	}

	public static String getInstanceId() {
		return System.getProperty(INSTANCE_ID);
	}
	

	public static boolean isCreateNewBuildIdWithEveryRestart() {
		boolean status = "true".equals(System.getProperty(CREATE_NEW_BUILD_ID_WITH_EVERY_RESTART));
		logWarning(status, CREATE_NEW_BUILD_ID_WITH_EVERY_RESTART);

		return status;
	}
	
	public static boolean isPerf4JEnable() {
		boolean status = "true".equals(System.getProperty(IS_PERF4J_ENABLE));
		logWarning(status, IS_PERF4J_ENABLE);

		return status;
	}

	public static boolean isDisableProxyGlobally() {
		boolean status = "true".equals(System.getProperty(DISABLE_PROXY_GLOBALLY));
		
		logWarning(status, DISABLE_PROXY_GLOBALLY);

		return status;
	}
	
}
