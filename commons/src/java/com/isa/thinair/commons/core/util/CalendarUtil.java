/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * Utility class for common date related functionalities
 * 
 * @author Lasantha Pambagoda, Byorn the man
 */
public class CalendarUtil {
	private static final Log log = LogFactory.getLog(CalendarUtil.class);
	public static final String PATTERN1 = "dd/MM/yyyy";
	public static final String PATTERN2 = "dd-MM-yyyy";
	public static final String PATTERN3 = "MM/dd/yyyy";
	public static final String PATTERN4 = "MM/dd/yy";
	public static final String PATTERN5 = "dd-MM-yyyy hh:mm";
	public static final String PATTERN6 = "ddMMMyy";
	public static final String PATTERN7 = "yyyy-MM-dd HH:mm:ss.S";
	public static final String PATTERN8 = "dd/MM/yyyy HH:mm";
	public static final String PATTERN9 = "EEE, MMMMM dd, yyyy";
	public static final String PATTERN10 = "h:mm a";
	public static final String PATTERN11 = "dd-MM-yyyy HH:mm";
	public static final String PATTERN12 = "dd-MMM-yyyy";
	public static final String PATTERN13 = "HH:mm";
	public static final String PATTERN14 = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String PATTERN15 = "dd-MM-yy HH.mm.ss";

	public static Date addHours(Date zuluDate, int hours) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		calander.add(GregorianCalendar.HOUR, hours);

		return calander.getTime();
	}

	public static Date addMinutes(Date zuluDate, int minutes) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		calander.add(GregorianCalendar.MINUTE, minutes);
		return calander.getTime();
	}

	public static Date addMinutesToCurrentZuluTime(int minutes) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(getCurrentSystemTimeInZulu());
		calander.add(GregorianCalendar.MINUTE, minutes);
		return calander.getTime();
	}

	public static Date addSeconds(Date zuluDate, int seconds) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		calander.add(GregorianCalendar.SECOND, seconds);
		return calander.getTime();
	}

	public static Date addMilliSeconds(Date zuluDate, long miliseconds) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTimeInMillis(zuluDate.getTime() + miliseconds);
		return calander.getTime();
	}

	public static Date addADayInHourMinSecs(Date zuluDate) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		calander.add(GregorianCalendar.HOUR, 23);
		calander.add(GregorianCalendar.MINUTE, 59);
		calander.add(GregorianCalendar.SECOND, 59);
		return calander.getTime();
	}

	public static String formatForSQL_toDate(Timestamp t) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(t);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String formattedDate = dateFormat.format(calendar.getTime());
		formattedDate = "'" + formattedDate + "'" + "," + "'dd-mon-yyyy HH24:mi:ss'";
		return formattedDate;
	}

	public static String formatForSQL_toDate(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String formattedDate = dateFormat.format(calendar.getTime());
		formattedDate = "'" + formattedDate + "'" + "," + "'dd-mon-yyyy HH24:mi:ss'";
		return formattedDate;
	}

	public static String formatDateForSQL_toDate(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = dateFormat.format(calendar.getTime());
		formattedDate = "'" + formattedDate + "'" + "," + "'dd-mon-yyyy'";
		return formattedDate;
	}

	public static String formatSQLDate(Date date) {
		String sqlDate = null;
		if (date != null) {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			sqlDate = dateFormat.format(date);
		}
		return sqlDate;
	}

	public static Date formatForSQL(Date d, String format) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat(format);

		String source = dateFormat.format(d);

		return dateFormat.parse(source);

	}
	
	public static Date safeFormat(Date date, String format) {
		Date formatedDate = null;
		try {
			formatedDate = formatForSQL(date, format);
		} catch (ParseException e) {
			formatedDate = date;
		}
		return formatedDate;
	}

	public static Date getLastMomentOfPreviousDaY() {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, -1); // set to previous day
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	/**
	 * methood to get the end time of a given day
	 * 
	 * @param date
	 * @return
	 */
	public static Date getLastMomentOfDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	public static int getLastDayOfYearAndMonth(int year, int month) {

		if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12 | month == 0) {
			return 31;
		} else if (month == 4 | month == 6 | month == 9 | month == 11) {
			return 30;
		}

		// if february
		else if (month == 2) {

			if (year % 4 == 0) {
				return 29;
			}

			else {

				return 28;
			}

		}
		return -1;
	}

	/**
	 * method to get the day number
	 * 
	 * @param dayOfWeek
	 * @return day number
	 */
	public static int getDayNumber(DayOfWeek dayOfWeek) {

		int standedDayNumber = 0;

		// get the standerd day number for the given day
		if (dayOfWeek.equals(DayOfWeek.SUNDAY)) {
			standedDayNumber = 0;
		}
		if (dayOfWeek.equals(DayOfWeek.MONDAY)) {
			standedDayNumber = 1;
		}
		if (dayOfWeek.equals(DayOfWeek.TUESDAY)) {
			standedDayNumber = 2;
		}
		if (dayOfWeek.equals(DayOfWeek.WEDNESDAY)) {
			standedDayNumber = 3;
		}
		if (dayOfWeek.equals(DayOfWeek.THURSDAY)) {
			standedDayNumber = 4;
		}
		if (dayOfWeek.equals(DayOfWeek.FRIDAY)) {
			standedDayNumber = 5;
		}
		if (dayOfWeek.equals(DayOfWeek.SATURDAY)) {
			standedDayNumber = 6;
		}

		int offset = getOffsetFromStanderdDayNumber();

		int convertedDayNumber = shiftDayNumberFrom(standedDayNumber, offset);

		return convertedDayNumber;
	}

	public static DayOfWeek getDayFromDayNumber(int dayNumber) {

		int offset = 7 - getOffsetFromStanderdDayNumber();
		int convertedDayNumber = shiftDayNumberFrom(dayNumber, offset);

		DayOfWeek dayOfWeek = DayOfWeek.MONDAY;

		if (convertedDayNumber == 0) {
			dayOfWeek = DayOfWeek.SUNDAY;
		} else if (convertedDayNumber == 1) {
			dayOfWeek = DayOfWeek.MONDAY;
		} else if (convertedDayNumber == 2) {
			dayOfWeek = DayOfWeek.TUESDAY;
		} else if (convertedDayNumber == 3) {
			dayOfWeek = DayOfWeek.WEDNESDAY;
		} else if (convertedDayNumber == 4) {
			dayOfWeek = DayOfWeek.THURSDAY;
		} else if (convertedDayNumber == 5) {
			dayOfWeek = DayOfWeek.FRIDAY;
		} else if (convertedDayNumber == 6) {
			dayOfWeek = DayOfWeek.SATURDAY;
		}

		return dayOfWeek;
	}

	public static Date getTimeAddedDate(Date date, Date time) {

		GregorianCalendar timeCalender = new GregorianCalendar();
		timeCalender.setTime(time);

		GregorianCalendar dateCalender = new GregorianCalendar();
		dateCalender.setTime(date);

		dateCalender.set(GregorianCalendar.HOUR_OF_DAY, timeCalender.get(GregorianCalendar.HOUR_OF_DAY));
		dateCalender.set(GregorianCalendar.MINUTE, timeCalender.get(GregorianCalendar.MINUTE));
		dateCalender.set(GregorianCalendar.SECOND, timeCalender.get(GregorianCalendar.SECOND));

		return dateCalender.getTime();
	}

	/**
	 * get frequncy for the standerd day numbers
	 * 
	 * @param frequency
	 * @return
	 */
	public Frequency getFrequencyForStanderdDayNumbers(Frequency frequency) {

		Frequency fqn = FrequencyUtil.getCopyOfFrequency(frequency);
		int offset = getOffsetFromStanderdDayNumber();
		offset = (offset != 0) ? (7 - offset) : 0;
		return FrequencyUtil.shiftFrequencyBy(fqn, offset);
	}

	/**
	 * method to get the day number form date
	 * 
	 * @param dayOfWeek
	 * @return day number
	 */
	public static int getDayNumber(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		// calander.get(GregorianCalendar.DAY_OF_WEEK) - 1 becouse calander
		// gives 1 for
		// SUNDAY but we starrt day numbers form 0
		// 1 <= GregorianCalendar.DAY_OF_WEEK <= 7 but 0 <= our day of week <= 6
		int standedDayNumber = calander.get(GregorianCalendar.DAY_OF_WEEK) - 1;

		int offset = getOffsetFromStanderdDayNumber();

		int convertedDayNumber = (standedDayNumber - offset < 0) ? (standedDayNumber + 7 - offset) : (standedDayNumber - offset);

		return convertedDayNumber;
	}

	/**
	 * method to get frequency from given day list
	 * 
	 * @param daysList
	 */
	public static Frequency getFrequencyFromDays(Collection<DayOfWeek> daysList) {

		Iterator<DayOfWeek> days = daysList.iterator();
		Frequency frequency = new Frequency();

		while (days.hasNext()) {

			DayOfWeek day = days.next();
			int dayNumber = getDayNumber(day);
			if (dayNumber == 0) {
				frequency.setDay0(true);
			}
			if (dayNumber == 1) {
				frequency.setDay1(true);
			}
			if (dayNumber == 2) {
				frequency.setDay2(true);
			}
			if (dayNumber == 3) {
				frequency.setDay3(true);
			}
			if (dayNumber == 4) {
				frequency.setDay4(true);
			}
			if (dayNumber == 5) {
				frequency.setDay5(true);
			}
			if (dayNumber == 6) {
				frequency.setDay6(true);
			}
		}

		return frequency;
	}

	public static DayOfWeek getDay(int dayNumber) {

		List<DayOfWeek> daysInStandardOrder = new ArrayList<DayOfWeek>();
		daysInStandardOrder.add(DayOfWeek.SUNDAY);
		daysInStandardOrder.add(DayOfWeek.MONDAY);
		daysInStandardOrder.add(DayOfWeek.TUESDAY);
		daysInStandardOrder.add(DayOfWeek.WEDNESDAY);
		daysInStandardOrder.add(DayOfWeek.THURSDAY);
		daysInStandardOrder.add(DayOfWeek.FRIDAY);
		daysInStandardOrder.add(DayOfWeek.SATURDAY);

		int offset = getOffsetFromStanderdDayNumber();
		int dayNumberWithOffset = ((dayNumber + offset) % 7);
		Object objDayOfWeek = daysInStandardOrder.get(dayNumberWithOffset);

		if (objDayOfWeek instanceof DayOfWeek) {
			return (DayOfWeek) objDayOfWeek;
		} else {
			return null;
		}
	}
	
	
	public static DayOfWeek getIATAStandardDay(int dayNumber) {
		List<DayOfWeek> daysInStandardOrder = new ArrayList<DayOfWeek>();
		daysInStandardOrder.add(DayOfWeek.MONDAY);
		daysInStandardOrder.add(DayOfWeek.TUESDAY);
		daysInStandardOrder.add(DayOfWeek.WEDNESDAY);
		daysInStandardOrder.add(DayOfWeek.THURSDAY);
		daysInStandardOrder.add(DayOfWeek.FRIDAY);
		daysInStandardOrder.add(DayOfWeek.SATURDAY);
		daysInStandardOrder.add(DayOfWeek.SUNDAY);

		dayNumber = dayNumber - 1;

		if (dayNumber < 0 || dayNumber >= daysInStandardOrder.size()) {
			dayNumber = 0;
		}
		DayOfWeek objDayOfWeek = daysInStandardOrder.get(dayNumber);

		if (objDayOfWeek != null) {
			return objDayOfWeek;
		}
		return null;
	}
	

	public static Collection<DayOfWeek> getDaysFromFrequency(Frequency frequency) {
		Collection<DayOfWeek> days = new ArrayList<DayOfWeek>();
		if (frequency.getDay0()) {
			days.add(getDay(0));
		}
		if (frequency.getDay1()) {
			days.add(getDay(1));
		}
		if (frequency.getDay2()) {
			days.add(getDay(2));
		}
		if (frequency.getDay3()) {
			days.add(getDay(3));
		}
		if (frequency.getDay4()) {
			days.add(getDay(4));
		}
		if (frequency.getDay5()) {
			days.add(getDay(5));
		}
		if (frequency.getDay6()) {
			days.add(getDay(6));
		}

		return days;
	}

	/**
	 * method to convert local date to the zulu date
	 * 
	 * @param localDate
	 * @param isOffsetAddition
	 * @param offsetInMins
	 * @param isDstAddition
	 * @param dstInMins
	 * @return zuludate
	 */
	public static Date getZuluDate(Date localDate, int offsetInMins, int dstInMins) {

		int offsetDiff = offsetInMins * -1;
		int dstDiff = dstInMins * -1;

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(localDate);
		calander.add(GregorianCalendar.MINUTE, offsetDiff + dstDiff);

		return calander.getTime();
	}

	/**
	 * method to convert local date to the zulu date by timepstamp
	 * 
	 * @param localDate
	 * @param isOffsetAddition
	 * @param offsetInMins
	 * @param isDstAddition
	 * @param dstInMins
	 * @return zuludate
	 */
	public static Timestamp getZuluTimestamp(Timestamp localDate, int offsetInMins, int dstInMins) {

		int offsetDiff = offsetInMins * -1;
		int dstDiff = dstInMins * -1;

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(localDate);
		calander.add(GregorianCalendar.MINUTE, offsetDiff + dstDiff);

		return (new Timestamp(calander.getTime().getTime()));
	}

	/**
	 * method to convert zulu date to the local date
	 * 
	 * @param localDate
	 * @param isOffsetAddition
	 * @param offsetInMins
	 * @param isDstAddition
	 * @param dstInMins
	 * @return zuludate
	 */
	public static Date getLocalDate(Date zuluDate, int offsetInMins, int dstInMins) {

		int offsetDiff = offsetInMins;
		int dstDiff = dstInMins;

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		calander.add(GregorianCalendar.MINUTE, offsetDiff + dstDiff);

		return calander.getTime();
	}

	/**
	 * methood to get the start time of a given day
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartTimeOfDate(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calander.set(GregorianCalendar.MINUTE, 0);
		calander.set(GregorianCalendar.SECOND, 0);
		calander.set(GregorianCalendar.MILLISECOND, 0);

		return calander.getTime();
	}

	/**
	 * methood to get the end time of a given day
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndTimeOfDate(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.HOUR_OF_DAY, 23);
		calander.set(GregorianCalendar.MINUTE, 59);
		calander.set(GregorianCalendar.SECOND, 59);

		return calander.getTime();
	}

	/**
	 * Method to get the half time of a given date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getHalfTimeOfDate(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.HOUR_OF_DAY, 12);
		calander.set(GregorianCalendar.MINUTE, 00);
		calander.set(GregorianCalendar.SECOND, 00);
		calander.set(GregorianCalendar.MILLISECOND, 0);

		return calander.getTime();
	}

	/**
	 * method to parse date as string to date as java.util.Date
	 * 
	 * @param date
	 * @param dateFormat
	 * @return parsed date
	 * @throws ParseException
	 */
	public static Date getParsedTime(String date, String dateFormat) throws ParseException {

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		Date parsedDate = format.parse(date);

		return parsedDate;
	}

	/**
	 * method to parse date as string to date as java.util.Date only for time
	 * 
	 * @param date
	 * @param dateFormat
	 * @return parsed date
	 * @throws ParseException
	 */
	public static Date getOnlyTime(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.YEAR, 1970);
		calander.set(GregorianCalendar.MONTH, 0);
		calander.set(GregorianCalendar.DATE, 1);

		return calander.getTime();
	}

	/**
	 * method to get offset + time + date
	 * 
	 * @param date
	 * @param time
	 * @param offset
	 * @return
	 */
	public static Date getOfssetAndTimeAddedDate(Date date, Date time, int offset) {

		Date timeAddedDate = getTimeAddedDate(date, time);
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(timeAddedDate);

		calander.add(GregorianCalendar.DATE, offset);

		return calander.getTime();
	}

	/**
	 * method to get offset in milli seconds + date
	 * 
	 * @param date
	 * @param time
	 * @param offset
	 * @return
	 */
	public static Date getOfssetInMilisecondAddedDate(Date date, int offset) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.add(GregorianCalendar.MILLISECOND, offset);
		return calander.getTime();
	}

	/**
	 * method to get offset + time
	 * 
	 * @param date
	 * @param time
	 * @param offset
	 * @return
	 */
	public static Date getOfssetAddedTime(Date time, int offset) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(time);

		calander.add(GregorianCalendar.DATE, offset);

		return calander.getTime();
	}

	/**
	 * method to get the day difference between two dates
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return dayDiff
	 */
	public static int daysUntil(Date from, Date to) {

		int daysSinceEpochToDate = daysSinceEpoch(to);
		int daysSinceEpochFromDate = daysSinceEpoch(from);

		return daysSinceEpochToDate - daysSinceEpochFromDate;
	}

	/**
	 * method to check is leap year or not
	 * 
	 * @param year
	 * @return
	 */
	private static boolean isLeapYear(int year) {
		return (year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0));
	}

	/**
	 * method to get the offset
	 * 
	 * @return offset
	 */
	public static int getOffsetFromStanderdDayNumber() {

		// get the offset from the system parameter
		LookupService lookup = LookupServiceFactory.getInstance();
		GlobalConfig config = (GlobalConfig) lookup.getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
		String offsetStr = config.getBizParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK);
		int offset = (offsetStr != null) ? (new Integer(offsetStr)).intValue() % 7 : 0;

		return offset;
	}

	/**
	 * method to get days since epoch
	 * 
	 * @param day
	 * @return
	 */
	private static int daysSinceEpoch(Date date) {

		GregorianCalendar day = new GregorianCalendar();
		day.setTime(date);

		int year = day.get(GregorianCalendar.YEAR);
		int month = day.get(GregorianCalendar.MONTH);
		int daysThisYear = cumulDaysToMonth[month] + day.get(GregorianCalendar.DAY_OF_MONTH) - 1;

		if ((month > 1) && isLeapYear(year)) {
			daysThisYear++;
		}

		int daysSinceEpoch = daysToYear(year) + daysThisYear;

		return daysSinceEpoch;
	}

	/**
	 * method to get days to year
	 * 
	 * @param year
	 * @return
	 */
	private static int daysToYear(int year) {
		int daysToYear = (365 * year) + numLeapsToYear(year);
		return daysToYear;
	}

	/**
	 * method to get mum leaps to year
	 * 
	 * @param year
	 * @return
	 */
	private static int numLeapsToYear(int year) {
		int num4y = (year - 1) / 4;
		int num100y = (year - 1) / 100;
		int num400y = (year - 1) / 400;
		return num4y - num100y + num400y;
	}

	private static final int[] cumulDaysToMonth = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };

	private static int shiftDayNumberFrom(int currentDayNumber, int shift) {

		if (currentDayNumber < 0 && currentDayNumber > 6) {
			currentDayNumber = 0;
		}
		if (shift < 0 && shift > 6) {
			shift = 0;
		}

		int convertedDayNumber = (currentDayNumber - shift < 0) ? (currentDayNumber + 7 - shift) : (currentDayNumber - shift);
		return convertedDayNumber;
	}

	/**
	 * 
	 * @param parseDate
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	public static boolean isBetween(Date parseDate, Date fromDate, Date toDate) {

		boolean status = false;
		// GregorianCalendar(int year, int month, int date, int hour, int
		// minute, int second)

		GregorianCalendar compareDate = getCalendar(parseDate);
		GregorianCalendar dateFrom = getCalendar(fromDate);
		GregorianCalendar dateTo = getCalendar(toDate);

		if (compareDate.after(dateFrom) && compareDate.before(dateTo)) {
			status = true;
		}
		return status;
	}

	public static boolean isGreaterThan(Date parseDate, Date compareWithDate) {

		boolean status = false;
		// GregorianCalendar(int year, int month, int date, int hour, int
		// minute, int second)

		GregorianCalendar parseDate1 = getCalendar(parseDate);
		GregorianCalendar dateFrom = getCalendar(compareWithDate);

		if (parseDate1.after(dateFrom)) {
			status = true;
		}
		return status;
	}

	public static boolean isLessThan(Date parseDate, Date compareWithDate) {

		boolean status = false;
		// GregorianCalendar(int year, int month, int date, int hour, int
		// minute, int second)

		GregorianCalendar parseDate1 = getCalendar(parseDate);
		GregorianCalendar dateFrom = getCalendar(compareWithDate);

		if (parseDate1.before(dateFrom)) {
			status = true;
		}
		return status;
	}

	public static boolean isSameDay(Date first, Date second) {

		boolean isSame = false;

		if (first != null && second != null) {
			GregorianCalendar firstCal = getCalendar(first);
			GregorianCalendar secondCal = getCalendar(second);

			if ((firstCal.get(GregorianCalendar.YEAR) == secondCal.get(GregorianCalendar.YEAR))
					&& (firstCal.get(GregorianCalendar.MONTH) == secondCal.get(GregorianCalendar.MONTH))
					&& (firstCal.get(GregorianCalendar.DATE) == secondCal.get(GregorianCalendar.DATE))) {
				isSame = true;
			}
		}

		return isSame;
	}

	public static boolean isDateEqual(Date dateOne, Date dateTwo) {
		GregorianCalendar calDateOne = new GregorianCalendar();
		calDateOne.setTime(dateOne);

		GregorianCalendar calDateTwo = new GregorianCalendar();
		calDateTwo.setTime(dateTwo);

		return ((calDateOne.get(Calendar.MINUTE) == calDateTwo.get(Calendar.MINUTE))
				&& (calDateOne.get(Calendar.HOUR_OF_DAY) == calDateTwo.get(Calendar.HOUR_OF_DAY))
				&& (calDateOne.get(Calendar.DATE) == calDateTwo.get(Calendar.DATE))
				&& (calDateOne.get(Calendar.MONTH) == calDateTwo.get(Calendar.MONTH)) && (calDateOne.get(Calendar.YEAR) == calDateTwo
				.get(Calendar.YEAR)));
	}
	
	public static boolean isEqualStartTimeOfDate(Date date1, Date date2) {

		GregorianCalendar calander1 = new GregorianCalendar();
		calander1.setTime(date1);
		GregorianCalendar calander2 = new GregorianCalendar();
		calander2.setTime(date2);
		return (calander1.get(GregorianCalendar.YEAR) == calander2.get(GregorianCalendar.YEAR)
				&& calander1.get(GregorianCalendar.MONTH) == calander2.get(GregorianCalendar.MONTH) && calander1
					.get(GregorianCalendar.DAY_OF_MONTH) == calander2.get(GregorianCalendar.DAY_OF_MONTH));
	}

	public static boolean isSameTime(Date first, Date second) {

		GregorianCalendar firstCal = getCalendar(first);
		GregorianCalendar secondCal = getCalendar(second);

		boolean isSame = false;

		if ((firstCal.get(GregorianCalendar.HOUR) == secondCal.get(GregorianCalendar.HOUR))
				&& (firstCal.get(GregorianCalendar.MINUTE) == secondCal.get(GregorianCalendar.MINUTE))) {
			isSame = true;
		}

		return isSame;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static GregorianCalendar getCalendar(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
	}

	public static Date getDate(int iYear, int iMonth, int iDay) {

		GregorianCalendar day = new GregorianCalendar();
		day.set(GregorianCalendar.YEAR, iYear);
		day.set(GregorianCalendar.MONTH, iMonth);
		day.set(GregorianCalendar.DAY_OF_MONTH, iDay);
		return day.getTime();
	}

	public static Date getCopyofDate(Date date) {

		Date copy = null;
		if (date != null) {
			copy = new Date(date.getTime());
		}
		return copy;
	}

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @param frq
	 * @return
	 */
	public static boolean frequencyMatchesDateRange(Date startDate, Date endDate, Frequency frq) {

		boolean freqOk = true;
		int daysInSchedule = CalendarUtil.daysUntil(startDate, endDate);

		if (daysInSchedule < 7) {
			int startDayNo = CalendarUtil.getDayNumber(startDate);
			int endDayNo = CalendarUtil.getDayNumber(endDate);

			if (frq.getDay0()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 0 && endDayNo >= 0) : !(endDayNo < 0 && startDayNo > 0);
			}
			if (frq.getDay1() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 1 && endDayNo >= 1) : !(endDayNo < 1 && startDayNo > 1);
			}
			if (frq.getDay2() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 2 && endDayNo >= 2) : !(endDayNo < 2 && startDayNo > 2);
			}
			if (frq.getDay3() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 3 && endDayNo >= 3) : !(endDayNo < 3 && startDayNo > 3);
			}
			if (frq.getDay4() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 4 && endDayNo >= 4) : !(endDayNo < 4 && startDayNo > 4);
			}
			if (frq.getDay5() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 5 && endDayNo >= 5) : !(endDayNo < 5 && startDayNo > 5);
			}
			if (frq.getDay6() && freqOk) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 6 && endDayNo >= 6) : !(endDayNo < 6 && startDayNo > 6);
			}
		}
		return freqOk;
	}

	/**
	 * Checks a day falling between two days (days are in numbers 0-6)
	 * 
	 * @param targetDay
	 * @param fromDay
	 * @param toDay
	 * @return
	 */
	public static boolean isBetween(int targetDay, int fromDay, int toDay) {
		return (fromDay <= toDay) ? (fromDay <= targetDay && toDay >= targetDay) : !(toDay < targetDay && toDay > fromDay);
	}

	public static Date getCurrentSystemTimeInZulu() {
		// it is assumed servers are configured with zulu time
		Calendar cal = new GregorianCalendar();

		if (log.isDebugEnabled()) {
			log.debug("Current system time in configured locale = " + cal.getTime());
			log.debug("Current system time GMT offset 			= " + cal.getTimeZone().getRawOffset());
			log.debug("Current system time DST saving 			= " + cal.getTimeZone().getDSTSavings());
		}

		cal.add(Calendar.MILLISECOND, -(cal.getTimeZone().getRawOffset() + cal.getTimeZone().getDSTSavings()));

		if (log.isDebugEnabled()) {
			log.debug("Current System time in GMT  				= " + cal.getTime());
		}

		return cal.getTime();
	}

	/**
	 * @author indika
	 * 
	 * @param pattern
	 *            string
	 * @param date
	 *            util.Date
	 * @return String
	 */
	/**
	 * Added additional logs to trace the date format issue. When the issue is resolved, pls remove the logs
	 * 
	 */
	public static String getDateInFormattedString(String pattern, Date date) {	
		String formattedDate =  null;		
		log.info("Caller ClassName : " + Thread.currentThread().getStackTrace()[2].getFileName() + " Tread Name : " + Thread.currentThread().getName()  + " Date : " + date);
		DateFormat df = new SimpleDateFormat(pattern);	
		formattedDate = df.format(date);
		log.info("Caller ClassName : " + Thread.currentThread().getStackTrace()[2].getFileName() + " Tread Name : " + Thread.currentThread().getName()  + " FormattedDate : " + formattedDate);
		return formattedDate;
	}

	/**
	 * @author indika
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static String getTimeDifference(Date from, Date to) {
		long differenceInMillis = to.getTime() - from.getTime();
		return milliSecs2TimeString(differenceInMillis);
	}

	public static int getTimeDifferenceInDays(Date from, Date to) {
		return millisecs2Days(getTimeDifferenceInMillis(getStartTimeOfDate(from), getStartTimeOfDate(to)));
	}

	/**
	 * @author indika
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static int getTimeDifferenceInHours(Date from, Date to) {
		return millisecs2Hours(getTimeDifferenceInMillis(from, to));
	}

	public static int getTimeDifferenceInMinutes(Date from, Date to) {
		return millisecs2Minutes(getTimeDifferenceInMillis(from, to));
	}

	/**
	 * @author indika
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static long getTimeDifferenceInMillis(Date from, Date to) {
		return to.getTime() - from.getTime();
	}

	/**
	 * @author indika
	 * 
	 * @param time
	 * @return
	 */
	public static String milliSecs2TimeString(long time) {
		int milliseconds = (int) (time % 1000);
		int seconds = (int) ((time / 1000) % 60);
		int minutes = (int) ((time / 60000) % 60);
		int hours = (int) ((time / 3600000) % 24);
		String millisecondsStr = (milliseconds < 10 ? "00" : (milliseconds < 100 ? "0" : "")) + milliseconds;
		String secondsStr = (seconds < 10 ? "0" : "") + seconds;
		String minutesStr = (minutes < 10 ? "0" : "") + minutes;
		String hoursStr = (hours < 10 ? "0" : "") + hours;
		return new String(hoursStr + ":" + minutesStr + ":" + secondsStr + "." + millisecondsStr);
	}

	public static int millisecs2Days(long time) {
		int days = (int) (time / 86400000);
		return days;
	}

	/**
	 * @author indika
	 * 
	 * @param time
	 * @return
	 */
	public static int millisecs2Hours(long time) {
		int hours = (int) (time / 3600000);
		return hours;
	}

	public static int millisecs2Minutes(long time) {
		int minutes = (int) (time / 60000);
		return minutes;
	}

	public static Date addDateAndTime(Date date, Date time) {
		GregorianCalendar gcDate = new GregorianCalendar();
		gcDate.clear();
		gcDate.setTime(date);

		int hours = 0;
		int mins = 0;
		int secs = 0;

		if (time != null) {
			GregorianCalendar gcTime = new GregorianCalendar();
			gcTime.clear();
			gcTime.setTime(time);

			hours = gcTime.get(Calendar.HOUR_OF_DAY);
			mins = gcTime.get(Calendar.MINUTE);
			secs = gcTime.get(Calendar.SECOND);
		}

		int year = gcDate.get(Calendar.YEAR);
		int month = gcDate.get(Calendar.MONTH);
		int day = gcDate.get(Calendar.DAY_OF_MONTH);

		GregorianCalendar gc2 = new GregorianCalendar(year, month, day, hours, mins, secs);
		return gc2.getTime();
	}

	public static Date addDateVarience(Date target, int varience) {
		GregorianCalendar gcDate = new GregorianCalendar();
		gcDate.clear();
		gcDate.setTime(target);

		int year = gcDate.get(Calendar.YEAR);
		int month = gcDate.get(Calendar.MONTH);
		int day = gcDate.get(Calendar.DAY_OF_MONTH) + varience;
		int hours = gcDate.get(Calendar.HOUR_OF_DAY);
		int mins = gcDate.get(Calendar.MINUTE);
		int secs = gcDate.get(Calendar.SECOND);

		GregorianCalendar gc2 = new GregorianCalendar(year, month, day, hours, mins, secs);
		return gc2.getTime();
	}

	public static Date truncateTime(Date date) {
		GregorianCalendar gcDate = new GregorianCalendar();
		gcDate.clear();

		gcDate.setTime(date);

		int year = gcDate.get(Calendar.YEAR);
		int month = gcDate.get(Calendar.MONTH);
		int day = gcDate.get(Calendar.DAY_OF_MONTH);

		GregorianCalendar gc2 = new GregorianCalendar(year, month, day);
		return gc2.getTime();
	}

	public static boolean isDateTimeNumericallyEqual(Date d1, Date d2) {
		boolean isEqual = true;

		GregorianCalendar calD1 = new GregorianCalendar();
		calD1.setTime(d1);

		GregorianCalendar calD2 = new GregorianCalendar();
		calD2.setTime(d2);

		if (calD1.get(Calendar.MINUTE) != calD2.get(Calendar.MINUTE)
				|| calD1.get(Calendar.HOUR_OF_DAY) != calD2.get(Calendar.HOUR_OF_DAY)
				|| calD1.get(Calendar.DAY_OF_MONTH) != calD2.get(Calendar.DAY_OF_MONTH)
				|| calD1.get(Calendar.MONTH) != calD2.get(Calendar.MONTH) || calD1.get(Calendar.YEAR) != calD2.get(Calendar.YEAR)) {
			isEqual = false;
		}

		return isEqual;
	}

	/**
	 * adds years, months,days,hours, minutes & seconds to a given date
	 * 
	 * @param date
	 * @param years
	 * @param months
	 * @param days
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @return
	 */
	public static Date add(Date date, int years, int months, int days, int hours, int minutes, int seconds) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(date);

		if (years != 0) {
			cal.add(Calendar.YEAR, years);
		}

		if (months != 0) {
			cal.add(Calendar.MONTH, months);
		}

		if (days != 0) {
			cal.add(Calendar.DAY_OF_MONTH, days);
		}

		if (hours != 0) {
			cal.add(Calendar.HOUR_OF_DAY, hours);
		}

		if (minutes != 0) {
			cal.add(Calendar.MINUTE, minutes);
		}

		if (seconds != 0) {
			cal.add(Calendar.SECOND, seconds);
		}

		return cal.getTime();

	}

	/**
	 * @param utilDate
	 * @return
	 */
	public static String formatDate(Date utilDate) {
		String format = "dd-MMM-yyyy HH:mm:ss";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(format);
			return sdFmt.format(utilDate);
		}
		return "";
	}

	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

	public static String getSQLToDateInputString(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String formattedDate = dateFormat.format(calendar.getTime());
		formattedDate = "'" + formattedDate + "'" + "," + "'dd-mon-yyyy HH24:mi:ss'";
		return formattedDate;

	}

	public static boolean isInEndBuffer(Date date, Integer startDurationMins, Integer endDurationMins) {
		Date now = new Date();
		Date startDate = null;
		Date endDate = null;

		if (startDurationMins != null) {
			startDate = add(date, 0, 0, 0, 0, startDurationMins.intValue() * -1, 0);
		}

		if (endDurationMins != null) {
			endDate = add(date, 0, 0, 0, 0, endDurationMins.intValue() * -1, 0);
		}

		return (startDate == null || now.after(startDate)) && (endDate == null || now.before(endDate));

	}

	public static boolean isBetweenIncludingLimits(Date targetDate, Date firstDate, Date secondDate) {

		boolean isBetweenOrEqualsToFirstDate = false;
		if (targetDate != null && firstDate != null && secondDate != null && firstDate.getTime() <= targetDate.getTime()
				&& targetDate.getTime() <= secondDate.getTime()) {
			isBetweenOrEqualsToFirstDate = true;
		}
		return isBetweenOrEqualsToFirstDate;
	}

	public static String formatDateYYYYMMDD(Date date) {
		String strDate = null;
		if (date != null) {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			strDate = dateFormat.format(date);
		}
		return strDate;
	}

	public static Date previousDate(Date date) {
		if (date == null) {
			date = new Date();
		}

		Calendar today = Calendar.getInstance();
		today.setTime(date);
		today.add(Calendar.DAY_OF_MONTH, -1);
		return today.getTime();
	}

	public static int getSecondsFromDate(Date zuluDate) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(zuluDate);
		return calander.get(Calendar.SECOND);

	}

	public static Date getTimeZoneConvertedDate(Date dateTime, String zoneName) {
		Calendar calTZ = new GregorianCalendar(TimeZone.getTimeZone(zoneName));
		calTZ.setTimeInMillis(dateTime.getTime());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, calTZ.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, calTZ.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, calTZ.get(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, calTZ.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, calTZ.get(Calendar.MINUTE));
		cal.set(Calendar.SECOND, calTZ.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, calTZ.get(Calendar.MILLISECOND));
		return cal.getTime();
	}

	public static Date getCurrentZuluDateTime() {
		String strTimeZone = "GMT";
		TimeZone timeZone = null;

		if (strTimeZone == null || "".equals(strTimeZone)) {
			timeZone = TimeZone.getDefault();
		} else {
			TimeZone.getTimeZone(strTimeZone);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(timeZone);

		return cal.getTime();
	}

	public static XMLGregorianCalendar asXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
		if (date != null) {
			DatatypeFactory dateTypeFactory = DatatypeFactory.newInstance();
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTimeInMillis(date.getTime());
			return dateTypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		}
		return null;
	}

	public static Date asDate(XMLGregorianCalendar gregorianCalendar) {
		if (gregorianCalendar != null) {
			return gregorianCalendar.toGregorianCalendar().getTime();
		}
		return null;
	}

	public static String convertDate(String strDate, String formatFrom, String formatTo) throws ParseException {
		String formatedDate = null;
		Date parsedDate = getParsedTime(strDate, formatFrom);
		SimpleDateFormat formatter = new SimpleDateFormat(formatTo);
		formatedDate = formatter.format(parsedDate);
		return formatedDate;
	}

	/**
	 * Method to get the date as string
	 * 
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String getFormattedDate(Date date, String dateFormat) {

		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		String formatedDate = formatter.format(date);

		return formatedDate;
	}

	/**
	 * get segment time as string element.
	 * 
	 * @param date
	 * @return
	 */
	public static String getSegmentTime(Date date) {

		String time = "";
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
			time = sdf.format(date);
		}
		return time;

	}

	/**
	 * Important : refer only the times , ignore the Day,Year,Month .
	 * 
	 * @param timeString
	 * @return
	 */
	public static Date getSegmentTime(String timeString) {

		if (timeString.length() != 4) {
			return null;
		}
		Date date = null;
		try {

			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeString.substring(0, 2)));
			cal.set(Calendar.MINUTE, Integer.parseInt(timeString.substring(2, 4)));
			cal.set(Calendar.SECOND, 00);
			cal.set(Calendar.YEAR, 0000);
			date = cal.getTime();

		} catch (Exception e) {
			return null;

		}
		return date;

	}

	/**
	 * Method Will compare the receiving date with current. if the month and day lesser than current month and date the
	 * value of year wil be next year
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date getSegmentDate(String dateStr) {
		Calendar recivedCal = null;

		try {
			Calendar cal = Calendar.getInstance();

			cal.set(Calendar.HOUR, 00);
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			cal.set(Calendar.HOUR_OF_DAY, 00);
			cal.set(Calendar.MILLISECOND, 00);
			
			int currentYear = cal.get(Calendar.YEAR);
			dateStr += currentYear;

			SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
			Date recivedDate = (Date) formatter.parse(dateStr);
			recivedCal = Calendar.getInstance();
			recivedCal.setTime(recivedDate);
			recivedCal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
			recivedCal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
			recivedCal.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));

			if (cal.compareTo(recivedCal) > 0) {
				recivedCal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
			}

			recivedCal.setTimeZone(TimeZone.getTimeZone("GMT"));
		} catch (Exception e) {
			return null;

		}
		return recivedCal.getTime();
	}

	public static Date parseDateTime01(String date, String time, boolean isFuture) {
		Date d = null;
		DateFormat f;
		Calendar c;
		Date tempDate;
		Calendar tempCal1, tempCal2;


		try {
			tempCal1 = Calendar.getInstance();
			tempCal2 = Calendar.getInstance();

			f = new SimpleDateFormat("ddMMM");
			tempDate = f.parse(date);

			c = Calendar.getInstance();
			c.setTime(tempDate);
			c.set(Calendar.YEAR, tempCal1.get(Calendar.YEAR));

			if (time != null) {
				f = new SimpleDateFormat("HHmm");
				tempDate = f.parse(time);

				tempCal2.setTime(tempDate);
				c.set(Calendar.HOUR_OF_DAY, tempCal2.get(Calendar.HOUR_OF_DAY));
				c.set(Calendar.MINUTE, tempCal2.get(Calendar.MINUTE));
			} else {
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
			}
			c.set(Calendar.SECOND, 0);

			if (isFuture && c.compareTo(tempCal1) < 0 ) {
				c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
			}

			d = c.getTime();
		} catch (ParseException e) {
			log.error("Date parse failed : " + date + "----" + time, e);
		}

		return d;
	}
	
	
	public static Date parseDate(String date, boolean isFuture) {
		Date parsedDate = null;
		DateFormat df;
		Calendar cal;
		Date tempDate;
		Calendar nowCal;

		try {

			if (date != null) {
				nowCal = Calendar.getInstance();
				nowCal.set(Calendar.HOUR_OF_DAY, 0);
				nowCal.set(Calendar.MINUTE, 0);
				nowCal.set(Calendar.SECOND, 0);
				nowCal.set(Calendar.MILLISECOND, 0);

				date = date.trim();
				StringBuilder sb = new StringBuilder();
				sb.append(date);
				if (date.length() > 5) {
					isFuture = false;
					df = new SimpleDateFormat(PATTERN6);
				} else {
					df = new SimpleDateFormat("ddMMMyyyy");
					if (date.length() < 5) {
						sb.append(nowCal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault()).toUpperCase());
					}
					sb.append(nowCal.get(Calendar.YEAR));
				}
				tempDate = df.parse(sb.toString());

				cal = Calendar.getInstance();
				cal.setTime(tempDate);

				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);

				if (isFuture && cal.compareTo(nowCal) < 0) {
					cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
				}

				parsedDate = cal.getTime();
			}

		} catch (ParseException e) {
			log.error("Date parse failed : " + date, e);
		}

		return parsedDate;
	}

	/**
	 * Method will return String representation of date. date format : ddMMMyy eg: 08JUN05
	 * 
	 * @param date
	 * @return
	 */
	public static String getAge(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		return sdf.format(date).toUpperCase().trim();
	}

	/**
	 * Method will return String representation of date. date format : DDMMM eg: 08JUN
	 * 
	 * @param date
	 * @return
	 */
	public static String getSegmentDayMonth(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMM");
		return sdf.format(date).toUpperCase().trim();
	}
	
	/**
	 * Method will return valid Frequency for the selected date range.
	 * 
	 * */
	public static Frequency getValidFrequencyMatchesDateRange(Date startDate, Date endDate, Frequency frq) {

		boolean freqOk = true;
		int daysInSchedule = CalendarUtil.daysUntil(startDate, endDate);

		if (daysInSchedule < 7) {
			int startDayNo = CalendarUtil.getDayNumber(startDate);
			int endDayNo = CalendarUtil.getDayNumber(endDate);

			if (frq.getDay0()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 0 && endDayNo >= 0) : !(endDayNo < 0 && startDayNo > 0);
				if (!freqOk) {
					frq.setDay0(false);
				}
			}
			if (frq.getDay1()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 1 && endDayNo >= 1) : !(endDayNo < 1 && startDayNo > 1);
				if (!freqOk) {
					frq.setDay1(false);
				}
			}
			if (frq.getDay2()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 2 && endDayNo >= 2) : !(endDayNo < 2 && startDayNo > 2);
				if (!freqOk) {
					frq.setDay2(false);
				}
			}
			if (frq.getDay3()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 3 && endDayNo >= 3) : !(endDayNo < 3 && startDayNo > 3);
				if (!freqOk) {
					frq.setDay3(false);
				}
			}
			if (frq.getDay4()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 4 && endDayNo >= 4) : !(endDayNo < 4 && startDayNo > 4);
				if (!freqOk) {
					frq.setDay4(false);
				}
			}
			if (frq.getDay5()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 5 && endDayNo >= 5) : !(endDayNo < 5 && startDayNo > 5);
				if (!freqOk) {
					frq.setDay5(false);
				}
			}
			if (frq.getDay6()) {
				freqOk = (startDayNo <= endDayNo) ? (startDayNo <= 6 && endDayNo >= 6) : !(endDayNo < 6 && startDayNo > 6);
				if (!freqOk) {
					frq.setDay6(false);
				}
			}
		}
		return frq;
	}

	/**
	 * Method to get time in millisecond of HH:MM type string
	 * 
	 * @param String
	 * @return Long
	 */
	public static Long convertHHMMtoMillis(String hHmmTimeStr) {
		String[] timeArr = hHmmTimeStr.split(":");
		Long hours = Long.parseLong(timeArr[0]);
		Long minutes = Long.parseLong(timeArr[1]);
		Long minReturnTransitionTime = hours * 60 * 60 * 1000 + minutes * 60 * 1000;
		return minReturnTransitionTime;
	}
	
	public static Date getEffectiveStartDate(Date startDate, Frequency frq) {
		int startDayNo = CalendarUtil.getDayNumber(startDate);
		Date newDay = startDate;
		boolean checkNextDay = true;

		if (frq.getDayCount(true) > 0 && frq.getDayCount(true) < 7) {
			while (checkNextDay) {
				boolean dayChecked = true;

				switch (startDayNo) {
				case 0:
					dayChecked = frq.getDay0();
					break;
				case 1:
					dayChecked = frq.getDay1();
					break;
				case 2:
					dayChecked = frq.getDay2();
					break;
				case 3:
					dayChecked = frq.getDay3();
					break;
				case 4:
					dayChecked = frq.getDay4();
					break;
				case 5:
					dayChecked = frq.getDay5();
					break;
				case 6:
					dayChecked = frq.getDay6();
					break;
				}
				if (!dayChecked) {

					newDay = CalendarUtil.addDateVarience(newDay, 1);
					startDayNo = CalendarUtil.getDayNumber(newDay);
				} else {
					checkNextDay = false;
				}
			}
		}

		return newDay;

	}

	public static Date getEffectiveStopDate(Date endDate, Frequency frq) {
		int endDayNo = CalendarUtil.getDayNumber(endDate);
		Date newDay = endDate;
		boolean checkNextDay = true;
		if (frq.getDayCount(true) > 0 && frq.getDayCount(true) < 7) {
			while (checkNextDay) {
				boolean dayChecked = true;

				switch (endDayNo) {
				case 0:
					dayChecked = frq.getDay0();
					break;
				case 1:
					dayChecked = frq.getDay1();
					break;
				case 2:
					dayChecked = frq.getDay2();
					break;
				case 3:
					dayChecked = frq.getDay3();
					break;
				case 4:
					dayChecked = frq.getDay4();
					break;
				case 5:
					dayChecked = frq.getDay5();
					break;
				case 6:
					dayChecked = frq.getDay6();
					break;
				}
				if (!dayChecked) {
					newDay = CalendarUtil.addDateVarience(newDay, -1);
					endDayNo = CalendarUtil.getDayNumber(newDay);
				} else {
					checkNextDay = false;
				}
			}
		}
		return newDay;

	}
	
	public static boolean isDateWithinSelectedDayOfOperation(Date startDate, Frequency frq) {

		boolean freqOk = false;

		int startDayNo = CalendarUtil.getDayNumber(startDate);

		if (frq.getDay0() && startDayNo == 0) {
			freqOk = true;
		} else if (frq.getDay1() && startDayNo == 1) {
			freqOk = true;
		} else if (frq.getDay2() && startDayNo == 2) {
			freqOk = true;
		} else if (frq.getDay3() && startDayNo == 3) {
			freqOk = true;
		} else if (frq.getDay4() && startDayNo == 4) {
			freqOk = true;
		} else if (frq.getDay5() && startDayNo == 5) {
			freqOk = true;
		} else if (frq.getDay6() && startDayNo == 6) {
			freqOk = true;
		}

		return freqOk;
	}

	public static int[] getYearMonthDay(String dateString) {
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));
		return new int[]{year, month, day};
	}

	public static String getMonthName(Date date) {
		Format formatter = new SimpleDateFormat("MMMM");
		return formatter.format(date);
	}

}
