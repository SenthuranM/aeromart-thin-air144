/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;

/**
 * 
 * @author MN byorn
 * @param <T>
 * 
 */
public abstract class PlatformBaseHibernateDaoSupport extends PlatformHibernateDaoSupport {
	private final Log log = LogFactory.getLog(getClass());
	private Map<String, Object> hqlParmsToCountRows;
	
	private Session session;


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Page getPagedData(List<ModuleCriterion> criteria, int startIndex, int pageSize, Class<?> clazz, List<String> orderByFields) {
		log.debug("Begin PlatformBaseHibernateDaoSupport.getPagedData()");
		Collection results = CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(clazz), orderByFields)
				.setFirstResult(startIndex).setMaxResults(pageSize).list();

		int totalRecords = -1;
		totalRecords = getCount(criteria, clazz);
		Page pagedResults = new Page(totalRecords, startIndex, pageSize, results);
		return pagedResults;
	}
	

	@SuppressWarnings({ "rawtypes" })
	public int getCount(List<ModuleCriterion> criteriaList, Class clazz) {
		StringBuffer query = new StringBuffer();
		query.append("select count(*) from " + clazz.getName() + " as M ");
		List<Object> values = new ArrayList<Object>();
		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			int size = criteriaList.size();

			for (int i = 0; i < size; i++) {
				ModuleCriterion criterion = criteriaList.get(i);
				if (i == 0) {
					query.append(" where ");
				}
				if (i != 0) {
					query.append(" AND ");
				}
				if (criterion.getCondition().equals(ModuleCriterion.CONDITION_EQUALS)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + "= ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + "> ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + ">= ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + "< ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + "<= ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_BETWEEN)) {
					values.add(criterion.getValue().get(0));
					values.add(criterion.getValue().get(1));
					query.append("M." + criterion.getFieldName() + " BETWEEN ? AND ? ");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {
					StringBuffer inStr = new StringBuffer("M." + criterion.getFieldName() + " IN (");
					for (int k = 0; k < criterion.getValue().size(); k++) {
						values.add(criterion.getValue().get(k));
						if (k == 0) {
							inStr.append("?");
						} else {
							inStr.append(",?");
						}
					}
					inStr.append(")");
					query.append(inStr.toString());
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE)) {
					values.add(criterion.getValue().get(0));
					query.append("M." + criterion.getFieldName() + " LIKE ? ");

				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_IGNORECASE)) {
					values.add(criterion.getValue().get(0).toString().toUpperCase());
					values.add(criterion.getValue().get(0).toString().toLowerCase());
					query.append("(M." + criterion.getFieldName() + " LIKE ? ");
					query.append("OR M." + criterion.getFieldName() + " LIKE ? )");
				} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IS_NOT_NULL)) {
					query.append("M." + criterion.getFieldName() + " IS NOT NULL ");

				}
			}
		}
		log.debug("query =[" + query.toString() + "]");
		Query q = getSession().createQuery(query.toString());
		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			int size = values.size();
			for (int j = 0; j < size; j++) {
				Object value = values.get(j);
				q.setParameter(j, value);
				/*
				 * if (value instanceof Integer){ q.setInteger(j, ((Integer)value).intValue()); } else if (value
				 * instanceof String){ q.setString(j, (String)value); } else if (value instanceof Double){
				 * q.setDouble(j, ((Double)value).doubleValue()); } else if (value instanceof Float){ q.setFloat(j,
				 * ((Float)value).floatValue()); } else if (value instanceof Boolean){ q.setBoolean(j, ((Boolean)
				 * value).booleanValue()); } else if (value instanceof Date) { q.setDate(j, ((Date) value)); }
				 */
			}
		}
		return ((Long) q.uniqueResult()).intValue();
	}

	// ----------------

	public Page getPagedDataNew(List<ModuleCriterion> criteria, int startIndex, int pageSize, Class<?> clazz,
			List<String> orderByFields, Session hibernateSession) {
		log.debug("Begin PlatformBaseHibernateDaoSupport.getPagedData()");
		Session queryingSession = (hibernateSession == null ? getSession() : hibernateSession);
		@SuppressWarnings("unchecked")
		Collection results = CriteriaParserForHibernate
				.parseNew(criteria, queryingSession.createCriteria(clazz), orderByFields).setFirstResult(startIndex)
				.setMaxResults(pageSize).list();
		int totalRecords = -1;
		totalRecords = getCountNew(criteria, clazz, queryingSession);
		Page pagedResults = new Page(totalRecords, startIndex, pageSize, results);

		return pagedResults;
	}

	@SuppressWarnings("rawtypes")
	public int getCountNew(List<ModuleCriterion> criteriaList, Class clazz, Session hibernateSession) {
		StringBuffer query = new StringBuffer();
		query.append("select count(*) from " + clazz.getName() + " as M ");
		// List values = new ArrayList();
		hqlParmsToCountRows = new HashMap<String, Object>();
		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			int size = criteriaList.size();
			boolean firstCondition = true;
			for (int i = 0; i < size; i++) {
				ModuleCriterion criterion = criteriaList.get(i);
				if (criterion != null) {
					if (firstCondition) {
						query.append(" where ");
					} else {
						query.append(" AND ");
					}
					firstCondition = false;
					query.append(getSQLExpression(criterion, null));
				}
			}
		}
		log.debug("query =[" + query.toString() + "]");
		Query q = hibernateSession.createQuery(query.toString());
		if ((criteriaList != null) && (criteriaList.size() > 0)) {
			for (String string : hqlParmsToCountRows.keySet()) {
				String paramKey = string;
				q.setParameter(paramKey, hqlParmsToCountRows.get(paramKey));
			}
		}
		return ((Long) q.uniqueResult()).intValue();
	}

	private String getSQLExpression(ModuleCriterion criterion, String paramId) {

		String currentParamId = paramId == null ? "param" + hqlParmsToCountRows.size() : paramId;

		if (criterion.isLeaf()) {
			String sqlExpression = " ( ";
			if (criterion.getCondition().equals(ModuleCriterion.CONDITION_EQUALS)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + "= :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + "> :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + ">= :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + "< :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + "<= :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_BETWEEN)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + " BETWEEN :" + currentParamId;
				currentParamId = "param" + hqlParmsToCountRows.size();
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(1));
				sqlExpression += " AND :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {
				sqlExpression += "M." + criterion.getFieldName() + " IN (";
				for (int k = 0; k < criterion.getValue().size(); k++) {
					hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(k));
					if (k == 0) {
						sqlExpression += " :" + currentParamId + " ";
					} else {
						sqlExpression += ", :" + currentParamId + " ";
					}
					currentParamId = "param" + hqlParmsToCountRows.size();
				}
				sqlExpression += ")";

			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + " LIKE :" + currentParamId + " ";

			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_LIKE_IGNORECASE)) {
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0).toString().toUpperCase());
				sqlExpression += "(M." + criterion.getFieldName() + " LIKE :" + currentParamId + " ";
				currentParamId = "param" + hqlParmsToCountRows.size();
				hqlParmsToCountRows.put(currentParamId, criterion.getValue().get(0).toString().toLowerCase());
				sqlExpression += "OR M." + criterion.getFieldName() + " LIKE :" + currentParamId + " ";
			} else if (criterion.getCondition().equals(ModuleCriterion.CONDITION_IS_NOT_NULL)) {
				// hqlParmsToCountRows.put(currentParamId,criterion.getValue().get(0));
				sqlExpression += "M." + criterion.getFieldName() + " IS NOT NULL ";
			}
			return sqlExpression += " ) ";
		} else {
			ModuleCriteria moduleCriteria = (ModuleCriteria) criterion;
			if (moduleCriteria.getModulecriterionList() != null && moduleCriteria.getModulecriterionList().size() > 1) {
				String or_sqlExpression = " ( ";
				for (int i = 0; i < moduleCriteria.getModulecriterionList().size(); i++) {

					if (i == 0) {
						hqlParmsToCountRows.put(currentParamId, (moduleCriteria.getModulecriterionList().get(i)).getValue()
								.get(0));

						or_sqlExpression += getSQLExpression(moduleCriteria.getModulecriterionList().get(i), currentParamId);
					} else {
						if (moduleCriteria.getCondition().equals(ModuleCriteria.JOIN_CONDITION_OR)) {
							currentParamId = "param" + hqlParmsToCountRows.size();
							hqlParmsToCountRows.put(currentParamId, (moduleCriteria.getModulecriterionList().get(i)).getValue()
									.get(0));
							or_sqlExpression += " OR "
									+ getSQLExpression(moduleCriteria.getModulecriterionList().get(i), currentParamId);
						}
					}

				}
				or_sqlExpression += " ) ";
				return or_sqlExpression;
			}

			if (moduleCriteria.getJoinCondition().equals(ModuleCriteria.JOIN_CONDITION_AND)) {
				return " ( " + getSQLExpression(moduleCriteria.getLeftCriterion(), null) + " AND "
						+ getSQLExpression(moduleCriteria.getRightCriterion(), null) + " ) ";
			} else if (moduleCriteria.getJoinCondition().equals(ModuleCriteria.JOIN_CONDITION_OR)) {
				return " ( " + getSQLExpression(moduleCriteria.getLeftCriterion(), null) + " OR "
						+ getSQLExpression(moduleCriteria.getRightCriterion(), null) + " ) ";
			}
		}
		return null;
	}
}
