/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Feb 25, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author sudheera
 * 
 *         ClientMessage is a property factory to get client side messages
 * 
 */

public class ClientMessage {
	// property file system property name
	private static final String SYS_PROP_NAME = "client.message.properties";

	// default property file if the system property not set
	private static String resouceBundleName = "client-message";

	private static final PropertyFactory pf = getFactory();

	/**
	 * gets the property
	 * 
	 * @param key
	 * @return String the property
	 */
	public static String getProperty(String key) {
		return pf.getProperty(key);
	}

	/**
	 * gets the property factory
	 * 
	 * @return
	 */
	private static final PropertyFactory getFactory() {
		String resouceBundleName = System.getProperty(SYS_PROP_NAME);

		if (resouceBundleName == null || resouceBundleName.equals("")) {
			resouceBundleName = ClientMessage.resouceBundleName;
		}

		return new PropertyFactory(resouceBundleName);
	}

}