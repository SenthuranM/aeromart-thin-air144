package com.isa.thinair.commons.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author nafly
 * @isa.module.config-bean
 * 
 */
public class CommonsConfig extends DefaultModuleConfig {

	public CommonsConfig() {
		super();
	}
}
