package com.isa.thinair.commons.core.framework;

import java.io.Serializable;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IServiceDelegate;

public class PlatformBaseServiceDelegate implements IServiceDelegate {

	private static final Log log = LogFactory.getLog(PlatformBaseServiceDelegate.class);

	public static interface ConfigSource {
		public static final String XML = "xml";
		public static final String SPRING_BEANS = "spring";
	}

	private String configSource = ConfigSource.SPRING_BEANS;

	public void setConfigurationSource(String configSource) {
		this.configSource = configSource;
	}

	public String getConfigSource() {
		return this.configSource;
	}

	protected void sendMessage(Serializable object, Properties jndiProperties, String destinationJNDIName,
			String connectionFactoryJNDIName) throws ModuleException {

		QueueConnection queueConnection = null;
		QueueSession jmsSession = null;

		try {
			queueConnection = getQueueConnection(jndiProperties, connectionFactoryJNDIName);
		} catch (JMSException e) {
			throw new ModuleException(e, "module.bd.jmsexception");
		} catch (NamingException e) {
			throw new ModuleException(e, "module.bd.namingException");
		}

		try {
			jmsSession = queueConnection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
			QueueSender sender = jmsSession.createSender(getQueue(jndiProperties, destinationJNDIName));
			// create the message
			ObjectMessage message = jmsSession.createObjectMessage();
			message.setObject(object);

			// send the message
			sender.send(message);
			sender.close();
			if (log.isDebugEnabled())
				log.debug("Message is written to jms queue");
		} catch (JMSException e) {
			// None of the exceptions are catch in the front ends.Just log the full stack trace.
			log.error("Sending message via jms failed", e);
		} catch (NamingException e) {
			log.error("Sending message via jms failed", e);
		} catch (Exception exception) {
			log.error("Error occured in emailing itinerary", exception);
		} finally {
			// closing the queConnection
			if (queueConnection != null) {
				try {
					queueConnection.close();
					if (log.isDebugEnabled())
						log.debug("Closed jms queue connection");
				} catch (JMSException e) {
					log.error("Closing jms queue connection failed", e);
				}
			}

			// closing the jms session
			if (jmsSession != null) {
				try {
					jmsSession.close();
					if (log.isDebugEnabled())
						log.debug("Closed jms session");
				} catch (JMSException e) {
					log.error("Closing jms session failed", e);
				}
			}
		}
	}

	private static Queue getQueue(Properties environment, String destinationJNDIName) throws NamingException {
		// Obtain initial context
		InitialContext initialContext = new InitialContext(environment);
		try {
			Object objRef = initialContext.lookup(destinationJNDIName);
			return (Queue) objRef;
		} finally {
			initialContext.close();
		}
	}

	private QueueConnection getQueueConnection(Properties environment, String connectionFactoryJNDIName) throws NamingException,
			JMSException {
		// Obtain initial context
		InitialContext initialContext = new InitialContext(environment);
		try {
			Object objRef = initialContext.lookup(connectionFactoryJNDIName);
			return ((QueueConnectionFactory) objRef).createQueueConnection();
		} finally {
			initialContext.close();
		}
	}
}
