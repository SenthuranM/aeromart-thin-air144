package com.isa.thinair.commons.core.interceptor;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 */
public class Perf4JBaseUtils {

	public static String getClassName(Object context) {
		String strContext = PlatformUtiltiies.nullHandler(context);

		if (strContext.length() > 0) {
			String className = strContext.substring(strContext.lastIndexOf(".") + 1, strContext.length());
			className = className.substring(0, className.indexOf("@"));
			return className;
		} else {
			return strContext;
		}
	}

}
