package com.isa.thinair.commons.core.util;

import java.sql.Blob;
import java.sql.Clob;

import org.hibernate.engine.jdbc.NonContextualLobCreator;

public class DataConverterUtil {

	public static Blob createBlob(byte[] data){		
		return NonContextualLobCreator.INSTANCE.createBlob(data);
	}
	
	public static Clob createClob(String data){		
		return NonContextualLobCreator.INSTANCE.createClob(data);
	}
	
}
