/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * GlobalConfig.java 
 * 
 * ============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.AdminFeeAgentTO;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessage;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.dto.BaseContactConfigDTO;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.BookingClassCharMappingTO;
import com.isa.thinair.commons.api.dto.CabinClassDTO;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.ReturnValidityPeriodTO;
import com.isa.thinair.commons.api.dto.SSRCategoryDTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.dto.ServiceBearerDTO;
import com.isa.thinair.commons.api.dto.ServiceClientDTO;
import com.isa.thinair.commons.api.dto.ServiceClientTTYDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateDTO;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.api.model.Merchant;
import com.isa.thinair.commons.api.model.WhiteListedEmailDomains;
import com.isa.thinair.commons.core.persistence.dao.CommonsDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * Manages configurations used across applications.
 * 
 * @author Nasly
 */
public class GlobalConfig {

	Log log = LogFactory.getLog(getClass());

	private String defaultLanguage = "en";
	private Map<String, HashMap<String, String>> bizPropertyMap = null;
	private List<PaxCategoryTO> paxCategoryList = null;
	private List<AdminFeeAgentTO> adminFeeAgentList = null;
	private Map<String, PaxTypeTO> paxTypesMap = null;
	private List<BaseContactConfigDTO> contactConfigDTOList;
	private List<BaseContactConfigDTO> xbeContactConfigDTOList;
	private List<BaseContactConfigDTO> ibeContactConfigDTOList;
	private List<BaseContactConfigDTO> wsContactConfigDTOList;
	private List<UserRegConfigDTO> userRegContactConfigDTOList;
	private List<BasePaxConfigDTO> paxConfigDTOList = null;
	private List<BasePaxConfigDTO> xbePaxConfigList = null;
	private List<BasePaxConfigDTO> gdsPaxConfigList = null;
	private Map<String, PaxContactConfigDTO> paxContactConfigDTOMap = null;
	private PaxContactConfigDTO paxContactConfigDTO = null;
	private List<BasePaxConfigDTO> ibePaxConfigList = null;
	private List<BasePaxConfigDTO> wsPaxConfigList = null;
	private Map<String, FareCategoryTO> fareCategoryMap = null;
	private Map<String, InterlinedAirLineTO> interlinedAirlinesMap = null;
	private List<String> interlinedAirlineCodesList = null;
	private Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = null;
	private Map<String, String> activeCarriersMap = null;
	private Set<String> interlinedAirlineCodes = null;
	private Set<String> ownAirlineCarrierCodes = null;
	private Set<String> ownAirCarrierCodesExcludingBus = null;
	private Set<String> chargeHideCurrencies = null;
	private Map<String, String> allCarriersMap = null;
	private Map<String, String> activeSystemCabinClassesMap = null;
	private Map<String, CabinClassDTO> activeSystemCabinClassDTOMap = null;
	private HashMap<String, String> iataModelMap = null;
	private HashMap<String, GDSStatusTO> gdsMap = null;
	private HashMap<String, List<ServiceClientTTYDTO>> requestWiseServiceClientTTYMap = null;
	private HashMap<String, ServiceClientDTO> serviceClientsMap = null;
	private HashMap<String, ServiceBearerDTO> serviceBearersMap = null;
	private Map<String, ArrayList<String>> airportMessagesMap = null;
	private Map<String, Object[]> airportInfoMap = null;
	private List<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes = null;
	private ChargeAdjustmentTypeDTO defaultChargeAdjustment = null;

	private Map<Integer, HubAirportTO> hubs = null;
	private Set<String> hubAirports = null;
	private List<String> currencies = null;
	private Map<String, String[]> cachedHubs = null;

	private String xbeServerAddressAndPort = "";
	private String adminServerAddressAndPort = "";
	private String cookieName = "";
	private Map<String, String> cookieValueMap;
	private boolean generateAppCookie;

	private boolean controlXBECachingHeaders;
	private boolean controlIBECachingHeaders;
	private boolean controlAirAdminCachingHeaders;
	private Map<String, String> cacheControlProperties;

	private boolean logIBEAccessInfo;
	private boolean logXBEAccessInfo;
	private boolean logAirAdminAccessInfo;

	private boolean logIBEReqParams;
	private boolean logXBEReqParams;
	private boolean logAirAdminReqParams;

	private String ftpServerName = "";
	private String ftpServerUserName = "";
	private String ftpServerPassword = "";
	private boolean ftpEnablePassiveMode;

	private String amadeusFtpServerName = "";
	private String amadeusFtpServerUserName = "";
	private String amadeusFtpServerPassword = "";
	private Integer amadeusFtpPort = 0;
	private String amadeusFtpRemotePath = "";

	private String aviatorFtpServerName = "";
	private String aviatorFtpServerUserName = "";
	private String aviatorFtpServerPassword = "";
	private Integer aviatorFtpPort = 0;
	private String aviatorFtpRemotePath = "";

	private String lastBuildVersion = "";

	private String defaultAACarrierCode = "AA";

	private Map<String, String> publishMechanismMap = null;

	private Map<BookingClassCharMappingTO, String> bookingClassCharMappingMap = null;

	private Map<Integer, Map<String, String>> gdsBookingClassMap;

	private Map<Integer, Map<String, String>> csBookingClassMap;

	private Map<Integer, SSRCategoryDTO> ssrCategoryMap = null;

	private Map<Integer, ReturnValidityPeriodTO> returnValidityPeriods = null;

	private Map<Integer, SSRInfoDTO> sSRInfoByIdMap = null;

	private Map<String, SSRInfoDTO> sSRInfoByCodeMap = null;

	private Map<String, Boolean> sSRApplicableAirportsMap = null;

	private Map<Integer, String> sSRCategoryEMailMap = null;

	private Map<String, String> petroFacEnabledAgentsMap = null;

	private Map<String, String> chargeGroupCodeNDescMap = null;

	private Map<String, String> codeshareBcCosMap = null;

	private Map<String, String> carrierUrls;

	private Map<String, String> carrierUrlsIBE;
	
	private Map<String, String> countryServiceTaxReportCategory = null;


	/**
	 * Map that will hold all the internationalized messages T_i18n_MESSAGES table Key will be the i18n_MESSAGE_KEY.
	 * Value is a Map<String,String> the key is the language code and value will be the internationalized message
	 * content.
	 */
	private Map<String, Map<String, String>> internationalizedMessages = null;

	private String httpProxy = "";

	private Integer httpPort = 0;

	private long masterDataCacheTimeout;

	private String staticFileSuffix;

	private boolean includeCancelledSSRDetailsInReport = false;

	private HashMap<Integer, String> actualPayModes = null;

	private Collection<String> logAccessInfoIncludes;

	private boolean useProxy;

	private Set<String> busConnectingAirports = null;

	private boolean allowProcessPfsBeforeFlightDeparture;

	private List<CabinClassWiseDefaultLogicalCCDetailDTO> defaultLogicalCCDetails = null;
	private Map<String, LogicalCabinClassDTO> availableLogicalCCMap = null;
	private Map<String, Map<String, LogicalCabinClassDTO>> availableLogicalCCLanguageMap = new HashMap<String, Map<String, LogicalCabinClassDTO>>();

	private long logicalCCRecordAge = 0;

	private static Map<String, String> mapGroundStationNames = new HashMap<String, String>();
	private static Map<String, Collection<String>> mapPrimaryStationCode = new HashMap<String, Collection<String>>();
	private static Map<String, String> mapPrimaryStationBySubStation = new HashMap<String, String>();
	private static Map<String, List<String[]>> mapParentStationByGroundStation = new HashMap<String, List<String[]>>();
	private static Map<String, String[]> mapAllAirportCodes = new HashMap<String, String[]>();
	private static Map<String, String> mapTopLevelStationByConnectedStation = new HashMap<String, String>();
	private static Map<String, String> mapConnectingAirportDetail = new HashMap<String, String>();
	private static Map<String, Collection<AirportMessage>> airportMessages = null;
	private static final String KEY_SEPERATOR = "|";

	private Map<String, String> carrierAirlineCodeMap = null;

	private final Date recordAge = new Date(System.currentTimeMillis() - masterDataCacheTimeout);

	private boolean noCaching;
	
	private boolean cacheAppParamsInCachingDB;

	private Map<String, Map<String, List<PaxCountryConfigDTO>>> countryWisePaxConfigs = new HashMap<String, Map<String, List<PaxCountryConfigDTO>>>();

	/*  */
	private Map<String, Map<String, Map<String, String>>> termsAndConditions = null;

	/* */
	private final Date termsAndConditionAge = new Date(System.currentTimeMillis() - masterDataCacheTimeout);

	private final Date hideItineraryChargeCurrenciesAge = new Date(System.currentTimeMillis() - masterDataCacheTimeout);

	private Map<String, String> agents = null;

	private Collection<String> whiteListedEmailDomains = new ArrayList<String>();
	private long emailDomainListAge = 0;

	private Map<Integer, List<String>> gdsMappedBookingClassList;

	private Map<String, List<String>> bookingClassByLogicalCCMap;

	private Map<Integer, List<String>> serviceTypesByOperationType;

	private List<String> postPayloadDumpURLKeywords = null;

	private boolean controlXBESecureHeaders;
	private boolean controlIBESecureHeaders;
	private boolean controlAirAdminSecureHeaders;
	private boolean secureHeadersIframeDeny;
	private boolean secureHeadersIframeSameOrigin;

	static {
		loadGroundServiceMap();
	}

	public GlobalConfig() {
		super();
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String localeLanguage) {
		this.defaultLanguage = localeLanguage;
	}

	public String getBizParam(String paramKey, String... additionalArgs) {
		
		Object value = null;
		if (isCacheAppParamsInCachingDB() && CommonsServices.getAerospikeCachingService().isCachingDBEnabled()) {
			value = CommonsServices.getAerospikeCachingService().getAppParamValueByParamKey(paramKey);
		}
		if (value == null) {
			if (bizPropertyMap == null) {
				this.loadBizProperties();
				if (SystemPropertyUtil.isDevOverrideAppParam()) {
					this.overrideBizPropertiesFromXML();
				}
			}
			HashMap<String, String> carrierBizProperty = null;
			
			if (additionalArgs == null || additionalArgs.length == 0) {
				// Get AccelAero default app param
				carrierBizProperty = bizPropertyMap.get(getDefaultAACarrierCode());
				if (carrierBizProperty != null) {
					value = carrierBizProperty.get(paramKey);
				}
			} else if (additionalArgs.length == 1) {
				// Get carrier specific app param
				carrierBizProperty = bizPropertyMap.get(additionalArgs[0]);
				if (carrierBizProperty != null) {
					value = carrierBizProperty.get(paramKey);
				}
	
				// Fall back to default AcceAero app param
				if (carrierBizProperty == null || !carrierBizProperty.containsKey(paramKey)) {
					carrierBizProperty = bizPropertyMap.get(getDefaultAACarrierCode());
					if (carrierBizProperty != null) {
						value = carrierBizProperty.get(paramKey);
					}
				}
			}			
		}
		return (value != null) ? value.toString() : null;
	}

	private void overrideBizPropertiesFromXML() {
		try {
			Map<String, String> xmlParams = CommonsServices.getDevConfig().getDevAppParams();
			if (xmlParams != null && xmlParams.size() > 0) {
				HashMap<String, String> defCarrierBizProperties = bizPropertyMap.get(getDefaultAACarrierCode());
				if (defCarrierBizProperties != null) {
					for (String key : xmlParams.keySet()) {
						log.warn("DEV MODE OVERRIDE APP PARAM [" + key + " = " + xmlParams.get(key) + "]");
						defCarrierBizProperties.put(key, xmlParams.get(key));
					}
				}
			}
		} catch (Exception e) {
			log.error("Error occurred while overriding the biz properties from XML", e);
		}
	}

	private void loadBizProperties() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		bizPropertyMap = platformDAO.getBizParams();
	}
	
	public Map<String, HashMap<String, String>> getAppParams() {
		if (bizPropertyMap == null) {
			this.loadBizProperties();
		}
		return bizPropertyMap;
	}
	
	private void loadAdminFeeAgents() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		adminFeeAgentList = platformDAO.getAdminFeeAgentList();
	}

	private void loadPaxCategories() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		paxCategoryList = platformDAO.getPaxCategoryList();
	}

	private void loadPaxTypes() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		paxTypesMap = platformDAO.getPaxTypesMap();
	}

	private void loadContactDetailsConfig(String appName) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		if (appName.equals(AppIndicatorEnum.APP_XBE.toString())) {
			xbeContactConfigDTOList = platformDAO.getContactDeailsConfig(appName);
		} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
			ibeContactConfigDTOList = platformDAO.getContactDeailsConfig(appName);
		} else {
			wsContactConfigDTOList = platformDAO.getContactDeailsConfig(appName);
		}
	}

	private void loadNewAirportMessages() {
		airportMessages = new HashMap<String, Collection<AirportMessage>>();
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		Collection<AirportMessage> messages = platformDAO.getNewAirportMessages();
		for (AirportMessage airportMessage : messages) {
			String key = airportMessage.getAirport() + KEY_SEPERATOR + airportMessage.getStage();
			if (airportMessages.get(key) == null) {
				airportMessages.put(key, new ArrayList<AirportMessage>());
			}
			airportMessages.get(key).add(airportMessage);
		}
	}

	private void loadUserRegConfig() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		userRegContactConfigDTOList = platformDAO.getUserRegConfig();
	}

	private void loadPaxDetailsConfig(String appName) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		if (appName.equals(AppIndicatorEnum.APP_XBE.toString())) {
			xbePaxConfigList = platformDAO.getPaxDetailsConfig(appName);
		} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
			ibePaxConfigList = platformDAO.getPaxDetailsConfig(appName);
		} else if (appName.equals(AppIndicatorEnum.APP_GDS.toString())) {
			gdsPaxConfigList = platformDAO.getPaxDetailsConfig(appName);
		} else {
			wsPaxConfigList = platformDAO.getPaxDetailsConfig(appName);
		}
	}

	private void loadInterlinedAirlines() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		interlinedAirlinesMap = platformDAO.getInterlinedAirlinesMap();
	}

	private void loadInterlineAirlineCodesList() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		interlinedAirlineCodesList = platformDAO.getInterlinedAirlineCodesList();
	}

	private void loadCarriers(String status) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		if (status == null) {
			allCarriersMap = platformDAO.getCarriersMap(status);
		} else if ("ACT".equals(status)) {
			activeCarriersMap = platformDAO.getCarriersMap(status);
		}
	}

	private void loadInterlinedAirlineCodes() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		interlinedAirlineCodes = platformDAO.getInterlinedAirlineCodes();

	}

	public Set<String> getInterlinedAirlineCodes() {
		if (interlinedAirlineCodes == null) {
			loadInterlinedAirlineCodes();
		}
		return interlinedAirlineCodes;
	}

	public Set<String> getOwnAirlineCarrierCodes() {
		if (ownAirlineCarrierCodes == null) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			ownAirlineCarrierCodes = platformDAO.getOwnAirlineCarrierCodes();
		}
		return ownAirlineCarrierCodes;
	}

	public Set<String> getOwnAirlineCarrierCodesExcludingBus() {
		if (ownAirCarrierCodesExcludingBus == null) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			ownAirCarrierCodesExcludingBus = platformDAO.getOwnAirCarrierCodesExcludingBus();
		}
		return ownAirCarrierCodesExcludingBus;
	}

	public Set<String> getChargehideCurrencies() {
		checkItineraryChargeHideCurrenciesRefreshNeeded();
		if (chargeHideCurrencies == null) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			chargeHideCurrencies = platformDAO.getChargehideCurrencies();
		}
		return chargeHideCurrencies;
	}

	private void loadBusConnectingAirports() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		busConnectingAirports = platformDAO.getBusConnectingAirports();
	}

	public Set<String> getBusConnectingAirports() {
		if (busConnectingAirports == null) {
			loadBusConnectingAirports();
		}
		return busConnectingAirports;
	}

	public void setBusConnectingAirports(Set<String> busConnectingAirports) {
		this.busConnectingAirports = busConnectingAirports;
	}

	private void loadDefaultLogicalCCDetails() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		defaultLogicalCCDetails = platformDAO.getDefaultLogicalCCDetails();
	}

	public List<CabinClassWiseDefaultLogicalCCDetailDTO> getDefaultLogicalCCDetails() {
		if (defaultLogicalCCDetails == null) {
			loadDefaultLogicalCCDetails();
		}
		return defaultLogicalCCDetails;
	}

	public CabinClassWiseDefaultLogicalCCDetailDTO getDefaultLogicalCCDetails(String cabinClass) {
		for (CabinClassWiseDefaultLogicalCCDetailDTO logicalCCDetailDTO : getDefaultLogicalCCDetails()) {
			if (logicalCCDetailDTO.getCcCode().toUpperCase().equals(cabinClass.toUpperCase())) {
				return logicalCCDetailDTO;
			}
		}
		return null;
	}

	public Map<String, LogicalCabinClassDTO> getAvailableLogicalCCMap() {
		return getAvailableLogicalCCMap(Locale.ENGLISH.toString());
	}

	public Map<String, LogicalCabinClassDTO> getAvailableLogicalCCMap(String language) {

		if (isLogicalCCRefreshNeeded()) {
			internationalizedMessages = null;
			logicalCCRecordAge = System.currentTimeMillis();
			loadInternationalizedMessages();
			availableLogicalCCMap = loadAvailableLogicalCCMap();
			availableLogicalCCLanguageMap.clear();
		}

		if (!availableLogicalCCLanguageMap.containsKey(language)) {
			Map<String, LogicalCabinClassDTO> availableLogicalCCMapClone = createCloneAvailableLogicalCCMap(availableLogicalCCMap);

			for (LogicalCabinClassDTO lccDto : availableLogicalCCMapClone.values()) {
				// Translate messages to the given language
				lccDto.setCurrentLanguage(language);
				lccDto.setComment(getTranslatedMessage(lccDto.getCommentI18nMessageKey(), language));
				lccDto.setDescription(getTranslatedMessage(lccDto.getDescriptionI18nKey(), language));
				if (lccDto.getDescription() == null) {
					lccDto.setDescription(getTranslatedMessage(lccDto.getDescriptionI18nKey(), "en"));
				}
				lccDto.setFlexiComment(getTranslatedMessage(lccDto.getFlexiCommentI18nMessageKey(), language));
				lccDto.setFlexiDescription(getTranslatedMessage(lccDto.getFlexiDescriptioni18nMessageKey(), language));
				if (lccDto.getFlexiDescription() == null) {
					lccDto.setFlexiDescription(getTranslatedMessage(lccDto.getFlexiDescriptioni18nMessageKey(), "en"));
				}
			}

			availableLogicalCCLanguageMap.put(language, availableLogicalCCMapClone);
		}

		return (availableLogicalCCLanguageMap.get(language) != null ? availableLogicalCCLanguageMap.get(language)
				: availableLogicalCCMap);
	}

	private Map<String, LogicalCabinClassDTO> createCloneAvailableLogicalCCMap(
			Map<String, LogicalCabinClassDTO> availableLogicalCCMap) {
		Map<String, LogicalCabinClassDTO> availableLogicalCCMapClone = new HashMap<String, LogicalCabinClassDTO>();

		for (Map.Entry<String, LogicalCabinClassDTO> entry : availableLogicalCCMap.entrySet()) {
			LogicalCabinClassDTO cloneLogicalCabinClassDTO = new LogicalCabinClassDTO();
			LogicalCabinClassDTO logicalCabinClassDTO = entry.getValue();
			cloneLogicalCabinClassDTO.setAllowSingleMealOnly(logicalCabinClassDTO.isAllowSingleMealOnly());
			cloneLogicalCabinClassDTO.setCabinClassCode(logicalCabinClassDTO.getCabinClassCode());
			cloneLogicalCabinClassDTO.setComment(logicalCabinClassDTO.getComment());
			cloneLogicalCabinClassDTO.setCommentI18nMessageKey(logicalCabinClassDTO.getCommentI18nMessageKey());
			cloneLogicalCabinClassDTO.setCurrentLanguage(logicalCabinClassDTO.getCurrentLanguage());
			cloneLogicalCabinClassDTO.setDescription(logicalCabinClassDTO.getDescription());
			cloneLogicalCabinClassDTO.setDescriptionI18nKey(logicalCabinClassDTO.getDescriptionI18nKey());
			cloneLogicalCabinClassDTO.setFlexiComment(logicalCabinClassDTO.getFlexiComment());
			cloneLogicalCabinClassDTO.setFlexiCommentI18nMessageKey(logicalCabinClassDTO.getFlexiCommentI18nMessageKey());
			cloneLogicalCabinClassDTO.setFlexiDescription(logicalCabinClassDTO.getFlexiDescription());
			cloneLogicalCabinClassDTO.setFlexiDescriptioni18nMessageKey(logicalCabinClassDTO.getFlexiDescriptioni18nMessageKey());
			cloneLogicalCabinClassDTO.setFreeFlexiEnabled(logicalCabinClassDTO.isFreeFlexiEnabled());
			cloneLogicalCabinClassDTO.setFreeSeatEnabled(logicalCabinClassDTO.isFreeSeatEnabled());
			cloneLogicalCabinClassDTO.setLogicalCCCode(logicalCabinClassDTO.getLogicalCCCode());
			cloneLogicalCabinClassDTO.setNestRank(logicalCabinClassDTO.getNestRank());
			cloneLogicalCabinClassDTO.setStatus(logicalCabinClassDTO.getStatus());
			cloneLogicalCabinClassDTO.setBundleFareDescriptionTemplateDTO(logicalCabinClassDTO
					.getBundleFareDescriptionTemplateDTO());
			availableLogicalCCMapClone.put(entry.getKey(), cloneLogicalCabinClassDTO);
		}

		return availableLogicalCCMapClone;
	}

	/**
	 * Returns the terms and conditions in a complex data structure.
	 * 
	 * A typical retrieval of terms and conditions will look like
	 * 
	 * getTermsAndCOnditionsMap().get(CARRIER_CODE).get(TERMS_AND_CONDITION_TEMPLATE_NAME).get(LANGUAGE_CODE)
	 * 
	 * Format :
	 * 
	 * Carrier Code -> Template Name -> Language -> Template Content
	 * 
	 * Map<Carrier,Map<TemplateName,Map<Language,TemplateContent>>>
	 * 
	 * !Implementation Details! : Will load the termsAndConditions map if it's null. In the current implementation this
	 * means data will be loaded form the database on initial access and after the cache age expires.
	 */
	public Map<String, Map<String, Map<String, String>>> getTermsAndCOnditionsMap() {
		checkTermsRefreshNeeded();
		if (termsAndConditions == null) {
			loadTermsAndConditions();
		}
		return termsAndConditions;
	}

	/**
	 * Loads the internationalized messages from the database if its not already loaded
	 */
	private void loadInternationalizedMessages() {
		if (internationalizedMessages == null) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			internationalizedMessages = platformDAO.getInternationalizedMessages();
		}
	}

	/**
	 * Returns the translated message as a string when the msgKey and language are provided
	 * 
	 * @param msgKey
	 * @param language
	 * @return translated message or the string "untranslated" if the key or language is null
	 */
	private String getTranslatedMessage(String msgKey, String language) {
		String translatedMsg = "untranslated";
		if (msgKey != null && language != null) {
			// Return null for null message keys in the database
			if (internationalizedMessages != null) {
				Map<String, String> msgContent = internationalizedMessages.get(msgKey);
				if (msgContent != null) {
					translatedMsg = msgContent.get(language);
				}
			} else {
				log.equals("Inernationalized Messages not loaded from Database.");
			}
		}
		return translatedMsg;

	}

	/**
	 * Retrieves the terms and conditions from the database and sorts it into a complex Map structure.
	 * 
	 * See {@link GlobalConfig#getTermsAndCOnditionsMap()} for the complex map format information.
	 */
	private void loadTermsAndConditions() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		Collection<TermsTemplateDTO> termsTemplates = commonsDAO.getTermsAndConditionsTemplates();
		Map<String, Map<String, Map<String, String>>> termsMap = new HashMap<String, Map<String, Map<String, String>>>();

		for (TermsTemplateDTO termsDTO : termsTemplates) {
			if (termsMap.get(termsDTO.getCarriers()) == null)
				termsMap.put(termsDTO.getCarriers(), new HashMap<String, Map<String, String>>());

			if (termsMap.get(termsDTO.getCarriers()).get(termsDTO.getTemplateName()) == null)
				termsMap.get(termsDTO.getCarriers()).put(termsDTO.getTemplateName(), new HashMap<String, String>());

			termsMap.get(termsDTO.getCarriers()).get(termsDTO.getTemplateName())
					.put(termsDTO.getLanguage(), termsDTO.getTemplateContent());
		}

		termsAndConditions = termsMap;
	}

	private Map<String, LogicalCabinClassDTO> loadAvailableLogicalCCMap() {
		LookupService lookup = LookupServiceFactory.getInstance();

		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO.getAvailableLogicalCCMap();
	}

	public void setDefaultLogicalCCDetails(List<CabinClassWiseDefaultLogicalCCDetailDTO> defaultLogicalCCDetails) {
		this.defaultLogicalCCDetails = defaultLogicalCCDetails;
	}

	private void loadGdsConfigs() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		gdsMap = platformDAO.getGdsConfigs();
	}

	private void loadServiceClientTTYInfo() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		requestWiseServiceClientTTYMap = platformDAO.getServiceClientTTYInfo();
	}

	public Map<String, List<ServiceClientTTYDTO>> getServiceClientTTYbyRequestType() {
		if (requestWiseServiceClientTTYMap == null) {
			this.loadServiceClientTTYInfo();
		}
		return requestWiseServiceClientTTYMap;
	}

	private void loadServiceClients() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		serviceClientsMap = platformDAO.getServiceClients();
	}

	private void loadServiceBearers() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		serviceBearersMap = platformDAO.getServiceBearers();
	}

	private void loadIataAircraftCodes() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		iataModelMap = platformDAO.getIATAAircraftModelCodes();
	}

	public Collection<InterlinedAirLineTO> getInterlinedAirlines() {
		if (interlinedAirlinesMap == null) {
			this.loadInterlinedAirlines();
		}
		return interlinedAirlinesMap.values();
	}

	public List<String> getInterlinedAirlineCodesList() {
		if (interlinedAirlineCodesList == null) {
			this.loadInterlineAirlineCodesList();
		}

		return interlinedAirlineCodesList;
	}

	/**
	 * Returns Airline Description
	 * 
	 * @param carrierCode
	 * @return
	 */
	public String getInterlineName(String carrierCode) {
		InterlinedAirLineTO interlinedAirLineTO = getActiveInterlinedCarriersMap().get(carrierCode);
		return interlinedAirLineTO.getAirlineDesc();
	}

	public Map<String, InterlinedAirLineTO> getActiveInterlinedCarriersMap() {
		if (interlinedAirlinesMap == null) {
			this.loadInterlinedAirlines();
		}
		return interlinedAirlinesMap;
	}

	/**
	 * @return the currencies
	 */
	public List<String> getCurrencies() {
		return currencies;
	}

	/**
	 * @param currencies
	 *            the currencies to set
	 */
	public void setCurrencies(List<String> currencies) {
		this.currencies = currencies;
	}

	private void loadHubs() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		this.hubs = commonsDAO.loadHubs();
	}

	private void loadCurrencies() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		this.currencies = commonsDAO.loadActiveCurrency();
	}

	public List<String> getActiveCurrencyList() {
		if (currencies == null) {
			this.loadCurrencies();
		}
		return this.currencies;
	}

	/**
	 * Returns map of HubAirportTO keyed by hub airport code
	 * 
	 * @return
	 */
	public Map<Integer, HubAirportTO> getHubsMap() {
		if (hubs == null) {
			this.loadHubs();
		}
		return this.hubs;
	}

	public Set<String> getHubAirports() {
		if (this.hubAirports == null) {
			hubAirports = new HashSet<String>();
			for (HubAirportTO hub : getHubsMap().values()) {
				hubAirports.add(hub.getAirportCode());
			}
		}
		return hubAirports;
	}

	public boolean isHubAirport(String airportCode) {
		return getHubAirports().contains(airportCode);
	}

	/**
	 * Returns min/max transit durations for given hub airport
	 * 
	 * @param airportCode
	 * @param inwardsCarrierCode
	 * @param outwardsCarrierCode
	 * @return
	 */
	public String[] getMixMaxTransitDurations(String airportCode, String inwardsCarrierCode, String outwardsCarrierCode) {

		String[] minMaxTransitDurations = null;
		String key = airportCode + (inwardsCarrierCode == null ? "null" : inwardsCarrierCode)
				+ (outwardsCarrierCode == null ? "null" : outwardsCarrierCode);
		boolean isHubDefined = true;

		if (cachedHubs != null && cachedHubs.containsKey(key)) {
			minMaxTransitDurations = cachedHubs.get(key);
		} else {
			String[] exactMatchMinMaxTransitDurations = null;
			String[] exactIBMatchMinMaxTransitDurations = null;
			String[] exactOBMatchMinMaxTransitDurations = null;
			String[] mostGeneralMinMaxTransitDurations = null;
			String[] defaultMinMaxTransitDurations = null;// inwardCarrierCode and outwardCarrierCode is not known

			if (this.hubs == null) {
				this.loadHubs();
			}

			if (this.hubs != null) {

				if (inwardsCarrierCode == null && outwardsCarrierCode == null) {// identify the min of min transit time
																				// and max of max transit time
					int minMinTransitDuration = -1;
					int maxMaxTransitDuration = -1;
					String minMinTranistDurationStr = null;
					String maxMaxTransitDurationStr = null;

					for (HubAirportTO hubAirportTO2 : getHubsMap().values()) {
						HubAirportTO hubAirportTO = hubAirportTO2;
						if (airportCode.equals(hubAirportTO.getAirportCode())) {
							int currentMinTransitDuration = getMinutes(hubAirportTO.getMinCxnTime());
							int currentMaxTransitDuration = getMinutes(hubAirportTO.getMaxCxnTime());

							if (minMinTransitDuration == -1 || minMinTransitDuration > currentMinTransitDuration) {
								minMinTransitDuration = currentMinTransitDuration;
								minMinTranistDurationStr = hubAirportTO.getMinCxnTime();
							}

							if (maxMaxTransitDuration == -1 || maxMaxTransitDuration < currentMaxTransitDuration) {
								maxMaxTransitDuration = currentMaxTransitDuration;
								maxMaxTransitDurationStr = hubAirportTO.getMaxCxnTime();
							}
						}
					}
					if (minMinTransitDuration != -1 && maxMaxTransitDuration != -1) {
						defaultMinMaxTransitDurations = new String[] { minMinTranistDurationStr, maxMaxTransitDurationStr };
					} else {
						isHubDefined = false;
					}
				} else {// either inwardCarrierCode or outwardCarrierCode or both are provided

					for (HubAirportTO hubAirportTO2 : getHubsMap().values()) {
						HubAirportTO hubAirportTO = hubAirportTO2;
						if (airportCode.equals(hubAirportTO.getAirportCode())) {
							if (((inwardsCarrierCode == null && hubAirportTO.getInwardsCarrierCode() == null) || (inwardsCarrierCode != null && inwardsCarrierCode
									.equals(hubAirportTO.getInwardsCarrierCode())))
									&& ((outwardsCarrierCode == null && hubAirportTO.getOutwardsCarrierCode() == null) || outwardsCarrierCode != null
											&& outwardsCarrierCode.equals(hubAirportTO.getOutwardsCarrierCode()))) {
								// exact match
								exactMatchMinMaxTransitDurations = new String[] { hubAirportTO.getMinCxnTime(),
										hubAirportTO.getMaxCxnTime() };
							} else if (((inwardsCarrierCode == null && hubAirportTO.getInwardsCarrierCode() == null) || (inwardsCarrierCode != null && inwardsCarrierCode
									.equals(hubAirportTO.getInwardsCarrierCode())))
									&& hubAirportTO.getOutwardsCarrierCode() == null) {
								// exact inbound to any outbound
								exactIBMatchMinMaxTransitDurations = new String[] { hubAirportTO.getMinCxnTime(),
										hubAirportTO.getMaxCxnTime() };
							} else if (hubAirportTO.getInwardsCarrierCode() == null
									&& ((outwardsCarrierCode == null && hubAirportTO.getOutwardsCarrierCode() == null) || outwardsCarrierCode != null
											&& outwardsCarrierCode.equals(hubAirportTO.getOutwardsCarrierCode()))) {
								// exact outbound from any inbound
								exactOBMatchMinMaxTransitDurations = new String[] { hubAirportTO.getMinCxnTime(),
										hubAirportTO.getMaxCxnTime() };
							} else if (hubAirportTO.getInwardsCarrierCode() == null
									&& hubAirportTO.getOutwardsCarrierCode() == null) {
								// any inbound to any out bound
								mostGeneralMinMaxTransitDurations = new String[] { hubAirportTO.getMinCxnTime(),
										hubAirportTO.getMaxCxnTime() };
							}
						}
					}
				}
			}
			if (defaultMinMaxTransitDurations != null) {
				minMaxTransitDurations = defaultMinMaxTransitDurations;
			} else if (exactMatchMinMaxTransitDurations != null) {
				minMaxTransitDurations = exactMatchMinMaxTransitDurations;
			} else if (exactIBMatchMinMaxTransitDurations != null) {
				minMaxTransitDurations = exactIBMatchMinMaxTransitDurations;
			} else if (exactOBMatchMinMaxTransitDurations != null) {
				minMaxTransitDurations = exactOBMatchMinMaxTransitDurations;
			} else if (mostGeneralMinMaxTransitDurations != null) {
				minMaxTransitDurations = mostGeneralMinMaxTransitDurations;
			} else {
				isHubDefined = false;
			}

			if (!isHubDefined) {
				log.error("Min/Max transit time not defined. " + "[airportCode=" + airportCode + ",inwardsCarrierCode="
						+ inwardsCarrierCode + ",outwardsCarrierCode=" + outwardsCarrierCode + "]");
				throw new ModuleRuntimeException("commons.data.transitdurations.notconfigured");
			}

			if (minMaxTransitDurations != null) {
				if (cachedHubs == null) {
					cachedHubs = new HashMap<String, String[]>();
				}
				cachedHubs.put(key, minMaxTransitDurations);
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("Min/Max Transit duration [airportCode=" + airportCode + ",inwardsCarrierCode=" + inwardsCarrierCode
					+ ",outwardsCarrierCode=" + outwardsCarrierCode + ",min Transit duration="
					+ (minMaxTransitDurations != null && minMaxTransitDurations.length == 2 ? minMaxTransitDurations[0] : "")
					+ ",max Transit duration="
					+ (minMaxTransitDurations != null && minMaxTransitDurations.length == 2 ? minMaxTransitDurations[1] : "")
					+ "]");
		}
		return minMaxTransitDurations;
	}

	private int getMinutes(String transitDurationStr) {
		String hours = transitDurationStr.substring(0, transitDurationStr.indexOf(":"));
		String mins = transitDurationStr.substring(transitDurationStr.indexOf(":") + 1);
		return Integer.parseInt(hours) * 60 + Integer.parseInt(mins);
	}

	public Collection<PaxTypeTO> getPaxTypes() {
		if (paxTypesMap == null) {
			this.loadPaxTypes();
		}
		return paxTypesMap.values();
	}

	public Map<String, PaxTypeTO> getPaxTypeMap() {
		if (paxTypesMap == null) {
			synchronized (this) {
				if (paxTypesMap == null) {
					this.loadPaxTypes();
				}
			}
		}
		return paxTypesMap;
	}

	public PaxContactConfigDTO getPaxContactConfig(String appName) {
		if (paxContactConfigDTOMap == null) {
			paxContactConfigDTOMap = new HashMap<String, PaxContactConfigDTO>();
		}

		if (!paxContactConfigDTOMap.containsKey(appName)) {
			paxContactConfigDTO = new PaxContactConfigDTO();
			// Load Pax Configurations
			paxContactConfigDTO.setPaxConfigList(getpaxConfig(appName));
			// Load contact configurations
			paxContactConfigDTO.setContactConfigList(getContactDetailsConfig(appName));
			paxContactConfigDTOMap.put(appName, paxContactConfigDTO);
		}

		return paxContactConfigDTOMap.get(appName);

	}

	public List<BaseContactConfigDTO> getContactDetailsConfig(String appName) {
		if (AppIndicatorEnum.APP_XBE.toString().equals(appName)) {
			if (xbeContactConfigDTOList == null) {
				this.loadContactDetailsConfig(appName);
			}
			contactConfigDTOList = xbeContactConfigDTOList;
		} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
			if (ibeContactConfigDTOList == null) {
				this.loadContactDetailsConfig(appName);
			}
			contactConfigDTOList = ibeContactConfigDTOList;
		} else {
			if (wsContactConfigDTOList == null) {
				this.loadContactDetailsConfig(appName);
			}

			contactConfigDTOList = wsContactConfigDTOList;
		}

		return contactConfigDTOList;
	}

	public List<BasePaxConfigDTO> getpaxConfig(String appName) {
		if (AppIndicatorEnum.APP_XBE.toString().equals(appName)) {
			if (xbePaxConfigList == null) {
				this.loadPaxDetailsConfig(appName);
			}
			paxConfigDTOList = xbePaxConfigList;
		} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
			if (ibePaxConfigList == null) {
				this.loadPaxDetailsConfig(appName);
			}
			paxConfigDTOList = ibePaxConfigList;
		} else if (AppIndicatorEnum.APP_GDS.toString().equals(appName)) {
			if (gdsPaxConfigList == null) {
				this.loadPaxDetailsConfig(appName);
			}
			paxConfigDTOList = gdsPaxConfigList;
		} else {
			if (wsPaxConfigList == null) {
				this.loadPaxDetailsConfig(appName);
			}
			paxConfigDTOList = wsPaxConfigList;
		}
		return paxConfigDTOList;
	}

	public List<UserRegConfigDTO> getUserRegConfig() {
		if (userRegContactConfigDTOList == null) {
			this.loadUserRegConfig();
		}

		return userRegContactConfigDTOList;
	}
	
	public Collection<AdminFeeAgentTO> getAdminFeeAgentCachedList() {
		if (adminFeeAgentList == null) {
			this.loadAdminFeeAgents();
		}
		return adminFeeAgentList;
	}

	public Collection<PaxCategoryTO> getPaxCategory() {
		if (paxCategoryList == null) {
			this.loadPaxCategories();
		}
		return paxCategoryList;
	}

	public Map<String, GDSStatusTO> getActiveGdsMap() {
		if (gdsMap == null) {
			this.loadGdsConfigs();
		}
		return gdsMap;
	}

	public Map<String, ServiceClientDTO> getServiceClientsMap() {
		if (serviceClientsMap == null) {
			this.loadServiceClients();
		}
		return serviceClientsMap;
	}

	public Map<String, ServiceBearerDTO> getServiceBearersMap() {
		if (serviceBearersMap == null) {
			this.loadServiceBearers();
		}
		return serviceBearersMap;
	}

	public Map<String, String> getIataAircraftCodes() {
		if (iataModelMap == null) {
			this.loadIataAircraftCodes();
		}
		return iataModelMap;
	}

	private void loadFareCategories() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		fareCategoryMap = commonsDAO.getFareCategoryMap();
	}

	public Map<String, FareCategoryTO> getFareCategories() {
		if (fareCategoryMap == null) {
			this.loadFareCategories();
		}

		return fareCategoryMap;
	}

	private void loadChargeApplicabilities() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		chargeApplicabilitiesMap = commonsDAO.loadChargeApplicabilitiesMap();
	}

	public Map<Integer, ChargeApplicabilityTO> getChargeApplicabilities() {
		if (chargeApplicabilitiesMap == null) {
			this.loadChargeApplicabilities();
		}

		return chargeApplicabilitiesMap;
	}

	private void loadAirportMessages() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		airportMessagesMap = commonsDAO.getAirportMessagesMap();
	}

	public String getAviatorFtpServerName() {
		return aviatorFtpServerName;
	}

	public String getAviatorFtpServerUserName() {
		return aviatorFtpServerUserName;
	}

	public String getAviatorFtpServerPassword() {
		return aviatorFtpServerPassword;
	}

	public void setAviatorFtpServerName(String aviatorFtpServerName) {
		this.aviatorFtpServerName = aviatorFtpServerName;
	}

	public void setAviatorFtpServerUserName(String aviatorFtpServerUserName) {
		this.aviatorFtpServerUserName = aviatorFtpServerUserName;
	}

	public void setAviatorFtpServerPassword(String aviatorFtpServerPassword) {
		this.aviatorFtpServerPassword = aviatorFtpServerPassword;
	}

	public Integer getAviatorFtpPort() {
		return aviatorFtpPort;
	}

	public String getAviatorFtpRemotePath() {
		return aviatorFtpRemotePath;
	}

	public void setAviatorFtpPort(Integer aviatorFtpPort) {
		this.aviatorFtpPort = aviatorFtpPort;
	}

	public void setAviatorFtpRemotePath(String aviatorFtpRemotePath) {
		this.aviatorFtpRemotePath = aviatorFtpRemotePath;
	}

	public Map<String, ArrayList<String>> getAirportMessages() {
		if (airportMessagesMap == null) {
			this.loadAirportMessages();
		}

		return airportMessagesMap;
	}

	public Object[] getAirportInfo(String airportCode, boolean cacheRefresh) {
		Object[] airportInfo = getAirportInfoMap(null).get(airportCode);
		if (airportInfo == null && cacheRefresh) {
			// Updated the cached airport list
			Map<String, Object[]> newAirportMap = getAirportInfo(airportCode);
			if (newAirportMap != null && newAirportMap.containsKey(airportCode)) {
				airportInfo = newAirportMap.get(airportCode);
				airportInfoMap.put(airportCode, airportInfo);
			}
		}
		return airportInfo;
	}

	/**
	 * Get the list of interline carriers list
	 * 
	 * @return
	 */
	public Collection<String> getInterlineCarriers() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO.getInterlineCarrierCodes();
	}

	public List<AirportMessageDisplayDTO> airportMessages(List<AirportFlightDetail> airportFlightDetails) {
		if (airportMessages == null) {
			loadNewAirportMessages();
		}

		Map<Integer, AirportMessageDisplayDTO> requestedAirportMessagesMap = new HashMap<Integer, AirportMessageDisplayDTO>();

		for (AirportFlightDetail airportFlightDetail : airportFlightDetails) {
			String key = airportFlightDetail.getAirportCode() + KEY_SEPERATOR + airportFlightDetail.getStage();
			if (airportMessages.get(key) != null) {
				AirportMessagesUtil.extractAirportMessageDisplayDTO(airportFlightDetail, requestedAirportMessagesMap,
						airportMessages.get(key));
			}
		}

		return new ArrayList<AirportMessageDisplayDTO>(requestedAirportMessagesMap.values());
	}

	private Map<String, Object[]> getAirportInfoMap(String airportCode) {
		if (airportInfoMap == null) {
			this.loadAirportInfo(airportCode);
		}
		return airportInfoMap;
	}

	private Map<String, Object[]> getAirportInfo(String airportCode) {
		Map<String, Object[]> airportInfo = null;
		if (getAirportInfoMap(airportCode).get(airportCode) == null) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			airportInfo = commonsDAO.getAirportInfo(airportCode);
		} else {
			airportInfo = getAirportInfoMap(airportCode);
		}
		return airportInfo;
	}

	private void loadAirportInfo(String airportCode) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		airportInfoMap = commonsDAO.getAirportInfo(airportCode);
	}
	
	public Object[] retrieveAirportInfo(String airportCode) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		Map<String, Object[]> airportInfo = commonsDAO.getAirportInfo(airportCode);
		return airportInfo.get(airportCode);
	}
	
	public Object[] retrieveCityInfo(String cityCode) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		Map<String, Object[]> airportInfo = commonsDAO.getCityInfo(cityCode);
		return airportInfo.get(cityCode);
	}

	public Map<String, String> getStateCodesForAirport(List<String> airportCodes) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO.getStateCodesForAirport(airportCodes);
	}

	public Map<String, List<PaxCountryConfigDTO>> getCountryWiseContactConfigs(String appName) {
		if (countryWisePaxConfigs != null && countryWisePaxConfigs.size() == 0) {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			countryWisePaxConfigs = commonsDAO.getPaxCountryWiseConfigurations();
		}

		return countryWisePaxConfigs.get(appName);
	}

	/**
	 * Gets the list of charge adjustment types in the system.
	 * 
	 * @return : a list of {@link ChargeAdjustmentTypeDTO}s.
	 */
	public List<ChargeAdjustmentTypeDTO> getChargeAdjustmentTypes() {
		if (this.chargeAdjustmentTypes == null) {
			this.loadChargeAdjustmentTypes();
		}
		return chargeAdjustmentTypes;
	}

	private void loadChargeAdjustmentTypes() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		chargeAdjustmentTypes = commonsDAO.getChargeAdjustmentTypes();
	}

	/**
	 * Returns the default charge adjustment type object of type {@link ChargeAdjustmentTypeDTO}.
	 * 
	 * @return
	 */
	public ChargeAdjustmentTypeDTO getDefaultChargeAdjustmentType() {
		if (defaultChargeAdjustment == null) {
			for (ChargeAdjustmentTypeDTO chargeAdjustment : this.getChargeAdjustmentTypes()) {
				if (chargeAdjustment.getDefaultAdjustment()) {
					this.defaultChargeAdjustment = chargeAdjustment;
					break;
				}
			}
		}
		return defaultChargeAdjustment;
	}

	/**
	 * @return Map<String,String> map of active carriers in the system. Key = Carrier code, Value = Description
	 */
	public Map<String, String> getActiveSysCarriersMap() {
		if (this.activeCarriersMap == null) {
			loadCarriers("ACT");
		}
		return this.activeCarriersMap;
	}

	/**
	 * @return Map<String,String> map of all system carriers in the system. Key = Carrier code, Value = Description
	 */
	public Map<String, String> getAllSysCarriersMap() {
		if (this.allCarriersMap == null) {
			loadCarriers(null);
		}
		return this.allCarriersMap;
	}

	/**
	 * Returns all the active cabin classes in the system
	 * 
	 * @return
	 */
	public Map<String, String> getActiveCabinClassesMap() {
		if (this.activeSystemCabinClassesMap == null) {
			loadActiveCabinClasses();
		}
		return this.activeSystemCabinClassesMap;
	}

	private void loadActiveCabinClasses() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		activeSystemCabinClassesMap = platformDAO.getActiveCabinClasses();
	}

	public Map<String, CabinClassDTO> getActiveCabinClassDTOMap() {
		if (this.activeSystemCabinClassDTOMap == null) {
			loadActiveCabinClassDTOMap();
		}
		return this.activeSystemCabinClassDTOMap;
	}

	private void loadActiveCabinClassDTOMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		activeSystemCabinClassDTOMap = platformDAO.getActiveCabinClassDTOs();
	}

	// Haider 23Oct08
	private void loadPublishMechanism() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		publishMechanismMap = platformDAO.getPublishMechanism();
	}

	private void loadBookingClassCharMapping() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		bookingClassCharMappingMap = platformDAO.getBookingClassCharMappingMap();
	}

	private void loadGDSBookingClassMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		gdsBookingClassMap = platformDAO.getGDSBookingClassMap();
	}

	private void loadCSBookingClassMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		csBookingClassMap = platformDAO.getCSBookingClassMap();
	}

	private void loadSingleEntryMappedGDSBookingClasses() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		gdsMappedBookingClassList = platformDAO.getSingleEntryMappedGDSBookingClasses();
	}

	private void loadBookingClassByLogicalCCMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		bookingClassByLogicalCCMap = platformDAO.getBookingClassByLogicalCCMap();
	}

	private void loadServiceTypesByOperationType() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		serviceTypesByOperationType = platformDAO.getServiceTypesByOperationTypeMap();
	}

	public String getAdminServerAddressAndPort() {
		return adminServerAddressAndPort;
	}

	public void setAdminServerAddressAndPort(String adminServerAddressAndPort) {
		this.adminServerAddressAndPort = adminServerAddressAndPort;
	}

	public String getXbeServerAddressAndPort() {
		return xbeServerAddressAndPort;
	}

	public void setXbeServerAddressAndPort(String xbeServerAddressAndPort) {
		this.xbeServerAddressAndPort = xbeServerAddressAndPort;
	}

	/**
	 * @return Returns the cookieName.
	 */
	public String getCookieName() {
		return cookieName;
	}

	/**
	 * @param cookieName
	 *            The cookieName to set.
	 */
	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	/**
	 * @return Returns the cookieValueMap.
	 */
	public Map<String, String> getCookieValueMap() {
		return cookieValueMap;
	}

	public Integer getNextValueFromSequence(String sequneceName) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO.getSequenceNextValue(sequneceName);
	}

	/**
	 * @param cookieValueMap
	 *            The cookieValueMap to set.
	 */
	public void setCookieValueMap(Map<String, String> cookieValueMap) {
		this.cookieValueMap = cookieValueMap;
	}

	public boolean isGenerateAppCookie() {
		return generateAppCookie;
	}

	public void setGenerateAppCookie(boolean generateAppCookie) {
		this.generateAppCookie = generateAppCookie;
	}

	public boolean isControlAirAdminCachingHeaders() {
		return controlAirAdminCachingHeaders;
	}

	public void setControlAirAdminCachingHeaders(boolean controlAirAdminCachingHeaders) {
		this.controlAirAdminCachingHeaders = controlAirAdminCachingHeaders;
	}

	public boolean isControlIBECachingHeaders() {
		return controlIBECachingHeaders;
	}

	public void setControlIBECachingHeaders(boolean controlIBECachingHeaders) {
		this.controlIBECachingHeaders = controlIBECachingHeaders;
	}

	public boolean isControlXBECachingHeaders() {
		return controlXBECachingHeaders;
	}

	public void setControlXBECachingHeaders(boolean controlXBECachingHeaders) {
		this.controlXBECachingHeaders = controlXBECachingHeaders;
	}

	public Map<String, String> getCacheControlProperties() {
		return cacheControlProperties;
	}

	public void setCacheControlProperties(Map<String, String> cacheControlProperties) {
		this.cacheControlProperties = cacheControlProperties;
	}

	public boolean isLogAirAdminAccessInfo() {
		return logAirAdminAccessInfo;
	}

	public void setLogAirAdminAccessInfo(boolean logAirAdminAccessInfo) {
		this.logAirAdminAccessInfo = logAirAdminAccessInfo;
	}

	public boolean isLogIBEAccessInfo() {
		return logIBEAccessInfo;
	}

	public void setLogIBEAccessInfo(boolean logIBEAccessInfo) {
		this.logIBEAccessInfo = logIBEAccessInfo;
	}

	public boolean isLogXBEAccessInfo() {
		return logXBEAccessInfo;
	}

	public void setLogXBEAccessInfo(boolean logXBEAccessInfo) {
		this.logXBEAccessInfo = logXBEAccessInfo;
	}

	/**
	 * @return Returns the ftpServerName.
	 */
	public String getFtpServerName() {
		return ftpServerName;
	}

	/**
	 * @param ftpServerName
	 *            The ftpServerName to set.
	 */
	public void setFtpServerName(String ftpServerName) {
		this.ftpServerName = ftpServerName;
	}

	/**
	 * @return Returns the ftpServerPassword.
	 */
	public String getFtpServerPassword() {
		return ftpServerPassword;
	}

	/**
	 * @param ftpServerPassword
	 *            The ftpServerPassword to set.
	 */
	public void setFtpServerPassword(String ftpServerPassword) {
		this.ftpServerPassword = ftpServerPassword;
	}

	/**
	 * @return Returns the ftpServerUserName.
	 */
	public String getFtpServerUserName() {
		return ftpServerUserName;
	}

	/**
	 * @param ftpServerUserName
	 *            The ftpServerUserName to set.
	 */
	public void setFtpServerUserName(String ftpServerUserName) {
		this.ftpServerUserName = ftpServerUserName;
	}

	/**
	 * @return Returns the lastBuildVersion.
	 */
	public String getLastBuildVersion() {
		return lastBuildVersion;
	}

	/**
	 * @param lastBuildVersion
	 *            The lastBuildVersion to set.
	 */
	public void setLastBuildVersion(String lastBuildVersion) {
		this.lastBuildVersion = lastBuildVersion;
	}

	public String getDefaultAACarrierCode() {
		return defaultAACarrierCode;
	}

	public void setDefaultAACarrierCode(String defaultAACarrierCode) {
		this.defaultAACarrierCode = defaultAACarrierCode;
	}

	/**
	 * Haider 23Oct08 Returns publish mechanism map
	 * 
	 * @return
	 */
	public Map<String, String> getPublishMechanismMap() {
		if (this.publishMechanismMap == null) {
			loadPublishMechanism();
		}
		return this.publishMechanismMap;
	}

	public Map<BookingClassCharMappingTO, String> getBookingClassCharMappingMap() {
		if (this.bookingClassCharMappingMap == null) {
			loadBookingClassCharMapping();
		}
		return this.bookingClassCharMappingMap;
	}

	public Map<Integer, Map<String, String>> getGdsBookingClassMap() {
		if (this.gdsBookingClassMap == null) {
			loadGDSBookingClassMap();
		}
		return this.gdsBookingClassMap;
	}

	public Map<Integer, Map<String, String>> getCsBookingClassMap() {
		if (this.csBookingClassMap == null) {
			loadCSBookingClassMap();
		}
		return this.csBookingClassMap;
	}

	public Map<Integer, List<String>> getSingleEntryMappedGDSBookingClasses() {
		if (this.gdsMappedBookingClassList == null) {
			loadSingleEntryMappedGDSBookingClasses();
		}
		return this.gdsMappedBookingClassList;
	}

	public Map<String, List<String>> getBookingClassByLogicalCCMap() {
		if (this.bookingClassByLogicalCCMap == null) {
			loadBookingClassByLogicalCCMap();
		}
		return this.bookingClassByLogicalCCMap;
	}

	public Map<Integer, List<String>> getServiceTypesByOperationType() {
		if (this.serviceTypesByOperationType == null) {
			loadServiceTypesByOperationType();
		}
		return this.serviceTypesByOperationType;
	}

	private void loadSsrCategoryMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		ssrCategoryMap = platformDAO.getSsrCategoryMap();
	}

	public Map<Integer, SSRCategoryDTO> getSsrCategoryMap() {

		checkSSRRefreshNeeded();

		if (ssrCategoryMap == null) {
			this.loadSsrCategoryMap();
		}

		return ssrCategoryMap;
	}

	public Map<Integer, ReturnValidityPeriodTO> getReturnValidityPeriods() {
		if (returnValidityPeriods == null) {
			this.loadReturnValidityPeriods();
		}

		return returnValidityPeriods;
	}

	private void loadReturnValidityPeriods() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		returnValidityPeriods = commonsDAO.getReturnValidityPeriods();
	}

	public Map<Integer, SSRInfoDTO> getSSRInfoByIdMap() {

		checkSSRRefreshNeeded();

		if (sSRInfoByIdMap == null) {
			this.loadSSRInfoMap();
		}

		return sSRInfoByIdMap;
	}

	public Map<String, SSRInfoDTO> getSSRInfoByCodeMap() {

		checkSSRRefreshNeeded();

		if (sSRInfoByCodeMap == null) {
			this.loadSSRInfoMap();
		}

		return sSRInfoByCodeMap;
	}

	private void loadSSRInfoMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		sSRInfoByIdMap = commonsDAO.getSSRInfoMap();
		sSRInfoByCodeMap = new HashMap<String, SSRInfoDTO>();

		for (SSRInfoDTO ssrInfoDTO : sSRInfoByIdMap.values()) {
			SSRInfoDTO ssr = ssrInfoDTO;

			sSRInfoByCodeMap.put(ssr.getSSRCode(), ssr);
		}
	}

	public Map<String, Boolean> getSSRApplicableAirportsMap() {

		checkSSRRefreshNeeded();

		if (sSRApplicableAirportsMap == null) {
			this.loadSSRApplicableAirportsMap();
		}

		return sSRApplicableAirportsMap;
	}

	private void loadSSRApplicableAirportsMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);

		sSRApplicableAirportsMap = commonsDAO.getSSRApplicableAirportsMap();
	}

	public Map<Integer, String> getSSRCategoryEMailMap() {

		checkSSRRefreshNeeded();

		if (sSRCategoryEMailMap == null) {
			this.loadSSRCategoryEMailMap();
		}

		return sSRCategoryEMailMap;
	}

	private boolean isLogicalCCRefreshNeeded() {
		if ((System.currentTimeMillis() - logicalCCRecordAge) >= masterDataCacheTimeout) {
			return true;
		}
		return false;
	}

	private void checkSSRRefreshNeeded() {
		if ((System.currentTimeMillis() - recordAge.getTime()) >= masterDataCacheTimeout) {
			recordAge.setTime(System.currentTimeMillis());
			ssrCategoryMap = null;
			sSRInfoByIdMap = null;
			sSRInfoByCodeMap = null;
			sSRApplicableAirportsMap = null;
			sSRCategoryEMailMap = null;
		}
	}

	/*
	 * Checks if the terms and conditions cache age has passed and if it needs to reload. Current implementation will
	 * set the termsAndConditions map to null if the cache age has expired.
	 */
	private void checkTermsRefreshNeeded() {
		if ((System.currentTimeMillis() - termsAndConditionAge.getTime()) >= masterDataCacheTimeout) {
			termsAndConditionAge.setTime(System.currentTimeMillis());
			termsAndConditions = null;
		}
	}

	private void checkItineraryChargeHideCurrenciesRefreshNeeded() {
		if ((System.currentTimeMillis() - hideItineraryChargeCurrenciesAge.getTime()) >= masterDataCacheTimeout) {
			hideItineraryChargeCurrenciesAge.setTime(System.currentTimeMillis());
			chargeHideCurrencies = null;
		}
	}

	private void loadSSRCategoryEMailMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		sSRCategoryEMailMap = commonsDAO.getSSRCategoryEMailMap();
	}

	public Map<String, String> getPetroFacAgentsMap() {
		if (petroFacEnabledAgentsMap == null) {
			this.loadPetroFacEnabledAgentsMap();
		}

		return petroFacEnabledAgentsMap;
	}

	private void loadPetroFacEnabledAgentsMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		petroFacEnabledAgentsMap = commonsDAO.getPetroFacEnabledAgentsMap();
	}

	public Map<String, String> getChargeGroupCodeNDescMap() {
		if (chargeGroupCodeNDescMap == null) {
			this.loadChargeGroupCodeNDescMap();
		}

		return chargeGroupCodeNDescMap;
	}

	private void loadChargeGroupCodeNDescMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		chargeGroupCodeNDescMap = commonsDAO.getChargeGroupCodeNDescMap();
	}

	private void loadActualPayModes() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		this.actualPayModes = commonsDAO.getActualPaymentMethods();
	}

	/**
	 * Returns actual payment modes
	 * 
	 * @return
	 */
	public HashMap<Integer, String> getActualPayModes() {
		try {
			if (actualPayModes == null) {
				this.loadActualPayModes();
			}
		} catch (Exception e) {
			return null;
		}
		return this.actualPayModes;
	}

	public String getHttpProxy() {
		return httpProxy;
	}

	public void setHttpProxy(String httpProxy) {
		this.httpProxy = httpProxy;
	}

	public Integer getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(Integer httpPort) {
		this.httpPort = httpPort;
	}

	/**
	 * @return the masterDataCacheTimeout
	 */
	public long getMasterDataCacheTimeout() {
		return masterDataCacheTimeout;
	}

	/**
	 * @param masterDataCacheTimeout
	 *            the masterDataCacheTimeout to set
	 */
	public void setMasterDataCacheTimeout(long masterDataCacheTimeout) {
		this.masterDataCacheTimeout = masterDataCacheTimeout;
	}

	public Map<String, String> getCarrierUrls() {
		return carrierUrls;
	}

	public void setCarrierUrls(Map<String, String> carrierUrls) {
		this.carrierUrls = carrierUrls;
	}

	public Map<String, String> getCarrierUrlsIBE() {
		return carrierUrlsIBE;
	}

	public void setCarrierUrlsIBE(Map<String, String> carrierUrlsIBE) {
		this.carrierUrlsIBE = carrierUrlsIBE;
	}

	public String getStaticFileSuffix() {
		return staticFileSuffix;
	}

	public void setStaticFileSuffix(String staticFileSuffix) {
		this.staticFileSuffix = staticFileSuffix;
	}

	// TODO try to reduce the number of maps used
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void loadGroundServiceMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		Map[] arrGroundStationData = platformDAO.getGroundStationDetailsMap();
		Map[] groundSegmentConnectivityArr = platformDAO.getParentStationDetailsMap();

		mapGroundStationNames.putAll(arrGroundStationData[0]);
		mapPrimaryStationCode.putAll(arrGroundStationData[1]);
		mapPrimaryStationBySubStation.putAll(arrGroundStationData[2]);
		mapParentStationByGroundStation.putAll(groundSegmentConnectivityArr[0]);
		mapTopLevelStationByConnectedStation.putAll(groundSegmentConnectivityArr[1]);
		mapConnectingAirportDetail.putAll(groundSegmentConnectivityArr[2]);
		mapAllAirportCodes = platformDAO.getAllAirportCodeData();

	}

	public Map<String, String> getGroundStationNamesMap() {
		return mapGroundStationNames;
	}

	public Map<String, Collection<String>> getPrimaryStationCodeMap() {
		return mapPrimaryStationCode;
	}

	public Map<String, String> getMapPrimaryStationBySubStation() {
		return mapPrimaryStationBySubStation;
	}

	public Map<String, List<String[]>> getMapParentStationByGroundStation() {
		return mapParentStationByGroundStation;
	}

	public Map<String, String[]> getMapAllAirportCodes() {
		return mapAllAirportCodes;
	}

	public Map<String, String> getMapTopLevelStationByConnectedStation() {
		return mapTopLevelStationByConnectedStation;
	}

	public Collection<String> getLogAccessInfoIncludes() {
		return logAccessInfoIncludes;
	}

	public void setLogAccessInfoIncludes(Collection<String> logAccessInfoIncludes) {
		this.logAccessInfoIncludes = logAccessInfoIncludes;
	}

	public Map<String, String> getMapConnectingAirportDetail() {
		return mapConnectingAirportDetail;
	}

	public boolean isLogIBEReqParams() {
		return logIBEReqParams;
	}

	public void setLogIBEReqParams(boolean logIBEReqParams) {
		this.logIBEReqParams = logIBEReqParams;
	}

	public boolean isLogXBEReqParams() {
		return logXBEReqParams;
	}

	public void setLogXBEReqParams(boolean logXBEReqParams) {
		this.logXBEReqParams = logXBEReqParams;
	}

	public boolean isLogAirAdminReqParams() {
		return logAirAdminReqParams;
	}

	public void setLogAirAdminReqParams(boolean logAirAdminReqParams) {
		this.logAirAdminReqParams = logAirAdminReqParams;
	}

	public String getAmadeusFtpServerName() {
		return amadeusFtpServerName;
	}

	public void setAmadeusFtpServerName(String amadeusFtpServerName) {
		this.amadeusFtpServerName = amadeusFtpServerName;
	}

	public String getAmadeusFtpServerUserName() {
		return amadeusFtpServerUserName;
	}

	public void setAmadeusFtpServerUserName(String amadeusFtpServerUserName) {
		this.amadeusFtpServerUserName = amadeusFtpServerUserName;
	}

	public String getAmadeusFtpServerPassword() {
		return amadeusFtpServerPassword;
	}

	public void setAmadeusFtpServerPassword(String amadeusFtpServerPassword) {
		this.amadeusFtpServerPassword = amadeusFtpServerPassword;
	}

	public Integer getAmadeusFtpPort() {
		return amadeusFtpPort;
	}

	public void setAmadeusFtpPort(Integer amadeusFtpPort) {
		this.amadeusFtpPort = amadeusFtpPort;
	}

	public boolean getUseProxy() {
		if (SystemPropertyUtil.isDisableProxyGlobally()) {
			return false;
		} else {
			return useProxy;
		}
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}
	
	public List<AdminFeeAgentTO> getAdminFeeAgentList() {
		return adminFeeAgentList;
	}

	public void setAdminFeeAgentList(List<AdminFeeAgentTO> adminFeeAgentList) {
		this.adminFeeAgentList = adminFeeAgentList;
	}

	public List<PaxCategoryTO> getPaxCategoryList() {
		return paxCategoryList;
	}

	public void setPaxCategoryList(List<PaxCategoryTO> paxCategoryList) {
		this.paxCategoryList = paxCategoryList;
	}

	public String getAmadeusFtpRemotePath() {
		return amadeusFtpRemotePath;
	}

	public void setAmadeusFtpRemotePath(String amadeusFtpRemotePath) {
		this.amadeusFtpRemotePath = amadeusFtpRemotePath;
	}

	public boolean isNoCaching() {
		return noCaching;
	}

	public void setNoCaching(boolean noCaching) {
		this.noCaching = noCaching;
	}

	public Map<String, String> getCarrierAirlineCodeMap() {
		if (this.carrierAirlineCodeMap == null) {
			loadCarrierAirlineCodeMap();
		}
		return this.carrierAirlineCodeMap;
	}

	private void loadCarrierAirlineCodeMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		this.carrierAirlineCodeMap = commonsDAO.getCarrierAilineCodeMap();
	}

	public boolean isAllowProcessPfsBeforeFlightDeparture() {
		return allowProcessPfsBeforeFlightDeparture;
	}

	public void setAllowProcessPfsBeforeFlightDeparture(boolean allowProcessPfsBeforeFlightDeparture) {
		this.allowProcessPfsBeforeFlightDeparture = allowProcessPfsBeforeFlightDeparture;
	}

	public void resetLoadedAgents() {
		synchronized (this) {
			this.agents = null;
		}
	}

	public Map<String, String> getAgentsByAgentCode() {
		if (this.agents == null) {
			synchronized (this) {
				if (this.agents == null) {
					LookupService lookup = LookupServiceFactory.getInstance();
					CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
					this.agents = commonsDAO.getAgentsByAgentCode();
				}
			}

		}
		return agents;
	}

	public boolean isIncludeCancelledSSRDetailsInReport() {
		return includeCancelledSSRDetailsInReport;
	}

	public void setIncludeCancelledSSRDetailsInReport(boolean includeCancelledSSRDetailsInReport) {
		this.includeCancelledSSRDetailsInReport = includeCancelledSSRDetailsInReport;
	}

	/**
	 * 
	 * @param isForceRefreshCache
	 * @return
	 */
	public Collection<String> getWhiteListedEmailDomains(boolean isForceRefreshCache) {

		if (isForceRefreshCache || isEmailDomainRefereshNeeded()) {
			emailDomainListAge = System.currentTimeMillis();
			Collection<String> emailDomainList = CommonsDAOUtil.getCommonsHibernateDAO().getActiveDomainNames();
			whiteListedEmailDomains = emailDomainList;
		}
		return whiteListedEmailDomains;
	}

	private boolean isEmailDomainRefereshNeeded() {
		if ((System.currentTimeMillis() - emailDomainListAge) >= masterDataCacheTimeout) {
			return true;
		}
		return false;
	}

	/**
	 * Once a new valid domain is found, we will persist that to the DB and efresh the cache forcefully, so that next
	 * time it is taken from the cahce instead of doing the validation all over again
	 * 
	 * @param hostName
	 */
	public void persistAndRefreshEmailDomainCache(String hostName) {

		if (hostName != null && !hostName.equals("")) {
			WhiteListedEmailDomains emailDomains = new WhiteListedEmailDomains();
			emailDomains.setDomainName(hostName);
			emailDomains.setStatus(WhiteListedEmailDomains.STATUS_ACT);

			CommonsDAOUtil.getCommonsHibernateDAO().saveOrUpdateEntity(WhiteListedEmailDomains.class.getName(), emailDomains);

			getWhiteListedEmailDomains(true);

		}
	}

	/**
	 * @return the ftpEnablePassiveMode
	 */
	public boolean isFtpEnablePassiveMode() {
		return ftpEnablePassiveMode;
	}

	/**
	 * @param ftpEnablePassiveMode
	 *            the ftpEnablePassiveMode to set
	 */
	public void setFtpEnablePassiveMode(boolean ftpEnablePassiveMode) {
		this.ftpEnablePassiveMode = ftpEnablePassiveMode;
	}

	public Map<String, String> getCodeshareBcCosMap() {
		if (codeshareBcCosMap == null) {
			this.loadCodeshareBcCosMap();
		}

		return codeshareBcCosMap;
	}

	private void loadCodeshareBcCosMap() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		codeshareBcCosMap = commonsDAO.getCodeShareBcCosMap();
	}

	public List<String> getPostPayloadDumpURLKeywords() {
		return postPayloadDumpURLKeywords;
	}

	public void setPostPayloadDumpURLKeywords(List<String> postPayloadDumpURLKeywords) {
		this.postPayloadDumpURLKeywords = postPayloadDumpURLKeywords;
	}

	public boolean isControlXBESecureHeaders() {
		return controlXBESecureHeaders;
	}

	public void setControlXBESecureHeaders(boolean controlXBESecureHeaders) {
		this.controlXBESecureHeaders = controlXBESecureHeaders;
	}

	public boolean isControlIBESecureHeaders() {
		return controlIBESecureHeaders;
	}

	public void setControlIBESecureHeaders(boolean controlIBESecureHeaders) {
		this.controlIBESecureHeaders = controlIBESecureHeaders;
	}

	public boolean isControlAirAdminSecureHeaders() {
		return controlAirAdminSecureHeaders;
	}

	public void setControlAirAdminSecureHeaders(boolean controlAirAdminSecureHeaders) {
		this.controlAirAdminSecureHeaders = controlAirAdminSecureHeaders;
	}

	public boolean isSecureHeadersIframeDeny() {
		return secureHeadersIframeDeny;
	}

	public void setSecureHeadersIframeDeny(boolean secureHeadersIframeDeny) {
		this.secureHeadersIframeDeny = secureHeadersIframeDeny;
	}

	public boolean isSecureHeadersIframeSameOrigin() {
		return secureHeadersIframeSameOrigin;
	}

	public void setSecureHeadersIframeSameOrigin(boolean secureHeadersIframeSameOrigin) {
		this.secureHeadersIframeSameOrigin = secureHeadersIframeSameOrigin;
	}

	public Merchant getMerchantByID(String merchantID) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		return commonsDAO.getMerchantByID(merchantID);
	}
	
	public boolean isCacheAppParamsInCachingDB() {
		return cacheAppParamsInCachingDB;
	}

	public Map<String, String> getCountryServiceTaxReportCategory() {
		if (countryServiceTaxReportCategory == null) {
			this.loadCountryServiceTaxReportCategory();
		}

		return countryServiceTaxReportCategory;
	}

	private void loadCountryServiceTaxReportCategory() {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
		countryServiceTaxReportCategory = commonsDAO.getCountryServiceTaxReportCategory();
	}

	public void setCacheAppParamsInCachingDB(boolean cacheAppParamsInCachingDB) {
		this.cacheAppParamsInCachingDB = cacheAppParamsInCachingDB;
	}
}
