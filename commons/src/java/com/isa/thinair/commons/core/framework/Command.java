/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.util.Collection;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command interface to represent command
 * 
 * @author Lasantha Pambagoda
 */
public interface Command {

	public ServiceResponce execute() throws ModuleException;

	public void setParameter(String paramName, Object param);

	public Object getParameter(String paramName);

	public Collection getParameterNames();

	public void clearCommandParameters();
}
