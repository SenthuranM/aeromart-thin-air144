package com.isa.thinair.commons.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Custom Class Loader
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * 
 *        TODO finds ways to improve.
 */
public class CustomClassLoader extends ClassLoader {

	private List classRepository = new Vector();

	private Map maploadedClasses = new HashMap();

	private List colExcludeApiList = new Vector();

	/**
	 * Pass JarFiles as parameters based on the required order
	 * 
	 * @param colJarFiles
	 * @param colExcludeApis
	 * @param debugOn
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public CustomClassLoader(Collection colJarFiles, Collection colExcludeApis, boolean debugOn) throws IOException,
			ClassNotFoundException {
		super(CustomClassLoader.class.getClassLoader());
		synchronized (this) {
			initialize(colJarFiles, colExcludeApis);
			loadClasses(debugOn);
		}
	}

	/**
	 * Loads the classes
	 * 
	 * @param debugOn
	 * @throws ClassNotFoundException
	 */
	private void loadClasses(boolean debugOn) throws ClassNotFoundException {
		Collection colLoadClassNames = new HashSet();
		Iterator itClassRepository = classRepository.iterator();
		Map mapKeyAndEntries;
		while (itClassRepository.hasNext()) {
			mapKeyAndEntries = (Map) itClassRepository.next();
			colLoadClassNames.addAll(mapKeyAndEntries.keySet());
		}

		String className;
		Class aClass;
		Iterator itColLoadClassNames = colLoadClassNames.iterator();
		while (itColLoadClassNames.hasNext()) {
			className = (String) itColLoadClassNames.next();

			if (debugOn) {
				System.out.println(className + " LOADING... ");
			}

			aClass = loadClass(className);
		}
	}

	/**
	 * Initialize the Jar Entries
	 * 
	 * @param colJarFiles
	 * @param colExcludeApis
	 * @throws IOException
	 */
	private void initialize(Collection colJarFiles, Collection colExcludeApis) throws IOException {
		for (Iterator itColJarFiles = colJarFiles.iterator(); itColJarFiles.hasNext();) {
			String fileNameAndPath = (String) itColJarFiles.next();
			Map mapKeyEntriesForPath = getContentsOfAJarFile(fileNameAndPath);

			if (mapKeyEntriesForPath != null && mapKeyEntriesForPath.size() > 0) {
				classRepository.add(mapKeyEntriesForPath);
			}
		}

		if (colExcludeApis != null && colExcludeApis.size() > 0) {
			colExcludeApiList.addAll(colExcludeApis);
		}
	}

	/**
	 * This method overrides the findClass method in the java.lang.ClassLoader Class. The method will be called from the
	 * loadClass method of the parent class loader when it is unable to find a class to load. This implementation will
	 * look for the class in the class repository.
	 * 
	 * @param className
	 *            A String specifying the class to be loaded
	 * @return A Class object which is loaded into the JVM by the CustomClassLoader
	 * @throws ClassNotFoundException
	 *             if the method is unable to load the class
	 */
	protected Class findClass(String className) throws ClassNotFoundException {
		byte[] classBytes = loadFromCustomRepository(className);

		if (classBytes != null) {
			return defineClass(className, classBytes, 0, classBytes.length);
		}

		return null;
	}

	/**
	 * Load the Class
	 */
	public Class loadClass(String name) throws ClassNotFoundException {
		// If perticular class is loaded no point of loading it again and again
		if (!maploadedClasses.containsKey(name)) {
			// Firstly Checking from the custom class loader
			Class loadedClass = findClass(name);
			if (loadedClass == null) {
				// If class not found delegate to Parent class loader
				boolean isExist = checkPatternMatchExist(colExcludeApiList, name);
				if (!isExist) {
					loadedClass = this.getClass().getClassLoader().loadClass(name);
				}
			}

			if (loadedClass != null) {
				maploadedClasses.put(name, loadedClass);
			}
			return loadedClass;
		} else {
			return (Class) maploadedClasses.get(name);
		}
	}

	/**
	 * Check the pattern exist or not for the passed exclution api(s)
	 * 
	 * @param colExcludeApiList
	 * @param name
	 * @return
	 */
	private boolean checkPatternMatchExist(List colExcludeApiList, String name) {
		String element;
		for (Iterator iter = colExcludeApiList.iterator(); iter.hasNext();) {
			element = (String) iter.next();

			if (element != null) {
				if (name.indexOf(element) != -1) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * A private method that loads binary class file data from the classRepository.
	 * 
	 * @param classFileName
	 * @return
	 * @throws ClassNotFoundException
	 */
	private byte[] loadFromCustomRepository(String classFileName) throws ClassNotFoundException {
		Iterator itMapRepositoryEntries = classRepository.iterator();
		Map mapKeyAndEntries;
		String key;

		while (itMapRepositoryEntries.hasNext()) {
			mapKeyAndEntries = (Map) itMapRepositoryEntries.next();

			for (Iterator iter = mapKeyAndEntries.keySet().iterator(); iter.hasNext();) {
				key = (String) iter.next();

				if (key.equals(classFileName)) {
					byte[] byteRecords = (byte[]) mapKeyAndEntries.get(key);
					return byteRecords;
				}
			}
		}

		return null;
	}

	/**
	 * Returns the content of a Jar file
	 * 
	 * @param fileNameAndPath
	 * @return
	 * @throws IOException
	 */
	private Map getContentsOfAJarFile(String fileNameAndPath) throws IOException {
		JarFile jarfile = new JarFile(fileNameAndPath);
		Map nameAndBytes = new HashMap();

		Object fieldName;
		JarEntry jarEntry;
		InputStream in;
		byte[] classBytes;
		String fieldKey;

		for (Enumeration enumeration = jarfile.entries(); enumeration.hasMoreElements();) {
			fieldName = (Object) enumeration.nextElement();

			// Only Taking .class files
			if (fieldName.toString().indexOf(".class") != -1) {
				jarEntry = new JarEntry(fieldName.toString());
				in = jarfile.getInputStream(jarEntry);

				ByteArrayOutputStream out = new ByteArrayOutputStream();
				byte[] tempBuffer = new byte[4096];
				for (int n; (n = in.read(tempBuffer)) != -1;) {
					out.write(tempBuffer, 0, n);
				}

				classBytes = out.toByteArray();
				fieldKey = fieldName.toString().replaceAll("/", ".");
				fieldKey = fieldKey.substring(0, fieldKey.indexOf(".class"));
				nameAndBytes.put(fieldKey, classBytes);
			}
		}

		return nameAndBytes;
	}

}
