package com.isa.thinair.commons.core.constants;

public class TasksUtil

{
	public static final String MASTER_VIEW_CURRENCY = "1000";

	public static final String MASTER_ADD_CURRENCY = "1001";

	public static final String MASTER_EDIT_CURRENCY = "1002";

	public static final String MASTER_DELETE_CURRENCY = "1003";

	public static final String MASTER_VIEW_COUNTRY = "1004";

	public static final String MASTER_ADD_COUNTRY = "1005";

	public static final String MASTER_EDIT_COUNTRY = "1006";

	public static final String MASTER_DELETE_COUNTRY = "1007";

	public static final String MASTER_VIEW_TERRITORY = "1008";

	public static final String MASTER_ADD_TERRITORY = "1009";

	public static final String MASTER_EDIT_TERRITORY = "1010";

	public static final String MASTER_DELETE_TERRITORY = "1011";

	public static final String MASTER_VIEW_STATION = "1012";

	public static final String MASTER_ADD_STATION = "1013";

	public static final String MASTER_EDIT_STATION = "1014";

	public static final String MASTER_DELETE_STATION = "1015";

	public static final String MASTER_VIEW_AIRPORT = "1016";

	public static final String MASTER_ADD_AIRPORT = "1017";

	public static final String MASTER_EDIT_AIRPORT = "1018";

	public static final String MASTER_DELETE_AIRPORT = "1019";

	public static final String MASTER_VIEW_EDIT_AIRPORT_DST = "1020";

	public static final String MASTER_ADD_AIRPORT_DST = "1021";

	public static final String MASTER_EDIT_AIRPORT_DST = "1022";

	public static final String MASTER_DISABLE_ROLLBACK_DST = "1023";

	public static final String MASTER_VIEW_AIRCRAFT_MODELS = "1024";

	public static final String MASTER_ADD_AIRCRAFT_MODELS = "1025";

	public static final String MASTER_EDIT_AIRCRAFT_MODELS = "1026";

	public static final String MASTER_DELETE_AIRCRAFT_MODELS = "1027";

	public static final String MASTER_VIEW_TAIL_NUMBERS = "1028";

	public static final String MASTER_ADD_TAIL_NUMBERS = "1029";

	public static final String MASTER_EDIT_TAIL_NUMBERS = "1030";

	public static final String MASTER_DELETE_TAIL_NUMBERS = "1031";

	public static final String MASTER_VIEW_ROUTES = "1032";

	public static final String MASTER_ADD_ROUTES = "1033";

	public static final String MASTER_EDIT_ROUTES = "1034";

	public static final String MASTER_DELETE_ROUTES = "1035";

	public static final String MASTER_VIEW_SITA_ADDRESSES = "1036";

	public static final String MASTER_ADD_SITA_ADDRESSES = "1037";

	public static final String MASTER_EDIT_SITA_ADDRESSES = "1038";

	public static final String MASTER_DELETE_SITA_ADDRESSES = "1039";

	public static final String MASTER_VIEW_ROLES = "1040";

	public static final String MASTER_ADD_ROLES = "1041";

	public static final String MASTER_EDIT_ROLES = "1042";

	public static final String MASTER_DELETE_ROLES = "1043";

	public static final String MASTER_VIEW_USERS = "1044";

	public static final String MASTER_ADD_USERS = "1045";

	public static final String MASTER_EDIT_USERS = "1046";

	public static final String MASTER_DELETE_USERS = "1047";

	public static final String MASTER_CHANGE_ROLE = "1148";

	public static final String MASTER_MAINTAIN_SYSTEM_PARAMETERS = "1048";

	public static final String MASTER_CHANGE_PASSWORD = "1049";

	public static final String TA_VIEW_TRAVEL_AGENTS = "1050";

	public static final String TA_ADD_TRAVEL_AGENTS = "1051";

	public static final String TA_EDIT_TRAVEL_AGENTS = "1052";

	public static final String TA_DELETE_TRAVEL_AGENTS = "1053";

	public static final String TA_VIEW_TRAVEL_AGENT_CREDIT_LIMIT = "1054";

	public static final String TA_UPDATE_TRAVEL_AGENT_CREDIT_LIMIT = "1055";

	public static final String TA_RECORD_TRAVEL_AGENT_PAYMENTS = "1056";

	public static final String TA_VIEW_TRAVEL_AGENT_RECIPTS = "1057";

	public static final String TA_SETTLE_TRAVEL_AGENT_INVOICES = "1058";

	public static final String TA_EMAIL_INVOICE = "1059";

	public static final String FLIGHT_ACCESS_SCHEDULES_MANAGEMENT = "1060";

	public static final String FLIGHT_ADD_FLIGHT_SCHEDULES = "1061";

	public static final String FLIGHT_EDIT_FLIGHT_SCHEDULES = "1062";

	public static final String FLIGHT_CANCEL_FLIGHT_SCHEDULES = "1063";

	public static final String FLIGHT_SPLIT_FLIGHT_SCHEDULES = "1064";

	public static final String FLIGHT_BUILD_FLIGHT_SCHEDULES = "1065";

	public static final String FLIGHT_COPY_FLIGHT_SCHEDULES = "1066";

	public static final String FLIGHT_PRINT_FLIGHT_SCHEDULES = "1067";

	public static final String FLIGHT_OVERLAP_SCHEDULE_SEGMENTS = "1068";

	public static final String FLIGHT_ADD_FLIGHTS = "1069";

	public static final String FLIGHT_EDIT_FLIGHTS = "1070";

	public static final String FLIGHT_CANCEL_FLIGHTS = "1071";

	public static final String FLIGHT_COPY_FLIGHTS = "1072";

	public static final String FLIGHT_OVERLAP_FLIGHT_SEGMENTS = "1073";

	public static final String FLIGHT_REPROTECT_FLIGHTS = "1074";

	public static final String INVENTORY_ACCESS_INVENTORY_PLANNING = "1075";

	public static final String INVENTORY_ACCESS_BOOKING_CODES_MANAGEMENT = "1076";

	public static final String INVENTORY_ADD_BOOKING_CODES = "1077";

	public static final String INVENTORY_EDIT_BOOKNG_CODES = "1078";

	public static final String INVENTORY_DELETE_BOOKING_CODES = "1079";

	public static final String INVENTORY_UPDATE_SEGMENT_ALLOCATIONS = "1080";

	public static final String INVENTORY_ADD_BOOKING_CODE_ALLLOCATIONS = "1081";

	public static final String INVENTORY_UPDATE_BOOKING_CODE_ALLLOCATIONS = "1082";

	public static final String INVENTORY_DELETE_BOOKING_CODE_ALLLOCATIONS = "1083";

	public static final String INVENTORY_ROLL_BOOKING_CODE_ALLLOCATIONS = "1084";

	public static final String INVENTORY_OPTIMISE_BOOKING_CODE_INVENTORY = "1085";

	public static final String INVENTORY_EXPORT_BOOKING_CODE_INVENTORY = "1086";

	public static final String FARES_ADD_FARE_RULES = "1087";

	public static final String FARES_EDIT_FARE_RULES = "1088";

	public static final String FARES_DELETE_FARE_RULES = "1089";

	public static final String FARES_VIEW_AGENTS_FARES = "1090";

	public static final String FARES_ACCESS_CHARGES_MAINTENANCE = "1091";

	public static final String FARES_ADD_CHARGE_CODES = "1092";

	public static final String FARES_EDIT_CHARGE_CODES = "1093";

	public static final String FARES_DELETE_CHARGE_CODES = "1094";

	public static final String FARES_ADD_CHARGE_RATES = "1095";

	public static final String FARES_EDIT_CHARGE_RATES = "1096";

	public static final String FARES_ACCESS_FARE_SETUP = "1097";

	public static final String FARES_ADD_FARE = "1098";

	public static final String FARES_EDIT_FARE = "1099";

	public static final String FARES_EDIT_FARE_RULE = "1100";

	public static final String FARES_EXTEND_FARE_DURATION = "1101";

	public static final String FARES_VIEW_APPLICABLE_CHARGES = "1102";

	public static final String SITA_TRANSFER_PNL_ADL = "1103";

	public static final String FINANCE_VIEW_PRINT_INVOICES = "1104";

	public static final String SITA_VIEW_PFS = "1105";

	public static final String FINANCE_CASH_SALES_TRANSFER = "1106";

	public static final String FINANCE_CARD_SALES_TRANSFER = "1107";

	public static final String SECURITY_LOG_IN = "1108";

	public static final String SECURITY_LOG_OUT = "1109";

	public static final String INVENTORY_CREATE_FLIGHT_INVENTORY = "1110";

	public static final String INVENTORY_UPDATE_FCCINVENTORIES_FOR_MODEL_UPDATE = "1111";

	public static final String INVENTORY_UPDATE_FCCINVENTORIES_FOR_MODEL_CHANGE = "1112";

	public static final String INVENTORY_UPDATE_MAKE_CNX_RESEVATION = "1113";

	public static final String INVENTORY_ROLL_FORWARD_FLIGHT_INVENTORY = "1114";

	public static final String INVENTORY_INVALIDATE_FLIGHT_INVENTORIES = "1115";

	public static final String INVENTORY_INVALIDATE_FLIGHT_SEGMENT_INVENTORIES = "1116";

	public static final String INVENTORY_REMOVE_INVENTORY_OVERLAP = "1117";

	public static final String INVENTORY_DELETE_FLIGHTINVENTORIES = "1118";

	public static final String INVENTORY_VALIDATE_FIGHT_SEGMENT_INVENTORIES = "1119";

	public static final String SEATMAP_ADD_TEMPLATE = "1120";

	public static final String SEATMAP_MODIFY_TEMPLATE = "1121";

	public static final String SEATMAP_DELETE_TEMPLATE = "1122";

	public static final String SEATMAP_ASSIGN_SEATCHARGE = "1123";

	public static final String SEATMAP_CHANGE_SEATCHARGE = "1124";

	public static final String INVENTORY_BLOCK_SEATS_FOR_RESERVATION = "2000";

	public static final String INVENTORY_MOVE_BLOCK_SEATS_TO_SOLD = "2001";

	public static final String MASTER_ADD_HUB = "2002";

	public static final String MASTER_EDIT_HUB = "2003";

	public static final String MASTER_DELETE_HUB = "2004";

	public static final String MASTER_NEW_PASSWORD = "1126";

	public static final String RESET_CHANGE_PASSWORD = "1125";

	public static final String MASTER_VIEW_SSR = "2005";

	public static final String MASTER_ADD_SSR = "2006";

	public static final String MASTER_EDIT_SSR = "2007";

	public static final String MASTER_DELETE_SSR = "2008";

	public static final String MEAL_ASSIGN_MEALCHARGE = "1127";

	public static final String MASTER_VIEW_MEAL = "1128";

	public static final String MASTER_ADD_MEAL = "1129";

	public static final String MASTER_EDIT_MEAL = "1130";

	public static final String MASTER_DELETE_MEAL = "1131";

	public static final String MEAL_ADD_TEMPLATE = "1132";

	public static final String MEAL_MODIFY_TEMPLATE = "1133";

	public static final String MEAL_DELETE_TEMPLATE = "1134";

	public static final String MASTER_EDIT_APP_PARAM = "1135";

	public static final String FARES_UPLOAD_ADD = "1136";

	public static final String FARES_UPLOAD_UPDATE = "1137";

	public static final String BAGGAGE_ASSIGN_BAGGAGECHARGE = "1138";

	// exchange rate automation relevant data audit task templates
	public static final String MASTER_EXCHANGE_RATE_UPDATE = "2009";

	public static final String MASTER_EXCHANGE_RATE_UPDATE_FAILED = "2010";

	public static final String BAGGAGE_ADD_TEMPLATE = "1139";

	public static final String BAGGAGE_MODIFY_TEMPLATE = "1140";

	public static final String BAGGAGE_DELETE_TEMPLATE = "1141";

	public static final String MASTER_ADD_BAGGAGE = "1143";

	public static final String MASTER_EDIT_BAGGAGE = "1144";

	public static final String MASTER_DELETE_BAGGAGE = "1142";

	public static final String MASTER_ADD_MEAL_CATEGORY = "1150";

	public static final String MASTER_EDIT_MEAL_CATEGORY = "1151";

	public static final String MASTER_DELETE_MEAL_CATEGORY = "1152";

	public static final String OND_BAGGAGE_ADD_TEMPLATE = "1153";

	public static final String OND_BAGGAGE_MODIFY_TEMPLATE = "1154";

	public static final String OND_BAGGAGE_DELETE_TEMPLATE = "1155";

	public static final String OND_CODE_ADD = "1156";

	public static final String OND_CODE_MODIFY = "1157";

	public static final String OND_CODE_DELETE = "1158";
	
	public static final String MASTER_ADD_APP_PARAM = "1162";

	public static final String MASTER_ADD_SSR_CHARGE = "2011";

	public static final String MASTER_MODIFY_SSR_CHARGE = "2012";

	public static final String MASTER_DELETE_SSR_CHARGE = "2013";

	public static final String MASTER_ADD_SSR_TEMPLATE = "2014";

	public static final String MASTER_MODIFY_SSR_TEMPLATE = "2015";

	public static final String MASTER_DELETE_SSR_TEMPLATE = "2016";

	public static final String OVERBOOK_SEAT_TEMPLATE = "2017";

	public static final String MASTER_ADD_PRIVILEGES = "2018";

	public static final String MASTER_EDIT_PRIVILEGES = "2019";

	public static final String MASTER_ADD_ONHOLD_TIME_CONFIG = "2020";

	public static final String MASTER_EDIT_ONHOLD_TIME_CONFIG = "2021";

	public static final String MASTER_DELETE_ONHOLD_TIME_CONFIG = "2022";

	public static final String PROMOTION_ADD_PROMO_CODE = "2023";

	public static final String PROMOTION_EDIT_PROMO_CODE = "2024";

	public static final String PROMOTION_DELETE_PROMO_CODE = "2025";

	// PNL ADL related tasks
	public static final String MANUALLY_RESEND_PNL_ADL = "2026";

	public static final String GROUP_BOOKING_REQUEST = "2027";

	// Anci Offer related audit tasks
	public static final String ANCI_OFFER_EDIT = "2028";

	public static final String BUNDLED_FARE_ADD = "2029";

	public static final String BUNDLED_FARE_EDIT = "2030";

	public static final String BUNDLED_FARE_DELETE = "2031";
	
	//Officers MobNums Config
	public static final String MASTER_ADD_OFFICERS_MOB_NUMS = "2044";

	public static final String MASTER_EDIT_OFFICERS_MOB_NUMS = "2045";

	public static final String MASTER_DELETE_OFFICERS_MOB_NUMS = "2046";

	public static final String MASTER_UPDATE_PAX_ET_STATUS = "2047";

	public static final String PGW_WISE_CHARGE_ADD = "2048";

	public static final String PGW_WISE_CHARGE_EDIT = "2049";
	
	//Update PAX/ET status in flown flight

	public static final String MASTER_SEND_SSM_RECAP_EXTERNAL = "2053";
	
	public static final String MASTER_SCHEDULE_SSM_RECAP_EXTERNAL = "2054";
	
	public static final String FLIGHT_PUBLISH_FLIGHT_SCHEDULE = "2055";
	
	public static final String FLIGHT_UNPUBLISH_FLIGHT_SCHEDULE = "2056";
	
	public static final String FLIGHT_PUBLISH_FLIGHT= "2057";
	
	public static final String FLIGHT_UNPUBLISH_FLIGHT = "2058";

	public static final String MASTER_SCHEDULE_SSM_RECAP_GDS = "2059";
	
	public static final String MASTER_SEND_SSM_RECAP_GDS = "2060";
	
	public static final String SITA_TRANSFER_PAL_CAL = "2067";
	
	public static final String MANUALLY_RESEND_PAL_CAL = "2068";

	// Agent Frequent Customer Profile related audits
	
	public static final String ADD_AGENT_FREQUENT_CUSTOMER = "2041";
	
	public static final String EDIT_AGENT_FREQUENT_CUSTOMER = "2042";
	
	public static final String REMOVE_AGENT_FREQUENT_CUSTOMER = "2043";

	// Inventory Template related audit tasks

	public static final String ADD_INVENTORY_TEMPLATE = "2038";

	public static final String MODIFY_INVENTORY_TEMPLATE = "2039";	
	
	// Voucher related audit tasks
	
	public static final String CANCEL_VOUCHER = "2036";
	
	public static final String DEFINE_VOUCHER_TEMPLATE = "2037";
	
	public static final String EDIT_VOUCHER_TERMS_TEMPLATE = "2040";

	public static final String MASTER_ADD_ADVERTISEMENT = "1159";

	public static final String MASTER_EDIT_ADVERTISEMENT = "1160";

	public static final String MASTER_DELETE_ADVERTISEMENT = "1161";

	public static final String EDIT_CUSTOMER = "2050";
	public static final String MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_CREATE = "2061";
	
	public static final String MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_UPDATE = "2062";

	public static final String MASTER_ROUTE_WISE_DEF_ANCI_TEMPLATE_DELETE="2063";

	public static final String PROCESS_SSM = "2064";

	public static final String PROCESS_ASM = "2065";

	public static final String PROCESS_SISM = "2066";
	
	public static final String MASTER_VIEW_STATE = "1012";

	public static final String MASTER_ADD_STATE= "2069";

	public static final String MASTER_EDIT_STATE = "2070";

	public static final String MASTER_DELETE_STATE = "2071";
	
	public static final String SCHEDULE_MSG_PROCESSING_DETAILS = "2072";	
	
	public static final String MASTER_ADD_CITY = "2073";

	public static final String MASTER_EDIT_CITY = "2074";
	
	public static final String MASTER_DELETE_CITY = "2075";
	
	public static final String INVENTORY_ADVANCE_ROLL_FORWARD_FLIGHT_INVENTORY = "2076";
}