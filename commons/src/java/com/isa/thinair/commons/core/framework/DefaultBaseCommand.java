/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Implementation of the command interface
 * 
 * @author Lasantha Pambagoda
 */
public abstract class DefaultBaseCommand implements Command {

	private HashMap<String, Object> paramMap = new HashMap<String, Object>();

	public abstract ServiceResponce execute() throws ModuleException;

	/**
	 * 
	 * @param params
	 *            key,value pairs to be set Set multiple parameters at once
	 */
	public void setParameters(HashMap<String, Object> paramsMap) {
		Iterator<String> it = paramsMap.keySet().iterator();
		while (it.hasNext()) {
			String pName = (String) it.next();
			setParameter(pName, paramsMap.get(pName));
		}
	}

	/**
	 * implementation of the set parameter
	 */
	public void setParameter(String paramName, Object param) {
		this.paramMap.put(paramName, param);
	}

	/**
	 * implementation of the get parameter
	 */
	public Object getParameter(String paramName) {
		return this.paramMap.get(paramName);
	}

	/**
	 * implementation of the get parameter
	 */
	public <T> T getParameter(String paramName, Class<T> clazz) {
		return clazz.cast(this.paramMap.get(paramName));
	}

	/**
	 * return the default parameter value in case defined paramName does not exists in the context
	 * 
	 * @param <T>
	 */
	public <T> T getParameter(String paramName, T defaultValue, Class<T> clazz) {
		if (this.paramMap.containsKey(paramName)) {
			return clazz.cast(this.paramMap.get(paramName));
		}
		return defaultValue;
	}

	/**
	 * implementation of the get parameter names
	 */
	public Collection<String> getParameterNames() {
		return paramMap.keySet();
	}

	/**
	 * implementation of the clear command parameters
	 */
	public void clearCommandParameters() {
		this.paramMap.clear();
	}
}
