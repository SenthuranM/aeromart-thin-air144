package com.isa.thinair.commons.core.interceptor;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.InvocationContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class MethodParamInterceptor {
	private static Log log = LogFactory.getLog(MethodParamInterceptor.class);
	private static Log interceptorLog = LogFactory.getLog("interceptor-log");

	@Resource
	private SessionContext sessionContext;

	public Object logMethod(InvocationContext context) throws Exception {
		long startTime = System.currentTimeMillis();
		Object object = null;
		Exception exception = null;
		try {
			object = context.proceed();
		} catch (Exception e) {
			exception = e;
		}
		long endTime = System.currentTimeMillis();
		try {
			String arguments = null;
			if (context.getParameters() != null) {
				Gson gson = new GsonBuilder().create();
				arguments = gson.toJson(context.getParameters());
			}
			UserPrincipal up = getUserPrincipal();
			String agentCode = null;
			String username = null;
			String ip = null;
			String salesChannel = null;
			if (up != null) {
				agentCode = up.getAgentCode();
				username = up.getUserId();
				ip = up.getIpAddress();
				salesChannel = up.getSalesChannel() + "";
			}
			String methodName = PlatformUtiltiies.nullHandler(context.getMethod().getName());
			long seconds = (endTime - startTime) / 1000;
			String error = null;
			if (exception != null) {
				error = exception.getClass().getName();
			}
			interceptorLog.debug("|MTD#" + methodName + "|DUR#" + seconds + "|ERR#" + error + "|AGT#" + agentCode + "|USR#"
					+ username + "|IPA#" + ip + "|SCH#" + salesChannel + "|PRM#" + arguments);
		} catch (Exception e) {

		}
		if (exception != null) {
			throw exception;
		} else {
			return object;
		}
	}

	public UserPrincipal getUserPrincipal() throws ModuleException {
		UserPrincipal principal = null;
		try {
			principal = (UserPrincipal) this.sessionContext.getCallerPrincipal();
		} catch (Exception ex) {
			log.error("Retrieving Caller Principal failed", ex);
		}
		return principal;
	}
}
