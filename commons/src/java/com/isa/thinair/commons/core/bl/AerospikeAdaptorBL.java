package com.isa.thinair.commons.core.bl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishToCachingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.model.IpToCountry;
import com.isa.thinair.commons.core.persistence.dao.CommonsDAO;
import com.isa.thinair.commons.core.util.AerospikeInternalConstants;
import com.isa.thinair.commons.core.util.CommonsDAOUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class AerospikeAdaptorBL {	

	private Log log = LogFactory.getLog(AerospikeAdaptorBL.class);

	private AerospikeClient aerospikeClient = null;
	
	private int defaultWriteTimeout = 0;
	
	private GlobalConfig globalConfig;
	
	private String namespace = null;

	public AerospikeAdaptorBL(AerospikeClient aerospikeClient, int defaultWriteTimeout, String namespace) {
		this.aerospikeClient = aerospikeClient;
		this.defaultWriteTimeout = defaultWriteTimeout;
		this.namespace = namespace;
		this.globalConfig = CommonsServices.getGlobalConfig();
	}

	/**
	 * Method for saving ip country data		
	 * 
	 * @throws ModuleException
	 */
	public void syncIpToCountryData() throws ModuleException {
		try {
			List<IpToCountry> ipToCountries = CommonsDAOUtil.getCommonsHibernateDAO().getUnsynedIPToCountryData();
			WritePolicy writePolicy = new WritePolicy();
			writePolicy.timeout = defaultWriteTimeout;
			if (ipToCountries != null && !ipToCountries.isEmpty()) {
				for (IpToCountry ipToCountry : ipToCountries) {
					Key key = new Key(namespace, AerospikeInternalConstants.UDFIpToCountry.SET_NAME,
							ipToCountry.getIpToCountryId());
					Bin startRange = new Bin(AerospikeInternalConstants.SET_IP_To_COUNTRY.BIN_START_RANGE,
							ipToCountry.getStartRange());
					Bin endRange = new Bin(AerospikeInternalConstants.SET_IP_To_COUNTRY.BIN_END_RANGE, ipToCountry.getEndRange());
					Bin countryCode = new Bin(AerospikeInternalConstants.SET_IP_To_COUNTRY.BIN_COUNTRY_CODE,
							ipToCountry.getCountryCode());
					aerospikeClient.put(writePolicy, key, startRange, endRange, countryCode);
					ipToCountry.setSyncedWithCachingDB(true);
				}

				// Update sync status
				CommonsDAOUtil.getCommonsHibernateDAO().saveOrUpdateAllIPToCountries(ipToCountries);
			}
		} catch (Exception e) {
			log.error("Error in syncing ip to country data with aerospike ", e);
			throw new ModuleException("airmaster.iptocountry.sync.fail", "airmaster.desc");
		}
	}

	public void syncAllAppParamsToCache() throws ModuleException {

		try {
			LookupService lookup = LookupServiceFactory.getInstance();
			CommonsDAO platformDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);
			Map<String, HashMap<String, String>> bizPropertyMap = platformDAO.getBizParams();
			WritePolicy writePolicy = new WritePolicy();
			writePolicy.timeout = defaultWriteTimeout;

			if (bizPropertyMap != null && !bizPropertyMap.isEmpty()) {
				for (String carrierCode : bizPropertyMap.keySet()) {
					for (Entry<String, String> entry : bizPropertyMap.get(carrierCode).entrySet()) {
						Key key = new Key(namespace, AerospikeInternalConstants.UDFAppParameter.SET_NAME,
								entry.getKey());
						Bin paramValue = new Bin(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE, entry.getValue()
								.toString());
						Bin applicableCarrierCode = new Bin(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_CARRIER_CODE,
								entry.getValue());
						aerospikeClient.put(writePolicy, key, paramValue, applicableCarrierCode);
					}
				}
			}
		} catch (Exception e) {
			log.error("Error syncing  data with aerospike ", e);
			throw new ModuleException("airmaster.ondCaching.sync.fail", "airmaster.desc");
		}
	}
	
	public void syncAppParameterToCache(String paramKey, String paramVal, String carrierCode) throws ModuleException {

		try {
			WritePolicy writePolicy = new WritePolicy();
			writePolicy.timeout = defaultWriteTimeout;

			Key key = new Key(namespace, AerospikeInternalConstants.UDFAppParameter.SET_NAME, paramKey);
			Bin paramValue = new Bin(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE, paramVal);
			Bin applicableCarrierCode = new Bin(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_CARRIER_CODE, carrierCode);
			aerospikeClient.put(writePolicy, key, paramValue, applicableCarrierCode);

		} catch (Exception e) {
			log.error("Error syncing  data with aerospike ", e);
			throw new ModuleException("airmaster.ondCaching.sync.fail", "airmaster.desc");
		}
	}

	/**
	 * Method for saving ond data
	 * 
	 * @throws ModuleException
	 */
	public void syncOndPublishToCache(List<OndPublishToCachingDTO> ondsForCaching) throws ModuleException {

		try {

			WritePolicy writePolicy = new WritePolicy();
			writePolicy.timeout = defaultWriteTimeout;

			if (ondsForCaching != null && !ondsForCaching.isEmpty()) {
				for (OndPublishToCachingDTO ondTOCache : ondsForCaching) {
					Key key = new Key(namespace, AerospikeInternalConstants.OndForCash.SET_NAME,
							ondTOCache.getOriginAndDestination());
					Bin operatingCarrier = new Bin(AerospikeInternalConstants.SET_OndCashing.OPERATING_CARRIER,
							ondTOCache.getOperatingCarriers());
					aerospikeClient.put(writePolicy, key, operatingCarrier);
				}
			}
		} catch (Exception e) {
			log.error("Error syncing  data with aerospike ", e);
			throw new ModuleException("airmaster.ondCaching.sync.fail", "airmaster.desc");
		}
	}
	/*
	public String getAppParamValueByParamKey(String paramKey) {
		String paramValue = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = externalConfig.getAppParamUDF();
			aerospikeClient = getAerospikeClient();
			if (!udfPopulated.get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (udfPopulated.get(udf)) {
				Statement st = new Statement();
				st.setNamespace(externalConfig.getNamespace());
				st.setSetName(AerospikeInternalConstants.UDFAppParameter.SET_NAME);

				Key key = new Key(externalConfig.getNamespace(), AerospikeInternalConstants.UDFAppParameter.SET_NAME, paramKey);
				Record record = aerospikeClient.get(null, key);

				if (record != null && record.bins.get(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE) != null) {
					paramValue = record.bins.get(AerospikeInternalConstants.SET_APP_PARAMETER.BIN_PARAM_VALUE).toString();
				} else {
					log.info("App Param results is empty in Aerospike");
				}
			}
		} catch (Exception e) {
			log.error("Error in retriving app params from Aerospike", e);
		} finally {
			if (aerospikeClient != null) {
				aerospikeClient.close();
			}
		}
		return paramValue;
	}
	*/
	/**
	 * Retrieving country by id
	 * 
	 * @param ip
	 * @return
	 * @throws ModuleException
	 */
	/*
	public String getCountryById(Long ip) throws ModuleException {
		String countryCode = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = externalConfig.getIpToCountryUDF();
			aerospikeClient = getAerospikeClient();
			if (!udfPopulated.get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (udfPopulated.get(udf)) {
				Statement st = new Statement();
				st.setNamespace(externalConfig.getNamespace());
				st.setSetName(AerospikeInternalConstants.UDFIpToCountry.SET_NAME);

				ResultSet resultSet = aerospikeClient.queryAggregate(null, st, externalConfig.getIpToCountryUDF(),
						AerospikeInternalConstants.UDFIpToCountry.CALLING_METHOD, Value.get(ip));

				if (resultSet == null) {
					log.info("IP to country results is empty in Aerospike");
				} else {
					while (resultSet.next()) {
						countryCode = (String) resultSet.getObject();
						log.info("Aerospike data country for ip " + ip + " is " + countryCode);
						break;
					}
				}
			}

		} catch (Exception e) {
			log.error("Error in retriving country code by ip from Aerospike", e);
		} finally {
			if (aerospikeClient != null) {
				aerospikeClient.close();
			}
		}
		return countryCode;
	}
	*/

	/**
	 * 
	 * @param ondInfoTOList
	 * @param requestingCarrier
	 * @return
	 * @throws ModuleException 
	 
	 */
	/*
	public SYSTEM getSearchSystem(List<String> ondInfoTOList, String requestingCarrier) {
		for (String originDestination : ondInfoTOList) {
			List<String> operatingCarrier = getOperatingCarriers(originDestination);
			if (operatingCarrier != null && operatingCarrier.size() > 1) {
				return SYSTEM.INT;
			} else if (operatingCarrier != null && !operatingCarrier.get(0).equals(requestingCarrier)) {
				return SYSTEM.INT;
			} else if (operatingCarrier == null) { 
				// set if not found result.
				
				AirportBL ondValidator = new AirportBL();
				boolean isValidOndRequest = ondValidator.isValidOndRequest(ondInfo.getOrigin(), ondInfo.getDestination());
				if(! isValidOndRequest){
					log.error("Error with airport codes");
					throw new ModuleException("ibe.flight.availability.ond.error");
				}
				
				return SYSTEM.INT;
			}
		}
		return SYSTEM.AA;
	}
	*/

	/**
	 * Retrieving carrier by origin and destination
	 * 
	 * @param originAndDestination
	 * @return
	 */
	/*
	@SuppressWarnings("unchecked")
	public List<String> getOperatingCarriers(String originAndDestination) {
		List<String> operatingCarriers = null;
		AerospikeClient aerospikeClient = null;
		try {
			String udf = externalConfig.getOndsCacheUDF();
			aerospikeClient = getAerospikeClient();
			if (!udfPopulated.get(udf)) {

				populateData(aerospikeClient, udf);
			}

			if (udfPopulated.get(udf)) {

				Key key = new Key(externalConfig.getOndNamespace(), AerospikeInternalConstants.OndForCash.SET_NAME,
						originAndDestination);

				Record record = aerospikeClient.get(null, key);
				// TODO until ond cash udf generate for LCC
				if (record == null) {
					return null;
				}
				operatingCarriers = (List<String>) record.bins.get(AerospikeInternalConstants.SET_OndCashing.OPERATING_CARRIER);

				if (operatingCarriers.isEmpty()) {
					log.info("No results found");
				}
			}

		} catch (Exception ex) {
			throw ex;
		} finally {
			if (aerospikeClient != null) {
				aerospikeClient.close();
			}
		}

		return operatingCarriers;
	}

	private AerospikeClient getAerospikeClient() {
		return new AerospikeClient(externalConfig.getHost(), externalConfig.getPort());
	}

	public void populateData(AerospikeClient aerospikeClient, String udf) {
		String localUDFLocatioc = getModuleConfigPath() + udf + externalConfig.getUdfExtension();
		String serverUDFLocation = udf + externalConfig.getUdfExtension();
		try {
			RegisterTask task = aerospikeClient.register(null, localUDFLocatioc, serverUDFLocation, Language.LUA);
			task.waitTillComplete();
			udfPopulated.put(udf, Boolean.TRUE);
		} catch (Exception e) {
			log.error("Error in registering " + udf + " UDF", e);
		}

	}

	private String getModuleConfigPath() {
		StringBuilder sb = new StringBuilder();
		sb.append(PlatformConstants.getConfigRootAbsPath()).append(File.separator).append("repository").append(File.separator)
				.append("modules").append(File.separator).append("commons").append(File.separator)
				.append(AerospikeInternalConstants.UDF_FOLDER).append(File.separator);
		return sb.toString();
	}
	*/
}
