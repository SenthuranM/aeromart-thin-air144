package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ErrorLogTO;
import com.isa.thinair.commons.core.persistence.dao.CommonsDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class LogUtil {
	private static Log log = LogFactory.getLog(LogUtil.class);
	private static final int LOG_SUBJECT_LENGTH = 100;
	private static final int LOG_CONTENT_LENGTH = 4000;

	public static void createErrorLog(ErrorLogTO errorLog) {
		LookupService lookup = LookupServiceFactory.getInstance();
		CommonsDAO commonsDAO = (CommonsDAO) lookup.getBean(PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);

		String subject = errorLog.getSubject();
		String content = errorLog.getContent();
		Collection<String> colContent = new ArrayList<String>();

		if (subject == null || "".equals(subject.trim())) {
			subject = "NO_SUBJECT";
		} else if (subject.length() > LOG_SUBJECT_LENGTH) {
			subject = subject.substring(0, LOG_SUBJECT_LENGTH - 3) + "...";
		}

		if (content == null || "".equals(content.trim())) {
			content = "NO_CONTENT";
			colContent.add(content);
		} else if (content.length() > LOG_CONTENT_LENGTH) {
			setContentCollection(colContent, content);
		} else {
			colContent.add(content);
		}

		errorLog.setSubject(subject);

		for (String contentStr : colContent) {
			errorLog.setContent(contentStr);

			try {
				commonsDAO.createErrorLog(errorLog);
			} catch (Exception e) {
				log.error(errorLog);
			}
		}

	}

	private static void setContentCollection(Collection<String> colContent, String content) {

		while (content != null && !"".equals(content)) {
			String contentStr = content.substring(0, LOG_CONTENT_LENGTH - 3) + "...";

			colContent.add(contentStr);
			content = content.substring(LOG_CONTENT_LENGTH - 3);

			if (content.length() <= LOG_CONTENT_LENGTH) {
				colContent.add(content);
				content = null;
			}
		}

	}
}
