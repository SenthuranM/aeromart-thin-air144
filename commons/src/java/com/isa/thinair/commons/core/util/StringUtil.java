package com.isa.thinair.commons.core.util;

import java.io.BufferedReader;
import java.sql.Clob;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class StringUtil {

	private static final String NON_ALPHA_NUMERIC_REPLACEMENT = " ";

	private static Random random = new Random(CalendarUtil.getCurrentSystemTimeInZulu().getTime());

	private static Log log = LogFactory.getLog(StringUtil.class);

	public static final String replaceInvalidSSRChars(String str) {
		String regex = "[^a-zA-Z0-9.]";
		String strNew = str.replaceAll(regex, NON_ALPHA_NUMERIC_REPLACEMENT);

		return strNew;
	}

	public static final String replaceNonAlphaNumerics(String str) {
		String regex = "[^a-zA-Z0-9]";
		String strNew = str.replaceAll(regex, NON_ALPHA_NUMERIC_REPLACEMENT);

		return strNew;
	}

	/**
	 * returns query string key/value pair map
	 * 
	 * @param query
	 * @return
	 */
	public static Map<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();

		for (String param : params) {
			String[] arrNamevalue = param.split("=");
			String name = arrNamevalue[0];
			String value = arrNamevalue[1];

			map.put(name, value);
		}

		return map;
	}

	public static String getUnicode(String in) {
		String out = "";
		if (in != null && in.length() > 0 && in.length() % 4 == 0) {
			char c4;
			StringBuffer output = new StringBuffer();
			String subStr = null;
			for (int i = 0; i < in.length(); i = i + 4) {
				subStr = in.substring(i, i + 4);
				c4 = (char) Integer.parseInt(subStr, 16);
				output.append(c4);
			}
			out = output.toString();
		}
		return out;
	}

	public static boolean isNullOrEmpty(String value) {
		return (value == null || "".equals(value.trim())) ? true : false;
	}

	/**
	 * Convert a byte[] array to readable string format. This makes the "hex" readable!
	 * 
	 * @return result String buffer in String format
	 * @param in
	 *            byte[] buffer to convert to string format
	 */
	public static String byteArrayToHexString(byte in[]) {

		byte ch = 0x00;
		int i = 0;
		if (in == null || in.length <= 0)
			return null;

		String pseudo[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };

		StringBuffer out = new StringBuffer(in.length * 2);

		while (i < in.length) {
			ch = (byte) (in[i] & 0xF0); // Strip off high nibble
			ch = (byte) (ch >>> 4); // shift the bits down
			ch = (byte) (ch & 0x0F); // must do this is high order bit is on!
			out.append(pseudo[(int) ch]); // convert the nibble to a String Character
			ch = (byte) (in[i] & 0x0F); // Strip off low nibble
			out.append(pseudo[(int) ch]); // convert the nibble to a String Character
			i++;
		}

		String rslt = new String(out);
		return rslt;
	}

	public static String getNotNullString(String str) {
		return (str == null) ? "" : str;
	}

	public static String getNotEmptyString(String str) {
		return (str == null || str.equals("")) ? "-" : str;
	}

	public static String convertToHex(String entry) {
		if (entry != null && (!entry.equals(""))) {
			char[] chtb = entry.toCharArray();
			StringBuffer temp = new StringBuffer(6 * chtb.length);
			int BIT_MASK = (1 << 16);

			for (int i = 0; i < chtb.length; i++) {
				temp.append(Integer.toHexString(chtb[i] | BIT_MASK).substring(1));
			}
			return temp.toString();
		} else {
			return "";
		}
	}

	/**
	 * Escape given string by the given replacement string
	 * 
	 * @param inStr
	 *            - this is the input string
	 * @param compileStr
	 *            - compilation string pattern
	 * @param replacementStr
	 *            - replacement string
	 * @return escaped string
	 */
	public static String escapeStr(String inStr, String compileStr, String replacementStr) {
		Pattern replace = Pattern.compile(compileStr);
		Matcher matcher2 = replace.matcher(inStr);
		return matcher2.replaceAll(replacementStr);
	}

	/**
	 * Get sub string
	 * 
	 * @param string
	 * @param size
	 * @return
	 */
	public static String getSubString(String string, int size) {
		if (string == null)
			string = "";
		else if (string.length() > size)
			string = string.substring(0, size);
		return string;
	}

	/**
	 * Convert clob to String
	 * 
	 * @param clob
	 * @return
	 */
	public static String clobToString(Clob clob) {
		StringBuilder strOut = new StringBuilder();
		if (null != clob) {
			String aux;
			try {
				BufferedReader br = new BufferedReader(clob.getCharacterStream());
				while ((aux = br.readLine()) != null) {
					strOut.append(aux);
				}
			} catch (Exception e) {
				log.error(e);
			}

		}
		return strOut.toString();
	}

	public static String extractNumberFromString(String numberString) {
		StringBuffer strNumber = new StringBuffer();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(numberString);
		while (m.find()) {
			strNumber.append(m.group());
		}
		return strNumber.toString();
	}

	/**
	 * Generates random alphanumeric string for given length. Uses A-Z, a-z & 0-9 for random string generation
	 * 
	 * @param length
	 * @return {@link String}
	 */
	public static String generateRandomString(int length) {
		char[] values = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
				'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
				'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		String out = "";
		for (int i = 0; i < length; i++) {
			int idx = random.nextInt(values.length);
			out += values[idx];
		}
		return out;
	}

	public static String getReverseOnDCode(String ondCode) {
		String[] elements = ondCode.split("/");
		String value = "";

		int i = elements.length - 1;
		while (i >= 0) {
			if (value.length() == 0) {
				value = elements[i];
			} else {
				value = value + "/" + elements[i];
			}
			i--;
		}

		return value;
	}
	
	/**
	 * check whether given string set contains give String object. method returns true if Set is null or empty, assuming
	 * null or empty means any
	 * 
	 * @param stringSet
	 * @param str
	 * @return
	 */
	public static boolean isMatchingStringFound(Set<String> stringSet, String str) {
		if (stringSet == null || (stringSet != null && stringSet.isEmpty())) {
			return true;
		}

		for (String strElement : stringSet) {
			if (strElement.contains(str)) {
				return true;
			}
		}

		return false;
	}

	public static String composeInitCapString(String... args) {
		StringBuilder concatedStr = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			String temp = args[i];
			if (temp != null && !temp.isEmpty()) {
				String formated = toInitCap(temp);
				concatedStr.append(formated).append(" ");
			}
		}

		return concatedStr.toString().trim();

	}
	
	//TODO remove the method from xbe utils
	public static String toInitCap(String str) {
		String intCap = str;
		if (str != null && !"".equals(str.trim())) {
			intCap = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
		}
		return intCap;
	}
	
	public static String trimStringMessage(String message, int length) {
		if (!isNullOrEmpty(message) && message.length() > length) {
			message = message.substring(0, length);
		}
		return message;
	}
}
