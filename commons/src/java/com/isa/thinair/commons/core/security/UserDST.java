/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.security;

import java.io.Serializable;
import java.util.Date;

/**
 * Holds the User, Day Light saving time Data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class UserDST implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1308928785127377737L;

	/** Holds the gmt offset minutes */
	private int gmtOffsetMinutes;

	/** Holds the dst start date time */
	private Date dstStartDateTime;

	/** Holds the dst end date time */
	private Date dstEndDateTime;

	/** Holds the adjust minutes */
	private int adjustMinutes;

	/**
	 * @return Returns the adjustMinutes.
	 */
	public int getAdjustMinutes() {
		return adjustMinutes;
	}

	/**
	 * @param adjustMinutes
	 *            The adjustMinutes to set.
	 */
	public void setAdjustMinutes(int adjustMinutes) {
		this.adjustMinutes = adjustMinutes;
	}

	/**
	 * @return Returns the dstEndDateTime.
	 */
	public Date getDstEndDateTime() {
		return dstEndDateTime;
	}

	/**
	 * @param dstEndDateTime
	 *            The dstEndDateTime to set.
	 */
	public void setDstEndDateTime(Date dstEndDateTime) {
		this.dstEndDateTime = dstEndDateTime;
	}

	/**
	 * @return Returns the dstStartDateTime.
	 */
	public Date getDstStartDateTime() {
		return dstStartDateTime;
	}

	/**
	 * @param dstStartDateTime
	 *            The dstStartDateTime to set.
	 */
	public void setDstStartDateTime(Date dstStartDateTime) {
		this.dstStartDateTime = dstStartDateTime;
	}

	/**
	 * @return Returns the gmtOffsetMinutes.
	 */
	public int getGmtOffsetMinutes() {
		return gmtOffsetMinutes;
	}

	/**
	 * @param gmtOffsetMinutes
	 *            The gmtOffsetMinutes to set.
	 */
	public void setGmtOffsetMinutes(int gmtOffsetMinutes) {
		this.gmtOffsetMinutes = gmtOffsetMinutes;
	}
}
