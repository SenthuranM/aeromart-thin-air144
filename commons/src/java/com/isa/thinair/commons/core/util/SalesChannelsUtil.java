package com.isa.thinair.commons.core.util;

import java.util.HashMap;
import java.util.Iterator;

import javax.security.auth.login.LoginException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.CommonsBusinessException;
import com.isa.thinair.commons.core.persistence.dao.CommonsDAO;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public abstract class SalesChannelsUtil {

	public static final String IS_BOOKING_ORIGINATED_FLAG = "Y";

	private static Log log = LogFactory.getLog(SalesChannelsUtil.class);

	/** Sales Channel Names */

	public static final String SALES_CHANNEL_PUBLIC_KEY = "Public";

	public static final String SALES_CHANNEL_TRAVELAGNET_KEY = "TravelAgent";

	public static final String SALES_CHANNEL_WEB_KEY = "Web";

	public static final String SALES_CHANNEL_HOLIDAYS_KEY = "AAHolidays";

	public static final String SALES_CHANNEL_EBI_ATM_KEY = "EBIATM";

	public static final String SALES_CHANNEL_EBI_CDM_KEY = "EBICDM";

	public static final String SALES_CHANNEL_EBI_INTERNET_KEY = "EBIInternet";

	public static final String SALES_CHANNEL_DNATA_AGENCIES_KEY = "DNATAAgencies";

	public static final String SALES_CHANNEL_RM_KEY = "AARM";

	public static final String SALES_CHANNEL_INTERLINED_KEY = "Interlined";

	public static final String SALES_CHANNEL_GDS_Key = "gds";
	
	public static final String SALES_CHANNEL_APIAGENCIES_KEY = "APIAgencies";
	
	public static final String SALES_CHANNEL_IOS_KEY = "IOS";
	
	public static final String SALES_CHANNEL_ANDROID_KEY = "ANDROID";

	/** Sales Channel Codes */

	public static final int SALES_CHANNEL_PUBLIC = 1;

	public static final int SALES_CHANNEL_AGENT = 3;

	public static final int SALES_CHANNEL_WEB = 4;

	public static final int SALES_CHANNEL_HOLIDAYS = 8;

	public static final int SALES_CHANNEL_EBI_ATM = 9;

	public static final int SALES_CHANNEL_EBI_CDM = 10;

	public static final int SALES_CHANNEL_EBI_INTERNET = 11;

	public static final int SALES_CHANNEL_DNATA_AGENCIES = 12;

	public static final int SALES_CHANNEL_RM = 13;

	public static final int SALES_CHANNEL_INTERLINED = 14;

	public static final int SALES_CHANNEL_LCC = 15;

	public static final int SALES_CHANNEL_GDS = 16;
	
	public static final int SALES_CHANNEL_DCS = 17;

	public static final int SALES_CHANNEL_CREW_MGMT = 18;
	
	public static final int SALES_CHANNEL_GOQUO = 19;
	
	public static final int SALES_CHANNEL_IOS = 20;
	
	public static final int SALES_CHANNEL_ANDROID = 21;

	private static HashMap<String,Integer> salesChannelsMap = null;

	public static CommonsDAO platformDAO = (CommonsDAO) LookupServiceFactory.getInstance().getBean(
			PlatformBeanUrls.Commons.COMMONS_DAO_BEAN);

	public static int getSalesChannelCode(String salesChannelName) {
		if (salesChannelsMap == null) {
			synchronized (SalesChannelsUtil.class) {
				salesChannelsMap = platformDAO.getSalesChannelMap();
			}
		}
		if (!salesChannelsMap.keySet().contains(salesChannelName)) {
			log.error("Sales channel name [" + salesChannelName + "] not found in the database");
			throw new CommonsBusinessException("platform.data.saleschannel.notdefined");
		}
		return ((Integer) salesChannelsMap.get(salesChannelName)).intValue();
	}

	public static String getSalesChannelName(int salesChannelId) {
		if (salesChannelsMap == null) {
			synchronized (SalesChannelsUtil.class) {
				salesChannelsMap = platformDAO.getSalesChannelMap();
			}
		}
		if (!salesChannelsMap.containsValue(new Integer(salesChannelId))) {
			log.error("Sales channel [" + salesChannelId + "] not found in the database");
			throw new CommonsBusinessException("platform.data.saleschannel.notdefined");
		}
		Iterator<String> salesChannelsMapIt = salesChannelsMap.keySet().iterator();
		while (salesChannelsMapIt.hasNext()) {
			String salesChannelName = (String) salesChannelsMapIt.next();
			if (((Integer) salesChannelsMap.get(salesChannelName)).intValue() == salesChannelId) {
				return salesChannelName;
			}
		}

		return null;
	}

	/**
	 * Validate and Get the sales channel for the web service
	 * 
	 * @param salesChannel
	 * @return
	 * @throws LoginException
	 */
	public static int validateAndGetSalesChannelForWS(String salesChannel) throws LoginException {
		int intSalesChannel = Integer.parseInt(salesChannel);

		if (intSalesChannel == SALES_CHANNEL_PUBLIC
				|| intSalesChannel == SALES_CHANNEL_AGENT
				|| intSalesChannel == SALES_CHANNEL_WEB 
				|| intSalesChannel == SALES_CHANNEL_IOS
				|| intSalesChannel == SALES_CHANNEL_ANDROID
				|| !(intSalesChannel == SALES_CHANNEL_HOLIDAYS || intSalesChannel == SALES_CHANNEL_EBI_ATM
						|| intSalesChannel == SALES_CHANNEL_EBI_CDM || intSalesChannel == SALES_CHANNEL_EBI_INTERNET
						|| intSalesChannel == SALES_CHANNEL_DNATA_AGENCIES || intSalesChannel == SALES_CHANNEL_RM
						|| intSalesChannel == SALES_CHANNEL_INTERLINED || intSalesChannel == SALES_CHANNEL_LCC 
						|| intSalesChannel == SALES_CHANNEL_DCS || intSalesChannel == SALES_CHANNEL_GDS
						|| intSalesChannel == SALES_CHANNEL_GOQUO
						|| intSalesChannel == SALES_CHANNEL_CREW_MGMT)) {
			log.error("Invalid sales channel [" + salesChannel + "]");
			throw new LoginException("Invalid Sales Channel For Web Services");
		}

		String salesChannelName = PlatformUtiltiies.nullHandler(getSalesChannelName(intSalesChannel));
		if (salesChannelName == null || salesChannelName.equals("")) {
			log.error("Invalid sales channel [" + salesChannel + "]");
			throw new LoginException("Invalid Sales Channel For Web Services");
		}

		return intSalesChannel;
	}

	public static AppIndicatorEnum getAppIndicatorFromSalesChannel(int salesChannel) {
		AppIndicatorEnum appIndicator = null;
		switch (salesChannel) {
		case SALES_CHANNEL_AGENT:
			appIndicator = AppIndicatorEnum.APP_XBE;
			break;
		case SALES_CHANNEL_PUBLIC:
			appIndicator = AppIndicatorEnum.APP_IBE;
			break;
		case SALES_CHANNEL_WEB:
			appIndicator = AppIndicatorEnum.APP_IBE;
			break;
		case SALES_CHANNEL_DNATA_AGENCIES:
			appIndicator = AppIndicatorEnum.APP_WS;
			break;
		case SALES_CHANNEL_LCC:
			appIndicator = AppIndicatorEnum.APP_LCC;
			break;
		case SALES_CHANNEL_GDS:
			appIndicator = AppIndicatorEnum.APP_GDS;
			break;
		case SALES_CHANNEL_GOQUO:
			appIndicator = AppIndicatorEnum.APP_GOQUO;
			break;
		case SALES_CHANNEL_IOS:
			appIndicator = AppIndicatorEnum.APP_IBE;
			break;
		case SALES_CHANNEL_ANDROID:
			appIndicator = AppIndicatorEnum.APP_IBE;
			break;
		default:
			break;
		}

		return appIndicator;
	}

	public static boolean isAppIndicatorWebOrMobile(AppIndicatorEnum appIndicator) {
		boolean isWebOrMobile = false;

		if (AppIndicatorEnum.APP_IBE.equals(appIndicator) || AppIndicatorEnum.APP_IOS.equals(appIndicator)
				|| AppIndicatorEnum.APP_ANDROID.equals(appIndicator)) {
			isWebOrMobile = true;
		}

		return isWebOrMobile;
	}
	
	public static boolean isAppIndicatorWebOrMobile(String appIndicatorStr) {
		boolean isWebOrMobile = false;

		if (AppIndicatorEnum.APP_IBE.toString().equalsIgnoreCase(appIndicatorStr)
				|| AppIndicatorEnum.APP_IOS.toString().equalsIgnoreCase(appIndicatorStr)
				|| AppIndicatorEnum.APP_ANDROID.toString().equalsIgnoreCase(appIndicatorStr)) {
			isWebOrMobile = true;
		}

		return isWebOrMobile;
	}
	
	public static boolean isAppEngineWebOrMobile(ApplicationEngine appEngine) {
		boolean isWebOrMobile = false;

		if (ApplicationEngine.IBE.equals(appEngine) || ApplicationEngine.IOS.equals(appEngine)
				|| ApplicationEngine.ANDROID.equals(appEngine)) {
			isWebOrMobile = true;
		}

		return isWebOrMobile;
	}

	public static boolean isAppEngineWebOrMobile(String appEngineStr) {
		boolean isWebOrMobile = false;

		if (ApplicationEngine.IBE.toString().equalsIgnoreCase(appEngineStr)
				|| ApplicationEngine.IOS.toString().equalsIgnoreCase(appEngineStr)
				|| ApplicationEngine.ANDROID.toString().equalsIgnoreCase(appEngineStr)) {
			isWebOrMobile = true;
		}

		return isWebOrMobile;
	}

	public static boolean isSalesChannelWebOrMobile(int salesChannel) {
		boolean isWebOrMobile = false;

		if (salesChannel == SALES_CHANNEL_WEB || salesChannel == SALES_CHANNEL_IOS || salesChannel == SALES_CHANNEL_ANDROID) {
			isWebOrMobile = true;
		}
		return isWebOrMobile;
	}

	public static boolean isSalesChannelNOTWebOrMobile(int salesChannel) {
		boolean isWebOrMobile = false;

		if (salesChannel != SALES_CHANNEL_WEB && salesChannel != SALES_CHANNEL_IOS && salesChannel != SALES_CHANNEL_ANDROID) {
			isWebOrMobile = true;
		}
		return isWebOrMobile;
	}

	public static boolean isBookingSystemWebOrMobile(BookingSystems system) {
		boolean isWebOrMobile = false;

		if (system == BookingSystems.AARES_IBE || system == BookingSystems.AARES_IOS || system == BookingSystems.AARES_ANDROID) {
			isWebOrMobile = true;

		}

		return isWebOrMobile;
	}
}
