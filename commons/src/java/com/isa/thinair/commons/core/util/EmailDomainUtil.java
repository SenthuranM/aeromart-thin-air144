/* ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 * utility class for validate Email domain
 * 
 * @author Jagath,nafly
 * 
 */

public class EmailDomainUtil {
	
	public static boolean isDomainWhiteListed(String hostName) throws NamingException {
		
		String lowerHostName = hostName.toLowerCase();
		Collection<String> whiteListedEmailDomains = CommonsServices.getGlobalConfig().getWhiteListedEmailDomains(false);
		if (whiteListedEmailDomains.contains(lowerHostName)) {
			return true;
		}

		boolean isValid = false;

		/**
		 * Below validation should happen first,if the domain is not valid, there is no point of going and checking for
		 * email domain
		 */
		if (AppSysParamsUtil.isValidateDomainAddressInEmail()) {
			isValid = isValidDomainAddress(lowerHostName) ? true : false;

			if (!isValid) {
				return false;
			}
		}

		if (AppSysParamsUtil.isValidateIfValidEmailDomain()) {
			isValid = isValidEmailDomain(lowerHostName) ? true : false;
		}

		if (isValid) {
			CommonsServices.getGlobalConfig().persistAndRefreshEmailDomainCache(lowerHostName);
		}
		return isValid;
	}

	private static boolean isValidEmailDomain(String hostName) throws NamingException {

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");
		DirContext ictx = new InitialDirContext(env);
		Attributes attrs = ictx.getAttributes(hostName, new String[] { "MX" });
		Attribute attr = attrs.get("MX");
		if (attr != null) {
			String strAttr = attr.toString();
			if (strAttr.matches("[0-9]{1,} .")) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param hostName
	 * @return This method will check if the domain is a valid domain, if the domain is not valid it will throw a
	 *         UnknownHostException
	 */
	private static boolean isValidDomainAddress(String hostName) {
		boolean isValidDomain = false;
		try {
			InetAddress inetAddress = InetAddress.getByName(hostName);
			isValidDomain = true;
		} catch (UnknownHostException e) {
			isValidDomain = false;
			e.printStackTrace();
		} catch (Exception e) {
			isValidDomain = false;
		}

		return isValidDomain;
	}


}
