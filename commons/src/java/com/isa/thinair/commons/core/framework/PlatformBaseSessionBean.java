/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * Platform Base Session bean
 *
 * @author Lasantha Pambagoda
 */
public class PlatformBaseSessionBean {

	protected static Log log = LogFactory.getLog(PlatformBaseSessionBean.class);

	/**
	 * Field sessionContext
	 */
	@Resource
	protected SessionContext sessionContext;

	/**
	 * Return the user principal
	 *
	 * @return
	 * @throws ModuleException
	 */
	public UserPrincipal getUserPrincipal() throws ModuleException {
		UserPrincipal principal = null;
		try {
			principal = (UserPrincipal) this.sessionContext.getCallerPrincipal();
		} catch (Exception ex) {
			log.error("Retrieving Caller Principal failed", ex);
		}
		return principal;
	}

	/**
	 * Sets tracking details.
	 *
	 * @param tracking
	 * @return
	 * @throws ModuleException
	 */
	public Tracking setUserDetails(Tracking tracking) throws ModuleException {
		UserPrincipal principal = getUserPrincipal();

		if (principal != null) {
			if (tracking.getVersion() < 0) { // save operation
				tracking.setCreatedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setCreatedDate(new Date());

				/*if (tracking.getModifiedBy() == null) {
					tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
					tracking.setModifiedDate(new Date());
				}*/
			} else { // update operation
				tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setModifiedDate(new Date());
			}
		}

		return tracking;
	}

	/**
	 * Handle exception utility to make life simple
	 *
	 * @param e
	 * @param rollback
	 * @param logger
	 * @throws ModuleException
	 */
	public ModuleException handleException(Exception e, boolean rollback, Log logger) throws ModuleException {
		logger.error("handleException ", e);

		ModuleException throwableModuleException;

		if (e instanceof ModuleException) {
			throwableModuleException = (ModuleException) e;
		} else if (e instanceof CommonsDataAccessException) {
			CommonsDataAccessException tmp = (CommonsDataAccessException) e;
			throwableModuleException = new ModuleException(tmp, tmp.getExceptionCode(), tmp.getModuleCode());
		} else {
			throwableModuleException = new ModuleException("module.unidentified.error");
		}

		if (rollback) {
			this.sessionContext.setRollbackOnly();
		}

		throw throwableModuleException;
	}


	public void sendMessage(Serializable object, String destinationJNDIName,
	                        String connectionFactoryJNDIName) throws ModuleException {

		QueueConnectionFactory connectionFactory;
		QueueConnection queueConnection = null;
		QueueSession jmsSession = null;

		try {
			connectionFactory = (QueueConnectionFactory) sessionContext.lookup(connectionFactoryJNDIName);
			queueConnection = connectionFactory.createQueueConnection();

			jmsSession = queueConnection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue) sessionContext.lookup(destinationJNDIName);
			QueueSender sender = jmsSession.createSender(queue);
			// create the message
			ObjectMessage message = jmsSession.createObjectMessage();
			message.setObject(object);

			// send the message
			sender.send(message);
			sender.close();
			if (log.isDebugEnabled())
				log.debug("Message is written to jms queue");
		} catch (JMSException e) {
			log.error("Sending message via jms failed", e);
		} catch (Exception exception) {
			log.error("Error occured in emailing itinerary", exception);
		} finally {
			if (queueConnection != null) {
				try {
					queueConnection.close();
					if (log.isDebugEnabled())
						log.debug("Closed jms queue connection");
				} catch (JMSException e) {
					log.error("Closing jms queue connection failed", e);
				}
			}

			if (jmsSession != null) {
				try {
					jmsSession.close();
					if (log.isDebugEnabled())
						log.debug("Closed jms session");
				} catch (JMSException e) {
					log.error("Closing jms session failed", e);
				}
			}
		}
	}
}
