package com.isa.thinair.commons.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxSSRTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;

public class SSRUtil {

	public static final String getSSRCode(Integer ssrId) {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		Map<Integer, SSRInfoDTO> ssrMap = globalConfig.getSSRInfoByIdMap();

		String ssrCode = null;

		if (ssrId != null) {
			ssrCode = ((SSRInfoDTO) ssrMap.get(ssrId)).getSSRCode();
		}

		return ssrCode;
	}

	public static final Integer getSSRId(String getSSRCode) {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		Map<String, SSRInfoDTO> ssrMap = globalConfig.getSSRInfoByCodeMap();

		Integer ssrId = null;

		if (getSSRCode != null && !getSSRCode.isEmpty()) {
			ssrId = ((SSRInfoDTO) ssrMap.get(getSSRCode)).getSSRId();
		}

		return ssrId;
	}

	/**
	 * pax SSR info
	 * 
	 * @param ssrInfo
	 * @param paxID
	 */
	public static final Map<Integer, Collection<PaxSSRTO>> setPaxSegmentSSRInfo(String ssrInfo) {
		// 0, segID, chargeCode, SSRCode, paxID
		String[] arrsegment = StringUtils.split(ssrInfo, "|"); // per Seg

		Map<Integer, Collection<PaxSSRTO>> paxSegmentSSR = new HashMap<Integer, Collection<PaxSSRTO>>();

		if (arrsegment.length > 0) {
			for (int i = 0; i < arrsegment.length; i++) {
				String[] paxSegSSRInfo = StringUtils.split(arrsegment[i], "#"); // pax ssr
				Integer passengerID = -1;
				for (int a = 0; a < paxSegSSRInfo.length; a++) {
					String[] splitPaxID = StringUtils.split(paxSegSSRInfo[a], "^");
					Collection<PaxSSRTO> paxSSR = (Collection<PaxSSRTO>) paxSegmentSSR.get(passengerID);
					if (paxSSR == null) {
						paxSSR = new ArrayList<PaxSSRTO>();
						PaxSSRTO paxSSRTO = new PaxSSRTO();
						paxSSRTO.setSegID(Integer.parseInt(splitPaxID[1]));
						paxSSRTO.setSsrID(Integer.parseInt(splitPaxID[3]));
						paxSSR.add(paxSSRTO); // SSR Code
					} else {
						paxSSR = new ArrayList<PaxSSRTO>();
						PaxSSRTO paxSSRTO = new PaxSSRTO();
						paxSSRTO.setSegID(Integer.parseInt(splitPaxID[1]));
						paxSSRTO.setSsrID(Integer.parseInt(splitPaxID[3]));
						paxSSR.add(paxSSRTO);
					}
					passengerID = Integer.parseInt(splitPaxID[4]);
					paxSegmentSSR.put(passengerID, paxSSR);
				} // end pax SSR
					// per pax SSR
			}
		}
		return paxSegmentSSR;
	}

	public static final Map<Integer, Collection<PaxSSRTO>> setPaxSegmentInFlightSSRInfo(String ssrInfo) {

		Map<Integer, Collection<PaxSSRTO>> paxSegmentSSR = new HashMap<Integer, Collection<PaxSSRTO>>();
		for (String perPax : StringUtils.split(ssrInfo, "|")) { // per pax
			Collection<PaxSSRTO> paxSSR = new ArrayList<PaxSSRTO>();
			Integer paxId = -1;
			for (String perSegment : StringUtils.split(perPax, "#")) { // per segment.Not consider in V1 IBE. SSR is
																		// only per PAX
				String[] ssrSegment = StringUtils.split(perSegment, "^");
				PaxSSRTO paxSSRTO = new PaxSSRTO();
				paxSSRTO.setSegID(Integer.valueOf(-1)); // IBE V1 SSR is only per pax.
				paxSSRTO.setSsrID(Integer.parseInt(ssrSegment[2]));
				if (ssrSegment.length == 4) { // SSR text is optional
					paxSSRTO.setSsrText(ssrSegment[3]);
				}
				paxSSR.add(paxSSRTO);
				paxId = Integer.parseInt(ssrSegment[0]);
				break;
			}
			paxSegmentSSR.put(paxId, paxSSR);
		}

		return paxSegmentSSR;
	}

	public static SSRInfoDTO getSSRInfo(Integer ssrId) {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		SSRInfoDTO ssrInfoDTO = globalConfig.getSSRInfoByIdMap().get(ssrId);

		return ssrInfoDTO;
	}

	public static SSRInfoDTO getSSRInfo(String ssrCode) {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		Integer ssrId = getSSRId(ssrCode);
		SSRInfoDTO ssrInfoDTO = null;
		if (ssrId != null) {
			ssrInfoDTO = globalConfig.getSSRInfoByIdMap().get(ssrId);
		}

		return ssrInfoDTO;

	}

	public static boolean isStarndardSSR(Integer ssrId) {
		SSRInfoDTO ssrInfo = getSSRInfo(ssrId);

		return (CommonsConstants.SSRCategoryCode.STANDARD.getCode().equals(ssrInfo.getSSRCategoryId()));
	}
	
}
