/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Macro Command to execute more than one commnd at once
 * 
 * @author Lasantha Pambagoda
 */
public class MacroCommand extends DefaultBaseCommand {

	private Map<String,Command> commandMap;

	/**
	 * implementation of the execute method for the Macro Command
	 */
	public ServiceResponce execute() throws ModuleException {

		ServiceResponce responce = null;
		int i = 1;

		if (commandMap != null) {

			Command currentCommnd = (Command) commandMap.get(String.valueOf(i));

			while (currentCommnd != null) {

				// set command parameters
				Collection<String> paramNames = this.getParameterNames();
				Iterator<String> it = paramNames.iterator();

				while (it.hasNext()) {

					String param = (String) it.next();
					currentCommnd.setParameter(param, this.getParameter(param));
				}

				// execute the inner commnd
				responce = currentCommnd.execute();

				if (!responce.isSuccess()) {
					break;
				}

				// set the new parameter to the macro command
				Collection<String> newParamNames = responce.getResponseParamNames();
				Iterator<String> newpit = newParamNames.iterator();

				while (newpit.hasNext()) {

					String param = (String) newpit.next();
					this.setParameter(param, responce.getResponseParam(param));
				}

				// get the next command to process
				i++;
				currentCommnd = (Command) commandMap.get(String.valueOf(i));
			}
		}

		return responce;
	}

	/**
	 * @return Returns the commandMap.
	 */
	public Map<String,Command> getCommandMap() {
		return commandMap;
	}

	/**
	 * @param commandMap
	 *            The commandMap to set.
	 */
	public void setCommandMap(Map<String,Command> commandMap) {
		this.commandMap = commandMap;
	}
}
