package com.isa.thinair.commons.core.util;

public class NumberUtil {

	public static Integer getInteger(String strInt) {
		Integer integer = null;

		if (strInt != null) {
			integer = new Integer(strInt);
		}

		return integer;
	}

}
