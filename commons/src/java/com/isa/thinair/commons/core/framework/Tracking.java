/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Tracking.java
 *
 * ===============================================================================
 *
 *@Virsion $$Id$$
 */
package com.isa.thinair.commons.core.framework;

import java.util.Date;

import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * Class to represent the Tracking in the object model
 * 
 * @author Lasantha Pambagoda
 */
public class Tracking extends Persistent {

	private static final long serialVersionUID = -5693588181257730748L;

	/**
	 * Gets the cretedBy
	 * 
	 * @return createdBy
	 * @hibernate.property column = "CREATED_BY" update = "false"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the createdDate
	 * 
	 * @return createdDate
	 * @hibernate.property column = "CREATED_DATE" update = "false"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the createdDate
	 * 
	 * @param createdDate
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Gets the modifiedBy
	 * 
	 * @return modifiedBy
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * Sets the modified By
	 * 
	 * @param modifiedBy
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * Gets the modifiedDate
	 * 
	 * @return modifiedDate
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * Sets modifiedDate
	 * 
	 * @param modifiedDate
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/** field created by */
	private String createdBy;

	/** field created date */
	private Date createdDate;

	/** field modified by */
	private String modifiedBy;

	/** field modified date */
	private Date modifiedDate;

	/** set User Details */
	public void setUserDetails(UserPrincipal userPrincipal) {
		if (userPrincipal != null) {
			if (this.getVersion() < 0) {
				this.setCreatedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
				this.setCreatedDate(new Date());

				/*if (this.getModifiedBy() == null) {
					this.setModifiedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
					this.setModifiedDate(new Date());
				}*/
			} else {
				this.setModifiedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
				this.setModifiedDate(new Date());
			}
		} else {
			if (this.getVersion() < 0) {
				this.setCreatedDate(new Date());

				if (this.getModifiedBy() == null) {
					this.setModifiedDate(new Date());
				}
			} else {
				this.setModifiedDate(new Date());
			}
		}
	}

}
