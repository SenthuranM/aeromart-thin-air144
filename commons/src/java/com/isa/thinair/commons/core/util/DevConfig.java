package com.isa.thinair.commons.core.util;

import java.util.HashMap;
import java.util.Map;

public class DevConfig {
	private Map<String, String> devAppParams = new HashMap<String, String>();

	public Map<String, String> getDevAppParams() {
		return devAppParams;
	}

	public void setDevAppParams(Map<String, String> devAppParams) {
		this.devAppParams = devAppParams;
	}

}
