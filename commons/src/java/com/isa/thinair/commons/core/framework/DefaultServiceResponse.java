/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.core.framework;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha Pambagoda
 */
public class DefaultServiceResponse implements ServiceResponce {

	private static final long serialVersionUID = 5853143962163720375L;

	private String responseCode = "";

	private boolean success = true;

	private HashMap<String, Object> responseParams = new HashMap<String, Object>();

	/**
	 * default constructor
	 */
	public DefaultServiceResponse() {

	}

	/**
	 * constructor with parameter for sucess or not
	 */
	public DefaultServiceResponse(boolean success) {
		this.success = success;
	}

	/**
	 * @return Returns the responseCode.
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            The responseCode to set.
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * method to get service responce param names
	 */
	public Collection<String> getResponseParamNames() {
		return responseParams.keySet();
	}

	/**
	 * method to get service responce param value
	 */
	public Object getResponseParam(String key) {
		return responseParams.get(key);
	}

	/**
	 * method to add responce param
	 */
	public void addResponceParam(String key, Object param) {
		responseParams.put(key, param);
	}

	/**
	 * Add responce params in bulck
	 */
	@Override
	public void addResponceParams(Map<String, Object> parms) {
		responseParams.putAll(parms);
	}

	/**
	 * method is success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            The success to set.
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
