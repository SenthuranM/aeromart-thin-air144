package com.isa.thinair.commons.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.isa.thinair.commons.core.constants.SystemParamKeys;

/**
 * This performs mainly AccelAero floating point arithmetic calculator operations. Using this is highly encouraged for
 * all floating point arithmetic operations.
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class AccelAeroCalculator {

	/** Holds the default number of decimal points */
	public static int DEFAULT_NUMBER_OF_DECIMAL_POINTS = 2;
	
	static {
		setDefaultDecimalPalaces();
	}

	/** Holds the default rounding mode */
	private static final RoundingMode ROUNDING_MODE = RoundingMode.UP;

	/** Holds the default decimal point format */
	private static final String DEFAULT_DECIMAL_POINT_FORMAT = getDecimalPointsFormat(DEFAULT_NUMBER_OF_DECIMAL_POINTS);

	/** Holds the decimal point format */
	private static final String DECIMAL_POINT_FORMAT = getDecimalPointsFormat(DEFAULT_NUMBER_OF_DECIMAL_POINTS + 2);

	/** Holds the default formatter */
	private static final ThreadLocal<NumberFormat> DEFAULT_FORMATTER = new ThreadLocal<NumberFormat>() {
		protected NumberFormat initialValue() {
			return new DecimalFormat(DEFAULT_DECIMAL_POINT_FORMAT);
		};
	};
	/** Holds the rounding number formatter */
	private static final ThreadLocal<NumberFormat> FORMATTER = new ThreadLocal<NumberFormat>() {
		protected NumberFormat initialValue() {
			return new DecimalFormat(DECIMAL_POINT_FORMAT);
		};
	};

	/**
	 * Returns the format of decimal points
	 * 
	 * @return
	 */
	private static String getDecimalPointsFormat(int numberOfDecimalPoints) {
		String strDecimalFormat = "0";
		for (int i = 1; i <= numberOfDecimalPoints; i++) {
			if (i == 1) {
				strDecimalFormat += ".0";
			} else {
				strDecimalFormat += "0";
			}
		}
		return strDecimalFormat;
	}

	private static void setDefaultDecimalPalaces() {
		String strDecimalPlaces = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DECIMALS);
		DEFAULT_NUMBER_OF_DECIMAL_POINTS = Integer.parseInt(strDecimalPlaces);
	}

	public static boolean isGreaterThan(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) == 1;
	}

	public static boolean isLessThan(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) == -1;
	}

	public static boolean isEqual(BigDecimal first, BigDecimal second) {
		return first.compareTo(second) == 0;
	}

	/**
	 * Returns the format of decimal points
	 * 
	 * @return
	 */
	private static String getDecimalPointsFormat(BigDecimal breakPoint) {
		return getDecimalPointsFormat(getNoOfDecimals(breakPoint));
	}

	/**
	 * Prints the values
	 * 
	 * @param values
	 */
	private static void showValues(BigDecimal[] values) {
		for (int i = 0; i < values.length; i++) {
			System.out.print(values[i] + " ");
		}
	}

	public static int getNoOfDecimals(BigDecimal breakPoint) {
		int numberOfDecimalPoints = DEFAULT_NUMBER_OF_DECIMAL_POINTS;
		if (breakPoint != null) {
			if (breakPoint.toString().indexOf(".") != -1) {
				numberOfDecimalPoints = breakPoint.toString().substring(breakPoint.toString().indexOf(".")).length() - 1;
			} else {
				numberOfDecimalPoints = 0;
			}
		}
		return numberOfDecimalPoints;
	}

	/**
	 * Initialize the value to default zero format
	 * 
	 * @return
	 */
	public static BigDecimal getDefaultBigDecimalZero() {
		return new BigDecimal(DEFAULT_DECIMAL_POINT_FORMAT);
	}

	/**
	 * Initialize the value to default zero format
	 * 
	 * @return
	 */
	public static BigDecimal getDefaultBigDecimalZero(BigDecimal breakPointValue) {
		return new BigDecimal(getDecimalPointsFormat(breakPointValue));
	}

	/**
	 * Parse the big decimal value
	 * 
	 * @param value
	 * @return
	 */
	public static BigDecimal parseBigDecimal(double value) {
		return new BigDecimal(FORMATTER.get().format(value)).setScale(DEFAULT_NUMBER_OF_DECIMAL_POINTS, ROUNDING_MODE);
	}

	/**
	 * Parse a double array value to big decimal array
	 * 
	 * @param value
	 * @return
	 */
	public static BigDecimal[] parseBigDecimal(double[] value) {
		BigDecimal[] results = new BigDecimal[value.length];

		for (int i = 0; i < value.length; i++) {
			results[i] = parseBigDecimal(value[i]);
		}

		return results;
	}

	/**
	 * Performs rounding operation and split to sets given by the size
	 * 
	 * @param totalAmount
	 * @param size
	 */
	public static BigDecimal[] roundAndSplit(BigDecimal bdTotalAmount, int size) { // Fix method name
		BigDecimal bdPerValue = divide(bdTotalAmount, new BigDecimal(size));
		BigDecimal bdRegeneratedValue = getDefaultBigDecimalZero();

		for (int i = 0; i < size; i++) {
			bdRegeneratedValue = add(bdRegeneratedValue, bdPerValue);
		}

		BigDecimal bdDifference = subtract(bdTotalAmount, bdRegeneratedValue);
		BigDecimal[] values = new BigDecimal[size];

		for (int i = 0; i < size; i++) {
			if (i == (size - 1)) {
				values[i] = add(bdPerValue, bdDifference);
			} else {
				values[i] = bdPerValue;
			}
		}

		return values;
	}

	/**
	 * Performs addition operation. Pass any number of BigDecimal(s)
	 * 
	 * @param bigDecimal
	 * @return
	 */
	public static BigDecimal add(BigDecimal... bigDecimal) {
		BigDecimal value = getDefaultBigDecimalZero();
		for (BigDecimal valueAdd : bigDecimal) {
			if (valueAdd != null) {
				value = value.add(valueAdd);
			}
		}
		return value;
	}

	/**
	 * Performs addition operation. Pass any number of BigDecimal(s)
	 * 
	 * @param bigDecimal
	 * @return
	 */
	public static BigDecimal addRounded(BigDecimal breakPoint, BigDecimal... bigDecimal) {
		BigDecimal value = getDefaultBigDecimalZero(breakPoint);
		for (int i = 0; i < bigDecimal.length; i++) {
			value = value.add(bigDecimal[i]);
		}
		return value;
	}

	/**
	 * Performs addition operation for primitive integers
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal add(BigDecimal first, int... second) {
		int value = 0;
		for (int i = 0; i < second.length; i++) {
			value += second[i];
		}

		return add(first, new BigDecimal(value));
	}

	/**
	 * Performs substraction operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal subtract(BigDecimal first, BigDecimal second) {
		BigDecimal value = getDefaultBigDecimalZero();
		value = value.add(first.subtract(second));
		return value;
	}

	/**
	 * Performs the multiply operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal multiplyRounded(BigDecimal first, BigDecimal second, BigDecimal breakPoint) {
		BigDecimal value = getDefaultBigDecimalZero(breakPoint);
		value = value.add(first.multiply(second));
		return value;
	}

	/**
	 * Performs the multiply operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal multiplyRounded(BigDecimal first, int second, BigDecimal breakPoint) {
		return multiplyRounded(first, new BigDecimal(second), breakPoint);
	}

	/**
	 * Performs the multiply operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal multiply(BigDecimal first, BigDecimal second) {
		BigDecimal value = getDefaultBigDecimalZero();
		value = value.add(first.multiply(second));
		return value;
	}

	/**
	 * Performs the multiply operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal multiply(BigDecimal first, int second) {
		return multiply(first, new BigDecimal(second));
	}

	/**
	 * Multiply to default decimal points
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal multiplyDefaultScale(BigDecimal first, BigDecimal second) {
		return multiplyWithDecimalRounding(first, second, DEFAULT_NUMBER_OF_DECIMAL_POINTS);
	}

	/**
	 * Calculate the percentage of a given BigDecimal
	 * 
	 * @param first
	 * @param percentage
	 * @return
	 */
	public static BigDecimal calculatePercentage(BigDecimal first, int percentage) {
		return divide(multiply(first, percentage), 100);
	}

	/**
	 * Calculate the percentage of a given BigDecimal with Rounding policy
	 * 
	 * @param first
	 * @param percentage
	 * @return
	 */
	public static BigDecimal calculatePercentage(BigDecimal first, float percentage, RoundingMode roundingMode) {
		return divide(multiply(first, new BigDecimal(percentage)), new BigDecimal("100"), DEFAULT_NUMBER_OF_DECIMAL_POINTS,
				roundingMode);
	}

	/**
	 * Calculate the percentage of a given BigDecimal with Rounding policy
	 * 
	 * @param first
	 * @param percentage
	 * @return
	 */
	public static BigDecimal calculatePercentage(BigDecimal first, int percentage, RoundingMode roundingMode) {
		return divide(multiply(first, percentage), new BigDecimal("100"), DEFAULT_NUMBER_OF_DECIMAL_POINTS, roundingMode);
	}

	/**
	 * First converting the double to bigDecimal and Calculate the percentage of a given double.
	 * 
	 * @param first
	 * @param percentage
	 * @return
	 */
	public static BigDecimal calculatePercentage(double first, int percentage) {
		return divide(multiply(parseBigDecimal(first), percentage), 100);
	}

	/**
	 * Multiply to N decimal points
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	private static BigDecimal multiplyWithDecimalRounding(BigDecimal first, BigDecimal second, int decimalsRounding) {
		return multiply(first, second).setScale(decimalsRounding, ROUNDING_MODE);
	}

	/**
	 * Performs the divide operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal divide(BigDecimal first, BigDecimal second) {
		BigDecimal value = getDefaultBigDecimalZero();
		value = value.add(first.divide(second, DEFAULT_NUMBER_OF_DECIMAL_POINTS, ROUNDING_MODE));
		return value;
	}

	/**
	 * Performs the divide operation for the given number of decimal points
	 * 
	 * @param first
	 * @param second
	 * @param noOfDecimalPoints
	 * @return
	 */
	public static BigDecimal divide(BigDecimal first, BigDecimal second, int noOfDecimalPoints) {
		BigDecimal value = getDefaultBigDecimalZero();
		value = value.add(first.divide(second, noOfDecimalPoints, ROUNDING_MODE));
		return value;
	}

	/**
	 * Performs the divide operation for the given number of decimal points
	 * 
	 * @param first
	 * @param second
	 * @param noOfDecimalPoints
	 * @return
	 */
	public static BigDecimal divide(BigDecimal first, BigDecimal second, int noOfDecimalPoints, RoundingMode roundingMode) {
		BigDecimal value = getDefaultBigDecimalZero();
		value = value.add(first.divide(second, noOfDecimalPoints, roundingMode));
		return value;
	}

	/**
	 * Performs the divide operation
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static BigDecimal divide(BigDecimal first, int second) {
		return divide(first, new BigDecimal(second));
	}

	/**
	 * Parse Big Decimal
	 * 
	 * @param colBigDecimals
	 * @return
	 */
	public static BigDecimal[] parseBigDecimal(Collection<BigDecimal> colBigDecimals) {
		BigDecimal[] newValues = new BigDecimal[colBigDecimals.size()];
		int i = 0;
		for (BigDecimal bigDecimal : colBigDecimals) {
			newValues[i] = bigDecimal;
			i++;
		}

		return newValues;
	}

	/**
	 * Returns to collection
	 * 
	 * @param bigDecimals
	 * @return
	 */
	public static Collection<BigDecimal> toCollection(BigDecimal[] bigDecimals) {
		Collection<BigDecimal> colBigDecimals = new ArrayList<BigDecimal>();

		for (int i = 0; i < bigDecimals.length; i++) {
			colBigDecimals.add(bigDecimals[i]);
		}

		return colBigDecimals;
	}

	/**
	 * Sorts a big decimal array
	 * 
	 * @param bigDecimals
	 * @return
	 */
	public static BigDecimal[] sort(BigDecimal[] bigDecimals) {
		Collection<BigDecimal> colBigDecimal = toCollection(bigDecimals);

		Collections.sort((List<BigDecimal>) colBigDecimal);

		return parseBigDecimal(colBigDecimal);
	}

	/**
	 * Returns whether the particular value exist or not
	 * 
	 * @param values
	 * @param bigDecimal
	 * @return
	 */
	public static boolean isValueExist(BigDecimal[] values, BigDecimal bigDecimal) {
		for (int i = 0; i < values.length; i++) {
			if (values[i].doubleValue() == bigDecimal.doubleValue()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Set new scale for passed BigDecimal as default scale
	 * 
	 * @param value
	 * @param scale
	 * @return
	 */
	public static BigDecimal scaleValueDefault(BigDecimal value) {
		return value.setScale(DEFAULT_NUMBER_OF_DECIMAL_POINTS, ROUNDING_MODE);
	}

	public static BigDecimal scaleDefaultRoundingDown(BigDecimal value) {
		return value.setScale(DEFAULT_NUMBER_OF_DECIMAL_POINTS, RoundingMode.DOWN);
	}

	/**
	 * Set new scale for passed BigDecimal to 2 using rounding mode of the system This is added in order to use, when
	 * default decimal places is not set for 2 in the system, where we need inputs to be taken with decimal places
	 * 
	 * @param value
	 * @return
	 */
	public static BigDecimal getTwoScaledBigDecimalFromString(String value) {
		return new BigDecimal(value).setScale(2, ROUNDING_MODE);
	}

	/**
	 * Format to the specified big decimal format
	 * 
	 * @param amount
	 * @return
	 */
	public static String formatAsDecimal(BigDecimal amount) {
		return DEFAULT_FORMATTER.get().format(amount);
	}

	public static BigDecimal[] getBigDecimalArray(int size, BigDecimal amount) {
		BigDecimal[] values = new BigDecimal[size];

		if (amount == null) {
			amount = getDefaultBigDecimalZero();
		}

		for (int i = 0; i < size; i++) {
			values[i] = amount;
		}
		return values;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BigDecimal a = new BigDecimal("211.12345678");
		BigDecimal b = new BigDecimal("3.12");

		// Formatting & Rounding
		System.out.println(formatAsDecimal(a)); // Chop off decimals up to 2 decimal places, no rounding

		// Multiply & Rounding
		System.out.println(multiplyWithDecimalRounding(a, b, 2)); // Multiply and roundup to 2 decimal places
		System.out.println(multiplyWithDecimalRounding(a, b, 10)); // Multiply and roundup to 10 decimal places

		// 4 principle math calculations
		System.out.println(add(a, b));
		System.out.println(add(b, 1, 5, 20, 30, 40));
		System.out.println(multiply(a, b)); // Multiply, no rounding up
		System.out.println(subtract(a, b));
		System.out.println(divide(a, b));

		// Round and Split
		int size = 7;
		BigDecimal[] values = roundAndSplit(b, size);
		showValues(values);
	}

	public static BigDecimal getRoundUpNearestCeilingInt(BigDecimal value, BigDecimal multiplyingExchangeRate) {
		return multiply(value, multiplyingExchangeRate).setScale(0, ROUNDING_MODE);
	}

	public static <T> T getNullSafeValue(T value, T defaultVal) {
		return value != null ? value : defaultVal;
	}
}