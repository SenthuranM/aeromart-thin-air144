/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * To hold Passenger SSR data transfer information
 * 
 * @author DumindaG
 * @since 1.0
 */
public class PaxSSRTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int segID;

	private int ssrID;

	private String ssrText;

	/**
	 * @return Returns the segID.
	 */
	public int getSegID() {
		return segID;
	}

	/**
	 * @param segID
	 *            The segID to set.
	 */
	public void setSegID(int segID) {
		this.segID = segID;
	}

	/**
	 * @return Returns the ssrID.
	 */
	public int getSsrID() {
		return ssrID;
	}

	/**
	 * @param ssrID
	 *            The ssrID to set.
	 */
	public void setSsrID(int ssrID) {
		this.ssrID = ssrID;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		return ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

}
