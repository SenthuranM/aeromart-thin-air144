package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TravellerTicketingInformation extends TravellerInformation implements Serializable {

    private TicketingAgentInformation ticketingAgentInformation;

    private List<ReservationControlInformation> reservationControlInformation;

    private List<MonetaryInformation> monetaryInformation;

    private List<FormOfPayment> formOfPayment;

    private PricingTicketingDetails pricingTicketingDetails;

    private OriginDestinationInformation originDestinationInfo;

    private FlightInformation flightInformation;

    private FrequentTravellerInformation frequentTravellerInformation;

    private AdditionalTourInformation additionalTourInformation;

    private NumberOfUnits numberOfUnits;

    private List<TaxDetails> taxDetails;

    private List<InteractiveFreeText> text;

    private List<ConsumerRefInformation> consumerRef;

    private List<TicketNumberDetails> tickets;

    private OriginatorInformation originatorInformation;

    private ErrorInformation errorInformation;

    public TicketingAgentInformation getTicketingAgentInformation() {
        return ticketingAgentInformation;
    }

    public void setTicketingAgentInformation(TicketingAgentInformation ticketingAgentInformation) {
        this.ticketingAgentInformation = ticketingAgentInformation;
    }

    public List<ReservationControlInformation> getReservationControlInformation() {
        return reservationControlInformation;
    }

    public void setReservationControlInformation(List<ReservationControlInformation> reservationControlInformation) {
        this.reservationControlInformation = reservationControlInformation;
    }

    public List<MonetaryInformation> getMonetaryInformation() {
        return monetaryInformation;
    }

    public void setMonetaryInformation(List<MonetaryInformation> monetaryInformation) {
        this.monetaryInformation = monetaryInformation;
    }

    public List<FormOfPayment> getFormOfPayment() {
        return formOfPayment;
    }

    public void setFormOfPayment(List<FormOfPayment> formOfPayment) {
        this.formOfPayment = formOfPayment;
    }

    public PricingTicketingDetails getPricingTicketingDetails() {
        return pricingTicketingDetails;
    }

    public void setPricingTicketingDetails(PricingTicketingDetails pricingTicketingDetails) {
        this.pricingTicketingDetails = pricingTicketingDetails;
    }

    public OriginDestinationInformation getOriginDestinationInfo() {
        return originDestinationInfo;
    }

    public void setOriginDestinationInfo(OriginDestinationInformation originDestinationInfo) {
        this.originDestinationInfo = originDestinationInfo;
    }

    public FlightInformation getFlightInformation() {
        return flightInformation;
    }

    public void setFlightInformation(FlightInformation flightInformation) {
        this.flightInformation = flightInformation;
    }

    public FrequentTravellerInformation getFrequentTravellerInformation() {
        return frequentTravellerInformation;
    }

    public void setFrequentTravellerInformation(FrequentTravellerInformation frequentTravellerInformation) {
        this.frequentTravellerInformation = frequentTravellerInformation;
    }

    public AdditionalTourInformation getAdditionalTourInformation() {
        return additionalTourInformation;
    }

    public void setAdditionalTourInformation(AdditionalTourInformation additionalTourInformation) {
        this.additionalTourInformation = additionalTourInformation;
    }

    public NumberOfUnits getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(NumberOfUnits numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public List<TaxDetails> getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(List<TaxDetails> taxDetails) {
        this.taxDetails = taxDetails;
    }

    public List<InteractiveFreeText> getText() {
        return text;
    }

    public void setText(List<InteractiveFreeText> text) {
        this.text = text;
    }

    public List<ConsumerRefInformation> getConsumerRef() {
        return consumerRef;
    }

    public void setConsumerRef(List<ConsumerRefInformation> consumerRef) {
        this.consumerRef = consumerRef;
    }

    public List<TicketNumberDetails> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketNumberDetails> tickets) {
        this.tickets = tickets;
    }

    public OriginatorInformation getOriginatorInformation() {
        return originatorInformation;
    }

    public void setOriginatorInformation(OriginatorInformation originatorInformation) {
        this.originatorInformation = originatorInformation;
    }

    public ErrorInformation getErrorInformation() {
        return errorInformation;
    }

    public void setErrorInformation(ErrorInformation errorInformation) {
        this.errorInformation = errorInformation;
    }
}
