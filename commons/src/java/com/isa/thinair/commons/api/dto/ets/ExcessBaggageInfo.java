package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExcessBaggageInfo implements Serializable {

    private String quantity;

    private String chargeQualifier;

    private String measurementUnit;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getChargeQualifier() {
        return chargeQualifier;
    }

    public void setChargeQualifier(String chargeQualifier) {
        this.chargeQualifier = chargeQualifier;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }
}
