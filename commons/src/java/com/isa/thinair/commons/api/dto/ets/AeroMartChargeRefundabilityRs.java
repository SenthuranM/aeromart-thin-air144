package com.isa.thinair.commons.api.dto.ets;

import java.util.Map;

public class AeroMartChargeRefundabilityRs extends BaseRs {
	
	private Map<String,Boolean> chargecodeRefundabiltyMap;

	public Map<String, Boolean> getChargecodeRefundabiltyMap() {
		return chargecodeRefundabiltyMap;
	}
	

	public void setChargecodeRefundabiltyMap(Map<String, Boolean> chargecodeRefundabiltyMap) {
		this.chargecodeRefundabiltyMap = chargecodeRefundabiltyMap;
	}
	
}
