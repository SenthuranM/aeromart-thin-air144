package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class AeroMartChargeRefundabilityRq implements Serializable {

	private List<String> chargeCodeList;

	public List<String> getChargeCodeList() {
		return chargeCodeList;
	}

	public void setChargeCodeList(List<String> chargeCodeList) {
		this.chargeCodeList = chargeCodeList;
	}

}
