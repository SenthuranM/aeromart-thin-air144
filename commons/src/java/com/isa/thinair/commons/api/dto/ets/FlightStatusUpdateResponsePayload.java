package com.isa.thinair.commons.api.dto.ets;

import java.util.Collection;

/**
 * 
 * @author rajitha
 *
 */
public class FlightStatusUpdateResponsePayload {

	private String flightNumber;
	private boolean success;
	private Integer errorCode;
	private Collection<TicketUpdateStatusDetails> records;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean isSuccess) {
		this.success = isSuccess;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public Collection<TicketUpdateStatusDetails> getRecords() {
		return records;
	}
	

	public void setRecords(Collection<TicketUpdateStatusDetails> records) {
		this.records = records;
	}
	

}
