package com.isa.thinair.commons.api.dto;

/**
 * includes XBE related pax configurations
 * 
 * @author rumesh
 * 
 */
public class XBEPaxConfigDTO extends BasePaxConfigDTO {
	private static final long serialVersionUID = 8061021334918340870L;

	private boolean xbeVisibility;
	private boolean xbeMandatory;

	public boolean isXbeVisibility() {
		return xbeVisibility;
	}

	public void setXbeVisibility(boolean xbeVisibility) {
		this.xbeVisibility = xbeVisibility;
	}

	public boolean isXbeMandatory() {
		return xbeMandatory;
	}

	public void setXbeMandatory(boolean xbeMandatory) {
		this.xbeMandatory = xbeMandatory;
	}
}
