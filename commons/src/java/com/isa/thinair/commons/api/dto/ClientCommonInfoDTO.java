package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class ClientCommonInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7204279818180454753L;

	private String ipAddress;
	private String browser;
	private String fwdIpAddress;
	private String url;
	private String carrierCode;

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the browser
	 */
	public String getBrowser() {
		return browser;
	}

	/**
	 * @param browser
	 *            the browser to set
	 */
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	/**
	 * @return the fwdIpAddress
	 */
	public String getFwdIpAddress() {
		return fwdIpAddress;
	}

	/**
	 * @param fwdIpAddress
	 *            the fwdIpAddress to set
	 */
	public void setFwdIpAddress(String fwdIpAddress) {
		this.fwdIpAddress = fwdIpAddress;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
}
