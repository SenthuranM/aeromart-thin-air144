package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TravellerInformation implements Serializable {

    private String travellerSurname;

    private String travellerQualifier;

    private String travellerCount;

    private List<Traveller> travellers;

    public String getTravellerSurname() {
        return travellerSurname;
    }

    public void setTravellerSurname(String travellerSurname) {
        this.travellerSurname = travellerSurname;
    }

    public String getTravellerQualifier() {
        return travellerQualifier;
    }

    public void setTravellerQualifier(String travellerQualifier) {
        this.travellerQualifier = travellerQualifier;
    }

    public String getTravellerCount() {
        return travellerCount;
    }

    public void setTravellerCount(String travellerCount) {
        this.travellerCount = travellerCount;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public void setTravellers(List<Traveller> travellers) {
        this.travellers = travellers;
    }
}
