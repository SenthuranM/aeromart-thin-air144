package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class CabinClassWiseDefaultLogicalCCDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String ccCode;
	
	private String defaultLogicalCCCode;
	
	private String logicalCCDes;
	
	private int logicalCCRank;

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getDefaultLogicalCCCode() {
		return defaultLogicalCCCode;
	}

	public void setDefaultLogicalCCCode(String defaultLogicalCCCode) {
		this.defaultLogicalCCCode = defaultLogicalCCCode;
	}

	public String getLogicalCCDes() {
		return logicalCCDes;
	}

	public void setLogicalCCDes(String logicalCCDes) {
		this.logicalCCDes = logicalCCDes;
	}

	public int getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(int logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}
	
}
