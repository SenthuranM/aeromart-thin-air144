package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */

public class BaseContactConfigDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final int GROUP_NORMAL_CONTACT = 1;
	public static final int GROUP_EMERGENCY_CONTACT = 2;

	public static final String FIELD_TITLE = "title";
	public static final String FIELD_FIRST_NAME = "firstName";
	public static final String FIELD_LAST_NAME = "lastName";
	public static final String FIELD_NATIONALITY = "nationality";
	public static final String FIELD_ADDRESS = "address";
	public static final String FIELD_CITY = "city";
	public static final String FIELD_ZIP_CODE = "zipCode";
	public static final String FIELD_COUNTRY = "country";
	public static final String FIELD_MOBILE = "mobileNo";
	public static final String FIELD_PHONE = "phoneNo";
	public static final String FIELD_FAX = "fax";
	public static final String FIELD_EMAIL = "email";
	public static final String FIELD_PASSWORD = "password";
	public static final String FIELD_SEC_QUES = "securityQuestion";
	public static final String FIELD_ALT_EMAIL = "alternateEmail";
	public static final String FIELD_GENDER = "gender";
	public static final String FIELD_DOB = "dob";
	public static final String FIELD_HOME_OFFICE_NO = "homeOfficeNo";
	public static final String FIELD_PROMOTION_PREF = "promotionPref";
	public static final String FIELD_EMAIL_PREF = "emailPref";
	public static final String FIELD_SMS_PREF = "smsPref";
	public static final String FIELD_LANGUAGE_PREF = "languagePref";
	public static final String FIELD_PREFERRED_LANG = "preferredLang";
	public static final String LABEL_CONTACT_INFO = "contact_detail_label";
	public static final String AREA_CODE = "areaCode";
	public static final String FIELD_TAX_REG_NO = "taxRegNo";

	private String fieldName;
	private int groupId;
	private boolean mandatory;
	private String validationGroup;
	private String maxLength;
	
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName
	 *            the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId
	 *            the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * @param mandatory
	 *            the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the validationGroupId
	 */
	public String getValidationGroup() {
		return validationGroup;
	}

	/**
	 * @param validationGroupId
	 *            the validationGroupId to set
	 */
	public void setValidationGroup(String validationGroupId) {
		this.validationGroup = validationGroupId;
	}

	public String getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}
}
