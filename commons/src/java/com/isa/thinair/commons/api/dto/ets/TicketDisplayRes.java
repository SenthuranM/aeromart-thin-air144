package com.isa.thinair.commons.api.dto.ets;

public class TicketDisplayRes {

	private String rawResponse;

	private boolean isSuccess;

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean success) {
		isSuccess = success;
	}
}
