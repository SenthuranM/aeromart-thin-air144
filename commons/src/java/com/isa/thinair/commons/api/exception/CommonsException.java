/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 5, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.exception;

import java.util.List;

/**
 * @author sudheera CommonsException is the supper class for all checked exceptions
 */
public class CommonsException extends Exception implements ICommonsException {

	private static final long serialVersionUID = 2178402378361609954L;
	private String exceptionCode;
	private String moduleCode;
	private Object exceptionDetails;
	private List<String> messageParameters;

	/**
	 * This constructor is prefered for communicating module exceptions to the outside,i.e. clients
	 * 
	 * @param cause
	 *            cause exception
	 * @param exceptionCode
	 *            Exception code that describes the exception, as per the resource bundle
	 * @param moduleCode
	 *            module code which identifies the module, as per resource bundle
	 */
	public CommonsException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause);
		setExceptionCode(exceptionCode);
		setModuleCode(moduleCode);
	}

	/**
	 * This constructor is prefered for communicating exceptions occuring within modules
	 * 
	 * @param cause
	 *            cause exception
	 * @param exceptionCode
	 *            exception code that describes the exception, as per the resource bundle
	 */
	public CommonsException(Throwable cause, String exceptionCode) {
		super(cause);
		setExceptionCode(exceptionCode);
	}

	public CommonsException(String exceptionCode, String moduleCode) {
		setExceptionCode(exceptionCode);
		setModuleCode(moduleCode);
	}

	public CommonsException(String exceptionCode, Object exceptionDetails) {
		setExceptionCode(exceptionCode);
		setExceptionDetails(exceptionDetails);
	}

	public CommonsException(String exceptionCode) {
		setExceptionCode(exceptionCode);
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**
	 * Gets the exception deatils as additional details
	 */
	public Object getExceptionDetails() {
		return exceptionDetails;
	}

	/**
	 * Sets the exception details
	 */
	public void setExceptionDetails(Object exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}

	/**
	 * Get the message corresponding to the message code from resource bundle
	 * 
	 * @return String exception message
	 */
	public String getMessageString() {
		return MessagesUtil.getMessage(getExceptionCode());
	}

	/**
	 * Get the module description corresponding to the module code from the resource bundle
	 * 
	 * @return String module description
	 */
	public String getModuleDesc() {
		return MessagesUtil.getModuleDescription(getModuleCode());
	}

	@Override
	public void setMessageParameters(List<String> messageParameters) {
		this.messageParameters = messageParameters;
	}

	@Override
	public List<String> getMessageParameters() {
		return this.messageParameters;
	}

}