package com.isa.thinair.commons.api.util;

public class I18nTranslationUtil {

	public enum I18nMessageCategory {
		AIRPORT_MSG, MEAL_DES, AIRPORT_DES, CURRECNY_DES, BAGGAGE_DES, SSR_DES, MEALCAT_DES, MCC_DES, ANCI_OFFER_NAME, ANCI_OFFER_DESC, BUNDLED_FARE_DESC, BUNDLED_FARE_NAME, BUNDLE_CATEGORY_NAME, BUNDLE_CATEGORY_DESC;
	}

	public static String getI18nAirportMessageKey(Object key) {
		return I18nMessageCategory.AIRPORT_MSG + String.valueOf(key);
	}

	public static String getI18nBaggageDescriptionKey(Object key) {
		return I18nMessageCategory.BAGGAGE_DES + String.valueOf(key);
	}

	public static String getI18nSSRDescriptionKey(Object key) {
		return I18nMessageCategory.SSR_DES + String.valueOf(key);
	}

	public static String getI18nMealCatDescriptionKey(Object key) {
		return I18nMessageCategory.MEALCAT_DES + String.valueOf(key);
	}

	/**
	 * Attaches MCC_DES to any object's String representation and returns the result
	 * 
	 * @param key
	 *            The Object which's toString() will be called
	 * @return MCC_DES concatenated with the passed objects String representation
	 */
	public static String getI18nLogicalCabinClassKey(Object key) {
		return I18nMessageCategory.MCC_DES + String.valueOf(key);
	}

	/**
	 * Attaches ANCI_OFFER_NAME to any object's String representation and returns the result
	 * 
	 * @param key
	 *            The Object which's toString() will be called
	 * @return ANCI_OFFER_NAME concatenated with the passed objects String representation
	 */
	public static String getI18nAnciOfferNameKey(Object key) {
		return I18nMessageCategory.ANCI_OFFER_NAME + String.valueOf(key);
	}

	/**
	 * Attaches ANCI_OFFER_DESC to any object's String representation and returns the result
	 * 
	 * @param key
	 *            The Object which's toString() will be called
	 * @return ANCI_OFFER_DESC concatenated with the passed objects String representation
	 */
	public static String getI18nAnciOfferDescKey(Object key) {
		return I18nMessageCategory.ANCI_OFFER_DESC + String.valueOf(key);
	}

	public static String getI18nBundledFareDescKey(Object key) {
		return I18nMessageCategory.BUNDLED_FARE_DESC + String.valueOf(key);
	}

	public static String getI18nBundledFareKey(Object key, I18nMessageCategory msgCat) {
		return msgCat + String.valueOf(key);
	}
}
