package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

/**
 * 
 * @author Panchatcharam.s
 *
 */
public class AutomaticCheckInRes implements Serializable {

	private static final long serialVersionUID = -4381525124694280195L;

	private String rawResponse;

	private boolean isSuccess;

	private String errorMsg;

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean success) {
		isSuccess = success;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
