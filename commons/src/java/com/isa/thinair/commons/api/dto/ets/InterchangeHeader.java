package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class InterchangeHeader implements Serializable {

	private String syntaxId;
	
	private String versionNo;
	
	private String interchangeControlRef;
	
	private String recipientRef;
	
	private String associationCode;
	
	private InterchangeInfo senderInfo;
	
	private InterchangeInfo receiverInfo;
	
	private XMLGregorianCalendar timestamp;

	public String getSyntaxId() {
		return syntaxId;
	}
	

	public void setSyntaxId(String syntaxId) {
		this.syntaxId = syntaxId;
	}
	

	public String getVersionNo() {
		return versionNo;
	}
	

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	

	public String getInterchangeControlRef() {
		return interchangeControlRef;
	}
	

	public void setInterchangeControlRef(String interchangeControlRef) {
		this.interchangeControlRef = interchangeControlRef;
	}
	

	public String getRecipientRef() {
		return recipientRef;
	}
	

	public void setRecipientRef(String recipientRef) {
		this.recipientRef = recipientRef;
	}
	

	public String getAssociationCode() {
		return associationCode;
	}
	

	public void setAssociationCode(String associationCode) {
		this.associationCode = associationCode;
	}
	

	public InterchangeInfo getSenderInfo() {
		return senderInfo;
	}
	

	public void setSenderInfo(InterchangeInfo senderInfo) {
		this.senderInfo = senderInfo;
	}
	

	public InterchangeInfo getReceiverInfo() {
		return receiverInfo;
	}
	

	public void setReceiverInfo(InterchangeInfo receiverInfo) {
		this.receiverInfo = receiverInfo;
	}
	

	public XMLGregorianCalendar getTimestamp() {
		return timestamp;
	}
	

	public void setTimestamp(XMLGregorianCalendar timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
