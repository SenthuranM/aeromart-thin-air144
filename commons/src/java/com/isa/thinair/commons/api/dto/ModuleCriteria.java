package com.isa.thinair.commons.api.dto;

import java.util.List;

public class ModuleCriteria extends ModuleCriterion {
	
	private static final long serialVersionUID = -5334613894962421351L;
	List<ModuleCriterion> modulecriterionList;
	ModuleCriterion leftCriterion;
	ModuleCriterion rightCriterion;
	String joinCondition;

	public ModuleCriteria() {
		super(false);
	}

	public String getJoinCondition() {
		return joinCondition;
	}

	public void setJoinCondition(String joinCondition) {
		this.joinCondition = joinCondition;
	}

	public ModuleCriterion getLeftCriterion() {
		return leftCriterion;
	}

	public void setLeftCriterion(ModuleCriterion leftCriterion) {
		this.leftCriterion = leftCriterion;
	}

	public ModuleCriterion getRightCriterion() {
		return rightCriterion;
	}

	public void setRightCriterion(ModuleCriterion rightCriterion) {
		this.rightCriterion = rightCriterion;
	}

	public static final String JOIN_CONDITION_OR = "OR";
	public static final String JOIN_CONDITION_AND = "AND";

	public List<ModuleCriterion> getModulecriterionList() {
		return modulecriterionList;
	}

	public void setModulecriterionList(List<ModuleCriterion> modulecriterionList) {
		this.modulecriterionList = modulecriterionList;
	}
}
