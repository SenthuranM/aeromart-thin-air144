package com.isa.thinair.commons.api.dto;

/**
 * Data transfer objects to hold VoucherTermsTemplate data.
 * 
 * @author chanaka
 * 
 */
public class VoucherTermsTemplateDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1128791234678948798L;

	private Integer templateID;

	private String language;

	private String templateName;

	private String templateContent;

	private Long version;
	
	private char voucherType;

	/**
	 * @return the voucherType
	 */
	public char getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(char voucherType) {
		this.voucherType = voucherType;
	}

	/**
	 * @return the templateID
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	/**
	 * @param templateID
	 *            the templateID to set
	 */
	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the templateContent
	 */
	public String getTemplateContent() {
		return templateContent;
	}

	/**
	 * @param templateContent
	 *            the templateContent to set
	 */
	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

}

