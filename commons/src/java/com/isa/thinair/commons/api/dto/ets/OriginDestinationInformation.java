package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class OriginDestinationInformation implements Serializable {

    private String departureAirportCityCode;
    
    private String arrivalAirportCityCode;
    
    private List<FlightInformation> flightInformation;
    
    private ErrorInformation errorInformation;

    public String getDepartureAirportCityCode() {
        return departureAirportCityCode;
    }

    public void setDepartureAirportCityCode(String departureAirportCityCode) {
        this.departureAirportCityCode = departureAirportCityCode;
    }

    public String getArrivalAirportCityCode() {
        return arrivalAirportCityCode;
    }

    public void setArrivalAirportCityCode(String arrivalAirportCityCode) {
        this.arrivalAirportCityCode = arrivalAirportCityCode;
    }

    public List<FlightInformation> getFlightInformation() {
        return flightInformation;
    }

    public void setFlightInformation(List<FlightInformation> flightInformation) {
        this.flightInformation = flightInformation;
    }

    public ErrorInformation getErrorInformation() {
        return errorInformation;
    }

    public void setErrorInformation(ErrorInformation errorInformation) {
        this.errorInformation = errorInformation;
    }
}
