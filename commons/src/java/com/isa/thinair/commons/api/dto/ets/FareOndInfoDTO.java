package com.isa.thinair.commons.api.dto.ets;

import java.util.List;

public class FareOndInfoDTO {
	
	private List<Integer> pnrSegIds;
	
	private String fareAmount;

	public List<Integer> getPnrSegIds() {
		return pnrSegIds;
	}

	public String getFareAmount() {
		return fareAmount;
	}

	public void setPnrSegIds(List<Integer> pnrSegIds) {
		this.pnrSegIds = pnrSegIds;
	}

	public void setFareAmount(String fareAmount) {
		this.fareAmount = fareAmount;
	}	
}
