package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class RefundTKTREQDTO implements Serializable {

	private MessageFunction messageFunction;

	private OriginatorInformation originatorInformation;

	private NumberOfUnits numberOfUnits;

	private List<TicketNumberDetails> tickets;

	private MessageHeader messageHeader;

	private RequestIdentity requestIdentity;

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}

	public NumberOfUnits getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(NumberOfUnits numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public List<TicketNumberDetails> getTickets() {
		return tickets;
	}

	public void setTickets(List<TicketNumberDetails> tickets) {
		this.tickets = tickets;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}

	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}

}
