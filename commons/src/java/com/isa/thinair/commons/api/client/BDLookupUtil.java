package com.isa.thinair.commons.api.client;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

import javax.naming.NamingException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;

public class BDLookupUtil {

	private final static Log log = LogFactory.getLog(BDLookupUtil.class);

	private static Object lock = new Object();

	private static BDLookupUtil instance = null;

	private Map _modules;

	// This will cache the Jndi objects.
	private Hashtable<String, JndiObjectFactoryBean> serviceLookups = new Hashtable<String, JndiObjectFactoryBean>();

	private BDLookupUtil() {
	}

	public static BDLookupUtil getInstance() {
		if (instance == null) {
			synchronized (lock) {
				if (instance == null) {
					instance = new BDLookupUtil();
					instance.initialize();
				}
			}
		}
		return instance;
	}

	public Object getBDInstance(String moduleName, String businessDelegateKey) {
		String serviceName = null;
		BDLookupConfig m = (BDLookupConfig) _modules.get(moduleName);
		try {
			serviceName = m.getFQClassName(businessDelegateKey);
			if (log.isDebugEnabled())
				log.debug("Instantiating BD [moduleName=" + moduleName + ",bdKey=" + businessDelegateKey + ",FQBDClass="
						+ serviceName + "]");
			return lookupService(m, serviceName);
		} catch (Exception e) {
			System.out.println("Instantiating BD failed [moduleName=" + moduleName + ",bdKey=" + businessDelegateKey
					+ ",FQBDClass=" + serviceName + "]");
			log.error("Instantiating BD failed [moduleName=" + moduleName + ",bdKey=" + businessDelegateKey + ",FQBDClass="
					+ serviceName + "]", e);
			throw new ModuleRuntimeException(e, "commons.client.bd.instantiation.failed");
		}
	}

	private Object lookupService(BDLookupConfig m, String serviceName) throws NamingException {
		JndiObjectFactoryBean jndiObjectFactoryBean = serviceLookups.get(serviceName);
		if (jndiObjectFactoryBean == null) {
			jndiObjectFactoryBean = new JndiObjectFactoryBean();
			jndiObjectFactoryBean.setJndiEnvironment(m.getJndiProperties());
			jndiObjectFactoryBean.setJndiName(serviceName);
			jndiObjectFactoryBean.afterPropertiesSet();
			serviceLookups.put(serviceName, jndiObjectFactoryBean);
		}

		return jndiObjectFactoryBean.getObject();
	}

	public Properties getJNDILookupProperties(String moduleName) {
		BDLookupConfig moduleLookupConfig = (BDLookupConfig) _modules.get(moduleName);

		if (moduleLookupConfig == null) {
			log.error("Requested BD lookup config does not exist [moduleName=" + moduleName + "]");
			throw new ModuleRuntimeException("commons.client.bd.config.notfound");
		}

		return moduleLookupConfig.getJndiProperties();
	}

	public BDLookupConfig getModuleLookupConfig(String moduleName) {
		BDLookupConfig moduleLookupConfig = (BDLookupConfig) _modules.get(moduleName);

		if (moduleLookupConfig == null) {
			log.error("Requested BD lookup config does not exists [moduleName=" + moduleName + "]");
			throw new ModuleRuntimeException("commons.client.bd.config.notfound");
		}

		return moduleLookupConfig;
	}

	public Collection getAllModulesLookupConfigs() {
		return _modules.values();
	}

	private void initialize() {
		try {
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			Handler h = new Handler();

			String configRootDir = ClientConstants.getConfigRootDirPath();
			if (!new File(configRootDir).isDirectory()) {
				log.error("Configuration repository does not exist [configRootDir=" + configRootDir + "]");
				throw new ModuleRuntimeException("commons.client.invalid.config.path");
			}
			String configFliePath = configRootDir + File.separator + "client" + File.separator + "config" + File.separator
					+ "module-lookup-configs.xml";

			if (!new File(configFliePath).exists()) {
				log.error("Module lookup configuration file does not exist [lookupConfigFile=" + configFliePath + "]");
				throw new ModuleRuntimeException("commons.client.invalid.config.path");
			}

			InputStream is = new BufferedInputStream(new FileInputStream(configFliePath));
			parser.parse(is, h);
			_modules = h.getModules();
		} catch (Exception ex) {
			log.error("Loading module lookup configs failed", ex);
			throw new ModuleRuntimeException(ex, "commons.client.invalid.config");
		}
	}

	public static class Handler extends DefaultHandler {
		private Stack _stack = new Stack();

		private boolean _ready = false;

		private Map _modules = new HashMap();

		private List _readForElements = Arrays.asList(new String[] { "jndi-property", "service", "locality", "name" });

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			log.debug("qName: " + qName);
			if (qName.equals("module")) {
				BDLookupConfig module = new BDLookupConfig();
				getStack().push(module);
				module.setModuleName(attributes.getValue(0));
				_ready = false;
			} else if (qName.equals("jndi-property")) {
				((BDLookupConfig) getStack().peek()).getJndiProperties().put("jndi-property", attributes.getValue(0));
				getStack().push(new StringBuffer());
				_ready = true;
			} else if (qName.equals("service")) {
				((BDLookupConfig) getStack().peek()).getBusinessDelegates().put("bd", attributes.getValue(0));
				getStack().push(new StringBuffer());
				_ready = true;

			} else if (checkForRead(qName)) {
				getStack().push(new StringBuffer());
				_ready = true;
			}
		}

		private boolean checkForRead(String name) {
			boolean flag = false;
			for (Iterator i = _readForElements.iterator(); i.hasNext();) {
				String key = i.next().toString();
				if (key.equals(name)) {
					flag = true;
					break;
				}
			}
			return flag;
		}

		public void endElement(String uri, String localName, String qName) throws SAXException {
			log.debug("qName: " + qName);
			StringBuffer buffer = null;
			if (!qName.equals("modules")) {

				if (checkForRead(qName)) {
					buffer = (StringBuffer) getStack().pop();
				}

				if (qName.equals("name"))
					((BDLookupConfig) getStack().peek()).setModuleName(buffer.toString());

				if (qName.equals("locality"))
					((BDLookupConfig) getStack().peek()).setLocality(buffer.toString());

				if (qName.equals("jndi-property")) {
					String value = (String) ((BDLookupConfig) getStack().peek()).getJndiProperties().get("jndi-property");
					((BDLookupConfig) getStack().peek()).getJndiProperties().remove("jndi-property");
					((BDLookupConfig) getStack().peek()).getJndiProperties().put(value, buffer.toString());
				}

				if (qName.equals("service")) {
					String value = (String) ((BDLookupConfig) getStack().peek()).getBusinessDelegates().get("bd");
					((BDLookupConfig) getStack().peek()).getBusinessDelegates().remove("bd");
					((BDLookupConfig) getStack().peek()).getBusinessDelegates().put(value, buffer.toString());
				}

				if (qName.equals("module")) {
					BDLookupConfig module = (BDLookupConfig) getStack().pop();
					_modules.put(module.getModuleName(), module);
				}
			}
			_ready = false;
		}

		public void characters(char[] ch, int start, int length) throws SAXException {
			if (_ready) {
				((StringBuffer) getStack().peek()).append(ch, start, length);
				log.debug("characters: " + ((StringBuffer) getStack().peek()).toString());
			}
		}

		private Stack getStack() {
			return _stack;
		}

		public Map getModules() {
			return _modules;
		}
	}

}
