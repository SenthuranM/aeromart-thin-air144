package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class InteractiveFreeText implements Serializable {

    private String textSubjectQualifier;

    private String informationType;

    private String status;

    private List<String> freeText;

    public String getTextSubjectQualifier() {
        return textSubjectQualifier;
    }

    public void setTextSubjectQualifier(String textSubjectQualifier) {
        this.textSubjectQualifier = textSubjectQualifier;
    }

    public String getInformationType() {
        return informationType;
    }

    public void setInformationType(String informationType) {
        this.informationType = informationType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getFreeText() {
        return freeText;
    }

    public void setFreeText(List<String> freeText) {
        this.freeText = freeText;
    }

}
