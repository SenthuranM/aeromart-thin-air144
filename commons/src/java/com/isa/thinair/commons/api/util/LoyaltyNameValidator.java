package com.isa.thinair.commons.api.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Name validation algorithm for loyalty member
 * 
 * @author rumesh
 * 
 */
public class LoyaltyNameValidator {

	private static String regex = "^[a-zA-Z]+[.-]?[a-zA-Z]*$";

	public static boolean compareNames(String firstName1, String lastName1, String firstName2, String lastName2) {
		
		/* Step 1 Start */
		String fname1 = firstName1.trim();
		String fname2 = firstName2.trim();
		String sname1 = lastName1.trim();
		String sname2 = lastName2.trim();

		if ((fname1 == null || sname1 == null)
				|| (!Pattern.matches(regex, fname1.replace(" ", "")) || !Pattern.matches(regex, sname1.replace(" ", "")))) {
			return false;
		}

		fname1 = getFormatString(fname1);
		fname2 = getFormatString(fname2);
		sname1 = getFormatString(sname1);
		sname2 = getFormatString(sname2);

		if ((fname1.equals(fname2)) && (sname1.equals(sname2))) {
			return true;
		} else {
			/* Step 2 Start */
			String param1 = soundex(fname1);
			String param2 = soundex(fname2);
			String param3 = soundex(sname1);
			String param4 = soundex(sname2);

			if ((param1.equals(param2)) && (param3.equals(param4))) {
				return true;
			} else {
				/* Step 3 Start */

				String fname1actual = firstName1;
				String fname1value = fname1actual.replaceAll(" ", "");
				String fname2actual = firstName2;
				String fname2value = fname2actual.replaceAll(" ", "");
				String sname1actual = lastName1;
				String sname1value = sname1actual.replaceAll(" ", "");
				String sname2actual = lastName2;
				String sname2value = sname2actual.replaceAll(" ", "");

				if ((fname1value.equals(fname2value)) && (sname1value.equals(sname2value))) {
					return true;
				} else {
					/* Step 4 Start */
					String param1value = soundex(fname1value);
					String param2value = soundex(fname2value);
					String param3value = soundex(sname1value);
					String param4value = soundex(sname2value);

					if ((param1value.equals(param2value)) && (param3value.equals(param4value))) {
						return true;
					} else {
						/* Step 5 Start */
						if ((fname1actual.equals(sname2actual)) && (fname2actual.equals(sname1actual))) {
							return true;
						} else {
							if ((fname1.equals(sname2)) && (fname2.equals(sname1))) {
								return true;
							} else {
								if ((param3.equals(param2)) && (param1.equals(param4))) {
									return true;
								} else {
									if ((fname1value.equals(sname2value)) && (fname2value.equals(sname1value))) {
										return true;
									} else {
										if ((param1value.equals(param4value)) && (param3value.equals(param2value))) {
											return true;
										} else {
											return algorithm1(fname1actual, fname2actual, sname1actual, sname2actual);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	private static String getFormatString(String name) {
		String param = name;
		List<String> getData = Arrays.asList(param.split(" "));

		Collections.sort(getData, new Comparator<String>() {

			@Override
			public int compare(String s1, String s2) {
				return s1.length() - s2.length();
			}
		});

		return getData.get(0).replace("-", "");
	}

	private static String removeH(String source) {
		List<String> vowels = new ArrayList<String>();
		vowels.add("a");
		vowels.add("e");
		vowels.add("i");
		vowels.add("o");
		vowels.add("u");

		int sourceLen = source.length();
		String result = "";
		for (int idx = 0; idx < sourceLen; idx++) {
			String subStr = source.substring(idx, idx + 1);
			if ((subStr.toUpperCase().equals("H") && idx < (sourceLen - 1) && vowels.indexOf(source.substring(idx + 1, idx + 2)) == -1)
					|| (subStr.toUpperCase().equals("H") && idx == (sourceLen - 1))) {
				continue;
			}
			result += source.substring(idx, idx + 1);
		}

		String[] chksingle = result.split(" ");
		result = "";
		for (int chk = 0; chk < chksingle.length; chk++) {
			if (chksingle[chk].length() != 1) {
				result += chksingle[chk] + " ";
			}
		}
		result = result.trim();

		return result.replace("-", "");
	}

	/* Step 2 - Remove Vowels */
	private static String removeVowels(String sourceStrng) {
		String vowels = "[aeiouAEIOU]";
		String replaceWith = "";
		return sourceStrng.replace(vowels, replaceWith);
	}

	/* Step 3 - Replace X With KS and TH with TY */
	private static String rplceStrings(String strngData) {
		return strngData.replace("X", "KS").replace("TH", "TY");
	}

	/* Step 4 - Remove repeated and spaces */
	private static String removeDuplicates(String replData) {
		return replData.replace("([A-Z])\1+", "$1").replace("\\s", "");
	}

	/* Step 5 - Compare lengths and search shorter names */
	private static boolean algorithm1(String fname1actual, String fname2actual, String sname1actual, String sname2actual) {
		String fname1removeH = removeH(fname1actual).toUpperCase();
		String fname1removeV = removeVowels(fname1removeH);
		String fname1replace = rplceStrings(fname1removeV);
		String fname1removeD = removeDuplicates(fname1replace);

		String fname2removeH = removeH(fname2actual).toUpperCase();
		String fname2removeV = removeVowels(fname2removeH);
		String fname2replace = rplceStrings(fname2removeV);
		String fname2removeD = removeDuplicates(fname2replace);

		String sname1removeH = removeH(sname1actual).toUpperCase();
		String sname1removeV = removeVowels(sname1removeH);
		String sname1replace = rplceStrings(sname1removeV);
		String sname1removeD = removeDuplicates(sname1replace);

		String sname2removeH = removeH(sname2actual).toUpperCase();
		String sname2removeV = removeVowels(sname2removeH);
		String sname2replace = rplceStrings(sname2removeV);
		String sname2removeD = removeDuplicates(sname2replace);

		boolean fnameResult;
		if (fname1removeD.length() > fname2removeD.length()) {
			fnameResult = fname1removeD.indexOf(fname2removeD) >= 0 ? true : false;
		} else {
			fnameResult = fname2removeD.indexOf(fname1removeD) >= 0 ? true : false;
		}

		boolean snameResult;
		if (sname1removeD.length() > sname2removeD.length()) {
			snameResult = sname1removeD.indexOf(sname2removeD) >= 0 ? true : false;
		} else {
			snameResult = sname2removeD.indexOf(sname1removeD) >= 0 ? true : false;
		}

		if (fnameResult && snameResult) {
			return true;
		} else {
			/*
			 * Step 6 - IF mismatch replace incoming name with db surname and incoming surname with db name
			 */
			if (fname1removeD.length() > sname2removeD.length()) {
				fnameResult = fname1removeD.indexOf(sname2removeD) >= 0 ? true : false;
			} else {
				fnameResult = sname2removeD.indexOf(fname1removeD) >= 0 ? true : false;
			}

			if (sname1removeD.length() > fname2removeD.length()) {
				snameResult = sname1removeD.indexOf(fname2removeD) >= 0 ? true : false;
			} else {
				snameResult = fname2removeD.indexOf(sname1removeD) >= 0 ? true : false;
			}
			if (fnameResult && snameResult) {
				return true;
			} else {
				return false;
			}
		}
	}

	/* ---- Algorithm 1 Steps End ---- */

	/* ---- Algorithm 3 Steps Start ---- */
	private static String soundex(String s) {

		char[] chars = s.toLowerCase().toCharArray();
		List<String> a = new ArrayList<String>();
		for (Character c : chars) {
			a.add(c.toString());
		}

		String f = new ArrayList<String>(a).remove(0);
	    String r = ""; 
		Map<String, String> codes = new HashMap<String, String>();
	    codes.put("a", "");
	    codes.put("e", "");
	    codes.put("i", "");
	    codes.put("o", "");
	    codes.put("u", "");
		codes.put("b", "1");
		codes.put("f", "1");
		codes.put("p", "1");
		codes.put("v", "1");
		codes.put("c", "2");
		codes.put("g", "2");
		codes.put("j", "2");
		codes.put("k", "2");
		codes.put("q", "2");
		codes.put("s", "2");
		codes.put("x", "2");
		codes.put("z", "2");
		codes.put("d", "3");
		codes.put("t", "3");
		codes.put("l", "4");
		codes.put("m", "5");
		codes.put("n", "5");
		codes.put("r", "6");
	    
		r = f + manipulateArray(a, codes, f);
		
		return (r + "000").substring(0, 4).toUpperCase();

	}

	private static String manipulateArray(List<String> a, Map<String, String> codes, String firstLetter) {
		StringBuilder str = new StringBuilder("");

		List<String> tmpList = new ArrayList<String>();
		for (String tmp : a) {
			tmpList.add(codes.get(tmp));
		}

		List<Object> manpArr = new ArrayList<Object>();
		for (int i = 0; i < tmpList.size(); i++) {
			String codeVal = tmpList.get(i);
			if ((i == 0)
			? !StringUtil.getNotNullString(codeVal).equals(StringUtil.getNotNullString(codes.get(firstLetter)))
			: !StringUtil.getNotNullString(codeVal).equals(StringUtil.getNotNullString(tmpList.get(i - 1)))) {
			manpArr.add(codeVal);
			}
			}
		for (int i = 0; i < manpArr.size(); i++) {
			Object elem = manpArr.get(i);
			if (elem != null && !elem.toString().equals("")) {
				str.append(elem.toString());
			}
		}

		return str.toString();
	}

}