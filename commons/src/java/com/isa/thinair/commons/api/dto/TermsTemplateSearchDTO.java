package com.isa.thinair.commons.api.dto;

/**
 * Data transfer object for TermsTemplate search parameters.
 * 
 * @author thihara
 * 
 */
public class TermsTemplateSearchDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1132456135876532456L;

	/**
	 * Template name to be searched for.
	 */
	private String name;

	/**
	 * Language of templates to be searched for.
	 */
	private String language;

	/**
	 * Carriers templates belong to. Current implementation will consider the following format for multiple carriers.
	 * Multiple : G9_3O_E5 | Single : G9
	 */
	private String carriers;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the carriers
	 */
	public String getCarriers() {
		return carriers;
	}

	/**
	 * @param carriers
	 *            the carriers to set
	 */
	public void setCarriers(String carriers) {
		this.carriers = carriers;
	}

}
