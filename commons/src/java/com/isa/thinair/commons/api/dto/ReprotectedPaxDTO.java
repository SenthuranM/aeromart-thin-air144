package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * @author chanaka
 *
 */
public class ReprotectedPaxDTO implements Serializable {

	private static final long serialVersionUID = -5003285867896379018L;

	Integer reprotectedPaxid;

	Integer pnrPaxId;

	Integer frmFltSegId;

	Integer toFltSegId;

	Integer pnrSegmentId;

	String voucherId;

	String paxEmail;

	String flightNumber;

	String flightDate;

	String paxFirstName;

	String paxlastName;

	String mobileNumber;

	String pnr;

	/**
	 * @return the reprotectedPaxid
	 */
	public Integer getReprotectedPaxid() {
		return reprotectedPaxid;
	}

	/**
	 * @param reprotectedPaxid
	 *            the reprotectedPaxid to set
	 */
	public void setReprotectedPaxid(Integer reprotectedPaxid) {
		this.reprotectedPaxid = reprotectedPaxid;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}

	/**
	 * @param voucherId
	 *            the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the paxEmail
	 */
	public String getPaxEmail() {
		return paxEmail;
	}

	/**
	 * @param paxEmail
	 *            the paxEmail to set
	 */
	public void setPaxEmail(String paxEmail) {
		this.paxEmail = paxEmail;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the flightDate
	 */
	public String getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            the flightDate to set
	 */
	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}

	/**
	 * @param paxFirstName
	 *            the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	/**
	 * @return the paxlastName
	 */
	public String getPaxlastName() {
		return paxlastName;
	}

	/**
	 * @param paxlastName
	 *            the paxlastName to set
	 */
	public void setPaxlastName(String paxlastName) {
		this.paxlastName = paxlastName;
	}

	/**
	 * @return the frmFltSegId
	 */
	public Integer getFrmFltSegId() {
		return frmFltSegId;
	}

	/**
	 * @param frmFltSegId
	 *            the frmFltSegId to set
	 */
	public void setFrmFltSegId(Integer frmFltSegId) {
		this.frmFltSegId = frmFltSegId;
	}

	/**
	 * @return the toFltSegId
	 */
	public Integer getToFltSegId() {
		return toFltSegId;
	}

	/**
	 * @param toFltSegId
	 *            the toFltSegId to set
	 */
	public void setToFltSegId(Integer toFltSegId) {
		this.toFltSegId = toFltSegId;
	}

	/**
	 * @return the pnrSegmentId
	 */
	public Integer getPnrSegmentId() {
		return pnrSegmentId;
	}

	/**
	 * @param pnrSegmentId
	 *            the pnrSegmentId to set
	 */
	public void setPnrSegmentId(Integer pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
