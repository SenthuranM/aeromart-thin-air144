package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * @since 10:16 AM 12/19/2008
 */
public class PayCurrencyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal payCurrMultiplyingExchangeRate = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String payCurrencyCode;

	private Long payCurrencyExchangeRateId;

	private BigDecimal boundaryValue;

	private BigDecimal breakPointValue;

	private Boolean skipCurrencyConversion = false;

	private Integer decimalPlaces;

	/** This value is calculated internally. You are not required to set this value */
	private BigDecimal totalPayCurrencyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal eDirhamFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private PayCurrencyDTO() {

	}

	public PayCurrencyDTO(String payCurrencyCode, BigDecimal payCurrMultiplyingExchangeRate) {
		this.setPayCurrencyCode(payCurrencyCode);
		this.setPayCurrMultiplyingExchangeRate(payCurrMultiplyingExchangeRate);
	}

	public PayCurrencyDTO(String payCurrencyCode, BigDecimal payCurrMultiplyingExchangeRate, BigDecimal boundaryValue,
			BigDecimal breakPointValue) {
		this.setPayCurrencyCode(payCurrencyCode);
		this.setPayCurrMultiplyingExchangeRate(payCurrMultiplyingExchangeRate);
		this.setBoundaryValue(boundaryValue);
		this.setBreakPointValue(breakPointValue);
	}

	public PayCurrencyDTO(String payCurrencyCode, BigDecimal payCurrMultiplyingExchangeRate, BigDecimal boundaryValue,
						  BigDecimal breakPointValue, Integer decimalPlaces) {
		this(payCurrencyCode, payCurrMultiplyingExchangeRate, boundaryValue, breakPointValue);
		this.decimalPlaces = decimalPlaces;
	}

	/**
	 * @return the payCurrencyCode
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the payCurrencyExchangeRateId
	 */
	public Long getPayCurrencyExchangeRateId() {
		return payCurrencyExchangeRateId;
	}

	/**
	 * @param payCurrencyExchangeRateId
	 *            the payCurrencyExchangeRateId to set
	 */
	public void setPayCurrencyExchangeRateId(Long payCurrencyExchangeRateId) {
		this.payCurrencyExchangeRateId = payCurrencyExchangeRateId;
	}

	/**
	 * Clones the PayCurrencyDTO
	 */
	public Object clone() {
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO();
		payCurrencyDTO.setPayCurrencyCode(this.getPayCurrencyCode());
		payCurrencyDTO.setPayCurrMultiplyingExchangeRate(this.getPayCurrMultiplyingExchangeRate());
		payCurrencyDTO.setPayCurrencyExchangeRateId(this.getPayCurrencyExchangeRateId());
		payCurrencyDTO.setTotalPayCurrencyAmount(this.getTotalPayCurrencyAmount());
		payCurrencyDTO.setBoundaryValue(this.getBoundaryValue());
		payCurrencyDTO.setBreakPointValue(this.getBreakPointValue());
		payCurrencyDTO.setSkipCurrencyConversion(this.getSkipCurrencyConversion());
		payCurrencyDTO.seteDirhamFee(this.geteDirhamFee());
		payCurrencyDTO.setDecimalPlaces(this.getDecimalPlaces());

		return payCurrencyDTO;
	}

	/**
	 * @return the totalPayCurrencyAmount
	 */
	public BigDecimal getTotalPayCurrencyAmount() {
		return totalPayCurrencyAmount;
	}

	/**
	 * @param totalPayCurrencyAmount
	 *            the totalPayCurrencyAmount to set
	 */
	public void setTotalPayCurrencyAmount(BigDecimal totalPayCurrencyAmount) {
		this.totalPayCurrencyAmount = totalPayCurrencyAmount;
	}

	/**
	 * @return the payCurrMultiplyingExchangeRate
	 */
	public BigDecimal getPayCurrMultiplyingExchangeRate() {
		return payCurrMultiplyingExchangeRate;
	}

	/**
	 * @param payCurrMultiplyingExchangeRate
	 *            the payCurrMultiplyingExchangeRate to set
	 */
	public void setPayCurrMultiplyingExchangeRate(BigDecimal payCurrMultiplyingExchangeRate) {
		this.payCurrMultiplyingExchangeRate = payCurrMultiplyingExchangeRate;
	}

	/**
	 * @return the boundaryValue
	 */
	public BigDecimal getBoundaryValue() {
		return boundaryValue;
	}

	/**
	 * @param boundaryValue
	 *            the boundaryValue to set
	 */
	public void setBoundaryValue(BigDecimal boundaryValue) {
		this.boundaryValue = boundaryValue;
	}

	/**
	 * @return the breakPointValue
	 */
	public BigDecimal getBreakPointValue() {
		return breakPointValue;
	}

	/**
	 * @param breakPointValue
	 *            the breakPointValue to set
	 */
	public void setBreakPointValue(BigDecimal breakPointValue) {
		this.breakPointValue = breakPointValue;
	}

	public Boolean getSkipCurrencyConversion() {
		return skipCurrencyConversion;
	}

	public void setSkipCurrencyConversion(Boolean skipCurrencyConvertion) {
		this.skipCurrencyConversion = skipCurrencyConvertion;
	}

	public BigDecimal geteDirhamFee() {
		return eDirhamFee;
	}

	public void seteDirhamFee(BigDecimal eDirhamFee) {
		this.eDirhamFee = eDirhamFee;
	}

	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PayCurrencyDTO [boundaryValue=");
		builder.append(boundaryValue);
		builder.append(", breakPointValue=");
		builder.append(breakPointValue);
		builder.append(", payCurrMultiplyingExchangeRate=");
		builder.append(payCurrMultiplyingExchangeRate);
		builder.append(", totalPayCurrencyAmount=");
		builder.append(totalPayCurrencyAmount);
		builder.append(", payCurrencyCode=");
		builder.append(payCurrencyCode);
		builder.append(", decimalPlaces=");
		builder.append(decimalPlaces);
		builder.append("]");
		return builder.toString();
	}
}
