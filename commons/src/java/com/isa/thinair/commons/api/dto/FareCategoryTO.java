/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * To hold Passenger Category data transfer information
 * 
 * @author Sudheera Liyanage
 * @since 1.0
 */
public class FareCategoryTO implements Serializable {

	private static final long serialVersionUID = -5144330838987290709L;

	/** Holds the valid fare category statuses */
	public static enum FareCategoryStatusCodes {
		ACT, INA
	};

	/** Holds the fare category code */
	private String fareCategoryCode;

	/** Holds the fare category desc */
	private String fareCategoryDesc;

	/** Holds the passenger category default sort order */
	private int defaultSortOrder;

	/** Holds the fare category status */
	private FareCategoryStatusCodes status;

	/** Holds the state to show comments or not */
	private boolean showComments;

	/** Holds the state to idenyify defaut fare category */
	private boolean isDefault;

	/**
	 * @return Returns the defaultSortOrder.
	 */
	public int getDefaultSortOrder() {
		return defaultSortOrder;
	}

	/**
	 * @param defaultSortOrder
	 *            The defaultSortOrder to set.
	 */
	public void setDefaultSortOrder(int defaultSortOrder) {
		this.defaultSortOrder = defaultSortOrder;
	}

	/**
	 * @return Returns the fareCategoryCode.
	 */
	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	/**
	 * @param fareCategoryCode
	 *            The fareCategoryCode to set.
	 */
	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	/**
	 * @return Returns the fareCategoryDesc.
	 */
	public String getFareCategoryDesc() {
		return fareCategoryDesc;
	}

	/**
	 * @param fareCategoryDesc
	 *            The fareCategoryDesc to set.
	 */
	public void setFareCategoryDesc(String fareCategoryDesc) {
		this.fareCategoryDesc = fareCategoryDesc;
	}

	/**
	 * @return Returns the status.
	 */
	public FareCategoryStatusCodes getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(FareCategoryStatusCodes status) {
		this.status = status;
	}

	/**
	 * @return Returns the showComments.
	 */
	public boolean getShowComments() {
		return showComments;
	}

	/**
	 * @return Returns the isDefault.
	 */
	public boolean isDefault() {
		return isDefault;
	}

	/**
	 * @param isDefault
	 *            The isDefault to set.
	 */
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 * @return Returns the showComments.
	 */
	public boolean isShowComments() {
		return showComments;
	}

	/**
	 * @param showComments
	 *            The showComments to set.
	 */
	public void setShowComments(boolean showComments) {
		this.showComments = showComments;
	}

}
