package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Holds configurations related to XBE contact details in create & modify reservations
 * 
 * @author rumesh
 * 
 */
public class XBEContactConfigDTO extends BaseContactConfigDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean xbeVisibility;

	/**
	 * @return the xbeVisibility
	 */
	public boolean isXbeVisibility() {
		return xbeVisibility;
	}

	/**
	 * @param xbeVisibility
	 *            the xbeVisibility to set
	 */
	public void setXbeVisibility(boolean xbeVisibility) {
		this.xbeVisibility = xbeVisibility;
	}
}
