package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class RelatedProductInfo implements Serializable {

    private String quantity;

    private List<String> qualifier;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public List<String> getQualifier() {
        return qualifier;
    }

    public void setQualifier(List<String> qualifier) {
        this.qualifier = qualifier;
    }
}
