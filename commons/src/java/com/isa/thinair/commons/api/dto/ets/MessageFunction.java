package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class MessageFunction implements Serializable {

	private String serviceType;

	private String messageFunction;

	private String subMessageFunction;

	private String responseType;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getMessageFunction() {
		return messageFunction;
	}

	public void setMessageFunction(String messageFunction) {
		this.messageFunction = messageFunction;
	}

	public String getSubMessageFunction() {
		return subMessageFunction;
	}

	public void setSubMessageFunction(String subMessageFunction) {
		this.subMessageFunction = subMessageFunction;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
}
