package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class CouponStatusTKCREQMessage implements Serializable {

	private MessageHeader messageHeader;
	
	private MessageFunction messageFunction;
	
	private OriginatorInformation originatorInformation;
	
	private TicketControlTicketNumberDetails tickets;

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}
	

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}
	

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}
	

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}
	

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}
	

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}
	

	public TicketControlTicketNumberDetails getTickets() {
		return tickets;
	}
	

	public void setTickets(TicketControlTicketNumberDetails tickets) {
		this.tickets = tickets;
	}
	
	
	
}
