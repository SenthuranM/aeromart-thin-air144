package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class AeroMartUpdateExternalCouponStatusRq implements Serializable {

	private static final long serialVersionUID = 5182130582757508269L;

	private List<ReservationControlInformation> reservationControlInformation;
	private List<TravellerTicketingInformation> travellers;

	public List<ReservationControlInformation> getReservationControlInformation() {
		return reservationControlInformation;
	}

	public void setReservationControlInformation(List<ReservationControlInformation> reservationControlInformation) {
		this.reservationControlInformation = reservationControlInformation;
	}

	public List<TravellerTicketingInformation> getTravellers() {
		return travellers;
	}

	public void setTravellers(List<TravellerTicketingInformation> travellers) {
		this.travellers = travellers;
	}
}
