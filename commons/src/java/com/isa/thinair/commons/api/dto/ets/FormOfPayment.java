package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class FormOfPayment implements Serializable {

    private FormOfPaymentIndicator formOfPaymentType;

    private String dataIndicator;

    private String paymentAmount;

    private String referenceNumber;

    private String accountHolderNumber;

    private FormOfPayment.AdditionalDetails additionalDetails;

    private String text;

    public FormOfPaymentIndicator getFormOfPaymentType() {
        return formOfPaymentType;
    }

    public void setFormOfPaymentType(FormOfPaymentIndicator formOfPaymentType) {
        this.formOfPaymentType = formOfPaymentType;
    }

    public String getDataIndicator() {
        return dataIndicator;
    }

    public void setDataIndicator(String dataIndicator) {
        this.dataIndicator = dataIndicator;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getAccountHolderNumber() {
        return accountHolderNumber;
    }

    public void setAccountHolderNumber(String accountHolderNumber) {
        this.accountHolderNumber = accountHolderNumber;
    }

    public AdditionalDetails getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(AdditionalDetails additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static class AdditionalDetails implements Serializable {

        private String vendorCode;

        private String expirationDate;

        private String approvalCode;

        private String sourceOfApproval;

        private String authorizedAmount;

        private String addressVerificationCode;

        private String extendedPaymentCode;

        private String membershipStatus;

        private String creditCardTransactionInformation;

        public String getVendorCode() {
            return vendorCode;
        }

        public void setVendorCode(String vendorCode) {
            this.vendorCode = vendorCode;
        }

        public String getExpirationDate() {
            return expirationDate;
        }

        public void setExpirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
        }

        public String getApprovalCode() {
            return approvalCode;
        }

        public void setApprovalCode(String approvalCode) {
            this.approvalCode = approvalCode;
        }

        public String getSourceOfApproval() {
            return sourceOfApproval;
        }

        public void setSourceOfApproval(String sourceOfApproval) {
            this.sourceOfApproval = sourceOfApproval;
        }

        public String getAuthorizedAmount() {
            return authorizedAmount;
        }

        public void setAuthorizedAmount(String authorizedAmount) {
            this.authorizedAmount = authorizedAmount;
        }

        public String getAddressVerificationCode() {
            return addressVerificationCode;
        }

        public void setAddressVerificationCode(String addressVerificationCode) {
            this.addressVerificationCode = addressVerificationCode;
        }

        public String getExtendedPaymentCode() {
            return extendedPaymentCode;
        }

        public void setExtendedPaymentCode(String extendedPaymentCode) {
            this.extendedPaymentCode = extendedPaymentCode;
        }

        public String getMembershipStatus() {
            return membershipStatus;
        }

        public void setMembershipStatus(String membershipStatus) {
            this.membershipStatus = membershipStatus;
        }

        public String getCreditCardTransactionInformation() {
            return creditCardTransactionInformation;
        }

        public void setCreditCardTransactionInformation(String creditCardTransactionInformation) {
            this.creditCardTransactionInformation = creditCardTransactionInformation;
        }
    }

}
