/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.constants;

import java.io.Serializable;

/**
 * sample enum for days of week
 * 
 * @author Lasantha Pambagoda
 */
public final class DayOfWeek implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9006072119649121951L;
	private String day;

	private DayOfWeek(String day) {
		this.day = day;
	}

	public String toString() {
		return this.day;
	}

	public boolean equals(DayOfWeek dayofweek) {
		return dayofweek.toString().equals(this.day);
	}

	public static final DayOfWeek SUNDAY = new DayOfWeek("SUNDAY");
	public static final DayOfWeek MONDAY = new DayOfWeek("MONDAY");
	public static final DayOfWeek TUESDAY = new DayOfWeek("TUESDAY");
	public static final DayOfWeek WEDNESDAY = new DayOfWeek("WEDNESDAY");
	public static final DayOfWeek THURSDAY = new DayOfWeek("THURSDAY");
	public static final DayOfWeek FRIDAY = new DayOfWeek("FRIDAY");
	public static final DayOfWeek SATURDAY = new DayOfWeek("SATURDAY");

}
