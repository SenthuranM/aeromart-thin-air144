package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class MessageHeader implements Serializable {
	
	private String referenceNo;

	private String commonAccessReference;

	private MessageIdentifier messageId;
	
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String value) {
		this.referenceNo = value;
	}

	public String getCommonAccessReference() {
		return commonAccessReference;
	}

	public void setCommonAccessReference(String value) {
		this.commonAccessReference = value;
	}

	public MessageIdentifier getMessageId() {
		return messageId;
	}

	public void setMessageId(MessageIdentifier value) {
		this.messageId = value;
	}


}
