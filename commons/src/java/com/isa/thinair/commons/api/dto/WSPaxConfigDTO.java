package com.isa.thinair.commons.api.dto;

/**
 * includes WS related pax configurations
 * 
 * @author rumesh
 * 
 */
public class WSPaxConfigDTO extends BasePaxConfigDTO {
	private static final long serialVersionUID = 8061021334918340870L;

	private boolean wsVisibility;
	private boolean wsMandatory;

	public boolean isWsVisibility() {
		return wsVisibility;
	}

	public void setWsVisibility(boolean wsVisibility) {
		this.wsVisibility = wsVisibility;
	}

	public boolean isWsMandatory() {
		return wsMandatory;
	}

	public void setWsMandatory(boolean wsMandatory) {
		this.wsMandatory = wsMandatory;
	}
}
