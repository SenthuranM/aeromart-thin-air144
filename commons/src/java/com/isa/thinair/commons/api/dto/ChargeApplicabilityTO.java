/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * To hold charge applicability data transfer information
 * 
 * @author Nilindra Fernando
 * @since 3/10/2008
 */
public class ChargeApplicabilityTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2027600514290920032L;

	/** Holds the applicability level */
	private int appliesTo;

	/** Holds the applicability level description */
	private String appliesToDesc;

	/** Holds the applicability level visibility */
	private boolean visibility;

	public static final int APPLICABLE_TO_ALL = 0; // Applicable for any passenger
													// type
	public static final int APPLICABLE_TO_ADULT_ONLY = 1; // Applicable for adult
															// passenger type only
	public static final int APPLICABLE_TO_INFANT_ONLY = 2; // Applicable for infant
															// passenger type only
	public static final int APPLICABLE_TO_CHILD_ONLY = 3; // Applicable for child
															// passenger type only
	public static final int APPLICABLE_TO_ADULT_INFANT = 4; // Applicable for both adult and
															// infant passenger type only
	public static final int APPLICABLE_TO_ADULT_CHILD = 5; // Applicable for both adult and
															// child passenger type only
	public static final int APPLICABLE_TO_RESERVATION = 6; // Applicable for the
															// reservation
	public static final int APPLICABLE_TO_OND = 7; // Applicable for the OND

	/**
	 * @return Returns the appliesTo.
	 */
	public int getAppliesTo() {
		return appliesTo;
	}

	/**
	 * @param appliesTo
	 *            The appliesTo to set.
	 */
	public void setAppliesTo(int appliesTo) {
		this.appliesTo = appliesTo;
	}

	/**
	 * @return Returns the appliesToDesc.
	 */
	public String getAppliesToDesc() {
		return appliesToDesc;
	}

	/**
	 * @param appliesToDesc
	 *            The appliesToDesc to set.
	 */
	public void setAppliesToDesc(String appliesToDesc) {
		this.appliesToDesc = appliesToDesc;
	}

	/**
	 * @return Returns the visibility.
	 */
	public boolean isVisibility() {
		return visibility;
	}

	/**
	 * @param visibility
	 *            The visibility to set.
	 */
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
}
