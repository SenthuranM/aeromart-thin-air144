package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class DocumentInformationDetails implements Serializable {

	private String documentNumber;

	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
}
