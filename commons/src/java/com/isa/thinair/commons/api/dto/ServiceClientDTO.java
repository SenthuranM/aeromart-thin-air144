package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class ServiceClientDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String serviceClientCode;

	private boolean addAddressToMessageBody;

	private String emailDomain;
	
	private String apiUrl;

	public boolean getAddAddressToMessageBody() {
		return addAddressToMessageBody;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setAddAddressToMessageBody(boolean addAddressToMessageBody) {
		this.addAddressToMessageBody = addAddressToMessageBody;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}

	public String getServiceClientCode() {
		return serviceClientCode;
	}

	public void setServiceClientCode(String serviceClientCode) {
		this.serviceClientCode = serviceClientCode;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}
}
