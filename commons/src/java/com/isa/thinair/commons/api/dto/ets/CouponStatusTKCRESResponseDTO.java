package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class CouponStatusTKCRESResponseDTO implements Serializable{

	private CouponStatusTKCRES payload;
	
	private RequestIdentity requestIdentity;
	
	private ServiceError error;
	
	private ServiceWarning serviceWarnings;
	
	public CouponStatusTKCRES getPayload() {
		return payload;
	}
	

	public void setPayload(CouponStatusTKCRES payload) {
		this.payload = payload;
	}
	

	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}
	

	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}
	

	public ServiceError getError() {
		return error;
	}
	

	public void setError(ServiceError error) {
		this.error = error;
	}
	

	public ServiceWarning getServiceWarnings() {
		return serviceWarnings;
	}
	

	public void setServiceWarnings(ServiceWarning serviceWarnings) {
		this.serviceWarnings = serviceWarnings;
	}
	
}
