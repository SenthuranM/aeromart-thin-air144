package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class BookingClassCharMappingTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2560988163037765427L;

	private String bookingCode;

	private int gdsId;

	public BookingClassCharMappingTO() {
		// do nothing
	}

	public BookingClassCharMappingTO(String bookingCode, int gdsId) {
		this.bookingCode = bookingCode;
		this.gdsId = gdsId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof BookingClassCharMappingTO)) {
			return false;
		}
		BookingClassCharMappingTO bookingClassCharMappingTO = (BookingClassCharMappingTO) o;
		if (bookingClassCharMappingTO != null && bookingClassCharMappingTO.getBookingCode() != null) {
			if (bookingClassCharMappingTO.getBookingCode().equals(this.bookingCode)
					&& bookingClassCharMappingTO.getGdsId() == this.gdsId) {
				return true;
			}
		}
		return false;
	}

	public int hashCode() {
		return (bookingCode == null ? 0 : bookingCode.hashCode());
	}
}
