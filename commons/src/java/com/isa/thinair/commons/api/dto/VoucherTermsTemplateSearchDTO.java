package com.isa.thinair.commons.api.dto;

/**
 * Data transfer object for VoucherTermsTemplate search parameters.
 * 
 * @author chanaka
 * 
 */
public class VoucherTermsTemplateSearchDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1132456135876532456L;

	/**
	 * Template name to be searched for.
	 */
	private String name;

	/**
	 * Language of templates to be searched for.
	 */
	private String language;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

}
