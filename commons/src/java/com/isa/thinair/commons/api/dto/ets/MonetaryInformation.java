package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class MonetaryInformation implements Serializable {

    private String amountTypeQualifier;

    private String amount;

    private String currencyCode;

    private String departureLocation;

    private String arrivalLocation;

    public String getAmountTypeQualifier() {
        return amountTypeQualifier;
    }

    public void setAmountTypeQualifier(String amountTypeQualifier) {
        this.amountTypeQualifier = amountTypeQualifier;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(String departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }
}
