package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Data transfer object to hold the TermsTemplateAudit details
 * 
 * @author thihara
 * 
 */
public class TermsTemplateAuditDTO implements Serializable {

	private static final long serialVersionUID = 5413245678943574L;

	private String userId;

	private Date modifiedTime;

	private String ipAddress;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the modifiedTime
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * @param modifiedTime
	 *            the modifiedTime to set
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
