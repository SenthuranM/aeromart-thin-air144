/**
 * 
 */
package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Janaka Padukka
 * 
 */
public class OndPublishDestinationDTO implements Serializable {


	private OndLocationDTO ondLocation;

	private String mktCarrierCode;

	private String currencyCode;

	private List<String> operatingCarriers;

	private int routeType;

	private Collection<List<String>> transitHubs = new ArrayList<List<String>>();

	public void addOperatingCarrier(String carrier) {

		if (operatingCarriers == null) {
			operatingCarriers = new ArrayList<String>();
		}
		operatingCarriers.add(carrier);

	}

	public List<String> getOperatingCarriers() {
		return operatingCarriers;
	}

	public void setOperatingCarriers(List<String> operatingCarriers) {
		this.operatingCarriers = operatingCarriers;
	}

	public String getMktCarrierCode() {
		return mktCarrierCode;
	}

	public void setMktCarrierCode(String mktCarrierCode) {
		this.mktCarrierCode = mktCarrierCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public int getRouteType() {
		return routeType;
	}

	public void setRouteType(int routeType) {
		this.routeType = routeType;
	}

	public Collection<List<String>> getTransitHubs() {
		return transitHubs;
	}

	public void setTransitHubs(Collection<List<String>> transitHubs) {
		this.transitHubs = transitHubs;
	}

	public OndLocationDTO getOndLocation() {
		return ondLocation;
	}

	public void setOndLocation(OndLocationDTO ondLocation) {
		this.ondLocation = ondLocation;
	}

}
