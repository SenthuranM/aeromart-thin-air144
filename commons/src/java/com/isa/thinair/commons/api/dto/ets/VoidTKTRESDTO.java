package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class VoidTKTRESDTO implements Serializable {

	private MessageFunction messageFunction;
	
	private TicketCoupon ticketCoupon;
	
	private List<TicketNumberDetails> tickets;
	
	private MessageHeader messageHeader;

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}

	public TicketCoupon getTicketCoupon() {
		return ticketCoupon;
	}

	public void setTicketCoupon(TicketCoupon ticketCoupon) {
		this.ticketCoupon = ticketCoupon;
	}

	public List<TicketNumberDetails> getTickets() {
		return tickets;
	}

	public void setTickets(List<TicketNumberDetails> tickets) {
		this.tickets = tickets;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

}
