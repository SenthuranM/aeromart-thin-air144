package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TaxDetails implements Serializable {

	private String indicator;

	private List<Tax> tax;

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	public List<Tax> getTax() {
		return tax;
	}

	public void setTax(List<Tax> tax) {
		this.tax = tax;
	}

	public static class Tax implements Serializable {

		private String amount;

		private String countryCode;

		private String currencyCode;

		private String type;

		private String taxFiledAmount;

		private String taxFiledCurrencyCode;

		private String taxFiledType;

		private String filedConversionRate;

		private String taxQualifier;

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getCurrencyCode() {
			return currencyCode;
		}

		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getTaxFiledAmount() {
			return taxFiledAmount;
		}

		public void setTaxFiledAmount(String taxFiledAmount) {
			this.taxFiledAmount = taxFiledAmount;
		}

		public String getTaxFiledCurrencyCode() {
			return taxFiledCurrencyCode;
		}

		public void setTaxFiledCurrencyCode(String taxFiledCurrencyCode) {
			this.taxFiledCurrencyCode = taxFiledCurrencyCode;
		}

		public String getTaxFiledType() {
			return taxFiledType;
		}

		public void setTaxFiledType(String taxFiledType) {
			this.taxFiledType = taxFiledType;
		}

		public String getFiledConversionRate() {
			return filedConversionRate;
		}

		public void setFiledConversionRate(String filedConversionRate) {
			this.filedConversionRate = filedConversionRate;
		}

		public String getTaxQualifier() {
			return taxQualifier;
		}

		public void setTaxQualifier(String taxQualifier) {
			this.taxQualifier = taxQualifier;
		}
	}
}
