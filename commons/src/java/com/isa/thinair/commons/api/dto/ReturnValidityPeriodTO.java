package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class ReturnValidityPeriodTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1590947701713121816L;
	private int id;
	private String description;
	private int salesChannelCode;
	private int validityPeriod;
	private String unitOfMessure;
	private String status;

	public static enum RV_UNIT_OF_MEASURE {
		Y, M, W, D
	};

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(int salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public int getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(int validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public String getUnitOfMessure() {
		return unitOfMessure;
	}

	public void setUnitOfMessure(String unitOfMessure) {
		this.unitOfMessure = unitOfMessure;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDerivedBoundaryDate(Date date) throws ModuleException {
		String measure = PlatformUtiltiies.nullHandler(getUnitOfMessure());

		if (!measure.isEmpty() && date != null) {
			if (measure.equals(RV_UNIT_OF_MEASURE.D.toString())) {
				return CalendarUtil.add(date, 0, 0, getValidityPeriod(), 0, 0, 0);
			} else if (measure.equals(RV_UNIT_OF_MEASURE.W.toString())) {
				return CalendarUtil.add(date, 0, 0, getValidityPeriod() * 7, 0, 0, 0);
			} else if (measure.equals(RV_UNIT_OF_MEASURE.M.toString())) {
				return CalendarUtil.add(date, 0, getValidityPeriod(), 0, 0, 0, 0);
			} else if (measure.equals(RV_UNIT_OF_MEASURE.Y.toString())) {
				return CalendarUtil.add(date, getValidityPeriod(), 0, 0, 0, 0, 0);
			}
		}

		throw new ModuleException("commons.data.return.validity.unit.of.measure.is.empty");
	}

	@Override
	public String toString() {
		return getId() + "|" + getDescription() + "|" + getSalesChannelCode() + "|" + getValidityPeriod() + "|"
				+ getUnitOfMessure() + "|" + getStatus();
	}
}
