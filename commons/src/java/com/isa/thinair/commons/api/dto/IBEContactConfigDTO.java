package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Holds configurations related to IBE contact details in create & modify reservations
 * 
 * @author rumesh
 * 
 */
public class IBEContactConfigDTO extends BaseContactConfigDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean ibeVisibility;

	/**
	 * @return the ibeVisibility
	 */
	public boolean isIbeVisibility() {
		return ibeVisibility;
	}

	/**
	 * @param ibeVisibility
	 *            the ibeVisibility to set
	 */
	public void setIbeVisibility(boolean ibeVisibility) {
		this.ibeVisibility = ibeVisibility;
	}
}
