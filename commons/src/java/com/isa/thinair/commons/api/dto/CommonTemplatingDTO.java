/**
 * 
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * @author Noshani
 * 
 */
public class CommonTemplatingDTO implements Serializable {

	private static final long serialVersionUID = 6466071649065864438L;

	private String carrierName;

	private String carrierCode;

	private String airlineDomain;

	private String airlineSubDomain;

	private String unsubscribeEMail;

	private String accelAeroClientID;

	public String getAccelAeroClientID() {
		return accelAeroClientID;
	}

	public void setAccelAeroClientID(String accelAeroClientID) {
		this.accelAeroClientID = accelAeroClientID;
	}

	/**
	 * @return Returns the carrierCode.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the carrierName.
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/**
	 * @param carrierName
	 *            The carrierName to set.
	 */
	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	/**
	 * @return Returns the airlineDomain.
	 */
	public String getAirlineDomain() {
		return airlineDomain;
	}

	/**
	 * @param airlineDomain
	 *            The airlineDomain to set.
	 */
	public void setAirlineDomain(String airlineDomain) {
		this.airlineDomain = airlineDomain;
	}

	/**
	 * @return Returns the airlineSubDomain.
	 */
	public String getAirlineSubDomain() {
		return airlineSubDomain;
	}

	/**
	 * @param airlineSubDomain
	 *            The airlineSubDomain to set.
	 */
	public void setAirlineSubDomain(String airlineSubDomain) {
		this.airlineSubDomain = airlineSubDomain;
	}

	/**
	 * @return Returns the unsubscribeEMail.
	 */
	public String getUnsubscribeEMail() {
		return unsubscribeEMail;
	}

	/**
	 * @param unsubscribeEMail
	 *            The unsubscribeEMail to set.
	 */
	public void setUnsubscribeEMail(String unsubscribeEMail) {
		this.unsubscribeEMail = unsubscribeEMail;
	}
}
