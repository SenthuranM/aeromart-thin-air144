/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.exception;

import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;

public class ModuleException extends CommonsException {

	private static final long serialVersionUID = 1L;

	private String custumValidateMessage;
	
	private ErrorType errorType = ErrorType.OTHER_ERROR;

	public ModuleException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause, exceptionCode, moduleCode);

		if (cause != null) {
			if (cause instanceof ModuleException) {
				setExceptionDetails(((ModuleException) cause).getExceptionDetails());
			} else if (cause instanceof CommonsException) {
				setExceptionDetails(((CommonsException) cause).getExceptionDetails());
			}
		}
	}

	public ModuleException(Throwable cause, String exceptionCode) {
		super(cause, exceptionCode);
	}

	public ModuleException(String exceptionCode, String moduleCode) {
		super(exceptionCode, moduleCode);
	}

	public ModuleException(String exceptionCode, Object exceptionDetails) {
		super(exceptionCode, exceptionDetails);
	}

	public ModuleException(String exceptionCode) {
		super(exceptionCode);
	}

	public String getCustumValidateMessage() {
		return custumValidateMessage;
	}

	public void setCustumValidateMessage(String custumValidateMessage) {
		this.custumValidateMessage = custumValidateMessage;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}	
}
