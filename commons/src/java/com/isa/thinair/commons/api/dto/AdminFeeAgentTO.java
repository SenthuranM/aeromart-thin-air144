package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class AdminFeeAgentTO implements Serializable {

	private static final long serialVersionUID = -5689854751254562575L;
                                                  
	private String adminFeeAgentCode;

	public String getAdminFeeAgentCode() {
		return adminFeeAgentCode;
	}

	public void setAdminFeeAgentCode(String adminFeeAgentCode) {
		this.adminFeeAgentCode = adminFeeAgentCode;
	}
}
