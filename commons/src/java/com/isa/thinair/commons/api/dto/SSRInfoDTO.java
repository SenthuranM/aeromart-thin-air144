package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;

public class SSRInfoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2603103180270678233L;
	private Integer sSRId;
	private String sSRCode;
	private String sSRDescription;
	private String sSRName;
	private String status;
	private Integer sSRCategoryId;
	private Integer sSRSubCategoryId;
	private boolean shownInIBE;
	private boolean shownInXBE;
	private boolean shownInGDS;
	private boolean shownInWs;
	private boolean validForPALCAL;
	private boolean userDefinedSSR;

	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
	private long version = -1;

	private String airportList;

	private String ondList;

	private String applDeparture;

	private String applArrival;

	private String applTransit;

	private String appConsId;

	private Integer transitMaxMins;

	private Integer transitMinMins;

	private String applyOn;

	private Integer pnrSegId;

	private String languageList;
	private String selectedLanguageTranslations;

	private int sortOrder;
	private String ssrCutoffTime;

	public String getLanguageList() {
		return languageList;
	}

	public void setLanguageList(String languageList) {
		this.languageList = languageList;
	}

	public String getSelectedLanguageTranslations() {
		return selectedLanguageTranslations;
	}

	public void setSelectedLanguageTranslations(String selectedLanguageTranslations) {
		this.selectedLanguageTranslations = selectedLanguageTranslations;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getApplyOn() {
		return applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	public Integer getTransitMaxMins() {
		return transitMaxMins;
	}

	public void setTransitMaxMins(Integer transitMaxMins) {
		this.transitMaxMins = transitMaxMins;
	}

	public Integer getTransitMinMins() {
		return transitMinMins;
	}

	public void setTransitMinMins(Integer transitMinMins) {
		this.transitMinMins = transitMinMins;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Integer getSSRId() {
		return this.sSRId;
	}

	public void setSSRId(Integer id) {
		this.sSRId = id;
	}

	public String getSSRCode() {
		return this.sSRCode;
	}

	public void setSSRCode(String code) {
		this.sSRCode = code;
	}

	public String getSSRDescription() {
		return this.sSRDescription;
	}

	public void setSSRDescription(String description) {
		this.sSRDescription = description;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getSSRCategoryId() {
		return this.sSRCategoryId;
	}

	public void setSSRCategoryId(Integer ssrCategoryId) {
		this.sSRCategoryId = ssrCategoryId;
	}

	public Integer getSSRSubCategoryId() {
		return this.sSRSubCategoryId;
	}

	public void setSSRSubCategoryId(Integer subCategoryId) {
		this.sSRSubCategoryId = subCategoryId;
	}

	public boolean isShownInIBE() {
		return this.shownInIBE;
	}

	public void setShownInIBE(boolean shownInIBE) {
		this.shownInIBE = shownInIBE;
	}

	public boolean isShownInXBE() {
		return this.shownInXBE;
	}

	/**
	 * @return the shownInGDS
	 */
	public boolean isShownInGDS() {
		return shownInGDS;
	}

	/**
	 * @param shownInGDS the shownInGDS to set
	 */
	public void setShownInGDS(boolean shownInGDS) {
		this.shownInGDS = shownInGDS;
	}

	public void setShownInXBE(boolean shownInXBE) {
		this.shownInXBE = shownInXBE;
	}

	public String getAirportList() {
		return airportList;
	}

	public void setAirportList(String airportList) {
		this.airportList = airportList;
	}

	public String getOndList() {
		return ondList;
	}

	public void setOndList(String ondList) {
		this.ondList = ondList;
	}

	public String getApplDeparture() {
		return applDeparture;
	}

	public void setApplDeparture(String applDeparture) {
		this.applDeparture = applDeparture;
	}

	public String getApplArrival() {
		return applArrival;
	}

	public void setApplArrival(String applArrival) {
		this.applArrival = applArrival;
	}

	public String getApplTransit() {
		return applTransit;
	}

	public void setApplTransit(String applTransit) {
		this.applTransit = applTransit;
	}

	public String getAppConsId() {
		return appConsId;
	}

	public void setAppConsId(String appConsId) {
		this.appConsId = appConsId;
	}

	public String getSSRName() {
		return sSRName;
	}

	public void setSSRName(String sSRName) {
		this.sSRName = sSRName;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSsrCutoffTime() {
		return ssrCutoffTime;
	}

	public void setSsrCutoffTime(String ssrCutoffTime) {
		this.ssrCutoffTime = ssrCutoffTime;
	}

	public boolean isShownInWs() {
		return shownInWs;
	}

	public void setShownInWs(boolean shownInWs) {
		this.shownInWs = shownInWs;
	}

	public boolean isValidForPALCAL() {
		return validForPALCAL;
	}

	public void setValidForPALCAL(boolean validForPALCAL) {
		this.validForPALCAL = validForPALCAL;
	}

	public boolean isUserDefinedSSR() {
		return userDefinedSSR;
	}

	public void setUserDefinedSSR(boolean userDefinedSSR) {
		this.userDefinedSSR = userDefinedSSR;
	}
	
}
