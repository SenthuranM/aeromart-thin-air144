package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class RefundTKTRESResponseDTO implements Serializable {

	private RefundTKTRESDTO payload;

	private RequestIdentity requestIdentity;

	private ServiceError error;

	private List<ServiceWarning> serviceWarnings;

	public RefundTKTRESDTO getPayload() {
		return payload;
	}

	public void setPayload(RefundTKTRESDTO payload) {
		this.payload = payload;
	}

	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}

	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}

	public ServiceError getError() {
		return error;
	}

	public void setError(ServiceError error) {
		this.error = error;
	}

	public List<ServiceWarning> getServiceWarnings() {
		return serviceWarnings;
	}

	public void setServiceWarnings(List<ServiceWarning> serviceWarnings) {
		this.serviceWarnings = serviceWarnings;
	}

}
