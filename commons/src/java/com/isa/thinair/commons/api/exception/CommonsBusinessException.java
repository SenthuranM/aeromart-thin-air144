/*
 * Created on Jul 5, 2005
 *
 */
package com.isa.thinair.commons.api.exception;

/**
 * @author Nasly
 * 
 */
public class CommonsBusinessException extends CommonsRuntimeException {
	private static final long serialVersionUID = 8814288433964847132L;

	public CommonsBusinessException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause, exceptionCode, moduleCode);
	}

	public CommonsBusinessException(Throwable cause, String exceptionCode) {
		super(cause, exceptionCode);
	}

	public CommonsBusinessException(String exceptionCode, String moduleCode) {
		super(exceptionCode, moduleCode);
	}

	public CommonsBusinessException(String exceptionCode) {
		super(exceptionCode);
	}
}
