package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class Header implements Serializable {

	private InterchangeHeader interchangeHeader;

	public InterchangeHeader getInterchangeHeader() {
		return interchangeHeader;
	}
	
	public void setInterchangeHeader(InterchangeHeader interchangeHeader) {
		this.interchangeHeader = interchangeHeader;
	}
	
}
