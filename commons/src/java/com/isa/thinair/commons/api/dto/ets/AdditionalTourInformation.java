package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class AdditionalTourInformation implements Serializable {

    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
