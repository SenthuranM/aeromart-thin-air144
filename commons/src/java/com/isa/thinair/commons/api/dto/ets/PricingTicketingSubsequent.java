package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class PricingTicketingSubsequent implements Serializable {

    private String fareBasis;

    public String getFareBasis() {
        return fareBasis;
    }

    public void setFareBasis(String fareBasis) {
        this.fareBasis = fareBasis;
    }
}
