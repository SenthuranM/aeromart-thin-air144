/**
 * 
 */
package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Janaka Padukka
 * 
 */
public class OndPublishedTO implements Serializable {

	private List<OndPublishAirportDTO> airports;

	private List<OndPublishCurrencyDTO> currencies;

	private List<OndPublishOriginDTO> origins;

	public List<OndPublishAirportDTO> getAirports() {
		return airports;
	}

	public void setAirports(List<OndPublishAirportDTO> airports) {
		this.airports = airports;
	}

	public List<OndPublishCurrencyDTO> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<OndPublishCurrencyDTO> currencies) {
		this.currencies = currencies;
	}
	
	

	public void setOrigins(List<OndPublishOriginDTO> origins) {
		this.origins = origins;
	}

	public void addOrigin(OndPublishOriginDTO origin) {

		if (origins == null) {
			origins = new ArrayList<OndPublishOriginDTO>();
		}

		origins.add(origin);
	}

	public List<OndPublishOriginDTO> getOrigins() {
		return origins;
	}
}
