package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chethiya
 *
 */
public class VoucherRedeemRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -769240072403066836L;

	private List<String> voucherIDList = new ArrayList<String>();

	private List<String> blockedVoucherIDList = new ArrayList<String>();

	private String balanceToPay;

	private List<String> paxNameList;
	
	private Integer unsuccessfulAttempts = 0;
	
	private String OTP;
	
	

	/**
	 * @return the voucherIDList
	 */
	public List<String> getVoucherIDList() {
		if (this.voucherIDList == null) {
			this.voucherIDList = new ArrayList<>();
		}
		return this.voucherIDList;
	}

	/**
	 * @param voucherIDList
	 *            the voucherIDList to set
	 */
	public void setVoucherIDList(List<String> voucherIDList) {
		this.voucherIDList = voucherIDList;
	}

	/**
	 * @return the balanceToPay
	 */
	public String getBalanceToPay() {
		return balanceToPay;
	}

	/**
	 * @param balanceToPay
	 *            the balanceToPay to set
	 */
	public void setBalanceToPay(String balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

	/**
	 * @return the previousVoucherIDList
	 */
	public List<String> getBlockedVoucherIDList() {
		return blockedVoucherIDList;
	}

	/**
	 * @param previousVoucherIDList the previousVoucherIDList to set
	 */
	public void setBlockedVoucherIDList(List<String> blockedVoucherIDList) {
		this.blockedVoucherIDList = blockedVoucherIDList;
	}

	/**
	 * @return the paxNameList
	 */
	public List<String> getPaxNameList() {
		return paxNameList;
	}

	/**
	 * @param paxNameList
	 *            the paxNameList to set
	 */
	public void setPaxNameList(ArrayList<String> paxNameList) {
		this.paxNameList = paxNameList;
	}

	public Integer getUnsuccessfulAttempts() {
		return unsuccessfulAttempts;
	}

	public void setUnsuccessfulAttempts(Integer unsuccessfulAttempts) {
		this.unsuccessfulAttempts = unsuccessfulAttempts;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String oTP) {
		OTP = oTP;
	}


}
