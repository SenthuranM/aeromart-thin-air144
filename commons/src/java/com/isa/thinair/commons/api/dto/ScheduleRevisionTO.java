package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Rikaz
 * 
 */
public class ScheduleRevisionTO implements Serializable {

	private static final long serialVersionUID = 8061021334918340870L;

	private String flightNumber;
	private Date startDate;
	private Date endDate;
	private Date updatedStartDate;
	private Date updatedEndDate;
	private Frequency updatedFrequency;
	private Frequency frequency;
	private boolean isLocalTimeMode;
	private boolean enableAttachDefaultAnciTemplate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getUpdatedStartDate() {
		return updatedStartDate;
	}

	public void setUpdatedStartDate(Date updatedStartDate) {
		this.updatedStartDate = updatedStartDate;
	}

	public Date getUpdatedEndDate() {
		return updatedEndDate;
	}

	public void setUpdatedEndDate(Date updatedEndDate) {
		this.updatedEndDate = updatedEndDate;
	}

	public Frequency getUpdatedFrequency() {
		return updatedFrequency;
	}

	public void setUpdatedFrequency(Frequency updatedFrequency) {
		this.updatedFrequency = updatedFrequency;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public boolean isLocalTimeMode() {
		return isLocalTimeMode;
	}

	public void setLocalTimeMode(boolean isLocalTimeMode) {
		this.isLocalTimeMode = isLocalTimeMode;
	}

	public boolean isEnableAttachDefaultAnciTemplate() {
		return enableAttachDefaultAnciTemplate;
	}

	public void setEnableAttachDefaultAnciTemplate(boolean enableAttachDefaultAnciTemplate) {
		this.enableAttachDefaultAnciTemplate = enableAttachDefaultAnciTemplate;
	}

}
