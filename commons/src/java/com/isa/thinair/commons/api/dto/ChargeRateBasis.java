package com.isa.thinair.commons.api.dto;

/**
 * @author eric
 * 
 */
public class ChargeRateBasis {

	public enum ChargeBasisEnum {

		VALUE("V", "Value", "V"), PERCENTAGE_OF_TOTAL_FARE("PTF", "Percentage Of Total Fare", "P"), PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES("PTFTS",
				"Percentage Of Total Fare Tax and Surcharges", "P");

		private ChargeBasisEnum(String code, String name, String type) {
			this.code = code;
			this.name = name;
			this.type = type;
		}

		private final String code;
		private final String name;
		private final String type;

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public boolean isPercentage() {
			return this.type.equals("P");
		}

		public static ChargeBasisEnum getChargeBasis(String code) {
			for (ChargeBasisEnum chargebasis : ChargeBasisEnum.values()) {
				if (chargebasis.getCode().equals(code)) {
					return chargebasis;
				}
			}
			return VALUE;
		}

	}

}
