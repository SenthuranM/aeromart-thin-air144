package com.isa.thinair.commons.api.dto;

/**
 * includes IBE related pax configurations
 * 
 * @author rumesh
 * 
 */
public class IBEPaxConfigDTO extends BasePaxConfigDTO {
	private static final long serialVersionUID = 8061021334918340870L;

	private boolean ibeVisibility;
	private boolean ibeMandatory;

	public boolean isIbeVisibility() {
		return ibeVisibility;
	}

	public void setIbeVisibility(boolean ibeVisibility) {
		this.ibeVisibility = ibeVisibility;
	}

	public boolean isIbeMandatory() {
		return ibeMandatory;
	}

	public void setIbeMandatory(boolean ibeMandatory) {
		this.ibeMandatory = ibeMandatory;
	}
}
