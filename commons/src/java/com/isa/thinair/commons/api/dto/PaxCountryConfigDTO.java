package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class PaxCountryConfigDTO implements Serializable, Cloneable {

	private String fieldName;

	private String paxCatCode;

	private String paxTypeCode;

	private String countryCode;

	private Boolean autoCheckinVisibility;

	private Boolean autoCheckinMandatory;

	private boolean mandatory;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getPaxCatCode() {
		return paxCatCode;
	}

	public void setPaxCatCode(String paxCatCode) {
		this.paxCatCode = paxCatCode;
	}

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the autoCheckinVisibility
	 */
	public Boolean getAutoCheckinVisibility() {
		return autoCheckinVisibility;
	}

	/**
	 * @param autoCheckinVisibility
	 *            the autoCheckinVisibility to set
	 */
	public void setAutoCheckinVisibility(Boolean autoCheckinVisibility) {
		this.autoCheckinVisibility = autoCheckinVisibility;
	}

	/**
	 * @return the autoCheckinMandatory
	 */
	public Boolean getAutoCheckinMandatory() {
		return autoCheckinMandatory;
	}

	/**
	 * @param autoCheckinMandatory
	 *            the autoCheckinMandatory to set
	 */
	public void setAutoCheckinMandatory(Boolean autoCheckinMandatory) {
		this.autoCheckinMandatory = autoCheckinMandatory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((paxCatCode == null) ? 0 : paxCatCode.hashCode());
		result = prime * result + ((paxTypeCode == null) ? 0 : paxTypeCode.hashCode());
		result = prime * result + ((autoCheckinVisibility == null) ? 0 : autoCheckinVisibility.hashCode());
		result = prime * result + ((autoCheckinMandatory == null) ? 0 : autoCheckinMandatory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaxCountryConfigDTO other = (PaxCountryConfigDTO) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (paxCatCode == null) {
			if (other.paxCatCode != null)
				return false;
		} else if (!paxCatCode.equals(other.paxCatCode))
			return false;
		if (paxTypeCode == null) {
			if (other.paxTypeCode != null)
				return false;
		} else if (!paxTypeCode.equals(other.paxTypeCode))
			return false;
		if (autoCheckinVisibility == null) {
			if (other.autoCheckinVisibility != null)
				return false;
		} else if (!autoCheckinVisibility.equals(other.autoCheckinVisibility))
			return false;
		if (autoCheckinMandatory == null) {
			if (other.autoCheckinMandatory != null)
				return false;
		} else if (!autoCheckinMandatory.equals(other.autoCheckinMandatory))
			return false;
		return true;
	}

	public Object clone() {
		PaxCountryConfigDTO clone = new PaxCountryConfigDTO();

		clone.setCountryCode(this.getCountryCode());
		clone.setFieldName(this.getFieldName());
		clone.setMandatory(this.isMandatory());
		clone.setPaxTypeCode(this.getPaxTypeCode());
		clone.setPaxCatCode(this.getPaxCatCode());
		clone.setAutoCheckinVisibility(this.getAutoCheckinVisibility());
		clone.setAutoCheckinMandatory(this.getAutoCheckinMandatory());

		return clone;
	}
}
