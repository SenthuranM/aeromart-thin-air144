package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class CountryLocation implements Serializable {

    private transient Object locationCode;

    private transient Object countryCode;

    public Object getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(Object locationCode) {
        this.locationCode = locationCode;
    }

    public Object getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Object countryCode) {
        this.countryCode = countryCode;
    }
}
