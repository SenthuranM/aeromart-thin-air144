package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class AirportMessageDisplayDTO implements Serializable {

	private static final long serialVersionUID = 2536783165233881607L;
	private Integer airportMsgId;
	private String airportCode;
	private String airportMessage;
	private String i18MessageKey;

	/**
	 * @return the airportMsgId
	 */
	public Integer getAirportMsgId() {
		return airportMsgId;
	}

	/**
	 * @param airportMsgId
	 *            the airportMsgId to set
	 */
	public void setAirportMsgId(Integer airportMsgId) {
		this.airportMsgId = airportMsgId;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the airportMessage
	 */
	public String getAirportMessage() {
		return airportMessage;
	}

	/**
	 * @param airportMessage
	 *            the airportMessage to set
	 */
	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public String getI18MessageKey() {
		return i18MessageKey;
	}

	public void setI18MessageKey(String i18MessageKey) {
		this.i18MessageKey = i18MessageKey;
	}

}
