package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.List;

/*
 * rajithaB : class for preparing objects to cache 
 */
public class OndPublishToCachingDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String originAndDestination;

	private List<String> operatingCarriers;

	public String getOriginAndDestination() {
		return originAndDestination;
	}

	public void setOriginAndDestination(String originAndDestination) {
		this.originAndDestination = originAndDestination;
	}

	public List<String> getOperatingCarriers() {
		return operatingCarriers;
	}

	public void setOperatingCarriers(List<String> operatingCarriers) {
		this.operatingCarriers = operatingCarriers;
	}

}
