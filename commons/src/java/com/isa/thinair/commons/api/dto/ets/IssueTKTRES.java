package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class IssueTKTRES implements Serializable {
	
    private MessageFunction messageFunction;
    
    private List<ReservationControlInformation> reservationControlInformation;
    
    private ErrorInformation errorInformation;
    
    private List<InteractiveFreeText> text;
    
    private List<TicketNumberDetails> tickets;
    
    private MessageHeader messageHeader;

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}

	public List<ReservationControlInformation> getReservationControlInformation() {
		return reservationControlInformation;
	}

	public ErrorInformation getErrorInformation() {
		return errorInformation;
	}

	public List<InteractiveFreeText> getText() {
		return text;
	}

	public List<TicketNumberDetails> getTickets() {
		return tickets;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}

	public void setReservationControlInformation(List<ReservationControlInformation> reservationControlInformation) {
		this.reservationControlInformation = reservationControlInformation;
	}

	public void setErrorInformation(ErrorInformation errorInformation) {
		this.errorInformation = errorInformation;
	}

	public void setText(List<InteractiveFreeText> text) {
		this.text = text;
	}

	public void setTickets(List<TicketNumberDetails> tickets) {
		this.tickets = tickets;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}
}
