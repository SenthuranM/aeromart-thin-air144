package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class EticketDTO implements Serializable {

	private static final long serialVersionUID = 1056879599780978338L;

	private String eticketNo;

	private int couponNo;

	public EticketDTO(String eticketNo, int couponNo) {
		super();
		this.eticketNo = eticketNo;
		this.couponNo = couponNo;
	}

	public String getEticketNo() {
		return eticketNo;
	}

	public void setEticketNo(String eticketNo) {
		this.eticketNo = eticketNo;
	}

	public int getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(int couponNo) {
		this.couponNo = couponNo;
	}

}
