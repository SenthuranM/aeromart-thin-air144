/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold Pax Type information
 * 
 * @author Dhanushka Ranatunga
 * @since 1.0
 */
public class PaxTypeTO implements Serializable {

	private static final long serialVersionUID = 7499656035447977495L;

	public static final String ADULT = "AD";
	public static final String CHILD = "CH";
	public static final String INFANT = "IN";
	public static final String PARENT = "PA";

	public static final String PAX_TYPE_ADULT_DISPLAY = "Adult(s)";
	public static final String PAX_TYPE_CHILD_DISPLAY = "Children";
	public static final String PAX_TYPE_INFANT_DISPLAY = "Infant(s)";

	/** Holds the Pax Typr Code */
	private String paxTypeCode;

	/** Holds the Pax desc */
	private String description;

	/** Holds the version Number */
	private long version;

	/** Holds the fare amount type */
	private String fareAmountType;

	/** Holds the fare amount */
	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the flag that determines whether fare is applicable or not */
	private boolean applyFare;

	/** Holds the flag that determines whether a modification charge is applicable or not */
	private boolean applyModCharge;

	/** Holds the flag that determines whether a cancellation charge is applicable or not */
	private boolean applyCnxCharge;

	/** Holds the flag that determines whether the fare is refundable or not */
	private boolean fareRefundable;

	/** Holds the flag that determines whether xbe bookings are allowed or not */
	private boolean xbeBookingsAllowed;

	/** Holds the cut off age in yeart */
	private int cutOffAgeInYears;

	/** Holds the no show charge amount */
	private BigDecimal noShowChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** JIRA -AARESAA:2715 (Lalanthi) */
	private int ageLowerBoundaryInMonths;
	private int ageLowerBoundaryInDays;

	/**
	 * @return the showCnxCharge
	 */
	public boolean isApplyCnxCharge() {
		return applyCnxCharge;
	}

	/**
	 * @param showCnxCharge
	 *            the showCnxCharge to set
	 */
	public void setApplyCnxCharge(boolean applyCnxCharge) {
		this.applyCnxCharge = applyCnxCharge;
	}

	/**
	 * @return the applyFare
	 */
	public boolean isApplyFare() {
		return applyFare;
	}

	/**
	 * @param applyFare
	 *            the applyFare to set
	 */
	public void setApplyFare(boolean applyFare) {
		this.applyFare = applyFare;
	}

	/**
	 * @return the showModCharge
	 */
	public boolean isApplyModCharge() {
		return applyModCharge;
	}

	/**
	 * @param showModCharge
	 *            the showModCharge to set
	 */
	public void setApplyModCharge(boolean applyModCharge) {
		this.applyModCharge = applyModCharge;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return the fareAmountType
	 */
	public String getFareAmountType() {
		return fareAmountType;
	}

	/**
	 * @param fareAmountType
	 *            the fareAmountType to set
	 */
	public void setFareAmountType(String fareAmountType) {
		this.fareAmountType = fareAmountType;
	}

	/**
	 * @return the fareRefundable
	 */
	public boolean isFareRefundable() {
		return fareRefundable;
	}

	/**
	 * @param fareRefundable
	 *            the fareRefundable to set
	 */
	public void setFareRefundable(boolean fareRefundable) {
		this.fareRefundable = fareRefundable;
	}

	/**
	 * @return the paxTypeCode
	 */
	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	/**
	 * @param paxTypeCode
	 *            the paxTypeCode to set
	 */
	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the cutOffAgeInYears
	 */
	public int getCutOffAgeInYears() {
		return cutOffAgeInYears;
	}

	/**
	 * @param cutOffAgeInYears
	 *            the cutOffAgeInYears to set
	 */
	public void setCutOffAgeInYears(int cutOffAgeInYears) {
		this.cutOffAgeInYears = cutOffAgeInYears;
	}

	/**
	 * @return the xbeBookingsAllowed
	 */
	public boolean isXbeBookingsAllowed() {
		return xbeBookingsAllowed;
	}

	/**
	 * @param xbeBookingsAllowed
	 *            the xbeBookingsAllowed to set
	 */
	public void setXbeBookingsAllowed(boolean xbeBookingsAllowed) {
		this.xbeBookingsAllowed = xbeBookingsAllowed;
	}

	/**
	 * @return Returns the noShowChargeAmount.
	 */
	public BigDecimal getNoShowChargeAmount() {
		return noShowChargeAmount;
	}

	/**
	 * @param noShowChargeAmount
	 *            The noShowChargeAmount to set.
	 */
	public void setNoShowChargeAmount(BigDecimal noShowChargeAmount) {
		this.noShowChargeAmount = noShowChargeAmount;
	}

	/**
	 * Returns lower age boundary in months
	 * 
	 * @return
	 */
	public int getAgeLowerBoundaryInMonths() {
		return ageLowerBoundaryInMonths;
	}

	/**
	 * Sets lower age boundary in months
	 * 
	 * @param ageLowerBoundaryInMonths
	 */
	public void setAgeLowerBoundaryInMonths(int ageLowerBoundaryInMonths) {
		this.ageLowerBoundaryInMonths = ageLowerBoundaryInMonths;
	}

	/**
	 * Returns lower age boundary in days
	 * 
	 * @return
	 */
	public int getAgeLowerBoundaryInDays() {
		return ageLowerBoundaryInDays;
	}

	/**
	 * Sets lower age boundary in days
	 * 
	 * @param ageLowerBoundaryInDays
	 */
	public void setAgeLowerBoundaryInDays(int ageLowerBoundaryInDays) {
		this.ageLowerBoundaryInDays = ageLowerBoundaryInDays;
	}

	/**
	 * Returns the number of passenger types
	 * 
	 * @return
	 */
	public static int getNoOfPassengerTypes() {
		Collection<String> colPaxTypes = new ArrayList<String>();
		colPaxTypes.add(PaxTypeTO.ADULT);
		colPaxTypes.add(PaxTypeTO.CHILD);
		colPaxTypes.add(PaxTypeTO.INFANT);

		return colPaxTypes.size();
	}

	public static String getPaxTypeDisplayForAAPaxType(String aaPaxType) {
		if (aaPaxType.equals(ADULT)) {
			return PAX_TYPE_ADULT_DISPLAY;
		} else if (aaPaxType.equals(CHILD)) {
			return PAX_TYPE_CHILD_DISPLAY;
		} else if (aaPaxType.equals(INFANT)) {
			return PAX_TYPE_INFANT_DISPLAY;
		}
		return null;
	}
}
