package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SSRChargesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2603103180270678233L;

	private Integer chargeId;

	private Integer maxAdultPax;

	private Integer minAdultPax;

	private Integer maxChildPax;

	private Integer minChildPax;

	private Integer maxInfantPax;

	private Integer minInfantPax;

	private BigDecimal chargeAmount;

	private String status;

	private String cabinClassCode;

	private String description;

	private Integer maxTotalPax;

	private Integer minTotalPax;

	private String commissionIn;

	private BigDecimal commissionValue;

	private Integer ssrId;

	private String ssrChargeCode;

	private String airportCode;

	private String applicablityType;

	private BigDecimal adultAmount;

	private BigDecimal childAmount;

	private BigDecimal infantAmount;

	private BigDecimal reservationAmount;

	private String ssrInfoCode;

	private long version = -1;

	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;

	private String adminVisibility;

	private String chargeLocalCurrencyCode;
	private BigDecimal localCurrChargeAmount;

	private BigDecimal localCurrAdultAmount;

	private BigDecimal localCurrChildAmount;

	private BigDecimal localCurrInfantAmount;

	private BigDecimal localCurrReservationAmount;

	public String getAdminVisibility() {
		return adminVisibility;
	}

	public void setAdminVisibility(String adminVisibility) {
		this.adminVisibility = adminVisibility;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSsrInfoCode() {
		return ssrInfoCode;
	}

	public void setSsrInfoCode(String ssrInfoCode) {
		this.ssrInfoCode = ssrInfoCode;
	}

	public String getSsrInfoDescription() {
		return ssrInfoDescription;
	}

	public void setSsrInfoDescription(String ssrInfoDescription) {
		this.ssrInfoDescription = ssrInfoDescription;
	}

	public String getSsrInfoSubCategory() {
		return ssrInfoSubCategory;
	}

	public void setSsrInfoSubCategory(String ssrInfoSubCategory) {
		this.ssrInfoSubCategory = ssrInfoSubCategory;
	}

	public String getSsrInfoCategory() {
		return ssrInfoCategory;
	}

	public void setSsrInfoCategory(String ssrInfoCategory) {
		this.ssrInfoCategory = ssrInfoCategory;
	}

	public Integer getSsrInfoSubCatId() {
		return ssrInfoSubCatId;
	}

	public void setSsrInfoSubCatId(Integer ssrInfoSubCatId) {
		this.ssrInfoSubCatId = ssrInfoSubCatId;
	}

	public Integer getSsrInfoCatId() {
		return ssrInfoCatId;
	}

	public void setSsrInfoCatId(Integer ssrInfoCatId) {
		this.ssrInfoCatId = ssrInfoCatId;
	}

	private String ssrInfoDescription;

	// private String ssrInfoSubCatId;
	private Integer ssrInfoSubCatId;

	private String ssrInfoSubCategory;

	// private String ssrInfoCatId;
	private Integer ssrInfoCatId;

	private String ssrInfoCategory;

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	public Integer getMaxAdultPax() {
		return maxAdultPax;
	}

	public void setMaxAdultPax(Integer maxAdultPax) {
		this.maxAdultPax = maxAdultPax;
	}

	public Integer getMinAdultPax() {
		return minAdultPax;
	}

	public void setMinAdultPax(Integer minAdultPax) {
		this.minAdultPax = minAdultPax;
	}

	public Integer getMaxChildPax() {
		return maxChildPax;
	}

	public void setMaxChildPax(Integer maxChildPax) {
		this.maxChildPax = maxChildPax;
	}

	public Integer getMinChildPax() {
		return minChildPax;
	}

	public void setMinChildPax(Integer minChildPax) {
		this.minChildPax = minChildPax;
	}

	public Integer getMaxInfantPax() {
		return maxInfantPax;
	}

	public void setMaxInfantPax(Integer maxInfantPax) {
		this.maxInfantPax = maxInfantPax;
	}

	public Integer getMinInfantPax() {
		return minInfantPax;
	}

	public void setMinInfantPax(Integer minInfantPax) {
		this.minInfantPax = minInfantPax;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxTotalPax() {
		return maxTotalPax;
	}

	public void setMaxTotalPax(Integer maxTotalPax) {
		this.maxTotalPax = maxTotalPax;
	}

	public Integer getMinTotalPax() {
		return minTotalPax;
	}

	public void setMinTotalPax(Integer minTotalPax) {
		this.minTotalPax = minTotalPax;
	}

	public String getCommissionIn() {
		return commissionIn;
	}

	public void setCommissionIn(String commissionIn) {
		this.commissionIn = commissionIn;
	}

	public BigDecimal getCommissionValue() {
		return commissionValue;
	}

	public void setCommissionValue(BigDecimal commissionValue) {
		this.commissionValue = commissionValue;
	}

	public Integer getSsrId() {
		return ssrId;
	}

	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	public String getSsrChargeCode() {
		return ssrChargeCode;
	}

	public void setSsrChargeCode(String ssrChargeCode) {
		this.ssrChargeCode = ssrChargeCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getApplicablityType() {
		return applicablityType;
	}

	public void setApplicablityType(String applicablityType) {
		this.applicablityType = applicablityType;
	}

	public BigDecimal getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	public BigDecimal getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	public BigDecimal getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}

	public BigDecimal getReservationAmount() {
		return reservationAmount;
	}

	public void setReservationAmount(BigDecimal reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	public String getChargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setChargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}

	public BigDecimal getLocalCurrChargeAmount() {
		return localCurrChargeAmount;
	}

	public void setLocalCurrChargeAmount(BigDecimal localCurrChargeAmount) {
		this.localCurrChargeAmount = localCurrChargeAmount;
	}

	public BigDecimal getLocalCurrAdultAmount() {
		return localCurrAdultAmount;
	}

	public void setLocalCurrAdultAmount(BigDecimal localCurrAdultAmount) {
		this.localCurrAdultAmount = localCurrAdultAmount;
	}

	public BigDecimal getLocalCurrChildAmount() {
		return localCurrChildAmount;
	}

	public void setLocalCurrChildAmount(BigDecimal localCurrChildAmount) {
		this.localCurrChildAmount = localCurrChildAmount;
	}

	public BigDecimal getLocalCurrInfantAmount() {
		return localCurrInfantAmount;
	}

	public void setLocalCurrInfantAmount(BigDecimal localCurrInfantAmount) {
		this.localCurrInfantAmount = localCurrInfantAmount;
	}

	public BigDecimal getLocalCurrReservationAmount() {
		return localCurrReservationAmount;
	}

	public void setLocalCurrReservationAmount(BigDecimal localCurrReservationAmount) {
		this.localCurrReservationAmount = localCurrReservationAmount;
	}
}
