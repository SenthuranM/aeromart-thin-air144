package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class CabinClassDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cabinClassCode;

	private Integer rank;

	public CabinClassDTO(String cabinClassCode, Integer rank) {
		this.cabinClassCode = cabinClassCode;
		this.rank = rank;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}
