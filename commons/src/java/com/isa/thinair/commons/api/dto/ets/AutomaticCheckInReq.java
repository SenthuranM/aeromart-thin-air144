package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

/**
 * 
 * @author Panchatcharam.s
 *
 */
public class AutomaticCheckInReq implements Serializable {
	private static final long serialVersionUID = -5440114187241124029L;
	private String pnr;
	private String flightNumber;
	private String checkinStatus;
	private String checkinSeat;
	private String title;
	private String firstName;
	private String lastName;
	private String departureDate;
	private String departureAirportCode;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getCheckinStatus() {
		return checkinStatus;
	}

	public void setCheckinStatus(String checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	public String getCheckinSeat() {
		return checkinSeat;
	}

	public void setCheckinSeat(String checkinSeat) {
		this.checkinSeat = checkinSeat;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

}