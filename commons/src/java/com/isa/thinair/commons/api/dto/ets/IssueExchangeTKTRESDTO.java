package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class IssueExchangeTKTRESDTO implements Serializable {
    
	private IssueTKTRES payload;
    
    private RequestIdentity requestIdentity;
    
    private ServiceError error;
    
    private List<ServiceWarning> serviceWarnings;

	public IssueTKTRES getPayload() {
		return payload;
	}

	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}

	public ServiceError getError() {
		return error;
	}

	public List<ServiceWarning> getServiceWarnings() {
		return serviceWarnings;
	}

	public void setPayload(IssueTKTRES payload) {
		this.payload = payload;
	}

	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}

	public void setError(ServiceError error) {
		this.error = error;
	}

	public void setServiceWarnings(List<ServiceWarning> serviceWarnings) {
		this.serviceWarnings = serviceWarnings;
	}
}
