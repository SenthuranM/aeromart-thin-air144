/**
 * 
 */
package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Janaka Padukka
 * 
 */
public class OndPublishCurrencyDTO implements Serializable {

	private String currencyCode;

	private List<LanguageDescriptionDTO> languageDescriptions;

	public void addLanguageDescription(LanguageDescriptionDTO langDesc) {

		if (languageDescriptions == null) {
			languageDescriptions = new ArrayList<LanguageDescriptionDTO>();
		}
		languageDescriptions.add(langDesc);

	}

	public List<LanguageDescriptionDTO> getLanguageDescriptions() {
		return languageDescriptions;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public void setLanguageDescriptions(
			List<LanguageDescriptionDTO> languageDescriptions) {
		this.languageDescriptions = languageDescriptions;
	}

}
