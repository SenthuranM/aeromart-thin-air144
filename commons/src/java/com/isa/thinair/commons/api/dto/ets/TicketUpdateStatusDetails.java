package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

/**
 * 
 * @author rajitha
 *
 */
public class TicketUpdateStatusDetails implements Serializable {
	
	private String ticketNumber;
	private Integer couponNumber;
	private String targetStatus;
	private boolean success;
	private Integer errorCode;
	
	public String getTicketNumber() {
		return ticketNumber;
	}
	
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	public Integer getCouponNumber() {
		return couponNumber;
	}
	
	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}
	
	public String getTargetStatus() {
		return targetStatus;
	}
	
	public void setTargetStatus(String targetStatus) {
		this.targetStatus = targetStatus;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean isSuccess) {
		this.success = isSuccess;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	

	
}
