package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class CouponStatusTKCREQRequestDTO implements Serializable{

	private Header header;
	
	private CouponStatusTKCREQMessage message;
	
	private RequestIdentity requestIdentity;
	
	public Header getHeader() {
		return header;
	}
	

	public void setHeader(Header header) {
		this.header = header;
	}
	

	public CouponStatusTKCREQMessage getMessage() {
		return message;
	}
	

	public void setMessage(CouponStatusTKCREQMessage message) {
		this.message = message;
	}
	

	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}
	

	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}
	
}
