package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * Holds configurations related to WS contact details in create & modify reservations
 * 
 * @author rumesh
 * 
 */

public class WSContactConfigDTO extends BaseContactConfigDTO implements Serializable {

	private static final long serialVersionUID = 4996045232931039852L;

	private boolean wsVisibility;

	public boolean isWsVisibility() {
		return wsVisibility;
	}

	public void setWsVisibility(boolean wsVisibility) {
		this.wsVisibility = wsVisibility;
	}
}
