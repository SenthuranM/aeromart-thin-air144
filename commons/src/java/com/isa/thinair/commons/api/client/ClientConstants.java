package com.isa.thinair.commons.api.client;

public class ClientConstants {

	public static final String CONFIG_ROOT_DIR_PATH_ENV_VAR_NAME = "repository.home";

	public static String getConfigRootDirPath() {
		return System.getProperty(CONFIG_ROOT_DIR_PATH_ENV_VAR_NAME);
	}
}
