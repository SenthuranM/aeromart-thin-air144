package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class TransitionTo<T extends Serializable> implements Serializable {
	private T oldVal;
	private T newVal;

	public T getOldVal() {
		return oldVal;
	}

	public void setOldVal(T oldVal) {
		this.oldVal = oldVal;
	}

	public T getNewVal() {
		return newVal;
	}

	public void setNewVal(T newVal) {
		this.newVal = newVal;
	}
}
