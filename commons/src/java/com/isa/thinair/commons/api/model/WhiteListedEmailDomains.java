package com.isa.thinair.commons.api.model;

/**
 * @author nafly
 * @hibernate.class table = "T_WHITE_LISTED_EMAIL_DOMAINS"
 * 
 **/

public class WhiteListedEmailDomains {

	public static final String STATUS_ACT = "ACT";

	private String domainName;
	private String status;

	/**
	 * @return the domainName
	 * @hibernate.id column = "DOMAIN_NAME" generator-class = "assigned"
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @param domainName
	 *            the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
