package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class BaseRs implements Serializable{
	private boolean success;
	private ServiceError serviceError;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ServiceError getServiceError() {
		return serviceError;
	}

	public void setServiceError(ServiceError serviceError) {
		this.serviceError = serviceError;
	}
}
