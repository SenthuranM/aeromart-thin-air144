package com.isa.thinair.commons.api.constants;

public class PriviledgeConstants {

	public static final String PRIVI_VIEW_FULL_SEAT_INVENT_RPT = "rpt.pln.icf.restrict";

	public static final String OVERRIDE_CHARGES = "xbe.res.alt.charge";

	public static final String ACCEPT_CASH_PAYMNETS = "xbe.res.make.pay.cash";
	
	public static final String ACCEPT_OFFLINE_PAYMNETS = "xbe.res.make.pay.offline.payment";

	public static final String ACCEPT_ONACCOUNT_PAYMNETS = "xbe.res.make.pay.acc";

	public static final String ACCEPT_ONACCOUNT_PAYMNETS_ANY = "xbe.res.make.pay.acc.any";

	public static final String ACCEPT_OPT_CARRIER_ONACCOUNT_PAYMNETS_ANY = "xbe.res.make.pay.opcarrier.acc.any";

	public static final Object ACCEPT_ONACCOUNT_PAYMNETS_REPORTING = "xbe.res.make.pay.acc.rpt";

	public static final String ACCEPT_ONACCOUNT_REFUND = "xbe.res.alt.acc.ref.acc";

	public static final Object ACCEPT_ONACCOUNT_REFUND_REPORTING = "xbe.res.alt.acc.ref.acc.rpt";

	public static final String ACCEPT_ONACCOUNT_REFUND_ANY = "xbe.res.alt.acc.ref.acc.any";

	public static final String ACCEPT_OPT_CARREIR_ONACCOUNT_REFUND_ANY = "xbe.res.alt.acc.ref.opcarrier.acc.any";

	public static final String ACCEPT_CREDIT_CARD_PAYMNETS = "xbe.res.make.pay.card";

	public static final String ACCEPT_VOUCHER_PAYMNETS = "xbe.res.make.pay.voucher";

	public static final String ACCEPT_CREDIT_PAYMNETS = "xbe.res.make.pay.credit";

	public static final String ACCEPT_CREDIT_PAYMNETS_ANY = "xbe.res.make.pay.credit.any";

	public static final String ACCEPT_PARTIAL_PAYMNETS = "xbe.res.make.pay.pax";

	public static final String PRINT_ITENERARY_FOR_PARTIAL_PAYMNETS = "xbe.res.make.pay.pax.print";

	public static final String RES_ACCEPT_NO_PAY = "xbe.res.make.confirm.nopay";

	public static final String PAX_ADJUST_CREDIT = "xbe.res.alt.acc.adj";

	public static final String PAX_ADJUST_CREDIT_ANY = "xbe.res.alt.acc.adj.any";

	public static final String PAX_ADJUST_CREDIT_REFUND = "xbe.res.alt.acc.adj.refund";

	public static final String PAX_ADJUST_CREDIT_NONREFUND = "xbe.res.alt.acc.adj.nonrefund";

	public static final String PAX_REFUND = "xbe.res.alt.acc.ref";

	public static final String ALT_RES_ADD_SEGMENT = "xbe.res.alt.seg.add";

	// public static final String ALT_RES_MODIFY_SEGMENT = "xbe.res.alt.seg.mod";
	/* segment modification */
	public static final String ALT_RES_MODIFY_SEGMENT_DATE = "xbe.res.alt.seg.mod.date";

	public static final String ALT_RES_MODIFY_SEGMENT_ROUTE = "xbe.res.alt.seg.mod.route";

	public static final String ALT_RES_CANCEL_SEGMENT = "xbe.res.alt.seg.cnx";

	public static final String ALT_RES_TRANSFER_SEGMENT = "xbe.res.alt.seg.tnx";

	public static final String ALT_RES_CLEAR_ALERTS = "xbe.alert.clear";

	public static final String ALT_RES_VIEW_PAX_ACC = "xbe.res.alt.acc";

	public static final String MAKE_RES_ALLFLIGHTS = "xbe.res.make.allflights";

	public static final String ALLOW_SEATBLOCKING = "xbe.res.make.seat.block";

	public static final String ALLOW_VIEW_AVAILABLESEATS = "xbe.res.make.view.avl";

	public static final String ALLOW_ADD_FARE_DISCOUNTS = "xbe.res.make.addFareDisc";

	public static final String OVERRIDE_FARE_DISCOUNT = "xbe.res.make.override.fare.disc";

	public static final String CHANGE_CURRENCY = "xbe.res.make.currency";

	public static final String ALT_RES_OLD = "xbe.res.alt.old";

	public static final String CANCEL_RES = "xbe.res.alt.cnx";

	public static final String CHANGE_CONTACT_DETAILS = "xbe.res.alt.contact";

	public static final String RES_ADD_INFANT = "xbe.res.alt.pax.inf";

	public static final String RES_REMOVE_PAX = "xbe.res.alt.pax.del";
	
	public static final String RES_BULK_TKT_REMOVE_PAX = "xbe.res.alt.pax.bulk.ticket.remove";

	public static final String RES_SPLIT = "xbe.res.alt.pax.split";
	
	public static final String RES_BULK_TKT_SPLIT = "xbe.res.alt.pax.bulk.ticket.split";

	public static final String RES_UPDATE = "xbe.res.alt.edit";

	public static final String RES_TRANSFER = "xbe.res.make.owner";

	public static final String RES_ANYCARRIER_TRANSFER = "xbe.res.make.anycarrier.owner";

	public static final String RES_MAKE_ONHOLD = "xbe.res.make.onhold";

	public static final String RES_EXTEND = "xbe.res.alt.extend";

	public static final String ALT_EXTEND_BUFFER_TIME = "xbe.res.alt.modify.buffer";

	public static final String ANY_PNR_SEARCH = "xbe.res.alt.find.any";
	
	public static final String WEB_ORIGINATED_OWNED_PNR_SEARCH = "xbe.res.alt.find.web.owned";

	public static final String ANY_GSA_PNR_SEARCH = "xbe.res.alt.find.gsa.any";

	public static final String ALL_REPORTING_AGENT_PNR_SEARCH = "xbe.res.alt.find.report.agent.all";

	public static final String REPORTING_AGENT_PNR_SEARCH = "xbe.res.alt.find.report.agent";

	public static final String ALT_RES_NO_PAY = "xbe.res.alt.confirm.nopay";

	public static final String PRIVI_PAX_REFUND_CCARD = "xbe.res.alt.acc.ref.card";

	public static final String PRIVI_PAX_REFUND_CCARD_ANY = "xbe.res.alt.acc.ref.card.any";

	public static final String PRIVI_NAME_CHANGE = "xbe.res.alt.name";

	public static final String PRIVI_NAME_CHANGE_FARE_ANY = "xbe.res.make.alterfare.any";

	public static final String MAKE_STANDBY_BOOKING = "xbe.res.make.standby.booking";

	public static final String MAKE_OVER_BOOKING = "xbe.res.make.over.booking";

	public static final String MAKE_WAITLISTING_BOOKING = "xbe.res.make.waitlisting.booking";

	public static final String PRIVI_SSR_CHANGE = "xbe.res.alt.ssr";

	public static final String PRIVI_SSR_ADD = "xbe.res.add.ssr";

	public static final String MAKE_RES_ITN_SELECT_PAX = "xbe.res.itn.selectpax";

	public static final String PRIVI_OVERRIDE_AGENT_FARE_ONLY = "xbe.res.make.alterfare.agfare";

	public static final String PRIVI_OVERRIDE_VISIBLE_FARE = "xbe.res.make.alterfare";

	public static final String PRIVI_ADD_SEATS_IN_CUTOVER = "xbe.res.add.seatcutover";

	public static final String PRIVI_CHILD_ONLY_BOOKINGS = "xbe.res.make.child.only";

	public static final String PRIVI_VIEW_ANY_FLIGHT_MANIFEST = "xbe.res.stat.any";

	public static final String PRIVI_VIEW_OTHER_FLIGHT_MANIFEST = "xbe.res.stat.other";
	// ext payments
	public static final String PRIVI_EXT_PAY_VIEW = "xbe.res.alt.ext.pay";

	public static final String PRIVI_EXT_PAY_VIEW_ALL = "xbe.res.alt.ext.pay.viewall";

	public static final String PRIVI_EXT_PAY_SYNC = "xbe.res.alt.ext.pay.sync";

	public static final String PRIVI_EXT_PAY_UPDATE = "xbe.res.alt.ext.pay.update";

	public static final String PRIVI_SEAT_UPDATE = "xbe.res.alt.seat.edit";

	public static final String ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME = "xbe.res.create.bookings.after.cutofftime";

	public static final String PRIVI_ALLOW_MODIFY_BOOKINGS_TO_LOW_PRIORITY_COS = "xbe.res.allow.modify.bookings.low.priority.cos";
	
	public static final String ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME = "xbe.res.make.over.booking.after.cutofftime";

	// view canceled itinerary
	public static final String PRIVI_VIEW_CNX_ITEN = "xbe.res.alt.view.cnx.itn";
	// make onhold within buffer time privilege
	public static final String PRIVI_MAKE_ONHOLD_WITHIN_BUFFER = "xbe.res.make.onhold.buffer";

	// privileges for access reservations
	public static final String PRIVI_ACC_XBE_RES = "xbe.res.allow.xbe";
	public static final String PRIVI_ACC_IBE_RES = "xbe.res.allow.ibe";
	public static final String PRIVI_ACC_HOLIDAY_RES = "xbe.res.allow.holiday";
	public static final String PRIVI_ACC_ALL_RES = "xbe.res.allow.all";
	public static final String PRIVI_ACC_DNATA_RES = "xbe.res.allow.dnata";
	public static final String PRIVI_ACC_INTERLINED_RES = "xbe.res.allow.interlined";
	public static final String PRIVI_ACC_GDS_RES = "xbe.res.allow.gds";
	public static final String PRIVI_ACC_LCC_RES = "xbe.res.allow.lcc";
	public static final String PRIVI_ACC_IOS_RES = "xbe.res.allow.ios";
	public static final String PRIVI_ACC_ANDROID_RES = "xbe.res.allow.android";

	// privilege to add infant
	public static final String PRIVI_ADD_INFANT = "xbe.res.add.inf";

	public static final String PRIVI_VIEW_CREDIT_INFO = "xbe.res.acc.view.pax.credit";

	public static final String ALLOW_VIEW_ANY_ALERT = "xbe.alert.view.any";

	public static final String ALLOW_VIEW_OWN_ALERT = "xbe.alert.view";

	public static final String ALLOW_VIEW_REP_AGENT_ALERT = "xbe.alert.view.reporing.agent";

	// privilege to search alerts from any airline.
	public static final String ALLOW_VIEW_ANY_AIRLINE_ALERT = "xbe.alert.view.search.all";

	public static final String PRIVI_TBA_CHANGE = "xbe.res.alt.tba";
	public static final String PRIVI_ENFORCE_OVERRIDE = "xbe.res.alt.modond.farecheck.override";

	public static final String PRIVI_INSURANCE = "xbe.insurance";

	public static final String PRIVI_OVERRIDE_ONHOLD = "xbe.res.override.onhold";

	public static final String PRIVI_INSURANCE_OHD = "xbe.insurance.onhold";

	public static final String PRIVI_MODIFY_SSR_WITHIN_BUFFER = "xbe.res.alt.modify.buffer.ssr";

	public static final String LCC_RES_ALLOW_ANY_OND_MODIFY = "xbe.res.lcc.allow.any.ond.modify";

	public static final String PRIVI_SHOW_REVENUE_SEAT_INVENT_RPT = "rpt.pln.icf.show.revenue";

	// Booking type wise privileges
	// create privileges
	public static final String PRIVI_CREATE_STANDARD_BOOKING = "xbe.res.create.std.booking";
	public static final String PRIVI_CREATE_CHARTER_BOOKING = "xbe.res.create.cht.booking";
	public static final String PRIVI_CREATE_HR_BOOKING = "xbe.res.create.hr.booking";

	// View privileges
	public static final String PRIVI_SEARCH_ANY_STANDARD_BOOKING = "xbe.res.find.std.booking";
	public static final String PRIVI_SEARCH_ANY_CHARTER_BOOKING = "xbe.res.find.cht.booking";
	public static final String PRIVI_SEARCH_ANY_HR_BOOKING = "xbe.res.find.hr.booking";

	// Baggage Privileges
	public static final String PRIVI_BAGGAGE_CHARGE_OVERRIDE = "xbe.res.baggage.charge.override";
	public static final String PRIVI_BAGGAGE_SHOW = "xbe.res.baggage.show";
	
	public static final String PRIVI_MANUALLY_SEND_PNL_ADL = "tool.pnl.manually.send";
	public static final String PRIVI_MANUALLY_SEND_PAL_CAL = "xbe.tool.pal.manually.send";

	public static final String PRIVI_ALLOW_CREATE_USER_ALERT = "xbe.res.create.alert";

	public static final String ALLOW_NOSHOW_REFUNDABLE_TAX_CALC = "xbe.res.calc.tax.noshow";

	public static final String ALLOW_GROUP_REFUND = "xbe.res.alt.acc.ref.multiple";
	public static final String ALLOW_TERMS_N_CONDITIONS_EDITING = "tool.tnc.edit";
	public static final String ALLOW_NO_SHOW_TAX_REVERSE = "xbe.res.alt.acc.noshow.tax.reverse";

	public static final String ALLOW_ANCI_OFFER_MANAGEMENT = "sys.mas.mgmt.promo.ancioffer";

	public static final String ALLOW_VIEW_BUNDLED_FARE_CONFIG = "sys.mas.mgmt.promo.bundledfares";
	public static final String ALLOW_ADD_BUNLED_FARE_CONFIG = "sys.mas.mgmt.promo.bundledfares.add";
	public static final String ALLOW_EDIT_BUNLED_FARE_CONFIG = "sys.mas.mgmt.promo.bundledfares.edit";
	public static final String ALLOW_DLEETE_BUNLED_FARE_CONFIG = "sys.mas.mgmt.promo.bundledfares.delete";	
	public static final String SHOW_FLOWN_FLIGHTS_CLS_BC = "xbe.res.alt.view.closebc.flown";

	// User notes related privileges.
	public static final String ADD_USER_NOTE = "xbe.res.usernote.add ";

	public static final String CLASSIFY_USER_NOTE = "xbe.res.usernote.classify";
	
	// Voucher terms and conditions related privileges.
	public static final String ALLOW_VOUCHER_TERMS_N_CONDITIONS_EDITING = "sys.mas.mgmt.promo.voucher.tnc.edit";
	
	public static final String ALLOW_AEROMART_PAY_OPERATIONS =  "xbe.res.alt.aeromartpay";
	
	public static final String ALLOW_SHOW_ALL_BOOKINGS_IN_FLIGHT_MANIFEST = "ta.maint.access.all.bookings";

}
