package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * @author chethiya
 *
 */
public class VoucherPaymentDTO implements Serializable{

	private static final long serialVersionUID = 4912023721654710616L;

	private String paymentMethod;

	private String cardType;

	private String cardNumber;

	private String cardHolderName;

	private String cardExpiry;

	private String cardCVV;

	private String amount;

	private String amountLocal;

	private String cardTxnFeeLocal;

	private String voucherID;

	private AppIndicatorEnum appIndicator;

	private int tnxModeCode;

	private int tempPaymentID;

	private int agentTransactionID;

	private int nominalCode;

	private String agentCode;

	private int ipgId;
	
	private String userIp;

	public VoucherPaymentDTO clone() {
		VoucherPaymentDTO cloneObject = new VoucherPaymentDTO();
		cloneObject.setPaymentMethod(this.paymentMethod);
		cloneObject.setCardType(this.cardType);
		cloneObject.setCardNumber(this.cardNumber);
		cloneObject.setCardHolderName(this.cardHolderName);
		cloneObject.setCardExpiry(this.cardExpiry);
		cloneObject.setCardCVV(this.cardCVV);
		cloneObject.setAmount(this.amount);
		cloneObject.setAmountLocal(this.amountLocal);
		cloneObject.setCardTxnFeeLocal(this.cardTxnFeeLocal);
		cloneObject.setVoucherID(this.voucherID);
		cloneObject.setAppIndicator(this.appIndicator);
		cloneObject.setTnxModeCode(this.tnxModeCode);
		cloneObject.setTempPaymentID(this.tempPaymentID);
		cloneObject.setAgentTransactionID(this.agentTransactionID);
		cloneObject.setNominalCode(this.nominalCode);
		cloneObject.setAgentCode(this.agentCode);
		cloneObject.setIpgId(this.ipgId);
		cloneObject.setUserIp(this.userIp);
		return cloneObject;
	}

	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *            the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 *            the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName
	 *            the cardHolderName to set
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * @return the cardExpiry
	 */
	public String getCardExpiry() {
		return cardExpiry;
	}

	/**
	 * @param cardExpiry
	 *            the cardExpiry to set
	 */
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	/**
	 * @return the cardCVV
	 */
	public String getCardCVV() {
		return cardCVV;
	}

	/**
	 * @param cardCVV
	 *            the cardCVV to set
	 */
	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	/**
	 * @return the cardAmountLocal
	 */
	public String getAmountLocal() {
		return amountLocal;
	}

	/**
	 * @param amountLocal
	 *            the cardAmountLocal to set
	 */
	public void setAmountLocal(String amountLocal) {
		this.amountLocal = amountLocal;
	}

	/**
	 * @return the cardTxnFeeLocal
	 */
	public String getCardTxnFeeLocal() {
		return cardTxnFeeLocal;
	}

	/**
	 * @param cardTxnFeeLocal
	 *            the cardTxnFeeLocal to set
	 */
	public void setCardTxnFeeLocal(String cardTxnFeeLocal) {
		this.cardTxnFeeLocal = cardTxnFeeLocal;
	}

	/**
	 * @return the voucherID
	 */
	public String getVoucherID() {
		return voucherID;
	}

	/**
	 * @param voucherID
	 *            the voucherID to set
	 */
	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	/**
	 * @return the appIndicator
	 */
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 *            the appIndicator to set
	 */
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return the tempPaymentID
	 */
	public int getTempPaymentID() {
		return tempPaymentID;
	}

	/**
	 * @param tempPaymentID
	 *            the tempPaymentID to set
	 */
	public void setTempPaymentID(int tempPaymentID) {
		this.tempPaymentID = tempPaymentID;
	}

	/**
	 * @return the nominalCode
	 */
	public int getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return the userIp
	 */
	public String getUserIp() {
		return userIp;
	}

	/**
	 * @param userIp
	 *            the userIp to set
	 */
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the agentTransactionID
	 */
	public int getAgentTransactionID() {
		return agentTransactionID;
	}

	/**
	 * @param agentTransactionID
	 *            the agentTransactionID to set
	 */
	public void setAgentTransactionID(int agentTransactionID) {
		this.agentTransactionID = agentTransactionID;
	}

	/**
	 * @return the ipgId
	 */
	public int getIpgId() {
		return ipgId;
	}

	/**
	 * @param ipgId the ipgId to set
	 */
	public void setIpgId(int ipgId) {
		this.ipgId = ipgId;
	}

	/**
	 * @return the tnxModeCode
	 */
	public int getTnxModeCode() {
		return tnxModeCode;
	}

	/**
	 * @param tnxModeCode the tnxModeCode to set
	 */
	public void setTnxModeCode(int tnxModeCode) {
		this.tnxModeCode = tnxModeCode;
	}

}
