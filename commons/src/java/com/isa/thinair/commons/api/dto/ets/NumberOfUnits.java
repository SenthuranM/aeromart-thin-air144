package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class NumberOfUnits implements Serializable {
	
    private String quantity;

    private String qualifier;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }
}
