package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class RefundTKTRESDTO implements Serializable {

	private MessageFunction messageFunction;

	private OriginatorInformation originatorInformation;

	private TicketingAgentInformation ticketingAgentInformation;

	private List<ReservationControlInformation> reservationControlInformation;

	private List<MonetaryInformation> monetaryInformation;

	private PricingTicketingDetails pricingTicketingDetails;

	private List<FormOfPayment> formOfPayment;

	private List<TaxDetails> taxDetails;

	private NumberOfUnits numberOfUnits;

	private TicketCoupon ticketCoupon;

	private List<InteractiveFreeText> text;

	private List<TravellerTicketingInformation> travellerTicketingInformation;

	private MessageHeader messageHeader;

	public MessageFunction getMessageFunction() {
		return messageFunction;
	}

	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}

	public TicketingAgentInformation getTicketingAgentInformation() {
		return ticketingAgentInformation;
	}

	public void setTicketingAgentInformation(TicketingAgentInformation ticketingAgentInformation) {
		this.ticketingAgentInformation = ticketingAgentInformation;
	}

	public List<ReservationControlInformation> getReservationControlInformation() {
		return reservationControlInformation;
	}

	public void setReservationControlInformation(List<ReservationControlInformation> reservationControlInformation) {
		this.reservationControlInformation = reservationControlInformation;
	}

	public List<MonetaryInformation> getMonetaryInformation() {
		return monetaryInformation;
	}

	public void setMonetaryInformation(List<MonetaryInformation> monetaryInformation) {
		this.monetaryInformation = monetaryInformation;
	}

	public PricingTicketingDetails getPricingTicketingDetails() {
		return pricingTicketingDetails;
	}

	public void setPricingTicketingDetails(PricingTicketingDetails pricingTicketingDetails) {
		this.pricingTicketingDetails = pricingTicketingDetails;
	}

	public List<FormOfPayment> getFormOfPayment() {
		return formOfPayment;
	}

	public void setFormOfPayment(List<FormOfPayment> formOfPayment) {
		this.formOfPayment = formOfPayment;
	}

	public List<TaxDetails> getTaxDetails() {
		return taxDetails;
	}

	public void setTaxDetails(List<TaxDetails> taxDetails) {
		this.taxDetails = taxDetails;
	}

	public NumberOfUnits getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(NumberOfUnits numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public TicketCoupon getTicketCoupon() {
		return ticketCoupon;
	}

	public void setTicketCoupon(TicketCoupon ticketCoupon) {
		this.ticketCoupon = ticketCoupon;
	}

	public List<InteractiveFreeText> getText() {
		return text;
	}

	public void setText(List<InteractiveFreeText> text) {
		this.text = text;
	}

	public List<TravellerTicketingInformation> getTravellerTicketingInformation() {
		return travellerTicketingInformation;
	}

	public void setTravellerTicketingInformation(List<TravellerTicketingInformation> travellerTicketingInformation) {
		this.travellerTicketingInformation = travellerTicketingInformation;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

}
