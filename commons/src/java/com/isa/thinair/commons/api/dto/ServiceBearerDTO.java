package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class ServiceBearerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String airlineCode;	

	private String outgoingEmailFromAddress;

	private String sitaAddress;

	public String getAirlineCode() {
		return airlineCode;
	}

	public String getOutgoingEmailFromAddress() {
		return outgoingEmailFromAddress;
	}

	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public void setOutgoingEmailFromAddress(String outgoingEmailFromAddress) {
		this.outgoingEmailFromAddress = outgoingEmailFromAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

}
