package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.Date;

public class DateAndTimeInformation implements Serializable {

    private String qualifier;

    private String date;

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
