package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * includes common configurations used by XBE, IBE & WS
 * 
 * @author rumesh
 * 
 */
public class BasePaxConfigDTO implements Serializable {
	private static final long serialVersionUID = 8061021334918340870L;

	public static final String FIELD_TITLE = "title";
	public static final String FIELD_FNAME = "firstName";
	public static final String FIELD_LNAME = "lastName";
	public static final String FIELD_NATIONALITY = "nationality";
	public static final String FIELD_DOB = "dob";
	public static final String FIELD_PASSPORT = "passportNo";
	public static final String FIELD_PSPT_EXPIRY = "passportExpiry";
	public static final String FIELD_PSPT_ISSUED_CNTRY = "passportIssuedCntry";
	public static final String FIELD_PLACE_OF_BIRTH = "placeOfBirth";
	public static final String FIELD_TRAVEL_DOC_TYPE = "travelDocumentType";
	public static final String FIELD_VISA_DOC_NUMBER = "visaDocNumber";
	public static final String FIELD_VISA_DOC_PLACE_OF_ISSUE = "visaDocPlaceOfIssue";
	public static final String FIELD_VISA_DOC_ISSUE_DATE = "visaDocIssueDate";
	public static final String FIELD_VISA_APPLICABLE_COUNTRY = "visaApplicableCountry";
	public static final String FIELD_ETICKET = "eticketNo";
	public static final String FIELD_TRAVELWITH = "travelWith";
	public static final String FIELD_EMPLOYEE_ID = "employeeId";
	public static final String FIELD_DATE_OF_JOIN = "dateOfJoin";
	public static final String FIELD_ID_CATEGORY = "idCategory";
	public static final String FIELD_MIN_LENGTH = "minLength";
	public static final String FIELD_NATIONAL_ID = "nationalIDNo";

	private int paxConfigId;
	private String fieldName;
	private String paxCatCode;
	private String paxTypeCode;
	private String ssrCode;
	private String uniqueness;
	private String uniquenessGroup;
	private int minLength;
	private Boolean autoCheckinVisibility;
	private Boolean autoCheckinMandatory;

	public int getPaxConfigId() {
		return paxConfigId;
	}

	public void setPaxConfigId(int paxConfigId) {
		this.paxConfigId = paxConfigId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getPaxCatCode() {
		return paxCatCode;
	}

	public void setPaxCatCode(String paxCatCode) {
		this.paxCatCode = paxCatCode.intern();
	}

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode.intern();
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getUniqueness() {
		return uniqueness;
	}

	public void setUniqueness(String uniqueness) {
		this.uniqueness = uniqueness.intern();
	}

	public String getUniquenessGroup() {
		return uniquenessGroup;
	}

	public void setUniquenessGroup(String uniquenessGroup) {
		this.uniquenessGroup = uniquenessGroup;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMinLength() {
		return minLength;
	}

	/**
	 * @return the autoCheckinVisibility
	 */
	public Boolean getAutoCheckinVisibility() {
		return autoCheckinVisibility;
	}

	/**
	 * @param autoCheckinVisibility
	 *            the autoCheckinVisibility to set
	 */
	public void setAutoCheckinVisibility(Boolean autoCheckinVisibility) {
		this.autoCheckinVisibility = autoCheckinVisibility;
	}

	/**
	 * @return the autoCheckinMandatory
	 */
	public Boolean getAutoCheckinMandatory() {
		return autoCheckinMandatory;
	}

	/**
	 * @param autoCheckinMandatory
	 *            the autoCheckinMandatory to set
	 */
	public void setAutoCheckinMandatory(Boolean autoCheckinMandatory) {
		this.autoCheckinMandatory = autoCheckinMandatory;
	}

}
