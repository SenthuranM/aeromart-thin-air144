/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * To hold Passenger Category data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaxCategoryTO implements Serializable {

	private static final long serialVersionUID = -5924467776324555066L;

	/** Holds the passenger category code */
	private String paxCategoryCode;

	/** Holds the passenger category desc */
	private String paxCategoryDesc;

	/** Holds the passenger category default currency */
	private String defaultCurrency;

	/** Holds the passenger category default sort order */
	private int defaultSortOrder;

	/** Holds the passenger category status */
	private PaxCategoryStatusCodes status;

	/** Holds the valid passenger category states */
	public static enum PaxCategoryStatusCodes {
		ACT, INA
	};

	/**
	 * @return Returns the defaultCurrency.
	 */
	public String getDefaultCurrency() {
		return defaultCurrency;
	}

	/**
	 * @param defaultCurrency
	 *            The defaultCurrency to set.
	 */
	public void setDefaultCurrency(String defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	/**
	 * @return Returns the defaultSortOrder.
	 */
	public int getDefaultSortOrder() {
		return defaultSortOrder;
	}

	/**
	 * @param defaultSortOrder
	 *            The defaultSortOrder to set.
	 */
	public void setDefaultSortOrder(int defaultSortOrder) {
		this.defaultSortOrder = defaultSortOrder;
	}

	/**
	 * @return Returns the paxCategoryCode.
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            The paxCategoryCode to set.
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return Returns the paxCategoryDesc.
	 */
	public String getPaxCategoryDesc() {
		return paxCategoryDesc;
	}

	/**
	 * @param paxCategoryDesc
	 *            The paxCategoryDesc to set.
	 */
	public void setPaxCategoryDesc(String paxCategoryDesc) {
		this.paxCategoryDesc = paxCategoryDesc;
	}

	/**
	 * @return Returns the status.
	 */
	public PaxCategoryStatusCodes getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(PaxCategoryStatusCodes status) {
		this.status = status;
	}
}
