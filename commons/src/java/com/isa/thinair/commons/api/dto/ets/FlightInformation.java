package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FlightInformation implements Serializable {

    private String departureDateTime;

    private String arrivalDateTime;

    private Location departureLocation;

    private Location arrivalLocation;

    private FlightInformation.CompanyInfo companyInfo;

    private String flightNumber;

    private FlightInformation.FlightPreference flightPreference;

    private List<String> flightRemarks;

    private String lineItemNumber;

    private String travellerCount;

    private String actionCode;

    private String sellType;

    private String segmentIdentifier;

    private ErrorInformation errorInformation;

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public Location getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(Location departureLocation) {
        this.departureLocation = departureLocation;
    }

    public Location getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(Location arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public FlightPreference getFlightPreference() {
        return flightPreference;
    }

    public void setFlightPreference(FlightPreference flightPreference) {
        this.flightPreference = flightPreference;
    }

    public List<String> getFlightRemarks() {
        return flightRemarks;
    }

    public void setFlightRemarks(List<String> flightRemarks) {
        this.flightRemarks = flightRemarks;
    }

    public String getLineItemNumber() {
        return lineItemNumber;
    }

    public void setLineItemNumber(String lineItemNumber) {
        this.lineItemNumber = lineItemNumber;
    }

    public String getTravellerCount() {
        return travellerCount;
    }

    public void setTravellerCount(String travellerCount) {
        this.travellerCount = travellerCount;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getSegmentIdentifier() {
        return segmentIdentifier;
    }

    public void setSegmentIdentifier(String segmentIdentifier) {
        this.segmentIdentifier = segmentIdentifier;
    }

    public ErrorInformation getErrorInformation() {
        return errorInformation;
    }

    public void setErrorInformation(ErrorInformation errorInformation) {
        this.errorInformation = errorInformation;
    }

    public static class CompanyInfo implements Serializable {

        private String marketingAirlineCode;

        private String operatingAirlineCode;

        public String getMarketingAirlineCode() {
            return marketingAirlineCode;
        }

        public void setMarketingAirlineCode(String marketingAirlineCode) {
            this.marketingAirlineCode = marketingAirlineCode;
        }

        public String getOperatingAirlineCode() {
            return operatingAirlineCode;
        }

        public void setOperatingAirlineCode(String operatingAirlineCode) {
            this.operatingAirlineCode = operatingAirlineCode;
        }
    }

    public static class FlightPreference implements Serializable {

        private String bookingClass;

        private String bookingClassModifier;

        public String getBookingClass() {
            return bookingClass;
        }

        public void setBookingClass(String bookingClass) {
            this.bookingClass = bookingClass;
        }

        public String getBookingClassModifier() {
            return bookingClassModifier;
        }

        public void setBookingClassModifier(String bookingClassModifier) {
            this.bookingClassModifier = bookingClassModifier;
        }
    }
}
