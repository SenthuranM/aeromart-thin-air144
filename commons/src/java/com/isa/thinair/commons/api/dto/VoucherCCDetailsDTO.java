package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * @author chanaka
 *
 */
public class VoucherCCDetailsDTO implements Serializable {
	
	private static final long serialVersionUID = 2033760901826640296L;
	
	private String cardType;
	
	private String cardNumber;
	
	private String cardExpiry;
	
	private String cardCVV;
	
	private String cardHolderName;
	
	private String paymentMethod;
	
	private String ipgId;

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardExpiry
	 */
	public String getCardExpiry() {
		return cardExpiry;
	}

	/**
	 * @param cardExpiry the cardExpiry to set
	 */
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	/**
	 * @return the cardCVV
	 */
	public String getCardCVV() {
		return cardCVV;
	}

	/**
	 * @param cardCVV the cardCVV to set
	 */
	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	/**
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName the cardHolderName to set
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the ipgId
	 */
	public String getIpgId() {
		return ipgId;
	}

	/**
	 * @param ipgId the ipgId to set
	 */
	public void setIpgId(String ipgId) {
		this.ipgId = ipgId;
	}
	

}
