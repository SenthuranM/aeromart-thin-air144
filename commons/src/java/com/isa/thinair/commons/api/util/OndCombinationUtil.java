package com.isa.thinair.commons.api.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class OndCombinationUtil {

	public static List<String> getApplicableOndCodeSelections(List<String> airports) {
		List<String> applicableOndCodeSelections = new ArrayList<String>();
		applicableOndCodeSelections.addAll(getAnyRouteOptions(airports.size()));
		applicableOndCodeSelections.addAll(getFromAllToAllCombinationWhenMoreThanTwoAirPorts(airports));
		List<List<String>> powerSet = new LinkedList<List<String>>();
		for (String airport : airports) {
			powerSet.addAll(getAllcombinations(airports, airports.indexOf(airport) + 1));
		}
		applicableOndCodeSelections.addAll(getAllOndCombinations(powerSet, airports));
		return applicableOndCodeSelections;
	}

	private static List<String> getAnyRouteOptions(int numOfAirports) {
		List<String> anyRouteOptions = new ArrayList<String>();
		StringBuilder any = new StringBuilder("***");
		for (int i = 0; i < numOfAirports - 1; i++) {
			any.append("/***");
			anyRouteOptions.add(any.toString());
		}
		return anyRouteOptions;
	}

	private static List<String> getFromAllToAllCombinationWhenMoreThanTwoAirPorts(List<String> airports) {
		if (airports.size() > 2) {
			return new ArrayList<String>(Arrays.asList(airports.get(0) + "/***", "***/" + airports.get(airports.size() - 1)));
		} else {
			return new ArrayList<String>();
		}
	}

	private static List<String> getAllOndCombinations(List<List<String>> airportCombinations, List<String> airports) {
		List<String> combinations = new ArrayList<String>();

		StringBuilder route = new StringBuilder();
		for (List<String> airportCombination : airportCombinations) {
			route = new StringBuilder();
			for (String airport : airports) {
				if (airportCombination.contains(airport)) {
					route.append("/" + airport);
				} else {
					route.append("/***");
				}
			}
			route.deleteCharAt(0);
			combinations.add(route.toString());
		}

		return combinations;
	}

	private static <T> List<List<T>> getAllcombinations(List<T> values, int size) {

		if (0 == size) {
			return Collections.singletonList(Collections.<T> emptyList());
		}
		if (values.isEmpty()) {
			return Collections.emptyList();
		}
		List<List<T>> combination = new LinkedList<List<T>>();
		T actual = values.iterator().next();
		List<T> subSet = new LinkedList<T>(values);
		subSet.remove(actual);
		List<List<T>> subSetCombination = getAllcombinations(subSet, size - 1);

		for (List<T> set : subSetCombination) {
			List<T> newSet = new LinkedList<T>(set);
			newSet.add(0, actual);
			combination.add(newSet);
		}
		combination.addAll(getAllcombinations(subSet, size));
		return combination;
	}

}
