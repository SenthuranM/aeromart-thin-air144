package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class ServiceClientTTYDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String serviceClientCode;
	
	private String inTTYAddress;
	
	private String outTTYAddress;
	
	public ServiceClientTTYDTO(String serviceClientCode, String inTTYAddress, String outTTYAddress) {
		this.serviceClientCode = serviceClientCode;
		this.inTTYAddress = inTTYAddress;
		this.outTTYAddress = outTTYAddress;
	}

	public String getServiceClientCode() {
		return serviceClientCode;
	}

	public String getInTTYAddress() {
		return inTTYAddress;
	}

	public String getOutTTYAddress() {
		return outTTYAddress;
	}

	public void setServiceClientCode(String serviceClientCode) {
		this.serviceClientCode = serviceClientCode;
	}

	public void setInTTYAddress(String inTTYAddress) {
		this.inTTYAddress = inTTYAddress;
	}

	public void setOutTTYAddress(String outTTYAddress) {
		this.outTTYAddress = outTTYAddress;
	}
}
