package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Since this is not specific to bundled Fare, this should be refactored to something like
 * FareClassDescriptionTempalteDTO
 * 
 * Currently used in below fare classes,
 * 
 * - Logical Cabin Class
 * 
 * - Bundled Fare
 * 
 */
public class BundleFareDescriptionTemplateDTO implements Serializable {

	private static final long serialVersionUID = 112879146457568798L;

	private Integer templateID;

	private String templateName;

	private String defaultFreeServicesContent;

	private String defaultPaidServicesContent;

	private Long version;

	private String createdDate;

	private Map<String, BundleFareDescriptionTranslationTemplateDTO> translationTemplates;

	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getDefaultFreeServicesContent() {
		return defaultFreeServicesContent;
	}

	public void setDefaultFreeServicesContent(String defaultFreeServicesContent) {
		this.defaultFreeServicesContent = defaultFreeServicesContent;
	}

	public String getDefaultPaidServicesContent() {
		return defaultPaidServicesContent;
	}

	public void setDefaultPaidServicesContent(String defaultPaidServicesContent) {
		this.defaultPaidServicesContent = defaultPaidServicesContent;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public void setTranslationTemplates(Map<String, BundleFareDescriptionTranslationTemplateDTO> translationTemplates) {
		this.translationTemplates = translationTemplates;
	}

	public Map<String, BundleFareDescriptionTranslationTemplateDTO> getTranslationTemplates() {
		if (translationTemplates == null) {
			setTranslationTemplates(new HashMap<>());
		}
		return translationTemplates;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}
