package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class RequestIdentity implements Serializable {
	
    private Tenant tenant;
    
    private String userToken;
    
    private String ipAddress;
    
    private static RequestIdentity etsInstance = null;
    
    private static RequestIdentity typeAMsgAdaptorInstance = null;
    
	private static String ETS = "ETS";
	
	private static String TYPEA_MSG_ADAPTOR = "TYPEA_MSG_ADAPTOR";
    
    private RequestIdentity() {
    }

	public static RequestIdentity getInstance(String carrierCode, String requestIdentitySystem) {
		if (ETS.equals(requestIdentitySystem)) {
			if (etsInstance == null) {
				etsInstance = new RequestIdentity();
				Tenant tenant = new Tenant();
				tenant.setCode(carrierCode);
				etsInstance.setTenant(tenant);
				etsInstance.setUserToken("USER_TOKEN");
			}
			return etsInstance;
		} else if (TYPEA_MSG_ADAPTOR.equals(requestIdentitySystem)) {
			if (typeAMsgAdaptorInstance == null) {
				typeAMsgAdaptorInstance = new RequestIdentity();
				Tenant tenant = new Tenant();
				tenant.setCode(carrierCode);
				typeAMsgAdaptorInstance.setTenant(tenant);
				typeAMsgAdaptorInstance.setUserToken("USER_TOKEN");
			}
			return typeAMsgAdaptorInstance;
		}
		return null;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public String getUserToken() {
		return userToken;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
