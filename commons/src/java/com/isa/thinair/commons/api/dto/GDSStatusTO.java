package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

public class GDSStatusTO implements Serializable {

	private static final long serialVersionUID = 282859409883785796L;

	private String gdsCode;

	private String status;
	
	private boolean airlineResModifiable;
	
	private String carrierCode;
	
	private boolean codeShareCarrier;
	
	private String userId;
	
	private Integer gdsId;
	
	private boolean aaValidatingCarrier;
	
	private boolean flightInvModifiable;
	
	private boolean processAvs;
	
	private String marketingFlightPrefix;
	
	private boolean applyGlobalOHDTimeLimit;
	
	private boolean sendOALSegments;
	
	private boolean recordOALSegments;
	
	private int typeBSyncType;
	
	private String paymentType;

	private boolean addAncillary;

	private boolean isEnableAutopublish;

	private boolean enableAttachDefaultAnciTemplate;
	
	private boolean enableAutoScheduleSplit;
	
	private boolean useAeroMartETS;
	
	private String typeASenderSubId;

	private Boolean primeFlightEnabled;
	
	private boolean refundGDSTax;

	public GDSStatusTO(String gdsCode, String status, boolean airlineResModifiable, String carrierCode, boolean codeShareCarrier,
			String userId, Integer gdsId, boolean aaValidatingCarrier, boolean flightInvModifiable, boolean processAvs,
			boolean applyGlobalOHDTimeLimit, boolean sendOALSegments, boolean recordOALSegments, int typeBSyncType, String paymentType, boolean isEnableAutopublish, boolean enableAttachDefaultAnciTemplate, boolean addAncillary,
			String typeASenderSubId, boolean primeFlightEnabled, boolean refundGDSTax) {
		this.gdsCode = gdsCode;
		this.status = status;
		this.airlineResModifiable = airlineResModifiable;
		this.carrierCode = carrierCode;
		this.codeShareCarrier = codeShareCarrier;
		this.userId = userId;
		this.gdsId = gdsId;
		this.aaValidatingCarrier = aaValidatingCarrier;
		this.flightInvModifiable = flightInvModifiable;
		this.processAvs = processAvs;
		this.applyGlobalOHDTimeLimit = applyGlobalOHDTimeLimit;
		this.sendOALSegments = sendOALSegments;
		this.recordOALSegments = recordOALSegments;
		this.typeBSyncType = typeBSyncType;
		this.paymentType = paymentType;
		this.isEnableAutopublish = isEnableAutopublish;
		this.enableAttachDefaultAnciTemplate = enableAttachDefaultAnciTemplate;
		this.addAncillary = addAncillary;
		this.refundGDSTax = refundGDSTax;
		this.typeASenderSubId = typeASenderSubId ;
		this.primeFlightEnabled = primeFlightEnabled;
	}

	public String getGdsCode() {
		return gdsCode;
	}

	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the airlineResModifiable
	 */
	public boolean isAirlineResModifiable() {
		return airlineResModifiable;
	}

	/**
	 * @param airlineResModifiable the airlineResModifiable to set
	 */
	public void setAirlineResModifiable(boolean airlineResModifiable) {
		this.airlineResModifiable = airlineResModifiable;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public boolean isAaValidatingCarrier() {
		return aaValidatingCarrier;
	}

	public void setAaValidatingCarrier(boolean aaValidatingCarrier) {
		this.aaValidatingCarrier = aaValidatingCarrier;
	}

	public boolean isFlightInvModifiable() {
		return flightInvModifiable;
	}

	public void setFlightInvModifiable(boolean flightInvModifiable) {
		this.flightInvModifiable = flightInvModifiable;
	}

	public boolean isProcessAvs() {
		return processAvs;
	}

	public void setProcessAvs(boolean processAvs) {
		this.processAvs = processAvs;
	}

	public String getMarketingFlightPrefix() {
		return marketingFlightPrefix;
	}

	public void setMarketingFlightPrefix(String marketingFlightPrefix) {
		this.marketingFlightPrefix = marketingFlightPrefix;
	}

	public boolean isApplyGlobalOHDTimeLimit() {
		return applyGlobalOHDTimeLimit;
	}

	public void setApplyGlobalOHDTimeLimit(boolean applyGlobalOHDTimeLimit) {
		this.applyGlobalOHDTimeLimit = applyGlobalOHDTimeLimit;
	}

	public boolean isSendOALSegments() {
		return sendOALSegments;
	}

	public void setSendOALSegments(boolean sendOALSegments) {
		this.sendOALSegments = sendOALSegments;
	}

	public boolean isRecordOALSegments() {
		return recordOALSegments;
	}

	public void setRecordOALSegments(boolean recordOALSegments) {
		this.recordOALSegments = recordOALSegments;
	}

	public int getTypeBSyncType() {
		return typeBSyncType;
	}

	public void setTypeBSyncType(int typeBSyncType) {
		this.typeBSyncType = typeBSyncType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public boolean isAddAncillary() {
		return addAncillary;
	}
	
	public boolean isEnableAutopublish() {
		return isEnableAutopublish;
	}

	public void setEnableAutopublish(boolean isEnableAutopublish) {
		this.isEnableAutopublish = isEnableAutopublish;
	}

	/**
	 * @return the enableAttachDefaultAnciTemplate
	 */
	public boolean isEnableAttachDefaultAnciTemplate() {
		return enableAttachDefaultAnciTemplate;
	}

	/**
	 * @param enableAttachDefaultAnciTemplate the enableAttachDefaultAnciTemplate to set
	 */
	public void setEnableAttachDefaultAnciTemplate(boolean enableAttachDefaultAnciTemplate) {
		this.enableAttachDefaultAnciTemplate = enableAttachDefaultAnciTemplate;
	}

	public boolean isEnableAutoScheduleSplit() {
		return enableAutoScheduleSplit;
	}

	public void setEnableAutoScheduleSplit(boolean enableAutoScheduleSplit) {
		this.enableAutoScheduleSplit = enableAutoScheduleSplit;
	}

	public boolean isGDSTaxRefundable() {
		return refundGDSTax;
	}

	public void setGDSTaxRefundable(boolean refundGDSTax) {
		this.refundGDSTax = refundGDSTax;
	}

	public boolean isUseAeroMartETS() {
		return useAeroMartETS;
	}

	public void setUseAeroMartETS(boolean useAeroMartETS) {
		this.useAeroMartETS = useAeroMartETS;
	}
	
	public String getTypeASenderSubId() {
		return typeASenderSubId;
	}

	public void setTypeASenderSubId(String typeASenderSubId) {
		this.typeASenderSubId = typeASenderSubId;
	}

	public Boolean getPrimeFlightEnabled() {
		return primeFlightEnabled;
	}

	public void setPrimeFlightEnabled(Boolean primeFlightEnabled) {
		this.primeFlightEnabled = primeFlightEnabled;
	}
}
