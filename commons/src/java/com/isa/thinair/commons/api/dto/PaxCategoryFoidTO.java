/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * To hold Passenger Category FOID data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaxCategoryFoidTO implements Serializable {

	private static final long serialVersionUID = 7077881533377491205L;

	/** Holds the Passenger Category information */
	private PaxCategoryTO paxCategoryTO;

	/** Holds the passenger category FOID Id */
	private int paxCategoryFoidId;

	/** Holds the passenger category Code */
	private String paxCategoryCode;

	/** Holds the passenger FOID code */
	private String paxFoidCode;

	/** Holds the passenger FOID description */
	private String paxFoidDesc;

	/** Holds the passenger FOID SSR Code */
	private String paxFoidSsrCode;

	/** Holds the Passenger Type */
	private String paxTypeCode;

	/** Holds the Passenger Category FOID required or not */
	private String paxCatFoidRequired;

	/** Holds the Passenger Category FOID Unique or not */
	private String paxCatFoidUnique;

	/**
	 * @return Returns the paxCategoryCode.
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            The paxCategoryCode to set.
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return Returns the paxCategoryFoidId.
	 */
	public int getPaxCategoryFoidId() {
		return paxCategoryFoidId;
	}

	/**
	 * @param paxCategoryFoidId
	 *            The paxCategoryFoidId to set.
	 */
	public void setPaxCategoryFoidId(int paxCategoryFoidId) {
		this.paxCategoryFoidId = paxCategoryFoidId;
	}

	/**
	 * @return Returns the paxFoidCode.
	 */
	public String getPaxFoidCode() {
		return paxFoidCode;
	}

	/**
	 * @param paxFoidCode
	 *            The paxFoidCode to set.
	 */
	public void setPaxFoidCode(String paxFoidCode) {
		this.paxFoidCode = paxFoidCode;
	}

	/**
	 * @return Returns the paxTypeCode.
	 */
	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	/**
	 * @param paxTypeCode
	 *            The paxTypeCode to set.
	 */
	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	/**
	 * @return Returns the paxFoidDesc.
	 */
	public String getPaxFoidDesc() {
		return paxFoidDesc;
	}

	/**
	 * @param paxFoidDesc
	 *            The paxFoidDesc to set.
	 */
	public void setPaxFoidDesc(String paxFoidDesc) {
		this.paxFoidDesc = paxFoidDesc;
	}

	/**
	 * @return Returns the paxFoidSsrCode.
	 */
	public String getPaxFoidSsrCode() {
		return paxFoidSsrCode;
	}

	/**
	 * @param paxFoidSsrCode
	 *            The paxFoidSsrCode to set.
	 */
	public void setPaxFoidSsrCode(String paxFoidSsrCode) {
		this.paxFoidSsrCode = paxFoidSsrCode;
	}

	/**
	 * @return Returns the paxCatFoidRequired.
	 */
	public String getPaxCatFoidRequired() {
		return paxCatFoidRequired;
	}

	/**
	 * @param paxCatFoidRequired
	 *            The paxCatFoidRequired to set.
	 */
	public void setPaxCatFoidRequired(String paxCatFoidRequired) {
		this.paxCatFoidRequired = paxCatFoidRequired;
	}

	/**
	 * @return Returns the paxCategoryTO.
	 */
	public PaxCategoryTO getPaxCategoryTO() {
		return paxCategoryTO;
	}

	/**
	 * @param paxCategoryTO
	 *            The paxCategoryTO to set.
	 */
	public void setPaxCategoryTO(PaxCategoryTO paxCategoryTO) {
		this.paxCategoryTO = paxCategoryTO;
	}

	/**
	 * @return the paxCatFoidUnique
	 */
	public String getPaxCatFoidUnique() {
		return paxCatFoidUnique;
	}

	/**
	 * @param paxCatFoidUnique
	 *            the paxCatFoidUnique to set
	 */
	public void setPaxCatFoidUnique(String paxCatFoidUnique) {
		this.paxCatFoidUnique = paxCatFoidUnique;
	}
}
