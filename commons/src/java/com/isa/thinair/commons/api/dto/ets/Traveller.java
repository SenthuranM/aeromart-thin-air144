package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class Traveller implements Serializable {

    private String title;

    private String givenName;

    private String passengerType;

    private String travellerReferenceNumber;

    private String hasInfant;

    private String otherNames;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public String getTravellerReferenceNumber() {
        return travellerReferenceNumber;
    }

    public void setTravellerReferenceNumber(String travellerReferenceNumber) {
        this.travellerReferenceNumber = travellerReferenceNumber;
    }

    public String getHasInfant() {
        return hasInfant;
    }

    public void setHasInfant(String hasInfant) {
        this.hasInfant = hasInfant;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }
}
