package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class TicketControlTicketNumberDetails implements Serializable {

	private TravellerInformation travellerInformation;

	private TicketingAgentInformation ticketingAgentInformation;

	private ReservationControlInformation reservationControlInformation;

	private FrequentTravellerInformation frequentTravellerInformation;

	private MonetaryInformation monetaryInformation;

	private FormOfPayment formOfPayment;

	private TaxDetails taxDetails;

	private OriginDestinationInformation originDestinationInfo;

	private AdditionalTourInformation additionalTourInfo;

	private NumberOfUnits numberOfUnits;

	private DocumentInformationDetails documentInfoDetails;

	private String ticketNumber;

	private String documentType;

	private String totalItems;

	private String dataIndicator;

	private InteractiveFreeText text;

	private PricingTicketingDetails pricingTicketingDetails;

	private TicketCoupon ticketCoupon;

	private DateAndTimeInformation dateAndTime;

	private OriginatorInformation originatorInformation;

	private ErrorInformation errorInformation;

	private ConsumerRefInformation consumerRef;

	public TravellerInformation getTravellerInformation() {
		return travellerInformation;
	}

	public void setTravellerInformation(TravellerInformation travellerInformation) {
		this.travellerInformation = travellerInformation;
	}

	public TicketingAgentInformation getTicketingAgentInformation() {
		return ticketingAgentInformation;
	}

	public void setTicketingAgentInformation(TicketingAgentInformation ticketingAgentInformation) {
		this.ticketingAgentInformation = ticketingAgentInformation;
	}

	public ReservationControlInformation getReservationControlInformation() {
		return reservationControlInformation;
	}

	public void setReservationControlInformation(ReservationControlInformation reservationControlInformation) {
		this.reservationControlInformation = reservationControlInformation;
	}

	public FrequentTravellerInformation getFrequentTravellerInformation() {
		return frequentTravellerInformation;
	}

	public void setFrequentTravellerInformation(FrequentTravellerInformation frequentTravellerInformation) {
		this.frequentTravellerInformation = frequentTravellerInformation;
	}

	public MonetaryInformation getMonetaryInformation() {
		return monetaryInformation;
	}

	public void setMonetaryInformation(MonetaryInformation monetaryInformation) {
		this.monetaryInformation = monetaryInformation;
	}

	public FormOfPayment getFormOfPayment() {
		return formOfPayment;
	}

	public void setFormOfPayment(FormOfPayment formOfPayment) {
		this.formOfPayment = formOfPayment;
	}

	public TaxDetails getTaxDetails() {
		return taxDetails;
	}

	public void setTaxDetails(TaxDetails taxDetails) {
		this.taxDetails = taxDetails;
	}

	public OriginDestinationInformation getOriginDestinationInfo() {
		return originDestinationInfo;
	}

	public void setOriginDestinationInfo(OriginDestinationInformation originDestinationInfo) {
		this.originDestinationInfo = originDestinationInfo;
	}

	public AdditionalTourInformation getAdditionalTourInfo() {
		return additionalTourInfo;
	}

	public void setAdditionalTourInfo(AdditionalTourInformation additionalTourInfo) {
		this.additionalTourInfo = additionalTourInfo;
	}

	public NumberOfUnits getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(NumberOfUnits numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public DocumentInformationDetails getDocumentInfoDetails() {
		return documentInfoDetails;
	}

	public void setDocumentInfoDetails(DocumentInformationDetails documentInfoDetails) {
		this.documentInfoDetails = documentInfoDetails;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	public String getDataIndicator() {
		return dataIndicator;
	}

	public void setDataIndicator(String dataIndicator) {
		this.dataIndicator = dataIndicator;
	}

	public InteractiveFreeText getText() {
		return text;
	}

	public void setText(InteractiveFreeText text) {
		this.text = text;
	}

	public PricingTicketingDetails getPricingTicketingDetails() {
		return pricingTicketingDetails;
	}

	public void setPricingTicketingDetails(PricingTicketingDetails pricingTicketingDetails) {
		this.pricingTicketingDetails = pricingTicketingDetails;
	}

	public TicketCoupon getTicketCoupon() {
		return ticketCoupon;
	}

	public void setTicketCoupon(TicketCoupon ticketCoupon) {
		this.ticketCoupon = ticketCoupon;
	}

	public DateAndTimeInformation getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(DateAndTimeInformation dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}

	public ErrorInformation getErrorInformation() {
		return errorInformation;
	}

	public void setErrorInformation(ErrorInformation errorInformation) {
		this.errorInformation = errorInformation;
	}

	public ConsumerRefInformation getConsumerRef() {
		return consumerRef;
	}

	public void setConsumerRef(ConsumerRefInformation consumerRef) {
		this.consumerRef = consumerRef;
	}

}
