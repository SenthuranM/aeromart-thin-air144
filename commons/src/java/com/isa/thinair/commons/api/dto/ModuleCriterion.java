/*
 * Created on Jul 5, 2005
 *
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nasly
 * 
 */
public class ModuleCriterion implements Serializable {

	private static final long serialVersionUID = -98017520633393552L;

	private String fieldName;
	private List value;
	private String condition;
	boolean leaf;

	public ModuleCriterion() {
		leaf = true;
	}

	public ModuleCriterion(boolean leaf) {
		this.leaf = leaf;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public List getValue() {
		return value;
	}

	public void setValue(List value) {
		this.value = value;
	}

	public static final String CONDITION_EQUALS = "=";
	public static final String CONDITION_GREATER_THAN = ">";
	public static final String CONDITION_GREATER_THAN_OR_EQUALS = ">=";
	public static final String CONDITION_LESS_THAN = "<";
	public static final String CONDITION_LESS_THAN_OR_EQUALS = "<=";
	public static final String CONDITION_BETWEEN = "BETWEEN";
	public static final String CONDITION_IN = "IN";
	public static final String CONDITION_NOT_IN = "NOT IN";
	public static final String CONDITION_LIKE = "LIKE";
	public static final String CONDITION_LIKE_IGNORECASE = "LIKE_IGNORE CASE";
	public static final String CONDITION_LIKE_INCASE_SENSITIVE = "ILIKE";
	public static final String CONDITION_NOT_EQUAL = "<>";
	public static final String CONDITION_IS_NOT_NULL = "IS NOT NULL";
}
