package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TicketCoupon implements Serializable {

    private String couponNumber;

    private String status;

    private String couponValue;

    private String exchangeMedia;

    private String settlementAuthCode;

    private String voluntaryInvoluntaryIndicator;

    private String previousStatus;

    private FlightInformation flightInformation;

    private List<ReservationControlInformation> reservationControlInfo;

    private RelatedProductInfo relatedProductInfo;

    private PricingTicketingSubsequent pricingTicketingSubsequent;

    private ExcessBaggageInfo excessBaggageInformation;

    private FrequentTravellerInformation frequentTravellerInformation;

    private List<DateAndTimeInformation> dateAndTimeInformation;

    private List<InteractiveFreeText> text;

    private PricingTicketingDetails pricingTicketingDetails;

    private OriginatorInformation originatorInformation;

    private ErrorInformation errorInformation;

    public String getCouponNumber() {
        return couponNumber;
    }

    public void setCouponNumber(String couponNumber) {
        this.couponNumber = couponNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public String getExchangeMedia() {
        return exchangeMedia;
    }

    public void setExchangeMedia(String exchangeMedia) {
        this.exchangeMedia = exchangeMedia;
    }

    public String getSettlementAuthCode() {
        return settlementAuthCode;
    }

    public void setSettlementAuthCode(String settlementAuthCode) {
        this.settlementAuthCode = settlementAuthCode;
    }

    public String getVoluntaryInvoluntaryIndicator() {
        return voluntaryInvoluntaryIndicator;
    }

    public void setVoluntaryInvoluntaryIndicator(String voluntaryInvoluntaryIndicator) {
        this.voluntaryInvoluntaryIndicator = voluntaryInvoluntaryIndicator;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public FlightInformation getFlightInformation() {
        return flightInformation;
    }

    public void setFlightInformation(FlightInformation flightInformation) {
        this.flightInformation = flightInformation;
    }

    public List<ReservationControlInformation> getReservationControlInfo() {
        return reservationControlInfo;
    }

    public void setReservationControlInfo(List<ReservationControlInformation> reservationControlInfo) {
        this.reservationControlInfo = reservationControlInfo;
    }

    public RelatedProductInfo getRelatedProductInfo() {
        return relatedProductInfo;
    }

    public void setRelatedProductInfo(RelatedProductInfo relatedProductInfo) {
        this.relatedProductInfo = relatedProductInfo;
    }

    public PricingTicketingSubsequent getPricingTicketingSubsequent() {
        return pricingTicketingSubsequent;
    }

    public void setPricingTicketingSubsequent(PricingTicketingSubsequent pricingTicketingSubsequent) {
        this.pricingTicketingSubsequent = pricingTicketingSubsequent;
    }

    public ExcessBaggageInfo getExcessBaggageInformation() {
        return excessBaggageInformation;
    }

    public void setExcessBaggageInformation(ExcessBaggageInfo excessBaggageInformation) {
        this.excessBaggageInformation = excessBaggageInformation;
    }

    public FrequentTravellerInformation getFrequentTravellerInformation() {
        return frequentTravellerInformation;
    }

    public void setFrequentTravellerInformation(FrequentTravellerInformation frequentTravellerInformation) {
        this.frequentTravellerInformation = frequentTravellerInformation;
    }

    public List<DateAndTimeInformation> getDateAndTimeInformation() {
        return dateAndTimeInformation;
    }

    public void setDateAndTimeInformation(List<DateAndTimeInformation> dateAndTimeInformation) {
        this.dateAndTimeInformation = dateAndTimeInformation;
    }

    public List<InteractiveFreeText> getText() {
        return text;
    }

    public void setText(List<InteractiveFreeText> text) {
        this.text = text;
    }

    public PricingTicketingDetails getPricingTicketingDetails() {
        return pricingTicketingDetails;
    }

    public void setPricingTicketingDetails(PricingTicketingDetails pricingTicketingDetails) {
        this.pricingTicketingDetails = pricingTicketingDetails;
    }

    public OriginatorInformation getOriginatorInformation() {
        return originatorInformation;
    }

    public void setOriginatorInformation(OriginatorInformation originatorInformation) {
        this.originatorInformation = originatorInformation;
    }

    public ErrorInformation getErrorInformation() {
        return errorInformation;
    }

    public void setErrorInformation(ErrorInformation errorInformation) {
        this.errorInformation = errorInformation;
    }
}
