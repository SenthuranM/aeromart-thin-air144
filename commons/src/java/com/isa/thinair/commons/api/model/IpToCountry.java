package com.isa.thinair.commons.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "t_ip_to_country"
 * @author rumesh
 * 
 */
public class IpToCountry extends Persistent {

	private static final long serialVersionUID = 5896348442217381449L;

	private Long ipToCountryId;

	private Long startRange;

	private Long endRange;

	private String countryCode;

	private boolean syncedWithCachingDB;

	/**
	 * @return the ipToCountryId
	 * @hibernate.id column = "IP_TO_COUNTRY_ID" generator-class = "native"
	 */
	public Long getIpToCountryId() {
		return ipToCountryId;
	}

	/**
	 * @param ipToCountryId
	 *            the ipToCountryId to set
	 */
	public void setIpToCountryId(Long ipToCountryId) {
		this.ipToCountryId = ipToCountryId;
	}

	/**
	 * @return the startRange
	 * @hibernate.property column = "START_RANGE"
	 */
	public Long getStartRange() {
		return startRange;
	}

	/**
	 * @param startRange
	 *            the startRange to set
	 */
	public void setStartRange(Long startRange) {
		this.startRange = startRange;
	}

	/**
	 * @return the endRange
	 * @hibernate.property column = "END_RANGE"
	 */
	public Long getEndRange() {
		return endRange;
	}

	/**
	 * @param endRange
	 *            the endRange to set
	 */
	public void setEndRange(Long endRange) {
		this.endRange = endRange;
	}

	/**
	 * @return the countryCode
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the syncedWithCachingDB
	 * @hibernate.property column = "SYNCED_WITH_CACHING_DB" type = "yes_no"
	 */
	public boolean isSyncedWithCachingDB() {
		return syncedWithCachingDB;
	}

	/**
	 * @param syncedWithCachingDB
	 *            the syncedWithCachingDB to set
	 */
	public void setSyncedWithCachingDB(boolean syncedWithCachingDB) {
		this.syncedWithCachingDB = syncedWithCachingDB;
	}

}
