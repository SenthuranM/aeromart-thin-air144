package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class IssueExchangeTKTREQDTO implements Serializable {
	
    private MessageFunction messageFunction;
    
    private OriginatorInformation originatorInformation;
    
    private TicketingAgentInformation ticketingAgentInformation;
    
    private List<ReservationControlInformation> reservationControlInformation;
    
    private NumberOfUnits numberOfUnits;
    
    private List<InteractiveFreeText> text;
    
    private List<TravellerTicketingInformation> travellerTicketingInformation;
    
    private MessageHeader messageHeader;
   
    private RequestIdentity requestIdentity;
    
	public MessageFunction getMessageFunction() {
		return messageFunction;
	}

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}

	public TicketingAgentInformation getTicketingAgentInformation() {
		return ticketingAgentInformation;
	}

	public NumberOfUnits getNumberOfUnits() {
		return numberOfUnits;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}
	
	public RequestIdentity getRequestIdentity() {
		return requestIdentity;
	}
	
	public void setMessageFunction(MessageFunction messageFunction) {
		this.messageFunction = messageFunction;
	}

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}

	public void setTicketingAgentInformation(TicketingAgentInformation ticketingAgentInformation) {
		this.ticketingAgentInformation = ticketingAgentInformation;
	}

	public void setNumberOfUnits(NumberOfUnits numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}
	
	public void setRequestIdentity(RequestIdentity requestIdentity) {
		this.requestIdentity = requestIdentity;
	}

	public List<ReservationControlInformation> getReservationControlInformation() {
		return reservationControlInformation;
	}

	public List<InteractiveFreeText> getText() {
		return text;
	}

	public List<TravellerTicketingInformation> getTravellerTicketingInformation() {
		return travellerTicketingInformation;
	}

	public void setReservationControlInformation(List<ReservationControlInformation> reservationControlInformation) {
		this.reservationControlInformation = reservationControlInformation;
	}

	public void setText(List<InteractiveFreeText> text) {
		this.text = text;
	}

	public void setTravellerTicketingInformation(List<TravellerTicketingInformation> travellerTicketingInformation) {
		this.travellerTicketingInformation = travellerTicketingInformation;
	}
	
}
