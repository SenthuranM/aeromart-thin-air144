package com.isa.thinair.commons.api.dto;

public class GDSPaxConfigDTO extends BasePaxConfigDTO{

	private boolean gdsMandatory;

	public boolean isGdsMandatory() {
		return gdsMandatory;
	}

	public void setGdsMandatory(boolean gdsMandatory) {
		this.gdsMandatory = gdsMandatory;
	}
}
