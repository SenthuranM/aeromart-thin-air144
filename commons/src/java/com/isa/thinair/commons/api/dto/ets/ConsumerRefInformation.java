package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class ConsumerRefInformation implements Serializable {

    private String referenceQualifier;

    private String referenceNumber;

    private String partyId;

    public String getReferenceQualifier() {
        return referenceQualifier;
    }

    public void setReferenceQualifier(String referenceQualifier) {
        this.referenceQualifier = referenceQualifier;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }
}
