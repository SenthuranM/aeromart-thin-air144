package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class InterchangeInfo implements Serializable {

	private String interchangeId;
	private String interchangeCode;

	public String getInterchangeId() {
		return interchangeId;
	}

	public void setInterchangeId(String interchangeId) {
		this.interchangeId = interchangeId;
	}

	public String getInterchangeCode() {
		return interchangeCode;
	}

	public void setInterchangeCode(String interchangeCode) {
		this.interchangeCode = interchangeCode;
	}

}
