package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * @author asiri
 */
public class FlightTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String flightType;
	private String flightTypeDesction;
	private String flightTypeStatus;

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getFlightTypeDesction() {
		return flightTypeDesction;
	}

	public void setFlightTypeDesction(String flightTypeDesction) {
		this.flightTypeDesction = flightTypeDesction;
	}

	public String getFlightTypeStatus() {
		return flightTypeStatus;
	}

	public void setFlightTypeStatus(String flightTypeStatus) {
		this.flightTypeStatus = flightTypeStatus;
	}

}
