package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;
import java.util.List;

public class TicketingAgentInformation implements Serializable {
	
    private String companyIdentifier;

    private List<InternalIdentificationDetails> internalIdentificationDetails;

    public String getCompanyIdentifier() {
        return companyIdentifier;
    }

    public void setCompanyIdentifier(String companyIdentifier) {
        this.companyIdentifier = companyIdentifier;
    }

    public List<InternalIdentificationDetails> getInternalIdentificationDetails() {
        return internalIdentificationDetails;
    }

    public void setInternalIdentificationDetails(List<InternalIdentificationDetails> internalIdentificationDetails) {
        this.internalIdentificationDetails = internalIdentificationDetails;
    }

    public static class InternalIdentificationDetails implements Serializable {

        private String requesterId;

        private String identificationType;

        private String agentId;

        public String getRequesterId() {
            return requesterId;
        }

        public void setRequesterId(String requesterId) {
            this.requesterId = requesterId;
        }

        public String getIdentificationType() {
            return identificationType;
        }

        public void setIdentificationType(String identificationType) {
            this.identificationType = identificationType;
        }

        public String getAgentId() {
            return agentId;
        }

        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }
    }

}
