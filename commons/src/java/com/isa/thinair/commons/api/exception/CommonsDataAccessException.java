/*
 * Created on Jul 5, 2005
 *
 */
package com.isa.thinair.commons.api.exception;

/**
 * @author Nasly
 */
public class CommonsDataAccessException extends CommonsRuntimeException {

	private static final long serialVersionUID = -4739152832396143077L;

	public CommonsDataAccessException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause, exceptionCode, moduleCode);
	}

	public CommonsDataAccessException(Throwable cause, String exceptionCode) {
		super(cause, exceptionCode);
	}

	public CommonsDataAccessException(String exceptionCode, String moduleCode) {
		super(exceptionCode, moduleCode);
	}

	public CommonsDataAccessException(String exceptionCode) {
		super(exceptionCode);
	}
}
