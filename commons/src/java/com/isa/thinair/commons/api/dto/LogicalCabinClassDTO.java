package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class LogicalCabinClassDTO implements Serializable, Comparable<LogicalCabinClassDTO> {
	private static final long serialVersionUID = 1194418999902106708L;

	public static final String IMG_PREFIX = "LCC_";
	public static final String IMG_SUFFIX = ".png";
	public static final String SEPARATOR = "_";
	public static final String TEMP_LCC_PREFIX = "logicalCCImage";
	public static final String TEMP_LCC_WITH_FLEXI_PREFIX = "logicalCCWithFlexiImage";

	private String logicalCCCode;

	private String cabinClassCode;

	private String description;

	/**
	 * Field to hold the internationalization key for description
	 */
	private String descriptionI18nKey;

	private String status;

	private Integer nestRank;

	private boolean allowSingleMealOnly;

	private boolean freeSeatEnabled;

	private boolean freeFlexiEnabled;

	private String flexiDescription;

	/**
	 * Field to hold the inetrnationalization key for description
	 */
	private String flexiDescriptioni18nMessageKey;

	private String comment;

	/**
	 * Field to hold the inetrnationalization key for description
	 */
	private String commentI18nMessageKey;

	private String flexiComment;

	/**
	 * Field to hold the inetrnationalization key for description
	 */
	private String flexiCommentI18nMessageKey;

	/**
	 * Filed that will be used to determine the current language when deciding whether to translate
	 */
	private String currentLanguage;

    private BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO;

	/**
	 * Filed that will be used to determine the bundle description template
	 */
	private Integer bundleDescTemplateId;
	/**
	 * @return the logicalCCCode
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the nestRank
	 */
	public Integer getNestRank() {
		return nestRank;
	}

	/**
	 * @param nestRank
	 *            the nestRank to set
	 */
	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	/**
	 * @return the allowSingleMealOnly
	 */
	public boolean isAllowSingleMealOnly() {
		return allowSingleMealOnly;
	}

	/**
	 * @param allowSingleMealOnly
	 *            the allowSingleMealOnly to set
	 */
	public void setAllowSingleMealOnly(boolean allowSingleMealOnly) {
		this.allowSingleMealOnly = allowSingleMealOnly;
	}

	/**
	 * @return the freeSeatEnabled
	 */
	public boolean isFreeSeatEnabled() {
		return freeSeatEnabled;
	}

	/**
	 * @param freeSeatEnabled
	 *            the freeSeatEnabled to set
	 */
	public void setFreeSeatEnabled(boolean freeSeatEnabled) {
		this.freeSeatEnabled = freeSeatEnabled;
	}

	/**
	 * @return the freeFlexiEnabled
	 */
	public boolean isFreeFlexiEnabled() {
		return freeFlexiEnabled;
	}

	/**
	 * @param freeFlexiEnabled
	 *            the freeFlexiEnabled to set
	 */
	public void setFreeFlexiEnabled(boolean freeFlexiEnabled) {
		this.freeFlexiEnabled = freeFlexiEnabled;
	}

	/**
	 * @return the flexiDescription
	 */
	public String getFlexiDescription() {
		return flexiDescription;
	}

	/**
	 * @param flexiDescription
	 *            the flexiDescription to set
	 */
	public void setFlexiDescription(String flexiDescription) {
		this.flexiDescription = flexiDescription;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the flexiComment
	 */
	public String getFlexiComment() {
		return flexiComment;
	}

	/**
	 * @param flexiComment
	 *            the flexiComment to set
	 */
	public void setFlexiComment(String flexiComment) {
		this.flexiComment = flexiComment;
	}

	public int compareTo(LogicalCabinClassDTO firstLogicalCabinClassDTO, LogicalCabinClassDTO secondLogicalCabinClassDTO) {
		return firstLogicalCabinClassDTO.getNestRank().compareTo(secondLogicalCabinClassDTO.getNestRank());
	}

	@Override
	public int compareTo(LogicalCabinClassDTO logicalCabinClassDTO) {
		return getNestRank().compareTo(logicalCabinClassDTO.getNestRank());
	}

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public void setDescriptionI18nKey(String descriptionI18nKey) {
		this.descriptionI18nKey = descriptionI18nKey;
	}

	public String getDescriptionI18nKey() {
		return descriptionI18nKey;
	}

	public void setFlexiDescriptioni18nMessageKey(String flexiDescriptioni18nMessageKey) {
		this.flexiDescriptioni18nMessageKey = flexiDescriptioni18nMessageKey;
	}

	public String getFlexiDescriptioni18nMessageKey() {
		return flexiDescriptioni18nMessageKey;
	}

	public void setCommentI18nMessageKey(String commentI18nMessageKey) {
		this.commentI18nMessageKey = commentI18nMessageKey;
	}

	public String getCommentI18nMessageKey() {
		return commentI18nMessageKey;
	}

	public void setFlexiCommentI18nMessageKey(String flexiCommentI18nMessageKey) {
		this.flexiCommentI18nMessageKey = flexiCommentI18nMessageKey;
	}

	public String getFlexiCommentI18nMessageKey() {
		return flexiCommentI18nMessageKey;
	}

	public Integer getBundleDescTemplateId() {
		return bundleDescTemplateId;
	}

	public void setBundleDescTemplateId(Integer bundleDescTemplateId) {
		this.bundleDescTemplateId = bundleDescTemplateId;
	}

	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateDTO() {
		return bundleFareDescriptionTemplateDTO;
	}

	public void setBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO) {
		this.bundleFareDescriptionTemplateDTO = bundleFareDescriptionTemplateDTO;
	}
	
	
}
