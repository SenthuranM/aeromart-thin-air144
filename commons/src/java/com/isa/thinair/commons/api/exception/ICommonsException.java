package com.isa.thinair.commons.api.exception;

import java.io.Serializable;
import java.util.List;

public interface ICommonsException extends Serializable {
	public String getExceptionCode();

	public String getModuleCode();

	public String getMessageString();

	public String getModuleDesc();

	public void setExceptionCode(String exceptionCode);

	public void setModuleCode(String moduleCode);

	public Object getExceptionDetails();

	public void setExceptionDetails(Object object);

	public void setMessageParameters(List<String> parameters);

	public List<String> getMessageParameters();
}
