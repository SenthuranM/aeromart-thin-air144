package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Locale;

/**
 * @author chethiya
 *
 */
public class VoucherDTO implements Serializable {

	private static final long serialVersionUID = -3651822989843187648L;

	private String voucherId;

	private String validFrom;

	private String validTo;

	private String amountInBase;

	private String amountInLocal;

	private String currencyCode;

	private String issuedUserId;

	private String issuedTime;

	private int templateId;

	private char status;

	private String paxFirstName;

	private String paxLastName;

	private String email;

	private String remarks;

	private String redeemdAmount = "0.00";

	private String cancelRemarks;

	private VoucherPaymentDTO voucherPaymentDTO;
	
	private Integer reprotectedPaxid;
	
	private String percentage;
	
	private String printLanguage = Locale.ENGLISH.getLanguage(); 
	
	private String voucherGroupId;
	
	private String mobileNumber;

	/**
	 * @return the printLanguage
	 */
	public String getPrintLanguage() {
		return printLanguage;
	}

	/**
	 * @param printLanguage the printLanguage to set
	 */
	public void setPrintLanguage(String printLanguage) {
		this.printLanguage = printLanguage;
	}
	
	/**
	 * @return the percentage
	 */
	public String getPercentage() {
		return percentage;
	}


	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	/**
	 * @return the reprotectedPaxid
	 */
	public Integer getReprotectedPaxid() {
		return reprotectedPaxid;
	}

	/**
	 * @param reprotectedPaxid the reprotectedPaxid to set
	 */
	public void setReprotectedPaxid(Integer reprotectedPaxid) {
		this.reprotectedPaxid = reprotectedPaxid;
	}

	public VoucherDTO clone() {
		VoucherDTO cloneObject = new VoucherDTO();
		cloneObject.setVoucherId(this.voucherId);
		cloneObject.setValidFrom(this.validFrom);
		cloneObject.setValidTo(this.validTo);
		cloneObject.setAmountInBase(this.amountInBase);
		cloneObject.setAmountInLocal(this.amountInLocal);
		cloneObject.setCurrencyCode(this.currencyCode);
		cloneObject.setIssuedUserId(this.issuedUserId);
		cloneObject.setIssuedTime(this.issuedTime);
		cloneObject.setTemplateId(this.templateId);
		cloneObject.setStatus(this.status);
		cloneObject.setPaxFirstName(this.paxFirstName);
		cloneObject.setPaxLastName(this.paxLastName);
		cloneObject.setEmail(this.email);
		cloneObject.setRemarks(this.remarks);
		cloneObject.setRedeemdAmount(this.redeemdAmount);
		cloneObject.setCancelRemarks(this.cancelRemarks);
		if (this.voucherPaymentDTO != null) {
			cloneObject.setVoucherPaymentDTO(this.voucherPaymentDTO.clone());
		}
		return cloneObject;
	}
	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return this.voucherId;
	}

	/**
	 * @param voucherId
	 *            the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the amountInBase
	 */
	public String getAmountInBase() {
		return amountInBase;
	}

	/**
	 * @param amountInBase
	 *            the amountInBase to set
	 */
	public void setAmountInBase(String amountInBase) {
		this.amountInBase = amountInBase;
	}

	/**
	 * @return the amountInLocal
	 */
	public String getAmountInLocal() {
		return amountInLocal;
	}

	/**
	 * @param amountInLocal
	 *            the amountInLocal to set
	 */
	public void setAmountInLocal(String amountInLocal) {
		this.amountInLocal = amountInLocal;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the issuedUserId
	 */
	public String getIssuedUserId() {
		return issuedUserId;
	}

	/**
	 * @param issuedUserId
	 *            the issuedUserId to set
	 */
	public void setIssuedUserId(String issuedUserId) {
		this.issuedUserId = issuedUserId;
	}

	/**
	 * @return the issuedTime
	 */
	public String getIssuedTime() {
		return issuedTime;
	}

	/**
	 * @param issuedTime
	 *            the issuedTime to set
	 */
	public void setIssuedTime(String issuedTime) {
		this.issuedTime = issuedTime;
	}

	/**
	 * @return the templateId
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the status
	 */
	public char getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(char status) {
		this.status = status;
	}

	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}

	/**
	 * @param paxFirstName
	 *            the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	/**
	 * @return the paxLastName
	 */
	public String getPaxLastName() {
		return paxLastName;
	}

	/**
	 * @param paxLastName
	 *            the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the redeemdAmount
	 */
	public String getRedeemdAmount() {
		return redeemdAmount;
	}

	/**
	 * @param redeemdAmount
	 *            the redeemdAmount to set
	 */
	public void setRedeemdAmount(String redeemdAmount) {
		this.redeemdAmount = redeemdAmount;
	}

	/**
	 * @return the cancelRemarks
	 */
	public String getCancelRemarks() {
		return cancelRemarks;
	}

	/**
	 * @param cancelRemarks
	 *            the cancelRemarks to set
	 */
	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	/**
	 * @return the voucherPaymentDTO
	 */
	public VoucherPaymentDTO getVoucherPaymentDTO() {
		return voucherPaymentDTO;
	}

	/**
	 * @param voucherPaymentDTO
	 *            the voucherPaymentDTO to set
	 */
	public void setVoucherPaymentDTO(VoucherPaymentDTO voucherPaymentDTO) {
		this.voucherPaymentDTO = voucherPaymentDTO;
	}

	public String getVoucherGroupId() {
		return voucherGroupId;
	}

	public void setVoucherGroupId(String voucherGroupId) {
		this.voucherGroupId = voucherGroupId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
}
