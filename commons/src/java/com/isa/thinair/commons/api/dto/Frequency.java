/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.commons.api.dto;

import java.io.Serializable;

/**
 * class to represent Frequency in the model
 * 
 * @author Lasantha Pambagoda
 */
public class Frequency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3404439303318746523L;

	private boolean day0 = false;

	private boolean day1 = false;

	private boolean day2 = false;

	private boolean day3 = false;

	private boolean day4 = false;

	private boolean day5 = false;

	private boolean day6 = false;

	/**
	 * returns the day0
	 * 
	 * @return Returns the day0.
	 * @hibernate.property column = "FREQUENCY_D0"
	 */
	public boolean getDay0() {
		return day0;
	}

	/**
	 * sets the day0
	 * 
	 * @param day0
	 *            The day0 to set.
	 */
	public void setDay0(boolean day0) {
		this.day0 = day0;
	}

	/**
	 * returns the day1
	 * 
	 * @return Returns the day1.
	 * @hibernate.property column = "FREQUENCY_D1"
	 */
	public boolean getDay1() {
		return day1;
	}

	/**
	 * sets the day1
	 * 
	 * @param day1
	 *            The day1 to set.
	 */
	public void setDay1(boolean day1) {
		this.day1 = day1;
	}

	/**
	 * returns the day2
	 * 
	 * @return Returns the day2.
	 * @hibernate.property column = "FREQUENCY_D2"
	 */
	public boolean getDay2() {
		return day2;
	}

	/**
	 * sets the day2
	 * 
	 * @param day2
	 *            The day2 to set.
	 */
	public void setDay2(boolean day2) {
		this.day2 = day2;
	}

	/**
	 * returns the day3
	 * 
	 * @return Returns the day3.
	 * @hibernate.property column = "FREQUENCY_D3"
	 */
	public boolean getDay3() {
		return day3;
	}

	/**
	 * sets the day3
	 * 
	 * @param day3
	 *            The day3 to set.
	 */
	public void setDay3(boolean day3) {
		this.day3 = day3;
	}

	/**
	 * returns the day4
	 * 
	 * @return Returns the day4.
	 * @hibernate.property column = "FREQUENCY_D4"
	 */
	public boolean getDay4() {
		return day4;
	}

	/**
	 * sets the day4
	 * 
	 * @param day4
	 *            The day4 to set.
	 */
	public void setDay4(boolean day4) {
		this.day4 = day4;
	}

	/**
	 * returns the day5
	 * 
	 * @return Returns the day5.
	 * @hibernate.property column = "FREQUENCY_D5"
	 */
	public boolean getDay5() {
		return day5;
	}

	/**
	 * sets the day5
	 * 
	 * @param day5
	 *            The day5 to set.
	 */
	public void setDay5(boolean day5) {
		this.day5 = day5;
	}

	/**
	 * returns the day6
	 * 
	 * @return Returns the day6.
	 * @hibernate.property column = "FREQUENCY_D6"
	 */
	public boolean getDay6() {
		return day6;
	}

	/**
	 * sets the day6
	 * 
	 * @param day6
	 *            The day6 to set.
	 */
	public void setDay6(boolean day6) {
		this.day6 = day6;
	}

	// @TODO implement this
	// public boolean isValid (int day){
	//
	// }
	//
	// public boolean isValid (String day) {
	//
	// }
	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append("\t day 0 set = " + getDay0() + "\n\r");
		summary.append("\t day 1 set = " + getDay1() + "\n\r");
		summary.append("\t day 2 set = " + getDay2() + "\n\r");
		summary.append("\t day 3 set = " + getDay3() + "\n\r");
		summary.append("\t day 4 set = " + getDay4() + "\n\r");
		summary.append("\t day 5 set = " + getDay5() + "\n\r");
		summary.append("\t day 6 set = " + getDay6() + "\n\r");
		return summary;
	}

	public int getDayCount(boolean state) {
		int count = 0;
		if (this.day0 == state)
			count++;
		if (this.day1 == state)
			count++;
		if (this.day2 == state)
			count++;
		if (this.day3 == state)
			count++;
		if (this.day4 == state)
			count++;
		if (this.day5 == state)
			count++;
		if (this.day6 == state)
			count++;
		return count;
	}

	@Override
	public String toString() {
		return "Frequency [Mon=" + day0 + ", Tue=" + day1 + ", Wed=" + day2
				+ ", Thu=" + day3 + ", Fri=" + day4 + ", Sat=" + day5
				+ ", Sun=" + day6 + "]";
	}
	
}
