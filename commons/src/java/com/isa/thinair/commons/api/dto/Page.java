/*
 * Created on Jul 17, 2005
 * 
 *
 */

package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Nasly
 * 
 */
public class Page<T> implements Serializable {
	private static final long serialVersionUID = -8502442667136669585L;

	private int totalNoOfRecords = 0;

	private int startPosition = 0;

	private int endPosition = 0;

	private int totalNoOfRecordsInSystem = 0;

	private Collection<T> pageData = null;

	public Page(int totalNoOfRecords, int startPosition, int endPosition, Collection<T> pageData) {
		this.totalNoOfRecords = totalNoOfRecords;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
		this.pageData = pageData;
	}

	/**
	 * 
	 * @param totalNoOfRecords
	 *            - Total records identical to the search criteria
	 * @param startPosition
	 *            - Start position of the Page
	 * @param endPosition
	 *            - End position of the Page
	 * @param totalNoOfRecordsInSystem
	 *            - Total records in the DB irrespective of the search criteria
	 * @param pageData
	 */

	public Page(int totalNoOfRecords, int startPosition, int endPosition, int totalNoOfRecordsInSystem, Collection<T> pageData) {
		this.totalNoOfRecords = totalNoOfRecords;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
		this.totalNoOfRecordsInSystem = totalNoOfRecordsInSystem;
		this.pageData = pageData;
	}

	public int getEndPosition() {
		return endPosition;
	}

	public Collection<T> getPageData() {
		return pageData;
	}

	public int getStartPosition() {
		return startPosition;
	}

	public int getTotalNoOfRecords() {
		return totalNoOfRecords;
	}

	public void setPageData(Collection<T> pageData) {
		this.pageData = pageData;
	}

	public int getTotalNoOfRecordsInSystem() {
		return totalNoOfRecordsInSystem;
	}

	public void setTotalNoOfRecordsInSystem(int totalNoOfRecordsInSystem) {
		this.totalNoOfRecordsInSystem = totalNoOfRecordsInSystem;
	}

	public void setEndPosition(int endPosition) {
		this.endPosition = endPosition;
	}

	public void setTotalNoOfRecords(int totalNoOfRecords) {
		this.totalNoOfRecords = totalNoOfRecords;
	}
}
