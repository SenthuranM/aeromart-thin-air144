/**
 * 
 */
package com.isa.thinair.commons.api.dto.ondpublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Janaka Padukka
 * 
 */
public class OndPublishOriginDTO implements Serializable {

	private OndLocationDTO ondLocation;

	private List<OndPublishDestinationDTO> destinations;

	public void addDestination(OndPublishDestinationDTO destination) {

		if (destinations == null) {
			destinations = new ArrayList<OndPublishDestinationDTO>();
		}
		destinations.add(destination);

	}

	public List<OndPublishDestinationDTO> getDestinations() {
		return destinations;
	}

	public void setDestinations(List<OndPublishDestinationDTO> destinations) {
		this.destinations = destinations;
	}

	public OndLocationDTO getOndLocation() {
		return ondLocation;
	}

	public void setOndLocation(OndLocationDTO ondLocation) {
		this.ondLocation = ondLocation;
	}
	
}
