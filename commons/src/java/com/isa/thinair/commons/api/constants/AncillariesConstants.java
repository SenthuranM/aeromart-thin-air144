package com.isa.thinair.commons.api.constants;

import java.util.Arrays;
import java.util.List;

public interface AncillariesConstants {

	enum AncillaryType {
		BAGGAGE,
		SEAT,
		AUTOMATIC_CHECKIN,
		MEAL,
		SSR_IN_FLIGHT,
		SSR_AIRPORT,
		INSURANCE,
		FLEXIBILITY,
		AIRPORT_TRANSFER;

		public static AncillaryType resolveAncillaryType(String ancillaryType) {
			return AncillaryType.valueOf(ancillaryType);
		}

        public static List<AncillaryType> getAllAncillaryTypes() {
            return Arrays.asList(values());
        }

		public String getCode() {
			return name();
		}
	}

    interface Type {
        String BAGGAGE = "BAGGAGE";
        String SEAT = "SEAT";
        String AUTOMATIC_CHECKIN="AUTOMATIC_CHECKIN";
        String MEAL = "MEAL";
        String SSR_IN_FLIGHT = "SSR_IN_FLIGHT";
        String SSR_AIRPORT = "SSR_AIRPORT";
        String INSURANCE = "INSURANCE";
        String FLEXIBILITY = "FLEXIBILITY";
        String AIRPORT_TRANSFER = "AIRPORT_TRANSFER";
    }

	enum MonetaryAmendment {
		PREVIOUS_SELECTION,
		JN_TAX,
		ANCILLARY_PENALTY,
		FREESERVICE
	}

}
