package com.isa.thinair.commons.api.dto.ets;

import java.io.Serializable;

public class ReservationControlInformation implements Serializable {

    private String airlineCode;

    private String reservationControlNumber;

    private String reservationControlType;

    private String recordCreateTimestamp;

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getReservationControlNumber() {
        return reservationControlNumber;
    }

    public void setReservationControlNumber(String reservationControlNumber) {
        this.reservationControlNumber = reservationControlNumber;
    }

    public String getReservationControlType() {
        return reservationControlType;
    }

    public void setReservationControlType(String reservationControlType) {
        this.reservationControlType = reservationControlType;
    }

    public String getRecordCreateTimestamp() {
        return recordCreateTimestamp;
    }

    public void setRecordCreateTimestamp(String recordCreateTimestamp) {
        this.recordCreateTimestamp = recordCreateTimestamp;
    }
}
