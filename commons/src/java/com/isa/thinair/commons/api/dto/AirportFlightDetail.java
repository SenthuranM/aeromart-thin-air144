package com.isa.thinair.commons.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nakhtar
 * 
 */
public class AirportFlightDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	private String airportCode;
	private Date travelDate;
	private String travelWay;
	private String salesChannel;
	private String stage;
	private String ond;
	private String flightIds;
	private Date flightDepDate;
	private String flightNumber;

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the travelDate
	 */
	public Date getTravelDate() {
		return travelDate;
	}

	/**
	 * @param travelDate
	 *            the travelDate to set
	 */
	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}

	/**
	 * @return the travelWay
	 */
	public String getTravelWay() {
		return travelWay;
	}

	/**
	 * @param travelWay
	 *            the travelWay to set
	 */
	public void setTravelWay(String travelWay) {
		this.travelWay = travelWay;
	}

	/**
	 * @return the salesChannel
	 */
	public String getSalesChannel() {
		return salesChannel;
	}

	/**
	 * @param salesChannel
	 *            the salesChannel to set
	 */
	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	/**
	 * @return the stage
	 */
	public String getStage() {
		return stage;
	}

	/**
	 * @param stage
	 *            the stage to set
	 */
	public void setStage(String stage) {
		this.stage = stage;
	}

	/**
	 * @return the ond
	 */
	public String getOnd() {
		return ond;
	}

	/**
	 * @param ond
	 *            the ond to set
	 */
	public void setOnd(String ond) {
		this.ond = ond;
	}

	/**
	 * @return the flightId
	 */
	public String getFlightIds() {
		return flightIds;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightIds(String flightIds) {
		this.flightIds = flightIds;
	}

	/**
	 * @return the flightDepDate
	 */
	public Date getFlightDepDate() {
		return flightDepDate;
	}

	/**
	 * @param flightDepDate
	 *            the flightDepDate to set
	 */
	public void setFlightDepDate(Date flightDepDate) {
		this.flightDepDate = flightDepDate;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

}
