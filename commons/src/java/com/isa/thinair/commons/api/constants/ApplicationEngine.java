package com.isa.thinair.commons.api.constants;

/**
 * Enum indicating the application engine
 * 
 */
public enum ApplicationEngine {
	IBE, XBE, WS,LCC,IOS,ANDROID
}