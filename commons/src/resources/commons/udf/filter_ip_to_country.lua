local function map_filtered(record)
	return record.country_code
end

function filter_country_by_id(stream, ipValue)
	local function filter_ip(record)
		return record.start_range <= ipValue and record.end_range >= ipValue
	end 

	return stream : filter(filter_ip) : map(map_filtered)
end
	
