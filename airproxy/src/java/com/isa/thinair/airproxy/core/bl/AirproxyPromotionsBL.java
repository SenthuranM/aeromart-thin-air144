package com.isa.thinair.airproxy.core.bl;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

/**
 * 
 * 
 * @author eshan
 * 
 */
public class AirproxyPromotionsBL {

	public static ApplicablePromotionDetailsTO pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria,
			SYSTEM prefSystem, BasicTrackInfo trackInfo, UserPrincipal userPrincipal) throws ModuleException {

		ApplicablePromotionDetailsTO applicablePromotionDetailsTO = null;

		if (prefSystem == SYSTEM.INT && (promoSelectionCriteria.getDryOperatingAirline() != null)) {
			applicablePromotionDetailsTO = AirproxyModuleUtils.getLCCSearchAndQuoteBD().pickApplicablePromotions(
					promoSelectionCriteria, trackInfo);
		} else if (prefSystem == SYSTEM.AA) {
			ServiceResponce promoResponce = AirproxyModuleUtils.getPromotionManagementBD().pickApplicablePromotion(
					promoSelectionCriteria);
			if (promoResponce.isSuccess()) {
				applicablePromotionDetailsTO = (ApplicablePromotionDetailsTO) promoResponce
						.getResponseParam(CommandParamNames.APPLICABLE_PROMOTION_DETAILS);
			}
		}

		return applicablePromotionDetailsTO;
	}
}
