package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRS;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ScheduleUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.dto.FlightInformationDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleRouteInfo;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfo;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.OndCombinationUtil;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FrequencyUtil;

public class AirproxyScheduleAvailabilityBL {

	private static Log log = LogFactory.getLog(AirproxyScheduleAvailabilityBL.class);

	/**
	 * 
	 * @param scheduleAvailRQ
	 * @param trackInfo
	 *            TODO
	 * @return ScheduleAvailRS
	 * @throws ModuleException
	 */
	public static ScheduleAvailRS getSchedules(ScheduleAvailRQ scheduleAvailRQ, BasicTrackInfo trackInfo) throws ModuleException {

		if (scheduleAvailRQ.isAAServiceRequest())
			return getAirlineFlightSchedules(scheduleAvailRQ);

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {

			ScheduleAvailRS scheduleAvailRS = AirproxyModuleUtils.getLCCSearchAndQuoteBD().getAvailableSchedules(scheduleAvailRQ,
					trackInfo);
			if (scheduleAvailRS.getFlightSchedules() != null && scheduleAvailRS.getFlightSchedules().size() > 0)
				return scheduleAvailRS;
			scheduleAvailRQ.setAAServiceRequest(false);
			return getAirlineFlightSchedules(scheduleAvailRQ);
		}

		return getAirlineFlightSchedules(scheduleAvailRQ);
	}

	/**
	 * @param scheduleSearchRQ
	 * @return
	 * @throws ModuleException
	 */
	public static ScheduleSearchRS getFlightSchedules(ScheduleSearchRQ scheduleSearchRQ) throws ModuleException {

		ScheduleSearchRS scheduleInfoRS = new ScheduleSearchRS();

		scheduleInfoRS.getOutboundScheduleRouteInfo().addAll(
				getFlightSchedules(scheduleSearchRQ.getFromDate(), scheduleSearchRQ.getToDate(),
						scheduleSearchRQ.getFromAirport(), scheduleSearchRQ.getToAirport(), scheduleSearchRQ.getAgentCode()));
		if (scheduleSearchRQ.isRoundTrip()) {
			scheduleInfoRS.getInboundScheduleRouteInfo().addAll(
					getFlightSchedules(scheduleSearchRQ.getFromDate(), scheduleSearchRQ.getToDate(),
							scheduleSearchRQ.getToAirport(), scheduleSearchRQ.getFromAirport(), scheduleSearchRQ.getAgentCode()));
		}
		return scheduleInfoRS;
	}

	/**
	 * @param fromDate
	 * @param toDate
	 * @param fromAirport
	 * @param toAirport
	 * @return
	 */
	private static Collection<FlightScheduleRouteInfo> getFlightSchedules(Date fromDate, Date toDate, String fromAirport,
			String toAirport, String agentCode) throws ModuleException {
		Collection<FlightScheduleRouteInfo> fltScheduleRouteInfo = new ArrayList<FlightScheduleRouteInfo>();

		// get available routes
		@SuppressWarnings("unchecked")
		List<RouteInfoTO> routes = AirproxyModuleUtils.getFlightBD().getAvailableRoutes(fromAirport, toAirport,
				AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);

		if (routes.isEmpty()) {
			return fltScheduleRouteInfo;
		}
		
		if (AppSysParamsUtil.isEnableRouteSelectionForAgents() && agentCode != null) {
			routes = getAgentWiseApplicableRoutes(agentCode, routes);
		}			

		for (RouteInfoTO route : routes) {

			FlightScheduleRouteInfo flightScheduleRouteInfo = new FlightScheduleRouteInfo();
			flightScheduleRouteInfo.setRoute(route.getOndCode());

			Collection<Collection<String[]>> hubWiseOndList = getOndBreakdown(route);
			for (Collection<String[]> ondList : hubWiseOndList) {
				for (String[] ond : ondList) {
					if (validSearch(ond[0], ond[1])) {
						Collection<FlightScheduleInfoDTO> flightSchedules = getScheduleInformationList(fromDate, toDate, ond[0],
								ond[1]);
						if (flightSchedules.isEmpty()) {
							// since no flight schedule found for ond[0] to ond[1] . no need to search for remaining
							// onds
							flightScheduleRouteInfo.getFlightScheduleSegmentInfo().clear();
							break;
						}
						FlightScheduleSegmentInfo flightSchduleSegmentInfo = new FlightScheduleSegmentInfo();
						flightSchduleSegmentInfo.setSegemntCode(ond[2]);

						flightSchduleSegmentInfo.getFlightSchedules().addAll(flightSchedules);
						flightScheduleRouteInfo.getFlightScheduleSegmentInfo().add(flightSchduleSegmentInfo);
					}
				}
			}
			fltScheduleRouteInfo.add(flightScheduleRouteInfo);
		}
		return fltScheduleRouteInfo;
	}

	/**
	 * @param route
	 * @return
	 */
	private static Collection<Collection<String[]>> getOndBreakdown(RouteInfoTO route) {

		Collection<Collection<String[]>> hubWiseOndList = new ArrayList<Collection<String[]>>();
		if (route.isDirectRoute()) {
			Collection<String[]> ondList = new ArrayList<String[]>();
			ondList.add(new String[] { route.getFromAirportCode(), route.getToAirportCode(), route.getOndCode() });
			hubWiseOndList.add(ondList);
		} else {
			Collection<String> connectedHubs = getConnectedHub(route);
			if (!connectedHubs.isEmpty()) {
				for (String hub : connectedHubs) {
					Collection<String[]> ondList = new ArrayList<String[]>();
					if (!route.getFromAirportCode().equals(hub) && !route.getToAirportCode().equals(hub)) {
						String[] segments = route.getOndCode().split(hub);
						ondList.add(new String[] { route.getFromAirportCode(), hub, (segments[0] + hub) });
						ondList.add(new String[] { hub, route.getToAirportCode(), (hub + segments[1]) });
					} else {
						ondList.add(new String[] { route.getFromAirportCode(), route.getToAirportCode(), route.getOndCode() });
					}
					hubWiseOndList.add(ondList);
				}
			} else {
				Collection<String[]> ondList = new ArrayList<String[]>();
				ondList.add(new String[] { route.getFromAirportCode(), route.getToAirportCode(), route.getOndCode() });
				hubWiseOndList.add(ondList);
			}
		}
		return hubWiseOndList;
	}

	/**
	 * @param route
	 * @return
	 */
	private static Collection<String> getConnectedHub(RouteInfoTO route) {
		@SuppressWarnings("unchecked")
		Map<Integer, HubAirportTO> hubMap = CommonsServices.getGlobalConfig().getHubsMap();
		Iterator<HubAirportTO> hubIte = hubMap.values().iterator();

		Collection<String> conectedHubs = new ArrayList<String>();

		while (hubIte.hasNext()) {
			HubAirportTO hub = hubIte.next();
			for (TransitAirport airPort : route.getTransitAirportCodesList()) {
				if (airPort.getAirportCode().equals(hub.getAirportCode())) {
					conectedHubs.add(hub.getAirportCode());
				}
			}
		}
		return conectedHubs;
	}

	/**
	 * @param fromAirport
	 * @param toAirport
	 * @return
	 */
	private static boolean validSearch(String fromAirport, String toAirport) {

		@SuppressWarnings("unchecked")
		Map<Integer, HubAirportTO> hubMap = CommonsServices.getGlobalConfig().getHubsMap();
		Iterator<HubAirportTO> hubIte = hubMap.values().iterator();

		while (hubIte.hasNext()) {
			HubAirportTO hub = hubIte.next();
			if (fromAirport.equals(hub.getAirportCode()) || toAirport.equals(hub.getAirportCode())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * 
	 * @param scheduleAvailRQ
	 * @param userPrincipal
	 * @return ScheduleAvailRS
	 * @throws ModuleException
	 */
	public static ScheduleAvailRS getAirlineFlightSchedules(ScheduleAvailRQ scheduleAvailRQ) throws ModuleException {
		ScheduleAvailRS scheduleAvailRS = new ScheduleAvailRS();

		FlightInformationDTO dto = getFlightSchedulesInfomation(scheduleAvailRQ.getStartDateTime(),
				scheduleAvailRQ.getStopDateTime(), scheduleAvailRQ.getOriginLocation(), scheduleAvailRQ.getDestLocation(),
				scheduleAvailRQ.isRoundTrip(), scheduleAvailRQ.getArSHJ(), scheduleAvailRQ.getArFrom(), scheduleAvailRQ.getArTo());

		if (!scheduleAvailRQ.isAAServiceRequest()) {
			Collection<Collection<Collection<FlightScheduleInfoDTO>>> colFlights = dto.getFlightInfoList();
			Collection<Collection<Collection<FlightScheduleInfoDTO>>> colReturnFlights = dto.getFlightInfoReturnCol();

			Collection<Collection<FlightScheduleInfoDTO>> newFlightsCol = new ArrayList<Collection<FlightScheduleInfoDTO>>();

			for (Iterator iterator = colFlights.iterator(); iterator.hasNext();) {
				Collection collection = (Collection) iterator.next();
				for (Iterator iter = collection.iterator(); iter.hasNext();) {
					newFlightsCol.add((Collection) iter.next());
				}
			}
			scheduleAvailRS.setFlightSchedules(newFlightsCol);

			if (scheduleAvailRQ.isRoundTrip()) {
				newFlightsCol = new ArrayList<Collection<FlightScheduleInfoDTO>>();
				Object[] listArr = colReturnFlights.toArray();
				for (int j = listArr.length - 1; j >= 0; j--) {
					Collection collection = (Collection) listArr[j];
					for (Iterator iter = collection.iterator(); iter.hasNext();) {
						newFlightsCol.add((Collection) iter.next());
					}
				}

				scheduleAvailRS.setFlightSchedulesReturns(newFlightsCol);
			}

		} else {
			scheduleAvailRS.setFlightSchedules(dto.getFlightInfoList());
			scheduleAvailRS.setFlightSchedulesReturns(dto.getFlightInfoReturnCol());
		}

		if (!scheduleAvailRQ.isAAServiceRequest()) {
			List<Collection<FlightScheduleInfoDTO>> flightOneWay = (List) scheduleAvailRS.getFlightSchedules();
			List<Collection<FlightScheduleInfoDTO>> flightReturnWay = (List) scheduleAvailRS.getFlightSchedulesReturns();

			try {

				if (flightOneWay != null && !flightOneWay.isEmpty()) {
					for (int i = 0; i < flightOneWay.size(); i++) {
						List colOfInfoDTO = (List) flightOneWay.get(i);
						for (int j = 0; j < colOfInfoDTO.size(); j++) {
							FlightScheduleInfoDTO fliInfoDTO = (FlightScheduleInfoDTO) colOfInfoDTO.get(j);
							if (fliInfoDTO == null)
								flightOneWay.remove(colOfInfoDTO);
						}
						if (colOfInfoDTO == null || colOfInfoDTO.isEmpty()) {
							scheduleAvailRS.setFlightSchedules(new ArrayList<Collection<FlightScheduleInfoDTO>>());
							break;
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				if (flightReturnWay != null && !flightReturnWay.isEmpty()) {
					for (int i = 0; i < flightReturnWay.size(); i++) {
						List colOfInfoDTO = (List) flightReturnWay.get(i);
						for (int j = 0; j < colOfInfoDTO.size(); j++) {
							FlightScheduleInfoDTO fliInfoDTO = (FlightScheduleInfoDTO) colOfInfoDTO.get(j);
							if (fliInfoDTO == null)
								flightReturnWay.remove(colOfInfoDTO);
						}
						if (colOfInfoDTO == null || colOfInfoDTO.isEmpty()) {
							scheduleAvailRS.setFlightSchedulesReturns(new ArrayList<Collection<FlightScheduleInfoDTO>>());
							break;
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return scheduleAvailRS;
	}

	public static FlightInformationDTO getFlightSchedulesInfomation(Date startDateTime, Date stopDateTime, String origin,
			String destination, boolean roundTrip, String arSHJ, String arFrom, String arTo) throws ModuleException {

		FlightInformationDTO flightInformationDTO = new FlightInformationDTO();

		String strFrom = origin;
		String strTo = destination;

		List<String> dstFrom = new ArrayList<String>();
		List<String> dstTo = new ArrayList<String>();
		List<TransitAirport> transitAirports = new ArrayList<TransitAirport>();

		dstFrom.add(strFrom);
		String commonHub = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		List<RouteInfoTO> routes = AirproxyModuleUtils.getFlightBD().getAvailableRoutes(origin, destination,
				AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		for (RouteInfoTO route : routes) {
			if (!route.isDirectRoute()) {
				transitAirports.addAll(route.getTransitAirportCodesList());
			}
		}
		if (transitAirports.isEmpty()){
			transitAirports.add(new TransitAirport(commonHub, false, null));
		}

		String directFrom = null;
		String directTo = null;

		for (TransitAirport transitAirport : transitAirports) {
			String hub = transitAirport.getAirportCode();
			if (!strFrom.equalsIgnoreCase(hub)) {
				dstTo.add(hub);
				if (!strTo.equalsIgnoreCase(hub)) {
					dstFrom.add(hub);
					dstTo.add(strTo);
	
					directFrom = strFrom;
					directTo = strTo;
				}
			} else {
				dstTo.add(strTo);
			}
		}

		int len = dstFrom.size();

		for (int j = 0; j < len; j++) {
			Collection<FlightScheduleInfoDTO> colFlightDetails = new ArrayList();
			for (FlightScheduleInfoDTO flightScheduleInfoDTO : getScheduleInformationList(startDateTime, stopDateTime,
					dstFrom.get(j), dstTo.get(j))) {
				colFlightDetails.add(flightScheduleInfoDTO);
			}

			if (colFlightDetails.size() == 0) {
				flightInformationDTO.getFlightInfoList().clear();
				break;
			}

			Collection<Collection<FlightScheduleInfoDTO>> colFlightsInfo = new ArrayList<Collection<FlightScheduleInfoDTO>>();
			colFlightsInfo.add(colFlightDetails);
			flightInformationDTO.getFlightInfoList().add(colFlightsInfo);
		}

		flightInformationDTO = checkAndAddForDirect(flightInformationDTO, startDateTime, stopDateTime, directFrom, directTo);

		if (roundTrip) {
			for (int j = len - 1; j >= 0; j--) {
				Collection<FlightScheduleInfoDTO> colFlightDetails = new ArrayList();
				for (FlightScheduleInfoDTO flightScheduleInfoDTO : getScheduleInformationList(startDateTime, stopDateTime,
						dstTo.get(j), dstFrom.get(j))) {
					colFlightDetails.add(flightScheduleInfoDTO);
				}

				if (colFlightDetails.size() == 0) {
					flightInformationDTO.getFlightInfoReturnCol().clear();
					break;
				}

				Collection<Collection<FlightScheduleInfoDTO>> colFlightsReturnInfo = new ArrayList<Collection<FlightScheduleInfoDTO>>();
				colFlightsReturnInfo.add(colFlightDetails);
				flightInformationDTO.getFlightInfoReturnCol().add(colFlightsReturnInfo);
			}

			flightInformationDTO = checkAndAddForDirect(flightInformationDTO, startDateTime, stopDateTime, directTo, directFrom);
		}

		return flightInformationDTO;
	}

	private static FlightInformationDTO checkAndAddForDirect(FlightInformationDTO flightInformationDTO, Date startDateTime,
			Date stopDateTime, String strFrom, String strTo) throws ModuleException {

		strFrom = BeanUtils.nullHandler(strFrom);
		strTo = BeanUtils.nullHandler(strTo);

		if (strFrom.length() > 0 && strTo.length() > 0) {
			Collection<FlightScheduleInfoDTO> colFlightDetails = new ArrayList();
			for (FlightScheduleInfoDTO flightScheduleInfoDTO : getScheduleInformationList(startDateTime, stopDateTime, strFrom,
					strTo)) {
				colFlightDetails.add(flightScheduleInfoDTO);
			}
			if (colFlightDetails.size() > 0) {
				Collection<Collection<FlightScheduleInfoDTO>> colFlightsInfo = new ArrayList<Collection<FlightScheduleInfoDTO>>();
				colFlightsInfo.add(colFlightDetails);
				flightInformationDTO.getFlightInfoList().add(colFlightsInfo);
			}
		}

		return flightInformationDTO;
	}

	private static Collection<FlightScheduleInfoDTO> getScheduleInformationList(Date dStartDate, Date dStopDate, String strFrom,
			String strTo) throws ModuleException {
		Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();

		// fields not relevant to the default search
		String strOperationtype = "2";
		String strStatus = "ACT";
		String strBuildStatus = "BLT";

		// setting the criteria list to search
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		// setting the start date as criteria
		if (ScheduleUtil.isNotNull(dStartDate)) {

			List<Date> valueStartDate = new ArrayList<Date>();
			valueStartDate.add(dStartDate);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE, valueStartDate));

		}
		// setting the stop date as criteria
		if (ScheduleUtil.isNotNull(dStopDate)) {

			List<Date> valueStopDate = new ArrayList<Date>();
			valueStopDate.add(dStopDate);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE, valueStopDate));

		}
		// setting the operation type as criteria
		if (ScheduleUtil.isNotEmptyOrNull(strOperationtype)) {

			List<Integer> valueOptType = new ArrayList<Integer>();
			valueOptType.add(new Integer(strOperationtype));

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.OPERATION_TYPE_ID, valueOptType));

		}
		// setting the schedule status as criteria
		if (ScheduleUtil.isNotEmptyOrNull(strStatus)) {

			List<String> valueStatus = new ArrayList<String>();
			valueStatus.add(strStatus);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.STATUS_CODE, valueStatus));

		}
		// setting the schedule build status as criteria
		if (ScheduleUtil.isNotEmptyOrNull(strBuildStatus)) {

			List<String> valueBuildStatus = new ArrayList<String>();
			valueBuildStatus.add(strBuildStatus);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.BUILD_STATUS_CODE, valueBuildStatus));

		}

		if (ScheduleUtil.isNotEmptyOrNull(strFrom)) {

			List<String> valueFromStn = new ArrayList<String>();
			valueFromStn.add(strFrom);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.DEPARTURE_APT_CODE, valueFromStn));

		}
		// setting the arrival airport code as criteria
		if (ScheduleUtil.isNotEmptyOrNull(strTo)) {

			List<String> valueToStn = new ArrayList<String>();
			valueToStn.add(strTo);

			critrian.add(ScheduleUtil.createModuleCriterion(ModuleCriterion.CONDITION_EQUALS,
					ModelNamesAndFieldNames.FlightSchedule.FieldNames.ARRIVAL_APT_CODE, valueToStn));

		}

		boolean localtime = true;
		int pageSize = 20;
		int startIndex = 0;
		int indexSeq = 1;

		Page[] page = AirproxyModuleUtils.getScheduleBD().searchAndReassembleSchedules(critrian, startIndex, pageSize, localtime,
				true);
		Collection<FlightSchedule> flightScheCollection = page[0].getPageData();

		for (FlightSchedule schedule : flightScheCollection) {
			FlightScheduleInfoDTO flightScheduleInfoDTO = new FlightScheduleInfoDTO();
			flightScheduleInfoDTO.setScheduleId(schedule.getScheduleId());
			flightScheduleInfoDTO.setFlightNumber(schedule.getFlightNumber());
			flightScheduleInfoDTO.setStartDate(schedule.getStartDate());
			flightScheduleInfoDTO.setStopDate(schedule.getStopDate());
			flightScheduleInfoDTO.setStartDateLocal(schedule.getStartDateLocal());
			flightScheduleInfoDTO.setStopDateLocal(schedule.getStopDateLocal());

			flightScheduleInfoDTO.setOverlapingScheduleId(schedule.getOverlapingScheduleId());
			flightScheduleInfoDTO.setAvailableSeatKilometers(schedule.getAvailableSeatKilometers());
			flightScheduleInfoDTO.setNumberOfDepartures(schedule.getNumberOfDepartures());

			flightScheduleInfoDTO.setDepartureStnCode(schedule.getDepartureStnCode());
			flightScheduleInfoDTO.setArrivalStnCode(schedule.getArrivalStnCode());
			flightScheduleInfoDTO.setModelNumber(schedule.getModelNumber());
			flightScheduleInfoDTO.setOperationTypeId(schedule.getOperationTypeId());
			flightScheduleInfoDTO.setBuildStatusCode(schedule.getBuildStatusCode());
			flightScheduleInfoDTO.setStatusCode(schedule.getStatusCode());
			flightScheduleInfoDTO.setManuallyChanged(schedule.getManuallyChanged());

			Airport depAirport = AirproxyModuleUtils.getAirportBD().getAirport(schedule.getDepartureStnCode());
			Airport arrAirport = AirproxyModuleUtils.getAirportBD().getAirport(schedule.getArrivalStnCode());

			flightScheduleInfoDTO.setArrivalAptName(schedule.getArrivalStnCode());
			flightScheduleInfoDTO.setDepartureAptName(schedule.getDepartureStnCode());
			if (depAirport != null && depAirport.getAirportName() != null && depAirport.getAirportName().compareTo("") != 0)
				flightScheduleInfoDTO.setDepartureAptName(depAirport.getAirportName());

			if (arrAirport != null && arrAirport.getAirportName() != null && arrAirport.getAirportName().compareTo("") != 0)
				flightScheduleInfoDTO.setArrivalAptName(arrAirport.getAirportName());

			flightScheduleInfoDTO.setLocalChange(schedule.getLocalChange());
			flightScheduleInfoDTO.setGdsIds(schedule.getGdsIds());
			flightScheduleInfoDTO.setNumberOfOpenFlights(schedule.getNumberOfFlights());

			flightScheduleInfoDTO.setFrequency(schedule.getFrequency());
			flightScheduleInfoDTO.setFrequencyLocal(schedule.getFrequencyLocal());
			flightScheduleInfoDTO.setSequenceId(indexSeq);

			Collection<FlightScheduleLeg> flightScheduleLegs = schedule.getFlightScheduleLegs();
			Set<FlightScheduleLegInfoDTO> flightSet = new HashSet<FlightScheduleLegInfoDTO>();

			for (FlightScheduleLeg legs : flightScheduleLegs) {
				FlightScheduleLegInfoDTO dto = new FlightScheduleLegInfoDTO();
				dto.setId(legs.getId());
				dto.setScheduleId(legs.getScheduleId());
				dto.setLegNumber(legs.getLegNumber());
				dto.setEstDepartureTimeZulu(legs.getEstDepartureTimeZulu());
				dto.setEstArrivalTimeZulu(legs.getEstArrivalTimeZulu());

				dto.setEstDepartureTimeLocal(legs.getEstDepartureTimeLocal());
				dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());
				dto.setDuration(legs.getDuration());

				dto.setEstDepartureDayOffset(legs.getEstDepartureDayOffset());
				dto.setEstArrivalDayOffset(legs.getEstArrivalDayOffset());

				dto.setEstDepartureDayOffsetLocal(legs.getEstDepartureDayOffsetLocal());
				dto.setEstArrivalDayOffsetLocal(legs.getEstArrivalDayOffsetLocal());

				dto.setOrigin(legs.getOrigin());
				dto.setDestination(legs.getDestination());
				dto.setModelRouteId(legs.getModelRouteId());

				flightSet.add(dto);
			}

			flightScheduleInfoDTO.setFlightScheduleLegs(flightSet);

			Collection<FlightScheduleSegment> flightScheduleSegments = schedule.getFlightScheduleSegments();
			Set<FlightScheduleSegmentInfoDTO> segmentDTOs = new HashSet<FlightScheduleSegmentInfoDTO>();

			for (FlightScheduleSegment segment : flightScheduleSegments) {
				FlightScheduleSegmentInfoDTO dto = new FlightScheduleSegmentInfoDTO();
				dto.setFlSchSegId(segment.getFlSchSegId());
				dto.setScheduleId(segment.getScheduleId());
				dto.setOverlapSegmentId(segment.getOverlapSegmentId());
				dto.setSegmentCode(segment.getSegmentCode());
				dto.setValidFlag(segment.getValidFlag());

				segmentDTOs.add(dto);
			}

			flightScheduleInfoDTO.setFlightScheduleSegments(segmentDTOs);

			colFlightScheduleInfoDTOs.add(flightScheduleInfoDTO);
			indexSeq++;
		}

		Collection<Flight> adhocFlights = page[1].getPageData();

		if (adhocFlights != null) {

			for (Flight flight : adhocFlights) {
				FlightScheduleInfoDTO flightScheduleInfoDTO = new FlightScheduleInfoDTO();
				flightScheduleInfoDTO.setScheduleId(flight.getScheduleId());
				flightScheduleInfoDTO.setFlightNumber(flight.getFlightNumber());
				flightScheduleInfoDTO.setStartDate(flight.getDepartureDate());
				flightScheduleInfoDTO.setStopDate(flight.getDepartureDate());
				flightScheduleInfoDTO.setStartDateLocal(flight.getDepartureDateLocal());
				flightScheduleInfoDTO.setStopDateLocal(flight.getDepartureDateLocal());

				flightScheduleInfoDTO.setOverlapingScheduleId(null);
				flightScheduleInfoDTO.setAvailableSeatKilometers(flight.getAvailableSeatKilometers());

				flightScheduleInfoDTO.setDepartureStnCode(flight.getOriginAptCode());
				flightScheduleInfoDTO.setArrivalStnCode(flight.getDestinationAptCode());
				flightScheduleInfoDTO.setModelNumber(flight.getModelNumber());
				flightScheduleInfoDTO.setBuildStatusCode(null);
				flightScheduleInfoDTO.setNumberOfDepartures(1);

				flightScheduleInfoDTO.setOperationTypeId(flight.getOperationTypeId());
				flightScheduleInfoDTO.setStatusCode(flight.getStatus());
				flightScheduleInfoDTO.setManuallyChanged(flight.getManuallyChanged());

				Airport depAirport = AirproxyModuleUtils.getAirportBD().getAirport(flight.getOriginAptCode());
				Airport arrAirport = AirproxyModuleUtils.getAirportBD().getAirport(flight.getDestinationAptCode());

				if (depAirport != null && depAirport.getAirportName() != null && depAirport.getAirportName().compareTo("") != 0)
					flightScheduleInfoDTO.setDepartureAptName(depAirport.getAirportName());

				if (arrAirport != null && arrAirport.getAirportName() != null && arrAirport.getAirportName().compareTo("") != 0)
					flightScheduleInfoDTO.setArrivalAptName(arrAirport.getAirportName());

				flightScheduleInfoDTO.setDepartureAptName(flight.getOriginAptCode());
				flightScheduleInfoDTO.setArrivalAptName(flight.getDestinationAptCode());

				flightScheduleInfoDTO.setGdsIds(flight.getGdsIds());
				flightScheduleInfoDTO.setNumberOfOpenFlights(1);

				int dayNumber = flight.getDayNumber();
				Frequency frequency = new Frequency();
				if (dayNumber == 0)
					frequency.setDay0(true);
				if (dayNumber == 1)
					frequency.setDay1(true);
				if (dayNumber == 2)
					frequency.setDay2(true);
				if (dayNumber == 3)
					frequency.setDay3(true);
				if (dayNumber == 4)
					frequency.setDay4(true);
				if (dayNumber == 5)
					frequency.setDay5(true);
				if (dayNumber == 6)
					frequency.setDay6(true);
				flightScheduleInfoDTO.setFrequency(frequency);
				flightScheduleInfoDTO.setSequenceId(indexSeq);
				Date depDateZulu = flight.getDepartureDate();
				Date depDateLocal = flight.getDepartureDateLocal();
				int dif = CalendarUtil.daysUntil(depDateZulu, depDateLocal);
				Frequency frequencyLocal = FrequencyUtil.getCopyOfFrequency(frequency);
				frequencyLocal = FrequencyUtil.shiftFrequencyBy(frequencyLocal, dif);
				flightScheduleInfoDTO.setFrequencyLocal(frequencyLocal);

				Collection<FlightLeg> flightLegs = flight.getFlightLegs();
				Set<FlightScheduleLegInfoDTO> flightSet = new HashSet<FlightScheduleLegInfoDTO>();

				for (FlightLeg legs : flightLegs) {
					FlightScheduleLegInfoDTO dto = new FlightScheduleLegInfoDTO();
					dto.setId(legs.getId());
					dto.setScheduleId(null);
					dto.setLegNumber(legs.getLegNumber());
					dto.setEstDepartureTimeZulu(legs.getEstDepartureTimeZulu());
					dto.setEstArrivalTimeZulu(legs.getEstArrivalTimeZulu());

					dto.setEstDepartureTimeLocal(legs.getEstDepartureTimeLocal());
					dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());
					dto.setDuration(legs.getDuration());

					dto.setEstDepartureDayOffset(legs.getEstDepartureDayOffset());
					dto.setEstArrivalDayOffset(legs.getEstArrivalDayOffset());

					dto.setEstDepartureDayOffsetLocal(legs.getEstDepartureDayOffsetLocal());
					dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());

					dto.setOrigin(legs.getOrigin());
					dto.setDestination(legs.getDestination());
					dto.setModelRouteId(legs.getModelRouteId());

					flightSet.add(dto);
				}

				flightScheduleInfoDTO.setFlightScheduleLegs(flightSet);

				Collection<FlightSegement> flightSegments = flight.getFlightSegements();
				Set<FlightScheduleSegmentInfoDTO> segmentDTOs = new HashSet<FlightScheduleSegmentInfoDTO>();

				for (FlightSegement segment : flightSegments) {
					FlightScheduleSegmentInfoDTO dto = new FlightScheduleSegmentInfoDTO();
					dto.setFlSchSegId(segment.getFltSegId());
					dto.setScheduleId(null);
					dto.setOverlapSegmentId(segment.getOverlapSegmentId());
					dto.setSegmentCode(segment.getSegmentCode());
					dto.setValidFlag(segment.getValidFlag());
					segmentDTOs.add(dto);
				}
				flightScheduleInfoDTO.setFlightScheduleSegments(segmentDTOs);

				colFlightScheduleInfoDTOs.add(flightScheduleInfoDTO);
				indexSeq++;
			}
		}

		return colFlightScheduleInfoDTOs;
	}
	
	private static List<RouteInfoTO> getAgentWiseApplicableRoutes(String agentCode, List<RouteInfoTO> routeInfoList)
			throws ModuleException {
		List<RouteInfoTO> fliteredRouteList = new ArrayList<RouteInfoTO>();
		if (!routeInfoList.isEmpty() && agentCode != null) {
			List<String> agentApplicableRoutes = AirSchedulesUtil.getTravelAgentBD().getAgentWiseApplicableRoutes(agentCode);
			for (RouteInfoTO routeInfo : routeInfoList) {
				List<String> airports = Arrays.asList(routeInfo.getOndCode().split("\\s*/\\s*"));
				List<String> applicableOndCodeSelections = OndCombinationUtil.getApplicableOndCodeSelections(airports);
				if (!Collections.disjoint(agentApplicableRoutes, applicableOndCodeSelections)) {
					fliteredRouteList.add(routeInfo);
				}
			}
		}
		return fliteredRouteList;
	}
}
