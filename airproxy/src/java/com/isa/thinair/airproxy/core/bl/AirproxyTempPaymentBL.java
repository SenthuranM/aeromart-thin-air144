package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AirproxyTempPaymentBL {

	private String groupPNR;
	private CommonReservationContactInfo reservationContactInfo;
	private LCCClientPaymentAssembler paymentAssembler;
	private boolean isCredit;
	private TrackInfoDTO trackInfoDTO;
	private SYSTEM system;
	private String originatorCarrierCode;
	private boolean isCsOcFlightAvailable;

	public AirproxyTempPaymentBL(String groupPNR, CommonReservationContactInfo reservationContactInfo,
			LCCClientPaymentAssembler paymentAssembler, boolean isCredit, TrackInfoDTO trackInfoDTO, SYSTEM system,
			String originatorCarrierCode, boolean isCsOcFlightAvailable) {
		super();
		this.groupPNR = groupPNR;
		this.reservationContactInfo = reservationContactInfo;
		this.paymentAssembler = paymentAssembler;
		this.isCredit = isCredit;
		this.trackInfoDTO = trackInfoDTO;
		this.system = system;
		this.originatorCarrierCode = originatorCarrierCode;
		this.setCsOcFlightAvailable(isCsOcFlightAvailable);
	}

	public Map<Integer, CommonCreditCardPaymentInfo> makeTemporaryPayment() throws ModuleException {
		if (system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCReservationBD().makeTemporyPaymentEntry(groupPNR, reservationContactInfo,
					paymentAssembler.getPayments(), isCredit, originatorCarrierCode, trackInfoDTO);
		} else {
			return makeOwnAirlineTempPayment();
		}
	}

	private Map<Integer, CommonCreditCardPaymentInfo> makeOwnAirlineTempPayment() throws ModuleException {

		IPayment iPayment = new PaymentAssembler();

		iPayment.addExternalCharges(tranformExternalCharges(this.getPaymentAssembler().getPerPaxExternalCharges()));
		BigDecimal consumedExternalCharges = paymentAssembler.getTotalConsumedExternalCharges();
		boolean isOfflinePayment = false;
		for (LCCClientPaymentInfo payment : this.getPaymentAssembler().getPayments()) {
			BigDecimal totalWOExtCharges = BigDecimal.ZERO;
			if (payment.includeExternalCharges()) {
				totalWOExtCharges = AccelAeroCalculator.subtract(payment.getTotalAmount(), consumedExternalCharges);
			} else {
				totalWOExtCharges = payment.getTotalAmount();
			}
			if (payment instanceof CommonCreditCardPaymentInfo) {
				if (!isOfflinePayment) {
					isOfflinePayment = payment.isOfflinePayment();
				}
				CommonCreditCardPaymentInfo creditCardPayment = (CommonCreditCardPaymentInfo) payment;
				iPayment.addCardPayment(creditCardPayment.getType(), creditCardPayment.geteDate(), creditCardPayment.getNo(),
						creditCardPayment.getName(), "", creditCardPayment.getSecurityCode(), totalWOExtCharges,
						creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode(), null,
						creditCardPayment.getIpgIdentificationParamsDTO(), null, creditCardPayment.getPayCurrencyDTO(), null,
						null, null, null);
			}
		}

		Map<Integer, CardPaymentInfo> mapTempPayMap = AirproxyModuleUtils.getReservationBD().makeTemporyPaymentEntry(
				this.getGroupPNR(), tranformContactInfo(), iPayment, isCredit, trackInfoDTO, isOfflinePayment, isCsOcFlightAvailable);

		return transformCreditInfoMap(mapTempPayMap);
	}

	private Map<Integer, CommonCreditCardPaymentInfo> transformCreditInfoMap(Map<Integer, CardPaymentInfo> mapTempPayMap) {
		Set<Integer> tempPayIds = mapTempPayMap.keySet();
		Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = new HashMap<Integer, CommonCreditCardPaymentInfo>();
		for (Integer payId : tempPayIds) {
			CardPaymentInfo cardPayInfo = mapTempPayMap.get(payId);

			CommonCreditCardPaymentInfo ccPayInfo = new CommonCreditCardPaymentInfo();
			ccPayInfo.setNo(cardPayInfo.getNo());
			ccPayInfo.seteDate(cardPayInfo.getEDate());
			ccPayInfo.setGroupPNR(cardPayInfo.getPnr());
			ccPayInfo.setAddress(cardPayInfo.getAddress());
			ccPayInfo.setAppIndicator(cardPayInfo.getAppIndicator());
			ccPayInfo.setAuthorizationId(cardPayInfo.getAuthorizationId());
			ccPayInfo.setCardHoldersName(cardPayInfo.getName());
			ccPayInfo.setIpgIdentificationParamsDTO(cardPayInfo.getIpgIdentificationParamsDTO());
			ccPayInfo.setName(cardPayInfo.getName());
			ccPayInfo.setPayCurrencyAmount(cardPayInfo.getTotalAmount());
			ccPayInfo.setPayCurrencyDTO(cardPayInfo.getPayCurrencyDTO());
			ccPayInfo.setPaymentBrokerId(cardPayInfo.getPaymentBrokerRefNo());
			ccPayInfo.setTnxMode(cardPayInfo.getTnxMode());
			ccPayInfo.setType(cardPayInfo.getType());
			ccPayInfo.setSecurityCode(cardPayInfo.getSecurityCode());
			ccPayInfo.setTemporyPaymentId(cardPayInfo.getTemporyPaymentId());
			tempPayMap.put(payId, ccPayInfo);
		}
		return tempPayMap;
	}

	private Collection<ExternalChgDTO> tranformExternalCharges(Collection<LCCClientExternalChgDTO> paxWiseExtChgs) {
		Collection<ExternalChgDTO> extChgDTOs = new ArrayList<ExternalChgDTO>();
		for (LCCClientExternalChgDTO chg : paxWiseExtChgs) {
			ExternalChgDTO extChg = new ExternalChgDTO();
			extChg.setAmount(chg.getAmount());
			extChg.setExternalChargesEnum(chg.getExternalCharges());
			extChg.setAmountConsumedForPayment(false);
			extChg.setChargeCode(chg.getCode());
			extChgDTOs.add(extChg);
		}
		return extChgDTOs;
	}

	private ReservationContactInfo tranformContactInfo() {
		CommonReservationContactInfo conInfo = this.getReservationContactInfo();
		ReservationContactInfo resConInfo = new ReservationContactInfo();
		resConInfo.setFirstName(conInfo.getFirstName());
		resConInfo.setLastName(conInfo.getLastName());
		resConInfo.setNationalityCode(conInfo.getNationalityCode() + "");
		resConInfo.setMobileNo(conInfo.getMobileNo());
		resConInfo.setPhoneNo(conInfo.getPhoneNo());
		resConInfo.setTitle(conInfo.getTitle());
		resConInfo.setStreetAddress1(conInfo.getStreetAddress1());
		resConInfo.setStreetAddress2(conInfo.getStreetAddress2());
		resConInfo.setCity(conInfo.getCity());
		resConInfo.setState(conInfo.getState());
		resConInfo.setCountryCode(conInfo.getCountryCode());
		resConInfo.setEmail(conInfo.getEmail());
		resConInfo.setFax(conInfo.getFax());
		resConInfo.setCustomerId(conInfo.getCustomerId());
		return resConInfo;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public CommonReservationContactInfo getReservationContactInfo() {
		return reservationContactInfo;
	}

	public void setReservationContactInfo(CommonReservationContactInfo reservationContactInfo) {
		this.reservationContactInfo = reservationContactInfo;
	}

	public boolean isCredit() {
		return isCredit;
	}

	public void setCredit(boolean isCredit) {
		this.isCredit = isCredit;
	}

	public LCCClientPaymentAssembler getPaymentAssembler() {
		return paymentAssembler;
	}

	public void setPaymentAssembler(LCCClientPaymentAssembler paymentAssembler) {
		this.paymentAssembler = paymentAssembler;
	}

	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getOriginatorCarrierCode() {
		return originatorCarrierCode;
	}

	public void setOriginatorCarrierCode(String originatorCarrierCode) {
		this.originatorCarrierCode = originatorCarrierCode;
	}

	public boolean isCsOcFlightAvailable() {
		return isCsOcFlightAvailable;
	}

	public void setCsOcFlightAvailable(boolean isCsOcFlightAvailable) {
		this.isCsOcFlightAvailable = isCsOcFlightAvailable;
	}
}
