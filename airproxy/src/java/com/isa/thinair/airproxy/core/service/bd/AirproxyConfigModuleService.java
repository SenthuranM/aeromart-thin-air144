package com.isa.thinair.airproxy.core.service.bd;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @isa.module.service-interface module-name="airproxy" description="AccelAero proxy for airline and lcc mediation"
 */
public class AirproxyConfigModuleService extends DefaultModule {

}
