package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airproxy.api.service.AirproxyVoucherBD;

/**
 * @author chethiya
 *
 */
@Local
public interface AirproxyVoucherBDLocal extends AirproxyVoucherBD {

}
