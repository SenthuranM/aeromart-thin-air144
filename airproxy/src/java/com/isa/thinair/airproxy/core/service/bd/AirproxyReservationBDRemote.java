package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airproxy.api.service.AirproxyReservationAllBD;

@Remote
public interface AirproxyReservationBDRemote extends AirproxyReservationAllBD {

}
