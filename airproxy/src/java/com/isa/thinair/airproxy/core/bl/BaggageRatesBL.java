package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.BaggageRatesTransformer;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class BaggageRatesBL {

	private TrackInfoDTO trackInfoDTO;
	private BaseAvailRS flightPriceRS;
	private String cabinClass;
	private String logicalCabinClass;
	private String selectedSystem;
	private ApplicationEngine applicationEngine;
	private UserPrincipal userPrincipal;
	private String locale;

	public BaggageRatesDTO execute() throws ModuleException {
		Collection<LCCBaggageResponseDTO> lccBaggageResponseDTOs = new ArrayList<>();
		for (OriginDestinationInformationTO ondInfoTO : flightPriceRS.getOriginDestinationInformationList()) {
			OND_OPTION: for (OriginDestinationOptionTO ondOptionTo : ondInfoTO.getOrignDestinationOptions()) {

				if (!ondOptionTo.isSelected()) {
					continue OND_OPTION;
				}
				LCCBaggageResponseDTO lccBaggageResponseDTO = null;
				List<FlightSegmentTO> flightSegmentTOs = ondOptionTo.getFlightSegmentList();
				LCCReservationBaggageSummaryTo lCCReservationBaggageSummaryTo = new LCCReservationBaggageSummaryTo();
				Map<String, String> bookingClasses = new HashMap<String, String>();
				Map<String, String> classesOfServices = new HashMap<String, String>();

				flightSegmentTOs
						.forEach(flightSegmentTo -> classesOfServices.put(flightSegmentTo.getFlightRefNumber(), cabinClass));

				flightSegmentTOs.forEach(
						flightSegmentTo -> classesOfServices.put(flightSegmentTo.getFlightRefNumber(), logicalCabinClass));
				bookingClasses.put("DummyVal", "DummyBC");
				lCCReservationBaggageSummaryTo.setBookingClasses(bookingClasses);
				lCCReservationBaggageSummaryTo.setClassesOfService(classesOfServices);

				String strTxnIdntifier = null;
				boolean modifyAncillary = false;
				boolean isRequote = false;
				String pnr = null;

				List<BundledFareDTO> bundleFares = null;
				if (flightPriceRS.getSelectedPriceFlightInfo() != null
						&& flightPriceRS.getSelectedPriceFlightInfo().getFareSegChargeTO() != null) {
					bundleFares = flightPriceRS.getSelectedPriceFlightInfo().getFareSegChargeTO().getOndBundledFareDTOs();
				} else if (flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
					bundleFares = flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO().getOndBundledFareDTOs();
				}

				SYSTEM system = SYSTEM.getEnum(selectedSystem);
				List<LCCBaggageRequestDTO> lccBaggageRequestDTOs = composeBaggageRequest(flightSegmentTOs, strTxnIdntifier);

				lccBaggageResponseDTO = this.getAvailableBaggages(lccBaggageRequestDTOs, strTxnIdntifier, system, locale,
						modifyAncillary, false, isRequote, applicationEngine, lCCReservationBaggageSummaryTo, bundleFares, pnr,
						trackInfoDTO);

				lccBaggageResponseDTOs.add(lccBaggageResponseDTO);
			}
		}
		BaggageRatesDTO baggageRatesDTO = BaggageRatesTransformer.transform(lccBaggageResponseDTOs);
		return baggageRatesDTO;
	}

	private LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, SYSTEM system, String selectedLanguage, boolean isModifyOperation,
			boolean hasFinalCutOverPrivilege, boolean isRequote, ApplicationEngine appEngine,
			LCCReservationBaggageSummaryTo baggageSummaryTo, List<BundledFareDTO> bundledFareDTOs, String pnr,
			BasicTrackInfo trackInfo) throws ModuleException {

		AirproxyBaggageBusinessLayer baggageBL = new AirproxyBaggageBusinessLayer();
		baggageBL.setBaggageRequestDTOs(lccBaggageRequestDTOs);
		baggageBL.setSystem(system);
		baggageBL.setAppEngine(appEngine);
		baggageBL.setTransactionIdentifier(transactionIdentifier);
		baggageBL.setSelectedLanguage(selectedLanguage);
		baggageBL.setModifyOperation(isModifyOperation);
		baggageBL.setHasFinalCutOverPrivilege(hasFinalCutOverPrivilege);
		baggageBL.setRequote(isRequote);
		baggageBL.setBaggageSummaryTo(baggageSummaryTo);
		baggageBL.setUserPrincipal(getUserPrincipal());
		baggageBL.setBundledFareDTOs(bundledFareDTOs);
		baggageBL.setPnr(pnr);
		baggageBL.setTrackInfo(trackInfo);
		baggageBL.setCarrierWiseOndWiseBundlePeriodID(this.getOndWiseBundlePeriodID());

		return baggageBL.execute();
	}

	private List<LCCBaggageRequestDTO> composeBaggageRequest(List<FlightSegmentTO> flightSegTO, String txnId) {
		List<LCCBaggageRequestDTO> baggageRequestList = new ArrayList<LCCBaggageRequestDTO>();
		for (FlightSegmentTO flightSegment : flightSegTO) {
			LCCBaggageRequestDTO baggageRequest = new LCCBaggageRequestDTO();
			baggageRequest.setTransactionIdentifier(txnId);
			baggageRequest.setCabinClass(flightSegment.getCabinClassCode());
			baggageRequest.setFlightSegment(flightSegment);
			baggageRequestList.add(baggageRequest);
		}
		return baggageRequestList;
	}

	private Map<String, Map<String, Integer>> getOndWiseBundlePeriodID() {
		Map<String, Map<String, Integer>> carrierWiseOndWiseBundlePeriodID = new HashMap<>();
		for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
			for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
				for (FlightSegmentTO flightSegment : ondOption.getFlightSegmentList()) {
					Map<String, Integer> ondWiseBundlePeriodID = carrierWiseOndWiseBundlePeriodID
							.get(flightSegment.getOperatingAirline());

					if (carrierWiseOndWiseBundlePeriodID.get(flightSegment.getOperatingAirline()) == null) {
						ondWiseBundlePeriodID = new HashMap<>();
					}

					if (ondInfo.getPreferredBundleFarePeriodId() != null && flightSegment.getSegmentCode() != null) {
						ondWiseBundlePeriodID.put(flightSegment.getSegmentCode(), ondInfo.getPreferredBundleFarePeriodId());
					}

					if (ondWiseBundlePeriodID != null && !ondWiseBundlePeriodID.isEmpty()) {
						carrierWiseOndWiseBundlePeriodID.put(flightSegment.getOperatingAirline(), ondWiseBundlePeriodID);
					}
				}
			}
		}
		return carrierWiseOndWiseBundlePeriodID;
	}

	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	public BaseAvailRS getFlightPriceRS() {
		return flightPriceRS;
	}

	public void setFlightPriceRS(BaseAvailRS flightPriceRS) {
		this.flightPriceRS = flightPriceRS;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public String getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(String selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public ApplicationEngine getApplicationEngine() {
		return applicationEngine;
	}

	public void setApplicationEngine(ApplicationEngine applicationEngine) {
		this.applicationEngine = applicationEngine;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
}
