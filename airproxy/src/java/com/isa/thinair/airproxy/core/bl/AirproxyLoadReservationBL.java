package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.ModifyReservationUtil;
import com.isa.thinair.airproxy.api.dto.PaxCreditReinstateTO;
import com.isa.thinair.airproxy.api.dto.ResBalancesSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.MODIFY_MODES;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.RouteTypes;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.InfantInfoTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.ResModifyQueryV2Util;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airproxy.core.utils.transformer.PaymentConvertUtil;
import com.isa.thinair.airproxy.core.utils.transformer.ReservationConvertUtil;
import com.isa.thinair.airproxy.core.utils.transformer.SegmentAssemblerConvertUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.decorator.ReservationMediator;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ChargeAdjustmentTypeUtil;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ResModifyEmailAgentModificationConst;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ResModifyEmailAgentModificationType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PaxValildationTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRs;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketOperationsHandler;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;

/**
 * BL for the reservation loading
 * 
 * @author Thushara Fernando
 */
public class AirproxyLoadReservationBL {

	private static Log log = LogFactory.getLog(AirproxyLoadReservationBL.class);
	private static final String RESERVATION_CANCELLATION = "cancellation";
	private static final String RESERVATION_CREATION = "creation";

	/**
	 * Search Reservations based on user input
	 * 
	 * @param reservationSearchDTO
	 * @param userPrincipal
	 *            TODO
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public static Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException, ParseException {

		Collection<ReservationListTO> reservationList = new ArrayList<ReservationListTO>();

		// Other systems search via lcc. No need to check whether lcc enable or not because
		// "search all" option is controlled by that
		if (reservationSearchDTO.isSearchAll()) {
			// First we query the own system. If there's a match it means this is a part of an interline booking.
			Collection<ReservationDTO> reservations = AirproxyModuleUtils.getAirReservationQueryBD()
					.getReservations(reservationSearchDTO);
			if (reservations != null && reservations.size() > 0) {
				reservationList = FindReservationUtil.composeReservationsList(reservations, userPrincipal);
			} else {
				// we search the interline results only if we don't get any results from the own airline. This is the
				// case for a dry booking.
				reservationList = AirproxyModuleUtils.getLCCReservationBD().searchReservations(reservationSearchDTO, trackInfo);

				// this result is dry reservation. Set the originator pnr number as the pnr of the reservation
				// so this can be identified as a dry booking (or interline)
				fillOriginatorPnrInformation(reservationList, userPrincipal);
			}
		} else {
			// Own airline search
			if (userPrincipal != null && (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES)) {
				log.info("###WSLOGS### SEARCHING RESERVATION LIST STARTED AIRPROXY: PNR " + reservationSearchDTO.getPnr());
			}
			reservationList = FindReservationUtil.composeReservationsList(
					AirproxyModuleUtils.getAirReservationQueryBD().getReservations(reservationSearchDTO), userPrincipal);

			if (userPrincipal != null && (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES)) {
				log.info("###WSLOGS### SEARCHING RESERVATION LIST END AIRPROXY: PNR " + reservationSearchDTO.getPnr()
						+ "RESERVATION LIST SIZE : " + reservationList.size());
			}

		}

		return reservationList;
	}

	// sets the originator pnr number as the pnr number for a given list of reservations.
	private static void fillOriginatorPnrInformation(Collection<ReservationListTO> reservationList, UserPrincipal userPrincipal)
			throws ModuleException {
		for (ReservationListTO reservationListItem : reservationList) {
			if (StringUtils.isEmpty(reservationListItem.getOriginatorPnr())) {
				reservationListItem.setOriginatorPnr(reservationListItem.getPnrNo());
			}

			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationListItem.getPnrStatus())) {
				String pnrStatus;
				if (userPrincipal != null) {
					String strStation = userPrincipal.getAgentStation();
					Collection colUserDST = userPrincipal.getColUserDST();
					String localTime = DateUtil.getAgentLocalTime(reservationListItem.getReleaseTimeStamp(), colUserDST,
							strStation, "dd-MM-yyyy HH:mm");
					pnrStatus = "<font color='#FFB900'>" + reservationListItem.getPnrStatus()
							+ "<BR><font color='RED' size='0' style='font-size: 9px;'>" + localTime + "</font></font>";
					reservationListItem.setPnrStatus(pnrStatus);
				}
			}

		}
	}

	/**
	 * Load Reservation based on the pnr data Load interline reservation if the group PNR is present, Load own airline
	 * reservation otherwise
	 * 
	 * @param pnrModesDTO
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation loadReservation(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfo) throws ModuleException {

		LCCClientReservation lccClientReservation = null;
		if (log.isDebugEnabled()) {
			log.debug("LOAD RESERVATION : PNR modes DTO : " + pnrModesDTO.toString());
		}
		if (pnrModesDTO.getGroupPNR() != null || (StringUtils.isNotEmpty(pnrModesDTO.getMarketingAirlineCode())
				&& StringUtils.isNotEmpty(pnrModesDTO.getAirlineCode()))) {
			lccClientReservation = AirproxyModuleUtils.getLCCReservationBD().searchReservationByPNR(pnrModesDTO, paramRQInfo,
					trackInfo);
			trimDataForReservation(lccClientReservation);
		} else {
			Reservation reservation = AirproxyModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackInfo);

			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			lccClientReservation = ReservationConvertUtil.transform(reservation, airportBD, pnrModesDTO);
			ReservationSegmentDTO firstDepartingSegment = ReservationApiUtils
					.getFirstConfirmedSegment(reservation.getSegmentsView());
			ServiceTaxContainer serviceTaxContainer = AirproxyModuleUtils.getReservationBD()
					.getApplServiceTaxContainer(firstDepartingSegment, EXTERNAL_CHARGES.JN_OTHER);
			if (serviceTaxContainer != null) {
				lccClientReservation.getApplicableServiceTaxes().add(serviceTaxContainer);
			}
		}

		// Skip promotion adjustment if any
		if (pnrModesDTO.isSkipPromoAdjustment()) {
			AirProxyReservationUtil.skipPromotionAdjustments(lccClientReservation);
		}

		return lccClientReservation;
	}

	public static ServiceResponce isPaymentReferenceAvailable(String externalReference, String status, char productType)
			throws ModuleException {
		return AirproxyModuleUtils.getReservationBD().isExternalReferenceExist(externalReference, status, productType);
	}

	public static ServiceResponce isPaymentReferenceAvailable(String externalReference, char productType) throws ModuleException {
		return AirproxyModuleUtils.getReservationBD().isExternalReferenceExist(externalReference, productType);
	}

	/**
	 * trim the data from lcc reservation too many cross reservation references making the object too heavy. no need of
	 * this data for display puposes.
	 * 
	 * @param lccClientReservation
	 * 
	 *            NOTE: We have JSON serialization escapse for {@Code LCCClientReservationSegment} #getLccReservation
	 *            and for {@Code LCCClientReservationPax}#getLccReservation So if any error is encountered from this
	 *            method, this could be safely removed.
	 */
	private static void trimDataForReservation(LCCClientReservation lccClientReservation) {
		for (LCCClientReservationSegment segs : lccClientReservation.getSegments()) {
			segs.setLccReservation(null);
		}
		for (LCCClientReservationPax pax : lccClientReservation.getPassengers()) {
			pax.setLccReservation(null);
		}
	}

	/**
	 * Consolidated balance payment
	 * 
	 * @param lccClientBalancePayment
	 * @param version
	 * @param isGroupPnr
	 * @param userPrincipal
	 * @param enableFraudCheck
	 * @param trackInfo
	 * @param isGdsSale
	 * @param isInfantPaymentSeparated
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static ServiceResponce balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version,
			boolean isGroupPnr, UserPrincipal userPrincipal, boolean enableFraudCheck, TrackInfoDTO trackInfo, boolean isGdsSale,
			boolean isInfantPaymentSeparated) throws ModuleException {

		LCCClientReservation lccClientReservation = null;
		String groupPnr = lccClientBalancePayment.getGroupPNR();
		Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers = lccClientBalancePayment.getPaxSeqWisePayAssemblers();
		String transactionIdentifier = UUID.randomUUID().toString();

		try {
			if (isGroupPnr) {
				lccClientReservation = AirproxyModuleUtils.getLCCReservationBD().balancePayment(lccClientBalancePayment, version,
						trackInfo, isInfantPaymentSeparated);
			} else {
				Reservation reservation = loadReservation(groupPnr);
				Set<ReservationPax> colPassengers = reservation.getPassengers();

				// capture other carrier payments
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to capture other carrier payments for reservation " + groupPnr
								+ " for balance payment operation");
					}
					lccClientBalancePayment.setOtherCarrierPaxCreditUser(true); // set the flag for other carrier pax
																				// credit use
					paxWisePaymentAssemblers = AirproxyModuleUtils.getLCCReservationBD().makePayment(
							lccClientBalancePayment.getContactInfo(), groupPnr, transactionIdentifier, paxWisePaymentAssemblers,
							userPrincipal, trackInfo);
					if (log.isDebugEnabled()) {
						log.debug("Successfully captured other carrier payments for reservation " + groupPnr
								+ " for balance payment operation");
					}
				}

				lccClientBalancePayment.setPaxSeqWisePayAssemblers(paxWisePaymentAssemblers);

				// FIXME handle force confirm
				ServiceResponce responce = null;
				if (lccClientBalancePayment.isSplittedBooking()) {
					responce = AirproxyModuleUtils.getReservationBD().updateReservationForSplit(
							lccClientBalancePayment.getGroupPNR(), populatePerPaxPayment(lccClientBalancePayment, colPassengers),
							new Long(version), trackInfo, enableFraudCheck, false,
							lccClientBalancePayment.isOtherCarrierPaxCreditUser(), lccClientBalancePayment.isActualPayment(),
							true);
				} else {
					boolean isFirstPayment = ReservationStatus.ON_HOLD.equals(lccClientBalancePayment.getReservationStatus());
					responce = AirproxyModuleUtils.getReservationBD().updateReservationForPayment(
							lccClientBalancePayment.getGroupPNR(), populatePerPaxPayment(lccClientBalancePayment, colPassengers),
							lccClientBalancePayment.isForceConfirm(), lccClientBalancePayment.isAcceptPaymentsThanPayable(),
							new Long(version), trackInfo, enableFraudCheck, false, true,
							lccClientBalancePayment.isOtherCarrierPaxCreditUser(), lccClientBalancePayment.isActualPayment(),
							isFirstPayment, lccClientBalancePayment.getLoyaltyPaymentInfo(), isGdsSale, null);
				}

				if (!isGdsSale) {
					// The service response does not send anything loading the reservation again
					reservation = loadReservation(groupPnr);
					lccClientReservation = ReservationConvertUtil.transform(reservation, AirproxyModuleUtils.getAirportBD());
				}
			}

			DefaultServiceResponse serviceResponce = new DefaultServiceResponse(true);
			serviceResponce.addResponceParam(CommandParamNames.RESERVATION, lccClientReservation);
			serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
					lccClientBalancePayment.isOtherCarrierPaxCreditUser());
			return serviceResponce;
		} catch (Exception exception) {
			log.error("balance payment operation failed for " + groupPnr, exception);

			if (lccClientBalancePayment.getLoyaltyPaymentInfo() != null) {
				ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(groupPnr,
						lccClientBalancePayment.getLoyaltyPaymentInfo().getLoyaltyRewardIds(),
						lccClientBalancePayment.getLoyaltyPaymentInfo().getMemberAccountId());
			}

			if (!isGroupPnr && ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
				if (log.isDebugEnabled()) {
					log.debug("Going to revert other carrier payments for reservation " + groupPnr
							+ " for balance payment operation");
				}
				AirproxyModuleUtils.getLCCReservationBD().refundPayment(lccClientBalancePayment.getContactInfo(), groupPnr,
						transactionIdentifier, paxWisePaymentAssemblers, userPrincipal, trackInfo);
				if (log.isDebugEnabled()) {
					log.debug("Successfully reverted other carrier payments for reservation " + groupPnr
							+ " for balance payment operation");
				}
			}

			if (exception instanceof ModuleException) {
				throw (ModuleException) exception;
			} else {
				throw new ModuleException("airreservation.airproxy.balance.payment.failed");
			}
		}
	}

	/**
	 * TODO : This MUST be optimized. Full reservation is loaded to get a single pax.
	 */
	public static LCCClientReservationPax loadPassenger(String groupPnr, int pnrPaxId, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientReservationPax passenger = new LCCClientReservationPax();
		if (!isGroupPnr) {
			ReservationPax pax = null;
			Reservation reservation = loadReservation(groupPnr);
			Set<ReservationPax> paxs = reservation.getPassengers();
			for (ReservationPax resPax : paxs) {
				if (resPax.getPnrPaxId().intValue() == pnrPaxId) {
					pax = resPax;
					break;
				}
			}

			// pax = AirproxyModuleUtils.getPassengerBD().getPassenger(Integer.valueOf(pnrPaxId), true);
			Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();
			colPnrPaxIds.add(pnrPaxId);
			Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReservationTnx = AirproxyModuleUtils.getPassengerBD()
					.getPNRPaxPaymentsAndRefunds(colPnrPaxIds);
			Map<Integer, Collection<CreditInfoDTO>> paxCredits = AirproxyModuleUtils.getPassengerBD().getAllCredits(colPnrPaxIds);
			passenger = ReservationConvertUtil.transformPassenger(pax, pnrPaxIdAndColReservationTnx, paxCredits, null, null,
					null, null, null, null, false, true, reservation.isInfantPaymentRecordedWithInfant(), null);
		} else {
			LCCClientPnrModesDTO lccClientPnrModesDTO = getDefaultLCCClientPnrModesDTO(groupPnr);
			lccClientPnrModesDTO.setGroupPNR(groupPnr);
			LCCClientReservation lccClientReservation = loadReservation(lccClientPnrModesDTO, null, trackInfoDTO);
			for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {
				if (lccClientReservationPax.getTravelerRefNumber().contains(String.valueOf(pnrPaxId))) {
					return lccClientReservationPax;
				}
			}
		}
		return passenger;

	}

	public static PaxContactConfigDTO loadPaxContactConfig(String system, String appName, List<String> carrierList,
			BasicTrackInfo trackInfo) throws ModuleException {
		PaxContactConfigDTO paxContactConfigDTO = new PaxContactConfigDTO();

		if (SYSTEM.getEnum(system) != SYSTEM.AA) {
			paxContactConfigDTO = AirproxyModuleUtils.getLCCReservationBD().loadPaxContactConfig(appName, carrierList, trackInfo);
		} else {
			paxContactConfigDTO = AirproxyModuleUtils.getGlobalConfig().getPaxContactConfig(appName);
			PaxValildationTO paxValidationTO = new PaxValildationTO();
			paxValidationTO.setAdultAgeCutOverYears(
					AirproxyModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.ADULT).getCutOffAgeInYears());
			paxValidationTO.setChildAgeCutOverYears(
					AirproxyModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.CHILD).getCutOffAgeInYears());
			paxValidationTO.setInfantAgeCutOverYears(
					AirproxyModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.INFANT).getCutOffAgeInYears());
			paxContactConfigDTO.setPaxValidation(paxValidationTO);
		}

		return paxContactConfigDTO;
	}

	public static Map<String, List<PaxCountryConfigDTO>> loadPaxCountryConfig(String system, String appName,
			List<String> originDestinations, BasicTrackInfo trackInfo) throws ModuleException {
		Map<String, List<PaxCountryConfigDTO>> paxCountryConfigDTOsForSelDests = new HashMap<String, List<PaxCountryConfigDTO>>();

		if (SYSTEM.getEnum(system) != SYSTEM.AA) {
			// TODO : dry/interline flow need to be implemented
		} else {
			// getting the country wise configs through the cached configs
			Map<String, List<PaxCountryConfigDTO>> countryConfigs = AirproxyModuleUtils.getGlobalConfig()
					.getCountryWiseContactConfigs(appName);
			for (String airportCode : originDestinations) {
				String countryCode = AirproxyModuleUtils.getAirportBD().getOwnCountryCodeForAirport(airportCode);
				if (countryConfigs.get(countryCode) != null) {
					paxCountryConfigDTOsForSelDests.put(countryCode, countryConfigs.get(countryCode));
				}
			}
		}

		return paxCountryConfigDTOsForSelDests;
	}

	/**
	 * 
	 * @param paymentAssembler
	 * @return
	 */
	private static IPassenger populatePerPaxPayment(LCCClientBalancePayment lccClientBalancePayment,
			Set<ReservationPax> colPassengers) throws ModuleException {

		IPassenger iPassenger = new PassengerAssembler(null);

		Map<Integer, LCCClientPaymentAssembler> clientPayments = lccClientBalancePayment.getPaxSeqWisePayAssemblers();
		for (Integer paxId : clientPayments.keySet()) {
			Integer pnrPaxId = null;
			IPayment ipaymentPassenger = new PaymentAssembler();
			LCCClientPaymentAssembler clientPayAssembler = clientPayments.get(paxId);
			Map extExternalChgDTOMap = lccClientBalancePayment.getExternalChargesMap();
			ipaymentPassenger = PaymentConvertUtil.populatePerPaxPayment(clientPayAssembler, extExternalChgDTOMap);
			// For v1 we need the pnrPaxid
			for (ReservationPax pax : colPassengers) {
				// How good is this check
				if (paxId.compareTo(pax.getPaxSequence()) == 0) {
					pnrPaxId = pax.getPnrPaxId();
					break;
				}
			}

			iPassenger.addPassengerPayments(pnrPaxId, ipaymentPassenger);
		}

		if (lccClientBalancePayment.getTemporyPaymentMap() != null && lccClientBalancePayment.getTemporyPaymentMap().size() > 0) {
			Map<Integer, CardPaymentInfo> cardPayInfoMap = new HashMap<Integer, CardPaymentInfo>();
			Set<Integer> tmpKeySet = lccClientBalancePayment.getTemporyPaymentMap().keySet();
			for (Integer key : tmpKeySet) {
				CommonCreditCardPaymentInfo commonInfo = lccClientBalancePayment.getTemporyPaymentMap().get(key);
				CardPaymentInfo cardPayInfo = new CardPaymentInfo();
				cardPayInfo.setAddress(commonInfo.getAddress());
				cardPayInfo.setAppIndicator(commonInfo.getAppIndicator());
				cardPayInfo.setAuthorizationId(commonInfo.getAuthorizationId());
				cardPayInfo.setEDate(commonInfo.geteDate());
				cardPayInfo.setIpgIdentificationParamsDTO(commonInfo.getIpgIdentificationParamsDTO());
				cardPayInfo.setName(commonInfo.getName());
				cardPayInfo.setNo(commonInfo.getNo());
				cardPayInfo.setNoFirstDigits(commonInfo.getNoFirstDigits());
				cardPayInfo.setNoLastDigits(commonInfo.getNoLastDigits());
				cardPayInfo.setPayCurrencyDTO(commonInfo.getPayCurrencyDTO());
				cardPayInfo.setPaymentBrokerRefNo(commonInfo.getPaymentBrokerId());
				cardPayInfo.setTemporyPaymentId(commonInfo.getTemporyPaymentId());
				cardPayInfo.setTnxMode(commonInfo.getTnxMode());
				cardPayInfo.setType(commonInfo.getType());
				cardPayInfo.setTotalAmount(commonInfo.getTotalAmount());
				cardPayInfo.setSecurityCode(commonInfo.getSecurityCode());
				cardPayInfo.setPnr(commonInfo.getGroupPNR());
				cardPayInfo.setSuccess(commonInfo.isPaymentSuccess());

				cardPayInfoMap.put(key, cardPayInfo);
			}

			iPassenger.addTemporyPaymentEntries(cardPayInfoMap);
		}

		return iPassenger;
	}

	/**
	 * Consolidated balance Calculation for Modifying
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param userPrincipal
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationBalanceTO getResAlterationBalanceSummary(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {

		ReservationBalanceTO reservationBalceSummary = null;

		if (lccClientResAlterQueryModesTO.isGroupPnr()) {
			reservationBalceSummary = AirproxyModuleUtils.getLCCReservationBD().getResAlterationBalanceSummary(
					lccClientResAlterQueryModesTO.getPassengers(), lccClientResAlterQueryModesTO, trackInfo);
		} else {
			Reservation reservation = loadReservation(lccClientResAlterQueryModesTO.getGroupPNR());
			FlightPriceRQ priceQuoteRQ = null;
			Collection<OndFareDTO> collFares = null;
			FareSegChargeTO fareSegChargeTO = null;
			boolean isFlexiQuote;
			OndRebuildCriteria ondRebuildCriteria = null;
			String alterationType = "";
			Collection<Integer> oldSegs = null;
			Collection<Integer> newFlightSegs = null;
			Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap = null;
			float fareDiscountPerc = 0;

			switch (lccClientResAlterQueryModesTO.getModifyModes()) {
			case CANCEL_RES:
				alterationType = ReservationConstants.AlterationType.ALT_CANCEL_RES;
				int uniqueCRFareIds = AirProxyReservationUtil.getUniqueFareIds(reservation);
				if (uniqueCRFareIds > 0) {
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomAdultCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseAdultCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomAdultCharge(), reservation, uniqueCRFareIds));
						lccClientResAlterQueryModesTO.setCustomAdultCharge(AccelAeroCalculator
								.divide(lccClientResAlterQueryModesTO.getCustomAdultCharge(), uniqueCRFareIds));
					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomChildCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseChildCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomChildCharge(), reservation, uniqueCRFareIds));
						lccClientResAlterQueryModesTO.setCustomChildCharge(AccelAeroCalculator
								.divide(lccClientResAlterQueryModesTO.getCustomChildCharge(), uniqueCRFareIds));
					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomInfantCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseInfantCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomInfantCharge(), reservation, uniqueCRFareIds));
						lccClientResAlterQueryModesTO.setCustomInfantCharge(AccelAeroCalculator
								.divide(lccClientResAlterQueryModesTO.getCustomInfantCharge(), uniqueCRFareIds));
					}
				}
				
				if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null
						&& lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap() != null
						&& !lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap().isEmpty()) {
					passengerExtChargeMap = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
							.getPassengerExtChargeMap();
				}
				
				break;
			case VOID_RES:
				alterationType = ReservationConstants.AlterationType.ALT_VOID_RES;
				int uniqueFareIds = AirProxyReservationUtil.getUniqueFareIds(reservation);
				if (uniqueFareIds > 0) {
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomAdultCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseAdultCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomAdultCharge(), reservation, uniqueFareIds));
						lccClientResAlterQueryModesTO.setCustomAdultCharge(
								AccelAeroCalculator.divide(lccClientResAlterQueryModesTO.getCustomAdultCharge(), uniqueFareIds));
					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomChildCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseChildCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomChildCharge(), reservation, uniqueFareIds));
						lccClientResAlterQueryModesTO.setCustomChildCharge(
								AccelAeroCalculator.divide(lccClientResAlterQueryModesTO.getCustomChildCharge(), uniqueFareIds));
					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomInfantCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseInfantCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomInfantCharge(), reservation, uniqueFareIds));
						lccClientResAlterQueryModesTO.setCustomInfantCharge(
								AccelAeroCalculator.divide(lccClientResAlterQueryModesTO.getCustomInfantCharge(), uniqueFareIds));
					}
				}
				
				break;
			case ADD_OND:
				alterationType = ReservationConstants.AlterationType.ALT_ADD_OND;
				priceQuoteRQ = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFlightPriceRQ();

				fareSegChargeTO = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSelectedFareSegChargeTO();
				isFlexiQuote = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().isFlexiSelected();
				ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ, fareSegChargeTO);
				collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria);

				passengerExtChargeMap = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap();
				fareDiscountPerc = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFareDiscountPercentage();
				break;
			case CANCEL_OND:
				alterationType = ReservationConstants.AlterationType.ALT_CANCEL_OND;
				oldSegs = getOldSegCollection(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation);
				break;

			case MODIFY_OND:
				priceQuoteRQ = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFlightPriceRQ();

				fareSegChargeTO = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getSelectedFareSegChargeTO();
				// isFlexiQuote = true; // Always set to true to get the balance summary with flexi charges.
				isFlexiQuote = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().isFlexiSelected();
				ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ, fareSegChargeTO);
				collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria);

				oldSegs = getOldSegCollection(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation);
				newFlightSegs = getNewFlightSegCollection(collFares);

				passengerExtChargeMap = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap();
				fareDiscountPerc = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFareDiscountPercentage();
				alterationType = ReservationConstants.AlterationType.ALT_MODIFY_OND;
				break;

			case REMOVE_PAX:
				alterationType = ReservationConstants.AlterationType.ALT_CANCEL_RES;
				int uniqueRFareIds = AirProxyReservationUtil.getUniqueFareIds(reservation);
				if (uniqueRFareIds > 0) {
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomAdultCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseAdultCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomAdultCharge(), reservation, uniqueRFareIds));
						lccClientResAlterQueryModesTO.setCustomAdultCharge(
								AccelAeroCalculator.divide(lccClientResAlterQueryModesTO.getCustomAdultCharge(), uniqueRFareIds));

					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomChildCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseChildCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomChildCharge(), reservation, uniqueRFareIds));
						lccClientResAlterQueryModesTO.setCustomChildCharge(
								AccelAeroCalculator.divide(lccClientResAlterQueryModesTO.getCustomChildCharge(), uniqueRFareIds));
					}
					if (lccClientResAlterQueryModesTO.getRouteType() != null
							&& lccClientResAlterQueryModesTO.getCustomInfantCharge() != null
							&& lccClientResAlterQueryModesTO.getRouteType().equals(RouteTypes.TOTAL_ROUTE.getRouteTypes())) {
						lccClientResAlterQueryModesTO.setOndWiseInfantCharge(getOndWideCustomCharges(
								lccClientResAlterQueryModesTO.getCustomInfantCharge(), reservation, uniqueRFareIds));
						lccClientResAlterQueryModesTO.setCustomInfantCharge(AccelAeroCalculator
								.divide(lccClientResAlterQueryModesTO.getCustomInfantCharge(), uniqueRFareIds));
					}
				}
				
				if (lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null
						&& lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap() != null
						&& !lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap().isEmpty()) {
					passengerExtChargeMap = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
							.getPassengerExtChargeMap();
				}
				
				break;
			case ADD_INF:
				int noOfInfants = 1;
				alterationType = ReservationConstants.AlterationType.ALT_ADD_INF;
				ReservationMediator rm = new ReservationMediator(reservation);
				Collection<OndFareDTO> ondDTOs = rm.getOndFareDTOs(noOfInfants);
				addInfant(reservation, lccClientResAlterQueryModesTO.getPaxAdultsList(),
						lccClientResAlterQueryModesTO.getAddInfant());
				collFares = AirproxyModuleUtils.getFlightInventoryResBD().getInfantQuote(ondDTOs, userPrincipal.getAgentStation(),
						true, userPrincipal.getAgentCode());

				ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = ServiceTaxConverterUtil
						.createServiceTaxQuoteForAddInfantRequest(collFares, reservation, userPrincipal, null, null);
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = ServiceTaxConverterUtil
						.adaptServiceTaxQuoteForTicketingRevenueRS(
								AirproxyModuleUtils.getChargeBD().quoteServiceTaxForTicketingRevenue(serviceTaxRQ));
				passengerExtChargeMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
				List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();

				if (serviceTaxQuoteRS != null && serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
						&& serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT) != null) {
					String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
					String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();
					List<ServiceTaxTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT);
					ServiceTaxCalculatorUtil.addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs,
							taxDepositCountryCode, taxDepositStateCode, false);
				}

				Integer adultSeqId = null;
				for (String adult : lccClientResAlterQueryModesTO.getPaxAdultsList()) {
					if (adult != null) {
						adultSeqId = new Integer(adult);
						break;
					}
				}
				if (adultSeqId != null && lccClientResAlterQueryModesTO.getLccClientSegmentAssembler() != null
						&& lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap() != null
						&& lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap()
								.get(adultSeqId) != null) {
					infantChgDTOs.addAll(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getPassengerExtChargeMap()
							.get(adultSeqId));
				}
				
				passengerExtChargeMap.put(adultSeqId, infantChgDTOs);
				break;
			default:
				throw new ModuleException("nothing is selected");
			}

			CustomChargesTO customChargesTO = new CustomChargesTO(lccClientResAlterQueryModesTO.getCustomAdultCharge(),
					lccClientResAlterQueryModesTO.getCustomChildCharge(), lccClientResAlterQueryModesTO.getCustomInfantCharge(),
					lccClientResAlterQueryModesTO.getCustomAdultCharge(), lccClientResAlterQueryModesTO.getCustomChildCharge(),
					lccClientResAlterQueryModesTO.getCustomInfantCharge(), lccClientResAlterQueryModesTO.getAdultCustomChargeTO(),
					lccClientResAlterQueryModesTO.getChildCustomChargeTO(),
					lccClientResAlterQueryModesTO.getInfantCustomChargeTO(),
					lccClientResAlterQueryModesTO.isSendingAbosoluteCharge(),
					lccClientResAlterQueryModesTO.getOndWiseAdultCharge(), lccClientResAlterQueryModesTO.getOndWiseChildCharge(),
					lccClientResAlterQueryModesTO.getOndWiseInfantCharge());

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
			Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap = null;
			if (ResModifyQueryV2Util.isRemoveInsurance(alterationType, reservation)) {
				quotedExtChgMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo,
						null);
			}

			ResBalancesSummaryTOV2 balancesSummaryTOV2 = ResModifyQueryV2Util.getResBalancesForAlt(alterationType, reservation,
					oldSegs, newFlightSegs, collFares, true, customChargesTO, passengerExtChargeMap, false, fareDiscountPerc,
					quotedExtChgMap);
			Map<Integer, ReservationPax> reservationPaxList = new HashMap<Integer, ReservationPax>();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				reservationPaxList.put(reservationPax.getPnrPaxId(), reservationPax);
			}
			reservationBalceSummary = ReservationConvertUtil.transformAltBalances(balancesSummaryTOV2, reservationPaxList,
					reservation.isInfantPaymentRecordedWithInfant());

			if (lccClientResAlterQueryModesTO.getModifyModes().equals(MODIFY_MODES.REMOVE_PAX)) {
				List<String> paxAdults = lccClientResAlterQueryModesTO.getPaxAdultsList();
				List<String> paxInfants = lccClientResAlterQueryModesTO.getPaxInfantList();
				Collection<LCCClientPassengerSummaryTO> updatedPassengerSummaryTOs = ModifyReservationUtil
						.getUpdatedPaxSummaryTOs(reservationBalceSummary.getPassengerSummaryList(), paxAdults, paxInfants,
								lccClientResAlterQueryModesTO.getPassengers(), reservation.isInfantPaymentRecordedWithInfant());
				reservationBalceSummary.setPassengerSummaryList(updatedPassengerSummaryTOs);
				ModifyReservationUtil.updatePayments(reservationBalceSummary, reservation.isInfantPaymentRecordedWithInfant());
			}

		}
		return reservationBalceSummary;
	}

	private static void addInfant(Reservation reservation, List<String> adultIds, LCCClientReservationPax lccClientReservationPax)
			throws ModuleException {

		for (String adult : adultIds) {
			for (ReservationPax pax : reservation.getPassengers()) {
				if (adult != null && (new Integer(adult)).compareTo(pax.getPaxSequence()) == 0) {
					ReservationPax newInfant = new ReservationPax();
					newInfant = ReservationConvertUtil.transformPassenger(lccClientReservationPax, newInfant);
					newInfant.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
					newInfant.addParent(pax);
					pax.addInfants(newInfant);
					reservation.addPassenger(newInfant);
					break;
				}
			}
		}

	}

	/**
	 * Consolidated Modify Segment
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param enableFraudCheck
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, boolean enableFraudCheck, TrackInfoDTO trackInfoDTO) throws ModuleException {
		LCCClientReservation clientReservation = null;

		if (lccClientResAlterQueryModesTO.isGroupPnr()) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().modifySegments(lccClientResAlterQueryModesTO,
					trackInfoDTO);
		} else {

			SegmentAssemblerConvertUtil convertUtil = new SegmentAssemblerConvertUtil();
			FareSegChargeTO fareSegChargeTO = null;
			Collection<OndFareDTO> collFares = null;
			Reservation reservation = null;
			ISegment iSegment = null;
			Collection<TempSegBcAlloc> collBlockID = null;
			boolean otherCarrierPaxCreditUse = false;
			LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();
			String transactionIdentifier = UUID.randomUUID().toString();
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers = lccClientSegmentAssembler
					.getPassengerPaymentsMap();
			CommonReservationContactInfo contactInfo = lccClientSegmentAssembler.getContactInfo();
			String groupPnr = lccClientResAlterQueryModesTO.getGroupPNR();
			try {
				// capture other carrier payments
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to capture other carrier payments for reservation " + groupPnr
								+ " for modify segments operation");
					}
					otherCarrierPaxCreditUse = true;
					paxWisePaymentAssemblers = AirproxyModuleUtils.getLCCReservationBD().makePayment(contactInfo, groupPnr,
							transactionIdentifier, paxWisePaymentAssemblers, userPrincipal, trackInfoDTO);
					if (log.isDebugEnabled()) {
						log.debug("Successfully captured other carrier payments for reservation " + groupPnr
								+ " for modify segments operation");
					}
				}
				lccClientSegmentAssembler.setPassengerPaymentsMap(paxWisePaymentAssemblers);

				reservation = loadReservation(lccClientResAlterQueryModesTO.getGroupPNR());
				FlightPriceRQ priceQuoteRQ = lccClientSegmentAssembler.getFlightPriceRQ();
				fareSegChargeTO = lccClientSegmentAssembler.getSelectedFareSegChargeTO();
				boolean isFlexiQuote = lccClientSegmentAssembler.isFlexiSelected();

				OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ,
						fareSegChargeTO);
				collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria);

				Integer modifiedONDReturnGrp = getModifiedONDReturnGroupId(lccClientResAlterQueryModesTO.getOldPnrSegs(),
						reservation);

				iSegment = convertUtil.getSegmentAssembler(collFares, lccClientSegmentAssembler, reservation,
						getOldSegCollection(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation), modifiedONDReturnGrp);
				iSegment.setUserNote(lccClientResAlterQueryModesTO.getUserNotes());

				if (SalesChannelsUtil.isAppIndicatorWebOrMobile(lccClientResAlterQueryModesTO.getAppIndicator())) {
					convertUtil.populateTempPaymentEntries(iSegment, lccClientSegmentAssembler.getTemporyPaymentMap());
				}

			} catch (ModuleException ex) {
				Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap = lccClientSegmentAssembler.getTemporyPaymentMap();
				if (temporyPaymentMap != null
						&& SalesChannelsUtil.isAppIndicatorWebOrMobile(lccClientResAlterQueryModesTO.getAppIndicator())) {
					for (int tempPaymentID : temporyPaymentMap.keySet()) {
						if (temporyPaymentMap.get(tempPaymentID).getTemporyPaymentId() != null) {
							try {
								iSegment = convertUtil
										.getSegmentAssembler(lccClientResAlterQueryModesTO.getLccClientSegmentAssembler());
								convertUtil.populateTempPaymentEntries(iSegment, temporyPaymentMap);
								AirproxyModuleUtils.getResSegmentBD().intermediateRefund(
										lccClientResAlterQueryModesTO.getGroupPNR(), iSegment, trackInfoDTO,
										ex.getExceptionCode());
							} catch (Exception ext) {
								log.error("Error occured in modifySegments AirproxyLoadReservationBL. ");
							}
							break;
						}
					}
				}

				if (ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to revert other carrier payments for reservation " + groupPnr
								+ " for modify segments operation");
					}
					AirproxyModuleUtils.getLCCReservationBD().refundPayment(contactInfo, groupPnr, transactionIdentifier,
							paxWisePaymentAssemblers, userPrincipal, trackInfoDTO);
					if (log.isDebugEnabled()) {
						log.debug("Successfully reverted other carrier payments for reservation " + groupPnr
								+ " for modify segments operation");
					}
				}
				throw ex;
			}
			
			if (lccClientResAlterQueryModesTO.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)
					|| (SalesChannelsUtil.isAppIndicatorWebOrMobile(lccClientResAlterQueryModesTO.getAppIndicator()) && fareSegChargeTO
							.hasBlockSeats())) {
				if (!fareSegChargeTO.hasBlockSeats()) {
					// FCCSegInventoryAuditDTO fccSegInventoryAuditDTO = new FCCSegInventoryAuditDTO(
					// ReservationInternalConstants.InventoryAudits.MODIFY_SEGMENT_BLOCK_SEAT, reservation.getPnr(),
					// userPrincipal.getUserId());
					// collBlockID = AirproxyModuleUtils.getReservationBD().blockSeats(collFares, null, null,
					// fccSegInventoryAuditDTO);
				} else {
					collBlockID = fareSegChargeTO.getBlockSeatIds();
				}

			}

			CustomChargesTO customChargesTO = new CustomChargesTO(lccClientResAlterQueryModesTO.getCustomAdultCharge(),
					lccClientResAlterQueryModesTO.getCustomChildCharge(), lccClientResAlterQueryModesTO.getCustomInfantCharge(),
					lccClientResAlterQueryModesTO.getCustomAdultCharge(), lccClientResAlterQueryModesTO.getCustomChildCharge(),
					lccClientResAlterQueryModesTO.getCustomInfantCharge(), lccClientResAlterQueryModesTO.getAdultCustomChargeTO(),
					lccClientResAlterQueryModesTO.getChildCustomChargeTO(),
					lccClientResAlterQueryModesTO.getInfantCustomChargeTO(),
					lccClientResAlterQueryModesTO.isSendingAbosoluteCharge());
			customChargesTO.setDefaultCustomAdultCharge(lccClientResAlterQueryModesTO.getDefaultCustomAdultCharge());
			customChargesTO.setDefaultCustomChildCharge(lccClientResAlterQueryModesTO.getDefaultCustomChildCharge());

			Collection<Integer> oldPnrSegments = getOldSegCollection(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation);

			if (lccClientSegmentAssembler.isAutoCancellationEnabled()) {
				Date firstDepatureFlightDateTimeInZulu = AirProxyReservationUtil
						.getFirstDepartureFlightDate(lccClientSegmentAssembler.getSegmentsMap().values());

				// Consider Existing reservation segments if insurance selected with modify segment operation
				if (lccClientSegmentAssembler.getResInsurances() != null
						&& !lccClientSegmentAssembler.getResInsurances().isEmpty()) {
					firstDepatureFlightDateTimeInZulu = AirProxyReservationUtil.getEffectiveFirstDepartureDate(
							firstDepatureFlightDateTimeInZulu, reservation.getSegmentsView(), oldPnrSegments);
				}

				AutoCancellationInfo autoCancellationInfo = AirproxyModuleUtils.getReservationBD().getAutoCancellationInfo(
						groupPnr, reservation, firstDepatureFlightDateTimeInZulu, oldPnrSegments,
						AutoCancellationInfo.AUTO_CNX_TYPE.SEG_EXP.toString(),
						lccClientSegmentAssembler.isHasBufferTimeAutoCnxPrivilege(), false);
				iSegment.setAutoCancellationInfo(autoCancellationInfo);
			}

			ServiceResponce sr = AirproxyModuleUtils.getResSegmentBD().changeSegmentsWithRequiresNew(
					lccClientResAlterQueryModesTO.getGroupPNR(), oldPnrSegments, iSegment, customChargesTO, collBlockID,
					lccClientResAlterQueryModesTO.getPaymentTypes(), getVersion(lccClientResAlterQueryModesTO.getVersion()),
					trackInfoDTO, false, enableFraudCheck, otherCarrierPaxCreditUse, false,
					isModifyingConfSegOfOpenReturn(oldPnrSegments, reservation), lccClientResAlterQueryModesTO.isActualPayment());

			boolean useOtherCarrierPaxCredit = (Boolean) sr.getResponseParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED);

			reservation = loadReservation(lccClientResAlterQueryModesTO.getGroupPNR());
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);
			clientReservation.setUseOtherCarrierPaxCredit(useOtherCarrierPaxCredit);

			ServiceResponce rs = AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(reservation.getPnr(), false,
					trackInfoDTO, false, null);
			clientReservation.setSuccessfulRefund(
					Boolean.valueOf(String.valueOf(rs.getResponseParam(PaymentConstants.PARAM_SUCCESSFULL_REFUND))));

		}
		return clientReservation;
	}

	/**
	 * Consolidated Modify Segment
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param enableFraudCheck
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation requoteModifySegmentsWithAutoRefundForIBE(RequoteModifyRQ requoteModifyRQ,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		ServiceResponce rs = AirproxyModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);

		boolean voidAutoRefundEnabled = AppSysParamsUtil.isVoidAutoRefundEnabled();
		boolean ibePaxCreditAutoRefundEnabled = AppSysParamsUtil.isIBEPaxCreditAutoRefundEnabled();
		if (voidAutoRefundEnabled || ibePaxCreditAutoRefundEnabled) {
			LCCClientReservation clientReservation = null;
			Reservation reservation = null;
			reservation = loadReservation(requoteModifyRQ.getPnr());
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);

			if (rs.isSuccess()) {

				ServiceResponce autoRefundRS = AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(reservation.getPnr(),
						false, trackInfoDTO, false, null);
				clientReservation.setSuccessfulRefund(Boolean
						.valueOf(String.valueOf(autoRefundRS.getResponseParam(PaymentConstants.PARAM_SUCCESSFULL_REFUND))));
			}
			return clientReservation;
		} else {
			LCCClientReservation clientReservation = new LCCClientReservation();
			clientReservation.setSuccessfulRefund(false);
			return clientReservation;
		}
	}

	/**
	 * Consolidated Modify Segment
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param enableFraudCheck
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static ServiceResponce requoteModifySegmentsWithAutoRefund(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		ServiceResponce rs = AirproxyModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);

		if ((AppSysParamsUtil.isAgentCreditRefundEnabled() || AppSysParamsUtil.isIBEPaxCreditAutoRefundEnabled()
				|| AppSysParamsUtil.isVoidAutoRefundEnabled()) && requoteModifyRQ.getSystem() == SYSTEM.AA
				&& (requoteModifyRQ.getGroupPnr() == null || requoteModifyRQ.getGroupPnr().isEmpty())) {
			if (rs.isSuccess()) {
				rs = AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(requoteModifyRQ.getPnr(), false, trackInfoDTO,
						false, requoteModifyRQ.getOwnerChannelId());
			}
		}

		return rs;

	}

	/**
	 * Add Segment
	 * 
	 * @param lccClientResAlterQueryModesTO
	 * @param userPrincipal
	 * @param enableFraudCheck
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation addSegment(LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			UserPrincipal userPrincipal, boolean enableFraudCheck, TrackInfoDTO trackInfoDTO) throws Exception {
		LCCClientReservation clientReservation = null;

		if (lccClientResAlterQueryModesTO.isGroupPnr()) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().addSegments(lccClientResAlterQueryModesTO,
					trackInfoDTO);
		} else {

			Reservation reservation = loadReservation(lccClientResAlterQueryModesTO.getGroupPNR());
			SegmentUtil.setPNRSegID(lccClientResAlterQueryModesTO, reservation);
			SegmentAssemblerConvertUtil convertUtil = new SegmentAssemblerConvertUtil();

			LCCClientSegmentAssembler lccClientSegmentAssembler = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler();
			String transactionIdentifier = UUID.randomUUID().toString();
			Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers = lccClientSegmentAssembler
					.getPassengerPaymentsMap();
			CommonReservationContactInfo contactInfo = lccClientSegmentAssembler.getContactInfo();
			String groupPnr = lccClientResAlterQueryModesTO.getGroupPNR();

			try {
				// capture other carrier payments
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to capture other carrier payments for reservation " + groupPnr
								+ " for add segment operation");
					}
					lccClientSegmentAssembler.setUseOtherCarrierPaxCredit(true);
					paxWisePaymentAssemblers = AirproxyModuleUtils.getLCCReservationBD().makePayment(contactInfo, groupPnr,
							transactionIdentifier, paxWisePaymentAssemblers, userPrincipal, trackInfoDTO);
					if (log.isDebugEnabled()) {
						log.debug("Successfully captured other carrier payments for reservation " + groupPnr
								+ " for add segments operation");
					}
				}
				lccClientSegmentAssembler.setPassengerPaymentsMap(paxWisePaymentAssemblers);

				FlightPriceRQ priceQuoteRQ = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFlightPriceRQ();
				FareSegChargeTO fareSegChargeTO = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
						.getSelectedFareSegChargeTO();
				boolean isFlexiQuote = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().isFlexiSelected();

				OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ,
						fareSegChargeTO);
				Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD()
						.recreateFareSegCharges(ondRebuildCriteria);

				ISegment iSegment = convertUtil.getSegmentAssembler(collFares,
						lccClientResAlterQueryModesTO.getLccClientSegmentAssembler(), reservation, null, 0);

				if (SalesChannelsUtil.isAppIndicatorWebOrMobile(lccClientResAlterQueryModesTO.getAppIndicator())) {
					convertUtil.populateTempPaymentEntries(iSegment, lccClientSegmentAssembler.getTemporyPaymentMap());
				}

				Collection<TempSegBcAlloc> collBlockID = null;
				if (!fareSegChargeTO.hasBlockSeats()) {
					collBlockID = AirproxyModuleUtils.getReservationBD().blockSeats(collFares, null);
				} else {// Getting the block seats from the session for early block seats.
					collBlockID = fareSegChargeTO.getBlockSeatIds();
				}

				if (lccClientSegmentAssembler.isAutoCancellationEnabled()) {
					Date firstDepatureFlightDateTimeInZulu = AirProxyReservationUtil
							.getFirstDepartureFlightDate(lccClientSegmentAssembler.getSegmentsMap().values());

					// Consider Existing reservation segments if insurance selected with add segment operation
					if (lccClientSegmentAssembler.getResInsurances() != null
							&& !lccClientSegmentAssembler.getResInsurances().isEmpty()) {
						firstDepatureFlightDateTimeInZulu = AirProxyReservationUtil.getEffectiveFirstDepartureDate(
								firstDepatureFlightDateTimeInZulu, reservation.getSegmentsView(), null);
					}

					AutoCancellationInfo autoCancellationInfo = AirproxyModuleUtils.getReservationBD().getAutoCancellationInfo(
							lccClientResAlterQueryModesTO.getGroupPNR(), reservation, firstDepatureFlightDateTimeInZulu, null,
							AutoCancellationInfo.AUTO_CNX_TYPE.SEG_EXP.toString(),
							lccClientSegmentAssembler.isHasBufferTimeAutoCnxPrivilege(), false);
					iSegment.setAutoCancellationInfo(autoCancellationInfo);
				}

				ServiceResponce responce = AirproxyModuleUtils.getResSegmentBD().addSegments(
						lccClientResAlterQueryModesTO.getGroupPNR(), iSegment, collBlockID,
						lccClientResAlterQueryModesTO.getPaymentTypes(), getVersion(lccClientResAlterQueryModesTO.getVersion()),
						trackInfoDTO, false, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
						lccClientResAlterQueryModesTO.getSelectedPnrSegID(), null,
						lccClientSegmentAssembler.isUseOtherCarrierPaxCredit(), false,
						lccClientResAlterQueryModesTO.isActualPayment());

				// do over booking inventory audit
				if (fareSegChargeTO.hasBlockSeats() && collBlockID != null && collBlockID.size() > 0) {
					for (OndFareDTO ondFareDTO : collFares) {
						if (!ondFareDTO.isFlown() && fareSegChargeTO.hasBlockSeats() && collBlockID != null
								&& collBlockID.size() > 0) {
							for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
								Collection<SeatDistribution> seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
								for (SeatDistribution seatDist : seatDistributions) {
									if (seatDist.isOverbook() && !seatDist.isSameBookingClassAsExisting()
											&& !seatDist.isBookedFlightCabinBeingSearched()
											&& !segmentSeatDistsDTO.isFixedQuotaSeats()) {
										Collection<String> strFltSegIds = new ArrayList<String>();
										strFltSegIds.add(String.valueOf(segmentSeatDistsDTO.getFlightSegId()));
										ReservationModuleUtils.getFlightInventoryBD().doAuditOverBookings(strFltSegIds,
												segmentSeatDistsDTO.getBookingCode(), null, groupPnr,
												String.valueOf(segmentSeatDistsDTO.getFlightId()), userPrincipal.getUserId(),
												seatDist.getNoOfSeats());
									}
								}
							}
						}
					}
				}

				boolean otherCarrierPaxCreditUse = (Boolean) responce
						.getResponseParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED);

				AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
				clientReservation = ReservationConvertUtil.transform(reservation, airportBD);
				clientReservation.setUseOtherCarrierPaxCredit(otherCarrierPaxCreditUse);

			} catch (Exception exception) {
				log.error("Add segment operation failed for " + groupPnr, exception);
				// other carrier refund
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(paxWisePaymentAssemblers.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to revert other carrier payments for reservation " + groupPnr
								+ " for add segment operation");
					}
					AirproxyModuleUtils.getLCCReservationBD().refundPayment(contactInfo, groupPnr, transactionIdentifier,
							paxWisePaymentAssemblers, userPrincipal, trackInfoDTO);
					if (log.isDebugEnabled()) {
						log.debug("Successfully reverted other carrier payments for reservation " + groupPnr
								+ " for add segment operation");
					}
				}
				throw exception;
			}
		}
		return clientReservation;
	}

	private static Long getVersion(String version) {
		if (version.indexOf("-") != -1) {
			return new Long(version.split("-")[1]);
		}
		return new Long(version);
	}

	/**
	 * get Ground Segment
	 * 
	 * @param trackInfo
	 *            TODO
	 * @param selectedOnd
	 * @param isAddGroundSegment
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static List[] getGroundSegment(GroundSegmentTO groundSegmentTO, BasicTrackInfo trackInfo) throws ModuleException {
		List[] arrList = null;
		if (groundSegmentTO.isGroupPNRNo()) {
			arrList = AirproxyModuleUtils.getLCCReservationBD().getGroundSegment(groundSegmentTO, trackInfo);
		} else {
			arrList = SubStationUtil.getGroundSegmentCombo(groundSegmentTO.getSelectedOnd(),
					groundSegmentTO.isAddGroundSegment());
		}
		return arrList;
	}

	/**
	 * Transfer Segment
	 * 
	 * @param clientTransferSegmentTO
	 * @param trackInfo
	 *            TODO
	 * @param salesCahnnelKey TODO
	 * @throws ModuleException
	 */
	public static void transferSegments(LCCClientTransferSegmentTO clientTransferSegmentTO, BasicTrackInfo trackInfo, String salesCahnnelKey)
			throws ModuleException {

		Map<Integer, Integer> pnrSegIdAndNewFlgSegIdTemp = new HashMap<Integer, Integer>();

		if (clientTransferSegmentTO.isGroupPNR()) {
			AirproxyModuleUtils.getLCCReservationBD().transferSegments(clientTransferSegmentTO, trackInfo, salesCahnnelKey);
		} else {
			// FIXME Without loading Reservation for own airline
			Reservation reservation = loadReservation(clientTransferSegmentTO.getPnr());
			Collection<ReservationSegmentDTO> colSegs = reservation.getSegmentsView();

			if (reservation.getSegmentsView() != null) {
				for (ReservationSegmentDTO reservationSegment : colSegs) {
					Set<String> keySet = clientTransferSegmentTO.getOldNewFlightSegId().keySet();
					for (String segID : keySet) {
						if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
								&& segID.equals(FlightRefNumberUtil.composeFlightRPH(reservationSegment))
								&& clientTransferSegmentTO.getOldNewFlightSegId().get(segID) != reservationSegment
										.getFlightSegId()) {
							pnrSegIdAndNewFlgSegIdTemp.put(reservationSegment.getPnrSegId(),
									clientTransferSegmentTO.getOldNewFlightSegId().get(segID));
							break;
						}
					}
				}
			}

			AirproxyModuleUtils.getResSegmentBD().transferReservationSegment(clientTransferSegmentTO.getPnr(),
					pnrSegIdAndNewFlgSegIdTemp, clientTransferSegmentTO.isAlert(), false,
					Long.parseLong(clientTransferSegmentTO.getReservationVersion()),
					clientTransferSegmentTO.isSegmentTransferredByNorecProcess(), null, null, salesCahnnelKey);
		}
	}

	public static void clearAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {

		if (lccClientClearAlertDTO.getGroupPNR() != null && !lccClientClearAlertDTO.getGroupPNR().isEmpty()) {
			AirproxyModuleUtils.getLCCReservationBD().clearSegmentAlerts(lccClientClearAlertDTO, trackInfo);
		} else {
			// FIXME Without loading Reservation for own airline
			Reservation reservation = loadReservation(lccClientClearAlertDTO.getPNR());
			Collection<ReservationSegmentDTO> colSegs = reservation.getSegmentsView();

			Map<Integer, String> mapPnrSegActions = new HashMap<Integer, String>();
			Set<Integer> pnrSegIDs = lccClientClearAlertDTO.getPnrSegIds().keySet();
			for (int segID : pnrSegIDs) {
				for (ReservationSegmentDTO segment : colSegs) {
					if (segment.getPnrSegId() == segID) {
						mapPnrSegActions.put(segment.getPnrSegId(),
								" Action Taken : " + getAlertActionCode(lccClientClearAlertDTO.getActionTaken())
										+ " Action Text : " + lccClientClearAlertDTO.getActionText());
					}
				}
			}
			AirproxyModuleUtils.getResSegmentBD().clearSegmentAlerts(mapPnrSegActions);
		}
	}

	/**
	 * Consolidated Reservation Audit
	 * 
	 * @param pnr
	 * @param carrierCode
	 * @param isGropPNR
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static List<UserNoteTO> searchReservationAudit(String pnr, String carrierCode, boolean isGropPNR, String originChanel,
			BasicTrackInfo trackInfo) throws ModuleException {

		List<UserNoteTO> userNotes = new ArrayList<UserNoteTO>();
		if (isGropPNR) {
			userNotes = AirproxyModuleUtils.getLCCReservationBD().searchReservationAudit(pnr, carrierCode, trackInfo);
		} else {
			return searchOwnAirLineReservationAudit(pnr);
		}
		return userNotes;
	}

	private static List<UserNoteTO> searchOwnAirLineReservationAudit(String pnr) throws ModuleException {
		List<UserNoteTO> userNotes = new ArrayList<UserNoteTO>();
		Map<String, String> mapUsers = new HashMap<String, String>();
		Set<String> usres = new HashSet<String>();
		Collection<ReservationModificationDTO> colHistory = AirproxyModuleUtils.getAirReservationQueryBD().getPnrHistory(pnr);

		for (ReservationModificationDTO modDto : colHistory) {
			usres.add(modDto.getUserId());
		}
		if (!usres.isEmpty()) {
			Collection<User> colUsers = AirproxyModuleUtils.getSecurityBD().getBasicUserInformation(usres);
			for (User user : colUsers) {
				mapUsers.put(user.getUserId(), user.getDisplayName());
			}
		}

		for (ReservationModificationDTO modDto : colHistory) {
			userNotes.add(ReservationConvertUtil.transformUserNotes(modDto, mapUsers));
		}

		return userNotes;
	}

	public static String getHistoryForPrint(String pnr, BasicTrackInfo trackInfo) throws ModuleException {
		Collection<UserNoteTO> colUserNoteTO = searchOwnAirLineReservationAudit(pnr);
		Collection<UserNoteHistoryTO> colUserNoteHistoryTO = ReservationConvertUtil.transformUserNoteTO(colUserNoteTO);
		return AirproxyModuleUtils.getReservationBD().getHistoryForPrint(pnr, colUserNoteHistoryTO);
	}

	public static List<UserNoteTO> searchUserNoteHistory(String pnr, String carrierCode, boolean isGropPNR, String originChanel,
			BasicTrackInfo trackInfo, boolean isClassifyUN) throws ModuleException {

		List<UserNoteTO> userNotes = new ArrayList<UserNoteTO>();
		if (isGropPNR) {
			userNotes = AirproxyModuleUtils.getLCCReservationBD().searchUserNoteHistory(pnr, carrierCode, trackInfo,
					isClassifyUN);
		} else {
			Collection<ReservationModificationDTO> colUserNotes = AirproxyModuleUtils.getAirReservationQueryBD().getUserNotes(pnr,
					isClassifyUN);
			Map<String, String> mapUsers = new HashMap<String, String>();
			Set<String> usres = new HashSet<String>();
			Collection<User> colUsers = null;
			for (ReservationModificationDTO modDto : colUserNotes) {
				usres.add(modDto.getUserId());
			}
			if (!usres.isEmpty()) {
				colUsers = AirproxyModuleUtils.getSecurityBD().getBasicUserInformation(usres);
				for (User user : colUsers) {
					mapUsers.put(user.getUserId(), user.getDisplayName());
				}
			}
			for (ReservationModificationDTO modDto : colUserNotes) {
				userNotes.add(ReservationConvertUtil.transformUserNotes(modDto, mapUsers));
			}
		}
		return userNotes;
	}

	/**
	 * Consolidated Contact Information modification
	 * 
	 * @param pnr
	 * @param version
	 * @param lccClientReservationContactInfo
	 * @param oldLccClientReservationContactInfo
	 * @param isGropPNR
	 * @param appIndicator
	 *            TODO
	 * @param trackInfo
	 * @throws ModuleException
	 */
	public static void modifyContactInfo(String pnr, String version, CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, boolean isGropPNR, String appIndicator,
			TrackInfoDTO trackInfo) throws ModuleException {
		if (isGropPNR) {
			AirproxyModuleUtils.getLCCReservationBD().modifyContactInfo(pnr, version, lccClientReservationContactInfo,
					oldLccClientReservationContactInfo, appIndicator, trackInfo);
		} else {
			AirproxyModuleUtils.getReservationBD().updateContactInfo(pnr, new Long(version),
					AirProxyReservationUtil.transformContactInfo(lccClientReservationContactInfo), trackInfo);
		}

	}

	/**
	 * 
	 * @param lccClientReservation
	 * @param oldLccClientReservation
	 * @param isGropPNR
	 * @param appIndicator
	 * @param skipDuplicateNameCheck
	 * @param trackInfo
	 * @throws ModuleException
	 */
	public static void modifyPassengerInfo(LCCClientReservation lccClientReservation,
			LCCClientReservation oldLccClientReservation, boolean isGropPNR, UserPrincipal userPrincipal, String appIndicator,
			boolean skipDuplicateNameCheck, boolean applyNameChangeCharge, Collection<String> allowedOperations,
			TrackInfoDTO trackInfo) throws ModuleException {
		if (isGropPNR) {
			AirproxyModuleUtils.getLCCReservationBD().modifyPassengerInfo(lccClientReservation, oldLccClientReservation,
					allowedOperations, appIndicator, trackInfo);
		} else {
			Reservation reservation = loadReservation(lccClientReservation.getPNR());
			reservation.setUserNote(lccClientReservation.getLastUserNote());
			reservation.setUserNoteType(lccClientReservation.getUserNoteType());
			reservation.setEndorsementNote(lccClientReservation.getEndorsementNote());
			reservation.setBookingCategory(lccClientReservation.getBookingCategory());
			if (AppIndicatorEnum.APP_XBE.toString().equals(appIndicator)) {
				reservation.setOriginCountryOfCall(lccClientReservation.getOriginCountryOfCall());
			}
			reservation.setItineraryFareMaskFlag(lccClientReservation.getItineraryFareMask());
			// Make sure the version from the UI is set. Otherwise hibernate OptimisticLocking will be bypassed
			reservation.setVersion(Integer.parseInt(lccClientReservation.getVersion()));
			Set<ReservationPax> colPassengers = reservation.getPassengers();
			for (ReservationPax reservationPax : colPassengers) {
				for (LCCClientReservationPax clientPax : lccClientReservation.getPassengers()) {
					if (clientPax.getPaxSequence().compareTo(reservationPax.getPaxSequence()) == 0) {
						reservationPax = ReservationConvertUtil.transformPassenger(clientPax, reservationPax);
						break;
					}
				}
			}

			/*
			 * Check privilege & app parameter for duplicate name validation
			 */
			boolean dupNameCheck = AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateNameCheck;

			TrackInfoDTO trackInfoDTO = CommonUtil.getTrackInfoDto(userPrincipal);
			if (trackInfo != null) {
				trackInfoDTO.setDirectBillId(trackInfo.getDirectBillId());
			}

			AirproxyModuleUtils.getReservationBD().updateReservation(reservation, trackInfoDTO, dupNameCheck,
					applyNameChangeCharge, allowedOperations, lccClientReservation.getReasonForAllowBlPax());
		}
	}

	/**
	 * 
	 * @param lccClientResAlterModesTO
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */

	public static LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO, TrackInfoDTO trackInfo)
			throws ModuleException {

		LCCClientReservation clientReservation = null;
		if (lccClientResAlterModesTO.isGroupPnr()) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().cancelSegments(lccClientResAlterModesTO, trackInfo);
		} else {
			Reservation reservation = loadReservation(lccClientResAlterModesTO.getGroupPNR());

			CustomChargesTO customChargesTO = new CustomChargesTO(lccClientResAlterModesTO.getCustomAdultCharge(),
					lccClientResAlterModesTO.getCustomChildCharge(), lccClientResAlterModesTO.getCustomInfantCharge(),
					lccClientResAlterModesTO.getCustomAdultCharge(), lccClientResAlterModesTO.getCustomChildCharge(),
					lccClientResAlterModesTO.getCustomInfantCharge(), lccClientResAlterModesTO.getAdultCustomChargeTO(),
					lccClientResAlterModesTO.getChildCustomChargeTO(), lccClientResAlterModesTO.getInfantCustomChargeTO(),
					lccClientResAlterModesTO.isSendingAbosoluteCharge());
			customChargesTO.setDefaultCustomAdultCharge(lccClientResAlterModesTO.getDefaultCustomAdultCharge());
			customChargesTO.setDefaultCustomChildCharge(lccClientResAlterModesTO.getDefaultCustomChildCharge());
			customChargesTO.setDefaultCustomInfantCharge(lccClientResAlterModesTO.getDefaultCustomInfantCharge());

			AirproxyModuleUtils.getResSegmentBD().cancelSegmentsWithRequiresNew(lccClientResAlterModesTO.getGroupPNR(),
					getOldSegCollection(lccClientResAlterModesTO.getOldPnrSegs(), reservation), customChargesTO,
					getVersion(lccClientResAlterModesTO.getVersion()), true, trackInfo, false,
					lccClientResAlterModesTO.getUserNotes(), true);
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);

			// Check whether reservation canceled
			Reservation resModified = loadReservation(lccClientResAlterModesTO.getGroupPNR());

			if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(resModified.getStatus())) {
				// Sends an email to agent when cancel reservation
				Collection<String> pnrList = new ArrayList<String>();
				pnrList.add(lccClientResAlterModesTO.getGroupPNR());

				AirproxyModuleUtils.getReservationBD().sendBookingChangesEmailToAgent(pnrList,
						reservation.getAdminInfo().getOwnerAgentCode(), ResModifyEmailAgentModificationType.CANCEL_RESERVATION,
						null);
			}

			AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(reservation.getPnr(), false, trackInfo, false, null);
		}
		return clientReservation;
	}

	/**
	 * Cancel Reservation
	 * 
	 * @param lccClientResAlterModesTO
	 * @param userPrincipal
	 * @param trackInfo
	 * @param isVoidOperation
	 *            TODO
	 * @param isOnHoldRelease TODO
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo, boolean isVoidOperation, boolean isOnHoldRelease) throws ModuleException {
		LCCClientReservation clientReservation = null;
		if (lccClientResAlterModesTO.isGroupPnr()) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().cancelReservation(lccClientResAlterModesTO, trackInfo);
		} else {
			Reservation reservation = loadReservation(lccClientResAlterModesTO.getGroupPNR());
			int uniqueFareIds = AirProxyReservationUtil.getUniqueFareIds(reservation);

			if (uniqueFareIds > 0) {
				if (lccClientResAlterModesTO.getCustomAdultCharge() != null) {
					lccClientResAlterModesTO.setCustomAdultCharge(
							AccelAeroCalculator.divide(lccClientResAlterModesTO.getCustomAdultCharge(), uniqueFareIds));
				}
				if (lccClientResAlterModesTO.getCustomChildCharge() != null) {
					lccClientResAlterModesTO.setCustomChildCharge(
							AccelAeroCalculator.divide(lccClientResAlterModesTO.getCustomChildCharge(), uniqueFareIds));
				}
				if (lccClientResAlterModesTO.getCustomInfantCharge() != null) {
					lccClientResAlterModesTO.setCustomInfantCharge(
							AccelAeroCalculator.divide(lccClientResAlterModesTO.getCustomInfantCharge(), uniqueFareIds));
				}
			}
			
			Map<Integer, List<ExternalChgDTO>> paxCharges = AirProxyReservationUtil
					.composeHandlingFeeChargesByPaxSequence(lccClientResAlterModesTO.getLccClientSegmentAssembler());	
			
			CustomChargesTO customChargesTO = new CustomChargesTO(lccClientResAlterModesTO.getCustomAdultCharge(),
					lccClientResAlterModesTO.getCustomChildCharge(), lccClientResAlterModesTO.getCustomInfantCharge(),
					lccClientResAlterModesTO.getCustomAdultCharge(), lccClientResAlterModesTO.getCustomChildCharge(),
					lccClientResAlterModesTO.getCustomInfantCharge(), lccClientResAlterModesTO.getAdultCustomChargeTO(),
					lccClientResAlterModesTO.getChildCustomChargeTO(), lccClientResAlterModesTO.getInfantCustomChargeTO(),
					lccClientResAlterModesTO.isSendingAbosoluteCharge());

			int gdsNotifyAction = GDSInternalCodes.GDSNotifyAction.CANCEL_RESERVATION.getCode();
			if (isOnHoldRelease) {
				gdsNotifyAction = GDSInternalCodes.GDSNotifyAction.ONHOLD_RELEASE.getCode();
			}
			AirproxyModuleUtils.getReservationBD().cancelReservation(lccClientResAlterModesTO.getGroupPNR(), customChargesTO,
					getVersion(lccClientResAlterModesTO.getVersion()), false, false, false, trackInfo, isVoidOperation,
					lccClientResAlterModesTO.getUserNotes(), gdsNotifyAction, paxCharges);
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);

			// Sends an email to agent when cancel reservation
			Collection<String> pnrList = new ArrayList<String>();
			pnrList.add(lccClientResAlterModesTO.getGroupPNR());

			AirproxyModuleUtils.getReservationBD().sendBookingChangesEmailToAgent(pnrList,
					reservation.getAdminInfo().getOwnerAgentCode(), ResModifyEmailAgentModificationType.CANCEL_RESERVATION, null);

			AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(reservation.getPnr(), false, trackInfo, isVoidOperation,
					null);

		}
		if (clientReservation.getPassengers().size() >= AppSysParamsUtil.getMaximumPassengerCount()) {
			AirproxyModuleUtils.getReservationBD().sendGroupBookingNotificationMessages(clientReservation,
					RESERVATION_CANCELLATION);
		}
		return clientReservation;
	}

	/**
	 * 
	 * @param groupPNr
	 * @param version
	 * @param chargeAdjustments
	 * @param isGroupPNR
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	@Deprecated
	public static LCCClientReservation adjustCharge(String groupPNr, String version,
			List<LCCClientChargeAdustment> chargeAdjustments, boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto,
			TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientReservation clientReservation = null;

		if (isGroupPNR) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().adjustCharge(groupPNr, version, chargeAdjustments,
					clientInfoDto, trackInfo, null, null);
		} else {
			Reservation reservation = loadReservation(groupPNr);
			for (LCCClientChargeAdustment chargeAdjustment : chargeAdjustments) {
				// just in case
				List<String> elements = Arrays.asList(StringUtils.split(chargeAdjustment.getCarrierOndGroupRPH(), "|"));
				int chargeRateId = -1;

				// Get the charge adjustment type object for the selected adjustment type id.
				ChargeAdjustmentTypeDTO chargeAdjustmentType = ChargeAdjustmentTypeUtil
						.getChargeAdjustmentTypeDTO(chargeAdjustment.getAdjustmentTypeId());

				// Get the associated charge rate id from the charge adjustment type.
				if (chargeAdjustment.isRefundable()) {
					chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, true);
				} else {
					chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, false);
				}

				TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
				trackInfoDTO.setIpAddress(clientInfoDto.getIpAddress());
				trackInfoDTO.setCarrierCode(clientInfoDto.getCarrierCode());

				for (ReservationPax reservationPax : reservation.getPassengers()) {
					Integer pnrPaxId = getPnrPaxId(chargeAdjustment.getTravelerRefNumber());

					if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {

						if (!reservation.isInfantPaymentRecordedWithInfant()
								&& ReservationApiUtils.isInfantType(reservationPax)) {
							throw new ModuleException("Charge adjustments are not supported for infants");
						}

						for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
							if (elements.contains(String.valueOf(reservationPaxFare.getPnrPaxFareId()))) {
								ReservationApiUtils.isValidChargeAdjustment(reservationPaxFare, chargeAdjustment.getAmount(),
										null);
								AirproxyModuleUtils.getPassengerBD().adjustCreditManual(groupPNr,
										reservationPaxFare.getPnrPaxFareId(), chargeRateId, chargeAdjustment.getAmount(),
										chargeAdjustment.getUserNote(), new Long(version), null, trackInfoDTO, null);
							}
						}
					}
				}
			}
			reservation = loadReservation(groupPNr);
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);
		}

		return clientReservation;
	}

	/**
	 * Adjusts charges for multiple segments, multiple passengers. This method assumes the variable details are
	 * passengers and ONDs and everything else is the same for all the chageAdjustments(adjustment amount, user notes,
	 * adjustment type etc.)
	 * 
	 * @param groupPNr
	 * @param version
	 *            Current version of the reservation for optimistic locking
	 * @param chargeAdjustments
	 *            List of {@link LCCClientChargeAdustment}
	 * @param isGroupPNR
	 *            Whether this is a groupPNR (LCC booking) or not.
	 * @param clientInfoDto
	 * @param trackInfo
	 * @param serviceTaxRS
	 *            TODO
	 * @param handlingFeeByPax
	 * @return Reservation with updated details after the changes
	 * @throws ModuleException
	 *             If an error occurs during operation.
	 * @throws IllegalArgumentException
	 *             if an empty list is passed
	 */
	public static LCCClientReservation adjustGroupCharge(String groupPNr, String version,
			List<LCCClientChargeAdustment> chargeAdjustments, boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto,
			TrackInfoDTO trackInfo, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS, Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax) throws ModuleException {
		LCCClientReservation clientReservation = null;

		if (isGroupPNR) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().adjustCharge(groupPNr, version, chargeAdjustments,
					clientInfoDto, trackInfo, serviceTaxRS, handlingFeeByPax);
		} else {
			if (chargeAdjustments == null || chargeAdjustments.isEmpty()) {
				throw new IllegalArgumentException("Charge Adjustment list cannot be empty.");
			}
			Reservation reservation = loadReservation(groupPNr);
			List<Integer> pnrPaxFareIDs = new ArrayList<Integer>();

			// Common values taken from the first element of the list.
			int chargeRateId = -1;
			BigDecimal amount = chargeAdjustments.get(0).getAmount();
			String userNote = chargeAdjustments.get(0).getUserNote();

			// Get the charge adjustment type object for the selected adjustment type id.
			ChargeAdjustmentTypeDTO chargeAdjustmentType = ChargeAdjustmentTypeUtil
					.getChargeAdjustmentTypeDTO(chargeAdjustments.get(0).getAdjustmentTypeId());

			// Get the associated charge rate id from the charge adjustment type.
			if (chargeAdjustments.get(0).isRefundable()) {
				chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, true);
			} else {
				chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, false);
			}

			TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
			trackInfoDTO.setIpAddress(clientInfoDto.getIpAddress());
			trackInfoDTO.setCarrierCode(clientInfoDto.getCarrierCode());
			trackInfoDTO.setAppIndicator(trackInfo.getAppIndicator());
			for (LCCClientChargeAdustment chargeAdjustment : chargeAdjustments) {
				// just in case
				List<String> elements = Arrays.asList(StringUtils.split(chargeAdjustment.getCarrierOndGroupRPH(), "|"));

				for (ReservationPax reservationPax : reservation.getPassengers()) {
					Integer pnrPaxId = getPnrPaxId(chargeAdjustment.getTravelerRefNumber());

					if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {

						if (!reservation.isInfantPaymentRecordedWithInfant()
								&& ReservationApiUtils.isInfantType(reservationPax)) {
							throw new ModuleException("Charge adjustments are not supported for infants");
						}

						for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
							if (elements.contains(String.valueOf(reservationPaxFare.getPnrPaxFareId()))) {
								ReservationApiUtils.isValidChargeAdjustment(reservationPaxFare, amount, null);
								pnrPaxFareIDs.add(reservationPaxFare.getPnrPaxFareId());
							}
						}
					}
				}
			}

			AirproxyModuleUtils.getPassengerBD().adjustCreditManual(groupPNr, pnrPaxFareIDs, chargeRateId, amount, userNote,
					new Long(version), null, trackInfoDTO, serviceTaxRS,
					AirProxyReservationUtil.convertLCCClientDTOChgToAAChgDTO(handlingFeeByPax));

			reservation = loadReservation(groupPNr);
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);
		}

		return clientReservation;
	}

	/**
	 * 
	 * @param groupPNR
	 * @param extendDateTimeZulu
	 * @param version
	 * @param isGroupPnr
	 * @param userPrincipal
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version, boolean isGroupPnr,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientReservation clientReservation = null;

		if (isGroupPnr) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().extendOnHold(groupPNR, extendDateTimeZulu, version,
					trackInfo);
		} else {
			Reservation reservation = loadReservation(groupPNR);

			Collection<FlightSegmentDTO> colFlightSegmentDTOs = ReleaseTimeUtil
					.getConfirmedDepartureSegments(reservation.getSegmentsView());
			Date maxAllowedReleaseTimeZulu = ReleaseTimeUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTOs);
			Date currReleaseTime = reservation.getReleaseTimeStamps()[0]; // The zulutime

			if (extendDateTimeZulu.after(maxAllowedReleaseTimeZulu)) {
				extendDateTimeZulu = maxAllowedReleaseTimeZulu;// Release time is allowed up to earliest of flight
																// closure and interline cut-over
			}
			long lDiff = 0l;
			try {
				lDiff = BeanUtils.getIdealReleaseDate(extendDateTimeZulu, currReleaseTime);
			} catch (Exception e) {
				// TODO throw the exception with error codes
				throw new ModuleException("wrong time");
			}

			int intDiff = Math.round(lDiff / (60 * 1000));
			AirproxyModuleUtils.getReservationBD().extendOnholdReservation(groupPNR, intDiff, new Long(version), trackInfo);
			reservation = loadReservation(groupPNR);
			AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
			clientReservation = ReservationConvertUtil.transform(reservation, airportBD);

			// Sends an email to agent when on hold time extended
			Collection<String> pnrList = new ArrayList<String>();
			pnrList.add(groupPNR);

			Map<String, Map<String, Date>> onHoldTimeMap = new HashMap<String, Map<String, Date>>();

			Map<String, Date> pnrOnHoldMap = new HashMap<String, Date>();
			pnrOnHoldMap.put(ResModifyEmailAgentModificationConst.OLD_OHD_TIME, currReleaseTime);
			pnrOnHoldMap.put(ResModifyEmailAgentModificationConst.EXT_OHD_TIME, extendDateTimeZulu);

			onHoldTimeMap.put(groupPNR, pnrOnHoldMap);

			AirproxyModuleUtils.getReservationBD().sendBookingChangesEmailToAgent(pnrList,
					reservation.getAdminInfo().getOwnerAgentCode(), ResModifyEmailAgentModificationType.EXT_ON_HOLD,
					onHoldTimeMap);

		}
		return clientReservation;
	}

	public static LCCClientReservation modifyWaitListedPriority(String groupPNR, Integer priority, Integer fltSegId,
			String version, boolean isGroupPnr, UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientReservation clientReservation = null;

		Reservation reservation = loadReservation(groupPNR);

		AirproxyModuleUtils.getReservationBD().modifyWaitListedPriority(groupPNR, priority, fltSegId, new Long(version),
				trackInfo);
		reservation = loadReservation(groupPNR);
		AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
		clientReservation = ReservationConvertUtil.transform(reservation, airportBD);

		return clientReservation;
	}

	/**
	 * 
	 * @param groupPNR
	 * @param transfereeAgent
	 * @param version
	 * @param isGropPNR
	 * @param userPrincipal
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version,
			boolean isGropPNR, UserPrincipal userPrincipal, TrackInfoDTO trackInfo) throws ModuleException {
		LCCClientReservation clientReservation = null;

		if (isGropPNR) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().transferOwnership(groupPNR, transfereeAgent, version,
					trackInfo);
		} else {
			AirproxyModuleUtils.getReservationBD().transferOwnerShip(groupPNR, transfereeAgent, new Long(version), trackInfo);
		}
		return clientReservation;
	}

	/**
	 * 
	 * @param lccClientReservation
	 * @param trackInfoDTO
	 * @param isGropPNR
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, TrackInfoDTO trackInfoDTO,
			boolean isGropPNR) throws ModuleException {
		LCCClientReservation clientReservation = null;
		if (isGropPNR) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().splitReservation(lccClientReservation, trackInfoDTO);
		} else {
			clientReservation = new LCCClientReservation();
			Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();

			for (LCCClientReservationPax reservationPax : lccClientReservation.getPassengers()) {
				Integer pnrPaxId = getPnrPaxId(reservationPax.getTravelerRefNumber());
				colPnrPaxIds.add(pnrPaxId);
			}

			ServiceResponce serviceRes = AirproxyModuleUtils.getReservationBD().splitReservation(lccClientReservation.getPNR(),
					colPnrPaxIds, new Long(lccClientReservation.getVersion()), null, null, trackInfoDTO, null, null);

			clientReservation.setPNR((String) serviceRes.getResponseParam(CommandParamNames.PNR));
		}
		LCCClientPnrModesDTO pnrModesDTO = getDefaultLCCClientPnrModesDTO(clientReservation.getPNR());
		if (isGropPNR) {
			pnrModesDTO.setGroupPNR(clientReservation.getPNR());
		}
		LCCClientReservation newReservation = loadReservation(pnrModesDTO, null, trackInfoDTO);
		if (newReservation.getPassengers().size() >= AppSysParamsUtil.getMaximumPassengerCount()) {
			AirproxyModuleUtils.getReservationBD().sendGroupBookingNotificationMessages(newReservation, RESERVATION_CREATION);
		}
		return clientReservation;
	}

	/**
	 * 
	 * @param lccClientReservation
	 * @param isGroupPNR
	 * @param userPricipal
	 * @param enableFraudCheck
	 * @param autoCancellationEnabled
	 * @param hasBufferTimeAutoCnx
	 * @param trackInfoDTO
	 * @param colBlockIDs
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservation addInfant(LCCClientReservation lccClientReservation, Integer intPaymentMode,
			boolean isGroupPNR, UserPrincipal userPricipal, Map<Integer, LCCClientPaymentAssembler> payments,
			boolean enableFraudCheck, boolean autoCancellationEnabled, boolean hasBufferTimeAutoCnx, TrackInfoDTO trackInfoDTO,
			boolean mcETGenerationEligible, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap) throws Exception {
		LCCClientReservation clientReservation = null;

		if (isGroupPNR) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().addInfant(lccClientReservation, trackInfoDTO, payments,
					autoCancellationEnabled, mcETGenerationEligible);
		} else {
			String pnr = lccClientReservation.getPNR();
			Reservation reservation = loadReservation(pnr);
			String stationCode = userPricipal.getAgentStation();
			String agentCode = userPricipal.getAgentCode();
			ReservationMediator reservationMediator = new ReservationMediator(reservation);
			Collection<OndFareDTO> ondFareDTOs = reservationMediator.getOndFareDTOs(1); // Adding only one infant at a
																						// time
			boolean otherCarrierPayments = false;
			Collection<TempSegBcAlloc> colBlockIDs = AirproxyModuleUtils.getReservationBD().blockSeats(ondFareDTOs, null);

			Collection<OndFareDTO> inventoryFares = AirproxyModuleUtils.getFlightInventoryResBD().getInfantQuote(ondFareDTOs,
					stationCode, true, agentCode);
			IPassenger iPassenger = new PassengerAssembler(inventoryFares);

			Integer adultPaxSeq = null;
			LCCClientReservationPax infant = null;
			CommonReservationContactInfo contactInfo = lccClientReservation.getContactInfo();
			String transactionIdentifier = UUID.randomUUID().toString();

			for (LCCClientReservationPax pax : lccClientReservation.getPassengers()) {
				if (pax.getInfants() != null && !pax.getInfants().isEmpty()) {
					adultPaxSeq = pax.getPaxSequence();
				} else {
					infant = pax;
				}
			}

			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = ServiceTaxConverterUtil.createServiceTaxQuoteForAddInfantRequest(
					inventoryFares, reservation, userPricipal, externalChargesMap, adultPaxSeq);
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS = AirproxyModuleUtils.getChargeBD()
					.quoteServiceTaxForTicketingRevenue(serviceTaxRQ);
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap = new HashMap<String, ServiceTaxQuoteForTicketingRevenueResTO>();
			serviceTaxQuoteForTktRevResTOMap.put(AppSysParamsUtil.getDefaultCarrierCode(),
					ServiceTaxConverterUtil.adaptServiceTaxQuoteForTicketingRevenueRS(serviceTaxRS));
			List<LCCClientExternalChgDTO> groupedServiceTaxes = ServiceTaxCalculatorUtil
					.groupServiceTax(serviceTaxQuoteForTktRevResTOMap, PaxTypeTO.INFANT, adultPaxSeq);
			if (groupedServiceTaxes != null) {
				ServiceTaxExtCharges serviceTaxExtCharge = ServiceTaxConverterUtil
						.addLccServiceTaxesAsExtCharges(groupedServiceTaxes);
				if (externalChargesMap == null) {
					externalChargesMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
				}
				
				boolean isOtherExternalChargesExist = AirProxyReservationUtil.isOtherExternalChargesExist(payments.get(adultPaxSeq));
				
				externalChargesMap.put(EXTERNAL_CHARGES.SERVICE_TAX, serviceTaxExtCharge);
				BigDecimal totalExtCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (LCCClientExternalChgDTO groupedServiceTax : groupedServiceTaxes) {
					payments.get(adultPaxSeq).addExternalCharges(groupedServiceTax);
					totalExtCharges = AccelAeroCalculator.add(totalExtCharges, groupedServiceTax.getAmount());

				}
				if (!isOtherExternalChargesExist && totalExtCharges.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0
						&& payments.get(adultPaxSeq).getTotalPayAmount()
								.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
					for (LCCClientPaymentInfo payInfo : payments.get(adultPaxSeq).getPayments()) {
						payInfo.setTotalAmount(AccelAeroCalculator.subtract(payInfo.getTotalAmount(), totalExtCharges));
						payInfo.setIncludeExternalCharges(true);
					}
				}
			}

			try {

				// capture other carrier payments
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(payments.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to capture other carrier payments for reservation " + pnr + " for add infant");
					}
					payments = AirproxyModuleUtils.getLCCReservationBD().makePayment(contactInfo, pnr, transactionIdentifier,
							payments, userPricipal, trackInfoDTO);
					otherCarrierPayments = true;
					if (log.isDebugEnabled()) {
						log.debug("Successfully captured other carrier payments for reservation " + pnr + " for add infant");
					}
				}

				for (ReservationPax reservationPax : reservation.getPassengers()) {
					if (!reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
						/*
						 * iPassenger.addPassengerPayments(reservationPax.getPnrPaxId(),
						 * PaymentConvertUtil.populatePerPaxPayment(payments.get(reservationPax.getPnrPaxId()), null));
						 */
						iPassenger.addPassengerPayments(reservationPax.getPnrPaxId(), PaymentConvertUtil
								.populatePerPaxPayment(payments.get(reservationPax.getPaxSequence()), externalChargesMap));
						// setPassengerPayments(payments.get(reservationPax.getPnrPaxId()), iPassenger,
						// reservationPax.getPnrPaxId());
					}
				}

				String pspt = "";
				Date expDate = null;
				String psptIssued = "";
				String eticket = "";
				String employeeId = "";
				Date dateOfJoin = null;
				String idCategory = "";
				String nationalID = "";
				String placeOfBirth = "";
				String visaDocNumber = "";
				String visaApplicableCOuntry = "";
				String visaDocPlaceOfIssue = "";
				Date visaDocIssueDate = null;

				if (infant.getLccClientAdditionPax() != null) {
					pspt = infant.getLccClientAdditionPax().getPassportNo();
					expDate = infant.getLccClientAdditionPax().getPassportExpiry();
					psptIssued = infant.getLccClientAdditionPax().getPassportIssuedCntry();
					employeeId = infant.getLccClientAdditionPax().getEmployeeId();
					dateOfJoin = infant.getLccClientAdditionPax().getDateOfJoin();
					idCategory = infant.getLccClientAdditionPax().getIdCategory();
					nationalID = infant.getLccClientAdditionPax().getNationalIDNo();
					placeOfBirth = infant.getLccClientAdditionPax().getPlaceOfBirth();
					visaDocNumber = infant.getLccClientAdditionPax().getVisaDocNumber();
					visaApplicableCOuntry = infant.getLccClientAdditionPax().getVisaApplicableCountry();
					visaDocIssueDate = infant.getLccClientAdditionPax().getVisaDocIssueDate();
					visaDocPlaceOfIssue = infant.getLccClientAdditionPax().getVisaDocPlaceOfIssue();
				}

				AutoCancellationInfo autoCnxInfo = null;
				if (autoCancellationEnabled) {
					Date firstDepatureFlightDateTimeInZulu = AirProxyReservationUtil
							.getFirstDepartureFlightDate(lccClientReservation.getSegments());
					autoCnxInfo = AirproxyModuleUtils.getReservationBD().getAutoCancellationInfo(pnr, reservation,
							firstDepatureFlightDateTimeInZulu, null, AutoCancellationInfo.AUTO_CNX_TYPE.INF_EXP.toString(),
							hasBufferTimeAutoCnx, false);
				}

				iPassenger.addInfant(infant.getFirstName(), infant.getLastName(), infant.getTitle(), infant.getDateOfBirth(),
						infant.getNationalityCode(), infant.getPaxSequence(), adultPaxSeq, pspt, expDate, psptIssued, employeeId,
						dateOfJoin, idCategory, eticket, infant.getPaxCategory(), null, null, nationalID, placeOfBirth,
						visaDocNumber, visaApplicableCOuntry, visaDocPlaceOfIssue, visaDocIssueDate);

				AirproxyModuleUtils.getPassengerBD().addInfant(lccClientReservation.getPNR(), iPassenger, colBlockIDs,
						intPaymentMode, new Long(lccClientReservation.getVersion()), null, trackInfoDTO, enableFraudCheck, false,
						otherCarrierPayments, autoCnxInfo);
			} catch (Exception exception) {
				log.error("Add infant operation failed for " + pnr, exception);
				// other carrier refund
				if (ReservationApiUtils.isOtherCarrierPaymentsExist(payments.values())) {
					if (log.isDebugEnabled()) {
						log.debug("Going to revert other carrier payments for reservation " + pnr + " for add infant");
					}
					AirproxyModuleUtils.getLCCReservationBD().refundPayment(contactInfo, pnr, transactionIdentifier, payments,
							userPricipal, trackInfoDTO);
					if (log.isDebugEnabled()) {
						log.debug("Successfully reverted other carrier payments for reservation " + pnr + " for add infant");
					}
				}
				throw exception;
			}

		}
		return clientReservation;
	}

	public static InfantInfoTO getInfantCharge(String groupPNR, boolean allowAddInfant, UserPrincipal userPricipal)
			throws ModuleException {
		Reservation reservation = loadReservation(groupPNR);
		String stationCode = userPricipal.getAgentStation();
		String agentCode = userPricipal.getAgentCode();
		ReservationMediator reservationMediator = new ReservationMediator(reservation);

		Collection<OndFareDTO> ondFareDTOs = reservationMediator.getOndFareDTOs(1); // Adding only one infant at a time
		Collection<OndFareDTO> ondFareDTOColForInfFQ = AirproxyModuleUtils.getFlightInventoryResBD().getInfantQuote(ondFareDTOs,
				stationCode, allowAddInfant, agentCode);
		Iterator<OndFareDTO> ondFareDTOColForInfFQIt = ondFareDTOColForInfFQ.iterator();

		double infantCharge = 0;
		OndFareDTO ondFareDTO;
		while (ondFareDTOColForInfFQIt.hasNext()) {
			ondFareDTO = ondFareDTOColForInfFQIt.next();
			infantCharge += ondFareDTO.getInfantFare() + ondFareDTO.getTotalCharges()[1];
		}

		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = ServiceTaxConverterUtil
				.createServiceTaxQuoteForAddInfantRequest(ondFareDTOColForInfFQ, reservation, userPricipal, null, null);
		ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS = AirproxyModuleUtils.getChargeBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxRQ);
		if (serviceTaxRS.getPaxTypeWiseServiceTaxes() != null && !serviceTaxRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
			List<ServiceTaxDTO> infantServiceTaxs = serviceTaxRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT);
			for (ServiceTaxDTO serviceTax : infantServiceTaxs) {
				infantCharge += serviceTax.getAmount().doubleValue();
			}
		}
		BigDecimal infCharge = AccelAeroCalculator.parseBigDecimal(infantCharge);
		InfantInfoTO infoTo = new InfantInfoTO();
		infoTo.setInfantCharge(infCharge);
		return infoTo;
	}

	/**
	 * Extend or reinstate credit for a given pax
	 * 
	 * @param paxCreditReinstateTO
	 * @throws ModuleException
	 */
	public static void reInstatePaxCredit(PaxCreditReinstateTO paxCreditReinstateTO, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		// No need to send OWN requests to LCC in inter-line scenario
		if (paxCreditReinstateTO.isBlnGroupPnr()
				&& !paxCreditReinstateTO.getCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			AirproxyModuleUtils.getLCCPaxCreditBD().extendOrReinstateCredits(paxCreditReinstateTO.getMode(),
					paxCreditReinstateTO.getCreditId().intValue(), paxCreditReinstateTO.getExtendingDate(),
					paxCreditReinstateTO.getUserNote(), paxCreditReinstateTO.getPnr(), paxCreditReinstateTO.getPaxID(),
					paxCreditReinstateTO.getTransactionId(), paxCreditReinstateTO.getCreditAmount(),
					paxCreditReinstateTO.getOcCreditAmount(), paxCreditReinstateTO.getCarrierCode(), trackInfoDTO);
		} else {
			if (paxCreditReinstateTO.getMode().equals(PaxCreditReinstateTO.EXTEND)) {
				AirproxyModuleUtils.getPassengerBD().extendCredits(paxCreditReinstateTO.getCreditId(),
						paxCreditReinstateTO.getExtendingDate(), paxCreditReinstateTO.getUserNote(),
						paxCreditReinstateTO.getPnr(), paxCreditReinstateTO.getPaxID(), trackInfoDTO);
			} else if (paxCreditReinstateTO.getMode().equals(PaxCreditReinstateTO.RE_INSTATE)) {
				AirproxyModuleUtils.getPassengerBD().reinstateCredit(paxCreditReinstateTO.getPnr(),
						paxCreditReinstateTO.getPaxID(), paxCreditReinstateTO.getTransactionId(),
						AccelAeroCalculator.multiply(paxCreditReinstateTO.getCreditAmount(), -1),
						paxCreditReinstateTO.getExtendingDate(), paxCreditReinstateTO.getUserNote(), trackInfoDTO);
			}
		}
	}

	/**
	 * Group passenger refund with multiple transactions
	 * 
	 * @param isGroupPnr
	 * @param isManualRefund
	 */
	public static boolean groupPassengerRefund(CommonReservationAssembler reservationAssembler, String userNotes,
			boolean isGroupPnr, TrackInfoDTO trackInfoDTO, Collection<String> preferredRefundOrder,
			Collection<Long> pnrPaxOndChgIds, boolean removeAgentCommission, boolean isManualRefund) throws ModuleException {
		ServiceResponce sr = null;
		boolean success = false;
		if (isGroupPnr) {
			success = AirproxyModuleUtils.getLCCReservationBD().refundPassengers(reservationAssembler, userNotes, trackInfoDTO,
					preferredRefundOrder, pnrPaxOndChgIds, isManualRefund);
		} else {
			IPassenger iPassenger = new PassengerAssembler(null);

			Set<LCCClientReservationPax> passengers = reservationAssembler.getLccreservation().getPassengers();
			for (LCCClientReservationPax reservationPax : passengers) {
				Integer pnrPaxId = getPnrPaxId(reservationPax.getTravelerRefNumber());
				IPayment iPayment = PaymentConvertUtil.populatePerPaxPayment(reservationPax.getLccClientPaymentAssembler(), null);
				iPassenger.addPassengerPayments(pnrPaxId, iPayment);
			}

			sr = AirproxyModuleUtils.getPassengerBD().groupPassengerRefund(reservationAssembler.getLccreservation().getPNR(),
					iPassenger, userNotes, true, Long.valueOf(reservationAssembler.getLccreservation().getVersion()),
					trackInfoDTO, false, preferredRefundOrder, pnrPaxOndChgIds, removeAgentCommission, isManualRefund);
			success = sr.isSuccess();
		}

		return success;
	}

	public static LCCClientReservation removePassenger(String groupPnr, String version,
			List<LCCClientReservationPax> reservationPaxs, boolean isGroupPnr, CustomChargesTO customChargesTO,
			TrackInfoDTO trackInfoDTO, String userNotes, Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException {

		ServiceResponce sr = null;
		LCCClientReservation clientReservation = null;
		if (isGroupPnr) {
			clientReservation = AirproxyModuleUtils.getLCCReservationBD().removePassenger(groupPnr, version, reservationPaxs,
					customChargesTO, trackInfoDTO, paxExternalCharges);
		} else {
			clientReservation = new LCCClientReservation();
			Collection<Integer> colPax = new ArrayList<Integer>();

			for (LCCClientReservationPax pax : reservationPaxs) {
				colPax.add(getPnrPaxId(pax.getTravelerRefNumber()));
			}

			Reservation reservation = loadReservation(groupPnr);
			int uniqueFareIds = AirProxyReservationUtil.getUniqueFareIds(reservation);

			if (uniqueFareIds > 0) {
				if (customChargesTO.getCustomAdultCCharge() != null) {
					customChargesTO.setCustomAdultCCharge(
							AccelAeroCalculator.divide(customChargesTO.getCustomAdultCCharge(), uniqueFareIds));
				}
				if (customChargesTO.getCustomChildCCharge() != null) {
					customChargesTO.setCustomChildCCharge(
							AccelAeroCalculator.divide(customChargesTO.getCustomChildCCharge(), uniqueFareIds));
				}
				if (customChargesTO.getCustomInfantCCharge() != null) {
					customChargesTO.setCustomInfantCCharge(
							AccelAeroCalculator.divide(customChargesTO.getCustomInfantCCharge(), uniqueFareIds));
				}
			}
			
			Map<Integer, List<ExternalChgDTO>> paxCharges = AirProxyReservationUtil
					.convertLCCClientDTOChgToAAChgDTO(paxExternalCharges);

			sr = AirproxyModuleUtils.getPassengerBD().removePassengersWithRequiresNew(groupPnr, null, colPax, customChargesTO,
					new Long(version), trackInfoDTO, userNotes, paxCharges);

			String newPnr = (String) sr.getResponseParam(CommandParamNames.PNR);
			AirproxyModuleUtils.getPassengerBD().passengerAutoRefund(newPnr, false, trackInfoDTO, false, null);

			clientReservation.setPNR((String) sr.getResponseParam(CommandParamNames.PNR));
			DefaultServiceResponse defaultServiceResponse = (DefaultServiceResponse) sr
					.getResponseParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
			defaultServiceResponse.getResponseParam(CommandParamNames.PNR);
			clientReservation.setPNR((String) defaultServiceResponse.getResponseParam(CommandParamNames.PNR));
		}

		return clientReservation;

	}

	private static LCCClientPnrModesDTO getDefaultLCCClientPnrModesDTO(String pnr) {
		// moved from loadReservation(String pnr)
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(false);
		pnrModesDTO.setLoadLocalTimes(false); // For IBE TODO Review
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(false);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		return pnrModesDTO;
	}

	/**
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation loadReservation(String pnr) throws ModuleException {
		Reservation reservation = null;
		LCCClientPnrModesDTO pnrModesDTO = getDefaultLCCClientPnrModesDTO(pnr);
		reservation = AirproxyModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		return reservation;
	}

	private static Integer getPnrPaxId(String lccTravelerReferenceNumber) {
		int pnrPaxIdStartIndex = lccTravelerReferenceNumber.lastIndexOf("$");
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = lccTravelerReferenceNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

	/**
	 * 
	 * @param oldPnrSegs
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<Integer> getOldSegCollection(Collection<LCCClientReservationSegment> oldPnrSegs,
			Reservation reservation) throws ModuleException {
		Collection<Integer> colOldsegs = new ArrayList<Integer>();
		Collection<ReservationSegment> colSegDTo = reservation.getSegments();

		for (LCCClientReservationSegment clientSeg : oldPnrSegs) {
			for (ReservationSegment segDto : colSegDTo) {
				if ((segDto.getFlightSegId()
						.compareTo(FlightRefNumberUtil.getSegmentIdFromFlightRPH(clientSeg.getFlightSegmentRefNumber())) == 0)
						&& segDto.getOndGroupId().toString().equals(clientSeg.getInterlineGroupKey())
						&& segDto.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					colOldsegs.add(segDto.getPnrSegId());
				}
			}

		}

		return colOldsegs;
	}

	private static Collection<Integer> getNewFlightSegCollection(Collection<OndFareDTO> colFares) throws ModuleException {
		Collection<Integer> colNewFlightSegs = new ArrayList<Integer>();
		LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap = null;
		for (OndFareDTO fareDto : colFares) {
			segmentsMap = fareDto.getSegmentsMap();
			if (segmentsMap != null && !segmentsMap.isEmpty()) {
				for (Integer flightSegId : segmentsMap.keySet()) {
					colNewFlightSegs.add(flightSegId);
				}
			}
		}
		return colNewFlightSegs;
	}

	/**
	 * gets a modified return ond group id
	 * 
	 * @param oldReservation
	 * @return
	 */
	private static int getModifiedONDReturnGroupId(Collection<LCCClientReservationSegment> oldPnrSegs,
			Reservation oldReservation) {
		int returnONDGroupId = 0;
		Collection<ReservationSegment> colSegDTo = oldReservation.getSegments();
		for (LCCClientReservationSegment clientSeg : oldPnrSegs) {
			for (ReservationSegment segDto : colSegDTo) {
				if (segDto.getFlightSegId()
						.compareTo(FlightRefNumberUtil.getSegmentIdFromFlightRPH(clientSeg.getFlightSegmentRefNumber())) == 0) {
					if (segDto.getReturnOndGroupId() != null) {
						returnONDGroupId = segDto.getReturnOndGroupId().intValue();
					}
				}
			}

		}
		return returnONDGroupId;
	}

	private static boolean isHalfReturnQuotedForModifySeg(Collection<FlightSegmentDTO> inverseSegments,
			Collection<OndFareDTO> colOndFares, boolean isInboundFareModified) throws ModuleException {

		Date outboundDepartureDateTime = null;
		Date inboundDepartureDateTime = null;
		Date outboundArrivalDateTime = null;

		List colInverseFlightSegments = (List) inverseSegments;

		ArrayList<FlightSegmentDTO> newFlightSegments = new ArrayList<FlightSegmentDTO>();
		for (OndFareDTO ondFareDTO : colOndFares) {
			newFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
		}
		if (isInboundFareModified) {
			// dep. date of first inverse seg in case of connection flight.
			outboundDepartureDateTime = ((FlightSegmentDTO) colInverseFlightSegments.get(0)).getDepartureDateTime();
			outboundArrivalDateTime = ((FlightSegmentDTO) colInverseFlightSegments.get(colInverseFlightSegments.size() - 1))
					.getArrivalDateTime();
			inboundDepartureDateTime = (newFlightSegments.get(0)).getDepartureDateTime();
		} else {
			// from start of day
			outboundDepartureDateTime = (newFlightSegments.get(0)).getDepartureDateTime();
			outboundArrivalDateTime = (newFlightSegments.get(newFlightSegments.size() - 1)).getArrivalDateTime();
			inboundDepartureDateTime = ((FlightSegmentDTO) colInverseFlightSegments.get(0)).getDepartureDateTime();
		}

		Calendar selectedOutboundArrivalTime = new GregorianCalendar();
		;
		Calendar selectedInboundDepartureTime = new GregorianCalendar();

		selectedOutboundArrivalTime.setTime(outboundArrivalDateTime);
		selectedInboundDepartureTime.setTime(inboundDepartureDateTime);

		// Check whether return transition time is ok
		String minRetTransitionTimeStr = CommonsServices.getGlobalConfig()
				.getBizParam(SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
		String hours = minRetTransitionTimeStr.substring(0, minRetTransitionTimeStr.indexOf(":"));
		String mins = minRetTransitionTimeStr.substring(minRetTransitionTimeStr.indexOf(":") + 1);

		GregorianCalendar selectedOutboundArrivalTimeWithTran = new GregorianCalendar();
		selectedOutboundArrivalTimeWithTran.setTime(selectedOutboundArrivalTime.getTime());
		selectedOutboundArrivalTimeWithTran.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		selectedOutboundArrivalTimeWithTran.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		// Half return fare applicable only if the modified booking is similar to existing return booking with
		// change in dates only
		if (outboundDepartureDateTime.compareTo(inboundDepartureDateTime) < 0
				&& selectedOutboundArrivalTimeWithTran.getTime().before(selectedInboundDepartureTime.getTime())) {
			return true;
		} else {
			return false;
		}

	}

	public static List<LCCClientReservationInsurance> resellInsurance(SYSTEM system, String pnr, List<Integer> insuranceIds)
			throws ModuleException {

		List<LCCClientReservationInsurance> updatedClientInsurances = new ArrayList<LCCClientReservationInsurance>();

		if (AppSysParamsUtil.isShowTravelInsurance()) {
			if (system == SYSTEM.AA) {
				if (insuranceIds != null && !insuranceIds.isEmpty()) {

					AirproxyModuleUtils.getReservationAuxilliaryBD().reconcileFailedInsurances(pnr, insuranceIds);

					// retrieval
					List<ReservationInsurance> reservationInsurances = new ArrayList<ReservationInsurance>();
					for (Integer insuranceId : insuranceIds) {
						ReservationInsurance persistedReservationInsurance = ReservationModuleUtils.getReservationAuxilliaryBD()
								.getInsurance(insuranceId);
						reservationInsurances.add(persistedReservationInsurance);
					}

					updatedClientInsurances = ReservationConvertUtil.transformInsurance(reservationInsurances);
				}
			}

		} // Interline not handled yet

		return updatedClientInsurances;
	}

	public static List<LCCClientReservationInsurance> loadReservationInsuranceByPnr(String pnr) throws ModuleException {
		List<ReservationInsurance> insurance = AirproxyModuleUtils.getAirReservationQueryBD().getReservationInsuranceByPnr(pnr);
		return ReservationConvertUtil.transformInsurance(insurance);

	}

	/**
	 * Business logic for Confirming the open return segments.
	 * 
	 * @param pnr
	 * @param lccClientResAlterQueryModesTO
	 * @param oldSegIds
	 * @param blockSeatIds
	 * @param userPrincipal
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public LCCClientReservation confirmOpenReturnSegments(String pnr, LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			Collection<Integer> oldSegIds, Collection<TempSegBcAlloc> blockSeatIds, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		LCCClientReservation lccClientReservation = null;
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		Reservation reservation = AirproxyModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		SegmentAssemblerConvertUtil convertUtil = new SegmentAssemblerConvertUtil();
		FlightPriceRQ priceQuoteRQ = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().getFlightPriceRQ();
		FareSegChargeTO fareSegChargeTO = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler()
				.getSelectedFareSegChargeTO();
		boolean isFlexiQuote = lccClientResAlterQueryModesTO.getLccClientSegmentAssembler().isFlexiSelected();
		OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ, fareSegChargeTO);
		Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD()
				.recreateFareSegCharges(ondRebuildCriteria);

		Integer modifiedONDReturnGrp = getModifiedONDReturnGroupId(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation);
		ISegment iSegment = convertUtil.getSegmentAssembler(collFares,
				lccClientResAlterQueryModesTO.getLccClientSegmentAssembler(), reservation,
				getOldSegCollection(lccClientResAlterQueryModesTO.getOldPnrSegs(), reservation), modifiedONDReturnGrp);
		Collection<Integer> oldPnrSegIds = getOldPnrSegIds(reservation, oldSegIds);
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

		ServiceResponce sr = AirproxyModuleUtils.getResSegmentBD().confirmOpenReturnSegments(pnr, oldPnrSegIds, iSegment,
				blockSeatIds, intPaymentMode, reservation.getVersion(), trackInfoDTO);

		if (sr.isSuccess()) {
			reservation = (Reservation) sr.getResponseParam(CommandParamNames.RESERVATION);
			if (reservation != null) {
				AirportBD airportBD = AirproxyModuleUtils.getAirportBD();
				lccClientReservation = ReservationConvertUtil.transform(reservation, airportBD, pnrModesDTO);
			}

		} else {
			AirproxyModuleUtils.getReservationBD().releaseBlockedSeats(blockSeatIds);

		}
		return lccClientReservation;
	}

	/**
	 * Retrieves the pnr segment ids for the given reservation segment ids.
	 * 
	 * @param reservation
	 * @param oldSegIds
	 * @return
	 */
	private Collection<Integer> getOldPnrSegIds(Reservation reservation, Collection<Integer> oldSegIds) {
		Collection<Integer> oldPnrSegIds = new ArrayList<Integer>();
		for (ReservationSegmentDTO reservationSegmentDTO : (List<ReservationSegmentDTO>) reservation.getSegmentsView()) {
			if (oldSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				oldPnrSegIds.add(reservationSegmentDTO.getPnrSegId());
			}
		}
		return oldPnrSegIds;
	}

	/**
	 * Checks whether the modifying segment is a confirmed segment of open return booking, with open segment
	 * 
	 * @param oldSegmentIds
	 * @param reservation
	 * @return
	 */
	private static boolean isModifyingConfSegOfOpenReturn(Collection<Integer> oldSegmentIds, Reservation reservation) {

		boolean isModifyingConfSegOfOr = false;

		List<ReservationSegmentDTO> segmentDTOs = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		for (Integer segId : oldSegmentIds) {
			Integer returnGroupId = null;
			for (ReservationSegmentDTO segment : segmentDTOs) {
				if (segment.getPnrSegId().equals(segId)) {
					returnGroupId = segment.getReturnOndGroupId();
					break;
				}
			}

			if (returnGroupId != null) {
				for (ReservationSegmentDTO segmentOr : segmentDTOs) {
					if (!segmentOr.getPnrSegId().equals(segId) && returnGroupId.equals(segmentOr.getReturnOndGroupId())
							&& segmentOr.getOpenRtConfirmBeforeZulu() != null
							&& segmentOr.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
						isModifyingConfSegOfOr = true;
						break;
					}
				}
			}

		}

		return isModifyingConfSegOfOr;

	}

	/**
	 * Loads bookingCode and cabin class details for confirming open return segments in order to search for the same
	 * booking class (which is used for outbound segment) to be included in the inbound confirming flight.
	 * 
	 * @param segmentIds
	 *            - New segment ID's
	 * @param pnr
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Map<String, List<Integer>>> loadReservationSegmentsBypnrBySegmentID(Collection<String> segmentIds,
			String pnr, TrackInfoDTO trackInfo) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		Reservation reservation = AirproxyModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackInfo);

		Map<String, Map<String, List<Integer>>> mapBookingCodeAndCabinClass = new HashMap<String, Map<String, List<Integer>>>();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		Collection<Integer> returnGroupIds = new HashSet<Integer>();
		Collection<Integer> inversePnrSegIds = new HashSet<Integer>();
		ReservationSegmentDTO reservationSegmentDTO;
		if (reservation != null && segmentIds != null) {
			for (Object element : reservation.getSegmentsView()) {
				reservationSegmentDTO = (ReservationSegmentDTO) element;

				if (segmentIds.contains(String.valueOf(reservationSegmentDTO.getPnrSegId()))) {
					returnGroupIds.add(reservationSegmentDTO.getReturnGroupId());
				}
			}

			for (Object element : reservation.getSegmentsView()) {
				reservationSegmentDTO = (ReservationSegmentDTO) element;

				if (returnGroupIds.contains(reservationSegmentDTO.getReturnGroupId())
						&& !reservationSegmentDTO.isOpenReturnSegment()
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegmentDTO.getStatus())) {
					inversePnrSegIds.add(reservationSegmentDTO.getPnrSegId());
				}
			}

			for (Object element : reservation.getPassengers()) {
				reservationPax = (ReservationPax) element;

				if (!ReservationApiUtils.isInfantType(reservationPax)) {

					for (Object element2 : reservationPax.getPnrPaxFares()) {
						reservationPaxFare = (ReservationPaxFare) element2;

						for (Object element3 : reservationPaxFare.getPaxFareSegments()) {
							reservationPaxFareSegment = (ReservationPaxFareSegment) element3;

							// previous logic was used to capture the OB segments CC & BC this logic has been revised
							// since half return is enabled for OPEN RETURN booking
							if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
									.equals(reservationPaxFareSegment.getSegment().getStatus())
									&& !inversePnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {

								String bookingCode = getBookingClassCode(reservationPaxFare.getFareId());

								if (!StringUtils.isBlank(bookingCode)) {
									if (!mapBookingCodeAndCabinClass.containsKey(bookingCode)) {
										mapBookingCodeAndCabinClass.put(bookingCode, new HashMap<String, List<Integer>>());
									}

									Map<String, List<Integer>> cabinClassSelection = mapBookingCodeAndCabinClass.get(bookingCode);

									if (!cabinClassSelection
											.containsKey(reservationPaxFareSegment.getSegment().getCabinClassCode())) {
										cabinClassSelection.put(reservationPaxFareSegment.getSegment().getCabinClassCode(),
												new ArrayList<Integer>());
									}

									cabinClassSelection.get(reservationPaxFareSegment.getSegment().getCabinClassCode())
											.add(reservationPaxFareSegment.getSegment().getFlightSegId());
								}

							}
						}
					}
				}
			}
		}
		if (mapBookingCodeAndCabinClass.size() == 1) {
			return mapBookingCodeAndCabinClass;
		}

		throw new ModuleException("airreservations.openreturn.invalid.open.return.segments");
	}

	public static Integer getAppliedFareDiscountPercentage(String pnr, boolean isGropPNR) throws ModuleException {
		if (isGropPNR) {
			// TODO: when enabling FARE DISCOUNT for dry/interline to be handled
			return 0;
		} else {
			return AirproxyModuleUtils.getReservationBD().getAppliedFareDiscountPercentage(pnr);
		}
	}

	public static BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resTO,
			TrackInfoDTO trackInfoDTO) throws ModuleException {

		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (resTO.isGroupPnr()) {
			balanceToPay = AirproxyModuleUtils.getLCCReservationBD().calculateBalanceToPayAfterAutoCancellation(resTO,
					trackInfoDTO);
		} else {
			Collection<LCCClientReservationSegment> cancelingPnrSegs = resTO.getOldPnrSegs();
			Map<String, Collection<Integer>> groupWiseSegIds = new HashMap<String, Collection<Integer>>();
			if (cancelingPnrSegs != null && !cancelingPnrSegs.isEmpty()) {
				for (LCCClientReservationSegment resSeg : cancelingPnrSegs) {
					String interlineGroupKey = resSeg.getInterlineGroupKey();
					if (!groupWiseSegIds.containsKey(interlineGroupKey)) {
						groupWiseSegIds.put(interlineGroupKey, new ArrayList<Integer>());
					}

					Integer pnrSegId = new Integer(resSeg.getBookingFlightSegmentRefNumber());
					groupWiseSegIds.get(interlineGroupKey).add(pnrSegId);
				}
			}

			List<String> paxInfantList = resTO.getPaxInfantList();
			List<Integer> autoCancelingInfants = new ArrayList<Integer>();

			for (String infantRefNum : paxInfantList) {
				autoCancelingInfants.add(PaxTypeUtils.getPnrPaxId(infantRefNum));
			}

			balanceToPay = AirproxyModuleUtils.getReservationBD().calculateBalanceToPayAfterAutoCancellation(groupWiseSegIds,
					autoCancelingInfants, null, resTO.getGroupPNR(), getVersion(resTO.getVersion()));

		}
		return balanceToPay;
	}

	public static Map<String, BigDecimal> getOndWideCustomCharges(BigDecimal customCharge, Reservation reservation,
			Integer uniqueFareIDs) throws ModuleException {

		Collection<ReservationSegmentDTO> pnrSegments = reservation.getSegmentsView();

		BigDecimal[] ondWiseCharges = AccelAeroCalculator.roundAndSplit(customCharge, uniqueFareIDs);

		Map<String, BigDecimal> ondWiseCustomCharges = new HashMap<String, BigDecimal>();
		int count = 0;
		for (ReservationSegmentDTO reservationSegmentDTO : pnrSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {
				if (count < ondWiseCharges.length) {
					if (null == ondWiseCustomCharges.put(reservationSegmentDTO.getFareTO().getOndCode(), ondWiseCharges[count])) {
						count++;
					}
				}

			}
		}
		return ondWiseCustomCharges;
	}

	public static String getBookingClassCode(Integer fareId) throws ModuleException {
		String bookingCode = null;

		if (fareId != null) {
			FareDTO fareDTO = ReservationModuleUtils.getFareBD().getFare(fareId);

			if (fareDTO != null && fareDTO.getFare() != null) {
				bookingCode = fareDTO.getFare().getBookingCode();
			}

		}
		return bookingCode;
	}

	public static ServiceResponce updatePaxNames(Map<Integer, NameDTO> changedPaxNamesMap, String pnr, long version,
			String appIndicator, boolean skipDuplicateNameCheck, TrackInfoDTO trackInfo, boolean isGroupPNR, String reasonForAllowBlPax)
			throws ModuleException {

		if (isGroupPNR) {
			DefaultServiceResponse serviceResponce = new DefaultServiceResponse(true);
			LCCClientPnrModesDTO lccClientPnrModesDTO = getDefaultLCCClientPnrModesDTO(pnr);
			lccClientPnrModesDTO.setGroupPNR(pnr);
			LCCClientReservation lccClientReservation = AirproxyModuleUtils.getLCCReservationBD()
					.searchReservationByPNR(lccClientPnrModesDTO, null, trackInfo);

			if (lccClientReservation != null) {
				LCCClientReservation oldLccClientReservation = new LCCClientReservation();
				oldLccClientReservation.setPNR(pnr);
				oldLccClientReservation.setVersion(lccClientReservation.getVersion());
				oldLccClientReservation.setLastUserNote(lccClientReservation.getLastUserNote());

				for (LCCClientReservationPax lccClientReservationPax : lccClientReservation.getPassengers()) {
					oldLccClientReservation.addPassenger(lccClientReservationPax);
					for (Integer pnrPaxId : changedPaxNamesMap.keySet()) {
						NameDTO nameDTO = changedPaxNamesMap.get(pnrPaxId);
						Integer paxSequence = PaxTypeUtils.getPaxSeq(nameDTO.getPaxReference());
						if (paxSequence.equals(lccClientReservationPax.getPaxSequence())) {
							lccClientReservationPax.setTitle(nameDTO.getTitle());
							lccClientReservationPax.setFirstName(nameDTO.getFirstname());
							lccClientReservationPax.setLastName(nameDTO.getLastName());
							lccClientReservationPax.getLccClientAdditionPax().setFfid(nameDTO.getFFID());
						}
					}
				}
				lccClientReservation.setReasonForAllowBlPax(reasonForAllowBlPax);
				AirproxyModuleUtils.getLCCReservationBD().modifyPassengerInfo(lccClientReservation, oldLccClientReservation,
						null, appIndicator, trackInfo);
			} else {
				serviceResponce.setSuccess(false);
			}

			return serviceResponce;

		} else {
			return AirproxyModuleUtils.getReservationBD().updatePaxNames(changedPaxNamesMap, pnr, version, appIndicator,
					skipDuplicateNameCheck, trackInfo,reasonForAllowBlPax);
		}
	}

	public static void exchangeSegments(boolean isGroupPnr, String pnr, TrackInfoDTO trackInfo) throws ModuleException {
		// API call to TypeA request, Obtain coupon control & exchange the tickets
		ReIssueTicketRq reIssueTicketRq = new ReIssueTicketRq();
		reIssueTicketRq.setPnr(pnr);
		ReIssueTicketRs reIssueTicketRs = AirproxyModuleUtils.getGdsRequiredBD().reIssueTicket(reIssueTicketRq);

		TicketOperationsHandler ticketOperationsHandler;
		TicketingEventRq ticketingEventRq;
		Map<String, GDSStatusTO> gdsMap = CommonsServices.getGlobalConfig().getActiveGdsMap();

		if (reIssueTicketRs != null && reIssueTicketRs.isSuccess()) {
			if (isGroupPnr) {
				// TODO
			} else {
				AirproxyModuleUtils.getResSegmentBD().exchangeReservationSegment(pnr, reIssueTicketRs.getTransitions(),
						trackInfo);

				ticketingEventRq = new TicketingEventRq();
				ticketingEventRq.setPnr(pnr);
				ticketingEventRq.setTransitions(reIssueTicketRs.getTransitions());
				ticketingEventRq.setTicketEvent(TicketingEventRq.Event.RE_ISSUE);

				ticketOperationsHandler = TicketOperationsHandler.getTicketOperationsHandler(
						gdsMap.get(String.valueOf(reIssueTicketRs.getGdsId())).isAaValidatingCarrier());
				ticketOperationsHandler.internalTicketReIssue(ticketingEventRq);

			}
		} else {
			// Sorry unable to complete your request from validating carrier
			throw new ModuleException("airreservations.ext.coupon.exchange.cpn.ctrl.request.failed");
		}

	}

	// method to add user Note by add user note button
	public static void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		if (userNoteTO != null) {
			if (userNoteTO.isGroupPNR()) {
				AirproxyModuleUtils.getLCCReservationBD().addUserNote(userNoteTO, trackInfoDTO);
			} else {
				AirproxyModuleUtils.getAirReservationQueryBD().addUserNote(userNoteTO, trackInfoDTO);
			}
		}

	}

	public static void noShowTaxChargesReversal(String pnr, boolean isGroupPnr, Collection<Integer> pnrPaxIds,
			Collection<Integer> pnrSegIds, TrackInfoDTO trackInfoDTO) throws ModuleException {
		// TODO: DRY & Interline
		AirproxyModuleUtils.getPassengerBD().reverseRefundableCharges(pnr, pnrPaxIds, pnrSegIds, true, trackInfoDTO);

	}

	public static void noShowTaxChargesReversal(String pnr, boolean isGroupPnr, String version,
			List<LCCClientChargeReverse> chargeReverseList, ClientCommonInfoDTO clientInfoDto, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		if (isGroupPnr) {

			AirproxyModuleUtils.getLCCReservationBD().reverseRefundableCharges(pnr, version, true, chargeReverseList,
					clientInfoDto, trackInfoDTO);

		} else {

			if (chargeReverseList != null && !chargeReverseList.isEmpty()) {
				Collection<Integer> pnrPaxIds = new ArrayList<>();
				for (LCCClientChargeReverse chargeReverse : chargeReverseList) {
					List<String> travelerRefNumbersList = chargeReverse.getTravelerRefNumbersList();
					if (travelerRefNumbersList != null && !travelerRefNumbersList.isEmpty()) {
						for (String refNumbersList : travelerRefNumbersList) {
							pnrPaxIds.add(getPnrPaxId(refNumbersList));
						}
					}

				}

				if (!pnrPaxIds.isEmpty()) {
					AirproxyModuleUtils.getPassengerBD().reverseRefundableCharges(pnr, pnrPaxIds, null, true, trackInfoDTO);
				}

			}

		}

	}
	public static Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo, boolean isGroupPNR, String operatingCarrier) throws ModuleException{
		
		if(isGroupPNR){
			return AirproxyModuleUtils.getLCCReservationBD().getSelfReprotectFlights(alertId, trackInfo, operatingCarrier);
		}else{
			return AirproxyModuleUtils.getReservationBD().getSelfReprotectFlights(alertId);
		}
		
	}

	public static boolean hasActionedByIBE(String pnr) throws ModuleException{
		
		return AirproxyModuleUtils.getReservationBD().hasActionedByIBE(pnr);
		
	}

	// TO GET THE ALERT ACTION DESCRIPTION
	private static String getAlertActionCode(String actionTaken) {
		if (actionTaken.equals("1")) {
			return ReservationInternalConstants.AlertActionType.OTHER;
		} else if (actionTaken.equals("2")) {
			return ReservationInternalConstants.AlertActionType.EMAIL;
		} else if (actionTaken.equals("3")) {
			return ReservationInternalConstants.AlertActionType.FAX;
		} else if (actionTaken.equals("4")) {
			return ReservationInternalConstants.AlertActionType.TELEPHONE;
		}
		return null;
	}
}
