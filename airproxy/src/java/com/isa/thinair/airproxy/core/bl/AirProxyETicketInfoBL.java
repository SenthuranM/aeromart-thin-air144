package com.isa.thinair.airproxy.core.bl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.Pair;

public class AirProxyETicketInfoBL {
	public static Map<Pair<String, Integer>, Set<ETicketInfoTO>> loadETicketInfo(List<String> lccUniqueIDs, List<Integer> txnIDs,
			BasicTrackInfo trackInfo) throws ModuleException {
		Map<Pair<String, Integer>, Set<ETicketInfoTO>> results = new HashMap<Pair<String, Integer>, Set<ETicketInfoTO>>();
		if (lccUniqueIDs != null && lccUniqueIDs.size() > 0 && AppSysParamsUtil.isLCCConnectivityEnabled()) {

			Map<Pair<String, Integer>, Set<ETicketInfoTO>> dryInfo = AirproxyModuleUtils.getLCCReservationBD()
					.getETicketInformation(lccUniqueIDs, trackInfo);
			results.putAll(dryInfo);

		}
		if (txnIDs != null) {
			List<ResSegmentEticketInfoDTO> ownETicektRawInfo = AirproxyModuleUtils.getResSegmentBD().getEticketInformation(null,
					txnIDs);
			Map<Pair<String, Integer>, Set<ETicketInfoTO>> ownInfo = composeETicketTnxWiseMap(ownETicektRawInfo);
			results.putAll(ownInfo);
		}
		return results;
	}

	private static Map<Pair<String, Integer>, Set<ETicketInfoTO>> composeETicketTnxWiseMap(
			List<ResSegmentEticketInfoDTO> eticketInfoDTOs) {

		Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoMap = new HashMap<Pair<String, Integer>, Set<ETicketInfoTO>>();

		for (ResSegmentEticketInfoDTO info : eticketInfoDTOs) {
			String txnID = info.getTransactionID() + "";
			int paxSequence = info.getPaxSequence();

			Pair<String, Integer> pair = Pair.of(txnID, paxSequence);

			ETicketInfoTO eTicketInfoTO = new ETicketInfoTO();

			eTicketInfoTO.setDepDateZulu(info.getDepartureDateZulu());
			eTicketInfoTO.seteTicketNo(info.geteTicketNumber());
			eTicketInfoTO.setFlightNumber(info.getFlightNumber());
			eTicketInfoTO.setSegmentCode(info.getSegmentCode());

			if (eTicketInfoMap.get(pair) == null) {
				eTicketInfoMap.put(pair, new HashSet<ETicketInfoTO>());
			}
			eTicketInfoMap.get(pair).add(eTicketInfoTO);
		}
		return eTicketInfoMap;
	}

	/**
	 * @param passengerCoupon
	 */
	public static void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions) throws ModuleException {

		if (CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes().contains(paxCouponUpdateRQ.getCarrierCode())) {
			AirproxyModuleUtils.getPassengerBD().updatePassengerCoupon(paxCouponUpdateRQ, trackInfo, allowExceptions);
		} else {
			AirproxyModuleUtils.getLCCReservationBD().updatePassengerCoupon(paxCouponUpdateRQ, trackInfo);
		}
	}
	
	public static void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions, boolean isGroupPnr) throws ModuleException {

		if(isGroupPnr){
			AirproxyModuleUtils.getLCCReservationBD().updateGroupPassengerCoupon(groupPaxCouponUpdateRQ, trackInfo);
		}else{
			AirproxyModuleUtils.getPassengerBD().updateGroupPassengerCoupon(groupPaxCouponUpdateRQ, trackInfo, allowExceptions);
			
		}
	}
}
