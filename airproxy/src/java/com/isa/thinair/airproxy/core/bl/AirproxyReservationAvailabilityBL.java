package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AuxillaryHelper;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Proxy BL to mediate calls for own airline and interline for availability searches
 * 
 * @author Dilan Anuruddha
 */
public class AirproxyReservationAvailabilityBL {

	private static Log log = LogFactory.getLog(AirproxyReservationAvailabilityBL.class);

	/**
	 * Underlying method for searchAvailableFlights and searchAvailableFlightsWithAllFares
	 * 
	 * @param flightAvailRQ
	 * @param userPrincipal
	 * @param withAllFares
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public static FlightAvailRS searchFlights(FlightAvailRQ flightAvailRQ, UserPrincipal userPrincipal, boolean withAllFares,
			TrackInfoDTO trackInfo) throws ModuleException {

		SYSTEM prefSystem = flightAvailRQ.getAvailPreferences().getSearchSystem();
		if (prefSystem == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCSearchAndQuoteBD().searchAvailableFlights(flightAvailRQ, trackInfo);
		} else if (prefSystem == SYSTEM.AA) {
			return searchOwnAirlineFlights(flightAvailRQ, userPrincipal, trackInfo, withAllFares);
		} else if (prefSystem == SYSTEM.ALL || prefSystem == SYSTEM.COND) {
			ModuleException ownE = null;
			FlightAvailRS ownAirlineResults = null;
			try {
				ownAirlineResults = searchOwnAirlineFlights(flightAvailRQ, userPrincipal, trackInfo, withAllFares);
			} catch (ModuleException me) {
				log.error("OWN AIRLINE AVAIL SEARCH FAILED!", me);
				ownE = me;
			}
			if (prefSystem == SYSTEM.COND && ownAirlineResults.getSelectedPriceFlightInfo() != null) {
				return ownAirlineResults;
			} else {
				FlightAvailRS interlineResults = null;
				try {
					interlineResults = AirproxyModuleUtils.getLCCSearchAndQuoteBD().searchAvailableFlights(flightAvailRQ,
							trackInfo);
				} catch (ModuleException intE) {
					log.error("INTERLIN AVAIL SEARCH FAILED!", intE);
					if (ownE != null) {
						throw ownE;
					}
				}
				FlightAvailRS combinedResults = combineResults(ownAirlineResults, interlineResults);
				return combinedResults;
			}

		} else {
			return new FlightAvailRS();
		}
	}

	/**
	 * Give the FlihgtAvailRQ and UserPrinciple, Search the Own airline and returns results in FlightAvailRS
	 * 
	 * @param flightAvailRQ
	 * @param userPrincipal
	 * @param trackInfo
	 * @return FlightAvailRS
	 * @throws ModuleException
	 */
	private static FlightAvailRS searchOwnAirlineFlights(FlightAvailRQ flightAvailRQ, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo, boolean withAllFares) throws ModuleException {
		AuxillaryHelper auxHelper = new AuxillaryHelper(AirproxyModuleUtils.getReservationAuxilliaryBD());
		AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(flightAvailRQ,
				userPrincipal, withAllFares, auxHelper);
		AvailableFlightDTO availableFlightDTO = AirproxyModuleUtils.getAirReservationQueryBD()
				.getAvailableFlightsWithAllFares(availableFlightSearchDTO, trackInfo);

		if (availableFlightSearchDTO.isFQOnLastFQDate()) {
			// If based on the last fare quoted date fares could not be found, look for current validity period fares
			if ((availableFlightDTO.getSelectedFlight() == null
					|| (!availableFlightDTO.getSelectedFlight().isSeatsAvailable()))) {
				availableFlightSearchDTO.setLastFareQuotedDate(null);
				availableFlightSearchDTO.setFQOnLastFQDate(false);
				availableFlightDTO = AirproxyModuleUtils.getAirReservationQueryBD()
						.getAvailableFlightsWithAllFares(availableFlightSearchDTO, trackInfo);
			}
		}

		FlightAvailRS flightAvailRS = null;

		if (flightAvailRQ.getAvailPreferences().isMultiCitySearch()) {
			flightAvailRS = AvailabilityConvertUtil.getMultiCityFlightRS(availableFlightDTO, flightAvailRQ,
					userPrincipal.getAgentCode());
		} else {
			flightAvailRS = AvailabilityConvertUtil.getFlightRS(availableFlightDTO, flightAvailRQ, userPrincipal.getAgentCode());
		}

		if (availableFlightDTO.getSelectedFlight() != null) {
			ServiceTaxContainer applicableServiceTax = AirproxyModuleUtils.getReservationBD().getApplicableServiceTax(
					availableFlightDTO.getSelectedFlight().getSelectedOndFlight(OndSequence.OUT_BOUND),
					EXTERNAL_CHARGES.JN_OTHER);
			flightAvailRS.getApplicableServiceTaxes().add(applicableServiceTax);
		}

		return flightAvailRS;
	}

	/**
	 * Combine and compose a unified FlightAvailRS based on the Interline and Ownairline FlightAvailRS results Selected
	 * flight is decided based on the minimum total price
	 * 
	 * @param responseOwn
	 * @param responseInterline
	 * @return FlightAvailRS
	 */
	private static FlightAvailRS combineResults(FlightAvailRS responseOwn, FlightAvailRS responseInterline) {
		if (responseOwn == null && responseInterline != null) {
			return responseInterline;
		}

		if (responseOwn != null && responseInterline == null) {
			return responseOwn;
		}

		responseOwn.setTransactionIdentifier(responseInterline.getTransactionIdentifier());
		if (responseOwn.getSelectedPriceFlightInfo() != null && responseInterline.getSelectedPriceFlightInfo() != null) {
			if (!isOwnAirlineFareIsLow(responseOwn.getSelectedPriceFlightInfo(),
					responseInterline.getSelectedPriceFlightInfo())) {
				responseOwn.setSelectedPriceFlightInfo(responseInterline.getSelectedPriceFlightInfo());
				responseOwn.setOpenReturnOptionsTO(responseInterline.getOpenReturnOptionsTO());
				resetSelectedOnds(responseOwn.getOriginDestinationInformationList());
			} else {
				resetSelectedOnds(responseInterline.getOriginDestinationInformationList());
			}
		} else if (responseOwn.getSelectedPriceFlightInfo() == null && responseInterline.getSelectedPriceFlightInfo() != null) {
			responseOwn.setSelectedPriceFlightInfo(responseInterline.getSelectedPriceFlightInfo());
			responseOwn.setOpenReturnOptionsTO(responseInterline.getOpenReturnOptionsTO());
		}

		if (responseOwn.getOpenReturnOptionsTO() == null && responseInterline.getOpenReturnOptionsTO() != null) {
			responseOwn.setOpenReturnOptionsTO(responseInterline.getOpenReturnOptionsTO());
		}

		Map<String, OriginDestinationInformationTO> ondMap = new HashMap<String, OriginDestinationInformationTO>();
		for (OriginDestinationInformationTO own : responseOwn.getOriginDestinationInformationList()) {
			String ondCode = own.getOrigin() + own.getDestination();
			if (ondMap.containsKey(ondCode)) {
				ondMap.get(ondCode).getOrignDestinationOptions().addAll(own.getOrignDestinationOptions());
			} else {
				ondMap.put(ondCode, own);
			}
		}
		for (OriginDestinationInformationTO intl : responseInterline.getOriginDestinationInformationList()) {
			String ondCode = intl.getOrigin() + intl.getDestination();
			if (ondMap.containsKey(ondCode)) {
				ondMap.get(ondCode).getOrignDestinationOptions().addAll(intl.getOrignDestinationOptions());
			} else {
				ondMap.put(ondCode, intl);
			}
		}
		responseOwn.setOriginDestinationInformationList(new ArrayList<OriginDestinationInformationTO>(ondMap.values()));

		return responseOwn;
	}

	private static boolean isOwnAirlineFareIsLow(PriceInfoTO ownPriceInfo, PriceInfoTO intlPriceInfo) {
		BigDecimal ownLowest = ownPriceInfo.getFareTypeTO().getTotalPrice();
		BigDecimal intlLower = intlPriceInfo.getFareTypeTO().getTotalPrice();

		if (ownLowest.compareTo(intlLower) <= 0) {
			return true;
		}

		return false;
	}

	/**
	 * Reset the already selected OND segments.. will be used when both interline and airline has selected segments and
	 * depending on the selection logic one set of ond should be un selected
	 * 
	 * @param ondList
	 */
	private static void resetSelectedOnds(Collection<OriginDestinationInformationTO> ondList) {
		for (OriginDestinationInformationTO ondInfoTO : ondList) {
			for (OriginDestinationOptionTO ondOpt : ondInfoTO.getOrignDestinationOptions()) {
				ondOpt.setSelected(false);
			}
		}
	}

	/**
	 * Given the FlightPriceRQ and UserPrincipal return the price Quote in FlightPriceRS for the selected airline
	 * Currently support only single system(either INT or AA)
	 * 
	 * @param priceQuoteRQ
	 * @param userPrincipal
	 * @param trackInfo
	 *            TODO
	 * @return FlightPriceRS
	 * @throws ModuleException
	 */
	public static FlightPriceRS quoteFlightPrice(FlightPriceRQ priceQuoteRQ, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {
		FlightPriceRS flightPriceRS = new FlightPriceRS();
		SYSTEM selectedFlightSystem = priceQuoteRQ.getAvailPreferences().getSearchSystem();
		// Selected system can only be either AA or INTERLINE for the moment
		if (selectedFlightSystem == SYSTEM.AA) {
			AuxillaryHelper auxHelper = new AuxillaryHelper(AirproxyModuleUtils.getReservationAuxilliaryBD());
			AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(priceQuoteRQ,
					userPrincipal, true, auxHelper);
			// logging for fare quote fixed class problem
			if (availableFlightSearchDTO != null && availableFlightSearchDTO.getSummary() != null) {
				if (log.isDebugEnabled()) {
					log.debug("AVIL FLT : " + availableFlightSearchDTO.getSummary().toString());
				}
			}

			if ((availableFlightSearchDTO.isReturnFlag() && !availableFlightSearchDTO.isOpenReturnSearch())
					|| availableFlightSearchDTO.getJourneyType() == JourneyType.OPEN_JAW
					|| (availableFlightSearchDTO.getJourneyType() == JourneyType.ROUNDTRIP
							&& !availableFlightSearchDTO.isOpenReturnSearch())) {
				if (!priceQuoteRQ.getAvailPreferences().isModifyBooking()) {
					availableFlightSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
				} else {
					availableFlightSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForModification());
				}

				if (JourneyType.OPEN_JAW == availableFlightSearchDTO.getJourneyType()) {
					availableFlightSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney());
				}
			}
			SelectedFlightDTO selectedFlightDTO = AirproxyModuleUtils.getAirReservationQueryBD()
					.getFareQuote(availableFlightSearchDTO, trackInfo);

			if (availableFlightSearchDTO.isFQOnLastFQDate()) {
				// If based on the last fare quoted date fares could not be found, look for current validity period
				// fares
				if ((selectedFlightDTO == null || (!selectedFlightDTO.isSeatsAvailable()))) {
					availableFlightSearchDTO.setLastFareQuotedDate(null);
					availableFlightSearchDTO.setFltSegWiseLastFQDates(null);
					availableFlightSearchDTO.setFQOnLastFQDate(false);
					selectedFlightDTO = AirproxyModuleUtils.getAirReservationQueryBD().getFareQuote(availableFlightSearchDTO,
							trackInfo);
				} else {
					selectedFlightDTO.setLastFQDate(availableFlightSearchDTO.getLastFareQuotedDate());
					selectedFlightDTO.setFQWithinValidity(availableFlightSearchDTO.isFQWithinValidity());
				}
			}

			// logging for fare quote fixed class problem
			if (selectedFlightDTO != null && selectedFlightDTO.getSummary() != null) {
				if (log.isDebugEnabled()) {
					log.debug("SelectedFlightDTO : " + selectedFlightDTO.getSummary().toString());
				}
			} else {
				if (log.isDebugEnabled()) {
					log.debug("SelectedFlightDTO is null ");
				}
			}

			flightPriceRS = AvailabilityConvertUtil.getFlightRS(selectedFlightDTO, priceQuoteRQ, userPrincipal.getAgentCode());

			if (selectedFlightDTO != null) {
				ServiceTaxContainer applicableServiceTax = AirproxyModuleUtils.getReservationBD().getApplicableServiceTax(
						selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND), EXTERNAL_CHARGES.JN_OTHER);
				if (applicableServiceTax != null) {
					flightPriceRS.getApplicableServiceTaxes().add(applicableServiceTax);
				}
			}

		} else if (selectedFlightSystem == SYSTEM.INT) {
			flightPriceRS = AirproxyModuleUtils.getLCCSearchAndQuoteBD().quoteFlightPrice(priceQuoteRQ, trackInfo);
		}

		PriceInfoTO selectedPriceInfo = flightPriceRS.getSelectedPriceFlightInfo();
		if (selectedPriceInfo != null && selectedPriceInfo.getFareTypeTO() != null) {

			flightPriceRS.getSelectedPriceFlightInfo().setAllRequestedOndsHasFares(
					flightPriceRS.getOriginDestinationInformationList().size() > 0 && selectedPriceInfo.getFareTypeTO()
							.getOndWiseFareType().size() == flightPriceRS.getOriginDestinationInformationList().size());

		}

		return flightPriceRS;
	}

	/**
	 * Early Block seat for privilege users . Currently block seat is only available for AA.
	 * 
	 * @param priceQuoteRQ
	 * @param fareSegChargeTO
	 * @return
	 * @throws ModuleException
	 */
	public static Collection priviledgeUserBlockSeat(FlightPriceRQ priceQuoteRQ, FareSegChargeTO fareSegChargeTO)
			throws ModuleException {

		Collection blockSeatIds = new ArrayList();
		SYSTEM selectedFlightSystem = priceQuoteRQ.getAvailPreferences().getSearchSystem();
		if (selectedFlightSystem == SYSTEM.AA) {

			OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ, fareSegChargeTO);
			Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD()
					.recreateFareSegCharges(ondRebuildCriteria);
			ReservationBD reservationBD = AirproxyModuleUtils.getReservationBD();
			Collection blockSeats = reservationBD.blockSeats(collFares, null);
			blockSeatIds.addAll(blockSeats);

		} else {
			// Do nothing since we are not blocking in interline flow for current implementation.
		}

		return blockSeatIds;
	}

	public static FareAvailRS searchFares(FareAvailRQ fareAvailRQ, UserPrincipal userPrinciple, BasicTrackInfo trackInfo)
			throws ModuleException {
		SYSTEM prefSystem = fareAvailRQ.getAvailPreferences().getSearchSystem();
		if (prefSystem == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCSearchAndQuoteBD().searchFares(fareAvailRQ, userPrinciple, trackInfo);
		} else if (prefSystem == SYSTEM.AA) {
			return searchOwnAirlineForFares(fareAvailRQ, userPrinciple);
		}
		return null;
	}

	/**
	 * Give the FlihgtAvailRQ and UserPrinciple, Search the Own airline and returns results in FlightAvailRS
	 * 
	 * @param flightAvailRQ
	 * @param userPrincipal
	 * @return FlightAvailRS
	 * @throws ModuleException
	 */
	private static FareAvailRS searchOwnAirlineForFares(FareAvailRQ fareAvailRQ, UserPrincipal userPrincipal)
			throws ModuleException {

		log.debug("Called searchOwnAirlineForFares : " + fareAvailRQ.getAvailPreferences().getFirstDepatureDate());
		AuxillaryHelper auxHelper = new AuxillaryHelper(AirproxyModuleUtils.getReservationAuxilliaryBD());
		AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(fareAvailRQ,
				userPrincipal, true, auxHelper);
		availableFlightSearchDTO.setChannelCode(fareAvailRQ.getSalesChannelCode());
		// merge possible ONDs for possible connection FareQuote
		ReservationApiUtils.splitPossibleOnDs(availableFlightSearchDTO);
		ReservationApiUtils.updateSplitONDListOnAvailRQ(availableFlightSearchDTO, fareAvailRQ);
		ReservationApiUtils.mergePossibleOnD(availableFlightSearchDTO);

		// update FareAvailRQ OND list if ONDs merged
		ReservationApiUtils.updateMergedONDListOnAvailRQ(availableFlightSearchDTO, fareAvailRQ);
		if (AppSysParamsUtil.isSubJourneyDetectionEnabled() && !availableFlightSearchDTO.isOpenReturnSearch()
				&& availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() > 1
				&& (fareAvailRQ.getSelectedIBFltRefNo() != null || fareAvailRQ.getSelectedOBFltRefNo() != null)) {

			List<AvailableFlightSearchDTO> availableFlightSearchDTOList = ReservationApiUtils
					.getAvailableSubJourneyCollection(availableFlightSearchDTO);

			if (availableFlightSearchDTOList != null && availableFlightSearchDTOList.size() > 1) {
				for (AvailableFlightSearchDTO availFlightSearchDTO : availableFlightSearchDTOList) {
					for (OriginDestinationInfoDTO ondInfoDTO : availFlightSearchDTO.getOriginDestinationInfoDTOs()) {
						if (ondInfoDTO.getFlightSegmentIds()
								.contains(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedIBFltRefNo()))
								|| ondInfoDTO.getFlightSegmentIds().contains(
										FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedOBFltRefNo()))) {
							availableFlightSearchDTO = availFlightSearchDTO;
							break;
						}
					}
				}

				// only flights associated with the selected flights journey will be considered
				if (availableFlightSearchDTO != null && availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() >= 1) {

					Iterator<OriginDestinationInformationTO> ondItr = fareAvailRQ.getOriginDestinationInformationList()
							.iterator();
					int count = 0;
					while (ondItr.hasNext()) {
						OriginDestinationInformationTO ondInfo = ondItr.next();
						boolean isSameGroup = false;
						for (OriginDestinationInfoDTO ondInfoDTO : availableFlightSearchDTO.getOriginDestinationInfoDTOs()) {
							if (ondInfo.getOrignDestinationOptions() != null) {
								for (OriginDestinationOptionTO ondOptTO : ondInfo.getOrignDestinationOptions()) {
									if (ondOptTO.getFlightSegmentList() != null) {
										for (FlightSegmentTO fltSegTO : ondOptTO.getFlightSegmentList()) {
											if (ondInfoDTO.getFlightSegmentIds().contains(FlightRefNumberUtil
													.getSegmentIdFromFlightRPH(fltSegTO.getFlightRefNumber()))) {
												isSameGroup = true;
												break;
											}
										}
									}
								}
							}
						}

						if (!isSameGroup) {
							ondItr.remove();
							ReservationApiUtils.updateQuoteOndFlexi(fareAvailRQ, count);
						}
						count++;
					}
				}
			}

		}

		availableFlightSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());
		FareQuoteUtil.setHalfReturnFareQuoteOption(availableFlightSearchDTO, (fareAvailRQ.getAvailPreferences().isModifyBooking()
				|| fareAvailRQ.getAvailPreferences().isRequoteFlightSearch()));
		AllFaresDTO allFaresDTO = AvailabilityConvertUtil.getAllFaresDTO(fareAvailRQ, AirproxyModuleUtils.getFlightBD());
		FareFilteringCriteria filteringCriteria = AvailabilityConvertUtil.getFareFilteringCriteria(fareAvailRQ, userPrincipal);
		allFaresDTO = AirproxyModuleUtils.getFlightInventoryResBD().getAllBucketFareCombinationsForSegment(allFaresDTO,
				filteringCriteria, availableFlightSearchDTO);

		return AvailabilityConvertUtil.getFareAvailRS(allFaresDTO, availableFlightSearchDTO);
	}

	public static FlightPriceRS changeFlightPrice(ChangeFareRQ changeFareRQ, UserPrincipal userPrincipal,
			BasicTrackInfo trackInfo) throws ModuleException {
		SYSTEM prefSystem = changeFareRQ.getAvailPreferences().getSearchSystem();
		if (prefSystem == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCSearchAndQuoteBD().changeFlightPrice(changeFareRQ, trackInfo);
		}
		return searchOwnAirlineForChangedFlightPrice(changeFareRQ, userPrincipal);
	}

	public static List<RollForwardFlightAvailRS> searchAvailableFlightForRollForward(
			Collection<LCCClientReservationSegment> colsegs, Date startDate, Date endDate, Integer adultCount, Integer childCount,
			Integer infantCount, SYSTEM searchSystem, boolean overBook, String agentCode) throws ModuleException {
		List<RollForwardFlightAvailRS> availFlightList = null;
		if (searchSystem == SYSTEM.INT) {
			// TODO Handle interline/dry scenarios
		} else if (searchSystem == SYSTEM.AA) {
			RollForwardFlightSearchDTO rollForwardFlightSearchDTO = AvailabilityConvertUtil.getRollForwardFlightSearch(colsegs,
					startDate, endDate, adultCount, childCount, infantCount, overBook, agentCode);
			availFlightList = AirproxyModuleUtils.getAirReservationQueryBD()
					.getAvaialableFlightForRollForward(rollForwardFlightSearchDTO);
		}
		SortUtil.sortByDepartureDate(availFlightList);
		return availFlightList;
	}

	public static boolean isDuplicateNameExist(Collection<Integer> fltSegmentIds, String pnr) {
		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			Set<ReservationPax> resPaxs = new HashSet<ReservationPax>();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				resPaxs.add(reservationPax);
			}
			ReservationApiUtils.checkEndValidations(fltSegmentIds, resPaxs, pnr);
			return false;
		} catch (ModuleException e) {
			return true;
		}
	}

	private static FlightPriceRS searchOwnAirlineForChangedFlightPrice(ChangeFareRQ changeFareRQ, UserPrincipal userPrincipal)
			throws ModuleException {
		AuxillaryHelper auxHelper = new AuxillaryHelper(AirproxyModuleUtils.getReservationAuxilliaryBD());
		AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(changeFareRQ,
				userPrincipal, true, auxHelper);
		availableFlightSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());
		FareQuoteUtil.setHalfReturnFareQuoteOption(availableFlightSearchDTO,
				changeFareRQ.getAvailPreferences().isModifyBooking());
		ChangeFaresDTO changeFaresDTO = new ChangeFaresDTO();
		changeFaresDTO.setPosAirport(changeFareRQ.getPosAirport());
		IPaxCountAssembler paxAssm = new PaxCountAssembler(changeFareRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		changeFaresDTO.setInfantSeats(paxAssm.getInfantCount());

		Collection<Integer> flightSegmentIds = null;

		if (changeFareRQ.getPnr() != null) {
			flightSegmentIds = AirproxyModuleUtils.getResSegmentBD().getFlightSegmentIds(changeFareRQ.getPnr(), true);
			if (flightSegmentIds != null && flightSegmentIds.size() > 0) {
				changeFaresDTO.getOldFltSegIdList().addAll(flightSegmentIds);
			}
		}

		Collection<OndFareDTO> collFares = null;
		if (changeFareRQ.getOndRebuildCriteria() != null) {
			collFares = AirproxyModuleUtils.getAirReservationQueryBD()
					.recreateFareSegCharges(changeFareRQ.getOndRebuildCriteria());
		}
		if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
			Map<Integer, FlightSegmentDTO> segmentMapping = new HashMap<Integer, FlightSegmentDTO>();
			List<FlightSegmentDTO> flightSegs = new ArrayList<FlightSegmentDTO>();
			for (OndFareDTO ondFare : collFares) {
				for (Entry<Integer, FlightSegmentDTO> entry : ondFare.getSegmentsMap().entrySet()) {
					flightSegs.add(entry.getValue());
				}
			}
			Collections.sort(flightSegs);
			int seq = 0;
			for (FlightSegmentDTO seg : flightSegs) {
				seg.setSegSequence(seq);
				segmentMapping.put(seg.getSegmentId(), seg);
				seq++;
			}
			for (OndFareDTO ondFare : collFares) {
				for (Entry<Integer, FlightSegmentDTO> entry : ondFare.getSegmentsMap().entrySet()) {
					int segSeq = segmentMapping.get(entry.getKey()).getSegSequence();
					entry.getValue().setSegSequence(segSeq);
				}
			}
		}
		
		changeFaresDTO.setExistingFareChargesSeatsSegmentsDTOs(collFares);
		for (SegmentsFareDTO chgFareSeg : changeFareRQ.getChangedFareSegments()) {
			changeFaresDTO.addChangedFaresSeatsSegmentsDTO(chgFareSeg);
		}

		changeFaresDTO = AirproxyModuleUtils.getFlightInventoryResBD().recalculateChargesForFareChange(changeFaresDTO,
				availableFlightSearchDTO);

		return AvailabilityConvertUtil.getFlightRS(changeFaresDTO, changeFareRQ, userPrincipal.getAgentCode());
	}
}