package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class AirproxyInFlightServicesBL {

	private SYSTEM system;
	private List<FlightSegmentTO> flightSegmentTOs;
	private String transactionIdentifier;
	private UserPrincipal userPrincipal;
	private String selectedLanguage;
	private BasicTrackInfo trackInfo;
	private Map<String, Set<String>> fltRefWiseSelectedSSR;
	private boolean modifyAnci;
	private boolean gdsPnr = false;
	private boolean hasPrivModifySSRWithinBuffer = false;
	private boolean isUserDefinedSsrOnly;

	public List<LCCFlightSegmentSSRDTO> execute() throws ModuleException {

		if (system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().getSpecialServiceRequests(flightSegmentTOs, transactionIdentifier,
					selectedLanguage, trackInfo, fltRefWiseSelectedSSR, modifyAnci);
		} else {
			return getOwnAirlineSpecialServiceRequests();
		}
	}

	private List<LCCFlightSegmentSSRDTO> getOwnAirlineSpecialServiceRequests() throws ModuleException {
		String salesChannelCode = getUserPrinciple().getSalesChannel() + "";
		if (gdsPnr) {
			salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_GDS + "";
		}

		List<LCCFlightSegmentSSRDTO> ssrDTOList = new ArrayList<LCCFlightSegmentSSRDTO>();

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = SegmentUtil.getFlightSegmentIdWiseClassOfServices(flightSegmentTOs);

		List<SpecialServiceRequestDTO> tempList = AirproxyModuleUtils.getSsrServiceBD().getActiveSSRsByFltSegIds(
				flightSegIdWiseCos, SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannelCode,
				AppSysParamsUtil.isInventoryCheckForSSREnabled(), selectedLanguage, modifyAnci, modifyAnci, hasPrivModifySSRWithinBuffer, isUserDefinedSsrOnly);

		for (Entry<Integer, ClassOfServiceDTO> cosEntry : flightSegIdWiseCos.entrySet()) {
			if (tempList != null && tempList.size() > 0) {
				Map<Integer, Set<String>> fltSegIdWiseSSRs = AncillaryCommonUtil
						.getFlightSegIdWiseSelectedAnci(fltRefWiseSelectedSSR);

				for (FlightSegmentTO flightSegment : flightSegmentTOs) {

					if (FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()).intValue() != cosEntry
							.getKey().intValue()) {
						continue;
					}

					LCCFlightSegmentSSRDTO flightSegSSRDTO = new LCCFlightSegmentSSRDTO();
					Set<String> segmentsList = new HashSet<String>();
					flightSegSSRDTO.setFlightSegmentTO(flightSegment);
					String arr[] = flightSegment.getSegmentCode().split("/");

					if (arr.length == 2) {
						segmentsList.add(arr[0] + "/" + arr[1]);
					} else {
						for (int i = 0; i < arr.length - 2; i++) {
							String ori = arr[i + 0];
							String con = arr[i + 1];
							String des = arr[i + 2];

							segmentsList.add(ori + "/" + con);
							segmentsList.add(con + "/" + des);

						}
					}

					for (String segment : segmentsList) {
						String segArr[] = segment.split("/");
						String fltSegOrigin = segArr[0];
						String fltSegDestination = segArr[1];

						for (SpecialServiceRequestDTO specialServiceRequestDTO : tempList) {

							// If the SSRs with 0 available quantity or Inactive status isn't added to reservation skip
							// those SSRs.
							if (!isUserDefinedSsrOnly && (specialServiceRequestDTO.getAvailableQty().intValue() == 0 || "N"
									.equals(specialServiceRequestDTO.getStatus()))
									&& !isSSRAlreadyAdded(fltSegIdWiseSSRs,
											FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()),
											specialServiceRequestDTO.getSsrCode())) {
								continue;
							}

							String ssArr[] = specialServiceRequestDTO.getOndCode().split("/");
							String ssrOrigin = ssArr[0];
							String ssrDestination = ssArr[1];

							if ((fltSegOrigin.equals(ssrOrigin) && fltSegDestination.equals(ssrDestination))) {

								LCCSpecialServiceRequestDTO specialServiceRequest = new LCCSpecialServiceRequestDTO();
								specialServiceRequest.setStatus(specialServiceRequestDTO.getStatus());
								specialServiceRequest.setSsrCode(specialServiceRequestDTO.getSsrCode());
								specialServiceRequest.setSsrName(specialServiceRequestDTO.getSsrName());

								BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
								if (specialServiceRequestDTO.getChargeAmount() != null
										&& !specialServiceRequestDTO.getChargeAmount().equals("")) {
									serviceCharge = new BigDecimal(specialServiceRequestDTO.getChargeAmount());
								}

								specialServiceRequest.setCharge(serviceCharge);
								specialServiceRequest.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								specialServiceRequest.setServiceQuantity("");
								specialServiceRequest.setAvailableQty(specialServiceRequestDTO.getAvailableQty());
								specialServiceRequest.setUserDefinedCharge(isUserDefinedSsrOnly);
								
								if (specialServiceRequestDTO.getDecriptionSelectedLanguage() == null
										|| StringUtil.getUnicode(specialServiceRequestDTO.getDecriptionSelectedLanguage())
												.equalsIgnoreCase("null")) {
									specialServiceRequest.setDescription(specialServiceRequestDTO.getDescription());
								} else {
									specialServiceRequest.setDescription(StringUtil.getUnicode(
											specialServiceRequestDTO.getDecriptionSelectedLanguage()).replace('^', ','));
								}
								if (!flightSegSSRDTO.getSpecialServiceRequest().contains(specialServiceRequest)) {
									flightSegSSRDTO.getSpecialServiceRequest().add(specialServiceRequest);
								}

							}
						}
					}

					ssrDTOList.add(flightSegSSRDTO);
				}
			}
		}
		Collections.sort(ssrDTOList);
		return ssrDTOList;
	}

	private boolean isSSRAlreadyAdded(Map<Integer, Set<String>> fltSegIdWiseSSRs, Integer flightSegId, String ssrCode) {

		if (fltSegIdWiseSSRs != null) {
			for (Entry<Integer, Set<String>> entry : fltSegIdWiseSSRs.entrySet()) {
				if (flightSegId != null && (flightSegId.intValue() == entry.getKey().intValue()) && entry.getValue() != null
						&& entry.getValue().contains(ssrCode)) {
					return true;
				}
			}
		}

		return false;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public void setUserPrinciple(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public UserPrincipal getUserPrinciple() {
		return userPrincipal;
	}

	/**
	 * @return the gdsPnr
	 */
	public boolean isGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr
	 *            the gdsPnr to set
	 */
	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public BasicTrackInfo getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(BasicTrackInfo trackInfo) {
		this.trackInfo = trackInfo;
	}

	public Map<String, Set<String>> getFltRefWiseSelectedSSR() {
		return fltRefWiseSelectedSSR;
	}

	public void setFltRefWiseSelectedSSR(Map<String, Set<String>> fltRefWiseSelectedSSR) {
		this.fltRefWiseSelectedSSR = fltRefWiseSelectedSSR;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}

	public boolean isHasPrivModifySSRWithinBuffer() {
		return hasPrivModifySSRWithinBuffer;
	}

	public void setHasPrivModifySSRWithinBuffer(boolean hasPrivModifySSRWithinBuffer) {
		this.hasPrivModifySSRWithinBuffer = hasPrivModifySSRWithinBuffer;
	}

	public boolean isUserDefinedSsrOnly() {
		return isUserDefinedSsrOnly;
	}

	public void setUserDefinedSsrOnly(boolean isUserDefinedSsrOnly) {
		this.isUserDefinedSsrOnly = isUserDefinedSsrOnly;
	} 
}
