package com.isa.thinair.airproxy.core.bl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Proxy the ancillary requests back and forth based on the requesting system
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxyAncillaryAvailabilityBL {

	private static Log log = LogFactory.getLog(AirproxyAncillaryAvailabilityBL.class);

	private LCCAncillaryAvailabilityInDTO ancillaryAvailabilityInDTO;
	private UserPrincipal userPrinciple;
	private BasicTrackInfo trackInfo;

	public AirproxyAncillaryAvailabilityBL(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityInDTO, UserPrincipal userPrinciple,
			BasicTrackInfo trackInfo) {
		super();
		this.ancillaryAvailabilityInDTO = ancillaryAvailabilityInDTO;
		this.userPrinciple = userPrinciple;
		this.trackInfo = trackInfo;
	}

	/**
	 * Return list of LCCAncillaryAvailabilityOutDTO which contain, ancillary availability data based on the input
	 * parameters set on this object
	 * 
	 * @return List<LCCAncillaryAvailabilityOutDTO>
	 * @throws ModuleException
	 */
	public AnciAvailabilityRS execute() throws ModuleException {
		if (this.ancillaryAvailabilityInDTO == null) {
			throw new ModuleException("Ancillary Availability DTO is null");
		}
		if (ancillaryAvailabilityInDTO.getQueryingSystem() == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().getAncillaryAvailability(ancillaryAvailabilityInDTO, trackInfo);
		} else if (ancillaryAvailabilityInDTO.getQueryingSystem() == SYSTEM.AA) {
			return getOwnAirlineAncillaryAvailability(this.ancillaryAvailabilityInDTO);
		}
		return null;
	}

	public AnciAvailabilityRS execute(String carrierCode, String segmentCode, EXTERNAL_CHARGES extChg) throws ModuleException {
		boolean isOwn = AppSysParamsUtil.getDefaultCarrierCode().equals(carrierCode);
		AnciAvailabilityRS anciAvailabilityRS = null;
		if (isOwn) {
			anciAvailabilityRS = new AnciAvailabilityRS();
			setAnciTaxApplicability(anciAvailabilityRS, segmentCode, extChg);
		} else {
			return AirproxyModuleUtils.getLCCAncillaryBD().getTaxApplicabilityForAncillaries(carrierCode, segmentCode, extChg,
					trackInfo);
		}
		return anciAvailabilityRS;
	}

	/**
	 * FIXME temporarily disable ancillary selection for own airline. FIX immediately after completing the reservation
	 * flow
	 * 
	 * @param ancillaryAvailabilityDTO
	 * @return
	 * @throws ModuleException
	 */
	private AnciAvailabilityRS getOwnAirlineAncillaryAvailability(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO)
			throws ModuleException {

		AnciAvailabilityRS anciAvailabilityRS = new AnciAvailabilityRS();
		List<LCCAncillaryAvailabilityOutDTO> segmentAnciAvailList = new ArrayList<LCCAncillaryAvailabilityOutDTO>();
		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		String departureSegCode = null;
		List<Integer> flightSegIds = new ArrayList<Integer>();

		anciAvailabilityRS.setLccAncillaryAvailabilityOutDTOs(segmentAnciAvailList);
		for (FlightSegmentTO fltSeg : ancillaryAvailabilityDTO.getFlightDetails()) {
			flightSegIds.add(fltSeg.getFlightSegId());
		}
		Map<Integer, Boolean> csOCFlightMapping = AirproxyModuleUtils.getFlightBD().isAtLeastOneSegmentHavingCSOCFlight(
				flightSegIds);
		ancillaryAvailabilityDTO.setAtLeastOneSegmentHavingCSOCFlight(csOCFlightMapping.values().contains(true));

		for (FlightSegmentTO fltSeg : ancillaryAvailabilityDTO.getFlightDetails()) {

			if (departureSegCode == null && !fltSeg.isForInsuranceAvailability()) {
				departureSegCode = fltSeg.getSegmentCode();
			}

			LCCAncillaryAvailabilityOutDTO segmentAnciAvail = new LCCAncillaryAvailabilityOutDTO();
			fltSeg.setSsrInventoryCheck(AppSysParamsUtil.isInventoryCheckForSSREnabled());

			String logicalCabinClass = fltSeg.getLogicalCabinClassCode();
			if (logicalCabinClass != null && !"".equals(logicalCabinClass)) {
				LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(logicalCabinClass);
				fltSeg.setFreeSeatEnabled(logicalCabinClassDTO.isFreeSeatEnabled());
				fltSeg.setFreeMealEnabled(logicalCabinClassDTO.isAllowSingleMealOnly());
			}

			fltSeg.setSurfaceSegment(AirproxyModuleUtils.getAirportBD().isBusSegment(fltSeg.getSegmentCode()));

			segmentAnciAvail.setFlightSegmentTO(fltSeg);
			List<LCCAncillaryStatus> ancillaryStatusList = new ArrayList<LCCAncillaryStatus>();
			for (LCCAncillaryType anciType : ancillaryAvailabilityDTO.getAncillaryTypes()) {
				if (LCCAncillaryType.AIRPORT_SERVICE.equals(anciType.getAncillaryType())
						|| LCCAncillaryType.AIRPORT_TRANSFER.equals(anciType.getAncillaryType())
						|| (AppSysParamsUtil.isONDBaggaeEnabled() && LCCAncillaryType.BAGGAGE.equals(anciType.getAncillaryType()))
						|| LCCAncillaryType.AUTOMATIC_CHECKIN.equals(anciType.getAncillaryType())) {
					continue;
				}

				LCCAncillaryStatus tempStatus = populateAncillaryAvailability(fltSeg, anciType, ancillaryAvailabilityDTO,
						csOCFlightMapping);
				if (tempStatus != null) {
					ancillaryStatusList.add(tempStatus);
				}
			}
			segmentAnciAvail.setAncillaryStatusList(ancillaryStatusList);
			segmentAnciAvailList.add(segmentAnciAvail);
		}

		if (AppSysParamsUtil.isAirportServicesEnabled()) {
			for (LCCAncillaryType anciType : ancillaryAvailabilityDTO.getAncillaryTypes()) {
				if (LCCAncillaryType.AIRPORT_SERVICE.equals(anciType.getAncillaryType())) {
					getSegmentWiseAirportLevelAvailablity(ancillaryAvailabilityDTO, segmentAnciAvailList,
							LCCAncillaryType.AIRPORT_SERVICE, SSRCategory.ID_AIRPORT_SERVICE, csOCFlightMapping);
				}
			}
		}

		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			for (LCCAncillaryType anciType : ancillaryAvailabilityDTO.getAncillaryTypes()) {
				if (LCCAncillaryType.AIRPORT_TRANSFER.equals(anciType.getAncillaryType())) {
					populateSegWiseAirportTransfers(ancillaryAvailabilityDTO, segmentAnciAvailList,
							LCCAncillaryType.AIRPORT_TRANSFER, csOCFlightMapping);
				}
			}
		}
		// checking availability in segment wise
		if (AppSysParamsUtil.isAutomaticCheckinEnabled()) {
			ancillaryAvailabilityDTO.getAncillaryTypes().forEach(
					anciType -> {
						if (LCCAncillaryType.AUTOMATIC_CHECKIN.equals(anciType.getAncillaryType())) {
							try {
								populateSegWiseAutomaticCheckins(ancillaryAvailabilityDTO, segmentAnciAvailList,
										LCCAncillaryType.AUTOMATIC_CHECKIN, csOCFlightMapping);
							} catch (Exception me) {
								log.error("Exception when populateSegWiseAutomaticCheckins " + me.toString());
							}
						}
					});
		}

		// check OND baggage
		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			for (LCCAncillaryType anciType : ancillaryAvailabilityDTO.getAncillaryTypes()) {
				if (LCCAncillaryType.BAGGAGE.equals(anciType.getAncillaryType())) {
					populateSegWiseBaggages(ancillaryAvailabilityDTO, segmentAnciAvailList, anciType, csOCFlightMapping);
				}
			}

		}

		setAnciTaxApplicability(anciAvailabilityRS, departureSegCode, EXTERNAL_CHARGES.JN_ANCI);

		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(ancillaryAvailabilityDTO.getAppIndicator())) {
			// If credit amount generated from anci modification is non-refundable, then add
			// penalty amount to wipe off that credit amount
			anciAvailabilityRS.setApplyPenaltyForModification(!AppSysParamsUtil.isRefundIBEAnciModifyCredit());
		}

		return anciAvailabilityRS;
	}

	/**
	 * Checking automatic checkin service availability for segment wise 
	 * 
	 * @param ancillaryAvailabilityDTO
	 * @param segmentAnciAvailList
	 * @param anciType
	 * @param csOCFlightMapping
	 * @throws ModuleException
	 */
	private void populateSegWiseAutomaticCheckins(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO,
			List<LCCAncillaryAvailabilityOutDTO> segmentAnciAvailList, String anciType, Map<Integer, Boolean> csOCFlightMapping)
			throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				ancillaryAvailabilityDTO.getFlightDetails(), ancillaryAvailabilityDTO.getAirportServiceCountMap(), false);
		Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> airportWiseResults = AirproxyModuleUtils.getAutomaticCheckinBD()
				.getAvailableAutomaticCheckins(airportWiseMap);

		segmentAnciAvailList.forEach(lccAnciAvailabilityOutDTO -> {
			FlightSegmentTO fltSegTO = lccAnciAvailabilityOutDTO.getFlightSegmentTO();
			LCCAncillaryStatus anciStatus = new LCCAncillaryStatus();
			anciStatus.setAncillaryType(new LCCAncillaryType(anciType));
			anciStatus.setAvailable(Boolean.FALSE);
			if (!fltSegTO.isForInsuranceAvailability()) {
				airportWiseResults.entrySet().forEach(
						apEntry -> {
							AirportServiceKeyTO keyTO = apEntry.getKey();
							FlightSegmentTO originalFltSeg = airportWiseMap.get(keyTO);
							if (fltSegTO.getSegmentCode().equals(originalFltSeg.getSegmentCode())
									&& fltSegTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {

								if (apEntry.getValue().size() > 0 && !csOCFlightMapping.get(originalFltSeg.getFlightSegId())) {
									try {
										anciStatus.setAvailable(isAutomaticCheckinAvailable(originalFltSeg));
									} catch (Exception me) {
										log.error("Exception when populateSegWiseAutomaticCheckins " + me.toString());
									}
								}
							}
						});
			}
			lccAnciAvailabilityOutDTO.getAncillaryStatusList().add(anciStatus);
		});

	}
	
	
	private static boolean isAutomaticCheckinAvailable(FlightSegmentTO flgSeg) throws ModuleException {

		Integer effectivePNLDepGapInMins;
		int autockinStopSellingMins;
		Timestamp pnlScheduledTimeStamp;

		// getting AutoCheckinStopSellingGap in mins from table
		int autoCheckinStopSellingGapMins = Integer.parseInt(AppSysParamsUtil.getAutoCheckinStopSellingGapMins());
		
		// Zulu Current Time
		Timestamp currentTime = new Timestamp((CalendarUtil.getCurrentSystemTimeInZulu()).getTime());

		// getting actual PNL sending time
		effectivePNLDepGapInMins = getActualPNLSendingDetails(flgSeg);

		if (effectivePNLDepGapInMins == 0) {
			// Assigning default PNL sending time if actual time not exist for flight
			effectivePNLDepGapInMins = Integer.parseInt(AppSysParamsUtil.getPnlDepartureGapMins());
		}

		// calculating 15 mins before pnl sending time
		autockinStopSellingMins = effectivePNLDepGapInMins.intValue() + autoCheckinStopSellingGapMins;
		pnlScheduledTimeStamp = new Timestamp(flgSeg.getDepartureDateTimeZulu().getTime() - 60 * autockinStopSellingMins * 1000);

		if (pnlScheduledTimeStamp.after(currentTime)){
			return true;
		}
			

		return false;
	}

	/**
	 * This is used to get exact PNL sending time for overriding default
	 * 
	 * @param flgSeg
	 * @return
	 */
	private static Integer getActualPNLSendingDetails(FlightSegmentTO flgSeg) {

		PNLTransMissionDetailsDTO pnlTransMissionDetailsDTO = new PNLTransMissionDetailsDTO();
		int pnlDepartureGap = 0;
		pnlTransMissionDetailsDTO.setFlightNumber(flgSeg.getFlightNumber());
		pnlTransMissionDetailsDTO.setDepartureStation(flgSeg.getSegmentCode().split("/")[0]);
		pnlTransMissionDetailsDTO.setDepartureTimeZulu(new java.sql.Timestamp(flgSeg.getDepartureDateTimeZulu().getTime()));		
		
		try {		
			
			pnlDepartureGap = AirproxyModuleUtils.getReservationAuxilliaryBD().getFlightForPnlAdlDepartureGap(pnlTransMissionDetailsDTO);
			
		} catch (Exception e) {
			log.error("NO PnlDepartureGap available for flight--" + flgSeg.getFlightNumber());
		}
		return pnlDepartureGap;

	}

	private void populateSegWiseAirportTransfers(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO,
			List<LCCAncillaryAvailabilityOutDTO> segmentAnciAvailList, String anciType, Map<Integer, Boolean> csOCFlightMapping)
			throws ModuleException {

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				ancillaryAvailabilityDTO.getFlightDetails(), ancillaryAvailabilityDTO.getAirportServiceCountMap(), true);
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseResults = AirproxyModuleUtils.getAirportTransferBD()
				.getAvailableAirportTransfers(airportWiseMap, csOCFlightMapping);

		for (LCCAncillaryAvailabilityOutDTO lccAnciAvailabilityOutDTO : segmentAnciAvailList) {
			FlightSegmentTO fltSegTO = lccAnciAvailabilityOutDTO.getFlightSegmentTO();
			LCCAncillaryStatus anciStatus = new LCCAncillaryStatus();
			anciStatus.setAncillaryType(new LCCAncillaryType(anciType));
			if (!fltSegTO.isForInsuranceAvailability()) {
				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> apEntry : airportWiseResults.entrySet()) {
					AirportServiceKeyTO keyTO = apEntry.getKey();
					FlightSegmentTO originalFltSeg = airportWiseMap.get(keyTO);

					if (fltSegTO.getSegmentCode().equals(originalFltSeg.getSegmentCode())
							&& fltSegTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {

						if (apEntry.getValue().size() > 0 && !csOCFlightMapping.get(originalFltSeg.getFlightSegId())) {
							// set airport code to support airport services
							fltSegTO.setAirportCode(keyTO.getAirport());
							anciStatus.setAvailable(true);
						}
					}

				}
			}

			lccAnciAvailabilityOutDTO.getAncillaryStatusList().add(anciStatus);
		}
	}

	private void populateSegWiseBaggages(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO,
			List<LCCAncillaryAvailabilityOutDTO> segmentAnciAvailList, LCCAncillaryType anciType,
			Map<Integer, Boolean> csOCFlightMapping) throws ModuleException {
		List<FlightSegmentTO> ownFlightSegmentTO = getOwnFlightSegmentTOs(ancillaryAvailabilityDTO.getFlightDetails());
		BaggageRq baggageRq = new BaggageRq();
		baggageRq.setFlightSegmentTOs(ownFlightSegmentTO);
		baggageRq.setCheckCutoverTime(true);
		baggageRq.setBookingClasses(ancillaryAvailabilityDTO.getBaggageSummaryTo().getBookingClasses());
		baggageRq.setClassesOfService(ancillaryAvailabilityDTO.getBaggageSummaryTo().getClassesOfService());
		baggageRq.setLogicalCC(ancillaryAvailabilityDTO.getBaggageSummaryTo().getLogicalCC());
		baggageRq.setLogicalCC(ancillaryAvailabilityDTO.getBaggageSummaryTo().getLogicalCC());
		baggageRq.setAgent(userPrinciple.getAgentCode());
		baggageRq.setSalesChannel(userPrinciple.getSalesChannel());
		baggageRq.setOwnReservation(true);
		baggageRq.setRequote(ancillaryAvailabilityDTO.isRequote());

		Map<Integer, List<FlightBaggageDTO>> baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggage(baggageRq);

		AncillaryCommonUtil.handleBufferTimes(baggageMap, ownFlightSegmentTO, ancillaryAvailabilityDTO.isModifyAncillary(),
				ancillaryAvailabilityDTO.getBaggageSummaryTo().isAllowTillFinalCutOver());

		// update segmentAnciAvailList
		for (LCCAncillaryAvailabilityOutDTO lccAnciAvailabilityOutDTO : segmentAnciAvailList) {
			FlightSegmentTO fltSegTO = lccAnciAvailabilityOutDTO.getFlightSegmentTO();
			List<FlightBaggageDTO> baggageDTOs = baggageMap.get(fltSegTO.getFlightSegId());
			if (baggageDTOs != null && baggageDTOs.size() > 0) {

				LCCAncillaryStatus anciStatus = new LCCAncillaryStatus();
				anciStatus.setAncillaryType(anciType);
				if (!csOCFlightMapping.get(fltSegTO.getFlightSegId())) {
					anciStatus.setAvailable(AncillaryCommonUtil.isBaggageAvailable(baggageMap));
				}
				lccAnciAvailabilityOutDTO.getAncillaryStatusList().add(anciStatus);
			}
		}
	}

	/*
	 * This is because flight segment tos contains other carriers as well. This is because for insurance we send other
	 * carrier information Since this is for baggages we should only have own carrier flight segments only. For more
	 * information plz visit AARESAA-10131 Nili August 14, 2012 6:32PM
	 */
	private List<FlightSegmentTO> getOwnFlightSegmentTOs(List<FlightSegmentTO> flightDetails) throws ModuleException {
		List<FlightSegmentTO> filteredFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (FlightSegmentTO flightSegmentTO : flightDetails) {
			if (CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodesExcludingBus()
					.contains(flightSegmentTO.getOperatingAirline())) {
				filteredFlightSegmentTOs.add(flightSegmentTO);
			}
		}
		return filteredFlightSegmentTOs;
	}

	private void getSegmentWiseAirportLevelAvailablity(LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO,
			List<LCCAncillaryAvailabilityOutDTO> segmentAnciAvailList, String anciType, int ssrCatId,
			Map<Integer, Boolean> csOCFlightMapping) throws ModuleException {

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				ancillaryAvailabilityDTO.getFlightDetails(), ancillaryAvailabilityDTO.getAirportServiceCountMap(), false);
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportServiceMap = AirproxyModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(airportWiseMap, ancillaryAvailabilityDTO.getAppIndicator(), ssrCatId, null, null,
						ancillaryAvailabilityDTO.isAddEditAnci());

		for (LCCAncillaryAvailabilityOutDTO lccAnciAvailabilityOutDTO : segmentAnciAvailList) {
			FlightSegmentTO fltSegTO = lccAnciAvailabilityOutDTO.getFlightSegmentTO();
			LCCAncillaryStatus anciStatus = new LCCAncillaryStatus();
			anciStatus.setAncillaryType(new LCCAncillaryType(anciType));
			if (!fltSegTO.isForInsuranceAvailability() && !csOCFlightMapping.get(fltSegTO.getFlightSegId())) {
				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> apEntry : airportServiceMap.entrySet()) {
					AirportServiceKeyTO keyTO = apEntry.getKey();

					if (fltSegTO.getSegmentCode().equals(keyTO.getSegmentCode())
							&& fltSegTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {

						if (apEntry.getValue().size() > 0) {
							// set airport code to support airport services
							fltSegTO.setAirportCode(keyTO.getAirport());
							anciStatus.setAvailable(true);
						}
					}

				}
			}

			lccAnciAvailabilityOutDTO.getAncillaryStatusList().add(anciStatus);
		}

	}

	private LCCAncillaryStatus populateAncillaryAvailability(FlightSegmentTO flightSegment, LCCAncillaryType anciTypeTO,
			LCCAncillaryAvailabilityInDTO ancillaryAvailabilityDTO, Map<Integer, Boolean> csOCFlightMapping)
			throws ModuleException {
		LCCAncillaryStatus anciStatus = new LCCAncillaryStatus();
		anciStatus.setAncillaryType(anciTypeTO);
		String anciType = anciTypeTO.getAncillaryType();
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = null;
		int salesChannelCode = userPrinciple.getSalesChannel();
		
		if (anciType.equals(LCCAncillaryType.SSR)) {
			flightSegIdWiseCos = SegmentUtil.getFlightSegmentIdWiseClassOfServices(flightSegment, null);
		} else {
			flightSegIdWiseCos = SegmentUtil.getFlightSegmentIdWiseClassOfServices(flightSegment, csOCFlightMapping);
		}

		if (anciType.equals(LCCAncillaryType.SEAT_MAP) && AppSysParamsUtil.isShowSeatMap()
				&& !flightSegment.isForInsuranceAvailability()) {

			Map<Integer, FlightSeatsDTO> seatMap = AirproxyModuleUtils.getSeatMapBD().getFlightSeats(flightSegIdWiseCos, null);
			anciStatus.setAvailable(AncillaryCommonUtil.isSeatAvailable(seatMap));

		} else if (anciType.equals(LCCAncillaryType.INSURANCE) && AppSysParamsUtil.isShowTravelInsurance()) {

			// T_AIG_COUNTRY - check origin, if present then send true
			CommonMasterBD commonMasterBD = AirproxyModuleUtils.getCommonMasterBD();
			anciStatus.setAvailable(commonMasterBD.isAIGCountryCodeActive(flightSegment.getSegmentCode().split("/")[0]));

		} else if (anciType.equals(LCCAncillaryType.MEALS) && AppSysParamsUtil.isShowMeals()
				&& !flightSegment.isForInsuranceAvailability()) {

			Map<Integer, Set<String>> flightSegIdWiseMeals = AncillaryCommonUtil
					.getFlightSegIdWiseSelectedAnci(ancillaryAvailabilityDTO.getFlightRefWiseSelectedMeals());

			Map<Integer, List<FlightMealDTO>> mealMap = AirproxyModuleUtils.getMealBD().getMeals(flightSegIdWiseCos,
					flightSegIdWiseMeals, false, null, salesChannelCode);
			anciStatus.setAvailable(AncillaryCommonUtil.isMealAvailable(mealMap, flightSegIdWiseMeals));

		} else if (anciType.equals(LCCAncillaryType.BAGGAGE) && AppSysParamsUtil.isShowBaggages()
				&& !flightSegment.isForInsuranceAvailability() && !ancillaryAvailabilityDTO.isAtLeastOneSegmentHavingCSOCFlight()) {

			Map<Integer, List<FlightBaggageDTO>> baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate().getBaggages(
					flightSegIdWiseCos, false, false);
			anciStatus.setAvailable(AncillaryCommonUtil.isBaggageAvailable(baggageMap));

		} else if (anciType.equals(LCCAncillaryType.SSR) && AppSysParamsUtil.isInFlightServicesEnabled()
				&& !flightSegment.isForInsuranceAvailability()) {

			SsrBD ssrDelegate = AirproxyModuleUtils.getSsrServiceBD();
			Set<String> ondCodes = getOndCodeList(flightSegment.getSegmentCode());
			String salesChannel = this.userPrinciple.getSalesChannel() + "";
			List<SpecialServiceRequestDTO> specialServiceRequestDTOs = ssrDelegate.getActiveSSRsByFltSegIds(flightSegIdWiseCos,
					SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannel, AppSysParamsUtil.isInventoryCheckForSSREnabled(),
					null, false, ancillaryAvailabilityDTO.isAddEditAnci(),
					ancillaryAvailabilityDTO.isEnableModifySSRWithinBuffer(), false);

			if (specialServiceRequestDTOs.size() > 0) {
				for (SpecialServiceRequestDTO specialServiceRequestDTO : specialServiceRequestDTOs) {
					if (ondCodes.contains(specialServiceRequestDTO.getOndCode())
							|| specialServiceRequestDTO.getOndCode().equals("***/***") 
							|| flightSegment.getSegmentCode().equals(specialServiceRequestDTO.getOndCode())) {
						if (AppSysParamsUtil.isInventoryCheckForSSREnabled()
								&& !this.ancillaryAvailabilityInDTO.isModifyAncillary()) {

							Map<Integer, Set<String>> flightSegIdWiseSSRs = AncillaryCommonUtil
									.getFlightSegIdWiseSelectedAnci(ancillaryAvailabilityDTO.getFlightRefWiseSelectedSSR());

							if (specialServiceRequestDTO.getAvailableQty() > 0) {
								anciStatus.setAvailable(true);
								break;
							} else if ((specialServiceRequestDTO.getAvailableQty().intValue() == 0 || "N"
									.equals(specialServiceRequestDTO.getStatus()))
									&& isSSRAlreadyAdded(flightSegIdWiseSSRs,
											FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()),
											specialServiceRequestDTO.getSsrCode())) {
								anciStatus.setAvailable(true);
								break;
							}
						} else {
							anciStatus.setAvailable(true);
							break;
						}
					}
				}
			} else {
				anciStatus.setAvailable(false);
			}

		} else if (anciType.equals(LCCAncillaryType.FLEXI) && AppSysParamsUtil.isFlexiEnabledInAnci()) {
			anciStatus.setAvailable(true);
		} else {
			anciStatus.setAvailable(false);
		}
		return anciStatus;
	}

	private Set<String> getOndCodeList(String segmentCode) {
		Set<String> ondCodeList = new HashSet<String>();
		Set<String> segments = getMultiLegSegList(segmentCode);

		for (String segment : segments) {
			String[] ondArray = segment.split("/");
			ondCodeList.add(segment);
			ondCodeList.add(ondArray[0] + "/***");
			ondCodeList.add("***/" + ondArray[1]);
		}

		return ondCodeList;
	}

	public static Set<String> getMultiLegSegList(String segmentCode) {
		Set<String> segments = new HashSet<String>();
		String ssArr[] = segmentCode.split("/");
		if (ssArr.length == 2) {
			segments.add(ssArr[0] + "/" + ssArr[1]);
		} else {
			for (int i = 0; i < ssArr.length - 2; i++) {
				String ori = ssArr[i + 0];
				String con = ssArr[i + 1];
				String des = ssArr[i + 2];

				segments.add(ori + "/" + con);
				segments.add(con + "/" + des);
			}
		}

		return segments;
	}

	private boolean isSSRAlreadyAdded(Map<Integer, Set<String>> fltSegIdWiseSSRs, Integer flightSegId, String ssrCode) {

		if (fltSegIdWiseSSRs != null) {
			for (Entry<Integer, Set<String>> entry : fltSegIdWiseSSRs.entrySet()) {
				if (flightSegId != null && (flightSegId.intValue() == entry.getKey().intValue()) && entry.getValue() != null
						&& entry.getValue().contains(ssrCode)) {
					return true;
				}
			}
		}

		return false;
	}

	private void setAnciTaxApplicability(AnciAvailabilityRS anciAvailabilityRS, String departureSegCode, EXTERNAL_CHARGES extChg)
			throws ModuleException {
		switch (extChg) {
		case JN_ANCI:
			if (AppSysParamsUtil.isJNTaxApplicableForAnci()) {
				boolean isJNTaxApplicableForAnci = AirproxyModuleUtils.getChargeBD().isValidChargeExists(ChargeCodes.JN_ANCI_TAX,
						departureSegCode);
				anciAvailabilityRS.setJnTaxApplicable(isJNTaxApplicableForAnci);
				if (isJNTaxApplicableForAnci) {
					Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
					colEXTERNAL_CHARGES.add(extChg);

					ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(AirproxyModuleUtils.getReservationBD()
							.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());
					anciAvailabilityRS.setTaxRatio(externalChgDTO.getRatioValue());
				}
			}
			break;

		default:
			break;
		}
	}

}