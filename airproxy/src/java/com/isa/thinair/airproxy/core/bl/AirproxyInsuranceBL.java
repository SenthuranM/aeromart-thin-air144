package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.INSURANCE_STATES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsurableFlightSegment;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * Insurance BL
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxyInsuranceBL {

	private static Log log = LogFactory.getLog(AirproxyInsuranceBL.class);

	private List<FlightSegmentTO> flightSegmentTOs;

	private String transactionIdentifier;

	private List<LCCInsuredPassengerDTO> insuredPassengerDTOs;

	private LCCInsuredJourneyDTO insuredJourneyDTO;

	private UserPrincipal userPrincipal;

	private SYSTEM system;

	private ApplicationEngine appEngine;

	private String pnr;

	private BigDecimal totalTicketPriceWithoutExternal;

	private BasicTrackInfo trackInfo;

	private LCCInsuredContactInfoDTO contactInfo;

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public List<LCCInsuranceQuotationDTO> execute() throws ModuleException {
		if (this.system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().getInsuranceQuotations(flightSegmentTOs, insuredPassengerDTOs,
					insuredJourneyDTO, transactionIdentifier, userPrincipal, trackInfo, contactInfo);
		} else {
			return getOwnAirlineInsuranceQuotation();
		}
	}

	private List<LCCInsuranceQuotationDTO> getOwnAirlineInsuranceQuotation() {
		List<LCCInsuranceQuotationDTO> insuranceDTOList = new ArrayList<LCCInsuranceQuotationDTO>();

		try {

			if (AppSysParamsUtil.isShowTravelInsurance()) {
				IInsuranceRequest insuranceRequest = populateInsuranceRequest();
				List<InsuranceResponse> insuranceResponses = AirproxyModuleUtils.getInsuranceClientBD().quoteInsurancePolicy(
						insuranceRequest);
				insuranceDTOList = transformInsuranceResponse(insuranceResponses);
			}

		} catch (ModuleException e) {
			log.error("Module exception caught.", e);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
		}

		return insuranceDTOList;
	}

	private IInsuranceRequest populateInsuranceRequest() {
		IInsuranceRequest insuranceRequest = new InsuranceRequestAssembler();

		insuranceRequest.setPnr(pnr);
		insuranceRequest.setTotalTicketPrice(totalTicketPriceWithoutExternal);
		insuranceRequest.setLccInsuranceContactInfo(contactInfo);

		List<InsurableFlightSegment> flightSegments = createFlightSegmentList(flightSegmentTOs);
		insuranceRequest.setFlightSegments(flightSegments);

		for (LCCInsuredPassengerDTO insuredAirTraveler : insuredPassengerDTOs) {
			InsurePassenger insurePassenger = getPopulatedPassenger(insuredAirTraveler);
			insuranceRequest.addPassenger(insurePassenger);
		}

		InsureSegment insureSegment = getPopulatedSegment(insuredJourneyDTO);
		insuranceRequest.addFlightSegment(insureSegment);

		return insuranceRequest;

	}

	private List<LCCInsuranceQuotationDTO> transformInsuranceResponse(List<InsuranceResponse> insuranceResponses)
			throws ModuleException {

		List<LCCInsuranceQuotationDTO> listInsuranceDTO = new ArrayList<LCCInsuranceQuotationDTO>();

		if (insuranceResponses != null) {

			for (InsuranceResponse insuranceResponse : insuranceResponses) {
				LCCInsuranceQuotationDTO insuranceDTO = new LCCInsuranceQuotationDTO();
				insuranceDTO.setResponseCode(insuranceResponse.getErrorCode());
				insuranceDTO.setReponseMessage(insuranceResponse.getErrorMessage());
				insuranceDTO.setIsEuropianCountry(insuranceResponse.isEuropianCountry());
				insuranceDTO.setRank(insuranceResponse.getInsProductConfig().getRank());
				insuranceDTO.setDisplayName(insuranceResponse.getInsProductConfig().getDisplayName());
				insuranceDTO.setGroupID(insuranceResponse.getInsProductConfig().getGroupID());
				insuranceDTO.setSubGroupID(insuranceResponse.getInsProductConfig().getSubGroupID());
				insuranceDTO.setExternalContent(insuranceResponse.getInsProductConfig().isExternalContent());
				insuranceDTO.setPopup(insuranceResponse.getInsProductConfig().isPopup());

				if (insuranceResponse.getErrorCode().equals("0") && insuranceResponse.getErrorMessage().equals("OK")) {
					insuranceDTO.setPolicyCode(insuranceResponse.getPolicyCode());
					insuranceDTO.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());
					insuranceDTO.setQuotedTotalPremiumAmount(insuranceResponse.getQuotedTotalPremiumAmount());
					insuranceDTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
					insuranceDTO.setInsTypeCharges(insuranceResponse.getInsTypeCharges());
					insuranceDTO.setInsuranceExternalContent(insuranceResponse.getInsuranceExternalContent());

					Integer insuranceId = persistReservationInsurance(insuranceResponse);

					if (log.isDebugEnabled()) {
						log.debug("Insurance quote saved successfully, insurance id saved : " + insuranceId);
					}

					insuranceDTO.setInsuranceRefNumber(insuranceId + "");
					insuranceDTO.setTotalPerPaxPremiumAmount(AccelAeroCalculator.divide(
							insuranceResponse.getTotalPremiumAmount(), new BigDecimal(insuredPassengerDTOs.size())));
					insuranceDTO.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
					insuranceDTO.setPlanCode(insuranceResponse.getPlanCode());
				}
				if (SalesChannelsUtil.isAppEngineWebOrMobile(appEngine)) {
					AnciOfferCriteriaTO availableOffer = getApplicableAnciOffer();
					if (availableOffer != null) {
						insuranceDTO.setAnciOffer(true);
						insuranceDTO.setSelectedLanguageAnciOfferName(availableOffer.getNameTranslations().get(
								insuredJourneyDTO.getSelectedLanguage()));
						insuranceDTO.setSelectedLanguageAnciOfferDescription(availableOffer.getDescriptionTranslations().get(
								insuredJourneyDTO.getSelectedLanguage()));
					}
				}

				listInsuranceDTO.add(insuranceDTO);
			}

		}

		Collections.sort(listInsuranceDTO);
		return listInsuranceDTO;
	}

	private Integer persistReservationInsurance(InsuranceResponse insuranceResponse) {
		Integer insuranceId = null;

		ReservationInsurance reservationInsurance = new ReservationInsurance();

		reservationInsurance.setQuoteTime(new Date());
		reservationInsurance.setState(INSURANCE_STATES.QO.toString());

		reservationInsurance.setPolicyCode(insuranceResponse.getPolicyCode());
		reservationInsurance.setMessageId(insuranceResponse.getMessageId());

		reservationInsurance.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());

		reservationInsurance.setAmount(insuranceResponse.getTotalPremiumAmount());
		reservationInsurance.setQuotedAmount(insuranceResponse.getQuotedTotalPremiumAmount());

		reservationInsurance.setTaxAmount(insuranceResponse.getTaxAmount());
		reservationInsurance.setQuotedTaxAmout(insuranceResponse.getQuotedTaxAmout());

		reservationInsurance.setNetAmount(insuranceResponse.getNetAmount());
		reservationInsurance.setQuotedNetAmount(insuranceResponse.getQuotedNetAmount());

		reservationInsurance.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
		reservationInsurance.setPlanCode(insuranceResponse.getPlanCode());

		reservationInsurance.setInsuranceProductConfigID(insuranceResponse.getInsProductConfig().getInsuranceProductConfigID());

		reservationInsurance.setRoundTrip(insuredJourneyDTO.isRoundTrip() ? "Y" : "N");
		reservationInsurance.setTotalPaxCount(insuredPassengerDTOs.size());
		reservationInsurance.setDateOfTravel(insuredJourneyDTO.getJourneyStartDate());
		reservationInsurance.setDateOfReturn(insuredJourneyDTO.getJourneyEndDate());
		reservationInsurance.setOrigin(insuredJourneyDTO.getJourneyStartAirportCode());
		reservationInsurance.setDestination(insuredJourneyDTO.getJourneyEndAirportCode());
		reservationInsurance.setMarketingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		reservationInsurance.setInsuranceType(0);
		reservationInsurance.setInsuranceTotalTicketPrice(totalTicketPriceWithoutExternal);
		reservationInsurance.setSalesChannel(insuredJourneyDTO.getSalesChannel());

		insuranceId = AirproxyModuleUtils.getReservationBD().saveReservationInsurance(reservationInsurance);

		return insuranceId;

	}

	private AnciOfferCriteriaTO getApplicableAnciOffer() throws ModuleException {
		Object[] inboundOutboundList = AncillaryDTOUtil.getOutboundInboundList(flightSegmentTOs);

		List<FlightSegmentTO> outboundFlightSegmentTOs = (List<FlightSegmentTO>) inboundOutboundList[0];
		List<FlightSegmentTO> inboundFlightSegmentTOs = (List<FlightSegmentTO>) inboundOutboundList[1];

		FlightSegmentTO firstDepartureSeg = outboundFlightSegmentTOs.iterator().hasNext() ? outboundFlightSegmentTOs.iterator()
				.next() : null;
		FlightSegmentTO firstArrivalSeg = inboundFlightSegmentTOs.iterator().hasNext() ? inboundFlightSegmentTOs.iterator()
				.next() : null;

		Collection<AnciOfferCriteriaTO> availableOutboundOffers = AirproxyModuleUtils.getAnciOfferBD().getAllAvailableAnciOffers(
				AnciOfferUtil.createAnciOfferSearch(firstDepartureSeg, AnciOfferType.INSURANCE));

		if (firstArrivalSeg != null) {
			Collection<AnciOfferCriteriaTO> availableInboundOffers = AirproxyModuleUtils.getAnciOfferBD()
					.getAllAvailableAnciOffers(AnciOfferUtil.createAnciOfferSearch(firstArrivalSeg, AnciOfferType.INSURANCE));
			if (availableInboundOffers.isEmpty()) {
				return null;
			} else {
				availableInboundOffers.retainAll(availableOutboundOffers);
				return availableInboundOffers.isEmpty() ? null : availableInboundOffers.iterator().next();
			}
		} else {
			return availableOutboundOffers.isEmpty() ? null : availableOutboundOffers.iterator().next();
		}
	}

	private InsureSegment getPopulatedSegment(LCCInsuredJourneyDTO insuredJrny) {

		InsureSegment insSegment = new InsureSegment();
		insSegment.setFromAirportCode(insuredJrny.getJourneyStartAirportCode());
		insSegment.setToAirportCode(insuredJrny.getJourneyEndAirportCode());
		insSegment.setDepartureDate(insuredJrny.getJourneyStartDate());
		insSegment.setArrivalDate(insuredJrny.getJourneyEndDate());
		insSegment.setRoundTrip(insuredJrny.isRoundTrip());
		insSegment.setSalesChanelCode(insuredJrny.getSalesChannel());

		addExtraFlightSegmentDetails(insSegment);
		return insSegment;
	}

	private InsureSegment addExtraFlightSegmentDetails(InsureSegment insSegment) {
		if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
			for (FlightSegmentTO flightsegment : flightSegmentTOs) {
				if (!flightsegment.isReturnFlag()) {
					insSegment.setDepartureFlightNo(flightsegment.getFlightNumber());
				} else {
					insSegment.setArrivalFlightNo(flightsegment.getFlightNumber());
				}

			}
		}
		return insSegment;
	}

	private InsurePassenger getPopulatedPassenger(LCCInsuredPassengerDTO passengerDTO) {

		InsurePassenger insurePassenger = new InsurePassenger();

		if (passengerDTO.getAddressDTO().getStreetLnNm1() == null) {
			insurePassenger.setAddressLn1(passengerDTO.getAddressDTO().getStreetLnNm1());
		}

		if (passengerDTO.getAddressDTO().getStreetLnNm2() == null) {
			insurePassenger.setAddressLn2(passengerDTO.getAddressDTO().getStreetLnNm2());
		}
		insurePassenger.setTitle(passengerDTO.getNameDTO().getPaxTitle());
		insurePassenger.setCity(passengerDTO.getAddressDTO().getCityName());
		insurePassenger.setEmail(passengerDTO.getEmail());
		insurePassenger.setCountryOfAddress(passengerDTO.getAddressDTO().getCountryName());
		insurePassenger.setHomePhoneNumber(passengerDTO.getHomePhoneNumber());
		insurePassenger.setFirstName(passengerDTO.getNameDTO().getFirstName());
		insurePassenger.setLastName(passengerDTO.getNameDTO().getLastName());
		insurePassenger.setDateOfBirth(passengerDTO.getBirthDate());
		insurePassenger.setNationality(passengerDTO.getNationality());
		return insurePassenger;
	}

	private List<InsurableFlightSegment> createFlightSegmentList(List<FlightSegmentTO> flightSegmentTOs) {
		List<InsurableFlightSegment> flightSegments = new ArrayList<InsurableFlightSegment>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			InsurableFlightSegment flightSegment = new InsurableFlightSegment();
			flightSegment.setDepartureStationCode(flightSegmentTO.getSegmentCode().substring(0, 3));
			flightSegment.setDepartureDateTimeLocal(flightSegmentTO.getDepartureDateTime());
			flightSegment.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
			flightSegment.setArrivalStationCode(flightSegmentTO.getSegmentCode().substring(
					flightSegmentTO.getSegmentCode().length() - 3, flightSegmentTO.getSegmentCode().length()));
			flightSegment.setFlightNo(flightSegmentTO.getFlightNumber());
			flightSegment.setDomesticFlight(flightSegmentTO.isDomesticFlight());
			flightSegments.add(flightSegment);
		}

		return flightSegments;

	}

	public void setFlightSegments(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public void setPassengers(List<LCCInsuredPassengerDTO> insuredPassengerDTOs) {
		this.insuredPassengerDTOs = insuredPassengerDTOs;
	}

	public void setJourneyDetails(LCCInsuredJourneyDTO insuredJourneyDTO) {
		this.insuredJourneyDTO = insuredJourneyDTO;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public void setTotalTicketPriceWithoutExternal(BigDecimal totalTicketPriceWithoutExternal) {
		this.totalTicketPriceWithoutExternal = totalTicketPriceWithoutExternal;
	}

	public BasicTrackInfo getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(BasicTrackInfo trackInfo) {
		this.trackInfo = trackInfo;
	}

	public ApplicationEngine getAppEngine() {
		return appEngine;
	}

	public void setAppEngine(ApplicationEngine appEngine) {
		this.appEngine = appEngine;
	}

	public LCCInsuredContactInfoDTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(LCCInsuredContactInfoDTO contactInfo) {
		this.contactInfo = contactInfo;
	}
}
