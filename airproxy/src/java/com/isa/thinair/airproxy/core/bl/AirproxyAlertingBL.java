package com.isa.thinair.airproxy.core.bl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author sanjaya
 * 
 */
public class AirproxyAlertingBL {

	/**
	 * Retrieves the alerts from either own airline or other selected airline.
	 * 
	 * @param searchCriteria
	 *            The search criteria for alert search.
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, BasicTrackInfo trackInfo)
			throws ModuleException {
		Page alertResultPage = null;

		if (StringUtils.isEmpty(searchCriteria.getSearchAirline())
				|| AppSysParamsUtil.getDefaultCarrierCode().equals(searchCriteria.getSearchAirline())) {
			alertResultPage = AirproxyModuleUtils.getAlertingBD().retrieveAlertsForSearchCriteria(searchCriteria);
		} else {
			alertResultPage = AirproxyModuleUtils.getLCCAlertingBD().retrieveAlertsForSearchCriteria(searchCriteria, trackInfo);
		}

		return alertResultPage;
	}

	/**
	 * Clears the given set of alerts in the airline
	 * 
	 * @param alertIDMap
	 * @param trackInfo
	 *            TODO
	 * @throws ModuleException
	 */
	public static void clearAlerts(Map<String, Collection<Integer>> alertIDMap, BasicTrackInfo trackInfo) throws ModuleException {
		if (alertIDMap != null) {
			for (String airline : alertIDMap.keySet()) {
				if (StringUtils.isEmpty(airline) || AppSysParamsUtil.getDefaultCarrierCode().equals(airline)) {
					// if this airline alert id's we can call the segment BD directly.
					AirproxyModuleUtils.getResSegmentBD().clearAlerts(alertIDMap.get(airline));
				} else {
					AirproxyModuleUtils.getLCCAlertingBD().clearAlerts(airline, alertIDMap.get(airline), trackInfo);
				}

			}
		}
	}
	
	public static Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException {
		
		return AirproxyModuleUtils.getAlertingBD().retrieveSubmittedRequests(searchCriteria);
		
	}
	
	public static void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException{
		
		AirproxyModuleUtils.getAlertingBD().submitOrEditRequest(queueRequest);
		
	}
	
	public static QueueRequest getQueueRequest(int requestId) throws ModuleException{
		
		return AirproxyModuleUtils.getAlertingBD().getQueueRequest(requestId);
		
	}
	
	public static void addRequestHistory(RequestHistory requestHistory) throws ModuleException{
		
		AirproxyModuleUtils.getAlertingBD().addRequestHistory(requestHistory);
		
	}
	
	public static void deleteRequest(int requestId) throws ModuleException{
		
		AirproxyModuleUtils.getAlertingBD().deleteRequest(requestId);
		
	}
	
	public static Page getRequestHistory(int requestId) throws ModuleException{
		
		return AirproxyModuleUtils.getAlertingBD().getRequestHistory(requestId);
		
	}
	
	public static Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException{
		
		return AirproxyModuleUtils.getAlertingBD().retrieveSubmittedRequestsForAction(userID, queueId, status);
		
	}
	
	public static Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException{
		
		return AirproxyModuleUtils.getAlertingBD().getRequestStatusForPnr(page,maxResult,pnrId);
		
	}
	
	public static Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException{
		
		return AirproxyModuleUtils.getAlertingBD().getHistoryForRequest(page,maxResult,requestId);
		
	}

}
