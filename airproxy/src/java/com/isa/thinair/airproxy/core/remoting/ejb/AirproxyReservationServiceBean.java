package com.isa.thinair.airproxy.core.remoting.ejb;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.LccAdaptor;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.dto.PaxCreditReinstateTO;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleSearchRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.InfantInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.core.bl.AirProxyETicketInfoBL;
import com.isa.thinair.airproxy.core.bl.AirproxyAirportServicesBL;
import com.isa.thinair.airproxy.core.bl.AirproxyAirportTransferBL;
import com.isa.thinair.airproxy.core.bl.AirproxyAlertingBL;
import com.isa.thinair.airproxy.core.bl.AirproxyAncillaryAvailabilityBL;
import com.isa.thinair.airproxy.core.bl.AirproxyAutomaticCheckinBL;
import com.isa.thinair.airproxy.core.bl.AirproxyBaggageBusinessLayer;
import com.isa.thinair.airproxy.core.bl.AirproxyDuplicateCheckBL;
import com.isa.thinair.airproxy.core.bl.AirproxyFlightServiceBL;
import com.isa.thinair.airproxy.core.bl.AirproxyInFlightServicesBL;
import com.isa.thinair.airproxy.core.bl.AirproxyInsuranceBL;
import com.isa.thinair.airproxy.core.bl.AirproxyItineraryBL;
import com.isa.thinair.airproxy.core.bl.AirproxyLoadReservationBL;
import com.isa.thinair.airproxy.core.bl.AirproxyLoyaltyManagementBL;
import com.isa.thinair.airproxy.core.bl.AirproxyMealBL;
import com.isa.thinair.airproxy.core.bl.AirproxyModifyAncillaryBL;
import com.isa.thinair.airproxy.core.bl.AirproxyOnholdAvailabilityBL;
import com.isa.thinair.airproxy.core.bl.AirproxyOnholdRollForwardBL;
import com.isa.thinair.airproxy.core.bl.AirproxyPnrGeneratorBL;
import com.isa.thinair.airproxy.core.bl.AirproxyPromotionsBL;
import com.isa.thinair.airproxy.core.bl.AirproxyReservationAvailabilityBL;
import com.isa.thinair.airproxy.core.bl.AirproxyReservationBL;
import com.isa.thinair.airproxy.core.bl.AirproxyScheduleAvailabilityBL;
import com.isa.thinair.airproxy.core.bl.AirproxySeatMapBL;
import com.isa.thinair.airproxy.core.bl.AirproxyServiceTaxBL;
import com.isa.thinair.airproxy.core.bl.AirproxyTempPaymentBL;
import com.isa.thinair.airproxy.core.bl.BaggageRatesBL;
import com.isa.thinair.airproxy.core.bl.ManageSeatMapBL;
import com.isa.thinair.airproxy.core.service.bd.AirproxyReservationBDLocal;
import com.isa.thinair.airproxy.core.service.bd.AirproxyReservationBDRemote;
import com.isa.thinair.airproxy.core.utils.transformer.PaymentConvertUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.lccclient.api.util.LccAncillaryUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * Bean class for reservation proxy, which will mediate between LCC and own airline and provide a uniform view to upper
 * tiers
 * 
 * @author Dilan Anuruddha
 */
@Stateless
@RemoteBinding(jndiBinding = "AirproxyReservationService.remote")
@LocalBinding(jndiBinding = "AirproxyReservationService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirproxyReservationServiceBean extends PlatformBaseSessionBean implements AirproxyReservationBDLocal,
		AirproxyReservationBDRemote {

	private static final Log log = LogFactory.getLog(AirproxyReservationServiceBean.class);

	/************************************************************************************************************************************
	 * RESERVATION QUERY SECTION *
	 ************************************************************************************************************************************/

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightPriceRS quoteFlightPrice(FlightPriceRQ priceQuoteRQ, TrackInfoDTO trackInfo) throws ModuleException {
		FlightPriceRS flightPriceRS = AirproxyReservationAvailabilityBL.quoteFlightPrice(priceQuoteRQ, this.getUserPrincipal(), trackInfo);
		flightPriceRS.fixMissingOndSequence();
		return flightPriceRS;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightAvailRS searchAvailableFlights(FlightAvailRQ flightAvailRQ, TrackInfoDTO trackInfo, boolean withAllFares)
			throws ModuleException {
		FlightAvailRS flightAvailRS =  AirproxyReservationAvailabilityBL.searchFlights(flightAvailRQ, this.getUserPrincipal(), withAllFares, trackInfo);
		flightAvailRS.fixMissingOndSequence();
		return flightAvailRS;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightAvailRS searchAvailableFlightsWithAllFares(FlightAvailRQ flightAvailRQ, TrackInfoDTO trackInfo)
			throws ModuleException {
		FlightAvailRS flightAvailRS = AirproxyReservationAvailabilityBL.searchFlights(flightAvailRQ, this.getUserPrincipal(), true, trackInfo);
		flightAvailRS.fixMissingOndSequence();
		return flightAvailRS;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FareAvailRS searchFares(FareAvailRQ fareAvailRQ, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyReservationAvailabilityBL.searchFares(fareAvailRQ, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightPriceRS changeFlightPrice(ChangeFareRQ changeFareRQ, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyReservationAvailabilityBL.changeFlightPrice(changeFareRQ, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<?>
			priviledgeUserBlockSeat(FlightPriceRQ priceQuoteRQ, FareSegChargeTO fareSegChargeTO)
					throws ModuleException {
		return AirproxyReservationAvailabilityBL.priviledgeUserBlockSeat(priceQuoteRQ, fareSegChargeTO);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ScheduleAvailRS getSchedulesList(ScheduleAvailRQ scheduleAvailRQ, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyScheduleAvailabilityBL.getSchedules(scheduleAvailRQ, trackInfo);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Map<String, List<Integer>>> getBookingClassDataForOpenReturn(Collection<String> segmentIds, String pnr,
			TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL resBL = new AirproxyLoadReservationBL();
		return resBL.loadReservationSegmentsBypnrBySegmentID(segmentIds, pnr, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCClientReservation confirmOpenReturnSegments(String pnr, LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			Collection<Integer> oldSegIds, Collection<TempSegBcAlloc> blockSeatIds, TrackInfoDTO trackInfo)
			throws ModuleException {
		AirproxyLoadReservationBL resBL = new AirproxyLoadReservationBL();
		return resBL.confirmOpenReturnSegments(pnr, lccClientResAlterQueryModesTO, oldSegIds, blockSeatIds,
				this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<RollForwardFlightAvailRS> searchAvailableFlightForRollForward(Collection<LCCClientReservationSegment> colsegs,
			Date startDate, Date endDate, Integer adultCount, Integer childCount, Integer infantCount, SYSTEM searchSystem,
			boolean isOverBook, String agentCode) throws ModuleException {
		return AirproxyReservationAvailabilityBL.searchAvailableFlightForRollForward(colsegs, startDate, endDate, adultCount,
				childCount, infantCount, searchSystem, isOverBook, agentCode);
	}

	@Override
	public boolean isDuplicateNameExist(Collection<Integer> fltSegmentIds, String pnr) throws ModuleException {
		return AirproxyReservationAvailabilityBL.isDuplicateNameExist(fltSegmentIds, pnr);
	}

	/**
	 * Method to check Onhold availability based on defined criteria
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isOnholdEnabled(List<FlightSegmentTO> flightSegmentTOs, List<ReservationPax> paxList, String email,
			int validationType, int salesChannel, SYSTEM searchSystem, String ipAddress, BasicTrackInfo trackInfo)
			throws ModuleException {
		return AirproxyOnholdAvailabilityBL.checkOnholdAvailability(flightSegmentTOs, paxList, email, validationType,
				salesChannel, searchSystem, ipAddress, trackInfo, getUserPrincipal());
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		return AirproxyServiceTaxBL.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfoDTO,
				this.getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfo, UserPrincipal userPrincipal)
			throws ModuleException {
		return AirproxyServiceTaxBL.quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO, trackInfo, userPrincipal);
	}

	@Override
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo, boolean isGroupPNR, String operatingCarrier) throws ModuleException{
		
		return AirproxyLoadReservationBL.getSelfReprotectFlights(alertId, trackInfo, isGroupPNR, operatingCarrier);
		
	}

	@Override
	public boolean hasActionedByIBE(String pnr) throws ModuleException{
		
		return AirproxyLoadReservationBL.hasActionedByIBE(pnr);
	}
		
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String generateNewPnr(List<FlightSegmentTO> flightSegments, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyPnrGeneratorBL.generateNewPnr(flightSegments, trackInfo);
	}

	/************************************************************************************************************************************
	 * Passenger Section *
	 ************************************************************************************************************************************/

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation addInfant(LCCClientReservation lccClientReservation, Integer intPaymentMode, boolean isGroupPNR,
			Map<Integer, LCCClientPaymentAssembler> payments, boolean enableFraudCheck, boolean autoCancellationEnabled,
			boolean hasBufferTimeAutoCnx, Map<String, String> mapPrivileges, TrackInfoDTO trackInfoDTO,
			boolean mcETGenerationEligible, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap) throws Exception {
		return AirproxyLoadReservationBL.addInfant(lccClientReservation, intPaymentMode, isGroupPNR, this.getUserPrincipal(),
				payments, enableFraudCheck, autoCancellationEnabled, hasBufferTimeAutoCnx, trackInfoDTO, mcETGenerationEligible, externalChargesMap);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public InfantInfoTO getInfantCharge(String groupPNR, boolean allowAddInfant) throws ModuleException {
		return AirproxyLoadReservationBL.getInfantCharge(groupPNR, allowAddInfant, this.getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reInstatePaxCredit(PaxCreditReinstateTO paxCreditReinstateTO, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.reInstatePaxCredit(paxCreditReinstateTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean refundPassenger(CommonReservationAssembler reservationAssembler, String userNotes, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			boolean removeAgentCommission, boolean isManualRefund) throws ModuleException {
		return AirproxyLoadReservationBL.groupPassengerRefund(reservationAssembler, userNotes, isGroupPnr, trackInfoDTO,
				preferredRefundOrder, pnrPaxOndChgIds, removeAgentCommission, isManualRefund);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation removePassenger(String groupPnr, String version, List<LCCClientReservationPax> reservationPaxs,
			boolean isGroupPnr, CustomChargesTO customChargesTO, TrackInfoDTO trackInfoDTO, String userNotes,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException {
		return AirproxyLoadReservationBL.removePassenger(groupPnr, version, reservationPaxs, isGroupPnr, customChargesTO,
				trackInfoDTO, userNotes, paxExternalCharges);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCClientReservationPax loadPassenger(String groupPnr, int pnrPaxId, TrackInfoDTO trackInfoDTO, boolean isGroupPnr)
			throws ModuleException {
		return AirproxyLoadReservationBL.loadPassenger(groupPnr, pnrPaxId, isGroupPnr, trackInfoDTO);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PaxContactConfigDTO loadPaxContactConfig(String system, String appName, List<String> carrierList,
			BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.loadPaxContactConfig(system, appName, carrierList, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, List<PaxCountryConfigDTO>> loadPaxCountryConfig(String system, String appName,
			List<String> originDestinations, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.loadPaxCountryConfig(system, appName, originDestinations, trackInfo);
	}

	/************************************************************************************************************************************
	 * ANCILLARY SECTION *
	 ************************************************************************************************************************************/
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AnciAvailabilityRS getAncillaryAvailability(LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO,
			BasicTrackInfo trackInfo) throws ModuleException {
		AirproxyAncillaryAvailabilityBL anciAvailBL = new AirproxyAncillaryAvailabilityBL(lccAncillaryAvailabilityDTO,
				this.getUserPrincipal(), trackInfo);
		return anciAvailBL.execute();

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCMealResponceDTO getAvailableMeals(List<LCCMealRequestDTO> lccMealRequestDTOs, BasicTrackInfo trackInfo,
			Map<String, Set<String>> fltSegWiseMeals, List<BundledFareDTO> bundledFareDTOs, SYSTEM system,
			String transactionIdentifier, ApplicationEngine appEngine, String pnr) throws ModuleException {
		AirproxyMealBL mealBL = new AirproxyMealBL(lccMealRequestDTOs, transactionIdentifier, fltSegWiseMeals, bundledFareDTOs,
				system, null, trackInfo, this.getUserPrincipal());
		mealBL.setAppEngine(appEngine);
		mealBL.setPnr(pnr);
		return mealBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCMealResponceDTO getAvailableMealsWithTranslations(List<LCCMealRequestDTO> lccMealRequestDTOs,
			String transactionIdentifier, List<BundledFareDTO> bundledFareDTOs, SYSTEM system, String selectedLanguage,
			BasicTrackInfo trackInfo, ApplicationEngine appEngine, String pnr) throws ModuleException {
		AirproxyMealBL mealBL = new AirproxyMealBL(lccMealRequestDTOs, transactionIdentifier, null, bundledFareDTOs, system,
				selectedLanguage, trackInfo, getUserPrincipal());
		mealBL.setAppEngine(appEngine);
		mealBL.setPnr(pnr);
		return mealBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCInsuranceQuotationDTO> getInsuranceQuotation(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCInsuredPassengerDTO> insuredPassengerDTOs, LCCInsuredJourneyDTO insuredJourneyDTO,
			String transactionIdentifier, SYSTEM system, String pnr, ClientCommonInfoDTO clientInfoDTO,
			BigDecimal totalTicketPriceWithoutExternal, BasicTrackInfo trackInfo, ApplicationEngine appEngine,
			LCCInsuredContactInfoDTO contactInfo) throws ModuleException {
		AirproxyInsuranceBL insuranceBL = new AirproxyInsuranceBL();
		insuranceBL.setFlightSegments(flightSegmentTOs);
		insuranceBL.setPassengers(insuredPassengerDTOs);
		insuranceBL.setJourneyDetails(insuredJourneyDTO);
		insuranceBL.setTransactionIdentifier(transactionIdentifier);
		insuranceBL.setSystem(system);
		insuranceBL.setPnr(pnr);
		insuranceBL.setUserPrincipal(getUserPrincipal(clientInfoDTO));
		insuranceBL.setTotalTicketPriceWithoutExternal(totalTicketPriceWithoutExternal);
		insuranceBL.setTrackInfo(trackInfo);
		insuranceBL.setAppEngine(appEngine);
		insuranceBL.setContactInfo(contactInfo);
		return insuranceBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCSeatMapDTO getSeatMap(LCCSeatMapDTO lccSeatMapDTO, String selectedLocale, BasicTrackInfo trackInfo,
			ApplicationEngine appEngine) throws ModuleException {
		AirproxySeatMapBL seatMapBL = new AirproxySeatMapBL(lccSeatMapDTO, selectedLocale, trackInfo);
		seatMapBL.setAppEngine(appEngine);
		return seatMapBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCFlightSegmentSSRDTO> getSpecialServiceRequests(List<FlightSegmentTO> flightSegmentTOs,
			String transactionIdentifier, SYSTEM system, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> fltRefWiseSelectedSSR, boolean isModifyAnci, boolean isGdsPnr, boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) throws ModuleException {
		AirproxyInFlightServicesBL inFlightServiceBL = new AirproxyInFlightServicesBL();
		inFlightServiceBL.setFlightSegmentTOs(flightSegmentTOs);
		inFlightServiceBL.setSystem(system);
		inFlightServiceBL.setTransactionIdentifier(transactionIdentifier);
		inFlightServiceBL.setUserPrinciple(this.getUserPrincipal());
		inFlightServiceBL.setSelectedLanguage(selectedLanguage);
		inFlightServiceBL.setTrackInfo(trackInfo);
		inFlightServiceBL.setFltRefWiseSelectedSSR(fltRefWiseSelectedSSR);
		inFlightServiceBL.setModifyAnci(isModifyAnci);
		inFlightServiceBL.setGdsPnr(isGdsPnr);
		inFlightServiceBL.setHasPrivModifySSRWithinBuffer(hasPrivModifySSRWithinBuffer);
		inFlightServiceBL.setUserDefinedSsrOnly(isUserDefinedSsrOnly);
		return inFlightServiceBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean blockSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, SYSTEM system,
			BasicTrackInfo trackInfo) throws ModuleException {
		ManageSeatMapBL blockSeatBL = new ManageSeatMapBL(blockSeatDTOs, transactionIdentifier, system, trackInfo);
		return blockSeatBL.blockSeats();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean releaseSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, SYSTEM system,
			BasicTrackInfo trackInfo) throws ModuleException {
		ManageSeatMapBL blockSeatBL = new ManageSeatMapBL(blockSeatDTOs, transactionIdentifier, system, trackInfo);
		return blockSeatBL.releaseSeats();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean updateAncillary(CommonAncillaryModifyAssembler anciAssembler, boolean enableFraudCheck,
			CommonReservationContactInfo contactInfo, boolean isInterlinePaymentAllowed,
			Map<String, Set<String>> flightSegWiseSelectedMeals, TrackInfoDTO trackInfo) throws ModuleException {

		AirproxyModifyAncillaryBL anciModBL = new AirproxyModifyAncillaryBL(anciAssembler, trackInfo, this.getUserPrincipal(),
				enableFraudCheck, contactInfo, isInterlinePaymentAllowed, flightSegWiseSelectedMeals);
		return anciModBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCClientReservationInsurance> loadReservationInsuranceByPnr(String pnr, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.loadReservationInsuranceByPnr(pnr);
	}

	/**
	 * For Baggage
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, SYSTEM system, String selectedLanguage, boolean isModifyOperation,
			boolean hasFinalCutOverPrivilege, boolean isRequote, ApplicationEngine appEngine,
			LCCReservationBaggageSummaryTo baggageSummaryTo, List<BundledFareDTO> bundledFareDTOs, String pnr,
			BasicTrackInfo trackInfo) throws ModuleException {

		AirproxyBaggageBusinessLayer baggageBL = new AirproxyBaggageBusinessLayer();
		baggageBL.setBaggageRequestDTOs(lccBaggageRequestDTOs);
		baggageBL.setSystem(system);
		baggageBL.setAppEngine(appEngine);
		baggageBL.setTransactionIdentifier(transactionIdentifier);
		baggageBL.setSelectedLanguage(selectedLanguage);
		baggageBL.setModifyOperation(isModifyOperation);
		baggageBL.setHasFinalCutOverPrivilege(hasFinalCutOverPrivilege);
		baggageBL.setRequote(isRequote);
		baggageBL.setBaggageSummaryTo(baggageSummaryTo);
		baggageBL.setUserPrincipal(getUserPrincipal());
		baggageBL.setBundledFareDTOs(bundledFareDTOs);
		baggageBL.setPnr(pnr);
		baggageBL.setTrackInfo(trackInfo);

		return baggageBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAvailableAiportServices(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, int ssrCategory, SYSTEM system,
			String selectedLanguage, String transactionIdentifier, BasicTrackInfo trackInfo, Map<String, Integer> serviceCount,
			boolean isModifyAnci, List<BundledFareDTO> bundledFareDTOs, String pnr) throws ModuleException {
		AirproxyAirportServicesBL airportServicesBL = new AirproxyAirportServicesBL();
		airportServicesBL.setAppIndicator(appIndicator);
		airportServicesBL.setFlightSegmentTOs(flightSegmentTOs);
		airportServicesBL.setSelectedLanguage(selectedLanguage);
		airportServicesBL.setServiceCategory(ssrCategory);
		airportServicesBL.setSystem(system);
		airportServicesBL.setTrackInfo(trackInfo);
		airportServicesBL.setTransactionIdentifier(transactionIdentifier);
		airportServicesBL.setServiceCount(serviceCount);
		airportServicesBL.setUserPrincipal(this.getUserPrincipal());
		airportServicesBL.setModifyAnci(isModifyAnci);
		airportServicesBL.setBundledFareDTOs(bundledFareDTOs);
		airportServicesBL.setPnr(pnr);

		return airportServicesBL.execute();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAvailableAiportTransfers(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, SYSTEM system,
			Map<String, Integer> serviceCount, boolean isModifyAnci) throws ModuleException {
		
		AirproxyAirportTransferBL airportTransferBL = new AirproxyAirportTransferBL();
		airportTransferBL.setAppIndicator(appIndicator);
		airportTransferBL.setFlightSegmentTOs(flightSegmentTOs);
		airportTransferBL.setSystem(system);
		airportTransferBL.setServiceCount(serviceCount);
		airportTransferBL.setUserPrincipal(this.getUserPrincipal());
		airportTransferBL.setModifyAnci(isModifyAnci);
		return airportTransferBL.execute();
	}

	/**
	 * This is to get available automatic checkins
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> getAvailableAutomaticCheckins(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, SYSTEM system,
			Map<String, Integer> serviceCount, boolean isModifyAnci) throws ModuleException {
		AirproxyAutomaticCheckinBL automaticCheckinBL = new AirproxyAutomaticCheckinBL();
		automaticCheckinBL.setAppIndicator(appIndicator);
		automaticCheckinBL.setFlightSegmentTOs(flightSegmentTOs);
		automaticCheckinBL.setSystem(system);
		automaticCheckinBL.setServiceCount(serviceCount);
		automaticCheckinBL.setUserPrincipal(this.getUserPrincipal());
		automaticCheckinBL.setModifyAnci(isModifyAnci);
		return automaticCheckinBL.execute();
	}

	/************************************************************************************************************************************
	 * RESERVATION SECTION *
	 ************************************************************************************************************************************/

	/**
	 * Method used to get the reservations list. Introduced for dry sell phase 2 project to search dry bookings in other
	 * carriers.
	 * 
	 * @throws ParseException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException, ParseException {
		return AirproxyLoadReservationBL.searchReservations(reservationSearchDTO, userPrincipal, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce makeReservation(CommonReservationAssembler lcclientReservationAssembler, TrackInfoDTO trackInfoDTO)
			throws Exception {
		return AirproxyReservationBL.makeReservation(lcclientReservationAssembler, this.getUserPrincipal(), trackInfoDTO);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LCCClientReservation searchReservationByPNR(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.loadReservation(pnrModesDTO, paramRQInfo, trackInfo);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce isPaymentReferenceAvailable(String externalReference,String status,char productType) throws ModuleException {
		return AirproxyLoadReservationBL.isPaymentReferenceAvailable(externalReference, status, productType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce isPaymentReferenceAvailable(String externalReference,char productType) throws ModuleException {
		return AirproxyLoadReservationBL.isPaymentReferenceAvailable(externalReference, productType);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation searchResByPNR(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.loadReservation(pnrModesDTO, paramRQInfo, trackInfo);
	}

	@Deprecated
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation adjustCharge(String groupPNr, String version, List<LCCClientChargeAdustment> chargeAdjustments,
			boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.adjustCharge(groupPNr, version, chargeAdjustments, isGroupPNR, clientInfoDto, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation adjustGroupCharge(String groupPNr, String version,
			List<LCCClientChargeAdustment> chargeAdjustments, boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto,
			TrackInfoDTO trackInfo, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS,
			Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax) throws ModuleException {
		return AirproxyLoadReservationBL.adjustGroupCharge(groupPNr, version, chargeAdjustments, isGroupPNR, clientInfoDto,
				trackInfo, serviceTaxRS, handlingFeeByPax);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version, boolean isGroupPnr,
			boolean enableFraudCheck, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo, boolean isGdsSale, boolean isInfantPaymentSeparated) throws ModuleException {
		return AirproxyLoadReservationBL.balancePayment(lccClientBalancePayment, version, isGroupPnr,
				getUserPrincipal(clientInfoDTO), enableFraudCheck, trackInfo, isGdsSale, isInfantPaymentSeparated);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce balancePaymentWithTransaction(LCCClientBalancePayment lccClientBalancePayment, String version, boolean isGroupPnr,
			boolean enableFraudCheck, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.balancePayment(lccClientBalancePayment, version, isGroupPnr,
				getUserPrincipal(clientInfoDTO), enableFraudCheck, trackInfo, false, false);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo, boolean isVoidOperaion, boolean isOnHoldRelease) throws ModuleException {
		return AirproxyLoadReservationBL.cancelReservation(lccClientResAlterModesTO, getUserPrincipal(clientInfoDTO), trackInfo,
				isVoidOperaion, isOnHoldRelease);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version, boolean isGroupPnr,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.extendOnHold(groupPNR, extendDateTimeZulu, version, isGroupPnr,
				getUserPrincipal(clientInfoDTO), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation modifyWaitListedPriority(String groupPNR, Integer priority, Integer fltSegId, String version,
			boolean isGroupPnr, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.modifyWaitListedPriority(groupPNR, priority, fltSegId, version, isGroupPnr,
				getUserPrincipal(clientInfoDTO), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, CommonCreditCardPaymentInfo> makeTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo lccClientReservationContactInfo, LCCClientPaymentAssembler paymentAssembler,
			boolean isCredit, SYSTEM system, String originatorCarrierCode, TrackInfoDTO trackInfoDTO, boolean isCsOcFlightAvailable) throws ModuleException {

		AirproxyTempPaymentBL tmpPayBL = new AirproxyTempPaymentBL(groupPNR, lccClientReservationContactInfo, paymentAssembler,
				isCredit, trackInfoDTO, system, originatorCarrierCode, isCsOcFlightAvailable);
		return tmpPayBL.makeTemporaryPayment();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyContactInfo(String pnr, String version, CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, boolean isGropPNR, String appIndicator,
			TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.modifyContactInfo(pnr, version, lccClientReservationContactInfo,
				oldLccClientReservationContactInfo, isGropPNR, appIndicator, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyPassengerInfo(LCCClientReservation lccClientReservation, LCCClientReservation oldLccClientReservation,
			boolean isGropPNR, ClientCommonInfoDTO clientInfoDTO, String appIndicator, boolean skipDuplicateNameChack,
			boolean applyNameChangeCharge, Collection<String> allowedOperations, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.modifyPassengerInfo(lccClientReservation, oldLccClientReservation, isGropPNR,
				getUserPrincipal(clientInfoDTO), appIndicator, skipDuplicateNameChack, applyNameChangeCharge, allowedOperations, trackInfo);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getHistoryForPrint(String pnr, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.getHistoryForPrint(pnr, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserNoteTO> searchReservationAudit(String pnr, String carrierCode, boolean isGropPNR, String originChanel,
			BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.searchReservationAudit(pnr, carrierCode, isGropPNR, originChanel, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserNoteTO> searchUserNoteHistory(String pnr, String carrierCode, boolean isGropPNR, String originChanel,
			BasicTrackInfo trackInfo, boolean isClassifyUN) throws ModuleException {
		return AirproxyLoadReservationBL.searchUserNoteHistory(pnr, carrierCode, isGropPNR, originChanel, trackInfo, isClassifyUN);
	}
    
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, boolean isGropPNR,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		return AirproxyLoadReservationBL.splitReservation(lccClientReservation, trackInfoDTO, isGropPNR);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version, boolean isGropPNR,
			TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.transferOwnership(groupPNR, transfereeAgent, version, isGropPNR,
				this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationBalanceTO getResAlterationBalanceSummary(LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO,
			TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.getResAlterationBalanceSummary(lccClientResAlterQueryModesTO, this.getUserPrincipal(),
				trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryTO, TrackInfoDTO trackInfo)
			throws ModuleException {
		if (balanceQueryTO.getSystem() == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCReservationBD().getRequoteBalanceSummary(balanceQueryTO, trackInfo);
		} else {
			return AirproxyModuleUtils.getReservationBD().getRequoteBalanceSummary(balanceQueryTO, this.getUserPrincipal(),
					trackInfo);
		}
	}

	// @Override
	// @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	// public ReservationBalanceTO getLCCRequoteBalanceSummary(LCCClientResRequoteQueryTO lccClientResRequoteQueryTO,
	// TrackInfoDTO trackInfo) throws ModuleException {
	// return AirproxyModuleUtils.getLCCReservationBD().getRequoteBalanceSummary(lccClientResRequoteQueryTO, trackInfo);
	// }

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce requoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {

		if (requoteModifyRQ.getLccTemporaryTnxMap() != null) {
			Map<Integer, CommonCreditCardPaymentInfo> lccTmpTnxMap = requoteModifyRQ.getLccTemporaryTnxMap();
			for (Integer pnrPaxId : lccTmpTnxMap.keySet()) {
				requoteModifyRQ
						.addTemporaryTnx(pnrPaxId, PaymentConvertUtil.convertCreditCardPayment(lccTmpTnxMap.get(pnrPaxId)));
			}
		}

		if (requoteModifyRQ.getSystem() == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCReservationBD().lccRequoteModifySegments(requoteModifyRQ, trackInfoDTO);
		} else {
			// TODO payment assembling to be totally revamped! Too much transformations unnecessarily. Added totally
			// with
			// other
			if (requoteModifyRQ.getLccPassengerPayments() != null) {
				Map<String, LCCClientPaymentAssembler> lccPassengerPayments = requoteModifyRQ.getLccPassengerPayments();
				for (String travelRefNo : lccPassengerPayments.keySet()) {
					Integer pnrPaxId = PaxTypeUtils.getPnrPaxIdFromLccRef(travelRefNo);
					if (pnrPaxId != null) {
						requoteModifyRQ.addPassengerPaymentAssembler(
								pnrPaxId + "",
								PaymentConvertUtil.populatePerPaxPayment(lccPassengerPayments.get(travelRefNo),
										requoteModifyRQ.getExternalChargesMap()));
					}

				}
			}

			return AirproxyModuleUtils.getReservationBD().requoteModifySegments(requoteModifyRQ, trackInfoDTO);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getItineraryForPrint(LCCClientPnrModesDTO pnrModesDTO, CommonItineraryParamDTO commonItineraryParam,
			ReservationSearchDTO reservationSearchDTO, TrackInfoDTO trackInfo) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" Search reservation initiated for getItineraryForPrint. " + pnrModesDTO.toString());
		}
		return AirproxyItineraryBL.getItineraryForPrint(searchReservationByPNR(pnrModesDTO, null, trackInfo),
				commonItineraryParam, reservationSearchDTO);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void sendEmailItinerary(LCCClientPnrModesDTO pnrModesDTO, CommonItineraryParamDTO commonItineraryParam,
			TrackInfoDTO trackInfo) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug(" Search reservation initiated for sendEmailItinerary. " + pnrModesDTO.toString());
		}
		AirproxyItineraryBL.sendEmailItinerary(searchReservationByPNR(pnrModesDTO, null, trackInfo), commonItineraryParam,
				trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkForDuplicates(DuplicateValidatorAssembler dva, UserPrincipal userPrincipal , BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyDuplicateCheckBL.checkForDuplicates(dva , this.getUserPrincipal() , trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<LCCClientReservationInsurance> resellInsurance(SYSTEM system, String pnr, List<Integer> failedInsuranceReferences) throws ModuleException {
		return AirproxyLoadReservationBL.resellInsurance(system, pnr, failedInsuranceReferences);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reconcileDummyCarrierReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyModuleUtils.getLCCReservationBD().reconcileDummyCarrierReservation(pnrModesDTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTimeStamp) throws ModuleException {
		AirproxyModuleUtils.getReservationBD().updateReleaseTimeForDummyReservation(pnr, releaseTimeStamp);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Collection<ChargeAdjustmentTypeDTO>> getCarrierWiseChargeAdjustmentTypes(Set<String> carrierCodes,
			BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyModuleUtils.getLCCReservationBD().getCarrierWiseChargeAdjustmentTypes(carrierCodes, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce rollForwardOnholdBooking(String pnr, Collection<LCCClientReservationSegment> colsegs,
			Integer adultCount, Integer childCount, Integer infantCount, Map<String, RollForwardFlightRQ> rollForwardingFlights,
			SYSTEM searchSystem, TrackInfoDTO trackInfo, boolean isDuplicateNameSkip, boolean isOverbok) throws ModuleException {
		AirproxyOnholdRollForwardBL ohdRollForwardBL = new AirproxyOnholdRollForwardBL();
		ohdRollForwardBL.setPnr(pnr);
		ohdRollForwardBL.setColsegs(colsegs);
		ohdRollForwardBL.setRollForwardingFlights(rollForwardingFlights);
		ohdRollForwardBL.setAdultCount(adultCount);
		ohdRollForwardBL.setChildCount(childCount);
		ohdRollForwardBL.setInfantCount(infantCount);
		ohdRollForwardBL.setSearchSystem(searchSystem);
		ohdRollForwardBL.setTrackInfo(trackInfo);
		ohdRollForwardBL.setUserPrincipal(getUserPrincipal());
		ohdRollForwardBL.setDuplicateNameSkip(isDuplicateNameSkip);
		ohdRollForwardBL.setOverBook(isOverbok);

		return ohdRollForwardBL.execute();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.addUserNote(userNoteTO, trackInfo);
	}

	/************************************************************************************************************************************
	 * SEGMENT SECTION *
	 ************************************************************************************************************************************/

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.cancelSegments(lccClientResAlterModesTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearSegmentAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, BasicTrackInfo trackInfo)
			throws ModuleException {
		AirproxyLoadReservationBL.clearAlerts(lccClientClearAlertDTO, this.getUserPrincipal(), trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterQueryModesTO, boolean enableFraudCheck,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.modifySegments(lccClientResAlterQueryModesTO, getUserPrincipal(clientInfoDTO),
				enableFraudCheck, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation requoteModifySegmentsWithAutoRefundForIBE(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return AirproxyLoadReservationBL.requoteModifySegmentsWithAutoRefundForIBE(requoteModifyRQ, trackInfoDTO);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce requoteModifySegmentsWithAutoRefund(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		return AirproxyLoadReservationBL.requoteModifySegmentsWithAutoRefund(requoteModifyRQ, trackInfoDTO);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LCCClientReservation addSegment(LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			ClientCommonInfoDTO clientInfoDTO, boolean enableFraudCheck, TrackInfoDTO trackInfo) throws Exception {
		return AirproxyLoadReservationBL.addSegment(lccClientResAlterQueryModesTO, getUserPrincipal(clientInfoDTO),
				enableFraudCheck, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List[] getGroundSegment(GroundSegmentTO groundSegmentTO, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyLoadReservationBL.getGroundSegment(groundSegmentTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void transferSegments(LCCClientTransferSegmentTO clientTransferSegmentTO, BasicTrackInfo trackInfo, String salesChannelKey)
			throws ModuleException {
		AirproxyLoadReservationBL.transferSegments(clientTransferSegmentTO, trackInfo, salesChannelKey);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getRecieptPrint(RecieptLayoutModesDTO recieptLayoutModesDTO, Collection colSelectionPnrPaxIds,
			boolean isCheckIndividual, TrackInfoDTO trackInfoDTO) throws ModuleException {
		return AirproxyModuleUtils.getReservationBD().getRecieptForPrint(recieptLayoutModesDTO, colSelectionPnrPaxIds,
				trackInfoDTO, isCheckIndividual);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Pair<String, Integer>, Set<ETicketInfoTO>> getETicketInformation(List<String> lccUniqueIDs, List<Integer> txnIDs,
			BasicTrackInfo trackInfo) throws ModuleException {
		return AirProxyETicketInfoBL.loadETicketInfo(lccUniqueIDs, txnIDs, trackInfo);
	}

	/************************************************************************************************************************************
	 * ALERTING SECTION *
	 ************************************************************************************************************************************/
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyAlertingBL.retrieveAlertsForSearchCriteria(searchCriteria, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clearAlerts(Map<String, Collection<Integer>> alertIDMap, BasicTrackInfo trackInfo) throws ModuleException {
		AirproxyAlertingBL.clearAlerts(alertIDMap, trackInfo);
	}
	
	@Override
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException{

		return AirproxyAlertingBL.retrieveSubmittedRequests(searchCriteria);
		
	}
	
	@Override
	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException{
		
		AirproxyAlertingBL.submitOrEditRequest(queueRequest);
		
	}
	
	@Override
	public QueueRequest getQueueRequest(int requestId) throws ModuleException{
		
		return AirproxyAlertingBL.getQueueRequest(requestId);
		
	}
	
	@Override
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException{
		
		AirproxyAlertingBL.addRequestHistory(requestHistory);
		
	}
	
	@Override
	public void deleteRequest(int requestId) throws ModuleException{
		
		AirproxyAlertingBL.deleteRequest(requestId);
		
	}
	
	public Page getRequestHistory(int requestId) throws ModuleException{
		
		return AirproxyAlertingBL.getRequestHistory(requestId);
		
	}
	
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException{
		
		return AirproxyAlertingBL.retrieveSubmittedRequestsForAction(userID , queueId , status);
		
	}
	
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException{
		
		return AirproxyAlertingBL.getRequestStatusForPnr(page,maxResult,pnrId);
		
	}
	
	public Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException{
		
		return AirproxyAlertingBL.getHistoryForRequest(page,maxResult,requestId);
		
	}

	/************************************************************************************************************************************
	 * Flight Services SECTION *
	 ************************************************************************************************************************************/
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchFlightsForDisplay(FlightManifestSearchDTO searchCriteria, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyFlightServiceBL.searchFlightsForDisplay(searchCriteria, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String viewFlightManifest(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws Exception {
		return AirproxyFlightServiceBL.viewFlightManifest(flightManifestOptionsDTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws Exception {
		return AirproxyFlightServiceBL.sendFlightManifestAsEmail(flightManifestOptionsDTO, trackInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FlightLoadReportDataDTO> getFlightLoadReportData(ReportsSearchCriteria search, BasicTrackInfo trackInfo)
			throws ModuleException {
		return AirproxyFlightServiceBL.getFlightLoadData(search, trackInfo);
	}

	/************************************************************************************************************************************
	 * COMMON SECTION *
	 ************************************************************************************************************************************/

	private UserPrincipal getUserPrincipal(ClientCommonInfoDTO clientInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal;
		if (this.getUserPrincipal() != null) {
			userPrincipal = this.getUserPrincipal();
		} else {
			userPrincipal = new UserPrincipal();
			// setting the default carrier to system default carrier only if the userPrinciple is null
			// Otherwise we need to keep the default carrier as the used's default carrier for dry user
			// functions to work correctly
			if (clientInfoDTO != null) {
				userPrincipal.setDefaultCarrierCode(clientInfoDTO.getCarrierCode());
			}
		}

		if (clientInfoDTO != null) {
			userPrincipal.setIpAddress(clientInfoDTO.getIpAddress());
		}
		return userPrincipal;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ScheduleSearchRS getSchedulesList(ScheduleSearchRQ scheduleSearchRQ) throws ModuleException {
		return AirproxyScheduleAvailabilityBL.getFlightSchedules(scheduleSearchRQ);
	}

	@Override
	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo, boolean allowExceptions)
			throws ModuleException {
		AirProxyETicketInfoBL.updatePassengerCoupon(paxCouponUpdateRQ, trackInfo, allowExceptions);

	}
	
	@Override
	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions, boolean isGroupPnr) throws ModuleException {

		AirProxyETicketInfoBL.updateGroupPassengerCoupon(groupPaxCouponUpdateRQ, trackInfo, allowExceptions, isGroupPnr);
	}

	public Integer getAppliedFareDiscountPercentage(String pnr, boolean isGropPNR) throws ModuleException {
		return AirproxyLoadReservationBL.getAppliedFareDiscountPercentage(pnr, isGropPNR);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resAlterQueryModesTO,
			TrackInfoDTO trackInfoDTO) throws ModuleException {
		return AirproxyLoadReservationBL.calculateBalanceToPayAfterAutoCancellation(resAlterQueryModesTO, trackInfoDTO);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean groupPassengerRefund(CommonReservationAssembler reservationAssembler, String userNotes, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			boolean removeAgentCommission, boolean isManualRefund) throws ModuleException {
		return AirproxyLoadReservationBL.groupPassengerRefund(reservationAssembler, userNotes, isGroupPnr, trackInfoDTO,
				preferredRefundOrder, pnrPaxOndChgIds, removeAgentCommission, isManualRefund);
	}

	@Override
	public AnciAvailabilityRS getTaxApplicabilityForAncillaries(String carrierCode, String segmentCode, EXTERNAL_CHARGES extChg,
			BasicTrackInfo trackInfo) throws ModuleException {
		AirproxyAncillaryAvailabilityBL anciAvailBL = new AirproxyAncillaryAvailabilityBL(null, this.getUserPrincipal(),
				trackInfo);
		return anciAvailBL.execute(carrierCode, segmentCode, extChg);
	}

	@Override
	public ApplicablePromotionDetailsTO pickApplicablePromotions(PromoSelectionCriteria promoSelectionCriteria,
			SYSTEM prefSystem, BasicTrackInfo trackInfo) throws ModuleException {
		return AirproxyPromotionsBL.pickApplicablePromotions(promoSelectionCriteria, prefSystem, trackInfo,
				this.getUserPrincipal());
	}
	
	public void exchangeSegment(String pnr, boolean isGroupPnr, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.exchangeSegments(isGroupPnr, pnr, trackInfo);
	}

	@Override
	public Map<Integer, Map<String, Map<String, BigDecimal>>> getPaxProduDueAmount(String pnr, boolean isGroupPnr,
			boolean isLoadLCCRes, double remainingLoyaltyPoints)
			throws ModuleException {
		AirproxyLoyaltyManagementBL loyaltyManagementBL = new AirproxyLoyaltyManagementBL();
		return loyaltyManagementBL.getPaxProductWiseDueAmount(pnr, isGroupPnr, isLoadLCCRes, remainingLoyaltyPoints);
	}

	@Override
	public ServiceResponce calculateLoyaltyPointRedeemables(RedeemCalculateRQ redeemCalculateRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		AirproxyLoyaltyManagementBL loyaltyManagementBL = new AirproxyLoyaltyManagementBL();
		loyaltyManagementBL.setMemberAccountId(redeemCalculateRQ.getMemberAccountID());
		loyaltyManagementBL.setRedeemRequestedAmount(redeemCalculateRQ.getRedeemRequestAmount());
		loyaltyManagementBL.setPaxCarrierExternalCharges(redeemCalculateRQ.getPaxCarrierExternalCharges());
		loyaltyManagementBL.setPaxCarrierProductDueAmount(redeemCalculateRQ.getPaxCarrierProductDueAmount());
		loyaltyManagementBL.setFareSegChargeTo(redeemCalculateRQ.getFareSegChargeTo());
		loyaltyManagementBL.setFlexiQuote(redeemCalculateRQ.isFlexiQuote());
		loyaltyManagementBL.setFlightPriceRQ(redeemCalculateRQ.getFlightPriceRQ());
		loyaltyManagementBL.setCarrierWiseFlightRPH(redeemCalculateRQ.getCarrierWiseFlightRPH());
		loyaltyManagementBL.setSystem(redeemCalculateRQ.getSystem());
		loyaltyManagementBL.setTxnIdntifier(redeemCalculateRQ.getTxnIdntifier());
		loyaltyManagementBL.setIssueRewardIds(redeemCalculateRQ.isIssueRewardIds());
		loyaltyManagementBL.setPayForOHD(redeemCalculateRQ.isPayForOHD());
		loyaltyManagementBL.setPnr(redeemCalculateRQ.getPnr());
		loyaltyManagementBL.setDiscountRQ(redeemCalculateRQ.getDiscountRQ());
		loyaltyManagementBL.setRemainingPoint(redeemCalculateRQ.getRemainingPoint());
		loyaltyManagementBL.setMemberEnrollingCarrier(redeemCalculateRQ.getMemberEnrollingCarrier());
		loyaltyManagementBL.setMemberExternalId(redeemCalculateRQ.getMemberExternalId());
		return loyaltyManagementBL.calculateLoyaltyPointRedeemables(this.getUserPrincipal(), trackInfoDTO);
	}

	@Override
	public ServiceResponce issueLoyaltyRewards(String pnr, String memberAccountId, Map<String, Double> productPoints, SYSTEM selectedSystem,
			AppIndicatorEnum appIndicator, String memberEnrollingCarrier, String memberExternalId) throws ModuleException {
		AirproxyLoyaltyManagementBL airproxyLoyaltyManagementBL = new AirproxyLoyaltyManagementBL();
		airproxyLoyaltyManagementBL.setSystem(selectedSystem);
		airproxyLoyaltyManagementBL.setMemberAccountId(memberAccountId);
		airproxyLoyaltyManagementBL.setPnr(pnr);
		airproxyLoyaltyManagementBL.setMemberEnrollingCarrier(memberEnrollingCarrier);
		airproxyLoyaltyManagementBL.setMemberExternalId(memberExternalId);
		return airproxyLoyaltyManagementBL.issueLoyaltyRewards(productPoints, appIndicator);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce updatePaxNames(Map<Integer, NameDTO> changedPaxNamesMap, String pnr, long version,
			String appIndicator, boolean skipDuplicateNameCheck, TrackInfoDTO trackInfoDTO, boolean isGroupPNR, String reasonForAllowBlPax) throws ModuleException {		
		return AirproxyLoadReservationBL.updatePaxNames(changedPaxNamesMap,  pnr,  version,
				 appIndicator,  skipDuplicateNameCheck,  trackInfoDTO, isGroupPNR, reasonForAllowBlPax);
	}

	@Override
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<LCCClientReservationPax> paxList,SYSTEM system, boolean getOnlyFirstElement,List<String> participatingOperatingCarriers,TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		if(SYSTEM.AA == system){			
			return AirproxyModuleUtils.getBlacklisPAXBD().getBalcklistedPaxReservation(BlacklistPAXUtil.createPaxTOList(paxList), getOnlyFirstElement);
		}else{
			return AirproxyModuleUtils.getLCCReservationBD().getBalcklistedPaxReservation(paxList, getOnlyFirstElement,participatingOperatingCarriers,trackInfoDTO);
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LCCMealDTO> getMealsForPreference(String selectedLanguage) throws ModuleException {
		AirproxyMealBL mealBL = new AirproxyMealBL(null, null, null, null, null,
				selectedLanguage, null, getUserPrincipal());
		return mealBL.getMealsForPreference();
	}
	
	@Override
	public ReservationDiscountDTO calculatePromotionDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		if (promotionRQ.getSystem() == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCReservationBD().calculateDiscount(promotionRQ, trackInfoDTO);
		} else {
			return AirproxyModuleUtils.getReservationBD().calculateDiscount(promotionRQ, trackInfoDTO);
		}

	}

	public LCCAncillaryQuotation getSelectedAncillaryDetails(LCCAncillaryQuotation selectedAncillary, Map<String, BundledFareDTO> segmentBundledFareMap, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException {

        LCCAncillaryQuotation lccAncillaryQuotation;

        // TODO -- this is duplicated code needs refactor
        if (selectedAncillary.getSystem() == SYSTEM.AA) {
            AASelectedAncillaryRQ aaSelectedAncillaryRQ;

            LCCSelectedAncillaryRQ selectedAncillaryRQ = LccAncillaryUtils.populateLCCSelectedAncillaryRQ(selectedAncillary, userPrincipal, trackInfo);
            try {
                aaSelectedAncillaryRQ = LccAdaptor.toAASelectedAncillaryRQ(selectedAncillaryRQ);
                aaSelectedAncillaryRQ.setPnr(selectedAncillary.getPnr());
                aaSelectedAncillaryRQ.setModifyAnci(selectedAncillary.isModifyAnci());
            } catch (Exception e) {
                throw new ModuleException("");
            }
            AASelectedAncillaryRS aaSelectedAncillaryRS = AirproxyModuleUtils.getFlightInventoryResBD().getSelectedAncillaryDetails(aaSelectedAncillaryRQ, segmentBundledFareMap, trackInfo);
            LCCSelectedAncillaryRS lccSelectedAncillaryRS = LccAdaptor.toLccSelectedAncillaryRS(aaSelectedAncillaryRS);

            lccAncillaryQuotation = LccAncillaryUtils.populateLCCSelectedAncillaryRS(lccSelectedAncillaryRS);

        } else {
            lccAncillaryQuotation = AirproxyModuleUtils.getLCCAncillaryBD().getSelectedAncillaryDetails(selectedAncillary, userPrincipal, trackInfo);
        }

        return lccAncillaryQuotation;

	}
	
	public void noShowTaxChargesReversal(String pnr, boolean isGroupPnr, Collection<Integer> pnrSegIds,
			String version, List<LCCClientChargeReverse> chargeReverseList, ClientCommonInfoDTO clientInfoDto, TrackInfoDTO trackInfo) throws ModuleException {
		AirproxyLoadReservationBL.noShowTaxChargesReversal(pnr, isGroupPnr, version, chargeReverseList, clientInfoDto, trackInfo);
	}

	public BaggageRatesDTO getBaggageRates(TrackInfoDTO trackInfoDTO, BaseAvailRS flightPriceRS,
			String cabinClass, String logicalCabinClass, String selectedSystem, ApplicationEngine appEngine,
			String locale) throws ModuleException {
		if (AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed()) {
			BaggageRatesBL baggageRatesBL = new BaggageRatesBL();
			baggageRatesBL.setCabinClass(cabinClass);
			baggageRatesBL.setFlightPriceRS(flightPriceRS);
			baggageRatesBL.setLogicalCabinClass(logicalCabinClass);
			baggageRatesBL.setSelectedSystem(selectedSystem);
			baggageRatesBL.setTrackInfoDTO(trackInfoDTO);
			baggageRatesBL.setUserPrincipal(getUserPrincipal());
			baggageRatesBL.setApplicationEngine(appEngine);
			baggageRatesBL.setLocale(locale);

			return baggageRatesBL.execute();
		}
		return null;
	}

	/************************************************************************************************************************************
	 * END OF ALL IMPLEMENTATION CODE BLOCKS -- NOTHING SHOULD BE BELOW THIS *
	 ************************************************************************************************************************************/
}
