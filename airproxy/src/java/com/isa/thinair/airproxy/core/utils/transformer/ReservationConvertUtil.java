package com.isa.thinair.airproxy.core.utils.transformer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airproxy.api.dto.PAXSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ResBalancesSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationPreferrenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOtherAirlineSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationAdditionalPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationOwnerAgentContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPaxNamesTranslation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PaymentSummaryTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.NameChangeUtil;
import com.isa.thinair.airproxy.api.utils.PNRModificationAuthorizationUtils;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.airreservation.api.model.PaxNameTranslations;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCreditPromotion;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.CHAEGE_CODES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airschedules.api.dto.ApplicableBufferTimeDTO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.LCCUtils;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyTo;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.FareTypeCodes;

public class ReservationConvertUtil {
	/**
	 * Transforms Reservation to LccClientReservation For display Reservation -----> LCCClientReservation
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static LCCClientReservation transform(Reservation reservation, AirportBD airportBD, LCCClientPnrModesDTO pnrModesDTO)
			throws ModuleException {
		LCCClientReservation lccReservation = new LCCClientReservation();

		// First Set the common fields for Both no conversions needed here
		lccReservation.setPNR(reservation.getPnr());
		lccReservation.setBookingCategory(reservation.getBookingCategory());
		lccReservation.setOriginCountryOfCall(reservation.getOriginCountryOfCall());
		lccReservation.setItineraryFareMask(reservation.getItineraryFareMaskFlag());
		String originatorPnr = BeanUtils.nullHandler(reservation.getOriginatorPnr());
		lccReservation.setGroupPNR(!originatorPnr.isEmpty());
		lccReservation.setLastUserNote(reservation.getUserNote());

		// void reservation should set reservation status as void
		if (reservation.isVoidReservation()
				&& ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			lccReservation.setStatus(ReservationInternalConstants.ReservationStatus.VOID);
			lccReservation.setDisplayStatus(ReservationInternalConstants.ReservationStatus.VOID);
		} else {
			lccReservation.setStatus(reservation.getStatus());
			lccReservation.setDisplayStatus(reservation.getStatus());
		}

		lccReservation.setTotalAvailableBalance(reservation.getTotalAvailableBalance());
		lccReservation.setTotalPaidAmount(reservation.getTotalPaidAmount());
		lccReservation.setTotalPaxAdultCount(reservation.getTotalPaxAdultCount());
		lccReservation.setTotalPaxChildCount(reservation.getTotalPaxChildCount());
		lccReservation.setTotalPaxInfantCount(reservation.getTotalPaxInfantCount());
		lccReservation.setTotalTicketAdjustmentCharge(reservation.getTotalTicketAdjustmentCharge());
		lccReservation.setTotalTicketAmounts(reservation.getTotalChargeAmounts());
		lccReservation.setTotalTicketCancelCharge(reservation.getTotalTicketCancelCharge());
		lccReservation.setTotalTicketFare(reservation.getTotalTicketFare());
		lccReservation.setTotalTicketModificationCharge(reservation.getTotalTicketModificationCharge());
		lccReservation.setTotalTicketSurCharge(reservation.getTotalTicketSurCharge());
		lccReservation.setTotalTicketTaxCharge(reservation.getTotalTicketTaxCharge());
		lccReservation.setVersion(new Long(reservation.getVersion()).toString());
		lccReservation.setZuluBookingTimestamp(reservation.getZuluBookingTimestamp());
		lccReservation.setZuluReleaseTimeStamp(reservation.getReleaseTimeStamps()[0]);
		lccReservation.setTotalTicketPrice(reservation.getTotalChargeAmount());
		lccReservation.setLastCurrencyCode(reservation.getLastCurrencyCode());
		lccReservation.setLastModificationTimestamp(reservation.getLastModificationTimestamp());
		lccReservation.setTotalDiscount(reservation.getTotalDiscount());
		lccReservation.setGdsId(reservation.getGdsId());
		lccReservation.setNameChangeCount(reservation.getNameChangeCount());
		lccReservation.setTotalGOQUOAmount(reservation.getTotalGoquoAmount());
		lccReservation.setInfantPaymentSeparated(reservation.isInfantPaymentRecordedWithInfant());

		// Setting the maximum ticket validity of the segment as reservation
		// ticket validity
		if (AppSysParamsUtil.isTicketValidityEnabled()) {
			Date furthestValidity = null;
			for (ReservationSegment resSeg : reservation.getSegments()) {
				Date segTktValidity = resSeg.getTicketValidTill();
				if (ReservationSegmentStatus.CONFIRMED.equals(resSeg.getStatus()) && segTktValidity != null
						&& (furthestValidity == null || furthestValidity.before(segTktValidity))) {
					furthestValidity = segTktValidity;
				}
			}
			lccReservation.setTicketValidTill(furthestValidity);
			// lccReservation.setTicketValidFrom(reservation.getMinimumStayOver());
		}
		// Setting the carrier wise details for old flow we can assume its
		// default carrier

		String strCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		String strCarrierDesc = AppSysParamsUtil.getCarrierDesc(strCarrierCode);
		lccReservation.setMarketingCarrier(strCarrierCode);

		Map<String, String> mapCarier = new HashMap<String, String>();
		mapCarier.put(strCarrierCode, strCarrierDesc);
		lccReservation.setCarrierName(mapCarier);

		Map<String, String> mapPnr = new HashMap<String, String>();
		mapPnr.put(strCarrierCode, reservation.getPnr());
		lccReservation.setCarrierPNR(mapPnr);

		lccReservation.setModifiableReservation(
				reservation.getModifiableReservation() == ReservationInternalConstants.Modifiable.YES ? true : false);

		// setting Contact Info
		lccReservation.setContactInfo(ReservationConvertUtil.transformContactInfo(reservation.getContactInfo()));

		// setting Admin Info
		lccReservation.setAdminInfo(ReservationConvertUtil.trnasformAdminInfo(reservation.getAdminInfo()));

		// setting Preferences Info
		lccReservation.setPreferrenceInfo((ReservationConvertUtil.transformPreferenceInfo(reservation)));

		// Setting insurance Info
		lccReservation.setReservationInsurances(ReservationConvertUtil.transformInsurance(reservation.getReservationInsurance()));

		List<LCCClientAlertInfoTO> listAlerts = new ArrayList<LCCClientAlertInfoTO>();

		// Fill flexi info list
		List<LCCClientAlertInfoTO> flexiAlertInfoList = new ArrayList<LCCClientAlertInfoTO>();

		/** Hold Alerts for Bus service */
		List<LCCClientAlertInfoTO> segmentAlertList = new ArrayList<LCCClientAlertInfoTO>();

		/** Hold Alerts for open return segment */
		List<LCCClientAlertInfoTO> openRetSegmentAlertList = new ArrayList<LCCClientAlertInfoTO>();

		lccReservation.setAutoCancellationInfo(reservation.getAutoCancellationInfo());

		Collection<ReservationSegmentDTO> setSegs = reservation.getSegmentsView();

		Map<Integer, ReservationSegmentDTO> pnrSegMap = new HashMap<Integer, ReservationSegmentDTO>();
		if (setSegs != null) {
			for (ReservationSegmentDTO segDTO : setSegs) {
				pnrSegMap.put(segDTO.getPnrSegId(), segDTO);
			}
		}
		// Add the passenger
		Set<ReservationPax> setPax = reservation.getPassengers();

		List<LCCClientCarrierOndGroup> carrierOndGrouping = createCarrierONDGrouping(setPax);
		lccReservation.setCarrierOndGrouping(carrierOndGrouping);

		Collection colPnrPaxIds = new ArrayList();
		for (ReservationPax reservationPax : setPax) {
			if (reservation.isInfantPaymentRecordedWithInfant()) {
				colPnrPaxIds.add(reservationPax.getPnrPaxId());
			}else{
				if(!ReservationApiUtils.isInfantType(reservationPax)){
					colPnrPaxIds.add(reservationPax.getPnrPaxId());
				}
			}
		}

		Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReservationTnx = AirproxyModuleUtils.getPassengerBD()
				.getPNRPaxPaymentsAndRefunds(colPnrPaxIds);
		Map<Integer, Collection<CreditInfoDTO>> paxCredits = AirproxyModuleUtils.getPassengerBD().getAllCredits(colPnrPaxIds);

		Map<Integer, Collection<PaxSeatTO>> paxSeatMap = new HashMap<Integer, Collection<PaxSeatTO>>();
		Map<Integer, Collection<PaxMealTO>> paxMealMap = new HashMap<Integer, Collection<PaxMealTO>>();
		Map<Integer, Collection<PaxAirportTransferTO>> paxAirportTransferMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();

		// baggage

		Map<Integer, Collection<PaxBaggageTO>> paxBaggageMap = new HashMap<Integer, Collection<PaxBaggageTO>>();

		// Autocheckin
		Map<Integer, Collection<PaxAutomaticCheckinTO>> paxAutoCheckinMap = new HashMap<Integer, Collection<PaxAutomaticCheckinTO>>();

		if (reservation.getSeats() != null) {
			for (PaxSeatTO paxSeat : reservation.getSeats()) {
				if (paxSeatMap.get(paxSeat.getPnrPaxId()) == null) {
					paxSeatMap.put(paxSeat.getPnrPaxId(), new ArrayList<PaxSeatTO>());
				}
				paxSeatMap.get(paxSeat.getPnrPaxId()).add(paxSeat);
			}
		}

		if (reservation.getAirportTransfers() != null) {
			for (PaxAirportTransferTO paxAirportTransfer : reservation.getAirportTransfers()) {
				if (paxAirportTransferMap.get(paxAirportTransfer.getPnrPaxId()) == null) {
					paxAirportTransferMap.put(paxAirportTransfer.getPnrPaxId(), new ArrayList<PaxAirportTransferTO>());
				}
				paxAirportTransferMap.get(paxAirportTransfer.getPnrPaxId()).add(paxAirportTransfer);
			}
		}
		// Autocheckin
		if (reservation.getAutoCheckins() != null) {
			for (PaxAutomaticCheckinTO paxAutoCheckin : reservation.getAutoCheckins()) {
				if (paxAutoCheckinMap.get(paxAutoCheckin.getPnrPaxId()) == null) {
					paxAutoCheckinMap.put(paxAutoCheckin.getPnrPaxId(), new ArrayList<PaxAutomaticCheckinTO>());
				}
				paxAutoCheckinMap.get(paxAutoCheckin.getPnrPaxId()).add(paxAutoCheckin);
			}
		}

		if (reservation.getMeals() != null) {
			for (PaxMealTO paxMeal : reservation.getMeals()) {

				if (paxMealMap.get(paxMeal.getPnrPaxId()) == null) {
					paxMealMap.put(paxMeal.getPnrPaxId(), new ArrayList<PaxMealTO>());
				}
				// Jira 4640 - Translated Meal Name
				if (!(paxMeal.getTranslatedMealName() == null)
						&& !(StringUtil.getUnicode(paxMeal.getTranslatedMealName()).equalsIgnoreCase("null"))) {
					String ucs = "";
					ucs = StringUtil.getUnicode(paxMeal.getTranslatedMealName());
					if (ucs != null) {
						ucs = ucs.replace("^", ",");
					}
					paxMeal.setMealName(ucs);
				}
				paxMealMap.get(paxMeal.getPnrPaxId()).add(paxMeal);
			}
		}

		// bagagge

		if (reservation.getBaggages() != null) {
			for (PaxBaggageTO paxBaggage : reservation.getBaggages()) {
				if (paxBaggageMap.get(paxBaggage.getPnrPaxId()) == null) {
					paxBaggageMap.put(paxBaggage.getPnrPaxId(), new ArrayList<PaxBaggageTO>());
				}

				paxBaggageMap.get(paxBaggage.getPnrPaxId()).add(paxBaggage);
			}
		}
		// end baggage changes
		Map<String, Collection<RefundableChargeDetailDTO>> lccRefundableChargeDetails = new HashMap<String, Collection<RefundableChargeDetailDTO>>();
		
		Collection<Integer> blacklistedPnrPaxId = ReservationModuleUtils.getBlacklisPAXBD().getBLackListPaxIdVsPnrPaxId(reservation.getPnr());
		
		for (ReservationPax reservationPax : setPax) {
			lccReservation.addPassenger(ReservationConvertUtil.transformPassenger(reservationPax, pnrPaxIdAndColReservationTnx,
					paxCredits, paxMealMap.get(reservationPax.getPnrPaxId()), 
					/**
					 * Muhammad Naeem Akhtar(NA) 20-OCT-2010 Passing TRUE is LoadFarePayment filed to get load payment
					 * detail with passenger These Payment Detail is need to display on Itinerary AARESAA-5047
					 */
					paxSeatMap.get(reservationPax.getPnrPaxId()), paxBaggageMap.get(reservationPax.getPnrPaxId()),
					paxAirportTransferMap.get(reservationPax.getPnrPaxId()), paxAutoCheckinMap.get(reservationPax.getPnrPaxId()),
					pnrSegMap, false, true, reservation.isInfantPaymentRecordedWithInfant(), blacklistedPnrPaxId));

			// If refundable charge details are loaded set them.
			if (reservation.getRefundableChargeDetails() != null) {
				String travellerRefNo = strCarrierCode + "|" + PaxTypeUtils.travelerReference(reservationPax);
				lccRefundableChargeDetails.put(travellerRefNo,
						reservation.getRefundableChargeDetails().get(reservationPax.getPnrPaxId()));
			}

		}
		lccReservation.setRefundableChargeDetails(lccRefundableChargeDetails);
		// Set External Charges Summary
		lccReservation.setExternalChargersSummary(createExternalChargesSummary(setPax));
		// Removing reservation setting from pax no need of reservation for
		// display purposes.
		// if needed remove the following block

		for (LCCClientReservationPax lccReservationPax : lccReservation.getPassengers()) {
			lccReservationPax.setLccReservation(null);
		}

		Map mapAlerts = AirproxyModuleUtils.getAlertingBD().getAlertsForPnrSegments(pnrSegMap.keySet());

		Map<String, CachedAirportDTO> airports = getCachedAirports(setSegs, airportBD);

		Map<Integer, Collection<Integer>> paxWiseFlownSegMap = CancellationUtils.getPaxWiseFlownPnrSegments(reservation);
		Map<Integer, Boolean> segFlownStatusMap = getSegmentFlownStatusMap(paxWiseFlownSegMap, setSegs);

		Map<Integer, List<Integer>> bundledFareJourneySeqIds = new HashMap<Integer, List<Integer>>();
		List<Integer> journeyWithoutBundleFare = new ArrayList<Integer>();

		ReservationSegmentDTO[] sortedReservationSegmentsArray = SortUtil.sortByDepDate(setSegs);
		setSegs.clear();
		Collections.addAll(setSegs, sortedReservationSegmentsArray);
		int ondsequence = 0;
		Map<Integer, String> ondSeqWithOndCodeMap = new HashMap<Integer, String>();
		for (ReservationSegmentDTO segDto : setSegs) {
			String segmentCode = segDto.getSegmentCode();
			String arrivalAirportCode = segmentCode.substring(segmentCode.length() - 3);
			String departureAirportCode = segmentCode.substring(0, 3);
			boolean isFlownSeg = segFlownStatusMap.get(segDto.getPnrSegId());
			boolean segmentModifiableAsPerETicketStatus  = PNRModificationAuthorizationUtils.isSegmentModifiableAsPerETicketStatus(reservation,segDto.getPnrSegId());  

			// Adding the alerts

			if (mapAlerts != null && !mapAlerts.isEmpty()) {
				Collection<Alert> colAlert = (Collection) mapAlerts.get(new Long(segDto.getPnrSegId().longValue()));
				if (colAlert != null && !colAlert.isEmpty()) {
					List<LCCClientAlertTO> alertList = new ArrayList<LCCClientAlertTO>();
					for (Alert alert : colAlert) {
						alertList.add(transform(alert));
					}
					LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
					alertInfo.setAlertTO(alertList);
					alertInfo.setFlightSegmantRefNumber(segDto.getPnrSegId().toString());
					listAlerts.add(alertInfo);
				}
			}

			// Add flexi info
			if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {
				Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = reservation
						.getPaxOndFlexibilities();
				if (!segDto.getStatus().equals("CNX")) {
					List<ReservationPaxOndFlexibilityDTO> segmentFlexiList = segmentFlexiList(ondFlexibilitiesMap,
							segDto.getPnrSegId(), reservation);
					if (segmentFlexiList != null && segmentFlexiList.size() > 0) {
						List<LCCClientAlertTO> flexiAlertList = transform(sortOndFlexi(segmentFlexiList));
						LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
						alertInfo.setAlertTO(flexiAlertList);
						alertInfo.setFlightSegmantRefNumber(segDto.getPnrSegId().toString());
						flexiAlertInfoList.add(alertInfo);
						segDto.setFlexiRuleID(segmentFlexiList.iterator().next().getFlexiRuleID());
					}
				}
			}

			// show common service alert
			if (!segDto.getStatus().equals("CNX") && segDto.getSubStationShortName() != null
					&& !segDto.getSubStationShortName().trim().equals("")) {
				List<LCCClientAlertTO> groundserviceAlertList = new ArrayList();
				LCCClientAlertTO lcc = new LCCClientAlertTO();
				lcc.setAlertId(0);
				String stationName = CommonsServices.getGlobalConfig().getGroundStationNamesMap()
						.get(segDto.getSubStationShortName());
				lcc.setContent(stationName);
				groundserviceAlertList.add(lcc);

				LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
				alertInfo.setAlertTO(groundserviceAlertList);
				alertInfo.setFlightSegmantRefNumber(segDto.getPnrSegId().toString());
				alertInfo.setAlertCategory(LCCClientAlertInfoTO.alertType.GROUND_SERVICE);
				segmentAlertList.add(alertInfo);
			}

			// Show open return alerts
			if (!segDto.getStatus().equals("CNX") && segDto.isOpenReturnSegment()) {
				List<LCCClientAlertTO> openReturnAlertList = new ArrayList();
				LCCClientAlertTO lcc = new LCCClientAlertTO();
				lcc.setAlertId(1);
				String openRetInfo = "Confirm Before : " + segDto.getOpenRtConfirmBefore(("dd-MM-yyyy HH:mm"));// CommonsServices.getGlobalConfig().getGroundStationNamesMap().get(segDto.getSubStationShortName());
				openRetInfo += "<br> Travel Expiry : " + segDto.getSegmentExpiryDate("dd-MM-yyyy HH:mm");
				lcc.setContent(openRetInfo);
				openReturnAlertList.add(lcc);

				LCCClientAlertInfoTO alertInfo = new LCCClientAlertInfoTO();
				alertInfo.setAlertTO(openReturnAlertList);
				alertInfo.setFlightSegmantRefNumber(segDto.getPnrSegId().toString());
				alertInfo.setAlertCategory(LCCClientAlertInfoTO.alertType.OPEN_RETURN);
				openRetSegmentAlertList.add(alertInfo);
			}

			lccReservation.addSegment(ReservationConvertUtil.transformSegment(segDto, reservation, lccReservation,
					airports.get(departureAirportCode), airports.get(arrivalAirportCode), pnrModesDTO, isFlownSeg, airportBD, segmentModifiableAsPerETicketStatus));

			if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(segDto.getStatus())
					&& isNewOndCode(segDto, ondSeqWithOndCodeMap)) {
				boolean isBundleFare = false;
				if (segDto.getBundledFarePeriodId() != null) {
					if (!bundledFareJourneySeqIds.containsKey(segDto.getBundledFarePeriodId())) {
						bundledFareJourneySeqIds.put(segDto.getBundledFarePeriodId(), new ArrayList<Integer>());
					}

					bundledFareJourneySeqIds.get(segDto.getBundledFarePeriodId()).add(ondsequence);
					isBundleFare = true;
				}

				if (!isBundleFare) {
					journeyWithoutBundleFare.add(ondsequence);
				}
				ondsequence++;
			}

		}

		lccReservation.setOthertAirlineSegments(
				ReservationConvertUtil.transformOtherAirlineSegment(reservation.getOtherAirlineSegments()));

		if (!bundledFareJourneySeqIds.isEmpty()) {
			Set<Integer> uniqueBundledFarePeriodIds = new HashSet<Integer>(bundledFareJourneySeqIds.keySet());
			if (uniqueBundledFarePeriodIds != null && !uniqueBundledFarePeriodIds.isEmpty()) {
				List<BundledFareDTO> bundledFareDTOs = AirproxyModuleUtils.getBundledFareBD()
						.getBundledFareDTOsByBundlePeriodIds(uniqueBundledFarePeriodIds);

				if (bundledFareDTOs != null) {

					TreeMap<Integer, BundledFareDTO> ondBundledFareMap = new TreeMap<Integer, BundledFareDTO>();
					List<BundledFareDTO> ondWiseSortedBundledFareDTOs = new ArrayList<BundledFareDTO>();

					Map<Integer, BundledFareDTO> bundledMap = new HashMap<Integer, BundledFareDTO>();
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						bundledMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
					}

					for (Entry<Integer, BundledFareDTO> bundledFareEntry : bundledMap.entrySet()) {
						Integer bundledFarePeriodId = bundledFareEntry.getKey();
						List<Integer> ondSeqs = bundledFareJourneySeqIds.get(bundledFarePeriodId);

						for (Integer ondSeq : ondSeqs) {
							ondBundledFareMap.put(ondSeq, bundledFareEntry.getValue());
						}

					}

					Iterator<Integer> journeyWithoutBundleFareItr = journeyWithoutBundleFare.iterator();
					while (journeyWithoutBundleFareItr.hasNext()) {
						Integer ondSeqId = journeyWithoutBundleFareItr.next();
						ondBundledFareMap.put(ondSeqId, null);
					}

					for (Entry<Integer, BundledFareDTO> entry : ondBundledFareMap.entrySet()) {
						ondWiseSortedBundledFareDTOs.add(entry.getValue());
					}

					lccReservation.setBundledFareDTOs(ondWiseSortedBundledFareDTOs);
				}
			}

		}

		lccReservation.setFlexiAlertInfoTOs(flexiAlertInfoList);
		lccReservation.setAlertInfoTOs(listAlerts);
		lccReservation.setSegmentInfoTOs(segmentAlertList);
		lccReservation.setOpenReturnSegmentInfoTOs(openRetSegmentAlertList);
		Collection<ExternalPnrSegment> extPnrSegments = reservation.getExternalReservationSegment();
		for (ExternalPnrSegment extSeg : extPnrSegments) {
			lccReservation.addSegment(ReservationConvertUtil.transformExternalSegment(extSeg, lccReservation));
		}

		lccReservation.setEndorsementNote(reservation.getEndorsementNote());

		// transform e tickets
		ReservationConvertUtil.transformEtickets(lccReservation.getPassengers(), reservation.getPassengers(), pnrSegMap);
		lccReservation.setAllowVoidReservation(AirproxyModuleUtils.getReservationBD().isAllowVoidReservation(reservation));
		lccReservation.setInfantPaymentSeparated(reservation.isInfantPaymentRecordedWithInfant());

		if (reservation.getPromotionId() != null) {
			lccReservation.setLccPromotionInfoTO(getPromotionInfoTO(reservation));
		}

		lccReservation.setNameChangeCutoffTime(AppSysParamsUtil.getPassenegerNameModificationCutoffTimeInMillis());
		lccReservation.setNameChangeThresholdTimePerStation(
				NameChangeUtil.populateStationWiseNameChangeInfo(AirproxyModuleUtils.getLocationDB(), reservation));

		if (reservation.getGroupBookingRequestID() != null && reservation.getGroupBookingRequestID() > 0) {
			lccReservation.setGroupBookingRequestID(reservation.getGroupBookingRequestID());
		}

		if (reservation.getUseAeroMartETS() != 0) {
			boolean useETS = reservation.getUseAeroMartETS() == ReservationInternalConstants.UseAccelAeroETS.YES ? true : false;
			lccReservation.setUseEtsForReservation(useETS);
		}
		
		if (reservation.getTaxInvoicesList() != null){
			lccReservation.setTaxInvoicesList(reservation.getTaxInvoicesList());
		}
				
		lccReservation.setActionedByIBE(AirproxyModuleUtils.getAirproxyReservationBD().hasActionedByIBE(reservation.getPnr()));
		
		return lccReservation;
	}

	private static LCCPromotionInfoTO getPromotionInfoTO(Reservation reservation) throws ModuleException {
		LCCPromotionInfoTO lccPromotionInfoTO = null;
		PromotionCriteriaTO promotionCriteriaTO = AirproxyModuleUtils.getPromotionCriteriaAdminBD()
				.findPromotionCriteria(reservation.getPromotionId());
		if (promotionCriteriaTO != null) {
			lccPromotionInfoTO = new LCCPromotionInfoTO();
			lccPromotionInfoTO.setPromoCodeId(promotionCriteriaTO.getPromoCriteriaID());
			lccPromotionInfoTO.setPromoType(promotionCriteriaTO.getPromotionCriteriaType());
			lccPromotionInfoTO.setDiscountType(promotionCriteriaTO.getDiscountType());
			lccPromotionInfoTO.setDiscountValue(
					(promotionCriteriaTO.getDiscountValue() != null) ? promotionCriteriaTO.getDiscountValue().floatValue() : 0);
			if (promotionCriteriaTO.getApplyTO() != null && !"".equals(promotionCriteriaTO.getApplyTO())) {
				for (DiscountApplyTo discAppltTo : DiscountApplyTo.values()) {
					if (discAppltTo.ordinal() == Integer.parseInt(promotionCriteriaTO.getApplyTO())) {
						lccPromotionInfoTO.setDiscountApplyTo(discAppltTo.getName());
					}
				}
			}
			lccPromotionInfoTO.setDiscountAs(promotionCriteriaTO.getApplyAs());
			lccPromotionInfoTO.setCancellationAllowed(!promotionCriteriaTO.getRestrictAppliedResCancelation());
			lccPromotionInfoTO.setModificationAllowed(!promotionCriteriaTO.getRestrictAppliedResModification());
			lccPromotionInfoTO.setSplitAllowed(!promotionCriteriaTO.getRestrictAppliedResSplitting());
			lccPromotionInfoTO.setRemovePaxAllowed(!promotionCriteriaTO.getRestrictAppliedResRemovePax());
			if (promotionCriteriaTO.getFreeCriteria() != null) {
				Collection<String> freeCriterias = promotionCriteriaTO.getFreeCriteria().values();
				if (freeCriterias != null) {
					String freeCriteria = freeCriterias.iterator().next();
					String[] paxTypeFree = freeCriteria
							.split(PromotionCriteriaConstants.PromotionCriteriaFormatConstants.BUY_N_GET_QUANTITY_SEPERATOR);

					lccPromotionInfoTO.setAppliedAdults(isALLCombinationForBuyNGetFree(paxTypeFree[0])
							? reservation.getTotalPaxAdultCount()
							: Integer.parseInt(paxTypeFree[0]));
					lccPromotionInfoTO.setAppliedChilds(isALLCombinationForBuyNGetFree(paxTypeFree[1])
							? reservation.getTotalPaxChildCount()
							: Integer.parseInt(paxTypeFree[1]));
					lccPromotionInfoTO.setAppliedInfants(isALLCombinationForBuyNGetFree(paxTypeFree[2])
							? reservation.getTotalPaxInfantCount()
							: Integer.parseInt(paxTypeFree[2]));
				}
			}

			lccPromotionInfoTO.setApplicableBINs(promotionCriteriaTO.getApplicableBINs());
			lccPromotionInfoTO.setAppliedAncis(promotionCriteriaTO.getApplicableAncillaries());
			lccPromotionInfoTO.setSystemGenerated(promotionCriteriaTO.getSystemGenerated());
			lccPromotionInfoTO.setCreditDiscountAmount(getCreditPromotionDiscount(reservation));
		}
		return lccPromotionInfoTO;
	}

	private static boolean isALLCombinationForBuyNGetFree(String count) {
		return count.equals(PromotionCriteriaConstants.BUY_AND_GET_FREE_ALL) ? true : false;
	}

	private static Map<Integer, Boolean> getSegmentFlownStatusMap(Map<Integer, Collection<Integer>> paxWiseFlownSegMap,
			Collection<ReservationSegmentDTO> resSegs) {
		Map<Integer, Boolean> segFlowStatusMap = new HashMap<Integer, Boolean>();
		Collection<Collection<Integer>> colFlownSeg = paxWiseFlownSegMap.values();

		for (ReservationSegmentDTO reservationSegmentDTO : resSegs) {
			boolean isFlown = false;
			for (Collection<Integer> paxFlownList : colFlownSeg) {
				if (paxFlownList != null && !paxFlownList.isEmpty()) {
					if (paxFlownList.contains(reservationSegmentDTO.getPnrSegId())) {
						isFlown = true;
						break;
					}
				}
			}

			segFlowStatusMap.put(reservationSegmentDTO.getPnrSegId(), isFlown);
		}

		return segFlowStatusMap;
	}

	private static Map<String, CachedAirportDTO> getCachedAirports(Collection<ReservationSegmentDTO> setSegs, AirportBD airportBD)
			throws ModuleException {
		Collection<String> lstAirports = new HashSet<String>();

		for (ReservationSegmentDTO segDto : setSegs) {
			String segmentCode = segDto.getSegmentCode();
			String arrivalAirportCode = segmentCode.substring(segmentCode.length() - 3);
			String departureAirportCode = segmentCode.substring(0, 3);

			lstAirports.add(arrivalAirportCode);
			lstAirports.add(departureAirportCode);
		}

		if (lstAirports != null && lstAirports.size() > 0) {
			return airportBD.getCachedOwnAirportMap(lstAirports);
		}

		throw new ModuleException("airreservation.invalid.airports");
	}

	public static LCCClientAlertTO transform(Alert alert) {
		LCCClientAlertTO clientAlert = new LCCClientAlertTO();
		clientAlert.setAlertId(alert.getAlertId().intValue());
		clientAlert.setContent(alert.getContent());
		clientAlert.setIbeVisibleOnly(alert.getVisibleIbeOnly());
		clientAlert.setIbeActioned(alert.getIbeActoined());
		clientAlert.setHasFlightSegments(alert.getFligthSegIds().size() == 0 ? false : true);
		return clientAlert;

	}

	public static LCCClientReservation transform(Reservation reservation, AirportBD airportBD) throws ModuleException {
		return transform(reservation, airportBD, null);
	}

	/**
	 * Transforms LccClientReservation to Reservation For display LCCClientReservation -----> Reservation
	 * 
	 * @param clientReservation
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation transform(LCCClientReservation clientReservation, Reservation reservation) throws ModuleException {
		reservation.setContactInfo(AirProxyReservationUtil.transformContactInfo(clientReservation.getContactInfo()));
		reservation.setVersion(new Long(clientReservation.getVersion()));
		reservation.setStatus(clientReservation.getStatus());
		reservation.setUserNote(clientReservation.getLastUserNote());
		return reservation;
	}

	/**
	 * Transforms the ReservationContactInfo to LCCClientReservationContactInfo
	 * 
	 * @param contactInfo
	 * @return
	 * @throws ModuleException
	 */
	public static CommonReservationContactInfo transformContactInfo(ReservationContactInfo contactInfo) throws ModuleException {
		CommonReservationContactInfo lccContactInfo = new CommonReservationContactInfo();

		// No need to change any thing just set the methods
		lccContactInfo.setCity(contactInfo.getCity());
		lccContactInfo.setCountryCode(contactInfo.getCountryCode());
		lccContactInfo.setZipCode(contactInfo.getZipCode());
		lccContactInfo.setCustomerId(contactInfo.getCustomerId());
		lccContactInfo.setEmail(contactInfo.getEmail());
		lccContactInfo.setFax(contactInfo.getFax());
		lccContactInfo.setFirstName(contactInfo.getFirstName());
		lccContactInfo.setLastName(contactInfo.getLastName());
		lccContactInfo.setMobileNo(contactInfo.getMobileNo());
		if (contactInfo.getNationalityCode() != null && !contactInfo.getNationalityCode().trim().equals("")) {
			lccContactInfo.setNationalityCode(new Integer(contactInfo.getNationalityCode()));
		}
		lccContactInfo.setPhoneNo(contactInfo.getPhoneNo());
		lccContactInfo.setState(contactInfo.getState());
		lccContactInfo.setStreetAddress1(contactInfo.getStreetAddress1());
		lccContactInfo.setStreetAddress2(contactInfo.getStreetAddress2());
		lccContactInfo.setTitle(contactInfo.getTitle());
		lccContactInfo.setPreferredLanguage(contactInfo.getPreferredLanguage());
		lccContactInfo.setTaxRegNo(contactInfo.getTaxRegNo());

		// set emergency contact information
		lccContactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		lccContactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		lccContactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		lccContactInfo.setEmgnPhoneNo(contactInfo.getEmgnPhoneNo());
		lccContactInfo.setEmgnEmail(contactInfo.getEmgnEmail());
		lccContactInfo.setSendPromoEmail(contactInfo.getSendPromoEmail());

		return lccContactInfo;
	}

	/**
	 * Transforms the ReservationAdminInfo to LCCClientReservationAdminInfo
	 * 
	 * @param adminInfo
	 * @return
	 * @throws ModuleException
	 */
	public static CommonReservationAdminInfo trnasformAdminInfo(ReservationAdminInfo adminInfo) throws ModuleException {
		CommonReservationAdminInfo lccAdminInfo = new CommonReservationAdminInfo();

		lccAdminInfo.setLastSalesTerminal(adminInfo.getLastSalesTerminal());
		lccAdminInfo.setOriginAgentCode(adminInfo.getOriginAgentCode());
		if (adminInfo.getOriginChannelId() != null) {
			lccAdminInfo.setOriginChannelId(adminInfo.getOriginChannelId().toString());
		}
		lccAdminInfo.setOriginSalesTerminal(adminInfo.getOriginSalesTerminal());
		lccAdminInfo.setOwnerAgentCode(adminInfo.getOwnerAgentCode());
		lccAdminInfo.setOriginIpAddress(adminInfo.getOriginIPAddress());
		lccAdminInfo.setOriginCountryCode(adminInfo.getOriginCountryCode());
		if (adminInfo.getOwnerChannelId() != null) {
			lccAdminInfo
					.setOwnerChannelId(AppSysParamsUtil.getDefaultCarrierCode() + "|" + adminInfo.getOwnerChannelId().toString());
		}
		// origin carrier code is same as owner carrier code
		lccAdminInfo
				.setOwnerAgentContactInfo(createOwnerAgentInfo(adminInfo.getOwnerAgentCode(), adminInfo.getOriginCarrierCode()));

		return lccAdminInfo;
	}

	/**
	 * Transforms the ReservationPreferenceInfo to LCCClientReservationPreferenceInfo
	 * 
	 * @param adminInfo
	 * @return
	 * @throws ModuleException
	 */
	public static CommonReservationPreferrenceInfo transformPreferenceInfo(Reservation reservation) throws ModuleException {
		CommonReservationPreferrenceInfo lccPreferenceInfo = new CommonReservationPreferrenceInfo();
		// lccPreferenceInfo.setPreferredLanguage(reservation.getPrefferedLanguage());
		lccPreferenceInfo.setPreferredLanguage(reservation.getContactInfo().getPreferredLanguage());

		return lccPreferenceInfo;
	}

	/**
	 * Creates the LCCClientReservationOwnerAgentContactInfo
	 * 
	 * @param strOwnerAgentCode
	 * @param carrierCode
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservationOwnerAgentContactInfo createOwnerAgentInfo(String strOwnerAgentCode, String carrierCode)
			throws ModuleException {
		LCCClientReservationOwnerAgentContactInfo ownerContactInfo = new LCCClientReservationOwnerAgentContactInfo();
		if (strOwnerAgentCode != null && !strOwnerAgentCode.trim().equals("")) {
			Agent agent = AirproxyModuleUtils.getTravelAgentBD().getAgent(strOwnerAgentCode);
			ownerContactInfo.setAgentCarrier(carrierCode);
			ownerContactInfo.setAgentEmail(agent.getEmailId());
			ownerContactInfo.setAgentName(agent.getAgentDisplayName());
			ownerContactInfo.setAgentTelephone(agent.getTelephone());
			ownerContactInfo.setAgentIATANumber(agent.getAgentIATANumber());
		}
		return ownerContactInfo;
	}

	/**
	 * Transforms the ReservationInsurance to LCCClientReservationInsurance
	 * 
	 * @param insurance
	 * @return
	 * @throws ModuleException
	 */
	public static List<LCCClientReservationInsurance> transformInsurance(List<ReservationInsurance> modelInsurances)
			throws ModuleException {

		List<LCCClientReservationInsurance> clientInsurances = new ArrayList<LCCClientReservationInsurance>();
		LCCClientReservationInsurance clientInsurance = null;

		if (modelInsurances != null && !modelInsurances.isEmpty()) {
			Iterator<ReservationInsurance> modelInsuranceIt = modelInsurances.iterator();

			while (modelInsuranceIt.hasNext()) {

				ReservationInsurance modelInsurance = modelInsuranceIt.next();

				clientInsurance = new LCCClientReservationInsurance();
				clientInsurance.setDateOfReturn(modelInsurance.getDateOfReturn());
				clientInsurance.setDateOfTravel(modelInsurance.getDateOfTravel());
				clientInsurance.setDestination(modelInsurance.getDestination());
				clientInsurance.setInsuranceQuoteRefNumber(String.valueOf(modelInsurance.getInsuranceId()));
				clientInsurance.setOperatingAirline(modelInsurance.getMarketingCarrier());
				clientInsurance.setOrigin(modelInsurance.getOrigin());
				clientInsurance.setPolicyCode(modelInsurance.getPolicyCode());
				clientInsurance.setState(modelInsurance.getState());
				clientInsurance.setQuotedTotalPremium(modelInsurance.getQuotedAmount());
				clientInsurance.setQuotedCurrencyCode(modelInsurance.getQuotedCurrencyCode());
				clientInsurance
						.setInsuranceType(modelInsurance.getInsuranceType() != null ? modelInsurance.getInsuranceType() : 0);
				clientInsurance.setAlertAutoCancellation(modelInsurance.getAutoCancellationId() != null);

				// for carrier bookings journey is same as wt's in ins object
				LCCInsuredJourneyDTO lccInsJourneyDTO = new LCCInsuredJourneyDTO();
				lccInsJourneyDTO.setJourneyEndAirportCode(modelInsurance.getDestination());
				lccInsJourneyDTO.setJourneyEndDate(modelInsurance.getDateOfReturn());
				lccInsJourneyDTO.setJourneyStartAirportCode(modelInsurance.getOrigin());
				lccInsJourneyDTO.setRoundTrip((modelInsurance.getRoundTrip().equalsIgnoreCase("Y")));
				clientInsurance.setInsuredJourneyDTO(lccInsJourneyDTO);

				clientInsurances.add(clientInsurance);
			}
		}

		return clientInsurances;
	}

	/**
	 * 
	 * @param reservationPax
	 * @param clientReservation
	 * @param paxMeals
	 * @param paxSeats
	 * @param pnrSegMap
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static LCCClientReservationPax transformPassenger(ReservationPax reservationPax,
			Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReservationTnx,
			Map<Integer, Collection<CreditInfoDTO>> paxCredits, Collection<PaxMealTO> paxMeals, Collection<PaxSeatTO> paxSeats,
			Collection<PaxBaggageTO> paxBaggages, Collection<PaxAirportTransferTO> paxAirportTransfers,
			Collection<PaxAutomaticCheckinTO> paxAutoCheckins,
			Map<Integer, ReservationSegmentDTO> pnrSegMap, boolean reccurCall, boolean loadFarePayment, boolean infantPaymentSeparated, Collection<Integer> blacklistedPnrPaxId) throws ModuleException {
		LCCClientReservationPax clientPax = null;
		ChargesDetailDTO chargesDetailDTO = null;

		if (reservationPax != null) {
			clientPax = new LCCClientReservationPax();
			Integer attachedId = null;
			String strCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			if (!reccurCall) {
				if (reservationPax.getAccompaniedPaxId() != null) {
					attachedId = reservationPax.getAccompaniedPaxId();
					if (reservationPax.getInfants() != null && reservationPax.getInfants().size() > 0) {
						ReservationPax infant = reservationPax.getInfants().iterator().next();
						clientPax.getInfants().add(
								transformPassenger(infant, pnrPaxIdAndColReservationTnx, paxCredits, paxMeals, paxSeats,
										paxBaggages, paxAirportTransfers, paxAutoCheckins, pnrSegMap, true, loadFarePayment,
										infantPaymentSeparated, null));
					}

				} else if (reservationPax.getInfants() != null && reservationPax.getInfants().size() > 0) {
					// one infant for one adult
					ReservationPax infant = reservationPax.getInfants().iterator().next();
					attachedId = infant.getPnrPaxId();
					clientPax.getInfants().add(
							transformPassenger(infant, pnrPaxIdAndColReservationTnx, paxCredits, paxMeals, paxSeats, paxBaggages,
									paxAirportTransfers, paxAutoCheckins, pnrSegMap, true, loadFarePayment,
									infantPaymentSeparated, null));
				}
			}
			Set<ReservationPaxAdditionalInfo> paxAdditionalInfoSet = reservationPax.getPaxAdditionalInfoSet();
			Iterator paxAdditionalInfoItr = paxAdditionalInfoSet.iterator();
			if (paxAdditionalInfoItr.hasNext()) {
				ReservationPaxAdditionalInfo additionlInfo = (ReservationPaxAdditionalInfo) paxAdditionalInfoItr.next();
			}
			clientPax.setAttachedPaxId(attachedId);
			clientPax.setDateOfBirth(reservationPax.getDateOfBirth());

			if (AppSysParamsUtil.isAutoCancellationEnabled() && reservationPax.getAutoCancellationId() != null) {
				clientPax.setAlertAutoCancellation(true);
			}

			ReservationPaxAdditionalInfo paxAddnInfo = (reservationPax.getPaxAdditionalInfo() == null)
					? new ReservationPaxAdditionalInfo()
					: reservationPax.getPaxAdditionalInfo();

			LCCClientReservationAdditionalPax addnInfo = new LCCClientReservationAdditionalPax();
			addnInfo.setPassportNo(paxAddnInfo.getPassportNo());
			addnInfo.setPassportExpiry(paxAddnInfo.getPassportExpiry());
			addnInfo.setPassportIssuedCntry(paxAddnInfo.getPassportIssuedCntry());
			addnInfo.setEmployeeId(paxAddnInfo.getEmployeeID());
			addnInfo.setDateOfJoin(paxAddnInfo.getDateOfJoin());
			addnInfo.setIdCategory(paxAddnInfo.getIdCategory());
			addnInfo.setPlaceOfBirth(paxAddnInfo.getPlaceOfBirth());
			addnInfo.setTravelDocumentType(paxAddnInfo.getTravelDocumentType());
			addnInfo.setVisaDocNumber(paxAddnInfo.getVisaDocNumber());
			addnInfo.setVisaDocPlaceOfIssue(paxAddnInfo.getVisaDocPlaceOfIssue());
			addnInfo.setVisaDocIssueDate(paxAddnInfo.getVisaDocIssueDate());
			addnInfo.setVisaApplicableCountry(paxAddnInfo.getVisaApplicableCountry());
			addnInfo.setFfid(paxAddnInfo.getFfid());
			addnInfo.setNationalIDNo(paxAddnInfo.getNationalIDNo());
			addnInfo.setArrivalIntlFltNo(paxAddnInfo.getArrivalIntlFlightNo());
			addnInfo.setIntlFltArrivalDate(paxAddnInfo.getIntlFlightArrivalDate());
			addnInfo.setDepartureIntlFltNo(paxAddnInfo.getDepartureIntlFlightNo());
			addnInfo.setIntlFltDepartureDate(paxAddnInfo.getIntlFlightDepartureDate());
			addnInfo.setPnrPaxGroupId(paxAddnInfo.getGroupId());

			clientPax.setLccClientAdditionPax(addnInfo);

			PaxNameTranslations paxNameTranslations = reservationPax.getPaxNameTranslation();

			if (paxNameTranslations != null) {
				String firstNameOl = paxNameTranslations.getFirstNameOl();
				String lastNameOl = paxNameTranslations.getLastNameOl();
				String languageCode = paxNameTranslations.getLanguageCode();
				if (firstNameOl != null & lastNameOl != null && languageCode != null) {
					LCCClientReservationPaxNamesTranslation nameTranslations = new LCCClientReservationPaxNamesTranslation();
					nameTranslations.setFirstNameOl(firstNameOl);
					nameTranslations.setLastNameOl(lastNameOl);
					nameTranslations.setLanguageCode(languageCode);
					nameTranslations.setTitleOl(paxNameTranslations.getTitleOl());
					clientPax.setLccClientPaxNameTranslations(nameTranslations);
				}
			}
			clientPax.setPaxCategory(reservationPax.getPaxCategory());

			// setting the charges list
			List<FeeTO> feeList = new ArrayList<FeeTO>();
			List<FareTO> fareList = new ArrayList<FareTO>();
			List<TaxTO> taxList = new ArrayList<TaxTO>();
			List<SurchargeTO> surchargeList = new ArrayList<SurchargeTO>();

			// /__________________________________
			if (loadFarePayment || AppSysParamsUtil.isPrintEmailIndItinerary()) {
				Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
				Set<Integer> fareIdSet = new HashSet<Integer>();
				Integer fareId = null;
				HashMap fareRuleMap = new HashMap();
				for (ReservationPaxFare reservationPaxFare : colResPaxFares) {
					fareId = reservationPaxFare.getFareId();
					if (!fareIdSet.contains(fareId)) {
						fareIdSet.add(fareId);
					}
				}
				Map<Integer, ChargesDetailDTO> chargeRateIdMap = new HashMap<Integer, ChargesDetailDTO>();
				// Map<Integer,String> chargeRateIdMap = new HashMap<Integer,
				// String>();
				if (reservationPax.getOndChargesView() != null) {
					for (ChargesDetailDTO chargeDetailsDTO : reservationPax.getOndChargesView()) {
						chargeRateIdMap.put(chargeDetailsDTO.getChargeRateId(), chargeDetailsDTO);
					}
				}

				fareRuleMap = AirproxyModuleUtils.getFareBD().getFareRules(fareIdSet);
				String bookingType = "";
				String strPaxStatus = "";
				String strFareRule = "";
				String bookingCode = "";
				String strFareBasisCode = "";
				String strFareVisibility = "";
				String reportingChargeGroupCode = null;
				String bookingClassCode = null;

				for (ReservationPaxFare reservationPaxFare : colResPaxFares) {
					HashMap<String, TaxTO> taxMap = new HashMap<String, TaxTO>();
					HashMap<String, FeeTO> feeMap = new HashMap<String, FeeTO>();
					HashMap<String, SurchargeTO> surchargeMap = new HashMap<String, SurchargeTO>();
					String ondCode = ReservationApiUtils.getOndCode(reservationPaxFare);
					Date departureDate = ReservationApiUtils.getFirstDepartureDate(reservationPaxFare);
					if (AppSysParamsUtil.isHideStopOverEnabled() && ondCode != null && ondCode.split("/").length > 2) {
						ondCode = ReservationApiUtils.hideStopOverSeg(ondCode);
					}

					Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments()
							.iterator();
					ReservationPaxFareSegment reservationPaxFareSegment;

					String subStatus = null;

					if (itReservationPaxFareSegment.hasNext()) {
						reservationPaxFareSegment = itReservationPaxFareSegment.next();
						subStatus = reservationPaxFareSegment.getSegment().getSubStatus();
						FareRule farerule = (FareRule) fareRuleMap.get(reservationPaxFare.getFareId());
						if (farerule != null) {
							if (farerule.getReturnFlag() == 'Y') {
								bookingType = "Return";
							} else {
								bookingType = "One Way";
							}
							strFareRule = String.valueOf(farerule.getFareRuleCode());
							strFareBasisCode = farerule.getFareBasisCode();
							strFareVisibility = getFareVisibility(farerule.getVisibleChannelIds());
						}

						if (strFareRule.equals("") || strFareRule.equalsIgnoreCase("null")) {
							strFareRule = " Ad-Hoc ";
						}

						strPaxStatus = getPaxSegmentStatus(reservationPaxFareSegment.getStatus());
						if (reservationPaxFareSegment.getBookingCode() != null) {
							bookingCode = "/" + reservationPaxFareSegment.getBookingCode() + "/" + strFareRule + "/" + bookingType
									+ strPaxStatus + "/" + strFareBasisCode + "/" + strFareVisibility;
							bookingClassCode = reservationPaxFareSegment.getBookingCode();
						}
					}

					// /-------------------Modify-----------------------
					boolean isGroupChargesByReportingCGCodeEnabled = AppSysParamsUtil
							.isGroupChargesByReportingChargeGroupCodeEnabled();
					Collection<ReservationPaxOndCharge> colCharge = reservationPaxFare.getCharges();
					Collection<String> exchangeExcludeChargeCodes = null;
					Charge appliedDiscountGroup = null;

					if (reservationPaxFare.getDiscountCode() != null) {
						appliedDiscountGroup = ReservationModuleUtils.getChargeBD()
								.getCharge(reservationPaxFare.getDiscountCode());
					}

					if (colCharge != null) {
						for (ReservationPaxOndCharge chgDetail : colCharge) {

							// Hide exchanged charges by not continuing the loop
							// iteration
							if (AppSysParamsUtil.hideExchangesCharges()
									&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED
											.equalsIgnoreCase(subStatus)) {
								if (exchangeExcludeChargeCodes == null) {
									exchangeExcludeChargeCodes = CancellationUtils.getExchangeExcludeChargeCodes();
								}
								if (chgDetail.getChargeRateId() == null || !exchangeExcludeChargeCodes
										.contains((chargeRateIdMap.get(chgDetail.getChargeRateId())).getChargeCode())) {
									continue;
								}
							}
							String chargeInfo = "";

							if (chgDetail.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
								FareTO fareTo = new FareTO();
								fareTo.setAmount(chgDetail.getAmount());
								fareTo.setApplicableDate(chgDetail.getZuluChargeDate());
								fareTo.setCarrierCode(strCarrierCode);
								fareTo.setDescription("FARE" + bookingCode);
								fareTo.setSegmentCode(ondCode);
								fareTo.setDepartureDate(departureDate);
								fareTo.setBookingClassCode(bookingClassCode);
								fareList.add(fareTo);

								chargeInfo = "FARE";

							} else if (chgDetail.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.TAX)) {
								TaxTO taxTo = new TaxTO();
								taxTo.setAmount(chgDetail.getAmount());
								taxTo.getApplicablePassengerTypeCode().add(reservationPax.getPaxType());
								taxTo.setApplicableTime(chgDetail.getZuluChargeDate());
								taxTo.setCarrierCode(strCarrierCode);
								taxTo.setSegmentCode(ondCode);
								taxTo.setDepartureDate(departureDate);
								taxTo.setTaxCode(chgDetail.getChargeGroupCode());
								taxTo.setTaxName(chgDetail.getChargeGroupCode() + bookingCode);
								if (chgDetail.getChargeRateId() != null) {
									taxTo.setChargeRateId(chgDetail.getChargeRateId().toString());
									chargesDetailDTO = chargeRateIdMap.get(chgDetail.getChargeRateId());
									if (chargesDetailDTO != null) {
										taxTo.setTaxName(chargesDetailDTO.getChargeDescription());
										taxTo.setChargeCode(chargesDetailDTO.getChargeCode());

										chargeInfo = chargesDetailDTO.getChargeCode();
									}
									if (isGroupChargesByReportingCGCodeEnabled) {
										reportingChargeGroupCode = ReservationModuleUtils.getChargeBD()
												.getReportingChargeGroupCodeByChargeRateID(chgDetail.getChargeRateId());
										if (reportingChargeGroupCode != null) {
											taxTo.setReportingChargeGroupCode(reportingChargeGroupCode);
											TaxTO temptaxTo = taxMap.get(reportingChargeGroupCode);
											if (temptaxTo == null) {
												taxTo.setTaxName(reportingChargeGroupCode);
												taxMap.put(reportingChargeGroupCode, taxTo);
											} else {
												temptaxTo.setAmount(
														AccelAeroCalculator.add(temptaxTo.getAmount(), taxTo.getAmount()));
											}
										}
									}
								} else {
									taxTo.setChagrgeGroupCode(chgDetail.getChargeGroupCode());
								}
								if (taxTo.getReportingChargeGroupCode() == null) {
									taxList.add(taxTo);
								}
							} else if (chgDetail.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR)) {
								SurchargeTO surchargeTo = new SurchargeTO();
								surchargeTo.setAmount(chgDetail.getAmount());
								surchargeTo.getApplicablePassengerTypeCode().add(reservationPax.getPaxType());
								surchargeTo.setApplicableTime(chgDetail.getZuluChargeDate());
								surchargeTo.setCarrierCode(strCarrierCode);
								surchargeTo.setSegmentCode(ondCode);
								surchargeTo.setDepartureDate(departureDate);
								surchargeTo.setSurchargeCode(chgDetail.getChargeGroupCode());
								surchargeTo.setSurchargeName(chgDetail.getChargeGroupCode() + bookingCode);
								if (chgDetail.getChargeRateId() != null) {
									surchargeTo.setChargeRateId(chgDetail.getChargeRateId().toString());
									chargesDetailDTO = chargeRateIdMap.get(chgDetail.getChargeRateId());
									if (chargesDetailDTO != null) {
										surchargeTo.setSurchargeName(chargesDetailDTO.getChargeDescription());
										surchargeTo.setChargeCode(chargesDetailDTO.getChargeCode());

										chargeInfo = chargesDetailDTO.getChargeCode();
									}
									if (isGroupChargesByReportingCGCodeEnabled) {
										reportingChargeGroupCode = ReservationModuleUtils.getChargeBD()
												.getReportingChargeGroupCodeByChargeRateID(chgDetail.getChargeRateId());
										if (reportingChargeGroupCode != null) {
											surchargeTo.setReportingChargeGroupCode(reportingChargeGroupCode);
											SurchargeTO tempSurchargeTo = surchargeMap.get(reportingChargeGroupCode);
											if (tempSurchargeTo == null) {
												surchargeTo.setSurchargeName(reportingChargeGroupCode);
												surchargeMap.put(reportingChargeGroupCode, surchargeTo);
											} else {
												tempSurchargeTo.setAmount(AccelAeroCalculator.add(tempSurchargeTo.getAmount(),
														surchargeTo.getAmount()));
											}
										}
									}
								} else {
									surchargeTo.setChagrgeGroupCode(chgDetail.getChargeGroupCode());
								}
								if (surchargeTo.getReportingChargeGroupCode() == null) {
									surchargeList.add(surchargeTo);
								}
							} else {
								FeeTO feeTo = new FeeTO();
								feeTo.setAmount(chgDetail.getAmount());
								feeTo.getApplicablePassengerTypeCode().add(reservationPax.getPaxType());
								feeTo.setApplicableTime(chgDetail.getZuluChargeDate());
								feeTo.setCarrierCode(strCarrierCode);
								feeTo.setFeeCode(chgDetail.getChargeGroupCode());
								feeTo.setFeeName(chgDetail.getChargeGroupCode() + bookingCode);
								feeTo.setSegmentCode(ondCode);
								feeTo.setDepartureDate(departureDate);
								if (chgDetail.getChargeRateId() != null) {
									feeTo.setChargeRateId(chgDetail.getChargeRateId().toString());
									chargesDetailDTO = chargeRateIdMap.get(chgDetail.getChargeRateId());
									if (chargesDetailDTO != null) {
										feeTo.setFeeName(chargesDetailDTO.getChargeDescription());
										feeTo.setChargeCode(chargesDetailDTO.getChargeCode());

										chargeInfo = chargesDetailDTO.getChargeCode();
									}
									if (isGroupChargesByReportingCGCodeEnabled) {
										reportingChargeGroupCode = ReservationModuleUtils.getChargeBD()
												.getReportingChargeGroupCodeByChargeRateID(chgDetail.getChargeRateId());
										if (reportingChargeGroupCode != null) {
											feeTo.setReportingChargeGroupCode(reportingChargeGroupCode);
											FeeTO tempFeeTo = feeMap.get(reportingChargeGroupCode);
											if (tempFeeTo == null) {
												feeTo.setFeeName(reportingChargeGroupCode);
												feeMap.put(reportingChargeGroupCode, feeTo);
											} else {
												tempFeeTo.setAmount(
														AccelAeroCalculator.add(tempFeeTo.getAmount(), feeTo.getAmount()));
											}
										}
									}
								} else {
									feeTo.setChagrgeGroupCode(chgDetail.getChargeGroupCode());
								}
								if (feeTo.getReportingChargeGroupCode() == null) {
									feeList.add(feeTo);
								}
							}

							// add discount details
							if (chgDetail.getDiscount() != null && chgDetail.getDiscount().doubleValue() != 0
									&& appliedDiscountGroup != null) {

								FeeTO discountTo = new FeeTO();
								discountTo.setAmount(chgDetail.getDiscount());
								discountTo.setApplicableTime(chgDetail.getZuluChargeDate());
								discountTo.setCarrierCode(strCarrierCode);
								discountTo.setFeeCode(appliedDiscountGroup.getChargeGroupCode());

								if (ChargeCodes.FARE_DISCOUNT.equals(reservationPaxFare.getDiscountCode())
										|| ChargeCodes.DOM_FARE_DISCOUNT.equals(reservationPaxFare.getDiscountCode())) {
									chargeInfo = appliedDiscountGroup.getChargeDescription();
								} else {
									chargeInfo = appliedDiscountGroup.getChargeDescription() + " - " + chargeInfo;
								}

								discountTo.setFeeName(chargeInfo);
								discountTo.setSegmentCode(ondCode);
								discountTo.setDepartureDate(departureDate);

								feeList.add(discountTo);

							}

							// add ROE Charge adjustment details
							if (chgDetail.getAdjustment() != null && chgDetail.getAdjustment().doubleValue() != 0) {

								FeeTO adjustmentTo = new FeeTO();
								adjustmentTo.setAmount(chgDetail.getAdjustment());
								adjustmentTo.setApplicableTime(chgDetail.getZuluChargeDate());
								adjustmentTo.setCarrierCode(strCarrierCode);
								adjustmentTo.setFeeCode(ChargeCodes.FARE_ADJUSTMENTS);

								adjustmentTo.setFeeName("Fare Adjustment");
								adjustmentTo.setSegmentCode(ondCode);
								adjustmentTo.setDepartureDate(departureDate);

								feeList.add(adjustmentTo);

							}

						}
					}
					taxList.addAll(taxMap.values());
				}
				// process the common charge codes if the lists are not empty.
			}
			clientPax.setFares(fareList);
			clientPax.setFees(feeList);
			clientPax.setSurcharges(surchargeList);
			clientPax.setTaxes(taxList);

			clientPax.setFirstName(reservationPax.getFirstName());
			clientPax.setLastName(reservationPax.getLastName());
			
			if(blacklistedPnrPaxId != null && !blacklistedPnrPaxId.isEmpty()){
				clientPax.setBlacklisted(blacklistedPnrPaxId.contains(reservationPax.getPnrPaxId()));
			}

			LCCClientPaymentHolder lccClientPaymentHolder = new LCCClientPaymentHolder();

			if (loadFarePayment || AppSysParamsUtil.isPrintEmailIndItinerary()) {
				transformPayInfo(lccClientPaymentHolder, reservationPax, pnrPaxIdAndColReservationTnx);
				linkPaxCreditInfo(lccClientPaymentHolder, reservationPax, paxCredits);
			}

			clientPax.setLccClientPaymentHolder(lccClientPaymentHolder);

			clientPax.setNationalityCode(reservationPax.getNationalityCode());

			clientPax.setNationality(LCCUtils.getNationalityName(reservationPax.getNationalityCode()));
			if (!reccurCall) {
				clientPax.setParent(ReservationConvertUtil.transformPassenger(reservationPax.getParent(),
						pnrPaxIdAndColReservationTnx, paxCredits, null, null, null, null, null, null, true, loadFarePayment,
						infantPaymentSeparated, null));
			}
			clientPax.setPaxSequence(reservationPax.getPaxSequence());
			clientPax.setPaxType(reservationPax.getPaxType());

			List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries = new ArrayList<LCCSelectedSegmentAncillaryDTO>();

			Map<Integer, Collection<PaxSSRDTO>> mapSSR = new HashMap<Integer, Collection<PaxSSRDTO>>();
			Map<Integer, PaxSeatTO> mapSeats = new HashMap<Integer, PaxSeatTO>();
			Map<Integer, Collection<PaxMealTO>> mapMeals = new HashMap<Integer, Collection<PaxMealTO>>();
			Map<Integer, Collection<PaxAirportTransferTO>> mapAirportTransfers = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
			Map<Integer, List<PaxSeatTO>> mapExtraSeats = new HashMap<Integer, List<PaxSeatTO>>();

			Map<Integer, Collection<PaxBaggageTO>> mapBaggages = new HashMap<Integer, Collection<PaxBaggageTO>>();
			// baggage
			
			// Automatic checkin
			Map<Integer, Collection<PaxAutomaticCheckinTO>> mapAutoCheckins = new HashMap<Integer, Collection<PaxAutomaticCheckinTO>>();

			Set<Integer> pnrSegSet = new HashSet<Integer>();

			if (reservationPax.getPaxSSR() != null) {
				for (PaxSSRDTO paxSSR : reservationPax.getPaxSSR()) {
					if (mapSSR.get(paxSSR.getPnrSegId()) == null) {
						mapSSR.put(paxSSR.getPnrSegId(), new ArrayList<PaxSSRDTO>());
						pnrSegSet.add(paxSSR.getPnrSegId());
					}
					mapSSR.get(paxSSR.getPnrSegId()).add(paxSSR);
				}
			}

			if (paxMeals != null) {
				for (PaxMealTO paxMeal : paxMeals) {
					if (mapMeals.get(paxMeal.getPnrSegId()) == null) {
						mapMeals.put(paxMeal.getPnrSegId(), new ArrayList<PaxMealTO>());
						pnrSegSet.add(paxMeal.getPnrSegId());
					}
					mapMeals.get(paxMeal.getPnrSegId()).add(paxMeal);
				}
			}

			if (paxAirportTransfers != null) {
				for (PaxAirportTransferTO paxAirportTransfer : paxAirportTransfers) {
					if (mapAirportTransfers.get(paxAirportTransfer.getPnrSegId()) == null) {
						mapAirportTransfers.put(paxAirportTransfer.getPnrSegId(), new ArrayList<PaxAirportTransferTO>());
						pnrSegSet.add(paxAirportTransfer.getPnrSegId());
					}
					mapAirportTransfers.get(paxAirportTransfer.getPnrSegId()).add(paxAirportTransfer);
				}
			}

			// baggages

			if (paxBaggages != null) {
				for (PaxBaggageTO paxBaggage : paxBaggages) {
					if (mapBaggages.get(paxBaggage.getPnrSegId()) == null) {
						mapBaggages.put(paxBaggage.getPnrSegId(), new ArrayList<PaxBaggageTO>());
						pnrSegSet.add(paxBaggage.getPnrSegId());
					}
					mapBaggages.get(paxBaggage.getPnrSegId()).add(paxBaggage);
				}
			}

			// automatic checkin
			if (paxAutoCheckins != null) {
				for (PaxAutomaticCheckinTO paxAutoCheckin : paxAutoCheckins) {
					if (mapAutoCheckins.get(paxAutoCheckin.getPnrSegId()) == null) {
						mapAutoCheckins.put(paxAutoCheckin.getPnrSegId(), new ArrayList<PaxAutomaticCheckinTO>());
						pnrSegSet.add(paxAutoCheckin.getPnrSegId());
					}
					mapAutoCheckins.get(paxAutoCheckin.getPnrSegId()).add(paxAutoCheckin);
				}
			}

			if (paxSeats != null) {

				List<Integer> segWiseHasSeatCharges = new ArrayList<Integer>();
				for (PaxSeatTO paxSeat : paxSeats) {
					if (paxSeat.getChgDTO().getAmount().doubleValue() > 0) {
						segWiseHasSeatCharges.add(paxSeat.getPnrSegId());
					}
				}
				for (PaxSeatTO paxSeat : paxSeats) {
					if (mapSeats.get(paxSeat.getPnrSegId()) == null) {
						if (segWiseHasSeatCharges.contains(paxSeat.getPnrSegId()) == true) {
							if (paxSeat.getChgDTO().getAmount().doubleValue() > 0) {
								mapSeats.put(paxSeat.getPnrSegId(), paxSeat);
								pnrSegSet.add(paxSeat.getPnrSegId());
							} else {
								if (mapExtraSeats.get(paxSeat.getPnrSegId()) == null) {
									List<PaxSeatTO> extraSeats = new ArrayList<PaxSeatTO>();
									extraSeats.add(paxSeat);
									mapExtraSeats.put(paxSeat.getPnrSegId(), extraSeats);
								} else {
									mapExtraSeats.get(paxSeat.getPnrSegId()).add(paxSeat);
								}
							}
						} else {
							mapSeats.put(paxSeat.getPnrSegId(), paxSeat);
							pnrSegSet.add(paxSeat.getPnrSegId());
						}
					} else {
						if (mapExtraSeats.get(paxSeat.getPnrSegId()) == null) {
							List<PaxSeatTO> extraSeats = new ArrayList<PaxSeatTO>();
							extraSeats.add(paxSeat);
							mapExtraSeats.put(paxSeat.getPnrSegId(), extraSeats);
						} else {
							mapExtraSeats.get(paxSeat.getPnrSegId()).add(paxSeat);
						}
						// throw new ModuleRuntimeException("mutiple seats " +
						// mapSeats.get(paxSeat.getPnrSegId()).getSeatCode()
						// + "," + paxSeat.getSeatCode() +
						// " allocated for pax :" + reservationPax.getPnrPaxId()
						// + " from same seg: " + paxSeat.getPnrSegId());
						// Not Throwing the error since there are multiple seats
						// showing the which ever comes first
					}
				}

			}
			if (pnrSegMap != null) {
				for (Integer pnrSegId : pnrSegSet) {
					ReservationSegmentDTO segDTO = pnrSegMap.get(pnrSegId);
					if (segDTO != null) {
						FlightSegmentTO flightSegment = new FlightSegmentTO();
						flightSegment.setArrivalDateTime(segDTO.getArrivalDate());
						flightSegment.setCabinClassCode(segDTO.getCabinClassCode());
						flightSegment.setDepartureDateTime(segDTO.getDepartureDate());
						flightSegment.setFlightNumber(segDTO.getFlightNo());
						flightSegment.setSegmentCode(segDTO.getSegmentCode());
						flightSegment.setOperatingAirline(segDTO.getCarrierCode());
						flightSegment.setOperationType(segDTO.getOperationTypeID());
						flightSegment.setReturnFlag("Y".equals(segDTO.getReturnFlag()));
						flightSegment.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(segDTO));
						flightSegment.setPnrSegId(pnrSegId.toString());
						flightSegment.setSegmentSequence(segDTO.getSegmentSeq());
						flightSegment.setCsOcCarrierCode(segDTO.getCsOcCarrierCode());

						// avoiding duplicates and transfered modified to the same
						// segment
						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segDTO.getStatus())
								|| isUserDefinedSSRBooked(mapSSR, segDTO)) {
							LCCSelectedSegmentAncillaryDTO anciDTO = new LCCSelectedSegmentAncillaryDTO();
							anciDTO.setFlightSegmentTO(flightSegment);
							anciDTO.setSpecialServiceRequestDTOs(transformSSRs(mapSSR.get(pnrSegId)));
							anciDTO.setAirportServiceDTOs(transformAPServices(mapSSR.get(pnrSegId)));
							anciDTO.setAirportTransferDTOs(transformAPTransfers(mapAirportTransfers.get(pnrSegId)));

							anciDTO.setMealDTOs(tranformMeals(mapMeals.get(pnrSegId)));

							anciDTO.setBaggageDTOs(tranformBaggages(mapBaggages.get(pnrSegId))); // addign baggages

							anciDTO.setAutomaticCheckinDTOs(tranformAutoCheckins(mapAutoCheckins.get(pnrSegId)));

							anciDTO.setAirSeatDTO(tranformSeats(mapSeats.get(pnrSegId)));
							if (anciDTO.getAirSeatDTO() != null) {
								anciDTO.getAirSeatDTO().setLogicalCabinClassCode(segDTO.getLogicalCCCode());
							}
							if (mapExtraSeats.size() > 0 && mapExtraSeats.get(pnrSegId) != null
									&& mapExtraSeats.get(pnrSegId).size() > 0) {
								List<LCCAirSeatDTO> extraSeatDTOs = new ArrayList<LCCAirSeatDTO>();
								for (PaxSeatTO extraSeat : mapExtraSeats.get(pnrSegId)) {
									extraSeatDTOs.add(tranformSeats(extraSeat));
								}
								anciDTO.setExtraSeatDTOs(extraSeatDTOs);
								for (LCCAirSeatDTO extraSeat : anciDTO.getExtraSeatDTOs()) {
									extraSeat.setLogicalCabinClassCode(segDTO.getLogicalCCCode());
								}

							}

							anciDTO.setTravelerRefNumber(PaxTypeUtils.travelerReference(reservationPax));
							anciDTO.setInsuranceQuotations(null); // TODO get the
																	// insurance
																	// quotation
							selectedAncillaries.add(anciDTO);
						}
					}
				}
			}

			BigDecimal actualCredit = getRefundableCreditAmount(reservationPax.getPnrPaxId(), paxCredits);
			if (reservationPax.getTotalAvailableBalance().doubleValue() < 0 && actualCredit.doubleValue() > 0) {
				clientPax.setTotalActualCredit(actualCredit.negate());
			}

			clientPax.setSelectedAncillaries(selectedAncillaries);

			clientPax.setStatus(reservationPax.getStatus());

			clientPax.setTitle(reservationPax.getTitle());
			clientPax.setTotalAdjustmentCharge(reservationPax.getTotalAdjustmentCharge());
			clientPax.setTotalAvailableBalance(reservationPax.getTotalAvailableBalance());
			clientPax.setTotalCancelCharge(reservationPax.getTotalCancelCharge());
			clientPax.setTotalChargeAmounts(reservationPax.getTotalChargeAmounts());
			clientPax.setTotalFare(reservationPax.getTotalFare());
			clientPax.setTotalModificationCharge(reservationPax.getTotalModificationCharge());
			clientPax.setTotalPaidAmount(reservationPax.getTotalPaidAmount());
			if (infantPaymentSeparated) {
				clientPax.setTotalPrice(reservationPax.getTotalChargeAmountWhenInfantHasPayment());
			} else {
				clientPax.setTotalPrice(reservationPax.getTotalChargeAmount());
			}
			clientPax.setTotalSurCharge(reservationPax.getTotalSurCharge());
			clientPax.setTotalTaxCharge(reservationPax.getTotalTaxCharge());
			clientPax.setTravelerRefNumber(strCarrierCode + "|" + PaxTypeUtils.travelerReference(reservationPax));
			clientPax.setZuluReleaseTimeStamp(reservationPax.getZuluReleaseTimeStamp());
			clientPax.setZuluStartTimeStamp(reservationPax.getZuluStartTimeStamp());
			clientPax.setNoShowRefundableAmount(reservationPax.getNoShowRefundableAmount());
			clientPax.setNoShowRefundable(reservationPax.isNoShowRefundable());
			// clientPax.setLccReservation(clientReservation); // no need of
			// this for display purposes
		}
		return clientPax;
	}

	public static LCCAirSeatDTO tranformSeats(PaxSeatTO paxSeatTO) {
		LCCAirSeatDTO seat = null;
		if (paxSeatTO != null) {
			seat = new LCCAirSeatDTO();
			seat.setSeatNumber(paxSeatTO.getSeatCode());
			seat.setSeatCharge(paxSeatTO.getChgDTO().getAmount());
			seat.setAlertAutoCancellation(paxSeatTO.getAutoCancellationId() != null);
		}
		return seat;
	}

	public static List<LCCMealDTO> tranformMeals(Collection<PaxMealTO> paxMeals) {
		List<LCCMealDTO> mealList = new ArrayList<LCCMealDTO>();

		if (paxMeals != null) {
			Map<String, LCCMealDTO> mealQuantityMap = new HashMap<String, LCCMealDTO>();
			LCCMealDTO tempMeal = null;
			for (PaxMealTO paxMeal : paxMeals) {
				if (mealQuantityMap.containsKey(paxMeal.getMealCode())) {
					tempMeal = mealQuantityMap.get(paxMeal.getMealCode());
					tempMeal.setSoldMeals(tempMeal.getSoldMeals() + 1);
					// tempMeal.setMealQuntity(tempMeal.getMealQuntity() +
					// 1);//to be removed
					tempMeal.setTotalPrice(AccelAeroCalculator
							.formatAsDecimal(AccelAeroCalculator.multiply(tempMeal.getMealCharge(), tempMeal.getSoldMeals())));
					mealQuantityMap.put(tempMeal.getMealCode(), tempMeal);
				} else {
					LCCMealDTO meal = new LCCMealDTO();
					meal.setMealCode(paxMeal.getMealCode());
					meal.setMealName(paxMeal.getMealName());
					meal.setMealCharge(paxMeal.getChgDTO().getAmount());
					meal.setSoldMeals(1);
					meal.setMealCategoryID(paxMeal.getMealCategoryID());
					// meal.setMealQuntity(1);//to be removed
					meal.setTotalPrice(AccelAeroCalculator.formatAsDecimal(meal.getMealCharge()));
					meal.setAlertAutoCancellation(paxMeal.getAutoCancellationId() != null);
					meal.setMealCategoryCode(paxMeal.getMealCategoryCode());
					meal.setCategoryRestricted(paxMeal.isCategoryRestricted());
					mealQuantityMap.put(meal.getMealCode(), meal);
				}
				// ancillaryDTO.setMealDTOs(new
				// ArrayList<LCCMealDTO>(mealQuantityMap.values()));
			}
			mealList.addAll(mealQuantityMap.values());
		}

		return mealList;
	}

	/**
	 * @param lccPassengers
	 * @param passengers
	 * @param pnrSegmentMap
	 */
	private static void transformEtickets(Collection<LCCClientReservationPax> lccPassengers,
			Collection<ReservationPax> passengers, Map<Integer, ReservationSegmentDTO> pnrSegmentMap) {

		for (ReservationPax paxObj : passengers) {

			for (LCCClientReservationPax lccPax : lccPassengers) {

				if (lccPax.getPaxSequence() == paxObj.getPaxSequence()) {

					Collection<LccClientPassengerEticketInfoTO> paxEticketInfo = new ArrayList<LccClientPassengerEticketInfoTO>();

					Collection<EticketTO> eTicketTOs = paxObj.geteTickets();
					if (eTicketTOs != null) {
						for (EticketTO eTicketTO : eTicketTOs) {
							ReservationSegmentDTO segmentDto = pnrSegmentMap.get(eTicketTO.getPnrSegId());

							LccClientPassengerEticketInfoTO eTicketInfo = new LccClientPassengerEticketInfoTO();
							eTicketInfo.setTravelerRefNumber(lccPax.getTravelerRefNumber());
							eTicketInfo.setPpfsId(eTicketTO.getPnrPaxFareSegId());
							eTicketInfo.setEticketId(eTicketTO.getEticketId());
							eTicketInfo.setPaxETicketNo(String.valueOf(eTicketTO.getEticketNumber()));
							eTicketInfo.setCouponNo(eTicketTO.getCouponNo());
							eTicketInfo.setExternalPaxETicketNo(eTicketTO.getExternalEticketNumber() != null
									? String.valueOf(eTicketTO.getExternalEticketNumber())
									: eTicketTO.getExternalEticketNumber());
							eTicketInfo.setExternalCouponNo(eTicketTO.getExternalCouponNo());
							eTicketInfo.setPaxETicketStatus(eTicketTO.getTicketStatus());
							eTicketInfo.setPaxStatus(eTicketTO.getPaxStatus());
							eTicketInfo.setPnrSegId(segmentDto.getPnrSegId().toString());
							eTicketInfo.seteTicketSequnce(eTicketTO.geteTicketSequnce());
							eTicketInfo.setSegmentCode(segmentDto.getSegmentCode());
							eTicketInfo.setSegmentStatus(segmentDto.getStatus());
							eTicketInfo.setCarrierCode(segmentDto.getCarrierCode());
							eTicketInfo.setFlightNo(segmentDto.getFlightNo());
							eTicketInfo.setDepartureDate(segmentDto.getDepartureDate());
							eTicketInfo.setDepartureDateZulu(segmentDto.getZuluDepartureDate());
							eTicketInfo.setExternalCouponStatus(eTicketTO.getExternalCouponStatus());
							eTicketInfo.setExternalCouponControl(eTicketTO.getExternalCouponControl());
							eTicketInfo.setFlightSegmentRef(FlightRefNumberUtil.composeFlightRPH(segmentDto));
							eTicketInfo.setCodeShareCarrierCode(segmentDto.getCsOcCarrierCode());
							eTicketInfo.setCodeShareFlightNumber(segmentDto.getCsOcFlightNumber());

							paxEticketInfo.add(eTicketInfo);
						}
					}
					lccPax.seteTickets(paxEticketInfo);
					break;
				}
			}
		}
	}

	public static List<LCCBaggageDTO> tranformBaggages(Collection<PaxBaggageTO> paxBaggages) {
		List<LCCBaggageDTO> baggageList = new ArrayList<LCCBaggageDTO>();
		if (paxBaggages != null) {
			for (PaxBaggageTO paxBaggage : paxBaggages) {
				LCCBaggageDTO baggage = new LCCBaggageDTO();
				baggage.setBaggageName(paxBaggage.getBaggageName());
				baggage.setBaggageCharge(paxBaggage.getChgDTO().getAmount());
				baggage.setSoldPieces(1);
				baggage.setBaggageDescription(paxBaggage.getBaggageDescription());
				baggage.setAlertAutoCancellation(paxBaggage.getAutoCancellationId() != null);
				baggage.setOndBaggageChargeId(paxBaggage.getBaggageChrgId().toString());
				baggage.setOndBaggageGroupId(paxBaggage.getBaggageOndGroupId());
				baggageList.add(baggage);
			}
		}
		return baggageList;
	}

	public static List<LCCAutomaticCheckinDTO> tranformAutoCheckins(Collection<PaxAutomaticCheckinTO> paxAutoCheckins) {
		List<LCCAutomaticCheckinDTO> automaticCheckinDTOList = new ArrayList<LCCAutomaticCheckinDTO>();
		if (paxAutoCheckins != null) {
			for (PaxAutomaticCheckinTO paxAutomaticCheckin : paxAutoCheckins) {
				LCCAutomaticCheckinDTO automaticCheckin = new LCCAutomaticCheckinDTO();
				automaticCheckin.setAutoCheckinId(paxAutomaticCheckin.getAutoCheckinTemplateId());
				automaticCheckin.setSeatPref(paxAutomaticCheckin.getSeatTypePreference());
				automaticCheckin.setAutomaticCheckinCharge(paxAutomaticCheckin.getAmount());
				automaticCheckin.setEmail(paxAutomaticCheckin.getEmail());
				automaticCheckin.setSeatCode(paxAutomaticCheckin.getSeatCode());
				automaticCheckinDTOList.add(automaticCheckin);
			}
		}
		return automaticCheckinDTOList;
	}

	public static List<LCCSpecialServiceRequestDTO> transformSSRs(Collection<PaxSSRDTO> paxSSRs) {
		List<LCCSpecialServiceRequestDTO> ssrList = new ArrayList<LCCSpecialServiceRequestDTO>();
		if (paxSSRs != null) {
			for (PaxSSRDTO ssrDTO : paxSSRs) {
				/*
				 * If airport code null then SSR is in-flight service
				 */
				if (ssrDTO.getAirportCode() == null) {
					LCCSpecialServiceRequestDTO ssr = new LCCSpecialServiceRequestDTO();
					ssr.setSsrCode(ssrDTO.getSsrCode());
					SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(ssrDTO.getSsrCode());
					if (ssrInfoDTO != null) {
						ssr.setSsrName(ssrInfoDTO.getSSRName());
					}
					ssr.setDescription(ssrDTO.getSsrDesc());
					ssr.setText(ssrDTO.getSsrText());
					ssr.setCharge(AccelAeroCalculator.parseBigDecimal(ssrDTO.getChargeAmount()));
					ssr.setAlertAutoCancellation(ssrDTO.getAutoCancellationId() != null);
					ssr.setUserDefinedCharge(ssrDTO.isUserDefinedSSR());
					ssr.setStatus(ssrDTO.getStatus());
					ssrList.add(ssr);
				}
			}
		}
		return ssrList;
	}

	public static List<LCCAirportServiceDTO> transformAPServices(Collection<PaxSSRDTO> paxSSRs) {
		List<LCCAirportServiceDTO> ssrList = new ArrayList<LCCAirportServiceDTO>();
		if (paxSSRs != null) {
			for (PaxSSRDTO paxSSRDTO : paxSSRs) {
				/*
				 * If airport code not null then SSR is airport service
				 */
				if (paxSSRDTO.getAirportCode() != null
						&& (paxSSRDTO.getTransferDate() == null || paxSSRDTO.getTransferDate().equals(""))) {
					LCCAirportServiceDTO lccAPService = new LCCAirportServiceDTO();
					lccAPService.setSsrCode(paxSSRDTO.getSsrCode());

					SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(paxSSRDTO.getSsrCode());
					if (ssrInfoDTO != null) {
						lccAPService.setSsrName(ssrInfoDTO.getSSRName());
					}
					lccAPService.setSsrDescription(paxSSRDTO.getSsrDesc());
					lccAPService.setAirportCode(paxSSRDTO.getAirportCode());
					lccAPService.setServiceCharge(new BigDecimal(paxSSRDTO.getChargeAmount()));
					lccAPService.setAlertAutoCancellation(paxSSRDTO.getAutoCancellationId() != null);
					ssrList.add(lccAPService);
				}
			}
		}
		return ssrList;
	}

	public static List<LCCAirportServiceDTO> transformAPTransfers(Collection<PaxAirportTransferTO> paxAirportTransfers) {
		List<LCCAirportServiceDTO> airportTransferList = new ArrayList<LCCAirportServiceDTO>();
		if (paxAirportTransfers != null) {
			for (PaxAirportTransferTO paxAirportTransfersDTO : paxAirportTransfers) {

				LCCAirportServiceDTO lccAPTransfer = new LCCAirportServiceDTO();
				lccAPTransfer.setSsrCode("Airport Transfer");
				// lccAPTransfer.setSsrName(ssrInfoDTO.getSSRName());
				lccAPTransfer.setSsrDescription("Airport Transfer");
				lccAPTransfer.setAirportCode(paxAirportTransfersDTO.getAirportCode());
				// lccAPTransfer.setApplyOn(paxSSRDTO.getAirportType());
				lccAPTransfer.setServiceCharge(paxAirportTransfersDTO.getChargeAmount());
				lccAPTransfer.setTranferDate(paxAirportTransfersDTO.getRequestTimeStamp());
				lccAPTransfer.setTransferAddress(paxAirportTransfersDTO.getAddress());
				lccAPTransfer.setTransferContact(paxAirportTransfersDTO.getContactNo());
				lccAPTransfer.setTransferType(paxAirportTransfersDTO.getPickupType());
				lccAPTransfer.setAirportTransferId(paxAirportTransfersDTO.getAirportTransferId());
				airportTransferList.add(lccAPTransfer);

			}
		}
		return airportTransferList;
	}

	public static ReservationPax transformPassenger(LCCClientReservationPax clientPax, ReservationPax reservationPax)
			throws ModuleException {
		reservationPax.setDateOfBirth(clientPax.getDateOfBirth());
		reservationPax.setFirstName(clientPax.getFirstName());
		reservationPax.setLastName(clientPax.getLastName());
		reservationPax.setNationalityCode(clientPax.getNationalityCode());
		reservationPax.setPaxSSR(null);// FIXME
		reservationPax.setPaxType(clientPax.getPaxType());

		ReservationPaxAdditionalInfo addnInfo = new ReservationPaxAdditionalInfo();
		if (reservationPax.getPaxAdditionalInfo() != null) {
			addnInfo.setAdditionalInfoId(reservationPax.getPaxAdditionalInfo().getAdditionalInfoId());
		}

		if (clientPax.getLccClientAdditionPax() != null) {
			addnInfo.setPassportNo(clientPax.getLccClientAdditionPax().getPassportNo());
			addnInfo.setPassportExpiry(clientPax.getLccClientAdditionPax().getPassportExpiry());
			addnInfo.setPassportIssuedCntry(clientPax.getLccClientAdditionPax().getPassportIssuedCntry());
			addnInfo.setEmployeeID(clientPax.getLccClientAdditionPax().getEmployeeId());
			addnInfo.setDateOfJoin(clientPax.getLccClientAdditionPax().getDateOfJoin());
			addnInfo.setIdCategory(clientPax.getLccClientAdditionPax().getIdCategory());

			addnInfo.setPlaceOfBirth(clientPax.getLccClientAdditionPax().getPlaceOfBirth());
			addnInfo.setTravelDocumentType(clientPax.getLccClientAdditionPax().getTravelDocumentType());
			addnInfo.setVisaDocNumber(clientPax.getLccClientAdditionPax().getVisaDocNumber());
			addnInfo.setVisaDocPlaceOfIssue(clientPax.getLccClientAdditionPax().getVisaDocPlaceOfIssue());
			addnInfo.setVisaDocIssueDate(clientPax.getLccClientAdditionPax().getVisaDocIssueDate());
			addnInfo.setVisaApplicableCountry(clientPax.getLccClientAdditionPax().getVisaApplicableCountry());
			addnInfo.setFfid(clientPax.getLccClientAdditionPax().getFfid());
			addnInfo.setNationalIDNo(clientPax.getLccClientAdditionPax().getNationalIDNo());
			addnInfo.setPassportIssuedCntry(clientPax.getLccClientAdditionPax().getPassportIssuedCntry());
			addnInfo.setArrivalIntlFlightNo(clientPax.getLccClientAdditionPax().getArrivalIntlFltNo());
			addnInfo.setIntlFlightArrivalDate(clientPax.getLccClientAdditionPax().getIntlFltArrivalDate());
			addnInfo.setDepartureIntlFlightNo(clientPax.getLccClientAdditionPax().getDepartureIntlFltNo());
			addnInfo.setIntlFlightDepartureDate(clientPax.getLccClientAdditionPax().getIntlFltDepartureDate());
			addnInfo.setGroupId(clientPax.getLccClientAdditionPax().getPnrPaxGroupId());
		}

		addnInfo.setVersion(reservationPax.getVersion()); // TODO check with
															// rumesh is this
															// correct

		reservationPax.setPaxAdditionalInfo(addnInfo);

		if (clientPax.getLccClientPaxNameTranslations() != null) {
			String NULL_STRING = "null";
			String firstNameOl = clientPax.getLccClientPaxNameTranslations().getFirstNameOl();
			String lastNameOl = clientPax.getLccClientPaxNameTranslations().getLastNameOl();
			String languageCode = clientPax.getLccClientPaxNameTranslations().getLanguageCode();
			if ((!(NULL_STRING).equals(firstNameOl)) && (!(NULL_STRING).equals(lastNameOl))
					&& (!(NULL_STRING).equals(languageCode))) {
				PaxNameTranslations paxNameTranslations = new PaxNameTranslations();
				paxNameTranslations.setFirstNameOl(StringUtil.convertToHex(firstNameOl));
				paxNameTranslations.setLastNameOl(StringUtil.convertToHex(lastNameOl));
				paxNameTranslations.setLanguageCode(languageCode);
				paxNameTranslations.setTitleOl(StringUtil.convertToHex(clientPax.getLccClientPaxNameTranslations().getTitleOl()));

				if (reservationPax.getPaxNameTranslation() != null) {
					paxNameTranslations.setPnrPaxNamesId(reservationPax.getPaxNameTranslation().getPnrPaxNamesId());
					paxNameTranslations.setVersion(reservationPax.getPaxNameTranslation().getVersion());
				} else {
					paxNameTranslations.setVersion(clientPax.getLccClientPaxNameTranslations().getVersion());
				}
				reservationPax.setPaxNameTranslationList(paxNameTranslations);
			}
		}

		reservationPax.setStatus(clientPax.getStatus());
		// if(clientPax.getTitle()!=null && !clientPax.getTitle().equals("")){
		reservationPax.setTitle(clientPax.getTitle());
		// }
		if (clientPax.getParent() != null && clientPax.getParent().getTravelerRefNumber() != null) {
			reservationPax.setIndexId(PaxTypeUtils.getPaxSeq(clientPax.getParent().getTravelerRefNumber()));
		}
		return reservationPax;
	}

	public static LCCClientReservationSegment transformSegment(ReservationSegmentDTO segment, Reservation reservation,
			LCCClientReservation clientReservation, CachedAirportDTO departureAirport, CachedAirportDTO arrivalAirport,
			LCCClientPnrModesDTO pnrModesDTO, boolean isFlownSegment, AirportBD airportBD, boolean segmentModifiableAsPerETicketStatus) throws ModuleException {
		LCCClientReservationSegment clientSegment = new LCCClientReservationSegment();
		clientSegment.setArrivalAirportName(arrivalAirport.getAirportName());
		clientSegment.setArrivalDate(segment.getArrivalDate());
		clientSegment.setArrivalDateZulu(segment.getZuluArrivalDate());
		clientSegment.setBookingFlightSegmentRefNumber(segment.getPnrSegId().toString());
		clientSegment.setCabinClassCode(segment.getCabinClassCode());
		clientSegment.setLogicalCabinClass(segment.getLogicalCCCode());
		clientSegment.setDepartureAirportName(departureAirport.getAirportName());
		clientSegment.setBookingType(segment.getBookingType());
		clientSegment.setDepartureDate(segment.getDepartureDate());
		clientSegment.setDepartureDateZulu(segment.getZuluDepartureDate());
		clientSegment.setFlightNo(segment.getFlightNo());
		clientSegment.setFlightSegmentRefNumber(FlightRefNumberUtil.composeFlightRPH(segment));
		clientSegment.setFlightBaggageAllowance(segment.getFlightSegBagAllowance());
		clientSegment.setInterlineGroupKey(segment.getFareGroupId().toString());
		clientSegment.setInterlineReturnGroupKey(segment.getReturnOndGroupId().toString());
		// clientSegment.setLccReservation(clientReservation); no need of this
		// for display
		clientSegment.setSubStationShortName(segment.getSubStationShortName());
		clientSegment.setGroundStationPnrSegmentID(segment.getGroundStationPnrSegmentID());
		clientSegment.setPnrSegID(segment.getPnrSegId());
		clientSegment.setFlownSegment(isFlownSegment);
		clientSegment.setAllPaxNoShow(
				ReservationApiUtils.checkAllPaxNoShowInTheSegment(reservation.getPassengers(), segment.getPnrSegId()));
		clientSegment.setUnSegment(FlightStatusEnum.CANCELLED.getCode().equals(segment.getFlightStatus())
				&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus()));

		// set terminal information
		clientSegment.setArrivalTerminalName(segment.getArrivalTerminalName());
		clientSegment.setDepartureTerminalName(segment.getDepartureTerminalName());

		// set the check-in starting/closing time gap for the segment.
		clientSegment.setCheckInTimeGap(
				airportBD.getAirportCheckInTimeGap(getDepartureAirportCode(segment.getSegmentCode()), segment.getFlightNo()));
		clientSegment.setCheckInClosingTime(airportBD
				.getAirportCheckInClosingTimeGap(getDepartureAirportCode(segment.getSegmentCode()), segment.getFlightNo()));

		clientSegment.setFlightDuration(segment.getFlightDuration());
		clientSegment.setFlightModelDescription(segment.getFlightModelDescription());
		clientSegment.setFlightModelNumber(segment.getFlightModelNumber());
		clientSegment.setRemarks(segment.getRemarks());
		clientSegment.setBaggageONDGroupId(segment.getBaggageOndGroupId());
		clientSegment.setStatusModifiedChannelCode(segment.getStatusModifiedChannelCode());
		clientSegment.setFlexiRuleID(segment.getFlexiRuleID());
		clientSegment.setBundledServicePeriodId(segment.getBundledFarePeriodId());

		if (FlightType.DOMESTIC.getType().equals(segment.getFlightType())) {
			clientSegment.setDomesticFlight(true);
		} else {
			clientSegment.setDomesticFlight(false);
		}

		// Setting Fare Rules Details
		FareTO fareTO = new FareTO();
		fareTO.setFareId(segment.getFareTO().getFareId());
		fareTO.setFareRuleID(segment.getFareTO().getFareRuleID());
		fareTO.setAmount(segment.getFareTO().getAdultFareAmount());
		fareTO.setSegmentCode(segment.getFareTO().getOndCode());
		fareTO.setCarrierCode(segment.getCarrierCode());
		fareTO.setAppliedFlightSegId(segment.getFlightSegId());
		fareTO.setLogicalCCCode(segment.getLogicalCCCode());

		fareTO.setFareCategoryCode(segment.getFareTO().getFareCategoryCode());
		fareTO.setFareCategoryDescription(segment.getFareTO().getFareCategoryTO().getFareCategoryDesc());
		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			fareTO.setFareRuleComments(segment.getFareTO().getFareRulesCommentsInSelected());
		} else {
			fareTO.setFareRuleComments(segment.getFareTO().getFareRulesComments());
		}
		fareTO.setShowComments(segment.getFareTO().getFareCategoryTO().isShowComments());
		fareTO.setFareBasisCode(segment.getFareTO().getFareBasisCode());
		fareTO.setBookingClassCode(segment.getFareTO().getBookingClassCode());
		fareTO.setModifyToSameFare(segment.getFareTO().isModifyToSameFare());
		fareTO.setBulkTicketFareRule(segment.getFareTO().isBulkTicketFareRule());

		// required for re-quote flow
		fareTO.setFareType(
				getFareType(SelectListGenerator.getFareType(segment.getPnrSegId().toString(), fareTO.getFareId() + "")));
		fareTO.setReturnGroupId(segment.getReturnOndGroupId());

		/* segment modification */
		clientSegment.setModifyByDate(segment.getFareTO().getIsAllowModifyDate());
		clientSegment.setModifyByRoute(segment.getFareTO().getIsAllowModifyRoute());

		clientSegment.setFareTO(fareTO);
		// Setting buffer times----------------------------------
		Date depDateZulu = segment.getZuluDepartureDate();

		ApplicableBufferTimeDTO applicableBufferDTO = FlightUtil.getApplicableModifyBufferTime(
				segment.getFareTO().getInternationalModifyBufferTimeInMillis(),
				segment.getFareTO().getDomesticModifyBufferTimeInMillis(), segment.getFlightId());

		long modifyBufferTime = applicableBufferDTO.getApplicableBufferTime();
		long defaultModifyBufferTime = 0;

		// Flexi buffer time in minutes
		List<Integer> pnrSegIdList = new ArrayList<Integer>();
		pnrSegIdList.add(segment.getPnrSegId());
		Object[] flexiParams = ReservationApiUtils.getFlexiModifyCutOverBuffer(reservation, pnrSegIdList);
		long flexiBufferTime = ((Long) flexiParams[0]).longValue();
		boolean isFlexiModificationAvail = ((Boolean) flexiParams[1]).booleanValue();
		// flexiBufferTime = CalendarUtil.millisecs2Minutes(flexiBufferTime);
		// TODO: Need the separation of cancellation and modification. If all
		// the modification flexibilities utilized
		// and
		// one cancellation remaining this will allow to modify the segment in
		// buffer time as well. This need to be
		// handled.
		/**
		 * NAEEM(NA): updated the -1 check by >0. AARESAA - 4691 (Issue 6) In the case of 0 flexi time the modification
		 * buffer time was becoming 0.
		 */
		defaultModifyBufferTime = modifyBufferTime;
		if (flexiBufferTime > 0 && flexiBufferTime < modifyBufferTime) {
			modifyBufferTime = flexiBufferTime;
		}

		Calendar segModTillBufferDateTime = new GregorianCalendar();
		segModTillBufferDateTime.setTime(depDateZulu);
		segModTillBufferDateTime.add(Calendar.MINUTE, -CalendarUtil.millisecs2Minutes(modifyBufferTime));

		Calendar segModTillFlgClosureDateTime = new GregorianCalendar();
		segModTillFlgClosureDateTime.setTime(depDateZulu);
		segModTillFlgClosureDateTime.add(Calendar.MINUTE, -AppSysParamsUtil.getFlightClosureGapInMins());

		Calendar defaultSegModTillBufferDateTime = new GregorianCalendar();
		defaultSegModTillBufferDateTime.setTime(depDateZulu);
		defaultSegModTillBufferDateTime.add(Calendar.MINUTE, -CalendarUtil.millisecs2Minutes(defaultModifyBufferTime));

		clientSegment.setModifyTillBufferDateTime(
				isFlexiModificationAvail ? segModTillBufferDateTime.getTime() : defaultSegModTillBufferDateTime.getTime());
		/**
		 * Note: Had to introduce two separate variables for modification and cancellation to handle a reservation in
		 * buffer time with all modification flexi utilized and cancellation flexi remaining.
		 */
		clientSegment.setCancelTillBufferDateTime(segModTillBufferDateTime.getTime());
		clientSegment.setModifyTillFlightClosureDateTime(segModTillFlgClosureDateTime.getTime());
		// -----------------------------------------------------
		clientSegment.setReturnFlag(segment.getReturnFlag());
		clientSegment.setCarrierCode(segment.getCarrierCode());
		clientSegment.setSegmentCode(segment.getSegmentCode());
		clientSegment.setSegmentSeq(segment.getSegmentSeq());
		clientSegment.setStatus(segment.getStatus());
		clientSegment.setSubStatus(segment.getSubStatus());
		clientSegment.setDisplayStatus(segment.getDisplayStatus());
		clientSegment.setLastFareQuoteDate(segment.getLastFareQuoteDateZulu());
		clientSegment.setTicketValidTill(segment.getTicketValidTill());
		clientSegment.setJourneySequence(segment.getJourneySeq());

		clientSegment.setSystem(SYSTEM.AA);
		clientSegment.setSubStationShortName(segment.getSubStationShortName()); // Related
																				// to
																				// Bus
																				// service
		/* Set whether the segment is open return segment */
		clientSegment.setOpenReturnSegment(segment.isOpenReturnSegment());
		if (segment.isOpenReturnSegment()) {
			clientSegment.setOpenRetConfirmBefore(segment.getOpenRtConfirmBefore("dd-MM-yyyy HH:mm"));
			clientSegment.setOpenReturnTravelExpiry(segment.getSegmentExpiryDate("dd-MM-yyyy HH:mm"));

			// Commenting out unnecassary code, why we need to set only arrival
			// date? this leads to validation failure in new reqoute
			// implementation when we try to modify out bound segment of OpenRT
			// res
			// clientSegment.setArrivalDate(segment.getSegmentExpiryDateLocal());
			// clientSegment.setArrivalDateZulu(segment.getSegmentExpiryDateZulu());
		}

		// Set Segment Modifiable State
		if (pnrModesDTO != null && pnrModesDTO.getAppIndicator() != null) {
			clientSegment.setModifible(PNRModificationAuthorizationUtils.getResSegmentModifiable(reservation, segment,
					pnrModesDTO.getAppIndicator(), pnrModesDTO.getMapPrivilegeIds()));
		}

		// Set auto cancellation alert
		clientSegment.setAlertAutoCancellation(segment.isAlertAutoCancellation());
		clientSegment.setCsOcCarrierCode(segment.getCsOcCarrierCode());
		clientSegment.setCsOcFlightNumber(segment.getCsOcFlightNumber());
		
		for(ReservationSegment resSegment : reservation.getSegments()){
			if(resSegment.getFlightSegId().equals(segment.getFlightSegId())){
				clientSegment.setBookingClassCode(resSegment.getBookingCode());
			}
		}
		clientSegment.setSegmentModifiableAsPerETicketStatus(segmentModifiableAsPerETicketStatus);
		
		return clientSegment;
	}

	/**
	 * 
	 * @param segment
	 * @param clientReservation
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientReservationSegment transformExternalSegment(ExternalPnrSegment segment,
			LCCClientReservation clientReservation) throws ModuleException {
		LCCClientReservationSegment clientSegment = new LCCClientReservationSegment();
		ExternalFlightSegment extFltSeg = segment.getExternalFlightSegment();
		clientSegment.setArrivalDate(extFltSeg.getEstTimeArrivalLocal());
		clientSegment.setArrivalDateZulu(extFltSeg.getEstTimeArrivalZulu());
		clientSegment.setBookingFlightSegmentRefNumber(segment.getExternalPnrSegId().toString());
		clientSegment.setCabinClassCode(segment.getCabinClassCode());
		clientSegment.setLogicalCabinClass(segment.getLogicalCabinClassCode());
		clientSegment.setDepartureDate(extFltSeg.getEstTimeDepatureLocal());
		clientSegment.setDepartureDateZulu(extFltSeg.getEstTimeDepatureZulu());
		clientSegment.setFlightNo(extFltSeg.getFlightNumber());
		clientSegment.setFlightSegmentRefNumber(extFltSeg.getExternalFlightSegId().toString());
		clientSegment.setInterlineGroupKey(segment.getUniqueSegmentKey());
		clientSegment.setModifyTillBufferDateTime(new Date());// FIXME
		clientSegment.setCancelTillBufferDateTime(new Date());// FIXME
		clientSegment.setModifyTillFlightClosureDateTime(new Date());// FIXME
		clientSegment.setReturnFlag(segment.getReturnFlag());
		clientSegment.setSegmentCode(extFltSeg.getSegmentCode());
		clientSegment.setSegmentSeq(segment.getSegmentSeq());
		clientSegment.setStatus(segment.getStatus());
		clientSegment.setSubStatus(segment.getSubStatus());
		clientSegment.setSystem(SYSTEM.AA);
		return clientSegment;
	}

	/**
	 * 
	 * @param isInfantPaymentSeparated TODO
	 * @param resBalanceDTO
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationBalanceTO transformAltBalances(ResBalancesSummaryTOV2 balancesSummaryTOV2,
			Map<Integer, ReservationPax> resPassengers, boolean isInfantPaymentSeparated) throws ModuleException {
		ReservationBalanceTO reservationBalanceTO = new ReservationBalanceTO();

		LCCClientSegmentSummaryTO paxseg = null;
		BigDecimal totalPaidAmount = BigDecimal.ZERO;
		Map<Integer, PAXSummaryTOV2> allPax = new HashMap<Integer, PAXSummaryTOV2>();
		for (PAXSummaryTOV2 paxSummaryTOV2 : balancesSummaryTOV2.getPaxSummaryTOV2()) {
			allPax.put(paxSummaryTOV2.getPnrPaxId(), paxSummaryTOV2);
		}

		Collection<LCCClientPassengerSummaryTO> paxArray = new ArrayList<LCCClientPassengerSummaryTO>();
		for (PAXSummaryTOV2 paxSummaryTOV2 : balancesSummaryTOV2.getPaxSummaryTOV2()) {
			paxseg = transformSegmentSummary(paxSummaryTOV2.getSegmentSummaryTOV2());
			paxArray.add(transformPassengerSummary(paxSummaryTOV2, paxseg, resPassengers, allPax));
			if (!paxSummaryTOV2.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT) || isInfantPaymentSeparated) {
				if (paxSummaryTOV2.getColPayments() != null) {
					for (ReservationTnx resTnx : paxSummaryTOV2.getColPayments()) {
						totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, resTnx.getAmount());
					}
				}
			}
		}
		reservationBalanceTO.setPassengerSummaryList(paxArray);
		reservationBalanceTO.addPaxSummaryList(paxArray);
		LCCClientSegmentSummaryTO clientseg = transformSegmentSummary(balancesSummaryTOV2.getSegmentSummaryTOV2());
		reservationBalanceTO.setSegmentSummary(clientseg);
		reservationBalanceTO.setTotalAmountDue(balancesSummaryTOV2.getTotalAmountDue());
		reservationBalanceTO.setTotalFareDiscount(balancesSummaryTOV2.getTotalFareDiscount());
		reservationBalanceTO.setTotalCreditAmount(balancesSummaryTOV2.getTotalCreditAmount());
		reservationBalanceTO.setTotalCnxCharge(balancesSummaryTOV2.getTotalCnxChargeForCurrentAlt());
		reservationBalanceTO.setTotalModCharge(balancesSummaryTOV2.getTotalModChargeForCurrentAlt());
		reservationBalanceTO.setTotalPrice(balancesSummaryTOV2.getTotalPrice());
		reservationBalanceTO.setTotalPaidAmount(totalPaidAmount.negate());
		return reservationBalanceTO;
	}

	/**
	 * 
	 * @param clientseg
	 * @param allPax
	 *            TODO
	 * @param summaryDTO
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientPassengerSummaryTO transformPassengerSummary(PAXSummaryTOV2 paxSummaryTOV2,
			LCCClientSegmentSummaryTO clientseg, Map<Integer, ReservationPax> resPassengers, Map<Integer, PAXSummaryTOV2> allPax)
			throws ModuleException {
		LCCClientPassengerSummaryTO clientPaxSummaryTO = new LCCClientPassengerSummaryTO();

		String strTravelrefNo = "";
		String strInfTravellerRefNo = "";

		if (resPassengers.containsKey(paxSummaryTOV2.getPnrPaxId())) {
			ReservationPax resPax = resPassengers.get(paxSummaryTOV2.getPnrPaxId());
			strTravelrefNo = AppSysParamsUtil.getDefaultCarrierCode() + "|" + PaxTypeUtils.travelerReference(resPax);
			if (resPax.getInfants() != null && !resPax.getInfants().isEmpty()) {
				ReservationPax accompaniedPax = resPax.getInfants().iterator().next();
				strInfTravellerRefNo = AppSysParamsUtil.getDefaultCarrierCode() + "|"
						+ PaxTypeUtils.travelerReference(accompaniedPax);
				clientPaxSummaryTO.setAccompaniedTravellerRef(strInfTravellerRefNo);
			}
		}

		if (paxSummaryTOV2.getInfantId() != null) {
			if (allPax.containsKey(paxSummaryTOV2.getInfantId())) {
				clientPaxSummaryTO.setInfantName((allPax.get(paxSummaryTOV2.getInfantId())).getPaxName());
			}
		} else {
			clientPaxSummaryTO.setInfantName("");
		}

		clientPaxSummaryTO.setSegmentSummaryTO(clientseg);
		clientPaxSummaryTO.setPaxName(paxSummaryTOV2.getPaxName());
		if (paxSummaryTOV2.getPaxType() != null) {
			clientPaxSummaryTO.setPaxType(paxSummaryTOV2.getPaxType());
		} else {
			clientPaxSummaryTO.setPaxType(PaxTypeTO.ADULT);
		}

		clientPaxSummaryTO.setTotalAmountDue(paxSummaryTOV2.getTotalAmountDue());
		clientPaxSummaryTO.setTotalCreditAmount(paxSummaryTOV2.getTotalCreditAmount());
		clientPaxSummaryTO.setTotalCnxCharge(paxSummaryTOV2.getTotalCnxChargeForCurrentAlt());
		clientPaxSummaryTO.setTotalModCharge(paxSummaryTOV2.getTotalModChargeForCurrentAlt());
		clientPaxSummaryTO.setCnxChargeDetailTO(paxSummaryTOV2.getCnxChargeDetailTO());
		clientPaxSummaryTO.setModChargeDetailTO(paxSummaryTOV2.getModChargeDetailTO());
		BigDecimal totalPaidAmount = BigDecimal.ZERO;
		for (ReservationTnx reservationTnx : paxSummaryTOV2.getColPayments()) {
			totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, reservationTnx.getAmount());
		}
		clientPaxSummaryTO.setTotalPaidAmount(totalPaidAmount.negate());
		clientPaxSummaryTO.setTotalPrice(paxSummaryTOV2.getTotalPrice());
		clientPaxSummaryTO.setTravelerRefNumber(strTravelrefNo);

		return clientPaxSummaryTO;
	}

	/**
	 * 
	 * @param segSummDTO
	 * @return
	 * @throws ModuleException
	 */
	public static LCCClientSegmentSummaryTO transformSegmentSummary(SegmentSummaryTOV2 segmentSummaryTOV2)
			throws ModuleException {
		LCCClientSegmentSummaryTO clientSegSummary = new LCCClientSegmentSummaryTO();

		clientSegSummary.setCurrentAdjAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalAdjCharge());
		clientSegSummary.setCurrentCnxAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalCanCharge());
		clientSegSummary.setCurrentFareAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalFare());
		clientSegSummary.setCurrentModAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalModCharge());
		clientSegSummary.setCurrentNonRefunds(segmentSummaryTOV2.getCurrentNonRefundableAmounts().getTotalPrice());
		clientSegSummary.setCurrentRefunds(segmentSummaryTOV2.getCurrentRefundableAmounts().getTotalPrice());
		if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
			BigDecimal currentSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal currentSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			currentSurchargeAmount = segmentSummaryTOV2.getCurrentAmounts().getTotalSurcharge();
			for (ChargeMetaTO charge : segmentSummaryTOV2.getCurrentAmounts().getSurcharges()) {
				if (charge.getChargeCode().equals(CHAEGE_CODES.SM.toString())) {
					currentSeatAmount = AccelAeroCalculator.add(currentSeatAmount, charge.getChargeAmount());
					currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount().negate());
				} else if (charge.getChargeCode().equals(CHAEGE_CODES.ML.toString())) {
					currentMealAmount = AccelAeroCalculator.add(currentMealAmount, charge.getChargeAmount());
					currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount().negate());
				} else if (charge.getChargeCode().equals(CHAEGE_CODES.BG.toString())) {
					currentBaggageAmount = AccelAeroCalculator.add(currentBaggageAmount, charge.getChargeAmount());
					currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount().negate());
				} else if (charge.getChargeCode().equals(CHAEGE_CODES.IN.toString())) {
					currentInsuranceAmount = AccelAeroCalculator.add(currentInsuranceAmount, charge.getChargeAmount());
					currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount().negate());
				} else if (charge.getChargeCode().equals(CHAEGE_CODES.SSR.toString())) {
					currentSSRAmount = AccelAeroCalculator.add(currentSSRAmount, charge.getChargeAmount());
					currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount, charge.getChargeAmount().negate());
				}
			}
			clientSegSummary.setCurrentSurchargeAmount(currentSurchargeAmount);
			clientSegSummary.setCurrentSeatAmount(currentSeatAmount);
			clientSegSummary.setCurrentMealAmount(currentMealAmount);
			clientSegSummary.setCurrentBaggageAmount(currentBaggageAmount);
			clientSegSummary.setCurrentInsuranceAmount(currentInsuranceAmount);
			clientSegSummary.setCurrentSSRAmount(currentSSRAmount);
		} else {
			clientSegSummary.setCurrentSurchargeAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalSurcharge());
		}
		clientSegSummary.setCurrentTaxAmount(segmentSummaryTOV2.getCurrentAmounts().getTotalTax());
		clientSegSummary.setCurrentTotalPrice(segmentSummaryTOV2.getCurrentAmounts().getTotalPrice());
		clientSegSummary.setCurrentDiscount(segmentSummaryTOV2.getCurrentAmounts().getTotalDiscount());

		clientSegSummary.setNewAdjAmount(segmentSummaryTOV2.getNewAmounts().getTotalAdjCharge());
		clientSegSummary.setNewCnxAmount(AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalCanCharge(),
				segmentSummaryTOV2.getIdentifiedTotalCnxCharge()));
		clientSegSummary.setNewFareAmount(segmentSummaryTOV2.getNewAmounts().getTotalFare());
		clientSegSummary.setNewModAmount(AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalModCharge(),
				segmentSummaryTOV2.getIdentifiedTotalModCharge()));
		if (AppSysParamsUtil.isShowAncillaryBreakDown()) {
			BigDecimal newSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal newSeatAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal newMealAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal newBaggageAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal newInsuranceAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal newSSRAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			newSurchargeAmount = segmentSummaryTOV2.getNewAmounts().getTotalSurcharge();
			for (ChargeMetaTO charge : segmentSummaryTOV2.getNewAmounts().getSurcharges()) {
				if (CHAEGE_CODES.SM.toString().equals(charge.getChargeCode())) {
					newSeatAmount = AccelAeroCalculator.add(newSeatAmount, charge.getChargeAmount());
					newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
				} else if (CHAEGE_CODES.ML.toString().equals(charge.getChargeCode())) {
					newMealAmount = AccelAeroCalculator.add(newMealAmount, charge.getChargeAmount());
					newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
				} else if (CHAEGE_CODES.BG.toString().equals(charge.getChargeCode())) {
					newBaggageAmount = AccelAeroCalculator.add(newBaggageAmount, charge.getChargeAmount());
					newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
				} else if (CHAEGE_CODES.IN.toString().equals(charge.getChargeCode())) {
					newInsuranceAmount = AccelAeroCalculator.add(newInsuranceAmount, charge.getChargeAmount());
					newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
				} else if (CHAEGE_CODES.SSR.toString().equals(charge.getChargeCode())) {
					newSSRAmount = AccelAeroCalculator.add(newSSRAmount, charge.getChargeAmount());
					newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, charge.getChargeAmount().negate());
				}
			}
			clientSegSummary.setNewSurchargeAmount(newSurchargeAmount);
			clientSegSummary.setNewSeatAmount(newSeatAmount);
			clientSegSummary.setNewMealAmount(newMealAmount);
			clientSegSummary.setNewBaggageAmount(newBaggageAmount);
			clientSegSummary.setNewInsuranceAmount(newInsuranceAmount);
			clientSegSummary.setNewSSRAmount(newSSRAmount);
		} else {
			clientSegSummary.setNewSurchargeAmount(segmentSummaryTOV2.getNewAmounts().getTotalSurcharge());
		}
		clientSegSummary.setNewSurchargeAmount(AccelAeroCalculator.add(clientSegSummary.getNewSurchargeAmount(),
				segmentSummaryTOV2.getIdentifiedExtraFeeAmount()));
		clientSegSummary.setNewTaxAmount(segmentSummaryTOV2.getNewAmounts().getTotalTax());
		clientSegSummary.setNewTotalPrice(AccelAeroCalculator.add(segmentSummaryTOV2.getNewAmounts().getTotalPrice(),
				segmentSummaryTOV2.getIdentifiedTotalCnxCharge(), segmentSummaryTOV2.getIdentifiedTotalModCharge()));
		clientSegSummary.setOutBoundExternalCharge(segmentSummaryTOV2.getOutBoundFlexiCharge());
		clientSegSummary.setInBoundExternalCharge(segmentSummaryTOV2.getInBoundFlexiCharge());
		clientSegSummary.setNewDiscount(segmentSummaryTOV2.getNewAmounts().getTotalDiscount());
		return clientSegSummary;
	}

	/**
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static List<LCCClientCarrierOndGroup> trnsformONDGrouping(Reservation reservation) throws ModuleException {
		List<LCCClientCarrierOndGroup> lccClientCarrierOndGrouping = new ArrayList<LCCClientCarrierOndGroup>();

		return lccClientCarrierOndGrouping;
	}

	public static Collection<UserNoteHistoryTO> transformUserNoteTO(Collection<UserNoteTO> colUserNoteTO) {
		Collection<UserNoteHistoryTO> colUserNoteHistoryTO = new ArrayList<UserNoteHistoryTO>();
		for (UserNoteTO userNote : colUserNoteTO) {
			UserNoteHistoryTO userNoteHistoryTO = new UserNoteHistoryTO();
			userNoteHistoryTO.setDisplayAction(userNote.getAction());
			if (userNote.getModifiedDate() != null) {
				userNoteHistoryTO.setDisplayModifyDate(DateFormatUtils.format(userNote.getModifiedDate(), "dd-MM-yyyy HH:mm"));
			}
			userNoteHistoryTO.setDisplaySystemNote(userNote.getSystemNote());
			userNoteHistoryTO.setDisplayUserName(userNote.getUserName());
			userNoteHistoryTO.setDisplayUserNote(userNote.getUserText());
			userNoteHistoryTO.setDisplayCarrierCode(userNote.getCarrierCode());
			colUserNoteHistoryTO.add(userNoteHistoryTO);
		}
		return colUserNoteHistoryTO;
	}

	public static UserNoteTO transformUserNotes(ReservationModificationDTO reservationModificationDTO,
			Map<String, String> mapUsers) throws ModuleException {

		UserNoteTO userNoteTO = new UserNoteTO();
		userNoteTO.setAction(reservationModificationDTO.getModificationTypeDesc());
		userNoteTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		if (reservationModificationDTO.getLocalModificationDate() != null
				&& !reservationModificationDTO.getLocalModificationDate().equals("")) {
			userNoteTO.setModifiedDate(reservationModificationDTO.getLocalModificationDate());
		} else {
			userNoteTO.setModifiedDate(reservationModificationDTO.getZuluModificationDate());
		}
		userNoteTO.setSystemNote(reservationModificationDTO.getDescription());
		if (reservationModificationDTO.getUserId().equals("")
				&& reservationModificationDTO.getSalesChannelCode() == ReservationInternalConstants.SalesChannel.WEB) {
			userNoteTO.setUserName("WEB");
		} else if (reservationModificationDTO.getUserId().equals("")
				&& reservationModificationDTO.getSalesChannelCode() == ReservationInternalConstants.SalesChannel.IOS) {
			userNoteTO.setUserName("IOS");
		} else if (reservationModificationDTO.getUserId().equals("")
				&& reservationModificationDTO.getSalesChannelCode() == ReservationInternalConstants.SalesChannel.ANDROID) {
			userNoteTO.setUserName("ANDROID");
		} else if (reservationModificationDTO.getDisplayName() == null || reservationModificationDTO.getDisplayName().equals("")) {
			userNoteTO.setUserName(mapUsers.get(reservationModificationDTO.getUserId()));
		} else {
			userNoteTO.setUserName(reservationModificationDTO.getDisplayName());
		}

		userNoteTO.setUserNotes(null);
		userNoteTO.setUserText(reservationModificationDTO.getUserNote());
		return userNoteTO;
	}

	private static void transformPayInfo(LCCClientPaymentHolder lccClientPaymentHolder, ReservationPax pax,
			Map<Integer, Collection<ReservationTnx>> pnrPaxIdAndColReservationTnx) throws ModuleException {
		Collection<ReservationTnx> colReservationTnx = pnrPaxIdAndColReservationTnx.get(pax.getPnrPaxId());

		Collection<?> colCashNominalCodes = ReservationTnxNominalCode.getCashTypeNominalCodes();
		Collection<?> colVisaNominalCodes = ReservationTnxNominalCode.getVisaTypeNominalCodes();
		Collection<?> colMasterNominalCodes = ReservationTnxNominalCode.getMasterTypeNominalCodes();
		Collection<?> colAmexNominalCodes = ReservationTnxNominalCode.getAmexTypeNominalCodes();
		Collection<?> colDinersNominalCodes = ReservationTnxNominalCode.getDinersTypeNominalCodes();
		Collection<?> colGenericNominalCodes = ReservationTnxNominalCode.getGenericTypeNominalCodes();
		Collection<?> colCMINominalCodes = ReservationTnxNominalCode.getCMITypeNominalCodes();
		Collection<?> colOnAccountNominalCodes = ReservationTnxNominalCode.getOnAccountTypeNominalCodes();
		Collection<?> colCreditNominalCodes = ReservationTnxNominalCode.getCreditTypeNominalCodes();
		Collection<?> colBSPNominalCodes = ReservationTnxNominalCode.getBSPAccountTypeNominalCodes();
		Collection<?> colPayPalNominalCodes = ReservationTnxNominalCode.getPayPalTypeNominalCodes();
		Collection<?> colGenericDebitNominalCodes = ReservationTnxNominalCode.getDebitCardNominalCodes();
		Collection<?> colLMSNominalCodes = ReservationTnxNominalCode.getLoyaltyNominalCodes();
		Collection<?> colNonRefundablePaymentCodes = ReservationTnxNominalCode.getNonRefundableTypePaymentCodes();
		Collection<?> colVoucherNominalCodes = ReservationTnxNominalCode.getVoucherNominalCodes();

		Map payTypeAndColReservationTnx = new HashMap();
		ReservationTnx reservationTnx;

		if (colReservationTnx != null) {
			for (Iterator<ReservationTnx> itReservationTnx = colReservationTnx.iterator(); itReservationTnx.hasNext();) {
				reservationTnx = itReservationTnx.next();

				if (colCashNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colCashNominalCodes, reservationTnx);
				} else if (colVisaNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colVisaNominalCodes, reservationTnx);
				} else if (colMasterNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colMasterNominalCodes, reservationTnx);
				} else if (colAmexNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colAmexNominalCodes, reservationTnx);
				} else if (colDinersNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colDinersNominalCodes, reservationTnx);
				} else if (colGenericNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colGenericNominalCodes, reservationTnx);
				} else if (colCMINominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colCMINominalCodes, reservationTnx);
				} else if (colOnAccountNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colOnAccountNominalCodes, reservationTnx);
				} else if (colCreditNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colCreditNominalCodes, reservationTnx);
				} else if (colBSPNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colBSPNominalCodes, reservationTnx);
				} else if (colPayPalNominalCodes.contains(reservationTnx.getNominalCode())) { // paypal
					updateNominalMap(payTypeAndColReservationTnx, colPayPalNominalCodes, reservationTnx);
				} else if (colGenericDebitNominalCodes.contains(reservationTnx.getNominalCode())) { // paypal
					updateNominalMap(payTypeAndColReservationTnx, colGenericDebitNominalCodes, reservationTnx);
				} else if (colLMSNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colLMSNominalCodes, reservationTnx);
				} else if (colVoucherNominalCodes.contains(reservationTnx.getNominalCode())) {
					updateNominalMap(payTypeAndColReservationTnx, colVoucherNominalCodes, reservationTnx);
				}
			}

			Collection colPayTypeNominalCodes;
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
			for (Iterator itPayType = payTypeAndColReservationTnx.keySet().iterator(); itPayType.hasNext();) {
				colPayTypeNominalCodes = (Collection) itPayType.next();
				colReservationTnx = (Collection) payTypeAndColReservationTnx.get(colPayTypeNominalCodes);

				Map<String, PaymentSummaryTO> mapPaymentSummaryTO = new HashMap<String, PaymentSummaryTO>();
				PaymentSummaryTO paymentSummaryTO;
				ReservationPaxTnxBreakdown txnBreak;
				// Identify payments (assumption : all transactions having same
				// timestamp assumed to belong to single
				// payment transaction)
				for (Iterator<ReservationTnx> it = colReservationTnx.iterator(); it.hasNext();) {
					reservationTnx = it.next();

					if (!mapPaymentSummaryTO
							.containsKey(reservationTnx.getTnxId().toString() + reservationTnx.getNominalCode().toString())) {
						paymentSummaryTO = new PaymentSummaryTO();
						mapPaymentSummaryTO.put(reservationTnx.getTnxId() + reservationTnx.getNominalCode().toString(),
								paymentSummaryTO);
					}
					paymentSummaryTO = mapPaymentSummaryTO
							.get(reservationTnx.getTnxId() + reservationTnx.getNominalCode().toString());

					if (paymentSummaryTO.getAmount() == null) {
						paymentSummaryTO.setTnxId(reservationTnx.getTnxId());
						paymentSummaryTO.setAmount(reservationTnx.getAmount().negate());
						paymentSummaryTO.setAgencyCode(reservationTnx.getAgentCode());
					} else {
						paymentSummaryTO.setAmount(
								AccelAeroCalculator.add(paymentSummaryTO.getAmount(), reservationTnx.getAmount().negate()));
					}
					paymentSummaryTO.setPaymentDate(reservationTnx.getDateTime());
					paymentSummaryTO.setDescription(ReservationTnxNominalCode.getDescription(reservationTnx.getNominalCode()));
					paymentSummaryTO.setPaymentReference(reservationTnx.getExternalReference());
					paymentSummaryTO.setActualPaymentMethod(reservationTnx.getExternalReferenceType());
					paymentSummaryTO.setRecieptNo(reservationTnx.getRecieptNo());
					paymentSummaryTO.setPaymentTnxReference(reservationTnx.getOriginalPaymentTnxID() != null
							? reservationTnx.getOriginalPaymentTnxID().toString()
							: null);
					txnBreak = reservationTnx.getReservationPaxTnxBreakdown();
					paymentSummaryTO.setDummyPayment(reservationTnx.getDummyPayment());
					paymentSummaryTO.setRemarks(reservationTnx.getRemarksRemovingUnwantedCharacters());
					if (txnBreak != null) { // Code assumes there could be
											// transactions not having
											// transaction breakdown
											// records as well.
						if (paymentSummaryTO.getAmountInPayCurrency() == null) {
							paymentSummaryTO.setAmountInPayCurrency(txnBreak.getTotalPriceInPayCurrency().negate());
							paymentSummaryTO.setPayCurrencyCode(txnBreak.getPaymentCurrencyCode());
						} else {
							paymentSummaryTO.setAmountInPayCurrency(AccelAeroCalculator.add(
									paymentSummaryTO.getAmountInPayCurrency(), txnBreak.getTotalPriceInPayCurrency().negate()));
						}
					}
				}

				// ---------------------end of summing the total

				for (String payDate : mapPaymentSummaryTO.keySet()) {
					paymentSummaryTO = mapPaymentSummaryTO.get(payDate);

					String carrierVisePayments = AppSysParamsUtil.getDefaultCarrierCode() + "-"
							+ paymentSummaryTO.getAmount().toString();
					// If it's a cash payment
					if (colPayTypeNominalCodes.equals(colCashNominalCodes)) {
						lccClientPaymentHolder.addCashPayment(carrierCode, paymentSummaryTO.getAgencyCode(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
								paymentSummaryTO.getAgencyCode(), paymentSummaryTO.getAmountInPayCurrency(),
								paymentSummaryTO.getPayCurrencyCode(), paymentSummaryTO.getPaymentReference(),
								paymentSummaryTO.getTnxId().toString(), paymentSummaryTO.getActualPaymentMethod(),
								paymentSummaryTO.getRecieptNo(), carrierVisePayments, null, Agent.PAYMENT_MODE_CASH,
								paymentSummaryTO.getRemarks(), paymentSummaryTO.getDummyPayment(),
								paymentSummaryTO.getPaymentTnxReference());

						// If it's a on account payment
					} else if (colPayTypeNominalCodes.equals(colOnAccountNominalCodes)) {
						lccClientPaymentHolder.addAgentCreditPayment(carrierCode, paymentSummaryTO.getAgencyCode(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
								paymentSummaryTO.getAgencyCode(), paymentSummaryTO.getAmountInPayCurrency(),
								paymentSummaryTO.getPayCurrencyCode(), paymentSummaryTO.getPaymentReference(),
								paymentSummaryTO.getTnxId().toString(), paymentSummaryTO.getActualPaymentMethod(),
								paymentSummaryTO.getRecieptNo(), carrierVisePayments, null, Agent.PAYMENT_MODE_ONACCOUNT,
								paymentSummaryTO.getRemarks(), paymentSummaryTO.getPaymentTnxReference());

						// If it's a BSP payment
					} else if (colPayTypeNominalCodes.equals(colBSPNominalCodes)) {
						lccClientPaymentHolder.addAgentCreditPayment(carrierCode, paymentSummaryTO.getAgencyCode(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
								paymentSummaryTO.getAgencyCode(), paymentSummaryTO.getAmountInPayCurrency(),
								paymentSummaryTO.getPayCurrencyCode(), paymentSummaryTO.getPaymentReference(),
								paymentSummaryTO.getTnxId().toString(), paymentSummaryTO.getActualPaymentMethod(),
								paymentSummaryTO.getRecieptNo(), carrierVisePayments, null, Agent.PAYMENT_MODE_BSP,
								paymentSummaryTO.getRemarks(), paymentSummaryTO.getPaymentTnxReference());
						// If it's a credit payment
					} else if (colPayTypeNominalCodes.equals(colCreditNominalCodes)) {
						lccClientPaymentHolder.addPaxCreditPayment(carrierCode, null, paymentSummaryTO.getDescription(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, null, null, null, carrierVisePayments, null,
								paymentSummaryTO.getRemarks(), paymentSummaryTO.getPaymentTnxReference());
						// If it's a LMS payment
					} else if (colPayTypeNominalCodes.equals(colLMSNominalCodes)) {
						boolean isNonRefundable = false;
						if (colNonRefundablePaymentCodes != null && !colNonRefundablePaymentCodes.isEmpty()) {
							isNonRefundable = CollectionUtils.containsAny(colLMSNominalCodes, colNonRefundablePaymentCodes);
						}
						lccClientPaymentHolder.addLMSPayment(carrierCode, paymentSummaryTO.getPaymentReference(),
								paymentSummaryTO.getDescription(), paymentSummaryTO.getAmount(), baseCurrencyCode,
								paymentSummaryTO.getPaymentDate(), paymentSummaryTO.getAmountInPayCurrency(),
								paymentSummaryTO.getPayCurrencyCode(), null, paymentSummaryTO.getTnxId().toString(), null,
								carrierVisePayments, null, paymentSummaryTO.getRemarks(),
								paymentSummaryTO.getPaymentTnxReference(), PaymentType.LOYALTY_PAYMENT.toString(),
								isNonRefundable);
						// If it's a credit payment
					} else if (colPayTypeNominalCodes.equals(colVoucherNominalCodes)) {
						VoucherDTO voucherDTO = new VoucherDTO();
						voucherDTO.setRedeemdAmount(paymentSummaryTO.getAmountInPayCurrency().toString());
						voucherDTO.setVoucherId(paymentSummaryTO.getPaymentReference());
						lccClientPaymentHolder.addVoucherPayment(carrierCode, voucherDTO, paymentSummaryTO.getPaymentReference(),
								paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
								paymentSummaryTO.getAmountInPayCurrency(), paymentSummaryTO.getPayCurrencyCode(), null,
								paymentSummaryTO.getTnxId().toString(), null, carrierVisePayments, null,
								paymentSummaryTO.getRemarks(), paymentSummaryTO.getPaymentTnxReference());
					} else if (colPayTypeNominalCodes.equals(colVisaNominalCodes)
							|| colPayTypeNominalCodes.equals(colMasterNominalCodes)
							|| colPayTypeNominalCodes.equals(colAmexNominalCodes)
							|| colPayTypeNominalCodes.equals(colDinersNominalCodes)
							|| colPayTypeNominalCodes.equals(colGenericNominalCodes)
							|| colPayTypeNominalCodes.equals(colCMINominalCodes)
							|| colPayTypeNominalCodes.equals(colPayPalNominalCodes)
							|| colPayTypeNominalCodes.equals(colGenericDebitNominalCodes)) {

						Collection<Integer> txnIds = new ArrayList<Integer>();
						txnIds.add(paymentSummaryTO.getTnxId());

						Map<Integer, CardPaymentInfo> mapCardPayInfo = AirproxyModuleUtils.getReservationBD().getCreditCardInfo(
								txnIds, true);
						if (mapCardPayInfo != null && mapCardPayInfo.containsKey(paymentSummaryTO.getTnxId())) {
							CardPaymentInfo cardPaymentInfo = mapCardPayInfo.get(paymentSummaryTO.getTnxId());

							lccClientPaymentHolder.addCardPayment(carrierCode, cardPaymentInfo.getType(),
									cardPaymentInfo.getEDate(), cardPaymentInfo.getNoLastDigits(), cardPaymentInfo.getName(),
									cardPaymentInfo.getAddress(), cardPaymentInfo.getSecurityCode(),
									paymentSummaryTO.getAmount(), baseCurrencyCode, paymentSummaryTO.getPaymentDate(),
									cardPaymentInfo.getAuthorizationId(), paymentSummaryTO.getAmountInPayCurrency(),
									cardPaymentInfo.getName(), paymentSummaryTO.getPayCurrencyCode(),
									paymentSummaryTO.getPaymentReference(), paymentSummaryTO.getTnxId().toString(),
									paymentSummaryTO.getActualPaymentMethod(), carrierVisePayments, null,
									paymentSummaryTO.getRemarks(), cardPaymentInfo.getPaymentBrokerRefNo(),
									paymentSummaryTO.getPaymentTnxReference());
						} else {
							lccClientPaymentHolder.addOfflineCardPayment(paymentSummaryTO.getAmount(),
									paymentSummaryTO.getAmountInPayCurrency(), baseCurrencyCode,
									paymentSummaryTO.getPaymentTnxReference(), paymentSummaryTO.getRemarks(),
									paymentSummaryTO.getDescription(), paymentSummaryTO.getPaymentDate());
						}

					}
				}
			}
		}
	}

	private static void linkPaxCreditInfo(LCCClientPaymentHolder lccClientPaymentHolder, ReservationPax pax,
			Map<Integer, Collection<CreditInfoDTO>> paxCredits) throws ModuleException {
		Collection<CreditInfoDTO> creditInfoDTOs = paxCredits.get(pax.getPnrPaxId());
		lccClientPaymentHolder.setCredits(creditInfoDTOs);
	}

	/**
	 * Updates the Nominal Map
	 * 
	 * @param payTypesNominalMap
	 * @param colNominalCodes
	 * @param reservationTnx
	 */
	private static void updateNominalMap(Map payTypesNominalMap, Collection colNominalCodes, ReservationTnx reservationTnx) {
		Collection<ReservationTnx> colPayTypeReservationTnx;

		if (payTypesNominalMap.containsKey(colNominalCodes)) {
			colPayTypeReservationTnx = (Collection) payTypesNominalMap.get(colNominalCodes);
			colPayTypeReservationTnx.add(reservationTnx);
		} else {
			colPayTypeReservationTnx = new ArrayList();
			colPayTypeReservationTnx.add(reservationTnx);
			payTypesNominalMap.put(colNominalCodes, colPayTypeReservationTnx);
		}
	}

	private static List<LCCClientCarrierOndGroup> createCarrierONDGrouping(Collection<ReservationPax> setpaxes)
			throws ModuleException {

		List<LCCClientCarrierOndGroup> ondList = new ArrayList<LCCClientCarrierOndGroup>();
		Map<Collection<Integer>, LCCClientCarrierOndGroup> ondWisePnrPaxFareIds = new HashMap<Collection<Integer>, LCCClientCarrierOndGroup>();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		for (ReservationPax reservationPax : setpaxes) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				Collection<Integer> pnrSegIds = new HashSet<Integer>();

				Date departure = null;
				String subStatus = "";
				String status = "";
				for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
					pnrSegIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());
					subStatus = reservationPaxFareSegment.getSegment().getSubStatus();
					status = reservationPaxFareSegment.getSegment().getStatus();
					for (ReservationSegmentDTO segmentDTO : reservationPax.getReservation().getSegmentsView()) {
						if (segmentDTO.getPnrSegId().equals(reservationPaxFareSegment.getSegment().getPnrSegId())) {
							departure = segmentDTO.getDepartureDate();
						}
					}
				}

				LCCClientCarrierOndGroup carrierOndGroup = null;
				if (ondWisePnrPaxFareIds.containsKey(pnrSegIds)) {
					carrierOndGroup = ondWisePnrPaxFareIds.get(pnrSegIds);
					carrierOndGroup.setCarrierOndGroupRPH(
							carrierOndGroup.getCarrierOndGroupRPH() + "|" + reservationPaxFare.getPnrPaxFareId());
				} else {
					carrierOndGroup = new LCCClientCarrierOndGroup();
					carrierOndGroup.setCarrierOndGroupRPH(String.valueOf(reservationPaxFare.getPnrPaxFareId()));
				}

				carrierOndGroup.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				carrierOndGroup.setSegmentCode(ReservationApiUtils.getOndCode(reservationPaxFare));
				carrierOndGroup.setCarrierPnrSegIds(pnrSegIds);
				carrierOndGroup.setSubStatus(subStatus);
				carrierOndGroup.setStatus(status);
				carrierOndGroup.setDepartureDateTime(departure == null ? null : dateFormatter.format(departure));
				carrierOndGroup.setTotalChargeAmount(AccelAeroCalculator.add(reservationPaxFare.getTotalChargeAmount(),
						(carrierOndGroup.getTotalChargeAmount() != null
								? carrierOndGroup.getTotalChargeAmount()
								: AccelAeroCalculator.getDefaultBigDecimalZero())));
				carrierOndGroup.setTotalCharges(AccelAeroCalculator.formatAsDecimal(carrierOndGroup.getTotalChargeAmount()) + " "
						+ AppSysParamsUtil.getBaseCurrency());

				ondWisePnrPaxFareIds.put(pnrSegIds, carrierOndGroup);

			}
		}
		ondList.addAll(ondWisePnrPaxFareIds.values());
		return ondList;
	}

	private static String getPaxSegmentStatus(String strIn) {
		String strStat = "";
		if (strIn != null) {
			if (strIn.equals("F")) {
				strStat = "/FLOWN";
			} else if (strIn.equals("N")) {
				strStat = "/NO SHOW";
			} else if (strIn.equals("G")) {
				strStat = "/GO SHOW";
			} else if (strIn.equals("R")) {
				strStat = "/NO REC";
			}
		}
		return strStat;
	}

	private static String getDepartureAirportCode(String segmentCode) {
		String departureAirportCode = "";

		if (segmentCode != null) {
			String[] airportCodes = segmentCode.split("/");
			if (airportCodes.length > 0) {
				departureAirportCode = airportCodes[0];
			}
		}

		return departureAirportCode;
	}

	@SuppressWarnings("unchecked")
	public static List<ReservationPaxOndFlexibilityDTO> segmentFlexiList(
			Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap, Integer pnrSegId,
			Reservation reservation) {
		int ppfId = 0;
		Set<ReservationPax> reservationPaxList = reservation.getPassengers();
		for (ReservationPax pax : reservationPaxList) {
			if (!("IN").equals(pax.getPaxType())) {
				for (ReservationPaxFare fare : pax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment fareSegment : fare.getPaxFareSegments()) {
						if (pnrSegId == fareSegment.getSegment().getPnrSegId().intValue()) {
							ppfId = fare.getPnrPaxFareId().intValue();
							return (List) ondFlexibilitiesMap.get(ppfId);
						}
					}
				}
			}
		}
		return (List) ondFlexibilitiesMap.get(ppfId);
	}

	public static List<LCCClientAlertTO> transform(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		List<LCCClientAlertTO> clientAlertList = new ArrayList<LCCClientAlertTO>();
		for (ReservationPaxOndFlexibilityDTO paxOndFlexi : segmentFlexiList) {
			LCCClientAlertTO alertTO = new LCCClientAlertTO();
			alertTO.setContent(String.valueOf(paxOndFlexi.getAvailableCount()));
			alertTO.setAlertId(paxOndFlexi.getFlexibilityTypeId());
			clientAlertList.add(alertTO);
		}
		LCCClientAlertTO alertTO = new LCCClientAlertTO();// Used for additional
															// details
		alertTO.setContent(getAdditionalFlexiDetails(segmentFlexiList));
		alertTO.setAlertId(new Integer(0));// Additional details against 0
		clientAlertList.add(alertTO);

		return clientAlertList;
	}

	public static String getAdditionalFlexiDetails(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		for (ReservationPaxOndFlexibilityDTO paxOndFlexi : segmentFlexiList) {
			return String.valueOf(paxOndFlexi.getCutOverBufferInMins() / 60);
		}
		return null;
	}

	public static List<ReservationPaxOndFlexibilityDTO> sortOndFlexi(List<ReservationPaxOndFlexibilityDTO> segmentFlexiList) {
		// Modification and Cancellation are saved against type ids 1 & 2
		Collections.sort(segmentFlexiList, new Comparator<ReservationPaxOndFlexibilityDTO>() {
			@Override
			public int compare(ReservationPaxOndFlexibilityDTO flexiDTO1, ReservationPaxOndFlexibilityDTO flexiDTO2) {
				return (new Integer(flexiDTO1.getFlexibilityTypeId())).compareTo(new Integer(flexiDTO2.getFlexibilityTypeId()));
			}
		});
		return segmentFlexiList;
	}

	public static Map<EXTERNAL_CHARGES, BigDecimal> createExternalChargesSummary(Set<ReservationPax> setPax) {
		Map<EXTERNAL_CHARGES, BigDecimal> chargesSummary = new HashMap<EXTERNAL_CHARGES, BigDecimal>();
		Map externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();

		BigDecimal insuranceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal seatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal mealCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal flixiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal creditCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal inflightServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal ssrCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal promotionRewardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal additionalSeatCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal airportTransferCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal autoCheckinCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal anciPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bspFee = AccelAeroCalculator.getDefaultBigDecimalZero();
		/**
		 * For baggage
		 */
		BigDecimal baggageCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (setPax != null) {
			for (ReservationPax reservationPax : setPax) {
				if (reservationPax != null) {
					if (reservationPax.getOndChargesView() != null) {
						for (ChargesDetailDTO chargeDetailsDTO : reservationPax.getOndChargesView()) {

							if (chargeDetailsDTO.getChargeCode() != null) {
								if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.INSURANCE.toString()))) {
									insuranceCharge = AccelAeroCalculator.add(insuranceCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.MEAL.toString()))) {
									mealCharge = AccelAeroCalculator.add(mealCharge, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.SEAT_MAP.toString()))) {
									seatCharge = AccelAeroCalculator.add(seatCharge, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString()))) {
									inflightServiceCharge = AccelAeroCalculator.add(inflightServiceCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()))) {
									flixiCharge = AccelAeroCalculator.add(flixiCharge, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.CREDIT_CARD.toString()))) {
									creditCardCharge = AccelAeroCalculator.add(creditCardCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.BAGGAGE.toString()))) { // baggage
									baggageCharge = AccelAeroCalculator.add(baggageCharge, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString()))) {
									ssrCharges = AccelAeroCalculator.add(ssrCharges, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString()))) {
									airportTransferCharge = AccelAeroCalculator.add(airportTransferCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString()))) {
									autoCheckinCharge = AccelAeroCalculator.add(autoCheckinCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.PROMOTION_REWARD.toString()))) {
									promotionRewardCharge = AccelAeroCalculator.add(promotionRewardCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE.toString()))) {
									additionalSeatCharge = AccelAeroCalculator.add(additionalSeatCharge,
											chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode()
										.equals(externalChargesMap.get(EXTERNAL_CHARGES.ANCI_PENALTY.toString()))) {
									anciPenalty = AccelAeroCalculator.add(anciPenalty, chargeDetailsDTO.getChargeAmount());
								} else if (chargeDetailsDTO.getChargeCode().equals(
										externalChargesMap.get(EXTERNAL_CHARGES.BSP_FEE.toString()))) {
									bspFee = AccelAeroCalculator.add(bspFee, chargeDetailsDTO.getChargeAmount());
								}
							}
						}
					}
				}
			}

		}

		chargesSummary.put(EXTERNAL_CHARGES.INSURANCE, insuranceCharge);
		chargesSummary.put(EXTERNAL_CHARGES.MEAL, mealCharge);
		chargesSummary.put(EXTERNAL_CHARGES.SEAT_MAP, seatCharge);
		chargesSummary.put(EXTERNAL_CHARGES.INFLIGHT_SERVICES, inflightServiceCharge);
		chargesSummary.put(EXTERNAL_CHARGES.FLEXI_CHARGES, flixiCharge);
		chargesSummary.put(EXTERNAL_CHARGES.CREDIT_CARD, creditCardCharge);
		chargesSummary.put(EXTERNAL_CHARGES.BAGGAGE, baggageCharge);
		chargesSummary.put(EXTERNAL_CHARGES.AIRPORT_SERVICE, ssrCharges);
		chargesSummary.put(EXTERNAL_CHARGES.PROMOTION_REWARD, promotionRewardCharge);
		chargesSummary.put(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE, additionalSeatCharge);
		chargesSummary.put(EXTERNAL_CHARGES.AIRPORT_TRANSFER, airportTransferCharge);
		chargesSummary.put(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN, autoCheckinCharge);
		chargesSummary.put(EXTERNAL_CHARGES.ANCI_PENALTY, anciPenalty);
		chargesSummary.put(EXTERNAL_CHARGES.BSP_FEE, bspFee);

		return chargesSummary;
	}

	public static BigDecimal getCreditPromotionDiscount(Reservation reservation) {

		BigDecimal creditPromotionDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCreditPromotion paxOndCreditPromotion : reservationPaxFare.getChargesPromotionCredits()) {
					if (paxOndCreditPromotion.getDiscount() != null && paxOndCreditPromotion.getDiscount().doubleValue() != 0) {
						creditPromotionDiscount = AccelAeroCalculator.add(creditPromotionDiscount,
								paxOndCreditPromotion.getDiscount());
					}
				}
			}
		}
		return creditPromotionDiscount;
	}

	private static String getFareVisibility(Set<Integer> visibleChannelIds) {

		Iterator<Integer> itrVisibleChannelIds = visibleChannelIds.iterator();
		String strFareVisibility = "";
		int count = 0;
		while (itrVisibleChannelIds.hasNext()) {
			Integer visibleChannelId = itrVisibleChannelIds.next();
			if (visibleChannelId != null) {

				if (count > 0)
					strFareVisibility += ",";

				if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_AGENT) {
					strFareVisibility += "Agent";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_PUBLIC) {
					strFareVisibility += "Public";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_WEB) {
					strFareVisibility += "Web";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES) {
					strFareVisibility += "APIAgencies";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_LCC) {
					strFareVisibility += "Lcc";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_IOS) {
					strFareVisibility += "Ios";
				} else if (visibleChannelId.intValue() == SalesChannelsUtil.SALES_CHANNEL_ANDROID) {
					strFareVisibility += "Android";
				}

				count++;
			}
		}

		return strFareVisibility;
	}

	private static String getFareType(String fareType) {
		int i = FareTypes.OTHER_FARE;
		if (!StringUtil.isNullOrEmpty(fareType)) {
			if (FareTypeCodes.SEGMENT.equals(fareType)) {
				i = FareTypes.PER_FLIGHT_FARE;
			} else if (FareTypeCodes.CONNECTION.equals(fareType)) {
				i = FareTypes.OND_FARE;
			} else if (FareTypeCodes.RETURN.equals(fareType)) {
				i = FareTypes.RETURN_FARE;
			} else if (FareTypeCodes.HALF_RETURN.equals(fareType)) {
				i = FareTypes.HALF_RETURN_FARE;
			}
		}
		return Integer.toString(i);
	}

	private static BigDecimal getRefundableCreditAmount(Integer pnrPaxId, Map<Integer, Collection<CreditInfoDTO>> paxCredits) {
		BigDecimal refundableCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxCredits != null && !paxCredits.isEmpty()) {
			Collection<CreditInfoDTO> creditInfoDTOs = paxCredits.get(pnrPaxId);

			if (creditInfoDTOs != null && !creditInfoDTOs.isEmpty()) {
				for (CreditInfoDTO creditInfoDTO : creditInfoDTOs) {
					if (!creditInfoDTO.isNonRefundableCredit()
							&& CreditInfoDTO.status.AVAILABLE.equals(creditInfoDTO.getEnumStatus())) {
						refundableCreditAmount = AccelAeroCalculator.add(refundableCreditAmount, creditInfoDTO.getMcAmount());
					}
				}
			}
		}
		return refundableCreditAmount;
	}

	private static boolean isNewOndCode(ReservationSegmentDTO segDto, Map<Integer, String> ondSeqWithOndCodeMap)
			throws ModuleException {
		if (!segDto.getOperationTypeID().equals(AirScheduleCustomConstants.OperationTypes.STANDARD)) {
			return true;
		}
		if (!ondSeqWithOndCodeMap.keySet().isEmpty() && segDto.getFareTO().getOndCode() != null) {
			if ("N".equals(segDto.getReturnFlag()) && segDto.getFareTO().getOndCode().equals(ondSeqWithOndCodeMap.get(0))) {
				if (segDto.getSegmentCode() != null) {
					String[] segCodeArr = segDto.getSegmentCode().split("/");
					String[] outBoundSegArr = ondSeqWithOndCodeMap.get(0).split("/");
					if (segCodeArr.length == 2 && outBoundSegArr.length > 1
							&& ondSeqWithOndCodeMap.get(0).contains(segCodeArr[1] + "/" + segCodeArr[0])) {
						// outbound is flown
						return true;
					}
				}
				// Then this is another segment of same ond
				return false;
			} else if ("N".equals(segDto.getReturnFlag())
					&& !segDto.getFareTO().getOndCode().equals(ondSeqWithOndCodeMap.get(0))) {
				// should be multi city
				return true;
			} else if ("Y".equals(segDto.getReturnFlag())) {
				if (ondSeqWithOndCodeMap.keySet().size() == 1) {
					// first segment of return ond
					ondSeqWithOndCodeMap.put(1, segDto.getFareTO().getOndCode());
					return true;
				} else {
					// now ondSeqWithOndCodeMap has both outbound and inbound ond codes
					return false;
				}
			}
		} else {
			// First segment of outbound ond
			ondSeqWithOndCodeMap.put(0, segDto.getFareTO().getOndCode());
			return true;
		}
		return false;
	}

	public static Set<LCCClientOtherAirlineSegment>
			transformOtherAirlineSegment(Set<OtherAirlineSegment> otherAirlineSegmentSet) {
		Map<String, String> codeShareBcCosMap = CommonsServices.getGlobalConfig().getCodeshareBcCosMap();
		Set<LCCClientOtherAirlineSegment> lccOtherAirlineSegmentSet = new HashSet<LCCClientOtherAirlineSegment>();
		if (otherAirlineSegmentSet != null) {
			for (OtherAirlineSegment otherAirlineSegment : otherAirlineSegmentSet) {
				LCCClientOtherAirlineSegment lccOtherAirlineSegment = new LCCClientOtherAirlineSegment();
				lccOtherAirlineSegment.setPnrOtherAirlineSegId(otherAirlineSegment.getPnrOtherAirlineSegId());
				lccOtherAirlineSegment.setFlightNumber(otherAirlineSegment.getFlightNumber());
				lccOtherAirlineSegment.setBookingCode(otherAirlineSegment.getBookingCode());
				lccOtherAirlineSegment.setDepartureDateTimeLocal(otherAirlineSegment.getDepartureDateTimeLocal());
				lccOtherAirlineSegment.setFlightSegId(otherAirlineSegment.getFlightSegId());
				lccOtherAirlineSegment.setMarketingBookingCode(otherAirlineSegment.getMarketingBookingCode());
				lccOtherAirlineSegment.setMarketingFlightNumber(otherAirlineSegment.getMarketingFlightNumber());
				lccOtherAirlineSegment.setStatus(otherAirlineSegment.getStatus());
				lccOtherAirlineSegment.setArrivalDateTimeLocal(otherAirlineSegment.getArrivalDateTimeLocal());
				lccOtherAirlineSegment
						.setSegmentCode(otherAirlineSegment.getOrigin() + "/" + otherAirlineSegment.getDestination());
				lccOtherAirlineSegment.setInterlineGroupKey(otherAirlineSegment.getUniqueSegmentKey());
				lccOtherAirlineSegment.setReservation(otherAirlineSegment.getReservation());
				lccOtherAirlineSegment.setCabinClassCode(codeShareBcCosMap.get(otherAirlineSegment.getBookingCode()));
				lccOtherAirlineSegmentSet.add(lccOtherAirlineSegment);
			}
		}
		return lccOtherAirlineSegmentSet;
	}
	
	/**
	 * allow loading CNX user defined SSR
	 * 
	 */
	private static boolean isUserDefinedSSRBooked(Map<Integer, Collection<PaxSSRDTO>> mapSSR, ReservationSegmentDTO segDTO) {
		Integer pnrSegId = segDTO.getPnrSegId();

		if (!ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segDTO.getStatus())
				&& !ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(segDTO.getSubStatus())
				&& mapSSR != null && mapSSR.get(pnrSegId) != null) {

			Collection<PaxSSRDTO> paxSSRs = mapSSR.get(pnrSegId);
			if (paxSSRs != null && !paxSSRs.isEmpty()) {
				for (PaxSSRDTO ssrDTO : paxSSRs) {
					if (ssrDTO.isUserDefinedSSR()) {
						return true;
					}
				}
			}

		}
		return false;
	}
}
