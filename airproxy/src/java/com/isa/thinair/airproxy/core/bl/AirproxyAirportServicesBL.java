package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

/**
 * 
 * @author rumesh
 * 
 */

public class AirproxyAirportServicesBL {

	private Log log = LogFactory.getLog(AirproxyAirportServicesBL.class);

	private SYSTEM system;
	private List<FlightSegmentTO> flightSegmentTOs;
	private int serviceCategory;
	private AppIndicatorEnum appIndicator;
	private String transactionIdentifier;
	private UserPrincipal userPrincipal;
	private String selectedLanguage;
	private Map<String, Integer> serviceCount;
	private BasicTrackInfo trackInfo;
	private boolean modifyAnci;
	private List<BundledFareDTO> bundledFareDTOs;
	private String pnr;

	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> execute() throws ModuleException {

		if (system == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().getAirportServiceRequests(flightSegmentTOs, serviceCount,
					serviceCategory, appIndicator, transactionIdentifier, selectedLanguage, trackInfo, modifyAnci, pnr);
		} else {
			return getOwnAirlineAiportServices();
		}
	}

	private Map<String, Integer> getSegmentBundledFareInclusion(Map<Integer, Integer> ondBundledFareInclusion) {
		Map<String, Integer> segmentBundledFareInclusion = null;

		if (ondBundledFareInclusion != null && !ondBundledFareInclusion.isEmpty()) {

			segmentBundledFareInclusion = new HashMap<String, Integer>();
			Map<Integer, Set<String>> ondSegments = new HashMap<Integer, Set<String>>();

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (!ondSegments.containsKey(flightSegmentTO.getOndSequence())) {
					ondSegments.put(flightSegmentTO.getOndSequence(), new HashSet<String>());
				}

				ondSegments.get(flightSegmentTO.getOndSequence()).add(flightSegmentTO.getSegmentCode());
			}

			for (Entry<Integer, Integer> ondEntry : ondBundledFareInclusion.entrySet()) {
				Integer ondSequence = ondEntry.getKey();
				Integer bundledFarePeriodId = ondEntry.getValue();
				if (ondSegments.containsKey(ondSequence)) {
					Set<String> segments = ondSegments.get(ondSequence);
					for (String segmentCode : segments) {
						segmentBundledFareInclusion.put(segmentCode, bundledFarePeriodId);
					}
				}
			}
		}

		return segmentBundledFareInclusion;
	}

	private Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getOwnAirlineAiportServices() throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				flightSegmentTOs, serviceCount, false);

		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> lccAiportWiseMap = new LinkedHashMap<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO>();

		if (isServiceEnabled()) {

			Map<Integer, Integer> ondBundledFareInclusion = InventoryAPIUtil.getOndWiseBundledFareInclusion(bundledFareDTOs,
					EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());
			Map<String, Integer> segmentBundledFareInclusion = getSegmentBundledFareInclusion(ondBundledFareInclusion);

			Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseResults = AirproxyModuleUtils.getSsrServiceBD()
					.getAvailableAirportServices(airportWiseMap, appIndicator, serviceCategory, selectedLanguage,
							segmentBundledFareInclusion, false);

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> entry : airportWiseResults.entrySet()) {

					AirportServiceKeyTO keyTO = entry.getKey();

					if (entry.getValue().size() > 0) {
						if (flightSegmentTO.getSegmentCode().equals(keyTO.getSegmentCode())
								&& flightSegmentTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {
							List<LCCAirportServiceDTO> lccAirportServiceList = new ArrayList<LCCAirportServiceDTO>();

							for (AirportServiceDTO airportServiceDTO : entry.getValue()) {
								if (modifyAnci && AppIndicatorEnum.APP_XBE.equals(appIndicator)
										&& "N".equals(airportServiceDTO.getStatus())) {
									lccAirportServiceList.add(populateLCCAirportServiceDTO(airportServiceDTO));
								} else if ("Y".equals(airportServiceDTO.getStatus())) {
									lccAirportServiceList.add(populateLCCAirportServiceDTO(airportServiceDTO));
								}
							}
							
							if (bundledFareDTOs != null && bundledFareDTOs.size() > 0) {
								BundledFareDTO tempBundleFareDTO = null;
								LCCAirportServiceDTO lccAirportServiceDTO = null;
								
								for (BundledFareDTO bundleFareDTO : bundledFareDTOs) {
									if (bundleFareDTO != null
											&& segmentBundledFareInclusion != null
											&& bundleFareDTO.getBundledFarePeriodId().equals(
											segmentBundledFareInclusion.get(flightSegmentTO.getSegmentCode()))) {
										tempBundleFareDTO = bundleFareDTO;
										break;
									}
								}
					
								if(tempBundleFareDTO != null) {
									for (int i = 0; i < lccAirportServiceList.size(); i++) {
										for (BundledFareFreeServiceTO bundledFareFreeServiceTO : tempBundleFareDTO.getApplicableServices()) {
											if (bundledFareFreeServiceTO.getIncludedFreeServices().contains(
													(int) (long) lccAirportServiceList.get(i).getSsrID())) {
												lccAirportServiceDTO = lccAirportServiceList.get(i);
												lccAirportServiceDTO.setIsFree(true);
												lccAirportServiceList.set(i, lccAirportServiceDTO);
											}
										}
									}
								}
							}

							LCCFlightSegmentAirportServiceDTO lccFSegAirportServiceDTO = new LCCFlightSegmentAirportServiceDTO();
							lccFSegAirportServiceDTO.setFlightSegmentTO(flightSegmentTO);
							lccFSegAirportServiceDTO.setAirportServices(lccAirportServiceList);

							lccAiportWiseMap.put(keyTO, lccFSegAirportServiceDTO);

						}
					}

				}
			}
		}

		return lccAiportWiseMap;
	}

	private boolean isServiceEnabled() {
		boolean request = false;
		if (serviceCategory == SSRCategory.ID_AIRPORT_SERVICE) {
			request = AppSysParamsUtil.isAirportServicesEnabled();
		} else if (serviceCategory == SSRCategory.ID_AIRPORT_TRANSFER) {
			request = AppSysParamsUtil.isAirportTransferEnabled();
		}
		return request;
	}

	private LCCAirportServiceDTO populateLCCAirportServiceDTO(AirportServiceDTO airportServiceDTO) {
		LCCAirportServiceDTO lccAirportServiceDTO = new LCCAirportServiceDTO();
		lccAirportServiceDTO.setAdultAmount(new BigDecimal(airportServiceDTO.getAdultAmount()));
		lccAirportServiceDTO.setApplicabilityType(airportServiceDTO.getApplicabilityType());
		lccAirportServiceDTO.setChildAmount(new BigDecimal(airportServiceDTO.getChildAmount()));
		lccAirportServiceDTO.setInfantAmount(new BigDecimal(airportServiceDTO.getInfantAmount()));
		lccAirportServiceDTO.setReservationAmount(new BigDecimal(airportServiceDTO.getReservationAmount()));
		lccAirportServiceDTO.setSsrCode(airportServiceDTO.getSsrCode());
		lccAirportServiceDTO.setSsrName(airportServiceDTO.getSsrName());
		lccAirportServiceDTO.setStatus(airportServiceDTO.getStatus());
		lccAirportServiceDTO.setSsrImagePath(airportServiceDTO.getSsrImagePath());
		lccAirportServiceDTO.setSsrThumbnailImagePath(airportServiceDTO.getSsrThumbnailImagePath());
		lccAirportServiceDTO.setSsrID(airportServiceDTO.getSsrID());
		
		if (airportServiceDTO.getDecriptionSelectedLanguage() == null
				|| StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null"))
			lccAirportServiceDTO.setSsrDescription(airportServiceDTO.getDescription());
		else {
			String ucs = StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage());
			ucs = ucs.replace("^", ",");
			lccAirportServiceDTO.setSsrDescription(ucs);
		}

		if (serviceCategory == SSRCategory.ID_AIRPORT_TRANSFER) {
			lccAirportServiceDTO.setProvider(airportServiceDTO.getProvider());
		}

		return lccAirportServiceDTO;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public int getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(int serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	public Map<String, Integer> getServiceCount() {
		return serviceCount;
	}

	public void setServiceCount(Map<String, Integer> serviceCount) {
		this.serviceCount = serviceCount;
	}

	public BasicTrackInfo getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(BasicTrackInfo trackInfo) {
		this.trackInfo = trackInfo;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
