package com.isa.thinair.airproxy.core.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryBookingTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFareMaskTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFinancialTotalsTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryOndChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPassengerTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPaymentTO;

public class ItineraryDecorateUtil {

	public static final String ITINERARY_FARE_MASK = "IT";

	private static boolean PASSENGER_DETAILS_MASK = true;
	private static boolean PAYMENT_DETAILS_MASK = true;
	private static boolean CHARGES_DETAILS_MASK = true;

	private static boolean FARE_MASK = true;
	private static boolean CHARGES_MASK = false;
	private static boolean BALLANCE_MASK = false;
	private static boolean PAID_AMOUNT_MASK = true;

	// TODO:same utility should be used for the Fare masking,currently it has been masked in side the ItineraryUtil.java
	// should be handled for each cases later (TAX,FARE,SUR..)
	
	public static ItineraryDTO applyItineraryFareMaskDecorater(ItineraryDTO itineraryDTO) {
		if(itineraryDTO != null
				&& itineraryDTO.getItineraryFareMaskTO() != null){
					overrideMaskingPriviledges(itineraryDTO.getItineraryFareMaskTO());
		}
		applyIteneraryFareMask(itineraryDTO);
		return itineraryDTO;
	}
	
	private static void overrideMaskingPriviledges(ItineraryFareMaskTO itineraryFareMaskTO){
		
		PASSENGER_DETAILS_MASK = itineraryFareMaskTO.isPassengerDetailsMask();
		PAYMENT_DETAILS_MASK = itineraryFareMaskTO.isPaymentDetailsMask();
		CHARGES_DETAILS_MASK = itineraryFareMaskTO.isChargeDetailsMask();
		
		FARE_MASK = itineraryFareMaskTO.isFareMask();
		CHARGES_MASK = itineraryFareMaskTO.isChargesMask();
		BALLANCE_MASK = itineraryFareMaskTO.isBallanceMask();
		PAID_AMOUNT_MASK = itineraryFareMaskTO.isPaidAmountMask();
	}
	
	private static void applyIteneraryFareMask(ItineraryDTO itineraryDTO) {
		if (PASSENGER_DETAILS_MASK) {
			applyPassengerDetailsMask(itineraryDTO);
		}

		if (PAYMENT_DETAILS_MASK) {
			applyPaymentDetailsMask(itineraryDTO);
		}

		if (CHARGES_DETAILS_MASK) {
			applyChargesDetailsMask(itineraryDTO);
		}
	}

	private static void applyPassengerDetailsMask(ItineraryDTO itineraryDTO) {

		for (ItineraryPassengerTO itineraryPassengerTO : itineraryDTO.getPassengers()) {

			ItineraryFinancialTotalsTO financialTotalsTO = itineraryPassengerTO.getBaseCurrencyFinancials();

			if (financialTotalsTO != null) {
				if (FARE_MASK)
					financialTotalsTO.setTotalFare(ITINERARY_FARE_MASK);

				if (CHARGES_MASK)
					financialTotalsTO.setTotalCharges(ITINERARY_FARE_MASK);

				if (BALLANCE_MASK)
					financialTotalsTO.setTotalBalance(ITINERARY_FARE_MASK);

				if (PAID_AMOUNT_MASK)
					financialTotalsTO.setTotalPaidAmount(ITINERARY_FARE_MASK);
			}

		}

		ItineraryBookingTO itineraryBookingTO = itineraryDTO.getBooking();
		ItineraryFinancialTotalsTO itineraryFinancialTotalsTO = itineraryBookingTO.getBaseCurrencyFinancials();
		ItineraryFinancialTotalsTO paidCurrFinancialTotals = itineraryBookingTO.getPaidCurrencyFinancials();

		if (itineraryFinancialTotalsTO != null) {
			if (FARE_MASK) {
				itineraryFinancialTotalsTO.setTotalFare(ITINERARY_FARE_MASK);
			}

			if (CHARGES_MASK) {
				itineraryFinancialTotalsTO.setTotalCharges(ITINERARY_FARE_MASK);
			}

			if (BALLANCE_MASK) {
				itineraryFinancialTotalsTO.setTotalBalance(ITINERARY_FARE_MASK);
			}

			if (PAID_AMOUNT_MASK) {
				itineraryFinancialTotalsTO.setTotalPaidAmount(ITINERARY_FARE_MASK);
			}
		}

		if (paidCurrFinancialTotals != null) {
			if (FARE_MASK) {
				paidCurrFinancialTotals.setTotalFare(ITINERARY_FARE_MASK);
			}

			if (CHARGES_MASK) {
				paidCurrFinancialTotals.setTotalCharges(ITINERARY_FARE_MASK);
			}

			if (BALLANCE_MASK) {
				paidCurrFinancialTotals.setTotalBalance(ITINERARY_FARE_MASK);
			}

			if (PAID_AMOUNT_MASK) {
				paidCurrFinancialTotals.setTotalPaidAmount(ITINERARY_FARE_MASK);
			}
		}

		itineraryBookingTO.setItinBallanceMask(BALLANCE_MASK);
		itineraryBookingTO.setItinChargesMask(CHARGES_DETAILS_MASK);
		itineraryBookingTO.setItinPaidAmountMask(PAID_AMOUNT_MASK);

	}

	private static void applyPaymentDetailsMask(ItineraryDTO itineraryDTO) {

		for (ItineraryPassengerTO itineraryPassengerTO : itineraryDTO.getPassengers()) {

			Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> payments = itineraryPassengerTO.getPayments();

			if (payments != null && payments.size() > 0) {
				for (ItineraryFinancialTotalsTO financialTotalsTO : payments.keySet()) {

					if (financialTotalsTO != null) {
						for (ItineraryPaymentTO itineraryPaymentTO : payments.get(financialTotalsTO)) {

							if (itineraryPaymentTO != null && PAID_AMOUNT_MASK) {
								itineraryPaymentTO.setPaidAmount(ITINERARY_FARE_MASK);
							}

						}

						if (BALLANCE_MASK)
							financialTotalsTO.setTotalBalance(ITINERARY_FARE_MASK);

						if (CHARGES_MASK)
							financialTotalsTO.setTotalCharges(ITINERARY_FARE_MASK);

						if (FARE_MASK)
							financialTotalsTO.setTotalFare(ITINERARY_FARE_MASK);

						if (PAID_AMOUNT_MASK)
							financialTotalsTO.setTotalPaidAmount(ITINERARY_FARE_MASK);
					}

				}
			}

		}
	}

	private static void applyChargesDetailsMask(ItineraryDTO itineraryDTO) {

		Collection<ItineraryOndChargeTO> ondChargeTOColl = itineraryDTO.getOndCharges();

		if (ondChargeTOColl != null && ondChargeTOColl.size() > 0) {
			for (ItineraryOndChargeTO ondChargeTOs : ondChargeTOColl) {

				for (ItineraryChargeTO charges : ondChargeTOs.getCharges()) {
					// charges.setAmount(ITINERARY_FARE_MASK);
					// charges.setCurrency("");
				}
				ondChargeTOs.setTotal(new BigDecimal(0), true);
			}

		}

	}

}
