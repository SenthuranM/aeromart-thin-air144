package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.util.ONDBaggageUtil;
import com.isa.thinair.airmaster.api.dto.SSRPaxSegmentDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationRequest;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAnciModificationResponse;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.core.utils.transformer.PaymentConvertUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferAssembler;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutomaticCheckinAssembler;
import com.isa.thinair.airreservation.api.dto.baggage.PassengerBaggageAssembler;
import com.isa.thinair.airreservation.api.dto.meal.PassengerMealAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PassengerSeatingAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.model.BundledFareFreeService;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * BL for Ancillary modification
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxyModifyAncillaryBL {

	private final CommonAncillaryModifyAssembler anciAssembler;
	private final TrackInfoDTO trackInfo;
	private final UserPrincipal userPrincipal;
	private ReservationBD reservationBD;
	private ReservationQueryBD resQBD;
	private MealBD mealBD;
	private Integer version;
	private Map<String, PnrFarePassenger> segmentFarePaxMap;
	private Map<Integer, Collection<FlightMealDTO>> segmentMealMap;
	private Map<Integer, Integer> fltSegSeqMap;
	private Map<String, Integer> ssrCodeIdMap;
	private boolean enableFraudCheck = false;
	private final Map<String, Set<String>> flightSegWiseSelectedMeals;
	private final CommonReservationContactInfo contactInfo;
	private final boolean isInterlinePaymentAllowed;
	private Map<Integer, Collection<FlightBaggageDTO>> segmentBaggageMap;
	private BaggageBusinessDelegate baggageBD;
	private boolean autoCnxEnabled;
	private boolean hasBufferTimeAutoCnx;
	private AutoCancellationInfo autoCancellationInfo;
	private Reservation reservation;

	private enum ACTION {
		ADD, REMOVE, UPDATE
	};

	private static Log log = LogFactory.getLog(AirproxyModifyAncillaryBL.class);

	public AirproxyModifyAncillaryBL(CommonAncillaryModifyAssembler anciAssembler, TrackInfoDTO trackInfo,
			UserPrincipal userPrincipal, boolean enableFraudCheck, CommonReservationContactInfo contactInfo,
			boolean isInterlinePaymentAllowed, Map<String, Set<String>> flightSegWiseSelectedMeals) {
		super();
		this.anciAssembler = anciAssembler;
		this.trackInfo = trackInfo;
		this.userPrincipal = userPrincipal;
		this.enableFraudCheck = enableFraudCheck;
		this.contactInfo = contactInfo;
		this.isInterlinePaymentAllowed = isInterlinePaymentAllowed;
		this.flightSegWiseSelectedMeals = flightSegWiseSelectedMeals;
		this.autoCnxEnabled = this.anciAssembler.isAutoCnxEnabled();
		this.hasBufferTimeAutoCnx = this.anciAssembler.isHasBufferTimeAutoCnx();
	}

	public boolean execute() throws ModuleException {

		if (anciAssembler.getTargetSystem() == SYSTEM.INT) {

			LCCAnciModificationRequest anciModificationRequest = new LCCAnciModificationRequest();
			anciModificationRequest.setCommonAncillaryModifyAssembler(anciAssembler);
			anciModificationRequest.setEnableFraudCheck(enableFraudCheck);
			anciModificationRequest.setTrackInfoDTO(trackInfo);
			anciModificationRequest.setUserPrincipal(userPrincipal);
			anciModificationRequest.setContactInfo(contactInfo);
			anciModificationRequest.setInterlinePaymentAllowed(isInterlinePaymentAllowed);
			anciModificationRequest.setFlightRefWiseSelectedMeals(flightSegWiseSelectedMeals);
			anciModificationRequest.setAutoCnxEnabled(autoCnxEnabled);

			LCCAnciModificationResponse anciModificationResponse = AirproxyModuleUtils.getLCCAncillaryBD()
					.modifyAncillary(anciModificationRequest);
			if (!anciModificationResponse.isModifcationSuccess()) {
				log.error("lccclient.modification.ancillary.error");
				throw new ModuleException("lccclient.modification.ancillary.error");
			}

		} else if (anciAssembler.getTargetSystem() == SYSTEM.AA) {
			return ownAirlineAncillaryUpdate(); // return true if we need to create the dummy carries for own booking as
												// well.
		}

		return false; // All other cases we do not need to create the dummy reservation for payments . It should happen
						// through the lcc
	}

	@SuppressWarnings("unchecked")
	private boolean ownAirlineAncillaryUpdate() throws ModuleException {
		reservationBD = AirproxyModuleUtils.getReservationBD();
		this.segmentFarePaxMap = new HashMap<String, PnrFarePassenger>();

		Collection<ReservationPax> resPaxCol = null;

		if (autoCnxEnabled && !anciAssembler.isSkipOwnAutoCnxCalculation()) {
			setAutoCancellationInfo();
			resPaxCol = reservation.getPassengers();
		} else {

			if (autoCnxEnabled && anciAssembler.isSkipOwnAutoCnxCalculation()) {
				autoCancellationInfo = anciAssembler.getAutoCancellationInfo();
			}

			resPaxCol = AirproxyModuleUtils.getPassengerBD().getPassengers(anciAssembler.getPnr(), false);
		}

		Map<Integer, Integer> paxSeqPnrIdMap = new HashMap<Integer, Integer>();
		for (ReservationPax resPax : resPaxCol) {
			paxSeqPnrIdMap.put(resPax.getPaxSequence(), resPax.getPnrPaxId());
		}
		Map<Integer, LCCClientPaymentAssembler> paymentMap = anciAssembler.getPassengerPaymentMap();
		String transactionIdentifier = UUID.randomUUID().toString();

		LoyaltyPaymentInfo loyaltyPaymentInfo = anciAssembler.getLoyaltyPaymentInfo();

		try {
			// capture other carrier payments
			if (ReservationApiUtils.isOtherCarrierPaymentsExist(paymentMap.values())) {
				if (log.isDebugEnabled())
					log.debug("Going to capture other carrier payments for reservation " + anciAssembler.getPnr()
							+ " for modify anci operation");
				paymentMap = AirproxyModuleUtils.getLCCReservationBD().makePayment(contactInfo, anciAssembler.getPnr(),
						transactionIdentifier, paymentMap, userPrincipal, trackInfo);
				if (log.isDebugEnabled())
					log.debug("Successfully captured other carrier payments for reservation " + anciAssembler.getPnr()
							+ " for modify anci operation");
			}

			anciAssembler.setPassengerPaymentMap(paymentMap);
			Set<Integer> paxSeqSet = paymentMap.keySet();
			IPassenger iPassenger = new PassengerAssembler(null);
			IPayment iPaymentPassenger;
			Map extExternalChgDTOMap = getExternalChargesMap(paymentMap.values());
			for (Integer paxSeqId : paxSeqSet) {
				LCCClientPaymentAssembler paxPayAsm = paymentMap.get(paxSeqId);
				iPaymentPassenger = PaymentConvertUtil.populatePerPaxPayment(paxPayAsm, extExternalChgDTOMap);
				iPassenger.addTemporyPaymentEntries(getTemporaryPaymentMap(anciAssembler.getTemporyPaymentMap()));
				iPassenger.addPassengerPayments(paxSeqPnrIdMap.get(paxSeqId), iPaymentPassenger);
			}

			boolean isInsurance = extExternalChgDTOMap.containsKey(ReservationInternalConstants.EXTERNAL_CHARGES.INSURANCE);
			boolean isAnciServiceTax = extExternalChgDTOMap.containsKey(ReservationInternalConstants.EXTERNAL_CHARGES.JN_ANCI)
					|| extExternalChgDTOMap.containsKey(EXTERNAL_CHARGES.SERVICE_TAX);
			boolean isAnciPenalty = extExternalChgDTOMap.containsKey(ReservationInternalConstants.EXTERNAL_CHARGES.ANCI_PENALTY);
			boolean isAdminFeeExist = extExternalChgDTOMap.containsKey(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD);
			boolean isHandlingCharge = extExternalChgDTOMap.containsKey(ReservationInternalConstants.EXTERNAL_CHARGES.HANDLING_CHARGE);
			// check whether other carrier payment existing in the payment for ancillary
			boolean dummyCarrierReconcileNeeded = ReservationApiUtils.isOtherCarrierPaymentsExist(paymentMap.values());
			Reservation reservationBeforeModify = AirproxyLoadReservationBL.loadReservation(anciAssembler.getPnr());
			this.version = Integer.parseInt(anciAssembler.getVersion());
			PaxAdjAssembler paxAdj = updateAncillary(iPassenger, anciAssembler.isForceConfirm(), false);
			if (iPassenger != null) {
				boolean isPaymentNeeded = !isZeroAmountPayment(iPassenger);
				if (isPaymentNeeded || isInsurance || isAnciServiceTax || isAnciPenalty || isAdminFeeExist || isHandlingCharge) {
					if (anciAssembler.isPartialPayment()) {
						reservationBD.updateReservationForPayment(anciAssembler.getPnr(), iPassenger, true, false, version++,
								this.trackInfo, enableFraudCheck, false, true, dummyCarrierReconcileNeeded, false, false,
								loyaltyPaymentInfo, false, paxAdj);
					} else {
						reservationBD.updateReservationForPayment(anciAssembler.getPnr(), iPassenger,
								anciAssembler.isForceConfirm(), false, version++, this.trackInfo, enableFraudCheck, false, true,
								dummyCarrierReconcileNeeded, false,
								isPaymentNeeded && anciAssembler.getReservationStatus().equals(ReservationStatus.ON_HOLD),
								loyaltyPaymentInfo, false, paxAdj);
					}
				}
			}
			reservationBD.detectReservationModification(reservationBeforeModify);
			// Remove auto cancellation flag if balance to pay is <= zero
			reservationBD.removeAutoCancellation(anciAssembler.getPnr(), getTrackInfo());
			return dummyCarrierReconcileNeeded;
		} catch (ModuleException exception) {
			log.error("Exception occured in modify anci operation", exception);

			if (loyaltyPaymentInfo != null) {
				AirproxyModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(anciAssembler.getPnr(),
						loyaltyPaymentInfo.getLoyaltyRewardIds(),
						loyaltyPaymentInfo.getMemberAccountId());
			}

			if (ReservationApiUtils.isOtherCarrierPaymentsExist(paymentMap.values())) {
				if (log.isDebugEnabled())
					log.debug("Going to revert other carrier payments for reservation " + anciAssembler.getPnr()
							+ " for modify anci operation");
				AirproxyModuleUtils.getLCCReservationBD().refundPayment(contactInfo, anciAssembler.getPnr(),
						transactionIdentifier, paymentMap, userPrincipal, trackInfo);
				if (log.isDebugEnabled())
					log.debug("Successfully reverted other carrier payments for reservation " + anciAssembler.getPnr()
							+ " for modify anci operation");
			}

			throw exception;
		}
	}

	private void redeemLoyaltyRewards(String[] rewardIds, AppIndicatorEnum appIndicator, String loyaltyMemberId)
			throws ModuleException {
		if (rewardIds != null && rewardIds.length > 0) {
			ServiceResponce response = ReservationModuleUtils.getLoyaltyManagementBD().redeemIssuedRewards(rewardIds,
					appIndicator, loyaltyMemberId);

			if (!response.isSuccess()) {
				throw new ModuleException("airreservations.loyalty.redeem.failed");
			}
		}
	}

	private void setAutoCancellationInfo() throws ModuleException {
		reservation = AirproxyLoadReservationBL.loadReservation(anciAssembler.getPnr());

		List<FlightSegmentTO> affectedSegments = AncillaryCommonUtil
				.getAutoCancellationAffectedSegments(anciAssembler.getPaxAddAncillaryMap());

		if (affectedSegments != null && !affectedSegments.isEmpty()) {
			IFlightSegment firstDeptFltSeg = null;
			for (IFlightSegment seg : affectedSegments) {
				if (firstDeptFltSeg == null) {
					firstDeptFltSeg = seg;
				} else {
					if (firstDeptFltSeg.getDepartureDateTimeZulu().after(seg.getDepartureDateTimeZulu())) {
						firstDeptFltSeg = seg;
					}
				}
			}

			boolean getExistingCnxTimeOnly = true;
			Date firstDepartFltDate = null;
			if (firstDeptFltSeg != null) {
				firstDepartFltDate = firstDeptFltSeg.getDepartureDateTimeZulu();
				getExistingCnxTimeOnly = false;
			}

			autoCancellationInfo = AirproxyModuleUtils.getReservationBD().getAutoCancellationInfo(anciAssembler.getPnr(),
					reservation, firstDepartFltDate, null, AutoCancellationInfo.AUTO_CNX_TYPE.ANCI_EXP.toString(),
					hasBufferTimeAutoCnx, getExistingCnxTimeOnly);
		} else {
			autoCancellationInfo = null;
		}

	}

	@SuppressWarnings("unchecked")
	private Map getTemporaryPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> cmnPaymentMap) {
		Map<Integer, CardPaymentInfo> cardPayMap = null;
		if (cmnPaymentMap != null) {
			cardPayMap = new HashMap<Integer, CardPaymentInfo>();
			Set<Integer> ids = cmnPaymentMap.keySet();
			for (Integer id : ids) {
				CommonCreditCardPaymentInfo cmnCard = cmnPaymentMap.get(id);

				CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();
				cardPaymentInfo.setType(cmnCard.getType());
				cardPaymentInfo.setTotalAmount(cmnCard.getTotalAmount());
				PayCurrencyDTO payCurrencyDTO = (PayCurrencyDTO) cmnCard.getPayCurrencyDTO().clone();
				cardPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);

				cardPaymentInfo.setAppIndicator(cmnCard.getAppIndicator());
				cardPaymentInfo.setTnxMode(cmnCard.getTnxMode());
				cardPaymentInfo.setIpgIdentificationParamsDTO(cmnCard.getIpgIdentificationParamsDTO());
				cardPaymentInfo.setSuccess(cmnCard.isPaymentSuccess());
				cardPaymentInfo.setNo(cmnCard.getNo());
				cardPaymentInfo.setPaymentBrokerRefNo(cmnCard.getPaymentBrokerId());
				/*
				 * Add generic card payment information This means we don't know the type of the credit card we simply
				 * make it Generic
				 */
				if (PaymentType.CARD_GENERIC.getTypeValue() != cmnCard.getType()) {
					cardPaymentInfo.setEDate(cmnCard.geteDate());
					cardPaymentInfo.setName(cmnCard.getName());
					cardPaymentInfo.setAddress(cmnCard.getAddress());
					cardPaymentInfo.setSecurityCode(cmnCard.getSecurityCode());
				}

				cardPayMap.put(id, cardPaymentInfo);
			}
		}
		return cardPayMap;
	}

	/**
	 * 
	 * @param passengers
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private Map getExternalChargesMap(Collection<LCCClientPaymentAssembler> paymentAsmColl) throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		for (LCCClientPaymentAssembler paymentAssembler : paymentAsmColl) {
			Collection<LCCClientExternalChgDTO> extChgColl = paymentAssembler.getPerPaxExternalCharges();
			for (LCCClientExternalChgDTO extChg : extChgColl) {
				colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
			}
		}

		Map extExternalChgDTOMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
				ChargeRateOperationType.MODIFY_ONLY);
		return extExternalChgDTOMap;
	}

	private PaxAdjAssembler updateAncillary(IPassenger iPassenger, boolean forceConfirm, boolean paymentCompleted)
			throws ModuleException {
		List<String> pnrs = new ArrayList<String>();
		pnrs.add(anciAssembler.getPnr());
		resQBD = AirproxyModuleUtils.getAirReservationQueryBD();
		SeatMapBD seatBD = AirproxyModuleUtils.getSeatMapBD();
		PaxAdjAssembler adjAssembler = new PaxAdjAssembler();
		PassengerSeatingAssembler ipaxSeating = new PassengerSeatingAssembler(null, adjAssembler, forceConfirm);
		PassengerMealAssembler ipaxMeal = new PassengerMealAssembler(null, adjAssembler, forceConfirm);
		PassengerBaggageAssembler ipaxBaggage = new PassengerBaggageAssembler(null, adjAssembler, forceConfirm);
		AirportTransferAssembler ipaxAirportTansfer = new AirportTransferAssembler(adjAssembler, forceConfirm);
		AutomaticCheckinAssembler ipaxAutoCheckin = new AutomaticCheckinAssembler(null, adjAssembler, forceConfirm);

		Map<Integer, SegmentSSRAssembler> paxSSRMap = new HashMap<Integer, SegmentSSRAssembler>();
		Map<Integer, SegmentSSRAssembler> paxAPSMap = new HashMap<Integer, SegmentSSRAssembler>();

		List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();

		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxAddAnciMap = anciAssembler.getPaxAddAncillaryMap();
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxRemAnciMap = anciAssembler.getPaxRemoveAncillaryMap();
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxUpdateAnciMap = anciAssembler.getPaxUpdateAncillaryMap();
		Set<Integer> paxSeqAddSet = paxAddAnciMap.keySet();
		Set<Integer> paxSeqRemSet = paxRemAnciMap.keySet();
		Set<Integer> paxSeqUpdateSet = paxUpdateAnciMap.keySet();

		int seatCount = 0;
		int mealCount = 0;
		int ssrCount = 0;
		int apsCount = 0;
		int aptCount = 0;
		int insCount = 0;
		/**
		 * For Baggage
		 */

		int baggageCount = 0;
		
		/**
		 * for AutoCheckin
		 */
		int autoCknCount = 0;
		
		boolean isRemoveInsurance = false;

		if (autoCnxEnabled && autoCancellationInfo != null) {
			// Save auto cancellation information
			autoCancellationInfo = reservationBD.saveAutoCancellationInfo(autoCancellationInfo, anciAssembler.getPnr(),
					getTrackInfo());
		}

		Set<Integer> insuranceRefNos = new HashSet<Integer>();

		for (Integer paxSeqId : paxSeqAddSet) {
			List<LCCSelectedSegmentAncillaryDTO> anciSegList = paxAddAnciMap.get(paxSeqId);
			SegmentSSRAssembler ipaxSSR = paxSSRMap.get(paxSeqId);
			if (ipaxSSR == null) {
				paxSSRMap.put(paxSeqId, new SegmentSSRAssembler(adjAssembler));
				ipaxSSR = paxSSRMap.get(paxSeqId);
			}

			SegmentSSRAssembler iPaxAPS = paxAPSMap.get(paxSeqId);
			if (iPaxAPS == null) {
				paxAPSMap.put(paxSeqId, new SegmentSSRAssembler(adjAssembler));
				iPaxAPS = paxAPSMap.get(paxSeqId);
			}

			for (LCCSelectedSegmentAncillaryDTO anciSeg : anciSegList) {
				FlightSegmentTO flightSegTO = anciSeg.getFlightSegmentTO();
				Integer flightSegmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegTO.getFlightRefNumber());
				seatCount += populateSeats(ipaxSeating, anciSeg, flightSegmentId, seatBD, ACTION.ADD);
				mealCount += populateMeals(ipaxMeal, anciSeg, flightSegmentId, ACTION.ADD);
				ssrCount += populateSSRs(ipaxSSR, anciSeg, flightSegmentId, ACTION.ADD);
				apsCount += populateAPSs(iPaxAPS, anciSeg, flightSegmentId, ACTION.ADD);
				aptCount += populateAPTs(ipaxAirportTansfer, anciSeg, flightSegmentId, ACTION.ADD);
				// baggageCount += populateBaggage(ipaxBaggage, anciSeg, flightSegmentId, ACTION.ADD);
				// TODO Please Check with Gavinda
				baggageCount += populateONDBaggage(ipaxBaggage, anciSeg, flightSegmentId, ACTION.ADD, anciSegList);
				/**
				 * Add AutoCheckin
				 */
				autoCknCount += populateAutoCheckins(ipaxAutoCheckin, anciSeg, flightSegmentId, seatBD, ACTION.ADD);
				List<LCCInsuranceQuotationDTO> insQuotations = anciSeg.getInsuranceQuotations();
				if (insQuotations != null && !insQuotations.isEmpty()) {
					for (LCCInsuranceQuotationDTO insQuotation : insQuotations) {
						if (insQuotation.getInsuranceRefNumber() != null) {
							insuranceRefNos.add(new Integer(insQuotation.getInsuranceRefNumber()));
						}
					}
				}
			}
		}

		for (Integer paxSeqId : paxSeqRemSet) {
			SegmentSSRAssembler ipaxSSR = paxSSRMap.get(paxSeqId);
			if (ipaxSSR == null) {
				paxSSRMap.put(paxSeqId, new SegmentSSRAssembler(adjAssembler));
				ipaxSSR = paxSSRMap.get(paxSeqId);
			}

			SegmentSSRAssembler iPaxAPS = paxAPSMap.get(paxSeqId);
			if (iPaxAPS == null) {
				paxAPSMap.put(paxSeqId, new SegmentSSRAssembler(adjAssembler));
				iPaxAPS = paxAPSMap.get(paxSeqId);
			}

			List<LCCSelectedSegmentAncillaryDTO> anciSegList = paxRemAnciMap.get(paxSeqId);
			for (LCCSelectedSegmentAncillaryDTO anciSeg : anciSegList) {
				FlightSegmentTO flightSegTO = anciSeg.getFlightSegmentTO();
				Integer flightSegmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegTO.getFlightRefNumber());
				seatCount += populateSeats(ipaxSeating, anciSeg, flightSegmentId, seatBD, ACTION.REMOVE);
				mealCount += populateMeals(ipaxMeal, anciSeg, flightSegmentId, ACTION.REMOVE);
				ssrCount += populateSSRs(ipaxSSR, anciSeg, flightSegmentId, ACTION.REMOVE);
				apsCount += populateAPSs(iPaxAPS, anciSeg, flightSegmentId, ACTION.REMOVE);
				aptCount += populateAPTs(ipaxAirportTansfer, anciSeg, flightSegmentId, ACTION.REMOVE);

				/**
				 * Remove baggage
				 */
				baggageCount += populateONDBaggage(ipaxBaggage, anciSeg, flightSegmentId, ACTION.REMOVE, anciSegList);
				/**
				 * Remove Autocheckcin
				 */
				autoCknCount += populateAutoCheckins(ipaxAutoCheckin, anciSeg, flightSegmentId, seatBD, ACTION.REMOVE);
				
				if (anciSeg.getInsuranceQuotations() != null && !anciSeg.getInsuranceQuotations().isEmpty()) {
					for (LCCInsuranceQuotationDTO quotation : anciSeg.getInsuranceQuotations()) {
						if (quotation.getInsuranceRefNumber() != null) {
							isRemoveInsurance = true;
							break;
						}
					}
				}
			}
		}

		for (Integer paxSeqId : paxSeqUpdateSet) {
			SegmentSSRAssembler ipaxSSR = paxSSRMap.get(paxSeqId);
			if (ipaxSSR == null) {
				paxSSRMap.put(paxSeqId, new SegmentSSRAssembler(adjAssembler));
				ipaxSSR = paxSSRMap.get(paxSeqId);
			}
			List<LCCSelectedSegmentAncillaryDTO> anciSegList = paxUpdateAnciMap.get(paxSeqId);
			for (LCCSelectedSegmentAncillaryDTO anciSeg : anciSegList) {
				FlightSegmentTO flightSegTO = anciSeg.getFlightSegmentTO();
				Integer flightSegmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegTO.getFlightRefNumber());
				// TODO meal
				// mealCount += populateMeals(ipaxMeal, anciSeg, flightSegmentId, ACTION.UPDATE);
				ssrCount += populateSSRs(ipaxSSR, anciSeg, flightSegmentId, ACTION.UPDATE);
			}
		}

		if (insuranceRefNos != null && !insuranceRefNos.isEmpty()) {
			insuranceRequests = getInsuranceRequests(insuranceRefNos, paxSeqAddSet);
			insCount = insuranceRequests.size();
		}

		String pnr = anciAssembler.getPnr();

		// Please keep assending order(from external charge enum) for following calls

		if (apsCount > 0) {
			reservationBD.modifySSRs(paxAPSMap, pnr, "", iPassenger, version++, true, EXTERNAL_CHARGES.AIRPORT_SERVICE,
					getTrackInfo(), false);
		}

		if (aptCount > 0) {
			reservationBD.modifyAirportTransfers(ipaxAirportTansfer, pnr, "", iPassenger, version++, true,
					EXTERNAL_CHARGES.AIRPORT_TRANSFER, getTrackInfo(), false);
		}

		/**
		 * For Baggage
		 */
		if (baggageCount > 0) {
			reservationBD.modifyBaggages(ipaxBaggage, pnr, "", version++, true, getTrackInfo());
		}

		if (insCount > 0) {
			reservationBD.addInsurance(insuranceRequests, pnr, iPassenger, forceConfirm, paymentCompleted);
			version++;
		}

		if (insuranceRefNos.isEmpty() && isRemoveInsurance) {
			reservationBD.removeInsurance(pnr, version++);
		}

		if (mealCount > 0) {
			reservationBD.modifyMeals(ipaxMeal, pnr, "", version++, true, getTrackInfo());
		}

		if (seatCount > 0) {
			reservationBD.modifySeats(ipaxSeating, pnr, "", version++, true, getTrackInfo());
		}

		if (ssrCount > 0) {
			reservationBD.modifySSRs(paxSSRMap, pnr, "", iPassenger, version++, true, EXTERNAL_CHARGES.INFLIGHT_SERVICES,
					getTrackInfo(), false);
		}
		
		/**
		 * For Autocheckin
		 */
		if (autoCknCount > 0) {
			reservationBD.modifyAutoCheckins(ipaxAutoCheckin, pnr, "", version++, true, getTrackInfo());
		}

		return adjAssembler;

	}

	private boolean isZeroAmountPayment(IPassenger iPassenger) {
		PassengerAssembler paxAssembler = (PassengerAssembler) iPassenger;
		Iterator payIterator = paxAssembler.getPassengerPaymentsMap().values().iterator();

		PaymentAssembler payAssembler;
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		while (payIterator.hasNext()) {
			payAssembler = (PaymentAssembler) payIterator.next();
			totalAmount = AccelAeroCalculator.add(totalAmount, payAssembler.getTotalPayAmount());
		}

		return (totalAmount.intValue() > 0) ? false : true;
	}

	private List<IInsuranceRequest> getInsuranceRequests(Set<Integer> insuranceRefNos, Set<Integer> paxSeqSet)
			throws ModuleException {

		List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();

		Collection<ReservationPax> resPaxColl = null;
		ReservationContactInfo contactInfo = null;
		if (autoCnxEnabled && !anciAssembler.isSkipOwnAutoCnxCalculation()) {
			// If auto cancellation is enabled, access required data from already loaded reservation
			resPaxColl = reservation.getPassengers();
			contactInfo = reservation.getContactInfo();
		} else {
			resPaxColl = AirproxyModuleUtils.getPassengerBD().getPassengers(this.anciAssembler.getPnr(), false);
			contactInfo = reservationBD.getContactInfo(this.anciAssembler.getPnr());
		}

		int i = 0;
		for (Integer insuranceRefNo : insuranceRefNos) {

			IInsuranceRequest insuranceRequest = new InsuranceRequestAssembler();

			ReservationInsurance reservationInsurance = reservationBD.getReservationInsurance(insuranceRefNo);

			InsureSegment insureSegment = new InsureSegment();
			insureSegment.setFromAirportCode(reservationInsurance.getOrigin());
			insureSegment.setToAirportCode(reservationInsurance.getDestination());
			insureSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
			insureSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
			insureSegment.setRoundTrip(reservationInsurance.getRoundTrip().equals("Y") ? true : false);

			insuranceRequest.addFlightSegment(insureSegment);
			insuranceRequest.setInsuranceId(reservationInsurance.getInsuranceId());
			insuranceRequest.setAutoCancellationId(getAutoCancellationId());

			BigDecimal amount = reservationInsurance.getQuotedAmount();
			insuranceRequest.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedAmount());

			BigDecimal paxWiseAmount[] = AccelAeroCalculator.roundAndSplit(amount, paxSeqSet.size());

			i = 0;
			for (ReservationPax pax : resPaxColl) {
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT) && paxSeqSet.contains(pax.getPaxSequence())) {
					InsurePassenger insurePassenger = new InsurePassenger();

					insurePassenger.setFirstName(pax.getFirstName());
					insurePassenger.setLastName(pax.getLastName());
					insurePassenger.setDateOfBirth(pax.getDateOfBirth());

					insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
					insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
					insurePassenger.setCity(contactInfo.getCity());
					insurePassenger.setEmail(contactInfo.getEmail());
					insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
					if (contactInfo.getPhoneNo() == null || contactInfo.getPhoneNo().trim().length() == 0
							|| "--".equals(contactInfo.getPhoneNo().trim())) {
						insurePassenger.setHomePhoneNumber(contactInfo.getMobileNo());
					} else {
						insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());
					}
					insuranceRequest.addPassenger(insurePassenger);
					insuranceRequest.addInsuranceCharge(pax.getPaxSequence(), paxWiseAmount[i++]);
				}
			}

			insuranceRequests.add(insuranceRequest);
		}

		return insuranceRequests;
	}

	@SuppressWarnings("unchecked")
	public Integer getSegmentSeq(Integer fltSegId) throws ModuleException {
		if (fltSegSeqMap == null) {
			fltSegSeqMap = new HashMap<Integer, Integer>();
			Collection<ReservationSegment> resSegs = null;

			if (autoCnxEnabled && !anciAssembler.isSkipOwnAutoCnxCalculation()) {
				resSegs = reservation.getSegments();
			} else {
				resSegs = AirproxyModuleUtils.getReservationBD().getSegments(this.anciAssembler.getPnr());
			}

			Set<Integer> flightSegIds = new HashSet<Integer>();
			for (ReservationSegment seg : resSegs) {
				flightSegIds.add(seg.getFlightSegId());
			}
			boolean noDuplicateFlightSegIds = false;
			if (resSegs != null && flightSegIds != null && resSegs.size() == flightSegIds.size()) {
				noDuplicateFlightSegIds = true;
			}

			for (ReservationSegment seg : resSegs) {
				if (noDuplicateFlightSegIds) {
					fltSegSeqMap.put(seg.getFlightSegId(), seg.getSegmentSeq());
				} else {
					if (!seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						fltSegSeqMap.put(seg.getFlightSegId(), seg.getSegmentSeq());
					}
				}
			}
		}
		return fltSegSeqMap.get(fltSegId);
	}

	@SuppressWarnings("unchecked")
	private int getSSRIdForCode(String ssrCode) throws ModuleException {
		if (ssrCodeIdMap == null) {
			ssrCodeIdMap = new HashMap<String, Integer>();
			Collection<SSR> ssrCol = AirproxyModuleUtils.getSsrServiceBD().getSSRs();

			for (SSR ssrItm : ssrCol) {
				ssrCodeIdMap.put(ssrItm.getSsrCode(), ssrItm.getSsrId());
			}
		}
		return ssrCodeIdMap.get(ssrCode);
	}

	/**
	 * populate In-flight services
	 */
	private int populateSSRs(SegmentSSRAssembler ipaxSSR, LCCSelectedSegmentAncillaryDTO anciSeg, Integer flightSegmentId,
			ACTION action) throws ModuleException {
		int count = 0;

		/*
		 * Populate in-flight services
		 */
		if (anciSeg.getSpecialServiceRequestDTOs() != null && anciSeg.getSpecialServiceRequestDTOs().size() > 0) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			Integer pnrPaxId = pnrFarePax.getPnrPaxId();
			for (LCCSpecialServiceRequestDTO ssr : anciSeg.getSpecialServiceRequestDTOs()) {
				if (action == ACTION.ADD) {
					ipaxSSR.addPaxSegmentSSR(getSegmentSeq(flightSegmentId), getSSRIdForCode(ssr.getSsrCode()), ssr.getText(),
							null, ssr.getCharge(), ssr.getSsrCode(), ssr.getDescription(), null, null,
							ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, getAutoCancellationId(), null, null, null);
				} else if (action == ACTION.REMOVE) {
					Integer ppssId = getPPSSId(flightSegmentId, pnrPaxId, getSSRIdForCode(ssr.getSsrCode()), null);
					ipaxSSR.addCanceledSegemntSSRCodeMap(ppssId, ssr.getSsrCode());
					ipaxSSR.addCanceledPaxSegmentSSR(ppssId);
				} else if (action == ACTION.UPDATE) {
					Integer ssrId = getSSRIdForCode(ssr.getSsrCode());
					Integer ppssId = getPPSSId(flightSegmentId, pnrPaxId, ssrId, null);
					ipaxSSR.addUpdatedPaxSegmentSSR(ppssId, ssrId, ssr.getText());
				} else {
					continue;
				}
				count++;
			}
		}

		return count;
	}

	/*
	 * Populate Airport services
	 */
	private int populateAPSs(SegmentSSRAssembler ipaxAPS, LCCSelectedSegmentAncillaryDTO anciSeg, Integer flightSegmentId,
			ACTION action) throws ModuleException {
		int count = 0;

		/*
		 * Populate airport services
		 */
		if (anciSeg.getAirportServiceDTOs() != null && anciSeg.getAirportServiceDTOs().size() > 0) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			Integer pnrPaxId = pnrFarePax.getPnrPaxId();
			for (LCCAirportServiceDTO airportService : anciSeg.getAirportServiceDTOs()) {
				if (action == ACTION.ADD) {
					ipaxAPS.addPaxSegmentSSR(getSegmentSeq(flightSegmentId), getSSRIdForCode(airportService.getSsrCode()), null,
							null, airportService.getServiceCharge(), airportService.getSsrCode(), airportService.getSsrName(),
							null, airportService.getAirportCode(), airportService.getApplyOn(), getAutoCancellationId(), null,
							null, null);
				} else if (action == ACTION.REMOVE) {
					Integer ppssId = getPPSSId(flightSegmentId, pnrPaxId, getSSRIdForCode(airportService.getSsrCode()),
							airportService.getAirportCode());
					if (ppssId != 0) {
						ipaxAPS.addCanceledSegemntSSRCodeMap(ppssId, airportService.getSsrCode());
						ipaxAPS.addCanceledPaxSegmentSSR(ppssId);
					}
				}
				count++;
			}
		}

		return count;
	}

	private int populateAPTs(AirportTransferAssembler ipaxAPT, LCCSelectedSegmentAncillaryDTO anciSeg, Integer flightSegmentId,
			ACTION action) throws ModuleException {
		int count = 0;
		if (anciSeg.getAirportTransferDTOs() != null && anciSeg.getAirportTransferDTOs().size() > 0) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			Integer pnrPaxId = pnrFarePax.getPnrPaxId();
			for (LCCAirportServiceDTO airportTransfer : anciSeg.getAirportTransferDTOs()) {
				if (action == ACTION.ADD) {
					ipaxAPT.addPaxSegmentApt(pnrPaxId, pnrFarePax.getPnrSegmentId(), pnrFarePax.getPnrPaxFareId(),
							airportTransfer.getServiceCharge(), airportTransfer.getAirportTransferId(),
							airportTransfer.getTranferDate(), airportTransfer.getTransferAddress(),
							airportTransfer.getTransferContact(), airportTransfer.getTransferType(),
							airportTransfer.getAirportCode(), getTrackInfo());
				} else if (action == ACTION.REMOVE) {
					ipaxAPT.removePaxSegmentApt(pnrPaxId, pnrFarePax.getPnrSegmentId(), pnrFarePax.getPnrPaxFareId(),
							airportTransfer.getServiceCharge(), airportTransfer.getAirportCode(), getTrackInfo());
				}
				count++;
			}
		}

		return count;
	}
	
	/**
	 * This is to populate automatic checkins
	 * 
	 * @param ipaxAutoCheckin
	 * @param anciSeg
	 * @param flightSegmentId
	 * @param seatBD
	 * @param action
	 * @return
	 * @throws ModuleException
	 */
	private int populateAutoCheckins(AutomaticCheckinAssembler ipaxAutoCheckin, LCCSelectedSegmentAncillaryDTO anciSeg,
			Integer flightSegmentId, SeatMapBD seatBD, ACTION action) throws ModuleException {

		if (anciSeg.getAutomaticCheckinDTOs() != null && anciSeg.getAutomaticCheckinDTOs().size() > 0) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			Integer pnrPaxId = pnrFarePax.getPnrPaxId();
			anciSeg.getAutomaticCheckinDTOs().forEach(
				autoCheckinDTO -> {
					Integer flightSeatId = null;
					Map<String, Integer> seatIdMap;
					if (autoCheckinDTO != null && !StringUtil.isNullOrEmpty(autoCheckinDTO.getSeatCode())) {
						List<String> seatCodes = new ArrayList<String>();
						seatCodes.add(autoCheckinDTO.getSeatCode());
						try {
							seatIdMap = seatBD.getFlightSeatIdMap(flightSegmentId, seatCodes);
							// Setting Flight AM seat Id number for automatic checkin
							flightSeatId = seatIdMap.get(autoCheckinDTO.getSeatCode());
						} catch (Exception e) {
							log.error("Exception on getFlightSeatIdMap " + e.toString());
						}
					}
					if (action == ACTION.ADD) {
						try {
							ipaxAutoCheckin.addPaxSegmentAutoCheckin(pnrPaxId, flightSeatId, pnrFarePax.getPnrSegmentId(),
									pnrFarePax.getPnrPaxFareId(), autoCheckinDTO.getAutomaticCheckinCharge(),
									autoCheckinDTO.getAutoCheckinId(), autoCheckinDTO.getSeatPref(), autoCheckinDTO.getEmail(),
									getTrackInfo());
						} catch (Exception e) {
							log.error("Exception when addPaxSegmentAutoCheckin " + e.toString());
						}
					} else if (action == ACTION.REMOVE) {
						try {
							ipaxAutoCheckin.removePaxSegmentAutoCheckin(pnrPaxId, flightSeatId, pnrFarePax.getPnrSegmentId(),
									pnrFarePax.getPnrPaxFareId(), autoCheckinDTO.getAutomaticCheckinCharge(),
									autoCheckinDTO.getAutoCheckinId(), autoCheckinDTO.getSeatPref(), autoCheckinDTO.getEmail(),
									getTrackInfo());
						} catch (Exception e) {
							log.error("Exception when removePaxSegmentAutoCheckin " + e.toString());
						}
					}

				});
		}
		return anciSeg.getAutomaticCheckinDTOs().size();
	}

	@SuppressWarnings("unchecked")
	private FlightMealDTO getFligthMeal(Integer flightSegmentId, String mealCode, String cabinClassCode, String logicalCabinClass,
			PnrFarePassenger pnrFarePax) throws ModuleException {
		if (mealBD == null) {
			mealBD = AirproxyModuleUtils.getMealBD();
		}
		FlightMealDTO flightMeal = null;
		Collection<FlightMealDTO> mealCollection = null;

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		Map<Integer, Set<String>> flightSegIdWiseSelectedMeal = AncillaryCommonUtil
				.getFlightSegIdWiseSelectedAnci(flightSegWiseSelectedMeals);
		int salesChannelCode = CommonUtil.getChannalCodeFromAppIndicator(trackInfo.getAppIndicator());

		if (pnrFarePax == null) {
			// add
			if (!segmentMealMap.containsKey(flightSegmentId)) {
				flightSegIdWiseCos.put(flightSegmentId, new ClassOfServiceDTO(cabinClassCode, logicalCabinClass, null));
				Map<Integer, List<FlightMealDTO>> mealMap = mealBD.getMeals(flightSegIdWiseCos, flightSegIdWiseSelectedMeal,
						anciAssembler.isSkipCutoverValidation(), null, salesChannelCode);
				segmentMealMap.putAll(mealMap);
			}
			mealCollection = segmentMealMap.get(flightSegmentId);
		} else {
			// remove
			flightSegIdWiseCos.put(flightSegmentId, new ClassOfServiceDTO(cabinClassCode, logicalCabinClass, null));
			Map<Integer, List<FlightMealDTO>> paxReservedMealMap = mealBD.getMeals(flightSegIdWiseCos,
					flightSegIdWiseSelectedMeal, anciAssembler.isSkipCutoverValidation(), pnrFarePax, salesChannelCode);
			mealCollection = paxReservedMealMap.get(flightSegmentId);
		}
		if (mealCollection != null) {
			for (FlightMealDTO meal : mealCollection) {
				if (meal.getMealCode().equalsIgnoreCase(mealCode)) {
					flightMeal = meal;
				}
			}
		}
		return flightMeal;
	}

	private int populateMeals(PassengerMealAssembler iPaxMeal, LCCSelectedSegmentAncillaryDTO anciSeg, Integer flightSegmentId,
			ACTION action) throws ModuleException {
		int count = 0;
		if (anciSeg.getMealDTOs() != null && anciSeg.getMealDTOs().size() > 0) {
			if (this.segmentMealMap == null) {
				this.segmentMealMap = new HashMap<Integer, Collection<FlightMealDTO>>();
			}
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			for (LCCMealDTO meal : anciSeg.getMealDTOs()) {

				FlightMealDTO newlyCreatedDTO = null;
				if (meal.isAnciOffer()) {
					newlyCreatedDTO = createFlightMealChargeIfDoesntExist(meal, flightSegmentId);
				} else {
					newlyCreatedDTO = createFlightMealChargeFromBundledServices(meal, anciSeg.getFlightSegmentTO().getPnrSegId(),
							flightSegmentId);
				}

				FlightMealDTO flightMeal = newlyCreatedDTO == null
						? getFligthMeal(flightSegmentId, meal.getMealCode(), anciSeg.getFlightSegmentTO().getCabinClassCode(),
								anciSeg.getFlightSegmentTO().getLogicalCabinClassCode(),
								(action == ACTION.REMOVE ? pnrFarePax : null))
						: newlyCreatedDTO;

				if (action == ACTION.ADD) {
					iPaxMeal.addMealInfo(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(), flightMeal.getFlightMealId(),
							pnrFarePax.getPnrSegmentId(), meal.getMealCharge(), flightMeal.getMealId(), meal.getAllocatedMeals(),
							getAutoCancellationId(), getTrackInfo());
				} else if (action == ACTION.REMOVE) {
					iPaxMeal.removePassengerFromFlightMeal(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
							pnrFarePax.getPnrSegmentId(), flightMeal.getFlightMealId(), meal.getMealCharge(),
							flightMeal.getMealId(), null, getTrackInfo(), meal.getAllocatedMeals());
				} else {
					continue;
				}
				count++;
			}
		}

		return count;
	}

	private static FlightMealDTO createFlightMealChargeFromBundledServices(LCCMealDTO meal, String pnrSegId, Integer fltSegId)
			throws ModuleException {
		List<BundledFareFreeService> bundledFareFreeServices = ReservationModuleUtils.getBundledFareBD()
				.getOfferedServices(pnrSegId);
		if (bundledFareFreeServices != null && !bundledFareFreeServices.isEmpty()) {
			for (BundledFareFreeService bundledFareFreeService : bundledFareFreeServices) {
				if (bundledFareFreeService.getServiceName().equals(EXTERNAL_CHARGES.MEAL.toString())) {
					return ReservationModuleUtils.getMealBD().createFlightMealTemplateIfDoesntExist(
							bundledFareFreeService.getTemplateId(), fltSegId, meal.getMealCode());
				}
			}
		}
		return null;
	}

	private static FlightMealDTO createFlightMealChargeIfDoesntExist(LCCMealDTO mealDTO, Integer flightSegID)
			throws ModuleException {
		return ReservationModuleUtils.getMealBD().createFlightMealTemplateIfDoesntExist(mealDTO.getOfferedTemplateID(),
				flightSegID, mealDTO.getMealCode());
	}

	private PnrFarePassenger getPnrFarePax(Integer flightSegmentId, int paxSequence) throws ModuleException {
		PnrFarePassenger pnrFarePax = null;
		String key = flightSegmentId + "|" + paxSequence;
		if (segmentFarePaxMap.containsKey(key)) {
			pnrFarePax = segmentFarePaxMap.get(key);
		} else {
			pnrFarePax = resQBD.getPnrFarePassenger(anciAssembler.getPnr(), flightSegmentId, paxSequence);
			segmentFarePaxMap.put(key, pnrFarePax);
		}
		return pnrFarePax;
	}

	private int getPPSSId(int fltSegId, int pnrPaxId, Integer ssrId, String airportCode) {
		Map<String, SSRPaxSegmentDTO> ssrPaxSegMap = new HashMap<String, SSRPaxSegmentDTO>();
		List<SSRPaxSegmentDTO> ssrList = AirproxyModuleUtils.getSsrServiceBD().getSSRPaxSegmentList(this.anciAssembler.getPnr());
		for (SSRPaxSegmentDTO ssrPaxSeg : ssrList) {
			String key = ssrPaxSeg.getFltSegId() + "|" + ssrPaxSeg.getPnrPaxId() + "|" + ssrPaxSeg.getSsrId();
			if (ssrPaxSeg.getAirportCode() != null) {
				key += "|" + ssrPaxSeg.getAirportCode();
			}
			ssrPaxSegMap.put(key, ssrPaxSeg);
		}
		String key = fltSegId + "|" + pnrPaxId + "|" + ssrId;
		if (airportCode != null) {
			key += "|" + airportCode;
		}
		return (ssrPaxSegMap.get(key) != null) ? ssrPaxSegMap.get(key).getPpssId() : 0;
	}

	private int populateSeats(PassengerSeatingAssembler iPaxSeating, LCCSelectedSegmentAncillaryDTO anciSeg,
			Integer flightSegmentId, SeatMapBD seatBD, ACTION action) throws ModuleException {
		LCCAirSeatDTO airSeat = anciSeg.getAirSeatDTO();
		int count = 0;
		if (airSeat != null && airSeat.getSeatNumber() != null && !"".equals(airSeat.getSeatNumber())) {
			BigDecimal charge = airSeat.getSeatCharge();
			List<String> seatCodes = new ArrayList<String>();
			seatCodes.add(airSeat.getSeatNumber());

			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);

			Map<String, Integer> seatIdMap = seatBD.getFlightSeatIdMap(flightSegmentId, seatCodes);
			if (action == ACTION.ADD) {
				iPaxSeating.addPassengertoNewFlightSeat(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
						pnrFarePax.getPnrSegmentId(), seatIdMap.get(airSeat.getSeatNumber()), charge, airSeat.getSeatNumber(),
						null, getAutoCancellationId(), getTrackInfo());
				count++;
			} else if (action == ACTION.REMOVE) {
				iPaxSeating.removePassengerFromFlightSeat(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
						pnrFarePax.getPnrSegmentId(), seatIdMap.get(airSeat.getSeatNumber()), charge, getTrackInfo());
				count++;
			}

		}
		return count;
	}

	/**
	 * @return the trackInfo
	 */
	private TrackInfoDTO getTrackInfo() {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		if (this.trackInfo.getIpAddress() != null) {
			trackInfoDTO.setIpAddress(this.trackInfo.getIpAddress());
		} else {
			trackInfoDTO.setIpAddress(userPrincipal.getIpAddress());
		}
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		return trackInfoDTO;
	}

	private int populateONDBaggage(PassengerBaggageAssembler iPaxBaggage, LCCSelectedSegmentAncillaryDTO anciSeg,
			Integer flightSegmentId, ACTION action, List<LCCSelectedSegmentAncillaryDTO> anciSegList) throws ModuleException {
		int count = 0;
		if (anciSeg.getBaggageDTOs() != null && anciSeg.getBaggageDTOs().size() > 0) {

			List<FlightSegmentTO> flightSegTos = ONDBaggageUtil.getModifySegInfo(anciSegList);

			if (this.segmentBaggageMap == null) {
				this.segmentBaggageMap = new HashMap<Integer, Collection<FlightBaggageDTO>>();
			}
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			for (LCCBaggageDTO baggage : anciSeg.getBaggageDTOs()) {
				FlightBaggageDTO flightBaggage = getFligthBaggage(flightSegmentId, baggage.getBaggageName(),
						anciSeg.getFlightSegmentTO().getCabinClassCode(), anciSeg.getFlightSegmentTO().getLogicalCabinClassCode(),
						flightSegTos, baggage, anciSeg.getFlightSegmentTO().getBaggageONDGroupId());

				if (action == ACTION.ADD) {
					iPaxBaggage.addBaggageInfo(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
							flightBaggage.getFlightBaggageId(), pnrFarePax.getPnrSegmentId(), baggage.getBaggageCharge(),
							flightBaggage.getBaggageId(), flightBaggage.getChargeId(), flightBaggage.getOndGroupId(),
							getAutoCancellationId(), getTrackInfo());
				} else if (action == ACTION.REMOVE) {
					iPaxBaggage.removePassengerFromFlightBaggage(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
							pnrFarePax.getPnrSegmentId(), flightBaggage.getFlightBaggageId(), baggage.getBaggageCharge(),
							flightBaggage.getBaggageId(), null, getTrackInfo());
				} else {
					continue;
				}
				count++;
			}
		}

		return count;
	}

	/**
	 * 
	 * @param iPaxBaggage
	 * @param anciSeg
	 * @param flightSegmentId
	 * @param action
	 * @return
	 * @throws ModuleException
	 */
	private int populateBaggage(PassengerBaggageAssembler iPaxBaggage, LCCSelectedSegmentAncillaryDTO anciSeg,
			Integer flightSegmentId, ACTION action) throws ModuleException {
		int count = 0;
		if (anciSeg.getBaggageDTOs() != null && anciSeg.getBaggageDTOs().size() > 0) {
			if (this.segmentBaggageMap == null) {
				this.segmentBaggageMap = new HashMap<Integer, Collection<FlightBaggageDTO>>();
			}
			Integer paxSeq = PaxTypeUtils.getPaxSeq(anciSeg.getTravelerRefNumber());
			PnrFarePassenger pnrFarePax = getPnrFarePax(flightSegmentId, paxSeq);
			for (LCCBaggageDTO baggage : anciSeg.getBaggageDTOs()) {
				FlightBaggageDTO flightBaggage = getFligthBaggage(flightSegmentId, baggage.getBaggageName(),
						anciSeg.getFlightSegmentTO().getCabinClassCode(), anciSeg.getFlightSegmentTO().getLogicalCabinClassCode(),
						null, null, baggage.getOndBaggageGroupId());

				if (action == ACTION.ADD) {
					iPaxBaggage.addBaggageInfo(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
							flightBaggage.getFlightBaggageId(), pnrFarePax.getPnrSegmentId(), baggage.getBaggageCharge(),
							flightBaggage.getBaggageId(), flightBaggage.getChargeId(), flightBaggage.getOndGroupId(),
							getAutoCancellationId(), getTrackInfo());
				} else if (action == ACTION.REMOVE) {
					iPaxBaggage.removePassengerFromFlightBaggage(pnrFarePax.getPnrPaxId(), pnrFarePax.getPnrPaxFareId(),
							pnrFarePax.getPnrSegmentId(), flightBaggage.getFlightBaggageId(), baggage.getBaggageCharge(),
							flightBaggage.getBaggageId(), null, getTrackInfo());
				} else {
					continue;
				}
				count++;
			}
		}

		return count;
	}

	@SuppressWarnings("unchecked")
	private FlightBaggageDTO getFligthBaggage(Integer flightSegmentId, String baggageName, String cabinClassCode,
			String logicalCabinClass, List<FlightSegmentTO> flightSegTos, LCCBaggageDTO lccBaggage, String baggageONDGroupId)
			throws ModuleException {
		boolean baggageFetched = false;
		if (baggageBD == null) {
			baggageBD = AirproxyModuleUtils.getBaggageBusinessDelegate();
		}
		FlightBaggageDTO flightBaggage = null;
		Collection<FlightBaggageDTO> baggageCollection = null;
		if (segmentBaggageMap.containsKey(flightSegmentId)) {
			for (FlightBaggageDTO baggageDTO : segmentBaggageMap.get(flightSegmentId)) {
				if (baggageDTO.getChargeId().equals(Integer.parseInt(lccBaggage.getOndBaggageChargeId()))) {
					baggageFetched = true;
				}
			}
		} else {
			baggageFetched = false;
			for (FlightSegmentTO flightSegmentTO : flightSegTos) {
				segmentBaggageMap.put(flightSegmentTO.getFlightSegId(), new ArrayList<FlightBaggageDTO>());
			}
		}

		if (!baggageFetched) {
			Map<Integer, ClassOfServiceDTO> flightSegWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			flightSegWiseCos.put(flightSegmentId, new ClassOfServiceDTO(cabinClassCode, logicalCabinClass, null));
			Map<Integer, List<FlightBaggageDTO>> baggageMap = null;
			if (flightSegTos != null && AppSysParamsUtil.isONDBaggaeEnabled()) {
				baggageMap = baggageBD.getBaggage(flightSegTos, Integer.parseInt(lccBaggage.getOndBaggageChargeId()));
			} else {
				baggageMap = baggageBD.getBaggages(flightSegWiseCos, false, anciAssembler.isSkipCutoverValidation());
			}

			for (FlightSegmentTO flightSegmentTO : flightSegTos) {
				if (baggageMap.get(flightSegmentTO.getFlightSegId()) != null) {
					segmentBaggageMap.get(flightSegmentTO.getFlightSegId())
							.addAll(baggageMap.get(flightSegmentTO.getFlightSegId()));
				}
			}
		}
		baggageCollection = segmentBaggageMap.get(flightSegmentId);
		if (baggageCollection != null) {
			for (FlightBaggageDTO baggage : baggageCollection) {
				if (baggage.getBaggageName().equalsIgnoreCase(baggageName)) {
					flightBaggage = baggage;
					if (lccBaggage != null) {
						flightBaggage.setAmount(lccBaggage.getBaggageCharge());
					}
				}
				baggage.setOndGroupId(baggageONDGroupId);
			}
			if (flightBaggage == null) {
				baggageCollection = baggageBD.getSoldBaggages(flightSegmentId);
				for (FlightBaggageDTO baggage : baggageCollection) {
					if (baggage.getBaggageName().equalsIgnoreCase(baggageName)) {
						flightBaggage = baggage;
						if (lccBaggage != null) {
							flightBaggage.setAmount(lccBaggage.getBaggageCharge());
						}
					}
				}
			}
		}
		return flightBaggage;
	}

	private Integer getAutoCancellationId() {
		return (autoCancellationInfo == null) ? null : autoCancellationInfo.getAutoCancellationId();
	}

}
