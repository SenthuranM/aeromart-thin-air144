package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.TempBlockSeatUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airproxy.core.utils.transformer.PaymentConvertUtil;
import com.isa.thinair.airproxy.core.utils.transformer.ReservationConvertUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Mediate the reservation between interline and own airline
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxyReservationBL {

	private static Log log = LogFactory.getLog(AirproxyReservationBL.class);
	private static final String RESERVATION_CREATION = "creation";

	/**
	 * Make the reservation and return the reservation At the moment interline and own airline reservations are mutually
	 * exclusive
	 * 
	 * @param commonReservationAssembler
	 * @param userPrincipal
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public static ServiceResponce makeReservation(CommonReservationAssembler commonReservationAssembler,
			UserPrincipal userPrincipal, TrackInfoDTO trackInfoDTO) throws Exception {
		Map<Integer, LCCClientPaymentAssembler> paxWisePaymentAssemblers = new HashMap<Integer, LCCClientPaymentAssembler>();

		String pnr = commonReservationAssembler.getLccreservation().getPNR();

		try {

			if (validateTBA(commonReservationAssembler)) {
				log.error("PNR:" + commonReservationAssembler.getLccreservation().getPNR() + " INVALID TBA Booking!");
				throw new ModuleException("airreservations.invalid.tba.booking");
			}

			if (commonReservationAssembler.getTargetSystem() == SYSTEM.INT) {
				LCCClientReservation reservation = AirproxyModuleUtils.getLCCReservationBD().book(commonReservationAssembler,
						trackInfoDTO);
				DefaultServiceResponse serviceResponce = new DefaultServiceResponse(true);
				serviceResponce.setResponseCode(CommandParamNames.RESERVATION);
				serviceResponce.addResponceParam(CommandParamNames.RESERVATION, reservation);
				serviceResponce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED, false);

				if (reservation.getLccOfflinePaymentDetails() != null) {
					serviceResponce.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER,
							reservation.getLccOfflinePaymentDetails().getPaymentGatewayReference());
					serviceResponce.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY,
							reservation.getLccOfflinePaymentDetails().getPaymentGatewayReferenceKey());
				}
				return serviceResponce;
			} else {

				/**
				 * We can execute the RSB.makepayment method for all the requests and skip calling LCC if it does not
				 * have other carrier payments.instead of this Preferred this way because we can filter it earliest
				 */

				if (ReservationApiUtils.isOtherCarrierPaymentsExist(commonReservationAssembler)) {
					// This is for external carrier credit capturing. We need to handle this only for own airline
					// bookings.
					// for interline bookings credit capturing will be taken place in book request itself
					commonReservationAssembler.setUseOtherCarrierPaxCredit(true);
					if (commonReservationAssembler.getLccreservation().getPNR() == null
							|| commonReservationAssembler.getLccreservation().getPNR().equals("")) {
						pnr = AirproxyModuleUtils.getReservationBD().getNewPNR(false, null);
						commonReservationAssembler.getLccreservation().setPNR(pnr);
					}

					for (LCCClientReservationPax lccClientReservationPax : commonReservationAssembler.getLccreservation()
							.getPassengers()) {
						if (!lccClientReservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
							// Note : had to introduce clone method as in local object references are not working
							// properly.
							// But they worked in cluster build in test
							paxWisePaymentAssemblers.put(lccClientReservationPax.getPaxSequence(),
									lccClientReservationPax.getLccClientPaymentAssembler().clone());
						}
					}

					if (log.isDebugEnabled()) {
						log.debug("Going to capture other carrier payments for reservation "
								+ commonReservationAssembler.getLccreservation().getPNR() + " for create reservation operation");
					}
					// Make the payment
					paxWisePaymentAssemblers = AirproxyModuleUtils.getLCCReservationBD().makePayment(
							commonReservationAssembler.getLccreservation().getContactInfo(),
							commonReservationAssembler.getLccreservation().getPNR(),
							commonReservationAssembler.getLccTransactionIdentifier(), paxWisePaymentAssemblers, userPrincipal,
							trackInfoDTO);

					if (log.isDebugEnabled()) {
						log.debug("Successfully captured other carrier payments for reservation "
								+ commonReservationAssembler.getLccreservation().getPNR() + " for create reservation operation ");
					}

					// Combine payments to reservation assembler
					commonReservationAssembler = PaymentConvertUtil.combineActualCreditPayments(paxWisePaymentAssemblers,
							commonReservationAssembler);
				}
				return makeOwnAirlineReservation(commonReservationAssembler, trackInfoDTO, userPrincipal);
			}
		} catch (Exception exception) {
			log.error("Generic exception in make reservation", exception);

			if (commonReservationAssembler.getCarrierWiseLoyaltyPaymentMap() != null) {
				for (LoyaltyPaymentInfo carrierLoyaltyPaymentInfo : commonReservationAssembler.getCarrierWiseLoyaltyPaymentMap()
						.values()) {
					ReservationModuleUtils.getLoyaltyManagementBD().cancelRedeemedLoyaltyPoints(pnr,
							carrierLoyaltyPaymentInfo.getLoyaltyRewardIds(), carrierLoyaltyPaymentInfo.getMemberAccountId());
				}
			}

			if (commonReservationAssembler.getTargetSystem() != SYSTEM.INT
					&& ReservationApiUtils.isOtherCarrierPaymentsExist(commonReservationAssembler)) {
				// we have to revert other carrier payments. We can do this in airreservation module if required.
				// We will need to do that if we are enabling credit card payments from any OC or MC.
				// Since we have all the details here we can proceed with that
				if (log.isDebugEnabled()) {
					log.debug("Going to revert other carrier payments for reservation "
							+ commonReservationAssembler.getLccreservation().getPNR() + " for create reservation operation");
				}
				AirproxyModuleUtils.getLCCReservationBD().refundPayment(
						commonReservationAssembler.getLccreservation().getContactInfo(),
						commonReservationAssembler.getLccreservation().getPNR(),
						commonReservationAssembler.getLccTransactionIdentifier(), paxWisePaymentAssemblers, userPrincipal,
						trackInfoDTO);
				if (log.isDebugEnabled()) {
					log.debug("Successfully reverted other carrier payments for reservation "
							+ commonReservationAssembler.getLccreservation().getPNR() + " for create reservation operation ");
				}
			}
			throw exception;
		}
	}

	private static boolean validateTBA(CommonReservationAssembler commonReservationAssembler) {
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(commonReservationAssembler.getAppIndicator())) {
			for (LCCClientReservationPax pax : commonReservationAssembler.getLccreservation().getPassengers()) {
				if (pax.isTBA()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Make own airline reservation given the CommonReservationAssembler
	 * 
	 * @param commonReservationAssembler
	 * @param trackInfoDTO
	 * @param userPrincipal
	 * @return
	 * @throws ModuleException
	 */
	private static ServiceResponce makeOwnAirlineReservation(CommonReservationAssembler commonReservationAssembler,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) throws ModuleException {
		FlightPriceRQ priceQuoteRQ = commonReservationAssembler.getFlightPriceRQ();
		FareSegChargeTO fareSegChargeTO = commonReservationAssembler.getSelectedFareSegChargeTO();
		boolean isFlexiQuote = commonReservationAssembler.isFlexiQuote();
		Collection<OndFareDTO> collFares = null;
		IReservation iReservation = null;
		ReservationBD reservationDelegate = AirproxyModuleUtils.getReservationBD();
		OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(priceQuoteRQ, fareSegChargeTO);

		try {
			collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria);
		} catch (ModuleException ex) {
			Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap = commonReservationAssembler.getTemporaryPaymentMap();
			// If IBE res fails with without seat availability in this stage we need to refund the CC payment
			if (temporyPaymentMap != null
					&& SalesChannelsUtil.isAppIndicatorWebOrMobile(commonReservationAssembler.getAppIndicator())) {
				for (int tempPaymentID : temporyPaymentMap.keySet()) {
					if (temporyPaymentMap.get(tempPaymentID).getTemporyPaymentId() != null) {
						try {
							iReservation = ReservationAssemblerConvertUtil.getReservationAssembler(commonReservationAssembler);
							reservationDelegate.intermediateRefund(iReservation, trackInfoDTO, ex.getExceptionCode());
						} catch (Exception ext) {
							log.error("### intermediateRefund :", ext);
						}
						break;
					}
				}
			}
			throw ex;
		}

		// IPaxCountAssembler paxUtil = new
		// PaxCountAssembler(priceQuoteRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		// FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(collFares, paxUtil, userPrincipal.getAgentCode(),
		// commonReservationAssembler.getAppIndicator().toString());
		iReservation = ReservationAssemblerConvertUtil.getReservationAssembler(collFares, commonReservationAssembler);

		ServiceResponce serviceResponce = null;
		Collection<TempSegBcAlloc> collBlockID = null;

		if (commonReservationAssembler.isOnHoldBooking()
				|| AppIndicatorEnum.APP_XBE.equals(commonReservationAssembler.getAppIndicator())
				|| AppIndicatorEnum.APP_WS.equals(commonReservationAssembler.getAppIndicator())
				|| (SalesChannelsUtil.isAppIndicatorWebOrMobile(commonReservationAssembler.getAppIndicator()) && fareSegChargeTO
						.hasBlockSeats())) {
			// For IBE we need to set block seats only if it's done before the payment else it will be done in the
			// MakePayment stage
			if (!fareSegChargeTO.hasBlockSeats()) {
				collBlockID = reservationDelegate.blockSeats(collFares, null);
			} else {// Getting the block seats form the the session for early block seats.
				collBlockID = fareSegChargeTO.getBlockSeatIds();
				if (TempBlockSeatUtil.isExpired(collBlockID)) {
					reservationDelegate.releaseBlockedSeats(collBlockID, true);
					collBlockID = reservationDelegate.blockSeats(collFares, null);
				}
			}
		}
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(commonReservationAssembler.getAppIndicator())) {
			if (commonReservationAssembler.isOnHoldBooking()) {
				serviceResponce = reservationDelegate.createOnHoldReservation(iReservation, collBlockID, false, trackInfoDTO,
						true);
			} else {
				serviceResponce = reservationDelegate.createWebReservation(iReservation, collBlockID, trackInfoDTO);
			}
		} else {
			if (commonReservationAssembler.isOnHoldBooking()) {
				serviceResponce = reservationDelegate.createOnHoldReservation(iReservation, collBlockID, false, trackInfoDTO,
						true);
			} else if (commonReservationAssembler.isWaitListingBooking()) {
				serviceResponce = reservationDelegate.createOnHoldReservation(iReservation, collBlockID, false, trackInfoDTO,
						true);
			} else if (commonReservationAssembler.isForceConfirm()) {
				serviceResponce = reservationDelegate.createOnHoldReservation(iReservation, collBlockID, true, trackInfoDTO,
						true);
			} else if (commonReservationAssembler.isSplitBooking()) {
				serviceResponce = reservationDelegate.createSplitReservation(iReservation, collBlockID, trackInfoDTO);
			} else {
				serviceResponce = reservationDelegate.createCCReservation(iReservation, collBlockID, trackInfoDTO,
						AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
						commonReservationAssembler.isActualPayment());
			}
		}
		String pnr = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);
		Collection<String> strSegIds = new ArrayList<String>();
		Set<String> flightIds = new HashSet<String>();
		String bookingClasses = "";
		for (OndFareDTO ondFareDTO : collFares) {
			bookingClasses += ondFareDTO.getFareSummaryDTO().getBookingClassCode() + ", ";
			for (Entry<Integer, FlightSegmentDTO> entry : ondFareDTO.getSegmentsMap().entrySet()) {
				strSegIds.add(entry.getKey().toString());
				flightIds.add(String.valueOf(entry.getValue().getFlightId()));
			}
			// do over booking inventory audit
			if (!ondFareDTO.isFlown() && fareSegChargeTO.hasBlockSeats() && collBlockID != null && collBlockID.size() > 0) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
					Collection<SeatDistribution> seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
					for (SeatDistribution seatDist : seatDistributions) {
						if (seatDist.isOverbook() && !seatDist.isSameBookingClassAsExisting()
								&& !seatDist.isBookedFlightCabinBeingSearched() && !segmentSeatDistsDTO.isFixedQuotaSeats()) {
							Collection<String> strFltSegIds = new ArrayList<String>();
							strFltSegIds.add(String.valueOf(segmentSeatDistsDTO.getFlightSegId()));
							ReservationModuleUtils.getFlightInventoryBD().doAuditOverBookings(strFltSegIds,
									segmentSeatDistsDTO.getBookingCode(), null, pnr,
									String.valueOf(segmentSeatDistsDTO.getFlightId()), userPrincipal.getUserId(),
									seatDist.getNoOfSeats());
						}
					}
				}
			}
		}

		if (commonReservationAssembler.isSplitBooking()) {
			pnr = (String) serviceResponce.getResponseParam(CommandParamNames.CONFIRM_PNR);
		}

		LCCClientPnrModesDTO pnrModesDTO = populatePnrModesDTO(pnr);
		LCCClientPnrModesDTO pnrSplitModesDTO = null;

		Reservation reservation = null;
		Reservation ohdReservation = null;

		if (commonReservationAssembler.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)) {
			if (transferReservation(commonReservationAssembler, userPrincipal)) {
				reservation = reservationDelegate.getReservation(pnrModesDTO, null);
				reservationDelegate.transferOwnerShip(pnr, commonReservationAssembler.getAgentCode(), reservation.getVersion(),
						trackInfoDTO);
				if (commonReservationAssembler.isSplitBooking()) {
					String strOnHoldPNR = (String) serviceResponce.getResponseParam(CommandParamNames.ON_HOLD_PNR);
					pnrSplitModesDTO = populatePnrModesDTO(strOnHoldPNR);
					ohdReservation = reservationDelegate.getReservation(pnrSplitModesDTO, null);
					reservationDelegate.transferOwnerShip(strOnHoldPNR, commonReservationAssembler.getAgentCode(),
							reservation.getVersion(), trackInfoDTO);
				}
			}
		}

		/* load the reservation if it's not already loaded */
		if (reservation == null) {
			reservation = reservationDelegate.getReservation(pnrModesDTO, null);
		}

		if (commonReservationAssembler.isSplitBooking() && ohdReservation == null) {
			String strOnHoldPNR = (String) serviceResponce.getResponseParam(CommandParamNames.ON_HOLD_PNR);
			pnrSplitModesDTO = populatePnrModesDTO(strOnHoldPNR);
			ohdReservation = reservationDelegate.getReservation(pnrSplitModesDTO, null);
		}
		LCCClientReservation ownReservation = ReservationConvertUtil.transform(reservation, AirproxyModuleUtils.getAirportBD());
		if (ownReservation.getPassengers().size() >= AppSysParamsUtil.getMaximumPassengerCount()) {
			AirproxyModuleUtils.getReservationBD().sendGroupBookingNotificationMessages(ownReservation, RESERVATION_CREATION);
		}
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.setResponseCode(CommandParamNames.RESERVATION);
		responce.addResponceParam(CommandParamNames.RESERVATION, ownReservation);
		responce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
				commonReservationAssembler.isUseOtherCarrierPaxCredit());
		responce.addResponceParam(CommandParamNames.BOOKING_TYPE, commonReservationAssembler.getBookingType());
		responce.addResponceParam(CommandParamNames.TAX_INVOICE_NUMBERS, serviceResponce.getResponseParam(CommandParamNames.TAX_INVOICE_NUMBERS));
		responce.addResponceParam(CommandParamNames.CMI_FATOURATI_REF, serviceResponce.getResponseParam(CommandParamNames.CMI_FATOURATI_REF));
		responce.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER,
				serviceResponce.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER));
		responce.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY,
				serviceResponce.getResponseParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY));

		if (commonReservationAssembler.isSplitBooking()) {
			LCCClientReservation ownOhdReservation = ReservationConvertUtil.transform(ohdReservation,
					AirproxyModuleUtils.getAirportBD());
			responce.setResponseCode(CommandParamNames.OHD_RESERVATION);
			responce.addResponceParam(CommandParamNames.OHD_RESERVATION, ownOhdReservation);
			responce.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED,
					commonReservationAssembler.isUseOtherCarrierPaxCredit());
		}

		return responce;
	}

	/**
	 * if on hold booking is done for user agent other than the logged in agent transfer the reservation
	 * 
	 * @param commonReservationAssembler
	 * @param userPrincipal
	 * @return
	 */
	private static boolean transferReservation(CommonReservationAssembler commonReservationAssembler,
			UserPrincipal userPrincipal) {
		return (commonReservationAssembler.isOnHoldBooking()
				&& !userPrincipal.getAgentCode().equals(commonReservationAssembler.getAgentCode()));
	}

	/**
	 * Form the PnrModesDTO to query the reservation
	 * 
	 * @param pnr
	 * @return
	 */
	private static LCCClientPnrModesDTO populatePnrModesDTO(String pnr) {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setGroupPNR(null);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadLocalTimes(false);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setSkipPromoAdjustment(AppSysParamsUtil.isPromoCodeEnabled());

		return pnrModesDTO;
	}

}