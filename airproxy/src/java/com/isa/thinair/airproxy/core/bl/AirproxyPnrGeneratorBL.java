package com.isa.thinair.airproxy.core.bl;

import java.util.Collections;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AirproxyPnrGeneratorBL {

	public static String generateNewPnr(List<FlightSegmentTO> flightSegments, BasicTrackInfo trackInfo)
					throws ModuleException {

		String originatingCarrierCode = getOriginatorCarrierCode(flightSegments);

		String groupPnr = null;

		if (AppSysParamsUtil.getDefaultCarrierCode().equals(originatingCarrierCode)) {
			groupPnr = AirproxyModuleUtils.getReservationBD().getNewPNR(false, null);
		} else {
			groupPnr = AirproxyModuleUtils.getLCCReservationBD().generateNewPnr(originatingCarrierCode, trackInfo);
		}

		return groupPnr;
	}

	private static String getOriginatorCarrierCode(List<FlightSegmentTO> flightSegmentTOs) {
		String originatorCarrierCode = null;
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			Collections.sort(flightSegmentTOs);
			FlightSegmentTO flightSegment = flightSegmentTOs.get(0);
			if (flightSegment != null && flightSegment.getFlightNumber() != null && flightSegment.getFlightNumber().length() > 2) {
				originatorCarrierCode = flightSegment.getFlightNumber().substring(0, 2);
			}
		}
		return originatorCarrierCode;
	}

}
