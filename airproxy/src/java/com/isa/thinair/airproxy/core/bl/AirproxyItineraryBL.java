package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.core.utils.ItineraryUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

public class AirproxyItineraryBL {

	public static String getItineraryForPrint(LCCClientReservation lccClientReservation,
			CommonItineraryParamDTO commonItineraryParam, ReservationSearchDTO reservationSearchDTO) throws ModuleException {

		authorizeReservationAccess(lccClientReservation, reservationSearchDTO);
		// TODO Auto-generated method stub
		ItineraryDTO itineraryDTO = new ItineraryDTO();
		if (SalesChannelsUtil.isAppEngineWebOrMobile(commonItineraryParam.getAppIndicator())) {
			itineraryDTO.setItineraryLanguage(getConfiguredLanguageForInternationalFlightBookings(
					commonItineraryParam.getItineraryLanguage(), lccClientReservation.getSegments()));
		} else {
			itineraryDTO.setItineraryLanguage(commonItineraryParam.getItineraryLanguage());
		}
		itineraryDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
		itineraryDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
		itineraryDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
		itineraryDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
		itineraryDTO.setIncludeFlightBaggageAllowance(commonItineraryParam.isIncludeBaggageAllowance());
		itineraryDTO.setIncludePaxContactDetails(commonItineraryParam.isIncludePaxContactDetails());
		itineraryDTO.setIncludeStationContactDetails(commonItineraryParam.isIncludeStationContactDetails());

		itineraryDTO.setShowDOB(AppSysParamsUtil.showDOBInItinerary());
		itineraryDTO.setShowETicketPerPax(AppSysParamsUtil.isShowPaxETKT()
				&& !lccClientReservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD));
		itineraryDTO.setShowFOID(AppSysParamsUtil.showPassportInItinerary());
		itineraryDTO.setStation(commonItineraryParam.getStation());
		itineraryDTO.setAppIndicator(commonItineraryParam.getAppIndicator().toString());
		itineraryDTO.setSelectedPaxDetails(commonItineraryParam.getSelectedPaxDetails());
		itineraryDTO.setShowCOS(true);
		itineraryDTO.setShowLogicalCC(AppSysParamsUtil.isLogicalCabinClassEnabled());
		itineraryDTO.setShowCheckInClosingTime(showCheckInClosingTimeInItinerary(lccClientReservation.getSegments()));
		itineraryDTO.setIncludePaxCreditExpiryDate(showPassengerCreditExpiryDate(lccClientReservation.getPassengers()));
		itineraryDTO.setForced(isThereBalanceToPay(lccClientReservation.getPassengers()));
		itineraryDTO.setShowBundledService(AppSysParamsUtil.isBundledFaresEnabled(commonItineraryParam.getAppIndicator()
				.toString()));
		itineraryDTO.setShowAeromartPayFares(commonItineraryParam.isAmountMaskingForcePriviledge());
		if (lccClientReservation.getItineraryFareMask() != null && lccClientReservation.getItineraryFareMask().equals("Y")) {
			itineraryDTO.setFareMasked(true);
		}
		if (lccClientReservation.getLccPromotionInfoTO() != null) {
			itineraryDTO.setShowDiscountAmounts(AppSysParamsUtil.isShowDiscountOnItinerary());
			if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(lccClientReservation.getLccPromotionInfoTO()
					.getDiscountAs())) {
				itineraryDTO.setCreditDiscount(true);
			}
		}

		itineraryDTO = ItineraryUtil.createInterlineItinerary(itineraryDTO, lccClientReservation,
				commonItineraryParam.getAirportMap(), itineraryDTO.getItineraryLanguage());
		return AirproxyModuleUtils.getReservationBD().getInterlineItineraryForPrint(itineraryDTO);
	}

	public static void sendEmailItinerary(LCCClientReservation lccClientReservation,
			CommonItineraryParamDTO commonItineraryParam, TrackInfoDTO trackInfoDTO) throws ModuleException {
		ItineraryDTO itineraryDTO = new ItineraryDTO();
		if (SalesChannelsUtil.isAppEngineWebOrMobile(commonItineraryParam.getAppIndicator())) {
			itineraryDTO.setItineraryLanguage(getConfiguredLanguageForInternationalFlightBookings(
					commonItineraryParam.getItineraryLanguage(), lccClientReservation.getSegments()));
		} else {
			itineraryDTO.setItineraryLanguage(commonItineraryParam.getItineraryLanguage());
		}
		itineraryDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
		itineraryDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
		itineraryDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
		itineraryDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
		itineraryDTO.setIncludeFlightBaggageAllowance(commonItineraryParam.isIncludeBaggageAllowance());
		itineraryDTO.setShowDOB(AppSysParamsUtil.showDOBInItinerary());
		itineraryDTO.setShowETicketPerPax(AppSysParamsUtil.isShowPaxETKT()
				&& !lccClientReservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD));
		itineraryDTO.setShowFOID(AppSysParamsUtil.showPassportInItinerary());
		itineraryDTO.setStation(commonItineraryParam.getStation());
		itineraryDTO.setAppIndicator(commonItineraryParam.getAppIndicator().toString());
		itineraryDTO.setTrackInfoDTO(trackInfoDTO);
		itineraryDTO.setSelectedPaxDetails(commonItineraryParam.getSelectedPaxDetails());
		itineraryDTO.setShowCOS(true);
		itineraryDTO.setShowLogicalCC(AppSysParamsUtil.isLogicalCabinClassEnabled());
		itineraryDTO.setShowCheckInClosingTime(showCheckInClosingTimeInItinerary(lccClientReservation.getSegments()));
		itineraryDTO.setIncludeEndorsements(AppSysParamsUtil.isEndorsementsEnabled());
		itineraryDTO.setIncludePaxCreditExpiryDate(showPassengerCreditExpiryDate(lccClientReservation.getPassengers()));
		itineraryDTO.setShowBundledService(AppSysParamsUtil.isBundledFaresEnabled(commonItineraryParam.getAppIndicator()
				.toString()));
		itineraryDTO.setShowAeromartPayFares(commonItineraryParam.isAmountMaskingForcePriviledge());
		if (lccClientReservation.getItineraryFareMask() != null && lccClientReservation.getItineraryFareMask().equals("Y")) {
			itineraryDTO.setFareMasked(true);
		}
		itineraryDTO.setIncludePaxContactDetails(commonItineraryParam.isIncludePaxContactDetails());
		itineraryDTO.setIncludeStationContactDetails(commonItineraryParam.isIncludeStationContactDetails());
		
		if (lccClientReservation.getLccPromotionInfoTO() != null) {
			itineraryDTO.setShowDiscountAmounts(AppSysParamsUtil.isShowDiscountOnItinerary());
			if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(lccClientReservation.getLccPromotionInfoTO()
					.getDiscountAs())) {
				itineraryDTO.setCreditDiscount(true);
			}
		}
		itineraryDTO = ItineraryUtil.createInterlineItinerary(itineraryDTO, lccClientReservation,
				commonItineraryParam.getAirportMap(), itineraryDTO.getItineraryLanguage());
		AirproxyModuleUtils.getReservationBD().emailInterlineItinerary(itineraryDTO);
	}
	
	private static boolean isSalesChannelIn(LCCClientReservation lccClientReservation,String salesChannel){
		boolean isSameSalesChannel = false;
		if(lccClientReservation.getAdminInfo() != null){
			if(lccClientReservation.getAdminInfo().getOwnerChannelId()
			.equals(salesChannel)){
				isSameSalesChannel = true;
			}
		}
		return isSameSalesChannel;
	}

	private static boolean showCheckInClosingTimeInItinerary(Set<LCCClientReservationSegment> segments) throws ModuleException {
		if (AppSysParamsUtil.getCheckInClosingTime() != null && !AppSysParamsUtil.getCheckInClosingTime().trim().equals("")) {
			for (LCCClientReservationSegment lccSeg : segments) {
				if (lccSeg.getCheckInClosingTime() >= lccSeg.getCheckInTimeGap()) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	private static boolean showPassengerCreditExpiryDate(Set<LCCClientReservationPax> passengers) {
		boolean isIncludePassengerCreditExpiryDate = false;

		for (LCCClientReservationPax passenger : passengers) {
			if (passenger.getLccClientPaymentHolder() != null) {
				for (CreditInfoDTO passengerCredit : passenger.getLccClientPaymentHolder().getCredits()) {
					if (passengerCredit.getMcAmount() != null && passengerCredit.getMcAmount().compareTo(BigDecimal.ZERO) > 0) {
						isIncludePassengerCreditExpiryDate = true;
					}
				}
			}
		}

		return isIncludePassengerCreditExpiryDate;
	}
	
	private static boolean isThereBalanceToPay(Set<LCCClientReservationPax> setClientReservationPaxs) {

		boolean isThereBalanceToPay;
		
		BigDecimal totalToPay = BigDecimal.ZERO;
		if (setClientReservationPaxs != null) {

			for (LCCClientReservationPax resPax : SortUtil.sortPax(setClientReservationPaxs)) {
				if (!resPax.getPaxType().equals(PaxTypeTO.INFANT)) {
					
					if (resPax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
						totalToPay = AccelAeroCalculator.add(totalToPay, resPax.getTotalAvailableBalance());
					}
					
				}
			}
		}
		
		if(totalToPay.compareTo(BigDecimal.ZERO) == 1){
			isThereBalanceToPay = true;
		}
		else{
			isThereBalanceToPay = false;
		}
		
		return isThereBalanceToPay;

	}

	private static String getConfiguredLanguageForInternationalFlightBookings(String language,
			Set<LCCClientReservationSegment> segments) {
		for (LCCClientReservationSegment segment : segments) {
			if (!segment.isDomesticFlight()
					&& !(ReservationInternalConstants.ReservationSegmentStatus.CANCEL).equals(segment.getStatus())) {
				String configuredLanguage = AppSysParamsUtil.getItineraryLanguageForInternationalFlightBookings(language);
				if (!"".equals(configuredLanguage)) {
					return configuredLanguage;
				}
				break;
			}
		}
		return language;
	}

	private static void authorizeReservationAccess(LCCClientReservation lccClientReservation,
			ReservationSearchDTO reservationSearchDTO) throws ModuleException {

		boolean authenticate = false;

		CommonReservationAdminInfo adminInfo = lccClientReservation.getAdminInfo();
		
		boolean isSearchWebOrMobile = SalesChannelsUtil.isSalesChannelWebOrMobile(Integer.valueOf(ItineraryUtil
				.getBookingChannel(adminInfo.getOwnerChannelId())));

		if (reservationSearchDTO.getOwnerAgentCode() != null) {
			if (reservationSearchDTO.isSearchIBEBookings() && (adminInfo.getOwnerAgentCode() == null || isSearchWebOrMobile)) {
				authenticate = true;
			}
			if (reservationSearchDTO.getBookingCategories() != null && reservationSearchDTO.getBookingCategories().size() > 0) {
				for (String bookingCategory : reservationSearchDTO.getBookingCategories()) {
					if (bookingCategory.equals(lccClientReservation.getBookingCategory())) {
						authenticate = true;
						break;
					}
				}
			}

			if (!authenticate && adminInfo.getOwnerAgentCode().contains(reservationSearchDTO.getOwnerAgentCode())) {
				authenticate = true;
			} else if (!authenticate && reservationSearchDTO.isSearchGSABookings()) {
				reservationSearchDTO.setSearchAll(true);

				try {
				Collection<ReservationListTO> reservations = AirproxyModuleUtils.getAirproxyReservationBD().searchReservations(
						reservationSearchDTO, null, null);
					if (reservations != null && reservations.size() > 0) {
						authenticate = true;
					} else {
						authenticate = false;
					}
				} catch (Exception exp) {
					authenticate = false;
				}
			}
		} else {
			authenticate = true;
		}

		if (!authenticate) {
			throw new ModuleException("Unauthorized Operation");
		}
	}
}
