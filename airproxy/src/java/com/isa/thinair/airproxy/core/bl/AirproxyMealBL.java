package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Proxy BL to handle Ancillary Meals
 * 
 * @author Dilan Anuruddha
 * 
 */

public class AirproxyMealBL {

	private Log log = LogFactory.getLog(AirproxyMealBL.class);

	private List<LCCMealRequestDTO> mealRequestDTOs;
	private String transactionIdentifier;
	private SYSTEM system;
	private String selectedLanguage;
	private BasicTrackInfo trackInfo;
	private Map<String, Set<String>> flightRefWiseSelectedMeals;
	private ApplicationEngine appEngine;
	private List<BundledFareDTO> bundledFareDTOs;
	private String pnr;
	private UserPrincipal userPrinciple;

	public AirproxyMealBL(List<LCCMealRequestDTO> mealRequestDTOs, String transactionIdentifier,
			Map<String, Set<String>> flightRefWiseSelctedMeals, List<BundledFareDTO> bundledFareDTOs, SYSTEM system,
			String selectedLanguage, BasicTrackInfo trackInfo, UserPrincipal userPrinciple) {
		super();
		this.mealRequestDTOs = mealRequestDTOs;
		this.transactionIdentifier = transactionIdentifier;
		this.system = system;
		this.selectedLanguage = selectedLanguage;
		this.trackInfo = trackInfo;
		this.flightRefWiseSelectedMeals = flightRefWiseSelctedMeals;
		this.bundledFareDTOs = bundledFareDTOs;
		this.userPrinciple = userPrinciple;
	}

	public LCCMealResponceDTO execute() throws ModuleException {
		if (selectedLanguage == null || selectedLanguage.equalsIgnoreCase(""))
			selectedLanguage = "en";

		if (system == SYSTEM.INT) {

			return AirproxyModuleUtils.getLCCAncillaryBD().getAvailableMeals(mealRequestDTOs, transactionIdentifier,
					selectedLanguage, trackInfo, flightRefWiseSelectedMeals, pnr);
		} else {
			return getOwnAirlineMeals(selectedLanguage);
		}

	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public LCCMealResponceDTO getOwnAirlineMeals(String selectedLanguage) throws ModuleException {
		Map<Integer, FlightSegmentTO> flightSegmentMap = new HashMap<Integer, FlightSegmentTO>();
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		List<Integer> pnrSegIds = new ArrayList<Integer>();
		Map<Integer, Set<String>> flightSegIdWiseSelectedMeals;
		int salesChannelCode = userPrinciple.getSalesChannel();

		for (LCCMealRequestDTO mealReq : mealRequestDTOs) {
			Set<Integer> segIdList = new HashSet<Integer>();
			for (FlightSegmentTO flightSeg : mealReq.getFlightSegment()) {
				Integer segmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSeg.getFlightRefNumber());
				segIdList.add(segmentId);
				flightSegmentMap.put(segmentId, flightSeg);

				flightSegIdWiseCos.put(
						segmentId,
						new ClassOfServiceDTO(flightSeg.getCabinClassCode(), flightSeg.getLogicalCabinClassCode(), flightSeg
								.getSegmentCode()));

				if (flightSeg.getPnrSegId() != null && !flightSeg.getPnrSegId().isEmpty()) {
					pnrSegIds.add(Integer.parseInt(flightSeg.getPnrSegId()));
				}
			}
		}

		flightSegIdWiseSelectedMeals = AirproxyModuleUtils.getMealBD().getExistingMeals(pnrSegIds);

		List<LCCFlightSegmentMealsDTO> segmentMealList = new ArrayList<LCCFlightSegmentMealsDTO>();

		Map<Integer, Set<String>> flightSegIdWiseSelectedMeal = AncillaryCommonUtil
				.getFlightSegIdWiseSelectedAnci(flightRefWiseSelectedMeals);

		Map<Integer, List<FlightMealDTO>> mealMap = null;
		if ("en".equalsIgnoreCase(selectedLanguage)) {
			mealMap = AirproxyModuleUtils.getMealBD().getMeals(flightSegIdWiseCos, flightSegIdWiseSelectedMeal, false, null, salesChannelCode);
		} else {
			mealMap = AirproxyModuleUtils.getMealBD().getMealsWithTranslations(flightSegIdWiseCos, selectedLanguage,
					flightSegIdWiseSelectedMeal, salesChannelCode);
		}

		Map<String, LogicalCabinClassDTO> availableLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

		Map<Integer, Integer> ondTemplateIds = InventoryAPIUtil.getOndWiseSelectedTemplateId(bundledFareDTOs,
				EXTERNAL_CHARGES.MEAL.toString());

		Map<Integer, Boolean> ondServiceMultiSelection = InventoryAPIUtil.getOndWiseServiceMultiSelection(bundledFareDTOs,
				EXTERNAL_CHARGES.MEAL.toString());

		boolean isBundledFareSelected = CommonUtil.isAtleastOneBundledFareSelected(bundledFareDTOs);

		log.error("----pnr:" + pnr + "---- isBundledFareSelected : " + isBundledFareSelected);
		if (bundledFareDTOs != null) {
			for (BundledFareDTO b : bundledFareDTOs) {
				if (b != null) {
					log.error("---- " + b.toString());
				}
			}
		}

		Set<Integer> segIdSet = mealMap.keySet();
		for (Integer segId : segIdSet) {

			LCCFlightSegmentMealsDTO segmentMeal = new LCCFlightSegmentMealsDTO();
			FlightSegmentTO flightSegmentTO = flightSegmentMap.get(segId);
			Boolean isAllowMultiSelectForBundleFare = null;
			if (ondServiceMultiSelection != null) {
				isAllowMultiSelectForBundleFare = ondServiceMultiSelection.get(flightSegmentTO.getOndSequence());
			}

			if (isBundledFareSelected) {
				if (ondTemplateIds != null && ondTemplateIds.containsKey(flightSegmentTO.getOndSequence())
						&& ondTemplateIds.get(flightSegmentTO.getOndSequence()) != null) {
					Integer bundledFareTemplateId = ondTemplateIds.get(flightSegmentTO.getOndSequence());

					List<FlightMealDTO> bundledFareMeals = AirproxyModuleUtils.getMealBD().getMealsByTemplate(
							bundledFareTemplateId, flightSegmentTO.getCabinClassCode(),
							flightSegmentTO.getLogicalCabinClassCode(), selectedLanguage, segId,
							flightSegIdWiseSelectedMeals.get(segId), salesChannelCode);

					if (!bundledFareMeals.isEmpty()) {
						mealMap.put(segId, bundledFareMeals);
					}
					log.error("---- bundledFareTemplateId:" + bundledFareTemplateId + "----bundledFareMeals:" + bundledFareMeals.size());

				}
			} else {

				// Anci offer related variables
				List<FlightMealDTO> offerMeals = new ArrayList<FlightMealDTO>();
				AnciOfferCriteriaTO availableOffer = null;

				if (SalesChannelsUtil.isAppEngineWebOrMobile(appEngine)) {
					AnciOfferCriteriaSearchTO searchCriteria = AnciOfferUtil.createAnciOfferSearch(flightSegmentTO,
							AnciOfferType.MEAL);
					availableOffer = AirproxyModuleUtils.getAnciOfferBD().getAvailableAnciOffer(searchCriteria);
				}
				if (availableOffer != null) {
					offerMeals = selectedLanguage == null ? AirproxyModuleUtils.getMealBD().getMealsByTemplate(
							availableOffer.getTemplateID(), flightSegmentTO.getCabinClassCode(),
							flightSegmentTO.getLogicalCabinClassCode(), Locale.ENGLISH.getLanguage(), segId,
							flightSegIdWiseSelectedMeals.get(segId), salesChannelCode) : AirproxyModuleUtils.getMealBD().getMealsByTemplate(
							availableOffer.getTemplateID(), flightSegmentTO.getCabinClassCode(),
							flightSegmentTO.getLogicalCabinClassCode(), selectedLanguage, segId,
							flightSegIdWiseSelectedMeals.get(segId), salesChannelCode);

					if (!offerMeals.isEmpty()) {
						List<FlightMealDTO> existingMeals = mealMap.get(segId);
						for (FlightMealDTO offerMeal : offerMeals) {
							for (FlightMealDTO existingMeal : existingMeals) {
								if (offerMeal.getMealId().equals(existingMeal.getMealId())) {
									offerMeal.setAllocatedMeal(existingMeal.getAllocatedMeal());
									offerMeal.setAvailableMeals(existingMeal.getAvailableMeals());
									offerMeal.setSoldMeals(existingMeal.getSoldMeals());
								}
							}
						}
						mealMap.put(segId, offerMeals);
						segmentMeal.setAnciOffer(true);
						segmentMeal.setAnciOfferName(availableOffer.getNameTranslations().get(selectedLanguage));
						segmentMeal.setAnciOfferDescription(availableOffer.getDescriptionTranslations().get(selectedLanguage));
						segmentMeal.setOfferedTemplateID(availableOffer.getTemplateID());
					}
				}

			}

			for (FlightMealDTO meal : mealMap.get(segId)) {
				LCCMealDTO mealDTO = new LCCMealDTO();
				String ucs = "";
				String catDes = "";
				if (meal.getTranslatedMealTitle() == null
						|| StringUtil.getUnicode(meal.getTranslatedMealTitle()).equalsIgnoreCase("null"))
					mealDTO.setMealName(meal.getMealName());
				else {
					ucs = StringUtil.getUnicode(meal.getTranslatedMealTitle());
					ucs = ucs.replace("^", ",");
					mealDTO.setMealName(ucs);
				}

				if (meal.getTranslatedMealDescription() == null
						|| StringUtil.getUnicode(meal.getTranslatedMealDescription()).equalsIgnoreCase("null"))
					mealDTO.setMealDescription(meal.getMealDescription());
				else {
					ucs = StringUtil.getUnicode(meal.getTranslatedMealDescription());
					ucs = ucs.replace("^", ",");
					mealDTO.setMealDescription(ucs);
				}

				if (meal.getTranslatedMealCategory() == null || ("null").equalsIgnoreCase(meal.getTranslatedMealCategory())
						|| StringUtil.getUnicode(meal.getTranslatedMealCategory()).equalsIgnoreCase("null")) {

					mealDTO.setMealCategoryCode(meal.getMealCategoryCode());
				} else {
					catDes = StringUtil.getUnicode(meal.getTranslatedMealCategory());
					catDes = catDes.replace("^", ",");
					mealDTO.setMealCategoryCode(catDes);
				}

				// mealDTO.setMealName(meal.getMealName());
				mealDTO.setMealCode(meal.getMealCode());
				// mealDTO.setMealDescription(meal.getMealDescription());
				mealDTO.setMealCharge(meal.getAmount());
				mealDTO.setAllocatedMeals(meal.getAllocatedMeal());
				mealDTO.setAvailableMeals(meal.getAvailableMeals());
				mealDTO.setSoldMeals(meal.getSoldMeals());
				mealDTO.setMealImagePath(getMealImagePath(meal, "meal_"));
				mealDTO.setMealThumbnailImagePath(getMealImagePath(meal, "meal_Thumbnail_"));
				mealDTO.setTranslatedMealDescription(meal.getTranslatedMealDescription());
				mealDTO.setTranslatedMealTitle(meal.getTranslatedMealTitle());
				mealDTO.setMealCategoryID(meal.getMealCategoryID());
				mealDTO.setMealStatus(meal.getMealStatus());
				mealDTO.setPopularity(meal.getPopularity());
				segmentMeal.getMeals().add(mealDTO);
				mealDTO.setCategoryRestricted(meal.isCategoryRestricted());
			}
			segmentMeal.setFlightSegmentTO(flightSegmentTO);

			String logicalCabinClassCode = flightSegmentTO.getLogicalCabinClassCode();

			if (isAllowMultiSelectForBundleFare != null) {
				segmentMeal.setMultiMealEnabledForLogicalCabinClass(isAllowMultiSelectForBundleFare);
			} else if (logicalCabinClassCode != null && !"".equals(logicalCabinClassCode)) {
				LogicalCabinClassDTO logicalCCObj = availableLogicalCCMap.get(logicalCabinClassCode);
				if (logicalCCObj != null) {
					segmentMeal.setMultiMealEnabledForLogicalCabinClass(!logicalCCObj.isAllowSingleMealOnly());
				} else {
					// if logical cc level multi meal mot defined, set system level configuration
					segmentMeal.setMultiMealEnabledForLogicalCabinClass(AppSysParamsUtil.isMultipleMealSelectionEnabled());
				}
			} else {
				// if logical cc level multi meal mot defined, set system level configuration
				segmentMeal.setMultiMealEnabledForLogicalCabinClass(AppSysParamsUtil.isMultipleMealSelectionEnabled());
			}

			segmentMealList.add(segmentMeal);
		}

		LCCMealResponceDTO mealResponseDTO = new LCCMealResponceDTO();
		mealResponseDTO.getFlightSegmentMeals().addAll(segmentMealList);
		mealResponseDTO.setMultipleMealSelectionEnabled(AppSysParamsUtil.isMultipleMealSelectionEnabled());
		return mealResponseDTO;
	}

	/**
	 * 
	 * @param flightMealDTO
	 * @return
	 */
	private static String getMealImagePath(FlightMealDTO flightMealDTO, String imageName) {
		String strMealPath = AppSysParamsUtil.getImageUploadPath();
		Integer mealId = flightMealDTO.getMealId();

		StringBuilder mealImgPath = new StringBuilder(strMealPath);
		mealImgPath.append(imageName);
		mealImgPath.append(mealId);
		mealImgPath.append(".jpg"); // no need to add the suffix since it is
									// uploaded by client

		return mealImgPath.toString();
	}
	
	public List<LCCMealDTO> getMealsForPreference() throws ModuleException {
		
		List<FlightMealDTO> mealsList = null;
		if ("en".equalsIgnoreCase(selectedLanguage)) {
			mealsList = AirproxyModuleUtils.getMealBD().getMealsForPreference();
		} else {
			// TODO change the method to get the meal with the translations 
			mealsList = AirproxyModuleUtils.getMealBD().getMealsForPreferenceWithTranslations(selectedLanguage);
		}
		
		List<LCCMealDTO> lccMealDTOList = new ArrayList<LCCMealDTO>();
		
		for (FlightMealDTO meal : mealsList) {
			LCCMealDTO mealDTO = new LCCMealDTO();
			String ucs = "";
			String catDes = "";
			if (meal.getTranslatedMealTitle() == null
					|| StringUtil.getUnicode(meal.getTranslatedMealTitle()).equalsIgnoreCase("null"))
				mealDTO.setMealName(meal.getMealName());
			else {
				ucs = StringUtil.getUnicode(meal.getTranslatedMealTitle());
				ucs = ucs.replace("^", ",");
				mealDTO.setMealName(ucs);
			}

			if (meal.getTranslatedMealDescription() == null
					|| StringUtil.getUnicode(meal.getTranslatedMealDescription()).equalsIgnoreCase("null"))
				mealDTO.setMealDescription(meal.getMealDescription());
			else {
				ucs = StringUtil.getUnicode(meal.getTranslatedMealDescription());
				ucs = ucs.replace("^", ",");
				mealDTO.setMealDescription(ucs);
			}

			if (meal.getTranslatedMealCategory() == null || ("null").equalsIgnoreCase(meal.getTranslatedMealCategory())
					|| StringUtil.getUnicode(meal.getTranslatedMealCategory()).equalsIgnoreCase("null")) {

				mealDTO.setMealCategoryCode(meal.getMealCategoryCode());
			} else {
				catDes = StringUtil.getUnicode(meal.getTranslatedMealCategory());
				catDes = catDes.replace("^", ",");
				mealDTO.setMealCategoryCode(catDes);
			}

			mealDTO.setMealCode(meal.getMealCode());
			mealDTO.setAllocatedMeals(meal.getAllocatedMeal());
			mealDTO.setAvailableMeals(meal.getAvailableMeals());
			mealDTO.setSoldMeals(meal.getSoldMeals());
			mealDTO.setMealImagePath(getMealImagePath(meal, "meal_"));
			mealDTO.setMealThumbnailImagePath(getMealImagePath(meal, "meal_Thumbnail_"));
			mealDTO.setTranslatedMealDescription(meal.getTranslatedMealDescription());
			mealDTO.setTranslatedMealTitle(meal.getTranslatedMealTitle());
			mealDTO.setMealCategoryID(meal.getMealCategoryID());
			mealDTO.setPopularity(meal.getPopularity());
			mealDTO.setMealStatus(meal.getMealStatus());
			mealDTO.setMealId(meal.getMealId());
			
			lccMealDTOList.add(mealDTO);
		}
		
		return lccMealDTOList;
		
	}

	public ApplicationEngine getAppEngine() {
		return appEngine;
	}

	public void setAppEngine(ApplicationEngine appEngine) {
		this.appEngine = appEngine;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
