package com.isa.thinair.airproxy.core.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ghasemkiani.util.icu.PersianDateFormat;
import com.ibm.icu.text.DateFormat;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.model.BookingClass.BookingClassType;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOtherAirlineSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentHolder;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationOwnerAgentContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientVoucherPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airproxy.api.utils.FlightStopOverDisplayUtil;
import com.isa.thinair.airproxy.api.utils.ItineraryValueFormatterUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryAgentTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryAirportServiceTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryAncillaryAvailabilityTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryBaggageTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryBookingTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryContactInfoTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryEticketTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFareMaskTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFareRuleTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFinancialTotalsTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFlightOndGroup;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFlightTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryInsuranceTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryMealTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryOndChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPassengerEticketTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPassengerTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPaymentTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItinerarySSRTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItinerarySeatTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItinerarySegmentAncillaryTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItinerarySegmentTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.util.I18NMessagingUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Zaki
 * @author Dilan Anuruddha
 */
public class ItineraryUtil {

	private static Log log = LogFactory.getLog(ItineraryUtil.class);

	public static final String CARRIER_SEPERATOR = "|";
	public static final String ITINERARY_FARE_MASK = "IT";
	public static final String YES = "Y";

	public static ItineraryDTO createInterlineItinerary(ItineraryDTO itineraryDTO, LCCClientReservation lccClientReservation,
			Map<String, String> airportMap, String languageCode) throws ModuleException {
		Map<String, String> airportCodeMap = new HashMap<String, String>();
		Map<String, String> airportNameMap = new HashMap<String, String>();
		if (!("en").equals(languageCode)) {
			airportCodeMap = SelectListGenerator.create_AirportCode_Map(languageCode);
			airportNameMap = SelectListGenerator.create_AirportName_Map(languageCode);
		}
		String itineraryFareMask = lccClientReservation.getItineraryFareMask();		
		// Fill ItineraryDTO
		itineraryDTO.setContactInfo(createContactInfo(lccClientReservation, itineraryDTO));
		itineraryDTO.setBooking(createBooking(lccClientReservation, itineraryDTO, itineraryDTO.getStation()));
		itineraryDTO.setAgent(createAgent(lccClientReservation));
		itineraryDTO.setFlightOndGroups(createFlightOndGroups(lccClientReservation, airportMap,
				itineraryDTO.getItineraryLanguage(), airportNameMap, itineraryDTO));
		itineraryDTO.setStationContacts(setStationContacts(lccClientReservation));
		itineraryDTO.setPassengers(createPassengers(lccClientReservation, itineraryDTO, languageCode));
		itineraryDTO.setInsuranceTO(createInsurance(lccClientReservation));
		itineraryDTO.setFareRules(createFareRules(lccClientReservation, languageCode, airportCodeMap));
		itineraryDTO.setOpenRetFlightOndGroups(createOpenRetFlightOndGroups(itineraryDTO.getFlightOndGroups()));
		itineraryDTO.setFlexiRules(createFlexiRules(lccClientReservation, languageCode, airportMap));

		/*
		 * Sets whether or not to display extra charge details by country's configuration.
		 */
		itineraryDTO.setShowExtraChargeDetails(isShowExtraChargesEnabled(lccClientReservation));

		if(AppSysParamsUtil.isAeroMartPayEnabled()){
			if(isSalesChannelIn(lccClientReservation,
					String.valueOf(SalesChannelsUtil.SALES_CHANNEL_GOQUO)) 
						&& !itineraryDTO.isShowAeromartPayFares()){
				itineraryFareMask = "Y";
				itineraryDTO.setItineraryFareMaskTO(new ItineraryFareMaskTO());
				itineraryDTO.setFareMasked(true);
			}
		}
		
		// set e ticket information
		if (!lccClientReservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
			itineraryDTO.setTicketInfo(createTicketingInfo(lccClientReservation, languageCode, airportCodeMap));
		}

		int paymentEntries = 0;
		if (itineraryDTO.isIncludePaymentDetails()) {
			for (ItineraryPassengerTO pax : itineraryDTO.getPassengers()) {
				Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> payments = pax.getPayments();
				paymentEntries += payments.values().size();
			}
		}
		itineraryDTO.setIncludePaymentDetails(itineraryDTO.isIncludePaymentDetails() && paymentEntries > 0);

		if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
			itineraryDTO.setOndTaxesFeesCharges(createTaxesFeesCharges(lccClientReservation, languageCode));
		}

		if (itineraryDTO.isIncludeTicketCharges()) {
			itineraryDTO.setOndCharges(createCharges(lccClientReservation, itineraryFareMask, languageCode, airportCodeMap));
			itineraryDTO.setIncludeTicketCharges(itineraryDTO.getOndCharges().size() > 0);
		}
		if (itineraryDTO.getFareRules().size() > 0) {
			itineraryDTO.setIncludeFareRules(true);
		}

		// set airport msgs
		List<String> airportMsgs = getAirportMsgs(lccClientReservation, itineraryDTO.getAppIndicator(), languageCode);
		itineraryDTO.setAirportMsgs(airportMsgs);

		itineraryDTO.setFirstDepartingCarrier(getFirstDepartingCarrier(lccClientReservation));
		itineraryDTO.setIncludeEndorsements(AppSysParamsUtil.isEndorsementsEnabled());
		itineraryDTO.setEndorsements(lccClientReservation.getEndorsementNote());

		boolean isHideDecimals = AppSysParamsUtil.isHideBaseCurrencyDecimalsInItinerary();
		if (isHideDecimals) {
			ItineraryValueFormatterUtil.removeDecimalsInBaseCurrencyValues(itineraryDTO);
		}
		if (itineraryFareMask != null && itineraryFareMask.equals("Y")) {
			// Mask Total Amounts
			ItineraryDecorateUtil.applyItineraryFareMaskDecorater(itineraryDTO);
		}
		
		
		
		itineraryDTO.setShowFlightStopOverInfo(AppSysParamsUtil.isShowFlightStopOvrDurInfo());

		// Set param to include auto cancellation info
		itineraryDTO.setIncludeAutoCancellationInfo(AppSysParamsUtil.isAutoCancellationEnabled()
				&& (lccClientReservation.getAutoCancellationInfo() != null));

		String lastPaidCurrency = null;
		boolean hasMoreThanTwoPaidCurrencies = false;
		boolean hideChargesinItinerary = false;
		boolean hasPayments = false;
		Set<String> chargeHideCurrencies = null;
		String selectedCurrency = null;

		Iterator<LCCClientReservationPax> lCCClientReservationPaxIterator = lccClientReservation.getPassengers().iterator();
		iterateCurrencies: while (lCCClientReservationPaxIterator.hasNext()) {
			Iterator<LCCClientPaymentInfo> lccpaymentIterator = lCCClientReservationPaxIterator.next()
					.getLccClientPaymentHolder().getPayments().iterator();
			while (lccpaymentIterator.hasNext()) {
				String paidCurrency = lccpaymentIterator.next().getPayCurrencyDTO().getPayCurrencyCode();
				if (lastPaidCurrency != null && !lastPaidCurrency.equals(paidCurrency)) {
					hasMoreThanTwoPaidCurrencies = true;
					break iterateCurrencies;
				}
				lastPaidCurrency = paidCurrency;
				hasPayments = true;

			}
		}
		if (lccClientReservation.getLastCurrencyCode() != null && lccClientReservation.getLastCurrencyCode() != "") {
			selectedCurrency = lccClientReservation.getLastCurrencyCode();
		} else {
			selectedCurrency = AppSysParamsUtil.getBaseCurrency();
		}

		if (AppSysParamsUtil.hideChargesInItineraryPassengerDetails()) {
			if (!lccClientReservation.isGroupPNR()) {
				hideChargesinItinerary = true;
			}
			chargeHideCurrencies = CommonsServices.getGlobalConfig().getChargehideCurrencies();
		}
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(lccClientReservation.getDisplayStatus())
				|| (itineraryDTO.getBooking().isForced() && !hasPayments)) {
			if (AppSysParamsUtil.hideChargesInItineraryPassengerDetails() && !lccClientReservation.isGroupPNR()) {

				for (String chargeHideCurrency : chargeHideCurrencies) {
					if (chargeHideCurrency.equals(selectedCurrency)) {
						itineraryDTO.setShowChargesInItineraryPassengerDetails(false);
						break;
					}
				}
			} else if (AppSysParamsUtil.getBaseCurrency().equals(selectedCurrency) || lccClientReservation.isGroupPNR()) {
				// if this is grp pnr we will hide total in row
				itineraryDTO.getBooking().setPaidCurrencyFinancials(null);
			}

		} else {
			Iterator<LCCClientReservationPax> IteratorLCCClientReservationPax = lccClientReservation.getPassengers().iterator();
			iterateCurrencies: while (IteratorLCCClientReservationPax.hasNext()) {
				Iterator<LCCClientPaymentInfo> lccpaymentIterator = IteratorLCCClientReservationPax.next()
						.getLccClientPaymentHolder().getPayments().iterator();
				while (lccpaymentIterator.hasNext()) {
					String paidCurrency = lccpaymentIterator.next().getPayCurrencyDTO().getPayCurrencyCode();
					if (hideChargesinItinerary) {
						for (String chargeHideCurrency : chargeHideCurrencies) {
							if (chargeHideCurrency.equals(paidCurrency)) {
								itineraryDTO.setShowChargesInItineraryPassengerDetails(false);
								break iterateCurrencies;
							}
						}
					}
				}
			}
		}
		if (!hasMoreThanTwoPaidCurrencies) {
			if (lastPaidCurrency != null && AppSysParamsUtil.getBaseCurrency().equalsIgnoreCase(lastPaidCurrency)) {
				itineraryDTO.getBooking().setPaidCurrencyFinancials(null); // if there is only paid currency and
																			// it
																			// is
																			// equal to the base currency, we
																			// will
																			// hide
																			// the "Total In" row in the
																			// Itinerary
			} else if (AppSysParamsUtil.getBaseCurrency().equals(selectedCurrency)) {
				if (itineraryDTO.isShowChargesInItineraryPassengerDetails()) {
					itineraryDTO.getBooking().setPaidCurrencyFinancials(null);
				}
			}
		}
		return itineraryDTO;
	}

	private static boolean isSalesChannelIn(LCCClientReservation lccClientReservation,String salesChannel){
		boolean isSameSalesChannel = false;
		if(lccClientReservation.getAdminInfo() != null && 
				lccClientReservation.getAdminInfo().getOwnerChannelId() != null &&
				lccClientReservation.getAdminInfo().getOwnerChannelId().contains("|")){
			String channelInformation = lccClientReservation.getMarketingCarrier() 
						+"|"+salesChannel;
			if(channelInformation != null 
					&& channelInformation != null
					&&  lccClientReservation.getAdminInfo().getOwnerChannelId().equals(channelInformation)){
				isSameSalesChannel = true;
			}
		}
		return isSameSalesChannel;
	}
	
	/**
	 * Determines whether the reservation's origin country has the Show Extra Charges option set to true. Country codes
	 * for multiple LCC segs should be separated with a ','. Carrier codes are not handles here since anyway origin
	 * country code will be the same regardless of carrier. They shouldn't be set by LCC either.
	 * 
	 * @param lccClientReservation
	 *            Reservation related to the itinerary
	 * @return true if the switch is set to Y, otherwise returns false.
	 * @throws ModuleException
	 *             If retrieving the country by code fails.
	 */
	private static boolean isShowExtraChargesEnabled(LCCClientReservation lccClientReservation) throws ModuleException {

		/*
		 * When debugging removing this will help find the problem easier. This is a safeguard to ensure that the
		 * itinerary will print as normal if any error occurs. Typically this will be null here if
		 * LCCClientPnrModesDTO.loadOriginCountry is set to false. Or not set when priting or emailing itinerary.
		 */
		if (lccClientReservation.getAdminInfo().getOriginCountryCode() == null
				|| lccClientReservation.getAdminInfo().getOriginCountryCode().isEmpty()) {
			return false;
		}

		String[] countries = lccClientReservation.getAdminInfo().getOriginCountryCode().split(",");
		String originCountry = null;
		// In case one of the countries are null loop through to find one thats not null.
		for (String str : countries) {
			if (str != null) {
				originCountry = str;
			}
		}
		return AirproxyModuleUtils.getLocationDB().getCountry(originCountry).isShowExtraChargeDetails();
	}

	private static ItineraryInsuranceTO createInsurance(LCCClientReservation lccClientReservation) {
		
		List<LCCClientReservationInsurance> lccInsurances = lccClientReservation.getReservationInsurances();
		if (lccInsurances == null || lccInsurances.isEmpty() || lccInsurances.get(0) == null) {
			return null;
		}
		ItineraryInsuranceTO insuranceTO = new ItineraryInsuranceTO();
		
		LCCClientReservationInsurance lccInsurance = lccInsurances.get(0);
				
		insuranceTO.setInsuranceQuoteRefNumber(lccInsurance.getInsuranceQuoteRefNumber());
		insuranceTO.setOperatingAirline(lccInsurance.getOperatingAirline());
		insuranceTO.setTotalPremium(lccInsurance.getQuotedTotalPremium());
		insuranceTO.setInsuredJourneyDTO(lccInsurance.getInsuredJourneyDTO());
		insuranceTO.setPolicyCode(lccInsurance.getPolicyCode());
		insuranceTO.setInsuranceDescription(null);
		if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.CCC)) {
			insuranceTO.setShowInsuranceType(true);
		}
		insuranceTO.setInsuranceType(lccInsurance.getInsuranceType());
		return insuranceTO;
	}

	private static ItineraryContactInfoTO createContactInfo(LCCClientReservation lccClientReservation, ItineraryDTO itineraryDTO) {

		ItineraryContactInfoTO itineraryContactInfoTO = new ItineraryContactInfoTO();

		CommonReservationContactInfo contactInfo = lccClientReservation.getContactInfo();
		itineraryContactInfoTO.setFirstName(contactInfo.getFirstName());
		itineraryContactInfoTO.setLastName(contactInfo.getLastName());
		itineraryContactInfoTO.setEmail(contactInfo.getEmail());
		itineraryContactInfoTO.setMobileNo(contactInfo.getMobileNo());

		return itineraryContactInfoTO;
	}

	private static ItineraryAgentTO createAgent(LCCClientReservation lccClientReservation) {

		LCCClientReservationOwnerAgentContactInfo lccOwnerAgentContactInfo = lccClientReservation.getAdminInfo()
				.getOwnerAgentContactInfo();

		ItineraryAgentTO itineraryAgentTO = new ItineraryAgentTO();
		itineraryAgentTO.setIsOwner(true);
		itineraryAgentTO.setCarrierCode(lccOwnerAgentContactInfo.getAgentCarrier());
		itineraryAgentTO.setAgentName(lccOwnerAgentContactInfo.getAgentName());
		itineraryAgentTO.setAgentTelephone(lccOwnerAgentContactInfo.getAgentTelephone());
		itineraryAgentTO.setAgentEmail(lccOwnerAgentContactInfo.getAgentEmail());
		itineraryAgentTO.setExternalCharges(new ArrayList<ItineraryPaymentTO>());
		itineraryAgentTO.setAgentIATANumber(BeanUtils.nullHandler(lccOwnerAgentContactInfo.getAgentIATANumber()));

		String ownerChannelId = "";
		for (String carrierId : lccClientReservation.getAdminInfo().getOwnerChannelId().split(",")) {
			String[] carrierIdSplit = carrierId.split("\\" + CARRIER_SEPERATOR);
			if (lccClientReservation.getMarketingCarrier().compareTo(carrierIdSplit[0]) == 0) {
				ownerChannelId = carrierIdSplit[1];
				break;
			}
		}
		itineraryAgentTO.setOwnerChannelId(ownerChannelId);

		return itineraryAgentTO;
	}

	private static ItineraryBookingTO createBooking(LCCClientReservation lccClientReservation, ItineraryDTO itineraryDTO,
			String station) throws ModuleException {
		ItineraryBookingTO bookingTO = new ItineraryBookingTO();
		bookingTO.setLocale(itineraryDTO.getItineraryLanguage());
		String itineraryFareMask = lccClientReservation.getItineraryFareMask();

		List<LCCClientReservationSegment> segList = new ArrayList<LCCClientReservationSegment>(lccClientReservation.getSegments());
		Collections.sort(segList);
		List<String> segCodesOrderByDep = new ArrayList<String>();
		for (LCCClientReservationSegment segment : segList) {
			segCodesOrderByDep.add(segment.getSegmentCode());
		}
		String barcodePath = AirproxyModuleUtils.getReservationBD().getBarcodeImagePathForItinerary(
				lccClientReservation.getPNR(), segCodesOrderByDep);
		// GDS PNR
		bookingTO.setGDSPNR(lccClientReservation.getPNR());

		// Set Marketing Carrier
		bookingTO.setMarketingCarrier(lccClientReservation.getMarketingCarrier());

		// PNRs (for each carrier)
		bookingTO.setPNR(lccClientReservation.getCarrierPNR());

		// Carrier Names
		bookingTO.setAirlineNames(lccClientReservation.getCarrierName());

		bookingTO.setItineraryFareMask(itineraryFareMask);
		bookingTO.setItineraryFareMaskLabel(ITINERARY_FARE_MASK);

		if (itineraryDTO.isForced()) {
			bookingTO.setForced(true);
		} else {
			bookingTO.setForced(false);
		}

		if (ReservationInternalConstants.ReservationStatus.VOID.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus(ReservationInternalConstants.ReservationStatus.VOID);
		} else if (hasStandbySegments(lccClientReservation)) {
			bookingTO.setStatus(ReservationInternalConstants.ReservationStatus.STAND_BY);
		} else if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccClientReservation.getStatus())) {
			bookingTO.setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		} else {
			bookingTO.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
			bookingTO.setReleaseDate(DateUtil.getLocalDateTime(lccClientReservation.getZuluReleaseTimeStamp(), station));
			if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
				DateFormat formatter = new PersianDateFormat("dd MMM yyyy HHH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
				bookingTO.setReleaseDateTimeInPersian(formatter.format(DateUtil.getLocalDateTime(
						lccClientReservation.getZuluReleaseTimeStamp(), station)));
				bookingTO.setIsPersianDatesSet(true);
			}
			itineraryDTO.setIncludePaymentDetails(false);
		}

		// Booking Date

		if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
			DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
			bookingTO.setBookingDateInPersian(formatter.format(lccClientReservation.getZuluBookingTimestamp()));
			bookingTO.setIsPersianDatesSet(true);
		} else {
			bookingTO.setBookingDate(lccClientReservation.getZuluBookingTimestamp());
		}

		// Setting date of issue
		bookingTO.setDateOfIssue(lccClientReservation.getDateOfIssue());

		if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())
				&& lccClientReservation.getDateOfIssue() != null) {
			DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
			bookingTO.setDateOfIssueInPersian(formatter.format(lccClientReservation.getDateOfIssue()));
			bookingTO.setIsPersianDatesSet(true);
		}

		if (lccClientReservation.getTicketValidTill() != null) {
			// Ticket Expiry Date

			if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
				DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
				bookingTO.setTicketExpiryDateInPersian(formatter.format(lccClientReservation.getTicketValidTill()));
				bookingTO.setIsPersianDatesSet(true);
			} else {
				bookingTO.setTicketExpiryDate(lccClientReservation.getTicketValidTill());
			}

			bookingTO.setShowTicketExpiryDate(true);
		}

		// Amounts in Base Currency
		ItineraryFinancialTotalsTO baseCurrency = new ItineraryFinancialTotalsTO();
		baseCurrency.setCurrency(AppSysParamsUtil.getBaseCurrency());
		baseCurrency.setTotalBalance(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalAvailableBalance()));

		// [0]fare [1]tax [2]surcharge [3]cancelcharge [4]modificationcharge [5]adjustmentcharge [6]discount
		BigDecimal[] totalTicketAmounts = lccClientReservation.getTotalTicketAmounts();

		if (itineraryFareMask != null && itineraryFareMask.equals("Y")) {
			baseCurrency.setTotalFare(ITINERARY_FARE_MASK);
		} else {
			baseCurrency.setTotalFare(formatAsDecimal(totalTicketAmounts[0]));
		}

		baseCurrency.setTotalCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(totalTicketAmounts[1],
				totalTicketAmounts[2], totalTicketAmounts[3], totalTicketAmounts[4], totalTicketAmounts[5])));
		baseCurrency.setTotalPaidAmount(formatAsDecimal(lccClientReservation.getTotalPaidAmount()));

		baseCurrency.setTotalTaxes(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalTicketTaxCharge()));

		/*
		 * Total surcharges are actually all the charges - taxes. (Thats how AirArabia wants it). getTotalChargeAmount()
		 * returns sum of all including fare. So need to subtract tax and fare from it to get the surcharges.
		 */
		BigDecimal fareAndTax = AccelAeroCalculator.add(lccClientReservation.getTotalTicketFare(),
				lccClientReservation.getTotalTicketTaxCharge());
		BigDecimal surcharge = AccelAeroCalculator.subtract(lccClientReservation.getTotalChargeAmount(), fareAndTax);
		baseCurrency.setTotalSurcharges((AccelAeroCalculator.formatAsDecimal(surcharge)));

		baseCurrency.setTotalDue(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getTotalChargeAmount()));

		baseCurrency.setTotalDiscount(AccelAeroCalculator.formatAsDecimal(totalTicketAmounts[6]));

		if (itineraryDTO.isCreditDiscount()) {
			baseCurrency.setTotalCreditDiscount(AccelAeroCalculator.formatAsDecimal(lccClientReservation.getLccPromotionInfoTO()
					.getCreditDiscountAmount()));
		}

		bookingTO.setBaseCurrencyFinancials(baseCurrency);
		bookingTO.setBarcode(barcodePath);

		if (!lccClientReservation.isGroupPNR()) {
			ItineraryFinancialTotalsTO paidCurrency = new ItineraryFinancialTotalsTO();
			String paidCurrencyPayments = getPaidCurrencyPayments(lccClientReservation);
			if (paidCurrencyPayments != null && !"".equals(paidCurrencyPayments)) {
				paidCurrency.setTotalPaidAmount(paidCurrencyPayments);
			} else {
				if (lccClientReservation.getLastCurrencyCode() != null && !"".equals(lccClientReservation.getLastCurrencyCode())) {
					paidCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(BigDecimal.ZERO) + " "
							+ lccClientReservation.getLastCurrencyCode());
				} else {
					paidCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(BigDecimal.ZERO) + " "
							+ AppSysParamsUtil.getBaseCurrency());
				}
			}
			paidCurrency.setCurrency("");
			paidCurrency.setTotalCharges("");
			paidCurrency.setTotalFare("");
			paidCurrency.setTotalDue("");
			paidCurrency.setTotalTaxes("");
			paidCurrency.setTotalSurcharges("");
			paidCurrency.setTotalBalance(getPaidCurrencyAvaialbleBalance(lccClientReservation));
			bookingTO.setPaidCurrencyFinancials(paidCurrency);
		}

		// Add auto cancellation time
		if (AppSysParamsUtil.isAutoCancellationEnabled() && lccClientReservation.getAutoCancellationInfo() != null) {
			Date localDateTime = DateUtil.getLocalDateTime(lccClientReservation.getAutoCancellationInfo().getExpireOn(), station);
			bookingTO.setAutoCancellationTime(DateUtil.formatDate(localDateTime, "EEE, dd-MMM-yyyy HH:mm"));
		}

		return bookingTO;
	}

	private static boolean hasStandbySegments(LCCClientReservation reservation) {
		for (LCCClientReservationSegment seg : reservation.getSegments()) {
			if (BookingClassType.STANDBY.equals(seg.getBookingType())
					&& ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())) {
				return true;
			}
		}
		return false;
	}

	private static String getPaidCurrencyAvaialbleBalance(LCCClientReservation reservation) {
		BigDecimal avlConvertedBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		String selectedCurrency = "";
		StringBuilder strAvailBal = new StringBuilder();

		if (reservation.getLastCurrencyCode() != null && reservation.getLastCurrencyCode() != "") {
			selectedCurrency = reservation.getLastCurrencyCode();
		} else {
			selectedCurrency = AppSysParamsUtil.getBaseCurrency();
		}

		BigDecimal avlBalance = reservation.getTotalAvailableBalance();

		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);

			if (currExRate != null && avlBalance.abs().doubleValue() > 0) {
				BigDecimal exchangeRate = currExRate.getMultiplyingExchangeRate();
				Currency currency = currExRate.getCurrency();
				avlConvertedBalance = AccelAeroRounderPolicy.convertAndRound(avlBalance, exchangeRate,
						currency.getBoundryValue(), currency.getBreakPoint());
			}

			strAvailBal.append(AccelAeroCalculator.formatAsDecimal(avlConvertedBalance) + " " + selectedCurrency);
		} catch (ModuleException e) {
		}

		return strAvailBal.toString();
	}

	private static String getPaidCurrencyPayments(LCCClientReservation reservation) throws ModuleException {

		Set<Integer> pnrPaxIdSet = new HashSet<Integer>();
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			pnrPaxIdSet.add(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()));
		}

		Map<String, BigDecimal> currencyWisePayAmounts = new HashMap<String, BigDecimal>();
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		// reservation.getLccClientPaymentHolder().getPayments().iterator().next().getPayCurrencyAmount();
		// reservation.getLccClientPaymentHolder().getPayments().iterator().next().getPayCurrencyDTO().getPayCurrencyCode();
		/*
		 * if (reservation != null) { LCCClientPaymentHolder lCCClientPaymentHolder =
		 * reservation.getLccClientPaymentHolder(); if (lCCClientPaymentHolder != null) {
		 * Collection<LCCClientPaymentInfo> collection = lCCClientPaymentHolder.getPayments(); if (collection != null) {
		 * Iterator<LCCClientPaymentInfo> iterator = collection.iterator(); if (iterator != null) { LCCClientPaymentInfo
		 * lCCClientPaymentInfo = iterator.next(); if (lCCClientPaymentInfo != null) { BigDecimal amount =
		 * lCCClientPaymentInfo.getPayCurrencyAmount(); String code = null; PayCurrencyDTO payCurrencyDTO =
		 * lCCClientPaymentInfo.getPayCurrencyDTO(); if (payCurrencyDTO != null) { code =
		 * payCurrencyDTO.getPayCurrencyCode(); } if (code == null) { code = baseCurrencyCode; }
		 * currencyWisePayAmounts.put(code, amount); } } } } }
		 */

		// above code should be removed. no use of it. it is wrong.

		Iterator<LCCClientReservationPax> lccIterator = reservation.getPassengers().iterator();
		while (lccIterator.hasNext()) {
			LCCClientReservationPax pax = lccIterator.next();
			LCCClientPaymentHolder lCCClientPaymentHolder = pax.getLccClientPaymentHolder();
			if (lCCClientPaymentHolder != null) {
				Collection<LCCClientPaymentInfo> collection = lCCClientPaymentHolder.getPayments();
				if (collection != null) {
					Iterator<LCCClientPaymentInfo> iterator = collection.iterator();
					if (iterator != null) {
						while (iterator.hasNext()) {
							LCCClientPaymentInfo lCCClientPaymentInfo = iterator.next();
							if (lCCClientPaymentInfo != null) {
								// BigDecimal amountz = lCCClientPaymentInfo.getPayCurrencyAmount();
								String code = null;
								PayCurrencyDTO payCurrencyDTO = lCCClientPaymentInfo.getPayCurrencyDTO();
								if (payCurrencyDTO != null) {
									code = payCurrencyDTO.getPayCurrencyCode();
								}
								if (code == null) {
									code = baseCurrencyCode;
								}
								BigDecimal amount = currencyWisePayAmounts.get(code);
								if (amount != null) {
									amount = AccelAeroCalculator.add(amount, lCCClientPaymentInfo.getPayCurrencyAmount());
									currencyWisePayAmounts.put(code, amount);
								} else {
									currencyWisePayAmounts.put(code, lCCClientPaymentInfo.getPayCurrencyAmount());
								}
							}
						}
					}
				}
			}
		}

		// reservation.getPassengers().iterator().next().getLccClientPaymentHolder().getp
		// reservation.getPassengers().iterator().next().getLccClientPaymentHolder().get

		/*
		 * Map<Integer, Collection<ReservationTnx>> paxPayments =
		 * AirproxyModuleUtils.getPassengerBD().getPNRPaxPaymentsAndRefunds( pnrPaxIdSet); for
		 * (Collection<ReservationTnx> paxTnxs : paxPayments.values()) { for (ReservationTnx tnx : paxTnxs) {
		 * ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = tnx.getReservationPaxTnxBreakdown();
		 * 
		 * if (reservationPaxTnxBreakdown != null) { BigDecimal amount =
		 * currencyWisePayAmounts.get(reservationPaxTnxBreakdown.getPaymentCurrencyCode());
		 * 
		 * if (amount != null) { amount = AccelAeroCalculator.add(amount,
		 * reservationPaxTnxBreakdown.getTotalPriceInPayCurrency());
		 * currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(), amount); } else {
		 * currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(),
		 * reservationPaxTnxBreakdown.getTotalPriceInPayCurrency()); }
		 * 
		 * } else { BigDecimal amount = currencyWisePayAmounts.get(baseCurrencyCode);
		 * 
		 * if (amount != null) { amount = AccelAeroCalculator.add(amount, tnx.getAmount());
		 * currencyWisePayAmounts.put(baseCurrencyCode, amount); } else { currencyWisePayAmounts.put(baseCurrencyCode,
		 * tnx.getAmount()); } } } }
		 */

		StringBuilder payCurrencyString = new StringBuilder("");
		int i = 0;
		for (String curr : currencyWisePayAmounts.keySet()) {
			BigDecimal amount = currencyWisePayAmounts.get(curr).abs();

			if (amount.compareTo(BigDecimal.ZERO) > 0) {
				if (i == 0) {
					payCurrencyString.append(AccelAeroCalculator.formatAsDecimal(amount) + " " + curr);
				} else {
					payCurrencyString.append(" + " + AccelAeroCalculator.formatAsDecimal(amount) + " " + curr);
				}
				i++;
			}
		}
		return payCurrencyString.toString();
	}

	private static List<ItineraryFareRuleTO> createFareRules(LCCClientReservation reservation, String languageCode,
			Map<String, String> airportCodeMap) throws ModuleException {
		List<ItineraryFareRuleTO> fareRules = new ArrayList<ItineraryFareRuleTO>();
		Set<String> ondSet = new HashSet<String>();
		Set<Integer> fareRuleSet = new HashSet<Integer>();

		for (LCCClientReservationSegment segment : SortUtil.sort(reservation.getSegments())) {
			if (segment.getFareTO() != null && segment.getFareTO().isShowComments()
					&& !segment.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				if (!ondSet.contains(segment.getSegmentCode())) {
					ItineraryFareRuleTO fareRuleTO = new ItineraryFareRuleTO();
					// fareRuleTO.setOndCode(segment.getFareTO().getSegmentCode());
					String originAirport = "";
					String destinationAirport = "";
					String viaPoint = "";
					if (airportCodeMap != null) {
						String[] airportCodes = segment.getSegmentCode().split("/");
						if (airportCodeMap.get(airportCodes[0]) != null) {
							originAirport = StringUtil.getUnicode(airportCodeMap.get(airportCodes[0]));
						}
						if (airportCodes.length == 2) {
							if (airportCodeMap.get(airportCodes[1]) != null) {
								destinationAirport = StringUtil.getUnicode(airportCodeMap.get(airportCodes[1]));
							}
						} else {
							if (airportCodeMap.get(airportCodes[1]) != null) {
								viaPoint = StringUtil.getUnicode(airportCodeMap.get(airportCodes[1])) + "/";
							}
							if (airportCodeMap.get(airportCodes[2]) != null) {
								destinationAirport = StringUtil.getUnicode(airportCodeMap.get(airportCodes[2]));
							}
						}
					}
					if (("").equals(originAirport) || ("").equals(destinationAirport)) {
						fareRuleTO.setOndCode(segment.getSegmentCode());
					} else {
						fareRuleTO.setOndCode(originAirport + "/" + viaPoint + destinationAirport);
					}

					fareRuleTO.setFareBasisCode(segment.getFareTO().getFareBasisCode());

					if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
						if (!fareRuleSet.contains(segment.getFareTO().getFareRuleID())) {
							fareRuleTO.setFareCategoryDescription(segment.getFareTO().getFareCategoryDescription());
						}
					} else {
						fareRuleTO.setFareCategoryDescription(segment.getFareTO().getFareCategoryDescription());
					}

					if (segment.getFareTO().getFareRuleComments() != null && !segment.getFareTO().getFareRuleComments().isEmpty()) {
						fareRuleTO.setFareRuleComments(segment.getFareTO().getFareRuleComments());
					}
					fareRuleTO.setSegmentTravelExpiary(segment.getOpenReturnTravelExpiry());

					fareRules.add(fareRuleTO);
					// ondSet.add(segment.getFareTO().getSegmentCode());
					ondSet.add(segment.getSegmentCode());
					fareRuleSet.add(new Integer(segment.getFareTO().getFareRuleID()));

				}

			}
		}
		return fareRules;
	}

	public static List<FlexiRuleFlexibilityDTO> createFlexiRules(LCCClientReservation lccClientReservation, String language,
			Map<String, String> airportMap) {
		List<FlexiRuleFlexibilityDTO> flexiInfo = new ArrayList<FlexiRuleFlexibilityDTO>();
		for (LCCClientReservationSegment segment : SortUtil.sort(lccClientReservation.getSegments())) {
			for (LCCClientAlertInfoTO flexiAlert : lccClientReservation.getFlexiAlertInfoTOs()) {
				if (flexiAlert.getFlightSegmantRefNumber().equals(segment.getPnrSegID().toString())) {
					String flexiMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiMessage", new Locale(language));
					String flexiCnxMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiCnxMessage", new Locale(language));
					String flexiModCnxMessage = I18NMessagingUtil.getMessage("itinerary_lblFlexiModCnxMessage", new Locale(
							language));
					List<LCCClientAlertTO> alertsList = flexiAlert.getAlertTO();
					/*
					 * Alert id 1 means modification and number of modifications contain in the alert content Alert id 2
					 * means cancellation and we dont need the number of cancellations because it is only one always
					 * Alert id 0 means buffer time in hours. We can push this to cancellation content but if will fail
					 * when a system doesnt have cancellation flexibily. Eg 3O
					 */
					for (LCCClientAlertTO alertTO : alertsList) {
						if (alertTO.getAlertId() == 1) {
							flexiMessage = flexiMessage.replace("#1", alertTO.getContent());
						} else if (alertTO.getAlertId() == 2) {
							flexiMessage = flexiMessage.replace("#3", flexiModCnxMessage);
						} else if (alertTO.getAlertId() == 0) {
							flexiMessage = flexiMessage.replace("#2", alertTO.getContent());
							flexiCnxMessage = flexiCnxMessage.replace("#2", alertTO.getContent());
						}
					}
					// All modifications consumed
					if (flexiMessage.contains("#1")) {
						flexiMessage = flexiCnxMessage;
					}
					// Flexi rule has no cancellations
					if (flexiMessage.contains("#3")) {
						flexiMessage = flexiMessage.replace("#3", "");
					}

					FlexiRuleFlexibilityDTO flexiDTO = new FlexiRuleFlexibilityDTO();
					String[] airportCodes = segment.getSegmentCode().split("/");
					String fromAirport = SubStationUtil.getSubStationName(airportCodes[0].toUpperCase(),
							segment.getSubStationShortName(), airportMap.get(airportCodes[0].toUpperCase()));
					String toAirport = (SubStationUtil
							.getSubStationName(airportCodes[airportCodes.length - 1].toUpperCase(),
									segment.getSubStationShortName(),
									airportMap.get(airportCodes[airportCodes.length - 1].toUpperCase())));
					flexiDTO.setFlexibilityDescription(flexiMessage);
					flexiDTO.setSegmentCode(fromAirport + " / " + toAirport);
					flexiInfo.add(flexiDTO);
				}
			}
		}
		return flexiInfo;
	}

	private static List<ItineraryFlightOndGroup> createFlightOndGroups(LCCClientReservation reservation,
			Map<String, String> airportMap, String language, Map<String, String> airportNameMap, ItineraryDTO itineraryDTO)
			throws ModuleException {

		Integer ondSeq = 1;
		Map<Integer, ItineraryFlightOndGroup> flightOndGroupMap = new HashMap<Integer, ItineraryFlightOndGroup>();
		String lastSegCode = "";
		boolean returnFlagFound = false;

		int standbyTravelValidityDays = 0;
		boolean showStandbyTicketValidity = false;
		if (AppSysParamsUtil.isStandbyTicketValidityShownInItinerary()) {
			standbyTravelValidityDays = AppSysParamsUtil.standbyTicketTravelValidityInDays();
		}

		Map<Integer, BundledFareDTO> bundledServicePeriodIdWiseDetails = new HashMap<Integer, BundledFareDTO>();
		if (reservation.getBundledFareDTOs() != null) {
			for (BundledFareDTO bundledFareDTO : reservation.getBundledFareDTOs()) {
				if (bundledFareDTO != null) {
					bundledServicePeriodIdWiseDetails.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
				}
			}
		}

		LCCClientReservationSegment[] resSegs = SortUtil.sortByDepDate(reservation.getSegments());
		int finalJourneyOnd = resSegs[resSegs.length - 1].getJourneySequence();

		if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
			Set<LCCClientReservationSegment> allSegs = new HashSet<LCCClientReservationSegment>();
			allSegs.addAll(reservation.getSegments());

			Set<LCCClientReservationSegment> transformedOtherAirlineSegs = transformOtherAirlineSegments(reservation
					.getOthertAirlineSegments());
			allSegs.addAll(transformedOtherAirlineSegs);
			resSegs = SortUtil.sortByLocalDepDate(allSegs);
		}

		for (LCCClientReservationSegment segment : resSegs) {
			// We only show non-cnx segments in itinerary
			if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(segment.getStatus())) {
				ItineraryFlightTO flight = new ItineraryFlightTO();

				String ondFullText = "";
				String[] airportCodes = segment.getSegmentCode().split("/");
				String originAirport = "";
				String destinationAirport = "";
				if ((!("en").equals(language)) && (airportNameMap.get(airportCodes[0]) != null)
						&& (airportNameMap.get(airportCodes[1]) != null)) {
					originAirport = StringUtil.getUnicode(airportNameMap.get(airportCodes[0].toUpperCase()));
					destinationAirport = StringUtil.getUnicode(airportNameMap.get(airportCodes[airportCodes.length - 1]
							.toUpperCase()));
				} else {
					originAirport = airportMap.get(airportCodes[0].toUpperCase());
					destinationAirport = airportMap.get(airportCodes[airportCodes.length - 1].toUpperCase());
				}
				flight.setOriginAirport(SubStationUtil.getSubStationName(airportCodes[0].toUpperCase(),
						segment.getSubStationShortName(), originAirport));
				flight.setDestinationAirport(SubStationUtil.getSubStationName(
						airportCodes[airportCodes.length - 1].toUpperCase(), segment.getSubStationShortName(), destinationAirport));
				// ondFullText = airportMap.get(airportCodes[0].toUpperCase())+" / " +
				// airportMap.get(airportCodes[1].toUpperCase());

				flight.setOriginAptCode(airportCodes[0].toUpperCase());
				flight.setDestinationAptCode(airportCodes[airportCodes.length - 1].toUpperCase());

				String originAirportName = airportMap.get(airportCodes[0].toUpperCase());
				// append the departure terminal name to the origin airport name.
				if (segment.getDepartureTerminalName() != null) {
					originAirportName += " - " + segment.getDepartureTerminalName();
					// This is temp fix for MAHAN
					if (("fa").equals(language)) {
						flight.setOriginAptTerminalCode("");
					} else {
						flight.setOriginAptTerminalCode(segment.getDepartureTerminalName());
					}
				}

				String destinationAirportName = airportMap.get(airportCodes[airportCodes.length - 1].toUpperCase());
				// append the arrival terminal name to the destination airport name.
				if (segment.getArrivalTerminalName() != null) {
					destinationAirportName += " - " + segment.getArrivalTerminalName();
					if (("fa").equals(language)) {
						flight.setDestinationAptTerminalCode("");
					} else {
						flight.setDestinationAptTerminalCode(segment.getArrivalTerminalName());
					}
				}

				// oNd text to be displayed on the itinerary.
				ondFullText = originAirportName + " / " + destinationAirportName;

				if (ondFullText.lastIndexOf("/") == ondFullText.length() - 1) {
					ondFullText = ondFullText.substring(0, ondFullText.lastIndexOf("/"));
				}
				flight.setOndFullText(ondFullText);
				flight.setSegmentSeq(segment.getSegmentSeq());

				if (reservation.isGroupPNR()) {
					String revSegCode = airportCodes[1] + "/" + airportCodes[0];
					if ((!lastSegCode.equals("") && !lastSegCode.split("/")[1].equals(airportCodes[0]))
							|| revSegCode.equals(lastSegCode)) {
						ondSeq++;
					}
				} else {
					if ("Y".equals(segment.getReturnFlag()) && !returnFlagFound) {
						ondSeq++;
						returnFlagFound = true;
					} else {
						if (!lastSegCode.equals("") && !lastSegCode.split("/")[1].equals(airportCodes[0])) {
							ondSeq++;
						}
					}
				}
				lastSegCode = segment.getSegmentCode();
				flight.setOndSeq(ondSeq);

				if (segment.getBookingType().equals("STANDBY")) {
					flight.setStatus(ReservationInternalConstants.ReservationStatus.STAND_BY);
					if (AppSysParamsUtil.isStandbyTicketValidityShownInItinerary()) {
						Calendar cal = new GregorianCalendar();
						cal.setTime(segment.getDepartureDate());
						cal.add(Calendar.DATE, standbyTravelValidityDays);
						flight.setStandbyTravelValidityEnd(cal.getTime());
						showStandbyTicketValidity = true;
					}
				} else {
					flight.setStatus(reservation.getStatus()); // For itinerary we use resevervation status, not segment
																// status
				}

				flight.setDisplayStatus(BeanUtils.nullHandler(segment.getDisplayStatus()));
				flight.setOpenReturnSegment(segment.isOpenReturnSegment());
				flight.setOpenReturnConfirmBeforeLocal(segment.getOpenRetConfirmBefore());
				flight.setSegmentExpiryLocal(segment.getOpenReturnTravelExpiry());

				flight.setFlightNo(segment.getFlightNo());

				if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
					DateFormat dateFormatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa",
							"IR", ""));
					DateFormat timeFormatter = new PersianDateFormat("HH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					flight.setDepartureDateInPersian(dateFormatter.format(segment.getDepartureDate()));
					flight.setArrivalDateInPersian(dateFormatter.format(segment.getArrivalDate()));
					flight.setDepartureTimeInPersian(timeFormatter.format(segment.getDepartureDate()));
					flight.setArrivalTimeInPersian(timeFormatter.format(segment.getArrivalDate()));

					if(segment.getDepartureDateZulu() != null && segment.getArrivalDateZulu() != null){
						flight.setDepartureDateTimeZuluInPersian(formatter.format(segment.getDepartureDateZulu()));
						flight.setArrivalDateTimeZuluInPersian(formatter.format(segment.getArrivalDateZulu()));
						flight.setDepartureDateTimeZulu(segment.getDepartureDateZulu());
						flight.setArrivalDateTimeZulu(segment.getArrivalDateZulu());
					}
					flight.setIsPersianDatesSet(true);
					flight.setDepartureDateTime(segment.getDepartureDate());
					flight.setArrivalDateTime(segment.getArrivalDate());
					
				} else {
					flight.setDepartureDateTime(segment.getDepartureDate());
					flight.setArrivalDateTime(segment.getArrivalDate());
					flight.setDepartureDateTimeZulu(segment.getDepartureDateZulu());
					flight.setArrivalDateTimeZulu(segment.getArrivalDateZulu());
				}

				flight.setSegmentCode(segment.getSegmentCode());

				String segmentAirlineCode = segment.getFlightNo().substring(0, 2);

				if (segment.getCheckInTimeGap() > 0) {

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa",
								"IR", ""));
						DateFormat timeFormatter = new PersianDateFormat("HH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
						flight.setCheckInDateInPersian(formatter.format(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInTimeGap())));
						flight.setCheckInTimeInPersian(timeFormatter.format(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInTimeGap())));
						flight.setIsPersianDatesSet(true);
					} else {
						flight.setCheckInDateTime(getCheckInDate(segment.getDepartureDate(), segment.getCheckInTimeGap()));
					}

				} else {

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa",
								"IR", ""));
						DateFormat timeFormatter = new PersianDateFormat("HH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
						flight.setCheckInDateInPersian(formatter.format(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInTimeGap())));
						flight.setCheckInTimeInPersian(timeFormatter.format(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInTimeGap())));
						flight.setIsPersianDatesSet(true);
					} else {
						flight.setCheckInDateTime(ReservationApiUtils.getCheckInDate(segment.getDepartureDate(), airportCodes[0],
								segment.getFlightNo()));
					}

				}

				if (segment.getCheckInClosingTime() > 0) {
					if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy HH:mm", new com.ibm.icu.util.ULocale(
								"fa", "IR", ""));
						flight.setCheckInClosingDateTimeInPersian(formatter.format(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInClosingTime())));
						flight.setIsPersianDatesSet(true);
					} else {
						flight.setCheckInClosingDateTime(getCheckInDate(segment.getDepartureDate(),
								segment.getCheckInClosingTime()));
					}

				} else {

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy HH:mm", new com.ibm.icu.util.ULocale(
								"fa", "IR", ""));
						flight.setCheckInClosingDateTimeInPersian(formatter.format(ReservationApiUtils.getCheckInClosingDate(
								segment.getDepartureDate(), airportCodes[0], segment.getFlightNo())));
						flight.setIsPersianDatesSet(true);
					} else {
						flight.setCheckInClosingDateTime(ReservationApiUtils.getCheckInClosingDate(segment.getDepartureDate(),
								airportCodes[0], segment.getFlightNo()));
					}

				}

				flight.setCarrierLogoURL(StaticFileNameUtil.getCorrected("LogoThumbnail" + segmentAirlineCode + ".jpg"));

				if (segment.getArrivalDateZulu() != null && segment.getDepartureDateZulu() != null) {
					flight.setDurationInMillis(segment.getArrivalDateZulu().getTime() - segment.getDepartureDateZulu().getTime());
				} else {
					flight.setDurationInMillis(segment.getArrivalDate().getTime() - segment.getDepartureDate().getTime());
				}

				flight.setCabinClassCode(segment.getCabinClassCode() != null ? segment.getCabinClassCode() : "");

				if (segment.getCabinClassCode() != null && segment.getCabinClassCode().compareTo("") != 0) {
					flight.setCabinClassDescription(CommonsServices.getGlobalConfig().getActiveCabinClassesMap()
							.get(segment.getCabinClassCode()));
				} else {
					flight.setCabinClassDescription("");
				}

				if (segment.getLogicalCabinClass() != null && segment.getLogicalCabinClass().compareTo("") != 0) {
					flight.setLogicalCCCode(segment.getLogicalCabinClass());
					LogicalCabinClassDTO logicalCC = (CommonsServices.getGlobalConfig().getAvailableLogicalCCMap(language)
							.get(segment.getLogicalCabinClass()));
					if (logicalCC != null) {
						if (segment.getFlexiRuleID() != null) {
							flight.setLogicalCCDescription(logicalCC.getFlexiDescription());
						} else {
							flight.setLogicalCCDescription(logicalCC.getDescription());
						}
					} else {
						flight.setLogicalCCDescription("");
					}
				} else {
					flight.setLogicalCCCode("");
					flight.setLogicalCCDescription("");
				}

				if (!flightOndGroupMap.containsKey(ondSeq)) {
					ItineraryFlightOndGroup ondGroup = new ItineraryFlightOndGroup(ondSeq);
					flightOndGroupMap.put(ondSeq, ondGroup);
				}
				flight.setLocale(language);

				String fltDuration = segment.getFlightDuration();
				String fltModelDescription = segment.getFlightModelDescription();
				String fltModelNo = segment.getFlightModelNumber();
				String fltRemarks = segment.getRemarks();

				fltDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDuration);
				fltModelDescription = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelDescription);
				fltModelNo = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelNo);
				fltRemarks = FlightStopOverDisplayUtil.formatNullOrEmpty(fltRemarks);

				flight.setFlightDuration(fltDuration);
				if (flight.isPersianDatesSet() && !fltDuration.equals("-") && flight.getDurationInMillis() > 0) {
					DateFormat df = new PersianDateFormat("HH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
					flight.setFlightDurationInPersian(df.format(flight.getDurationInMillis()));
				}
				flight.setFlightModelDescription(fltModelDescription);
				flight.setFlightModelNumber(fltModelNo);
				flight.setRemarks(fltRemarks);

				String stopOverDuration = FlightStopOverDisplayUtil.getStopOverDuration(reservation.getSegments(), segment);

				stopOverDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(stopOverDuration);
				flight.setFlightStopOverDuration(stopOverDuration);

				String[] hoursNMins = stopOverDuration.split(":");
				if (flight.isPersianDatesSet() && !stopOverDuration.equals("-")) {
					try {
						long stopOverDurationInMillis = Long.parseLong(hoursNMins[0]) * 60 * 60 * 1000
								+ Long.parseLong(hoursNMins[1]) * 60 * 1000;
						DateFormat df = new PersianDateFormat("HH:mm", new com.ibm.icu.util.ULocale("fa", "IR", ""));
						flight.setFlightDurationInPersian(df.format(stopOverDurationInMillis));
					} catch (Exception e) {
						flight.setFlightDurationInPersian("-");
					}
				}
				if ("en".equals(language)) {
					flight.setNoOfStops(FlightStopOverDisplayUtil.getStopOverStations(segment.getSegmentCode(), language,
							airportMap));
				} else {
					flight.setNoOfStops(FlightStopOverDisplayUtil.getStopOverStations(segment.getSegmentCode(), language,
							airportNameMap));
				}
				flight.setFlightBaggageAllowance(segment.getFlightBaggageAllowance() + " Kg");

				if (segment.getJourneySequence() != null && finalJourneyOnd == segment.getJourneySequence()
						&& segment.getTicketValidTill() != null) {

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(language)) {
						DateFormat formatter = new PersianDateFormat("dd MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
						flight.setTicketExpiryDateInPersian(formatter.format(segment.getTicketValidTill()));
						flight.setTicketValidTill(segment.getTicketValidTill());
						flight.setIsPersianDatesSet(true);
					} else {
						flight.setTicketValidTill(segment.getTicketValidTill());
					}

				}

				if (segment.getBundledServicePeriodId() != null
						&& bundledServicePeriodIdWiseDetails.containsKey(segment.getBundledServicePeriodId())) {
					BundledFareDTO bundledFareDTO = bundledServicePeriodIdWiseDetails.get(segment.getBundledServicePeriodId());
					flight.setBundledServiceName(bundledFareDTO.getBundledFareName());
				} else {
					flight.setBundledServiceName("");
				}
				
				if (segment.getCsOcFlightNumber() != null && segment.getCsOcFlightNumber().compareTo("") != 0) {
					flight.setCsOcFlightNumber(segment.getCsOcFlightNumber());
				} else {
					flight.setCsOcFlightNumber("");
				}

				flightOndGroupMap.get(ondSeq).addFlight(flight);
			}
		}

		itineraryDTO.setShowStandbyTravelValidity(showStandbyTicketValidity);
		List<ItineraryFlightOndGroup> ondList = new ArrayList<ItineraryFlightOndGroup>();
		for (ItineraryFlightOndGroup ondGrp : flightOndGroupMap.values()) {
			ondList.add(ondGrp);
		}
		Collections.sort(ondList);
		return ondList;
	}

	private static Set<LCCClientReservationSegment> transformOtherAirlineSegments(
			Set<LCCClientOtherAirlineSegment> otherAirlineSegments) {
		Set<LCCClientReservationSegment> reservationSegs = new HashSet<LCCClientReservationSegment>();
		for (LCCClientOtherAirlineSegment otherAirlineSegment : otherAirlineSegments) {
			if (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED.equals(otherAirlineSegment
					.getStatus())) {
				LCCClientReservationSegment resSeg = new LCCClientReservationSegment();
				resSeg.setDepartureDate(otherAirlineSegment.getDepartureDateTimeLocal());
				resSeg.setArrivalDate(otherAirlineSegment.getArrivalDateTimeLocal());
				resSeg.setFlightNo(otherAirlineSegment.getFlightNumber());
				resSeg.setSegmentCode(otherAirlineSegment.getSegmentCode());
				resSeg.setStatus(otherAirlineSegment.getStatus());
				resSeg.setSegmentSeq(0);
				resSeg.setBookingType(ReservationInternalConstants.ReservationStatus.CONFIRMED);
				resSeg.setCabinClassCode(otherAirlineSegment.getCabinClassCode());
				reservationSegs.add(resSeg);
			}
		}
		return reservationSegs;
	}

	private static List<Map<String, String>> setStationContacts(LCCClientReservation reservation) throws ModuleException {

		LCCClientReservationSegment[] resSegs = SortUtil.sortByDepDate(reservation.getSegments());
		List<Map<String, String>> stationList = new ArrayList<Map<String, String>>();
		Set<String> tempAirports = new LinkedHashSet<String>();

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			if (reservation.getStationContacts() != null && reservation.getStationContacts().size() > 0) {
				Set<StationContactDTO> lccStations = reservation.getStationContacts();
				for (StationContactDTO lccStation : lccStations) {
					Map<String, String> lccStationMap = new LinkedHashMap<String, String>();
					lccStationMap.put("stationName", lccStation.getStationName());
					lccStationMap.put("stationCode", lccStation.getStationCode());
					lccStationMap.put("stationContact", lccStation.getStationContact());
					lccStationMap.put("stationTelephone", lccStation.getStationTelephone());
					stationList.add(lccStationMap);
				}
			}
		}

		for (LCCClientReservationSegment segment : resSegs) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())) {
				continue;
			}
			String[] airportCodes = segment.getSegmentCode().split("/");
			for (String airportCode : airportCodes) {
				tempAirports.add(airportCode);
			}
		}

		for (String strAirport : tempAirports) {
			Map<String, String> stationContactDetails = AirproxyModuleUtils.getLocationDB().getStationContactDetails(strAirport);
			if (stationContactDetails != null) {
				if (stationList.size() > 0) {
					boolean hasStationContactAlready = false;
					for (Map<String, String> stationContactMap : stationList) {
						if (stationContactMap.get("stationCode").equals(stationContactDetails.get("stationCode"))) {
							hasStationContactAlready = true;
						}
					}
					if (!hasStationContactAlready) {
						stationList.add(stationContactDetails);
					}
				} else {
					stationList.add(stationContactDetails);
				}
			}
		}

		return stationList;
	}

	private static List<ItineraryFlightOndGroup> createOpenRetFlightOndGroups(List<ItineraryFlightOndGroup> ondList)
			throws ModuleException {
		List<ItineraryFlightOndGroup> openReturnOndList = new ArrayList<ItineraryFlightOndGroup>();
		for (ItineraryFlightOndGroup flightOnd : ondList) {
			for (ItineraryFlightTO flight : flightOnd.getFlights()) {
				if (flight.isOpenReturnSegment()) {
					openReturnOndList.add(flightOnd);
					break;
				}
			}
		}

		return openReturnOndList;
	}

	/**
	 * @param lccClientReservation
	 * @param languageCode
	 *            TODO
	 * @param airportCodeMap
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private static List<ItineraryPassengerEticketTO> createTicketingInfo(LCCClientReservation lccClientReservation,
			String languageCode, Map<String, String> airportCodeMap) throws ModuleException {
		// set passenger eticketInfo
		List<ItineraryPassengerEticketTO> itineraryPassengerEticketTOs = new ArrayList<ItineraryPassengerEticketTO>();
		for (LCCClientReservationPax lccPassenger : lccClientReservation.getPassengers()) {
			ItineraryPassengerEticketTO itineraryPassengerTicketInfo = new ItineraryPassengerEticketTO();
			if (lccPassenger.getLccClientPaxNameTranslations() != null
					&& languageCode.equals(lccPassenger.getLccClientPaxNameTranslations().getLanguageCode())) {
				itineraryPassengerTicketInfo.setPassengerDisplayName(composeName(
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getTitleOl()),
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getFirstNameOl()),
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getLastNameOl())));
			} else {
				itineraryPassengerTicketInfo.setPassengerDisplayName(composeName(lccPassenger.getTitle(),
						lccPassenger.getFirstName(), lccPassenger.getLastName()));
			}
			itineraryPassengerTicketInfo.setPaxSequnce(lccPassenger.getPaxSequence());
			itineraryPassengerTicketInfo.setPassengerType(lccPassenger.getPaxType());

			for (LccClientPassengerEticketInfoTO eTcicketInfo : lccPassenger.geteTickets()) {
				if (eTcicketInfo.getPaxETicketStatus().equals(EticketStatus.OPEN.code())
						|| eTcicketInfo.getPaxETicketStatus().equals(EticketStatus.BOARDED.code())
						|| eTcicketInfo.getPaxETicketStatus().equals(EticketStatus.CHECKEDIN.code())
						|| eTcicketInfo.getPaxETicketStatus().equals(EticketStatus.FLOWN.code())) {
					ItineraryEticketTO itineraryEticketTo = new ItineraryEticketTO();
					String segmentCode = getSegmentCodeInOtherLanguage(airportCodeMap, eTcicketInfo.getSegmentCode());
					itineraryEticketTo.setSegmentCode(segmentCode);
					
					String eTicketNo = null;
					if (eTcicketInfo.getExternalPaxETicketNo() != null && eTcicketInfo.getExternalPaxETicketNo().length() > 0) {
						eTicketNo = eTcicketInfo.getExternalPaxETicketNo();
					} else {
						eTicketNo = eTcicketInfo.getPaxETicketNo();
					}
					itineraryEticketTo.seteTicketNumber(eTicketNo + "/" + eTcicketInfo.getCouponNo());
					itineraryEticketTo.setFlightNo(eTcicketInfo.getFlightNo());

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode) && eTcicketInfo.getDepartureDate() != null) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy", new com.ibm.icu.util.ULocale("fa",
								"IR", ""));
						itineraryEticketTo.setDepartureDate(eTcicketInfo.getDepartureDate());
						itineraryEticketTo.setDepartureDateInPersian(formatter.format(eTcicketInfo.getDepartureDate()));
						itineraryEticketTo.setIsPersianDatesSet(true);
					} else {
						itineraryEticketTo.setDepartureDate(eTcicketInfo.getDepartureDate());
					}
					itineraryPassengerTicketInfo.geteTickets().add(itineraryEticketTo);
				}
			}
			Collections.sort(itineraryPassengerTicketInfo.geteTickets());
			itineraryPassengerEticketTOs.add(itineraryPassengerTicketInfo);
		}
		Collections.sort(itineraryPassengerEticketTOs);
		return itineraryPassengerEticketTOs;
	}

	private static List<ItineraryPassengerTO> createPassengers(LCCClientReservation lccClientReservation,
			ItineraryDTO itineraryDTO, String languageCode) throws ModuleException {

		List<ItineraryPassengerTO> passengerList = new ArrayList<ItineraryPassengerTO>();
		Set<LCCClientReservationPax> lccReservationsPaxList = new HashSet<LCCClientReservationPax>();
		boolean selectedPaxForItinerary = false;

		BigDecimal totalAvailBalance = new BigDecimal(0);
		BigDecimal totalFare = new BigDecimal(0);
		BigDecimal totalTaxes = new BigDecimal(0);
		BigDecimal totalSurcharges = new BigDecimal(0);
		BigDecimal totalAmountDue = new BigDecimal(0);
		BigDecimal totalCharges = new BigDecimal(0);
		BigDecimal totalDiscount = new BigDecimal(0);
		BigDecimal totalPaidAmount = new BigDecimal(0);
		String itineraryFareMask = lccClientReservation.getItineraryFareMask();

		String policyCode = null;
		if (lccClientReservation.getReservationInsurances() != null && !lccClientReservation.getReservationInsurances().isEmpty()) {
			if (lccClientReservation.getReservationInsurances().get(0) != null) {
				policyCode = lccClientReservation.getReservationInsurances().get(0).getPolicyCode();
			}
		}

		boolean insured = (policyCode != null && !policyCode.equals(""));
		ItineraryAncillaryAvailabilityTO anciAvailTO = new ItineraryAncillaryAvailabilityTO();
		anciAvailTO.setInsuranceAvailable(insured);
		itineraryDTO.setAnciAvailabilityTO(anciAvailTO);

		if (itineraryDTO.getSelectedPaxDetails() != null && !itineraryDTO.getSelectedPaxDetails().equals("")) {
			String[] selPassangers = itineraryDTO.getSelectedPaxDetails().split(",");
			for (String selectedPax : selPassangers) {
				if (selectedPax != null && !selectedPax.equals("")) {
					for (LCCClientReservationPax lccPax : lccClientReservation.getPassengers()) {
						if (lccPax.getPaxSequence().toString().equals(selectedPax)) {
							lccReservationsPaxList.add(lccPax);
						} else if (lccPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)
								&& lccPax.getParent() != null
								&& lccPax.getParent().getPaxSequence().equals(Integer.parseInt(selectedPax))) {
							lccReservationsPaxList.add(lccPax);
						}
					}
				}
			}
			selectedPaxForItinerary = true;
		} else {
			lccReservationsPaxList.addAll(lccClientReservation.getPassengers());
		}

		lccClientReservation.getPassengers().clear();
		lccClientReservation.getPassengers().addAll(lccReservationsPaxList);
		for (LCCClientReservationPax lccPassenger : SortUtil.sortPax(lccClientReservation.getPassengers())) {

			if (ReservationInternalConstants.PassengerType.INFANT.compareTo(lccPassenger.getPaxType()) == 0) {
				continue;
			}

			ItineraryPassengerTO passenger = new ItineraryPassengerTO();
			passengerList.add(passenger);

			// Pax Sequence
			passenger.setPaxSeq(lccPassenger.getPaxSequence());

			// Pax Display Name

			if (lccPassenger.getLccClientPaxNameTranslations() != null
					&& languageCode.equals(lccPassenger.getLccClientPaxNameTranslations().getLanguageCode())) {
				passenger.setPaxDisplayName(composeName(
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getTitleOl()),
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getFirstNameOl()),
						StringUtil.getUnicode(lccPassenger.getLccClientPaxNameTranslations().getLastNameOl())));
			} else {
				passenger.setPaxDisplayName(composeName(lccPassenger.getTitle(), lccPassenger.getFirstName(),
						lccPassenger.getLastName()));
			}
			// Pax Nationality
			passenger.setPaxNationality(lccPassenger.getNationality());

			if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode) && lccPassenger.getDateOfBirth() != null) {
				DateFormat formatter = new PersianDateFormat("dd MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
				passenger.setDateOfBirthInPersian(formatter.format(lccPassenger.getDateOfBirth()));
				passenger.setDateOfBirth(lccPassenger.getDateOfBirth());
				passenger.setIsPersianDatesSet(true);
			} else {
				passenger.setDateOfBirth(lccPassenger.getDateOfBirth());
			}

			if (lccPassenger.getLccClientAdditionPax() != null) {
				String psptNo = lccPassenger.getLccClientAdditionPax().getPassportNo();
				Date psptExpiryDt = lccPassenger.getLccClientAdditionPax().getPassportExpiry();
				String psptIssuedCntry = lccPassenger.getLccClientAdditionPax().getPassportIssuedCntry();

				if (psptExpiryDt != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					passenger.setFoidExpiry(sdf.format(psptExpiryDt));
				} else {
					passenger.setFoidExpiry("");
				}

				passenger.setFoidPlace(BeanUtils.nullHandler(psptIssuedCntry));

				if (psptNo != null) {
					/*
					 * split FOID number to get PSPT #, expiry date & issued location This is to support for old records
					 * with comma separated values
					 */

					String[] foidArr = psptNo.split(ReservationInternalConstants.PassengerConst.FOID_SEPARATOR);

					if (foidArr.length == 0) {
						passenger.setFoidNumber("");
					} else if (foidArr.length == 1) {
						passenger.setFoidNumber(BeanUtils.nullHandler(foidArr[0]));
					} else if (foidArr.length == 2) {
						passenger.setFoidNumber(BeanUtils.nullHandler(foidArr[0]));
						if (passenger.getFoidExpiry().equals("")) {
							passenger.setFoidExpiry(BeanUtils.nullHandler(foidArr[1]));
						}
					} else if (foidArr.length == 3) {
						passenger.setFoidNumber(BeanUtils.nullHandler(foidArr[0]));
						if (passenger.getFoidExpiry().equals("")) {
							passenger.setFoidExpiry(BeanUtils.nullHandler(foidArr[1]));
						}
						if (passenger.getFoidPlace().equals("")) {
							passenger.setFoidPlace(BeanUtils.nullHandler(foidArr[2]));
						}
					}
				} else {
					passenger.setFoidNumber("");
				}

				if (lccPassenger.getLccClientAdditionPax().getEmployeeId() != null) {
					passenger.setEmployeeId(lccPassenger.getLccClientAdditionPax().getEmployeeId());
				}

			} else {
				passenger.setFoidNumber("");
				passenger.setFoidExpiry("");
				passenger.setFoidPlace("");
			}

			// Pax Type
			String paxType = lccPassenger.getPaxType();
			passenger.setPaxType(paxType);

			List<ItinerarySegmentAncillaryTO> paxAnciSegs = new ArrayList<ItinerarySegmentAncillaryTO>();
			List<LCCSelectedSegmentAncillaryDTO> lccPaxAnci = lccPassenger.getSelectedAncillaries();

			for (LCCSelectedSegmentAncillaryDTO lccAnci : SortUtil.sortPaxWiseAnci(lccPaxAnci)) {
				ItinerarySegmentAncillaryTO segAnci = new ItinerarySegmentAncillaryTO();
				ItinerarySegmentTO seg = new ItinerarySegmentTO();
				ItinerarySeatTO paxSeat = new ItinerarySeatTO();
				String seatNo = "";
				if (lccAnci.getAirSeatDTO() != null && lccAnci.getAirSeatDTO().getSeatNumber() != null) {
					seatNo = lccAnci.getAirSeatDTO().getSeatNumber();
				}
				paxSeat.setSeatNumber(seatNo);
				if (!seatNo.equals("")) {
					anciAvailTO.setSeatAvailable(true);
				}

				List<ItinerarySeatTO> paxExtraSeatList = new ArrayList<ItinerarySeatTO>();
				if (lccAnci.getExtraSeatDTOs() != null && lccAnci.getExtraSeatDTOs().size() > 0) {
					for (LCCAirSeatDTO lccExtraSeat : lccAnci.getExtraSeatDTOs()) {
						ItinerarySeatTO paxExtraSeat = new ItinerarySeatTO();
						paxExtraSeat.setSeatNumber(lccExtraSeat.getSeatNumber());
						paxExtraSeatList.add(paxExtraSeat);
						anciAvailTO.setExtraSeatAvailable(true);
					}

				}
				seg.setSegmentCode(BeanUtils.nullHandler(lccAnci.getFlightSegmentTO().getSegmentCode()));
				seg.setFlightNo(BeanUtils.nullHandler(lccAnci.getFlightSegmentTO().getFlightNumber()));
				List<ItineraryMealTO> paxMList = new ArrayList<ItineraryMealTO>();
				for (LCCMealDTO lccMeal : lccAnci.getMealDTOs()) {
					ItineraryMealTO paxMeal = new ItineraryMealTO();
					paxMeal.setMealCode(lccMeal.getMealCode());
					paxMeal.setMealName(lccMeal.getMealName());
					paxMeal.setSoldMeals(lccMeal.getSoldMeals());
					paxMList.add(paxMeal);
					anciAvailTO.setMealAvailable(true);
				}
				List<ItineraryBaggageTO> paxBGList = new ArrayList<ItineraryBaggageTO>();
				for (LCCBaggageDTO lccBaggage : lccAnci.getBaggageDTOs()) {
					ItineraryBaggageTO paxBaggage = new ItineraryBaggageTO();

					paxBaggage.setBaggageName(lccBaggage.getBaggageName());
					paxBGList.add(paxBaggage);
					anciAvailTO.setBaggageAvailable(true);
				}
				List<ItinerarySSRTO> paxSSRList = new ArrayList<ItinerarySSRTO>();
				for (LCCSpecialServiceRequestDTO lccSSR : lccAnci.getSpecialServiceRequestDTOs()) {
					ItinerarySSRTO paxSSR = new ItinerarySSRTO();
					paxSSR.setSsrCode(lccSSR.getSsrCode());
					paxSSR.setDescription(lccSSR.getDescription());
					paxSSR.setText(lccSSR.getText());
					paxSSRList.add(paxSSR);
					anciAvailTO.setSsrAvailable(true);
				}
				List<ItineraryAirportServiceTO> paxAPSList = new ArrayList<ItineraryAirportServiceTO>();
				for (LCCAirportServiceDTO lccAPS : lccAnci.getAirportServiceDTOs()) {
					ItineraryAirportServiceTO paxAPS = new ItineraryAirportServiceTO();
					paxAPS.setSsrCode(lccAPS.getSsrCode());
					paxAPS.setSsrText(lccAPS.getServiceNumber());
					paxAPS.setDescription(lccAPS.getSsrName());
					paxAPS.setAirportCode(lccAPS.getAirportCode());
					paxAPSList.add(paxAPS);
					anciAvailTO.setAirportServiceAvailable(true);
				}

				List<ItineraryAirportTransferTO> paxAPTList = new ArrayList<ItineraryAirportTransferTO>();
				if (lccAnci.getAirportTransferDTOs() != null && lccAnci.getAirportTransferDTOs().size() > 0) {
					for (LCCAirportServiceDTO lccAPT : lccAnci.getAirportTransferDTOs()) {
						ItineraryAirportTransferTO paxAPT = new ItineraryAirportTransferTO();
						paxAPT.setSsrCode(lccAPT.getSsrCode());
						paxAPT.setDescription(lccAPT.getSsrName());
						paxAPT.setTransferAddress(lccAPT.getTransferAddress());
						paxAPT.setTransferDate(lccAPT.getTranferDate());
						paxAPT.setTransferType(deriveTransferType(lccAPT.getTransferType()));
						AirportTransfer airportTransfer = AirproxyModuleUtils.getAirportTransferBD().getAirportTransfer(
								lccAPT.getAirportTransferId());
						// ServiceProviderDTO provider = lccAPT.getProvider();
						paxAPT.setProviderName(airportTransfer.getProviderName());
						paxAPT.setProviderEmail(airportTransfer.getProviderEmail());
						paxAPT.setProviderContact(airportTransfer.getProviderNumber());

						paxAPTList.add(paxAPT);
						anciAvailTO.setAirportTransferAvailable(true);
					}
				}

				segAnci.setSegment(seg);
				segAnci.setInsured(insured);
				segAnci.setMeals(paxMList);
				segAnci.setBaggages(paxBGList);
				segAnci.setSeat(paxSeat);
				segAnci.setSsrs(paxSSRList);
				segAnci.setApss(paxAPSList);
				segAnci.setExtraSeats(paxExtraSeatList);
				segAnci.setApts(paxAPTList);
				paxAnciSegs.add(segAnci);
			}
			passenger.setAncillary(paxAnciSegs);

			// Infant Display Name (if infant attached)
			String infantDisplayName = "";
			Date infantDob = null;
			BigDecimal[] totalInfantChargeAmounts = new BigDecimal[7];
			// initialize the infant amounts array to zero, as there might be passengers without infants.
			for (int x = 0; x < totalInfantChargeAmounts.length; x++) {
				totalInfantChargeAmounts[x] = AccelAeroCalculator.getDefaultBigDecimalZero();
			}

			if (ReservationInternalConstants.PassengerType.ADULT.compareTo(paxType) == 0) {
				if (lccPassenger.getInfants() != null && lccPassenger.getInfants().size() > 0) {
					itineraryDTO.getBooking().setHasInfants(true);

					LCCClientReservationPax infant = BeanUtils.getFirstElement(lccPassenger.getInfants());
					if (infant.getLccClientPaxNameTranslations() != null
							&& languageCode.equals(infant.getLccClientPaxNameTranslations().getLanguageCode())) {
						String infTitleOl = infant.getLccClientPaxNameTranslations().getTitleOl() != null ? infant
								.getLccClientPaxNameTranslations().getTitleOl() : "";
						infantDisplayName = composeName(infTitleOl,
								StringUtil.getUnicode(infant.getLccClientPaxNameTranslations().getFirstNameOl()),
								StringUtil.getUnicode(infant.getLccClientPaxNameTranslations().getLastNameOl()));
					} else {
						String title = infant.getTitle() != null ? infant.getTitle() : "";
						infantDisplayName = composeName(title, infant.getFirstName(), infant.getLastName());
					}
					totalInfantChargeAmounts = infant.getTotalChargeAmounts();
					infantDob = infant.getDateOfBirth();
					/* add infant SSR to the parent ancillary list */
					List<LCCSelectedSegmentAncillaryDTO> lccInfAnci = infant.getSelectedAncillaries();
					for (LCCSelectedSegmentAncillaryDTO lccAnci : lccInfAnci) {

						List<ItinerarySSRTO> infSSRList = new ArrayList<ItinerarySSRTO>();
						for (LCCSpecialServiceRequestDTO lccSSR : lccAnci.getSpecialServiceRequestDTOs()) {
							ItinerarySSRTO paxSSR = new ItinerarySSRTO();
							paxSSR.setSsrCode(lccSSR.getSsrCode());
							paxSSR.setDescription(lccSSR.getDescription());
							infSSRList.add(paxSSR);
							anciAvailTO.setSsrAvailable(true);
						}
						for (ItinerarySegmentAncillaryTO paxAnci : passenger.getAncillary()) {
							if (paxAnci.getSegment().getSegmentCode().equals(lccAnci.getFlightSegmentTO().getSegmentCode())) {
								paxAnci.getSsrs().addAll(infSSRList);
							}
						}

					}
				}
			}

			passenger.setInfantDisplayName(infantDisplayName);
			if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode) && (infantDob != null)) {
				DateFormat formatter = new PersianDateFormat("dd MMMM yyyy", new com.ibm.icu.util.ULocale("fa", "IR", ""));
				passenger.setInfantDateOfBirthInPersian(formatter.format(infantDob));
				passenger.setIsPersianDatesSet(true);
				passenger.setInfantDateOfBirth(infantDob);
			} else {
				passenger.setInfantDateOfBirth(infantDob);
			}

			// Base Currency Financials
			if (itineraryDTO.isIncludePaxFinancials()) {
				ItineraryFinancialTotalsTO baseCurrencyFinancials = new ItineraryFinancialTotalsTO();
				baseCurrencyFinancials.setCurrency(AppSysParamsUtil.getBaseCurrency());
				baseCurrencyFinancials.setTotalBalance(AccelAeroCalculator.formatAsDecimal(lccPassenger
						.getTotalAvailableBalance()));

				if (lccPassenger.getTotalAvailableBalance() != null
						&& (lccPassenger.getTotalAvailableBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
					passenger.setAlertAutoCancellation(true);
				}

				if (itineraryDTO.isIncludePaxCreditExpiryDate()) {
					Date expiryDate = null;
					boolean defaultSelected = false;
					for (CreditInfoDTO clientCredits : lccPassenger.getLccClientPaymentHolder().getCredits()) {
						if (clientCredits.getEnumStatus().equals(CreditInfoDTO.status.AVAILABLE)
								&& clientCredits.getMcAmount().compareTo(BigDecimal.ZERO) > 0 && !defaultSelected) {
							expiryDate = clientCredits.getExpireDate();
							defaultSelected = true;
						}
						if (clientCredits.getEnumStatus().equals(CreditInfoDTO.status.AVAILABLE)
								&& clientCredits.getMcAmount().compareTo(BigDecimal.ZERO) > 0 && defaultSelected
								&& expiryDate.after(clientCredits.getExpireDate())) {
							expiryDate = clientCredits.getExpireDate();
						}
					}
					if (expiryDate != null) {
						if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
							DateFormat formatter = new PersianDateFormat("EEEE, dd MMMM yyyy", new com.ibm.icu.util.ULocale("fa",
									"IR", ""));
							baseCurrencyFinancials.setCreditExpiryDateInPersian(formatter.format(expiryDate));
							baseCurrencyFinancials.setPersianDatesSet(true);
						}
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						baseCurrencyFinancials.setCreditExpiryDate(sdf.format(expiryDate));
					} else {
						baseCurrencyFinancials.setCreditExpiryDate("-");
					}

				}
				// [0]fare [1]tax [2]surcharge [3]cancelcharge [4]modificationcharge [5]adjustmentcharge [6]discount
				BigDecimal[] totalChargeAmounts = lccPassenger.getTotalChargeAmounts();

				BigDecimal adultFare = totalChargeAmounts[0];
				BigDecimal infantFare = totalInfantChargeAmounts[0];

				BigDecimal adultDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal infantDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

				// discounted fare to shown in itinerary
				if (itineraryDTO.isShowDiscountAmounts()) {
					adultDiscount = totalChargeAmounts[6].negate();
					infantDiscount = totalInfantChargeAmounts[6].negate();
				} else {
					adultFare = AccelAeroCalculator.add(totalChargeAmounts[0], totalChargeAmounts[6]);
					infantFare = AccelAeroCalculator.add(totalInfantChargeAmounts[0], totalInfantChargeAmounts[6]);
				}

				if (itineraryFareMask != null && itineraryFareMask.equals("Y")) {
					baseCurrencyFinancials.setTotalFare(ITINERARY_FARE_MASK);
				} else {

					baseCurrencyFinancials.setTotalFare(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
					// adult amount
							adultFare,
							// infant amount
							infantFare)));
				}

				baseCurrencyFinancials.setTotalCharges(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.add(
						// adults amounts
						totalChargeAmounts[1], totalChargeAmounts[2], totalChargeAmounts[3], totalChargeAmounts[4],
						totalChargeAmounts[5],
						// infant amounts
						totalInfantChargeAmounts[1], totalInfantChargeAmounts[2], totalInfantChargeAmounts[3],
						totalInfantChargeAmounts[4], totalInfantChargeAmounts[5])));
				
				/*
				 * New itinerary charge breakdown details
				 */
				BigDecimal totalPaxTaxes = lccPassenger.getTotalTaxCharge();
				BigDecimal totalPaxFare = lccPassenger.getTotalFare();
				BigDecimal totalPaxChargeAmount = lccPassenger.getTotalChargeAmount();
				BigDecimal totalPaxDiscount = lccPassenger.getTotalDiscount();
				BigDecimal totalPaxPaidAmount = lccPassenger.getTotalPaidAmount();

				/*
				 * If infant is present take the charges to it into account. This is cleaner than relying on array index
				 */
				if (ReservationInternalConstants.PassengerType.ADULT.compareTo(paxType) == 0) {
					if (lccPassenger.getInfants() != null && lccPassenger.getInfants().size() > 0) {
						LCCClientReservationPax infant = BeanUtils.getFirstElement(lccPassenger.getInfants());
						totalPaxTaxes = AccelAeroCalculator.add(totalPaxTaxes, infant.getTotalTaxCharge());
						totalPaxFare = AccelAeroCalculator.add(totalPaxFare, infant.getTotalFare());
						totalPaxChargeAmount = AccelAeroCalculator.add(totalPaxChargeAmount, infant.getTotalChargeAmount());
						totalPaxDiscount = AccelAeroCalculator.add(totalPaxDiscount, infant.getTotalDiscount());
						if(lccClientReservation.isInfantPaymentSeparated()){
							totalPaxPaidAmount = AccelAeroCalculator.add(totalPaxPaidAmount, infant.getTotalPaidAmount());
						}
					}
				}

				baseCurrencyFinancials.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(totalPaxPaidAmount));
				baseCurrencyFinancials.setTotalTaxes(AccelAeroCalculator.formatAsDecimal(totalPaxTaxes));

				/*
				 * Total surcharges are actually all the charges - taxes. (Thats how AirArabia wants it).
				 * getTotalChargeAmount() returns sum of all including fare. So need to subtract tax and fare from it to
				 * get the surcharges.
				 */
				BigDecimal taxAndFare = AccelAeroCalculator.add(totalPaxTaxes, totalPaxFare);
				BigDecimal surcharge = AccelAeroCalculator.subtract(totalPaxChargeAmount, taxAndFare);
				baseCurrencyFinancials.setTotalSurcharges((AccelAeroCalculator.formatAsDecimal(surcharge)));
				baseCurrencyFinancials.setTotalDiscount(AccelAeroCalculator.formatAsDecimal(totalPaxDiscount));
				baseCurrencyFinancials.setTotalDue(AccelAeroCalculator.formatAsDecimal(totalPaxChargeAmount));

				if (itineraryDTO.isCreditDiscount()) {
					baseCurrencyFinancials.setTotalCreditDiscount(AccelAeroCalculator.formatAsDecimal(lccClientReservation
							.getLccPromotionInfoTO().getCreditDiscountAmount()));
				}

				passenger.setBaseCurrencyFinancials(baseCurrencyFinancials);

				totalAvailBalance = AccelAeroCalculator.add(totalAvailBalance, lccPassenger.getTotalAvailableBalance());
				totalFare = AccelAeroCalculator.add(totalFare,
				// adult amount
						adultFare,
						// infant amount
						infantFare);
				totalTaxes = AccelAeroCalculator.add(totalTaxes, totalChargeAmounts[1], totalInfantChargeAmounts[1]);
				totalSurcharges = AccelAeroCalculator.add(totalSurcharges, totalChargeAmounts[2], totalInfantChargeAmounts[2]);
				totalAmountDue = AccelAeroCalculator.add(totalAmountDue, totalPaxChargeAmount);
				totalCharges = AccelAeroCalculator.add(
						totalCharges,
						// adult amounts
						totalChargeAmounts[1], totalChargeAmounts[2], totalChargeAmounts[3], totalChargeAmounts[4],
						totalChargeAmounts[5],
						// infant amounts
						totalInfantChargeAmounts[1], totalInfantChargeAmounts[2], totalInfantChargeAmounts[3],
						totalInfantChargeAmounts[4], totalInfantChargeAmounts[5]

				);
				totalDiscount = AccelAeroCalculator.add(totalDiscount, adultDiscount, infantDiscount);
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, totalPaxPaidAmount);

			}

			// Payments
			if (itineraryDTO.isIncludePaymentDetails() || AppSysParamsUtil.isPrintEmailIndItinerary()) {

				Collection<LCCClientPaymentInfo> lccPaymentList = lccPassenger.getLccClientPaymentHolder().getPayments();
				Collection<LCCClientPaymentInfo> infantPaymentList = null;
				
				if(lccClientReservation.isInfantPaymentSeparated() && lccPassenger.getInfants() != null && lccPassenger.getInfants().size() > 0){
					for(LCCClientReservationPax infant : lccPassenger.getInfants()){
						infantPaymentList = infant.getLccClientPaymentHolder().getPayments();
					}
				}
				
				if (lccPaymentList != null && lccPaymentList.size() == 0) {
					itineraryDTO.setIncludePaymentDetails(false);
				} else {
					setPassengerPayments(passenger,lccPassenger,lccPaymentList,itineraryDTO,null);
				}
				
				if(infantPaymentList != null){
					setPassengerPayments(passenger,lccPassenger,infantPaymentList,itineraryDTO,infantDisplayName);
				}
				
			} else {
				if (AppSysParamsUtil.showCreditCardInfoInItineraryAlways()) {
					Collection<LCCClientPaymentInfo> lccPaymentList = lccPassenger.getLccClientPaymentHolder().getPayments();
					Collection<LCCClientPaymentInfo> infantPaymentList = null;
					
					if(lccClientReservation.isInfantPaymentSeparated() && lccPassenger.getInfants() != null && lccPassenger.getInfants().size() > 0){
						for(LCCClientReservationPax infant : lccPassenger.getInfants()){
							infantPaymentList = infant.getLccClientPaymentHolder().getPayments();
						}
					}
					
					if (lccPaymentList != null && lccPaymentList.size() == 0) {
						itineraryDTO.setIncludePaymentDetails(false);
					} else {
						setPassengerCCPayments(passenger,lccPassenger,lccPaymentList,itineraryDTO,null);
					}
					
					if(infantPaymentList != null){
						setPassengerCCPayments(passenger,lccPassenger,infantPaymentList,itineraryDTO,infantDisplayName);
					}
				}
			}

			// To Calculate passenger Individual total
			if (selectedPaxForItinerary) {
				ItineraryFinancialTotalsTO itiFinancialTotalsTO = new ItineraryFinancialTotalsTO();
				itiFinancialTotalsTO.setCurrency(AppSysParamsUtil.getBaseCurrency());
				itiFinancialTotalsTO.setTotalBalance(AccelAeroCalculator.scaleValueDefault(totalAvailBalance).toString());
				itiFinancialTotalsTO.setTotalCharges(AccelAeroCalculator.scaleValueDefault(totalCharges).toString());
				itiFinancialTotalsTO.setTotalFare(AccelAeroCalculator.scaleValueDefault(totalFare).toString());
				itiFinancialTotalsTO.setTotalSurcharges(AccelAeroCalculator.scaleValueDefault(totalSurcharges).toString());
				itiFinancialTotalsTO.setTotalTaxes(AccelAeroCalculator.scaleValueDefault(totalTaxes).toString());
				itiFinancialTotalsTO.setTotalDue(AccelAeroCalculator.scaleValueDefault(totalAmountDue).toString());

				if (itineraryFareMask != null && itineraryFareMask.equals("Y")) {
					itiFinancialTotalsTO.setTotalFare(ITINERARY_FARE_MASK);
				} else {
					itiFinancialTotalsTO.setTotalFare(AccelAeroCalculator.scaleValueDefault(totalFare).toString());
				}

				itiFinancialTotalsTO.setTotalDiscount(AccelAeroCalculator.scaleValueDefault(totalDiscount).toString());
				itiFinancialTotalsTO.setTotalPaidAmount(AccelAeroCalculator.scaleValueDefault(totalPaidAmount).toString());
				if (itineraryDTO.isCreditDiscount()) {
					itiFinancialTotalsTO.setTotalCreditDiscount(AccelAeroCalculator.formatAsDecimal(lccClientReservation
							.getLccPromotionInfoTO().getCreditDiscountAmount()));
				}
				itineraryDTO.getBooking().setBaseCurrencyFinancials(itiFinancialTotalsTO);

				ItineraryFinancialTotalsTO paidCurrency = new ItineraryFinancialTotalsTO();
				String paidCurrencyPayments = getPaidCurrencyPayments(lccClientReservation);
				if (paidCurrencyPayments != null && !"".equals(paidCurrencyPayments)) {
					paidCurrency.setTotalPaidAmount(paidCurrencyPayments);
				} else {
					if (lccClientReservation.getLastCurrencyCode() != null
							&& !"".equals(lccClientReservation.getLastCurrencyCode())) {
						paidCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(BigDecimal.ZERO) + " "
								+ lccClientReservation.getLastCurrencyCode());
					} else {
						paidCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(BigDecimal.ZERO) + " "
								+ AppSysParamsUtil.getBaseCurrency());
					}
				}
				paidCurrency.setCurrency("");
				paidCurrency.setTotalCharges("");
				paidCurrency.setTotalFare("");
				paidCurrency.setTotalSurcharges("");
				paidCurrency.setTotalTaxes("");
				paidCurrency.setTotalDue("");
				paidCurrency.setTotalBalance(getPaidCurrencyAvaialbleBalance(lccClientReservation));
				itineraryDTO.getBooking().setPaidCurrencyFinancials(paidCurrency);
			}
		}

		return passengerList;
	}

	private static void setPassengerPayments(ItineraryPassengerTO passenger,LCCClientReservationPax reservaionPax,
										Collection<LCCClientPaymentInfo> lccPaymentList,
											ItineraryDTO itineraryDTO, String paxDisplayName) throws ModuleException {

		// Group by Payments by Currency
		Map<String, List<LCCClientPaymentInfo>> lccPaymentsByCurrency = new HashMap<String, List<LCCClientPaymentInfo>>();
		for (LCCClientPaymentInfo lccPayment : lccPaymentList) {
			// Ignore payments of zero amount
			if (lccPayment.getTotalAmount().doubleValue() > 0.00 || lccPayment.getTotalAmount().doubleValue() < 0.00) {
				String currencyCode = lccPayment.getTotalAmountCurrencyCode();
				if (lccPaymentsByCurrency.get(currencyCode) == null) {
					List<LCCClientPaymentInfo> arrayList = new ArrayList<LCCClientPaymentInfo>();
					arrayList.add(lccPayment);
					lccPaymentsByCurrency.put(currencyCode, arrayList);
				} else {
					lccPaymentsByCurrency.get(currencyCode).add(lccPayment);
				}
			}
		}

		// Sum up Payments by Currency
		if(passenger.getPayments() == null){
			passenger.setPayments(new HashMap<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>>());
		}
		
		for (Entry<String, List<LCCClientPaymentInfo>> entry : lccPaymentsByCurrency.entrySet()) {
			String payCurrencyCode = null;
			BigDecimal totalBaseCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			List<ItineraryPaymentTO> payments = new ArrayList<ItineraryPaymentTO>();
			for (LCCClientPaymentInfo lccPayment : entry.getValue()) {

				ItineraryPaymentTO itineraryPaymentTO = new ItineraryPaymentTO();
				itineraryPaymentTO.setAuthorizationId("");

				String actualPaymentMethodAppend = "";
				if (lccPayment.getActualPaymentMethod() != null) {
					actualPaymentMethodAppend += "/"
							+ AirproxyModuleUtils.getReservationBD()
									.getActualPaymentMethodById(lccPayment.getActualPaymentMethod()).toString();
				}

				if (lccPayment instanceof LCCClientCashPaymentInfo) {
					itineraryPaymentTO.setPaymentType("CASH" + actualPaymentMethodAppend);
				} else if (lccPayment instanceof LCCClientOnAccountPaymentInfo) {
					if (SalesChannelsUtil.isAppEngineWebOrMobile(itineraryDTO.getAppIndicator())) {
						continue;
					}

					LCCClientOnAccountPaymentInfo lccOnAccountPaymentInfo = (LCCClientOnAccountPaymentInfo) lccPayment;
					if (Agent.PAYMENT_MODE_BSP.equals(lccOnAccountPaymentInfo.getPaymentMethod())) {
						itineraryPaymentTO.setPaymentType("BSP" + actualPaymentMethodAppend);
					} else {
						itineraryPaymentTO.setPaymentType("ONACCOUNT" + actualPaymentMethodAppend);
					}
				} else if (lccPayment instanceof CommonCreditCardPaymentInfo) {
					if (SalesChannelsUtil.isAppEngineWebOrMobile(itineraryDTO.getAppIndicator())
							&& !AppSysParamsUtil.showCCDetailsInItinerary()) {
						continue;
					}
					CommonCreditCardPaymentInfo lccCcPmt = (CommonCreditCardPaymentInfo) lccPayment;
					itineraryPaymentTO.setPaymentType(ReservationTnxNominalCode.getDescription(PaymentType
							.getReservationTnxNominalCodeForPayment(lccCcPmt.getType()).getCode())
							+ actualPaymentMethodAppend);
					itineraryPaymentTO.setLast4DigitsOfCCNo(BeanUtils.nullHandler(lccCcPmt.getNoLastDigits()));

					if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
						itineraryPaymentTO.setPaymentType("CC/***");
						if (itineraryPaymentTO.getLast4DigitsOfCCNo() == null
								|| itineraryPaymentTO.getLast4DigitsOfCCNo().compareTo("") == 0) {
							itineraryPaymentTO.setPaymentType("CC");
						}
					}

					itineraryPaymentTO.setAuthorizationId(BeanUtils.nullHandler(lccCcPmt.getAuthorizationId()));
				} else if (lccPayment instanceof LCCClientPaxCreditPaymentInfo) {
					LCCClientPaxCreditPaymentInfo paxCreditPmt = (LCCClientPaxCreditPaymentInfo) lccPayment;
					itineraryPaymentTO.setPaymentType(paxCreditPmt.getDescription());
				} else if (lccPayment instanceof LCCClientLMSPaymentInfo) {
					LCCClientLMSPaymentInfo lccClientLMSPaymentInfo = (LCCClientLMSPaymentInfo) lccPayment;
					itineraryPaymentTO.setPaymentType("LOYALTY_PAYMENT / "
							+ lccClientLMSPaymentInfo.getLoyaltyMemberAccountId());
				} else if (lccPayment instanceof LCCClientVoucherPaymentInfo){
					LCCClientVoucherPaymentInfo lccClientVoucherPaymentInfo = (LCCClientVoucherPaymentInfo) lccPayment;
					itineraryPaymentTO.setPaymentType("VOUCHER PAYMENT / "
							+ lccClientVoucherPaymentInfo.getVoucherDTO().getVoucherId());
				}

				if (lccPayment.getPayCurrencyDTO() != null
						&& lccPayment.getPayCurrencyDTO().getPayCurrencyCode() != null) {
					itineraryPaymentTO.setPaidAmount(AccelAeroCalculator.formatAsDecimal(lccPayment
							.getPayCurrencyAmount()));
					itineraryPaymentTO.setPaidCurrency(lccPayment.getPayCurrencyDTO().getPayCurrencyCode());
					totalPayCurrency = AccelAeroCalculator.add(totalPayCurrency, lccPayment.getTotalAmount());
					payCurrencyCode = lccPayment.getTotalAmountCurrencyCode();
				} else {
					itineraryPaymentTO
							.setPaidAmount(AccelAeroCalculator.formatAsDecimal(lccPayment.getTotalAmount()));
					itineraryPaymentTO.setPaidCurrency(lccPayment.getTotalAmountCurrencyCode());
					totalBaseCurrency = AccelAeroCalculator.add(totalBaseCurrency, lccPayment.getTotalAmount());
				}

				if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
					DateFormat formatter = new PersianDateFormat("EEEE, dd MMMM yyyy", new com.ibm.icu.util.ULocale(
							"fa", "IR", ""));
					itineraryPaymentTO.setPaymentDateInPersian(formatter.format(lccPayment.getTxnDateTime()));
					itineraryPaymentTO.setPaymentDate(lccPayment.getTxnDateTime());
					itineraryPaymentTO.setIsPersianDatesSet(true);
				} else {
					itineraryPaymentTO.setPaymentDate(lccPayment.getTxnDateTime());
				}
				itineraryPaymentTO.setPaxDisplayName(paxDisplayName);
				payments.add(itineraryPaymentTO);
			}

			ItineraryFinancialTotalsTO paxTotalsByCurrency = new ItineraryFinancialTotalsTO();
			if (payCurrencyCode != null) {
				paxTotalsByCurrency.setCurrency(payCurrencyCode);
				paxTotalsByCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(totalPayCurrency));
			} else {
				paxTotalsByCurrency.setCurrency(entry.getKey());
				paxTotalsByCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(totalBaseCurrency));
			}
			if (payments.size() > 0) {
				if(passenger.getPayments().get(paxTotalsByCurrency) != null && !passenger.getPayments().get(paxTotalsByCurrency).isEmpty()){
					passenger.getPayments().get(paxTotalsByCurrency).addAll(payments);
				}else{
					passenger.getPayments().put(paxTotalsByCurrency, payments);
				}
			}
		}
	
	}
	
	private static void setPassengerCCPayments(ItineraryPassengerTO passenger,LCCClientReservationPax reservaionPax,
			Collection<LCCClientPaymentInfo> lccPaymentList,
				ItineraryDTO itineraryDTO, String paxDisplayName) throws ModuleException {
		// Group by Payments by Currency
		Map<String, List<LCCClientPaymentInfo>> lccPaymentsByCurrency = new HashMap<String, List<LCCClientPaymentInfo>>();
		for (LCCClientPaymentInfo lccPayment : lccPaymentList) {
			// Ignore payments of zero amount
			if (lccPayment.getTotalAmount().doubleValue() > 0.00
					|| lccPayment.getTotalAmount().doubleValue() < 0.00) {
				String currencyCode = lccPayment.getTotalAmountCurrencyCode();
				if (lccPaymentsByCurrency.get(currencyCode) == null) {
					List<LCCClientPaymentInfo> arrayList = new ArrayList<LCCClientPaymentInfo>();
					arrayList.add(lccPayment);
					lccPaymentsByCurrency.put(currencyCode, arrayList);
				} else {
					lccPaymentsByCurrency.get(currencyCode).add(lccPayment);
				}
			}
		}

		// Sum up Payments by Currency
		if (passenger.getPayments() == null) {
			passenger.setPayments(new HashMap<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>>());
		}
		for (Entry<String, List<LCCClientPaymentInfo>> entry : lccPaymentsByCurrency.entrySet()) {
			String payCurrencyCode = null;
			BigDecimal totalBaseCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			List<ItineraryPaymentTO> payments = new ArrayList<ItineraryPaymentTO>();
			for (LCCClientPaymentInfo lccPayment : entry.getValue()) {

				if (lccPayment instanceof CommonCreditCardPaymentInfo) {
					ItineraryPaymentTO itineraryPaymentTO = new ItineraryPaymentTO();
					itineraryPaymentTO.setAuthorizationId("");

					String actualPaymentMethodAppend = "";
					if (lccPayment.getActualPaymentMethod() != null) {
						actualPaymentMethodAppend += "/"
								+ AirproxyModuleUtils.getReservationBD()
										.getActualPaymentMethodById(lccPayment.getActualPaymentMethod())
										.toString();
					}

					if (SalesChannelsUtil.isAppEngineWebOrMobile(itineraryDTO.getAppIndicator())
							&& !AppSysParamsUtil.showCCDetailsInItinerary()) {
						continue;
					}
					CommonCreditCardPaymentInfo lccCcPmt = (CommonCreditCardPaymentInfo) lccPayment;
					itineraryPaymentTO.setPaymentType(ReservationTnxNominalCode.getDescription(PaymentType
							.getReservationTnxNominalCodeForPayment(lccCcPmt.getType()).getCode())
							+ actualPaymentMethodAppend);
					itineraryPaymentTO.setLast4DigitsOfCCNo(BeanUtils.nullHandler(lccCcPmt.getNoLastDigits()));

					if (AppSysParamsUtil.isPrintEmailIndItinerary()) {
						itineraryPaymentTO.setPaymentType("CC/***");
						if (itineraryPaymentTO.getLast4DigitsOfCCNo() == null
								|| itineraryPaymentTO.getLast4DigitsOfCCNo().compareTo("") == 0) {
							itineraryPaymentTO.setPaymentType("CC");
						}
					}

					itineraryPaymentTO.setAuthorizationId(BeanUtils.nullHandler(lccCcPmt.getAuthorizationId()));

					if (lccPayment.getPayCurrencyDTO() != null
							&& lccPayment.getPayCurrencyDTO().getPayCurrencyCode() != null) {
						itineraryPaymentTO.setPaidAmount(AccelAeroCalculator.formatAsDecimal(lccPayment
								.getPayCurrencyAmount()));
						itineraryPaymentTO.setPaidCurrency(lccPayment.getPayCurrencyDTO().getPayCurrencyCode());
						totalPayCurrency = AccelAeroCalculator.add(totalPayCurrency,
								lccPayment.getPayCurrencyAmount());
						payCurrencyCode = lccPayment.getPayCurrencyDTO().getPayCurrencyCode();
					} else {
						itineraryPaymentTO.setPaidAmount(AccelAeroCalculator.formatAsDecimal(lccPayment
								.getTotalAmount()));
						itineraryPaymentTO.setPaidCurrency(lccPayment.getTotalAmountCurrencyCode());
						totalBaseCurrency = AccelAeroCalculator.add(totalBaseCurrency,
								lccPayment.getTotalAmount());
					}

					if (AppSysParamsUtil.displayTimeUsingPersianFormat(itineraryDTO.getItineraryLanguage())) {
						DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
								new com.ibm.icu.util.ULocale("fa", "IR", ""));
						itineraryPaymentTO.setPaymentDateInPersian(formatter.format(lccPayment.getTxnDateTime()));
						itineraryPaymentTO.setIsPersianDatesSet(true);
					} else {
						itineraryPaymentTO.setPaymentDate(lccPayment.getTxnDateTime());
					}
					itineraryPaymentTO.setPaxDisplayName(paxDisplayName);
					payments.add(itineraryPaymentTO);
				}

			}

			ItineraryFinancialTotalsTO paxTotalsByCurrency = new ItineraryFinancialTotalsTO();
			if (payCurrencyCode != null) {
				paxTotalsByCurrency.setCurrency(payCurrencyCode);
				paxTotalsByCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(totalPayCurrency));
			} else {
				paxTotalsByCurrency.setCurrency(entry.getKey());
				paxTotalsByCurrency.setTotalPaidAmount(AccelAeroCalculator.formatAsDecimal(totalBaseCurrency));
			}
			if (payments.size() > 0) {
				if(passenger.getPayments().get(paxTotalsByCurrency) != null && !passenger.getPayments().get(paxTotalsByCurrency).isEmpty()){
					passenger.getPayments().get(paxTotalsByCurrency).addAll(payments);
				}else{
					passenger.getPayments().put(paxTotalsByCurrency, payments);
				}
			}
		}
	}
	
	private static void addChargesToMap(Map<String, ItineraryOndChargeTO> ondChargeTOMap, ItineraryChargeTO charge,
			BigDecimal amount) {
		if (!ondChargeTOMap.containsKey(charge.getSegmentCode())) {
			ItineraryOndChargeTO ondChargeTO = new ItineraryOndChargeTO();
			ondChargeTO.setOnd(charge.getSegmentCode());
			ondChargeTOMap.put(charge.getSegmentCode(), ondChargeTO);
		}
		ondChargeTOMap.get(charge.getSegmentCode()).addCharge(charge, amount);
	}

	private static Collection<ItineraryOndChargeTO> createCharges(LCCClientReservation reservation, String itineraryFareMask,
			String languageCode, Map<String, String> airportCodeMap) {
		Map<String, ItineraryOndChargeTO> ondChargeTOMap = new HashMap<String, ItineraryOndChargeTO>();

		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			String paxName = composeName(pax.getTitle(), pax.getFirstName(), pax.getLastName());

			// Fare amounts are shown in the charges details of the itinerary if this application parameter is enabled
			// if (AppSysParamsUtil.isShowFareAmtsInItineraryChargesDisplay()) {
			// TODO: above privellege to be removed - RES_113
			for (FareTO fare : pax.getFares()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();

				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode) && fare.getApplicableDate() != null) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(fare.getApplicableDate()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(fare.getApplicableDate());
				}

				chargeTO.setChargeType("Fare");
				chargeTO.setSegmentCode(fare.getSegmentCode());
				chargeTO.setDescription(fare.getDescription());
				chargeTO.setPassengerName(paxName);

				if (itineraryFareMask != null && itineraryFareMask.equals("Y")) {
					chargeTO.setAmount(ITINERARY_FARE_MASK);
					addChargesToMap(ondChargeTOMap, chargeTO, new BigDecimal(0));
				} else {
					chargeTO.setAmount(AccelAeroCalculator.formatAsDecimal(fare.getAmount()));
					addChargesToMap(ondChargeTOMap, chargeTO, fare.getAmount());
				}

			}
			// }

			Collection<Integer> colChargeRateIds = new HashSet<Integer>();
			for (SurchargeTO surcharge : pax.getSurcharges()) {
				colChargeRateIds.add(new Integer(surcharge.getChargeRateId()));
			}
			for (TaxTO tax : pax.getTaxes()) {
				colChargeRateIds.add(new Integer(tax.getChargeRateId()));
			}
			for (FeeTO fee : pax.getFees()) {
				if (fee.getChargeRateId() != null) {
					colChargeRateIds.add(new Integer(fee.getChargeRateId()));
				}
			}
			FareBD fareBD = ReservationModuleUtils.getFareBD();
			FaresAndChargesTO faresAndChargesTO = null;
			Map chargesMap = new TreeMap();

			try {
				faresAndChargesTO = fareBD.getRefundableStatuses(null, colChargeRateIds, null);
				chargesMap = faresAndChargesTO.getChargeTOs();
			} catch (ModuleException e) {
				log.error(e);
			}

			for (SurchargeTO surcharge : pax.getSurcharges()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(surcharge.getAmount()));
				// chargeTO.setAmount(AccelAeroCalculator.formatAsDecimal(surcharge.getAmount()));
				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode) && surcharge.getApplicableTime() != null) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(surcharge.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(surcharge.getApplicableTime());
				}

				chargeTO.setChargeType("Surcharge");
				chargeTO.setSegmentCode(surcharge.getSegmentCode());
				// TODO add and app param to enable/disable display chargeCode and description
				chargeTO.setDescription(surcharge.getChargeCode() + "/" + surcharge.getSurchargeName());
				chargeTO.setPassengerName(paxName);
				if (surcharge.getChargeRateId() != null && chargesMap.size() > 0) {
					chargeTO.setChargeCode((String) chargesMap.get(surcharge.getChargeRateId()));
				}
				addChargesToMap(ondChargeTOMap, chargeTO, surcharge.getAmount());
			}
			for (TaxTO tax : pax.getTaxes()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(tax.getAmount()));

				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode)) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(tax.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(tax.getApplicableTime());
				}
				chargeTO.setChargeType("Tax");
				chargeTO.setSegmentCode(tax.getSegmentCode());
				// TODO add and app param to enable/disable display chargeCode and description
				chargeTO.setDescription(tax.getChargeCode() + "/" + tax.getTaxName());
				chargeTO.setPassengerName(paxName);
				if (tax.getChargeRateId() != null && chargesMap.size() > 0) {
					chargeTO.setChargeCode((String) chargesMap.get(tax.getChargeRateId()));
				}
				addChargesToMap(ondChargeTOMap, chargeTO, tax.getAmount());
			}
			for (FeeTO fee : pax.getFees()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(fee.getAmount()));

				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode)) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(fee.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(fee.getApplicableTime());
				}
				chargeTO.setChargeType("Fee");
				chargeTO.setSegmentCode(fee.getSegmentCode());
				chargeTO.setDescription(fee.getFeeName());
				chargeTO.setPassengerName(paxName);
				if (fee.getChargeRateId() != null && chargesMap.size() > 0) {
					chargeTO.setChargeCode((String) chargesMap.get(fee.getChargeRateId()));
				}
				addChargesToMap(ondChargeTOMap, chargeTO, fee.getAmount());
			}
		}
		LinkedList<ItineraryOndChargeTO> ondChargeList = new LinkedList<ItineraryOndChargeTO>();
		ItineraryOndChargeTO ondCharge = null;
		String segCode = "";
		for (LCCClientReservationSegment seg : SortUtil.sort(reservation.getSegments())) {
			if (YES.equals(seg.getReturnFlag())) {
				segCode = getInverseSegmentCode(seg.getFareTO().getSegmentCode());
				if (!ondChargeList.contains(ondChargeTOMap.get(segCode))) {
					ondChargeList.add(ondChargeTOMap.get(segCode));
				}
			}
			
			if (!ondChargeList.contains(ondChargeTOMap.get(seg.getFareTO().getSegmentCode()))) {
				if (("en").equals(languageCode)) {
					ItineraryOndChargeTO chargeTO = ondChargeTOMap.get(seg.getFareTO().getSegmentCode());
					segCode = "";
					if (chargeTO == null
							&& (seg.getFareTO().getFareType().equals(Integer.toString(FareTypes.HALF_RETURN_FARE)) || seg
									.getFareTO().getFareType().equals(Integer.toString(FareTypes.RETURN_FARE)))) {
						segCode = getInverseSegmentCode(seg.getFareTO().getSegmentCode());
						if (!ondChargeList.contains(ondChargeTOMap.get(segCode))) {
							chargeTO = ondChargeTOMap.get(segCode);
							ondChargeList.add(chargeTO);
						}
					} else {
						ondChargeList.add(chargeTO);
					}
				} else {
					segCode = getSegmentCodeInOtherLanguage(airportCodeMap, seg.getFareTO().getSegmentCode());
					ondCharge = ondChargeTOMap.get(seg.getFareTO().getSegmentCode());
					for (ItineraryChargeTO charge : ondCharge.getCharges()) {
						charge.setSegmentCode(segCode);
					}
					ondChargeList.add(ondCharge);
				}
			}
		}
		return ondChargeList;
	}

	private static String getInverseSegmentCode(String ond) {
		String inverseOND = "";
		String airports[] = ond.split("/");
		for (int i = airports.length - 1; i >= 0; i--) {
			inverseOND += i == airports.length - 1 ? airports[i] : "/" + airports[i];
		}
		return inverseOND;
	}

	private static Collection<ItineraryOndChargeTO> createTaxesFeesCharges(LCCClientReservation reservation, String languageCode) {
		Map<String, ItineraryOndChargeTO> ondChargeTOMap = new HashMap<String, ItineraryOndChargeTO>();
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			String paxName = composeName(pax.getTitle(), pax.getFirstName(), pax.getLastName());

			Collection<Integer> colChargeRateIds = new HashSet<Integer>();
			for (SurchargeTO surcharge : pax.getSurcharges()) {
				colChargeRateIds.add(new Integer(surcharge.getChargeRateId()));
			}
			for (TaxTO tax : pax.getTaxes()) {
				colChargeRateIds.add(new Integer(tax.getChargeRateId()));
			}
			for (FeeTO fee : pax.getFees()) {
				if (fee.getChargeRateId() != null) {
					colChargeRateIds.add(new Integer(fee.getChargeRateId()));
				}
			}
			FareBD fareBD = ReservationModuleUtils.getFareBD();
			FaresAndChargesTO faresAndChargesTO = null;
			Map chargesMap = new TreeMap();

			try {
				faresAndChargesTO = fareBD.getRefundableStatuses(null, colChargeRateIds, null);
				chargesMap = faresAndChargesTO.getChargeTOs();
			} catch (ModuleException e) {
				log.error(e);
			}

			for (SurchargeTO surcharge : pax.getSurcharges()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(surcharge.getAmount()));
				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode)) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(surcharge.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(surcharge.getApplicableTime());
				}
				chargeTO.setChargeType("Surcharge");
				chargeTO.setSegmentCode(surcharge.getSegmentCode());
				chargeTO.setDescription(surcharge.getSurchargeName());
				chargeTO.setPassengerName(paxName);
				if (surcharge.getChargeRateId() != null && chargesMap.size() > 0) {
					// chargeTO.setChargeCode((String)chargesMap.get(surcharge.getChargeRateId()));
					// ChargeTO chDTO = (ChargeTO)chargesMap.get(surcharge.getChargeRateId());
					chargeTO.setChargeCode(surcharge.getChargeCode());
				}
				addChargesToMap(ondChargeTOMap, chargeTO, surcharge.getAmount());
			}
			for (TaxTO tax : pax.getTaxes()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(tax.getAmount()));
				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode)) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(tax.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(tax.getApplicableTime());
				}
				chargeTO.setChargeType("Tax");
				chargeTO.setSegmentCode(tax.getSegmentCode());
				chargeTO.setDescription(tax.getTaxCode());
				chargeTO.setPassengerName(paxName);
				if (tax.getChargeRateId() != null && chargesMap.size() > 0) {
					// chargeTO.setChargeCode(((ChargeTO)chargesMap.get(tax.getChargeRateId())).getChargeCode());
					ChargeTO chDTO = (ChargeTO) chargesMap.get(tax.getChargeRateId());
					chargeTO.setChargeCode(tax.getChargeCode());
				}
				addChargesToMap(ondChargeTOMap, chargeTO, tax.getAmount());
			}
			for (FeeTO fee : pax.getFees()) {
				ItineraryChargeTO chargeTO = new ItineraryChargeTO();
				chargeTO.setAmount(formatAsDecimal(fee.getAmount()));
				if (AppSysParamsUtil.displayTimeUsingPersianFormat(languageCode)) {
					DateFormat formatter = new PersianDateFormat("EEEE, d MMMM yyyy",
							new com.ibm.icu.util.ULocale("fa", "IR", ""));
					chargeTO.setChargeDateInPersian(formatter.format(fee.getApplicableTime()));
					chargeTO.setIsPersianDatesSet(true);
				} else {
					chargeTO.setChargeDate(fee.getApplicableTime());
				}
				chargeTO.setChargeType("Fee");
				chargeTO.setSegmentCode(fee.getSegmentCode());
				chargeTO.setDescription(fee.getFeeName());
				chargeTO.setPassengerName(paxName);
				if (fee.getChargeRateId() != null && chargesMap.size() > 0) {
					// chargeTO.setChargeCode((String)chargesMap.get(fee.getChargeRateId()));
					// chargeTO.setChargeCode(((ChargeTO)chargesMap.get(fee.getChargeRateId())).getChargeCode());
					// /ChargeTO chDTO = (ChargeTO)chargesMap.get(fee.getChargeRateId());
					chargeTO.setChargeCode(fee.getChargeCode());
				}
				addChargesToMap(ondChargeTOMap, chargeTO, fee.getAmount());
			}
		}
		List<ItineraryOndChargeTO> ondChargeList = new ArrayList<ItineraryOndChargeTO>();
		for (ItineraryOndChargeTO ondCT : ondChargeTOMap.values()) {
			ondChargeList.add(ondCT);
		}

		return ondChargeList;
	}

	private static String composeName(String title, String firstName, String lastName) {
		title = BeanUtils.nullHandler(title);
		firstName = BeanUtils.nullHandler(firstName);
		lastName = BeanUtils.nullHandler(lastName);

		StringBuilder name = new StringBuilder();

		if (title.length() > 0) {
			name.append(title).append(" ");
		}

		if (firstName.length() > 0) {
			name.append(firstName).append(" ");
		}

		if (lastName.length() > 0) {
			name.append(lastName).append(" ");
		}

		return name.toString().trim();
	}

	private static List<String> getAirportMsgs(LCCClientReservation clientReservation, String appIndicator, String languageCode)
			throws ModuleException {
		Set<LCCClientReservationSegment> reservationSegements = clientReservation.getSegments();
		String salesChannel = "";

		if (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator)) {
			salesChannel = ReservationInternalConstants.AirportMessageSalesChannel.IBE;
		} else if (appIndicator.equals(ApplicationEngine.XBE.toString())) {
			salesChannel = ReservationInternalConstants.AirportMessageSalesChannel.XBE;
		} else {
			salesChannel = ReservationInternalConstants.AirportMessageSalesChannel.BOTH;
		}

		List<String> airportMsgs = AirportMessageDisplayUtil.getAirportMessagesForFlightSegmentWise(reservationSegements,
				salesChannel, ReservationInternalConstants.AirportMessageStages.ITINERARY, languageCode);

		return airportMsgs;
	}
	

	private static Date getCheckInDate(Date departureDate, int checkInGap) throws ModuleException {
		if (departureDate != null) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(departureDate);

			// Ideally we should have the check in gap for each segment along with the segment details.
			// This should not happen! But this is for extra safety if by any change the segment does not contain the
			// check in gap.
			if (checkInGap <= 0) {
				checkInGap = AppSysParamsUtil.getTotalMinutes(AppSysParamsUtil.getCheckInTimeDifference());
			}

			calendar.add(Calendar.MINUTE, -checkInGap);
			return calendar.getTime();
		} else {
			return departureDate;
		}
	}

	private static String formatAsDecimal(BigDecimal amount) {
		String strAmount = AccelAeroCalculator.formatAsDecimal(amount);
		if (!AppSysParamsUtil.isPrintEmailIndItinerary()) {
			return strAmount;
		}

		if (strAmount != null && strAmount.compareTo("") != 0 && strAmount.indexOf(".") != -1) {
			if (new Integer(strAmount.substring(strAmount.indexOf(".") + 1, strAmount.length())).equals(0)) {
				return strAmount.substring(0, strAmount.indexOf("."));
			}
		}

		return strAmount;
	}

	private static String getFirstDepartingCarrier(LCCClientReservation lccClientReservation) {
		String firstDepartingCarrier = AppSysParamsUtil.getDefaultCarrierCode();

		if (lccClientReservation.getSegments() != null) {
			ArrayList<LCCClientReservationSegment> segmentList = new ArrayList<LCCClientReservationSegment>();
			for (LCCClientReservationSegment segment : lccClientReservation.getSegments()) {
				if (!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
						&& segment.getSubStationShortName() == null) {
					segmentList.add(segment);
				}
			}

			Collections.sort(segmentList);
			if (segmentList.size() > 0) {
				firstDepartingCarrier = segmentList.get(0).getCarrierCode();
			}
		}

		return firstDepartingCarrier;
	}

	private static String deriveTransferType(String applyOn) {
		String code = "";
		if (applyOn != null) {
			if (applyOn.equals("HOME")) {
				code = "Pick-Up";
			} else if (applyOn.equals("AIRPORT")) {
				code = "Drop-Off";
			}
		}
		return code;
	}
	
	private static String getSegmentCodeInOtherLanguage(Map<String, String> airportCodeMap, String segmentCode) {
		String originAirport = "";
		String destinationAirport = "";
		String viaPoints = "";
		if (airportCodeMap != null) {
			String[] airportCodes = segmentCode.split("/");
			if (airportCodeMap.get(airportCodes[0]) != null) {
				originAirport = StringUtil.getUnicode(airportCodeMap.get(airportCodes[0]));
			}
			if (airportCodeMap.get(airportCodes[(airportCodes.length) - 1]) != null) {
				destinationAirport = StringUtil.getUnicode(airportCodeMap.get(airportCodes[(airportCodes.length) - 1]));
			}
			if (airportCodes.length > 2) {
				for (int i = 1; airportCodes.length > i + 1; i++)
					if (airportCodeMap.get(airportCodes[i]) != null) {
						viaPoints = viaPoints + "/" + StringUtil.getUnicode(airportCodeMap.get(airportCodes[i]));
					}
			}
		}
		if (("").equals(originAirport) || ("").equals(destinationAirport)) {
			return segmentCode;
		} else {
			return (originAirport + viaPoints + "/" + destinationAirport);
		}
	}
	
	public static String getBookingChannel(String bookingChannel) {
		String originBokingChannel = "";
		StringTokenizer st = new StringTokenizer(bookingChannel, ",");
		while (st.hasMoreTokens()) {
			originBokingChannel = st.nextToken();
			if (originBokingChannel.indexOf("|") > -1) {
				originBokingChannel = originBokingChannel.substring(originBokingChannel.indexOf("|") + 1);
				break;
			}

		}
		return originBokingChannel;

	}
}
