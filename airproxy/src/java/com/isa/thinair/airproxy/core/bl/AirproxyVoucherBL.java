package com.isa.thinair.airproxy.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.service.VoucherBD;

/**
 * @author chethiya
 *
 */
public class AirproxyVoucherBL {

	private static Log log = LogFactory.getLog(AirproxyVoucherBL.class);

	public static VoucherRedeemResponse redeemVouchers(VoucherRedeemRequest voucherRedeemRequest) throws Exception {
		return redeemOwnAirlineVouchers(voucherRedeemRequest);
	}

	public static VoucherRedeemResponse redeemOwnAirlineVouchers(VoucherRedeemRequest voucherRedeemRequest) throws Exception {
		log.info("Executing redeemOwnAirlineVouchers()");
		VoucherBD voucherBD = AirproxyModuleUtils.getVoucherBD();
		VoucherRedeemResponse voucherRedeemResponse = null;
		try {
			return voucherBD.redeemIssuedVouchers(voucherRedeemRequest);
		} catch (ModuleException me) {
			log.error("AirproxyVoucherBL ==> redeemOwnAirlineVouchers(VoucherRedeemRequest)", me);
			if (me.getExceptionCode().equals("error.voucher.redeem.invalid") || (me.getExceptionCode()
					.equals("error.voucher.pax.name.invalid")) || (me.getExceptionCode()
					.equals("error.voucher.mobile.number.invalid")) || me.getExceptionCode()
					.equals("error.voucher.otp.code.invalid") || me.getExceptionCode()
					.equals("error.voucher.valid.period.invalid")) {
				throw me;
			}
		} catch (Exception ex) {
			log.error("AirproxyVoucherBL ==> redeemOwnAirlineVouchers(VoucherRedeemRequest)", ex);
		}
		return voucherRedeemResponse;
	}

}
