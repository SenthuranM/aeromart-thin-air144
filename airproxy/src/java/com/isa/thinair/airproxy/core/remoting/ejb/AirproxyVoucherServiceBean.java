package com.isa.thinair.airproxy.core.remoting.ejb;

import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.airproxy.core.bl.AirproxyVoucherBL;
import com.isa.thinair.airproxy.core.service.bd.AirproxyVoucherBDLocal;
import com.isa.thinair.airproxy.core.service.bd.AirproxyVoucherBDRemote;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

/**
 * @author chethiya
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "AirproxyVoucherService.remote")
@LocalBinding(jndiBinding = "AirproxyVoucherService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirproxyVoucherServiceBean extends PlatformBaseSessionBean implements AirproxyVoucherBDLocal,
		AirproxyVoucherBDRemote {

	@Override
	public VoucherRedeemResponse getVoucherRedeemResponse(VoucherRedeemRequest voucherRedeemRequest) throws Exception {
		VoucherRedeemResponse voucherRedeemResponse = null;
		try {
			voucherRedeemResponse = AirproxyVoucherBL.redeemVouchers(voucherRedeemRequest);
		} catch (ModuleException me) {
			log.error("AirproxyVoucherServiceBean ==> getVoucherRedeemResponse(VoucherRedeemRequest)", me);
			if (me.getExceptionCode().equals("error.voucher.redeem.invalid") || (me.getExceptionCode()
					.equals("error.voucher.pax.name.invalid")) || (me.getExceptionCode()
					.equals("error.voucher.mobile.number.invalid")) || me.getExceptionCode()
					.equals("error.voucher.otp.code.invalid") || me.getExceptionCode()
					.equals("error.voucher.valid.period.invalid")) {
				throw me;
			}
		} catch (Exception ex) {
			log.error("AirproxyVoucherServiceBean ==> getVoucherRedeemResponse(VoucherRedeemRequest)", ex);
		}
		return voucherRedeemResponse;
	}

}
