package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.FlightSeatStatuses;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirColumnGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCCabinClassDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCPassengerTypeQuantityDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDetailDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

/**
 * BL for seat map
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxySeatMapBL {

	private LCCSeatMapDTO inSeatMapDTO;

	private String selectedLocale;

	private BasicTrackInfo trackInfo;

	private ApplicationEngine appEngine;

	public AirproxySeatMapBL(LCCSeatMapDTO inSeatMapDTO, String selectedLocale, BasicTrackInfo trackInfo) {
		super();
		this.inSeatMapDTO = inSeatMapDTO;
		this.selectedLocale = selectedLocale;
		this.trackInfo = trackInfo;
	}

	private void validateParameters() {
		// TODO to the parameter validation for execution
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public LCCSeatMapDTO execute() throws ModuleException {
		validateParameters();
		if (inSeatMapDTO.getQueryingSystem() == SYSTEM.INT) {
			return AirproxyModuleUtils.getLCCAncillaryBD().getSeatMap(inSeatMapDTO, selectedLocale, trackInfo);
		} else {
			return getOwnAirlineSeatMap();
		}
	}

	/**
	 * Retrieve own airline seat map
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private LCCSeatMapDTO getOwnAirlineSeatMap() throws ModuleException {
		Map<Integer, FlightSeatsDTO> seatMap = null;
		Map<FlightSegmentTO, Map<Integer, FlightSeatsDTO>> flightSegmentSeatMap = new HashMap<FlightSegmentTO, Map<Integer, FlightSeatsDTO>>();
		Map<Integer, Integer> ondTemplateIds = InventoryAPIUtil.getOndWiseSelectedTemplateId(
				this.inSeatMapDTO.getBundledFareDTOs(), EXTERNAL_CHARGES.SEAT_MAP.toString());
		boolean isBundledFareSelected = CommonUtil.isAtleastOneBundledFareSelected(this.inSeatMapDTO.getBundledFareDTOs());

		for (FlightSegmentTO flightSegment : this.inSeatMapDTO.getFlightDetails()) {
			Collection<Integer> flightSegIds = SegmentUtil.getFlightSegmentIds(flightSegment);
			seatMap = AirproxyModuleUtils.getSeatMapBD().getFlightSeats(new ArrayList<Integer>(flightSegIds), selectedLocale,
					isSocialSeatingEnabled());

			if (isBundledFareSelected) {
				Integer bundledFareTemplateId = ondTemplateIds.get(flightSegment.getOndSequence());
				if (bundledFareTemplateId != null) {
					Collection<SeatDTO> bundledFareSeats = AirproxyModuleUtils.getSeatMapBD().getFlightSeatsByChargeTemplate(
							bundledFareTemplateId);
					replaceAttachedSeatValuesWithOffer(seatMap, bundledFareSeats);
				}
			} else {
				AnciOfferCriteriaTO availableOffer = null;

				if (SalesChannelsUtil.isAppEngineWebOrMobile(appEngine)) {
					availableOffer = AirproxyModuleUtils.getAnciOfferBD().getAvailableAnciOffer(
							AnciOfferUtil.createAnciOfferSearch(flightSegment, AnciOfferType.SEAT));
				}
				if (availableOffer != null) {

					Collection<SeatDTO> offerSeats = AirproxyModuleUtils.getSeatMapBD().getFlightSeatsByChargeTemplate(
							availableOffer.getTemplateID());

					replaceAttachedSeatValuesWithOffer(seatMap, offerSeats,
							availableOffer.getNameTranslations().get(selectedLocale), availableOffer.getDescriptionTranslations()
									.get(selectedLocale), availableOffer.getTemplateID());

				}
			}

			flightSegmentSeatMap.put(flightSegment, AncillaryCommonUtil.rearrangeSeatRows(seatMap));
		}
		List<LCCSegmentSeatMapDTO> segmentSeatMapDTOs = transform(flightSegmentSeatMap);
		LCCSeatMapDTO outSeatMapDTO = new LCCSeatMapDTO();
		outSeatMapDTO.setFlightDetails(this.inSeatMapDTO.getFlightDetails());
		outSeatMapDTO.setTransactionIdentifier(this.inSeatMapDTO.getTransactionIdentifier());
		outSeatMapDTO.setQueryingSystem(this.inSeatMapDTO.getQueryingSystem());
		outSeatMapDTO.setLccSegmentSeatMapDTOs(segmentSeatMapDTOs);

		return outSeatMapDTO;
	}

	/**
	 * Transform DTO from Ownairline to proxy
	 * 
	 * @param flightSegmentSeatMap
	 * @return
	 */
	private List<LCCSegmentSeatMapDTO> transform(Map<FlightSegmentTO, Map<Integer, FlightSeatsDTO>> flightSegmentSeatMap) {
		List<LCCSegmentSeatMapDTO> segmentSeatMaps = new ArrayList<LCCSegmentSeatMapDTO>();

		Set<Entry<FlightSegmentTO, Map<Integer, FlightSeatsDTO>>> flightSegmentSeatEntrySet = flightSegmentSeatMap.entrySet();
		for (Entry<FlightSegmentTO, Map<Integer, FlightSeatsDTO>> flightSegmentSeatEntry : flightSegmentSeatEntrySet) {
			FlightSegmentTO flightSegment = flightSegmentSeatEntry.getKey();
			Map<Integer, FlightSeatsDTO> flightSegmentIdFlightSeatsMap = flightSegmentSeatEntry.getValue();

			LCCSegmentSeatMapDTO segmentSeatMap = new LCCSegmentSeatMapDTO();
			segmentSeatMap.setFlightSegmentDTO(flightSegment);

			LCCSeatMapDetailDTO seatMapDetail = getSeatMapDetails(flightSegmentIdFlightSeatsMap, flightSegment, segmentSeatMap);
			segmentSeatMap.setLccSeatMapDetailDTO(seatMapDetail);

			segmentSeatMaps.add(segmentSeatMap);
		}

		return segmentSeatMaps;
	}

	/**
	 * 
	 * @param flightSegmentIdFlightSeatsMap
	 * @param flightSegment
	 * @param segmentSeatMap
	 * @return
	 */
	private LCCSeatMapDetailDTO getSeatMapDetails(Map<Integer, FlightSeatsDTO> flightSegmentIdFlightSeatsMap,
			IFlightSegment flightSegment, LCCSegmentSeatMapDTO segmentSeatMap) {
		LCCSeatMapDetailDTO seatMapDetail = new LCCSeatMapDetailDTO();

		for (Entry<Integer, FlightSeatsDTO> flightSeatsEntry : flightSegmentIdFlightSeatsMap.entrySet()) {
			FlightSeatsDTO flightSeatsDTO = flightSeatsEntry.getValue();
			segmentSeatMap.setAnciOffer(flightSeatsDTO.isAnciOffer());
			segmentSeatMap.setAnciOfferName(flightSeatsDTO.getAnciOfferName());
			segmentSeatMap.setAnciOfferDescription(flightSeatsDTO.getAnciOfferDescription());
			segmentSeatMap.setOfferedAnciTemplateID(flightSeatsDTO.getOfferedTemplateID());

			// Seats in the flightSegment.
			Collection<SeatDTO> seats = flightSeatsDTO.getSeats();

			// Gets the populated list of all the cabin classes.
			List<LCCCabinClassDTO> cabinClass = getCabinClassList(seats, flightSegment);

			seatMapDetail.getCabinClass().addAll(cabinClass);
		}
		return seatMapDetail;
	}

	/**
	 * 
	 * @param seats
	 * @param flightSegment
	 * @return
	 */
	private List<LCCCabinClassDTO> getCabinClassList(Collection<SeatDTO> seats, IFlightSegment flightSegment) {

		List<LCCCabinClassDTO> cabinClassList = new ArrayList<LCCCabinClassDTO>();

		// A map of SeatDTOs and the relevant cabin class code.
		Map<String, List<SeatDTO>> cabinClassSeatMap = AncillaryCommonUtil.getCabinClassSeatMap(seats);
		for (Entry<String, List<SeatDTO>> cabinClassSeatMapEntry : cabinClassSeatMap.entrySet()) {

			// The cabin class code.
			String cabinClassCode = cabinClassSeatMapEntry.getKey();

			// The list of all the Seats in the cabin.
			List<SeatDTO> cabinSeats = cabinClassSeatMapEntry.getValue();

			// Get the list of all the air row groups in cabin class.
			List<LCCAirRowGroupDTO> airRowGroups = getSeatMapRowGroupSeats(cabinSeats, flightSegment);

			LCCCabinClassDTO cabinClass = new LCCCabinClassDTO();
			cabinClass.setCabinType(cabinClassCode);
			cabinClass.getAirRowGroupDTOs().addAll(airRowGroups);

			cabinClassList.add(cabinClass);
		}
		return cabinClassList;
	}

	/**
	 * 
	 * @param cabinSeats
	 * @param flightSegment
	 * @return
	 */
	private List<LCCAirRowGroupDTO> getSeatMapRowGroupSeats(List<SeatDTO> cabinSeats, IFlightSegment flightSegment) {

		List<LCCAirRowGroupDTO> airRowGroups = new ArrayList<LCCAirRowGroupDTO>();
		// A map of SeatDTOs and the relevant cabin class code.
		Map<String, List<SeatDTO>> rowGroupSeatMap = AncillaryCommonUtil.getRowGroupSeatMap(cabinSeats);
		for (Entry<String, List<SeatDTO>> rowGroupSeatMapEntry : rowGroupSeatMap.entrySet()) {

			// The list of all the Seats in the row group.
			List<SeatDTO> rowGroupSeats = rowGroupSeatMapEntry.getValue();

			List<LCCAirColumnGroupDTO> SeatMapColumnGroups = getSeatMapColumnGroupSeats(rowGroupSeats, flightSegment);

			LCCAirRowGroupDTO airRowGroup = new LCCAirRowGroupDTO();
			airRowGroup.setRowGroupId(rowGroupSeatMapEntry.getKey());
			airRowGroup.getAirColumnGroups().addAll(SeatMapColumnGroups);

			airRowGroups.add(airRowGroup);
		}
		return airRowGroups;
	}

	/**
	 * 
	 * @param rowGroupSeats
	 * @param flightSegment
	 * @return
	 */
	private List<LCCAirColumnGroupDTO> getSeatMapColumnGroupSeats(List<SeatDTO> rowGroupSeats, IFlightSegment flightSegment) {
		List<LCCAirColumnGroupDTO> seatMapColumnGroups = new ArrayList<LCCAirColumnGroupDTO>();

		// A map of SeatDTOs and the relevant column group.
		Map<String, List<SeatDTO>> columnGroupSeatMap = AncillaryCommonUtil.getColumnGroupSeatMap(rowGroupSeats);
		for (Entry<String, List<SeatDTO>> columnGroupSeatMapEntry : columnGroupSeatMap.entrySet()) {

			int columnNumber = Integer.parseInt(columnGroupSeatMapEntry.getKey());
			boolean isOddColumn = (columnNumber % 2 != 0) ? true : false;

			// The list of all the Seats in the column group.
			List<SeatDTO> columnGroupSeats = columnGroupSeatMapEntry.getValue();

			List<LCCAirRowDTO> airRows = getAirRowsSeats(columnGroupSeats, flightSegment, isOddColumn);

			LCCAirColumnGroupDTO SeatMapColumnGroup = new LCCAirColumnGroupDTO();
			SeatMapColumnGroup.setColumnGroupId(columnGroupSeatMapEntry.getKey());
			SeatMapColumnGroup.getAirRows().addAll(airRows);

			seatMapColumnGroups.add(SeatMapColumnGroup);
		}
		return seatMapColumnGroups;
	}

	/**
	 * 
	 * @param columnGroupSeats
	 * @param flightSegment
	 * @param isOddColumn
	 * @return
	 */
	private List<LCCAirRowDTO>
			getAirRowsSeats(List<SeatDTO> columnGroupSeats, IFlightSegment flightSegment, boolean isOddColumn) {
		List<LCCAirRowDTO> airRows = new ArrayList<LCCAirRowDTO>();

		Map<String, List<SeatDTO>> rowSeatsMap = AncillaryCommonUtil.getRowSeatMap(columnGroupSeats);
		for (Entry<String, List<SeatDTO>> rowSeatsMapEntry : rowSeatsMap.entrySet()) {
			int rowNumber = Integer.parseInt(rowSeatsMapEntry.getKey());
			List<SeatDTO> rowSeats = rowSeatsMapEntry.getValue();

			LCCAirRowDTO airRow = new LCCAirRowDTO();
			airRow.setRowNumber(rowNumber);

			boolean isOddRow = (rowNumber % 2 != 0) ? true : false;

			boolean isInfantBooked = false;
			int vacantSeats = 0;
			int seatsOpenForReservation = 0;

			List<LCCAirSeatDTO> airSeats = new ArrayList<LCCAirSeatDTO>();
			for (SeatDTO seatDTO : rowSeats) {

				LCCAirSeatDTO airSeat = getPopulatedAirSeat(seatDTO);
				airSeats.add(airSeat);

				String paxType = seatDTO.getPaxType();
				if (paxType != null && paxType.equalsIgnoreCase(PaxTypeTO.PARENT)) {
					isInfantBooked = true;
				}

				if (seatDTO.getStatus().equals("VAC")) {
					vacantSeats = vacantSeats + 1;
				}

				if (seatDTO.getStatus().equals("VAC") || seatDTO.getStatus().equals("ACQ") || seatDTO.getStatus().equals("BLK")
						|| seatDTO.getStatus().equals("RES")) {
					seatsOpenForReservation = seatsOpenForReservation + 1;
				}
			}
			List<LCCPassengerTypeQuantityDTO> passengerTypeQuantities = new ArrayList<LCCPassengerTypeQuantityDTO>();
			if (vacantSeats > 0) {
				airRow.setSeatsVacant(true);

				LCCPassengerTypeQuantityDTO passengerTypeQuantity = new LCCPassengerTypeQuantityDTO();
				passengerTypeQuantity.setPaxType(PaxTypeTO.ADULT);
				passengerTypeQuantity.setQuantity(vacantSeats);

				passengerTypeQuantities.add(passengerTypeQuantity);

				passengerTypeQuantity = new LCCPassengerTypeQuantityDTO();
				passengerTypeQuantity.setPaxType(PaxTypeTO.CHILD);
				passengerTypeQuantity.setQuantity(vacantSeats);

				passengerTypeQuantities.add(passengerTypeQuantity);

				if (!isInfantBooked) {

					if ((isOddColumn && isOddRow) || !isOddColumn) {
						passengerTypeQuantity = new LCCPassengerTypeQuantityDTO();
						passengerTypeQuantity.setPaxType(PaxTypeTO.INFANT);
						passengerTypeQuantity.setQuantity(1);
					} else {
						passengerTypeQuantity = new LCCPassengerTypeQuantityDTO();
						passengerTypeQuantity.setPaxType(PaxTypeTO.INFANT);
						passengerTypeQuantity.setQuantity(0);
					}

					passengerTypeQuantities.add(passengerTypeQuantity);
				} else {
					passengerTypeQuantity = new LCCPassengerTypeQuantityDTO();
					passengerTypeQuantity.setPaxType(PaxTypeTO.INFANT);
					passengerTypeQuantity.setQuantity(0);

					passengerTypeQuantities.add(passengerTypeQuantity);
				}
			}

			airRow.getPassengerTypeQuantity().addAll(passengerTypeQuantities);
			airRow.getAirSeats().addAll(airSeats);

			airRows.add(airRow);
		}

		return airRows;
	}

	/**
	 * 
	 * @param seatDTO
	 * @return
	 */
	private LCCAirSeatDTO getPopulatedAirSeat(SeatDTO seatDTO) {
		LCCAirSeatDTO airSeat = new LCCAirSeatDTO();
		airSeat.setSeatAvailability(seatDTO.getStatus());
		airSeat.setSeatCharge(seatDTO.getChargeAmount());
		airSeat.setSeatNumber(seatDTO.getSeatCode());
		airSeat.setBookedPassengerType(seatDTO.getPaxType());
		airSeat.setSeatType(seatDTO.getSeatType());
		airSeat.setSeatLocationType(seatDTO.getSeatLocationType());
		airSeat.setSeatMessage(seatDTO.getSeatMessage());
		airSeat.setLogicalCabinClassCode(seatDTO.getLogicalCabinClassCode());
		airSeat.setSocialIdentity(seatDTO.getSocialIdentity());
		airSeat.setPaxName(seatDTO.getName());
		airSeat.setSeatVisibility(seatDTO.getSeatVisibility());

		if (seatDTO.getSeatType() != null && seatDTO.getSeatType().equalsIgnoreCase("EXIT")) {
			airSeat.getNotAllowedPassengerType().add(PaxTypeTO.INFANT);
			//fix for AARESAA-24633 and removed from AARESAA-25494
			airSeat.getNotAllowedPassengerType().add(PaxTypeTO.CHILD);
		}
		return airSeat;
	}

	/**
	 * Replaces the seat charge amounts with the offer charge amounts and set the anci offer related data.
	 * 
	 * @param selectedSeatMap
	 *            The selected seat map details to be modified.
	 * @param offerSeats
	 *            Offer seats with the offer charge and seat code populated.
	 * @param offerName
	 *            Selected offer name.
	 * @param offerDescription
	 *            Selected offer description.
	 * @param offeredTemplateID
	 *            Selected offer seat charge template ID.
	 */
	private void replaceAttachedSeatValuesWithOffer(Map<Integer, FlightSeatsDTO> selectedSeatMap, Collection<SeatDTO> offerSeats,
			String offerName, String offerDescription, Integer offeredTemplateID) {
		for (Entry<Integer, FlightSeatsDTO> segWiseSeats : selectedSeatMap.entrySet()) {

			FlightSeatsDTO selFlightSeatDTO = segWiseSeats.getValue();
			selFlightSeatDTO.setAnciOffer(true);
			selFlightSeatDTO.setAnciOfferName(offerName);
			selFlightSeatDTO.setAnciOfferDescription(offerDescription);
			selFlightSeatDTO.setOfferedTemplateID(offeredTemplateID);

			Collection<SeatDTO> selectedSeats = selFlightSeatDTO.getSeats();

			overrideSeatCharges(selectedSeats, offerSeats);
		}
	}

	private void replaceAttachedSeatValuesWithOffer(Map<Integer, FlightSeatsDTO> selectedSeatMap, Collection<SeatDTO> offerSeats) {
		for (Entry<Integer, FlightSeatsDTO> segWiseSeats : selectedSeatMap.entrySet()) {
			FlightSeatsDTO selFlightSeatDTO = segWiseSeats.getValue();
			Collection<SeatDTO> selectedSeats = selFlightSeatDTO.getSeats();

			overrideSeatCharges(selectedSeats, offerSeats);
		}
	}

	private void overrideSeatCharges(Collection<SeatDTO> selectedSeats, Collection<SeatDTO> offeredSeats) {
		for (SeatDTO selectedSeat : selectedSeats) {
			for (SeatDTO offeredSeat : offeredSeats) {
				if (selectedSeat.getSeatCode().equals(offeredSeat.getSeatCode())) {
					selectedSeat.setChargeAmount(offeredSeat.getChargeAmount());
					if (FlightSeatStatuses.INACTIVE.equals(offeredSeat.getStatus())) {
						selectedSeat.setStatus(FlightSeatStatuses.RESERVED);
					}
				}
			}
		}
	}

	private boolean isSocialSeatingEnabled() {
		boolean enabled = false;
		if (appEngine != null && SalesChannelsUtil.isAppEngineWebOrMobile(appEngine)) {
			enabled = AppSysParamsUtil.isSocialSeatingEnabledWithFacebook()
					|| AppSysParamsUtil.isSocialSeatingEnablesWithLinkedIn();
		}
		return enabled;
	}

	public ApplicationEngine getAppEngine() {
		return appEngine;
	}

	public void setAppEngine(ApplicationEngine appEngine) {
		this.appEngine = appEngine;
	}
}
