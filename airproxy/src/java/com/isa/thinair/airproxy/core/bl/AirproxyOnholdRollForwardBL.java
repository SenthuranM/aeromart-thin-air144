package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * BL for roll forwarding OHD bookings
 * 
 * @author rumesh
 * 
 */
public class AirproxyOnholdRollForwardBL {
	private static Log log = LogFactory.getLog(AirproxyOnholdRollForwardBL.class);

	private String pnr;
	private Collection<LCCClientReservationSegment> colsegs;
	private Integer adultCount;
	private Integer childCount;
	private Integer infantCount;
	private Map<String, RollForwardFlightRQ> rollForwardingFlights;
	private SYSTEM searchSystem;
	private UserPrincipal userPrincipal;
	private TrackInfoDTO trackInfo;
	private boolean duplicateNameSkip;
	private boolean overBook;

	public ServiceResponce execute() throws ModuleException {
		if (searchSystem == SYSTEM.INT) {
			// TODO Handle interline/dry onhold booking roll forward
			return null;
		} else {
			return rollForwardOwnOnholdBooking();
		}
	}

	private ServiceResponce rollForwardOwnOnholdBooking() throws ModuleException {

		// populate AvailableFlightSearchDTO
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(adultCount);
		availableFlightSearchDTO.setChildCount(childCount);
		availableFlightSearchDTO.setInfantCount(infantCount);
		availableFlightSearchDTO.setChannelCode(userPrincipal.getSalesChannel());
		availableFlightSearchDTO.setPosAirport(userPrincipal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(userPrincipal.getAgentCode());
		if (overBook) {
			availableFlightSearchDTO.setBookingType(BookingClass.BookingClassType.OVERBOOK);
		}

		// populate ReservationSegmentDTO List
		List<ReservationSegmentDTO> reservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();
		if (colsegs != null && colsegs.size() > 0) {
			for (LCCClientReservationSegment lccSeg : colsegs) {

				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(lccSeg.getStatus())) {
					continue;
				}

				ReservationSegmentDTO resDTO = new ReservationSegmentDTO();
				resDTO.setArrivalDate(lccSeg.getArrivalDate());
				resDTO.setArrivalTerminalName(lccSeg.getArrivalTerminalName());
				resDTO.setBookingType(lccSeg.getBookingType());
				resDTO.setCabinClassCode(lccSeg.getCabinClassCode());
				resDTO.setLogicalCCCode(lccSeg.getLogicalCabinClass());
				resDTO.setLogicalCCCode(null);
				resDTO.setDepartureDate(lccSeg.getDepartureDate());
				resDTO.setDepartureTerminalName(lccSeg.getDepartureTerminalName());
				resDTO.setFareGroupId(new Integer(lccSeg.getInterlineGroupKey()));
				resDTO.setFlightModelNumber(lccSeg.getFlightModelNumber());
				resDTO.setFlightNo(lccSeg.getFlightNo());
				resDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(lccSeg.getFlightSegmentRefNumber()));
				resDTO.setReturnFlag(lccSeg.getReturnFlag());
				resDTO.setReturnOndGroupId(new Integer(lccSeg.getInterlineReturnGroupKey()));
				resDTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(lccSeg.getFlightSegmentRefNumber()));
				resDTO.setSegmentSeq(lccSeg.getSegmentSeq());
				resDTO.setStatus(lccSeg.getStatus());
				resDTO.setSubStatus(lccSeg.getSubStatus());
				resDTO.setSubStationShortName(lccSeg.getSubStationShortName());

				reservationSegmentDTOs.add(resDTO);
			}
		}

		ServiceResponce response = AirproxyModuleUtils.getReservationBD().rollForwardOnholdBooking(pnr, availableFlightSearchDTO,
				reservationSegmentDTOs, rollForwardingFlights, userPrincipal.getUserId(), trackInfo, duplicateNameSkip);

		ServiceResponce output = (ServiceResponce) response.getResponseParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		return output;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Collection<LCCClientReservationSegment> getColsegs() {
		return colsegs;
	}

	public void setColsegs(Collection<LCCClientReservationSegment> colsegs) {
		this.colsegs = colsegs;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public Map<String, RollForwardFlightRQ> getRollForwardingFlights() {
		return rollForwardingFlights;
	}

	public void setRollForwardingFlights(Map<String, RollForwardFlightRQ> rollForwardingFlights) {
		this.rollForwardingFlights = rollForwardingFlights;
	}

	public SYSTEM getSearchSystem() {
		return searchSystem;
	}

	public void setSearchSystem(SYSTEM searchSystem) {
		this.searchSystem = searchSystem;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public TrackInfoDTO getTrackInfo() {
		return trackInfo;
	}

	public void setTrackInfo(TrackInfoDTO trackInfo) {
		this.trackInfo = trackInfo;
	}

	public boolean isDuplicateNameSkip() {
		return duplicateNameSkip;
	}

	public void setDuplicateNameSkip(boolean duplicateNameSkip) {
		this.duplicateNameSkip = duplicateNameSkip;
	}

	public boolean isOverBook() {
		return overBook;
	}

	public void setOverBook(boolean overBook) {
		this.overBook = overBook;
	}
}
