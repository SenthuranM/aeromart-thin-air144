package com.isa.thinair.airproxy.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Manoj Dhanushka
 */
public class AirproxyAirportTransferBL {

	private Log log = LogFactory.getLog(AirproxyAirportTransferBL.class);

	private SYSTEM system;
	private List<FlightSegmentTO> flightSegmentTOs;
	private AppIndicatorEnum appIndicator;
	private UserPrincipal userPrincipal;
	private Map<String, Integer> serviceCount;
	private boolean modifyAnci;
	
	public static final String APPLICABILITY_TYPE_PAX = "P";
	public static final String APPLICABILITY_TYPE_RESERVATION = "R";
	
	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> execute() throws ModuleException {

		if (system == SYSTEM.INT) {
			//Not yet implemented.
			return null;
		} else {
			return getOwnAirlineAiportTransfers();
		}
	}
	
	private Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getOwnAirlineAiportTransfers() throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				flightSegmentTOs, serviceCount, true);
		Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> lccAiportWiseMap = new LinkedHashMap<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO>();
		List<Integer> flightSegIds = new ArrayList<Integer>();

		for (FlightSegmentTO fltSeg : airportWiseMap.values()) {
			flightSegIds.add(fltSeg.getFlightSegId());
		}
		Map<Integer, Boolean> csOCFlightMapping = AirproxyModuleUtils.getFlightBD().isAtLeastOneSegmentHavingCSOCFlight(
				flightSegIds);

		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseResults = AirproxyModuleUtils.getAirportTransferBD()
					.getAvailableAirportTransfers(airportWiseMap, csOCFlightMapping);

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> entry : airportWiseResults.entrySet()) {
					AirportServiceKeyTO keyTO = entry.getKey();
					if (entry.getValue().size() > 0) {
						IFlightSegment originalFltSeg = airportWiseMap.get(keyTO);
						if (flightSegmentTO.getSegmentCode().equals(originalFltSeg.getSegmentCode())
								&& flightSegmentTO.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {
							List<LCCAirportServiceDTO> lccAirportServiceList = new ArrayList<LCCAirportServiceDTO>();

							for (AirportServiceDTO airportServiceDTO : entry.getValue()) {
								if ("Y".equals(airportServiceDTO.getStatus()) && lccAirportServiceList.isEmpty()) {
									lccAirportServiceList.add(populateLCCAirportServiceDTO(airportServiceDTO));
								}
							}
							LCCFlightSegmentAirportServiceDTO lccFSegAirportServiceDTO = new LCCFlightSegmentAirportServiceDTO();
							lccFSegAirportServiceDTO.setFlightSegmentTO(flightSegmentTO);
							lccFSegAirportServiceDTO.setAirportServices(lccAirportServiceList);
							lccAiportWiseMap.put(keyTO, lccFSegAirportServiceDTO);
						}
					}
				}
			}
		}
		return lccAiportWiseMap;
	}
	
	private LCCAirportServiceDTO populateLCCAirportServiceDTO(AirportServiceDTO airportServiceDTO) {
		LCCAirportServiceDTO lccAirportServiceDTO = new LCCAirportServiceDTO();
		lccAirportServiceDTO.setAirportCode(airportServiceDTO.getAirport());
		lccAirportServiceDTO.setAdultAmount(new BigDecimal(airportServiceDTO.getAdultAmount()));
		lccAirportServiceDTO.setChildAmount(new BigDecimal(airportServiceDTO.getChildAmount()));
		lccAirportServiceDTO.setInfantAmount(new BigDecimal(airportServiceDTO.getInfantAmount()));
		lccAirportServiceDTO.setSsrName(airportServiceDTO.getSsrName());
		lccAirportServiceDTO.setStatus(airportServiceDTO.getStatus());
		lccAirportServiceDTO.setSsrDescription(airportServiceDTO.getDescription());
		lccAirportServiceDTO.setApplicabilityType(APPLICABILITY_TYPE_PAX);
//		lccAirportServiceDTO.setReservationAmount(new BigDecimal(airportServiceDTO.getReservationAmount()));
//		lccAirportServiceDTO.setSsrCode(airportServiceDTO.getSsrCode());
//		lccAirportServiceDTO.setSsrImagePath(airportServiceDTO.getSsrImagePath());
//		lccAirportServiceDTO.setSsrThumbnailImagePath(airportServiceDTO.getSsrThumbnailImagePath());
//		if (airportServiceDTO.getDecriptionSelectedLanguage() == null
//				|| StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null"))
//			lccAirportServiceDTO.setSsrDescription(airportServiceDTO.getDescription());
//		else {
//			String ucs = StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage());
//			ucs = ucs.replace("^", ",");
//			lccAirportServiceDTO.setSsrDescription(ucs);
//		}
		lccAirportServiceDTO.setProvider(airportServiceDTO.getProvider());
		return lccAirportServiceDTO;
	}
	
	public SYSTEM getSystem() {
		return system;
	}
	
	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}
	
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}
	
	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}
	
	public Map<String, Integer> getServiceCount() {
		return serviceCount;
	}
	
	public boolean isModifyAnci() {
		return modifyAnci;
	}
	
	public void setSystem(SYSTEM system) {
		this.system = system;
	}
	
	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}
	
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}
	
	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}
	
	public void setServiceCount(Map<String, Integer> serviceCount) {
		this.serviceCount = serviceCount;
	}
	
	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}	
}
