package com.isa.thinair.airproxy.core.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.lccclient.core.bl.LCCClientDuplicateNameCheckBL;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AirproxyDuplicateCheckBL {

	public static boolean checkForDuplicates(DuplicateValidatorAssembler duplicateValidatorAssm , UserPrincipal userPrincipal , BasicTrackInfo trackInfo) {
		
		if (duplicateValidatorAssm.getTargetSystem() == SYSTEM.AA) {
			boolean hasDuplicate = false;
			try {
				List<Integer> fltSegIds = new ArrayList<Integer>();
				for (FlightSegmentTO fltSeg : duplicateValidatorAssm.getFlightSegments()) {
					Integer fltId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber());
					fltSegIds.add(fltId);
				}
				Set<ReservationPax> resPaxs = new HashSet<ReservationPax>();
				for (LCCClientReservationPax pax : duplicateValidatorAssm.getPaxList()) {
					ReservationPax resPax = new ReservationPax();
					resPax.setTitle(pax.getTitle());
					resPax.setFirstName(pax.getFirstName());
					resPax.setLastName(pax.getLastName());
					resPax.setPaxType(pax.getPaxType());
					if (pax.getTravelerRefNumber() != null && !pax.getTravelerRefNumber().isEmpty()) {
						resPax.setPnrPaxId(new Integer(pax.getTravelerRefNumber()));
					}
					resPaxs.add(resPax);
				}
				ReservationApiUtils.checkEndValidations(fltSegIds, resPaxs, duplicateValidatorAssm.getPnr());
			} catch (ModuleException me) {
				if (ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT.equals(me.getExceptionCode())) {
					hasDuplicate = true;
				}
			}
			return hasDuplicate;
		} else if(duplicateValidatorAssm.getTargetSystem() == SYSTEM.INT){
			
			LCCClientDuplicateNameCheckBL lccClientDuplicateNameCheckBL = new LCCClientDuplicateNameCheckBL(
					duplicateValidatorAssm.getFlightSegments(),
					duplicateValidatorAssm.getPaxList() , trackInfo , userPrincipal);
			
			return lccClientDuplicateNameCheckBL.duplicateNameCheck();
						
		} else {
			return false;
		}
	}

}
