package com.isa.thinair.airproxy.core.bl;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AirproxyServiceTaxBL {

	public static Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfo, UserPrincipal userPrincipal)
			throws ModuleException {
		Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap = new HashMap<String, ServiceTaxQuoteForTicketingRevenueResTO>();
		SYSTEM prefSystem = serviceTaxQuoteCriteriaDTO.getBaseAvailRQ().getAvailPreferences().getSearchSystem();
		if (prefSystem == SYSTEM.AA) {
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = ServiceTaxConverterUtil
					.createServiceTaxQuoteCriteriaForTicketingRevenueRequest(serviceTaxQuoteCriteriaDTO, userPrincipal);
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxQuoteForTktRevRS = AirproxyModuleUtils.getChargeBD()
					.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);
			serviceTaxQuoteForTktRevResTOMap.put(AppSysParamsUtil.getDefaultCarrierCode(),
					ServiceTaxConverterUtil.adaptServiceTaxQuoteForTicketingRevenueRS(serviceTaxQuoteForTktRevRS));
		} else if (prefSystem == SYSTEM.INT) {
			serviceTaxQuoteForTktRevResTOMap = AirproxyModuleUtils.getLCCSearchAndQuoteBD().quoteServiceTaxForTicketingRevenue(
					serviceTaxQuoteCriteriaDTO, trackInfo, userPrincipal);
		}
		return serviceTaxQuoteForTktRevResTOMap;
	}

	public static ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfo, UserPrincipal userPrincipal)
			throws ModuleException {
		ServiceTaxQuoteForNonTicketingRevenueRS serviceTaxQuoteForNonTktRevRS = null;
		SYSTEM prefSystem = serviceTaxQuoteCriteriaDTO.getPrefSystem();
		if (prefSystem == SYSTEM.AA) {
			ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ = ServiceTaxConverterUtil
					.createServiceTaxQuoteCriteriaForNonTicketingRevenueRequest(serviceTaxQuoteCriteriaDTO, userPrincipal);
			serviceTaxQuoteForNonTktRevRS = AirproxyModuleUtils.getChargeBD().quoteServiceTaxForNonTicketingRevenue(
					serviceTaxQuoteForNonTktRevRQ);
		} else if (prefSystem == SYSTEM.INT) {
			// Not required at the moment
		}
		return serviceTaxQuoteForNonTktRevRS;
	}
}
