package com.isa.thinair.airproxy.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airproxy.api.service.AirproxyAutomaticCheckinBD;

@Remote
public interface AirproxyAutomaticCheckinBDRemote extends AirproxyAutomaticCheckinBD {

}
