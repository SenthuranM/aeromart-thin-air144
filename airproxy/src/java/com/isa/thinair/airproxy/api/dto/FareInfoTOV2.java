package com.isa.thinair.airproxy.api.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FareInfoTOV2 {

	private BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalSurcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalCanCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalAdjCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalPrice = null;

	private Collection<ChargeMetaTO> fare = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> taxes = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> surcharges = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> modifications = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> cancellations = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> adjustments = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> discounts = new ArrayList<ChargeMetaTO>();
	private Collection<ChargeMetaTO> penalty = new ArrayList<ChargeMetaTO>();

	public void addMetaCharges(Collection<ChargeMetaTO> colChargeMetaTO) {
		for (ChargeMetaTO chargeMetaTO : colChargeMetaTO) {
			addMetaCharge(chargeMetaTO);
		}
	}

	public void addMetaCharge(ChargeMetaTO chargeMetaTO) {
		if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeMetaTO.getChargeGroupCode())) {
			addFare(chargeMetaTO);

			// currently ROE adjustment is only for FARE applying only for FAR Group
			if (chargeMetaTO.getAdjustmentAmount() != null && chargeMetaTO.getAdjustmentAmount().doubleValue() != 0) {
				totalAdjCharge = AccelAeroCalculator.add(totalAdjCharge, chargeMetaTO.getAdjustmentAmount());
			}

		} else if (ReservationInternalConstants.ChargeGroup.TAX.equals(chargeMetaTO.getChargeGroupCode())) {
			addTax(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.SUR.equals(chargeMetaTO.getChargeGroupCode())
				|| ReservationInternalConstants.ChargeGroup.INF.equals(chargeMetaTO.getChargeGroupCode())) {
			addSurcharge(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.MOD.equals(chargeMetaTO.getChargeGroupCode())) {
			addModification(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.CNX.equals(chargeMetaTO.getChargeGroupCode())) {
			addCancellation(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeMetaTO.getChargeGroupCode())) {
			addAdjustments(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.DISCOUNT.equals(chargeMetaTO.getChargeGroupCode())) {
			addDiscounts(chargeMetaTO);
		} else if (ReservationInternalConstants.ChargeGroup.PEN.equals(chargeMetaTO.getChargeGroupCode())) {
			addPenalty(chargeMetaTO);
		}
		addDiscounts(chargeMetaTO);
	}

	/**
	 * @return the totalFare
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return the totalTax
	 */
	public BigDecimal getTotalTax() {
		return totalTax;
	}

	/**
	 * @param totalTax
	 *            the totalTax to set
	 */
	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	/**
	 * @return the totalSurcharge
	 */
	public BigDecimal getTotalSurcharge() {
		return totalSurcharge;
	}

	/**
	 * @param totalSurcharge
	 *            the totalSurcharge to set
	 */
	public void setTotalSurcharge(BigDecimal totalSurcharge) {
		this.totalSurcharge = totalSurcharge;
	}

	/**
	 * @return the totalModCharge
	 */
	public BigDecimal getTotalModCharge() {
		return totalModCharge;
	}

	/**
	 * @param totalModCharge
	 *            the totalModCharge to set
	 */
	public void setTotalModCharge(BigDecimal totalModCharge) {
		this.totalModCharge = totalModCharge;
	}

	/**
	 * @return the totalCanCharge
	 */
	public BigDecimal getTotalCanCharge() {
		return totalCanCharge;
	}

	/**
	 * @param totalCanCharge
	 *            the totalCanCharge to set
	 */
	public void setTotalCanCharge(BigDecimal totalCanCharge) {
		this.totalCanCharge = totalCanCharge;
	}

	/**
	 * @return the totalAdjCharge
	 */
	public BigDecimal getTotalAdjCharge() {
		return totalAdjCharge;
	}

	/**
	 * @param totalAdjCharge
	 *            the totalAdjCharge to set
	 */
	public void setTotalAdjCharge(BigDecimal totalAdjCharge) {
		this.totalAdjCharge = totalAdjCharge;
	}

	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		if (totalPrice == null) {
			totalPrice = AccelAeroCalculator.add(totalFare, totalSurcharge, totalTax, totalModCharge, totalCanCharge,
					totalAdjCharge, totalDiscount, totalPenalty);
		}
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the fare
	 */
	public Collection<ChargeMetaTO> getFare() {
		return fare;
	}

	/**
	 * @return the taxes
	 */
	public Collection<ChargeMetaTO> getTaxes() {
		return taxes;
	}

	/**
	 * @return the surcharges
	 */
	public Collection<ChargeMetaTO> getSurcharges() {
		return surcharges;
	}

	/**
	 * @return the modifications
	 */
	public Collection<ChargeMetaTO> getModifications() {
		return modifications;
	}

	/**
	 * @return the cancellations
	 */
	public Collection<ChargeMetaTO> getCancellations() {
		return cancellations;
	}

	/**
	 * @return the adjustments
	 */
	public Collection<ChargeMetaTO> getAdjustments() {
		return adjustments;
	}

	public Collection<ChargeMetaTO> getDiscounts() {
		return discounts;
	}

	public Collection<ChargeMetaTO> getPenalty() {
		return penalty;
	}

	public void addFare(ChargeMetaTO chargeMetaTO) {
		totalFare = AccelAeroCalculator.add(totalFare, chargeMetaTO.getChargeAmount());
		this.getFare().add(chargeMetaTO);
	}

	public void addTax(ChargeMetaTO chargeMetaTO) {
		totalTax = AccelAeroCalculator.add(totalTax, chargeMetaTO.getChargeAmount());
		this.getTaxes().add(chargeMetaTO);
	}

	public void addSurcharge(ChargeMetaTO chargeMetaTO) {
		totalSurcharge = AccelAeroCalculator.add(totalSurcharge, chargeMetaTO.getChargeAmount());
		this.getSurcharges().add(chargeMetaTO);
	}

	public void addCancellation(ChargeMetaTO chargeMetaTO) {
		totalCanCharge = AccelAeroCalculator.add(totalCanCharge, chargeMetaTO.getChargeAmount());
		this.getCancellations().add(chargeMetaTO);
	}

	public void addModification(ChargeMetaTO chargeMetaTO) {
		totalModCharge = AccelAeroCalculator.add(totalModCharge, chargeMetaTO.getChargeAmount());
		this.getModifications().add(chargeMetaTO);
	}

	public void addAdjustments(ChargeMetaTO chargeMetaTO) {
		totalAdjCharge = AccelAeroCalculator.add(totalAdjCharge, chargeMetaTO.getChargeAmount());
		this.getAdjustments().add(chargeMetaTO);
	}

	public void addDiscounts(ChargeMetaTO chargeMetaTO) {
		totalDiscount = AccelAeroCalculator.add(totalDiscount, chargeMetaTO.getDiscountAmount());
	}

	public void addPenalty(ChargeMetaTO chargeMetaTO) {
		totalPenalty = AccelAeroCalculator.add(totalPenalty, chargeMetaTO.getChargeAmount());
		this.getPenalty().add(chargeMetaTO);
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public BigDecimal getTotalPenalty() {
		return totalPenalty;
	}

	public void setTotalPenalty(BigDecimal totalPenalty) {
		this.totalPenalty = totalPenalty;
	}

}
