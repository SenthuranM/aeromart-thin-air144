package com.isa.thinair.airproxy.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.LccPaxPaymentTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class PAXSummaryTOV2 {

	private Integer pnrPaxId;

	private String paxName;

	private String paxType;

	private Integer infantId;

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCnxChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPaxFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Collection<ReservationTnx> colPayments;

	private Collection<LccPaxPaymentTO> colLccPaxPayments;

	private Collection<ReservationTnx> colCredits;

	private SegmentSummaryTOV2 segmentSummaryTOV2;

	private PnrChargeDetailTO cnxChargeDetailTO;

	private PnrChargeDetailTO modChargeDetailTO;

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the infantId
	 */
	public Integer getInfantId() {
		return infantId;
	}

	/**
	 * @param infantId
	 *            the infantId to set
	 */
	public void setInfantId(Integer infantId) {
		this.infantId = infantId;
	}

	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalCnxChargeForCurrentAlt
	 */
	public BigDecimal getTotalCnxChargeForCurrentAlt() {
		return totalCnxChargeForCurrentAlt;
	}

	/**
	 * @param totalCnxChargeForCurrentAlt
	 *            the totalCnxChargeForCurrentAlt to set
	 */
	public void setTotalCnxChargeForCurrentAlt(BigDecimal totalCnxChargeForCurrentAlt) {
		this.totalCnxChargeForCurrentAlt = totalCnxChargeForCurrentAlt;
	}

	/**
	 * @return the totalModChargeForCurrentAlt
	 */
	public BigDecimal getTotalModChargeForCurrentAlt() {
		return totalModChargeForCurrentAlt;
	}

	/**
	 * @param totalModChargeForCurrentAlt
	 *            the totalModChargeForCurrentAlt to set
	 */
	public void setTotalModChargeForCurrentAlt(BigDecimal totalModChargeForCurrentAlt) {
		this.totalModChargeForCurrentAlt = totalModChargeForCurrentAlt;
	}

	/**
	 * @return the colPayments
	 */
	public Collection<ReservationTnx> getColPayments() {
		return colPayments;
	}

	/**
	 * @param colPayments
	 *            the colPayments to set
	 */
	public void setColPayments(Collection<ReservationTnx> colPayments) {
		this.colPayments = colPayments;
	}

	/**
	 * @return the colCredits
	 */
	public Collection<ReservationTnx> getColCredits() {
		return colCredits;
	}

	/**
	 * @param colCredits
	 *            the colCredits to set
	 */
	public void setColCredits(Collection<ReservationTnx> colCredits) {
		this.colCredits = colCredits;
	}

	/**
	 * @return the segmentSummaryTOV2
	 */
	public SegmentSummaryTOV2 getSegmentSummaryTOV2() {
		return segmentSummaryTOV2;
	}

	/**
	 * @param segmentSummaryTOV2
	 *            the segmentSummaryTOV2 to set
	 */
	public void setSegmentSummaryTOV2(SegmentSummaryTOV2 segmentSummaryTOV2) {
		this.segmentSummaryTOV2 = segmentSummaryTOV2;
	}

	/**
	 * @return the colLccPaxPayments
	 */
	public Collection<LccPaxPaymentTO> getColLccPaxPayments() {
		return colLccPaxPayments;
	}

	/**
	 * @param colLccPaxPayments
	 *            the colLccPaxPayments to set
	 */
	public void setColLccPaxPayments(Collection<LccPaxPaymentTO> colLccPaxPayments) {
		this.colLccPaxPayments = colLccPaxPayments;
	}

	/**
	 * @return the totalAmountDue
	 */
	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	/**
	 * @param totalAmountDue
	 *            the totalAmountDue to set
	 */
	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	/**
	 * @return the totalCreditAmount
	 */
	public BigDecimal getTotalCreditAmount() {
		return totalCreditAmount;
	}

	/**
	 * @param totalCreditAmount
	 *            the totalCreditAmount to set
	 */
	public void setTotalCreditAmount(BigDecimal totalCreditAmount) {
		this.totalCreditAmount = totalCreditAmount;
	}

	/**
	 * @return the cnxChargeDetailTO
	 */
	public PnrChargeDetailTO getCnxChargeDetailTO() {
		return cnxChargeDetailTO;
	}

	/**
	 * @param cnxChargeDetailTO
	 *            the cnxChargeDetailTO to set
	 */
	public void setCnxChargeDetailTO(PnrChargeDetailTO cnxChargeDetailTO) {
		this.cnxChargeDetailTO = cnxChargeDetailTO;
	}

	/**
	 * @return the modChargeDetailTO
	 */
	public PnrChargeDetailTO getModChargeDetailTO() {
		return modChargeDetailTO;
	}

	/**
	 * @param modChargeDetailTO
	 *            the modChargeDetailTO to set
	 */
	public void setModChargeDetailTO(PnrChargeDetailTO modChargeDetailTO) {
		this.modChargeDetailTO = modChargeDetailTO;
	}

	public void setTotalFareDiscountAmount(BigDecimal totalPaxFareDiscount) {
		this.totalPaxFareDiscount = totalPaxFareDiscount;
	}

	public BigDecimal getTotalPaxFareDiscount() {
		return totalPaxFareDiscount;
	}
}
