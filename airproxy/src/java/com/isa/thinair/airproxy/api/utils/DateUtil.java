package com.isa.thinair.airproxy.api.utils;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Date Utility
 */
public class DateUtil {

	/**
	 * Convert Zulu date to a Local date
	 * 
	 * @param airportCode
	 * @param zuluDate
	 * @return
	 * @throws ModuleException
	 */
	public static Date getLocalDateTime(Date zuluDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(AirproxyModuleUtils.getAirportBD());

		return helper.getLocalDateTime(airportCode, zuluDate);
	}

	/**
	 * Convert Zulu date to a Local date string
	 * 
	 * @param zuluDate
	 * @param airPortCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getLocalDateTimeString(Date zuluDate, String airportCode, String strDateFormat) throws ModuleException {
		Date localDate = DateUtil.getLocalDateTime(zuluDate, airportCode);

		return BeanUtils.parseDateFormat(localDate, strDateFormat);
	}

	/**
	 * Convert Local date to a Zulu date
	 * 
	 * @param airportCode
	 * @param zuluDate
	 * @return
	 * @throws ModuleException
	 */
	public static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(AirproxyModuleUtils.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}

	/**
	 * Convert Local date to a Zulu date string
	 * 
	 * @param zuluDate
	 * @param airPortCode
	 * @return
	 * @throws ModuleException
	 */
	public static String getZuluDateTimeString(Date localDate, String airportCode, String strDateFormat) throws ModuleException {
		Date zuluDate = DateUtil.getLocalDateTime(localDate, airportCode);
		return BeanUtils.parseDateFormat(zuluDate, strDateFormat);
	}

	/**
	 * get the local time for a given agent. This is also support the dry agents as well
	 * 
	 * @param zuluTime
	 * @param colUserDST
	 * @param strStation
	 * @param strDateFormat
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentLocalTime(Date zuluTime, Collection colUserDST, String strStation, String strDateFormat)
			throws ModuleException {
		if (colUserDST != null && !colUserDST.isEmpty()) {
			Iterator itColUserDST = colUserDST.iterator();
			boolean matchFound = false;
			Date localDate = null;
			UserDST userDST;
			while (itColUserDST.hasNext()) {
				userDST = (UserDST) itColUserDST.next();

				if (compareZuluDates(userDST.getDstStartDateTime(), userDST.getDstEndDateTime(), zuluTime)) {
					localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), userDST.getAdjustMinutes());
					matchFound = true;
					break;
				}
			}
			// If match is not found
			if (matchFound == false) {
				itColUserDST = colUserDST.iterator();
				if (itColUserDST.hasNext()) {
					userDST = (UserDST) itColUserDST.next();
					localDate = CalendarUtil.getLocalDate(zuluTime, userDST.getGmtOffsetMinutes(), 0);
				}
			}
			return DateUtil.formatDate(localDate, strDateFormat);
		} else {
			return getLocalDateTimeString(zuluTime, strStation, strDateFormat);
		}
	}

	/**
	 * Formats specified date with specified format
	 * 
	 * @return String the formated date
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}

	/**
	 * Compare Zulu Dates
	 * 
	 * @param from
	 * @param to
	 * @param currentTime
	 * @return
	 */
	private static boolean compareZuluDates(Date from, Date to, Date currentTime) {

		if (from == null || to == null || currentTime == null) {
			return false;
		} else if (currentTime.after(from) && currentTime.before(to)) {
			return true;
		}
		return false;
	}

}