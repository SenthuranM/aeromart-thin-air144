package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InvFareTypeTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fareOndCode;
	private Integer fareType;
	private List<InvFareAllocTO> invFareAllocations;

	public String getFareOndCode() {
		return fareOndCode;
	}

	public void setFareOndCode(String fareOndCode) {
		this.fareOndCode = fareOndCode;
	}

	public Integer getFareType() {
		return fareType;
	}

	public void setFareType(Integer fareType) {
		this.fareType = fareType;
	}

	public List<InvFareAllocTO> getInvFareAllocations() {
		if (invFareAllocations == null) {
			invFareAllocations = new ArrayList<InvFareAllocTO>();
		}
		return invFareAllocations;
	}

	public void setInvFareAllocations(List<InvFareAllocTO> invFareAllocations) {
		this.invFareAllocations = invFareAllocations;
	}

	public void addInvFareAlloc(InvFareAllocTO invFareAlloc) {
		getInvFareAllocations().add(invFareAlloc);
	}

	public boolean hasInvFareAllocations() {
		return getInvFareAllocations().size() > 0;
	}

}
