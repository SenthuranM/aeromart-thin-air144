package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.FlightStopOverDisplayUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * FIXME This seems like a mix of FETO and DTO. Separate and cleanup the presentation specific logic to
 * HTML/CSS/Javascript level
 * 
 */
public class FlightInfoTO implements Comparable<FlightInfoTO>, Serializable {

	private static final long serialVersionUID = 1L;

	private static final String IMAGE_PATH_SPLITONE = "../images/LogoThumbnail";

	private static final String IMAGE_PATH_SPLITTWO = ".jpg";

	private String type;

	private String orignNDest;

	private String flightNo;

	private String airLine;

	private String departureDate;

	private String departureDateValue;

	private String departureDateZuluValue;

	private String departureText;

	private String departure;

	private String departureTime;

	private long departureDateLong;

	private long departureTimeZuluLong;

	private String arrivalText;

	private String arrivalDate;

	private String arrivalDateValue;

	private String arrivalDateZuluValue;

	private String arrival;

	private String arrivalTime;

	private String status;

	private String subStatus;

	private String displayStatus;

	private String paxCount;

	private String bkgClass;

	private String cabinClass;

	private String logicalCC;

	private String cosDes;

	private boolean isOpenReturnSegment;

	private String flightSegmentRefNumber;

	private String interLineGroupKey;

	private boolean isCancellable = false;

	private boolean isModifiable = false;

	private int alertFlag = 0;

	private String carrierImagePath;

	private String flightRefNumber;

	private boolean returnFlag;

	private String system;

	private String segmentShortCode;

	private long arrivalTimeLong;

	private long arrivalTimeZuluLong;

	private String flightId;

	private String duration;

	private String surfaceStation;

	private String departureDisplayDate;

	private boolean groundStationAddAllowed;

	private String pnrSegID;
	/* segment modification */
	private boolean isAllowModifyByDate;

	private boolean isAllowModifyByRoute;

	private boolean groundSegment;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String remarks;

	private String flightStopOverDuration;

	private String noOfStops;

	private String ticketValidTill;

	private int ondSequence;

	private String groundSegmentPnrSegId;

	private boolean isFlownSegment;

	private String departureTerminal;

	private String arrivalTerminal;

	private int waitListedPriority;

	private List<Integer> segmentWaitListedPriorities;

	private Integer flightSegId;

	private int subJourneyGroup = -1;

	// TODO -- refactor transfer this field
	private String baggageOndGroupId;

	private String csOcCarrierCode;

	private String filghtOperatingCarrier;

	private Integer segmentTypeId;

	private int segmentSequence;

	private int requestedOndSequence;

	public FlightInfoTO() {

	}

	public FlightInfoTO(FlightSegmentTO flightSegmentTO) {
		String segmentCode = flightSegmentTO.getSegmentCode();
		String depAirport = segmentCode.substring(0, 3);
		String arrAirport = segmentCode.substring(segmentCode.length() - 3);

		// Prepare date & time for departure and arrival
		Date depatureDateTime = flightSegmentTO.getDepartureDateTime();
		Date arrivalDateTime = flightSegmentTO.getArrivalDateTime();

		// Fill FlightInfoTO object
		setOrignNDest(segmentCode);
		setFlightNo(flightSegmentTO.getFlightNumber());
		setAirLine(flightSegmentTO.getOperatingAirline());

		Integer segId = flightSegmentTO.getFlightSegId();

		if (segId == null || "".equals(segId)) {
			segId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentTO.getFlightRefNumber());
		}

		setFlightSegmentRefNumber(segId + ""); // FIXME refactor
		setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
		setFlightId(flightSegmentTO.getFlightId() + "");

		setDepartureDate(DateUtil.formatDate(depatureDateTime, "EEE ddMMMyy"));
		setDepartureDateValue(DateUtil.formatDate(flightSegmentTO.getDepartureDateTime(), "yyMMddHHmm"));
		setDepartureDateZuluValue(DateUtil.formatDate(flightSegmentTO.getDepartureDateTimeZulu(), "yyMMddHHmm"));
		setDepartureDateLong(depatureDateTime.getTime());
		setDepartureTimeZuluLong(flightSegmentTO.getDepartureDateTimeZulu().getTime());
		setDepartureText("DEP"); // FIXME refactor
		setDeparture(depAirport);
		setDepartureTime(DateUtil.formatDate(depatureDateTime, "HH:mm"));

		setArrivalText("ARR"); // FIXME refactor
		setArrival(arrAirport);
		setArrivalDate(DateUtil.formatDate(arrivalDateTime, "EEE ddMMMyy"));
		setArrivalDateValue(DateUtil.formatDate(flightSegmentTO.getArrivalDateTime(), "yyMMddHHmm"));
		setArrivalDateZuluValue(DateUtil.formatDate(flightSegmentTO.getArrivalDateTimeZulu(), "yyMMddHHmm"));
		setArrivalDateLong(arrivalDateTime.getTime());
		setArrivalTime(DateUtil.formatDate(arrivalDateTime, "HH:mm"));
		setArrivalTimeZuluLong(flightSegmentTO.getArrivalDateTimeZulu().getTime());


		setCabinClass(flightSegmentTO.getCabinClassCode());
		setLogicalCC(flightSegmentTO.getLogicalCabinClassCode());
		setReturnFlag(flightSegmentTO.isReturnFlag());

		String fltDuration = flightSegmentTO.getFlightDuration();
		String fltModelDescription = flightSegmentTO.getFlightModelDescription();
		String fltModelNo = flightSegmentTO.getFlightModelNumber();
		String fltRemarks = flightSegmentTO.getRemarks();

		String fltDepartureTerminalName = flightSegmentTO.getDepartureTerminalName();
		String fltArrivalTerminalName = flightSegmentTO.getArrivalTerminalName();

		if (StringUtil.isNullOrEmpty(fltDepartureTerminalName)) {
			fltDepartureTerminalName = "-";
		}

		if (StringUtil.isNullOrEmpty(fltArrivalTerminalName)) {
			fltArrivalTerminalName = "-";
		}

		setDepartureTerminal(fltDepartureTerminalName);
		setArrivalTerminal(fltArrivalTerminalName);

		// FIXME
		if (StringUtil.isNullOrEmpty(fltDuration)) {
			fltDuration = "-";
		}

		if (StringUtil.isNullOrEmpty(fltModelDescription)) {
			fltModelDescription = "-";
		}

		if (StringUtil.isNullOrEmpty(fltModelNo)) {
			fltModelNo = "-";
		}

		if (StringUtil.isNullOrEmpty(fltRemarks)) {
			fltRemarks = "-";
		}

		setFlightDuration(fltDuration);
		setFlightModelDescription(fltModelDescription);
		setFlightModelNumber(fltModelNo);
		setRemarks(fltRemarks);
		setSubJourneyGroup(flightSegmentTO.getSubJourneyGroup());
		setRequestedOndSequence(flightSegmentTO.getRequestedOndSequence());
		// flightSegmentTO.getRequestedOndSequence()
		setNoOfStops(FlightStopOverDisplayUtil.getStopOverStations(segmentCode));
		setBaggageOndGroupId(flightSegmentTO.getBaggageONDGroupId());
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the orignNDest
	 */
	public String getOrignNDest() {
		return orignNDest;
	}

	/**
	 * @param orignNDest
	 *            the orignNDest to set
	 */
	public void setOrignNDest(String orignNDest) {
		this.orignNDest = orignNDest;
	}

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the airLine
	 */
	public String getAirLine() {
		return airLine;
	}

	/**
	 * @param airLine
	 *            the airLine to set
	 */
	public void setAirLine(String airLine) {
		this.airLine = airLine;
		setCarrierImagePath(airLine);
	}

	/**
	 * @return the departureDate
	 */
	public String getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the departureText
	 */
	public String getDepartureText() {
		return departureText;
	}

	/**
	 * @param departureText
	 *            the departureText to set
	 */
	public void setDepartureText(String departureText) {
		this.departureText = departureText;
	}

	/**
	 * @return the departure
	 */
	public String getDeparture() {
		return departure;
	}

	/**
	 * @param departure
	 *            the departure to set
	 */
	public void setDeparture(String departure) {
		this.departure = departure;
	}

	/**
	 * @return the departureTime
	 */
	public String getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime
	 *            the departureTime to set
	 */
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return the arrivalText
	 */
	public String getArrivalText() {
		return arrivalText;
	}

	/**
	 * @param arrivalText
	 *            the arrivalText to set
	 */
	public void setArrivalText(String arrivalText) {
		this.arrivalText = arrivalText;
	}

	/**
	 * @return the arrivalDate
	 */
	public String getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            the arrivalDate to set
	 */
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return the arrival
	 */
	public String getArrival() {
		return arrival;
	}

	/**
	 * @param arrival
	 *            the arrival to set
	 */
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	/**
	 * @return the arrivalTime
	 */
	public String getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param arrivalTime
	 *            the arrivalTime to set
	 */
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the paxCount
	 */
	public String getPaxCount() {
		return paxCount;
	}

	/**
	 * @param paxCount
	 *            the paxCount to set
	 */
	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}

	/**
	 * @return the bkgClass
	 */
	public String getBkgClass() {
		return bkgClass;
	}

	/**
	 * @param bkgClass
	 *            the bkgClass to set
	 */
	public void setBkgClass(String bkgClass) {
		this.bkgClass = bkgClass;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getLogicalCC() {
		return logicalCC;
	}

	public void setLogicalCC(String logicalCC) {
		this.logicalCC = logicalCC;
	}

	/**
	 * Compare departure date time using date's long value.
	 * 
	 * @param flightInfoTO
	 *            FlightInfoTO object to be compared with
	 * @return 0 if the argument Date is equal to this Date; -1 if this Date is before the passed Date; and 1 if this
	 *         Date is after the Date argument
	 */
	@Override
	public int compareTo(FlightInfoTO flightInfoTO) {
		if (this.departureDateLong > flightInfoTO.getDepartureDateLong()) {
			return 1; // After
		} else if (this.departureDateLong < flightInfoTO.getDepartureDateLong()) {
			return -1; // Before
		} else {
			return 0; // Same Date
		}
	}

	/**
	 * @return the isOpenReturnSegment
	 */
	public boolean isOpenReturnSegment() {
		return isOpenReturnSegment;
	}

	/**
	 * @param isOpenReturnSegment
	 *            the isOpenReturnSegment to set
	 */
	public void setOpenReturnSegment(boolean isOpenReturnSegment) {
		this.isOpenReturnSegment = isOpenReturnSegment;
	}

	/**
	 * @return the flightSegmentRefNumber
	 */
	public String getFlightSegmentRefNumber() {
		return flightSegmentRefNumber;
	}

	/**
	 * @param flightSegmentRefNumber
	 *            the flightSegmentRefNumber to set
	 */
	public void setFlightSegmentRefNumber(String flightSegmentRefNumber) {
		this.flightSegmentRefNumber = flightSegmentRefNumber;
	}

	/**
	 * @return the interLineGroupKey
	 */
	public String getInterLineGroupKey() {
		return interLineGroupKey;
	}

	/**
	 * @param interLineGroupKey
	 *            the interLineGroupKey to set
	 */
	public void setInterLineGroupKey(String interLineGroupKey) {
		this.interLineGroupKey = interLineGroupKey;
	}

	/**
	 * @return the departureDateLong
	 */
	public long getDepartureDateLong() {
		return departureDateLong;
	}

	/**
	 * @param departureDateLong
	 *            the departureDateLong to set
	 */
	public void setDepartureDateLong(long departureDateLong) {
		this.departureDateLong = departureDateLong;
	}

	public boolean isCancellable() {
		return isCancellable;
	}

	public void setCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}

	public boolean isModifiable() {
		return isModifiable;
	}

	public void setModifiable(boolean isModifiable) {
		this.isModifiable = isModifiable;
	}

	public int getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(int alertFlag) {
		this.alertFlag = alertFlag;
	}

	public String getCarrierImagePath() {
		return carrierImagePath;
	}

	public void setCarrierImagePath(String carrierCode) {
		this.carrierImagePath = IMAGE_PATH_SPLITONE + carrierCode + IMAGE_PATH_SPLITTWO;
	}

	/*
	 * FIXME there is a confusion with the way the things are converted to json by struts and our util class Remove this
	 * once it's resolved
	 */

	public void setIsModifiable(boolean isModifiable) {
		this.isModifiable = isModifiable;
	}

	public boolean isIsModifiable() {
		return isModifiable;
	}

	public void setIsCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}

	public boolean isIsCancellable() {
		return isCancellable;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getSegmentShortCode() {
		return segmentShortCode;
	}

	public void setSegmentShortCode(String segmentShortCode) {
		this.segmentShortCode = segmentShortCode;
	}

	public void setArrivalDateLong(long arrivalTimeLong) {
		this.arrivalTimeLong = arrivalTimeLong;
	}

	public long getArrivalDateLong() {
		return arrivalTimeLong;
	}

	public long getDepartureTimeZuluLong() {
		return departureTimeZuluLong;
	}

	public void setDepartureTimeZuluLong(long departureTimeZuluLong) {
		this.departureTimeZuluLong = departureTimeZuluLong;
	}

	public long getArrivalTimeZuluLong() {
		return arrivalTimeZuluLong;
	}

	public void setArrivalTimeZuluLong(long arrivalTimeZuluLong) {
		this.arrivalTimeZuluLong = arrivalTimeZuluLong;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getSurfaceStation() {
		return surfaceStation;
	}

	public void setSurfaceStation(String surfaceStation) {
		this.surfaceStation = surfaceStation;
	}

	public String getDepartureDisplayDate() {
		return departureDisplayDate;
	}

	public void setDepartureDisplayDate(String departureDisplayDate) {
		this.departureDisplayDate = departureDisplayDate;
	}

	@Override
	public Object clone() {
		FlightInfoTO inst = new FlightInfoTO();
		inst.type = this.type;
		inst.orignNDest = this.orignNDest;
		inst.flightNo = this.flightNo;
		inst.airLine = this.airLine;
		inst.departureDate = this.departureDate;
		inst.departureText = this.departureText;
		inst.departure = this.departure;
		inst.departureTime = this.departureTime;
		inst.departureDateLong = this.departureDateLong;
		inst.departureTimeZuluLong = this.departureTimeZuluLong;
		inst.arrivalText = this.arrivalText;
		inst.arrivalDate = this.arrivalDate;
		inst.arrival = this.arrival;
		inst.arrivalTime = this.arrivalTime;
		inst.status = this.status;
		inst.subStatus = this.subStatus;
		inst.paxCount = this.paxCount;
		inst.bkgClass = this.bkgClass;
		inst.cosDes = this.cosDes;
		inst.isOpenReturnSegment = this.isOpenReturnSegment;
		inst.flightSegmentRefNumber = this.flightSegmentRefNumber;
		inst.interLineGroupKey = this.interLineGroupKey;
		inst.isCancellable = this.isCancellable;
		inst.isModifiable = this.isModifiable;
		inst.alertFlag = this.alertFlag;
		inst.carrierImagePath = this.carrierImagePath;
		inst.flightRefNumber = this.flightRefNumber;
		inst.returnFlag = this.returnFlag;
		inst.system = this.system;
		inst.segmentShortCode = this.segmentShortCode;
		inst.arrivalTimeLong = this.arrivalTimeLong;
		inst.arrivalTimeZuluLong = this.arrivalTimeZuluLong;
		inst.flightId = this.flightId;
		inst.duration = this.duration;
		/* segment modification */
		inst.isAllowModifyByDate = this.isAllowModifyByDate;
		inst.isAllowModifyByRoute = this.isAllowModifyByRoute;

		inst.setGroundStationAddAllowed(this.isGroundStationAddAllowed());
		inst.setPnrSegID(this.getPnrSegID());
		inst.flightModelNumber = this.flightModelNumber;
		inst.flightModelDescription = this.flightModelDescription;
		inst.flightDuration = this.flightDuration;
		inst.remarks = this.remarks;
		inst.flightStopOverDuration = this.flightStopOverDuration;
		inst.noOfStops = this.noOfStops;
		inst.ondSequence = this.ondSequence;
		inst.baggageOndGroupId = this.baggageOndGroupId;

		return inst;
	}

	public boolean isGroundStationAddAllowed() {
		return groundStationAddAllowed;
	}

	public void setGroundStationAddAllowed(boolean groundStationAddAllowed) {
		this.groundStationAddAllowed = groundStationAddAllowed;
	}

	public String getPnrSegID() {
		return pnrSegID;
	}

	public void setPnrSegID(String pnrSegID) {
		this.pnrSegID = pnrSegID;
	}

	public boolean getIsAllowModifyByDate() {
		return isAllowModifyByDate;
	}

	public void setIsAllowModifyByDate(boolean isAllowModifyByDate) {
		this.isAllowModifyByDate = isAllowModifyByDate;
	}

	public boolean getIsAllowModifyByRoute() {
		return isAllowModifyByRoute;
	}

	public void setIsAllowModifyByRoute(boolean isAllowModifyByRoute) {
		this.isAllowModifyByRoute = isAllowModifyByRoute;
	}

	public boolean isGroundSegment() {
		return groundSegment;
	}

	public void setGroundSegment(boolean isGroundSegment) {
		this.groundSegment = isGroundSegment;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFlightStopOverDuration() {
		return flightStopOverDuration;
	}

	public void setFlightStopOverDuration(String flightStopOverDuration) {
		this.flightStopOverDuration = flightStopOverDuration;
	}

	public String getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(String noOfStops) {
		this.noOfStops = noOfStops;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public String getCosDes() {
		return cosDes;
	}

	public void setCosDes(String cosDes) {
		this.cosDes = cosDes;
	}

	public String getGroundSegmentPnrSegId() {
		return groundSegmentPnrSegId;
	}

	public void setGroundSegmentPnrSegId(String groundSegmentPnrSegId) {
		this.groundSegmentPnrSegId = groundSegmentPnrSegId;
	}

	public String getTicketValidTill() {
		return ticketValidTill;
	}

	public void setTicketValidTill(String ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public boolean isFlownSegment() {
		return isFlownSegment;
	}

	public void setFlownSegment(boolean isFlownSegment) {
		this.isFlownSegment = isFlownSegment;
	}

	public void setDepartureTerminal(String departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public String getDepartureTerminal() {
		return departureTerminal;
	}

	public void setArrivalTerminal(String arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	public String getArrivalTerminal() {
		return arrivalTerminal;
	}

	public int getWaitListedPriority() {
		return waitListedPriority;
	}

	public void setWaitListedPriority(int waitListedPriority) {
		this.waitListedPriority = waitListedPriority;
	}

	public List<Integer> getSegmentWaitListedPriorities() {
		return segmentWaitListedPriorities;
	}

	public void setSegmentWaitListedPriorities(List<Integer> segmentWaitListedPriorities) {
		this.segmentWaitListedPriorities = segmentWaitListedPriorities;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getDepartureDateValue() {
		return departureDateValue;
	}

	public void setDepartureDateValue(String departureDateValue) {
		this.departureDateValue = departureDateValue;
	}

	public String getArrivalDateValue() {
		return arrivalDateValue;
	}

	public void setArrivalDateValue(String arrivalDateValue) {
		this.arrivalDateValue = arrivalDateValue;
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;
	}

	public String getBaggageOndGroupId() {
		return baggageOndGroupId;
	}

	public void setBaggageOndGroupId(String baggageOndGroupId) {
		this.baggageOndGroupId = baggageOndGroupId;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getFilghtOperatingCarrier() {
		return filghtOperatingCarrier;
	}

	public void setFilghtOperatingCarrier(String filghtOperatingCarrier) {
		this.filghtOperatingCarrier = filghtOperatingCarrier;
	}

	public Integer getSegmentTypeId() {
		return segmentTypeId;
	}

	public void setSegmentTypeId(Integer segmentTypeId) {
		this.segmentTypeId = segmentTypeId;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public String getDepartureDateZuluValue() {
		return departureDateZuluValue;
	}

	public void setDepartureDateZuluValue(String departureDateZuluValue) {
		this.departureDateZuluValue = departureDateZuluValue;
	}

	public String getArrivalDateZuluValue() {
		return arrivalDateZuluValue;
	}

	public void setArrivalDateZuluValue(String arrivalDateZuluValue) {
		this.arrivalDateZuluValue = arrivalDateZuluValue;
	}

}