package com.isa.thinair.airproxy.api.utils;

import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;

public class AncillaryUtil {
	public static Collection<FlightBaggageDTO> injectBaggageONDGroupID(Collection<FlightBaggageDTO> flightBaggageDTOs,
			String ondGroupID) {
		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			flightBaggageDTO.setOndGroupId(ondGroupID);
		}
		return flightBaggageDTOs;
	}
}
