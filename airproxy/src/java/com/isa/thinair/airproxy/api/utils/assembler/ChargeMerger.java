package com.isa.thinair.airproxy.api.utils.assembler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Utility class to merge charges to form v2 compatible data from own airline data
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ChargeMerger {

	private Map<String, FeeTO> feeMap = new HashMap<String, FeeTO>();
	private Map<String, SurchargeTO> surchargeMap = new HashMap<String, SurchargeTO>();
	private Map<String, TaxTO> taxMap = new HashMap<String, TaxTO>();
	// private Map<String, ExternalChargeTO> outBoundExternalChargeMap = new HashMap<String, ExternalChargeTO>();
	// private Map<String, ExternalChargeTO> inBoundExternalChargeMap = new HashMap<String, ExternalChargeTO>();
	private List<Map<String, ExternalChargeTO>> ondExternalChargeMapList = new ArrayList<Map<String, ExternalChargeTO>>();

	private List<BaseFareTO> baseFareList = new ArrayList<BaseFareTO>();

	private Set<String> feePaxTypes = new HashSet<String>();
	private Set<String> taxPaxTypes = new HashSet<String>();
	private Set<String> surchargePaxTypes = new HashSet<String>();

	private BigDecimal feeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal surchargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal taxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal baseFareTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal outBoundExternalChargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal inBoundExternalChargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal priceTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal priceTotalWithInfant = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totalAgentCommission = AccelAeroCalculator.getDefaultBigDecimalZero();// for 2*AD + 1*CD + 1*INT

	private AgentCommissionDetails applicableAgentCommissions = null;

	/**
	 * Add collection of FeeTOs of given pax count of given paxtype
	 * 
	 * @param feeList
	 * @param paxType
	 * @param count
	 */
	public void addFees(List<FeeTO> feeList, String paxType, int count) {
		for (FeeTO fee : feeList) {
			fee = fee.clone();
			BigDecimal feeVal = AccelAeroCalculator.multiply(fee.getAmount(), count);
			feeTotal = AccelAeroCalculator.add(feeTotal, feeVal);
			fee.setAmount(feeVal);
			if (fee.isDefinedInLocal()) {
				fee.setLocalCurrencyAmount(AccelAeroCalculator.multiply(fee.getLocalCurrencyAmount(), count));
			}
			FeeTO temp = feeMap.get(fee.getFeeCode());
			feePaxTypes.add(paxType);
			if (temp == null) {
				feeMap.put(fee.getFeeCode(), fee);
			} else {
				temp.setAmount(AccelAeroCalculator.add(temp.getAmount(), fee.getAmount()));
				if (temp.isDefinedInLocal()) {
					temp.setLocalCurrencyAmount(AccelAeroCalculator.add(temp.getLocalCurrencyAmount(),
							fee.getLocalCurrencyAmount()));
				}
				temp.getApplicablePassengerTypeCode().clear();
				temp.getApplicablePassengerTypeCode().addAll(feePaxTypes);
			}
		}
	}

	/**
	 * Return merged collection of FeeTO
	 * 
	 * @return Collection<FeeTO>
	 */
	public Collection<FeeTO> getFees() {
		return feeMap.values();
	}

	/**
	 * Add collection of SurchargeTO of given pax count of given paxtype
	 * 
	 * @param surchargesList
	 * @param paxType
	 * @param count
	 */
	public void addSurcharges(List<SurchargeTO> surchargesList, String paxType, int count) {
		for (SurchargeTO surcharge : surchargesList) {
			surcharge = surcharge.clone();
			BigDecimal surchargeVal = AccelAeroCalculator.multiply(surcharge.getAmount(), count);
			surchargeTotal = AccelAeroCalculator.add(surchargeTotal, surchargeVal);
			surcharge.setAmount(surchargeVal);
			if (surcharge.isDefinedInLocal()) {
				surcharge.setLocalCurrencyAmount(AccelAeroCalculator.multiply(surcharge.getLocalCurrencyAmount(), count));
			}
			SurchargeTO temp = surchargeMap.get(surcharge.getSurchargeCode());
			surchargePaxTypes.add(paxType);
			if (temp == null) {
				surchargeMap.put(surcharge.getSurchargeCode(), surcharge);
			} else {
				temp.setAmount(AccelAeroCalculator.add(temp.getAmount(), surcharge.getAmount()));
				if (temp.isDefinedInLocal()) {
					temp.setLocalCurrencyAmount(AccelAeroCalculator.add(temp.getLocalCurrencyAmount(),
							surcharge.getLocalCurrencyAmount()));
				}
				temp.getApplicablePassengerTypeCode().clear();
				temp.getApplicablePassengerTypeCode().addAll(surchargePaxTypes);
			}
		}
	}

	/**
	 * Return merged collection of SurchargeTO for out bound External charges
	 * 
	 * @return Collection<ExternalChargeTO>
	 */
	// public Collection<ExternalChargeTO> getOutBoundExternalCharges() {
	// return outBoundExternalChargeMap.values();
	// }

	/**
	 * Return merged collection of SurchargeTO for in bound External charges
	 * 
	 * @return Collection<ExternalChargeTO>
	 */
	// public Collection<ExternalChargeTO> getInBoundExternalCharges() {
	// return inBoundExternalChargeMap.values();
	// }

	public List<ONDExternalChargeTO> getONDExternalCharges() {
		List<ONDExternalChargeTO> ondExternalCharges = new ArrayList<ONDExternalChargeTO>();

		int ondSequence = 0;
		for (Map<String, ExternalChargeTO> extChgMap : ondExternalChargeMapList) {
			ONDExternalChargeTO ondExtChg = new ONDExternalChargeTO();
			ondExtChg.setOndSequence(ondSequence);
			for (ExternalChargeTO extChg : extChgMap.values()) {
				ondExtChg.addExternalCharge(extChg);
				ondExtChg.setOndSequence(extChg.getOndSequence());
			}
			ondExternalCharges.add(ondExtChg);
			ondSequence++;
		}
		return ondExternalCharges;
	}

	/**
	 * Add collection of SurchargeTO (for external charges) of given pax count of given paxtype.
	 * 
	 * @param ondExternalCharges
	 * @param paxType
	 * @param count
	 */
	public void addExternalCharges(List<ONDExternalChargeTO> ondExternalCharges, String paxType, int count, boolean isInBound) {
		for (ONDExternalChargeTO ondExtChg : ondExternalCharges) {
			int ondSequence = ondExtChg.getOndSequence();
			if (ondExternalChargeMapList.size() == ondSequence) {
				ondExternalChargeMapList.add(new HashMap<String, ExternalChargeTO>());
			}
			for (ExternalChargeTO externalCharge : ondExtChg.getExternalCharges()) {
				externalCharge = externalCharge.clone();
				BigDecimal exChargeVal = AccelAeroCalculator.multiply(externalCharge.getAmount(), count);
				externalCharge.setAmount(exChargeVal);
				// if (!isInBound) {
				// outBoundExternalChargeTotal = AccelAeroCalculator.add(outBoundExternalChargeTotal, exChargeVal);
				Map<String, ExternalChargeTO> extChgMap = ondExternalChargeMapList.get(ondSequence);
				SurchargeTO temp = extChgMap.get(externalCharge.getSurchargeCode());
				if (temp == null) {
					extChgMap.put(externalCharge.getSurchargeCode(), externalCharge);
				} else {
					temp.setAmount(AccelAeroCalculator.add(temp.getAmount(), externalCharge.getAmount()));
				}
				// } else {
				// inBoundExternalChargeTotal = AccelAeroCalculator.add(inBoundExternalChargeTotal, exChargeVal);
				// SurchargeTO temp = inBoundExternalChargeMap.get(externalCharge.getSurchargeCode());
				// if (temp == null) {
				// inBoundExternalChargeMap.put(externalCharge.getSurchargeCode(), externalCharge);
				// } else {
				// temp.setAmount(AccelAeroCalculator.add(temp.getAmount(), externalCharge.getAmount()));
				// }
				// }
			}
		}
	}

	/**
	 * Return merged collection of SurchargeTO
	 * 
	 * @return Collection<SurchargeTO>
	 */
	public Collection<SurchargeTO> getSurcharges() {
		return surchargeMap.values();
	}

	/**
	 * Add collection of TaxTO of given pax count of given paxtype
	 * 
	 * @param taxList
	 * @param paxType
	 * @param count
	 */
	public void addTaxes(List<TaxTO> taxList, String paxType, int count) {
		for (TaxTO tax : taxList) {
			tax = tax.clone();
			BigDecimal taxVal = AccelAeroCalculator.multiply(tax.getAmount(), count);
			taxTotal = AccelAeroCalculator.add(taxTotal, taxVal);
			tax.setAmount(taxVal);
			if (tax.isDefinedInLocal()) {
				tax.setLocalCurrencyAmount(AccelAeroCalculator.multiply(tax.getLocalCurrencyAmount(), count));
			}
			TaxTO temp = taxMap.get(tax.getTaxCode());
			taxPaxTypes.add(paxType);
			if (temp == null) {
				taxMap.put(tax.getTaxCode(), tax);
			} else {
				temp.setAmount(AccelAeroCalculator.add(temp.getAmount(), tax.getAmount()));
				if (temp.isDefinedInLocal()) {
					temp.setLocalCurrencyAmount(AccelAeroCalculator.add(temp.getLocalCurrencyAmount(),
							tax.getLocalCurrencyAmount()));
				}
				temp.getApplicablePassengerTypeCode().clear();
				temp.getApplicablePassengerTypeCode().addAll(taxPaxTypes);
			}
		}
	}

	/**
	 * Return merged collection of TaxTO
	 * 
	 * @return Collection<TaxTO>
	 */
	public Collection<TaxTO> getTaxes() {
		return taxMap.values();
	}

	/**
	 * Add Collection of base fares of given count
	 * 
	 * @param baseList
	 * @param count
	 */
	public void addBaseFare(List<BaseFareTO> baseList, int count) {
		for (BaseFareTO base : baseList) {
			base = base.clone();
			BigDecimal baseAmt = AccelAeroCalculator.multiply(base.getAmount(), count);
			baseFareTotal = AccelAeroCalculator.add(baseFareTotal, baseAmt);
			base.setAmount(baseAmt);
			baseFareList.add(base);
		}
	}

	/**
	 * Return merged collection of BaseFareTO
	 * 
	 * @return Collection<BaseFareTO>
	 */
	public List<BaseFareTO> getBaseFares() {
		return baseFareList;
	}

	/**
	 * Add specified number of pax of given PerPaxPriceInfoTO while calculating total price,
	 * 
	 * @param pax
	 * @param count
	 */
	public void addPax(PerPaxPriceInfoTO pax, int count) {
		addFees(pax.getPassengerPrice().getFees(), pax.getPassengerType(), count);
		addSurcharges(pax.getPassengerPrice().getSurcharges(), pax.getPassengerType(), count);
		addTaxes(pax.getPassengerPrice().getTaxes(), pax.getPassengerType(), count);
		// addExternalCharges(pax.getPassengerPrice().getOutBoundExternalCharges(), pax.getPassengerType(), count,
		// false);
		addExternalCharges(pax.getPassengerPrice().getOndExternalCharges(), pax.getPassengerType(), count, true);

		// TODO check this condition for the accuracy --- seems smelly!!
		// if (pax.getPassengerType().equals(PaxTypeTO.ADULT)) {
		addBaseFare(pax.getPassengerPrice().getBaseFares(), count);
		addAgentCommissionInfo(pax, count);
		// }

		priceTotal = AccelAeroCalculator.add(priceTotal,
				AccelAeroCalculator.multiply(pax.getPassengerPrice().getTotalPrice(), count));
		priceTotalWithInfant = AccelAeroCalculator.add(priceTotalWithInfant,
				AccelAeroCalculator.multiply(pax.getPassengerPrice().getTotalPriceWithInfant(), count));

	}

	private void addAgentCommissionInfo(PerPaxPriceInfoTO pax, int count) {
		AgentCommissionDetails applicableCommissions = pax.getPassengerPrice().getApplicableAgentCommissions();
		if (applicableCommissions != null) {
			Set<Integer> fareIds = applicableCommissions.getCommissionApplicableFareIds();
			Set<Integer> excludedfareIds = applicableCommissions.getCommissionRemovalExcludedFareIds();
			if ((fareIds != null && !fareIds.isEmpty()) || (excludedfareIds != null && !excludedfareIds.isEmpty())) {
				if (applicableAgentCommissions == null) {
					applicableAgentCommissions = new AgentCommissionDetails();
				}
				applicableAgentCommissions.getCommissionApplicableFareIds().addAll(fareIds);
				applicableAgentCommissions.getCommissionRemovalExcludedFareIds().addAll(excludedfareIds);
			}
		}

		BigDecimal perPaxCommission = pax.getPassengerPrice().getPerPaxTotalAgentCommission();
		if (perPaxCommission != null && perPaxCommission.compareTo(BigDecimal.ZERO) > 0) {
			totalAgentCommission = AccelAeroCalculator.add(totalAgentCommission,
					AccelAeroCalculator.multiply(perPaxCommission, count));
		}

	}

	/**
	 * Return total base fare
	 * 
	 * @return
	 */
	public BigDecimal getTotalBaseFare() {
		return baseFareTotal;
	}

	/**
	 * Return total fee
	 * 
	 * @return
	 */
	public BigDecimal getTotalFee() {
		return feeTotal;
	}

	/**
	 * Return total price
	 * 
	 * @return
	 */
	public BigDecimal getTotalPrice() {
		return priceTotal;
	}

	/**
	 * Return total price with infant
	 * 
	 * @return
	 */
	public BigDecimal getTotalPriceWithInfant() {
		return priceTotalWithInfant;
	}

	/**
	 * Return total surcharges
	 * 
	 * @return
	 */
	public BigDecimal getTotalSurcharge() {
		return surchargeTotal;
	}

	/**
	 * Return total taxes
	 * 
	 * @return
	 */
	public BigDecimal getTotalTax() {
		return taxTotal;
	}

	public BigDecimal getTotalOutBoundExternalCharge() {
		return outBoundExternalChargeTotal;
	}

	public BigDecimal getTotalInBoundExternalCharge() {
		return inBoundExternalChargeTotal;
	}

	public BigDecimal getTotalAgentCommission() {
		return totalAgentCommission;
	}

	public AgentCommissionDetails getApplicableAgentCommissions() {
		return applicableAgentCommissions;
	}

}
