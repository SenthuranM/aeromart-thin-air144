package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.StringUtil;

/**
 * To keep track of available meals.
 * 
 * @author Abheek Das Gupta
 * @since 1.0
 */
public class LCCMealDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds the meal code */
	private String mealCode;

	/** Holds the meal description */
	private String mealDescription;

	/** Holds the meal charge */
	private BigDecimal mealCharge;

	/** Holds the name of the meal */
	private String mealName;

	/** Holds the available meal quantity */
	private int availableMeals;

	/** Holds the sold meal quantity */
	private int soldMeals;

	/** Holds the allocated meal quantity */
	private int allocatedMeals;

	private String mealImagePath;

	private String mealThumbnailImagePath;

	private String translatedMealTitle;

	private String translatedMealDescription;

	private int mealCategoryID;

	private String mealCategoryCode;

	private String totalPrice;

	private boolean alertAutoCancellation;

	private boolean isAnciOffer;

	private Integer offeredTemplateID;
	
	private String mealStatus;
	
	private String popularity;

	private int mealId;
	
	private boolean isCategoryRestricted;

	public int getMealId() {
		return mealId;
	}

	public void setMealId(int mealId) {
		this.mealId = mealId;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getMealDescription() {
		return mealDescription;
	}

	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}

	public BigDecimal getMealCharge() {
		return mealCharge;
	}

	public void setMealCharge(BigDecimal mealCharge) {
		this.mealCharge = mealCharge;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public int getAvailableMeals() {
		return availableMeals;
	}

	public void setAvailableMeals(int availableMeals) {
		this.availableMeals = availableMeals;
	}

	public int getSoldMeals() {
		return soldMeals;
	}

	public void setSoldMeals(int soldMeals) {
		this.soldMeals = soldMeals;
	}

	public int getAllocatedMeals() {
		return allocatedMeals;
	}

	public void setAllocatedMeals(int allocatedMeals) {
		this.allocatedMeals = allocatedMeals;
	}

	public String getMealImagePath() {
		return mealImagePath;
	}

	public void setMealImagePath(String mealImagePath) {
		this.mealImagePath = mealImagePath;
	}

	public String getTranslatedMealTitle() {
		String ucs = "";
		ucs = StringUtil.getUnicode(translatedMealTitle);
		if (ucs != null)
			ucs = ucs.replace("^", ",");
		return ucs;
	}

	public void setTranslatedMealTitle(String translatedMealTitle) {
		this.translatedMealTitle = translatedMealTitle;
	}

	public String getTranslatedMealDescription() {
		String ucs = "";
		ucs = StringUtil.getUnicode(translatedMealDescription);
		if (ucs != null)
			ucs = ucs.replace("^", ",");
		return ucs;
	}

	public void setTranslatedMealDescription(String translatedMealDescription) {
		this.translatedMealDescription = translatedMealDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCMealDTO [allocatedMeals=");
		builder.append(allocatedMeals);
		builder.append(", availableMeals=");
		builder.append(availableMeals);
		builder.append(", mealCharge=");
		builder.append(mealCharge);
		builder.append(", mealCode=");
		builder.append(mealCode);
		builder.append(", mealDescription=");
		builder.append(mealDescription);
		builder.append(", mealImagePath=");
		builder.append(mealImagePath);
		builder.append(", mealName=");
		builder.append(mealName);
		builder.append(", soldMeals=");
		builder.append(soldMeals);
		builder.append(", translatedMealTitle=");
		builder.append(translatedMealTitle);
		builder.append(", translatedMealDescription=");
		builder.append(translatedMealDescription);
		builder.append(", alertAutoCancellation=");
		builder.append(",mealStatus=");
		builder.append(mealStatus);
		builder.append(",isCategoryRestricted=");
		builder.append(isCategoryRestricted);
		builder.append(",popularity=");
		builder.append(popularity);
		builder.append(alertAutoCancellation);

		builder.append("]");
		return builder.toString();
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getMealCategoryID() {
		return mealCategoryID;
	}

	public void setMealCategoryID(int mealCategoryID) {
		this.mealCategoryID = mealCategoryID;
	}

	public String getMealThumbnailImagePath() {
		return mealThumbnailImagePath;
	}

	public void setMealThumbnailImagePath(String mealThumbnailImagePath) {
		this.mealThumbnailImagePath = mealThumbnailImagePath;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the offeredTemplateID
	 */
	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	/**
	 * @param offeredTemplateID
	 *            the offeredTemplateID to set
	 */
	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}

	public String getMealStatus() {
		return mealStatus;
	}

	public void setMealStatus(String mealStatus) {
		this.mealStatus = mealStatus;
	}

	/**
	 * @return the isCategoryRestricted
	 */
	public boolean isCategoryRestricted() {
		return isCategoryRestricted;
	}

	/**
	 * @param isCategoryRestricted the isCategoryRestricted to set
	 */
	public void setCategoryRestricted(boolean isCategoryRestricted) {
		this.isCategoryRestricted = isCategoryRestricted;
	}
	

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}
}
