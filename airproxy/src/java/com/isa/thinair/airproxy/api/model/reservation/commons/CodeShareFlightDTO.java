package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;

public class CodeShareFlightDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String flightNumber;
	
	private String bookingClass;
	
	private String codeShareFlightNumber;
	
	private String codeShareBookingClass;
	
	private Date departureDate;

	public String getFlightNumber() {
		return flightNumber;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public String getCodeShareFlightNumber() {
		return codeShareFlightNumber;
	}

	public String getCodeShareBookingClass() {
		return codeShareBookingClass;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public void setCodeShareFlightNumber(String codeShareFlightNumber) {
		this.codeShareFlightNumber = codeShareFlightNumber;
	}

	public void setCodeShareBookingClass(String codeShareBookingClass) {
		this.codeShareBookingClass = codeShareBookingClass;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}	

}
