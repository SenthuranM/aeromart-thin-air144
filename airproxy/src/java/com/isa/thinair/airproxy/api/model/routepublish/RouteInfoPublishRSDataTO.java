package com.isa.thinair.airproxy.api.model.routepublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Navod Ediriweera
 * @since Oct 21, 2009
 */
public class RouteInfoPublishRSDataTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<String> updatedRInfoIDs;
	private List<String> errors;

	public List<String> getUpdatedRInfoIDs() {
		if (updatedRInfoIDs == null) {
			updatedRInfoIDs = new ArrayList<String>();
		}
		return updatedRInfoIDs;
	}

	public void setUpdatedRInfoIDs(List<String> updatedRInfoIDs) {
		this.updatedRInfoIDs = updatedRInfoIDs;
	}

	public List<String> getErrors() {
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

}
