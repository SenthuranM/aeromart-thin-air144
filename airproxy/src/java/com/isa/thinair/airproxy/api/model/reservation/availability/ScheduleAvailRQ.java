package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.util.Date;

/**
 * @author Sandun Abeyweera
 */
public class ScheduleAvailRQ extends BaseAvailRQ {

	private String originLocation = null;
	private String destLocation = null;
	private boolean roundTrip = false;
	private Date startDateTime = null;
	private Date stopDateTime = null;

	private String arFrom = null;
	private String arTo = null;
	private String arSHJ = null;

	private String systemHub = null;

	private String defaultCarrierCode = null;

	private boolean isAAServiceRequest = false;

	public boolean isAAServiceRequest() {
		return isAAServiceRequest;
	}

	public void setAAServiceRequest(boolean isAAServiceRequest) {
		this.isAAServiceRequest = isAAServiceRequest;
	}

	/**
	 * @return the defaultCarrierCode
	 */
	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	/**
	 * @param defaultCarrierCode
	 *            the defaultCarrierCode to set
	 */
	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	public String getSystemHub() {
		return systemHub;
	}

	public void setSystemHub(String systemHub) {
		this.systemHub = systemHub;
	}

	/**
	 * @return the arFrom
	 */
	public String getArFrom() {
		return arFrom;
	}

	/**
	 * @param arFrom
	 *            the arFrom to set
	 */
	public void setArFrom(String arFrom) {
		this.arFrom = arFrom;
	}

	/**
	 * @return the arTo
	 */
	public String getArTo() {
		return arTo;
	}

	/**
	 * @param arTo
	 *            the arTo to set
	 */
	public void setArTo(String arTo) {
		this.arTo = arTo;
	}

	/**
	 * @return the arSHJ
	 */
	public String getArSHJ() {
		return arSHJ;
	}

	/**
	 * @param arSHJ
	 *            the arSHJ to set
	 */
	public void setArSHJ(String arSHJ) {
		this.arSHJ = arSHJ;
	}

	/**
	 * @return the originLocation
	 */
	public String getOriginLocation() {
		return originLocation;
	}

	/**
	 * @param originLocation
	 *            the originLocation to set
	 */
	public void setOriginLocation(String originLocation) {
		this.originLocation = originLocation;
	}

	/**
	 * @return the destLocation
	 */
	public String getDestLocation() {
		return destLocation;
	}

	/**
	 * @param destLocation
	 *            the destLocation to set
	 */
	public void setDestLocation(String destLocation) {
		this.destLocation = destLocation;
	}

	/**
	 * @return the roundTrip
	 */
	public boolean isRoundTrip() {
		return roundTrip;
	}

	/**
	 * @param roundTrip
	 *            the roundTrip to set
	 */
	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	/**
	 * @return the startDateTime
	 */
	public Date getStartDateTime() {
		return startDateTime;
	}

	/**
	 * @param startDateTime
	 *            the startDateTime to set
	 */
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	/**
	 * @return the stopDateTime
	 */
	public Date getStopDateTime() {
		return stopDateTime;
	}

	/**
	 * @param stopDateTime
	 *            the stopDateTime to set
	 */
	public void setStopDateTime(Date stopDateTime) {
		this.stopDateTime = stopDateTime;
	}

}
