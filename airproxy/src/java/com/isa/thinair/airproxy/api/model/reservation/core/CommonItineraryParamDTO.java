package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.commons.api.constants.ApplicationEngine;

public class CommonItineraryParamDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String itineraryLanguage;

	private boolean includePaxFinancials;

	private boolean includePaymentDetails;

	private boolean includeTicketCharges;

	private boolean includeTermsAndConditions;

	private boolean includeBaggageAllowance;

	private boolean includePaxContactDetails;

	private boolean includeStationContactDetails;

	private String station;

	private Map<String, String> airportMap;

	private ApplicationEngine appIndicator;

	private String selectedPaxDetails;
	
	private boolean amountMaskingForcePriviledge;

	private String operationType;

	private String viewMode;

	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}

	public boolean isIncludePaxFinancials() {
		return includePaxFinancials;
	}

	public void setIncludePaxFinancials(boolean includePaxFinancials) {
		this.includePaxFinancials = includePaxFinancials;
	}

	public boolean isIncludePaymentDetails() {
		return includePaymentDetails;
	}

	public void setIncludePaymentDetails(boolean includePaymentDetails) {
		this.includePaymentDetails = includePaymentDetails;
	}

	public boolean isIncludeTicketCharges() {
		return includeTicketCharges;
	}

	public void setIncludeTicketCharges(boolean includeTicketCharges) {
		this.includeTicketCharges = includeTicketCharges;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public Map<String, String> getAirportMap() {
		return airportMap;
	}

	public void setAirportMap(Map<String, String> airportMap) {
		this.airportMap = airportMap;
	}

	public void setAppIndicator(ApplicationEngine appIndicator) {
		this.appIndicator = appIndicator;
	}

	public ApplicationEngine getAppIndicator() {
		return appIndicator;
	}

	public String getSelectedPaxDetails() {
		return selectedPaxDetails;
	}

	public void setSelectedPaxDetails(String selectedPaxDetails) {
		this.selectedPaxDetails = selectedPaxDetails;
	}

	public boolean isIncludeTermsAndConditions() {
		return includeTermsAndConditions;
	}

	public void setIncludeTermsAndConditions(boolean includeTermsAndConditions) {
		this.includeTermsAndConditions = includeTermsAndConditions;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getViewMode() {
		return viewMode;
	}

	public void setViewMode(String viewMode) {
		this.viewMode = viewMode;
	}

	public boolean isIncludeBaggageAllowance() {
		return includeBaggageAllowance;
	}

	public void setIncludeBaggageAllowance(boolean includeBaggageAllowance) {
		this.includeBaggageAllowance = includeBaggageAllowance;
	}

	public boolean isIncludePaxContactDetails() {
		return includePaxContactDetails;
	}

	public void setIncludePaxContactDetails(boolean includePaxContactDetails) {
		this.includePaxContactDetails = includePaxContactDetails;
	}

	public boolean isIncludeStationContactDetails() {
		return includeStationContactDetails;
	}

	public void setIncludeStationContactDetails(boolean includeStationContactDetails) {
		this.includeStationContactDetails = includeStationContactDetails;
	}

	public boolean isAmountMaskingForcePriviledge() {
		return amountMaskingForcePriviledge;
	}

	public void setAmountMaskingForcePriviledge(boolean amountMaskingForcePriviledge) {
		this.amountMaskingForcePriviledge = amountMaskingForcePriviledge;
	}

	
	
}
