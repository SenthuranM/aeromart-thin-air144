package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;

public class BasicChargeInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The charge adjustment type id under which this charge adjustment is done. */
	private Integer adjustmentTypeId;

	private BigDecimal amount;

	public Integer getAdjustmentTypeId() {
		return adjustmentTypeId;
	}

	public void setAdjustmentTypeId(Integer adjustmentTypeId) {
		this.adjustmentTypeId = adjustmentTypeId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
