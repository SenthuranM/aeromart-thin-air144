package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.util.Date;

public class ReservationSegmentTO implements Comparable<ReservationSegmentTO> {
	@Deprecated
	private int segmentSeq;
	private int flightSegId;
	@Deprecated
	private Integer ondGroupId;
	@Deprecated
	private Integer returnOndGroupId;
	@Deprecated
	private String originatorRef;
	private int ondFareType;
	private Date departureDateZulu;
	private Integer selectedBundledFarePeriodId;
	private String codeShareFlightNumber;
	private String codeShareBookingClass;
	private String csOcCarrierCode;
	private String csOcFlightNumber;

	public ReservationSegmentTO() {
	}

	public ReservationSegmentTO(int flightSegId, int ondFareType, Date departureDateZulu, Integer selectedBundledFarePeriodId,
			String codeShareFlightNumber, String codeShareBookingClass, String csOcCarrierCode, String csOcFlightNumber) {
		this.flightSegId = flightSegId;
		this.ondFareType = ondFareType;
		this.departureDateZulu = departureDateZulu;
		this.selectedBundledFarePeriodId = selectedBundledFarePeriodId;
		this.codeShareBookingClass = codeShareBookingClass;
		this.codeShareFlightNumber = codeShareFlightNumber;
		this.csOcCarrierCode = csOcCarrierCode;
		this.csOcFlightNumber = csOcFlightNumber;
	}

	@Deprecated
	public ReservationSegmentTO(int flightSegId, Integer ondGroupId, Integer returnOndGroupId, Integer selectedBundledFarePeriodId) {
		setFlightSegId(flightSegId);
		setOndGroupId(ondGroupId);
		setReturnOndGroupId(returnOndGroupId);
		setSelectedBundledFarePeriodId(selectedBundledFarePeriodId);
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public int getOndFareType() {
		return ondFareType;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Integer getOndGroupId() {
		return ondGroupId;
	}

	public void setOndGroupId(Integer ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

	public int getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(int segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public Integer getReturnOndGroupId() {
		return returnOndGroupId;
	}

	public void setReturnOndGroupId(Integer returnOndGroupId) {
		this.returnOndGroupId = returnOndGroupId;
	}

	public String getOriginatorRef() {
		return originatorRef;
	}

	public void setOriginatorRef(String originatorRef) {
		this.originatorRef = originatorRef;
	}

	public Integer getSelectedBundledFarePeriodId() {
		return selectedBundledFarePeriodId;
	}

	public void setSelectedBundledFarePeriodId(Integer selectedBundledFarePeriodId) {
		this.selectedBundledFarePeriodId = selectedBundledFarePeriodId;
	}

	public String getCodeShareFlightNumber() {
		return codeShareFlightNumber;
	}

	public String getCodeShareBookingClass() {
		return codeShareBookingClass;
	}

	public void setCodeShareFlightNumber(String codeShareFlightNumber) {
		this.codeShareFlightNumber = codeShareFlightNumber;
	}

	public void setCodeShareBookingClass(String codeShareBookingClass) {
		this.codeShareBookingClass = codeShareBookingClass;
	}

	@Override
	public int compareTo(ReservationSegmentTO in) {
		return this.getDepartureDateZulu().compareTo(in.getDepartureDateZulu());
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}

}
