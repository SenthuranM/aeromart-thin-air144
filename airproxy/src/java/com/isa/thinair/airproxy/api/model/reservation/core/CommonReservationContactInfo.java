/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 * 
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

/**
 * To keep track of Reservation Contact information.
 * 
 * @author Nilindra Fernando
 */
/**
 * @author aalva
 * 
 */
public class CommonReservationContactInfo implements Serializable {

	private static final long serialVersionUID = 123213264151L;

	/** Holds the customer id */
	private Integer customerId;

	/** Holds the customer title */
	private String title;

	/** Holds the customer first name */
	private String firstName;

	/** Holds the customer last name */
	private String lastName;

	/** Holds the customer phone number */
	private String phoneNo;

	/** Holds the customer mobile number */
	private String mobileNo;

	/** Holds the customer fax number */
	private String fax;

	/** Holds the customer email address */
	private String email;

	/** Holds the customer nationality code */
	private Integer nationalityCode;

	/** Holds the customer nationality -- use for display */
	private String nationality;

	/** Holds the customer code */
	private String countryCode;

	/** Holds Zip Code */
	private String zipCode;

	/** Holds street address field 1 */
	private String streetAddress1;

	/** Holds street address field 2 */
	private String streetAddress2;

	/** Holds city */
	private String city;

	/** Holds state */
	private String state;

	private String userNotes;
	
	private String userNoteType;

	/** Holds preferred language */
	private String preferredLanguage;

	/** Holds Tax Registration Number */
	private String taxRegNo;
	
	/*
	 * Emergency contact information
	 */
	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneNo;

	private String emgnEmail;
	
	
	/** Toggling promotional email check-box */
	
	private Boolean sendPromoEmail;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo
	 *            the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nationalityCode
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            the nationalityCode to set
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the streetAddress1
	 */
	public String getStreetAddress1() {
		return streetAddress1;
	}

	/**
	 * @param streetAddress1
	 *            the streetAddress1 to set
	 */
	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	/**
	 * @return the streetAddress2
	 */
	public String getStreetAddress2() {
		return streetAddress2;
	}

	/**
	 * @param streetAddress2
	 *            the streetAddress2 to set
	 */
	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return Returns the customerId.
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @return the emgnTitle
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnPhoneNo
	 */
	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	/**
	 * @param emgnPhoneNo
	 *            the emgnPhoneNo to set
	 */
	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	/**
	 * @return the emgnEmail
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}
    
	/**
	 * @return the userNoteType
	 */
	public String getUserNoteType() {
		return userNoteType;
	}
    
	/**
	 * @param userNoteType
	 *            the userNoteType to set
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public String getTaxRegNo() {
		return taxRegNo;
	}

	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}

	public Boolean getSendPromoEmail() {
		if (sendPromoEmail == null) {
			sendPromoEmail = false;
		}
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}
	
}
