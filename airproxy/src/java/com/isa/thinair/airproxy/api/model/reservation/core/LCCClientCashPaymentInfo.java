/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold cash payment data transfer information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientCashPaymentInfo implements Serializable, LCCClientPaymentInfo {

	private static final long serialVersionUID = 1L;

	/** Hold total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String totalAmountCurrencyCode;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private String agentCode;

	private String agentName;

	private Integer paxSequence;

	private Date txnDateTime;

	private String carrierCode;

	private String lccUniqueTnxId;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private boolean includeExternalCharges;

	private String payReference;

	private Integer actualPaymentMethod;

	private String originalPayReference;

	private String recieptNumber;

	private String paymentMethod;

	private String carrierVisePayments;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private String dummyPayment;

	private boolean offlinePayment = false;
	
	private boolean nonRefundable = false;	

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public Date getTxnDateTime() {
		return this.txnDateTime;
	}

	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	@Override
	public LCCClientCashPaymentInfo clone() {
		LCCClientCashPaymentInfo lccClientCashPaymentInfo = new LCCClientCashPaymentInfo();
		lccClientCashPaymentInfo.setTotalAmount(this.getTotalAmount());
		lccClientCashPaymentInfo.setTotalAmountCurrencyCode(this.getTotalAmountCurrencyCode());
		lccClientCashPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) this.getPayCurrencyDTO().clone());
		lccClientCashPaymentInfo.setPaxSequence(this.getPaxSequence());
		lccClientCashPaymentInfo.setLccUniqueTnxId(this.getLccUniqueTnxId());
		lccClientCashPaymentInfo.setPaymentTnxReference(this.getPaymentTnxReference());
		lccClientCashPaymentInfo.setOriginalPaymentUID(this.getOriginalPaymentUID());
		lccClientCashPaymentInfo.setPaymentTnxDateTime(this.getPaymentTnxDateTime());

		return lccClientCashPaymentInfo;
	}

	/**
	 * @return the totalAmountCurrencyCode
	 */
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	/**
	 * @param totalAmountCurrencyCode
	 *            the totalAmountCurrencyCode to set
	 */
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientCashPaymentInfo [carrierCode=");
		builder.append(carrierCode);
		builder.append(", lccUniqueTnxId=");
		builder.append(lccUniqueTnxId);
		builder.append(", paxSequence=");
		builder.append(paxSequence);
		builder.append(", payCurrencyDTO=");
		builder.append(payCurrencyDTO);
		builder.append(", totalAmount=");
		builder.append(totalAmount);
		builder.append(", totalAmountCurrencyCode=");
		builder.append(totalAmountCurrencyCode);
		builder.append(", txnDateTime=");
		builder.append(txnDateTime);
		builder.append(", paymentTnxReference=");
		builder.append(paymentTnxReference);
		builder.append(", originalPaymentUID=");
		builder.append(originalPaymentUID);
		builder.append(", paymentTnxDateTime=");
		builder.append(paymentTnxDateTime);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;

	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;

	}

	@Override
	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	@Override
	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public String getPayReference() {
		return payReference;
	}

	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	public String getOriginalPayReference() {
		return originalPayReference;
	}

	public Integer getActualPaymentMethod() {

		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {

		this.actualPaymentMethod = actualPaymentMethod;
	}

	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	public String getRecieptNumber() {
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	/**
	 * @return the carrierVisePayments
	 */
	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	/**
	 * @param carrierVisePayments
	 *            the carrierVisePayments to set
	 */
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the paymentMethod
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 *            the paymentMethod to set
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getDummyPayment() {
		return dummyPayment;
	}

	public void setDummyPayment(String dummyPayment) {
		this.dummyPayment = dummyPayment;
	}

	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	public String getOriginalPaymentUID() {
		return originalPaymentUID;
	}

	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	public boolean isOfflinePayment() {
		return this.offlinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	@Override
	public boolean isNonRefundable() {		
		return this.nonRefundable;
	}
	
	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;		
	}
}