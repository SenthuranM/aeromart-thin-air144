package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public class RedeemCalculateRQ implements Serializable {

	private static final long serialVersionUID = -1379957412579860540L;

	private String memberAccountID;
	private BigDecimal redeemRequestAmount;
	private Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges;
	private FlightPriceRQ flightPriceRQ;
	private Map<String, List<String>> carrierWiseFlightRPH;
	private FareSegChargeTO fareSegChargeTo;
	private boolean isFlexiQuote;
	private Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount;
	private SYSTEM system;
	private String txnIdntifier;
	private boolean issueRewardIds;
	private boolean isPayForOHD;
	private String pnr;
	private DiscountRQ discountRQ;
	private double remainingPoint;
	private String memberExternalId;
	private String memberEnrollingCarrier;

	public String getMemberAccountID() {
		return memberAccountID;
	}

	public void setMemberAccountID(String memberAccountID) {
		this.memberAccountID = memberAccountID;
	}

	public BigDecimal getRedeemRequestAmount() {
		return redeemRequestAmount;
	}

	public void setRedeemRequestAmount(BigDecimal redeemRequestAmount) {
		this.redeemRequestAmount = redeemRequestAmount;
	}

	public Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> getPaxCarrierExternalCharges() {
		return paxCarrierExternalCharges;
	}

	public void
			setPaxCarrierExternalCharges(Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges) {
		this.paxCarrierExternalCharges = paxCarrierExternalCharges;
	}

	public FlightPriceRQ getFlightPriceRQ() {
		return flightPriceRQ;
	}

	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	public FareSegChargeTO getFareSegChargeTo() {
		return fareSegChargeTo;
	}

	public void setFareSegChargeTo(FareSegChargeTO fareSegChargeTo) {
		this.fareSegChargeTo = fareSegChargeTo;
	}

	public boolean isFlexiQuote() {
		return isFlexiQuote;
	}

	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public String getTxnIdntifier() {
		return txnIdntifier;
	}

	public void setTxnIdntifier(String txnIdntifier) {
		this.txnIdntifier = txnIdntifier;
	}

	public boolean isIssueRewardIds() {
		return issueRewardIds;
	}

	public void setIssueRewardIds(boolean issueRewardIds) {
		this.issueRewardIds = issueRewardIds;
	}

	public Map<Integer, Map<String, Map<String, BigDecimal>>> getPaxCarrierProductDueAmount() {
		return paxCarrierProductDueAmount;
	}

	public void setPaxCarrierProductDueAmount(Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount) {
		this.paxCarrierProductDueAmount = paxCarrierProductDueAmount;
	}

	public boolean isPayForOHD() {
		return isPayForOHD;
	}

	public void setPayForOHD(boolean isPayForOHD) {
		this.isPayForOHD = isPayForOHD;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<String, List<String>> getCarrierWiseFlightRPH() {
		return carrierWiseFlightRPH;
	}

	public void setCarrierWiseFlightRPH(Map<String, List<String>> carrierWiseFlightRPH) {
		this.carrierWiseFlightRPH = carrierWiseFlightRPH;
	}

	public DiscountRQ getDiscountRQ() {
		return discountRQ;
	}

	public void setDiscountRQ(DiscountRQ discountRQ) {
		this.discountRQ = discountRQ;
	}

	public double getRemainingPoint() {
		return remainingPoint;
	}

	public void setRemainingPoint(double remainingPoint) {
		this.remainingPoint = remainingPoint;
	}

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}

	public String getMemberEnrollingCarrier() {
		return memberEnrollingCarrier;
	}

	public void setMemberEnrollingCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier = memberEnrollingCarrier;
	}
}
