package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;

public class CommonOfflinePaymentInfo implements Serializable, LCCClientPaymentInfo {

	private String carrierCode;

	private BigDecimal totalAmount;
	
	private String totalAmountCurrencyCode;

	private PayCurrencyDTO payCurrencyDTO;

	private boolean includeExternalCharges;

	private Integer actualPaymentMethod;

	private String payReference;

	private String paymentTnxReference;

	private String remarks;	
	
	private String description;
	
	private BigDecimal payCurrencyAmount;

	private IPGIdentificationParamsDTO ipgIdentificationParamsDTO;
	
	private Date paymentTnxDateTime;

	private String lccUniqueTnxId;

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;

	}

	@Override
	public Integer getPaxSequence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPaxSequence(Integer paxSequence) {
		// TODO Auto-generated method stub

	}

	@Override
	public Date getTxnDateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTxnDateTime(Date date) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCarrierCode() {
		return carrierCode;
	}

	@Override
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	@Override
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	@Override
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	@Override
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	@Override
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;

	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	@Override
	public String getPayReference() {
		return payReference;
	}

	@Override
	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	@Override
	public Integer getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	@Override
	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	@Override
	public String getOriginalPayReference() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setOriginalPayReference(String originalPayReference) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRecieptNumber() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRecieptNumber(String recieptNumber) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getCarrierVisePayments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCarrierVisePayments(String carrierVisePayments) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	@Override
	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;

	}

	@Override
	public String getOriginalPaymentUID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setOriginalPaymentUID(String originalPaymentUID) {
		// TODO Auto-generated method stub

	}

	@Override
	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	@Override
	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;

	}

	@Override
	public boolean isOfflinePayment() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setOfflinePayment(boolean isOfflinePayment) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isNonRefundable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		// TODO Auto-generated method stub

	}

	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
