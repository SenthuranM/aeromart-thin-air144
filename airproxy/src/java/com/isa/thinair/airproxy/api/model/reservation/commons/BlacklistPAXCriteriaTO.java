package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;

public class BlacklistPAXCriteriaTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long blacklistPAXCriteriaID;

	private String fName;
	
	private String lName;
	
	private String paxFullName;
	
	private String paxStatus;
	
	private String passportNo;
	
	private String nationality;
	
	private Date dateOfBirth;
	
	private String remarks;
	
	private String blacklistType;
	
	private Date effectiveFrom;
	
	private Date effectiveTo;
	
	private String removedReason;
	
	private Date validUntil;
	
	private Long version;


	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}
	
	public String getPaxFullName() {
		return paxFullName;
	}

	public void setPaxFullName(String paxFullName) {
		this.paxFullName = paxFullName;
	}
	
	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBlacklistType() {
		return blacklistType;
	}

	public void setBlacklistType(String blacklistType) {
		this.blacklistType = blacklistType;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public Long getBlacklistPAXCriteriaID() {
		return blacklistPAXCriteriaID;
	}

	public void setBlacklistPAXCriteriaID(Long blacklistPAXCriteriaID) {
		this.blacklistPAXCriteriaID = blacklistPAXCriteriaID;
	}
	
	public String getRemovedReason() {
		return removedReason;
	}
	
	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}
	
}
