package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class ModificationParamRQInfo implements Serializable {

	private static final long serialVersionUID = 3979912643697738346L;

	private AppIndicatorEnum appIndicator;

	private Boolean isRegisteredUser;

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public Boolean getIsRegisteredUser() {
		return isRegisteredUser;
	}

	public void setIsRegisteredUser(Boolean isRegisteredUser) {
		this.isRegisteredUser = isRegisteredUser;
	}
}
