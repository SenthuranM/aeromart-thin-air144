package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class LCCAnciModificationRequest implements Serializable {

	private static final long serialVersionUID = 2238328835567450861L;

	private CommonAncillaryModifyAssembler commonAncillaryModifyAssembler;

	private TrackInfoDTO trackInfoDTO;

	private UserPrincipal userPrincipal;

	private boolean enableFraudCheck;

	private CommonReservationContactInfo contactInfo;

	private boolean interlinePaymentAllowed;

	private Map<String, Set<String>> flightRefWiseSelectedMeals;

	private boolean autoCnxEnabled;

	public boolean isInterlinePaymentAllowed() {
		return interlinePaymentAllowed;
	}

	public void setInterlinePaymentAllowed(boolean interlinePaymentAllowed) {
		this.interlinePaymentAllowed = interlinePaymentAllowed;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public CommonAncillaryModifyAssembler getCommonAncillaryModifyAssembler() {
		return commonAncillaryModifyAssembler;
	}

	public void setCommonAncillaryModifyAssembler(CommonAncillaryModifyAssembler commonAncillaryModifyAssembler) {
		this.commonAncillaryModifyAssembler = commonAncillaryModifyAssembler;
	}

	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public boolean isEnableFraudCheck() {
		return enableFraudCheck;
	}

	public void setEnableFraudCheck(boolean enableFraudCheck) {
		this.enableFraudCheck = enableFraudCheck;
	}

	public Map<String, Set<String>> getFlightRefWiseSelectedMeals() {
		return flightRefWiseSelectedMeals;
	}

	public void setFlightRefWiseSelectedMeals(Map<String, Set<String>> flightRefWiseSelectedMeals) {
		this.flightRefWiseSelectedMeals = flightRefWiseSelectedMeals;
	}

	public boolean isAutoCnxEnabled() {
		return autoCnxEnabled;
	}

	public void setAutoCnxEnabled(boolean autoCnxEnabled) {
		this.autoCnxEnabled = autoCnxEnabled;
	}
}
