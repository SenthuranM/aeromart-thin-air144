/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * LCCClientSegmentAssembler is the main assembling utility for the Modifying Segments
 * 
 * @author Nilindra Fernando
 * @since 5:07 PM 11/5/2009
 */
public class LCCClientSegmentAssembler implements Serializable {

	private static final long serialVersionUID = 9171191800609547337L;

	/* Holds segment objects map */
	private Map<String, LCCClientReservationSegment> segmentsMap;

	/* Holds the Release Time Stamp which is unique for all passengers */
	private Date pnrZuluReleaseTimeStamp;

	/* Holds the Lcc transaction identifier */
	private String lccTransactionIdentifier;

	/* Holds passenger payments map */
	private Map<Integer, LCCClientPaymentAssembler> passengerPaymentsMap;

	/* Holds the external charges per pax */
	private Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap;

	private String ownerAgent;

	/* Price Quote Request */
	private FlightPriceRQ flightPriceRQ;

	/* Used to rebuild the OndFareDTO rather than re-quoting */
	private FareSegChargeTO selectedFareSegChargeTO;

	private CommonReservationContactInfo contactInfo;
	/* Hold Tempory Payment Map */
	private Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap;

	private Map externalChargesMap;

	private List<LCCClientReservationInsurance> resInsurances;

	private boolean isFlexiSelected;

	private boolean useOtherCarrierPaxCredit;

	private DiscountedFareDetails discountedFareDetails;

	private AgentCommissionDetails applicableAgentCommissions;

	/* Holds the last currency code */
	private String lastCurrencyCode;

	private Date lastFareQuotedDate;

	private boolean autoCancellationEnabled;

	private boolean hasBufferTimeAutoCnxPrivilege;

	private BigDecimal serviceTaxRatio = AccelAeroCalculator.getDefaultBigDecimalZero();

	public LCCClientSegmentAssembler() {
		setSegmentsMap(new HashMap<String, LCCClientReservationSegment>());
		setPassengerPaymentsMap(new HashMap<Integer, LCCClientPaymentAssembler>());
		setResInsurances(new ArrayList<LCCClientReservationInsurance>());
	}

	/**
	 * Adds the passenger payments
	 * 
	 * @param paxSequence
	 * @param lccClientPaymentAssembler
	 */
	public void addPassengerPayments(Integer paxSequence, LCCClientPaymentAssembler lccClientPaymentAssembler) {
		getPassengerPaymentsMap().put(paxSequence, lccClientPaymentAssembler);
	}

	/**
	 * Add outgoing segment
	 * 
	 * @param segmentSeq
	 * @param flightSegRefNumber
	 * @param flightNo
	 * @param segmentCode
	 * @param cabinClassCode
	 * @param logicalCabinClass
	 * @param departureDate
	 * @param arrivalDate
	 * @param subStationShortName
	 * @param departureDateZulu
	 * @param arrivalDateZulu
	 * @param carrierCode
	 * @param baggageONDGroupId
	 * @param anciOfferFlightSegmentRefNumber
	 * @param routeRefNumber
	 *            TODO
	 */
	public void addOutgoingSegment(int segmentSeq, String flightSegRefNumber, String flightNo, String segmentCode,
			String cabinClassCode, String logicalCabinClass, Date departureDate, Date arrivalDate, String subStationShortName,
			Date departureDateZulu, Date arrivalDateZulu, String carrierCode, String baggageONDGroupId,
			String anciOfferFlightSegmentRefNumber, String routeRefNumber) {
		LCCClientReservationSegment pnrSeg = new LCCClientReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		pnrSeg.setFlightSegmentRefNumber(flightSegRefNumber);
		pnrSeg.setRouteRefNumber(routeRefNumber);

		pnrSeg.setFlightNo(flightNo);
		pnrSeg.setSegmentCode(segmentCode);
		pnrSeg.setCabinClassCode(cabinClassCode);
		pnrSeg.setLogicalCabinClass(logicalCabinClass);
		pnrSeg.setDepartureDate(departureDate);
		pnrSeg.setDepartureDateZulu(departureDateZulu);
		pnrSeg.setArrivalDate(arrivalDate);
		pnrSeg.setArrivalDateZulu(arrivalDateZulu);
		pnrSeg.setSubStationShortName(subStationShortName);
		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		pnrSeg.setCarrierCode(carrierCode);
		pnrSeg.setBaggageONDGroupId(baggageONDGroupId);
		pnrSeg.setAnciOfferFlightSegmentRefNumber(anciOfferFlightSegmentRefNumber);

		getSegmentsMap().put(flightSegRefNumber, pnrSeg);
	}

	/**
	 * Add Return segment
	 * 
	 * @param segmentSeq
	 * @param flightSegRefNumber
	 * @param flightNo
	 * @param segmentCode
	 * @param cabinClassCode
	 * @param logicalCabinClass
	 * @param departureDate
	 * @param arrivalDate
	 * @param subStationShortName
	 * @param departureDateZulu
	 * @param arrivalDateZulu
	 * @param carrierCode
	 * @param baggageONDGroupId
	 * @param bookingFlightSegmentRefNumber
	 *            TODO
	 * @param routeRefNumber
	 *            TODO
	 */
	public void addReturnSegment(int segmentSeq, String flightSegRefNumber, String flightNo, String segmentCode,
			String cabinClassCode, String logicalCabinClass, Date departureDate, Date arrivalDate, String subStationShortName,
			Date departureDateZulu, Date arrivalDateZulu, String carrierCode, String baggageONDGroupId,
			String bookingFlightSegmentRefNumber, String routeRefNumber) {
		LCCClientReservationSegment pnrSeg = new LCCClientReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE);
		pnrSeg.setFlightSegmentRefNumber(flightSegRefNumber);
		pnrSeg.setRouteRefNumber(routeRefNumber);

		pnrSeg.setFlightNo(flightNo);
		pnrSeg.setSegmentCode(segmentCode);
		pnrSeg.setCabinClassCode(cabinClassCode);
		pnrSeg.setLogicalCabinClass(logicalCabinClass);
		pnrSeg.setDepartureDate(departureDate);
		pnrSeg.setDepartureDateZulu(departureDateZulu);
		pnrSeg.setArrivalDate(arrivalDate);
		pnrSeg.setArrivalDateZulu(arrivalDateZulu);
		pnrSeg.setSubStationShortName(subStationShortName);
		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		pnrSeg.setCarrierCode(carrierCode);
		pnrSeg.setBaggageONDGroupId(baggageONDGroupId);

		pnrSeg.setBookingFlightSegmentRefNumber(bookingFlightSegmentRefNumber);

		getSegmentsMap().put(flightSegRefNumber, pnrSeg);
	}

	/**
	 * @return the segmentsMap
	 */
	public Map<String, LCCClientReservationSegment> getSegmentsMap() {
		return segmentsMap;
	}

	/**
	 * @param segmentsMap
	 *            the segmentsMap to set
	 */
	public void setSegmentsMap(Map<String, LCCClientReservationSegment> segmentsMap) {
		this.segmentsMap = segmentsMap;
	}

	/**
	 * @return the pnrZuluReleaseTimeStamp
	 */
	public Date getPnrZuluReleaseTimeStamp() {
		return pnrZuluReleaseTimeStamp;
	}

	/**
	 * @param pnrZuluReleaseTimeStamp
	 *            the pnrZuluReleaseTimeStamp to set
	 */
	public void setPnrZuluReleaseTimeStamp(Date pnrZuluReleaseTimeStamp) {
		this.pnrZuluReleaseTimeStamp = pnrZuluReleaseTimeStamp;
	}

	/**
	 * @return the lccTransactionIdentifier
	 */
	public String getLccTransactionIdentifier() {
		return lccTransactionIdentifier;
	}

	/**
	 * @param lccTransactionIdentifier
	 *            the lccTransactionIdentifier to set
	 */
	public void setLccTransactionIdentifier(String lccTransactionIdentifier) {
		this.lccTransactionIdentifier = lccTransactionIdentifier;
	}

	/*
	 * @return the passengerPaymentsMap
	 */
	public Map<Integer, LCCClientPaymentAssembler> getPassengerPaymentsMap() {
		return passengerPaymentsMap;
	}

	/**
	 * @param passengerPaymentsMap
	 *            the passengerPaymentsMap to set
	 */
	public void setPassengerPaymentsMap(Map<Integer, LCCClientPaymentAssembler> passengerPaymentsMap) {
		this.passengerPaymentsMap = passengerPaymentsMap;
	}

	/**
	 * @return the ownerAgent
	 */
	public String getOwnerAgent() {
		return ownerAgent;
	}

	/**
	 * @param ownerAgent
	 *            the ownerAgent to set
	 */
	public void setOwnerAgent(String ownerAgent) {
		this.ownerAgent = ownerAgent;
	}

	/**
	 * @return the contactInfo
	 */
	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public FlightPriceRQ getFlightPriceRQ() {
		return flightPriceRQ;
	}

	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	public Map<Integer, CommonCreditCardPaymentInfo> getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	public void setTemporyPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public Map getExternalChargesMap() {
		return externalChargesMap;
	}

	public void setExternalChargesMap(Map externalChargesMap) {
		this.externalChargesMap = externalChargesMap;
	}

	public List<LCCClientReservationInsurance> getResInsurances() {
		return resInsurances;
	}

	public void setResInsurances(List<LCCClientReservationInsurance> resInsurances) {
		this.resInsurances = resInsurances;
	}

	public void addResInsurance(LCCClientReservationInsurance resInsurance) {
		this.getResInsurances().add(resInsurance);
	}

	/**
	 * @return the selectedFareSegChargeTO
	 */
	public FareSegChargeTO getSelectedFareSegChargeTO() {
		return selectedFareSegChargeTO;
	}

	/**
	 * @param selectedFareSegChargeTO
	 *            the selectedFareSegChargeTO to set
	 */
	public void setSelectedFareSegChargeTO(FareSegChargeTO selectedFareSegChargeTO) {
		this.selectedFareSegChargeTO = selectedFareSegChargeTO;
	}

	/**
	 * @return the isFlexiSelected
	 */
	@Deprecated
	public boolean isFlexiSelected() {
		return isFlexiSelected;
	}

	/**
	 * @param isFlexiSelected
	 *            the isFlexiSelected to set
	 */
	@Deprecated
	public void setFlexiSelected(boolean isFlexiSelected) {
		this.isFlexiSelected = isFlexiSelected;
	}

	public Map<Integer, List<LCCClientExternalChgDTO>> getPassengerExtChargeMap() {
		if (passengerExtChargeMap == null) {
			passengerExtChargeMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		}
		return passengerExtChargeMap;
	}

	public void setPassengerExtChargeMap(Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap) {
		this.passengerExtChargeMap = passengerExtChargeMap;
	}

	/**
	 * @return the useOtherCarrierPaxCredit
	 */
	public boolean isUseOtherCarrierPaxCredit() {
		return useOtherCarrierPaxCredit;
	}

	/**
	 * @param useOtherCarrierPaxCredit
	 *            the useOtherCarrierPaxCredit to set
	 */
	public void setUseOtherCarrierPaxCredit(boolean useOtherCarrierPaxCredit) {
		this.useOtherCarrierPaxCredit = useOtherCarrierPaxCredit;
	}

	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;
	}

	public AgentCommissionDetails getApplicableAgentCommissions() {
		return applicableAgentCommissions;
	}

	public void setApplicableAgentCommissions(AgentCommissionDetails applicableAgentCommissions) {
		this.applicableAgentCommissions = applicableAgentCommissions;
	}

	public float getFareDiscountPercentage() {
		float perc = 0;
		if (getDiscountedFareDetails() != null) {
			perc = getDiscountedFareDetails().getFarePercentage();
		}
		return perc;
	}

	public String getLastCurrencyCode() {
		return lastCurrencyCode;
	}

	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.lastCurrencyCode = lastCurrencyCode;
	}

	/**
	 * This is only applicable in own airline modification at the moment.. so don't use for any other purpose!
	 * 
	 * @param lastFareQuotedDate
	 */
	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	/**
	 * This is only applicable in own airline modification at the moment.. so don't use for any other purpose!
	 * 
	 */
	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	/**
	 * @return the autoCancellationEnabled
	 */
	public boolean isAutoCancellationEnabled() {
		return autoCancellationEnabled;
	}

	/**
	 * @param autoCancellationEnabled
	 *            the autoCancellationEnabled to set
	 */
	public void setAutoCancellationEnabled(boolean autoCancellationEnabled) {
		this.autoCancellationEnabled = autoCancellationEnabled;
	}

	/**
	 * @return the hasBufferTimeAutoCnxPrivilege
	 */
	public boolean isHasBufferTimeAutoCnxPrivilege() {
		return hasBufferTimeAutoCnxPrivilege;
	}

	/**
	 * @param hasBufferTimeAutoCnxPrivilege
	 *            the hasBufferTimeAutoCnxPrivilege to set
	 */
	public void setHasBufferTimeAutoCnxPrivilege(boolean hasBufferTimeAutoCnxPrivilege) {
		this.hasBufferTimeAutoCnxPrivilege = hasBufferTimeAutoCnxPrivilege;
	}

	public BigDecimal getServiceTaxRatio() {
		return serviceTaxRatio;
	}

	public void setServiceTaxRatio(BigDecimal serviceTaxRatio) {
		this.serviceTaxRatio = serviceTaxRatio;
	}
}
