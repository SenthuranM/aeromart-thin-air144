package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class LCCAncillaryAvailabilityInDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private SYSTEM queryingSystem;

	public static class LCCAncillaryType implements Serializable {

		private static final long serialVersionUID = 1L;
		public static final String SEAT_MAP = "SEAT_MAP";
		public static final String MEALS = "MEALS";
		public static final String SSR = "SSR";
		public static final String HALA = "HALA";
		public static final String INSURANCE = "INSURANCE";
		/**
		 * Baggage Ancillary Const.
		 */
		public static final String BAGGAGE = "BAGGAGES";
		public static final String AIRPORT_SERVICE = "AIRPORT_SERVICE";
		public static final String AIRPORT_TRANSFER = "AIRPORT_TRANSFER";

		/**
		 * Automatic ancillary Const.
		 */
		public static final String AUTOMATIC_CHECKIN = "AUTOMATIC_CHECKIN";
		public static final String FLEXI = "FLEXI";

		private String ancillaryType;

		public LCCAncillaryType(String ancillaryType) {
			this.ancillaryType = ancillaryType;
		}

		public String getAncillaryType() {
			return ancillaryType;
		}

		@Override
		public String toString() {
			StringBuilder toStringBuilder = new StringBuilder("LCCAncillaryType [");
			toStringBuilder.append("ancillaryType=").append(ancillaryType);
			toStringBuilder.append("]");
			return toStringBuilder.toString();
		}
	};

	private List<FlightSegmentTO> flightDetails;
	private List<LCCAncillaryType> ancillaryTypes;
	private String transactionIdentifier;
	private String insuranceOrigin;
	private boolean onHold;

	private AppIndicatorEnum appIndicator;
	private boolean modifyAncillary;

	private Map<String, Integer> airportServiceCountMap;
	private Map<String, Set<String>> flightRefWiseSelectedMeals;
	private Map<String, Set<String>> flightRefWiseSelectedSSR;
	private List<FlightSegmentTO> flightSegONDBaggage;
	private boolean isRequote;
	private LCCReservationBaggageSummaryTo baggageSummaryTo;
	private String departureSegmentCode;
	private boolean atLeastOneSegmentHavingCSOCFlight = false;
	private boolean isAddEditAnci = false;
	private boolean isEnableModifySSRWithinBuffer = false;

	public List<FlightSegmentTO> getFlightDetails() {
		return flightDetails;
	}

	public void setFlightDetails(List<FlightSegmentTO> flightDetails) {
		this.flightDetails = flightDetails;
	}

	public void addAllFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		if (this.flightDetails == null) {
			this.flightDetails = new ArrayList<FlightSegmentTO>();
		}
		this.flightDetails.addAll(flightSegmentTOs);
	}

	public List<LCCAncillaryType> getAncillaryTypes() {
		if (this.ancillaryTypes == null) {
			this.ancillaryTypes = new ArrayList<LCCAncillaryType>();
		}
		return ancillaryTypes;
	}

	public void setAncillaryTypes(List<LCCAncillaryType> ancillaryTypes) {
		this.ancillaryTypes = ancillaryTypes;
	}

	public void addAncillaryType(String ancillaryType) {
		getAncillaryTypes().add(new LCCAncillaryType(ancillaryType));
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public String getInsuranceOrigin() {
		return insuranceOrigin;
	}

	public void setInsuranceOrigin(String insuranceOrigin) {
		this.insuranceOrigin = insuranceOrigin;
	}

	public boolean isOnHold() {
		return onHold;
	}

	public void setOnHold(boolean onHold) {
		this.onHold = onHold;
	}

	/**
	 * Return the system, from which the reservation availability was searched for this system
	 * 
	 * @return system
	 */
	public SYSTEM getQueryingSystem() {
		return queryingSystem;
	}

	/**
	 * Set the system from which the reservation availability was served
	 * 
	 * @param queryingSystem
	 *            the queryingSystem to set
	 */
	public void setQueryingSystem(SYSTEM queryingSystem) {
		this.queryingSystem = queryingSystem;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	/**
	 * @return the airportServiceCountMap
	 */
	public Map<String, Integer> getAirportServiceCountMap() {
		if (airportServiceCountMap == null) {
			airportServiceCountMap = new HashMap<String, Integer>();
		}
		return airportServiceCountMap;
	}

	/**
	 * @param airportServiceCountMap
	 *            the airportServiceCountMap to set
	 */
	public void setAirportServiceCountMap(Map<String, Integer> airportServiceCountMap) {
		this.airportServiceCountMap = airportServiceCountMap;
	}

	/**
	 * @return the flightRefWiseSelectedMeals
	 */
	public Map<String, Set<String>> getFlightRefWiseSelectedMeals() {
		return flightRefWiseSelectedMeals;
	}

	/**
	 * @param flightRefWiseSelectedMeals
	 *            the flightRefWiseSelectedMeals to set
	 */
	public void setFlightRefWiseSelectedMeals(Map<String, Set<String>> flightRefWiseSelectedMeals) {
		this.flightRefWiseSelectedMeals = flightRefWiseSelectedMeals;
	}

	/**
	 * @return the flightRefWiseSelectedSSR
	 */
	public Map<String, Set<String>> getFlightRefWiseSelectedSSR() {
		return flightRefWiseSelectedSSR;
	}

	/**
	 * @param flightRefWiseSelectedSSR
	 *            the flightRefWiseSelectedSSR to set
	 */
	public void setFlightRefWiseSelectedSSR(Map<String, Set<String>> flightRefWiseSelectedSSR) {
		this.flightRefWiseSelectedSSR = flightRefWiseSelectedSSR;
	}

	/**
	 * @return the flightSegONDBaggage
	 */
	public List<FlightSegmentTO> getFlightSegONDBaggage() {
		return flightSegONDBaggage;
	}

	/**
	 * @param flightSegONDBaggage
	 *            the flightSegONDBaggage to set
	 */
	public void setFlightSegONDBaggage(List<FlightSegmentTO> flightSegONDBaggage) {
		this.flightSegONDBaggage = flightSegONDBaggage;
	}

	public LCCReservationBaggageSummaryTo getBaggageSummaryTo() {
		return baggageSummaryTo;
	}

	public void setBaggageSummaryTo(LCCReservationBaggageSummaryTo baggageSummaryTo) {
		this.baggageSummaryTo = baggageSummaryTo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCAncillaryAvailabilityInDTO [ancillaryTypes=");
		builder.append(ancillaryTypes);
		builder.append(", flightDetails=");
		builder.append(flightDetails);
		builder.append(", insuranceOrigin=");
		builder.append(insuranceOrigin);
		builder.append(", onHold=");
		builder.append(onHold);
		builder.append(", transactionIdentifier=");
		builder.append(transactionIdentifier);
		builder.append(", queryingSystem=" + queryingSystem);
		builder.append("]");
		return builder.toString();
	}

	public boolean isRequote() {
		return isRequote;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	public String getDepartureSegmentCode() {
		return departureSegmentCode;
	}

	public void setDepartureSegmentCode(String departureSegmentCode) {
		this.departureSegmentCode = departureSegmentCode;
	}

	public boolean isAtLeastOneSegmentHavingCSOCFlight() {
		return atLeastOneSegmentHavingCSOCFlight;
	}

	public void setAtLeastOneSegmentHavingCSOCFlight(boolean atLeastOneSegmentHavingCSOCFlight) {
		this.atLeastOneSegmentHavingCSOCFlight = atLeastOneSegmentHavingCSOCFlight;
	}

	public boolean isAddEditAnci() {
		return isAddEditAnci;
	}

	public void setAddEditAnci(boolean isAddEditAnci) {
		this.isAddEditAnci = isAddEditAnci;
	}

	public boolean isEnableModifySSRWithinBuffer() {
		return isEnableModifySSRWithinBuffer;
	}

	public void setEnableModifySSRWithinBuffer(boolean isEnableModifySSRWithinBuffer) {
		this.isEnableModifySSRWithinBuffer = isEnableModifySSRWithinBuffer;
	}
}