/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.dto.RefundInfoTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * LCCClientPaymentAssembler is the main assembling utility for the payments
 * 
 * @author Nilindra Fernando
 */
public class LCCClientPaymentAssembler implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds types of PaymentInfo Objects */
	private Collection<LCCClientPaymentInfo> payments;

	/** Holds the total payment amount */
	private BigDecimal totalPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds whether or not the external charges are consumed or not */
	private boolean isExternalChargesConsumed = false;

	/** Holds whether a payment exist or not */
	private boolean paymentExist = false;

	/** Holds per passenger external charges */
	private Collection<LCCClientExternalChgDTO> perPaxExternalCharges;

	/** Holds Passenger type */
	private String paxType;

	/**
	 * Construct Reservation Object
	 */
	public LCCClientPaymentAssembler() {
		this.payments = new ArrayList<LCCClientPaymentInfo>();
		this.perPaxExternalCharges = new ArrayList<LCCClientExternalChgDTO>();
	}

	/**
	 * Captures the external charges
	 * 
	 * @param colLCCClientExternalChgDTO
	 */

	public void addExternalCharges(Collection<ExternalChgDTO> colExternalChgDTO) {
		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			for (ExternalChgDTO externalChgDTO : colExternalChgDTO) {
				this.perPaxExternalCharges.add(new LCCClientExternalChgDTO(externalChgDTO));
			}
		}
	}

	// to skip adding cc charge
	public void addExternalChargesWithoutCCCharge(Collection<ExternalChgDTO> colExternalChgDTO) {
		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			for (ExternalChgDTO externalChgDTO : colExternalChgDTO) {
				if (externalChgDTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.CREDIT_CARD)
						|| externalChgDTO.getExternalChargesEnum().equals(EXTERNAL_CHARGES.JN_OTHER)) {

				}
				else {
					this.perPaxExternalCharges.add(new LCCClientExternalChgDTO(externalChgDTO));
				}
			}
		}
	}

	public void addExternalCharges(LCCClientExternalChgDTO lccClientExternalChgDTO) {
		this.perPaxExternalCharges.add(lccClientExternalChgDTO);
	}

	public void addAllExternalCharges(Collection<LCCClientExternalChgDTO> lccClientExternalChgDTOs) {
		for (LCCClientExternalChgDTO clientExternalChgDTO : lccClientExternalChgDTOs) {
			this.perPaxExternalCharges.add(clientExternalChgDTO);
		}
	}

	/**
	 * Add agent credit information
	 * 
	 * @param agentCode
	 * @param amount
	 * @param payCurrencyDTO
	 * @param actualPaymentMethod
	 * @param refundInfo
	 *            TODO
	 */
	public LCCClientPaymentInfo
			addAgentCreditPayment(String agentCode, BigDecimal amount, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
					String payReference, Integer actualPaymentMethod, String paymentMethod, RefundInfoTO refundInfo) {
		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		LCCClientOnAccountPaymentInfo lccOnAccountPaymentInfo = new LCCClientOnAccountPaymentInfo();
		lccOnAccountPaymentInfo.setAgentCode(agentCode);
		// If you need to enable other carrier agent payments set the carrier code correctly
		lccOnAccountPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		lccOnAccountPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		lccOnAccountPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lccOnAccountPaymentInfo.setTxnDateTime(paymentTimestamp);

		lccOnAccountPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		if (paymentMethod != null && paymentMethod.equals(Agent.PAYMENT_MODE_ONACCOUNT)) {
			lccOnAccountPaymentInfo.setRemarks("Mode of Payment :ONACC (" + agentCode + ")");
		} else if (paymentMethod != null && paymentMethod.equals(Agent.PAYMENT_MODE_BSP)) {
			lccOnAccountPaymentInfo.setRemarks("Mode of Payment :BSP (" + agentCode + ")");
		}
		lccOnAccountPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccOnAccountPaymentInfo.setPayReference(payReference);
		lccOnAccountPaymentInfo.setActualPaymentMethod(actualPaymentMethod);

		lccOnAccountPaymentInfo.setPaymentMethod(paymentMethod);

		if (refundInfo != null) {
			lccOnAccountPaymentInfo.setPaymentTnxReference(refundInfo.getOriginalPaymentTnxId());
			lccOnAccountPaymentInfo.setOriginalPaymentUID(refundInfo.getOriginalPaymentLccUniqueTnxId());
			lccOnAccountPaymentInfo.setPaymentTnxDateTime(refundInfo.getPaymentdate());
			lccOnAccountPaymentInfo.setCarrierVisePayments(refundInfo.getCarrierVisePayments());
		}

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lccOnAccountPaymentInfo);

		return lccOnAccountPaymentInfo;
	}

	public void mergePayment(LCCClientPaymentInfo originalPayment, BigDecimal amount, PayCurrencyDTO payCurrencyDTO)
			throws ModuleException {

		PayCurrencyDTO originalPayCurrency = originalPayment.getPayCurrencyDTO();
		PayCurrencyDTO mergingPayCurrency = getNullSafePayCurrencyDTO(payCurrencyDTO);
		if (!originalPayCurrency.getPayCurrencyCode().equals(mergingPayCurrency.getPayCurrencyCode())) {
			throw new ModuleException("Currency Mismatch"); // TODO -- exc doe
		}

		originalPayment.setTotalAmount(originalPayment.getTotalAmount().add(amount));

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);

	}

	/**
	 * Add Cash Payment
	 * 
	 * @param amount
	 * @param payCurrencyDTO
	 * @param paymentTimestamp
	 * @param payReference
	 * @param actualPaymentMethod
	 * @param remarks
	 * @param refundInfo
	 *            TODO
	 */
	public void addCashPayment(BigDecimal amount, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String payReference,
			Integer actualPaymentMethod, String remarks, RefundInfoTO refundInfo) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		LCCClientCashPaymentInfo lccCashPaymentInfo = new LCCClientCashPaymentInfo();
		lccCashPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		// Ideally other carrier cash payments are not possible
		lccCashPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		// Payment currency for cash payment is always base currency
		lccCashPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lccCashPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));

		lccCashPaymentInfo.setTxnDateTime(paymentTimestamp);
		if (remarks == null) {
			lccCashPaymentInfo.setRemarks("Mode of Payment : CASH");
		} else {
			lccCashPaymentInfo.setRemarks(remarks);
		}
		lccCashPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccCashPaymentInfo.setPayReference(payReference);
		lccCashPaymentInfo.setActualPaymentMethod(actualPaymentMethod);

		if (refundInfo != null) {
			lccCashPaymentInfo.setPaymentTnxReference(refundInfo.getOriginalPaymentTnxId());
			lccCashPaymentInfo.setOriginalPaymentUID(refundInfo.getOriginalPaymentLccUniqueTnxId());
			lccCashPaymentInfo.setPaymentTnxDateTime(refundInfo.getPaymentdate());
			lccCashPaymentInfo.setCarrierVisePayments(refundInfo.getCarrierVisePayments());
		}

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lccCashPaymentInfo);
	}

	/**
	 * Note - we need to remove pnr & isUtilizedFullAmount Add PaxCredit Payment PAx credit is always in marketing
	 * carrier base currency
	 * 
	 * @param carrierCode
	 *            carrier which the credit is belong to
	 * @param amount
	 * @param payCurrencyDTO
	 *            TODO
	 * @param pnr
	 *            pnr
	 * @param agentCode
	 */
	public void addPaxCreditPayment(String carrierCode, Integer debitPaxRefNo, BigDecimal amount, Date paymentTimestamp,
			BigDecimal residingCarrierAmount, PayCurrencyDTO payCurrencyDTO, Date expiryDate, Integer paxSequence,
			String lccUniqueTnxId, String pnr, String paxCreditId) {

		LCCClientPaxCreditPaymentInfo lccPaxCreditPaymentInfo = new LCCClientPaxCreditPaymentInfo();
		// May be other carrier pax credit. Backend will take care for other carrier pax credits
		lccPaxCreditPaymentInfo.setCarrierCode(carrierCode);
		lccPaxCreditPaymentInfo.setTotalAmount(amount);
		lccPaxCreditPaymentInfo.setDebitPaxRefNo(debitPaxRefNo);
		lccPaxCreditPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lccPaxCreditPaymentInfo.setTxnDateTime(paymentTimestamp);
		lccPaxCreditPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		lccPaxCreditPaymentInfo.setIncludeExternalCharges(false);
		lccPaxCreditPaymentInfo.setResidingCarrierAmount(residingCarrierAmount);
		lccPaxCreditPaymentInfo.setExpiryDate(expiryDate);
		lccPaxCreditPaymentInfo.setPaxSequence(paxSequence);
		lccPaxCreditPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccPaxCreditPaymentInfo.setResidingPnr(pnr);
		lccPaxCreditPaymentInfo.setPaxCreditId(paxCreditId);
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);

		this.payments.add(lccPaxCreditPaymentInfo);
	}

	/**
	 * Track Payment Exist or not
	 * 
	 */
	private void trackPaymentExistOrNot() {
		if (!isPaymentExist()) {
			setPaymentExist(true);
		}
	}

	/**
	 * Add card payment information
	 * 
	 * @param cardType
	 * @param eDate
	 * @param cardNumber
	 *            Could be full card number or only last 4 digits
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param ipgIdentificationParamsDTO
	 * @param payCurrencyDTO
	 * @param payReference
	 * @param actualPaymentMethod
	 * @param userInputDTO
	 *            TODO
	 * @param oldCCRecordId
	 */
	public void addInternalCardPayment(int cardType, String eDate, String cardNumber, String name, String address,
			String securityCode, BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String cardHoldersName, String payReference, Integer actualPaymentMethod, String authorizationId,
			UserInputDTO userInputDTO) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = getCardPaymentInfo(cardType, eDate, cardNumber, name,
				address, securityCode, appIndicatorEnum, tnxModeEnum, ipgIdentificationParamsDTO, payCurrencyDTO,
				paymentTimestamp, cardHoldersName, totalPayAmountWithServiceCharges, authorizationId);
		// If you need to enable other carrier credit payments set the carrier code correctly.
		// Please note you will have to remove db constraints in t_res_pcd table
		lccClientCreditCardPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		lccClientCreditCardPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccClientCreditCardPaymentInfo.setPayReference(payReference);
		lccClientCreditCardPaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		lccClientCreditCardPaymentInfo.setUserInputDTO(userInputDTO);

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lccClientCreditCardPaymentInfo);
	}
	
	public void addOfflineCardPayment(BigDecimal amount, TnxModeEnum tnxModeEnum,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String payReference, Integer actualPaymentMethod,
			PayCurrencyDTO payCurrencyDTO) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonOfflinePaymentInfo offlinePaymentInfo = new CommonOfflinePaymentInfo();
		offlinePaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		offlinePaymentInfo.setPayReference(payReference);
		offlinePaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		offlinePaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		offlinePaymentInfo.setPaymentTnxReference(payReference);
		offlinePaymentInfo.setPayCurrencyDTO(payCurrencyDTO);

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);
		offlinePaymentInfo.setTotalAmount(totalPayAmount);
		this.payments.add(offlinePaymentInfo);
	}

	/**
	 * Add Internal card payment
	 * 
	 * @param cardType
	 * @param eDate
	 * @param cardNumber
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param ipgIdentificationParamsDTO
	 * @param payCurrencyDTO
	 * @param paymentTimestamp
	 * @param cardHoldersName
	 * @param payReference
	 * @param actualPaymentMethod
	 */
	public void addInternalCardPayment(int cardType, String eDate, String cardNumber, String name, String address,
			String securityCode, BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String cardHoldersName, String payReference, Integer actualPaymentMethod,
			LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo, String lccUniqueKey, String authorizationId) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		// handle negative pay amounts in anci modification flow
		if (totalPayAmountWithServiceCharges.compareTo(BigDecimal.ZERO) < 0) {
			totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		// handle negative pay amounts in anci modification flow
		if (totalPayAmountWithServiceCharges.compareTo(BigDecimal.ZERO) < 0) {
			totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = getCardPaymentInfo(cardType, eDate, cardNumber, name,
				address, securityCode, appIndicatorEnum, tnxModeEnum, ipgIdentificationParamsDTO, payCurrencyDTO,
				paymentTimestamp, cardHoldersName, totalPayAmountWithServiceCharges, authorizationId);

		lccClientCreditCardPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccClientCreditCardPaymentInfo.setPayReference(payReference);
		lccClientCreditCardPaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		lccClientCreditCardPaymentInfo.setLccUniqueTnxId(lccUniqueKey);
		// If you need to enable other carrier credit payments set the carrier code correctly.
		// Please note you will have to remove db constraints in t_res_pcd table
		lccClientCreditCardPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		if (lccClientCCPaymentMetaInfo != null) {
			if (lccClientCCPaymentMetaInfo.getPaymentBrokerId() != null) {
				lccClientCreditCardPaymentInfo.setPaymentBrokerId(lccClientCCPaymentMetaInfo.getPaymentBrokerId());
			}

			if (lccClientCCPaymentMetaInfo.getTemporyPaymentId() != null) {
				lccClientCreditCardPaymentInfo.setTemporyPaymentId(lccClientCCPaymentMetaInfo.getTemporyPaymentId());
			}

			lccClientCreditCardPaymentInfo.setPaymentSuccess(lccClientCCPaymentMetaInfo.isPaymentSuccessful());
		}

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lccClientCreditCardPaymentInfo);
	}

	/**
	 * Add card payment information
	 * 
	 * @param cardType
	 * @param eDate
	 * @param cardNumber
	 *            Could be full card number or only last 4 digits
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param ipgIdentificationParamsDTO
	 * @param payCurrencyDTO
	 *            TODO
	 * @param oldCCRecordId
	 * @param payReference
	 * @param actualPaymentMethod
	 * @param refundInfo
	 *            TODO
	 */
	public void addInternalCardPayment(int cardType, String eDate, String cardNumber, String name, String address,
			String securityCode, BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String cardHoldersName, Integer tptId, String authorizationId, Integer oldCCRecordId, String payReference,
			Integer actualPaymentMethod, RefundInfoTO refundInfo) {
		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = getCardPaymentInfo(cardType, eDate, cardNumber, name,
				address, securityCode, appIndicatorEnum, tnxModeEnum, ipgIdentificationParamsDTO, payCurrencyDTO,
				paymentTimestamp, cardHoldersName, totalPayAmountWithServiceCharges, authorizationId);
		// If you need to enable other carrier credit payments set the carrier code correctly.
		// Please note you will have to remove db constraints in t_res_pcd table
		lccClientCreditCardPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		lccClientCreditCardPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccClientCreditCardPaymentInfo.setTemporyPaymentId(tptId);
		lccClientCreditCardPaymentInfo.setOldCCRecordId(oldCCRecordId);
		lccClientCreditCardPaymentInfo.setPayReference(payReference);
		lccClientCreditCardPaymentInfo.setActualPaymentMethod(actualPaymentMethod);

		if (refundInfo != null) {
			lccClientCreditCardPaymentInfo.setPaymentTnxReference(refundInfo.getOriginalPaymentTnxId());
			lccClientCreditCardPaymentInfo.setOriginalPaymentUID(refundInfo.getOriginalPaymentLccUniqueTnxId());
			lccClientCreditCardPaymentInfo.setPaymentTnxDateTime(refundInfo.getPaymentdate());
			lccClientCreditCardPaymentInfo.setCarrierVisePayments(refundInfo.getCarrierVisePayments());
		}

		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(lccClientCreditCardPaymentInfo);
	}

	private CommonCreditCardPaymentInfo getCardPaymentInfo(int cardType, String eDate, String cardNumber, String name,
			String address, String securityCode, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String cardHoldersName, BigDecimal totalPayAmountWithServiceCharges, String authorizationId) {
		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();
		lccClientCreditCardPaymentInfo.setType(cardType);
		lccClientCreditCardPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		lccClientCreditCardPaymentInfo.setNo(cardNumber);

		lccClientCreditCardPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lccClientCreditCardPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		lccClientCreditCardPaymentInfo.setTxnDateTime(paymentTimestamp);
		lccClientCreditCardPaymentInfo.setPaymentSuccess(false);

		lccClientCreditCardPaymentInfo.setAppIndicator(appIndicatorEnum);
		lccClientCreditCardPaymentInfo.setTnxMode(tnxModeEnum);
		lccClientCreditCardPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		String last4Digits = (cardNumber != null && !cardNumber.trim().equals("")) ? (cardNumber
				.substring(cardNumber.length() - 4)) : "";
		String remarks = "Mode of Payment :CC(" + PaymentType.getPaymentTypeDesc(cardType) + ")" + "Last 4 digits :"
				+ last4Digits + ")";
		lccClientCreditCardPaymentInfo.setRemarks(remarks);
		lccClientCreditCardPaymentInfo.setCardHoldersName(cardHoldersName);

		// If you need to enable other carrier credit payments set the carrier code correctly.
		// Please note you will have to remove db constraints in t_res_pcd table
		lccClientCreditCardPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		/*
		 * Add generic card payment information This means we don't know the type of the credit card we simply make it
		 * Generic
		 */
		if (PaymentType.CARD_GENERIC.getTypeValue() != cardType) {
			lccClientCreditCardPaymentInfo.seteDate(eDate);
			lccClientCreditCardPaymentInfo.setNo(cardNumber);
			lccClientCreditCardPaymentInfo.setName(name);
			lccClientCreditCardPaymentInfo.setAddress(address);
			lccClientCreditCardPaymentInfo.setSecurityCode(securityCode);
		} else {
			// Added to use CB card type internally as Generic card
			if (eDate != null) {
				lccClientCreditCardPaymentInfo.seteDate(eDate);
			}
			if (securityCode != null) {
				lccClientCreditCardPaymentInfo.setSecurityCode(securityCode);
			}
			if (cardNumber != null) {
				lccClientCreditCardPaymentInfo.setNo(cardNumber);
			}
		}
		lccClientCreditCardPaymentInfo.setAuthorizationId(authorizationId);
		return lccClientCreditCardPaymentInfo;
	}

	/**
	 * Add External card payment
	 * 
	 * @param cardType
	 * @param cardNumber
	 *            Could be full card number or only last 4 digits
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param ipgIdentificationParamsDTO
	 * @param payCurrencyDTO
	 *            TODO
	 * @param paymentTnxReference
	 *            TODO
	 * @param paymentBrokerId
	 * @param isPaymentSuccess
	 */
	public void addExternalCardPayment(int cardType, String cardNumber, BigDecimal amount, AppIndicatorEnum appIndicatorEnum,
			TnxModeEnum tnxModeEnum, LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp,
			String lccUniqueId, String authorizationId, boolean isofflinePayment, String paymentTnxReference) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(amount, externalCharges);

		// handle negative pay amounts in anci modification flow
		if (totalPayAmountWithServiceCharges.compareTo(BigDecimal.ZERO) < 0) {
			totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		// handle negative pay amounts in anci modification flow
		if (totalPayAmountWithServiceCharges.compareTo(BigDecimal.ZERO) < 0) {
			totalPayAmountWithServiceCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();
		lccClientCreditCardPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lccClientCreditCardPaymentInfo.setType(cardType);
		lccClientCreditCardPaymentInfo.setNo(cardNumber);

		// Note : total in payment currency get sets in PayCurrencyDTO at temp payment recording stage.
		lccClientCreditCardPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges); // base currency amount

		lccClientCreditCardPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lccClientCreditCardPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));

		lccClientCreditCardPaymentInfo.setTxnDateTime(paymentTimestamp);

		if (lccClientCCPaymentMetaInfo != null) {
			if (lccClientCCPaymentMetaInfo.getPaymentBrokerId() != null) {
				lccClientCreditCardPaymentInfo.setPaymentBrokerId(lccClientCCPaymentMetaInfo.getPaymentBrokerId());
			}

			if (lccClientCCPaymentMetaInfo.getTemporyPaymentId() != null) {
				lccClientCreditCardPaymentInfo.setTemporyPaymentId(lccClientCCPaymentMetaInfo.getTemporyPaymentId());
			}

			lccClientCreditCardPaymentInfo.setPaymentSuccess(lccClientCCPaymentMetaInfo.isPaymentSuccessful());
		}
		if (paymentTnxReference != null) {
			lccClientCreditCardPaymentInfo.setPaymentTnxReference(paymentTnxReference);
		}

		lccClientCreditCardPaymentInfo.setAppIndicator(appIndicatorEnum);
		lccClientCreditCardPaymentInfo.setTnxMode(tnxModeEnum);
		lccClientCreditCardPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		lccClientCreditCardPaymentInfo.setLccUniqueTnxId(lccUniqueId);
		lccClientCreditCardPaymentInfo.setOfflinePayment(isofflinePayment);
		// If you need to enable other carrier credit payments set the carrier code correctly.
		// Please note you will have to remove db constraints in t_res_pcd table
		lccClientCreditCardPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);
		lccClientCreditCardPaymentInfo.setAuthorizationId(authorizationId);
		this.payments.add(lccClientCreditCardPaymentInfo);
	}

	public void addLoyaltyCreditPayment(String loyaltyAgentCode, BigDecimal totalPaymentAmount, String loyaltyAccount,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp) {
		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(totalPaymentAmount, externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		CommonLoyaltyPaymentInfo loyaltyPaymentInfo = new CommonLoyaltyPaymentInfo();
		loyaltyPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		loyaltyPaymentInfo.setAgentCode(loyaltyAgentCode);
		loyaltyPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		loyaltyPaymentInfo.setLoyaltyAccount(loyaltyAccount);
		loyaltyPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		loyaltyPaymentInfo.setTxnDateTime(paymentTimestamp);

		loyaltyPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		loyaltyPaymentInfo.setRemarks("Mode of Payment :LOYALTY [" + loyaltyAccount + "," + loyaltyAccount + "]");
		// If you need to enable other carrier loyalty credit payments set the carrier code correctly.
		loyaltyPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(loyaltyPaymentInfo);
	}

	public void addLMSMemberPayment(String loyaltyMemberId, String[] rewardIDs,
			Map<Integer, Map<String, BigDecimal>> paxProductRedeemed, BigDecimal totalPaymentAmount,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String carrierCode) {
		BigDecimal externalCharges = getTotalExternalChargesAmount();
		// BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.subtract(totalPaymentAmount,
		// externalCharges);

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		LCCClientLMSPaymentInfo lmsPaymentInfo = new LCCClientLMSPaymentInfo();
		lmsPaymentInfo.setLoyaltyMemberAccountId(loyaltyMemberId);
		lmsPaymentInfo.setRewardIDs(rewardIDs);
		lmsPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		lmsPaymentInfo.setTxnDateTime(paymentTimestamp);
		lmsPaymentInfo.setPaxProductRedeemedAmounts(paxProductRedeemed);
		payCurrencyDTO = null;
		lmsPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		lmsPaymentInfo.setRemarks("Mode of Payment :LMS Loyalty [" + loyaltyMemberId + "," + Arrays.toString(rewardIDs) + "]");
		lmsPaymentInfo.setCarrierCode(carrierCode);

		lmsPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		lmsPaymentInfo.setTotalAmount(totalPaymentAmount);
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPaymentAmount);

		this.trackPaymentExistOrNot();

		this.payments.add(lmsPaymentInfo);

	}

	public void addVoucherPayment(VoucherDTO voucherDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp) {

		BigDecimal externalCharges = getTotalExternalChargesAmount();
		BigDecimal totalPayAmountWithServiceCharges = AccelAeroCalculator.add(new BigDecimal(voucherDTO.getRedeemdAmount()),
				externalCharges);
		voucherDTO.setRedeemdAmount(totalPayAmountWithServiceCharges.toString());

		boolean includeExternalCharges = (externalCharges.doubleValue() > 0);

		LCCClientVoucherPaymentInfo voucherPaymentInfo = new LCCClientVoucherPaymentInfo();
		voucherPaymentInfo.setIncludeExternalCharges(includeExternalCharges);
		voucherPaymentInfo.setTotalAmount(totalPayAmountWithServiceCharges);
		voucherPaymentInfo.setTotalAmountCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		voucherPaymentInfo.setTxnDateTime(paymentTimestamp);
		voucherPaymentInfo.setVoucherDTO(voucherDTO);

		voucherPaymentInfo.setPayCurrencyDTO(getNullSafePayCurrencyDTO(payCurrencyDTO));
		voucherPaymentInfo.setRemarks("Mode of Payment :VOUCHER [" + voucherDTO.getVoucherId() + "," + voucherDTO.getVoucherId()
				+ "]");
		// If you need to enable other carrier loyalty credit payments set the carrier code correctly.
		voucherPaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		this.trackPaymentExistOrNot();
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, totalPayAmountWithServiceCharges);

		this.payments.add(voucherPaymentInfo);

	}

	/**
	 * Returns the total per passenger external charges amount
	 * 
	 * @return
	 */
	private BigDecimal getTotalExternalChargesAmount() {
		BigDecimal totalServiceChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!this.isExternalChargesConsumed && this.perPaxExternalCharges != null && this.perPaxExternalCharges.size() > 0) {
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : this.perPaxExternalCharges) {
				if (!lccClientExternalChgDTO.isAmountConsumedForPayment()) {
					totalServiceChargeAmount = AccelAeroCalculator.add(totalServiceChargeAmount,
							lccClientExternalChgDTO.getAmount());
				}
			}
			this.isExternalChargesConsumed = true;
		}
		return totalServiceChargeAmount;
	}

	/**
	 * Returns the total per passenger external charges amount
	 * 
	 * @return
	 */
	public BigDecimal calculateTotalExternalChargesAmount() {
		BigDecimal totalServiceChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.perPaxExternalCharges != null && this.perPaxExternalCharges.size() > 0) {
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : this.perPaxExternalCharges) {
				totalServiceChargeAmount = AccelAeroCalculator.add(totalServiceChargeAmount, lccClientExternalChgDTO.getAmount());
			}
		}
		return totalServiceChargeAmount;
	}

	public BigDecimal getTotalConsumedExternalCharges() {
		BigDecimal totalServiceChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.isExternalChargesConsumed && this.perPaxExternalCharges != null && this.perPaxExternalCharges.size() > 0) {
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : this.perPaxExternalCharges) {
				if (!lccClientExternalChgDTO.isAmountConsumedForPayment()) {
					totalServiceChargeAmount = AccelAeroCalculator.add(totalServiceChargeAmount,
							lccClientExternalChgDTO.getAmount());
				}
			}
		}

		return totalServiceChargeAmount;
	}

	/**
	 * Return Payments
	 * 
	 * @return
	 */
	public Collection<LCCClientPaymentInfo> getPayments() {
		return this.payments;
	}

	/**
	 * Injects the unique transaction id
	 * 
	 * @param lccUniqueTnxId
	 */
	public void injectLccPaymentRefNumber(String lccPaymentRefNumber) {
		if (this.payments != null && this.payments.size() > 0) {
			for (LCCClientPaymentInfo lccClientPaymentInfo : this.payments) {
				lccClientPaymentInfo.setLccUniqueTnxId(lccPaymentRefNumber);
			}
		}
	}

	/**
	 * @return Returns the totalPayAmount.
	 */
	public BigDecimal getTotalPayAmount() {
		return totalPayAmount;
	}

	/**
	 * @return Returns the paymentExist.
	 */
	public boolean isPaymentExist() {
		return paymentExist;
	}

	/**
	 * @param paymentExist
	 *            The paymentExist to set.
	 */
	private void setPaymentExist(boolean paymentExist) {
		this.paymentExist = paymentExist;
	}

	private PayCurrencyDTO getNullSafePayCurrencyDTO(PayCurrencyDTO payDTO) {
		if (payDTO == null) {
			return new PayCurrencyDTO(AppSysParamsUtil.getBaseCurrency(), BigDecimal.ONE);
		} else {
			return (PayCurrencyDTO) payDTO.clone();
		}
	}

	/**
	 * @return Returns the perPaxExternalCharges.
	 */
	public Collection<LCCClientExternalChgDTO> getPerPaxExternalCharges(EXTERNAL_CHARGES externalCharges) {
		Collection<LCCClientExternalChgDTO> tmpExternalChgDTO = new ArrayList<LCCClientExternalChgDTO>();

		for (LCCClientExternalChgDTO lccClientExternalChgDTO : perPaxExternalCharges) {

			if (lccClientExternalChgDTO.getExternalCharges() == externalCharges) {
				tmpExternalChgDTO.add(lccClientExternalChgDTO);
			}
		}

		return tmpExternalChgDTO;
	}

	/**
	 * @return the perPaxExternalCharges
	 */
	public Collection<LCCClientExternalChgDTO> getPerPaxExternalCharges() {
		return perPaxExternalCharges;
	}

	public BigDecimal getTotalWithoutExternalCharges() {
		BigDecimal totalWoExt = BigDecimal.ZERO;
		BigDecimal totalExtCharges = BigDecimal.ZERO;
		if (getPerPaxExternalCharges() != null && isExternalChargesConsumed) {
			for (LCCClientExternalChgDTO lccClientExternalChgDTO : getPerPaxExternalCharges()) {
				// if (lccClientExternalChgDTO.isAmountConsumedForPayment()) {
				totalExtCharges = AccelAeroCalculator.add(totalExtCharges, lccClientExternalChgDTO.getAmount());
				// }
			}

		}
		totalWoExt = AccelAeroCalculator.subtract(getTotalPayAmount(), totalExtCharges);

		return totalWoExt;
	}

	public boolean isExternalChargesConsumed() {
		return isExternalChargesConsumed;
	}

	public void setExternalChargesConsumed(boolean isExternalChargesConsumed) {
		this.isExternalChargesConsumed = isExternalChargesConsumed;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientPaymentAssembler [payCurrencyDTO=");
		builder.append(", paymentExist=");
		builder.append(paymentExist);
		builder.append(", payments=");
		builder.append(payments);
		builder.append(", totalPayAmount=");
		builder.append(totalPayAmount);
		builder.append("]");
		return builder.toString();
	}

	public LCCClientPaymentAssembler clone() {
		LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
		lccClientPaymentAssembler.setPaymentExist(this.isPaymentExist());
		lccClientPaymentAssembler.setExternalChargesConsumed(this.isExternalChargesConsumed());
		for (LCCClientExternalChgDTO lccClientExternalChgDTO : this.getPerPaxExternalCharges()) {
			lccClientPaymentAssembler.addExternalCharges(lccClientExternalChgDTO.clone());
		}

		for (LCCClientPaymentInfo lcclientPaymentInfo : this.getPayments()) {
			BigDecimal totalWOExtCharges = BigDecimal.ZERO;
			if (lcclientPaymentInfo.includeExternalCharges()) {
				lccClientPaymentAssembler.setExternalChargesConsumed(false);
				totalWOExtCharges = AccelAeroCalculator.subtract(lcclientPaymentInfo.getTotalAmount(),
						this.getTotalConsumedExternalCharges());
			} else {
				totalWOExtCharges = lcclientPaymentInfo.getTotalAmount();
			}

			if (lcclientPaymentInfo instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo commonCreditCardPaymentInfo = (CommonCreditCardPaymentInfo) lcclientPaymentInfo;
				lccClientPaymentAssembler.addInternalCardPayment(commonCreditCardPaymentInfo.getType(),
						commonCreditCardPaymentInfo.geteDate(), commonCreditCardPaymentInfo.getNo(), commonCreditCardPaymentInfo
								.getCardHoldersName(), commonCreditCardPaymentInfo.getAddress(), commonCreditCardPaymentInfo
								.getSecurityCode(), totalWOExtCharges, commonCreditCardPaymentInfo.getAppIndicator(),
						commonCreditCardPaymentInfo.getTnxMode(),
						(commonCreditCardPaymentInfo.getIpgIdentificationParamsDTO() == null ? null : commonCreditCardPaymentInfo
								.getIpgIdentificationParamsDTO().clone()),
						(commonCreditCardPaymentInfo.getPayCurrencyDTO() == null
								? null
								: (PayCurrencyDTO) commonCreditCardPaymentInfo.getPayCurrencyDTO().clone()),
						(commonCreditCardPaymentInfo.getTxnDateTime() == null ? null : (Date) commonCreditCardPaymentInfo
								.getTxnDateTime().clone()), commonCreditCardPaymentInfo.getCardHoldersName(),
						commonCreditCardPaymentInfo.getPayReference(), commonCreditCardPaymentInfo.getActualPaymentMethod(),
						commonCreditCardPaymentInfo.getAuthorizationId(), null);
			} else if (lcclientPaymentInfo instanceof LCCClientOnAccountPaymentInfo) {
				LCCClientOnAccountPaymentInfo lccClientOnAccountPaymentInfo = (LCCClientOnAccountPaymentInfo) lcclientPaymentInfo;
				lccClientPaymentAssembler.addAgentCreditPayment(lccClientOnAccountPaymentInfo.getAgentCode(), totalWOExtCharges,
						(lccClientOnAccountPaymentInfo.getPayCurrencyDTO() == null
								? null
								: (PayCurrencyDTO) lccClientOnAccountPaymentInfo.getPayCurrencyDTO().clone()),
						(lccClientOnAccountPaymentInfo.getTxnDateTime() == null ? null : (Date) lccClientOnAccountPaymentInfo
								.getTxnDateTime().clone()), lccClientOnAccountPaymentInfo.getPayReference(),
						lccClientOnAccountPaymentInfo.getActualPaymentMethod(), lccClientOnAccountPaymentInfo.getPaymentMethod(),
						null);
			} else if (lcclientPaymentInfo instanceof LCCClientCashPaymentInfo) {
				LCCClientCashPaymentInfo lccClientCashPaymentInfo = (LCCClientCashPaymentInfo) lcclientPaymentInfo;
				lccClientPaymentAssembler
						.addCashPayment(totalWOExtCharges, (lccClientCashPaymentInfo.getPayCurrencyDTO() == null
								? null
								: (PayCurrencyDTO) lccClientCashPaymentInfo.getPayCurrencyDTO().clone()),
								(lccClientCashPaymentInfo.getTxnDateTime() == null ? null : (Date) lccClientCashPaymentInfo
										.getTxnDateTime().clone()), lccClientCashPaymentInfo.getPayReference(),
								lccClientCashPaymentInfo.getActualPaymentMethod(), null, null);
			} else if (lcclientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo) {
				LCCClientPaxCreditPaymentInfo lccClientPaxCreditPaymentInfo = (LCCClientPaxCreditPaymentInfo) lcclientPaymentInfo;
				lccClientPaymentAssembler.addPaxCreditPayment(lccClientPaxCreditPaymentInfo.getCarrierCode(),
						lccClientPaxCreditPaymentInfo.getDebitPaxRefNo(), totalWOExtCharges,
						(lccClientPaxCreditPaymentInfo.getTxnDateTime() == null ? null : (Date) lccClientPaxCreditPaymentInfo
								.getTxnDateTime().clone()), lccClientPaxCreditPaymentInfo.getResidingCarrierAmount(),
						(lccClientPaxCreditPaymentInfo.getPayCurrencyDTO() == null
								? null
								: (PayCurrencyDTO) lccClientPaxCreditPaymentInfo.getPayCurrencyDTO().clone()),
						(lccClientPaxCreditPaymentInfo.getExpiryDate() == null ? null : (Date) lccClientPaxCreditPaymentInfo
								.getExpiryDate().clone()), lccClientPaxCreditPaymentInfo.getPaxSequence(),
						lccClientPaxCreditPaymentInfo.getLccUniqueTnxId(), lccClientPaxCreditPaymentInfo.getResidingPnr(),
						lccClientPaxCreditPaymentInfo.getPaxCreditId());
			} else if (lcclientPaymentInfo instanceof LCCClientVoucherPaymentInfo) {
				LCCClientVoucherPaymentInfo lccClientVoucherPaymentInfo = (LCCClientVoucherPaymentInfo) lcclientPaymentInfo;
				lccClientPaymentAssembler.addVoucherPayment(lccClientVoucherPaymentInfo.getVoucherDTO().clone(),
						(lccClientVoucherPaymentInfo.getPayCurrencyDTO() == null
								? null
								: (PayCurrencyDTO) lccClientVoucherPaymentInfo.getPayCurrencyDTO().clone()),
						(lccClientVoucherPaymentInfo.getTxnDateTime() == null ? null : (Date) lccClientVoucherPaymentInfo
								.getTxnDateTime().clone()));
			}

		}
		return lccClientPaymentAssembler;
	}
}