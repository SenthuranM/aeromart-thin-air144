package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class GDSPaxChargesTO implements Serializable {

	private static final long serialVersionUID = -1379957412579860540L;
	private boolean payment;
	private boolean invokeAutoRefund;
	private BigDecimal paidAmount;
	private BigDecimal balanceAmount;
	private BigDecimal fareAmount;
	private BigDecimal taxAmount;
	private BigDecimal surChargeAmount;
	private BigDecimal otherAmount;
	private String paxType;
	private Integer pnrPaxId;
	private boolean inactiveSegmentsApplicable;

	public GDSPaxChargesTO() {
		inactiveSegmentsApplicable = false;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}
	
	public boolean isPayment() {
		return payment;
	}

	public void setPayment(boolean payment) {
		this.payment = payment;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getTotalCharges() {
		return AccelAeroCalculator.add(getFareAmount(), getTaxAmount(), getSurChargeAmount());
	}

	public BigDecimal getFareAmount() {
		if (this.fareAmount == null)
			return AccelAeroCalculator.getDefaultBigDecimalZero();

		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public BigDecimal getTaxAmount() {
		if (this.taxAmount == null)
			return AccelAeroCalculator.getDefaultBigDecimalZero();

		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getSurChargeAmount() {
		if (this.surChargeAmount == null)
			return AccelAeroCalculator.getDefaultBigDecimalZero();

		return surChargeAmount;
	}

	public void setSurChargeAmount(BigDecimal surChargeAmount) {
		this.surChargeAmount = surChargeAmount;
	}

	public BigDecimal getOtherAmount() {
		if (this.otherAmount == null)
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		return otherAmount;
	}

	public void setOtherAmount(BigDecimal otherAmount) {
		this.otherAmount = otherAmount;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public boolean isInvokeAutoRefund() {
		return invokeAutoRefund;
	}

	public void setInvokeAutoRefund(boolean invokeAutoRefund) {
		this.invokeAutoRefund = invokeAutoRefund;
	}

	public boolean isInactiveSegmentsApplicable() {
		return inactiveSegmentsApplicable;
	}

	public void setInactiveSegmentsApplicable(boolean inactiveSegmentsApplicable) {
		this.inactiveSegmentsApplicable = inactiveSegmentsApplicable;
	}
}
