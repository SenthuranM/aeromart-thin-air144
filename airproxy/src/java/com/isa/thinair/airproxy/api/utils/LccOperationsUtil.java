package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.thinair.airproxy.api.LccSubOperationConstants;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;

public class LccOperationsUtil {
	private LccOperationsUtil() {
	}


	public static List<String> getModifyReservationOperations(Reservation currentRes, AAReservation modifiedRes) {
		List<String> operations = new ArrayList<String>();

		AirReservation modifiedAirRes = modifiedRes.getAirReservation();
		Collection<AirTraveler> modifiedPax = modifiedAirRes.getTravelerInfo().getAirTraveler();
		Map<String, AirTraveler> modifiedPaxMap = new HashMap<String, AirTraveler>();
		String travellerRef;
		PersonName personName;
		boolean nameChange = false;

		for (AirTraveler airTraveler : modifiedAirRes.getTravelerInfo().getAirTraveler()) {
			modifiedPaxMap.put(PaxTypeUtils.getTravellerRefFromLccRef(airTraveler.getTravelerRefNumber()), airTraveler);
		}


		for (ReservationPax passenger : currentRes.getPassengers()) {
			travellerRef = PaxTypeUtils.travelerReference(passenger);
			nameChange = false;

			if (modifiedPaxMap.containsKey(travellerRef)) {
				personName = modifiedPaxMap.get(travellerRef).getPersonName();

				if (passenger.getTitle() != null && personName.getTitle() != null &&
						!passenger.getTitle().equals(personName.getTitle())) {
					nameChange = true;
				}
//				else if (!(passenger.getTitle() == null && personName.getTitle() == null)) {
//					nameChange = true;
//				}

				if (passenger.getFirstName() != null && personName.getFirstName() != null &&
						!passenger.getFirstName().equals(personName.getFirstName())) {
					nameChange = true;
				}
//				else if (!(passenger.getFirstName() == null && personName.getFirstName() == null)) {
//
//				}

				if (passenger.getLastName() != null && personName.getSurName() != null &&
						!passenger.getLastName().equals(personName.getSurName())) {
					nameChange = true;
				}
//				else if (!(passenger.getLastName() == null && personName.getSurName() == null)) {
//					nameChange = true;
//				}


				if (nameChange) {
					operations.add(LccSubOperationConstants.ModifyReservation.ALTER_PAX_INFO);
					break;
				}
			} else {
				// TODO -- throw exception
			}
		}

		if (currentRes.getUserNote() != null && modifiedAirRes.getLastUserNote() != null
				&& modifiedAirRes.getLastUserNote().getNoteText() != null
				&& !currentRes.getUserNote().equals(modifiedAirRes.getLastUserNote().getNoteText())) {
			operations.add(LccSubOperationConstants.ModifyReservation.ALTER_USER_NOTE);
		}


		return operations;
	}

	public static boolean isAuthorized(Collection<String> operations, Collection<String> allowedOperations) {
		return allowedOperations.containsAll(operations);
	}


	public static Collection<String> getGrantedOperations(Collection<String> privileges) {
		Collection<String> grantedOperations = new HashSet<String>();
		Collection<String> requiredPrivileges;

		requiredPrivileges = LccSubOperationConstants.ModifyReservation
				.getPrivileges(LccSubOperationConstants.ModifyReservation.ALTER_PAX_INFO);

		if(privileges.containsAll(requiredPrivileges)) {
			grantedOperations.add(LccSubOperationConstants.ModifyReservation.ALTER_PAX_INFO);
		}

		requiredPrivileges = LccSubOperationConstants.ModifyReservation
				.getPrivileges(LccSubOperationConstants.ModifyReservation.ALTER_USER_NOTE);

		if(privileges.containsAll(requiredPrivileges)) {
			grantedOperations.add(LccSubOperationConstants.ModifyReservation.ALTER_USER_NOTE);
		}

		return grantedOperations;
	}

}
