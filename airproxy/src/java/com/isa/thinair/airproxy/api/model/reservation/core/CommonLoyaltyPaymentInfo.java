package com.isa.thinair.airproxy.api.model.reservation.core;

public class CommonLoyaltyPaymentInfo extends LCCClientOnAccountPaymentInfo {

	private static final long serialVersionUID = 1L;
	private String loyaltyAccount;

	public void setLoyaltyAccount(String loyaltyAccount) {
		this.loyaltyAccount = loyaltyAccount;
	}

	public String getLoyaltyAccount() {
		return loyaltyAccount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommonLoyaltyPaymentInfo [loyaltyAgentCode=");
		builder.append(getAgentCode());
		builder.append(", carrierCode=");
		builder.append(getCarrierCode());
		builder.append(", lccUniqueTnxId=");
		builder.append(getLccUniqueTnxId());
		builder.append(", paxSequence=");
		builder.append(getPaxSequence());
		builder.append(", payCurrencyDTO=");
		builder.append(getPayCurrencyDTO());
		builder.append(", totalAmount=");
		builder.append(getTotalAmount());
		builder.append(", totalAmountCurrencyCode=");
		builder.append(getTotalAmountCurrencyCode());
		builder.append(", txnDateTime=");
		builder.append(getTxnDateTime());
		builder.append(", agentName=");
		builder.append(getAgentName());
		builder.append(", loyaltyAcountNo=");
		builder.append(getLoyaltyAccount());
		builder.append("]");
		return builder.toString();
	}

}
