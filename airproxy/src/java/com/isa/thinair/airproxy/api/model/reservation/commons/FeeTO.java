package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeeTO extends BasicChargeTO implements Serializable, Comparable<FeeTO> {

	private static final long serialVersionUID = 1L;
	
	protected String feeCode;
	protected String carrierCode;
	protected String segmentCode;
	protected List<String> applicablePassengerTypeCode;
	protected String feeName;
	protected Date applicableTime;
	protected String chargeRateId;
	protected String chagrgeGroupCode;
	protected String chargeCode;
	protected String reportingChargeGroupCode;

	public Date getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(Date applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getFeeName() {
		return feeName;
	}

	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}

	public String getFeeCode() {
		return feeCode;
	}

	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public List<String> getApplicablePassengerTypeCode() {
		if (applicablePassengerTypeCode == null)
			return new ArrayList<String>();
		return applicablePassengerTypeCode;
	}

	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public String getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(String chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public String getChagrgeGroupCode() {
		return chagrgeGroupCode;
	}

	public void setChagrgeGroupCode(String chagrgeGroupCode) {
		this.chagrgeGroupCode = chagrgeGroupCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	@Override
	public FeeTO clone() {
		FeeTO feeObj = new FeeTO();
		feeObj.setAmount(this.getAmount());
		feeObj.setApplicableTime(this.getApplicableTime());
		feeObj.setCarrierCode(this.getCarrierCode());
		feeObj.setFeeCode(this.getFeeCode());
		feeObj.setFeeName(this.getFeeName());
		feeObj.setSegmentCode(this.getSegmentCode());
		feeObj.getApplicablePassengerTypeCode().addAll(this.getApplicablePassengerTypeCode());
		feeObj.setChagrgeGroupCode(this.getChagrgeGroupCode());
		feeObj.setChargeRateId(this.getChargeRateId());
		feeObj.setChargeCode(this.getChargeCode());
		feeObj.setLocalCurrencyAmount(this.getLocalCurrencyAmount());
		feeObj.setLocalCurrencyCode(this.getLocalCurrencyCode());
		feeObj.setReportingChargeGroupCode(this.getReportingChargeGroupCode());
		feeObj.setOndSequence(this.getOndSequence());
		return feeObj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeeTO [amount=");
		builder.append(amount);
		builder.append(", applicablePassengerTypeCode=");
		builder.append(applicablePassengerTypeCode);
		builder.append(", applicableTime=");
		builder.append(applicableTime);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", feeCode=");
		builder.append(feeCode);
		builder.append(", feeName=");
		builder.append(feeName);
		builder.append(", segmentCode=");
		builder.append(segmentCode);
		builder.append(", chargeRateId=");
		builder.append(chargeRateId);
		builder.append(", chagrgeGroupCode=");
		builder.append(chagrgeGroupCode);
		builder.append(", chargeCode=");
		builder.append(chargeCode);
		builder.append(", localCurrencyCode=");
		builder.append(localCurrencyCode);
		builder.append(", localCurrencyAmount=");
		builder.append(localCurrencyAmount);
		builder.append("]");
		builder.append(", reportingChargeGroupCode=");
		builder.append(reportingChargeGroupCode);
		builder.append("]");
		return builder.toString();
	}

	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}

	@Override
	public int compareTo(FeeTO o) {
		return o.amount.compareTo(this.amount);
	}
}
