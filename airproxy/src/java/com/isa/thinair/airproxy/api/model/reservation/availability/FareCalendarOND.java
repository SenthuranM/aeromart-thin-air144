package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class FareCalendarOND implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String origin;
	private String destination;
	private String cabinClass;
	private List<FareCalendarDate> calendarDates;

	public FareCalendarOND(String origin, String destination, String cabinClass) {
		this.origin = origin;
		this.destination = destination;
		this.cabinClass = cabinClass;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public Date getFirstDepartureDate() {
		Date date = null;
		for (FareCalendarDate calDate : getCalendarDates()) {
			if (date == null || date.after(calDate.getDate())) {
				date = calDate.getDate();
			}
		}
		return date;
	}

	public List<FareCalendarDate> getCalendarDates() {
		if (calendarDates == null) {
			this.calendarDates = new ArrayList<FareCalendarDate>();
		}
		return calendarDates;
	}

	public void addCalendarDate(FareCalendarDate fareCalDate) {
		getCalendarDates().add(fareCalDate);
		Collections.sort(getCalendarDates());
	}

}
