package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.VoucherPaymentOption;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author chethiya
 *
 */
public class PayByVoucherInfo implements Serializable {

	private static final long serialVersionUID = 1527433953492460913L;

	private ArrayList<VoucherDTO> voucherDTOList;

	private BigDecimal redeemedTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String vouchersTotal;

	private VoucherPaymentOption voucherPaymentOption;

	private Integer unsuccessfulAttempts = 0;

	private String balTotalPay;
	
	private Date voucherRedeemDateTime;

	public PayByVoucherInfo clone() {
		PayByVoucherInfo cloneObj = new PayByVoucherInfo();
		cloneObj.setRedeemedTotal(this.getRedeemedTotal());
		cloneObj.setUnsuccessfulAttempts(this.getUnsuccessfulAttempts());
		for (VoucherDTO voucherDTO : this.voucherDTOList) {
			cloneObj.getVoucherDTOList().add(voucherDTO.clone());
		}
		cloneObj.setVoucherPaymentOption(this.voucherPaymentOption);
		cloneObj.setVouchersTotal(this.getVouchersTotal());
		return cloneObj;
	}

	public BigDecimal getRedeemedTotal() {
		return redeemedTotal;
	}

	public void setRedeemedTotal(BigDecimal redeemedTotal) {
		this.redeemedTotal = redeemedTotal;
	}

	public String getVouchersTotal() {
		return vouchersTotal;
	}

	public void setVouchersTotal(String vouchersTotal) {
		this.vouchersTotal = vouchersTotal;
	}

	public ArrayList<VoucherDTO> getVoucherDTOList() {
		if (this.voucherDTOList == null) {
			this.voucherDTOList = new ArrayList<>();
		}
		return this.voucherDTOList;
	}

	public void setVoucherDTOList(ArrayList<VoucherDTO> voucherDTOList) {
		this.voucherDTOList = voucherDTOList;
	}

	public ArrayList<String> getVoucherIDList() {
		ArrayList<String> voucherIDList = new ArrayList<String>();
		for (VoucherDTO voucherDTO : voucherDTOList) {
			voucherIDList.add(voucherDTO.getVoucherId());
		}
		return voucherIDList;
	}

	public int getUnsuccessfulAttempts() {
		return unsuccessfulAttempts;
	}

	public void setUnsuccessfulAttempts(int unsuccessfulAttempts) {
		this.unsuccessfulAttempts = unsuccessfulAttempts;
	}

	public void incrementUnsuccessfullAttempts() {
		this.unsuccessfulAttempts++;
	}

	/**
	 * @return the voucherPaymentOption
	 */
	public VoucherPaymentOption getVoucherPaymentOption() {
		return voucherPaymentOption;
	}

	/**
	 * @param voucherPaymentOption
	 *            the voucherPaymentOption to set
	 */
	public void setVoucherPaymentOption(VoucherPaymentOption voucherPaymentOption) {
		this.voucherPaymentOption = voucherPaymentOption;
	}

	public String getBalTotalPay() {
		return balTotalPay;
	}

	public void setBalTotalPay(String balTotalPay) {
		this.balTotalPay = balTotalPay;
	}

	public Date getVoucherRedeemDateTime() {
		return voucherRedeemDateTime;
	}

	public void setVoucherRedeemDateTime(Date voucherRedeemDateTime) {
		this.voucherRedeemDateTime = voucherRedeemDateTime;
	}
}
