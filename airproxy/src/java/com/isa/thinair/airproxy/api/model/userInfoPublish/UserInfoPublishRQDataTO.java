package com.isa.thinair.airproxy.api.model.userInfoPublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;

public class UserInfoPublishRQDataTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<User> publishUsers;

	private List<Role> publishRoles;

	private List<Agent> publishAgents;

	public List<User> getPublishUsers() {
		if (publishUsers == null) {
			publishUsers = new ArrayList<User>();
		}
		return publishUsers;
	}

	public void setPublishUsers(List<User> publishUsers) {
		this.publishUsers = publishUsers;
	}

	public List<Role> getPublishRoles() {
		if (publishRoles == null) {
			publishRoles = new ArrayList<Role>();
		}
		return publishRoles;
	}

	public void setPublishRoles(List<Role> publishRoles) {
		this.publishRoles = publishRoles;
	}

	public List<Agent> getPublishAgents() {
		if (publishAgents == null) {
			publishAgents = new ArrayList<Agent>();
		}
		return publishAgents;
	}

	public void setPublishAgents(List<Agent> publishAgents) {
		this.publishAgents = publishAgents;
	}

}
