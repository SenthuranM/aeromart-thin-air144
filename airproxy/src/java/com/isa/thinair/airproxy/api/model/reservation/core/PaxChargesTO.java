/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * @author Rikaz
 */
public class PaxChargesTO implements Serializable,Comparable<PaxChargesTO> {
	private static final long serialVersionUID = 7499656035447977496L;
	private int paxSequence;
	private String paxTypeCode;
	private boolean parent;
	private List<LCCClientExternalChgDTO> extChgList;

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public List<LCCClientExternalChgDTO> getExtChgList() {
		return extChgList;
	}

	public void setExtChgList(List<LCCClientExternalChgDTO> extChgList) {
		this.extChgList = extChgList;
	}

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	public boolean isParent() {
		return parent;
	}

	public void setParent(boolean parent) {
		this.parent = parent;
	}

	@Override
	public int compareTo(PaxChargesTO o) {

		if (o != null)
			return new Integer(this.getPaxSequence()).compareTo(new Integer(o.getPaxSequence()));

		return 0;
	}

}
