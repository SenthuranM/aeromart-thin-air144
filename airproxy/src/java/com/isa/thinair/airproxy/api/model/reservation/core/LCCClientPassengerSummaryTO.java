package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * @since 6:09 PM 11/5/2009
 */
public class LCCClientPassengerSummaryTO implements Serializable, Comparable<LCCClientPassengerSummaryTO> {

	private static final long serialVersionUID = 2248815986030688419L;

	private String travelerRefNumber;

	private String paxName;

	private String paxFirstName;

	private String paxLastName;

	private String paxType;

	private String infantName;

	private LCCClientSegmentSummaryTO lccClientSegmentSummaryTO;

	private BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalExcludedSegAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private PnrChargeDetailTO cnxChargeDetailTO;

	private PnrChargeDetailTO modChargeDetailTO;

	private String accompaniedTravellerRef;

	private BigDecimal totalPenalty;

	private Map<String, Map<String, BigDecimal>> carrierProductCodeWiseAmountDue = new HashMap<String, Map<String, BigDecimal>>();

	/**
	 * @return the travelerRefNumber
	 */
	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	/**
	 * @param travelerRefNumber
	 *            the travelerRefNumber to set
	 */
	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the infantName
	 */
	public String getInfantName() {
		return infantName;
	}

	/**
	 * @param infantName
	 *            the infantName to set
	 */
	public void setInfantName(String infantName) {
		this.infantName = infantName;
	}

	/**
	 * @return the lccClientSegmentSummaryTO
	 */
	public LCCClientSegmentSummaryTO getSegmentSummary() {
		return lccClientSegmentSummaryTO;
	}

	/**
	 * @param lccClientSegmentSummaryTO
	 *            the lccClientSegmentSummaryTO to set
	 */
	public void setSegmentSummaryTO(LCCClientSegmentSummaryTO lccClientSegmentSummaryTO) {
		this.lccClientSegmentSummaryTO = lccClientSegmentSummaryTO;
	}

	/**
	 * @return the totalCnxCharge
	 */
	public BigDecimal getTotalCnxCharge() {
		return totalCnxCharge;
	}

	/**
	 * @param totalCnxCharge
	 *            the totalCnxCharge to set
	 */
	public void setTotalCnxCharge(BigDecimal totalCnxCharge) {
		this.totalCnxCharge = totalCnxCharge;
	}

	/**
	 * @return the totalModCharge
	 */
	public BigDecimal getTotalModCharge() {
		return totalModCharge;
	}

	/**
	 * @param totalModCharge
	 *            the totalModCharge to set
	 */
	public void setTotalModCharge(BigDecimal totalModCharge) {
		this.totalModCharge = totalModCharge;
	}

	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalAmountDue
	 */
	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	/**
	 * @param totalAmountDue
	 *            the totalAmountDue to set
	 */
	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	/**
	 * @return the totalPaidAmount
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	public int compareTo(LCCClientPassengerSummaryTO paxSummary) {
		return PaxTypeUtils.getPaxSeq(this.getTravelerRefNumber())
				.compareTo(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()));

	}

	/**
	 * @return the totalCreditAmount
	 */
	public BigDecimal getTotalCreditAmount() {
		return totalCreditAmount;
	}

	/**
	 * @param totalCreditAmount
	 *            the totalCreditAmount to set
	 */
	public void setTotalCreditAmount(BigDecimal totalCreditAmount) {
		this.totalCreditAmount = totalCreditAmount;
	}

	/**
	 * @return the cnxChargeDetailTO
	 */
	public PnrChargeDetailTO getCnxChargeDetailTO() {
		return cnxChargeDetailTO;
	}

	/**
	 * @param cnxChargeDetailTO
	 *            the cnxChargeDetailTO to set
	 */
	public void setCnxChargeDetailTO(PnrChargeDetailTO cnxChargeDetailTO) {
		this.cnxChargeDetailTO = cnxChargeDetailTO;
	}

	/**
	 * @return the modChargeDetailTO
	 */
	public PnrChargeDetailTO getModChargeDetailTO() {
		return modChargeDetailTO;
	}

	/**
	 * @param modChargeDetailTO
	 *            the modChargeDetailTO to set
	 */
	public void setModChargeDetailTO(PnrChargeDetailTO modChargeDetailTO) {
		this.modChargeDetailTO = modChargeDetailTO;
	}

	public void setTotalFareDiscount(BigDecimal totalFareDiscount) {
		this.totalFareDiscount = totalFareDiscount;
	}

	public BigDecimal getTotalFareDiscount() {
		return totalFareDiscount;
	}

	public String getPaxFirstName() {
		return paxFirstName;
	}

	public String getPaxLastName() {
		return paxLastName;
	}

	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}

	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}

	public void setAccompaniedTravellerRef(String accompaniedTravellerRef) {
		this.accompaniedTravellerRef = accompaniedTravellerRef;
	}

	public String getAccompaniedTravellerRef() {
		return accompaniedTravellerRef;
	}

	public boolean hasInfant() {
		return getInfantName() != null && !"".equals(getInfantName().trim());
	}

	public void setTotalPenalty(BigDecimal totalPenalty) {
		this.totalPenalty = totalPenalty;
	}

	public BigDecimal getTotalPenalty() {
		return totalPenalty;
	}

	public BigDecimal getTotalExcludedSegAmount() {
		return totalExcludedSegAmount;
	}

	public void setTotalExcludedSegAmount(BigDecimal totalExcludedSegAmount) {
		this.totalExcludedSegAmount = totalExcludedSegAmount;
	}

	public Map<String, Map<String, BigDecimal>> getCarrierProductCodeWiseAmountDue() {
		return carrierProductCodeWiseAmountDue;
	}

	public void setCarrierProductCodeWiseAmountDue(Map<String, Map<String, BigDecimal>> carrierProductCodeWiseAmountDue) {
		this.carrierProductCodeWiseAmountDue = carrierProductCodeWiseAmountDue;
	}

	public BigDecimal getBalance() {
		BigDecimal balance = getTotalAmountDue();
		if (getTotalCreditAmount().compareTo(BigDecimal.ZERO) > 0) {
			balance = getTotalCreditAmount().negate();
		}
		return balance;
	}

}
