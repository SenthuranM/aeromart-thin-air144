package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.List;

/**
 * Tranfer Object used for charge reversal operations
 * 
 */
public class LCCClientChargeReverse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String carrierCode;

	private String carrierOndGroupRPH;

	private String segmentCode;

	private String travelerRefNumber;

	private String userNote;
	
	private List<String> travelerRefNumbersList;

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCarrierOndGroupRPH() {
		return carrierOndGroupRPH;
	}

	public void setCarrierOndGroupRPH(String carrierOndGroupRPH) {
		this.carrierOndGroupRPH = carrierOndGroupRPH;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public List<String> getTravelerRefNumbersList() {
		return travelerRefNumbersList;
	}

	public void setTravelerRefNumbersList(List<String> travelerRefNumbersList) {
		this.travelerRefNumbersList = travelerRefNumbersList;
	}

}
