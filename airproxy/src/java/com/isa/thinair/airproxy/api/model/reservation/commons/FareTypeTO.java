package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

public class FareTypeTO implements Serializable {

	private static final long serialVersionUID = 1L;
	protected List<PerPaxPriceInfoTO> perPaxPriceInfo;
	protected List<BaseFareTO> baseFares;
	protected List<TaxTO> taxes;
	protected List<FeeTO> fees;
	protected List<SurchargeTO> surcharges;

	protected List<ONDExternalChargeTO> ondExternalCharges;

	private List<FareRuleDTO> applicableFareRules;

	private FareDiscountTO fareDiscountInfo;

	private ApplicablePromotionDetailsTO promotionTO;

	private boolean onHoldRestricted;
	private Date onHoldStartTime;
	private Date onHoldStopTime;
	private Date onHoldReleaseTimeStamp;

	protected BigDecimal totalBaseFare;
	protected BigDecimal totalTaxes;
	protected BigDecimal totalSurcharges;
	protected BigDecimal totalFees;
	protected BigDecimal totalPrice;
	protected BigDecimal totalPriceWithInfant;
	private BigDecimal serviceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	protected int fareType;
	protected Map<Integer, Integer> ondWiseFareType;
	protected String logicalCCtype;

	private Map<Integer, Boolean> ondFlexiSelection;

	private BigDecimal totAgentCommission; // // for x*AD + y*CD + z*INT
	private BigDecimal perPaxTotalAgentCommission; // for a time AD or CH or IN
	private AgentCommissionDetails applicableAgentCommissions;

	protected Map<Integer, Integer> ondWiseFareId;

	private List<BundledFareDTO> ondBundledFareDTOs;

	public List<PerPaxPriceInfoTO> getPerPaxPriceInfo() {
		return perPaxPriceInfo;
	}

	public void setPerPaxPriceInfo(List<PerPaxPriceInfoTO> perPaxPriceInfo) {
		this.perPaxPriceInfo = perPaxPriceInfo;
	}

	public List<TaxTO> getTaxes() {
		if (taxes == null) {
			taxes = new ArrayList<TaxTO>();
		}
		return taxes;
	}

	public void setTaxes(List<TaxTO> taxes) {
		this.taxes = taxes;
	}

	public List<FeeTO> getFees() {
		if (fees == null) {
			fees = new ArrayList<FeeTO>();
		}
		return fees;
	}

	public void setFees(List<FeeTO> fees) {
		this.fees = fees;
	}

	public List<SurchargeTO> getSurcharges() {
		if (surcharges == null) {
			surcharges = new ArrayList<SurchargeTO>();
		}
		return surcharges;
	}

	public void setSurcharges(List<SurchargeTO> surcharges) {
		this.surcharges = surcharges;
	}

	public BigDecimal getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(BigDecimal totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public BigDecimal getTotalSurcharges() {
		return totalSurcharges;
	}

	public void setTotalSurcharges(BigDecimal totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	public BigDecimal getTotalFees() {
		return totalFees;
	}

	public void setTotalFees(BigDecimal totalFees) {
		this.totalFees = totalFees;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalBaseFare
	 */
	public BigDecimal getTotalBaseFare() {
		return totalBaseFare;
	}

	/**
	 * @param totalBaseFare
	 *            the totalBaseFare to set
	 */
	public void setTotalBaseFare(BigDecimal totalBaseFare) {
		this.totalBaseFare = totalBaseFare;
	}

	/**
	 * @return the baseFares
	 */
	public List<BaseFareTO> getBaseFares() {
		if (baseFares == null) {
			baseFares = new ArrayList<BaseFareTO>();
		}
		return baseFares;
	}

	/**
	 * @param baseFares
	 *            the baseFares to set
	 */
	public void setBaseFares(List<BaseFareTO> baseFares) {
		this.baseFares = baseFares;
	}

	/**
	 * @return the totalPriceWithInfant
	 */
	public BigDecimal getTotalPriceWithInfant() {
		return totalPriceWithInfant;
	}

	/**
	 * @param totalPriceWithInfant
	 *            the totalPriceWithInfant to set
	 */
	public void setTotalPriceWithInfant(BigDecimal totalPriceWithInfant) {
		this.totalPriceWithInfant = totalPriceWithInfant;
	}

	public void setOnHoldRestricted(boolean onHoldRestricted) {
		this.onHoldRestricted = onHoldRestricted;
	}

	public boolean isOnHoldRestricted() {
		return onHoldRestricted;
	}

	public Date getOnHoldStartTime() {
		return onHoldStartTime;
	}

	public void setOnHoldStartTime(Date onHoldStartTime) {
		this.onHoldStartTime = onHoldStartTime;
	}

	public Date getOnHoldStopTime() {
		return onHoldStopTime;
	}

	public void setOnHoldStopTime(Date onHoldStopTime) {
		this.onHoldStopTime = onHoldStopTime;
	}

	public void setOnHoldReleaseTimeStamp(Date onHoldReleaseTimeStamp) {
		this.onHoldReleaseTimeStamp = onHoldReleaseTimeStamp;
	}

	public Date getOnHoldReleaseTimeStamp() {
		return onHoldReleaseTimeStamp;
	}

	@Deprecated
	public int getFareType() {
		return fareType;
	}

	@Deprecated
	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	public Map<Integer, Integer> getOndWiseFareType() {
		if (ondWiseFareType == null) {
			ondWiseFareType = new HashMap<Integer, Integer>();
		}
		return ondWiseFareType;
	}

	public void setOndWiseFareType(Map<Integer, Integer> ondWiseFareType) {
		this.ondWiseFareType = ondWiseFareType;
	}

	/**
	 * @return the logicalCCtype
	 */
	public String getLogicalCCtype() {
		return logicalCCtype;
	}

	/**
	 * @param logicalCCtype
	 *            the logicalCCtype to set
	 */
	public void setLogicalCCtype(String logicalCCtype) {
		this.logicalCCtype = logicalCCtype;
	}

	public void setApplicableFareRules(List<FareRuleDTO> applicableFareRules) {
		this.applicableFareRules = applicableFareRules;
	}

	public List<FareRuleDTO> getApplicableFareRules() {
		if (applicableFareRules == null) {
			applicableFareRules = new ArrayList<FareRuleDTO>();
		}
		return applicableFareRules;
	}

	public BigDecimal getPerPaxTotalAgentCommission() {
		return perPaxTotalAgentCommission;
	}

	public void setPerPaxTotalAgentCommission(BigDecimal perPaxTotalAgentCommission) {
		this.perPaxTotalAgentCommission = perPaxTotalAgentCommission;
	}

	public BigDecimal getTotAgentCommission() {
		return totAgentCommission;
	}

	public void setTotAgentCommission(BigDecimal totAgentCommission) {
		this.totAgentCommission = totAgentCommission;
	}

	public FareDiscountTO getFareDiscountInfo() {
		return fareDiscountInfo;
	}

	public void setFareDiscountInfo(FareDiscountTO fareDiscountInfo) {
		this.fareDiscountInfo = fareDiscountInfo;
	}

	public ApplicablePromotionDetailsTO getPromotionTO() {
		return promotionTO;
	}

	public void setPromotionTO(ApplicablePromotionDetailsTO promotionTO) {
		this.promotionTO = promotionTO;
	}

	public BigDecimal getDiscountableFareTotal(Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (BaseFareTO baseF : getBaseFares()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, baseF.getSegmentCode())) {
				value = AccelAeroCalculator.add(value, baseF.getAmount());
			}
		}

		return value;
	}

	public BigDecimal getFarePortionDiscountTotal(float fareDiscountPercentage, Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (BaseFareTO baseF : getBaseFares()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, baseF.getSegmentCode())) {
				BigDecimal discPortion = AccelAeroCalculator.calculatePercentage(baseF.getAmount(), fareDiscountPercentage,
						RoundingMode.DOWN);
				value = AccelAeroCalculator.add(value, discPortion);
			}
		}

		return value;
	}

	public BigDecimal getDiscountedFareAmount(float fareDiscountPercentage, Set<String> applicableOnds) {
		return getFarePortionDiscountTotal(fareDiscountPercentage, applicableOnds);
	}

	public BigDecimal getDomesticDiscountedFareAmount(int fareDiscountPercentage, ArrayList<String> domesticSegmentCodeList) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (fareDiscountPercentage > 0 && domesticSegmentCodeList != null && domesticSegmentCodeList.size() > 0) {
			for (BaseFareTO baseF : getBaseFares()) {
				if (baseF.getSegmentCode() != null && domesticSegmentCodeList.contains(baseF.getSegmentCode())) {
					value = AccelAeroCalculator
							.add(value, AccelAeroCalculator.calculatePercentage(baseF.getAmount(), fareDiscountPercentage,
									RoundingMode.DOWN));
				}

			}
		}
		return value;
	}

	public BigDecimal getDiscountableSurchargeTotal(Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (SurchargeTO surcharge : getSurcharges()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, surcharge.getSegmentCode())) {
				value = AccelAeroCalculator.add(value, surcharge.getAmount());
			}
		}

		return value;
	}

	public BigDecimal getSurchargePortionDiscountTotal(float discountedPercentage, Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (SurchargeTO surcharge : getSurcharges()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, surcharge.getSegmentCode())) {
				BigDecimal discPortion = AccelAeroCalculator.calculatePercentage(surcharge.getAmount(), discountedPercentage,
						RoundingMode.DOWN);
				value = AccelAeroCalculator.add(value, discPortion);
			}
		}

		return value;
	}

	public BigDecimal getDiscountedSurchargeAmount(float discountedPercentage, Set<String> applicableOnds) {
		return getSurchargePortionDiscountTotal(discountedPercentage, applicableOnds);
	}

	public BigDecimal getDiscountableTaxTotal(Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (TaxTO tax : getTaxes()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, tax.getSegmentCode())) {
				value = AccelAeroCalculator.add(value, tax.getAmount());
			}
		}

		return value;
	}

	public BigDecimal getTaxPortionDiscountTotal(float discountedPercentage, Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (TaxTO tax : getTaxes()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, tax.getSegmentCode())) {
				BigDecimal discPortion = AccelAeroCalculator.calculatePercentage(tax.getAmount(), discountedPercentage,
						RoundingMode.DOWN);
				value = AccelAeroCalculator.add(value, discPortion);
			}
		}

		return value;
	}

	public BigDecimal getDiscountedTaxAmount(float discountedPercentage, Set<String> applicableOnds) {
		return getTaxPortionDiscountTotal(discountedPercentage, applicableOnds);
	}

	public BigDecimal getDiscountableFeeTotal(Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (FeeTO fee : getFees()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, fee.getSegmentCode())) {
				value = AccelAeroCalculator.add(value, fee.getAmount());
			}
		}

		return value;
	}

	public BigDecimal getFeePortionDiscountTotal(float discountedPercentage, Set<String> applicableOnds) {
		BigDecimal value = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (FeeTO fee : getFees()) {
			if (StringUtil.isMatchingStringFound(applicableOnds, fee.getSegmentCode())) {
				BigDecimal discPortion = AccelAeroCalculator.calculatePercentage(fee.getAmount(), discountedPercentage,
						RoundingMode.DOWN);
				value = AccelAeroCalculator.add(value, discPortion);
			}
		}

		return value;
	}

	public BigDecimal getDiscountedFeeAmount(float discountedPercentage, Set<String> applicableOnds) {
		return getFeePortionDiscountTotal(discountedPercentage, applicableOnds);
	}

	public List<ONDExternalChargeTO> getOndExternalCharges() {
		if (ondExternalCharges == null) {
			ondExternalCharges = new ArrayList<ONDExternalChargeTO>();
		}
		return ondExternalCharges;
	}

	public void setOndExternalCharges(List<ONDExternalChargeTO> ondExternalCharges) {
		this.ondExternalCharges = ondExternalCharges;
	}

	public boolean hasOndExternalCharge(int ondSequence) {
		if (getOndExternalCharges().size() > ondSequence) {
			return true;
		}
		return false;
	}

	public ONDExternalChargeTO addOndExternalCharge(int ondSequence) {
		return getOndExternalCharge(ondSequence);
	}

	public ONDExternalChargeTO getOndExternalCharge(int ondSequence) {
		while (getOndExternalCharges().size() <= ondSequence) {
			ONDExternalChargeTO ondExternalChargeTO = new ONDExternalChargeTO();
			ondExternalChargeTO.setOndSequence(getOndExternalCharges().size());
			getOndExternalCharges().add(ondExternalChargeTO);
		}
		return getOndExternalCharges().get(ondSequence);
	}

	public Map<Integer, Boolean> getOndFlexiSelection() {
		if (ondFlexiSelection == null) {
			ondFlexiSelection = new HashMap<Integer, Boolean>();
		}
		return ondFlexiSelection;
	}

	public void setOndFlexiSelection(Map<Integer, Boolean> ondFlexiSelection) {
		this.ondFlexiSelection = ondFlexiSelection;
	}

	public boolean isFlexiExists() {
		for (Entry<Integer, Boolean> entry : getOndFlexiSelection().entrySet()) {
			if (entry.getValue()) {
				return true;
			}
		}
		return false;
	}

	public AgentCommissionDetails getApplicableAgentCommissions() {
		return applicableAgentCommissions;
	}

	public void setApplicableAgentCommissions(AgentCommissionDetails applicableAgentCommissions) {
		this.applicableAgentCommissions = applicableAgentCommissions;
	}

	public Map<Integer, Integer> getOndWiseFareId() {

		if (ondWiseFareId == null) {
			ondWiseFareId = new HashMap<Integer, Integer>();
		}
		return ondWiseFareId;
	}

	public void setOndWiseFareId(Map<Integer, Integer> ondWiseFareId) {
		this.ondWiseFareId = ondWiseFareId;
	}

	public List<BundledFareDTO> getOndBundledFareDTOs() {
		return ondBundledFareDTOs;
	}

	public void setOndBundledFareDTOs(List<BundledFareDTO> ondBundledFareDTOs) {
		this.ondBundledFareDTOs = ondBundledFareDTOs;
	}

	public BigDecimal getServiceTaxAmount() {
		return serviceTaxAmount;
	}

	public void setServiceTaxAmount(BigDecimal serviceTaxAmount) {
		this.serviceTaxAmount = serviceTaxAmount;
	}
}
