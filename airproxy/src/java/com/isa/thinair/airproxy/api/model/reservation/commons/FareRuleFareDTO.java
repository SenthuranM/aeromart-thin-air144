package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class FareRuleFareDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fareRuleCode;
	private int fareRuleId;
	private String fareBasisCode;
	private String bookingClassCode;
	private BigDecimal adultFareAmount;
	private BigDecimal childFareAmount;
	private BigDecimal infantFareAmount;
	private boolean adultFareApplicable;
	private boolean childFareApplicable;
	private boolean infantFareApplicable;
	private Integer fareId;
	private Collection<String> visibleAgents;
	private Collection<String> visibleChannelNames;
	private String applicableAgentCommission; // formatted

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public BigDecimal getAdultFareAmount() {
		return adultFareAmount;
	}

	public void setAdultFareAmount(BigDecimal adultFareAmount) {
		this.adultFareAmount = adultFareAmount;
	}

	public BigDecimal getChildFareAmount() {
		return childFareAmount;
	}

	public void setChildFareAmount(BigDecimal childFareAmount) {
		this.childFareAmount = childFareAmount;
	}

	public BigDecimal getInfantFareAmount() {
		return infantFareAmount;
	}

	public void setInfantFareAmount(BigDecimal infantFareAmount) {
		this.infantFareAmount = infantFareAmount;
	}

	public boolean isAdultFareApplicable() {
		return adultFareApplicable;
	}

	public void setAdultFareApplicable(boolean adultFareApplicable) {
		this.adultFareApplicable = adultFareApplicable;
	}

	public boolean isChildFareApplicable() {
		return childFareApplicable;
	}

	public void setChildFareApplicable(boolean childFareApplicable) {
		this.childFareApplicable = childFareApplicable;
	}

	public boolean isInfantFareApplicable() {
		return infantFareApplicable;
	}

	public void setInfantFareApplicable(boolean infantFareApplicable) {
		this.infantFareApplicable = infantFareApplicable;
	}

	public Integer getFareId() {
		return fareId;
	}

	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	public Collection<String> getVisibleAgents() {
		if (visibleAgents == null) {
			visibleAgents = new ArrayList<String>();
		}
		return visibleAgents;
	}

	public void setVisibleAgents(Collection<String> visibleAgents) {
		this.visibleAgents = visibleAgents;
	}

	public Collection<String> getVisibleChannelNames() {
		if (visibleChannelNames == null) {
			visibleChannelNames = new ArrayList<String>();
		}
		return visibleChannelNames;
	}

	public void setVisibleChannelNames(Collection<String> visibleChannelNames) {
		this.visibleChannelNames = visibleChannelNames;
	}

	public boolean hasVisibleAgents() {
		return getVisibleAgents().size() > 0;
	}

	public boolean hasVisibleChannelNames() {
		return getVisibleChannelNames().size() > 0;
	}

	public String getApplicableAgentCommission() {
		return applicableAgentCommission;
	}

	public void setApplicableAgentCommission(String applicableAgentCommission) {
		this.applicableAgentCommission = applicableAgentCommission;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

}
