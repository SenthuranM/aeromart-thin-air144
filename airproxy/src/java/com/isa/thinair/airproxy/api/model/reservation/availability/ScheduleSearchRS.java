package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airschedules.api.dto.FlightScheduleRouteInfo;

/**
 * @author eric
 * 
 */
public class ScheduleSearchRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<FlightScheduleRouteInfo> outboundScheduleRouteInfo;

	private Collection<FlightScheduleRouteInfo> inboundScheduleRouteInfo;

	public Collection<FlightScheduleRouteInfo> getOutboundScheduleRouteInfo() {
		if (outboundScheduleRouteInfo == null) {
			outboundScheduleRouteInfo = new ArrayList<FlightScheduleRouteInfo>();
		}
		return outboundScheduleRouteInfo;
	}

	public Collection<FlightScheduleRouteInfo> getInboundScheduleRouteInfo() {
		if (inboundScheduleRouteInfo == null) {
			inboundScheduleRouteInfo = new ArrayList<FlightScheduleRouteInfo>();

		}
		return inboundScheduleRouteInfo;
	}
}
