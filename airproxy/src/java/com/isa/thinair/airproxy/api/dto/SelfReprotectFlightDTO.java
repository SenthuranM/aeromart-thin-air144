/**
 * 
 */
package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author suneth
 *
 */
public class SelfReprotectFlightDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int flightId;
	
	private String flightNumber;
	
	private Date departureDate;
	
	private Date arrivalDate;
	
	private String fltSegId;
	
	private String segmentCode;

	private String flightDuration;
	
	public SelfReprotectFlightDTO(){
	}

	/**
	 * @return the flightId
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId the flightId to set
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the arrivalDate
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate the arrivalDate to set
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return the fltSegId
	 */
	public String getFltSegId() {
		return fltSegId;
	}

	/**
	 * @param fltSegId the fltSegId to set
	 */
	public void setFltSegId(String fltSegId) {
		this.fltSegId = fltSegId;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}
}
