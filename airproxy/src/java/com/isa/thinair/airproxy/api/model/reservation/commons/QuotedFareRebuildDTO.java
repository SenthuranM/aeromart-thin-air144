package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OnDFareSegChargesRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class QuotedFareRebuildDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<FareSegChargeTO> fareSegCharges = new ArrayList<FareSegChargeTO>();
	private IPaxCountAssembler paxAssm = null;
	private String bookingType;
	private DiscountedFareDetails discountedFareDetails;
	private List<BundledFareDTO> bundledFareDTOs;
	private boolean allowOverbookAfterCutoffTime;

	public QuotedFareRebuildDTO(FareSegChargeTO fareSegChargeTO, IPaxCountAssembler paxAssm,
			DiscountedFareDetails discountedFareDetails, String bookingType, boolean isAllowOverbookAfterCutoffTime) {
		setFareSegCharge(fareSegChargeTO);
		setPaxAssm(paxAssm);
		setBookingType(bookingType);
		setDiscountedFareDetails(discountedFareDetails);
		setAllowOverbookAfterCutoffTime(isAllowOverbookAfterCutoffTime);
	}

	public QuotedFareRebuildDTO(FareSegChargeTO fareSegChargeTO, BaseAvailRQ baseAvailRQ,
			DiscountedFareDetails discountedFareDetails) {
		setFareSegCharge(fareSegChargeTO);
		setPaxAssm(new PaxCountAssembler(baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList()));
		setBookingType(baseAvailRQ.getTravelPreferences().getBookingType());
		setDiscountedFareDetails(discountedFareDetails);
		setAllowOverbookAfterCutoffTime(baseAvailRQ.getAvailPreferences().isAllowDoOverbookAfterCutOffTime());
	}

	public QuotedFareRebuildDTO(List<FareSegChargeTO> fareSegChargeTOs, IPaxCountAssembler paxAssm,
			DiscountedFareDetails discountedFareDetails, String bookingType) {
		setFareSegCharges(fareSegChargeTOs);
		setPaxAssm(paxAssm);
		setBookingType(bookingType);
		setDiscountedFareDetails(discountedFareDetails);
	}

	private void setFareSegCharges(List<FareSegChargeTO> fareSegChargeTOs) {
		if (fareSegChargeTOs != null) {
			this.fareSegCharges = fareSegChargeTOs;
		}
	}

	private void setFareSegCharge(FareSegChargeTO fareSegCharge) {
		this.fareSegCharges.add(fareSegCharge);
	}

	private void setPaxAssm(IPaxCountAssembler paxAssm) {
		this.paxAssm = paxAssm;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getBookingType() {
		return bookingType;
	}

	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;
	}

	private Iterator<Map<String, BigDecimal>> getDividePaxFareWithoutLoss(Map<String, BigDecimal> paxFares, int ondCount) {
		List<Map<String, BigDecimal>> lossLessOverridePaxFares = new ArrayList<Map<String, BigDecimal>>();

		BigDecimal adtTotalAmt = paxFares.get(PaxTypeTO.ADULT);
		BigDecimal chdTotalAmt = paxFares.get(PaxTypeTO.CHILD);
		BigDecimal infTotalAmt = paxFares.get(PaxTypeTO.INFANT);
		BigDecimal[] adtPerPaxAmts = AccelAeroCalculator.roundAndSplit(adtTotalAmt, ondCount);
		BigDecimal[] chdPerPaxAmts = AccelAeroCalculator.roundAndSplit(chdTotalAmt, ondCount);
		BigDecimal[] infPerPaxAmts = AccelAeroCalculator.roundAndSplit(infTotalAmt, ondCount);
		for (int i = 0; i < ondCount; i++) {
			Map<String, BigDecimal> ondPaxOverride = new HashMap<String, BigDecimal>();
			ondPaxOverride.put(PaxTypeTO.ADULT, adtPerPaxAmts[i]);
			ondPaxOverride.put(PaxTypeTO.CHILD, chdPerPaxAmts[i]);
			ondPaxOverride.put(PaxTypeTO.INFANT, infPerPaxAmts[i]);
			lossLessOverridePaxFares.add(ondPaxOverride);
		}
		return lossLessOverridePaxFares.iterator();
	}

	public List<OndRebuildCriteria> getOndRebuildCriterias() {
		List<OndRebuildCriteria> criterias = new ArrayList<OndRebuildCriteria>();
		if (paxAssm != null) {
			for (FareSegChargeTO fareSegCharge : fareSegCharges) {
				OndRebuildCriteria criteria = new OndRebuildCriteria();

				criteria.setTotalJourneyFare(fareSegCharge.getTotalJourneyFare());
				criteria.setTotlaAdults(paxAssm.getAdultCount());
				criteria.setTotalChildrens(paxAssm.getChildCount());
				criteria.setTotalInfants(paxAssm.getInfantCount());

				criteria.setBlockSeats(fareSegCharge.hasBlockSeats());
				criteria.setBookingType(bookingType);
				criteria.setAllowOverbookAfterCutoffTime(allowOverbookAfterCutoffTime);

				boolean overridePaxFare = false;
				Iterator<Map<String, BigDecimal>> ondWiseOverridePaxFaresIterator = null;

				overridePaxFare = fareSegCharge.getOverridePaxFares() != null && fareSegCharge.getOverridePaxFares().size() > 0;
				if (overridePaxFare) {
					ondWiseOverridePaxFaresIterator = getDividePaxFareWithoutLoss(fareSegCharge.getOverridePaxFares(),
							fareSegCharge.getOndFareSegChargeTOs().size());
				}

				List<BundledFareDTO> selectedOndBundledFareDTOs = fareSegCharge.getOndBundledFareDTOs();

				for (OndFareSegChargeTO ondFareSegChargeTO : fareSegCharge.getOndFareSegChargeTOs()) {
					OnDFareSegChargesRebuildCriteria ondCriteria = new OnDFareSegChargesRebuildCriteria();
					ondCriteria.setFareID(ondFareSegChargeTO.getFareID());
					ondCriteria.setFarePercentage(ondFareSegChargeTO.getFarePercentage());
					ondCriteria.setFareType(ondFareSegChargeTO.getFareType());
					// setting flags
					ondCriteria.setInbound(ondFareSegChargeTO.isInbound());
					ondCriteria.setOndSequence(ondFareSegChargeTO.getOndSequence());
					ondCriteria.setRequestedOndSequence(ondFareSegChargeTO.getRequestedOndSequence());
					ondCriteria.setModifyInbound(ondFareSegChargeTO.isModifyInbound());
					ondCriteria.setOpenReturn(ondFareSegChargeTO.isOpenReturn());

					ondCriteria.setFsegBCAlloc(ondFareSegChargeTO.getFsegBCAlloc());
					ondCriteria.setChargeRateIds(ondFareSegChargeTO.getChargeRateIds());
					ondCriteria.setFlexiChargeIds(ondFareSegChargeTO.getFlexiChargeIds());
					ondCriteria.setSubJourneyGroup(ondFareSegChargeTO.getSubJourneyGroup());
					ondCriteria.setTotalSubJourneyFare(
							fareSegCharge.getSubJourneyTotalFare(ondFareSegChargeTO.getSubJourneyGroup()));
					if (!ondFareSegChargeTO.isBusSegmentExists() && selectedOndBundledFareDTOs != null
							&& !selectedOndBundledFareDTOs.isEmpty()
							&& selectedOndBundledFareDTOs.size() > ondFareSegChargeTO.getOndSequence()) {
						ondCriteria.setBundledFareDTO(selectedOndBundledFareDTOs.get(ondFareSegChargeTO.getOndSequence()));
					}

					if (overridePaxFare) {
						ondCriteria.setOverridePaxFares(ondWiseOverridePaxFaresIterator.next());
					}
					
					ondCriteria.setSplitOperationForBusInvoked(ondFareSegChargeTO.isSplitOperationForBusInvoked());
					criteria.addOndCriteria(ondCriteria);
				}
				criterias.add(criteria);
			}
		}
		return criterias;
	}

	public OndRebuildCriteria getFirstOndRebuildCriteria() {
		List<OndRebuildCriteria> criterias = getOndRebuildCriterias();
		if (criterias != null && criterias.size() > 0) {
			return criterias.get(0);
		}
		return null;
	}

	public boolean hasOndFares() {
		if (fareSegCharges.size() > 0 && paxAssm != null) {
			return true;
		}
		return false;
	}

	public Collection<TempSegBcAlloc> getBlockSeats() {
		if (fareSegCharges.isEmpty()) {
			return null;
		}
		Collection<TempSegBcAlloc> blockSeats = new ArrayList<TempSegBcAlloc>();
		for (FareSegChargeTO fareSegCharge : fareSegCharges) {
			blockSeats.addAll(fareSegCharge.getBlockSeatIds());
		}

		Collection<TempSegBcAlloc> uniqueBlockSeats = new ArrayList<TempSegBcAlloc>();
		if (blockSeats.size() > 1) {
			Iterator<TempSegBcAlloc> iterator = blockSeats.iterator();

			while (iterator.hasNext()) {
				TempSegBcAlloc tempBlockSeat = iterator.next();
				if (!uniqueBlockSeats.contains(tempBlockSeat))
					uniqueBlockSeats.add(tempBlockSeat);
			}
		} else {
			uniqueBlockSeats = blockSeats;
		}
		return uniqueBlockSeats;

	}

	public boolean hasBlockSeats() {
		for (FareSegChargeTO fareSegCharge : fareSegCharges) {
			if (fareSegCharge.hasBlockSeats()) {
				return true;
			}
		}
		return false;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public void setAllowOverbookAfterCutoffTime(boolean allowOverbookAfterCutoffTime) {
		this.allowOverbookAfterCutoffTime = allowOverbookAfterCutoffTime;
	}
}
