package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data object contains the Origin destination informations Per a given OndInformation Object will hold a collection of
 * OND Options from both interline and own airline
 * 
 * {@see OriginDestinationOptionTO}
 * 
 */
public class OriginDestinationInformationTO implements Serializable, Comparable<OriginDestinationInformationTO> {
	private static Log log = LogFactory.getLog(OriginDestinationInformationTO.class);

	private static final long serialVersionUID = 1L;

	private String origin;

	private String destination;

	private Date departureDateTimeStart;

	private Date departureDateTimeEnd;

	private Date preferredDate;

	private Date arrivalDateTimeStart;

	private Date arrivalDateTimeEnd;

	private String preferredClassOfService;

	private String preferredLogicalCabin;

	private boolean returnFlag;

	private List<OriginDestinationOptionTO> originDestinationOptions = null;

	/**
	 * Segment fare options. This is only applicable if originDestinationOptions are for a return fare.
	 */
	private List<OriginDestinationOptionTO> originDestinationSegFareOptions = null;

	private String preferredBookingType;

	private String preferredBookingClass;

	private Integer preferredBundleFarePeriodId;

	private List<Integer> existingFlightSegIds;

	private List<String> existingPnrSegRPHs;

	private boolean flownOnd = false;

	private BigDecimal oldPerPaxFare = BigDecimal.ZERO;

	private List<FareTO> oldPerPaxFareTOList;

	private List<Integer> dateChangedResSegList;

	private List<String> unTouchedResSegList;

	private boolean eligibleToSameBCMod = false;

	private String status;

	private boolean sameFlightModification;

	private boolean unTouchedOnd = false;

	private Map<String, Integer> hubTimeDetailMap;

	private Map<String, String> segmentBookingClassSelection;

	private boolean isSpecificFlightsAvailability;

	private boolean isFlexiSelected;
	
	private boolean departureCitySearch;
	
	private boolean arrivalCitySearch;

	public String getSelectedFullONDCode() {
		if (originDestinationOptions != null) {
			for (OriginDestinationOptionTO ondOpt : getOrignDestinationOptions()) {
				if (ondOpt.isSelected()) {
					return ondOpt.getOndCode();
				}
			}
		}
		return null;
	}

	public boolean isFlexiSelected() {
		return isFlexiSelected;
	}

	public void setFlexiSelected(boolean isFlexiSelected) {
		this.isFlexiSelected = isFlexiSelected;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public List<OriginDestinationOptionTO> getOrignDestinationOptions() {
		if (originDestinationOptions == null)
			originDestinationOptions = new ArrayList<OriginDestinationOptionTO>();
		return originDestinationOptions;
	}

	public void setOrignDestinationOptions(List<OriginDestinationOptionTO> orignDestinationOptions) {
		this.originDestinationOptions = orignDestinationOptions;
	}

	public Date getDepartureDateTimeStart() {
		return departureDateTimeStart;
	}

	public void setDepartureDateTimeStart(Date departureDateTimeStart) {
		this.departureDateTimeStart = departureDateTimeStart;
	}

	public Date getDepartureDateTimeEnd() {
		return departureDateTimeEnd;
	}

	public void setDepartureDateTimeEnd(Date departureDateTimeEnd) {
		this.departureDateTimeEnd = departureDateTimeEnd;
	}

	public Date getArrivalDateTimeStart() {
		return arrivalDateTimeStart;
	}

	public void setArrivalDateTimeStart(Date arrivalDateTimeStart) {
		this.arrivalDateTimeStart = arrivalDateTimeStart;
	}

	public Date getArrivalDateTimeEnd() {
		return arrivalDateTimeEnd;
	}

	public void setArrivalDateTimeEnd(Date arrivalDateTimeEnd) {
		this.arrivalDateTimeEnd = arrivalDateTimeEnd;
	}

	public Date getPreferredDate() {
		return preferredDate;
	}

	public void setPreferredDate(Date preferredDate) {
		this.preferredDate = preferredDate;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getPreferredClassOfService() {
		return preferredClassOfService;
	}

	public void setPreferredClassOfService(String preferredClassOfService) {
		this.preferredClassOfService = preferredClassOfService;
	}

	public String getPreferredLogicalCabin() {
		return preferredLogicalCabin;
	}

	public void setPreferredLogicalCabin(String preferredLogicalCabin) {
		this.preferredLogicalCabin = preferredLogicalCabin;
	}

	public void setPreferredBookingClass(String preferredBookingClass) {
		this.preferredBookingClass = preferredBookingClass;
	}

	public void setPreferredBookingType(String preferredBookingType) {
		this.preferredBookingType = preferredBookingType;
	}

	public String getPreferredBookingType() {
		return preferredBookingType;
	}

	public String getPreferredBookingClass() {
		return preferredBookingClass;
	}
	
	public Integer getPreferredBundleFarePeriodId() {
		return preferredBundleFarePeriodId;
	}

	public void setPreferredBundleFarePeriodId(Integer preferredBundleFarePeriodId) {
		this.preferredBundleFarePeriodId = preferredBundleFarePeriodId;
	}

	@Override
	public int compareTo(OriginDestinationInformationTO o) {
		Date comparableOtherDate = o.getDepartureDateTimeStart();
		Date comparableThisDate = this.getDepartureDateTimeStart();
		if ((this.isSpecificFlightsAvailability() || o.isSpecificFlightsAvailability) && o.getPreferredDate() != null
				&& this.getPreferredDate() != null) {
			comparableOtherDate = o.getPreferredDate();
			comparableThisDate = this.getPreferredDate();
		}
		// For open return segments we dont have departure date time etc..
		if (comparableOtherDate != null && comparableThisDate != null) {
			return comparableThisDate.compareTo(comparableOtherDate);
		}
		return 0;
	}

	public List<Integer> getExistingFlightSegIds() {
		if (existingFlightSegIds == null) {
			existingFlightSegIds = new ArrayList<Integer>();
		}
		return existingFlightSegIds;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public List<String> getExistingPnrSegRPHs() {
		if (existingPnrSegRPHs == null) {
			existingPnrSegRPHs = new ArrayList<String>();
		}
		return existingPnrSegRPHs;
	}

	public List<OriginDestinationOptionTO> getOriginDestinationSegFareOptions() {
		if (originDestinationSegFareOptions == null)
			originDestinationSegFareOptions = new ArrayList<OriginDestinationOptionTO>();
		return originDestinationSegFareOptions;
	}

	public void setOriginDestinationSegFareOptions(List<OriginDestinationOptionTO> originDestinationSegFareOptions) {
		this.originDestinationSegFareOptions = originDestinationSegFareOptions;
	}

	public BigDecimal getOldPerPaxFare() {
		return oldPerPaxFare;
	}

	public void setOldPerPaxFare(BigDecimal oldPerPaxFare) {
		this.oldPerPaxFare = oldPerPaxFare;
	}

	public List<FareTO> getOldPerPaxFareTOList() {
		return oldPerPaxFareTOList;
	}

	public void setOldPerPaxFareTOList(List<FareTO> oldPerPaxFareTOList) {
		this.oldPerPaxFareTOList = oldPerPaxFareTOList;
	}

	public List<Integer> getDateChangedResSegList() {
		return dateChangedResSegList;
	}

	public void setDateChangedResSegList(List<Integer> dateChangedResSegList) {
		this.dateChangedResSegList = dateChangedResSegList;
	}

	public boolean isEligibleToSameBCMod() {
		return eligibleToSameBCMod;
	}

	public void setEligibleToSameBCMod(boolean eligibleToSameBCMod) {
		this.eligibleToSameBCMod = eligibleToSameBCMod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	public boolean isSameFlightModification() {
		return sameFlightModification;
	}

	public void setSameFlightModification(boolean sameFlightModification) {
		this.sameFlightModification = sameFlightModification;
	}

	public Map<String, Integer> getHubTimeDetailMap() {
		return hubTimeDetailMap;
	}

	public void setHubTimeDetailMap(Map<String, Integer> hubTimeDetailMap) {
		this.hubTimeDetailMap = hubTimeDetailMap;
	}

	public List<String> getUnTouchedResSegList() {
		return unTouchedResSegList;
	}

	public void setUnTouchedResSegList(List<String> unTouchedResSegList) {
		this.unTouchedResSegList = unTouchedResSegList;
	}

	public void setSegmentBookingClassSelection(Map<String, String> segmentBookingClassSelection) {
		this.segmentBookingClassSelection = segmentBookingClassSelection;
	}

	public Map<String, String> getSegmentBookingClassSelection() {
		return segmentBookingClassSelection;
	}

	public OriginDestinationInformationTO cloneForSplit() {
		OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();

		ondInfoTO.setOrigin(this.getOrigin());
		ondInfoTO.setDestination(this.getDestination());
		ondInfoTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoTO.setDepartureDateTimeEnd(this.getDepartureDateTimeEnd());
		ondInfoTO.setPreferredDate(this.getPreferredDate());
		ondInfoTO.setArrivalDateTimeStart(this.getArrivalDateTimeStart());
		ondInfoTO.setArrivalDateTimeEnd(this.getArrivalDateTimeEnd());
		ondInfoTO.setPreferredClassOfService(this.getPreferredClassOfService());
		ondInfoTO.setPreferredLogicalCabin(this.getPreferredLogicalCabin());
		ondInfoTO.setReturnFlag(this.isReturnFlag());
		// ondInfoTO.setOrignDestinationOptions(this.getOrignDestinationOptions());
		// ondInfoTO.setOriginDestinationSegFareOptions(this.getOriginDestinationSegFareOptions());
		// ondInfoTO.getExistingFlightSegIds().addAll(this.getExistingFlightSegIds());
		ondInfoTO.getExistingPnrSegRPHs().addAll(this.getExistingPnrSegRPHs());
		ondInfoTO.setFlownOnd(this.isFlownOnd());
		ondInfoTO.setOldPerPaxFare(this.getOldPerPaxFare());
		ondInfoTO.setOldPerPaxFareTOList(this.getOldPerPaxFareTOList());
		ondInfoTO.setDateChangedResSegList(this.getDateChangedResSegList());
		ondInfoTO.setUnTouchedResSegList(this.getUnTouchedResSegList());
		ondInfoTO.setEligibleToSameBCMod(this.isEligibleToSameBCMod());
		ondInfoTO.setStatus(this.getStatus());
		ondInfoTO.setSameFlightModification(this.isSameFlightModification());
		ondInfoTO.setUnTouchedOnd(this.isUnTouchedOnd());
		ondInfoTO.setHubTimeDetailMap(this.getHubTimeDetailMap());
		ondInfoTO.setPreferredBookingClass(this.getPreferredBookingClass());
		ondInfoTO.setPreferredBookingType(this.getPreferredBookingType());
		ondInfoTO.setDepartureCitySearch(this.isDepartureCitySearch());
		ondInfoTO.setArrivalCitySearch(this.isArrivalCitySearch());

		return ondInfoTO;
	}

	public OriginDestinationInformationTO clone() {
		OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();

		ondInfoTO.setOrigin(this.getOrigin());
		ondInfoTO.setDestination(this.getDestination());
		ondInfoTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoTO.setDepartureDateTimeEnd(this.getDepartureDateTimeEnd());
		ondInfoTO.setPreferredDate(this.getPreferredDate());
		ondInfoTO.setArrivalDateTimeStart(this.getArrivalDateTimeStart());
		ondInfoTO.setArrivalDateTimeEnd(this.getArrivalDateTimeEnd());
		ondInfoTO.setPreferredClassOfService(this.getPreferredClassOfService());
		ondInfoTO.setPreferredLogicalCabin(this.getPreferredLogicalCabin());
		ondInfoTO.setReturnFlag(this.isReturnFlag());
		ondInfoTO.setOrignDestinationOptions(this.getOrignDestinationOptions());
		ondInfoTO.setOriginDestinationSegFareOptions(this.getOriginDestinationSegFareOptions());
		ondInfoTO.getExistingFlightSegIds().addAll(this.getExistingFlightSegIds());
		ondInfoTO.getExistingPnrSegRPHs().addAll(this.getExistingPnrSegRPHs());
		ondInfoTO.setFlownOnd(this.isFlownOnd());
		ondInfoTO.setOldPerPaxFare(this.getOldPerPaxFare());
		ondInfoTO.setOldPerPaxFareTOList(this.getOldPerPaxFareTOList());
		ondInfoTO.setDateChangedResSegList(this.getDateChangedResSegList());
		ondInfoTO.setUnTouchedResSegList(this.getUnTouchedResSegList());
		ondInfoTO.setEligibleToSameBCMod(this.isEligibleToSameBCMod());
		ondInfoTO.setStatus(this.getStatus());
		ondInfoTO.setSameFlightModification(this.isSameFlightModification());
		ondInfoTO.setUnTouchedOnd(this.isUnTouchedOnd());
		ondInfoTO.setHubTimeDetailMap(this.getHubTimeDetailMap());
		ondInfoTO.setPreferredBookingClass(this.getPreferredBookingClass());
		ondInfoTO.setPreferredBookingType(this.getPreferredBookingType());
		ondInfoTO.setDepartureCitySearch(this.isDepartureCitySearch());
		ondInfoTO.setArrivalCitySearch(this.isArrivalCitySearch());

		return ondInfoTO;
	}

	public OriginDestinationInformationTO ondMerge(OriginDestinationInformationTO nextOndInfoTO) {
		OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();

		ondInfoTO.setOrigin(this.getOrigin());
		ondInfoTO.setDestination(nextOndInfoTO.getDestination());
		ondInfoTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoTO.setDepartureDateTimeEnd(this.getDepartureDateTimeEnd());
		ondInfoTO.setPreferredDate(this.getPreferredDate());
		ondInfoTO.setArrivalDateTimeStart(nextOndInfoTO.getArrivalDateTimeStart());
		ondInfoTO.setArrivalDateTimeEnd(nextOndInfoTO.getArrivalDateTimeEnd());
		ondInfoTO.setPreferredClassOfService(this.getPreferredClassOfService());
		ondInfoTO.setPreferredLogicalCabin(this.getPreferredLogicalCabin());
		ondInfoTO.setReturnFlag(this.isReturnFlag());

		ondInfoTO.setOrignDestinationOptions(this.getOrignDestinationOptions());
		if (nextOndInfoTO.getOrignDestinationOptions() != null) {

			Iterator<OriginDestinationOptionTO> ondOptionItr = ondInfoTO.getOrignDestinationOptions().iterator();
			while (ondOptionItr.hasNext()) {
				OriginDestinationOptionTO ondOptionTo = ondOptionItr.next();
				ondOptionTo.getFlightSegmentList()
						.addAll(nextOndInfoTO.getOrignDestinationOptions().get(0).getFlightSegmentList());

			}
		}

		ondInfoTO.setOriginDestinationSegFareOptions(this.getOriginDestinationSegFareOptions());
		if (ondInfoTO.getOriginDestinationSegFareOptions() == null
				&& nextOndInfoTO.getOriginDestinationSegFareOptions() != null) {
			ondInfoTO.setOriginDestinationSegFareOptions(nextOndInfoTO.getOriginDestinationSegFareOptions());
		}

		ondInfoTO.getExistingFlightSegIds().addAll(this.getExistingFlightSegIds());
		ondInfoTO.getExistingFlightSegIds().addAll(nextOndInfoTO.getExistingFlightSegIds());

		ondInfoTO.getExistingPnrSegRPHs().addAll(this.getExistingPnrSegRPHs());
		ondInfoTO.getExistingPnrSegRPHs().addAll(nextOndInfoTO.getExistingPnrSegRPHs());

		ondInfoTO.setOldPerPaxFare(this.getOldPerPaxFare());
		ondInfoTO.setOldPerPaxFareTOList(null);

		ondInfoTO.setDateChangedResSegList(this.getDateChangedResSegList());
		if (nextOndInfoTO.getDateChangedResSegList() != null) {

			if (ondInfoTO.getDateChangedResSegList() == null) {
				ondInfoTO.setDateChangedResSegList(nextOndInfoTO.getDateChangedResSegList());
			} else {
				ondInfoTO.getDateChangedResSegList().addAll(nextOndInfoTO.getDateChangedResSegList());
			}

		}

		ondInfoTO.setUnTouchedResSegList(this.getUnTouchedResSegList());
		if (nextOndInfoTO.getUnTouchedResSegList() != null) {

			if (ondInfoTO.getUnTouchedResSegList() == null) {
				ondInfoTO.setUnTouchedResSegList(nextOndInfoTO.getUnTouchedResSegList());
			} else {
				ondInfoTO.getUnTouchedResSegList().addAll(nextOndInfoTO.getUnTouchedResSegList());
			}

		}

		ondInfoTO.setStatus(this.getStatus());
		ondInfoTO.setUnTouchedOnd(false);
		ondInfoTO.setPreferredBookingClass(this.getPreferredBookingClass());
		ondInfoTO.setPreferredBookingType(this.getPreferredBookingType());
		ondInfoTO.setDepartureCitySearch(this.isDepartureCitySearch());
		ondInfoTO.setArrivalCitySearch(this.isArrivalCitySearch());

		return ondInfoTO;
	}

	public boolean isSpecificFlightsAvailability() {
		return isSpecificFlightsAvailability;
	}

	public void setSpecificFlightsAvailability(boolean isSpecificFlightsAvailability) {
		this.isSpecificFlightsAvailability = isSpecificFlightsAvailability;
	}

	public void fixMissingOndSequence(int ondSequence) {
		for (OriginDestinationOptionTO ondOption : getOrignDestinationOptions()) {
			ondOption.fixMissingOndSequence(ondSequence);
		}
		for (OriginDestinationOptionTO ondOption : getOriginDestinationSegFareOptions()) {
			ondOption.fixMissingOndSequence(ondSequence);
		}
	}	

	public boolean isDepartureCitySearch() {
		return departureCitySearch;
	}

	public void setDepartureCitySearch(boolean departureCitySearch) {
		this.departureCitySearch = departureCitySearch;
	}

	public boolean isArrivalCitySearch() {
		return arrivalCitySearch;
	}

	public void setArrivalCitySearch(boolean arrivalCitySearch) {
		this.arrivalCitySearch = arrivalCitySearch;
	}
	
}
