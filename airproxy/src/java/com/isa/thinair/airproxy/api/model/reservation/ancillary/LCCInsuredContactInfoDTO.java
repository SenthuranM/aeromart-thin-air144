package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

/**
 * @author primal
 * 
 */

public class LCCInsuredContactInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ContactPerson;
	private String Address1;
	private String Address2;
	private String Address3;
	private String HomePhoneNum;
	private String MobilePhoneNum;
	private String OtherPhoneNum;
	private String PostCode;
	private String City;
	private String State;
	private String Country;
	private String EmailAddress;
	private String prefLanguage;

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

	public String getHomePhoneNum() {
		return HomePhoneNum;
	}

	public void setHomePhoneNum(String homePhoneNum) {
		HomePhoneNum = homePhoneNum;
	}

	public String getMobilePhoneNum() {
		return MobilePhoneNum;
	}

	public void setMobilePhoneNum(String mobilePhoneNum) {
		MobilePhoneNum = mobilePhoneNum;
	}

	public String getOtherPhoneNum() {
		return OtherPhoneNum;
	}

	public void setOtherPhoneNum(String otherPhoneNum) {
		OtherPhoneNum = otherPhoneNum;
	}

	public String getPostCode() {
		return PostCode;
	}

	public void setPostCode(String postCode) {
		PostCode = postCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPrefLanguage() {
		return prefLanguage;
	}

	public void setPrefLanguage(String prefLanguage) {
		this.prefLanguage = prefLanguage;
	}

}
