package com.isa.thinair.airproxy.api.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airsecurity.api.model.User;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCCPaymentMetaInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Nilindra Fernando
 */
public class ConfirmReservationUtil {

	private static Log log = LogFactory.getLog(ConfirmReservationUtil.class);

	public static boolean isReservationConfirmationSuccess(IPGQueryDTO ipgQueryDTO) {

		return isConfirmationSuccess(ipgQueryDTO, false);

	}

	public static boolean isConfirmationSuccess(IPGQueryDTO ipgQueryDTO, boolean isConfirmed) {

		if (!AppSysParamsUtil.isConfirmReservationIfPaymentExistsAtIPG()) {
			return false;
		}
		if (!isReservationExist(ipgQueryDTO)) {
			return false;
		}

		boolean isSuccess = false;

		try {
			boolean isGroupPNR = ipgQueryDTO.getGroupPnr() != null;
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())
					|| ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())) {

				ExternalChgDTO externalChgDTO = null;

				String pnr = ipgQueryDTO.getPnr();
				String groupPnr = ipgQueryDTO.getGroupPnr() != null ? ipgQueryDTO.getGroupPnr() : ipgQueryDTO.getPnr();
				BigDecimal totalCreditCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

				LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, isGroupPNR, true,
						getApplicationEngine(ipgQueryDTO.getAppIndicatorEnum()));
				LCCClientReservation commonReservation = AirproxyModuleUtils.getAirproxyReservationBD()
						.searchReservationByPNR(pnrModesDTO, null, getTrackingInfo(ipgQueryDTO));
				BigDecimal totalReservationBalance = getTotalAvailableBalance(commonReservation);
				if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
					externalChgDTO = getCreditCardChargeMap(commonReservation, totalReservationBalance, ipgQueryDTO);
					if (externalChgDTO != null) {
						totalCreditCardCharge = externalChgDTO.getAmount();
					}
				}

				int tempTransactionId = ipgQueryDTO.getTemporyPaymentId();

				boolean isLMSBlockCreditEnabled = AirproxyModuleUtils.getChargeBD()
						.isEnableLMSCreditBlock(tempTransactionId);

				LmsBlockedCredit lmsBlockedCredit = null;
				BigDecimal totalAmount = ipgQueryDTO.getBaseAmount();
				boolean hasLMSCreditPayment = false;
				if (AppSysParamsUtil.isLMSEnabled() && isLMSBlockCreditEnabled) {
					lmsBlockedCredit = AirproxyModuleUtils.getReservationBD()
							.getLmsBlockCreditInfoByTmpTnxId(tempTransactionId);
					if (lmsBlockedCredit != null && lmsBlockedCredit.getBlockedCreditAmount().doubleValue() > 0) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lmsBlockedCredit.getBlockedCreditAmount());
						hasLMSCreditPayment = true;
					}
				}

				if (checkAmountsWithInRange(totalAmount,
						AccelAeroCalculator.add(totalReservationBalance, totalCreditCardCharge))) {
					IPGIdentificationParamsDTO ipgDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
					PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(ipgQueryDTO);
					if (ipgQueryDTO.geteDirhamFee() != null
							&& ipgQueryDTO.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero())
							> 0) {
						payCurrencyDTO.seteDirhamFee(ipgQueryDTO.geteDirhamFee());
					}
					LCCClientBalancePayment balancePayment = getBalancePayment(groupPnr, payCurrencyDTO, ipgDTO,
							ipgQueryDTO, commonReservation, externalChgDTO, lmsBlockedCredit);

					Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds = getTemporaryPaymentIds(ipgQueryDTO,
							payCurrencyDTO, ipgDTO);
					balancePayment.setTemporyPaymentMap(mapTnxIds);

					ServiceResponce response = AirproxyModuleUtils.getAirproxyReservationBD()
							.balancePaymentWithTransaction(balancePayment, commonReservation.getVersion(), isGroupPNR,
									false, getClientInfoDTO(), getTrackingInfo(ipgQueryDTO));
					if (!isConfirmed) {
						commonReservation = (LCCClientReservation) response
								.getResponseParam(CommandParamNames.RESERVATION);
					}

					try {
						if (response.isSuccess() && hasLMSCreditPayment
								&& ReservationInternalConstants.ReservationStatus.CONFIRMED
								.equals(commonReservation.getStatus())) {
							AirproxyModuleUtils.getReservationBD().updateLMSCreditUtilizationSuccess(tempTransactionId);
						}
					} catch (Exception e) {
						log.error("ERROR @ UPDATING LMS CREDIT STATUS >> PNR CREATED");
					}

					isSuccess = true;
					AirproxyModuleUtils.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO,
							composeCommonItineraryParamDTO(isGroupPNR, commonReservation, ipgQueryDTO),
							getTrackingInfo(ipgQueryDTO));
				}
			}

		} catch (Exception e) {
			log.error("isReservationConfirmationSuccess", e);
		}

		return isSuccess;
	}



	private static boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}

	// Assume we are going to confirm fresh ibe onhold
	private static ExternalChgDTO getCreditCardChargeMap(LCCClientReservation commonReservation,
			BigDecimal totalReservationBalance, IPGQueryDTO ipgQueryDTO) throws ModuleException {
		int payablePaxCount = getPayablePaxCount(commonReservation);
		int segmentCount = getSegmentCount(commonReservation);

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, ChargeRateOperationType.MAKE_ONLY);
		ExternalChgDTO ccChgDTO_Default = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);

		PaymentGatewayWiseCharges pgwCharge = null;
		String pgwName = (ipgQueryDTO == null) ? null : ipgQueryDTO.getGatewayName();

		if (pgwName != null && pgwName.length() > 2) {
			int ipgId = Integer.parseInt(ipgQueryDTO.getGatewayName().split("_")[0]);
			pgwCharge = AirproxyModuleUtils.getChargeBD().getPaymentGatewayWiseCharge(ipgId);
		}

		// applying paymentgateway wise charges, if pgw wise charges are defined for this particular pgw
		if (pgwCharge != null && pgwCharge.getChargeId() > 0) {
			// valid charge is defined for the selected pgw
			if (pgwCharge.getValuePercentageFlag().equals("P")) {
				ccChgDTO_Default.setRatioValueInPercentage(true);
				ccChgDTO_Default.setRatioValue(new BigDecimal(pgwCharge.getChargeValuePercentage()));
				ccChgDTO_Default.calculateAmount(totalReservationBalance);
			} else {
				ccChgDTO_Default.setRatioValueInPercentage(false);
				ccChgDTO_Default.setRatioValue(new BigDecimal(pgwCharge.getValueInDefaultCurrency()));
				ccChgDTO_Default.calculateAmount(payablePaxCount, segmentCount);
			}

		} else {

			if (ccChgDTO_Default.isRatioValueInPercentage()) {
				ccChgDTO_Default.calculateAmount(totalReservationBalance);
			} else {
				ccChgDTO_Default.calculateAmount(payablePaxCount, segmentCount);
			}
		}

		if (ipgQueryDTO.geteDirhamFee() != null
				&& ipgQueryDTO.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			ccChgDTO_Default.setRatioValueInPercentage(false);
			ccChgDTO_Default.setRatioValue(ipgQueryDTO.geteDirhamFee());
			ccChgDTO_Default.setAmount(ipgQueryDTO.geteDirhamFee());
		}

		Set<ServiceTaxContainer> serviceTaxList = commonReservation.getApplicableServiceTaxes();
		if (serviceTaxList != null && serviceTaxList.size() > 0) {
			for (ServiceTaxContainer serviceTaxContainer : serviceTaxList) {
				if (serviceTaxContainer.getExternalCharge().equals(EXTERNAL_CHARGES.JN_OTHER)
						&& serviceTaxContainer.isTaxApplicable()) {

					BigDecimal totalJnTax = AccelAeroCalculator.getDefaultBigDecimalZero();

					BigDecimal serviceCharge = ccChgDTO_Default.getAmount();
					if (AccelAeroCalculator.isGreaterThan(serviceCharge, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal serviceJNTax = AccelAeroCalculator.multiplyDefaultScale(serviceCharge,
								serviceTaxContainer.getTaxRatio());
						totalJnTax = AccelAeroCalculator.add(totalJnTax, serviceJNTax);
					}

					if (AccelAeroCalculator.isGreaterThan(totalJnTax, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						ccChgDTO_Default.setAmount(AccelAeroCalculator.add(ccChgDTO_Default.getAmount(), totalJnTax));
					}

				}
			}
		}

		return ccChgDTO_Default;
	}

	private static int getPayablePaxCount(LCCClientReservation commonReservation) {
		int paxCount = 0;
		if (commonReservation != null) {
			boolean isInfantPaymentSeparated = commonReservation.isInfantPaymentSeparated();
			if (commonReservation.getPassengers() != null) {
				for (LCCClientReservationPax pax : commonReservation.getPassengers()) {
					if (!isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						continue;
					}

					if (pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) >= 0) {
						paxCount++;
					}
				}
			} else {
				if (log.isInfoEnabled()) {
					log.info("### LCCClientReservationPax list is null ###");
				}
			}
		} else {
			if (log.isInfoEnabled()) {
				log.info("### Reservation is null ###");
			}
		}
		return paxCount;
	}

	private static int getSegmentCount(LCCClientReservation commonReservation) {
		int segmentCount = 0;
		if (commonReservation != null) {
			if (commonReservation.getSegments() != null) {
				segmentCount = commonReservation.getSegments().size();
			} else {
				if (log.isInfoEnabled()) {
					log.info("### LCCClientReservationSegment list is null ###");
				}
			}
		} else {
			if (log.isInfoEnabled()) {
				log.info("### Reservation is null ###");
			}
		}
		return segmentCount;
	}

	private static TrackInfoDTO getTrackingInfo(IPGQueryDTO ipgQueryDTO) throws ModuleException {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfoDTO.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
		trackInfoDTO.setCallingInstanceId(SystemPropertyUtil.getInstanceId());

		if(ipgQueryDTO.getAppIndicatorEnum().equals(AppIndicatorEnum.APP_XBE)) {
			trackInfoDTO.setOriginChannelId(3);
			trackInfoDTO.setOriginUserId(ipgQueryDTO.getUserId());

			User user = AirproxyModuleUtils.getSecurityBD().getUser(ipgQueryDTO.getUserId());
			trackInfoDTO.setOriginAgentCode(user.getAgentCode());
		}

		return trackInfoDTO;
	}

	private static ApplicationEngine getApplicationEngine(AppIndicatorEnum appIndicatorEnum) {
		if (AppIndicatorEnum.APP_IBE.equals(appIndicatorEnum)) {
			return ApplicationEngine.IBE;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicatorEnum)) {
			return ApplicationEngine.ANDROID;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicatorEnum)) {
			return ApplicationEngine.IOS;
		} else {
			return ApplicationEngine.XBE;
		}
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit,
			ApplicationEngine appIndicator) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(false);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(appIndicator);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);

		return pnrModesDTO;
	}

	private static boolean checkAmountsWithInRange(BigDecimal amountOne, BigDecimal amountTwo, boolean isGroupPNR) {
		// If it's a group pnr. We don't have the full amount in a carrier side. We need to load the full reservation
		// to identify this.
		if (isGroupPNR) {
			return true;
		} else {
			return checkAmountsWithInRange(amountOne, amountTwo);
		}
	}

	// If loyalty payment is there. result will be false
	// onhold reservations with loyalty payments can not confirm right now
	public static boolean checkAmountsWithInRange(BigDecimal amountOne, BigDecimal amountTwo) {
		BigDecimal finalAmount = AccelAeroCalculator.subtract(amountOne.abs(), amountTwo.abs());
		return finalAmount.abs().doubleValue() < 0.5 ? true : false;
	}

	private static CommonItineraryParamDTO composeCommonItineraryParamDTO(boolean isGroupPnr,
			LCCClientReservation commonReservation, IPGQueryDTO ipgQueryDTO) throws ModuleException {
		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(commonReservation.getContactInfo().getPreferredLanguage());
		commonItineraryParam.setIncludePaxFinancials(true);
		commonItineraryParam.setIncludePaymentDetails(true);
		commonItineraryParam.setIncludeTicketCharges(false);
		commonItineraryParam.setIncludeTermsAndConditions(true);
		commonItineraryParam.setStation(AirproxyModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM));
		commonItineraryParam.setAppIndicator(getApplicationEngine(ipgQueryDTO.getAppIndicatorEnum()));
		commonItineraryParam.setAirportMap(getAirportsList(isGroupPnr));
		return commonItineraryParam;
	}

	private static Map<String, String> getAirportsList(boolean isGroupPnr) throws ModuleException {

		Map<String, String> airportCodeAndNameMap = new HashMap<String, String>();
		Map<String, CachedAirportDTO> mapAirportCodeAndCachedAirportDTO = AirproxyModuleUtils.getAirportBD()
				.getAllAirportOperatorMap();

		for (Entry<String, CachedAirportDTO> entry : mapAirportCodeAndCachedAirportDTO.entrySet()) {
			airportCodeAndNameMap.put(entry.getValue().getAirportCode().toUpperCase(), entry.getValue().getAirportName());
		}

		return airportCodeAndNameMap;
	}

	private static ClientCommonInfoDTO getClientInfoDTO() {
		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
		clientInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		return clientInfoDTO;
	}

	private static Map<Integer, CommonCreditCardPaymentInfo> getTemporaryPaymentIds(IPGQueryDTO ipgQueryDTO,
			PayCurrencyDTO payCurrencyDTO, IPGIdentificationParamsDTO ipgDTO) {
		Map<Integer, CommonCreditCardPaymentInfo> tempPayMap = new HashMap<Integer, CommonCreditCardPaymentInfo>();

		CommonCreditCardPaymentInfo ccPayInfo = new CommonCreditCardPaymentInfo();
		ccPayInfo.setNo(ipgQueryDTO.getCreditCardNo());
		ccPayInfo.seteDate(null);
		ccPayInfo.setGroupPNR(ipgQueryDTO.getGroupPnr());
		ccPayInfo.setAddress(null);
		ccPayInfo.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
		ccPayInfo.setAuthorizationId(ipgQueryDTO.getAuthorizationNo());
		ccPayInfo.setCardHoldersName(null);
		ccPayInfo.setIpgIdentificationParamsDTO(ipgDTO);
		ccPayInfo.setName(null);
		ccPayInfo.setPayCurrencyAmount(ipgQueryDTO.getAmount());
		ccPayInfo.setPayCurrencyDTO(payCurrencyDTO);
		ccPayInfo.setPaymentBrokerId(ipgQueryDTO.getPaymentBrokerRefNo());
		ccPayInfo.setTnxMode(null);
		ccPayInfo.setType(ipgQueryDTO.getPaymentType().getTypeValue());
		ccPayInfo.setSecurityCode(null);
		ccPayInfo.setTemporyPaymentId(ipgQueryDTO.getTemporyPaymentId());

		tempPayMap.put(ipgQueryDTO.getTemporyPaymentId(), ccPayInfo);
		return tempPayMap;
	}

	private static PayCurrencyDTO getPayCurrencyDTO(IPGQueryDTO ipgQueryDTO) throws ModuleException {
		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(ipgQueryDTO.getTimestamp())
				.getCurrencyExchangeRate(ipgQueryDTO.getPaymentCurrencyCode());
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currencyExchangeRate.getCurrency().getCurrencyCode(),
				currencyExchangeRate.getMultiplyingExchangeRate(), currencyExchangeRate.getCurrency().getBoundryValue(),
				currencyExchangeRate.getCurrency().getBreakPoint(), currencyExchangeRate.getCurrency().getDecimalPlaces());
		return payCurrencyDTO;
	}

	@SuppressWarnings("unchecked")
	private static LCCClientBalancePayment getBalancePayment(String groupPnr, PayCurrencyDTO cardPayCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, IPGQueryDTO ipgQueryDTO, LCCClientReservation reservation,
			ExternalChgDTO externalChgDTO, LmsBlockedCredit lmsBlockedCredit) throws ModuleException {
		LinkedList perPaxExternalCharges = null;
		LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
		lccClientBalancePayment.setGroupPNR(groupPnr);
		lccClientBalancePayment.setContactInfo(reservation.getContactInfo());
		// e-ticket generation would fail without the pnr status
		lccClientBalancePayment.setReservationStatus(reservation.getStatus());
		lccClientBalancePayment.setPnrSegments(reservation.getSegments());
		lccClientBalancePayment.setPassengers(reservation.getPassengers());
		Map<EXTERNAL_CHARGES, ExternalChgDTO> creditCard = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		creditCard.put(EXTERNAL_CHARGES.CREDIT_CARD, externalChgDTO);
		lccClientBalancePayment.setExternalChargesMap(creditCard);
		boolean isInfantPaymentSeparated = reservation.isInfantPaymentSeparated();
		perPaxExternalCharges = getExternalChargesForAFreshPayment(creditCard, getPayablePaxCount(reservation));
		
		
		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			BigDecimal amountDue = pax.getTotalAvailableBalance();
			
			if (!isInfantPaymentSeparated && PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				continue;
			}
			
			if (amountDue.compareTo(BigDecimal.ZERO) >= 0) {
				
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();				
				BigDecimal totalLMSMemberPayment = AccelAeroCalculator.getDefaultBigDecimalZero();

				try {
					// add LMS payment entry first
					if (lmsBlockedCredit != null && lmsBlockedCredit.getBlockedCreditAmount() != null
							&& lmsBlockedCredit.getBlockedCreditAmount().doubleValue() > 0) {

						Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments = (Map<String, LoyaltyPaymentInfo>) com.isa.thinair.commons.core.util.XMLStreamer
								.decompose(lmsBlockedCredit.getLmsCreditPaymentInfo());

						for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
							LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments.get(operatingCarrierCode);
							lccClientBalancePayment.setLoyaltyPaymentInfo(opCarrierLoyaltyPaymentInfo);
							Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
									.getPaxProductPaymentBreakdown();
							BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil
									.getPaxRedeemedTotal(carrierPaxProductRedeemed, pax.getPaxSequence());
							String loyaltyMemberAccountId;
							String[] rewardIDs;
							loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
							rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
							totalLMSMemberPayment = AccelAeroCalculator.add(totalLMSMemberPayment, carrierLmsMemberPayment);
							if (cardPayCurrencyDTO != null) {
								cardPayCurrencyDTO.setTotalPayCurrencyAmount(carrierLmsMemberPayment);
							}
							if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
								paymentAssembler.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs, carrierPaxProductRedeemed,
										carrierLmsMemberPayment, cardPayCurrencyDTO, ipgQueryDTO.getTimestamp(),
										operatingCarrierCode);
							}
						}

					}
					log.info("LMS PAyment Amount : " + totalLMSMemberPayment + " TOTAL PAYMENT : " + amountDue);
				} catch (Exception e) {
					log.error("ERROR while fetching Blocked LMS Credit Payment details");
				}

				amountDue = AccelAeroCalculator.subtract(amountDue, totalLMSMemberPayment);
				
				if (BigDecimal.ZERO.compareTo(amountDue) < 0) {
					LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
					lccClientCCPaymentMetaInfo.setPaymentSuccessful(true);
					lccClientCCPaymentMetaInfo.setPaymentBrokerId(ipgQueryDTO.getPaymentBrokerRefNo());
					lccClientCCPaymentMetaInfo.setTemporyPaymentId(ipgQueryDTO.getTemporyPaymentId());
					paymentAssembler.addExternalCharges((Collection<ExternalChgDTO>) perPaxExternalCharges.pop());
					paymentAssembler.addInternalCardPayment(ipgQueryDTO.getPaymentType().getTypeValue(), null, ipgQueryDTO.getCreditCardNo(), null,
							null, null, amountDue, ipgQueryDTO.getAppIndicatorEnum(), TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO,
							cardPayCurrencyDTO, ipgQueryDTO.getTimestamp(), null, ipgQueryDTO.getTransacationId(), null, lccClientCCPaymentMetaInfo, null,
							ipgQueryDTO.getAidCompany());
				}
				lccClientBalancePayment.addPassengerPayments(pax.getPaxSequence(), paymentAssembler);
			}
		}

		return lccClientBalancePayment;

	}
	

	// FIXME
	// This method need to refactor with ExternalChargesMediator
	private static LinkedList getExternalChargesForAFreshPayment(Map<EXTERNAL_CHARGES, ExternalChgDTO> creditCard,
			int noOfPayingPassengers) {
		Collection[] externalCharges = new Collection[noOfPayingPassengers];
		if (creditCard != null) {
			ExternalChgDTO ccExtChgDTO = creditCard.get(EXTERNAL_CHARGES.CREDIT_CARD);
			externalCharges = sum(externalCharges, composeExternalCharges(ccExtChgDTO, noOfPayingPassengers));
		}

		return ReservationApiUtils.createLinkedList(externalCharges);
	}

	// FIXME
	// This method need to refactor with ExternalChargesMediator
	private static Collection[] sum(Collection[] externalCharges, Collection<ExternalChgDTO> colExternalChgDTO) {

		if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
			externalCharges = ReservationApiUtils.copyToCollectionArray(externalCharges, colExternalChgDTO);
		}

		return externalCharges;
	}

	// FIXME
	// This method need to refactor with ExternalChargesMediator
	private static Collection<ExternalChgDTO> composeExternalCharges(ExternalChgDTO extChgDTO, int noOfAdults) {
		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();
		if (extChgDTO != null) {
			extChgDTO = (ExternalChgDTO) extChgDTO.clone();
			extChgDTO.setAmountConsumedForPayment(false);

			if (extChgDTO.getAmount().doubleValue() > 0) {
				colExternalChgDTO.addAll(ReservationApiUtils.getPerPaxExternalCharges(extChgDTO, noOfAdults));
			}
		}

		return colExternalChgDTO;
	}

	private static BigDecimal getTotalAvailableBalance(LCCClientReservation reservation) throws ModuleException {

		BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			BigDecimal amountDue = pax.getTotalAvailableBalance();

			if (!reservation.isInfantPaymentSeparated() && PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				continue;
			}

			if (amountDue.compareTo(BigDecimal.ZERO) >= 0) {
				totalAvailableBalance = AccelAeroCalculator.add(totalAvailableBalance, amountDue);
			}
		}

		return totalAvailableBalance;
	}

	public static boolean isReservationConfirmationSuccessForOfflinePayments(IPGQueryDTO ipgQueryDTO) {

		if (!AppSysParamsUtil.isConfirmReservationIfPaymentExistsAtIPG()) {
			return false;
		}
		if (!isReservationExist(ipgQueryDTO)) {
			return false;
		}

		boolean isSuccess = false;

		try {
			boolean isGroupPNR = ipgQueryDTO.getGroupPnr() != null ? true : false;
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())) {

				ExternalChgDTO externalChgDTO = null;

				String pnr = ipgQueryDTO.getPnr();
				String groupPnr = ipgQueryDTO.getGroupPnr() != null ? ipgQueryDTO.getGroupPnr() : ipgQueryDTO.getPnr();
				BigDecimal totalCreditCardCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
				
				LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, isGroupPNR, true,
						getApplicationEngine(ipgQueryDTO.getAppIndicatorEnum()));
				LCCClientReservation commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(
						pnrModesDTO, null, getTrackingInfo(ipgQueryDTO));
				BigDecimal totalReservationBalance = getTotalAvailableBalance(commonReservation);
				if (!AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
					externalChgDTO = getCreditCardChargeMap(commonReservation, totalReservationBalance, ipgQueryDTO);
					if (externalChgDTO != null) {
						totalCreditCardCharge = externalChgDTO.getAmount();
					}
				}
				
				int tempTransactionId = ipgQueryDTO.getTemporyPaymentId();
				
				boolean isLMSBlockCreditEnabled = AirproxyModuleUtils.getChargeBD().isEnableLMSCreditBlock(tempTransactionId);

				LmsBlockedCredit lmsBlockedCredit = null;
				BigDecimal totalAmount = ipgQueryDTO.getBaseAmount();
				boolean hasLMSCreditPayment = false;
				if (AppSysParamsUtil.isLMSEnabled() && isLMSBlockCreditEnabled) {
					lmsBlockedCredit = AirproxyModuleUtils.getReservationBD().getLmsBlockCreditInfoByTmpTnxId(tempTransactionId);
					if (lmsBlockedCredit != null && lmsBlockedCredit.getBlockedCreditAmount().doubleValue() > 0) {
						totalAmount = AccelAeroCalculator.add(totalAmount, lmsBlockedCredit.getBlockedCreditAmount());
						hasLMSCreditPayment = true;
					}
				}
				
				
				
					IPGIdentificationParamsDTO ipgDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
					PayCurrencyDTO payCurrencyDTO = getPayCurrencyDTO(ipgQueryDTO);
					if (ipgQueryDTO.geteDirhamFee() != null
							&& ipgQueryDTO.geteDirhamFee().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						payCurrencyDTO.seteDirhamFee(ipgQueryDTO.geteDirhamFee());
					}
					LCCClientBalancePayment balancePayment = getBalancePayment(groupPnr, payCurrencyDTO, ipgDTO, ipgQueryDTO,
							commonReservation, externalChgDTO, lmsBlockedCredit);

					Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds = getTemporaryPaymentIds(ipgQueryDTO, payCurrencyDTO,
							ipgDTO);
					balancePayment.setTemporyPaymentMap(mapTnxIds);

					ServiceResponce response = AirproxyModuleUtils.getAirproxyReservationBD().balancePaymentWithTransaction(
							balancePayment, commonReservation.getVersion(), isGroupPNR, false, getClientInfoDTO(),
							getTrackingInfo(ipgQueryDTO));
					commonReservation = (LCCClientReservation) response.getResponseParam(CommandParamNames.RESERVATION);
					
					try {
						if (response.isSuccess() && hasLMSCreditPayment
								&& ReservationInternalConstants.ReservationStatus.CONFIRMED
										.equals(commonReservation.getStatus())) {
							AirproxyModuleUtils.getReservationBD().updateLMSCreditUtilizationSuccess(tempTransactionId);
						}
					} catch (Exception e) {
						log.error("ERROR @ UPDATING LMS CREDIT STATUS >> PNR CREATED");
					}

					isSuccess = true;
					AirproxyModuleUtils.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO,
							composeCommonItineraryParamDTO(isGroupPNR, commonReservation, ipgQueryDTO),
							getTrackingInfo(ipgQueryDTO));
				}
			
		} catch (Exception e) {
			log.error("isReservationConfirmationSuccess", e);
		}

		return isSuccess;
	}

}
