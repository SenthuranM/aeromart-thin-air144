package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class RequoteBalanceQueryRQ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* Segment data */

	// segments to be removed
	private Set<String> removedSegmentIds = new HashSet<String>();

	/* New fare data */
	private QuotedFareRebuildDTO fareInfo = null;

	/* Reservation data */

	private String pnr;

	private String groupPnr;

	private String reservationStatus;

	private String version;

	private CustomChargesTO customCharges = null;

	private Date lastFareQuotedDate;

	private boolean fqWithinValidity;

	private boolean sameFlightModification = false;

	Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();

	private Map<String, String> requoteSegmentMap = new HashMap<String, String>();

	private List<String> excludedSegFarePnrSegIds;

	private Map<Integer, String> ondFareTypeByFareIdMap = new HashMap<Integer, String>();

	private Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();

	private String transactionIdentifier;

	private SYSTEM system = SYSTEM.AA;

	private Map<String, List<String>> carrierWiseFlightRPHMap = new HashMap<String, List<String>>();

	private boolean applyModificationCharge = false;

	private Map<String, String> groundFltSegByFltSegId = new HashMap<String, String>();

	private Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = new HashMap<Integer, BigDecimal>();

	private Map<Integer, NameDTO> nameChangedPaxMap;

	private boolean duplicateNameCheck = false;

	private Double remainingLoyaltyPoints = null;

	public void setRemovedSegmentIds(Set<String> removedSegmentIds) {
		this.removedSegmentIds = removedSegmentIds;
	}

	public void addRemovedSegmentIds(String uniqueId) {
		removedSegmentIds.add(uniqueId);
	}

	public Set<String> getRemovedSegmentIds() {
		return removedSegmentIds;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getGroupPnr() {
		return groupPnr;
	}

	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public QuotedFareRebuildDTO getFareInfo() {
		return fareInfo;
	}

	public void setFareInfo(QuotedFareRebuildDTO fareInfo) {
		this.fareInfo = fareInfo;
	}

	/**
	 * If both new segments and removed segments exits in this reservation, it will return as true
	 * 
	 * @return
	 */
	public boolean isModifySegment() {
		if (hasNewSegments() && hasRemovedSegments()) {
			return true;
		}
		return false;
	}

	public boolean isCancelSegment() {
		if (!hasNewSegments() && hasRemovedSegments()) {
			return true;
		}
		return false;
	}

	public boolean isAddSegment() {
		if (hasNewSegments() && !hasRemovedSegments()) {
			return true;
		}
		return false;
	}

	/**
	 * Return true if in modification request, some segments are added to the reservation as new segments
	 * 
	 * @return
	 */
	public boolean hasNewSegments() {
		if (transactionIdentifier == null) {
			if (getFareInfo() != null && getFareInfo().hasOndFares()) {
				return true;
			}
		} else {
			for (String carrier : carrierWiseFlightRPHMap.keySet()) {
				if (!carrierWiseFlightRPHMap.get(carrier).isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Return true if in this modification request, some of the segments in the reservation is being removed
	 * 
	 * @return
	 */
	public boolean hasRemovedSegments() {
		if (getRemovedSegmentIds().size() > 0) {
			return true;
		}
		return false;
	}

	public Collection<TempSegBcAlloc> getBlockSeats() {
		if (hasNewSegments() && getFareInfo().hasBlockSeats()) {
			return getFareInfo().getBlockSeats();
		}
		return null;
	}

	public PnrChargeDetailTO getCustomCharge(String paxType) {
		if (hasCustomCharge(paxType)) {
			return customCharges.getPaxCustomChargeDetail(paxType);
		}
		return null;
	}

	public boolean hasCustomCharge(String paxType) {
		return customCharges != null && customCharges.hasCustomCharge(paxType);
	}

	public CustomChargesTO getCustomCharges() {
		return customCharges;
	}

	public void setCustomCharges(CustomChargesTO customCharges) {
		this.customCharges = customCharges;
	}

	public void setLastFareQuoteDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public Date getLastFareQuoteDate() {
		return lastFareQuotedDate;
	}

	public boolean isFQWithinValidity() {
		return fqWithinValidity;
	}

	public void setFQWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public boolean isSameFlightModification() {
		return sameFlightModification;
	}

	public void setSameFlightModification(boolean sameFlightModification) {
		this.sameFlightModification = sameFlightModification;
	}

	public Map<Integer, List<LCCClientExternalChgDTO>> getPaxExtChgMap() {
		return paxExtChgMap;
	}

	public void setPaxExtChgMap(Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap) {
		this.paxExtChgMap = paxExtChgMap;
	}

	public Map<String, String> getRequoteSegmentMap() {
		return requoteSegmentMap;
	}

	public void setRequoteSegmentMap(Map<String, String> requoteSegmentMap) {
		this.requoteSegmentMap = requoteSegmentMap;
	}

	public List<String> getExcludedSegFarePnrSegIds() {
		return excludedSegFarePnrSegIds;
	}

	public void setExcludedSegFarePnrSegIds(List<String> excludedSegFarePnrSegIds) {
		this.excludedSegFarePnrSegIds = excludedSegFarePnrSegIds;
	}

	public Map<Integer, String> getOndFareTypeByFareIdMap() {
		return ondFareTypeByFareIdMap;
	}

	public void setOndFareTypeByFareIdMap(Map<Integer, String> ondFareTypeByFareIdMap) {
		this.ondFareTypeByFareIdMap = ondFareTypeByFareIdMap;
	}

	public Map<Integer, Integer> getOldFareIdByFltSegIdMap() {
		return oldFareIdByFltSegIdMap;
	}

	public void setOldFareIdByFltSegIdMap(Map<Integer, Integer> oldFareIdByFltSegIdMap) {
		this.oldFareIdByFltSegIdMap = oldFareIdByFltSegIdMap;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public Map<String, List<String>> getCarrierWiseFlightRPHMap() {
		return carrierWiseFlightRPHMap;
	}

	public void setCarrierWiseFlightRPHMap(Map<String, List<String>> carrierWiseFlightRPHMap) {
		this.carrierWiseFlightRPHMap = carrierWiseFlightRPHMap;
	}

	public boolean isApplyModificationCharge() {
		return applyModificationCharge;
	}

	public void setApplyModificationCharge(boolean applyModificationCharge) {
		this.applyModificationCharge = applyModificationCharge;
	}

	public Map<String, String> getGroundFltSegByFltSegId() {
		return groundFltSegByFltSegId;
	}

	public void setGroundFltSegByFltSegId(Map<String, String> groundFltSegByFltSegId) {
		this.groundFltSegByFltSegId = groundFltSegByFltSegId;
	}

	public Map<Integer, BigDecimal> getPaxWiseAdjustmentAmountMap() {
		return paxWiseAdjustmentAmountMap;
	}

	public void setPaxWiseAdjustmentAmountMap(Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap) {
		this.paxWiseAdjustmentAmountMap = paxWiseAdjustmentAmountMap;
	}

	public Map<Integer, NameDTO> getNameChangedPaxMap() {
		return nameChangedPaxMap;
	}

	public void setNameChangedPaxMap(Map<Integer, NameDTO> nameChangedPaxMap) {
		this.nameChangedPaxMap = nameChangedPaxMap;
	}

	public boolean isDuplicateNameCheck() {
		return duplicateNameCheck;
	}

	public void setDuplicateNameCheck(boolean duplicateNameCheck) {
		this.duplicateNameCheck = duplicateNameCheck;
	}

	public Double getRemainingLoyaltyPoints() {
		return remainingLoyaltyPoints;
	}

	public void setRemainingLoyaltyPoints(Double remainingLoyaltyPoints) {
		this.remainingLoyaltyPoints = remainingLoyaltyPoints;
	}
}
