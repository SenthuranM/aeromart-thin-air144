package com.isa.thinair.airproxy.api.model.reservation.dto;

import java.io.Serializable;

import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;

public class PassengerCouponUpdateRQ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PassengerTicketCouponAuditDTO couponAudit;

	private Integer eticketId;

	private String eticketStatus;

	private String paxStatus;

	private String carrierCode;

	public PassengerTicketCouponAuditDTO getCouponAudit() {
		return couponAudit;
	}

	public void setCouponAudit(PassengerTicketCouponAuditDTO couponAudit) {
		this.couponAudit = couponAudit;
	}

	public Integer getEticketId() {
		return eticketId;
	}

	public void setEticketId(Integer eticketId) {
		this.eticketId = eticketId;
	}

	public String getEticketStatus() {
		return eticketStatus;
	}

	public void setEticketStatus(String eticketStatus) {
		this.eticketStatus = eticketStatus;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

}
