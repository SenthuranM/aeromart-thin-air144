package com.isa.thinair.airproxy.api.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;

public class FlightStopOverDisplayUtil {

	private static Log log = LogFactory.getLog(FlightStopOverDisplayUtil.class);

	public static String getStopOverDuration(Set<LCCClientReservationSegment> setReservationSegments,
			LCCClientReservationSegment currentSegment) {

		try {

			if (currentSegment != null && setReservationSegments != null && !setReservationSegments.isEmpty()) {

				LCCClientReservationSegment[] sortedSegmentArray = SortUtil.sortByDepDate(setReservationSegments);

				LCCClientReservationSegment nextReservSegment = getNextSegment(sortedSegmentArray, currentSegment.getPnrSegID());

				if (nextReservSegment != null) {

					if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(currentSegment.getStatus())
							&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL
									.equals(nextReservSegment.getStatus())) {

						String curreSegAirportArr[] = currentSegment.getSegmentCode().split("/");
						String currSegDepCityId = AirproxyModuleUtils.getAirportBD()
								.getOwnCityIdForAirport(curreSegAirportArr[0]);
						String currSegArrCityId = AirproxyModuleUtils.getAirportBD()
								.getOwnCityIdForAirport(curreSegAirportArr[1]);

						String nextSegAirportArr[] = nextReservSegment.getSegmentCode().split("/");
						String nextSegDepCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(nextSegAirportArr[0]);
						String nextSegArrCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(nextSegAirportArr[1]);

						boolean isReturn = false;

						if (currSegDepCityId != null && nextSegArrCityId != null && currSegDepCityId.equals(nextSegArrCityId)) {
							isReturn = true;
						} else if (curreSegAirportArr[0].equals(nextSegAirportArr[1])) {
							isReturn = true;
						}

						if (!isReturn) {

							String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
									nextSegAirportArr[0], null, null);

							if (isWithinTheTransitTime(minMaxTransitDurations, currentSegment.getArrivalDate(),
									nextReservSegment.getDepartureDate())) {
								if (curreSegAirportArr[1].equals(nextSegAirportArr[0])) {
									return getDuration(currentSegment.getArrivalDate(), nextReservSegment.getDepartureDate());
								} else if (currSegArrCityId != null && nextSegDepCityId != null
										&& currSegArrCityId.equals(nextSegDepCityId)) {
									return getDuration(currentSegment.getArrivalDate(), nextReservSegment.getDepartureDate());
								}
							}

						}

					}

				}
			}
		} catch (Exception e) {
			log.error(e);
		}

		return "-";
	}

	private static boolean isWithinTheTransitTime(String[] minMaxTransitDurations, Date curSegArrDate, Date nextSegDepDate) {

		String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
		String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

		GregorianCalendar minCal = new GregorianCalendar();
		minCal.setTime(curSegArrDate);
		minCal.add(GregorianCalendar.HOUR, Integer.parseInt(minTransitHoursMin[0]));
		minCal.add(GregorianCalendar.MINUTE, Integer.parseInt(minTransitHoursMin[1]));

		GregorianCalendar maxCal = new GregorianCalendar();
		maxCal.setTime(curSegArrDate);
		maxCal.add(GregorianCalendar.HOUR, Integer.parseInt(maxTransitHoursMin[0]));
		maxCal.add(GregorianCalendar.MINUTE, Integer.parseInt(maxTransitHoursMin[1]));

		GregorianCalendar nextSegDep = new GregorianCalendar();
		nextSegDep.setTime(nextSegDepDate);

		if (nextSegDep.getTimeInMillis() >= minCal.getTimeInMillis() && nextSegDep.getTimeInMillis() <= maxCal.getTimeInMillis()) {
			return true;
		}

		return false;
	}

	private static LCCClientReservationSegment getNextSegment(LCCClientReservationSegment[] sortedSegmentArray, Integer pnrSegId) {

		if (sortedSegmentArray != null && sortedSegmentArray.length > 1) {
			for (int i = 0; i < sortedSegmentArray.length; i++) {
				LCCClientReservationSegment curentSeg = sortedSegmentArray[i];
				if (curentSeg.getPnrSegID().intValue() == pnrSegId && (i + 1) < sortedSegmentArray.length) {
					return sortedSegmentArray[i + 1];
				}
			}
		}

		return null;
	}

	private static String getDuration(Date departureDate, Date arrivalDate) {

		NumberFormat nfr = new DecimalFormat("##00");

		Calendar depCal = Calendar.getInstance();
		depCal.setTime(departureDate);

		Calendar arrCal = Calendar.getInstance();
		arrCal.setTime(arrivalDate);

		long diff = arrCal.getTimeInMillis() - depCal.getTimeInMillis();

		long diffMinutes = diff / (60 * 1000);

		String durationHoursStr = nfr.format((diffMinutes / 60)) + ":" + nfr.format((diffMinutes % 60));

		return durationHoursStr;
	}
	
	public static String getStopOverStations(String segCode) {

		String stopOverStations = "Non-Stop";

		if (!StringUtil.isNullOrEmpty(segCode)) {
			String[] segmentArr = segCode.split("/");
			if (segmentArr != null && segmentArr.length > 2) {
				stopOverStations = (segmentArr.length - 2) + " Stop";
			}
		}

		return formatNullOrEmpty(stopOverStations);
	}

	public static String formatNullOrEmpty(String textStr) {

		if (StringUtil.isNullOrEmpty(textStr) || "null".equalsIgnoreCase(textStr.trim())) {
			textStr = "-";
		}

		return textStr;
	}

	public static String getStopOverStations(String segCode, String language, Map<String, String> airportMap) {

		String stopOverStations = "Non-Stop";

		if (!StringUtil.isNullOrEmpty(segCode)) {
			String[] segmentArr = segCode.split("/");
			if (segmentArr != null && segmentArr.length > 2) {
				if (segmentArr.length == 3) {
					stopOverStations = (segmentArr.length - 2) + " Stop";
				} else if (segmentArr.length > 3) {
					stopOverStations = (segmentArr.length - 2) + " Stops";
				}
				segmentArr = getAirportNamesFromSegmentCodes(segmentArr, language, airportMap);
				if (AppSysParamsUtil.displayMultilegDetailsInItinerary()) {
					String viaPoints = "";
					for (int i = 1; i < segmentArr.length - 1; i++) {
						if (i == 1) {
							viaPoints = segmentArr[i];
						} else {
							viaPoints = viaPoints + "," + segmentArr[i];
						}
					}
					stopOverStations = stopOverStations + ":" + viaPoints;
				}
			}
		}

		return stopOverStations;
	}
	
	private static String[] getAirportNamesFromSegmentCodes(String[] segCodes,String language, Map<String, String> airportMap) {
		String[] airportNames = new String[segCodes.length];
		int i = 0;
		for(String segCode : segCodes){
			if ((!("en").equals(language)) && (airportMap.get(segCode) != null)) {
				airportNames[i] = StringUtil.getUnicode(airportMap.get(segCode.toUpperCase()));
			} else {
				airportNames[i] = airportMap.get(segCode.toUpperCase());
			}
			i++;
		}
		return airportNames;		
	}

}
