/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;

public class ModifyReservationUtil {

	public static void updatePayments(ReservationBalanceTO balanceTO, boolean isInfantPaymentSeparated) {
		updatePayments(balanceTO, null, null, null, isInfantPaymentSeparated);
	}

	public static void updatePayments(ReservationBalanceTO balanceTO, String adultCnxCharge, String childCnxCharge,
			String infantCnxCharge, boolean isInfantPaymentSeparated) {

		BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		List<LCCClientSegmentSummaryTO> applicableSegSummaryTOs = new ArrayList<LCCClientSegmentSummaryTO>();

		for (LCCClientPassengerSummaryTO passengerSummaryTO : balanceTO.getPassengerSummaryList()) {
			if (!ReservationInternalConstants.PassengerType.INFANT.equals(passengerSummaryTO.getPaxType()) || isInfantPaymentSeparated) {
				LCCClientSegmentSummaryTO paxSegmentSummaryTO = passengerSummaryTO.getSegmentSummary();
				BigDecimal paxCnxAmt = getPaxCnxCharge(passengerSummaryTO, adultCnxCharge, childCnxCharge);
				paxSegmentSummaryTO.setNewCnxAmount(paxCnxAmt);

				if (passengerSummaryTO.getTotalAmountDue().compareTo(new BigDecimal("0")) < 0) {
					passengerSummaryTO.setTotalAmountDue(AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				totalCnxCharge = AccelAeroCalculator.add(totalCnxCharge, paxCnxAmt);
				totalPrice = AccelAeroCalculator.add(totalPrice, passengerSummaryTO.getTotalPrice());
				totalAmountDue = AccelAeroCalculator.add(totalAmountDue, passengerSummaryTO.getTotalAmountDue());
				totalModCharge = AccelAeroCalculator.add(totalModCharge, passengerSummaryTO.getTotalModCharge());
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, passengerSummaryTO.getTotalPaidAmount());
				totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, passengerSummaryTO.getTotalCreditAmount());

				applicableSegSummaryTOs.add(paxSegmentSummaryTO);
			}
		}

		LCCClientSegmentSummaryTO segmentSummaryTO = getAccumulatedLCCClientSegmentSummaryTO(applicableSegSummaryTOs,
				totalCnxCharge, totalModCharge, totalCreditAmount);

		balanceTO.setSegmentSummary(segmentSummaryTO);
		balanceTO.setTotalAmountDue(totalAmountDue);
		balanceTO.setTotalCnxCharge(totalCnxCharge);
		balanceTO.setTotalCreditAmount(totalCreditAmount);
		balanceTO.setTotalModCharge(totalModCharge);
		balanceTO.setTotalPaidAmount(totalPaidAmount);
		balanceTO.setTotalPrice(totalPrice);
	}

	// TODO possible move to web platform
	/**
	 * Not sure about the above method using this to calculate the values
	 * 
	 * @param balanceTO
	 * @param adultCnxCharge
	 * @param childCnxCharge
	 * @param infantCnxCharge
	 */
	public static void updateOverrideCharges(ReservationBalanceTO balanceTO, String adultCnxCharge, String childCnxCharge,
			String infantCnxCharge, String modifyMethod) {

		BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal effectiveCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (LCCClientPassengerSummaryTO passengerSummaryTO : balanceTO.getPassengerSummaryList()) {

			LCCClientSegmentSummaryTO paxSegmentSummaryTO = passengerSummaryTO.getSegmentSummary();
			if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(modifyMethod)) {
				effectiveCharge = paxSegmentSummaryTO.getNewModAmount();
				BigDecimal paxModAmt = getPaxModCharge(passengerSummaryTO, adultCnxCharge, childCnxCharge, infantCnxCharge);
				paxSegmentSummaryTO.setNewModAmount(paxModAmt);
				effectiveCharge = AccelAeroCalculator.add(paxSegmentSummaryTO.getNewModAmount(),
						paxSegmentSummaryTO.getCurrentModAmount(), effectiveCharge.negate());
			} else {
				effectiveCharge = paxSegmentSummaryTO.getNewCnxAmount();
				BigDecimal paxCnxAmt = getPaxCnxCharge(passengerSummaryTO, adultCnxCharge, childCnxCharge);
				paxSegmentSummaryTO.setNewCnxAmount(paxCnxAmt);
				effectiveCharge = AccelAeroCalculator.add(paxSegmentSummaryTO.getNewCnxAmount(),
						paxSegmentSummaryTO.getCurrentCnxAmount(), effectiveCharge.negate());
			}

			if (passengerSummaryTO.getTotalAmountDue().compareTo(new BigDecimal("0")) > 0) {
				passengerSummaryTO.setTotalAmountDue(AccelAeroCalculator.add(passengerSummaryTO.getTotalAmountDue(),
						effectiveCharge));

			} else {
				passengerSummaryTO.setTotalCreditAmount(AccelAeroCalculator.add(passengerSummaryTO.getTotalCreditAmount(),
						effectiveCharge.negate()));
			}

			if (passengerSummaryTO.getTotalCreditAmount().compareTo(new BigDecimal("0")) < 0) {
				passengerSummaryTO.setTotalAmountDue(passengerSummaryTO.getTotalCreditAmount().negate());
				passengerSummaryTO.setTotalCreditAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
			}

			passengerSummaryTO.setTotalPrice(AccelAeroCalculator.add(passengerSummaryTO.getTotalPrice(), effectiveCharge));

			if (!passengerSummaryTO.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, passengerSummaryTO.getTotalPaidAmount());
				totalAmountDue = AccelAeroCalculator.add(totalAmountDue, passengerSummaryTO.getTotalAmountDue());
				totalCnxCharge = AccelAeroCalculator.add(totalCnxCharge, paxSegmentSummaryTO.getNewCnxAmount());
				totalPrice = AccelAeroCalculator.add(totalPrice, passengerSummaryTO.getTotalPrice());
				totalModCharge = AccelAeroCalculator.add(totalModCharge, paxSegmentSummaryTO.getNewModAmount());
				totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, passengerSummaryTO.getTotalCreditAmount());
			}

			// after the calculation put back the values tobe calculated in fron end not to break the existing flow.
			if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(modifyMethod)) {
				paxSegmentSummaryTO.setNewModAmount(AccelAeroCalculator.add(paxSegmentSummaryTO.getCurrentModAmount(),
						paxSegmentSummaryTO.getNewModAmount()));
			} else {
				paxSegmentSummaryTO.setNewCnxAmount(AccelAeroCalculator.add(paxSegmentSummaryTO.getCurrentCnxAmount(),
						paxSegmentSummaryTO.getNewCnxAmount()));
			}
		}
		LCCClientSegmentSummaryTO segmentSummaryTO = balanceTO.getSegmentSummary();
		if (ReservationConstants.AlterationType.ALT_MODIFY_OND.equals(modifyMethod)) {
			getOverRiddenLCCClientSegmentSummaryTO(BigDecimal.ZERO, totalModCharge, segmentSummaryTO);
		} else {
			getOverRiddenLCCClientSegmentSummaryTO(totalCnxCharge, BigDecimal.ZERO, segmentSummaryTO);
		}

		balanceTO.setSegmentSummary(segmentSummaryTO);
		balanceTO.setTotalAmountDue(totalAmountDue);
		balanceTO.setTotalCnxCharge(totalCnxCharge);
		balanceTO.setTotalCreditAmount(totalCreditAmount);
		balanceTO.setTotalModCharge(totalModCharge);
		balanceTO.setTotalPaidAmount(totalPaidAmount);
		balanceTO.setTotalPrice(totalPrice);
	}

	public static Collection<LCCClientPassengerSummaryTO> filterSelectedPassengers(
			Collection<LCCClientPassengerSummaryTO> passengerSummaryTOs, List<String> paxAdults, List<String> paxInfants) {
		Collection<LCCClientPassengerSummaryTO> filteredPassengerSummaryTOs = new ArrayList<LCCClientPassengerSummaryTO>();
		for (LCCClientPassengerSummaryTO passengerSummaryTO : passengerSummaryTOs) {
			if (ModifyReservationUtil.isSelectedTraveler(passengerSummaryTO.getTravelerRefNumber(), paxAdults)) {
				filteredPassengerSummaryTOs.add(passengerSummaryTO);
			} else if (ModifyReservationUtil.isSelectedTraveler(passengerSummaryTO.getTravelerRefNumber(), paxInfants)) {
				filteredPassengerSummaryTOs.add(passengerSummaryTO);
			}
		}
		return filteredPassengerSummaryTOs;
	}

	/**
	 * Update the PassengerSummaryTO list for missing parents, who will be credited the infant surcharges.
	 * 
	 * @param passengerSummaryTOs
	 * @param paxAdults
	 * @param paxInfants
	 * @param passengers
	 * @param adultCnxCharge
	 * @param childCnxCharge
	 * @param infantCnxCharge
	 * @param isInfantPaymentSeparated TODO
	 * @return
	 */
	public static Collection<LCCClientPassengerSummaryTO> getUpdatedPaxSummaryTOs(
			Collection<LCCClientPassengerSummaryTO> passengerSummaryTOs, List<String> paxAdults, List<String> paxInfants,
			Set<LCCClientReservationPax> passengers, String adultCnxCharge, String childCnxCharge, String infantCnxCharge, boolean isInfantPaymentSeparated) {

		Collection<LCCClientPassengerSummaryTO> updatedPassengerSummaryTOs = new ArrayList<LCCClientPassengerSummaryTO>();
		List<String> updatedPaxTravelerRefs = new ArrayList<String>();

		Collection<LCCClientPassengerSummaryTO> filteredPassengerSummaryTOs = ModifyReservationUtil.filterSelectedPassengers(
				passengerSummaryTOs, paxAdults, paxInfants);

		for (LCCClientPassengerSummaryTO passengerSummaryTO : filteredPassengerSummaryTOs) {

			// Update parent and/or infant
			if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)) {

				LCCClientReservationPax parentReservationPax = ModifyReservationUtil.getParent(passengers, passengerSummaryTO);

				String parentTravelerRef = parentReservationPax.getTravelerRefNumber().replaceAll(" ", "");

				List<String> parentTravelRefList = PaxTypeUtils.getTravelRefList(parentTravelerRef);

				if (ModifyReservationUtil.isSelectedTraveler(parentTravelerRef, paxAdults) && !isInfantPaymentSeparated) {
					for (LCCClientPassengerSummaryTO parentSummaryTO : filteredPassengerSummaryTOs) {

						List<String> paxTravelRefList = PaxTypeUtils.getTravelRefList(parentSummaryTO.getTravelerRefNumber());

						if (parentTravelRefList != null && paxTravelRefList != null
								&& parentTravelRefList.containsAll(paxTravelRefList)) {

							updatedPaxTravelerRefs.add(passengerSummaryTO.getTravelerRefNumber());
							updatedPaxTravelerRefs.add(parentSummaryTO.getTravelerRefNumber());

							if (!BeanUtils.nullHandler(adultCnxCharge).equals("")) {
								BigDecimal customAdultCnxCharge = new BigDecimal(adultCnxCharge);
								parentSummaryTO.setTotalCnxCharge(customAdultCnxCharge);
								parentSummaryTO.getSegmentSummary().setNewCnxAmount(customAdultCnxCharge);
							}

							if (!BeanUtils.nullHandler(infantCnxCharge).equals("")) {
								BigDecimal customInfantCnxCharge = new BigDecimal(infantCnxCharge);
								passengerSummaryTO.setTotalCnxCharge(customInfantCnxCharge);
								passengerSummaryTO.getSegmentSummary().setNewCnxAmount(customInfantCnxCharge);
							}

							ModifyReservationUtil.updateInfantAndParentSummariesWhenBothSelected(passengerSummaryTO,
									parentSummaryTO, parentReservationPax.getTotalAvailableBalance());

							updatedPassengerSummaryTOs.add(parentSummaryTO);
							updatedPassengerSummaryTOs.add(passengerSummaryTO);
							break;
						}
					}
				} else {
					
					if(isInfantPaymentSeparated){
						if (!BeanUtils.nullHandler(infantCnxCharge).equals("")) {
							BigDecimal customInfantCnxCharge = new BigDecimal(infantCnxCharge);
							passengerSummaryTO.setTotalCnxCharge(customInfantCnxCharge);
							passengerSummaryTO.getSegmentSummary().setNewCnxAmount(customInfantCnxCharge);
						}
						updatedPassengerSummaryTOs.add(passengerSummaryTO);
					}else{
					
						for (LCCClientPassengerSummaryTO parentSummaryTO : passengerSummaryTOs) {
	
							if (!BeanUtils.nullHandler(parentSummaryTO.getInfantName()).equals("")) {
	
								List<String> paxTravelRefList = PaxTypeUtils.getTravelRefList(parentSummaryTO.getTravelerRefNumber());
	
								if (parentTravelRefList != null && paxTravelRefList != null
										&& parentTravelRefList.containsAll(paxTravelRefList)
										&& !ModifyReservationUtil.isSelectedTraveler(parentTravelerRef, paxAdults)) {
	
									if (!BeanUtils.nullHandler(adultCnxCharge).equals("")) {
										BigDecimal customAdultCnxCharge = new BigDecimal(adultCnxCharge);
										parentSummaryTO.setTotalCnxCharge(customAdultCnxCharge);
										parentSummaryTO.getSegmentSummary().setNewCnxAmount(customAdultCnxCharge);
									}
	
									if (!BeanUtils.nullHandler(infantCnxCharge).equals("")) {
										BigDecimal customInfantCnxCharge = new BigDecimal(infantCnxCharge);
										passengerSummaryTO.setTotalCnxCharge(customInfantCnxCharge);
										passengerSummaryTO.getSegmentSummary().setNewCnxAmount(customInfantCnxCharge);
									}
	
									// Update parent charges
									ModifyReservationUtil.updateParentAndInfantSummariesWhenOnlyInfantSelected(passengerSummaryTO,
											parentSummaryTO, parentReservationPax.getTotalAvailableBalance());
	
									updatedPassengerSummaryTOs.add(parentSummaryTO);
									updatedPassengerSummaryTOs.add(passengerSummaryTO);
									break;
								}
							}
						}
					}
					
				}
			} else if (!ModifyReservationUtil.isSelectedTraveler(passengerSummaryTO.getTravelerRefNumber(),
					updatedPaxTravelerRefs) && BeanUtils.nullHandler(passengerSummaryTO.getInfantName()).equals("")) {
				if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.ADULT)) {
					if (!BeanUtils.nullHandler(adultCnxCharge).equals("")) {
						BigDecimal customAdultCnxCharge = new BigDecimal(adultCnxCharge);
						passengerSummaryTO.setTotalCnxCharge(customAdultCnxCharge);
						passengerSummaryTO.getSegmentSummary().setNewCnxAmount(customAdultCnxCharge);
					}
				} else if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.CHILD)) {
					if (!BeanUtils.nullHandler(childCnxCharge).equals("")) {
						BigDecimal customChildCnxCharge = new BigDecimal(childCnxCharge);
						passengerSummaryTO.setTotalCnxCharge(customChildCnxCharge);
						passengerSummaryTO.getSegmentSummary().setNewCnxAmount(customChildCnxCharge);
					}
				}

				// ModifyReservationUtil.updateAdultSummary(passengerSummaryTO);
				updatedPassengerSummaryTOs.add(passengerSummaryTO);
			}
		}
		return updatedPassengerSummaryTOs;
	}

	/**
	 * Update the PassengerSummaryTO list for missing parents, who will be credited the infant surcharges.
	 * 
	 * @param passengerSummaryTOs
	 * @param paxAdults
	 * @param paxInfants
	 * @param isInfantPaymentSeparated TODO
	 * @param filteredPassengerSummaryTOs
	 * @param clientReservation
	 * @return
	 */
	public static Collection<LCCClientPassengerSummaryTO> getUpdatedPaxSummaryTOs(
			Collection<LCCClientPassengerSummaryTO> passengerSummaryTOs, List<String> paxAdults, List<String> paxInfants,
			Set<LCCClientReservationPax> passengers, boolean isInfantPaymentSeparated) {

		return getUpdatedPaxSummaryTOs(passengerSummaryTOs, paxAdults, paxInfants, passengers, null, null, null, isInfantPaymentSeparated);
	}

	public static void updateAdultSummary(LCCClientPassengerSummaryTO passengerSummaryTO) {
		LCCClientSegmentSummaryTO passengerSegmentSummaryTO = passengerSummaryTO.getSegmentSummary();

		BigDecimal paxTotalAmountDue = AccelAeroCalculator.add(passengerSummaryTO.getTotalAmountDue(), passengerSegmentSummaryTO
				.getCurrentRefunds().negate(), passengerSegmentSummaryTO.getNewCnxAmount());

		BigDecimal paxTotalCreditAmount = AccelAeroCalculator.add(passengerSegmentSummaryTO.getCurrentRefunds(),
				passengerSegmentSummaryTO.getNewCnxAmount().negate());

		passengerSummaryTO.setTotalCreditAmount(paxTotalCreditAmount);
		// passengerSegmentSummaryTO.setCurrentRefunds(passengerCurrentRefunds);
		passengerSummaryTO.setTotalAmountDue(paxTotalAmountDue);
	}

	public static void updateInfantAndParentSummariesWhenBothSelected(LCCClientPassengerSummaryTO infantSummaryTO,
			LCCClientPassengerSummaryTO parentSummaryTO, BigDecimal parentAvalableBalance) {

		// Nili AARESAA-7219 Web Feb 1, 2012
		// Dear Thushara / future bug fixer, please review.
		// I really don't understand the logic behind this. Mainly because;
		// 1. Parent is having the available balance and credit amount(s)
		// 2. In revenue accounting we keep records for adult/child not for infants
		// 3. The credit amount which is coming with the credit needs to be mark as zero. Since it's contradicting with
		// the total
		// reservation credit amount.

		// LCCClientSegmentSummaryTO parentSegmentSummaryTO = parentSummaryTO.getLccClientSegmentSummaryTO();
		// LCCClientSegmentSummaryTO infantSegmentSummaryTO = infantSummaryTO.getLccClientSegmentSummaryTO();
		// BigDecimal parentTotalAmountDue = AccelAeroCalculator.add(parentSummaryTO.getTotalAmountDue(),
		// infantSegmentSummaryTO
		// .getCurrentRefunds().negate(), parentAvalableBalance.negate());
		//
		// BigDecimal parentTotalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		// if(parentTotalAmountDue.compareTo(BigDecimal.ZERO)<0){
		// parentTotalCreditAmount = parentTotalAmountDue;
		// parentTotalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		// }
		//
		// infantSummaryTO.setTotalCreditAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		// parentSummaryTO.setTotalCreditAmount(parentTotalCreditAmount);
		// parentSummaryTO.setTotalAmountDue(parentTotalAmountDue);

		infantSummaryTO.setTotalCreditAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		// parentSummaryTO.setTotalCreditAmount(parentSummaryTO.getTotalCreditAmount());
		// parentSummaryTO.setTotalAmountDue(parentSummaryTO.getTotalAmountDue());
	}

	/**
	 * Recalculate the Parent's PassengerSummaryTO if only the infant is to be removed.
	 * 
	 * @param infantSummaryTO
	 * @param parentSummaryTO
	 * @param parentAvailableBalance
	 */
	public static void updateParentAndInfantSummariesWhenOnlyInfantSelected(LCCClientPassengerSummaryTO infantSummaryTO,
			LCCClientPassengerSummaryTO parentSummaryTO, BigDecimal parentAvailableBalance) {

		LCCClientSegmentSummaryTO parentSegmentSummaryTO = parentSummaryTO.getSegmentSummary();
		LCCClientSegmentSummaryTO infantSegmentSummaryTO = infantSummaryTO.getSegmentSummary();

		BigDecimal parentCurrentSurcharge = infantSegmentSummaryTO.getCurrentSurchargeAmount();
		BigDecimal parentCurrentTotalPrice = infantSegmentSummaryTO.getCurrentTotalPrice();
		BigDecimal parentCurrentNonRefunds = infantSegmentSummaryTO.getCurrentNonRefunds();

		BigDecimal parentCurrentRefunds = infantSegmentSummaryTO.getCurrentRefunds();
		BigDecimal newCnxChg = infantSegmentSummaryTO.getNewCnxAmount();
		BigDecimal parentTotalAmountDue = infantSummaryTO.getTotalAmountDue();
		BigDecimal parentNewSurchargeAmount = infantSegmentSummaryTO.getNewSurchargeAmount();

		if (newCnxChg.compareTo(parentCurrentRefunds) > 0) {
			parentTotalAmountDue = AccelAeroCalculator.add(parentTotalAmountDue, newCnxChg.negate(), parentCurrentRefunds);
			newCnxChg = parentCurrentRefunds;
		}

		parentSegmentSummaryTO.setCurrentTotalPrice(parentCurrentTotalPrice);
		parentSegmentSummaryTO.setCurrentSurchargeAmount(parentCurrentSurcharge);
		parentSegmentSummaryTO.setCurrentRefunds(parentCurrentRefunds);
		parentSegmentSummaryTO.setCurrentNonRefunds(parentCurrentNonRefunds);

		parentSegmentSummaryTO.setNewCnxAmount(newCnxChg);
		parentSegmentSummaryTO.setNewSurchargeAmount(parentNewSurchargeAmount);
		parentSegmentSummaryTO.setNewTaxAmount(infantSegmentSummaryTO.getNewTaxAmount());
		parentSegmentSummaryTO.setNewFareAmount(infantSegmentSummaryTO.getNewFareAmount());

		parentSummaryTO.setSegmentSummaryTO(parentSegmentSummaryTO);
		parentSummaryTO.setTotalCnxCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
		parentSummaryTO.setTotalCreditAmount(parentCurrentRefunds);
		parentSummaryTO.setTotalAmountDue(parentTotalAmountDue);

		infantSegmentSummaryTO.setCurrentRefunds(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSegmentSummaryTO.setCurrentSurchargeAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSegmentSummaryTO.setCurrentTotalPrice(AccelAeroCalculator.getDefaultBigDecimalZero());

		infantSummaryTO.setSegmentSummaryTO(infantSegmentSummaryTO);
		infantSummaryTO.setTotalCreditAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSummaryTO.setTotalPaidAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSummaryTO.setTotalPrice(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSummaryTO.setTotalAmountDue(AccelAeroCalculator.getDefaultBigDecimalZero());
		infantSummaryTO.setTotalModCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
	}

	/**
	 * Retrieve the ParentTraveler reference number.
	 * 
	 * @param clientReservation
	 * @param passengerSummaryTO
	 * @return
	 */
	public static LCCClientReservationPax getParent(Set<LCCClientReservationPax> passengers,
			LCCClientPassengerSummaryTO passengerSummaryTO) {
		LCCClientReservationPax parentReservationPax = null;

		parentIndentification: for (LCCClientReservationPax reservationPax : passengers) {
			if (reservationPax.getPaxType().equals(PaxTypeTO.ADULT)) {
				if (reservationPax.getInfants() != null && reservationPax.getInfants().size() > 0) {
					for (LCCClientReservationPax infantPax : reservationPax.getInfants()) {

						List<String> paxTravelRefList = PaxTypeUtils.getTravelRefList(passengerSummaryTO.getTravelerRefNumber());
						List<String> infantTravelRefList = PaxTypeUtils.getTravelRefList(infantPax.getTravelerRefNumber());

						if (paxTravelRefList != null && infantTravelRefList != null) {
							if (paxTravelRefList.containsAll(infantTravelRefList)) {
								parentReservationPax = reservationPax;
								break parentIndentification;
							}
						}

					}
				}
			}
		}
		return parentReservationPax;
	}

	private static BigDecimal getPaxCnxCharge(LCCClientPassengerSummaryTO passengerSummaryTO, String adultCnxCharge,
			String childCnxCharge) {

		BigDecimal paxCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.ADULT) && (adultCnxCharge != null && !"".equals(adultCnxCharge))) {
			paxCnxCharge = new BigDecimal(adultCnxCharge);
			passengerSummaryTO.setTotalCnxCharge(paxCnxCharge);
		} else if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.CHILD)
				&& (childCnxCharge != null && !"".equals(childCnxCharge))) {
			paxCnxCharge = new BigDecimal(childCnxCharge);
			passengerSummaryTO.setTotalCnxCharge(paxCnxCharge);
		} else if (!passengerSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)) {
			paxCnxCharge = passengerSummaryTO.getTotalCnxCharge();
		}

		return paxCnxCharge;
	}

	private static BigDecimal getPaxModCharge(LCCClientPassengerSummaryTO passengerSummaryTO, String adultModCharge,
			String childModCharge, String infantModCharge) {

		BigDecimal paxModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.ADULT) && (adultModCharge != null && !"".equals(adultModCharge))) {
			paxModCharge = new BigDecimal(adultModCharge);
			passengerSummaryTO.setTotalModCharge(paxModCharge);
		} else if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.CHILD)
				&& (childModCharge != null && !"".equals(childModCharge))) {
			paxModCharge = new BigDecimal(childModCharge);
			passengerSummaryTO.setTotalModCharge(paxModCharge);
		} else if (passengerSummaryTO.getPaxType().equals(PaxTypeTO.INFANT)
				&& (infantModCharge != null && !"".equals(infantModCharge))) {
			paxModCharge = new BigDecimal(infantModCharge);
		} else {
			paxModCharge = passengerSummaryTO.getTotalModCharge();
		}

		return paxModCharge;
	}

	public static LCCClientSegmentSummaryTO getAccumulatedLCCClientSegmentSummaryTO(
			List<LCCClientSegmentSummaryTO> applicableSegSummaryTOs, BigDecimal totalCnxCharge, BigDecimal totalModCharge,
			BigDecimal totalCreditAmount) {

		LCCClientSegmentSummaryTO segmentSummaryTO = new LCCClientSegmentSummaryTO();

		// Current
		BigDecimal currentFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentNonRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal currentPenaltyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// New
		BigDecimal newFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newSurchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newCnxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newModAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newAdjAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newOutBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newInBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal newExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		

		for (LCCClientSegmentSummaryTO lccClientSegmentSummaryTO : applicableSegSummaryTOs) {

			currentFareAmount = AccelAeroCalculator.add(currentFareAmount, lccClientSegmentSummaryTO.getCurrentFareAmount());
			currentTaxAmount = AccelAeroCalculator.add(currentTaxAmount, lccClientSegmentSummaryTO.getCurrentTaxAmount());
			currentSurchargeAmount = AccelAeroCalculator.add(currentSurchargeAmount,
					lccClientSegmentSummaryTO.getCurrentSurchargeAmount());
			currentCnxAmount = AccelAeroCalculator.add(currentCnxAmount, lccClientSegmentSummaryTO.getCurrentCnxAmount());
			currentModAmount = AccelAeroCalculator.add(currentModAmount, lccClientSegmentSummaryTO.getCurrentModAmount());
			currentAdjAmount = AccelAeroCalculator.add(currentAdjAmount, lccClientSegmentSummaryTO.getCurrentAdjAmount());
			currentTotalPrice = AccelAeroCalculator.add(currentTotalPrice, lccClientSegmentSummaryTO.getCurrentTotalPrice());
			currentNonRefunds = AccelAeroCalculator.add(currentNonRefunds, lccClientSegmentSummaryTO.getCurrentNonRefunds());
			currentRefunds = AccelAeroCalculator.add(currentRefunds, lccClientSegmentSummaryTO.getCurrentRefunds());
			currentDiscount = AccelAeroCalculator.add(currentDiscount, lccClientSegmentSummaryTO.getCurrentDiscount());
			currentPenaltyAmount = AccelAeroCalculator.add(currentPenaltyAmount,
					lccClientSegmentSummaryTO.getCurrentModificationPenatly());

			newFareAmount = AccelAeroCalculator.add(newFareAmount, lccClientSegmentSummaryTO.getNewFareAmount());
			newTaxAmount = AccelAeroCalculator.add(newTaxAmount, lccClientSegmentSummaryTO.getNewTaxAmount());
			newSurchargeAmount = AccelAeroCalculator.add(newSurchargeAmount, lccClientSegmentSummaryTO.getNewSurchargeAmount());
			newCnxAmount = AccelAeroCalculator.add(newCnxAmount, lccClientSegmentSummaryTO.getNewCnxAmount());
			newModAmount = AccelAeroCalculator.add(newModAmount, lccClientSegmentSummaryTO.getNewModAmount());
			newAdjAmount = AccelAeroCalculator.add(newAdjAmount, lccClientSegmentSummaryTO.getNewAdjAmount());
			newTotalPrice = AccelAeroCalculator.add(newTotalPrice, lccClientSegmentSummaryTO.getNewTotalPrice());
			newOutBoundFlexiCharge = AccelAeroCalculator.add(newOutBoundFlexiCharge,
					lccClientSegmentSummaryTO.getOutBoundExternalCharge());
			newInBoundFlexiCharge = AccelAeroCalculator.add(newInBoundFlexiCharge,
					lccClientSegmentSummaryTO.getInBoundExternalCharge());
			newDiscount = AccelAeroCalculator.add(newDiscount, lccClientSegmentSummaryTO.getNewDiscount());
			
			newExtraFeeAmount = AccelAeroCalculator.add(newExtraFeeAmount, lccClientSegmentSummaryTO.getNewExtraFeeAmount());
			
		}

		// segmentSummaryTO.setCurrentAdjAmount(currentAdjAmount);
		// segmentSummaryTO.setCurrentCnxAmount(currentCnxAmount);
		segmentSummaryTO.setCurrentFareAmount(currentFareAmount);
		// segmentSummaryTO.setCurrentModAmount(currentModAmount);
		segmentSummaryTO.setCurrentNonRefunds(currentNonRefunds);
		segmentSummaryTO.setCurrentRefunds(currentRefunds);
		segmentSummaryTO.setCurrentSurchargeAmount(currentSurchargeAmount);
		segmentSummaryTO.setCurrentTaxAmount(currentTaxAmount);
		segmentSummaryTO.setCurrentTotalPrice(currentTotalPrice);
		segmentSummaryTO.setOutBoundExternalCharge(newOutBoundFlexiCharge);
		segmentSummaryTO.setInBoundExternalCharge(newInBoundFlexiCharge);
		segmentSummaryTO.setNewAdjAmount(newAdjAmount);
		segmentSummaryTO.setCurrentDiscount(currentDiscount);
		segmentSummaryTO.setCurrentModificationPenatly(currentPenaltyAmount);

		if (totalCnxCharge != null) {
			segmentSummaryTO.setNewCnxAmount(totalCnxCharge);
		}

		if (totalModCharge != null) {
			segmentSummaryTO.setNewModAmount(totalModCharge);
		}

		segmentSummaryTO.setNewFareAmount(newFareAmount);
		segmentSummaryTO.setNewModAmount(newModAmount);
		segmentSummaryTO.setNewSurchargeAmount(newSurchargeAmount);
		segmentSummaryTO.setNewTaxAmount(newTaxAmount);
		segmentSummaryTO.setNewTotalPrice(newTotalPrice);
		segmentSummaryTO.setNewDiscount(newDiscount);
		segmentSummaryTO.setNewExtraFeeAmount(newExtraFeeAmount);

		return segmentSummaryTO;
	}

	private static void getOverRiddenLCCClientSegmentSummaryTO(BigDecimal totalCnxCharge, BigDecimal totalModCharge,
			LCCClientSegmentSummaryTO segmentSummaryTO) {

		BigDecimal effectiveCnxAmount = AccelAeroCalculator.add(totalCnxCharge, segmentSummaryTO.getNewCnxAmount().negate());
		BigDecimal effectiveModAmount = AccelAeroCalculator.add(totalModCharge, segmentSummaryTO.getNewModAmount().negate());
		segmentSummaryTO.setNewCnxAmount(totalCnxCharge);
		segmentSummaryTO.setNewModAmount(totalModCharge);
		segmentSummaryTO.setNewTotalPrice(AccelAeroCalculator.add(segmentSummaryTO.getNewTotalPrice(), effectiveCnxAmount,
				effectiveModAmount));

	}

	public static boolean isSelectedTraveler(String travelerRef, List<String> paxTravelerRefs) {
		travelerRef = travelerRef.replace(" ", "");
		// split this carrier wise
		String[] trrefArray = travelerRef.split(",");
		boolean found = false;
		for (String paxTravelerRef : paxTravelerRefs) {
			paxTravelerRef = paxTravelerRef.replace(" ", "");

			for (String traveler : trrefArray) {
				if (paxTravelerRef.indexOf(traveler) > -1) {
					found = true;
				} else {
					found = false;
				}
			}

			if (found) {
				return found;
			}

		}
		return false;
	}
	
	public static List<Integer> getExclusionPaxSequenceFromHandlingFee(Collection<LCCClientReservationPax> colPaxes,
			List<String> paxInfantList, List<String> paxAdultsList) {
		List<Integer> paxExclusionSeq = new ArrayList<Integer>();
		if (colPaxes != null && paxInfantList != null && paxAdultsList != null) {
			for (LCCClientReservationPax reservationPax : colPaxes) {
				if (isSelectedTraveler(reservationPax.getTravelerRefNumber(), paxAdultsList)
						|| isSelectedTraveler(reservationPax.getTravelerRefNumber(), paxInfantList)) {
					continue;
				}
				paxExclusionSeq.add(reservationPax.getPaxSequence());
			}
		}

		return paxExclusionSeq;
	}
}
