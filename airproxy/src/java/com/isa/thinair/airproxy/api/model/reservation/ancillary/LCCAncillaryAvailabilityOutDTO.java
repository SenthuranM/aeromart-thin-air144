package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCAncillaryAvailabilityOutDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private FlightSegmentTO flightSegmentTO;
	private List<LCCAncillaryStatus> ancillaryStatusList;

	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	public List<LCCAncillaryStatus> getAncillaryStatusList() {
		return ancillaryStatusList;
	}

	public void setAncillaryStatusList(List<LCCAncillaryStatus> ancillaryStatusList) {
		this.ancillaryStatusList = ancillaryStatusList;
	}

	public void addAncillaryStatus(LCCAncillaryStatus ancillaryStatus) {
		if (this.ancillaryStatusList == null) {
			this.ancillaryStatusList = new ArrayList<LCCAncillaryStatus>();
		}
		this.ancillaryStatusList.add(ancillaryStatus);
	}
}