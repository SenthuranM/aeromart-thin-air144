package com.isa.thinair.airproxy.api.utils;

/**
 * This should be in Airproxy. These could be used in both modules and web modules. Therefore moving to the webplatform
 * is out of the question. Having this in airproxy makes sense than keeping in commons.
 * 
 * @author mekanayake
 */
public interface PrivilegesKeys {

	public interface MakeResPrivilegesKeys {
		public static final String ACCEPT_CASH_PAYMNETS = "xbe.res.make.pay.cash";
		public static final String ACCEPT_BSP_PAYMNETS = "xbe.res.make.pay.bsp";
		public static final String ACCEPT_ONACCOUNT_PAYMNETS = "xbe.res.make.pay.acc";
		public static final String ACCEPT_ONACCOUNT_PAYMNETS_ANY = "xbe.res.make.pay.acc.any";
		public static final String ACCEPT_ONACCOUNT_PAYMNETS_REPORTING = "xbe.res.make.pay.acc.rpt";
		public static final String ACCEPT_CREDIT_CARD_PAYMNETS = "xbe.res.make.pay.card";
		public static final String ACCEPT_VOUCHER_PAYMNETS = "xbe.res.make.pay.voucher";
		public static final String ACCEPT_CREDIT_PAYMNETS = "xbe.res.make.pay.credit";
		public static final String ACCEPT_CREDIT_PAYMNETS_ANY = "xbe.res.make.pay.credit.any";
		public static final String ACCEPT_OFFLINE_PAYMNETS = "xbe.res.make.pay.offline.payment";
		public static final String ACCEPT_PARTIAL_PAYMNETS = "xbe.res.make.pay.pax";
		public static final String RES_ACCEPT_NO_PAY = "xbe.res.make.confirm.nopay";
		public static final String MAKE_RES_ALLFLIGHTS = "xbe.res.make.allflights";
		public static final String ALLOW_SEATBLOCKING = "xbe.res.make.seat.block";
		public static final String ALLOW_VIEW_AVAILABLESEATS = "xbe.res.make.view.avl";
		public static final String CHANGE_CURRENCY = "xbe.res.make.currency";
		public static final String RES_TRANSFER = "xbe.res.make.owner";
		public static final String RES_ANYCARRIER_TRANSFER = "xbe.res.make.anycarrier.owner";
		public static final String RES_MAKE_ONHOLD = "xbe.res.make.onhold";
		public static final String PRIVI_NAME_CHANGE_FARE_ANY = "xbe.res.make.alterfare.any";
		public static final String MAKE_STANDBY_BOOKING = "xbe.res.make.standby.booking";
		public static final String MAKE_WAITLIST_BOOKING = "xbe.res.make.waitlisting.booking";
		public static final String PRIVI_OVERRIDE_AGENT_FARE_ONLY = "xbe.res.make.alterfare.agfare";
		public static final String PRIVI_VIEW_PUBLIC_ONEWAY_FOR_RETURNS = "xbe.res.make.altfare.view.public.oneway.forreturn";
		public static final String PRIVI_OVERRIDE_VISIBLE_FARE = "xbe.res.make.alterfare";
		public static final String PRIVI_ADD_SEATS_IN_CUTOVER = "xbe.res.add.seatcutover";
		public static final String PRIVI_OVERRIDE_ONHOLD = "xbe.res.override.onhold";
		public static final String PRIVI_INSURANCE_OHD = "xbe.insurance.onhold";
		public static final String PRIVI_INSURANCE = "xbe.insurance";
		public static final String PRIVI_SSR_ADD = "xbe.res.add.ssr";
		public static final String LOAD_PROFILE = "xbe.res.make.profile";
		public static final String PRIVI_ADD_INF = "xbe.res.add.inf";
		public static final String MAKE_RES_CHILDONLY = "xbe.res.make.child.only";
		public static final String ALLOW_TBA = "xbe.res.make.tba";
		public static final String ALLOW_CAPTURE_EXTERNAL_INTERNATIONAL_FLT_DETAILS = "xbe.res.add.intl.flightdata";
		public static final String ALLOW_MULTICITY_SEARCH = "xbe.res.make.multicity"; 
		public static final String ADD_USER_NOTE = "xbe.res.usernote.add";
		public static final String CLASSIFY_USER_NOTE = "xbe.res.usernote.classify";
		public static final String ALLOW_BYPASS_MAX_PAX_LIMIT = "xbe.res.make.group";

		/**
		 * Privilege of uploading pax data from a csv file in XBE
		 */
		public static final String ALLOW_CSV_DETAIL_UPLOAD = "xbe.res.make.uplcsv";

		public static final String ALLOW_COS = "xbe.res.make.cos";
		public static final String ALLOW_CUST_ITI = "xbe.res.cust.iti";
		public static final String ALLOW_RCPT_PRINT = "xbe.res.rcpt.print";
		public static final String PRIVI_ITINERARY_FARE_MASK = "xbe.res.make.itin.fare.mask";
		public static final String PRIVI_DUPLI_NAME_SKIP = "xbe.res.make.dupli.name.skip";
		public static final String PRIVI_BAGGAGE_SHOW = "xbe.res.baggage.show";
		public static final String ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER = "xbe.res.baggage.allow.till.final.cut.over";
		public static final String ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG = "xbe.override.country.pax.config";
		public static final String ALLOW_OVERBOOK = "xbe.res.make.over.booking";
		public static final String VIEW_RES_PAX_NAMES = "xbe.res.view.paxnames";
		public static final String ALLOW_MAKE_OPEN_RETURN_BOOKINGS_FOR_DOMESTIC_FLIGHTS = "xbe.res.make.openreturn.booking";
		public static final String PRIVI_EXCLUDE_CHARGES = "xbe.res.exclude.charges";

		public static final String ACCEPT_MULTIPLE_AGENT_ONACCOUNT_PAYMNETS = "xbe.res.make.pay.mult.agent.acc";
		public static final String ALLOW_CREATE_USER_ALERT = "xbe.res.create.alert";
		public static final String ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME = "xbe.res.make.over.booking.after.cutofftime";
		public static final String ALLOW_MAKE_RESERVATION_AFTER_CUT_OFF_TIME = "xbe.res.create.bookings.after.cutofftime";
		public static final String CREATE_NEW_PNR_FROM_OLD = "xbe.res.make.create.from.old";
	}

	public interface AlterResPrivilegesKeys {
		public static final String OVERRIDE_CHARGES = "xbe.res.alt.charge";
		public static final String PAX_ADJUST_CREDIT = "xbe.res.alt.acc.adj";
		public static final String PAX_ADJUST_CREDIT_ANY = "xbe.res.alt.acc.adj.any";
		public static final String PAX_ADJUST_CREDIT_REFUND = "xbe.res.alt.acc.adj.refund";
		public static final String PAX_ADJUST_CREDIT_NONREFUND = "xbe.res.alt.acc.adj.nonrefund";
		public static final String PAX_ADJUST_CREDIT_EXP_DATE_CHANGE = "xbe.res.alt.acc.adj.expirychange";
		public static final String PAX_ADJUST_CREDIT_REINSTATE = "xbe.res.alt.acc.adj.reinstate";
		public static final String PAX_REFUND = "xbe.res.alt.acc.ref";
		public static final String ALT_RES_ADD_SEGMENT = "xbe.res.alt.seg.add";
		// public static final String ALT_RES_MODIFY_SEGMENT = "xbe.res.alt.seg.mod";
		public static final String ALT_RES_CANCEL_SEGMENT = "xbe.res.alt.seg.cnx";
		public static final String ALT_RES_TRANSFER_SEGMENT = "xbe.res.alt.seg.tnx";
		public static final String ALT_RES_VIEW_PAX_ACC = "xbe.res.alt.acc";
		public static final String ALT_RES_OLD = "xbe.res.alt.old";
		public static final String ALT_RES_OLD_SPECIAL_FARE = "xbe.res.alt.splfare";
		public static final String ALT_RES_NOSHOW = "xbe.res.alt.noshow";
		public static final String CANCEL_RES = "xbe.res.alt.cnx";
		public static final String CHANGE_CONTACT_DETAILS = "xbe.res.alt.contact";
		public static final String RES_ADD_INFANT = "xbe.res.alt.pax.inf";
		public static final String RES_REMOVE_PAX = "xbe.res.alt.pax.del";
		public static final String RES_BULK_TKT_REMOVE_PAX = "xbe.res.alt.pax.bulk.ticket.remove";
		public static final String RES_SPLIT = "xbe.res.alt.pax.split";
		public static final String RES_BULK_TKT_SPLIT = "xbe.res.alt.pax.bulk.ticket.split";
		public static final String RES_UPDATE = "xbe.res.alt.edit";
		public static final String RES_EXTEND = "xbe.res.alt.extend";
		public static final String RES_CHANGE_WL_PRIORITY = "xbe.res.alt.wlpriority";
		public static final String ALT_BUFFERTIME_SEG_OR_PAX = "xbe.res.alt.modify.buffer";
		public static final String ALT_BUFFERTIME_CHARGES = "xbe.res.alt.modify.buffer.charges";
		public static final String ALT_BUFFERTIME_NAME = "xbe.res.alt.modify.buffer.name";
		public static final String ALT_BUFFERTIME_SSR = "xbe.res.alt.modify.buffer.ssr";
		public static final String RES_ALLOW_BUFFERTIME_OHD = "xbe.res.make.onhold.buffer";
		public static final String ANY_PNR_SEARCH = "xbe.res.alt.find.any";
		public static final String ALT_RES_NO_PAY = "xbe.res.alt.confirm.nopay";
		public static final String ALT_RES_OHD_CONF = "xbe.res.alt.confirm.onhold";
		public static final String ALT_RES_OHD_NO_PAY = "xbe.res.alt.ohd.confirm.nopay";
		public static final String PRIVI_PAX_REFUND_CCARD = "xbe.res.alt.acc.ref.card";
		public static final String PRIVI_PAX_REFUND_CCARD_ANY = "xbe.res.alt.acc.ref.card.any";
		public static final String PRIVI_NAME_CHANGE = "xbe.res.alt.name";
		public static final String PRIVI_ONHOlD_NAME_CHANGE = "xbe.res.alt.name.ohd";
		public static final String PRIVI_TBA_NAME_CHANGE = "xbe.res.alt.tba";
		public static final String PRIVI_OVERRIDE_NAME_CHANGE = "xbe.res.alt.namechange.chg.override";
		public static final String PRIVI_OVERRIDE_REQUOTE_NAME_CHANGE = "xbe.res.alt.namechange.requote.override";
		public static final String PRIVI_TBA_GROUP_NAME_UPLOAD = "xbe.res.alt.tba.uplcsv";
		public static final String PRIVI_SSR_CHANGE = "xbe.res.alt.ssr";
		public static final String MAKE_RES_ITN_SELECT_PAX = "xbe.res.itn.selectpax";
		public static final String PRIVI_EXT_PAY_VIEW = "xbe.res.alt.ext.pay";
		public static final String PRIVI_EXT_PAY_VIEW_ALL = "xbe.res.alt.ext.pay.viewall";
		public static final String PRIVI_EXT_PAY_SYNC = "xbe.res.alt.ext.pay.sync";
		public static final String PRIVI_EXT_PAY_UPDATE = "xbe.res.alt.ext.pay.update";
		public static final String PRIVI_APPLY_CREDIT = "xbe.res.make.pay.credit";
		public static final String PRIVI_APPLY_CREDIT_ANY = "xbe.res.make.pay.credit.any";
		public static final String PRIVI_VIEW_CNX_ITEN = "xbe.res.alt.view.cnx.itn";
		public static final String PRIVI_SEAT_UPDATE = "xbe.res.alt.seat.edit";
		public static final String PRIVI_ONHOLD = "xbe.res.itn.onhold";
		public static final String PRIVI_ONHOLD_ROLL_FORWARD = "xbe.res.alt.ohd.roll.fwd";
		public static final String PRIVI_VIEW_CREDIT = "xbe.res.acc.view.pax.credit";

		public static final String ALT_RES_ALLOW_ACCESS_ANY_CHANNEL = "xbe.res.allow.all";
		public static final String ALT_RES_ALLOW_ACCESS_DNATA_CHANNEL = "xbe.res.allow.dnata";
		public static final String ALT_RES_ALLOW_ACCESS_HOLIDAY_CHANNEL = "xbe.res.allow.holiday";
		public static final String ALT_RES_ALLOW_ACCESS_IBE_CHANNEL = "xbe.res.allow.ibe";
		public static final String ALT_RES_ALLOW_ACCESS_XBE_CHANNEL = "xbe.res.allow.xbe";
		public static final String ALT_RES_ALLOW_FARE_BUFFER = "xbe.res.alt.modify.buffer.fare";
		public static final String ALT_RES_ALLOW_CNX_MODIFY = "xbe.res.alt.modify.cnx";
		public static final String ALT_RES_ALLOW_AEROMART_PAY = "xbe.res.alt.aeromartpay";
		public static final String ALT_RES_ALLOW_ADD_FFID = "xbe.res.alt.add.ffid";
		public static final String ALT_RES_ALLOW_EDIT_FFID = "xbe.res.alt.edit.ffid";
		public static final String ALT_RES_ALLOW_MODIFY_GDS_CS = "xbe.res.alt.modify.gds.cs";

		public static final String ALT_RES_ALLOW_ACCESS_IOS_CHANNEL = "xbe.res.allow.ios";
		public static final String ALT_RES_ALLOW_ACCESS_ANDROID_CHANNEL = "xbe.res.allow.android";
		/* segment modification */
		public static final String ALT_RES_MODIFY_SEGMENT_DATE = "xbe.res.alt.seg.mod.date";
		public static final String ALT_RES_MODIFY_SEGMENT_ROUTE = "xbe.res.alt.seg.mod.route";
		public static final String ALT_RES_MODIFY_PASSENGER_DETAIL = "xbe.res.alt.pax.detail";

		public static final String VIEW_ITN_WITH_PAYMENT_INFO = "xbe.res.itn.withpay";
		public static final String VIEW_ITN_WITHOUT_PAX_CHARGES = "xbe.res.itn.chg";
		public static final String VIEW_ITN_WITH_CHARGES = "xbe.res.itn.withcharges";
		public static final String VIEW_RES_HISTORY = "xbe.res.alt.history";
		public static final String VIEW_RES_HISTORY_OTHERS = "xbe.res.view.otherhistory";
		public static final String VIEW_RES_DETAILS = "xbe.res.alt.main";

		public static final String ADJUST_OTHER_CARRIER_CHARGES = "xbe.res.alt.acc.adj.other.carriers";
		public static final String MULTIPLE_CHARGE_ADJUSTMENT = "xbe.res.alt.acc.adj.multiple";
		public static final String ALLOW_REFUND_NO_CREDIT = "xbe.res.alt.acc.ref.nocred";
		public static final String ALLOW_CASH_REFUND = "xbe.res.alt.acc.ref.cash";
		public static final String ALLOW_ON_ACC_REFUND = "xbe.res.alt.acc.ref.acc";
		public static final String ALLOW_OFFLINE_REFUND = "xbe.res.alt.acc.ref.offline";
		public static final String ALLOW_BSP_REFUND = "xbe.res.alt.acc.ref.bsp";
		public static final String ALLOW_ON_ACC_ANY = "xbe.res.alt.acc.ref.acc.any";
		public static final String ALLOW_ON_ACCOUNT_RPT_REFUND = "xbe.res.alt.acc.ref.acc.rpt";
		public static final String ALLOW_ANY_CARRIER_REFUND = "xbe.res.alt.acc.ref.carrier.any";
		public static final String ALLOW_ANY_OPCARRIER_ON_ACC_REFUND = "xbe.res.alt.acc.ref.opcarrier.acc.any";
		public static final String ALLOW_KEEP_AGENT_COMMISSION_ON_REFUND = "xbe.res.alt.acc.ref.commission";
		public static final String ALLOW_LCC_ANY_OND_MODIFY = "xbe.res.lcc.allow.any.ond.modify";
		public static final String PRINT_ITN_FOR_PARTIAL_PAYMENTS = "xbe.res.make.pay.pax.print";
		public static final String CLEAR_ALERT = "xbe.res.alt.alert";
		public static final String ALLOW_VIEW_CREDIT = "xbe.res.creditview";
		public static final String ALLOW_ENDORSEMENTS = "xbe.res.alt.endorsement";
		public static final String PRIVI_ITINERARY_FARE_MASK_MODIFY = "xbe.res.alt.itin.fare.mask";
		public static final String PRINT_PNR_HISTORY_ALLOW = "xbe.res.alt.pnr.history.print";
		public static final String PRIVI_DUPLI_NAME_SKIP = "xbe.res.alt.dupli.name.skip";
		public static final String ALLOW_NO_SHOW_TAX_REVERSE = "xbe.res.alt.acc.noshow.tax.reverse";

		public static final String PRIVI_MODIFY_PASSENGER_COUPON_STATUS = "xbe.res.alt.pax.ticketCoupon.status";
		public static final String ALLOW_EXCEPTIONAL_ETICKET_COUPON_MODIFICATIONS = "xbe.res.alt.pax.ticketCoupon.status.exceptional";
		public static final String ALLOW_VOID_RESERVATION = "xbe.res.alt.modify.cnx.void";
		public static final String ALLOW_VOID_OWN_RESERVATION = "xbe.res.alt.modify.cnx.void.own.bookings";

		public static final String PRIVI_ALT_RES_AUTO_CNX = "xbe.res.alt.auto.cnx";
		public static final String PRIVI_VIEW_ALL_BC_FARE = "xbe.res.alt.view.allBC.avl";
		public static final String PRIVI_ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE = "xbe.res.allow.override.sameorhigher.fare";
		public static final String PRIVI_OVERRIDE_SAME_FARE_MODIFICATION = "xbe.res.allow.override.samefare";
		public static final String PRIVI_ALLOW_MODIFY_ANY_BC_ON_SAME_COS = "xbe.res.allow.modify.same.cos.any.bc";

		public static final String PRIVI_MODIFY_EXTENDED_NAME_WITHIN_BUFFER = "xbe.res.alt.extended.buffer.name";
		public static final String PRIVI_MODIFY_NAME_WITHIN_THRESHOLD = "xbe.res.alt.override.threshold.time.name";
		public static final String PRIVI_MODIFY_NAME_CREDITUTILIZED_PNR = "xbe.res.alt.modify.creditutilized.pnr.name";
		public static final String PRIVI_MODIFY_NAME_WITH_DEPARTED_SEGMENTS = "xbe.res.alt.modify.departed.segment.name";
		public static final String PRIVI_MODIFY_USER_NOTE_WITH_DEPARTED_SEGMENTS = "xbe.res.alt.modify.departed.segment.usernote";
		public static final String PRIVI_MODIFY_EXTENDED_NAME = "xbe.res.alt.extended.name";
		public static final String PRIVI_MODIFY_NAME_WITHIN_CUTOFF = "xbe.res.alt.modify.cutoff.time.name";

		public static final String RES_GROUP_BOOKING_SPLIT = "xbe.res.alt.pax.splitgroupbooking";
		public static final String ALT_CNX_RES_AFTER_CUTOFF_TIME = "xbe.res.alt.cnx.after.cutofftime";
		public static final String ALLOW_SKIP_NIC = "xbe.res.alt.nic.skip";
		public static final String PRIVI_ALLOW_EXTERNAL_TICKET_EXCHANGE = "xbe.res.alt.pax.external.ticket.exchange";

		public static final String ALLOW_VIEW_MCO = "xbe.ares.mco.view";
		public static final String ALL_REPORTING_AGENT_PNR_SEARCH = "xbe.res.alt.find.report.agent.all";
		public static final String REPORTING_AGENT_PNR_SEARCH = "xbe.res.alt.find.report.agent";
	}

	public interface XBESystemPrivilageKeys {
		public static final String PRIVI_SYSTEM_DEMO_DSIPLAY = "plan.fares360";
		public static final String PRIVI_SYSTEM_DBTOOLS_DSIPLAY = "xbe.tools.utility.display";
		public static final String PRIVI_SYSTEM_DBTOOLS_ANCI = "xbe.tools.dashboard.anci.view";
		
	}

	public interface AdminFuncPrivilegesKeys {
		public static final String ALLOW_UPDATE_RM_INV_OPT_RECOMENDATIONS = "plan.invn.rm.optimize.update";
		public static final String ALLOW_RM_INV_BATCH_UPDATE = "plan.invn.rm.batch.update";
	}

	public interface WSFuncPrivilegesKeys {
		public static final String ALLOW_WS_PING = "ws.ping.allow";
		public static final String ALLOW_WS_EBI_STATUS_CHECK = "ws.ebi.status.check";
		public static final String ALLOW_WS_EBI_DAILY_RECON = "ws.ebi.recon.daily";

		public static final String WS_RES_ALLOW_VIEW_ITINERARY_TERMS = "ws.res.itin.terms.view";
		public static final String WS_RES_ALLOW_VIEW_RES_HISTORY = "ws.res.history.view";
		public static final String WS_RES_ALLOW_VIEW_FLIGHT_LEG_LOAD = "ws.res.flightleg.load.view";

		public static final String WS_ADMIN_ALLOW_VIEW_AGENT_CREDIT_AVAILABLE = "ws.admin.ag.credit.view";
		public static final String WS_ADMIN_FLT_ALLOW_VIEW_DETAILS = "ws.admin.flt.details.view";
		public static final String WS_ADMIN_SCHEDULE_ALLOW_VIEW_DETAILS = "ws.admin.schedule.details.view";
		public static final String WS_ADD_LOYALTY_PROFILE = "ws.add.loyalty.profile";

		public static final String WS_EMAIL_ITINERARY_BALANCE_PAYMENT = "ws.res.email.balancepayment";
		public static final String WS_EMAIL_ITINERARY_CNFBOOKING = "ws.res.email.cnfbooking";
		public static final String WS_EMAIL_ITINERARY_OHDBOOKING = "ws.res.email.ohdbooking";

		public static final String WS_LOGIN_API_ACCESS = "ws.login.api.access";
		public static final String WS_LOGIN_API_GENERATE_TOKEN = "ws.login.api.generate.token";
		public static final String WS_LOGIN_API_VIEW_RESERVATIONS = "ws.login.api.view.reservations";

		public static final String WS_CREW_MGMT_ACCESS = "ws.crew.management.access";
		public static final String WS_CREW_MGMT_VIEW_MEAL_RECS = "ws.crew.management.meal.records";
		public static final String WS_RES_ALLOW_VIEW_FLIGHT_LOAD_INFO = "ws.res.flight.load.view";

        public static final String WS_ROUTES_FOR_GIVEN_ORIGINS = "ws.routes.by.origins.view";
        public static final String WS_CONNECTION_PX = "ws.connection.pax.view";


	}

	public interface MCOPrivilegesKeys {
		public static final String ALLOW_PRINT_MCO = "xbe.tool.mco.print";
		public static final String ALLOW_EMAIL_MCO = "xbe.tool.mco.email";
		public static final String ALLOW_SEARCH_OWN = "xbe.res.mco.search.own";
		public static final String ALLOW_SEARCH_ANY = "xbe.res.mco.search.any";
		public static final String ALLOW_SEARCH_ANY_MCO = "rpt.res.mco.summary.view.any";
	}

	public interface XBECustomerPrivilageKeys {
		public static final String XBE_CUSTOMER_EDIT = "xbe.cus.reg.edit";
		public static final String XBE_CUSTOMER_VIEW = "xbe.cus.reg.view";
		public static final String XBE_CUSTOMER_ADD = "xbe.cus.reg.add";
		public static final String XBE_CUSTOMER_FULL_SEARCH_ACCESS ="xbe.cus.reg.full.search";
		public static final String XBE_CUSTOMER_NAME_EDIT = "xbe.cus.reg.edit.name";
	}

}
