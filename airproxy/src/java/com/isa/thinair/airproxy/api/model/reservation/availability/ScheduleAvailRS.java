package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.util.Collection;

import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;

public class ScheduleAvailRS extends BaseAvailRS {

	private static final long serialVersionUID = 1L;

	private Collection<Collection<FlightScheduleInfoDTO>> flightSchedules;
	private Collection<Collection<FlightScheduleInfoDTO>> flightSchedulesReturns;

	public ScheduleAvailRS() {
	}

	public ScheduleAvailRS(Collection<Collection<FlightScheduleInfoDTO>> flightSchedules) {
		this.flightSchedules = flightSchedules;
	}

	/**
	 * @return the flightSchedulesReturns
	 */
	public Collection<Collection<FlightScheduleInfoDTO>> getFlightSchedulesReturns() {
		return flightSchedulesReturns;
	}

	/**
	 * @param flightSchedulesReturns
	 *            the flightSchedulesReturns to set
	 */
	public void setFlightSchedulesReturns(Collection<Collection<FlightScheduleInfoDTO>> flightSchedulesReturns) {
		this.flightSchedulesReturns = flightSchedulesReturns;
	}

	/**
	 * @return the flightSchedules
	 */
	public Collection<Collection<FlightScheduleInfoDTO>> getFlightSchedules() {
		return flightSchedules;
	}

	/**
	 * @param flightSchedules
	 *            the flightSchedules to set
	 */
	public void setFlightSchedules(Collection<Collection<FlightScheduleInfoDTO>> flightSchedules) {
		this.flightSchedules = flightSchedules;
	}
}
