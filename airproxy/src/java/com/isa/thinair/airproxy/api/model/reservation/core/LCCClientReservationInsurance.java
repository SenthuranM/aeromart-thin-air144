package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;

public class LCCClientReservationInsurance implements Serializable {

	private static final long serialVersionUID = 1L;

	private String insuranceQuoteRefNumber;
	private String operatingAirline;
	private String policyCode;
	private String origin;
	private String destination;
	private Date dateOfTravel;
	private Date dateOfReturn;
	private BigDecimal quotedTotalPremium;
	private LCCInsuredJourneyDTO insuredJourneyDTO;
	private String state;
	private String quotedCurrencyCode;
	private int insuranceType;
	private int applicablePaxCount;
	private boolean alertAutoCancellation;
	private boolean isAllDomastic = true;
	private String ssrFeeCode;
	private String planCode;

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getInsuranceQuoteRefNumber() {
		return insuranceQuoteRefNumber;
	}

	public void setInsuranceQuoteRefNumber(String insuranceQuoteRefNumber) {
		this.insuranceQuoteRefNumber = insuranceQuoteRefNumber;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public BigDecimal getQuotedTotalPremium() {
		return quotedTotalPremium;
	}

	/**
	 * field totalPremium was removed since, there is no requirement to have it in upper layers
	 */
	public void setQuotedTotalPremium(BigDecimal quotedTotalPremium) {
		this.quotedTotalPremium = quotedTotalPremium;
	}

	public LCCInsuredJourneyDTO getInsuredJourneyDTO() {
		return insuredJourneyDTO;
	}

	public void setInsuredJourneyDTO(LCCInsuredJourneyDTO insuredJourneyDTO) {
		this.insuredJourneyDTO = insuredJourneyDTO;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDateOfTravel() {
		return dateOfTravel;
	}

	public void setDateOfTravel(Date dateOfTravel) {
		this.dateOfTravel = dateOfTravel;
	}

	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setQuotedCurrencyCode(String quotedCurrencyCode) {
		this.quotedCurrencyCode = quotedCurrencyCode;
	}

	public String getQuotedCurrencyCode() {
		return quotedCurrencyCode;
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

	public int getApplicablePaxCount() {
		return applicablePaxCount;
	}

	public void setApplicablePaxCount(int applicablePaxCount) {
		this.applicablePaxCount = applicablePaxCount;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public boolean isAllDomastic() {
		return isAllDomastic;
	}

	public void setAllDomastic(boolean isAllDomastic) {
		this.isAllDomastic = isAllDomastic;
	}

	public String getSsrFeeCode() {
		return ssrFeeCode;
	}

	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientReservationInsurance [insuranceQuoteRefNumber=");
		builder.append(insuranceQuoteRefNumber);
		builder.append(", insuredJourneyDTO=");
		builder.append(insuredJourneyDTO);
		builder.append(", operatingAirline=");
		builder.append(operatingAirline);
		builder.append(", applicablePaxCount=");
		builder.append(applicablePaxCount);
		builder.append(", autoCnxEnabled=");
		builder.append(alertAutoCancellation);
		builder.append("]");
		return builder.toString();
	}

}
