package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

/**
 * @author Nilindra Fernando
 */
public class LCCClientExternalChgDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds whether or not the amount is consumed for payment */
	private boolean isAmountConsumedForPayment;

	private BigDecimal amount;

	private int journeyType;

	private EXTERNAL_CHARGES externalCharges;

	private String carrierCode;

	private String flightRefNumber;

	private String code;

	private String segmentCode;

	private int segmentSequence;

	// Holds the service applicable airport for airport services
	private String airportCode;

	private String applyOn;

	// Holds the additional user note for inflight services
	private String userNote;

	private String ondBaggageChargeGroupId;

	private String ondBaggageGroupId;

	private boolean isAnciOffer;

	private Integer offeredAnciTemplateID;

	/* Holds additional information which is required by airport transfer */
	private String transferDate;

	private String transferAddress;

	private String transferContact;
	
	private String transferType;
	
	private boolean ticketingRevenue;

	private BigDecimal taxableAmount;

	private BigDecimal nonTaxableAmount;
	
	private String taxDepositStateCode;
	
	private String taxDepositCountryCode;
	
	private boolean isServiceTaxAppliedToCCFee;

	/**
	 * This is for automatic checkin ancillary
	 */
	private String seatTypePreference;

	private String email;

	private String seatCode;

	/**
	 * CREATE_RES(1), MODIFY_SEGMENT(2), CANCEL_SEGMENT(3), ADD_SEGMENT(4), NAME_CHANGE(5), MODIFY_ANCI(6),
	 * REMOVE_PAX(7), ADD_INFANT(8)
	 */
	private Integer operationMode;

	public LCCClientExternalChgDTO(ExternalChgDTO externalChgDTO) {
		this();
		this.setAmount(externalChgDTO.getAmount());
		this.setJourneyType(externalChgDTO.getJourneyType());
		this.setExternalCharges(externalChgDTO.getExternalChargesEnum());
		this.setCode(externalChgDTO.getChargeCode());
		
		if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
			ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) externalChgDTO;
			this.setFlightRefNumber(serviceTaxExtChgDTO.getFlightRefNumber());
			this.setTaxableAmount(serviceTaxExtChgDTO.getTaxableAmount());
			this.setNonTaxableAmount(serviceTaxExtChgDTO.getNonTaxableAmount());
		}
	}

	public LCCClientExternalChgDTO() {
	}

	/**
	 * @return the isAmountConsumedForPayment
	 */
	public boolean isAmountConsumedForPayment() {
		return isAmountConsumedForPayment;
	}

	/**
	 * @param isAmountConsumedForPayment
	 *            the isAmountConsumedForPayment to set
	 */
	public void setAmountConsumedForPayment(boolean isAmountConsumedForPayment) {
		this.isAmountConsumedForPayment = isAmountConsumedForPayment;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public LCCClientExternalChgDTO clone() {
		LCCClientExternalChgDTO clone = new LCCClientExternalChgDTO();
		clone.setExternalCharges(this.getExternalCharges());
		clone.setAmount(this.getAmount());
		clone.setCode(this.getCode());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setCarrierCode(this.getCarrierCode());
		clone.setFlightRefNumber(this.getFlightRefNumber());
		clone.setSegmentCode(this.getSegmentCode());
		clone.setSegmentSequence(this.segmentSequence);
		clone.setAirportCode(this.airportCode);
		clone.setJourneyType(this.getJourneyType());
		clone.setOndBaggageGroupId(this.getOndBaggageGroupId());
		clone.setOndBaggageChargeGroupId(this.getOndBaggageChargeGroupId());
		clone.setAnciOffer(this.isAnciOffer);
		clone.setOfferedAnciTemplateID(this.offeredAnciTemplateID);
		clone.setServiceTaxAppliedToCCFee(this.isServiceTaxAppliedToCCFee);
		clone.setOperationMode(this.getOperationMode());
		clone.setSeatTypePreference(this.seatTypePreference);
		clone.setEmail(this.email);
		clone.setSeatCode(this.seatCode);

		return clone;
	}

	/**
	 * @return the externalCharges
	 */
	public EXTERNAL_CHARGES getExternalCharges() {
		return externalCharges;
	}

	/**
	 * @param externalCharges
	 *            the externalCharges to set
	 */
	public void setExternalCharges(EXTERNAL_CHARGES externalCharges) {
		this.externalCharges = externalCharges;
	}

	/**
	 * @return
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the flightRefNumber
	 */
	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	/**
	 * @param flightRefNumber
	 *            the flightRefNumber to set
	 */
	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the segmentSequence
	 */
	public int getSegmentSequence() {
		return segmentSequence;
	}

	/**
	 * @param segmentSequence
	 *            the segmentSequence to set
	 */
	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return
	 */
	public int getJourneyType() {
		return journeyType;
	}

	/**
	 * @param journeyType
	 */
	public void setJourneyType(int journeyType) {
		this.journeyType = journeyType;
	}

	/**
	 * @return the applyOn
	 */
	public String getApplyOn() {
		return applyOn;
	}

	/**
	 * @param applyOn
	 *            the applyOn to set
	 */
	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	/**
	 * @return the userNote
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            the userNote to set
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @return the ondBaggageChargeGroupId
	 */
	public String getOndBaggageChargeGroupId() {
		return ondBaggageChargeGroupId;
	}

	/**
	 * @param ondBaggageChargeGroupId
	 *            the ondBaggageChargeGroupId to set
	 */
	public void setOndBaggageChargeGroupId(String ondBaggageChargeGroupId) {
		this.ondBaggageChargeGroupId = ondBaggageChargeGroupId;
	}

	/**
	 * @return the ondBaggageGroupId
	 */
	public String getOndBaggageGroupId() {
		return ondBaggageGroupId;
	}

	/**
	 * @param ondBaggageGroupId
	 *            the ondBaggageGroupId to set
	 */
	public void setOndBaggageGroupId(String ondBaggageGroupId) {
		this.ondBaggageGroupId = ondBaggageGroupId;
	}

	/**
	 * @return the offeredAnciTemplateID
	 */
	public Integer getOfferedAnciTemplateID() {
		return offeredAnciTemplateID;
	}

	/**
	 * @param offeredAnciTemplateID
	 *            the offeredAnciTemplateID to set
	 */
	public void setOfferedAnciTemplateID(Integer offeredAnciTemplateID) {
		this.offeredAnciTemplateID = offeredAnciTemplateID;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public String getTransferContact() {
		return transferContact;
	}

	public void setTransferContact(String transferContact) {
		this.transferContact = transferContact;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public boolean isTicketingRevenue() {
		return ticketingRevenue;
	}

	public void setTicketingRevenue(boolean ticketingRevenue) {
		this.ticketingRevenue = ticketingRevenue;
	}

	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}

	public String getTaxDepositStateCode() {
		return taxDepositStateCode;
	}

	public void setTaxDepositStateCode(String taxDepositStateCode) {
		this.taxDepositStateCode = taxDepositStateCode;
	}

	public String getTaxDepositCountryCode() {
		return taxDepositCountryCode;
	}

	public void setTaxDepositCountryCode(String taxDepositCountryCode) {
		this.taxDepositCountryCode = taxDepositCountryCode;
	}

	public boolean isServiceTaxAppliedToCCFee() {
		return isServiceTaxAppliedToCCFee;
	}

	public void setServiceTaxAppliedToCCFee(boolean isServiceTaxAppliedToCCFee) {
		this.isServiceTaxAppliedToCCFee = isServiceTaxAppliedToCCFee;
	}

	public Integer getOperationMode() {
		return operationMode;
	}

	public void setOperationMode(Integer operationMode) {
		this.operationMode = operationMode;
	}

	/**
	 * @return the seatTypePreference
	 */
	public String getSeatTypePreference() {
		return seatTypePreference;
	}

	/**
	 * @param seatTypePreference
	 *            the seatTypePreference to set
	 */
	public void setSeatTypePreference(String seatTypePreference) {
		this.seatTypePreference = seatTypePreference;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}
	
}
