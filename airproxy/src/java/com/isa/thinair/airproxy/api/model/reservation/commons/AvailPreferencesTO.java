package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.BookingPaxType;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FareCategoryType;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.constants.ApplicationEngine;

public class AvailPreferencesTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// TODO refactor AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED to Enum
	private int restrictionLevel = AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

	private int flightsPerOndRestriction = AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS;

	private SYSTEM searchSystem = SYSTEM.ALL;

	private String bookingPaxType = BookingPaxType.ANY;

	private String fareCategoryType = FareCategoryType.ANY;

	/** Hold AccelAero System IBE /XBE */
	private ApplicationEngine appIndicator;

	// Modifing Segments
	/** Hold modifying Segment Fare ID **/
	@Deprecated
	private Integer oldFareID;
	/** Hold modifying Fare Type **/
	@Deprecated
	private Integer oldFareType;
	/** Hold modifying Fare Amount **/
	@Deprecated
	private BigDecimal oldFareAmount;
	@Deprecated
	private String oldFareCarrierCode;

	/** Hold modifying Segment information **/
	private List<FlightSegmentTO> oldflightSegmentList;

	private Date firstDepatureDate;

	private Date lastArrivalDate;

	private Collection<String> participatingCarriers;

	// Half return paramsf
	private boolean halfReturnFareQuote = false;

	private long stayOverTimeInMillis = 0;

	// TODO to be replaced with com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO
	private Collection<FlightSegmentDTO> existingSegmentsList;

	private Collection<FlightSegmentDTO> modifiedSegmentsList;

	private Collection<FlightSegmentDTO> inverseSegmentsList;
	@Deprecated
	private Integer inverseFareID;
	@Deprecated
	private boolean inboundFareModified = false;

	private boolean forceHalfReturnSearch = false;

	private String travelAgentCode;

	private String posAirportCode;

	@Deprecated
	private boolean modifyBooking = false;
	@Deprecated
	private boolean addSegment = false;

	private boolean quoteFares;
	@Deprecated
	private Map<Integer, Boolean> quoteOndFlexi = new HashMap<Integer, Boolean>();

	private Date maxTicketValidityDate;

	private boolean skipValidityFQ;

	private String preferredCurrency;
	@Deprecated
	private Map<Integer, Boolean> ondFlexiSelected = new HashMap<Integer, Boolean>();

	private Date ticketValidTill;

	private Date lastFareQuotedDate;
	@Deprecated
	private boolean isFixedFareAgent = false;
	@Deprecated
	private boolean isGoshoFareAgent = false;

	private boolean requoteFlightSearch = false;

	private boolean searchAllBC;

	private boolean includeHRTFares;

	private boolean allowFlightSearchAfterCutOffTime;

	private boolean multiCitySearch = false;

	/** Holds list of charges that should exclude from charge quote by privilege users */
	private List<String> excludedCharges;

	/**
	 * This will check route is eligible for promotion, because interline routes are not eligible for promotion. only
	 * own & dry routes are eligible.
	 */
	private boolean promoApplicable = true;

	/** Holds promotion code entered by user */
	private String promoCode;

	/** Holds BIN of bank for promotion applicability */
	private Integer bankIdentificationNumber;

	private boolean allowOverrideSameOrHigherFareMod = false;

	private String preferredLanguage;

	private boolean hasPrivAddSeatsInCutover;

	private boolean isAllowTillFinalCutOver;

	private Map<Double, List<Integer>> flightDateFareMap = new HashMap<Double, List<Integer>>();

	private Map<Integer, List<Integer>> existRetGropMap = new HashMap<Integer, List<Integer>>();
	private boolean isAddGroundSegment = false;

	private boolean allowDoOverbookAfterCutOffTime = false;

	private boolean allowDoOverbookBeforeCutOffTime = false;

	private boolean bundledFareApplicable = true;

	private String pnr;

	private boolean fromNameChange = false;

	private String pointOfSale;

	private boolean isSourceFlightInCutOffTime;

	private boolean fareCalendarSearch;

	private boolean preserveOndOrder;

	public int getRestrictionLevel() {
		return restrictionLevel;
	}

	public void setRestrictionLevel(int restrictionLevel) {
		this.restrictionLevel = restrictionLevel;
	}

	/**
	 * @return the bookingPaxType
	 */
	public String getBookingPaxType() {
		return bookingPaxType;
	}

	/**
	 * @param bookingPaxType
	 *            the bookingPaxType to set
	 */
	public void setBookingPaxType(String bookingPaxType) {
		this.bookingPaxType = bookingPaxType;
	}

	/**
	 * @return the fareCategoryType
	 */
	public String getFareCategoryType() {
		return fareCategoryType;
	}

	/**
	 * @param fareCategoryType
	 *            the fareCategoryType to set
	 */
	public void setFareCategoryType(String fareCategoryType) {
		this.fareCategoryType = fareCategoryType;
	}

	/**
	 * @return the searchSystem
	 */
	public SYSTEM getSearchSystem() {
		return searchSystem;
	}

	/**
	 * @param searchSystem
	 *            the searchSystem to set
	 */
	public void setSearchSystem(SYSTEM searchSystem) {
		this.searchSystem = searchSystem;
	}

	@Deprecated
	public String getOldFareCarrierCode() {
		return oldFareCarrierCode;
	}

	@Deprecated
	public void setOldFareCarrierCode(String oldFareCarrierCode) {
		this.oldFareCarrierCode = oldFareCarrierCode;
	}

	@Deprecated
	public BigDecimal getOldFareAmount() {
		return oldFareAmount;
	}

	@Deprecated
	public void setOldFareAmount(BigDecimal oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public List<FlightSegmentTO> getOldflightSegmentList() {
		return oldflightSegmentList;
	}

	public void setOldflightSegmentList(List<FlightSegmentTO> oldflightSegmentList) {
		this.oldflightSegmentList = oldflightSegmentList;
	}

	/**
	 * @return the participatingCarriers
	 */
	public Collection<String> getParticipatingCarriers() {
		return participatingCarriers;
	}

	/**
	 * @param participatingCarriers
	 *            the participatingCarriers to set
	 */
	public void setParticipatingCarriers(Collection<String> participatingCarriers) {
		this.participatingCarriers = participatingCarriers;
	}

	public Date getFirstDepatureDate() {
		return firstDepatureDate;
	}

	public void setFirstDepatureDate(Date firstDepatureDate) {
		this.firstDepatureDate = firstDepatureDate;
	}

	public Date getLastArrivalDate() {
		return lastArrivalDate;
	}

	public void setLastArrivalDate(Date lastArrivalDate) {
		this.lastArrivalDate = lastArrivalDate;
	}

	public boolean isHalfReturnFareQuote() {
		return halfReturnFareQuote;
	}

	public void setHalfReturnFareQuote(boolean halfReturnFareQuote) {
		this.halfReturnFareQuote = halfReturnFareQuote;
	}

	@Deprecated
	// We have change backend to pass immediate previous segment arrival segment. This can be removed with open return
	// changes
			public
			long getStayOverTimeInMillis() {
		return stayOverTimeInMillis;
	}

	@Deprecated
	public void setStayOverTimeInMillis(long stayOverTimeInMillis) {
		this.stayOverTimeInMillis = stayOverTimeInMillis;
	}

	public Collection<FlightSegmentDTO> getExistingSegmentsList() {
		return existingSegmentsList;
	}

	public void setExistingSegmentsList(Collection<FlightSegmentDTO> existingSegmentsList) {
		this.existingSegmentsList = existingSegmentsList;
	}

	public Collection<FlightSegmentDTO> getModifiedSegmentsList() {
		return modifiedSegmentsList;
	}

	public void setModifiedSegmentsList(Collection<FlightSegmentDTO> modifiedSegmentsList) {
		this.modifiedSegmentsList = modifiedSegmentsList;
	}

	public Collection<FlightSegmentDTO> getInverseSegmentsList() {
		return inverseSegmentsList;
	}

	public void setInverseSegmentsList(Collection<FlightSegmentDTO> inverseSegmentsList) {
		this.inverseSegmentsList = inverseSegmentsList;
	}

	@Deprecated
	public boolean isInboundFareModified() {
		return inboundFareModified;
	}

	@Deprecated
	public void setInboundFareModified(boolean inboundFareModified) {
		this.inboundFareModified = inboundFareModified;
	}

	// @Deprecated
	// public boolean isQuoteOutboundFlexi() {
	// return quoteOutboundFlexi;
	// }

	// @Deprecated
	// public void setQuoteOutboundFlexi(boolean quoteOutboundFlexi) {
	// this.quoteOutboundFlexi = quoteOutboundFlexi;
	// }

	public void setQuoteOndFlexi(int ondSequence, boolean quoteOndFlexi) {
		this.quoteOndFlexi.put(ondSequence, quoteOndFlexi);
	}

	@Deprecated
	public void setQuoteOndFlexi(Map<Integer, Boolean> quoteOndFlexi) {
		this.quoteOndFlexi = quoteOndFlexi;
	}

	@Deprecated
	public Map<Integer, Boolean> getQuoteOndFlexi() {
		return quoteOndFlexi;
	}

	// @Deprecated
	// public boolean isQuoteInboundFlexi() {
	// return quoteInboundFlexi;
	// }

	// @Deprecated
	// public void setQuoteInboundFlexi(boolean quoteInboundFlexi) {
	// this.quoteInboundFlexi = quoteInboundFlexi;
	// }

	public Integer getOldFareID() {
		return oldFareID;
	}

	@Deprecated
	public void setOldFareID(Integer oldFareID) {
		this.oldFareID = oldFareID;
	}

	@Deprecated
	public Integer getOldFareType() {
		return oldFareType;
	}

	@Deprecated
	public void setOldFareType(Integer oldFareType) {
		this.oldFareType = oldFareType;
	}

	public int getFlightsPerOndRestriction() {
		return flightsPerOndRestriction;
	}

	public void setFlightsPerOndRestriction(int flightsPerOndRestriction) {
		this.flightsPerOndRestriction = flightsPerOndRestriction;
	}

	public boolean isForceHalfReturnSearch() {
		return forceHalfReturnSearch;
	}

	public void setForceHalfReturnSearch(boolean forceHalfReturnSearch) {
		this.forceHalfReturnSearch = forceHalfReturnSearch;
	}

	@Deprecated
	public Integer getInverseFareID() {
		return inverseFareID;
	}

	@Deprecated
	public void setInverseFareID(Integer inverseFareID) {
		this.inverseFareID = inverseFareID;
	}

	public ApplicationEngine getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(ApplicationEngine appIndicator) {
		this.appIndicator = appIndicator;
	}

	public String getTravelAgentCode() {
		return travelAgentCode;
	}

	public void setTravelAgentCode(String travelAgentCode) {
		this.travelAgentCode = travelAgentCode;
	}

	@Deprecated
	public void setModifyBooking(boolean modifyBooking) {
		this.modifyBooking = modifyBooking;
	}

	@Deprecated
	public boolean isModifyBooking() {
		return modifyBooking;
	}

	@Deprecated
	public boolean isAddSegment() {
		return addSegment;
	}

	@Deprecated
	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

	public boolean isQuoteFares() {
		return quoteFares;
	}

	public void setQuoteFares(boolean quoteFares) {
		this.quoteFares = quoteFares;
	}

	public boolean isSkipValidityFQ() {
		return skipValidityFQ;
	}

	public void setSkipValidityFQ(boolean skipValidityFQ) {
		this.skipValidityFQ = skipValidityFQ;
	}

	/**
	 * @return the preferredCurrency
	 */
	public String getPreferredCurrency() {
		return preferredCurrency;
	}

	/**
	 * @param preferredCurrency
	 *            the preferredCurrency to set
	 */
	public void setPreferredCurrency(String preferredCurrency) {
		this.preferredCurrency = preferredCurrency;
	}

	@Deprecated
	public Map<Integer, Boolean> getOndFlexiSelected() {
		return ondFlexiSelected;
	}

	@Deprecated
	public void setOndFlexiSelected(Map<Integer, Boolean> ondFlexiSelected) {
		this.ondFlexiSelected = ondFlexiSelected;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public boolean isFixedFareAgent() {
		return isFixedFareAgent;
	}

	public void setFixedFareAgent(boolean isFixedFareAgent) {
		this.isFixedFareAgent = isFixedFareAgent;
	}

	public void setSearchAllBC(boolean searchAllBC) {
		this.searchAllBC = searchAllBC;
	}

	public boolean isSearchAllBC() {
		return searchAllBC;
	}

	public boolean isAllowFlightSearchAfterCutOffTime() {
		return allowFlightSearchAfterCutOffTime;
	}

	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime) {
		this.allowFlightSearchAfterCutOffTime = allowFlightSearchAfterCutOffTime;
	}

	public boolean isMultiCitySearch() {
		return multiCitySearch;
	}

	public void setMultiCitySearch(boolean multiCitySearch) {
		this.multiCitySearch = multiCitySearch;
	}

	public List<String> getExcludedCharges() {
		return excludedCharges;
	}

	public void setExcludedCharges(List<String> excludedCharges) {
		this.excludedCharges = excludedCharges;
	}

	/**
	 * @return the isGoshoFareAgent
	 */
	@Deprecated
	public boolean isGoshoFareAgent() {
		return isGoshoFareAgent;
	}

	/**
	 * @param isGoshoFareAgent
	 *            the isGoshoFareAgent to set
	 */
	@Deprecated
	public void setGoshoFareAgent(boolean isGoshoFareAgent) {
		this.isGoshoFareAgent = isGoshoFareAgent;
	}

	public boolean isPromoApplicable() {
		return promoApplicable;
	}

	public void setPromoApplicable(boolean promoApplicable) {
		this.promoApplicable = promoApplicable;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Integer getBankIdentificationNumber() {
		return bankIdentificationNumber;
	}

	public void setBankIdentificationNumber(Integer bankIdentificationNumber) {
		this.bankIdentificationNumber = bankIdentificationNumber;
	}

	/**
	 * @return the allowOverrideSameOrHigherFareMod
	 */
	public boolean isAllowOverrideSameOrHigherFareMod() {
		return allowOverrideSameOrHigherFareMod;
	}

	/**
	 * @param allowOverrideSameOrHigherFareMod
	 *            the allowOverrideSameOrHigherFareMod to set
	 */
	public void setAllowOverrideSameOrHigherFareMod(boolean allowOverrideSameOrHigherFareMod) {
		this.allowOverrideSameOrHigherFareMod = allowOverrideSameOrHigherFareMod;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public boolean isHasPrivAddSeatsInCutover() {
		return hasPrivAddSeatsInCutover;
	}

	public void setHasPrivAddSeatsInCutover(boolean hasPrivAddSeatsInCutover) {
		this.hasPrivAddSeatsInCutover = hasPrivAddSeatsInCutover;
	}

	public boolean isAllowTillFinalCutOver() {
		return isAllowTillFinalCutOver;
	}

	public void setAllowTillFinalCutOver(boolean isAllowTillFinalCutOver) {
		this.isAllowTillFinalCutOver = isAllowTillFinalCutOver;
	}

	public Map<Double, List<Integer>> getFlightDateFareMap() {
		return flightDateFareMap;
	}

	public void setFlightDateFareMap(Map<Double, List<Integer>> flightDateFareMap) {
		this.flightDateFareMap = flightDateFareMap;
	}

	public Map<Integer, List<Integer>> getExistRetGropMap() {
		return existRetGropMap;
	}

	public void setExistRetGropMap(Map<Integer, List<Integer>> existRetGropMap) {
		this.existRetGropMap = existRetGropMap;
	}

	public boolean isAddGroundSegment() {
		return isAddGroundSegment;
	}

	public void setAddGroundSegment(boolean isAddGroundSegment) {
		this.isAddGroundSegment = isAddGroundSegment;
	}

	public boolean isAllowDoOverbookAfterCutOffTime() {
		return allowDoOverbookAfterCutOffTime;
	}

	public void setAllowDoOverbookAfterCutOffTime(boolean allowDoOverbookAfterCutOffTime) {
		this.allowDoOverbookAfterCutOffTime = allowDoOverbookAfterCutOffTime;
	}

	public boolean isAllowDoOverbookBeforeCutOffTime() {
		return allowDoOverbookBeforeCutOffTime;
	}

	public void setAllowDoOverbookBeforeCutOffTime(boolean allowDoOverbookBeforeCutOffTime) {
		this.allowDoOverbookBeforeCutOffTime = allowDoOverbookBeforeCutOffTime;
	}

	public boolean isBundledFareApplicable() {
		return bundledFareApplicable;
	}

	public void setBundledFareApplicable(boolean bundledFareApplicable) {
		this.bundledFareApplicable = bundledFareApplicable;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isFromNameChange() {
		return fromNameChange;
	}

	public void setFromNameChange(boolean fromNameChange) {
		this.fromNameChange = fromNameChange;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public boolean isSourceFlightInCutOffTime() {
		return isSourceFlightInCutOffTime;
	}

	public void setSourceFlightInCutOffTime(boolean isSourceFlightInCutOffTime) {
		this.isSourceFlightInCutOffTime = isSourceFlightInCutOffTime;
	}

	public String getPosAirportCode() {
		return posAirportCode;
	}

	public void setPosAirportCode(String posAirportCode) {
		this.posAirportCode = posAirportCode;
	}

	public boolean isFareCalendarSearch() {
		return fareCalendarSearch;
	}

	public void setFareCalendarSearch(boolean fareCalendarSearch) {
		this.fareCalendarSearch = fareCalendarSearch;
	}

	public void setPreserveOndOrder(boolean preserveOndOrder) {
		this.preserveOndOrder = preserveOndOrder;
	}

	public boolean isPreserveOndOrder() {
		return preserveOndOrder;
	}

	public boolean isIncludeHRTFares() {
		return includeHRTFares;
	}

	public void setIncludeHRTFares(boolean includeHRTFares) {
		this.includeHRTFares = includeHRTFares;
	}
}
