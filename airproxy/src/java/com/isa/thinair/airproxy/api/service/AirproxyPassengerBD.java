package com.isa.thinair.airproxy.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PaxCreditReinstateTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.InfantInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AirproxyPassengerBD {
	public static final String SERVICE_NAME = "AirproxyReservationService";

	public LCCClientReservation addInfant(LCCClientReservation lccClientReservation, Integer intPaymentMode, boolean isGroupPNR,
			Map<Integer, LCCClientPaymentAssembler> payments, boolean enableFraudCheck, boolean autoCancellationEnabled,
			boolean hasBufferTimeAutoCnx, Map<String, String> mapPrivileges, TrackInfoDTO trackInfoDTO,
			boolean mcETGenerationEligible, Map<EXTERNAL_CHARGES, ExternalChgDTO> allExternalChargesMap) throws Exception;

	public InfantInfoTO getInfantCharge(String groupPNR, boolean allowAddInfant) throws ModuleException;

	public void reInstatePaxCredit(PaxCreditReinstateTO paxCreditReinstateTO, TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * @param isGroupPnr
	 * @param isManualRefund
	 * 
	 */
	public boolean refundPassenger(CommonReservationAssembler reservationAssembler, String userNotes, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			boolean removeAgentCommission, boolean isManualRefund) throws ModuleException;

	/**
	 * Passenger refund for multiple passengers and multiple refunds
	 * 
	 * @param userNotes
	 * @param isGroupPnr
	 * @param isManualRefund
	 */
	public boolean groupPassengerRefund(CommonReservationAssembler reservationAssembler, String userNotes, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO, Collection<String> preferredRefundOrder, Collection<Long> pnrPaxOndChgIds,
			boolean removeAgentCommission, boolean isManualRefund) throws ModuleException;

	public LCCClientReservation removePassenger(String groupPnr, String version, List<LCCClientReservationPax> reservationPaxs,
			boolean isGroupPnr, CustomChargesTO customChargesTO, TrackInfoDTO trackInfoDTO, String userNotes, Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges)
			throws ModuleException;

	public LCCClientReservationPax loadPassenger(String groupPNR, int pnrPaxId, TrackInfoDTO trackInfoDTO, boolean isGroupPnr)
			throws ModuleException;

	/**
	 * Uses for Loading Passenger & Contact Information Configurations
	 * 
	 * @param system
	 * @param appName
	 * @param carrierList
	 * @param trackInfo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public PaxContactConfigDTO loadPaxContactConfig(String system, String appName, List<String> carrierList,
			BasicTrackInfo trackInfo) throws ModuleException;

	public Map<String, List<PaxCountryConfigDTO>> loadPaxCountryConfig(String system, String appName,
			List<String> originDestinations, BasicTrackInfo trackInfo) throws ModuleException;
	
	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo, boolean allowExceptions)
			throws ModuleException;
	
	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions, boolean isGroupPnr) throws ModuleException;

}
