package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FareSegChargeTO implements Serializable {

	private static final long serialVersionUID = -3481275504200576965L;

	/** The fare type of the full journey. */
	private int journeyFareType;

	/** Total fare of the journey . For charge calculation */
	private Map<String, Double> totalJourneyFare;

	private Collection<OndFareSegChargeTO> ondFareSegChargeTOs;

	/** block seats collection. used for early block seats */
	private Collection<TempSegBcAlloc> blockSeatIds = null;

	private Map<String, BigDecimal> overridePaxFares;

	/** use for rebuilding OND Fares */
	private List<BundledFareDTO> ondBundledFareDTOs;

	private boolean inbound = false;

	private Map<Integer, Map<String, Double>> totalFareByJourneyMap;

	private Map<String, Double> paxWiseTotalCharges;
	
	private boolean allowOnhold;

	private BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	
	/**
	 * @return the jurneyFareType
	 */
	public int getJourneyFareType() {
		return journeyFareType;
	}

	/**
	 * @param jurneyFareType
	 *            the jurneyFareType to set
	 */
	public void setJourneyFareType(int jurneyFareType) {
		this.journeyFareType = jurneyFareType;
	}

	/**
	 * 
	 * @return the isBlockSeats
	 */
	public boolean hasBlockSeats() {
		return ((blockSeatIds != null) && (!blockSeatIds.isEmpty()));
	}

	/**
	 * @return the blockSeatIds
	 */
	public Collection<TempSegBcAlloc> getBlockSeatIds() {
		return blockSeatIds;
	}

	/**
	 * @param blockSeatIds
	 *            the blockSeatIds to set
	 */
	public void setBlockSeatIds(Collection<TempSegBcAlloc> blockSeatIds) {
		this.blockSeatIds = blockSeatIds;
	}

	/**
	 * @return the ondFareSegChargeTOs
	 */
	public Collection<OndFareSegChargeTO> getOndFareSegChargeTOs() {
		if (ondFareSegChargeTOs == null) {
			ondFareSegChargeTOs = new ArrayList<OndFareSegChargeTO>();
		}
		return ondFareSegChargeTOs;
	}

	/**
	 * @param ondFareSegChargeTOs
	 *            the ondFareSegChargeTOs to set
	 */
	public void setOndFareSegChargeTOs(Collection<OndFareSegChargeTO> ondFareSegChargeTOs) {
		this.ondFareSegChargeTOs = ondFareSegChargeTOs;
	}

	public void addOndFareSegChargeTOs(OndFareSegChargeTO ondFareSegChargeTO) {
		getOndFareSegChargeTOs().add(ondFareSegChargeTO);
	}

	/**
	 * @return the totalJourneyFare
	 */
	public Map<String, Double> getTotalJourneyFare() {
		if (totalJourneyFare == null) {
			totalJourneyFare = new HashMap<String, Double>();
		}
		return totalJourneyFare;
	}

	/**
	 * @param totalJourneyFare
	 *            the totalJourneyFare to set
	 */
	public void setTotalJourneyFare(Map<String, Double> totalJourneyFare) {
		this.totalJourneyFare = totalJourneyFare;
	}

	public void addTotalJourneyFare(String paxType, Double totalFare) {
		getTotalJourneyFare().put(paxType, totalFare);
	}

	public Map<String, BigDecimal> getOverridePaxFares() {
		if (overridePaxFares == null)
			overridePaxFares = new HashMap<String, BigDecimal>();
		return overridePaxFares;
	}

	/**
	 * Add the total Ond fare amount per pax (prorated intl fare for carrier) (per single carrier - not segment wise
	 * broken down) <br />
	 * e.g. SHJ/SAW <br />
	 * or BAH/SHJ/SAW
	 * 
	 * @param paxType
	 * @param fareAmt
	 */
	public void addOverridePaxFares(String paxType, BigDecimal fareAmt) {
		getOverridePaxFares().put(paxType, fareAmt);
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	public void addFareTypeByJourneyMap(Integer subJourney, Integer fareType) {
	}

	public Map<Integer, Map<String, Double>> getTotalFareByJourneyMap() {

		if (totalFareByJourneyMap == null) {
			totalFareByJourneyMap = new HashMap<Integer, Map<String, Double>>();
		}

		return totalFareByJourneyMap;
	}

	public void setTotalFareByJourneyMap(Map<Integer, Map<String, Double>> totalFareByJourneyMap) {
		this.totalFareByJourneyMap = totalFareByJourneyMap;
	}

	public void addJourneyWiseTotalFareByPaxType(Integer subJourney, String paxType, Double fareAmount) {

		Map<String, Double> fareMap = getTotalFareByJourneyMap().get(subJourney);

		if (fareMap == null) {
			fareMap = new HashMap<String, Double>();
		}

		Double totalFare = new Double(0);

		if (fareMap.get(paxType) != null && fareMap.get(paxType).doubleValue() > 0) {
			totalFare = fareMap.get(paxType);
		}

		totalFare = totalFare + fareAmount;

		fareMap.put(paxType, totalFare);

		getTotalFareByJourneyMap().put(subJourney, fareMap);

	}

	public void addTotalFareByJourneyMap(Integer subJourney, Map<String, Double> fareMap) {
		getTotalFareByJourneyMap().put(subJourney, fareMap);
	}

	public Map<String, Double> getSubJourneyTotalFare(Integer subJourney) {

		if (getTotalFareByJourneyMap().get(subJourney) != null) {
			return getTotalFareByJourneyMap().get(subJourney);
		}

		return null;
	}

	public List<BundledFareDTO> getOndBundledFareDTOs() {
		return ondBundledFareDTOs;
	}

	public void setOndBundledFareDTOs(List<BundledFareDTO> ondBundledFareDTOs) {
		this.ondBundledFareDTOs = ondBundledFareDTOs;
	}

	public Map<String, Double> getPaxWiseTotalCharges() {
		if (paxWiseTotalCharges == null) {
			paxWiseTotalCharges = new HashMap<String, Double>();
		}
		return paxWiseTotalCharges;
	}

	public void setPaxWiseTotalCharges(Map<String, Double> paxWiseTotalCharges) {
		this.paxWiseTotalCharges = paxWiseTotalCharges;
	}

	public void addPaxWiseTotalCharges(String paxType, Double totalFare) {
		getPaxWiseTotalCharges().put(paxType, totalFare);
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public void setAllowOnhold(boolean allowOnhold) {
		this.allowOnhold = allowOnhold;
	}

	public BigDecimal getTotalServiceTaxAmount() {
		return totalServiceTaxAmount;
	}

	public void setTotalServiceTaxAmount(BigDecimal totalServiceTaxAmount) {
		this.totalServiceTaxAmount = totalServiceTaxAmount;
	}


	
}
