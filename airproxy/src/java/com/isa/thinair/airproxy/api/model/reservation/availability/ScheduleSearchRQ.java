package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eric
 * 
 */
public class ScheduleSearchRQ implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fromAirport;
	private String toAirport;
	private Date fromDate;
	private Date toDate;
	private boolean roundTrip;
	private String agentCode;

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}	

}
