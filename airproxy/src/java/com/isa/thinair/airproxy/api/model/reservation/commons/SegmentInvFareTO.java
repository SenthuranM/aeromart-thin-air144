package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SegmentInvFareTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String flightRefNumber;
	private List<InvFareTypeTO> invetoryFareTypes;

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public List<InvFareTypeTO> getInvetoryFareTypes() {
		if (invetoryFareTypes == null) {
			invetoryFareTypes = new ArrayList<InvFareTypeTO>();
		}
		return invetoryFareTypes;
	}

	public void setInvetoryFareTypes(List<InvFareTypeTO> invetoryFareTypes) {
		this.invetoryFareTypes = invetoryFareTypes;
	}

	public void addInvetoryFareType(InvFareTypeTO invFareTypeTO) {
		getInvetoryFareTypes().add(invFareTypeTO);
	}

}
