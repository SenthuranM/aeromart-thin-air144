package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;

public class LCCAncillaryStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	private LCCAncillaryType ancillaryType;
	private boolean available;

	public LCCAncillaryType getAncillaryType() {
		return ancillaryType;
	}

	public void setAncillaryType(LCCAncillaryType ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
}