package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.gdsservices.api.dto.external.NameDTO;

public class LCCInsuredPassengerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean policyHolder;
	private String insurancePlan;
	private Date birthDate;
	private NameDTO nameDTO;
	private LCCAddressDTO addressDTO;
	private String homePhoneNumber;
	private String email;
	private String nationality;

	public boolean isPolicyHolder() {
		return policyHolder;
	}

	public void setPolicyHolder(boolean policyHolder) {
		this.policyHolder = policyHolder;
	}

	public String getInsurancePlan() {
		return insurancePlan;
	}

	public void setInsurancePlan(String insurancePlan) {
		this.insurancePlan = insurancePlan;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public NameDTO getNameDTO() {
		return nameDTO;
	}

	public void setNameDTO(NameDTO nameDTO) {
		this.nameDTO = nameDTO;
	}

	public LCCAddressDTO getAddressDTO() {
		return addressDTO;
	}

	public void setAddressDTO(LCCAddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
}
