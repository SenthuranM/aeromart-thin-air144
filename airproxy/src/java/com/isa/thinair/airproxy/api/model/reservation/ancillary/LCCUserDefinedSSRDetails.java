package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.core.util.StringUtil;

public class LCCUserDefinedSSRDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> userDefinedSsrCodes;
	private List<String> ssrCodes;

	public boolean isUserDefinedSSROnly() {
		return (isUserDefinedSSRExist() && getSsrCodes().size() == 0);
	}

	public boolean isUserDefinedSSRExist() {
		return (getUserDefinedSsrCodes().size() > 0);
	}	

	public List<String> getUserDefinedSsrCodes() {
		if (userDefinedSsrCodes == null) {
			userDefinedSsrCodes = new ArrayList<String>();
		}
		return userDefinedSsrCodes;
	}

	public void addUserDefinedSsrCode(String ssrCode) {

		if (!StringUtil.isNullOrEmpty(ssrCode))
			this.getUserDefinedSsrCodes().add(ssrCode);

	}

	public List<String> getSsrCodes() {
		if (ssrCodes == null) {
			ssrCodes = new ArrayList<String>();
		}
		return ssrCodes;
	}

	public void addSsrCode(String ssrCode) {

		if (!StringUtil.isNullOrEmpty(ssrCode))
			this.getSsrCodes().add(ssrCode);

	}

}
