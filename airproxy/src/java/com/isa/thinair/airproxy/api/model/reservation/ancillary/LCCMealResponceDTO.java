package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCMealResponceDTO implements Serializable {

	private final static long serialVersionUID = 1L;
	private List<LCCFlightSegmentMealsDTO> flightSegmentMeals;
	private boolean isMultipleMealSelectionEnabled;

	public List<LCCFlightSegmentMealsDTO> getFlightSegmentMeals() {
		if (flightSegmentMeals == null) {
			flightSegmentMeals = new ArrayList<LCCFlightSegmentMealsDTO>();
		}
		return flightSegmentMeals;
	}

	public boolean isMultipleMealSelectionEnabled() {
		return isMultipleMealSelectionEnabled;
	}

	public void setMultipleMealSelectionEnabled(boolean isMultipleMealSelectionEnabled) {
		this.isMultipleMealSelectionEnabled = isMultipleMealSelectionEnabled;
	}
}
