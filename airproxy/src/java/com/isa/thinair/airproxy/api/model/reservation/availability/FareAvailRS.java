package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;

public class FareAvailRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<SegmentInvFareTO> segmentInvFares;

	public List<SegmentInvFareTO> getSegmentInvFares() {
		if (segmentInvFares == null) {
			segmentInvFares = new ArrayList<SegmentInvFareTO>();
		}
		return segmentInvFares;
	}

	public void setSegmentInvFares(List<SegmentInvFareTO> segmentInvFares) {
		this.segmentInvFares = segmentInvFares;
	}

	public void addSegmentInvFare(SegmentInvFareTO segmentInvFareTO) {
		getSegmentInvFares().add(segmentInvFareTO);
	}
}
