package com.isa.thinair.airproxy.api.model.reservation.core;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * @author Nilindra Fernando
 * @since 5:07 PM 11/5/2009
 */
public class LCCClientResAlterModesTO extends LCCClientResAlterQueryModesTO {

	private static final long serialVersionUID = 1703785773439940736L;

	private Integer paymentTypes;

	private AppIndicatorEnum appIndicator;

	private String selectedPnrSegID; // set from Add Segment

	private boolean hasMadePayments;

	public static LCCClientResAlterModesTO composeCancelSegmentRequest(String groupPNR,
			Collection<LCCClientReservationSegment> oldPnrSegs, Collection<LCCClientReservationSegment> colExistingAllPnrSegs,
			BigDecimal customAdultCancelCharge, BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge,
			String version, String reservationStatus) {

		LCCClientResAlterModesTO lccClientResAlterModesTO = new LCCClientResAlterModesTO(MODIFY_MODES.CANCEL_OND);
		lccClientResAlterModesTO.setPNR(groupPNR);
		lccClientResAlterModesTO.setOldPnrSegs(oldPnrSegs); // selected segments to be canceled
		lccClientResAlterModesTO.setExistingAllSegs(colExistingAllPnrSegs); // all existing segments in the reservation
		lccClientResAlterModesTO.setCustomAdultCharge(customAdultCancelCharge);
		lccClientResAlterModesTO.setCustomChildCharge(customChildCancelCharge);
		lccClientResAlterModesTO.setCustomInfantCharge(customInfantCancelCharge);
		lccClientResAlterModesTO.setVersion(version);
		lccClientResAlterModesTO.setReservationStatus(reservationStatus);

		return lccClientResAlterModesTO;
	}

	public static LCCClientResAlterModesTO composeAddSegmentRequest(String groupPNR,
			Collection<LCCClientReservationSegment> oldPnrSegs, Collection<LCCClientReservationSegment> existingAllSegs,
			LCCClientSegmentAssembler lccClientSegmentAssembler, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge, String version, String selectedPnrSegID,
			String reservationStatus) throws ModuleException {

		LCCClientResAlterModesTO lccClientResAlterModesTO = new LCCClientResAlterModesTO(MODIFY_MODES.ADD_OND);
		lccClientResAlterModesTO.setPNR(groupPNR);
		lccClientResAlterModesTO.setOldPnrSegs(oldPnrSegs);
		lccClientResAlterModesTO.setExistingAllSegs(existingAllSegs);
		lccClientResAlterModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);
		lccClientResAlterModesTO.setCustomAdultCharge(customAdultCancelCharge);
		lccClientResAlterModesTO.setCustomChildCharge(customChildCancelCharge);
		lccClientResAlterModesTO.setCustomInfantCharge(customInfantCancelCharge);
		lccClientResAlterModesTO.setVersion(version);
		lccClientResAlterModesTO.setSelectedPnrSegID(selectedPnrSegID);
		lccClientResAlterModesTO.setReservationStatus(reservationStatus);
		return lccClientResAlterModesTO;
	}

	public static LCCClientResAlterModesTO composeModifySegmentRequest(String groupPNR,
			Collection<LCCClientReservationSegment> oldPnrSegs, Collection<LCCClientReservationSegment> colExistingAllPnrSegs,
			LCCClientSegmentAssembler lccClientSegmentAssembler, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge, String version, String reservationStatus) {

		LCCClientResAlterModesTO lccClientResAlterModesTO = new LCCClientResAlterModesTO(MODIFY_MODES.MODIFY_OND);
		lccClientResAlterModesTO.setPNR(groupPNR);
		lccClientResAlterModesTO.setOldPnrSegs(oldPnrSegs);
		lccClientResAlterModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);
		lccClientResAlterModesTO.setExistingAllSegs(colExistingAllPnrSegs);
		lccClientResAlterModesTO.setCustomAdultCharge(customAdultCancelCharge);
		lccClientResAlterModesTO.setCustomChildCharge(customChildCancelCharge);
		lccClientResAlterModesTO.setCustomInfantCharge(customInfantCancelCharge);
		lccClientResAlterModesTO.setVersion(version);
		lccClientResAlterModesTO.setReservationStatus(reservationStatus);

		return lccClientResAlterModesTO;
	}

	public static LCCClientResAlterModesTO composeCancelReservationRequest(String groupPNR, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge, String version, String userNotes) {

		LCCClientResAlterModesTO lccClientResAlterModesTO = new LCCClientResAlterModesTO(MODIFY_MODES.CANCEL_RES);
		lccClientResAlterModesTO.setPNR(groupPNR);
		lccClientResAlterModesTO.setCustomAdultCharge(customAdultCancelCharge);
		lccClientResAlterModesTO.setCustomChildCharge(customChildCancelCharge);
		lccClientResAlterModesTO.setCustomInfantCharge(customInfantCancelCharge);
		lccClientResAlterModesTO.setVersion(version);
		lccClientResAlterModesTO.setUserNotes(userNotes);

		return lccClientResAlterModesTO;
	}

	protected LCCClientResAlterModesTO(MODIFY_MODES modifyModes) {
		super(modifyModes);
	}

	public Integer getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(Integer paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public String getSelectedPnrSegID() {
		return selectedPnrSegID;
	}

	public void setSelectedPnrSegID(String selectedPnrSegID) {
		this.selectedPnrSegID = selectedPnrSegID;
	}

	public boolean HasMadePayments() {
		return hasMadePayments;
	}

	public void setHasMadePayments(boolean hasMadePayments) {
		this.hasMadePayments = hasMadePayments;
	}

}
