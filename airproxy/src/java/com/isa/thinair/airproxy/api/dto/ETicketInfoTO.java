package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.Date;

public class ETicketInfoTO implements Serializable {
	private Date depDateZulu;

	private long eTicketNo;

	private String flightNumber;

	private String segmentCode;

	public Date getDepDateZulu() {
		return depDateZulu;
	}

	public void setDepDateZulu(Date depDateZulu) {
		this.depDateZulu = depDateZulu;
	}

	public long geteTicketNo() {
		return eTicketNo;
	}

	public void seteTicketNo(long eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public boolean equals(Object obj) {
		if ((obj instanceof ETicketInfoTO) && ((ETicketInfoTO) obj).getDepDateZulu().equals(this.getDepDateZulu())
				&& ((ETicketInfoTO) obj).geteTicketNo() == this.geteTicketNo()
				&& ((ETicketInfoTO) obj).getDepDateZulu().equals(this.getDepDateZulu())
				&& ((ETicketInfoTO) obj).getSegmentCode().equals(this.getSegmentCode())) {
			return true;
		} else {
			return false;
		}
	}

	public String toString() {
		return segmentCode + ":" + eTicketNo + ":" + flightNumber + ":" + depDateZulu.toString();
	}
}
