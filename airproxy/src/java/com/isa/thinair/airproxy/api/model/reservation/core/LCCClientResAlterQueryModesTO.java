package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

/**
 * @author Nilindra Fernando
 * @since 5:07 PM 11/5/2009
 */
public class LCCClientResAlterQueryModesTO implements Serializable {

	private static final long serialVersionUID = -981139775748815547L;

	private String groupPNR;

	private Collection<LCCClientReservationSegment> oldPnrSegs; // selectedPnrSeg

	private Collection<LCCClientReservationSegment> existingAllSegs; // allPnrSeg

	private LCCClientSegmentAssembler lccClientSegmentAssembler;

	private String version;

	private MODIFY_MODES modifyModes;

	private boolean groupPnr;

	private List<String> paxInfantList;

	private List<String> paxAdultsList;

	private LCCClientReservationPax addInfant;

	private Set<LCCClientReservationPax> passengers;

	private BigDecimal customAdultCharge;

	private BigDecimal customChildCharge;

	private BigDecimal customInfantCharge;

	private BigDecimal defaultCustomAdultCharge;

	private BigDecimal defaultCustomChildCharge;

	private BigDecimal defaultCustomInfantCharge;

	private PnrChargeDetailTO adultCustomChargeTO;

	private PnrChargeDetailTO childCustomChargeTO;

	private PnrChargeDetailTO infantCustomChargeTO;

	private String routeType;

	private boolean isSendingAbosoluteCharge;

	private String reservationStatus;

	private String userNotes;

	private Map<String, BigDecimal> ondWiseAdultCharge;

	private Map<String, BigDecimal> ondWiseChildCharge;

	private Map<String, BigDecimal> ondWiseInfantCharge;

	private Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates;

	private boolean actualPayment = true;
	
	private boolean infantPaymentSeparated = false;

	private Map<Integer, CnxModPenServiceTaxDTO> paxWiseCnxModPenServiceTaxes;

	public static enum MODIFY_MODES {

		CANCEL_RES, CANCEL_OND, MODIFY_OND, ADD_OND, ADD_INF, REMOVE_PAX, VOID_RES
	};

	/**
	 * Holds applicable Applicable Route types
	 * 
	 */
	public static enum RouteTypes {

		PER_OND("POND"), TOTAL_ROUTE("TOT");

		private String routeType;

		private RouteTypes(String routeType) {
			this.routeType = routeType;
		}

		public String getRouteTypes() {
			return routeType;
		}
	};

	public static LCCClientResAlterQueryModesTO composeCancelSegmentQueryRequest(String groupPNR,
			Collection<LCCClientReservationSegment> oldPnrSegs, String version, boolean blnGroupPnr) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.CANCEL_OND);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setOldPnrSegs(oldPnrSegs);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);

		return lccClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO composeModifySegmentQueryRequest(String groupPNR,
			Collection<LCCClientReservationSegment> oldPnrSegs, LCCClientSegmentAssembler newlccClientSegmentAssembler,
			String version, boolean blnGroupPnr) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.MODIFY_OND);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setOldPnrSegs(oldPnrSegs);
		lccClientResAlterQueryModesTO.setLccClientSegmentAssembler(newlccClientSegmentAssembler);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);

		return lccClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO composeCancelReservationQueryRequest(String groupPNR, String version,
			boolean blnGroupPnr) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.CANCEL_RES);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);

		return lccClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO composeVoidReservationQueryRequest(String groupPNR, String version,
			boolean blnGroupPnr) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.VOID_RES);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);

		return lccClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO composeRemovePaxQueryRequest(String groupPNR, String version,
			List<String> adultPnrPaxIds, List<String> infantPnrPaxIds, boolean blnGroupPnr,
			Set<LCCClientReservationPax> passengers) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.REMOVE_PAX);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setPaxAdultsList(adultPnrPaxIds);
		lccClientResAlterQueryModesTO.setPaxInfantList(infantPnrPaxIds);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);
		lccClientResAlterQueryModesTO.setPassengers(passengers);
		return lccClientResAlterQueryModesTO;
	}

	public static LCCClientResAlterQueryModesTO composeAddInfantQueryRequest(String groupPNR, String version,
			boolean blnGroupPnr, LCCClientReservationPax addInfant) {
		LCCClientResAlterQueryModesTO lccClientResAlterQueryModesTO = new LCCClientResAlterQueryModesTO(MODIFY_MODES.ADD_INF);
		lccClientResAlterQueryModesTO.setPNR(groupPNR);
		lccClientResAlterQueryModesTO.setVersion(version);
		lccClientResAlterQueryModesTO.setGroupPnr(blnGroupPnr);
		lccClientResAlterQueryModesTO.setAddInfant(addInfant);
		return lccClientResAlterQueryModesTO;
	}

	protected LCCClientResAlterQueryModesTO(MODIFY_MODES modifyModes) {
		this.modifyModes = modifyModes;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the modifyModes
	 */
	public MODIFY_MODES getModifyModes() {
		return modifyModes;
	}

	/**
	 * @param modifyModes
	 *            the modifyModes to set
	 */
	public void setModifyModes(MODIFY_MODES modifyModes) {
		this.modifyModes = modifyModes;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the oldPnrSegs
	 */
	public Collection<LCCClientReservationSegment> getOldPnrSegs() {
		return oldPnrSegs;
	}

	/**
	 * @param oldPnrSegs
	 *            the oldPnrSegs to set
	 */
	public void setOldPnrSegs(Collection<LCCClientReservationSegment> oldPnrSegs) {
		this.oldPnrSegs = oldPnrSegs;
	}

	/**
	 * @return the lccClientSegmentAssembler
	 */
	public LCCClientSegmentAssembler getLccClientSegmentAssembler() {
		return lccClientSegmentAssembler;
	}

	/**
	 * @param lccClientSegmentAssembler
	 *            the lccClientSegmentAssembler to set
	 */
	public void setLccClientSegmentAssembler(LCCClientSegmentAssembler lccClientSegmentAssembler) {
		this.lccClientSegmentAssembler = lccClientSegmentAssembler;
	}

	/**
	 * @return the paxInfantList
	 */
	public List<String> getPaxInfantList() {
		return paxInfantList;
	}

	/**
	 * @param paxInfantList
	 *            the paxInfantList to set
	 */
	public void setPaxInfantList(List<String> paxInfantList) {
		this.paxInfantList = paxInfantList;
	}

	/**
	 * @return the paxAdultsList
	 */
	public List<String> getPaxAdultsList() {
		return paxAdultsList;
	}

	/**
	 * @param paxAdultsList
	 *            the paxAdultsList to set
	 */
	public void setPaxAdultsList(List<String> paxAdultsList) {
		this.paxAdultsList = paxAdultsList;
	}

	/**
	 * @return the paxAdultsList
	 */
	public LCCClientReservationPax getAddInfant() {
		return addInfant;
	}

	/**
	 * @param addInfant
	 *            the addInfant to set
	 */
	public void setAddInfant(LCCClientReservationPax addInfant) {
		this.addInfant = addInfant;
	}

	/**
	 * @return the groupPnr
	 */
	public boolean isGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr
	 *            the groupPnr to set
	 */
	public void setGroupPnr(boolean groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return
	 */
	public Set<LCCClientReservationPax> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 */
	public void setPassengers(Set<LCCClientReservationPax> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return
	 */
	public BigDecimal getCustomAdultCharge() {
		return customAdultCharge;
	}

	/**
	 * @param customAdultCharge
	 */
	public void setCustomAdultCharge(BigDecimal customAdultCharge) {
		this.customAdultCharge = customAdultCharge;
	}

	/**
	 * @return
	 */
	public BigDecimal getCustomChildCharge() {
		return customChildCharge;
	}

	/**
	 * @param customChildCharge
	 */
	/**
	 * @param customChildCharge
	 */
	public void setCustomChildCharge(BigDecimal customChildCharge) {
		this.customChildCharge = customChildCharge;
	}

	/**
	 * @return
	 */
	public BigDecimal getCustomInfantCharge() {
		return customInfantCharge;
	}

	/**
	 * @param customInfantCharge
	 */
	public void setCustomInfantCharge(BigDecimal customInfantCharge) {
		this.customInfantCharge = customInfantCharge;
	}

	/**
	 * @return
	 */
	public PnrChargeDetailTO getAdultCustomChargeTO() {
		return adultCustomChargeTO;
	}

	/**
	 * @param adultCustomChargeTO
	 */
	public void setAdultCustomChargeTO(PnrChargeDetailTO adultCustomChargeTO) {
		this.adultCustomChargeTO = adultCustomChargeTO;
	}

	/**
	 * @return
	 */
	public PnrChargeDetailTO getChildCustomChargeTO() {
		return childCustomChargeTO;
	}

	/**
	 * @param childCustomChargeTO
	 */
	public void setChildCustomChargeTO(PnrChargeDetailTO childCustomChargeTO) {
		this.childCustomChargeTO = childCustomChargeTO;
	}

	/**
	 * @return
	 */
	public PnrChargeDetailTO getInfantCustomChargeTO() {
		return infantCustomChargeTO;
	}

	/**
	 * @param infantCustomChargeTO
	 */
	public void setInfantCustomChargeTO(PnrChargeDetailTO infantCustomChargeTO) {
		this.infantCustomChargeTO = infantCustomChargeTO;
	}

	/**
	 * @return the isSendingAbosoluteCharge
	 */
	public boolean isSendingAbosoluteCharge() {
		return isSendingAbosoluteCharge;
	}

	/**
	 * @param isSendingAbosoluteCharge
	 *            the isSendingAbosoluteCharge to set
	 */
	public void setSendingAbosoluteCharge(boolean isSendingAbosoluteCharge) {
		this.isSendingAbosoluteCharge = isSendingAbosoluteCharge;
	}

	/**
	 * @return
	 */
	public String getRouteType() {
		return routeType;
	}

	/**
	 * @param routeType
	 */
	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	/**
	 * @return
	 */
	public Collection<LCCClientReservationSegment> getExistingAllSegs() {
		return existingAllSegs;
	}

	/**
	 * @param existingAllSegs
	 */
	public void setExistingAllSegs(Collection<LCCClientReservationSegment> existingAllSegs) {
		this.existingAllSegs = existingAllSegs;
	}

	/**
	 * @return
	 */
	public String getReservationStatus() {
		return reservationStatus;
	}

	/**
	 * @param reservationStatus
	 */
	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	/**
	 * @return
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public void setOndWiseAdultCharge(Map<String, BigDecimal> ondWiseAdultCharge) {
		this.ondWiseAdultCharge = ondWiseAdultCharge;
	}

	public Map<String, BigDecimal> getOndWiseAdultCharge() {
		return ondWiseAdultCharge;
	}

	public void setOndWiseChildCharge(Map<String, BigDecimal> ondWiseChildCharge) {
		this.ondWiseChildCharge = ondWiseChildCharge;
	}

	public Map<String, BigDecimal> getOndWiseChildCharge() {
		return ondWiseChildCharge;
	}

	public void setOndWiseInfantCharge(Map<String, BigDecimal> ondWiseInfantCharge) {
		this.ondWiseInfantCharge = ondWiseInfantCharge;
	}

	public Map<String, BigDecimal> getOndWiseInfantCharge() {
		return ondWiseInfantCharge;
	}

	/**
	 * @return the anciOfferTemplates
	 */
	public Map<String, Map<EXTERNAL_CHARGES, Integer>> getAnciOfferTemplates() {
		if (anciOfferTemplates == null) {
			anciOfferTemplates = new HashMap<String, Map<EXTERNAL_CHARGES, Integer>>();
		}
		return anciOfferTemplates;
	}

	/**
	 * @param anciOfferTemplates
	 *            the anciOfferTemplates to set
	 */
	public void setAnciOfferTemplates(Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates) {
		this.anciOfferTemplates = anciOfferTemplates;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public BigDecimal getDefaultCustomAdultCharge() {
		return defaultCustomAdultCharge;
	}

	public void setDefaultCustomAdultCharge(BigDecimal defaultCustomAdultCharge) {
		this.defaultCustomAdultCharge = defaultCustomAdultCharge;
	}

	public BigDecimal getDefaultCustomChildCharge() {
		return defaultCustomChildCharge;
	}

	public void setDefaultCustomChildCharge(BigDecimal defaultCustomChildCharge) {
		this.defaultCustomChildCharge = defaultCustomChildCharge;
	}

	public BigDecimal getDefaultCustomInfantCharge() {
		return defaultCustomInfantCharge;
	}

	public void setDefaultCustomInfantCharge(BigDecimal defaultCustomInfantCharge) {
		this.defaultCustomInfantCharge = defaultCustomInfantCharge;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	public Map<Integer, CnxModPenServiceTaxDTO> getPaxWiseCnxModPenServiceTaxes() {
		return paxWiseCnxModPenServiceTaxes;
	}

	public void setPaxWiseCnxModPenServiceTaxes(Map<Integer, CnxModPenServiceTaxDTO> paxWiseCnxModPenServiceTaxes) {
		this.paxWiseCnxModPenServiceTaxes = paxWiseCnxModPenServiceTaxes;
	}
}
