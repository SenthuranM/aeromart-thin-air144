/**
 * 
 */
package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * @author Indika Athauda
 * 
 */
public class CommonUtil {

	/**
	 * 1. Iterates through LCCClientReservationSegment and if ground station is
	 * present it adds the data to a map with key = segmentCode | value =
	 * groundStation 2. //Set Ground Flight Ref ID to Air Segment //Steps - 1.
	 * Identify GroundSegments And AirSegments | 2. Identify the correct
	 * AirSegment for GroundSegment //3. Add the segment ID Assumption is
	 * segment can only have one ground station (either from/to) ONLY
	 * 
	 * @param segments
	 * @param segmentMap
	 *            the map that holds groundstations. Can be null
	 * @return
	 * @throws ModuleException
	 * @throws NumberFormatException
	 */
	public static void mapStationsBySegment(Collection<LCCClientReservationSegment> segments,
			Map<Integer, String> segmentMap, Map<Integer, Integer> groundSegmentByAirFlightSegmentID)
			throws NumberFormatException, ModuleException {
		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return;
		}

		if (segmentMap == null) {
			segmentMap = new LinkedHashMap<Integer, String>();
		}
		if (groundSegmentByAirFlightSegmentID == null) {
			groundSegmentByAirFlightSegmentID = new LinkedHashMap<Integer, Integer>();
		}

		List<LCCClientReservationSegment> filteredGroundSegments = new ArrayList<LCCClientReservationSegment>();
		List<LCCClientReservationSegment> filteredAirSegments = new ArrayList<LCCClientReservationSegment>();

		for (LCCClientReservationSegment segment : segments) {
			Collection<String> airports = Arrays.asList(segment.getSegmentCode().split("/"));
			if (ReservationModuleUtils.getAirportBD().isContainGroundSegment(airports)) {
				filteredGroundSegments.add(segment);
			} else {
				filteredAirSegments.add(segment);
			}
		}

		if (!filteredGroundSegments.isEmpty() && !filteredAirSegments.isEmpty()) {
			Collections.sort(filteredAirSegments);
			Collections.sort(filteredGroundSegments);
			for (LCCClientReservationSegment airSegment : filteredAirSegments) {
				Iterator<LCCClientReservationSegment> groundSegIte = filteredGroundSegments.iterator();
				if (groundSegIte.hasNext()) {
					LCCClientReservationSegment groundSegment = groundSegIte.next();

					String groundStation = null;
					if (groundSegment.getSubStationShortName() != null) {
						groundStation = groundSegment.getSubStationShortName();
					} else {
						groundStation = returnGroundSegment(AirproxyModuleUtils.getAirportBD()
								.getCachedOwnAirportMap(getAirportCollection(groundSegment)));
					}
					segmentMap.put(Integer.parseInt(groundSegment.getFlightSegmentRefNumber()), groundStation);

					String[] airSegmentAirports = airSegment.getSegmentCode().split("/");
					String airSegmentDeparture = airSegmentAirports[0];
					String airSegmentArrival = airSegmentAirports[airSegmentAirports.length - 1];

					String[] groundSegmentAirports = groundSegment.getSegmentCode().split("/");
					String groundSegmentDeparture = groundSegmentAirports[0];
					String groundSegmentArrival = groundSegmentAirports[groundSegmentAirports.length - 1];
					;

					if ((airSegment.getDepartureDateZulu().after(groundSegment.getArrivalDateZulu())
							&& airSegmentDeparture.equals(groundSegmentArrival))
							|| (groundSegment.getDepartureDateZulu().after(airSegment.getArrivalDateZulu())
									&& groundSegmentDeparture.equals(airSegmentArrival))) {
						groundSegmentByAirFlightSegmentID.put(
								Integer.parseInt(groundSegment.getFlightSegmentRefNumber()),
								Integer.parseInt(airSegment.getFlightSegmentRefNumber()));
						groundSegIte.remove();
					}
				}
			}
		}
	}

	private static Collection getAirportCollection(LCCClientReservationSegment clientReservationSegment) {
		Collection airports = Arrays.asList(clientReservationSegment.getSegmentCode().split("/"));
		return airports;
	}

	public static boolean isContainGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Assumption only one ground station will return.
	 * 
	 * @param airportCodes
	 * @return
	 */
	public static String returnGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return airport.getAirportCode();
			}
		}
		return null;
	}

	public static TrackInfoDTO getTrackInfoDto(UserPrincipal userPrincipal) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		if (userPrincipal != null) {
			trackInfoDTO.setCarrierCode(userPrincipal.getDefaultCarrierCode());
			trackInfoDTO.setIpAddress(userPrincipal.getIpAddress());
		}
		return trackInfoDTO;
	}

	public static ApplicationEngine getAppIndicator(AppIndicatorEnum appIndicator) {
		if (AppIndicatorEnum.APP_IBE.equals(appIndicator) || AppIndicatorEnum.APP_KIOSK.equals(appIndicator)) {
			return ApplicationEngine.IBE;
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicator) || AppIndicatorEnum.APP_MYID.equals(appIndicator)) {
			return ApplicationEngine.WS;
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicator)) {
			return ApplicationEngine.XBE;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicator)) {
			return ApplicationEngine.IOS;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicator)) {
			return ApplicationEngine.ANDROID;
		}
		return null;
	}

	public static String getAppIndicator(String appEngine) {
		String appIndicator;

		if (ApplicationEngine.IBE.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_IBE.toString();
		} else if (ApplicationEngine.IOS.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_IOS.toString();
		} else if (ApplicationEngine.ANDROID.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_ANDROID.toString();
		} else if (ApplicationEngine.XBE.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_XBE.toString();
		} else {
			appIndicator = AppIndicatorEnum.APP_WS.toString();
		}

		return appIndicator;
	}

	public static boolean isAtleastOneBundledFareSelected(List<BundledFareDTO> bundledFareDTOs) {
		if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				if (bundledFareDTO != null) {
					return true;
				}
			}
		}

		return false;
	}

	public static SYSTEM deriveSystem(List<FlightSegmentTO> flightSegments) {
		SYSTEM selectedSystem = SYSTEM.AA;
		Set<String> ownCarriers = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();

		for (FlightSegmentTO flightSegment : flightSegments) {
			if (!ownCarriers.contains(flightSegment.getOperatingAirline())) {
				selectedSystem = SYSTEM.INT;
				break;
			}
		}
		return selectedSystem;
	}
	
	public static int getChannalCodeFromAppIndicator(AppIndicatorEnum appIndicator) {
		if (AppIndicatorEnum.APP_IBE.equals(appIndicator) || AppIndicatorEnum.APP_KIOSK.equals(appIndicator)) {
			return SalesChannelsUtil.SALES_CHANNEL_WEB;
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicator) || AppIndicatorEnum.APP_MYID.equals(appIndicator)) {
			return SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES;
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicator)) {
			return SalesChannelsUtil.SALES_CHANNEL_AGENT;
		} else if (AppIndicatorEnum.APP_IOS.equals(appIndicator)) {
			return SalesChannelsUtil.SALES_CHANNEL_IOS;
		} else if (AppIndicatorEnum.APP_ANDROID.equals(appIndicator)) {
			return SalesChannelsUtil.SALES_CHANNEL_ANDROID;
		}
		return -1;
	}

}
