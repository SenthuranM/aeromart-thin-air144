package com.isa.thinair.airproxy.api.service;

import com.isa.thinair.airproxy.api.dto.VoucherRedeemResponse;
import com.isa.thinair.commons.api.dto.VoucherRedeemRequest;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chethiya
 *
 */
public interface AirproxyVoucherBD {
	public static final String SERVICE_NAME = "AirproxyVoucherService";
	
	/**
	 * @param voucherRedeemRequest
	 * @return
	 * @throws ModuleException
	 * @throws Exception
	 */
	public VoucherRedeemResponse getVoucherRedeemResponse(VoucherRedeemRequest voucherRedeemRequest) throws Exception;

}
