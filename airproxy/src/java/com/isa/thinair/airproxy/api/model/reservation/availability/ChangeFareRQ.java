package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO.FareType;

public class ChangeFareRQ extends BaseAvailRQ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String posAirport;
	private List<SegmentsFareDTO> changedFareSegments;
	private FareType selectedFareType;
	private OndRebuildCriteria ondRebuildCriteria;
	private Map<String, List<String>> carrierWiseOnd;
	private String pnr;

	public ChangeFareRQ() {
	}

	public ChangeFareRQ(BaseAvailRQ baseAvail) {
		super(baseAvail);
	}

	public void setPosAirport(String posAirport) {
		this.posAirport = posAirport;
	}

	public String getPosAirport() {
		return posAirport;
	}

	public void setChangedFareSegments(List<SegmentsFareDTO> changedFareSegments) {
		this.changedFareSegments = changedFareSegments;
	}

	public List<SegmentsFareDTO> getChangedFareSegments() {
		if (changedFareSegments == null) {
			this.changedFareSegments = new ArrayList<SegmentsFareDTO>();
		}
		return changedFareSegments;
	}

	public FareType getSelectedFareType() {
		return selectedFareType;
	}

	public void setSelectedFareType(FareType selectedFareType) {
		this.selectedFareType = selectedFareType;
	}

	public void setOndRebuildCriteria(OndRebuildCriteria ondRebuildCriteria) {
		this.ondRebuildCriteria = ondRebuildCriteria;
	}

	public OndRebuildCriteria getOndRebuildCriteria() {
		return ondRebuildCriteria;
	}

	public Map<String, List<String>> getCarrierWiseOnd() {
		return carrierWiseOnd;
	}

	public void setCarrierWiseOnd(Map<String, List<String>> carrierWiseOnd) {
		this.carrierWiseOnd = carrierWiseOnd;
	}

	public List<Integer> getExistingFlightSegIds() {
		List<Integer> list = new ArrayList<Integer>();
		for (OriginDestinationInformationTO ondInfoTO : getOriginDestinationInformationList()) {
			list.addAll(ondInfoTO.getExistingFlightSegIds());
		}
		return list;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
