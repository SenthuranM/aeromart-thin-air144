package com.isa.thinair.airproxy.api.service;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public interface AirProxyFlightServiceBD {
	public static final String SERVICE_NAME = "AirproxyReservationService";

	public Page searchFlightsForDisplay(FlightManifestSearchDTO flightManifestSearchDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public String viewFlightManifest(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws Exception;

	public boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO flightManifestOptionsDTO, BasicTrackInfo trackInfo)
			throws Exception;

	public List<FlightLoadReportDataDTO> getFlightLoadReportData(ReportsSearchCriteria search, BasicTrackInfo trackInfo)
			throws ModuleException;
}
