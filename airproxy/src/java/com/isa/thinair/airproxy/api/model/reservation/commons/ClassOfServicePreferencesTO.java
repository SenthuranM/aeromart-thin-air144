package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class ClassOfServicePreferencesTO implements Serializable {
	private static final long serialVersionUID = -7976344558087471019L;

	private Map<String, List<Integer>> cabinClassSelection;
	private Map<String, List<Integer>> logicalCabinClassSelection;

	/**
	 * @return the cabinClassSelection
	 */
	public Map<String, List<Integer>> getCabinClassSelection() {
		if (this.cabinClassSelection == null) {
			this.cabinClassSelection = new HashMap<String, List<Integer>>();
		}
		return cabinClassSelection;
	}

	/**
	 * @param cabinClassSelection
	 *            the cabinClassSelection to set
	 */
	public void setCabinClassSelection(Map<String, List<Integer>> cabinClassSelection) {
		this.cabinClassSelection = cabinClassSelection;
	}

	/**
	 * @return the logicalCabinClassSelection
	 */
	public Map<String, List<Integer>> getLogicalCabinClassSelection() {
		if (this.logicalCabinClassSelection == null) {
			this.logicalCabinClassSelection = new HashMap<String, List<Integer>>();
		}
		return logicalCabinClassSelection;
	}

	/**
	 * @param logicalCabinClassSelection
	 *            the logicalCabinClassSelection to set
	 */
	public void setLogicalCabinClassSelection(Map<String, List<Integer>> logicalCabinClassSelection) {
		this.logicalCabinClassSelection = logicalCabinClassSelection;
	}

}
