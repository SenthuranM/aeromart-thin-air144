/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO.status;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * LCCClientPaymentHolder holds all the payments, which were assembled via LCCClientPaymentAssembler when initially
 * making the payment
 * 
 * @author Nilindra Fernando
 * @since 3:27 PM 1/15/2010
 */
public class LCCClientPaymentHolder implements Serializable {

	private static final long serialVersionUID = -483766127596801119L;

	/** Holds types of PaymentInfo Objects */
	private final Collection<LCCClientPaymentInfo> payments;

	private Collection<CreditInfoDTO> credits;

	/** Holds the total payment amount */
	private BigDecimal totalPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * Construct Reservation Object
	 */
	public LCCClientPaymentHolder() {
		this.payments = new ArrayList<LCCClientPaymentInfo>();
		this.credits = new ArrayList<CreditInfoDTO>();
	}

	/**
	 * Add agent credit information
	 * 
	 * @param carrierCode
	 * @param agentCode
	 * @param amount
	 * @param paidAirlineCurrencyCode
	 * @param txnDateTime
	 * @param agentName
	 * @param payCurrAmount
	 * @param payCurrencyCode
	 * @param payRef
	 * @param originalPayRef
	 * @param actualPaymentMethod
	 * @param recieptNum
	 * @param carrierVisePayments
	 * @param lccUniqueTnxId
	 * @param paymentMethod
	 * @param paymentTnxReference
	 */
	public void addAgentCreditPayment(String carrierCode, String agentCode, BigDecimal amount, String paidAirlineCurrencyCode,
			Date txnDateTime, String agentName, BigDecimal payCurrAmount, String payCurrencyCode, String payRef,
			String originalPayRef, Integer actualPaymentMethod, String recieptNum, String carrierVisePayments,
			String lccUniqueTnxId, String paymentMethod, String remarks, String paymentTnxReference) {
		LCCClientOnAccountPaymentInfo lccOnAccountPaymentInfo = new LCCClientOnAccountPaymentInfo();
		lccOnAccountPaymentInfo.setCarrierCode(carrierCode);
		lccOnAccountPaymentInfo.setAgentCode(agentCode);
		lccOnAccountPaymentInfo.setTotalAmount(amount);
		lccOnAccountPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrencyCode);
		lccOnAccountPaymentInfo.setTxnDateTime(txnDateTime);
		lccOnAccountPaymentInfo.setAgentName(agentName);
		lccOnAccountPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccOnAccountPaymentInfo.setPayReference(payRef);
		lccOnAccountPaymentInfo.setOriginalPayReference(originalPayRef);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode, null);
		lccOnAccountPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccOnAccountPaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		lccOnAccountPaymentInfo.setRecieptNumber(recieptNum);
		lccOnAccountPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccOnAccountPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccOnAccountPaymentInfo.setPaymentMethod(paymentMethod);
		lccOnAccountPaymentInfo.setRemarks(remarks);
		lccOnAccountPaymentInfo.setPaymentTnxReference(paymentTnxReference);

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);
		this.payments.add(lccOnAccountPaymentInfo);
	}

	/**
	 * Add Cash Payment
	 * 
	 * @param carrierCode
	 * @param amount
	 * @param paidAirlineCurrCode
	 * @param txnDateTime
	 * @param payCurrAmount
	 * @param payCurrCode
	 * @param payRef
	 * @param originalPayRef
	 * @param actualPaymentMethod
	 * @param recieptNumber
	 * @param carrierVisePayments
	 * @param lccUniqueTnxId
	 * @param actualPayment
	 * @param paymentTnxReference
	 *            TODO
	 */
	public void addCashPayment(String carrierCode, String agentCode, BigDecimal amount, String paidAirlineCurrCode,
			Date txnDateTime, String agentName, BigDecimal payCurrAmount, String payCurrCode, String payRef,
			String originalPayRef, Integer actualPaymentMethod, String recieptNumber, String carrierVisePayments,
			String lccUniqueTnxId, String paymentMethod, String remarks, String dummyPayment, String paymentTnxReference) {
		LCCClientCashPaymentInfo lccCashPaymentInfo = new LCCClientCashPaymentInfo();
		lccCashPaymentInfo.setCarrierCode(carrierCode);
		lccCashPaymentInfo.setAgentCode(agentCode);
		lccCashPaymentInfo.setAgentName(agentName);
		lccCashPaymentInfo.setTotalAmount(amount);
		lccCashPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrCode);
		lccCashPaymentInfo.setTxnDateTime(txnDateTime);
		lccCashPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccCashPaymentInfo.setPayReference(payRef);
		lccCashPaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		lccCashPaymentInfo.setOriginalPayReference(originalPayRef);
		lccCashPaymentInfo.setRecieptNumber(recieptNumber);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrCode, null);
		lccCashPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccCashPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccCashPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccCashPaymentInfo.setRemarks(remarks);
		lccCashPaymentInfo.setPaymentMethod(paymentMethod);
		lccCashPaymentInfo.setDummyPayment(dummyPayment);
		lccCashPaymentInfo.setPaymentTnxReference(paymentTnxReference);

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);
		this.payments.add(lccCashPaymentInfo);
	}

	/**
	 * Add PaxCredit Payment
	 * 
	 * @param carrierCode
	 * @param debitPaxRefNo
	 * @param description
	 * @param amount
	 * @param paidAirlineCurrencyCode
	 * @param txnDateTime
	 * @param payCurrAmount
	 * @param payCurrencyCode
	 * @param payRef
	 * @param originalPayRef
	 * @param recieptNumber
	 * @param carrierVisePayments
	 * @param lccUniqueTnxId
	 * @param paymentTnxReference
	 */
	public void addPaxCreditPayment(String carrierCode, Integer debitPaxRefNo, String description, BigDecimal amount,
			String paidAirlineCurrencyCode, Date txnDateTime, BigDecimal payCurrAmount, String payCurrencyCode,
			String payRef, String originalPayRef, String recieptNumber, String carrierVisePayments,
			String lccUniqueTnxId, String remarks, String paymentTnxReference) {
		LCCClientPaxCreditPaymentInfo lccPaxCreditPaymentInfo = new LCCClientPaxCreditPaymentInfo();
		lccPaxCreditPaymentInfo.setCarrierCode(carrierCode);
		lccPaxCreditPaymentInfo.setDebitPaxRefNo(debitPaxRefNo);
		lccPaxCreditPaymentInfo.setDescription(description);
		lccPaxCreditPaymentInfo.setTotalAmount(amount);
		lccPaxCreditPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrencyCode);
		lccPaxCreditPaymentInfo.setTxnDateTime(txnDateTime);
		lccPaxCreditPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccPaxCreditPaymentInfo.setPayReference(payRef);
		lccPaxCreditPaymentInfo.setOriginalPayReference(originalPayRef);
		lccPaxCreditPaymentInfo.setRecieptNumber(recieptNumber);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode, null);
		lccPaxCreditPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccPaxCreditPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccPaxCreditPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccPaxCreditPaymentInfo.setRemarks(remarks);
		lccPaxCreditPaymentInfo.setPaymentTnxReference(paymentTnxReference);
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);

		this.payments.add(lccPaxCreditPaymentInfo);
	}

	/**
	 * Add card payment information
	 * 
	 * @param cardType
	 * @param eDate
	 * @param cardNumber
	 *            Could be full card number or only last 4 digits
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param payRef
	 * @param originalPayRef
	 * @param actualPaymentMethod
	 * @param carrierVisePayments
	 * @param lccUniqueTnxId
	 *            TODO
	 * @param paymentBrokerRefNo
	 * @param paymentTnxReference
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param oldCCRecordId
	 * @param ipgIdentificationParamsDTO
	 */
	public void addCardPayment(String carrierCode, int cardType, String eDate, String cardNumber, String name, String address,
			String securityCode, BigDecimal amount, String paidAirlineCurrencyCode, Date txnDateTime, String authorizationId,
			BigDecimal payCurrAmount, String cardHoldersName, String payCurrencyCode, String payRef, String originalPayRef,
			Integer actualPaymentMethod, String carrierVisePayments, String lccUniqueTnxId, String remarks, Integer paymentBrokerRefNo, String paymentTnxReference) {

		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();
		lccClientCreditCardPaymentInfo.setCarrierCode(carrierCode);
		lccClientCreditCardPaymentInfo.setType(cardType);
		lccClientCreditCardPaymentInfo.seteDate(eDate);
		lccClientCreditCardPaymentInfo.setNo(cardNumber);
		lccClientCreditCardPaymentInfo.setName(name);
		lccClientCreditCardPaymentInfo.setAddress(address);
		lccClientCreditCardPaymentInfo.setSecurityCode(securityCode);
		lccClientCreditCardPaymentInfo.setTotalAmount(amount);
		lccClientCreditCardPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrencyCode);
		lccClientCreditCardPaymentInfo.setTxnDateTime(txnDateTime);
		lccClientCreditCardPaymentInfo.setAuthorizationId(authorizationId);
		lccClientCreditCardPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccClientCreditCardPaymentInfo.setCardHoldersName(cardHoldersName);
		lccClientCreditCardPaymentInfo.setPayReference(payRef);
		lccClientCreditCardPaymentInfo.setActualPaymentMethod(actualPaymentMethod);
		lccClientCreditCardPaymentInfo.setOriginalPayReference(originalPayRef);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode, null);
		lccClientCreditCardPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccClientCreditCardPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccClientCreditCardPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccClientCreditCardPaymentInfo.setRemarks(remarks);
		lccClientCreditCardPaymentInfo.setPaymentBrokerRefNo(paymentBrokerRefNo);
		lccClientCreditCardPaymentInfo.setPaymentTnxReference(paymentTnxReference);
		

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);

		this.payments.add(lccClientCreditCardPaymentInfo);
	}

	public void addLMSPayment(String carrierCode, String loyaltyMemberAccountId, String description, BigDecimal amount,
			String paidAirlineCurrencyCode, Date txnDateTime, BigDecimal payCurrAmount, String payCurrencyCode, String payRef,
			String originalPayRef, String recieptNumber, String carrierVisePayments, String lccUniqueTnxId, String remarks,
			String paymentTnxReference, String paymentMethod, boolean isNonRefundable) {
		LCCClientLMSPaymentInfo lccClientLMSPaymentInfo = new LCCClientLMSPaymentInfo();
		lccClientLMSPaymentInfo.setCarrierCode(carrierCode);
		lccClientLMSPaymentInfo.setLoyaltyMemberAccountId(loyaltyMemberAccountId);
		lccClientLMSPaymentInfo.setTotalAmount(amount);
		lccClientLMSPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrencyCode);
		lccClientLMSPaymentInfo.setTxnDateTime(txnDateTime);
		lccClientLMSPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccClientLMSPaymentInfo.setPayReference(payRef);
		lccClientLMSPaymentInfo.setOriginalPayReference(originalPayRef);
		lccClientLMSPaymentInfo.setRecieptNumber(recieptNumber);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode, null);
		lccClientLMSPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccClientLMSPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccClientLMSPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccClientLMSPaymentInfo.setRemarks(remarks);
		lccClientLMSPaymentInfo.setPaymentTnxReference(paymentTnxReference);
		lccClientLMSPaymentInfo.setPaymentMethod(paymentMethod);
		lccClientLMSPaymentInfo.setNonRefundable(isNonRefundable);
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);

		this.payments.add(lccClientLMSPaymentInfo);
	}

	public void addVoucherPayment(String carrierCode, VoucherDTO voucherDTO, String description, BigDecimal amount,
			String paidAirlineCurrencyCode, Date txnDateTime, BigDecimal payCurrAmount, String payCurrencyCode, String payRef,
			String originalPayRef, String recieptNumber, String carrierVisePayments, String lccUniqueTnxId, String remarks,
			String paymentTnxReference) {
		LCCClientVoucherPaymentInfo lccClientVoucherPaymentInfo = new LCCClientVoucherPaymentInfo();
		lccClientVoucherPaymentInfo.setCarrierCode(carrierCode);
		lccClientVoucherPaymentInfo.setVoucherDTO(voucherDTO);
		lccClientVoucherPaymentInfo.setTotalAmount(amount);
		lccClientVoucherPaymentInfo.setTotalAmountCurrencyCode(paidAirlineCurrencyCode);
		lccClientVoucherPaymentInfo.setTxnDateTime(txnDateTime);
		lccClientVoucherPaymentInfo.setPayCurrencyAmount(payCurrAmount);
		lccClientVoucherPaymentInfo.setPayReference(payRef);
		lccClientVoucherPaymentInfo.setOriginalPayReference(originalPayRef);
		lccClientVoucherPaymentInfo.setRecieptNumber(recieptNumber);
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode, null);
		lccClientVoucherPaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		lccClientVoucherPaymentInfo.setCarrierVisePayments(carrierVisePayments);
		lccClientVoucherPaymentInfo.setLccUniqueTnxId(lccUniqueTnxId);
		lccClientVoucherPaymentInfo.setRemarks(remarks);
		lccClientVoucherPaymentInfo.setPaymentTnxReference(paymentTnxReference);
		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);
		
		this.payments.add(lccClientVoucherPaymentInfo);

	}

	public void addOfflineCardPayment(BigDecimal amount, BigDecimal amountInPayCurrency, String currencyCode,
			String payReference, String remarks, String description, Date paymentDate) {

		CommonOfflinePaymentInfo offlinePaymentInfo = new CommonOfflinePaymentInfo();
		offlinePaymentInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		offlinePaymentInfo.setPayReference(payReference);
		offlinePaymentInfo.setPaymentTnxReference(payReference);
		offlinePaymentInfo.setRemarks(remarks);
		offlinePaymentInfo.setTotalAmount(amount);
		offlinePaymentInfo.setTotalAmountCurrencyCode(currencyCode);
		offlinePaymentInfo.setPayCurrencyAmount(amountInPayCurrency);
		offlinePaymentInfo.setDescription("offline");
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currencyCode, new BigDecimal(1));
		offlinePaymentInfo.setPayCurrencyDTO(payCurrencyDTO);
		offlinePaymentInfo.setPaymentTnxDateTime(paymentDate);

		totalPayAmount = AccelAeroCalculator.add(totalPayAmount, amount);
		this.payments.add(offlinePaymentInfo);
	}

	/**
	 * Return Payments
	 * 
	 * @return
	 */
	public Collection<LCCClientPaymentInfo> getPayments() {
		return this.payments;
	}

	/**
	 * @return Returns the totalPayAmount.
	 */
	public BigDecimal getTotalPayAmount() {
		return totalPayAmount;
	}

	public Collection<CreditInfoDTO> getCredits() {
		if (this.credits == null) {
			this.credits = new ArrayList<CreditInfoDTO>();
		}
		return credits;
	}

	public void addCredit(BigDecimal amount, BigDecimal mcAmount, Integer creditId, status enumStatus, Date expireDate,
			Integer txnId, String userNote, String carrierCode, String currencyCode, String lccUniqueTnxId, boolean isNonRefundableCredit) {
		CreditInfoDTO creditInfoDTO = new CreditInfoDTO();
		creditInfoDTO.setAmount(amount);
		creditInfoDTO.setCreditId(creditId);
		creditInfoDTO.setMcAmount(mcAmount);
		creditInfoDTO.setEnumStatus(enumStatus);
		creditInfoDTO.setExpireDate(expireDate);
		creditInfoDTO.setTxnId(txnId);
		creditInfoDTO.setUserNote(userNote);
		creditInfoDTO.setCarrierCode(carrierCode);
		creditInfoDTO.setCurrencyCode(currencyCode);
		creditInfoDTO.setLccUniqueTnxId(lccUniqueTnxId);
		creditInfoDTO.setNonRefundableCredit(isNonRefundableCredit);
		this.credits.add(creditInfoDTO);
	}

	public void setCredits(Collection<CreditInfoDTO> credits) {
		this.credits = credits;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientPaymentHolder [payments=");
		builder.append(payments);
		builder.append(", totalPayAmount=");
		builder.append(totalPayAmount);
		builder.append(", credits=");
		builder.append(credits);
		builder.append("]");
		return builder.toString();
	}
}