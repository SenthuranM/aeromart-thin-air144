/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of reservation passenger information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientReservationPax implements Serializable, Comparable<LCCClientReservationPax> {

	private static final long serialVersionUID = -7886410773706072536L;

	// Reservation related
	/** Holds the reservation */
	private LCCClientReservation lccReservation;

	private String travelerRefNumber;
	/** Holds reservation passenger type */
	private String paxType;
	/** Holds reservation passenger sequence */
	private Integer paxSequence;
	/** Holds reservation zulu passenger initialize time */
	private Date zuluStartTimeStamp;
	/** Holds reservation zulu passenger release time */
	private Date zuluReleaseTimeStamp;
	/** Holds reservation passenger status */
	private String status;

	// Contact related
	private Integer attachedPaxId;
	/** Holds reservation passenger title */
	private String title;
	/** Holds reservation passenger first name */
	private String firstName;
	/** Holds reservation passenger last name */
	private String lastName;
	/** Holds reservation passenger nationality code */
	private Integer nationalityCode;
	/** Holds reservation passenger date of birth */
	private Date dateOfBirth;

	/** Holds passenger category */
	private String paxCategory;

	private LCCClientReservationAdditionalPax lccClientAdditionPax;

	private LCCClientReservationPaxNamesTranslation lccClientPaxNameTranslations;

	// Charges related
	/** Holds reservation passenger total fare */
	private BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total sur-charge */
	private BigDecimal totalSurCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total tax */
	private BigDecimal totalTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total cancel charge */
	private BigDecimal totalCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total modification charge */
	private BigDecimal totalModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total adjustment charge */
	private BigDecimal totalAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger available credit or debit amount */
	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger price */
	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger fare discount */
	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger refundable credit amount */
	private BigDecimal totalActualCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

	// Used temporary when the reservation is created. Please use LCCClientPaymentHolder for all payment details
	private LCCClientPaymentAssembler lccClientPaymentAssembler;

	/** Holds the Parent */
	private LCCClientReservationPax parent;

	/** Holds a set of infants */
	private Set<LCCClientReservationPax> infants;

	/** Holds passenger payment information */
	private LCCClientPaymentHolder lccClientPaymentHolder;

	/** Holds fare/ charges information */
	private List<TaxTO> taxes;
	private List<FeeTO> fees;
	private List<SurchargeTO> surcharges;
	private List<FareTO> fares;

	/** Holds the selected ancillary details */
	private List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries;

	/** Holds a set of E ticket info */
	private Collection<LccClientPassengerEticketInfoTO> eTickets;

	private String nationality;

	private boolean alertAutoCancellation;

	private boolean blacklisted;
	
	private BigDecimal noShowRefundableAmount;

	private boolean isNoShowRefundable;

	/**
	 * Reset total charges
	 */
	public void resetTotalChargeAmounts() {
		this.setTotalChargeAmounts(ReservationApiUtils.getChargeTypeAmounts());
	}

	/**
	 * Set the total charges amounts
	 * 
	 * @param totalCharges
	 */
	public void setTotalChargeAmounts(BigDecimal[] totalChargeAmounts) {
		this.setTotalFare(totalChargeAmounts[0]);
		this.setTotalTaxCharge(totalChargeAmounts[1]);
		this.setTotalSurCharge(totalChargeAmounts[2]);
		this.setTotalCancelCharge(totalChargeAmounts[3]);
		this.setTotalModificationCharge(totalChargeAmounts[4]);
		this.setTotalAdjustmentCharge(totalChargeAmounts[5]);
		this.setTotalDiscount(totalChargeAmounts[7]);
	}

	/**
	 * Return the total charges amounts ==> [0]fare [1]tax [2]surcharge [3]cancelcharge [4]modificationcharge
	 * [5]adjustmentcharge [6]discount
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalChargeAmounts() {
		return new BigDecimal[] { this.getTotalFare(), this.getTotalTaxCharge(), this.getTotalSurCharge(),
				this.getTotalCancelCharge(), this.getTotalModificationCharge(), this.getTotalAdjustmentCharge(),
				this.getTotalDiscount().negate() };
	}

	/**
	 * Returns the total charges (Parent + Infant) charges
	 * 
	 * @return
	 */
	public BigDecimal getTotalChargeAmount() {
		// If parent exist
		if (ReservationInternalConstants.PassengerType.ADULT.equals(this.getPaxType()) && this.getParent() != null) {
			BigDecimal[] parentAndInfantChgs = ReservationApiUtils.getChargeTypeAmounts();

			for (LCCClientReservationPax infant : this.getInfants()) {
				parentAndInfantChgs = ReservationApiUtils.getUpdatedTotals(infant.getTotalChargeAmounts(),
						this.getTotalChargeAmounts(), true);
			}

			return AccelAeroCalculator.add(parentAndInfantChgs);
		}
		// Adult / Child or an Infant
		else {
			return AccelAeroCalculator.add(getTotalFare(), getTotalTaxCharge(), getTotalSurCharge(), getTotalCancelCharge(),
					getTotalModificationCharge(), getTotalAdjustmentCharge(), getTotalDiscount().negate());
		}
	}

	/* ********************************************* */
	/* 												 */
	/* GETTERS AND SETTERS */
	/* 												 */
	/* ********************************************* */

	/**
	 * @return the totalFare
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return the totalSurCharge
	 */
	public BigDecimal getTotalSurCharge() {
		return totalSurCharge;
	}

	/**
	 * @param totalSurCharge
	 *            the totalSurCharge to set
	 */
	public void setTotalSurCharge(BigDecimal totalSurCharge) {
		this.totalSurCharge = totalSurCharge;
	}

	/**
	 * @return the totalTaxCharge
	 */
	public BigDecimal getTotalTaxCharge() {
		return totalTaxCharge;
	}

	/**
	 * @param totalTaxCharge
	 *            the totalTaxCharge to set
	 */
	public void setTotalTaxCharge(BigDecimal totalTaxCharge) {
		this.totalTaxCharge = totalTaxCharge;
	}

	/**
	 * @return the totalCancelCharge
	 */
	public BigDecimal getTotalCancelCharge() {
		return totalCancelCharge;
	}

	/**
	 * @param totalCancelCharge
	 *            the totalCancelCharge to set
	 */
	public void setTotalCancelCharge(BigDecimal totalCancelCharge) {
		this.totalCancelCharge = totalCancelCharge;
	}

	/**
	 * @return the totalModificationCharge
	 */
	public BigDecimal getTotalModificationCharge() {
		return totalModificationCharge;
	}

	/**
	 * @param totalModificationCharge
	 *            the totalModificationCharge to set
	 */
	public void setTotalModificationCharge(BigDecimal totalModificationCharge) {
		this.totalModificationCharge = totalModificationCharge;
	}

	/**
	 * @return the totalAdjustmentCharge
	 */
	public BigDecimal getTotalAdjustmentCharge() {
		return totalAdjustmentCharge;
	}

	/**
	 * @param totalAdjustmentCharge
	 *            the totalAdjustmentCharge to set
	 */
	public void setTotalAdjustmentCharge(BigDecimal totalAdjustmentCharge) {
		this.totalAdjustmentCharge = totalAdjustmentCharge;
	}

	/**
	 * @return the totalPaidAmount
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return the totalAvailableBalance
	 */
	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	/**
	 * @param totalAvailableBalance
	 *            the totalAvailableBalance to set
	 */
	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	/**
	 * @return the lccReservation
	 */
	@JSON(serialize = false)
	public LCCClientReservation getLccReservation() {
		return lccReservation;
	}

	/**
	 * @param lccReservation
	 *            the lccReservation to set
	 */
	public void setLccReservation(LCCClientReservation lccReservation) {
		this.lccReservation = lccReservation;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return the lccClientAdditionPax
	 */
	public LCCClientReservationAdditionalPax getLccClientAdditionPax() {
		return lccClientAdditionPax;
	}

	/**
	 * @param lccClientAdditionPax
	 *            the lccClientAdditionPax to set
	 */
	public void setLccClientAdditionPax(LCCClientReservationAdditionalPax lccClientAdditionPax) {
		this.lccClientAdditionPax = lccClientAdditionPax;
	}

	/**
	 * @return the zuluStartTimeStamp
	 */
	public Date getZuluStartTimeStamp() {
		return zuluStartTimeStamp;
	}

	/**
	 * @param zuluStartTimeStamp
	 *            the zuluStartTimeStamp to set
	 */
	public void setZuluStartTimeStamp(Date zuluStartTimeStamp) {
		this.zuluStartTimeStamp = zuluStartTimeStamp;
	}

	/**
	 * @return the zuluReleaseTimeStamp
	 */
	public Date getZuluReleaseTimeStamp() {
		return zuluReleaseTimeStamp;
	}

	/**
	 * @param zuluReleaseTimeStamp
	 *            the zuluReleaseTimeStamp to set
	 */
	public void setZuluReleaseTimeStamp(Date zuluReleaseTimeStamp) {
		this.zuluReleaseTimeStamp = zuluReleaseTimeStamp;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the nationalityCode
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            the nationalityCode to set
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the paxCategory
	 */
	public String getPaxCategory() {
		return paxCategory;
	}

	/**
	 * @param paxCategory
	 *            the paxCategory to set
	 */
	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}

	/**
	 * @return the parent
	 */
	public LCCClientReservationPax getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(LCCClientReservationPax parent) {
		this.parent = parent;
	}

	/**
	 * @return the infants
	 */
	public Set<LCCClientReservationPax> getInfants() {

		if (this.infants == null) {
			this.setInfants(new HashSet<LCCClientReservationPax>());
		}

		return infants;
	}

	/**
	 * @param infants
	 *            the infants to set
	 */
	private void setInfants(Set<LCCClientReservationPax> infants) {
		this.infants = infants;
	}

	/**
	 * @return the travelerRefNumber
	 */
	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	/**
	 * @param travelerRefNumber
	 *            the travelerRefNumber to set
	 */
	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	/**
	 * @return the attachedPaxId
	 */
	public Integer getAttachedPaxId() {
		return attachedPaxId;
	}

	/**
	 * @param attachedPaxId
	 *            the attachedPaxId to set
	 */
	public void setAttachedPaxId(Integer attachedPaxId) {
		this.attachedPaxId = attachedPaxId;
	}

	public List<TaxTO> getTaxes() {
		return taxes;
	}

	public void setTaxes(List<TaxTO> taxes) {
		this.taxes = taxes;
	}

	public List<FeeTO> getFees() {
		return fees;
	}

	public void setFees(List<FeeTO> fees) {
		this.fees = fees;
	}

	public List<SurchargeTO> getSurcharges() {
		return surcharges;
	}

	public void setSurcharges(List<SurchargeTO> surcharges) {
		this.surcharges = surcharges;
	}

	public List<FareTO> getFares() {
		return fares;
	}

	public void setFares(List<FareTO> fares) {
		this.fares = fares;
	}

	/**
	 * @return the lccClientPaymentHolder
	 */
	public LCCClientPaymentHolder getLccClientPaymentHolder() {
		return lccClientPaymentHolder;
	}

	/**
	 * @param lccClientPaymentHolder
	 *            the lccClientPaymentHolder to set
	 */
	public void setLccClientPaymentHolder(LCCClientPaymentHolder lccClientPaymentHolder) {
		this.lccClientPaymentHolder = lccClientPaymentHolder;
	}

	/**
	 * @return the lccClientPaymentAssembler
	 */
	public LCCClientPaymentAssembler getLccClientPaymentAssembler() {
		return lccClientPaymentAssembler;
	}

	/**
	 * @param lccClientPaymentAssembler
	 *            the lccClientPaymentAssembler to set
	 */
	public void setLccClientPaymentAssembler(LCCClientPaymentAssembler lccClientPaymentAssembler) {
		this.lccClientPaymentAssembler = lccClientPaymentAssembler;
	}

	/**
	 * @return the totalTicketPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalTicketPrice
	 *            the totalTicketPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the selectedAncillaries
	 */
	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		return selectedAncillaries;
	}

	/**
	 * @param selectedAncillaries
	 *            the selectedAncillaries to set
	 */
	public void setSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}

	/**
	 * 
	 * @param selectedAncillaries
	 */
	public void addSelectedAncillaries(LCCSelectedSegmentAncillaryDTO selectedAncillaries) {
		if (this.selectedAncillaries == null) {
			this.selectedAncillaries = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		}

		this.selectedAncillaries.add(selectedAncillaries);
	}

	/**
	 * 
	 * @param selectedAncillaries
	 */
	public void addSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		if (this.selectedAncillaries == null) {
			this.selectedAncillaries = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		}

		this.selectedAncillaries.addAll(selectedAncillaries);
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return
	 */
	public Collection<LccClientPassengerEticketInfoTO> geteTickets() {
		return eTickets;
	}

	/**
	 * @param eTickets
	 */
	public void seteTickets(Collection<LccClientPassengerEticketInfoTO> eTickets) {
		this.eTickets = eTickets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientReservationPax [attachedPaxId=");
		builder.append(attachedPaxId);
		builder.append(", dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", eTicket=");
		builder.append("");
		builder.append(", fares=");
		builder.append(fares);
		builder.append(", fees=");
		builder.append(fees);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", infants=");
		builder.append(infants);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", lccClientPaymentAssembler=");
		builder.append(lccClientPaymentAssembler);
		builder.append(", lccClientPaymentHolder=");
		builder.append(lccClientPaymentHolder);
		builder.append(", lccReservation=");
		builder.append(lccReservation);
		builder.append(", nationalityCode=");
		builder.append(nationalityCode);
		builder.append(", nationality=");
		builder.append(nationality);
		// removed coz stack overflow due to the recursion
		// builder.append(", parent=");
		// builder.append(parent);
		builder.append(", paxSequence=");
		builder.append(paxSequence);
		builder.append(", paxType=");
		builder.append(paxType);
		builder.append(", pnrPaxCatFOIDNumber=");
		builder.append("");
		builder.append(", pnrPaxCatFOIDType=");
		builder.append("");
		builder.append(", selectedAncillaries=");
		builder.append(selectedAncillaries);
		builder.append(", status=");
		builder.append(status);
		builder.append(", surcharges=");
		builder.append(surcharges);
		builder.append(", taxes=");
		builder.append(taxes);
		builder.append(", title=");
		builder.append(title);
		builder.append(", totalAdjustmentCharge=");
		builder.append(totalAdjustmentCharge);
		builder.append(", totalAvailableBalance=");
		builder.append(totalAvailableBalance);
		builder.append(", totalCancelCharge=");
		builder.append(totalCancelCharge);
		builder.append(", totalFare=");
		builder.append(totalFare);
		builder.append(", totalModificationCharge=");
		builder.append(totalModificationCharge);
		builder.append(", totalPaidAmount=");
		builder.append(totalPaidAmount);
		builder.append(", totalSurCharge=");
		builder.append(totalSurCharge);
		builder.append(", totalTaxCharge=");
		builder.append(totalTaxCharge);
		builder.append(", travelerRefNumber=");
		builder.append(travelerRefNumber);
		builder.append(", zuluReleaseTimeStamp=");
		builder.append(zuluReleaseTimeStamp);
		builder.append(", zuluStartTimeStamp=");
		builder.append(zuluStartTimeStamp);
		builder.append(", alertAutoCancellation=");
		builder.append(alertAutoCancellation);
		builder.append("]");
		return builder.toString();
	}

	public LCCClientReservationPaxNamesTranslation getLccClientPaxNameTranslations() {
		return lccClientPaxNameTranslations;
	}

	public void setLccClientPaxNameTranslations(LCCClientReservationPaxNamesTranslation lccClientPaxNameTranslations) {
		this.lccClientPaxNameTranslations = lccClientPaxNameTranslations;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {

		if (totalDiscount.doubleValue() < 0)
			totalDiscount = totalDiscount.negate();

		this.totalDiscount = totalDiscount;
	}

	@Override
	public int compareTo(LCCClientReservationPax o) {
		return (this.travelerRefNumber.compareTo(o.getTravelerRefNumber()));
	}

	public BigDecimal getTotalActualCredit() {
		return totalActualCredit;
	}

	public void setTotalActualCredit(BigDecimal totalActualCredit) {
		this.totalActualCredit = totalActualCredit;
	}

	public BigDecimal getNoShowRefundableAmount() {
		return noShowRefundableAmount;
	}

	public void setNoShowRefundableAmount(BigDecimal noShowRefundableAmount) {
		this.noShowRefundableAmount = noShowRefundableAmount;
	}

	public boolean isNoShowRefundable() {
		return isNoShowRefundable;
	}

	public void setNoShowRefundable(boolean isNoShowRefundable) {
		this.isNoShowRefundable = isNoShowRefundable;
	}

	public boolean isTBA() {
		return "T B A".equals(getFirstName()) && "T B A".equals(getLastName());
	}

	public boolean isBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(boolean blacklisted) {
		this.blacklisted = blacklisted;
	}	
		
}