package com.isa.thinair.airproxy.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeReverse;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airreservation.api.dto.GroundSegmentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

public interface AirproxySegmentBD {

	public static final String SERVICE_NAME = "AirproxyReservationService";

	public LCCClientReservation cancelSegments(LCCClientResAlterModesTO lccClientResAlterModesTO,
			ClientCommonInfoDTO clientCommonInfoDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation modifySegments(LCCClientResAlterModesTO lccClientResAlterQueryModesTO, boolean enableFraudCheck,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation addSegment(LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			ClientCommonInfoDTO clientInfoDTO, boolean enableFraudCheck, TrackInfoDTO trackInfo) throws Exception;

	public void clearSegmentAlerts(LCCClientClearAlertDTO lccClientClearAlertDTO, BasicTrackInfo trackInfo)
			throws ModuleException;

	public void transferSegments(LCCClientTransferSegmentTO clientTransferSegmentTO, BasicTrackInfo trackInfo, String salesChannelKey)
			throws ModuleException;

	public LCCClientReservation confirmOpenReturnSegments(String pnr, LCCClientResAlterModesTO lccClientResAlterQueryModesTO,
			Collection<Integer> oldSegIds, Collection<TempSegBcAlloc> blockSeatIds, TrackInfoDTO trackInfo)
			throws ModuleException;

	public void clearAlerts(Map<String, Collection<Integer>> alertIDMap, BasicTrackInfo trackInfo) throws ModuleException;

	public List[] getGroundSegment(GroundSegmentTO groundSegmentTO, BasicTrackInfo trackInfo) throws ModuleException;

	public Map<Pair<String, Integer>, Set<ETicketInfoTO>> getETicketInformation(List<String> lccUniqueIDs, List<Integer> txnIDs,
			BasicTrackInfo trackInfo) throws ModuleException;
	
	public void exchangeSegment(String pnr,boolean isGroupPnr, TrackInfoDTO trackInfo) throws ModuleException;

	public void noShowTaxChargesReversal(String pnr, boolean isGroupPnr, Collection<Integer> pnrSegIds, String version,
			List<LCCClientChargeReverse> chargeReverseList, ClientCommonInfoDTO clientInfoDto, TrackInfoDTO trackInfo)
			throws ModuleException;
}
