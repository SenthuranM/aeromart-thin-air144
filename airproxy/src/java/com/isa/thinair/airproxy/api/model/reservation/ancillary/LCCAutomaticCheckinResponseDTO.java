package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aravinth.r
 * 
 */
public class LCCAutomaticCheckinResponseDTO implements Serializable {

	private static final long serialVersionUID = 8709053369282488351L;
	private List<LCCFlightSegmentAutomaticCheckinDTO> flightSegmentAutomaticCheckin;

	/**
	 * @return the flightSegmentAutocheckins
	 */
	public List<LCCFlightSegmentAutomaticCheckinDTO> getFlightSegmentAutomaticCheckin() {

		if (flightSegmentAutomaticCheckin == null) {
			flightSegmentAutomaticCheckin = new ArrayList<LCCFlightSegmentAutomaticCheckinDTO>();
		}
		return flightSegmentAutomaticCheckin;

	}

}
