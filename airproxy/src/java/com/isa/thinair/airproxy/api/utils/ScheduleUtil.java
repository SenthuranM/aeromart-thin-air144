package com.isa.thinair.airproxy.api.utils;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ModuleCriterion;

public class ScheduleUtil {

	private static final Log log = LogFactory.getLog(ScheduleUtil.class);

	public static ModuleCriterion createModuleCriterion(String condition, String fieldName, List valueList) {

		ModuleCriterion moduleCriterion = new ModuleCriterion();
		moduleCriterion.setCondition(condition);
		moduleCriterion.setFieldName(fieldName);
		moduleCriterion.setValue(valueList);

		return moduleCriterion;
	}

	/**
	 * Method to Check Empty or not null
	 * 
	 * @param str
	 *            the String need to be Checked
	 * @return boolean true false
	 */
	public static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("All") || str.trim().equals("-1"));
	}

	/**
	 * Method to Check not null
	 * 
	 * @param str
	 *            the String need to be Checked
	 * @return boolean true false
	 */
	public static boolean isNotNull(Object obj) {
		return !((obj == null));
	}

	/**
	 * Method to Check null
	 * 
	 * @param str
	 *            the String need to be Checked
	 * @return boolean true false
	 */
	public static boolean isNull(Object obj) {
		return ((obj == null));
	}
}
