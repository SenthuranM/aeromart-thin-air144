/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;

/**
 * To hold payment data transfer information
 * 
 * @author Nilindra Fernando
 */
public interface LCCClientPaymentInfo {

	/**
	 * Returns total amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalAmount();

	/**
	 * Set total amount
	 * 
	 * @param totalAmount
	 */
	public void setTotalAmount(BigDecimal totalAmount);

	public PayCurrencyDTO getPayCurrencyDTO();

	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO);

	public Integer getPaxSequence();

	public void setPaxSequence(Integer paxSequence);

	public Date getTxnDateTime();

	public void setTxnDateTime(Date date);

	public String getCarrierCode();

	public void setCarrierCode(String carrierCode);

	public String getTotalAmountCurrencyCode();

	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode);

	public String getLccUniqueTnxId();

	public void setLccUniqueTnxId(String lccUniqueTnxId);

	public String getRemarks();

	public void setRemarks(String remarks);

	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount);

	public BigDecimal getPayCurrencyAmount();

	public void setIncludeExternalCharges(boolean includeExternalCharges);

	public boolean includeExternalCharges();

	public String getPayReference();

	public void setPayReference(String payReference);

	public Integer getActualPaymentMethod();

	public void setActualPaymentMethod(Integer actualPaymentMethod);

	public String getOriginalPayReference();

	public void setOriginalPayReference(String originalPayReference);

	public String getRecieptNumber();

	public void setRecieptNumber(String recieptNumber);

	public String getCarrierVisePayments();

	public void setCarrierVisePayments(String carrierVisePayments);

	public String getPaymentTnxReference();

	public void setPaymentTnxReference(String paymentTnxReference);

	public String getOriginalPaymentUID();

	public void setOriginalPaymentUID(String originalPaymentUID);

	public Date getPaymentTnxDateTime();

	public void setPaymentTnxDateTime(Date paymentTnxDateTime);

	public boolean isOfflinePayment();

	public void setOfflinePayment(boolean isOfflinePayment);
	
	public boolean isNonRefundable();
	
	public void setNonRefundable(boolean isNonRefundable);	

}
