package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;

public class PaxCnxModPenChargeForServiceTaxDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private boolean totalAmountIncludingServiceTax;

	private List<LCCClientExternalChgDTO> charges;

	public boolean isTotalAmountIncludingServiceTax() {
		return totalAmountIncludingServiceTax;
	}

	public List<LCCClientExternalChgDTO> getCharges() {
		return charges;
	}

	public void setTotalAmountIncludingServiceTax(boolean totalAmountIncludingServiceTax) {
		this.totalAmountIncludingServiceTax = totalAmountIncludingServiceTax;
	}

	public void setCharges(List<LCCClientExternalChgDTO> charges) {
		this.charges = charges;
	}
}
