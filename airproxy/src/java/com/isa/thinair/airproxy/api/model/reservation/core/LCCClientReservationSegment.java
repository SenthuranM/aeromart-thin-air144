/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Date;

import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;

/**
 * To keep track of reservation segment information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientReservationSegment implements Comparable<LCCClientReservationSegment>, Serializable {

	private static final long serialVersionUID = 1131346579091130496L;

	private String flightSegmentRefNumber;

	private Integer flightBaggageAllowance;

	private String bookingFlightSegmentRefNumber;

	private String anciOfferFlightSegmentRefNumber;

	/** Holds external segment sequence */
	private Integer segmentSeq;

	/** Holds reservation segment return flag */
	private String returnFlag;

	/** Holds flight number */
	private String flightNo;

	/** Holds departure date */
	private Date departureDate;

	private Date departureDateZulu;

	/** Holds arrival date */
	private Date arrivalDate;

	private Date arrivalDateZulu;

	/** Holds segment code */
	private String segmentCode;

	/** Holds external segment status */
	private String status;

	/** Holds external segment display status */
	private String displayStatus;

	/** Holds the reservation object */
	private LCCClientReservation lccReservation;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/** Hold the logical cabin class code */
	private String logicalCabinClass;

	private String interlineGroupKey;

	private Date modifyTillBufferDateTime;

	private Date cancelTillBufferDateTime;

	private Date modifyTillFlightClosureDateTime;

	private String departureAirportName;

	private String arrivalAirportName;

	private String bookingType;

	private SYSTEM system;

	private String carrierCode;

	/** Holds Segment Modifiable Status */
	private boolean isModifible;

	/** Holds segment cancellable status */
	private boolean isCancellable;

	/** Hold Fare Rule Details **/
	private FareTO fareTO;

	private String interlineReturnGroupKey;

	private boolean isOpenReturnSegment;

	private String openRetConfirmBefore;

	private String openReturnTravelExpiry;

	/** Holds the Ground Segment Code if the segment is a Ground Segment */
	private String subStationShortName;

	private Integer groundStationPnrSegmentID;

	private Integer pnrSegID;

	/** The arrival terminal name */
	private String arrivalTerminalName;

	/** The departure terminal name. */
	private String departureTerminalName;

	/** The check-in time gap for the segment **/
	private int checkInTimeGap;

	/** The check-in closing time for the segment **/
	private int checkInClosingTime;

	/** segment modification */
	private boolean modifyByDate;

	private boolean modifyByRoute;

	private Integer statusModifiedChannelCode;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String remarks;

	private boolean domesticFlight;

	private String depAirportMinConTime;
	private String depAirportMaxConTime;
	private String arriAirportMinConTime;
	private String arriAirportMaxConTime;

	private Date lastFareQuoteDate;

	private String baggageONDGroupId;

	private Date ticketValidTill;

	private Integer journeySequence;

	private boolean flownSegment;

	private boolean allPaxNoShow;

	private boolean unSegment;

	private String subStatus;

	private boolean alertAutoCancellation;

	private Integer flexiRuleID;
	
	private String routeRefNumber;

	private Integer bundledServicePeriodId;

	private String csOcCarrierCode;
	
	private String csOcFlightNumber;
	
	private String bookingClassCode;
	
	private boolean isSegmentModifiableAsPerETicketStatus;

	/**
	 * @return the flightSegmentRefNumber
	 */
	public String getFlightSegmentRefNumber() {
		return flightSegmentRefNumber;
	}

	/**
	 * @param flightSegmentRefNumber
	 *            the flightSegmentRefNumber to set
	 */
	public void setFlightSegmentRefNumber(String flightSegmentRefNumber) {
		this.flightSegmentRefNumber = flightSegmentRefNumber;
	}

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the arrivalDate
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            the arrivalDate to set
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the lccReservation
	 */
	@JSON(serialize = false)
	public LCCClientReservation getLccReservation() {
		return lccReservation;
	}

	/**
	 * @param lccReservation
	 *            the lccReservation to set
	 */
	public void setLccReservation(LCCClientReservation lccReservation) {
		this.lccReservation = lccReservation;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCabinClass
	 */
	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	/**
	 * @param logicalCabinClass
	 *            the logicalCabinClass to set
	 */
	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	/**
	 * @return the segmentSeq
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            the segmentSeq to set
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return the returnFlag
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getInterlineGroupKey() {
		return interlineGroupKey;
	}

	public void setInterlineGroupKey(String interlineGroupKey) {
		this.interlineGroupKey = interlineGroupKey;
	}

	/**
	 * @return the modifyTillBufferDateTime
	 */
	public Date getModifyTillBufferDateTime() {
		return modifyTillBufferDateTime;
	}

	/**
	 * @param modifyTillBufferDateTime
	 *            the modifyTillBufferDateTime to set
	 */
	public void setModifyTillBufferDateTime(Date modifyTillBufferDateTime) {
		this.modifyTillBufferDateTime = modifyTillBufferDateTime;
	}

	/**
	 * Had to introduce this to differentiate modification and cancellation buffer time
	 * 
	 * @return the cancelTillBufferDateTime
	 */
	public Date getCancelTillBufferDateTime() {
		return cancelTillBufferDateTime;
	}

	/**
	 * @param cancelTillBufferDateTime
	 *            the cancelTillBufferDateTime to set
	 */
	public void setCancelTillBufferDateTime(Date cancelTillBufferDateTime) {
		this.cancelTillBufferDateTime = cancelTillBufferDateTime;
	}

	/**
	 * @return the modifyTillFlightClosureDateTime
	 */
	public Date getModifyTillFlightClosureDateTime() {
		return modifyTillFlightClosureDateTime;
	}

	/**
	 * @param modifyTillFlightClosureDateTime
	 *            the modifyTillFlightClosureDateTime to set
	 */
	public void setModifyTillFlightClosureDateTime(Date modifyTillFlightClosureDateTime) {
		this.modifyTillFlightClosureDateTime = modifyTillFlightClosureDateTime;
	}

	/**
	 * @return the bookingFlightSegmentRefNumber
	 */
	public String getBookingFlightSegmentRefNumber() {
		return bookingFlightSegmentRefNumber;
	}

	/**
	 * @param bookingFlightSegmentRefNumber
	 *            the bookingFlightSegmentRefNumber to set
	 */
	public void setBookingFlightSegmentRefNumber(String bookingFlightSegmentRefNumber) {
		this.bookingFlightSegmentRefNumber = bookingFlightSegmentRefNumber;
	}

	public String getDepartureAirportName() {
		return departureAirportName;
	}

	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}

	public String getArrivalAirportName() {
		return arrivalAirportName;
	}

	public void setArrivalAirportName(String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}

	public String getBookingType() {
		return this.bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public boolean isModifible() {
		return isModifible;
	}

	public void setModifible(boolean isModifible) {
		this.isModifible = isModifible;
	}

	public boolean isCancellable() {
		return isCancellable;
	}

	public void setCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}

	public FareTO getFareTO() {
		return fareTO;
	}

	public void setFareTO(FareTO fareTO) {
		this.fareTO = fareTO;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	public Date getArrivalDateZulu() {
		return arrivalDateZulu;
	}

	public void setArrivalDateZulu(Date arrivalDateZulu) {
		this.arrivalDateZulu = arrivalDateZulu;
	}

	public String getInterlineReturnGroupKey() {
		return interlineReturnGroupKey;
	}

	public void setInterlineReturnGroupKey(String interlineReturnGroupKey) {
		this.interlineReturnGroupKey = interlineReturnGroupKey;
	}

	public int compareTo(LCCClientReservationSegment o) {
		return this.departureDateZulu.compareTo(o.getDepartureDateZulu());
	}

	public boolean isOpenReturnSegment() {
		return isOpenReturnSegment;
	}

	public void setOpenReturnSegment(boolean isOpenReturnSegment) {
		this.isOpenReturnSegment = isOpenReturnSegment;
	}

	public String getOpenRetConfirmBefore() {
		return openRetConfirmBefore;
	}

	public void setOpenRetConfirmBefore(String openRetConfirmBefore) {
		this.openRetConfirmBefore = openRetConfirmBefore;
	}

	public String getOpenReturnTravelExpiry() {
		return openReturnTravelExpiry;
	}

	public void setOpenReturnTravelExpiry(String openReturnTravelExpiry) {
		this.openReturnTravelExpiry = openReturnTravelExpiry;
	}

	public String getSubStationShortName() {
		return subStationShortName;
	}

	public void setSubStationShortName(String subStationShortName) {
		this.subStationShortName = subStationShortName;
	}

	public Integer getGroundStationPnrSegmentID() {
		return groundStationPnrSegmentID;
	}

	public void setGroundStationPnrSegmentID(Integer groundStationPnrSegmentID) {
		this.groundStationPnrSegmentID = groundStationPnrSegmentID;
	}

	public Integer getPnrSegID() {
		return pnrSegID;
	}

	public void setPnrSegID(Integer pnrSegID) {
		this.pnrSegID = pnrSegID;
	}

	/**
	 * @return The arrival terminal name.
	 */
	public String getArrivalTerminalName() {
		return arrivalTerminalName;
	}

	/**
	 * @param arrivalTerminalName
	 *            The arrival terminal name to set.
	 */
	public void setArrivalTerminalName(String arrivalTerminalName) {
		this.arrivalTerminalName = arrivalTerminalName;
	}

	/**
	 * @return the departure terminal name.
	 */
	public String getDepartureTerminalName() {
		return departureTerminalName;
	}

	/**
	 * @param departureTerminalName
	 *            The departure terminal name to set.
	 */
	public void setDepartureTerminalName(String departureTerminalName) {
		this.departureTerminalName = departureTerminalName;
	}

	/**
	 * @return The check in time gap for the segment
	 */
	public int getCheckInTimeGap() {
		return checkInTimeGap;
	}

	/**
	 * @param checkInTimeGap
	 *            The check in time gap to set.
	 */
	public void setCheckInTimeGap(int checkInTimeGap) {
		this.checkInTimeGap = checkInTimeGap;
	}

	public int getCheckInClosingTime() {
		return checkInClosingTime;
	}

	public void setCheckInClosingTime(int checkInClosingTime) {
		this.checkInClosingTime = checkInClosingTime;
	}

	public boolean isModifyByDate() {
		return modifyByDate;
	}

	public void setModifyByDate(boolean modifyByDate) {
		this.modifyByDate = modifyByDate;
	}

	public boolean isModifyByRoute() {
		return modifyByRoute;
	}

	public void setModifyByRoute(boolean modifyByRoute) {
		this.modifyByRoute = modifyByRoute;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isDomesticFlight() {
		return domesticFlight;
	}

	public void setDomesticFlight(boolean domesticFlight) {
		this.domesticFlight = domesticFlight;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public String getDepAirportMinConTime() {
		return depAirportMinConTime;
	}

	public void setDepAirportMinConTime(String depAirportMinConTime) {
		this.depAirportMinConTime = depAirportMinConTime;
	}

	public String getDepAirportMaxConTime() {
		return depAirportMaxConTime;
	}

	public void setDepAirportMaxConTime(String depAirportMaxConTime) {
		this.depAirportMaxConTime = depAirportMaxConTime;
	}

	public String getArriAirportMinConTime() {
		return arriAirportMinConTime;
	}

	public void setArriAirportMinConTime(String arriAirportMinConTime) {
		this.arriAirportMinConTime = arriAirportMinConTime;
	}

	public String getArriAirportMaxConTime() {
		return arriAirportMaxConTime;
	}

	public void setArriAirportMaxConTime(String arriAirportMaxConTime) {
		this.arriAirportMaxConTime = arriAirportMaxConTime;
	}

	/**
	 * Set the last fare quote time in zulu
	 * 
	 * @param lastFareQuoteDate
	 */
	public void setLastFareQuoteDate(Date lastFareQuoteDate) {
		this.lastFareQuoteDate = lastFareQuoteDate;
	}

	/**
	 * Return the last fare quote time in zulu
	 * 
	 * @return
	 */
	public Date getLastFareQuoteDate() {
		return lastFareQuoteDate;
	}

	/**
	 * @return the baggageONDGroupId
	 */
	public String getBaggageONDGroupId() {
		return baggageONDGroupId;
	}

	/**
	 * @param baggageONDGroupId
	 *            the baggageONDGroupId to set
	 */
	public void setBaggageONDGroupId(String baggageONDGroupId) {
		this.baggageONDGroupId = baggageONDGroupId;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public Integer getJourneySequence() {
		return journeySequence;
	}

	public void setJourneySequence(Integer journeySequence) {
		this.journeySequence = journeySequence;
	}

	public Integer getFlightBaggageAllowance() {
		return flightBaggageAllowance;
	}

	public void setFlightBaggageAllowance(Integer flightBaggageAllowance) {
		this.flightBaggageAllowance = flightBaggageAllowance;
	}

	public boolean isFlownSegment() {
		return flownSegment;
	}

	public void setFlownSegment(boolean flownSegment) {
		this.flownSegment = flownSegment;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public boolean isAllPaxNoShow() {
		return allPaxNoShow;
	}

	public void setAllPaxNoShow(boolean allPaxNoShow) {
		this.allPaxNoShow = allPaxNoShow;
	}

	public boolean isUnSegment() {
		return unSegment;
	}

	public void setUnSegment(boolean unSegment) {
		this.unSegment = unSegment;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public Integer getStatusModifiedChannelCode() {
		return statusModifiedChannelCode;
	}

	public void setStatusModifiedChannelCode(Integer statusModifiedChannelCode) {
		this.statusModifiedChannelCode = statusModifiedChannelCode;
	}

	/**
	 * @return the flexiRuleID
	 */
	public Integer getFlexiRuleID() {
		return flexiRuleID;
	}

	/**
	 * @param flexiRuleID
	 *            the flexiRuleID to set
	 */
	public void setFlexiRuleID(Integer flexiRuleID) {
		this.flexiRuleID = flexiRuleID;
	}

	public String getAnciOfferFlightSegmentRefNumber() {
		return anciOfferFlightSegmentRefNumber;
	}

	public void setAnciOfferFlightSegmentRefNumber(String anciOfferFlightSegmentRefNumber) {
		this.anciOfferFlightSegmentRefNumber = anciOfferFlightSegmentRefNumber;
	}

	/**
	 * @return the routeRefNumber
	 */
	public String getRouteRefNumber() {
		return routeRefNumber;
	}

	/**
	 * @param routeRefNumber the routeRefNumber to set
	 */
	public void setRouteRefNumber(String routeRefNumber) {
		this.routeRefNumber = routeRefNumber;
	}

	public Integer getBundledServicePeriodId() {
		return bundledServicePeriodId;
	}

	public void setBundledServicePeriodId(Integer bundledServicePeriodId) {
		this.bundledServicePeriodId = bundledServicePeriodId;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}
	
	public boolean isSegmentModifiableAsPerETicketStatus() {
		return isSegmentModifiableAsPerETicketStatus;
	}

	public void setSegmentModifiableAsPerETicketStatus(boolean isSegmentModifiableAsPerETicketStatus) {
		this.isSegmentModifiableAsPerETicketStatus = isSegmentModifiableAsPerETicketStatus;
	}
	
}
