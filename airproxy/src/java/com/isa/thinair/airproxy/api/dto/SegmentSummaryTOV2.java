package com.isa.thinair.airproxy.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class SegmentSummaryTOV2 {

	private FareInfoTOV2 currentAmounts;

	private FareInfoTOV2 currentNonRefundableAmounts;

	private FareInfoTOV2 currentRefundableAmounts;

	private FareInfoTOV2 newAmounts;

	private FareInfoTOV2 newNonRefundableAmounts;

	private FareInfoTOV2 newRefundableAmounts;

	private BigDecimal identifiedTotalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal identifiedTotalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal outBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal inBoundFlexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal modificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal excludedSegCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal identifiedExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public static FareInfoTOV2 convert(Collection<ChargeMetaTO> colChargeMetaTO, BigDecimal totalFareDiscount) {
		FareInfoTOV2 fareInfoTOV2 = new FareInfoTOV2();
		fareInfoTOV2.addMetaCharges(colChargeMetaTO);

		// not required since it has been added as a minus charge
		// fareInfoTOV2.setTotalFareDiscount(totalFareDiscount);

		// BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal totalSurcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal totalCanCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		// BigDecimal totalAdjCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		//
		// if (totalFareDiscount == null) {
		// totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		// }
		//
		// for (ChargeMetaTO chargeMetaTO : colChargeMetaTO) {
		// if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addFare(chargeMetaTO);
		// totalFare = AccelAeroCalculator.add(totalFare, chargeMetaTO.getChargeAmount());
		// } else if (ReservationInternalConstants.ChargeGroup.TAX.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addTax(chargeMetaTO);
		// totalTax = AccelAeroCalculator.add(totalTax, chargeMetaTO.getChargeAmount());
		// } else if (ReservationInternalConstants.ChargeGroup.SUR.equals(chargeMetaTO.getChargeGroupCode())
		// || ReservationInternalConstants.ChargeGroup.INF.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addSurcharge(chargeMetaTO);
		// totalSurcharge = AccelAeroCalculator.add(totalSurcharge, chargeMetaTO.getChargeAmount());
		// } else if (ReservationInternalConstants.ChargeGroup.MOD.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addModification(chargeMetaTO);
		// totalModCharge = AccelAeroCalculator.add(totalModCharge, chargeMetaTO.getChargeAmount());
		// } else if (ReservationInternalConstants.ChargeGroup.CNX.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addCancellation(chargeMetaTO);
		// totalCanCharge = AccelAeroCalculator.add(totalCanCharge, chargeMetaTO.getChargeAmount());
		// } else if (ReservationInternalConstants.ChargeGroup.ADJ.equals(chargeMetaTO.getChargeGroupCode())) {
		// fareInfoTOV2.addAdjustments(chargeMetaTO);
		// totalAdjCharge = AccelAeroCalculator.add(totalAdjCharge, chargeMetaTO.getChargeAmount());
		// }
		// }
		//
		// fareInfoTOV2.setTotalFare(totalFare);
		// fareInfoTOV2.setTotalFareDiscount(totalFareDiscount);
		// fareInfoTOV2.setTotalTax(totalTax);
		// fareInfoTOV2.setTotalSurcharge(totalSurcharge);
		// fareInfoTOV2.setTotalCanCharge(totalCanCharge);
		// fareInfoTOV2.setTotalModCharge(totalModCharge);
		// fareInfoTOV2.setTotalAdjCharge(totalAdjCharge);
		// fareInfoTOV2.setTotalPrice(AccelAeroCalculator.add(totalFare, totalTax, totalSurcharge, totalCanCharge,
		// totalModCharge,
		// totalAdjCharge));

		return fareInfoTOV2;
	}

	public void addNewMetaCharges(Collection<ChargeMetaTO> colChargeMetaTO) {
		if (newAmounts == null) {
			newAmounts = new FareInfoTOV2();
		}
		newAmounts.addMetaCharges(colChargeMetaTO);
	}

	public void addCurrentMetaCharges(Collection<ChargeMetaTO> colChargeMetaTO) {
		if (currentAmounts == null) {
			currentAmounts = new FareInfoTOV2();
		}
		currentAmounts.addMetaCharges(colChargeMetaTO);
	}

	public void addCurrentRefundableMetaCharges(Collection<ChargeMetaTO> colChargeMetaTO) {
		if (currentRefundableAmounts == null) {
			currentRefundableAmounts = new FareInfoTOV2();
		}
		currentRefundableAmounts.addMetaCharges(colChargeMetaTO);
	}

	public void addCurrentNonRefundableMetaCharges(Collection<ChargeMetaTO> colChargeMetaTO) {
		if (currentNonRefundableAmounts == null) {
			currentNonRefundableAmounts = new FareInfoTOV2();
		}
		currentNonRefundableAmounts.addMetaCharges(colChargeMetaTO);
	}

	/**
	 * @return the currentAmounts
	 */
	public FareInfoTOV2 getCurrentAmounts() {
		if (currentAmounts == null) {
			currentAmounts = new FareInfoTOV2();
		}
		return currentAmounts;
	}

	/**
	 * @param currentAmounts
	 *            the currentAmounts to set
	 */
	public void setCurrentAmounts(FareInfoTOV2 currentAmounts) {
		this.currentAmounts = currentAmounts;
	}

	/**
	 * @return the currentNonRefundableAmounts
	 */
	public FareInfoTOV2 getCurrentNonRefundableAmounts() {
		if (currentNonRefundableAmounts == null) {
			currentNonRefundableAmounts = new FareInfoTOV2();
		}
		return currentNonRefundableAmounts;
	}

	/**
	 * @param currentNonRefundableAmounts
	 *            the currentNonRefundableAmounts to set
	 */
	public void setCurrentNonRefundableAmounts(FareInfoTOV2 currentNonRefundableAmounts) {
		this.currentNonRefundableAmounts = currentNonRefundableAmounts;
	}

	/**
	 * @return the currentRefundableAmounts
	 */
	public FareInfoTOV2 getCurrentRefundableAmounts() {
		if (currentRefundableAmounts == null) {
			currentRefundableAmounts = new FareInfoTOV2();
		}
		return currentRefundableAmounts;
	}

	/**
	 * @param currentRefundableAmounts
	 *            the currentRefundableAmounts to set
	 */
	public void setCurrentRefundableAmounts(FareInfoTOV2 currentRefundableAmounts) {
		this.currentRefundableAmounts = currentRefundableAmounts;
	}

	/**
	 * @return the newAmounts
	 */
	public FareInfoTOV2 getNewAmounts() {
		if (newAmounts == null) {
			newAmounts = new FareInfoTOV2();
		}
		return newAmounts;
	}

	/**
	 * @param newAmounts
	 *            the newAmounts to set
	 */
	public void setNewAmounts(FareInfoTOV2 newAmounts) {
		this.newAmounts = newAmounts;
	}

	/**
	 * @return the newNonRefundableAmounts
	 */
	public FareInfoTOV2 getNewNonRefundableAmounts() {
		return newNonRefundableAmounts;
	}

	/**
	 * @param newNonRefundableAmounts
	 *            the newNonRefundableAmounts to set
	 */
	public void setNewNonRefundableAmounts(FareInfoTOV2 newNonRefundableAmounts) {
		this.newNonRefundableAmounts = newNonRefundableAmounts;
	}

	/**
	 * @return the newRefundableAmounts
	 */
	public FareInfoTOV2 getNewRefundableAmounts() {
		return newRefundableAmounts;
	}

	/**
	 * @param newRefundableAmounts
	 *            the newRefundableAmounts to set
	 */
	public void setNewRefundableAmounts(FareInfoTOV2 newRefundableAmounts) {
		this.newRefundableAmounts = newRefundableAmounts;
	}

	/**
	 * @return the identifiedTotalCnxCharge
	 */
	public BigDecimal getIdentifiedTotalCnxCharge() {
		return identifiedTotalCnxCharge;
	}

	/**
	 * @param identifiedTotalCnxCharge
	 *            the identifiedTotalCnxCharge to set
	 */
	public void setIdentifiedTotalCnxCharge(BigDecimal identifiedTotalCnxCharge) {
		this.identifiedTotalCnxCharge = identifiedTotalCnxCharge;
	}

	/**
	 * @return the identifiedTotalModCharge
	 */
	public BigDecimal getIdentifiedTotalModCharge() {
		return identifiedTotalModCharge;
	}

	/**
	 * @param identifiedTotalModCharge
	 *            the identifiedTotalModCharge to set
	 */
	public void setIdentifiedTotalModCharge(BigDecimal identifiedTotalModCharge) {
		this.identifiedTotalModCharge = identifiedTotalModCharge;
	}

	public BigDecimal getOutBoundFlexiCharge() {
		return outBoundFlexiCharge;
	}

	public void setOutBoundFlexiCharge(BigDecimal outBoundFlexiCharge) {
		this.outBoundFlexiCharge = outBoundFlexiCharge;
	}

	public BigDecimal getInBoundFlexiCharge() {
		return inBoundFlexiCharge;
	}

	public void setInBoundFlexiCharge(BigDecimal inBoundFlexiCharge) {
		this.inBoundFlexiCharge = inBoundFlexiCharge;
	}

	public LCCClientSegmentSummaryTO getSegmentSummaryTO() {
		LCCClientSegmentSummaryTO segmentSummary = new LCCClientSegmentSummaryTO();

		segmentSummary.setCurrentAdjAmount(getCurrentAmounts().getTotalAdjCharge());
		segmentSummary.setCurrentCnxAmount(getCurrentAmounts().getTotalCanCharge());
		segmentSummary.setCurrentFareAmount(getCurrentAmounts().getTotalFare());
		segmentSummary.setCurrentModAmount(getCurrentAmounts().getTotalModCharge());
		segmentSummary.setCurrentNonRefunds(getCurrentNonRefundableAmounts().getTotalPrice());
		segmentSummary.setCurrentRefunds(getCurrentRefundableAmounts().getTotalPrice());
		segmentSummary.setCurrentSurchargeAmount(getCurrentAmounts().getTotalSurcharge());
		segmentSummary.setCurrentTaxAmount(getCurrentAmounts().getTotalTax());
		segmentSummary.setCurrentTotalPrice(getCurrentAmounts().getTotalPrice());
		segmentSummary.setCurrentDiscount(getCurrentAmounts().getTotalDiscount());
		segmentSummary.setCurrentModificationPenatly(getCurrentAmounts().getTotalPenalty());

		segmentSummary.setNewAdjAmount(getNewAmounts().getTotalAdjCharge());
		segmentSummary.setNewCnxAmount(AccelAeroCalculator
				.add(getNewAmounts().getTotalCanCharge(), getIdentifiedTotalCnxCharge()));
		segmentSummary.setNewFareAmount(getNewAmounts().getTotalFare());
		segmentSummary.setNewModAmount(AccelAeroCalculator
				.add(getNewAmounts().getTotalModCharge(), getIdentifiedTotalModCharge()));
		segmentSummary.setNewSurchargeAmount(getNewAmounts().getTotalSurcharge());
		segmentSummary.setNewTaxAmount(getNewAmounts().getTotalTax());
		segmentSummary.setNewTotalPrice(AccelAeroCalculator.add(getNewAmounts().getTotalPrice(), getIdentifiedTotalCnxCharge(),
				getIdentifiedTotalModCharge()));
		segmentSummary.setOutBoundExternalCharge(getOutBoundFlexiCharge());
		segmentSummary.setInBoundExternalCharge(getInBoundFlexiCharge());
		segmentSummary.setModificationPenalty(getModificationPenatly());
		segmentSummary.setNewDiscount(getNewAmounts().getTotalDiscount());
		segmentSummary.setNewExtraFeeAmount(getIdentifiedExtraFeeAmount());
		
		return segmentSummary;
	}

	public void setModificationPenalty(BigDecimal modificationPenalty) {
		this.modificationPenalty = modificationPenalty;
	}

	public BigDecimal getModificationPenatly() {
		return modificationPenalty;
	}

	public BigDecimal getExcludedSegCharge() {
		return excludedSegCharge;
	}

	public void setExcludedSegCharge(Collection<ChargeMetaTO> colChargeMetaTO) {
		for (ChargeMetaTO chargeMetaTO : colChargeMetaTO) {
			excludedSegCharge = AccelAeroCalculator.add(excludedSegCharge, chargeMetaTO.getChargeAmount());
		}
	}

	public BigDecimal getIdentifiedExtraFeeAmount() {
		return identifiedExtraFeeAmount;
	}

	public void setIdentifiedExtraFeeAmount(BigDecimal identifiedExtraFeeAmount) {
		this.identifiedExtraFeeAmount = identifiedExtraFeeAmount;
	}	

}
