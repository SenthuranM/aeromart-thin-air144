package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ReservationDiscountDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int promoPaxCount;
	private Long promotionId;
	private Map<Integer, PaxDiscountDetailTO> paxDiscountDetails;
	private BigDecimal totalDiscount;
	private String discountCode;
	private double discountPercentage;
	private Map<Collection<Integer>, String> segCodeByFltSegId;
	private String discountAs;
	public int getPromoPaxCount() {
		return promoPaxCount;
	}

	public boolean isDiscountExist() {
		if (totalDiscount != null && totalDiscount.doubleValue() > 0) {
			return true;
		}
		return false;
	}

	public void setPromoPaxCount(int promoPaxCount) {
		this.promoPaxCount = promoPaxCount;
	}

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public Map<Integer, PaxDiscountDetailTO> getPaxDiscountDetails() {
		return paxDiscountDetails;
	}

	public void setPaxDiscountDetails(Map<Integer, PaxDiscountDetailTO> paxDiscountDetails) {
		this.paxDiscountDetails = paxDiscountDetails;
	}

	public PaxDiscountDetailTO getPaxDiscountDetail(Integer paxSequence) {
		if (getPaxDiscountDetails() != null && getPaxDiscountDetails().get(paxSequence) != null) {
			return getPaxDiscountDetails().get(paxSequence);
		}
		return null;
	}

	public boolean isDiscountApplicable(Integer paxSequence) {
		PaxDiscountDetailTO paxDiscountDetailTO = getPaxDiscountDetail(paxSequence);
		if (paxDiscountDetailTO != null && paxDiscountDetailTO.getTotalDiscount().doubleValue() != 0) {
			return true;
		}
		return false;
	}

	public Collection<DiscountChargeTO> getPaxDiscountChargeTOs(Integer paxSequence) {
		if (paxSequence != null) {
			PaxDiscountDetailTO paxDiscountDetailTO = getPaxDiscountDetail(paxSequence);
			if (paxDiscountDetailTO != null) {
				return paxDiscountDetailTO.getPaxDiscountChargeTOs();

			}
		}
		return null;
	}

	public Collection<DiscountChargeTO> getPaxDiscountChargeTOsByFltSegId(Integer paxSequence, Collection<Integer> fltSegIds) {
		if (paxSequence != null && fltSegIds != null && !fltSegIds.isEmpty()) {
			PaxDiscountDetailTO paxDiscountDetailTO = getPaxDiscountDetail(paxSequence);
			if (paxDiscountDetailTO != null) {
				return paxDiscountDetailTO.getDiscountChargeTOsByFltSegId(fltSegIds);

			}
		}
		return null;
	}

	public BigDecimal getTotalDiscount() {

		if (this.totalDiscount == null)
			this.totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Map<Collection<Integer>, String> getSegCodeByFltSegId() {

		return segCodeByFltSegId;
	}

	public void addSegmentCode(Collection<Integer> flightSegIds, String segmentCode) {
		if (flightSegIds != null && !flightSegIds.isEmpty() && segmentCode != null) {
			if (getSegCodeByFltSegId() == null) {
				segCodeByFltSegId = new HashMap<Collection<Integer>, String>();
			}
			segCodeByFltSegId.put(flightSegIds, segmentCode);
		}

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n--------------------------Calculated Discount for Reservation--------------------------");
		sb.append("\nDiscount Code :" + this.discountCode);
		sb.append("\nTotal Discount :" + this.totalDiscount);
		sb.append("\nDiscount %:" + this.discountPercentage);
		sb.append("\nPromo Pax Count :" + this.promoPaxCount);
		sb.append("\nPromotion Id :" + this.promotionId);
		sb.append("\nPax Discount Summary :");
		if (paxDiscountDetails != null && !paxDiscountDetails.isEmpty()) {
			for (Integer paxSequence : paxDiscountDetails.keySet()) {
				PaxDiscountDetailTO paxDiscountDetailTO = paxDiscountDetails.get(paxSequence);
				if (paxDiscountDetailTO != null) {
					sb.append("\nPax Sequence :" + paxDiscountDetailTO.getPaxSequence());
					sb.append("\tTotal Discount :" + paxDiscountDetailTO.getTotalDiscount());
					if (paxDiscountDetailTO.isInfantChargesExist()) {
						sb.append("\tapplied to infant");
					}
				}

			}
		}

		sb.append("\n---------------------------------------------------------------------------------------");

		return sb.toString();
	}

	public String getDiscountAs() {
		return discountAs;
	}

	public void setDiscountAs(String discountAs) {
		this.discountAs = discountAs;
	}
}
