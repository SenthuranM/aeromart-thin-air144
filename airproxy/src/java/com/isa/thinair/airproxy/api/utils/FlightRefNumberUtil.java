package com.isa.thinair.airproxy.api.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;

/**
 * FlightRefNumber RPH creation util for aaServices and AA
 * 
 * @author malaka
 * 
 */
public class FlightRefNumberUtil {

	private static Log log = LogFactory.getLog(FlightRefNumberUtil.class);

	private final static String DELIM = "$";
	private final static String SPLIT_DELIM = "\\$";
	private final static String DATE_FORMAT = "yyyyMMddHHmmss";

	/**
	 * Given the FlightSegmentTO return the Interline compatible RPH
	 * 
	 * @param flightSegment
	 * @param segmentId
	 * @return String flightRPH
	 * @see #rphBuilder(String, String, String, Date, Date)
	 */
	public static String composeFlightRPH(FlightSegmentDTO flightSegment) {
		String operatingAirlineCode = flightSegment.getCarrierCode();
		String segmentCode = flightSegment.getSegmentCode();
		Date dDate = flightSegment.getDepartureDateTime();
		Date aDate = flightSegment.getArrivalDateTime();
		Integer segId = flightSegment.getSegmentId();
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);

	}

	/**
	 * Given the LCCClientReservationSegment return the Interline compatible RPH
	 * 
	 * @param segment
	 * @param segmentId
	 * @return
	 * @see #rphBuilder(String, String, String, Date, Date)
	 */
	public static String composeFlightRPH(ReservationSegmentDTO segment) {
		String operatingAirlineCode = segment.getCarrierCode();
		String segmentCode = segment.getSegmentCode();
		Date dDate = segment.getDepartureDate();
		Date aDate = segment.getArrivalDate();
		Integer segId = segment.getFlightSegId();
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);
	}

	public static String composePnrSegRPH(ReservationSegmentDTO segment) {
		String operatingAirlineCode = segment.getCarrierCode();
		String segmentCode = segment.getSegmentCode();
		Date dDate = segment.getDepartureDate();
		Date aDate = segment.getArrivalDate();
		Integer pnrSegId = segment.getPnrSegId();
		return rphBuilder(operatingAirlineCode, segmentCode, pnrSegId, dDate, aDate);
	}

	/**
	 * Given a ExternalFlightSegment returns the compatible RPH
	 * 
	 * @param externalFlightSegment
	 * @return
	 */
	public static String composeFlightRPH(ExternalFlightSegment externalFlightSegment) {

		String operatingAirlineCode = externalFlightSegment.getExternalCarrierCode();
		String segmentCode = externalFlightSegment.getSegmentCode();
		Date dDate = externalFlightSegment.getEstTimeDepatureLocal();
		Date aDate = externalFlightSegment.getEstTimeArrivalLocal();
		Integer segId = externalFlightSegment.getExternalFlightSegId();
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);
	}

	/**
	 * Given the FlightReconcileDTO return the compatible RPH
	 * 
	 * @param flightReconcileDTO
	 * @param carrierCode
	 * @return String flightRPH
	 * @see #rphBuilder(String, String, String, Date, Date)
	 */
	public static String composeFlightRPH(FlightReconcileDTO flightReconcileDTO, String carrierCode) {
		String operatingAirlineCode = carrierCode;
		String segmentCode = flightReconcileDTO.getSegementCode();
		Date dDate = flightReconcileDTO.getDepartureDateTimeLocal();
		Date aDate = flightReconcileDTO.getArrivalDateTimeLocal();
		Integer segId = flightReconcileDTO.getFlightSegId();
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);

	}

	/**
	 * Given the FlightSegmentTO return the compatible RPH
	 * 
	 * @param flightSegment
	 * @param segmentId
	 * @return String flightRPH
	 * @see #rphBuilder(String, String, String, Date, Date)
	 */
	public static String composeFlightRPH(FlightSegmentTO flightSegment) {
		String operatingAirlineCode = flightSegment.getOperatingAirline();
		String segmentCode = flightSegment.getSegmentCode();
		Date dDate = flightSegment.getDepartureDateTime();
		Date aDate = flightSegment.getArrivalDateTime();
		Integer segId = flightSegment.getFlightSegId();
		return rphBuilder(operatingAirlineCode, segmentCode, segId, dDate, aDate);

	}

	/**
	 * FlightRefNumber = Operating_Airline$Segment_Code$Carrier_RPH$Depature_Timepstamp$Arrival_Timespstamp sample
	 * format G9$SHJ/SAW$13123$20091011123501$20091012123501
	 * 
	 * @param operatingAirlineCode
	 * @param segmentCode
	 * @param segmentId
	 * @param dDate
	 * @param aDate
	 * @return
	 */
	private static String rphBuilder(String operatingAirlineCode, String segmentCode, Integer segmentId, Date dDate, Date aDate) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		String depatureDateTime = format.format(dDate);
		String arrivalDateTime = format.format(aDate);
		String fltRPH = operatingAirlineCode + DELIM + segmentCode + DELIM + segmentId + DELIM + depatureDateTime + DELIM
				+ arrivalDateTime;
		return fltRPH;
	}

	/**
	 * 
	 * @param flightRPH
	 * @return
	 */
	public static Date getDepartureDateFromFlightRPH(String flightRPH) {
		Date dDate = null;
		if (flightRPH != null) {
			String arr[] = flightRPH.split(SPLIT_DELIM);
			if (arr.length >= 5) {
				SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
				try {
					dDate = format.parse(arr[3]);
				} catch (ParseException e) {
					dDate = null;
				}
			}
		}
		return dDate;
	}

	/**
	 * 
	 * @param flightRPH
	 * @return
	 */
	public static Date getDepartureDateFromFlightRPHString(String flightRPH) {
		Date dDate = null;
		GregorianCalendar calendar = new GregorianCalendar();
		if (flightRPH != null) {
			String arr[] = flightRPH.split(SPLIT_DELIM);
			String dateString = arr[3];

			String year = dateString.substring(0, 4);
			String month = dateString.substring(4, 6);
			String date = dateString.substring(6, 8);
			String hour = dateString.substring(8, 10);
			String min = dateString.substring(10, 12);
			String sec = dateString.substring(12, dateString.length());

			calendar.set(Calendar.YEAR, Integer.valueOf(year));
			calendar.set(Calendar.MONTH, Integer.valueOf(month) - 1);
			calendar.set(Calendar.DATE, Integer.valueOf(date));
			calendar.set(Calendar.HOUR, Integer.valueOf(hour));
			calendar.set(Calendar.MINUTE, Integer.valueOf(min));
			calendar.set(Calendar.SECOND, Integer.valueOf(sec));

			dDate = calendar.getTime();
		}
		return dDate;
	}

	/**
	 * Given the interline compatible flight RPH of format G9$SHJ/CMB$205921$20100622215500$20100623043500 return the
	 * flight segment no (205921)
	 * 
	 * @return
	 */
	public static Integer getSegmentIdFromFlightRPH(String flightRPH) {
		Integer intVal = null;
		if (flightRPH == null)
			return intVal;
		String arr[] = flightRPH.split(SPLIT_DELIM);
		if (arr.length >= 5 && !arr[2].trim().equals("")) {
			intVal = new Integer(arr[2]);
		} else if (arr.length == 1 && !flightRPH.trim().equals("")) {
			intVal = new Integer(flightRPH); // FIXME Invalid Flight Reference No
		} else {
			log.warn("WARNING CANNOT GET FLTSEGID FROM FLIGHT RPH " + flightRPH);
		}
		return intVal;
	}

	/**
	 * Given the interline compatible Pnr Seg RPH of format G9$SHJ/CMB$2005921$20100622215500$20100623043500 return the
	 * pnr segment no (2005921)
	 * 
	 * @return
	 */
	public static Integer getPnrSegIdFromPnrSegRPH(String pnrSegRPH) {
		Integer intVal = null;
		if (pnrSegRPH == null)
			return intVal;
		String arr[] = pnrSegRPH.split(SPLIT_DELIM);
		if (arr.length >= 3) {
			intVal = new Integer(arr[2]);
		} else if (arr.length == 1 && !pnrSegRPH.trim().equals("")) {
			intVal = new Integer(arr[0]);
		} else {
			log.warn("WARNING CANNOT GET FLTSEGID FROM FLIGHT RPH " + pnrSegRPH);
		}
		return intVal;
	}

	/**
	 * Given the interline compatible flight RPH of format G9$SHJ/CMB$205921$20100622215500$20100623043500 return the
	 * flight segment code (SHJ/CMB)
	 * 
	 * @return
	 */
	public static String getSegmentCodeFromFlightRPH(String flightRPH) {
		String segCode = null;
		if (flightRPH == null)
			return segCode;
		String arr[] = flightRPH.split(SPLIT_DELIM);
		if (arr.length > 1) {
			segCode = arr[1];
		}
		return segCode;
	}

	/**
	 * Given the interline compatible flight RPH of format G9$SHJ/CMB$205921$20100622215500$20100623043500 return the
	 * operating airline code (G9)
	 * 
	 * @return
	 */
	public static String getOperatingAirline(String flightRPH) {
		String arr[] = flightRPH.split(SPLIT_DELIM);
		String airlineCode = null;
		if (arr.length > 0) {
			airlineCode = arr[0];
		}
		return airlineCode;
	}

	public static FlightSegmentTO getFlightSegmentFromRPH(String flightRPH) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

		flightSegmentTO.setSegmentCode(getSegmentCodeFromFlightRPH(flightRPH));
		flightSegmentTO.setDepartureDateTime(getDepartureDateFromFlightRPH(flightRPH));
		flightSegmentTO.setFlightSegId(getSegmentIdFromFlightRPH(flightRPH));
		flightSegmentTO.setOperatingAirline(getOperatingAirline(flightRPH));

		flightSegmentTO.setFlightRefNumber(flightRPH);
		return flightSegmentTO;
	}

	public static String composePnrSegRPHFromFlightRPH(String flightRPH, String pnrSegId) {
		String pnrSegRph = flightRPH;
		Integer fltSegId = getSegmentIdFromFlightRPH(flightRPH);
		if (fltSegId != null) {
			pnrSegRph = pnrSegRph.replaceFirst(fltSegId.toString(), pnrSegId);
		}

		return pnrSegRph;
	}

	public static String filterFlightRPH(String flightRPH, String delimeter) {

		if (delimeter == null) {
			delimeter = "#";
		}

		if (flightRPH == null) {
			flightRPH = "";
		} else {
			flightRPH = flightRPH.split(delimeter)[0];
		}

		return flightRPH;
	}

}
