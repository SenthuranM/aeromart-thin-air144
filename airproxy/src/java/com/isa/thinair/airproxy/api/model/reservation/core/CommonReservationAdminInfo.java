/*
 * =============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

/**
 * To keep track of Reservation Administrator information
 * 
 * @author Nilindra Fernando
 */
public class CommonReservationAdminInfo implements Serializable {

	private static final long serialVersionUID = 5250421715117476347L;
	private String originAgentCode;
	private String originChannelId;
	private String originSalesTerminal;
	private String ownerAgentCode;
	private String ownerChannelId;
	private String lastSalesTerminal;
	private String originIpAddress;

	/**
	 * This will be the country code of the originAgent for XBE and the country determined by originIPAddress for IBE.
	 */
	private String originCountryCode;

	private LCCClientReservationOwnerAgentContactInfo ownerAgentContactInfo;

	/**
	 * @return the originAgentCode
	 */
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	/**
	 * @param originAgentCode
	 *            the originAgentCode to set
	 */
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	/**
	 * @return the originChannelId
	 */
	public String getOriginChannelId() {
		return originChannelId;
	}

	/**
	 * @param originChannelId
	 *            the originChannelId to set
	 */
	public void setOriginChannelId(String originChannelId) {
		this.originChannelId = originChannelId;
	}

	/**
	 * @return the originSalesTerminal
	 */
	public String getOriginSalesTerminal() {
		return originSalesTerminal;
	}

	/**
	 * @param originSalesTerminal
	 *            the originSalesTerminal to set
	 */
	public void setOriginSalesTerminal(String originSalesTerminal) {
		this.originSalesTerminal = originSalesTerminal;
	}

	/**
	 * @return the ownerAgentCode
	 */
	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	/**
	 * @param ownerAgentCode
	 *            the ownerAgentCode to set
	 */
	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	/**
	 * @return the ownerChannelId
	 */
	public String getOwnerChannelId() {
		return ownerChannelId;
	}

	/**
	 * @param ownerChannelId
	 *            the ownerChannelId to set
	 */
	public void setOwnerChannelId(String ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	/**
	 * @return the lastSalesTerminal
	 */
	public String getLastSalesTerminal() {
		return lastSalesTerminal;
	}

	/**
	 * @param lastSalesTerminal
	 *            the lastSalesTerminal to set
	 */
	public void setLastSalesTerminal(String lastSalesTerminal) {
		this.lastSalesTerminal = lastSalesTerminal;
	}

	/**
	 * @return the ownerAgentContactInfo
	 */
	public LCCClientReservationOwnerAgentContactInfo getOwnerAgentContactInfo() {
		return ownerAgentContactInfo;
	}

	/**
	 * @param ownerAgentContactInfo
	 *            the ownerAgentContactInfo to set
	 */
	public void setOwnerAgentContactInfo(LCCClientReservationOwnerAgentContactInfo ownerAgentContactInfo) {
		this.ownerAgentContactInfo = ownerAgentContactInfo;
	}

	/**
	 * @return the originIpAddress
	 */
	public String getOriginIpAddress() {
		return originIpAddress;
	}

	/**
	 * @param originIpAddress
	 *            the originIpAddress to set
	 */
	public void setOriginIpAddress(String originIpAddress) {
		this.originIpAddress = originIpAddress;
	}

	/**
	 * This will be the country code of the originAgent for XBE and the country determined by originIPAddress for IBE.
	 * 
	 * @return the originCountryCode
	 */
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	/**
	 * @param originCountryCode
	 *            the originCountryCode to set
	 */
	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
}