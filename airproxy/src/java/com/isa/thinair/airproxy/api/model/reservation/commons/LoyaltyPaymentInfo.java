package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class LoyaltyPaymentInfo implements Serializable {
	private static final long serialVersionUID = 3830250318206752362L;

	private BigDecimal totalPayment;

	private Map<Integer, Map<String, BigDecimal>> paxProductPaymentBreakdown;

	String[] loyaltyRewardIds;

	private String memberAccountId;

	public BigDecimal getTotalPayment() {
		return totalPayment;
	}

	public void setTotalPayment(BigDecimal totalPayment) {
		this.totalPayment = totalPayment;
	}

	public Map<Integer, Map<String, BigDecimal>> getPaxProductPaymentBreakdown() {
		return paxProductPaymentBreakdown;
	}

	public void setPaxProductPaymentBreakdown(Map<Integer, Map<String, BigDecimal>> paxProductPaymentBreakdown) {
		this.paxProductPaymentBreakdown = paxProductPaymentBreakdown;
	}

	public String[] getLoyaltyRewardIds() {
		return loyaltyRewardIds;
	}

	public void setLoyaltyRewardIds(String[] loyaltyRewardIds) {
		this.loyaltyRewardIds = loyaltyRewardIds;
	}

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

}
