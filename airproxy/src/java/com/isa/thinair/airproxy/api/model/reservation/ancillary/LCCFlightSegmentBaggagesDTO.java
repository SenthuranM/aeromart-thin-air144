/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * @author mano
 * 
 */
public class LCCFlightSegmentBaggagesDTO implements Serializable, Comparable<LCCFlightSegmentBaggagesDTO> {

	private static final long serialVersionUID = -5423421959528605658L;

	private FlightSegmentTO flightSegmentTO;
	private List<LCCBaggageDTO> baggages;

	private boolean isAnciOffer;
	private String offerName;
	private String offerDescription;

	/** Anci Offer's Seat template id used to customize the seat charges */
	private Integer offeredTemplateID;

	/**
	 * @return the flightSegmentTO
	 */
	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	/**
	 * @param flightSegmentTO
	 *            the flightSegmentTO to set
	 */
	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	/**
	 * @return the baggages
	 */
	public List<LCCBaggageDTO> getBaggages() {

		if (baggages == null) {
			baggages = new ArrayList<LCCBaggageDTO>();
		}
		return baggages;
	}

	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferDescription() {
		return offerDescription;
	}

	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	/**
	 * @return the offeredTemplateID
	 */
	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	/**
	 * @param offeredTemplateID
	 *            the offeredTemplateID to set
	 */
	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}

	@Override
	public int compareTo(LCCFlightSegmentBaggagesDTO o) {
		return this.getFlightSegmentTO().getDepartureDateTime().compareTo(o.getFlightSegmentTO().getDepartureDateTime());
	}

}
