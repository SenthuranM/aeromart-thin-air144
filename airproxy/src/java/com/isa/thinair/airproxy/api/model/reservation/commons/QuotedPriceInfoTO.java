package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

 
/**
 * 
 * @author nafly
 * 
 */
public class QuotedPriceInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String bookingType;

	private BigDecimal totalTicketPrice;

	private BigDecimal totalBaseFare;

	private boolean isOnholdAllowed;

	private boolean isReturnQuote;

	private List<FlightSegmentTO> quotedSegments = new ArrayList<FlightSegmentTO>();

	// <PaxType,PerPaxChargesTO>
	private Map<String, PerPaxChargesTO> perPaxCharges = new HashMap<String, PerPaxChargesTO>();

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	/**
	 * @return the totalTicketPrice
	 */
	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

	/**
	 * @param totalTicketPrice
	 *            the totalTicketPrice to set
	 */
	public void setTotalTicketPrice(BigDecimal totalTicketPrice) {
		this.totalTicketPrice = totalTicketPrice;
	}

	/**
	 * @return the totalBaseFare
	 */
	public BigDecimal getTotalBaseFare() {
		return totalBaseFare;
	}

	/**
	 * @param totalBaseFare
	 *            the totalBaseFare to set
	 */
	public void setTotalBaseFare(BigDecimal totalBaseFare) {
		this.totalBaseFare = totalBaseFare;
	}

	/**
	 * @return the isOnholdAllowed
	 */
	public boolean isOnholdAllowed() {
		return isOnholdAllowed;
	}

	/**
	 * @param isOnholdAllowed
	 *            the isOnholdAllowed to set
	 */
	public void setOnholdAllowed(boolean isOnholdAllowed) {
		this.isOnholdAllowed = isOnholdAllowed;
	}

	/**
	 * @return the perPaxCharges
	 */
	public Map<String, PerPaxChargesTO> getPerPaxCharges() {
		return perPaxCharges;
	}

	/**
	 * @param perPaxCharges
	 *            the perPaxCharges to set
	 */
	public void setPerPaxCharges(Map<String, PerPaxChargesTO> perPaxCharges) {
		this.perPaxCharges = perPaxCharges;
	}

	/**
	 * @return the isReturnQuote
	 */
	public boolean isReturnQuote() {
		return isReturnQuote;
	}

	/**
	 * @param isReturnQuote
	 *            the isReturnQuote to set
	 */
	public void setReturnQuote(boolean isReturnQuote) {
		this.isReturnQuote = isReturnQuote;
	}

	/**
	 * @return the quotedSegments
	 */
	public List<FlightSegmentTO> getQuotedSegments() {
		return quotedSegments;
	}

	/**
	 * @param quotedSegments
	 *            the quotedSegments to set
	 */
	public void setQuotedSegments(List<FlightSegmentTO> quotedSegments) {
		this.quotedSegments = quotedSegments;
	}

	/**
	 * 
	 * @param paxType
	 * @return
	 */
	public PerPaxChargesTO getPerPaxChargeByPaxType(String paxType) {
		Map<String, PerPaxChargesTO> perPaxCharges = getPerPaxCharges();

		if (perPaxCharges != null && !perPaxCharges.isEmpty()) {
			return perPaxCharges.get(paxType);
		}

		return null;

	}

 
}
