package com.isa.thinair.airproxy.api.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientChargeAdustment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.ServiceResponce;

public interface AirproxyReservationBD {
	public static final String SERVICE_NAME = "AirproxyReservationService";

	public Collection<ReservationListTO> searchReservations(ReservationSearchDTO reservationSearchDTO,
			UserPrincipal userPrincipal, BasicTrackInfo trackInfo) throws ModuleException, ParseException;

	public ServiceResponce makeReservation(CommonReservationAssembler lcclientReservationAssembler, TrackInfoDTO trackInfoDTO)
			throws Exception;

	public LCCClientReservation searchReservationByPNR(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfo) throws ModuleException;
	
	public ServiceResponce isPaymentReferenceAvailable(String externalReference,String status,char productType) throws ModuleException;

	public ServiceResponce isPaymentReferenceAvailable(String externalReference,char productType) throws ModuleException;
	
	public LCCClientReservation searchResByPNR(LCCClientPnrModesDTO pnrModesDTO, ModificationParamRQInfo paramRQInfo,
			TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation cancelReservation(LCCClientResAlterModesTO lccClientResAlterModesTO,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo, boolean isVoidOperaion, boolean isOnHoldRelease) throws ModuleException;

	public void modifyContactInfo(String pnr, String version, CommonReservationContactInfo lccClientReservationContactInfo,
			CommonReservationContactInfo oldLccClientReservationContactInfo, boolean isGroupPnr, String appIndicator,
			TrackInfoDTO trackInfo) throws ModuleException;

	public void modifyPassengerInfo(LCCClientReservation lccClientReservation, LCCClientReservation oldLccClientReservation,
			boolean isGroupPnr, ClientCommonInfoDTO clientInfoDTO, String appIndicator, boolean skipDuplicateNameChack,
			boolean applyNameChangeCharge, Collection<String> allowedOperations, TrackInfoDTO trackInfo) throws ModuleException;

	public ServiceResponce balancePayment(LCCClientBalancePayment lccClientBalancePayment, String version, boolean isGroupPnr,
			boolean enableFraudCheck, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo, boolean isGdsSale, boolean isInfantPaymentSeparated) throws ModuleException;
	
	public ServiceResponce balancePaymentWithTransaction(LCCClientBalancePayment lccClientBalancePayment, String version, boolean isGroupPnr,
			boolean enableFraudCheck, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation splitReservation(LCCClientReservation lccClientReservation, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public LCCClientReservation extendOnHold(String groupPNR, Date extendDateTimeZulu, String version, boolean isGroupPnr,
			ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation modifyWaitListedPriority(String groupPNR, Integer priority, Integer fltSegId, String version,
			boolean isGroupPnr, ClientCommonInfoDTO clientInfoDTO, TrackInfoDTO trackInfo) throws ModuleException;

	@Deprecated
	public LCCClientReservation adjustCharge(String groupPNr, String version, List<LCCClientChargeAdustment> chargeAdjustments,
			boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto, TrackInfoDTO trackInfo) throws ModuleException;

	public LCCClientReservation adjustGroupCharge(String groupPNr, String version,
			List<LCCClientChargeAdustment> chargeAdjustments, boolean isGroupPNR, ClientCommonInfoDTO clientInfoDto,
			TrackInfoDTO trackInfo, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS,
			Map<Integer, List<LCCClientExternalChgDTO>> handlingFeeByPax) throws ModuleException;

	public LCCClientReservation transferOwnership(String groupPNR, String transfereeAgent, String version, boolean isGroupPnr,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public List<UserNoteTO> searchReservationAudit(String pnr, String carrierCode, boolean isGroupPnr, String originChanel,
			BasicTrackInfo trackInfo) throws ModuleException;

	public List<UserNoteTO> searchUserNoteHistory(String pnr, String carrierCode, boolean isGroupPnr, String originChanel,
			BasicTrackInfo trackInfo, boolean isClassifyUN) throws ModuleException;
	
	public Map<Integer, CommonCreditCardPaymentInfo> makeTemporyPaymentEntry(String groupPNR,
			CommonReservationContactInfo reservationContactInfo, LCCClientPaymentAssembler paymentAssembler, boolean isCredit,
			SYSTEM system, String originatorCarrierCode, TrackInfoDTO trackInfoDTO, boolean isCsOcFlightAvailable) throws ModuleException;

	public ReservationBalanceTO getResAlterationBalanceSummary(LCCClientResAlterQueryModesTO resAlterQueryModesTO,
			TrackInfoDTO trackInfo) throws ModuleException;

	public ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public ServiceResponce requoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;
	
	public LCCClientReservation requoteModifySegmentsWithAutoRefundForIBE(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;
	
	public ServiceResponce requoteModifySegmentsWithAutoRefund(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public boolean updateAncillary(CommonAncillaryModifyAssembler anciAssembler, boolean enableFraudCheck,
			CommonReservationContactInfo contactInfo, boolean isInterlinePaymentAllowed,
			Map<String, Set<String>> flightSegWiseSelectedMeals, TrackInfoDTO trackInfo) throws ModuleException;

	public String getHistoryForPrint(String pnr, BasicTrackInfo trackInfo) throws ModuleException;

	public String getItineraryForPrint(LCCClientPnrModesDTO pnrModesDTO, CommonItineraryParamDTO commonItineraryParam,
			ReservationSearchDTO reservationSearchDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public void sendEmailItinerary(LCCClientPnrModesDTO pnrModesDTO, CommonItineraryParamDTO commonItineraryParam,
			TrackInfoDTO trackInfo) throws ModuleException;

	public boolean checkForDuplicates(DuplicateValidatorAssembler dva , UserPrincipal userPrincipal , BasicTrackInfo trackInfo) throws ModuleException;

	public List<LCCClientReservationInsurance> loadReservationInsuranceByPnr(String pnr, TrackInfoDTO trackInfo) throws ModuleException;

	public Map<String, Map<String, List<Integer>>> getBookingClassDataForOpenReturn(Collection<String> segmentIds, String pnr,
			TrackInfoDTO trackInfo) throws ModuleException;

	public List<LCCClientReservationInsurance> resellInsurance(SYSTEM system, String pnr, List<Integer> insReferencesToReSell) throws ModuleException;

	public String getRecieptPrint(RecieptLayoutModesDTO recieptLayoutModesDTO, Collection colSelectionPnrPaxIds,
			boolean isCheckIndividual, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void reconcileDummyCarrierReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTime) throws ModuleException;

	/**
	 * 
	 * @param carrierCodes
	 *            : The set of carrier codes for which the adjustment types are to be retrieved.
	 * @param trackInfo
	 *            : The {@link BasicTrackInfo} object for tracking purposes.
	 * @return : The carrier wise {@link ChargeAdjustmentTypeDTO} collection.
	 * 
	 * @throws ModuleException
	 */
	public Map<String, Collection<ChargeAdjustmentTypeDTO>> getCarrierWiseChargeAdjustmentTypes(Set<String> carrierCodes,
			BasicTrackInfo trackInfo) throws ModuleException;

	public ServiceResponce rollForwardOnholdBooking(String pnr, Collection<LCCClientReservationSegment> colsegs,
			Integer adultCount, Integer childCount, Integer infantCount, Map<String, RollForwardFlightRQ> rollForwardingFlights,
			SYSTEM searchSystem, TrackInfoDTO trackInfo, boolean isDuplicateNameSkip, boolean isOverbok) throws ModuleException;

	public BigDecimal calculateBalanceToPayAfterAutoCancellation(LCCClientResAlterQueryModesTO resAlterQueryModesTO,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public ServiceResponce updatePaxNames(Map<Integer, NameDTO> changedPaxNamesMap, String pnr, long version,
			String appIndicator, boolean skipDuplicateNameCheck, TrackInfoDTO trackInfo, boolean isGroupPNR, String reasonForAllowBlPax) throws ModuleException;

	// public ReservationBalanceTO getLCCRequoteBalanceSummary(LCCClientResRequoteQueryTO lccClientResRequoteQueryTO,
	// TrackInfoDTO trackInfo) throws ModuleException;

	public Map<Integer, Map<String, Map<String, BigDecimal>>> getPaxProduDueAmount(String pnr, boolean isGroupPnr,
			boolean isLoadLCCRes, double remainingLoyaltyPoints)
			throws ModuleException;

	public ServiceResponce calculateLoyaltyPointRedeemables(RedeemCalculateRQ redeemCalculateRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public ServiceResponce issueLoyaltyRewards(String pnr, String memberAccountId, Map<String, Double> productPoints, SYSTEM selectedSystem,
			AppIndicatorEnum appIndicator, String memberEnrollingCarrier, String memberExternalId) throws ModuleException;
	
	public ReservationDiscountDTO calculatePromotionDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfo) throws ModuleException;

	public Map<String, ServiceTaxQuoteForTicketingRevenueResTO> quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfo) throws ModuleException;

	public ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO, TrackInfoDTO trackInfo, UserPrincipal userPrincipal)
			throws ModuleException;
	
	public String generateNewPnr(List<FlightSegmentTO> flightSegments, BasicTrackInfo trackInfo) throws ModuleException;
	
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<LCCClientReservationPax> paxList,SYSTEM system,boolean getOnlyFirstElement,List<String> participatingOperatingCarriers, TrackInfoDTO trackInfoDTO)throws ModuleException;

	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId, TrackInfoDTO trackInfo, boolean isGroupPNR, String operatingCarrier) throws ModuleException;
	
	public boolean hasActionedByIBE(String pnr) throws ModuleException;
}
