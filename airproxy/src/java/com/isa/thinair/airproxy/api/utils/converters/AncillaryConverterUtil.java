package com.isa.thinair.airproxy.api.utils.converters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class AncillaryConverterUtil {
	public static LCCReservationBaggageSummaryTo toLccReservationBaggageSummaryTo(BaseAvailRS baseAvailRS) {
		LCCReservationBaggageSummaryTo baggageSummaryTo = new LCCReservationBaggageSummaryTo();

		Map<String, String> bookingClasses = new HashMap<String, String>();
		Map<String, String> classesOfService = new HashMap<String, String>();

		Map<Integer, Set<String>> ondSeqRPHMap = new HashMap<Integer, Set<String>>();
		List<FlightSegmentTO> flightSegmentTOs = baseAvailRS.getAllFlightSegments();
		for (FlightSegmentTO flightSegmentTo : flightSegmentTOs) {
			if (!ondSeqRPHMap.containsKey(flightSegmentTo.getOndSequence())) {
				ondSeqRPHMap.put(flightSegmentTo.getOndSequence(), new HashSet<String>());
			}
			ondSeqRPHMap.get(flightSegmentTo.getOndSequence()).add(flightSegmentTo.getFlightRefNumber());
			classesOfService.put(flightSegmentTo.getFlightRefNumber(), flightSegmentTo.getCabinClassCode());
		}

		if (baseAvailRS.getSelectedPriceFlightInfo() != null) {
			for (FareRuleDTO fareRuleDTO : baseAvailRS.getSelectedPriceFlightInfo().getFareTypeTO().getApplicableFareRules()) {
				bookingClasses.put(fareRuleDTO.getOrignNDest(), fareRuleDTO.getBookingClassCode());
//				Set<String> fltRefs = ondSeqRPHMap.get(fareRuleDTO.getOndSequence());
//				if (fltRefs != null && !fltRefs.isEmpty()) {
//					for (String fltRef : fltRefs) {
//						classesOfService.put(fltRef, fareRuleDTO.getCabinClassCode());
//					}
//				}
			}
		}

		baggageSummaryTo.setBookingClasses(bookingClasses);
		baggageSummaryTo.setClassesOfService(classesOfService);

		return baggageSummaryTo;
	}
}
