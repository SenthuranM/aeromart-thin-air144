package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

public class LCCClientReservationPaxNamesTranslation extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer pnrPaxNamesId;
	private String languageCode;
	private String firstNameOl;
	private String lastNameOl;
	private String titleOl;

	public Integer getPnrPaxNamesId() {
		return pnrPaxNamesId;
	}

	public void setPnrPaxNamesId(Integer pnrPaxNamesId) {
		this.pnrPaxNamesId = pnrPaxNamesId;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getFirstNameOl() {
		return firstNameOl;
	}

	public void setFirstNameOl(String firstNameOl) {
		this.firstNameOl = firstNameOl;
	}

	public String getLastNameOl() {
		return lastNameOl;
	}

	public void setLastNameOl(String lastNameOl) {
		this.lastNameOl = lastNameOl;
	}

	public String getTitleOl() {
		return titleOl;
	}

	public void setTitleOl(String title) {
		this.titleOl = title;
	}

}