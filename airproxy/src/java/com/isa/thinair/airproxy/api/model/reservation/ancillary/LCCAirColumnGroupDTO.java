package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCAirColumnGroupDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String columnGroupId;
	private List<LCCAirRowDTO> airRows;

	public String getColumnGroupId() {
		return columnGroupId;
	}

	public void setColumnGroupId(String columnGroupId) {
		this.columnGroupId = columnGroupId;
	}

	public List<LCCAirRowDTO> getAirRows() {
		if (airRows == null)
			airRows = new ArrayList<LCCAirRowDTO>();
		return airRows;
	}

	public void setAirRows(List<LCCAirRowDTO> airRows) {
		this.airRows = airRows;
	}
}
