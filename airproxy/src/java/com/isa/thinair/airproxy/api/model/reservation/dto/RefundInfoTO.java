package com.isa.thinair.airproxy.api.model.reservation.dto;

import java.io.Serializable;
import java.util.Date;

public class RefundInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date paymentdate;

	private String originalPaymentCarrier;

	private String originalPaymentLccUniqueTnxId;

	private String originalPaymentTnxId;

	private String carrierVisePayments;

	/**
	 * @param originalPaymentTnxId
	 * @param originalPaymentLccUniqueTnxId
	 * @param paymentdate
	 * @param carrierVisePayments
	 * @param paymentCarrier
	 *            TODO
	 */
	public RefundInfoTO(String originalPaymentTnxId, String originalPaymentLccUniqueTnxId, Date paymentdate,
			String carrierVisePayments, String originalPaymentCarrier) {
		super();
		this.originalPaymentTnxId = originalPaymentTnxId;
		this.originalPaymentLccUniqueTnxId = originalPaymentLccUniqueTnxId;
		this.paymentdate = paymentdate;
		this.carrierVisePayments = carrierVisePayments;
		this.originalPaymentCarrier = originalPaymentCarrier;
	}

	/**
	 * @return the paymentdate
	 */
	public Date getPaymentdate() {
		return paymentdate;
	}

	/**
	 * @param paymentdate
	 *            the paymentdate to set
	 */
	public void setPaymentdate(Date paymentdate) {
		this.paymentdate = paymentdate;
	}

	/**
	 * @return the originalPaymentCarrier
	 */
	public String getOriginalPaymentCarrier() {
		return originalPaymentCarrier;
	}

	/**
	 * @param originalPaymentCarrier
	 *            the originalPaymentCarrier to set
	 */
	public void setOriginalPaymentCarrier(String originalPaymentCarrier) {
		this.originalPaymentCarrier = originalPaymentCarrier;
	}

	/**
	 * @return the originalPaymentLccUniqueTnxId
	 */
	public String getOriginalPaymentLccUniqueTnxId() {
		return originalPaymentLccUniqueTnxId;
	}

	/**
	 * @param originalPaymentLccUniqueTnxId
	 *            the originalPaymentLccUniqueTnxId to set
	 */
	public void setOriginalPaymentLccUniqueTnxId(String originalPaymentLccUniqueTnxId) {
		this.originalPaymentLccUniqueTnxId = originalPaymentLccUniqueTnxId;
	}

	/**
	 * 
	 * @return the originalPaymentTnxId
	 */
	public String getOriginalPaymentTnxId() {
		return originalPaymentTnxId;
	}

	/**
	 * 
	 * @param originalPaymentTnxId
	 *            the originalPaymentTnxId to set
	 */
	public void setOriginalPaymentTnxId(String originalPaymentTnxId) {
		this.originalPaymentTnxId = originalPaymentTnxId;
	}

	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

}
