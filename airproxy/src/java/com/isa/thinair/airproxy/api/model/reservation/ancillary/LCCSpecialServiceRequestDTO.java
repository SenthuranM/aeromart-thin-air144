package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

public class LCCSpecialServiceRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ssrCode;
	private String ssrName;
	private String serviceQuantity;
	private String status;
	private String carrierCode;
	private BigDecimal charge;
	private String text;
	private String description;
	private int availableQty;
	private boolean alertAutoCancellation;
	private boolean userDefinedCharge;
	private boolean showSsrValue = true;

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrName() {
		return ssrName;
	}

	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	public String getServiceQuantity() {
		return serviceQuantity;
	}

	public void setServiceQuantity(String serviceQuantity) {
		this.serviceQuantity = serviceQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public BigDecimal getCharge() {
		return charge;
	}

	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(int availableQty) {
		this.availableQty = availableQty;
	}

	public boolean isAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCSpecialServiceRequestDTO [carrierCode=");
		builder.append(carrierCode);
		builder.append(", charge=");
		builder.append(charge);
		builder.append(", description=");
		builder.append(description);
		builder.append(", serviceQuantity=");
		builder.append(serviceQuantity);
		builder.append(", ssrCode=");
		builder.append(ssrCode);
		builder.append(", status=");
		builder.append(status);
		builder.append(", text=");
		builder.append(text);
		builder.append(", alertAutoCancellation=");
		builder.append(alertAutoCancellation);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LCCSpecialServiceRequestDTO) {

			LCCSpecialServiceRequestDTO ssrObj = (LCCSpecialServiceRequestDTO) obj;
			return this.ssrCode.equals(ssrObj.getSsrCode());
		}
		return false;
	}

	public boolean isUserDefinedCharge() {
		return userDefinedCharge;
	}

	public void setUserDefinedCharge(boolean userDefinedCharge) {
		this.userDefinedCharge = userDefinedCharge;
	}

	public boolean isShowSsrValue() {
		return showSsrValue;
	}

	public void setShowSsrValue(boolean showSsrValue) {
		this.showSsrValue = showSsrValue;
	}
}
