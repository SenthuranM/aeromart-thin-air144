/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * To keep track of Credit Card details which belongs to a <strong> perticular payment.<strong>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CommonCreditCardPaymentInfo implements Serializable, LCCClientPaymentInfo {

	private static final long serialVersionUID = 1L;

	/** Hold total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String totalAmountCurrencyCode;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	/** Holds credit card number */
	private String no;

	/** Holds the credit card number first digits */
	private String noFirstDigits;

	/** Holds the credit card number last digits */
	private String noLastDigits;

	/** Holds credit card name */
	private String name;

	/** Holds credit card expiry date (YYMM) format */
	private String eDate;

	/** Holds credit card security code */
	private String securityCode;

	/** Holds credit card address */
	private String address;

	/** Holds credit card type */
	private int type;

	/** Holds the application indicator */
	private AppIndicatorEnum appIndicator;

	/** Holds the transaction mode enum */
	private TnxModeEnum tnxMode;

	/** Holds the payment gateway identification parameters */
	private IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	private Integer paxSequence;

	private Date txnDateTime;

	private String carrierCode;

	private String groupPNR;

	private Integer temporyPaymentId;

	private Integer paymentBrokerId;

	private boolean isPaymentSuccess;

	private String lccUniqueTnxId;

	private String authorizationId;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private String cardHoldersName;

	private boolean includeExternalCharges;

	private String payReference;

	private Integer actualPaymentMethod;

	private String originalPayReference;

	private Integer oldCCRecordId;

	private String recieptNumber;

	private String carrierVisePayments;

	private UserInputDTO userInputDTO;

	private Integer paymentBrokerRefNo;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private boolean offlinePayment = false;
	
	private boolean nonRefundable = false;
	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the no
	 */
	public String getNo() {
		return no;
	}

	/**
	 * @param no
	 *            the no to set
	 */
	public void setNo(String no) {
		this.no = no;
		this.setNoFirstDigits(BeanUtils.getFirst6Digits(no));
		this.setNoLastDigits(BeanUtils.getLast4Digits(no));
	}

	/**
	 * @return the noFirstDigits
	 */
	public String getNoFirstDigits() {
		return noFirstDigits;
	}

	/**
	 * @param noFirstDigits
	 *            the noFirstDigits to set
	 */
	public void setNoFirstDigits(String noFirstDigits) {
		this.noFirstDigits = noFirstDigits;
	}

	/**
	 * @return the noLastDigits
	 */
	public String getNoLastDigits() {
		return noLastDigits;
	}

	/**
	 * @param noLastDigits
	 *            the noLastDigits to set
	 */
	public void setNoLastDigits(String noLastDigits) {
		this.noLastDigits = noLastDigits;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the eDate
	 */
	public String geteDate() {
		return eDate;
	}

	/**
	 * @param eDate
	 *            the eDate to set
	 */
	public void seteDate(String eDate) {
		this.eDate = eDate;
	}

	/**
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * @param securityCode
	 *            the securityCode to set
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the appIndicator
	 */
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 *            the appIndicator to set
	 */
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return the tnxMode
	 */
	public TnxModeEnum getTnxMode() {
		return tnxMode;
	}

	/**
	 * @param tnxMode
	 *            the tnxMode to set
	 */
	public void setTnxMode(TnxModeEnum tnxMode) {
		this.tnxMode = tnxMode;
	}

	/**
	 * @return the ipgIdentificationParamsDTO
	 */
	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param ipgIdentificationParamsDTO
	 *            the ipgIdentificationParamsDTO to set
	 */
	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * Compare two LCCClientCreditCardPaymentInfo objects
	 */
	public static boolean compare(CommonCreditCardPaymentInfo lccclientCreditCardPaymentInfo1,
			CommonCreditCardPaymentInfo lccclientCreditCardPaymentInfo2) {
		String strNoObj1 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo1.getNo());
		String strNameObj1 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo1.getName());
		String strEDateObj1 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo1.geteDate());

		String strNoObj2 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo2.getNo());
		String strNameObj2 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo2.getName());
		String strEDateObj2 = BeanUtils.nullHandler(lccclientCreditCardPaymentInfo2.geteDate());

		if (strNoObj1.equals(strNoObj2) && strNameObj1.equals(strNameObj2) && strEDateObj1.equals(strEDateObj2)) {
			return true;
		} else {
			return false;
		}
	}

	public CreditCardPayment transform(String groupPNR) {
		CreditCardPayment creditCardPayment = new CreditCardPayment();

		creditCardPayment.setCardholderName(this.getName());
		creditCardPayment.setCardNumber(this.getNo());
		creditCardPayment.setExpiryDate(this.geteDate());

		PayCurrencyDTO payCurrencyDTO = this.getPayCurrencyDTO();
		creditCardPayment.setAmount(payCurrencyDTO.getTotalPayCurrencyAmount().toString());
		creditCardPayment.setCurrency(payCurrencyDTO.getPayCurrencyCode());

		int noOfDecimalPoints = BeanUtils.countNoOfDecimals(payCurrencyDTO.getBoundaryValue());
		if (noOfDecimalPoints > 0) {
			creditCardPayment.setNoOfDecimalPoints(noOfDecimalPoints);
		}

		creditCardPayment.setCvvField(this.getSecurityCode());
		creditCardPayment.setPnr(groupPNR);
		creditCardPayment.setAppIndicator(this.getAppIndicator());
		creditCardPayment.setTnxMode(this.getTnxMode());
		creditCardPayment.setTemporyPaymentId(this.getTemporyPaymentId());
		creditCardPayment.setPaymentBrokerRefNo(this.getPaymentBrokerId() == null ? 0 : this.getPaymentBrokerId());
		creditCardPayment.setIpgIdentificationParamsDTO(this.getIpgIdentificationParamsDTO());
		creditCardPayment.setCardType(this.getType());
		return creditCardPayment;
	}

	public PreviousCreditCardPayment transformForPreviousCreditCardPayment(String groupPNR) {
		PreviousCreditCardPayment previousCCPayment = new PreviousCreditCardPayment();

		previousCCPayment.setNoFirstDigits(this.getNoFirstDigits());
		previousCCPayment.setNoLastDigits(this.getNoLastDigits());
		previousCCPayment.setName(this.getName());
		previousCCPayment.setEDate(this.geteDate());
		previousCCPayment.setSecurityCode(this.getSecurityCode());
		previousCCPayment.setPnr(groupPNR);
		previousCCPayment.setPaymentBrokerRefNo(this.getPaymentBrokerId());
		previousCCPayment.setTemporyPaymentId(this.getTemporyPaymentId());
		previousCCPayment.setTotalAmount(this.getTotalAmount());

		return previousCCPayment;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	@Override
	public CommonCreditCardPaymentInfo clone() {
		CommonCreditCardPaymentInfo lccClientCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();
		lccClientCreditCardPaymentInfo.setTotalAmount(this.getTotalAmount());
		lccClientCreditCardPaymentInfo.setTotalAmountCurrencyCode(this.getTotalAmountCurrencyCode());
		lccClientCreditCardPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) this.getPayCurrencyDTO().clone());
		lccClientCreditCardPaymentInfo.setNo(this.getNo());
		lccClientCreditCardPaymentInfo.setName(this.getName());
		lccClientCreditCardPaymentInfo.seteDate(this.geteDate());
		lccClientCreditCardPaymentInfo.setSecurityCode(this.getSecurityCode());
		lccClientCreditCardPaymentInfo.setAddress(this.getAddress());
		lccClientCreditCardPaymentInfo.setType(this.getType());
		lccClientCreditCardPaymentInfo.setAppIndicator(this.getAppIndicator());
		lccClientCreditCardPaymentInfo.setTnxMode(this.getTnxMode());
		lccClientCreditCardPaymentInfo.setIpgIdentificationParamsDTO(this.getIpgIdentificationParamsDTO());
		lccClientCreditCardPaymentInfo.setTemporyPaymentId(this.getTemporyPaymentId());
		lccClientCreditCardPaymentInfo.setPaxSequence(this.getPaxSequence());
		lccClientCreditCardPaymentInfo.setGroupPNR(this.getGroupPNR());
		lccClientCreditCardPaymentInfo.setPaymentBrokerId(this.getPaymentBrokerId());
		lccClientCreditCardPaymentInfo.setLccUniqueTnxId(this.getLccUniqueTnxId());
		lccClientCreditCardPaymentInfo.setPaymentSuccess(this.isPaymentSuccess());
		lccClientCreditCardPaymentInfo.setCardHoldersName(this.getCardHoldersName());
		lccClientCreditCardPaymentInfo.setPayCurrencyAmount(this.getPayCurrencyAmount());
		lccClientCreditCardPaymentInfo.setPaymentBrokerRefNo(this.getPaymentBrokerRefNo());
		lccClientCreditCardPaymentInfo.setPaymentTnxReference(this.getPaymentTnxReference());
		lccClientCreditCardPaymentInfo.setOriginalPaymentUID(this.getOriginalPaymentUID());
		lccClientCreditCardPaymentInfo.setPaymentTnxDateTime(this.getPaymentTnxDateTime());

		return lccClientCreditCardPaymentInfo;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public Date getTxnDateTime() {
		return this.txnDateTime;
	}

	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the totalAmountCurrencyCode
	 */
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	/**
	 * @param totalAmountCurrencyCode
	 *            the totalAmountCurrencyCode to set
	 */
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the paymentBrokerId
	 */
	public Integer getPaymentBrokerId() {
		return paymentBrokerId;
	}

	/**
	 * @param paymentBrokerId
	 *            the paymentBrokerId to set
	 */
	public void setPaymentBrokerId(Integer paymentBrokerId) {
		this.paymentBrokerId = paymentBrokerId;
	}

	/**
	 * @return the temporyPaymentId
	 */
	public Integer getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            the temporyPaymentId to set
	 */
	public void setTemporyPaymentId(Integer temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return the isPaymentSuccess
	 */
	public boolean isPaymentSuccess() {
		return isPaymentSuccess;
	}

	/**
	 * @param isPaymentSuccess
	 *            the isPaymentSuccess to set
	 */
	public void setPaymentSuccess(boolean isPaymentSuccess) {
		this.isPaymentSuccess = isPaymentSuccess;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	/**
	 * @return the authorizationId
	 */
	public String getAuthorizationId() {
		return authorizationId;
	}

	/**
	 * @param authorizationId
	 *            the authorizationId to set
	 */
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientCreditCardPaymentInfo [address=");
		builder.append(address);
		builder.append(", appIndicator=");
		builder.append(appIndicator);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", eDate=");
		builder.append(eDate);
		builder.append(", groupPNR=");
		builder.append(groupPNR);
		builder.append(", ipgIdentificationParamsDTO=");
		builder.append(ipgIdentificationParamsDTO);
		builder.append(", isPaymentSuccess=");
		builder.append(isPaymentSuccess);
		builder.append(", lccUniqueTnxId=");
		builder.append(lccUniqueTnxId);
		builder.append(", name=");
		builder.append(name);
		builder.append(", no=");
		builder.append(no);
		builder.append(", noFirstDigits=");
		builder.append(noFirstDigits);
		builder.append(", noLastDigits=");
		builder.append(noLastDigits);
		builder.append(", paxSequence=");
		builder.append(paxSequence);
		builder.append(", payCurrencyDTO=");
		builder.append(payCurrencyDTO);
		builder.append(", paymentBrokerId=");
		builder.append(paymentBrokerId);
		builder.append(", securityCode=");
		builder.append(securityCode);
		builder.append(", temporyPaymentId=");
		builder.append(temporyPaymentId);
		builder.append(", tnxMode=");
		builder.append(tnxMode);
		builder.append(", totalAmount=");
		builder.append(totalAmount);
		builder.append(", totalAmountCurrencyCode=");
		builder.append(totalAmountCurrencyCode);
		builder.append(", txnDateTime=");
		builder.append(txnDateTime);
		builder.append(", type=");
		builder.append(type);
		builder.append(", paymentTnxReference=");
		builder.append(paymentTnxReference);
		builder.append(", originalPaymentUID=");
		builder.append(originalPaymentUID);
		builder.append(", paymentTnxDateTime=");
		builder.append(paymentTnxDateTime);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;

	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;

	}

	public String getCardHoldersName() {
		return cardHoldersName;
	}

	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	public String getPayReference() {
		return payReference;
	}

	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	public Integer getActualPaymentMethod() {

		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {

		this.actualPaymentMethod = actualPaymentMethod;
	}

	public String getOriginalPayReference() {
		return originalPayReference;
	}

	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	public Integer getOldCCRecordId() {
		return oldCCRecordId;
	}

	public void setOldCCRecordId(Integer oldCCRecordId) {
		this.oldCCRecordId = oldCCRecordId;
	}

	public String getRecieptNumber() {
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	/**
	 * @return the carrierVisePayments
	 */
	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	/**
	 * @param carrierVisePayments
	 *            the carrierVisePayments to set
	 */
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

	public Integer getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setPaymentBrokerRefNo(Integer paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	public String getOriginalPaymentUID() {
		return originalPaymentUID;
	}

	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	public boolean isOfflinePayment() {
		return this.offlinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	@Override
	public boolean isNonRefundable() {
		return this.nonRefundable;
	}
	
	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;		
	}

}