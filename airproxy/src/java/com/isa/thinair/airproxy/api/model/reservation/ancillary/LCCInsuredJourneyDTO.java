package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

public class LCCInsuredJourneyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date journeyStartDate;
	private Date journeyEndDate;
	private String journeyStartAirportCode;
	private String journeyEndAirportCode;
	private boolean roundTrip;
	private int salesChannel = 3;

	private String startDateOffset;

	private String endDateOffset;

	private String selectedLanguage = Locale.ENGLISH.getLanguage();

	public Date getJourneyStartDate() {
		return journeyStartDate;
	}

	public void setJourneyStartDate(Date journeyStartDate) {
		this.journeyStartDate = journeyStartDate;
	}

	public Date getJourneyEndDate() {
		return journeyEndDate;
	}

	public void setJourneyEndDate(Date journeyEndDate) {
		this.journeyEndDate = journeyEndDate;
	}

	public String getJourneyStartAirportCode() {
		return journeyStartAirportCode;
	}

	public void setJourneyStartAirportCode(String journeyStartAirportCode) {
		this.journeyStartAirportCode = journeyStartAirportCode;
	}

	public String getJourneyEndAirportCode() {
		return journeyEndAirportCode;
	}

	public void setJourneyEndAirportCode(String journeyEndAirportCode) {
		this.journeyEndAirportCode = journeyEndAirportCode;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	public String getJourneyStartDateOffset() {
		return startDateOffset;
	}

	public void setJourneyStartDateOffset(String startDateOffset) {
		this.startDateOffset = startDateOffset;
	}

	public String getJourneyEndDateOffset() {
		return endDateOffset;
	}

	public void setJourneyEndDateOffset(String endDateOffset) {
		this.endDateOffset = endDateOffset;
	}

	public int getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(int salesChannel) {
		this.salesChannel = salesChannel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCInsuredJourneyDTO [journeyEndAirportCode=");
		builder.append(journeyEndAirportCode);
		builder.append(", journeyEndDate=");
		builder.append(journeyEndDate);
		builder.append(", journeyEndDateOffset=");
		builder.append(endDateOffset);
		builder.append(", journeyStartAirportCode=");
		builder.append(journeyStartAirportCode);
		builder.append(", journeyStartDate=");
		builder.append(journeyStartDate);
		builder.append(", journeyStartDateOffset=");
		builder.append(startDateOffset);
		builder.append(", roundTrip=");
		builder.append(roundTrip);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the selectedLanguage
	 */
	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	/**
	 * @param selectedLanguage
	 *            the selectedLanguage to set
	 */
	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

}
