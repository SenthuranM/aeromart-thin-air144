package com.isa.thinair.airproxy.api.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AncillaryCommonUtil {

	private static Log log = LogFactory.getLog(AncillaryCommonUtil.class);

	public static boolean isMealAvailable(Map<Integer, List<FlightMealDTO>> meals, Map<Integer, Set<String>> selectedMeals) {
		if (meals != null && !meals.isEmpty()) {
			for (Entry<Integer, List<FlightMealDTO>> entry : meals.entrySet()) {
				List<FlightMealDTO> flightMealDTOs = entry.getValue();
				Set<String> selMealCodes = selectedMeals.get(entry.getKey());
				selMealCodes = selMealCodes == null ? new HashSet<String>() : selMealCodes;
				if (flightMealDTOs.size() > 0) {
					for (FlightMealDTO flightMealDTO : flightMealDTOs) {
						if ((flightMealDTO.getAllocatedMeal() - flightMealDTO.getSoldMeals() > 0)
								|| selMealCodes.contains(flightMealDTO.getMealCode())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static boolean isSeatAvailable(Map<Integer, FlightSeatsDTO> seatMap) {
		for (Entry<Integer, FlightSeatsDTO> seatMapEntry : seatMap.entrySet()) {
			FlightSeatsDTO flightSeatsDTO = seatMapEntry.getValue();

			for (Iterator<SeatDTO> iterator = flightSeatsDTO.getSeats().iterator(); iterator.hasNext();) {
				SeatDTO seatDTO = iterator.next();

				if (seatDTO.getStatus().equals("VAC")) {
					return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static Map<Integer, FlightSeatsDTO> rearrangeSeatRows(Map<Integer, FlightSeatsDTO> seatMap) {
		Map<Integer, FlightSeatsDTO> rearrangedSeatMap = new HashMap<Integer, FlightSeatsDTO>();

		for (Entry<Integer, FlightSeatsDTO> seatMapEntry : seatMap.entrySet()) {

			FlightSeatsDTO flightSeatsDTO = seatMapEntry.getValue();
			Integer segmentId = seatMapEntry.getKey();

			Collection<SeatDTO> seats = new ArrayList<SeatDTO>();
			for (Iterator<SeatDTO> iterator = flightSeatsDTO.getSeats().iterator(); iterator.hasNext();) {
				SeatDTO seatDTO = iterator.next();
				int rowId = seatDTO.getRowId();
				int colId = seatDTO.getColId();
				int rowGrpId = seatDTO.getRowGroupId();
				int colGrpId = seatDTO.getColGroupId();

				seatDTO.setColId(rowId);
				seatDTO.setRowId(colId);
				seatDTO.setColGroupId(rowGrpId);
				seatDTO.setRowGroupId(colGrpId);

				seats.add(seatDTO);
			}
			flightSeatsDTO.setSeats(seats);
			rearrangedSeatMap.put(segmentId, flightSeatsDTO);
		}
		return rearrangedSeatMap;
	}

	private static List<String> extractStringListFromSeatDtoList(Collection collection, String getMethodName) {
		List<String> stringList = new ArrayList<String>();

		try {
			for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
				SeatDTO seatDTO = (SeatDTO) iterator.next();

				Method methods[] = SeatDTO.class.getDeclaredMethods();
				for (int i = 0; i < methods.length; i++) {
					if (methods[i].getName().equals(getMethodName)) {
						Object[] arguments = null;
						String retobj = ((Object) methods[i].invoke(seatDTO, arguments)).toString();
						if (!stringList.contains(retobj)) {
							stringList.add(retobj);
						}
					}
				}
			}

		} catch (Exception e) {
			log.error(e);
		}
		return stringList;
	}

	private static List<String> getCabinClassCodeList(Collection<SeatDTO> seats) {
		return extractStringListFromSeatDtoList(seats, "getCabinClassCode");
	}

	public static List<String> getAllRowGroupIds(List<SeatDTO> cabinSeatDTOList) {
		return extractStringListFromSeatDtoList(cabinSeatDTOList, "getRowGroupId");
	}

	public static List<String> getAllColGroupIds(List<SeatDTO> seatDtoPerGroupId) {
		return extractStringListFromSeatDtoList(seatDtoPerGroupId, "getColGroupId");
	}

	public static List<String> getAllRowIds(List<SeatDTO> columnGroupSeats) {
		return extractStringListFromSeatDtoList(columnGroupSeats, "getRowId");
	}

	public static Map<String, List<SeatDTO>> getCabinClassSeatMap(Collection<SeatDTO> seats) {

		List<String> cabinClassList = getCabinClassCodeList(seats);
		Map<String, List<SeatDTO>> cabinClassSeatDTOMap = new LinkedHashMap<String, List<SeatDTO>>();

		for (String cabinClass : cabinClassList) {
			List<SeatDTO> seatDTOList = new ArrayList<SeatDTO>();
			for (SeatDTO seatDTO : seats) {
				if (cabinClass.equals(seatDTO.getCabinClassCode())) {
					seatDTOList.add(seatDTO);
				}
			}
			cabinClassSeatDTOMap.put(cabinClass, seatDTOList);
		}
		return cabinClassSeatDTOMap;
	}

	public static Map<String, List<SeatDTO>> getRowGroupSeatMap(List<SeatDTO> cabinClassSeatList) {
		List<String> rowGroupIdList = getAllRowGroupIds(cabinClassSeatList);
		Map<String, List<SeatDTO>> rowGroupSeatMap = new HashMap<String, List<SeatDTO>>();

		for (String rowGroupId : rowGroupIdList) {
			List<SeatDTO> seatDTOList = new ArrayList<SeatDTO>();

			for (SeatDTO seatDTO : cabinClassSeatList) {
				if (rowGroupId.equals(String.valueOf(seatDTO.getRowGroupId()))) {
					seatDTOList.add(seatDTO);
				}
			}
			rowGroupSeatMap.put(rowGroupId, seatDTOList);
		}
		return rowGroupSeatMap;
	}

	public static Map<String, List<SeatDTO>> getColumnGroupSeatMap(List<SeatDTO> rowGroupSeats) {
		List<String> colGroupIdList = getAllColGroupIds(rowGroupSeats);
		Map<String, List<SeatDTO>> colGroupSeatMap = new HashMap<String, List<SeatDTO>>();

		for (String colGroupId : colGroupIdList) {
			List<SeatDTO> seatDTOList = new ArrayList<SeatDTO>();

			for (SeatDTO seatDTO : rowGroupSeats) {
				if (colGroupId.equals(String.valueOf(seatDTO.getColGroupId()))) {
					seatDTOList.add(seatDTO);
				}
			}
			colGroupSeatMap.put(colGroupId, seatDTOList);
		}
		return colGroupSeatMap;
	}

	public static Map<String, List<SeatDTO>> getRowSeatMap(List<SeatDTO> columnGroupSeats) {
		List<String> rowIdList = getAllRowIds(columnGroupSeats);
		Map<String, List<SeatDTO>> rowSeatMap = new HashMap<String, List<SeatDTO>>();

		for (String rowId : rowIdList) {
			List<SeatDTO> seatDTOList = new ArrayList<SeatDTO>();

			for (SeatDTO seatDTO : columnGroupSeats) {
				if (rowId.equals(String.valueOf(seatDTO.getRowId()))) {
					seatDTOList.add(seatDTO);
				}
			}
			rowSeatMap.put(rowId, seatDTOList);
		}
		return rowSeatMap;
	}

	public static void handleBufferTimes(Map<Integer, List<FlightBaggageDTO>> baggageMap, List<FlightSegmentTO> flightSegments,
			boolean isModifyAncillary, boolean isAllowUntilFinalCutover) {

		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			boolean isStandBy = isStandby(flightSegmentTO);
			long millis = BaggageTimeWatcher.getBaggageCutOverTimeForXBEInMillis(isModifyAncillary, isAllowUntilFinalCutover,
					isStandBy);

			if (flightSegmentTO.getDepartureDateTimeZulu().getTime() - millis < new Date().getTime()) {
				if (baggageMap.containsKey(flightSegmentTO.getFlightSegId())) {
					baggageMap.get(flightSegmentTO.getFlightSegId()).clear();
				}
			}
		}
	}

	private static boolean isStandby(FlightSegmentTO flightSegmentTO) {
		boolean isStandBy = true;
		if (flightSegmentTO != null) {
			if (!BookingClass.BookingClassType.STANDBY.equals(flightSegmentTO.getBookingType())) {
				isStandBy = false;
			}
		}
		return isStandBy;
	}

	/**
	 * 
	 * @param baggageMap
	 * @return
	 */
	public static boolean isBaggageAvailable(Map<Integer, List<FlightBaggageDTO>> baggageMap) {

		if (baggageMap != null && !baggageMap.isEmpty()) {
			for (Collection<FlightBaggageDTO> flightBaggageDTOs : baggageMap.values()) {
				if (flightBaggageDTOs.size() > 0) {
					return true;
				}
			}
		}
		return false;

	}

	public static Map<AirportServiceKeyTO, FlightSegmentTO> populateAiportWiseFltSegments(List<FlightSegmentTO> flightSegmentTOs,
			Map<String, Integer> apServiceCount, boolean isAirportTransfer) throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, FlightSegmentTO>();

		List<FlightSegmentTO> outboundFlightSegmentTOs = getInOutFltSegments(flightSegmentTOs, false);
		List<FlightSegmentTO> inboundFlightSegmentTOs = getInOutFltSegments(flightSegmentTOs, true);

		// Get valid airport wise flight segment for out-bound segments
		if (outboundFlightSegmentTOs.size() > 0) {
			airportWiseMap.putAll(createAirportWiseSegForInOut(outboundFlightSegmentTOs, apServiceCount, isAirportTransfer));
		}

		// Get valid airport wise flight segment for in-bound segments
		if (inboundFlightSegmentTOs.size() > 0) {
			airportWiseMap.putAll(createAirportWiseSegForInOut(inboundFlightSegmentTOs, apServiceCount, isAirportTransfer));
		}

		return airportWiseMap;
	}

	private static List<FlightSegmentTO> getInOutFltSegments(List<FlightSegmentTO> flightSegmentTOs, boolean returnFlag)
			throws ModuleException {
		List<FlightSegmentTO> inOutFltSegList = new ArrayList<FlightSegmentTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (!AirproxyModuleUtils.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
				if (flightSegmentTO.isReturnFlag() == returnFlag) {
					inOutFltSegList.add(flightSegmentTO);
				}
			}
		}

		return inOutFltSegList;
	}

	private static Map<AirportServiceKeyTO, FlightSegmentTO> createAirportWiseSegForInOut(List<FlightSegmentTO> flightSegList,
			Map<String, Integer> serviceCounter, boolean isAirportTransfer) throws ModuleException {

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, FlightSegmentTO>();

		if (flightSegList.size() > 1) {
			FlightSegmentTO prevFltSeg = null;
			int count = 0;
			for (FlightSegmentTO flightSegmentTO : flightSegList) {
				count++;
				String[] airports = flightSegmentTO.getSegmentCode().split("/");
				if (prevFltSeg != null) {

					String[] prevAirports = prevFltSeg.getSegmentCode().split("/");

					if (!prevAirports[prevAirports.length - 1].equals(airports[0])) {

						airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
								flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
								flightSegmentTO);

						airportWiseMap.put(new AirportServiceKeyTO(prevAirports[prevAirports.length - 1],
								ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu().getTime(),
								prevFltSeg.getSegmentCode()), prevFltSeg);

					} else {
						HubInfo hubInfo = AirproxyModuleUtils.getLocationDB().getHubInfo(airports[0],
								prevFltSeg.getOperatingAirline(), flightSegmentTO.getOperatingAirline());

						if (hubInfo != null) {
							long transitTime = flightSegmentTO.getDepartureDateTimeZulu().getTime()
									- prevFltSeg.getArrivalDateTimeZulu().getTime();

							double transitTimeInMin = transitTime / (1000 * 60);

							// calculate minCnxTime in min
							String minCnxStr = hubInfo.getMinConnectionTime();
							String[] minCnxArr = minCnxStr.split(":");

							double minCnxTimeInMin = new Integer(minCnxArr[0]) * 60 + new Integer(minCnxArr[1]);

							// Calculate max cnx time in min
							String maxCnxStr = hubInfo.getMaxConnectionTime();
							String[] maxCnxArr = maxCnxStr.split(":");

							double maxCnxTimeInMin = new Integer(maxCnxArr[0]) * 60 + new Integer(maxCnxArr[1]);

							if (transitTimeInMin < minCnxTimeInMin) {
								// Don't consider the airport for airport services
							} else if (transitTimeInMin >= minCnxTimeInMin && transitTimeInMin <= maxCnxTimeInMin) {
								
								if (!isAirportTransfer) {

									if (isAirportServiceBelongsToSecondSegment(serviceCounter, prevFltSeg, airports[0])) {
										/*
										 * If first segment of the connection doesn't have existing airport services then
										 * airport services will bind to 2nd segment
										 */
	
										airportWiseMap.put(new AirportServiceKeyTO(airports[0],
												ReservationPaxSegmentSSR.APPLY_ON_TRANSIT, flightSegmentTO.getDepartureDateTimeZulu()
														.getTime(), flightSegmentTO.getSegmentCode()), flightSegmentTO);
	
									} else {
										/*
										 * If first segment of the connection already have airport services then airport
										 * services will bind to 1st segment
										 */
	
										airportWiseMap.put(new AirportServiceKeyTO(airports[0],
												ReservationPaxSegmentSSR.APPLY_ON_TRANSIT, prevFltSeg.getDepartureDateTimeZulu()
														.getTime(), prevFltSeg.getSegmentCode()), prevFltSeg);
	
									}
									
								} else {
									//Will have to change if there are multi-leg flights
									airportWiseMap.put(new AirportServiceKeyTO(prevAirports[0],
											ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE, prevFltSeg.getDepartureDateTimeZulu()
													.getTime(), prevFltSeg.getSegmentCode() + "/" + airports[1]), prevFltSeg);
									
									airportWiseMap.put(new AirportServiceKeyTO(airports[airports.length - 1],
											ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, flightSegmentTO.getDepartureDateTimeZulu()
													.getTime(), prevAirports[0] + "/" +flightSegmentTO.getSegmentCode()), flightSegmentTO);								
									
								}

							} else if (transitTimeInMin > maxCnxTimeInMin) {
								/*
								 * provide airport services separately for connection airport when transit time exceeds
								 * max connection time
								 */

								airportWiseMap.put(new AirportServiceKeyTO(airports[0],
										ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE, flightSegmentTO.getDepartureDateTimeZulu()
												.getTime(), flightSegmentTO.getSegmentCode()), flightSegmentTO);

								airportWiseMap.put(new AirportServiceKeyTO(airports[0],
										ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu()
												.getTime(), prevFltSeg.getSegmentCode()), prevFltSeg);

							}

						} else {

							airportWiseMap.put(new AirportServiceKeyTO(prevAirports[prevAirports.length - 1],
									ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu().getTime(),
									prevFltSeg.getSegmentCode()), prevFltSeg);

							airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
									flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
									flightSegmentTO);

						}
					}

				} else {

					airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
							flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
							flightSegmentTO);
				}

				if (count == flightSegList.size()) {

					airportWiseMap.put(new AirportServiceKeyTO(airports[airports.length - 1],
							ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, flightSegmentTO.getDepartureDateTimeZulu().getTime(),
							flightSegmentTO.getSegmentCode()), flightSegmentTO);

				}

				prevFltSeg = flightSegmentTO;
			}
		} else if (flightSegList.size() == 1) {
			FlightSegmentTO fltSeg = flightSegList.get(0);
			String[] airports = fltSeg.getSegmentCode().split("/");

			airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE, fltSeg
					.getDepartureDateTimeZulu().getTime(), fltSeg.getSegmentCode()), fltSeg);

			airportWiseMap.put(new AirportServiceKeyTO(airports[airports.length - 1], ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL,
					fltSeg.getDepartureDateTimeZulu().getTime(), fltSeg.getSegmentCode()), fltSeg);

		}

		return airportWiseMap;
	}

	private static boolean isAirportServiceBelongsToSecondSegment(Map<String, Integer> serviceCounter,
			FlightSegmentTO flightSegmentTO, String airportCode) {
		boolean result = true;
		if (serviceCounter != null) {
			for (Entry<String, Integer> entry : serviceCounter.entrySet()) {
				if (entry.getKey().equals(flightSegmentTO.getFlightRefNumber() + AirportServiceDTO.SEPERATOR + airportCode)
						&& entry.getValue() > 0) {
					result = false;
					break;
				}
			}
		}

		return result;
	}

	public static Map<Integer, Set<String>> getFlightSegIdWiseSelectedAnci(Map<String, Set<String>> flightRefWiseSelectedMeals) {
		Map<Integer, Set<String>> flightSegIdWiseSelectedMeals = new HashMap<Integer, Set<String>>();
		if (flightRefWiseSelectedMeals != null) {
			for (Entry<String, Set<String>> entry : flightRefWiseSelectedMeals.entrySet()) {
				Integer fltSegmentId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(entry.getKey());
				if (flightSegIdWiseSelectedMeals.get(fltSegmentId) == null) {
					flightSegIdWiseSelectedMeals.put(fltSegmentId, new HashSet<String>());
				}

				if (entry.getValue() != null) {
					flightSegIdWiseSelectedMeals.get(fltSegmentId).addAll(entry.getValue());
				}
			}
		}

		return flightSegIdWiseSelectedMeals;
	}

	public static List<FlightSegmentTO> getAutoCancellationAffectedSegments(
			Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxAddAncillaryMap) {
		List<FlightSegmentTO> affectedSegments = new ArrayList<FlightSegmentTO>();

		for (Entry<Integer, List<LCCSelectedSegmentAncillaryDTO>> anciEntry : paxAddAncillaryMap.entrySet()) {
			List<LCCSelectedSegmentAncillaryDTO> selAnciList = anciEntry.getValue();
			if (selAnciList != null && !selAnciList.isEmpty()) {
				for (LCCSelectedSegmentAncillaryDTO selAnci : selAnciList) {
					if (isSeatAutoCancellable(selAnci.getAirSeatDTO()) || isMealAutoCancellable(selAnci.getMealDTOs())
							|| isBaggageAutoCancellable(selAnci.getBaggageDTOs())
							|| isInsuranceAutoCancellable(selAnci.getInsuranceQuotations())
							|| isInflightServiceAutoCancellable(selAnci.getSpecialServiceRequestDTOs())
							|| isAirportServiceAutoCancellable(selAnci.getAirportServiceDTOs())) {
						affectedSegments.add(selAnci.getFlightSegmentTO());
					}
				}
			}
		}

		return affectedSegments;
	}

	private static boolean isSeatAutoCancellable(LCCAirSeatDTO airSeatDTO) {
		if (airSeatDTO != null && airSeatDTO.getSeatCharge() != null
				&& (airSeatDTO.getSeatCharge().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
			return true;
		}
		return false;
	}

	private static boolean isMealAutoCancellable(List<LCCMealDTO> mealDTOs) {
		if (mealDTOs != null && !mealDTOs.isEmpty()) {
			for (LCCMealDTO meal : mealDTOs) {
				if (meal.getMealCharge() != null
						&& (meal.getMealCharge().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isBaggageAutoCancellable(List<LCCBaggageDTO> baggageDTOs) {
		if (baggageDTOs != null && !baggageDTOs.isEmpty()) {
			for (LCCBaggageDTO baggage : baggageDTOs) {
				if (baggage.getBaggageCharge() != null
						&& (baggage.getBaggageCharge().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isInsuranceAutoCancellable(List<LCCInsuranceQuotationDTO> insuranceQuotationDTOs) {
		if (insuranceQuotationDTOs != null && !insuranceQuotationDTOs.isEmpty()) {
			return true;
		}
		return false;
	}

	private static boolean isInflightServiceAutoCancellable(List<LCCSpecialServiceRequestDTO> specialServiceRequestDTOs) {
		if (specialServiceRequestDTOs != null && !specialServiceRequestDTOs.isEmpty()) {
			for (LCCSpecialServiceRequestDTO ssr : specialServiceRequestDTOs) {
				if (ssr.getCharge() != null && (ssr.getCharge().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isAirportServiceAutoCancellable(List<LCCAirportServiceDTO> airportServiceRequestDTOs) {
		if (airportServiceRequestDTOs != null && !airportServiceRequestDTOs.isEmpty()) {
			for (LCCAirportServiceDTO aps : airportServiceRequestDTOs) {
				if ((aps.getAdultAmount() != null && (aps.getAdultAmount().compareTo(
						AccelAeroCalculator.getDefaultBigDecimalZero()) == 1))
						|| (aps.getChildAmount() != null && (aps.getChildAmount().compareTo(
								AccelAeroCalculator.getDefaultBigDecimalZero()) == 1))
						|| (aps.getInfantAmount() != null && (aps.getInfantAmount().compareTo(
								AccelAeroCalculator.getDefaultBigDecimalZero()) == 1))
						|| (aps.getReservationAmount() != null && (aps.getReservationAmount().compareTo(
								AccelAeroCalculator.getDefaultBigDecimalZero()) == 1))
						|| (aps.getServiceCharge() != null && (aps.getServiceCharge().compareTo(
								AccelAeroCalculator.getDefaultBigDecimalZero()) == 1))) {
					return true;
				}
			}
		}
		return false;
	}

	public static Map<AirportServiceKeyTO, FlightSegmentDTO> populateAiportWiseFltDTOSegments(
			List<FlightSegmentDTO> flightSegmentTOs) throws ModuleException {
		Map<AirportServiceKeyTO, FlightSegmentDTO> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, FlightSegmentDTO>();

		List<FlightSegmentDTO> outboundFlightSegmentTOs = getInOutFltDtoSegments(flightSegmentTOs, false);
		List<FlightSegmentDTO> inboundFlightSegmentTOs = getInOutFltDtoSegments(flightSegmentTOs, true);

		// Get valid airport wise flight segment for out-bound segments
		if (outboundFlightSegmentTOs.size() > 0) {
			airportWiseMap.putAll(createAirportWiseSegForInOut(outboundFlightSegmentTOs));
		}

		// Get valid airport wise flight segment for in-bound segments
		if (inboundFlightSegmentTOs.size() > 0) {
			airportWiseMap.putAll(createAirportWiseSegForInOut(inboundFlightSegmentTOs));
		}

		return airportWiseMap;
	}

	public static LCCReservationBaggageSummaryTo getResBaggageSummary(LCCClientReservation reservation) {
		Map<String, String> bookingClasses = new HashMap<String, String>();
		Map<String, String> classesOfService = new HashMap<String, String>();
		
		for (LCCClientReservationSegment segment : reservation.getSegments()){
			if (ReservationConstants.SegmentStatus.CONFIRMED.equals(segment.getStatus())){
				bookingClasses.put(segment.getSegmentCode(), segment.getFareTO().getBookingClassCode());
				classesOfService.put(segment.getFlightSegmentRefNumber(), segment.getCabinClassCode());
			}
		}

		LCCReservationBaggageSummaryTo baggageSummaryTo = new LCCReservationBaggageSummaryTo();
		baggageSummaryTo.setBookingClasses(bookingClasses);
		baggageSummaryTo.setClassesOfService(classesOfService);

		return baggageSummaryTo;
	}

	public static Map<String, String> getBookingClassesMapping(Collection<FareTO> fareTOs) {
		Map<String, String> bookingClasses = new HashMap<String, String>();
		for (FareTO fareTO : fareTOs) {
			bookingClasses.put(fareTO.getSegmentCode(), fareTO.getBookingClassCode());
		}
		return bookingClasses;
	}

	private static List<FlightSegmentDTO> getInOutFltDtoSegments(List<FlightSegmentDTO> flightSegmentDTOs, boolean returnFlag)
			throws ModuleException {
		List<FlightSegmentDTO> inOutFltSegList = new ArrayList<FlightSegmentDTO>();

		for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
			if (!AirproxyModuleUtils.getAirportBD().isBusSegment(flightSegmentDTO.getSegmentCode())) {
				if (flightSegmentDTO.isOpenReturnSegment() == returnFlag) {
					inOutFltSegList.add(flightSegmentDTO);
				}
			}
		}

		return inOutFltSegList;
	}

	private static Map<AirportServiceKeyTO, FlightSegmentDTO> createAirportWiseSegForInOut(List<FlightSegmentDTO> flightSegList)
			throws ModuleException {

		Map<AirportServiceKeyTO, FlightSegmentDTO> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, FlightSegmentDTO>();

		if (flightSegList.size() > 1) {
			FlightSegmentDTO prevFltSeg = null;
			int count = 0;
			for (FlightSegmentDTO flightSegmentTO : flightSegList) {
				count++;
				String[] airports = flightSegmentTO.getSegmentCode().split("/");
				if (prevFltSeg != null) {

					String[] prevAirports = prevFltSeg.getSegmentCode().split("/");

					if (!prevAirports[prevAirports.length - 1].equals(airports[0])) {

						airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
								flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
								flightSegmentTO);

						airportWiseMap.put(new AirportServiceKeyTO(prevAirports[prevAirports.length - 1],
								ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu().getTime(),
								prevFltSeg.getSegmentCode()), prevFltSeg);

					} else {
						HubInfo hubInfo = AirproxyModuleUtils.getLocationDB().getHubInfo(airports[0],
								prevFltSeg.getCarrierCode(), flightSegmentTO.getCarrierCode());

						if (hubInfo != null) {
							long transitTime = flightSegmentTO.getDepartureDateTimeZulu().getTime()
									- prevFltSeg.getArrivalDateTimeZulu().getTime();

							double transitTimeInMin = transitTime / (1000 * 60);

							// calculate minCnxTime in min
							String minCnxStr = hubInfo.getMinConnectionTime();
							String[] minCnxArr = minCnxStr.split(":");

							double minCnxTimeInMin = new Integer(minCnxArr[0]) * 60 + new Integer(minCnxArr[1]);

							// Calculate max cnx time in min
							String maxCnxStr = hubInfo.getMaxConnectionTime();
							String[] maxCnxArr = maxCnxStr.split(":");

							double maxCnxTimeInMin = new Integer(maxCnxArr[0]) * 60 + new Integer(maxCnxArr[1]);

							if (transitTimeInMin < minCnxTimeInMin) {
								// Don't consider the airport for airport services
							} else if (transitTimeInMin >= minCnxTimeInMin && transitTimeInMin <= maxCnxTimeInMin) {

								/*
								 * If first segment of the connection already have airport services then airport
								 * services will bind to 1st segment
								 */

								airportWiseMap.put(new AirportServiceKeyTO(airports[0],
										ReservationPaxSegmentSSR.APPLY_ON_TRANSIT, prevFltSeg.getDepartureDateTimeZulu()
												.getTime(), prevFltSeg.getSegmentCode()), prevFltSeg);

							} else if (transitTimeInMin > maxCnxTimeInMin) {
								/*
								 * provide airport services separately for connection airport when transit time exceeds
								 * max connection time
								 */

								airportWiseMap.put(new AirportServiceKeyTO(airports[0],
										ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE, flightSegmentTO.getDepartureDateTimeZulu()
												.getTime(), flightSegmentTO.getSegmentCode()), flightSegmentTO);

								airportWiseMap.put(new AirportServiceKeyTO(airports[0],
										ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu()
												.getTime(), prevFltSeg.getSegmentCode()), prevFltSeg);

							}

						} else {

							airportWiseMap.put(new AirportServiceKeyTO(prevAirports[prevAirports.length - 1],
									ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, prevFltSeg.getDepartureDateTimeZulu().getTime(),
									prevFltSeg.getSegmentCode()), prevFltSeg);

							airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
									flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
									flightSegmentTO);

						}
					}

				} else {

					airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE,
							flightSegmentTO.getDepartureDateTimeZulu().getTime(), flightSegmentTO.getSegmentCode()),
							flightSegmentTO);
				}

				if (count == flightSegList.size()) {

					airportWiseMap.put(new AirportServiceKeyTO(airports[airports.length - 1],
							ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL, flightSegmentTO.getDepartureDateTimeZulu().getTime(),
							flightSegmentTO.getSegmentCode()), flightSegmentTO);

				}

				prevFltSeg = flightSegmentTO;
			}
		} else if (flightSegList.size() == 1) {
			FlightSegmentDTO fltSeg = flightSegList.get(0);
			String[] airports = fltSeg.getSegmentCode().split("/");

			airportWiseMap.put(new AirportServiceKeyTO(airports[0], ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE, fltSeg
					.getDepartureDateTimeZulu().getTime(), fltSeg.getSegmentCode()), fltSeg);

			airportWiseMap.put(new AirportServiceKeyTO(airports[airports.length - 1], ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL,
					fltSeg.getDepartureDateTimeZulu().getTime(), fltSeg.getSegmentCode()), fltSeg);

		}

		return airportWiseMap;
	}

}
