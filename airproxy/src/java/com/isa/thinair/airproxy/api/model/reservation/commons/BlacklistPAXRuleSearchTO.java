package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class BlacklistPAXRuleSearchTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID field for rule name
	 */
	private String label;

	/**
	 * check field for firstname
	 */
	private boolean isFirstNameChecked;

	/**
	 * check field for lastname
	 */
	private boolean isLaststNameChecked;

	/**
	 * check field for date of birth
	 */
	private boolean isDateOfBirthChecked;

	/**
	 * check field for passport no
	 */
	private boolean isPassportNoChecked;

	/**
	 * check field for nationality
	 */
	private boolean isNationalityChecked;

	/**
	 * check field for nationality
	 */
	private String status;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isFirstNameChecked() {
		return isFirstNameChecked;
	}

	public void setFirstNameChecked(boolean isFirstNameChecked) {
		this.isFirstNameChecked = isFirstNameChecked;
	}

	public boolean isLaststNameChecked() {
		return isLaststNameChecked;
	}

	public void setLaststNameChecked(boolean isLaststNameChecked) {
		this.isLaststNameChecked = isLaststNameChecked;
	}

	public boolean isDateOfBirthChecked() {
		return isDateOfBirthChecked;
	}

	public void setDateOfBirthChecked(boolean isDateOfBirthChecked) {
		this.isDateOfBirthChecked = isDateOfBirthChecked;
	}

	public boolean isPassportNoChecked() {
		return isPassportNoChecked;
	}

	public void setPassportNoChecked(boolean isPassportNoChecked) {
		this.isPassportNoChecked = isPassportNoChecked;
	}

	public boolean isNationalityChecked() {
		return isNationalityChecked;
	}

	public void setNationalityChecked(boolean isNationalityChecked) {
		this.isNationalityChecked = isNationalityChecked;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
