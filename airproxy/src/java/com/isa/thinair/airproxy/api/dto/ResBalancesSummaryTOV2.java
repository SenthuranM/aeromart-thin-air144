package com.isa.thinair.airproxy.api.dto;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.LccPaxPaymentTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Wraps OND summary and PAX summary info for reservation alterations.
 */
public class ResBalancesSummaryTOV2 {

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCnxChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModChargeForCurrentAlt = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Collection<ReservationTnx> colPayments;

	private Collection<LccPaxPaymentTO> colLccPaxPayments;

	private Collection<ReservationTnx> colCredits;

	private SegmentSummaryTOV2 segmentSummaryTOV2;

	private Collection<PAXSummaryTOV2> paxSummaryTOV2;

	private BigDecimal infantCharge;

	private BigDecimal totalFareDiscount;

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getTotalCnxChargeForCurrentAlt() {
		return totalCnxChargeForCurrentAlt;
	}

	public void setTotalCnxChargeForCurrentAlt(BigDecimal totalCnxChargeForCurrentAlt) {
		this.totalCnxChargeForCurrentAlt = totalCnxChargeForCurrentAlt;
	}

	public BigDecimal getTotalModChargeForCurrentAlt() {
		return totalModChargeForCurrentAlt;
	}

	public void setTotalModChargeForCurrentAlt(BigDecimal totalModChargeForCurrentAlt) {
		this.totalModChargeForCurrentAlt = totalModChargeForCurrentAlt;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the segmentSummaryTOV2
	 */
	public SegmentSummaryTOV2 getSegmentSummaryTOV2() {
		return segmentSummaryTOV2;
	}

	/**
	 * @param segmentSummaryTOV2
	 *            the segmentSummaryTOV2 to set
	 */
	public void setSegmentSummaryTOV2(SegmentSummaryTOV2 segmentSummaryTOV2) {
		this.segmentSummaryTOV2 = segmentSummaryTOV2;
	}

	/**
	 * @return the paxSummaryTOV2
	 */
	public Collection<PAXSummaryTOV2> getPaxSummaryTOV2() {
		return paxSummaryTOV2;
	}

	/**
	 * @param paxSummaryTOV2
	 *            the paxSummaryTOV2 to set
	 */
	public void setPaxSummaryTOV2(Collection<PAXSummaryTOV2> paxSummaryTOV2) {
		this.paxSummaryTOV2 = paxSummaryTOV2;
	}

	/**
	 * @return the colPayments
	 */
	public Collection<ReservationTnx> getColPayments() {
		return colPayments;
	}

	/**
	 * @param colPayments
	 *            the colPayments to set
	 */
	public void setColPayments(Collection<ReservationTnx> colPayments) {
		this.colPayments = colPayments;
	}

	/**
	 * @return the colCredits
	 */
	public Collection<ReservationTnx> getColCredits() {
		return colCredits;
	}

	/**
	 * @param colCredits
	 *            the colCredits to set
	 */
	public void setColCredits(Collection<ReservationTnx> colCredits) {
		this.colCredits = colCredits;
	}

	/**
	 * @return the colLccPaxPayments
	 */
	public Collection<LccPaxPaymentTO> getColLccPaxPayments() {
		return colLccPaxPayments;
	}

	/**
	 * @param colLccPaxPayments
	 *            the colLccPaxPayments to set
	 */
	public void setColLccPaxPayments(Collection<LccPaxPaymentTO> colLccPaxPayments) {
		this.colLccPaxPayments = colLccPaxPayments;
	}

	/**
	 * @return the totalCreditAmount
	 */
	public BigDecimal getTotalCreditAmount() {
		return totalCreditAmount;
	}

	/**
	 * @param totalCreditAmount
	 *            the totalCreditAmount to set
	 */
	public void setTotalCreditAmount(BigDecimal totalCreditAmount) {
		this.totalCreditAmount = totalCreditAmount;
	}

	public BigDecimal getInfantCharge() {
		return infantCharge;
	}

	public void setInfantCharge(BigDecimal infantCharge) {
		this.infantCharge = infantCharge;
	}

	public void setTotalFareDiscount(BigDecimal totalFareDiscount) {
		this.totalFareDiscount = totalFareDiscount;
	}
	
	public BigDecimal getTotalFareDiscount(){
		return totalFareDiscount;
	}
}
