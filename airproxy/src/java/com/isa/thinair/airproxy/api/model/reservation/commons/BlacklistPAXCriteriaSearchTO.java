package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;

public class BlacklistPAXCriteriaSearchTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long blacklistPAXCriteriaID;
	
	private String paxFullName;
	
	private String paxStatus;

	private String passportNo;

	private String nationality;

	private Date dateOfBirth;

	private String remarks;

	private String blacklistType;

	public String getPaxFullName() {
		return paxFullName;
	}

	public void setPaxFullName(String paxFullName) {
		this.paxFullName = paxFullName;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBlacklistType() {
		return blacklistType;
	}

	public void setBlacklistType(String blacklistType) {
		this.blacklistType = blacklistType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getBlacklistPAXCriteriaID() {
		return blacklistPAXCriteriaID;
	}

	public void setBlacklistPAXCriteriaID(Long blacklistPAXCriteriaID) {
		this.blacklistPAXCriteriaID = blacklistPAXCriteriaID;
	}
	
}
