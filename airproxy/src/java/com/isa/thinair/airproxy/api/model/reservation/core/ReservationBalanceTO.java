package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * @since 5:54 PM 11/5/2009
 */
public class ReservationBalanceTO implements Serializable {

	private static final long serialVersionUID = 4902617819835432728L;

	private LCCClientSegmentSummaryTO segmentSummary;

	private Collection<LCCClientPassengerSummaryTO> passengerSummaryList;

	private Map<String, LCCClientPassengerSummaryTO> passengerSummaryMap = new HashMap<String, LCCClientPassengerSummaryTO>();

	private BigDecimal totalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal infantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String version;

	private boolean showCnxCharge;

	private boolean showModCharge;

	private boolean fareAdjustmentValid;

	private Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = new HashMap<Integer, BigDecimal>();

	private Map<Integer, List<ExternalChgDTO>> paxEffectiveTax = new HashMap<Integer, List<ExternalChgDTO>>();
	
	private boolean infantPaymentSeparated;

	/**
	 * @return the totalCnxCharge
	 */
	public BigDecimal getTotalCnxCharge() {
		return totalCnxCharge;
	}

	/**
	 * @param totalCnxCharge
	 *            the totalCnxCharge to set
	 */
	public void setTotalCnxCharge(BigDecimal totalCnxCharge) {
		this.totalCnxCharge = totalCnxCharge;
	}

	/**
	 * @return the totalModCharge
	 */
	public BigDecimal getTotalModCharge() {
		return totalModCharge;
	}

	/**
	 * @param totalModCharge
	 *            the totalModCharge to set
	 */
	public void setTotalModCharge(BigDecimal totalModCharge) {
		this.totalModCharge = totalModCharge;
	}

	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalAmountDue
	 */
	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	/**
	 * @param totalAmountDue
	 *            the totalAmountDue to set
	 */
	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	/**
	 * @return the lccClientSegmentSummaryTO
	 */
	public LCCClientSegmentSummaryTO getSegmentSummary() {
		return segmentSummary;
	}

	/**
	 * @param segmentSummary
	 *            the lccClientSegmentSummaryTO to set
	 */
	public void setSegmentSummary(LCCClientSegmentSummaryTO segmentSummary) {
		this.segmentSummary = segmentSummary;
	}

	/**
	 * @return the colPassengerSummary
	 */
	public Collection<LCCClientPassengerSummaryTO> getPassengerSummaryList() {
		return passengerSummaryList;
	}

	/**
	 * @param passengerSummaryList
	 *            the colPassengerSummary to set
	 */
	public void setPassengerSummaryList(Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {
		this.passengerSummaryList = passengerSummaryList;
	}

	/**
	 * @return the totalPaidAmount
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return the totalCreditAmount
	 */
	public BigDecimal getTotalCreditAmount() {
		return totalCreditAmount;
	}

	/**
	 * @param totalCreditAmount
	 *            the totalCreditAmount to set
	 */
	public void setTotalCreditAmount(BigDecimal totalCreditAmount) {
		this.totalCreditAmount = totalCreditAmount;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public BigDecimal getInfantCharge() {
		return infantCharge;
	}

	public void setInfantCharge(BigDecimal infantCharge) {
		this.infantCharge = infantCharge;
	}

	public BigDecimal getTotalFareDiscount() {
		return totalFareDiscount;
	}

	public void setTotalFareDiscount(BigDecimal totalFareDiscount) {
		this.totalFareDiscount = totalFareDiscount;
	}

	public void addPaxSummaryList(Collection<LCCClientPassengerSummaryTO> paxSummaryList) {
		this.passengerSummaryList = paxSummaryList;
		for (LCCClientPassengerSummaryTO pax : paxSummaryList) {
			passengerSummaryMap.put(pax.getTravelerRefNumber(), pax);
			if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) || isInfantPaymentSeparated()) {
				totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount, pax.getTotalFareDiscount());
				totalCnxCharge = AccelAeroCalculator.add(totalCnxCharge, pax.getTotalCnxCharge());
				totalModCharge = AccelAeroCalculator.add(totalModCharge, pax.getTotalModCharge());
				totalPrice = AccelAeroCalculator.add(totalPrice, pax.getTotalPrice());
				totalAmountDue = AccelAeroCalculator.add(totalAmountDue, pax.getTotalAmountDue());
				totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, pax.getTotalCreditAmount());
				totalPaidAmount = AccelAeroCalculator.add(totalPaidAmount, pax.getTotalPaidAmount());
			}

		}

	}

	public LCCClientPassengerSummaryTO getPassengerSummary(String travellerRefNumber) {
		return passengerSummaryMap.get(travellerRefNumber);
	}

	public boolean hasBalanceToPay() {
		if (getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		return false;
	}

	public void setShowCnxCharge(boolean showCnxCharge) {
		this.showCnxCharge = showCnxCharge;
	}

	public void setShowModCharge(boolean showModCharge) {
		this.showModCharge = showModCharge;
	}

	public boolean isShowCnxCharge() {
		return showCnxCharge;
	}

	public boolean isShowModCharge() {
		return showModCharge;
	}

	public boolean isFareAdjustmentValid() {
		return fareAdjustmentValid;
	}

	public void setFareAdjustmentValid(boolean fareAdjustmentValid) {
		this.fareAdjustmentValid = fareAdjustmentValid;
	}

	public Map<Integer, BigDecimal> getPaxWiseAdjustmentAmountMap() {
		return paxWiseAdjustmentAmountMap;
	}

	public void setPaxWiseAdjustmentAmountMap(Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap) {
		this.paxWiseAdjustmentAmountMap = paxWiseAdjustmentAmountMap;
	}

	public Map<Integer, List<ExternalChgDTO>> getPaxEffectiveTax() {
		return paxEffectiveTax;
	}

	public void setPaxEffectiveTax(Map<Integer, List<ExternalChgDTO>> paxEffectiveTax) {
		this.paxEffectiveTax = paxEffectiveTax;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}
	
}
