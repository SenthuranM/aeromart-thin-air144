package com.isa.thinair.airproxy.api.model.masterDataPublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;

public class MasterDataPublishRQDataTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private List<AirportDSTPublishRQDataTO> publishAirports;

	private List<Station> publishStations;

	private List<Territory> publishTerritories;
	
	private List<City> publishCities;


	public List<AirportDSTPublishRQDataTO> getPublishAirports() {
		if (publishAirports == null) {
			publishAirports = new ArrayList<AirportDSTPublishRQDataTO>();
		}
		return publishAirports;
	}

	public void setPublishAirports(List<AirportDSTPublishRQDataTO> publishAirports) {
		this.publishAirports = publishAirports;
	}

	public List<Station> getPublishStations() {
		if (publishStations == null) {
			publishStations = new ArrayList<Station>();
		}
		return publishStations;
	}

	public void setPublishStations(List<Station> publishStations) {
		this.publishStations = publishStations;
	}

	public List<Territory> getPublishTerritories() {
		if (publishTerritories == null) {
			publishTerritories = new ArrayList<Territory>();
		}
		return publishTerritories;
	}

	public void setPublishTerritories(List<Territory> publishTerritories) {
		this.publishTerritories = publishTerritories;
	}

	public List<City> getPublishCities() {
		if (publishCities == null) {
			publishCities = new ArrayList<City>();
		}
		return publishCities;	}

	public void setPublishCities(List<City> publishCities) {
		this.publishCities = publishCities;
	}

}
