package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

public class PriceInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum FareType {
		STANDARD,
		FIXED;

		public static FareType getEnum(String strEnum) {
			for (FareType enm : FareType.values()) {
				if (enm.toString().equals(strEnum)) {
					return enm;
				}
			}

			return FareType.STANDARD;
		}
	}

	private FareTypeTO fareTypeTO;

	private FareSegChargeTO fareSegChargeTO;

	private final List<OndClassOfServiceSummeryTO> availableLogicalCCList = new ArrayList<OndClassOfServiceSummeryTO>();

	private Date lastFareQuotedDate;

	private boolean fqWithinValidity;

	private boolean allRequestedOndsHasFares;

	public List<PerPaxPriceInfoTO> getPerPaxPriceInfo() {
		if (this.fareTypeTO != null) {
			return this.fareTypeTO.getPerPaxPriceInfo();
		} else {
			return null;
		}
	}

	public PerPaxPriceInfoTO getPerPaxPriceInfoTO(String paxType) {
		List<PerPaxPriceInfoTO> lstPerPaxPriceInfoTO = getPerPaxPriceInfo();
		if (lstPerPaxPriceInfoTO != null) {
			for (PerPaxPriceInfoTO perPaxPriceInfoTO : lstPerPaxPriceInfoTO) {
				if (perPaxPriceInfoTO.getPassengerType().equals(paxType)) {
					return perPaxPriceInfoTO;
				}
			}
		}

		return null;
	}

	public BigDecimal getPerPaxDiscountFareAmount(String paxType, float fareDiscountPercentage) {
		PerPaxPriceInfoTO pppInfo = getPerPaxPriceInfoTO(paxType);
		BigDecimal discAmt = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (pppInfo != null) {
			discAmt = AccelAeroCalculator.add(discAmt,
					pppInfo.getPassengerPrice().getDiscountedFareAmount(fareDiscountPercentage, null));
		}
		return discAmt;
	}

	public BigDecimal getPerPaxDomesticDiscountFareAmount(String paxType, int fareDiscountPercentage,
			ArrayList<String> domesticSegmentCodeList) {
		PerPaxPriceInfoTO pppInfo = getPerPaxPriceInfoTO(paxType);
		BigDecimal discAmt = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (pppInfo != null) {
			discAmt = AccelAeroCalculator.add(discAmt,
					pppInfo.getPassengerPrice().getDomesticDiscountedFareAmount(fareDiscountPercentage, domesticSegmentCodeList));
		}
		return discAmt;
	}

	// calculate per pax discount for promotion
	public BigDecimal getPerPaxDiscountAmount(String paxType, float discountValue, String discountType, int discountApplyTo,
			Set<String> applicableOnds) {
		BigDecimal discAmt = AccelAeroCalculator.getDefaultBigDecimalZero();
		PerPaxPriceInfoTO pppInfo = getPerPaxPriceInfoTO(paxType);
		if (pppInfo != null) {
			if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(discountType)) {
				if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.FARE.ordinal()) {
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedFareAmount(discountValue, applicableOnds));
				} else if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.FARE_SURCHARGE.ordinal()) {
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedFareAmount(discountValue, applicableOnds));
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedSurchargeAmount(discountValue, applicableOnds));
				} else if (discountApplyTo == PromotionCriteriaConstants.DiscountApplyTo.TOTAL.ordinal()) {
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedFareAmount(discountValue, applicableOnds));
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedSurchargeAmount(discountValue, applicableOnds));
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedTaxAmount(discountValue, applicableOnds));
					discAmt = AccelAeroCalculator.add(discAmt,
							pppInfo.getPassengerPrice().getDiscountedFeeAmount(discountValue, applicableOnds));
				}
			} else if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(discountType)) {
				Float fltDiscValue = discountValue;
				BigDecimal offeredDiscount = new BigDecimal(fltDiscValue.toString());
				BigDecimal paxDiscountableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (applicableOnds != null && !applicableOnds.isEmpty()) {
					paxDiscountableTotal = AccelAeroCalculator.add(paxDiscountableTotal,
							pppInfo.getPassengerPrice().getDiscountableFareTotal(applicableOnds));
					paxDiscountableTotal = AccelAeroCalculator.add(paxDiscountableTotal,
							pppInfo.getPassengerPrice().getDiscountableSurchargeTotal(applicableOnds));
					paxDiscountableTotal = AccelAeroCalculator.add(paxDiscountableTotal,
							pppInfo.getPassengerPrice().getDiscountableTaxTotal(applicableOnds));
					paxDiscountableTotal = AccelAeroCalculator.add(paxDiscountableTotal,
							pppInfo.getPassengerPrice().getDiscountableFeeTotal(applicableOnds));
				} else {
					paxDiscountableTotal = pppInfo.getPassengerPrice().getTotalPrice();
				}

				if (AccelAeroCalculator.isGreaterThan(offeredDiscount, paxDiscountableTotal)) {
					discAmt = paxDiscountableTotal;
				} else {
					discAmt = offeredDiscount;
				}
			}
		}

		return discAmt;
	}

	public boolean hasReturnFlights() {

		if (this.fareSegChargeTO != null) {
			for (OndFareSegChargeTO ondFareSegChargeTO : this.fareSegChargeTO.getOndFareSegChargeTOs()) {
				if (ondFareSegChargeTO.isInbound()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return the fareTypeTO
	 */
	public FareTypeTO getFareTypeTO() {
		return fareTypeTO;
	}

	/**
	 * @param fareTypeTO
	 *            the fareTypeTO to set
	 */
	public void setFareTypeTO(FareTypeTO fareTypeTO) {
		this.fareTypeTO = fareTypeTO;
	}

	/**
	 * @return the fareSegChargeTO
	 */
	public FareSegChargeTO getFareSegChargeTO() {
		return fareSegChargeTO;
	}

	/**
	 * @param fareSegChargeTO
	 *            the fareSegChargeTO to set
	 */
	public void setFareSegChargeTO(FareSegChargeTO fareSegChargeTO) {
		this.fareSegChargeTO = fareSegChargeTO;
	}

	public List<OndClassOfServiceSummeryTO> getAvailableLogicalCCList() {
		return availableLogicalCCList;
	}

	public void addAvailableLogicalCC(String ondCode, int sequence, List<LogicalCabinClassInfoTO> segmentLogicalCabinClassTypes,
			Integer requestedOndSequence) {
		availableLogicalCCList.add(new OndClassOfServiceSummeryTO(ondCode,
				(requestedOndSequence != null ? ((requestedOndSequence + 1) % 2 == 0) : ((sequence + 1) % 2 == 0)), sequence,
				requestedOndSequence, segmentLogicalCabinClassTypes));
	}

	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setFQWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public boolean isFQWithinValidity() {
		return fqWithinValidity;
	}

	public BigDecimal getTotalFarebyPaxType(PriceInfoTO priceInfoTO, String paxType) {
		BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);
		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null) {
			FareTypeTO fareTypeTO = paxPriceInfoTO.getPassengerPrice();
			totalFare = fareTypeTO.getTotalBaseFare();
		}

		return totalFare;
	}

	public BigDecimal getTotalChargesbyPaxType(PriceInfoTO priceInfoTO, String paxType) {
		BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);
		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null) {
			FareTypeTO fareTypeTO = paxPriceInfoTO.getPassengerPrice();
			totalCharges = AccelAeroCalculator.add(fareTypeTO.getTotalSurcharges(), fareTypeTO.getTotalTaxes());
		}

		return totalCharges;
	}

	public BigDecimal getTotalPricebyPaxType(PriceInfoTO priceInfoTO, String paxType) {
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);
		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null) {
			FareTypeTO fareTypeTO = paxPriceInfoTO.getPassengerPrice();
			totalPrice = fareTypeTO.getTotalPrice();
		}

		return totalPrice;
	}

	public BigDecimal getTotalTaxbyPaxType(PriceInfoTO priceInfoTO, String paxType) {
		BigDecimal totalTax = AccelAeroCalculator.getDefaultBigDecimalZero();

		PerPaxPriceInfoTO paxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(paxType);
		if (paxPriceInfoTO != null && paxPriceInfoTO.getPassengerPrice() != null) {
			FareTypeTO fareTypeTO = paxPriceInfoTO.getPassengerPrice();
			totalTax = fareTypeTO.getTotalTaxes();
		}

		return totalTax;
	}

	public boolean isAllRequestedOndsHasFares() {
		return allRequestedOndsHasFares;
	}

	public void setAllRequestedOndsHasFares(boolean allRequestedOndsHasFares) {
		this.allRequestedOndsHasFares = allRequestedOndsHasFares;
	}
}
