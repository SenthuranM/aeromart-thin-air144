package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import org.json.simple.parser.ParseException;

import com.google.common.collect.Sets;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.constants.AnciOfferConstants;

/**
 * Miscellaneous methods related to the anci offer criteria functionality.
 * 
 * @author thihara
 * 
 */
public final class AnciOfferUtil {

	public static final String SEGMENT_SEPERATOR = "/";

	public static final Integer INBOUND = 2;
	public static final Integer OUTBOUND = 1;

	public static AnciOfferCriteriaSearchTO createAnciOfferSearch(FlightSegmentTO flightSegmentTO, AnciOfferType anciOfferType) {
		AnciOfferCriteriaSearchTO searchCriteria = new AnciOfferCriteriaSearchTO();

		searchCriteria.setAdults(flightSegmentTO.getAdultCount());
		searchCriteria.setChildren(flightSegmentTO.getChildCount());
		searchCriteria.setInfants(flightSegmentTO.getInfantCount());

		searchCriteria.setAnciOfferType(anciOfferType);
		searchCriteria.setFareBasisCode(flightSegmentTO.getFareBasisCode());
		searchCriteria.setBookingClass(flightSegmentTO.getBookingClass());
		searchCriteria.setFlexiID(flightSegmentTO.getFlexiID());
		searchCriteria.setLcc(flightSegmentTO.getLogicalCabinClassCode());
		searchCriteria.setOND(flightSegmentTO.getSegmentCode().equals(flightSegmentTO.getSectorONDCode()) ? flightSegmentTO
				.getSegmentCode() : flightSegmentTO.getSectorONDCode());
		searchCriteria.setDepartureDate(flightSegmentTO.getDepartureDateTime());

		searchCriteria.setJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
		searchCriteria.setSegmentReturn(flightSegmentTO.isReturnFlag());
		searchCriteria.setInverseSegmentDepartureDate(flightSegmentTO.getInverseSegmentDeparture());

		return searchCriteria;
	}

	public static AnciOfferCriteriaSearchTO createAnciOfferSearch(BookingFlightSegment flightSegmentTO,
			AnciOfferType anciOfferType, Collection<BookingFlightSegment> allJourneySegments) {
		AnciOfferCriteriaSearchTO searchCriteria = new AnciOfferCriteriaSearchTO();

		searchCriteria.setAdults(flightSegmentTO.getAdultCount());
		searchCriteria.setChildren(flightSegmentTO.getChildCount());
		searchCriteria.setInfants(flightSegmentTO.getInfantCount());

		searchCriteria.setAnciOfferType(anciOfferType);
		searchCriteria.setFareBasisCode(flightSegmentTO.getFareBasisCode());
		searchCriteria.setBookingClass(flightSegmentTO.getBookingClass());
		searchCriteria.setFlexiID(flightSegmentTO.getFlexiID() == null ? null : flightSegmentTO.getFlexiID().intValue());
		searchCriteria.setLcc(flightSegmentTO.getLogicalCabinClassCode());
		searchCriteria.setOND(flightSegmentTO.getSegmentCode().equals(flightSegmentTO.getSectorONDCode()) ? flightSegmentTO
				.getSegmentCode() : flightSegmentTO.getSectorONDCode());

		searchCriteria.setDepartureDate(flightSegmentTO.getDepatureDateTime());

		searchCriteria.setJourneyReturn(flightSegmentTO.isParticipatingJourneyReturn());
		searchCriteria.setSegmentReturn("Y".equalsIgnoreCase(flightSegmentTO.getReturnFlag()));
		searchCriteria.setInverseSegmentDepartureDate(flightSegmentTO.getInverseSegmentDeparture());

		return searchCriteria;
	}

	/**
	 * Populates the given flight segments with the passenger counts, Flexi Rule ID and the Fare Basis Code.
	 * 
	 * @param doesOldSegmentsHaveReturn
	 */
	public static List<FlightSegmentTO> populateMissingDataForAnciOfferSearch(PriceInfoTO priceInfo,
			List<FlightSegmentTO> flightSegments, boolean doesOldSegmentsHaveReturn, List<FlightSegmentTO> oldFlightSegments) {

		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;

		for (PerPaxPriceInfoTO perPaxPriceInfo : priceInfo.getPerPaxPriceInfo()) {
			String paxType = perPaxPriceInfo.getPassengerType();
			if (PaxTypeTO.ADULT.equals(paxType)) {
				adultCount++;
			} else if (PaxTypeTO.CHILD.equals(paxType)) {
				childCount++;
			} else if (PaxTypeTO.INFANT.equals(paxType)) {
				infantCount++;
			}
		}

		for (FlightSegmentTO flightSegment : flightSegments) {

			flightSegment.setAdultCount(adultCount);
			flightSegment.setChildCount(childCount);
			flightSegment.setInfantCount(infantCount);

			FLEXI_ID_LOOP: for (OndClassOfServiceSummeryTO cosSummary : priceInfo.getAvailableLogicalCCList()) {
				for (LogicalCabinClassInfoTO lccInfo : cosSummary.getAvailableLogicalCCList()) {
					if (cosSummary.getOndCode().contains(flightSegment.getSegmentCode()) && lccInfo.isSelected()
							&& lccInfo.isWithFlexi()) {
						flightSegment.setFlexiID(lccInfo.getFlexiRuleID());
						break FLEXI_ID_LOOP;
					}
				}
			}

			for (FareRuleDTO fareRule : priceInfo.getFareTypeTO().getApplicableFareRules()) {
				if (fareRule.getOrignNDest().contains(flightSegment.getSegmentCode())) {
					flightSegment.setFareBasisCode(fareRule.getFareBasisCode());
					flightSegment.setBookingClass(fareRule.getBookingClassCode());
					flightSegment.setOndSequence(fareRule.getOndSequence());
					break;
				}
			}

			for (FlightSegmentTO oldFLightSegTO : oldFlightSegments) {
				if (flightSegment.getSegmentCode().equalsIgnoreCase(oldFLightSegTO.getSegmentCode())) {
					flightSegment.setReturnFlag(oldFLightSegTO.isReturnFlag());
				}
			}

			boolean hasReturnSegment = AppSysParamsUtil.isRequoteEnabled()
					? hasReturnSegments(flightSegments)
					: (doesOldSegmentsHaveReturn || hasReturnSegments(flightSegments));
			if (hasReturnSegment) {
				flightSegment.setParticipatingJourneyReturn(true);
				Date inverseDeparture = getInverseSegmentsDeparture(flightSegments, flightSegment);
				if (inverseDeparture == null) {
					inverseDeparture = getInverseSegmentsDeparture(oldFlightSegments, flightSegment);
					if (inverseDeparture == null) {
						inverseDeparture = getInverseONDDeparture(flightSegments, flightSegment);
						if (inverseDeparture == null) {
							inverseDeparture = getInverseONDDeparture(oldFlightSegments, flightSegment);
						}
					}
				}
				flightSegment.setInverseSegmentDeparture(inverseDeparture);
			}

			String sectorCode = null;
			Integer mapKey = flightSegment.isReturnFlag() ? INBOUND : OUTBOUND;
			if (oldFlightSegments == null || oldFlightSegments.isEmpty()) {
				Map<Integer, Map<String, String>> journeyCarrierWiseONDs = getJourneyCarrierWiseSectorCodes(flightSegments);
				sectorCode = journeyCarrierWiseONDs.get(mapKey).get(flightSegment.getOperatingAirline());
			} else {
				Map<Integer, Map<String, String>> journeyCarrierWiseONDs = getJourneyCarrierWiseSectorCodes(oldFlightSegments);
				sectorCode = journeyCarrierWiseONDs.get(mapKey).get(flightSegment.getOperatingAirline());
			}
			sectorCode = sectorCode == null ? "" : sectorCode;
			flightSegment.setSectorONDCode(sectorCode);
		}

		return flightSegments;
	}

	private static Map<Integer, Map<String, String>> getJourneyCarrierWiseSectorCodes(List<FlightSegmentTO> flightSegments) {

		Map<Integer, Map<String, String>> journeyCarrierWiseSectorCodes = new HashMap<Integer, Map<String, String>>();

		Map<Integer, List<FlightSegmentTO>> journeyWiseSegments = getInBoundOutBoundSegments(flightSegments);
		for (Entry<Integer, List<FlightSegmentTO>> journeySegEntry : journeyWiseSegments.entrySet()) {
			Map<String, List<FlightSegmentTO>> carrierWiseSegments = getCarrierWiseSegments(journeySegEntry.getValue());
			for (Entry<String, List<FlightSegmentTO>> carrierWiseSegEntries : carrierWiseSegments.entrySet()) {

				String carrierWiseOND = getONDCode(carrierWiseSegEntries.getValue());

				if (journeyCarrierWiseSectorCodes.get(journeySegEntry.getKey()) == null) {
					journeyCarrierWiseSectorCodes.put(journeySegEntry.getKey(), new HashMap<String, String>());
				}

				journeyCarrierWiseSectorCodes.get(journeySegEntry.getKey()).put(carrierWiseSegEntries.getKey(), carrierWiseOND);

			}
		}

		return journeyCarrierWiseSectorCodes;
	}

	private static Map<String, List<FlightSegmentTO>> getCarrierWiseSegments(List<FlightSegmentTO> segments) {
		Map<String, List<FlightSegmentTO>> carrierWiseSegments = new HashMap<String, List<FlightSegmentTO>>();
		for (FlightSegmentTO flightSegmentTO : segments) {
			if (carrierWiseSegments.get(flightSegmentTO.getOperatingAirline()) == null) {
				carrierWiseSegments.put(flightSegmentTO.getOperatingAirline(), new ArrayList<FlightSegmentTO>());
			}
			carrierWiseSegments.get(flightSegmentTO.getOperatingAirline()).add(flightSegmentTO);
		}

		return carrierWiseSegments;
	}

	private static boolean hasReturnSegments(List<FlightSegmentTO> flightSegments) {
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (flightSegmentTO.isReturnFlag()) {
				return true;
			}
		}
		return false;
	}

	private static void setONDCodes(List<FlightSegmentTO> flightSegments) {
		Map<Integer, Map<String, String>> journeyCarrierWiseSegs = getJourneyCarrierWiseSectorCodes(flightSegments);

		for (FlightSegmentTO seg : flightSegments) {
			Integer mapKey = seg.isReturnFlag() ? INBOUND : OUTBOUND;
			seg.setSectorONDCode(journeyCarrierWiseSegs.get(mapKey).get(seg.getOperatingAirline()));
		}

	}

	private static Map<Integer, List<FlightSegmentTO>> getInBoundOutBoundSegments(List<FlightSegmentTO> flightSegments) {
		Map<Integer, List<FlightSegmentTO>> journeyWiseSegs = new HashMap<Integer, List<FlightSegmentTO>>();
		for (FlightSegmentTO seg : flightSegments) {
			Integer mapKey = seg.isReturnFlag() ? INBOUND : OUTBOUND;
			if (journeyWiseSegs.get(mapKey) == null) {
				journeyWiseSegs.put(mapKey, new ArrayList<FlightSegmentTO>());
			}
			journeyWiseSegs.get(mapKey).add(seg);
		}
		return journeyWiseSegs;
	}

	private static String getONDCode(List<FlightSegmentTO> segments) {
		List<String> ondCodes = new ArrayList<String>();
		Collections.sort(segments, new Comparator<FlightSegmentTO>() {

			@Override
			public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
				return o1.getSegmentSequence() - o2.getSegmentSequence();
			}
		});

		for (int i = 0; i < segments.size(); i++) {
			IFlightSegment seg = segments.get(i);
			String airportCodes[] = seg.getSegmentCode().split(SEGMENT_SEPERATOR);
			if (i == 0) {
				// First segment
				for (int j = 0; j < airportCodes.length; j++) {
					ondCodes.add(airportCodes[j]);
				}
			} else {
				for (int j = 0; j < airportCodes.length; j++) {
					if (j == 0) {
						continue;
					} else {
						ondCodes.add(airportCodes[j]);
					}
				}
			}
		}

		return generateONDFromAirportList(ondCodes);
	}

	private static String generateONDFromAirportList(List<String> airportCodes) {
		String ond = "";
		for (int i = 0; i < airportCodes.size(); i++) {
			ond += i == 0 ? airportCodes.get(i) : SEGMENT_SEPERATOR + airportCodes.get(i);
		}
		return ond;
	}

	public static List<FlightSegmentTO> populateMissingDataForAnciOfferSearch(List<FlightSegmentTO> oldFlightSegmentTOs,
			List<FlightSegmentTO> flightSegments, Integer adultCount, Integer childCount, Integer infantCount,
			boolean hasReturnSegments) throws ParseException {

		if (oldFlightSegmentTOs != null && oldFlightSegmentTOs.size() > 0) {
			for (FlightSegmentTO flightSegTO : flightSegments) {
				for (FlightSegmentTO oldFLightSegTO : oldFlightSegmentTOs) {
					if (flightSegTO.getFlightRefNumber().contains(oldFLightSegTO.getFlightRefNumber())) {
						flightSegTO.setFareBasisCode(oldFLightSegTO.getFareBasisCode());
						flightSegTO.setBookingClass(oldFLightSegTO.getBookingClass());
						flightSegTO.setFlexiID(oldFLightSegTO.getFlexiID());
						flightSegTO.setAdultCount(adultCount);
						flightSegTO.setChildCount(childCount);
						flightSegTO.setInfantCount(infantCount);
						flightSegTO.setReturnFlag(oldFLightSegTO.isReturnFlag());
						flightSegTO.setParticipatingJourneyReturn(hasReturnSegments);
						flightSegTO.setOndSequence(oldFLightSegTO.getOndSequence());
						flightSegTO.setSegmentSequence(oldFLightSegTO.getSegmentSequence());

						Date inverseSegDepartureDate = getInverseSegmentsDeparture(oldFlightSegmentTOs, flightSegTO);
						if (inverseSegDepartureDate == null) {
							inverseSegDepartureDate = getInverseONDDeparture(oldFlightSegmentTOs, flightSegTO);
						}
						flightSegTO.setInverseSegmentDeparture(inverseSegDepartureDate);
					}
				}
			}
		}
		setONDCodes(flightSegments);
		return flightSegments;
	}

	public static String getFlightRefWithoutLCCTxnRefDetails(String flightRef) {
		String cleanedFlightRef = null;
		if (flightRef != null) {
			cleanedFlightRef = flightRef.split(Pattern.quote("-"))[0];
		}
		return cleanedFlightRef;
	}

	public static Set<String> getWildCardAppendedONDsExcludingOriginal(String ondCode) {
		Set<String> mutatedONDSet = getWildCardAppendedONDs(ondCode);
		mutatedONDSet.remove(ondCode);
		return mutatedONDSet;
	}

	public static Set<String> getWildCardAppendedONDs(String ondCode) {
		Set<String> mutatedONDSet = new HashSet<String>();
		String[] onds = ondCode.split(SEGMENT_SEPERATOR);
		Set<Set<String>> powerSets = Sets.powerSet(Sets.newHashSet(onds[0], onds[onds.length - 1]));
		for (Set<String> set : powerSets) {
			String mutatedSeg = ondCode;
			for (String string : set) {
				mutatedSeg = mutatedSeg.replace(string, AnciOfferConstants.ALL);
			}
			mutatedONDSet.add(mutatedSeg);
		}
		mutatedONDSet.add(AnciOfferConstants.ALL_OPTION);
		return mutatedONDSet;
	}

	public static boolean doesAllOptionExistInCriteria(AnciOfferCriteria criteria, Set<String> allOptionSet, String OND) {
		Set<String> criteriaONDs = criteria.getOnds();
		allOptionSet.remove(OND);
		allOptionSet.remove(AnciOfferConstants.ALL_OPTION);
		return criteriaONDs.containsAll(allOptionSet);
	}

	public static Integer getONDCriteriaPriority(AnciOfferCriteria criteria, Set<String> wildCardONDs) {
		Set<String> criteriaONDs = criteria.getOnds();
		if (criteriaONDs.contains(AnciOfferConstants.ALL_OPTION)) {
			// Return a large number so ALL option is always pushed to last.
			return 100;
		} else {
			return getMaxAllCount(criteriaONDs, wildCardONDs);
		}
	}

	private static int getMaxAllCount(Set<String> criteriaONDs, Set<String> wildCardONDs) {
		criteriaONDs.retainAll(wildCardONDs);
		int maxAllCount = 0;
		for (String cOND : criteriaONDs) {
			int ondAllCount = 0;
			for (String airport : cOND.split(SEGMENT_SEPERATOR)) {
				if (AnciOfferConstants.ALL.equals(airport))
					ondAllCount++;
			}
			if (ondAllCount > maxAllCount)
				maxAllCount = ondAllCount;

		}
		return maxAllCount;
	}

	private static Date getInverseSegmentsDeparture(List<FlightSegmentTO> allSegments, IFlightSegment segment) {
		String inverseSegCode = getInverseSegmentCode(segment.getSegmentCode());
		for (IFlightSegment seg : allSegments) {
			if (seg.getSegmentCode().equals(inverseSegCode)) {
				return seg.getDepartureDateTime();
			}
		}
		return null;
	}

	private static Date getInverseONDDeparture(List<FlightSegmentTO> allSegments, IFlightSegment segment) {
		String inverseONDCode = getInverseONDCode(segment.getSegmentCode());
		for (IFlightSegment seg : allSegments) {
			String ondCode = getOND(seg.getSegmentCode());
			if (ondCode.equals(inverseONDCode)) {
				return seg.getDepartureDateTime();
			}
		}
		return null;
	}

	private static String getInverseONDCode(String segCode) {
		String airports[] = segCode.split(SEGMENT_SEPERATOR);
		String origin = airports[0];
		String destination = airports[airports.length - 1];
		return destination + SEGMENT_SEPERATOR + origin;
	}

	private static String getInverseSegmentCode(String ond) {
		String inverseOND = "";
		String airports[] = ond.split(SEGMENT_SEPERATOR);
		for (int i = airports.length - 1; i >= 0; i--) {
			inverseOND += i == airports.length - 1 ? airports[i] : SEGMENT_SEPERATOR + airports[i];
		}
		return inverseOND;
	}

	private static String getOND(String segCode) {
		String airports[] = segCode.split(SEGMENT_SEPERATOR);
		String origin = airports[0];
		String destination = airports[airports.length - 1];
		return origin + SEGMENT_SEPERATOR + destination;
	}
}
