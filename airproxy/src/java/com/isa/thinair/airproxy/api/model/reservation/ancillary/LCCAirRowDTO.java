package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCAirRowDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int rowNumber;
	private List<LCCAirSeatDTO> airSeats;
	private boolean seatsVacant;
	private List<LCCPassengerTypeQuantityDTO> lCCPassengerTypeQuantityDTO;

	public int getRowNumber() {
		return rowNumber;
	}

	public boolean isSeatsVacant() {
		return seatsVacant;
	}

	public void setSeatsVacant(boolean seatsVacant) {
		this.seatsVacant = seatsVacant;
	}

	public List<LCCPassengerTypeQuantityDTO> getPassengerTypeQuantity() {
		if (lCCPassengerTypeQuantityDTO == null)
			lCCPassengerTypeQuantityDTO = new ArrayList<LCCPassengerTypeQuantityDTO>();
		return lCCPassengerTypeQuantityDTO;
	}

	public void setPassengerTypeQuantity(List<LCCPassengerTypeQuantityDTO> lCCPassengerTypeQuantityDTO) {
		this.lCCPassengerTypeQuantityDTO = lCCPassengerTypeQuantityDTO;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public List<LCCAirSeatDTO> getAirSeats() {
		if (airSeats == null)
			airSeats = new ArrayList<LCCAirSeatDTO>();
		return airSeats;
	}

	public void setAirSeats(List<LCCAirSeatDTO> airSeats) {
		this.airSeats = airSeats;
	}
}
