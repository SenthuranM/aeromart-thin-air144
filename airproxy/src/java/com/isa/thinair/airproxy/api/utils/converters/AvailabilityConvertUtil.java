package com.isa.thinair.airproxy.api.utils.converters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FCCSegBCInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareIdDetailDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OpenReturnPeriodsTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO.BCType;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.ResUniqueIdMapper;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class AvailabilityConvertUtil {

	private static Log logger = LogFactory.getLog(AvailabilityConvertUtil.class);

	/**
	 * Given the BaseAvailRQ, OndConvertUtil, PaxInfoConvertUtil compose and return AvailabilityFlightSeachDTO, the DTO
	 * used by the own airline availability search
	 * 
	 * @param baseAvailRQ
	 * @param userPrincipal
	 * @param auxHelper
	 * @param ondAssembler
	 * @param paxAssm
	 * @param userPrincipal
	 * @return AvailableFlightSearchDTO
	 */
	public static AvailableFlightSearchDTO getAvailableFlightSearchDTO(BaseAvailRQ baseAvailRQ, UserPrincipal userPrincipal,
			boolean withAllFares, AuxillaryHelper auxHelper) throws ModuleException {

		// OndConvertAssembler ondAssm = new OndConvertAssembler(baseAvailRQ.getOriginDestinationInformationList());
		IPaxCountAssembler paxAssm = new PaxCountAssembler(baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		boolean flownOndExist = false;
		List<Integer> pnrSegIds = new ArrayList<Integer>();
		List<Integer> flownPnrSegIds = new ArrayList<Integer>();
		List<Date> modOndDepDateList = new ArrayList<Date>();
		boolean isModifyToSameFare = false;
		boolean eligibleToSameBCMod = false;
		String commonBookingClassCode = "";
		boolean isRouteChange = true;

		Map<Integer, FareIdDetailDTO> flightSegFareDetails = new HashMap<Integer, FareIdDetailDTO>();
		availableFlightSearchDTO.setFlightSegFareDetails(flightSegFareDetails);

		for (OriginDestinationInformationTO ondInfoTO : baseAvailRQ.getOriginDestinationInformationList()) {
			OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
			ondInfoDTO.setOrigin(ondInfoTO.getOrigin());
			ondInfoDTO.setDestination(ondInfoTO.getDestination());
			ondInfoDTO.setDepartureDateTimeStart(ondInfoTO.getDepartureDateTimeStart());
			ondInfoDTO.setDepartureDateTimeEnd(ondInfoTO.getDepartureDateTimeEnd());
			ondInfoDTO.setArrivalDateTimeStart(ondInfoTO.getArrivalDateTimeStart());
			ondInfoDTO.setArrivalDateTimeEnd(ondInfoTO.getArrivalDateTimeEnd());
			ondInfoDTO.setSelectedFltFirstDepartureDateTime(getFirstDepartureDateTime(ondInfoTO.getOrignDestinationOptions()));
			if (ondInfoTO.getPreferredDate() != null) {
				Date prefDTStart = CalendarUtil.getStartTimeOfDate(ondInfoTO.getPreferredDate());
				Date prefDTEnd = CalendarUtil.getEndTimeOfDate(ondInfoTO.getPreferredDate());
				if (prefDTStart.before(ondInfoTO.getDepartureDateTimeStart())) {
					prefDTStart = ondInfoTO.getDepartureDateTimeEnd();
				}

				if (AppSysParamsUtil.isGroundServiceEnabled() && baseAvailRQ.getAvailPreferences().isAddGroundSegment()) {
					ondInfoDTO.setPreferredDateTimeStart(ondInfoTO.getDepartureDateTimeStart());
					ondInfoDTO.setPreferredDateTimeEnd(ondInfoTO.getDepartureDateTimeEnd());
				} else {
					ondInfoDTO.setPreferredDateTimeStart(prefDTStart);
					ondInfoDTO.setPreferredDateTimeEnd(prefDTEnd);
				}
				if (ondInfoTO.getPreferredDate() != null) {
					// to sort by preferred date in return search
					ondInfoDTO.setPreferredDate(ondInfoTO.getPreferredDate());
				}
			}
			ondInfoDTO.setFlightSegmentIds(OndConvertAssembler.extractSegIdsFromOnds(ondInfoTO.getOrignDestinationOptions()));
			ondInfoDTO.setPreferredClassOfService(ondInfoTO.getPreferredClassOfService());
			ondInfoDTO.setPreferredLogicalCabin(ondInfoTO.getPreferredLogicalCabin());
			ondInfoDTO.setPreferredBookingClass(ondInfoTO.getPreferredBookingClass());
			ondInfoDTO.setPreferredBookingType(ondInfoTO.getPreferredBookingType());
			ondInfoDTO.setPreferredBundledFarePeriodId(ondInfoTO.getPreferredBundleFarePeriodId());
			ondInfoDTO.setExistingFlightSegIds(ondInfoTO.getExistingFlightSegIds());
			pnrSegIds.addAll(ResUniqueIdMapper.getPnrSegIds(ondInfoTO.getExistingPnrSegRPHs()));
			ondInfoDTO.setFlownOnd(ondInfoTO.isFlownOnd());
			ondInfoDTO.setOldPerPaxFare(ondInfoTO.getOldPerPaxFare());
			FareTO oldPerPaxFareTO = getOwnOldPerPaxFareTO(ondInfoTO);
			ondInfoDTO.setOldPerPaxFareTO(oldPerPaxFareTO);
			ondInfoDTO.setUnTouchedOnd(ondInfoTO.isUnTouchedOnd());
			flownOndExist = flownOndExist || ondInfoTO.isFlownOnd();
			if (ondInfoTO.isFlownOnd()) {
				flownPnrSegIds.addAll(ResUniqueIdMapper.getPnrSegIds(ondInfoTO.getExistingPnrSegRPHs()));
			}

			if (!ondInfoTO.isFlownOnd()
					&& (ondInfoTO.getExistingFlightSegIds() == null || ondInfoTO.getExistingFlightSegIds().size() == 0)
					&& ondInfoTO.getPreferredDate() != null) {
				modOndDepDateList.add(ondInfoTO.getPreferredDate());
			}

			if (ondInfoTO.getHubTimeDetailMap() != null && ondInfoTO.getHubTimeDetailMap().size() > 0) {
				ondInfoDTO.setHubTimeDetailMap(ondInfoTO.getHubTimeDetailMap());
			}

			availableFlightSearchDTO.addOriginDestination(ondInfoDTO);
			if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs() && ondInfoTO.getUnTouchedResSegList() != null
					&& oldPerPaxFareTO != null) {

				eligibleToSameBCMod = false;
				for (Integer fltSegId : OndConvertAssembler.extractSegIdsFromOnds(ondInfoTO.getOrignDestinationOptions())) {
					FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
					fareIdDetailDTO.setFareId(oldPerPaxFareTO.getFareId());
					fareIdDetailDTO.setFareType(new Integer(oldPerPaxFareTO.getFareType()));
					flightSegFareDetails.put(fltSegId, fareIdDetailDTO);
				}

				if (ondInfoTO.getDateChangedResSegList() != null || ondInfoTO.isSameFlightModification()) {
					isRouteChange = false;
				}

			} else {
				if (!ondInfoTO.isFlownOnd() && AppSysParamsUtil.isSameFareModificationEnabled()
						&& ondInfoTO.getDateChangedResSegList() != null && oldPerPaxFareTO != null) {
					isModifyToSameFare = AirproxyModuleUtils.getFareBD().isModifyToSameFareEnabled(oldPerPaxFareTO.getFareId());

					if (isModifyToSameFare) {
						eligibleToSameBCMod = false;
						for (Integer fltSegId : OndConvertAssembler
								.extractSegIdsFromOnds(ondInfoTO.getOrignDestinationOptions())) {
							FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
							fareIdDetailDTO.setFareId(oldPerPaxFareTO.getFareId());
							flightSegFareDetails.put(fltSegId, fareIdDetailDTO);
						}
						flightSegFareDetails = fillSameReturnGroupSegmentFareId(flightSegFareDetails, ondInfoTO);
					}
				} else if (!ondInfoTO.isFlownOnd() && AppSysParamsUtil.isSameFareModificationEnabled()
						&& ondInfoTO.isEligibleToSameBCMod() && oldPerPaxFareTO != null) {
					isModifyToSameFare = AirproxyModuleUtils.getFareBD().isModifyToSameFareEnabled(oldPerPaxFareTO.getFareId());
					if (isModifyToSameFare) {
						eligibleToSameBCMod = true;
						commonBookingClassCode = oldPerPaxFareTO.getBookingClassCode();
					}
				}

				if (AppSysParamsUtil.isEnforceSameHigher()
						&& (ondInfoTO.getDateChangedResSegList() != null || ondInfoTO.isSameFlightModification())) {
					isRouteChange = false;
				}
			}

			ondInfoDTO.setSegmentBookingClass(ondInfoTO.getSegmentBookingClassSelection());
			ondInfoDTO.setSpecificFlightsAvailability(ondInfoTO.isSpecificFlightsAvailability());
			ondInfoDTO.setDepartureCitySearch(ondInfoTO.isDepartureCitySearch());
			ondInfoDTO.setArrivalCitySearch(ondInfoTO.isArrivalCitySearch());

		}

		if (eligibleToSameBCMod) {
			flightSegFareDetails = fillFlightSegFareBCDetails(flightSegFareDetails,
					baseAvailRQ.getOriginDestinationInformationList(), commonBookingClassCode);
		}

		if (pnrSegIds.size() > 0) {
			Map<Integer, String> fltSegBkgClasses = auxHelper.getFltSegWiseBkgClasses(pnrSegIds);
			availableFlightSearchDTO.setFltSegWiseExtBookingClasses(fltSegBkgClasses);
			availableFlightSearchDTO.setFltSegWiseExtCabinClasses(auxHelper.getFltSegWiseCabinClasses(pnrSegIds));
		}
		if (flownPnrSegIds.size() > 0) {
			availableFlightSearchDTO.setFltSegWiseLastFQDates(auxHelper.getFltSegWiseLastFQDates(flownPnrSegIds));
		}

		// Setting the open return dummy segment details for availability search
		if (baseAvailRQ.getTravelPreferences().isOpenReturn()
				&& availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() == 1) {
			availableFlightSearchDTO
					.addOriginDestination(availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).cloneAndSwapOnD());
		}
		availableFlightSearchDTO.setReturnValidityPeriodId(baseAvailRQ.getTravelPreferences().getValidityId());

		availableFlightSearchDTO.setAdultCount(paxAssm.getAdultCount());
		availableFlightSearchDTO.setChildCount(paxAssm.getChildCount());
		availableFlightSearchDTO.setInfantCount(paxAssm.getInfantCount());

		availableFlightSearchDTO.setPosAirport(userPrincipal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(userPrincipal.getAgentCode());
		if (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_GDS) {
			availableFlightSearchDTO
					.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		} else {
			availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.getSalesChannelName(userPrincipal.getSalesChannel())));
		}

		availableFlightSearchDTO.setAvailabilityRestrictionLevel(baseAvailRQ.getAvailPreferences().getRestrictionLevel());
		availableFlightSearchDTO.setFlightsPerOndRestriction(baseAvailRQ.getAvailPreferences().getFlightsPerOndRestriction());
		availableFlightSearchDTO.setBookingPaxType(baseAvailRQ.getAvailPreferences().getBookingPaxType());
		availableFlightSearchDTO.setFareCategoryType(baseAvailRQ.getAvailPreferences().getFareCategoryType());
		if (AppSysParamsUtil.isEnforceSameHigher()
				&& (!isRouteChange || !baseAvailRQ.getAvailPreferences().isRequoteFlightSearch())) {
			availableFlightSearchDTO.setModifiedOndFareId(baseAvailRQ.getAvailPreferences().getOldFareID());
			availableFlightSearchDTO.setModifiedOndFareType(baseAvailRQ.getAvailPreferences().getOldFareType());
			if (!baseAvailRQ.getAvailPreferences().isAllowOverrideSameOrHigherFareMod()) {
				availableFlightSearchDTO.setModifiedOndPaxFareAmount(baseAvailRQ.getAvailPreferences().getOldFareAmount());
				availableFlightSearchDTO.setEnforceFareCheckForModOnd(true);
			}
			availableFlightSearchDTO.setModifiedFlightSegments(baseAvailRQ.getAvailPreferences().getModifiedSegmentsList());
		}

		// half return parameters
		availableFlightSearchDTO.setHalfReturnFareQuote(baseAvailRQ.getAvailPreferences().isHalfReturnFareQuote());
		setFirstAndLastArrivalDates(availableFlightSearchDTO, baseAvailRQ.getAvailPreferences().getExistingSegmentsList(),
				baseAvailRQ.getAvailPreferences().getModifiedSegmentsList());
		availableFlightSearchDTO
				.setInverseSegmentsOfModifiedFlightSegment(baseAvailRQ.getAvailPreferences().getInverseSegmentsList());
		availableFlightSearchDTO.setInboundFareQuote(baseAvailRQ.getAvailPreferences().isInboundFareModified());
		availableFlightSearchDTO.setInverseFareID(baseAvailRQ.getAvailPreferences().getInverseFareID());
		availableFlightSearchDTO.setAppIndicator(baseAvailRQ.getAvailPreferences().getAppIndicator().toString());
		availableFlightSearchDTO.setFareCalendarSearch(baseAvailRQ.getAvailPreferences().isFareCalendarSearch());
		availableFlightSearchDTO.setPreferredLanguage(baseAvailRQ.getAvailPreferences().getPreferredLanguage());
		availableFlightSearchDTO.setInboundOutboundChanged(isInboundOutboundChanged(availableFlightSearchDTO));
		availableFlightSearchDTO.setFromNameChange(baseAvailRQ.getAvailPreferences().isFromNameChange());
		availableFlightSearchDTO.setPreserveOndOrder(baseAvailRQ.getAvailPreferences().isPreserveOndOrder());

		if (!baseAvailRQ.getAvailPreferences().isSkipValidityFQ() && AppSysParamsUtil.isModifyFQOnInitialSegmentFQDate()) {

			if (baseAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
				if (baseAvailRQ.getAvailPreferences().getModifiedSegmentsList() != null
						&& baseAvailRQ.getAvailPreferences().getModifiedSegmentsList().size() > 0) {
					Date maxValidDate = null;
					Date lastFQDate = null;
					for (FlightSegmentDTO segment : baseAvailRQ.getAvailPreferences().getModifiedSegmentsList()) {
						lastFQDate = segment.getLastFareQuotedDate();
						maxValidDate = segment.getTicketValidTill();
						break;
					}
					if (maxValidDate == null) {
						maxValidDate = getMaxValidDate(baseAvailRQ.getAvailPreferences());
					}
					if (maxValidDate != null
					// && maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())
					) {
						availableFlightSearchDTO.setLastFareQuotedDate(lastFQDate);
						availableFlightSearchDTO.setFQOnLastFQDate(true);
						if (maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())) {
							availableFlightSearchDTO.setFQWithinValidity(true);
						}
					}
				}
			} else if (flownOndExist) {
				Date maxValidDate = baseAvailRQ.getAvailPreferences().getTicketValidTill();
				Date lastFQDate = baseAvailRQ.getAvailPreferences().getLastFareQuotedDate();
				if (maxValidDate != null && lastFQDate != null
				// && maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())
				) {
					availableFlightSearchDTO.setLastFareQuotedDate(lastFQDate);
					availableFlightSearchDTO.setFQOnLastFQDate(true);
					if (maxValidDate.after(CalendarUtil.getCurrentSystemTimeInZulu())) {
						availableFlightSearchDTO.setFQWithinValidity(true);
					}

					// Calculate Penalty when extend ticket validity
					if (modOndDepDateList != null && modOndDepDateList.size() > 0
							&& availableFlightSearchDTO.isFQWithinValidity()) {

						Collections.sort(modOndDepDateList);

						Date modFirstDepDate = modOndDepDateList.get(0);

						if (modFirstDepDate.after(maxValidDate)) {
							availableFlightSearchDTO.setFQWithinValidity(false);
						}

					}

				}
			}

		}

		for (Integer ondSequence : baseAvailRQ.getAvailPreferences().getQuoteOndFlexi().keySet()) {
			OriginDestinationInfoDTO ondInfoDTO = availableFlightSearchDTO.getOndInfo(ondSequence);
			if (ondInfoDTO != null) {
				ondInfoDTO.setQuoteFlexi(baseAvailRQ.getAvailPreferences().getQuoteOndFlexi().get(ondSequence));

			}
		}

		if (baseAvailRQ.getTravelPreferences().isOpenReturnConfirm()) {
			availableFlightSearchDTO.setConfirmOpenReturn(true);
		}
		if (baseAvailRQ.getAvailPreferences().isAllowFlightSearchAfterCutOffTime()) {
			availableFlightSearchDTO.setAllowFlightSearchAfterCutOffTime(true);
		} else {
			availableFlightSearchDTO.setAllowFlightSearchAfterCutOffTime(false);
		}
		if (baseAvailRQ.getAvailPreferences().isAllowDoOverbookAfterCutOffTime()) {
			availableFlightSearchDTO.setAllowDoOverbookAfterCutOffTime(true);
		} else {
			availableFlightSearchDTO.setAllowDoOverbookAfterCutOffTime(false);
		}

		availableFlightSearchDTO.setSelectedAgentCode(baseAvailRQ.getAvailPreferences().getTravelAgentCode());
		availableFlightSearchDTO.setFixedAgentFareOnly(baseAvailRQ.getAvailPreferences().isFixedFareAgent());
		availableFlightSearchDTO.setGoshoFareAgentOnly(baseAvailRQ.getAvailPreferences().isGoshoFareAgent());
		availableFlightSearchDTO.setSearchAllBC(baseAvailRQ.getAvailPreferences().isSearchAllBC());
		availableFlightSearchDTO.setBookingType(baseAvailRQ.getTravelPreferences().getBookingType());

		availableFlightSearchDTO.setQuoteFares(withAllFares);
		availableFlightSearchDTO.setRequoteFlightSearch(baseAvailRQ.getAvailPreferences().isRequoteFlightSearch());
		availableFlightSearchDTO.setMultiCitySearch(baseAvailRQ.getAvailPreferences().isMultiCitySearch());
		availableFlightSearchDTO.setSourceFlightInCutOffTime(baseAvailRQ.getAvailPreferences().isSourceFlightInCutOffTime());
		availableFlightSearchDTO.setModifyBooking(baseAvailRQ.getAvailPreferences().isModifyBooking());
		availableFlightSearchDTO.setExcludedCharges(baseAvailRQ.getAvailPreferences().getExcludedCharges());
		availableFlightSearchDTO.setPromoApplicable(baseAvailRQ.getAvailPreferences().isPromoApplicable());
		availableFlightSearchDTO.setPromoCode(baseAvailRQ.getAvailPreferences().getPromoCode());
		availableFlightSearchDTO.setBankIdentificationNo(baseAvailRQ.getAvailPreferences().getBankIdentificationNumber());
		availableFlightSearchDTO.setExistRetGropMap(baseAvailRQ.getAvailPreferences().getExistRetGropMap());
		availableFlightSearchDTO.setBundledFareApplicable(baseAvailRQ.getAvailPreferences().isBundledFareApplicable());
		availableFlightSearchDTO.setPointOfSale(baseAvailRQ.getAvailPreferences().getPointOfSale());
		availableFlightSearchDTO.setIncludeHRTFares(baseAvailRQ.getAvailPreferences().isIncludeHRTFares());

		if (!StringUtil.isNullOrEmpty(availableFlightSearchDTO.getSelectedAgentCode())) {
			availableFlightSearchDTO.setAgentCode(availableFlightSearchDTO.getSelectedAgentCode());
		}

		availableFlightSearchDTO.setHasPrivAddSeatsInCutover(baseAvailRQ.getAvailPreferences().isHasPrivAddSeatsInCutover());
		availableFlightSearchDTO.setFlightDateFareMap(baseAvailRQ.getAvailPreferences().getFlightDateFareMap());
		availableFlightSearchDTO
				.setAllowDoOverbookBeforeCutOffTime(baseAvailRQ.getAvailPreferences().isAllowDoOverbookBeforeCutOffTime());
		availableFlightSearchDTO
				.setAllowDoOverbookAfterCutOffTime(baseAvailRQ.getAvailPreferences().isAllowDoOverbookAfterCutOffTime());

		if (availableFlightSearchDTO.isRequoteFlightSearch()) {
			availableFlightSearchDTO.setPnr(baseAvailRQ.getAvailPreferences().getPnr());
		}
		
		availableFlightSearchDTO.setCustomerId(baseAvailRQ.getCustomerId());

		return availableFlightSearchDTO;
	}

	private static Map<Integer, FareIdDetailDTO> fillSameReturnGroupSegmentFareId(
			Map<Integer, FareIdDetailDTO> flightSegFareDetails, OriginDestinationInformationTO ondInfoTO) throws ModuleException {
		Map<Integer, Integer> dateChangedPnrSegFlightSeg = new HashMap<Integer, Integer>();
		dateChangedPnrSegFlightSeg = AirproxyModuleUtils.getFareBD()
				.getSameReturnGroupSegDetails(ondInfoTO.getDateChangedResSegList());
		if (dateChangedPnrSegFlightSeg.size() > 1) {
			for (Map.Entry<Integer, Integer> entry : dateChangedPnrSegFlightSeg.entrySet()) {
				if (!ondInfoTO.getDateChangedResSegList().contains(entry.getKey())) {
					FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
					fareIdDetailDTO.setFareId(getOwnOldPerPaxFareTO(ondInfoTO).getFareId());
					flightSegFareDetails.put(entry.getValue(), fareIdDetailDTO);
				}
			}
		}
		return flightSegFareDetails;
	}

	private static Map<Integer, FareIdDetailDTO> fillFlightSegFareBCDetails(Map<Integer, FareIdDetailDTO> flightSegFareDetails,
			List<OriginDestinationInformationTO> ondInfoTO, String bookingClassCode) {
		for (OriginDestinationInformationTO tmpOndInfoTO : ondInfoTO) {
			if (!tmpOndInfoTO.isFlownOnd()) {
				for (Integer fltSegId : OndConvertAssembler.extractSegIdsFromOnds(tmpOndInfoTO.getOrignDestinationOptions())) {
					FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
					fareIdDetailDTO.setBookingClassCode(bookingClassCode);
					flightSegFareDetails.put(fltSegId, fareIdDetailDTO);
				}
			}
		}
		return flightSegFareDetails;
	}

	public static Date getMaxValidDate(AvailPreferencesTO availPreferences) {
		int validMonths = AppSysParamsUtil.getDefaultTicketValidityPeriodInMonths();
		Date firstDepDate = null;
		for (FlightSegmentDTO fltSeg : availPreferences.getModifiedSegmentsList()) {
			if (firstDepDate == null || firstDepDate.after(fltSeg.getDepartureDateTimeZulu())) {
				firstDepDate = fltSeg.getDepartureDateTimeZulu();
			}
		}
		if (firstDepDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(firstDepDate.getTime());
			cal.add(Calendar.MONTH, validMonths);
			return cal.getTime();
		}
		return null;
	}

	public static void setFirstAndLastArrivalDates(AvailableFlightSearchDTO availableFlightSearchDTO,
			Collection<FlightSegmentDTO> existingSegmentsList, Collection<FlightSegmentDTO> modifiedSegmentsList) {

		if (existingSegmentsList != null && existingSegmentsList.size() > 0) {

			Set<Integer> fltSegIds = new HashSet<Integer>();
			Set<Integer> unconfirmedORSegIDsForModSegs = new HashSet<Integer>();

			if (modifiedSegmentsList != null && modifiedSegmentsList.size() > 0) {

				for (FlightSegmentDTO fs : modifiedSegmentsList) {
					fltSegIds.add(fs.getSegmentId());
				}

				// This is to filter the open return segments which are not yet confirmed when modifying an out bound
				// segment of
				// an Open return segment.

				for (FlightSegmentDTO msl : modifiedSegmentsList) {
					for (FlightSegmentDTO esl : existingSegmentsList) {
						if (!fltSegIds.contains(esl.getSegmentId()) && esl.getInterlineReturnGroupKey() != null
								&& msl.getInterlineReturnGroupKey() != null
								&& esl.getInterlineReturnGroupKey().equals(msl.getInterlineReturnGroupKey())
								&& esl.isOpenReturnSegment() && esl.getOpenRetConfirmBefore() != null) {
							unconfirmedORSegIDsForModSegs.add(esl.getSegmentId());
						}
					}
				}
			}
			Date fDepZ = null;
			Date lArrZ = null;

			List<FlightSegmentDTO> allExstSegments = new ArrayList<FlightSegmentDTO>(existingSegmentsList);
			Collections.sort(allExstSegments);
			for (FlightSegmentDTO fs : allExstSegments) {
				if (!(ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(fs.getPnrSegmentStatus()))) {
					if (!fltSegIds.contains(fs.getSegmentId()) && !unconfirmedORSegIDsForModSegs.contains(fs.getSegmentId())) {
						if (fDepZ == null || fDepZ.compareTo(fs.getDepartureDateTimeZulu()) > 0) {
							fDepZ = fs.getDepartureDateTimeZulu();
						}
						if (lArrZ == null || lArrZ.compareTo(fs.getArrivalDateTimeZulu()) < 0) {
							lArrZ = fs.getArrivalDateTimeZulu();
						}
					}
				}
			}
			availableFlightSearchDTO.setFirstDepartureDateTimeZulu(fDepZ);
			availableFlightSearchDTO.setLastArrivalDateTimeZulu(lArrZ);

			// if (lArrZ != null
			// &&
			// lArrZ.before(availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getDepartureDateTimeStart()))
			// {
			// availableFlightSearchDTO.setOutBoundModifiedToAFutureDepartureDateThanInbound(true);
			// }
		}

	}

	/**
	 * Check if the modification causes a date change where inbound and outbound reverses.
	 */
	public static boolean isInboundOutboundChanged(AvailableFlightSearchDTO availableFlightSearchDTO) {

		List<OriginDestinationInfoDTO> modifiedONDInfos = availableFlightSearchDTO.getOriginDestinationInfoDTOs();

		Date earliestModifiedDepartureDate = null;
		for (OriginDestinationInfoDTO ondInfo : modifiedONDInfos) {
			if (earliestModifiedDepartureDate == null
					|| earliestModifiedDepartureDate.after(ondInfo.getDepartureDateTimeStart())) {
				earliestModifiedDepartureDate = ondInfo.getDepartureDateTimeStart();
			}
		}

		if (availableFlightSearchDTO.getInverseSegmentsOfModifiedFlightSegment() != null
				&& availableFlightSearchDTO.getInverseSegmentsOfModifiedFlightSegment().size() > 0) {
			List<FlightSegmentDTO> inverseSegmentsOfModified = new ArrayList<FlightSegmentDTO>(
					availableFlightSearchDTO.getInverseSegmentsOfModifiedFlightSegment());
			Collections.sort(inverseSegmentsOfModified);
			Date inverseSegmentDepartureDate = inverseSegmentsOfModified.get(0).getDepatureDate();
			if (availableFlightSearchDTO.isInboundFareQuote()) {
				if (earliestModifiedDepartureDate.before(inverseSegmentDepartureDate)) {
					return true;
				}
			} else {
				if (inverseSegmentDepartureDate.before(earliestModifiedDepartureDate)) {
					return true;
				}
			}

		}

		return false;
	}

	/**
	 * Returns the last departure date per segment code.
	 * 
	 * @param existingSegmentsList
	 *            The segment list containing all the current segments for the current reservation.
	 * @return Map<String,Date> with the last departure dates for each segment code. Key -> Segment Code , Value -> Last
	 *         departure date for the segment
	 */
	private static Map<String, Date> getSegCodeWiseLastDepartureDates(Collection<FlightSegmentDTO> existingSegmentsList) {
		Map<String, Date> segCodeWiseLastDepartures = new HashMap<String, Date>();

		for (FlightSegmentDTO existingSeg : existingSegmentsList) {
			// If current segments departure time is after the existing segment of same segment code.
			if (segCodeWiseLastDepartures.get(existingSeg.getSegmentCode()) == null || segCodeWiseLastDepartures
					.get(existingSeg.getSegmentCode()).before(existingSeg.getDepartureDateTimeZulu())) {
				segCodeWiseLastDepartures.put(existingSeg.getSegmentCode(), existingSeg.getDepartureDateTimeZulu());
			}
		}

		return segCodeWiseLastDepartures;
	}

	/**
	 * Given baseAvailRQ and fareSegChargeTO compose OndRebuildCriteria for recreate the OndFareDTOs
	 * 
	 * @param baseAvailRQ
	 * @param fareSegChargeTO
	 * @return
	 */
	public static OndRebuildCriteria getOndRebuildCriteria(BaseAvailRQ baseAvailRQ, FareSegChargeTO fareSegChargeTO) {
		IPaxCountAssembler paxAssm = new PaxCountAssembler(baseAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		return getOndRebuildCriteria(paxAssm, fareSegChargeTO, baseAvailRQ.getTravelPreferences().getBookingType(),
				baseAvailRQ.getAvailPreferences().isAllowDoOverbookAfterCutOffTime());
	}

	/**
	 * 
	 * @param paxAssm
	 * @param fareSegChargeTO
	 * @param allowOverbookAfterCutoffTime
	 * @return
	 */
	public static OndRebuildCriteria getOndRebuildCriteria(IPaxCountAssembler paxAssm, FareSegChargeTO fareSegChargeTO,
			String bookingType, boolean allowOverbookAfterCutoffTime) {
		QuotedFareRebuildDTO quotedFareRBuilder = new QuotedFareRebuildDTO(fareSegChargeTO, paxAssm, null, bookingType,
				allowOverbookAfterCutoffTime);
		return quotedFareRBuilder.getFirstOndRebuildCriteria();
	}

	public static FlightAvailRS getFlightRS(AvailableFlightDTO availableFlightDTO, FlightAvailRQ flightAvailRQ, String agentCode)
			throws ModuleException {
		FlightAvailRS flightAvailRS = new FlightAvailRS();
		IPaxCountAssembler paxUtil = new PaxCountAssembler(flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();
		// For open return search set the dummy inbound details
		if (flightAvailRQ.getTravelPreferences().isOpenReturn()) {
			OriginDestinationInformationTO originDestinationInformationTO = flightAvailRQ
					.getOriginDestinationInformation(OndSequence.OUT_BOUND);
			OriginDestinationInformationTO arrivalOnD = flightAvailRQ.addNewOriginDestinationInformation();
			arrivalOnD.setOrigin(originDestinationInformationTO.getDestination());
			arrivalOnD.setDestination(originDestinationInformationTO.getOrigin());
		}
		OndConvertUtil.getOndList(flightAvailRQ.getOriginDestinationInformation(OndSequence.OUT_BOUND),
				availableFlightDTO.getAvailableOndFlights(OndSequence.OUT_BOUND),
				selectedFlightDTO == null ? null : selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND), false, false,
				false, flightAvailRQ, null);
		OndConvertUtil.getOndList(flightAvailRQ.getOriginDestinationInformation(OndSequence.IN_BOUND),
				availableFlightDTO.getAvailableOndFlights(OndSequence.IN_BOUND),
				selectedFlightDTO == null ? null : selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND), true, false,
				false, flightAvailRQ, null);
		flightAvailRS.getOriginDestinationInformationList().addAll(flightAvailRQ.getOriginDestinationInformationList());

		if (selectedFlightDTO != null && selectedFlightDTO.getOndWiseMinimumSegFareFlights() != null
				&& !selectedFlightDTO.getOndWiseMinimumSegFareFlights().isEmpty()) {

			if (selectedFlightDTO.getSelectedOndSegFlight(OndSequence.OUT_BOUND) != null) {
				List<AvailableIBOBFlightSegment> selOutboundFlights = Arrays
						.asList(selectedFlightDTO.getSelectedOndSegFlight(OndSequence.OUT_BOUND));
				OndConvertUtil.getOndList(flightAvailRQ.getOriginDestinationInformation(OndSequence.OUT_BOUND),
						selOutboundFlights, selectedFlightDTO.getSelectedOndSegFlight(OndSequence.OUT_BOUND), false, false, true,
						flightAvailRQ, null);
			}

			if (selectedFlightDTO.getSelectedOndSegFlight(OndSequence.IN_BOUND) != null) {
				List<AvailableIBOBFlightSegment> selInboundFlights = Arrays
						.asList(selectedFlightDTO.getSelectedOndSegFlight(OndSequence.IN_BOUND));
				OndConvertUtil.getOndList(flightAvailRQ.getOriginDestinationInformation(OndSequence.IN_BOUND), selInboundFlights,
						selectedFlightDTO.getSelectedOndSegFlight(OndSequence.IN_BOUND), true, false, true, flightAvailRQ, null);
			}

		}

		PriceInfoTO priceInfoTO = FareConvertUtil.getSelectedPriceFltInfo(availableFlightDTO.getSelectedFlight(), paxUtil,
				agentCode, CommonUtil.getAppIndicator(flightAvailRQ.getAvailPreferences().getAppIndicator().toString()), false,
				flightAvailRQ.getLogicalCabinClassSelection(), false, flightAvailRQ);

		PriceInfoTO segFltPriceInfoTO = FareConvertUtil.getSelectedPriceFltInfo(availableFlightDTO.getSelectedFlight(), paxUtil,
				agentCode, CommonUtil.getAppIndicator(flightAvailRQ.getAvailPreferences().getAppIndicator().toString()), false,
				flightAvailRQ.getLogicalCabinClassSelection(), true, flightAvailRQ);

		flightAvailRS.setSelectedPriceFlightInfo(priceInfoTO);
		flightAvailRS.setSelectedSegmentFlightPriceInfo(segFltPriceInfoTO);
		flightAvailRS.setOpenReturnOptionsTO(OndConvertUtil.getOpenReturnOptions(availableFlightDTO.getSelectedFlight()));
		// flightAvailRS.setOnHoldEnabled(availableFlightDTO.isOnHoldEnabled());
		return flightAvailRS;
	}

	public static FlightAvailRS getMultiCityFlightRS(AvailableFlightDTO availableFlightDTO, FlightAvailRQ flightAvailRQ,
			String agentCode) throws ModuleException {

		IPaxCountAssembler paxAssm = new PaxCountAssembler(flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

		SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();

		Iterator<Integer> availOndSegIdItr = availableFlightDTO.getOndSequenceIds().iterator();

		while (availOndSegIdItr.hasNext()) {
			Integer ondSegId = availOndSegIdItr.next();
			boolean isIBFlight = false;
			if (selectedFlightDTO != null && selectedFlightDTO.getSelectedOndFlight(ondSegId) != null) {
				isIBFlight = selectedFlightDTO.getSelectedOndFlight(ondSegId).isInboundFlightSegment();
			}

			OndConvertUtil.getOndList(flightAvailRQ.getOriginDestinationInformation(ondSegId),
					availableFlightDTO.getAvailableOndFlights(ondSegId),
					selectedFlightDTO == null ? null : selectedFlightDTO.getSelectedOndFlight(ondSegId), isIBFlight, false, false,
					flightAvailRQ, ondSegId);
		}

		FlightAvailRS flightAvailRS = new FlightAvailRS();
		flightAvailRS.getOriginDestinationInformationList().addAll(flightAvailRQ.getOriginDestinationInformationList());

		PriceInfoTO priceInfoTO = FareConvertUtil.getSelectedPriceFltInfo(selectedFlightDTO, paxAssm, agentCode,
				CommonUtil.getAppIndicator(flightAvailRQ.getAvailPreferences().getAppIndicator().toString()), false,
				flightAvailRQ.getLogicalCabinClassSelection(), false, flightAvailRQ);

		flightAvailRS.setSelectedPriceFlightInfo(priceInfoTO);
		flightAvailRS.setOpenReturnOptionsTO(OndConvertUtil.getOpenReturnOptions(selectedFlightDTO));
		// flightPriceRS.setOnHoldEnabled(selectedFlightDTO.isOnHoldEnabled());
		return flightAvailRS;
	}

	public static FlightPriceRS getFlightRS(SelectedFlightDTO selectedFlightDTO, FlightPriceRQ priceQuoteRQ, String agentCode)
			throws ModuleException {
		IPaxCountAssembler paxAssm = new PaxCountAssembler(priceQuoteRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

		List<AvailableIBOBFlightSegment> selectedONDFlights = selectedFlightDTO.getSelectedOndFlights();

		// For open return search set the dummy inbound details
		if (priceQuoteRQ.getTravelPreferences().isOpenReturn()) {
			OriginDestinationInformationTO originDestinationInformationTO = priceQuoteRQ
					.getOriginDestinationInformation(OndSequence.OUT_BOUND);
			OriginDestinationInformationTO arrivalOnD = priceQuoteRQ.addNewOriginDestinationInformation();
			arrivalOnD.setOrigin(originDestinationInformationTO.getDestination());
			arrivalOnD.setDestination(originDestinationInformationTO.getOrigin());
		} else {
			splitONDsBasedOnSelectedQuote(priceQuoteRQ, selectedONDFlights);
		}

		// AvailableIBOBFlightSegment selIn = selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND);
		// List<AvailableIBOBFlightSegment> inFlts = new ArrayList<AvailableIBOBFlightSegment>();
		for (int ondSequence = 0; ondSequence < selectedONDFlights.size(); ondSequence++) {
			AvailableIBOBFlightSegment selOut = selectedFlightDTO.getSelectedOndFlight(ondSequence);
			List<AvailableIBOBFlightSegment> outFlts = new ArrayList<AvailableIBOBFlightSegment>();
			outFlts.add(selOut);
			OndConvertUtil.getOndList(priceQuoteRQ.getOriginDestinationInformation(selOut.getOndSequence()), outFlts, selOut,
					selOut.isInboundFlightSegment(), true, false, priceQuoteRQ, ondSequence);
		}

		if (selectedFlightDTO.getOndWiseMinimumSegFareFlights() != null
				&& !selectedFlightDTO.getOndWiseMinimumSegFareFlights().isEmpty()) {
			for (int ondSequence = 0; ondSequence < selectedONDFlights.size(); ondSequence++) {
				AvailableIBOBFlightSegment selOut = selectedFlightDTO.getSelectedOndSegFlight(ondSequence);
				List<AvailableIBOBFlightSegment> outFlts = new ArrayList<AvailableIBOBFlightSegment>();
				outFlts.add(selOut);
				OndConvertUtil.getOndList(priceQuoteRQ.getOriginDestinationInformation(selOut.getOndSequence()), outFlts, selOut,
						selOut.isInboundFlightSegment(), true, true, priceQuoteRQ, ondSequence);
			}
		}

		FlightPriceRS flightPriceRS = new FlightPriceRS();
		flightPriceRS.getOriginDestinationInformationList().addAll(priceQuoteRQ.getOriginDestinationInformationList());

		PriceInfoTO priceInfoTO = FareConvertUtil.getSelectedPriceFltInfo(selectedFlightDTO, paxAssm, agentCode,
				CommonUtil.getAppIndicator(priceQuoteRQ.getAvailPreferences().getAppIndicator().toString()), true,
				priceQuoteRQ.getLogicalCabinClassSelection(), false, priceQuoteRQ);
		updateServiceTaxes(priceInfoTO, selectedFlightDTO);

		PriceInfoTO segPriceInfoTO = FareConvertUtil.getSelectedPriceFltInfo(selectedFlightDTO, paxAssm, agentCode,
				CommonUtil.getAppIndicator(priceQuoteRQ.getAvailPreferences().getAppIndicator().toString()), true,
				priceQuoteRQ.getLogicalCabinClassSelection(), true, priceQuoteRQ);
		updateServiceTaxes(segPriceInfoTO, selectedFlightDTO);

		HashMap<String, List<String>> subJourneyGroupMap = new HashMap<String, List<String>>();
		if (selectedFlightDTO.getSelectedDateAllFlights() != null && !selectedFlightDTO.getSelectedDateAllFlights().isEmpty()) {
			for (AvailableIBOBFlightSegment avaINOBFltSegment : selectedFlightDTO.getSelectedDateAllFlights()) {
				for (FlightSegmentDTO fltSegmentDTO : avaINOBFltSegment.getFlightSegmentDTOs()) {
					avaINOBFltSegment.getSubJourneyGroup();
					List<String> fltSegIdList = new ArrayList<String>();
					String subJourneyId = avaINOBFltSegment.getSubJourneyGroup() + "";
					if (subJourneyGroupMap.get(subJourneyId) != null && subJourneyGroupMap.get(subJourneyId).size() > 0) {
						fltSegIdList = subJourneyGroupMap.get(subJourneyId);
					}
					fltSegIdList.add(FlightRefNumberUtil.composeFlightRPH(fltSegmentDTO));
					subJourneyGroupMap.put(subJourneyId, fltSegIdList);
				}
			}
		}

		flightPriceRS.setSubJourneyGroupMap(subJourneyGroupMap);
		flightPriceRS.setSelectedPriceFlightInfo(priceInfoTO);
		flightPriceRS.setSelectedSegmentFlightPriceInfo(segPriceInfoTO);
		flightPriceRS.setOpenReturnOptionsTO(OndConvertUtil.getOpenReturnOptions(selectedFlightDTO));
		// flightPriceRS.setOnHoldEnabled(selectedFlightDTO.isOnHoldEnabled());
		return flightPriceRS;
	}

	private static void splitONDsBasedOnSelectedQuote(BaseAvailRQ availRQ, List<AvailableIBOBFlightSegment> selectedONDFlights) {
		if (selectedONDFlights != null
				&& selectedONDFlights.size() > availRQ.getOrderedOriginDestinationInformationList().size()) {

			List<AvailableIBOBFlightSegment> selectedFlts = new ArrayList<>(selectedONDFlights);
			Iterator<AvailableIBOBFlightSegment> sltItr = null;
			List<OriginDestinationInformationTO> ondNewList = new ArrayList<>();
			List<OriginDestinationInformationTO> nonMatchedList = new ArrayList<>();
			for (OriginDestinationInformationTO requestedOndInfo : availRQ.getOrderedOriginDestinationInformationList()) {
				sltItr = selectedFlts.iterator();
				boolean found = false;
				while (sltItr.hasNext()) {
					AvailableIBOBFlightSegment selectedResponseOndOption = sltItr.next();
					if (isResponseWithinRequestedOnd(requestedOndInfo, selectedResponseOndOption)) {
						ondNewList.add(requestedOndInfo);
						sltItr.remove();
						found = true;
						break;
					}
				}

				if (!found) {
					nonMatchedList.add(requestedOndInfo);// 2
				}

			}

			for (AvailableIBOBFlightSegment ondOption : selectedFlts) {
				for (OriginDestinationInformationTO ondInfo : nonMatchedList) {
					if (isSameOrigin(ondInfo, ondOption) || isSameDestination(ondInfo, ondOption)
					/* && isOnDOptionDatesWithinRequestedDates(ondInfo, ondOption) */) { // AEROMART-3770
						OriginDestinationInformationTO newOnd = ondInfo.clone();

						newOnd.setOrigin(ondOption.getOriginAirport());
						newOnd.setDestination(ondOption.getDestinationAirport());
						newOnd.setOrignDestinationOptions(new ArrayList<>());
						newOnd.setOriginDestinationSegFareOptions(new ArrayList<>());
						ondNewList.add(newOnd);
						break;
					}
				}
			}

			if (ondNewList.size() == selectedONDFlights.size()) {
				availRQ.getOriginDestinationInformationList().clear();
				availRQ.getOriginDestinationInformationList().addAll(ondNewList);
				availRQ.getOrderedOriginDestinationInformationList();
			}
		}
	}

	private static boolean isResponseWithinRequestedOnd(OriginDestinationInformationTO requestedOndInfo,
			AvailableIBOBFlightSegment selectedResponseOndOption) {
		return isSameOrigin(requestedOndInfo, selectedResponseOndOption)
				&& isSameDestination(requestedOndInfo, selectedResponseOndOption)
				&& isOnDOptionDatesWithinRequestedDates(requestedOndInfo, selectedResponseOndOption);
	}

	private static boolean isSameOrigin(OriginDestinationInformationTO ondInfo, AvailableIBOBFlightSegment ondOption) {
		return ondInfo.getOrigin().equals(ondOption.getOriginAirport());
	}

	private static boolean isSameDestination(OriginDestinationInformationTO ondInfo, AvailableIBOBFlightSegment ondOption) {
		return ondInfo.getDestination().equals(ondOption.getDestinationAirport());
	}

	private static boolean isOnDOptionDatesWithinRequestedDates(OriginDestinationInformationTO ondInfo,
			AvailableIBOBFlightSegment ondOption) {
		return ondOption.getFirstFlightSegment().getDepartureDateTime().after(ondInfo.getDepartureDateTimeStart())
				&& ondOption.getFirstFlightSegment().getDepartureDateTime().before(ondInfo.getDepartureDateTimeEnd());
	}

	// Assume that service tax applicable for all passenger types
	private static void updateServiceTaxes(PriceInfoTO priceInfoTO, SelectedFlightDTO selectedFlightDTO) {
		double serviceTaxAmount = 0.0;
		// Here assumed that only percentages are available for service taxes
		Map<String, QuotedChargeDTO> serviceTaxes;
		if (!CollectionUtils.isEmpty(selectedFlightDTO.getSelectedOndFlights())) {
			serviceTaxes = selectedFlightDTO.getSelectedOndFlights().get(0).getServiceTaxes();
			if (!CollectionUtils.isEmpty(serviceTaxes)) {
				Map<String, List<String>> serviceTaxExclusionsUnifiedMap = getUnifiedServiceTaxExclusionMap(selectedFlightDTO);

				if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
					serviceTaxAmount += getServiceTaxAmountForTaxes(priceInfoTO, serviceTaxes, serviceTaxExclusionsUnifiedMap);
					serviceTaxAmount += getServiceTaxAmountForSurcharges(priceInfoTO, serviceTaxes,
							serviceTaxExclusionsUnifiedMap);

					for (BaseFareTO baseFareTo : priceInfoTO.getFareTypeTO().getBaseFares()) {
						for (QuotedChargeDTO serviceTax : serviceTaxes.values()) {
							if (serviceTax.isChargeValueInPercentage()) {
								serviceTaxAmount += baseFareTo.getAmount().doubleValue() * serviceTax.getChargeValuePercentage()
										/ 100.0;
							}
						}
					}
					priceInfoTO.getFareTypeTO().setServiceTaxAmount(AccelAeroCalculator.parseBigDecimal(serviceTaxAmount));
					// priceInfoTO.getFareTypeTO().setTotalPrice(AccelAeroCalculator.add(priceInfoTO.getFareTypeTO().getTotalPrice(),
					// priceInfoTO.getFareTypeTO().getServiceTaxAmount()));
				}

			}
		}
	}

	private static double getServiceTaxAmountForSurcharges(PriceInfoTO priceInfoTO, Map<String, QuotedChargeDTO> serviceTaxes,
			Map<String, List<String>> serviceTaxExclusionsUnifiedMap) {
		double serviceTaxAmount = 0.0;
		if (!CollectionUtils.isEmpty(priceInfoTO.getFareTypeTO().getSurcharges())) {
			for (SurchargeTO surchargeTO : priceInfoTO.getFareTypeTO().getSurcharges()) {
				for (Entry<String, QuotedChargeDTO> serviceTax : serviceTaxes.entrySet()) {
					List<String> excludedTaxes = serviceTaxExclusionsUnifiedMap.get(serviceTax.getKey());
					if (excludedTaxes == null || !excludedTaxes.contains(surchargeTO.getSurchargeCode())) {
						if (serviceTax.getValue().isChargeValueInPercentage()) {
							serviceTaxAmount += surchargeTO.getAmount().doubleValue()
									* serviceTax.getValue().getChargeValuePercentage() / 100.0;
						}
					}
				}
			}
		}
		return serviceTaxAmount;
	}

	private static double getServiceTaxAmountForTaxes(PriceInfoTO priceInfoTO, Map<String, QuotedChargeDTO> serviceTaxes,
			Map<String, List<String>> serviceTaxExclusionsUnifiedMap) {
		double serviceTaxAmount = 0.0;
		if (!CollectionUtils.isEmpty(priceInfoTO.getFareTypeTO().getTaxes())) {
			for (TaxTO tax : priceInfoTO.getFareTypeTO().getTaxes()) {
				for (Entry<String, QuotedChargeDTO> serviceTax : serviceTaxes.entrySet()) {
					List<String> excludedTaxes = serviceTaxExclusionsUnifiedMap.get(serviceTax.getKey());
					if (excludedTaxes == null || !excludedTaxes.contains(tax.getTaxCode())) {
						if (serviceTax.getValue().isChargeValueInPercentage()) {
							serviceTaxAmount += tax.getAmount().doubleValue() * serviceTax.getValue().getChargeValuePercentage()
									/ 100.0;
						}
					}
				}
			}
		}
		return serviceTaxAmount;
	}

	private static Map<String, List<String>> getUnifiedServiceTaxExclusionMap(SelectedFlightDTO selectedFlightDTO) {
		Map<String, List<String>> serviceTaxExclusionsUnifiedMap = new HashMap<>();
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedFlightDTO.getSelectedOndFlights()) {
			if (!CollectionUtils.isEmpty(availableIBOBFlightSegment.getServiceTaxExcludedCharges())) {
				for (Entry<String, List<String>> exclusion : availableIBOBFlightSegment.getServiceTaxExcludedCharges()
						.entrySet()) {
					List<String> excludedList = serviceTaxExclusionsUnifiedMap.get(exclusion.getKey());
					if (!CollectionUtils.isEmpty(excludedList)) {
						excludedList.addAll(exclusion.getValue());
					} else {
						serviceTaxExclusionsUnifiedMap.put(exclusion.getKey(), exclusion.getValue());
					}
				}
			}
		}
		return serviceTaxExclusionsUnifiedMap;
	}

	public static FlightPriceRS getFlightRS(ChangeFaresDTO changeFaresDTO, BaseAvailRQ priceQuoteRQ, String agentCode)
			throws ModuleException {
		FlightPriceRS flightPriceRS = new FlightPriceRS();

		Collection<OriginDestinationInformationTO> retOriginDestinationInformationTOList = new ArrayList<OriginDestinationInformationTO>();
		List<OriginDestinationInformationTO> orderedOndInfoList = priceQuoteRQ.getOrderedOriginDestinationInformationList();

		boolean isSplitForBusInvoked = isSplitOperationforBusInvoked(changeFaresDTO.getExistingFareChargesSeatsSegmentsDTOs());

		if (priceQuoteRQ.getTravelPreferences().isOpenReturn()) {
			if (changeFaresDTO.hasInboundOnds()) {
				int ondSeq = 0;
				OriginDestinationInformationTO retOriginDestinationInformationTO = null;
				for (OriginDestinationInformationTO originDestinationInformationTO : orderedOndInfoList) {
					if (ondSeq == OndSequence.OUT_BOUND) {
						retOriginDestinationInformationTO = new OriginDestinationInformationTO();
						retOriginDestinationInformationTO.setOrigin(originDestinationInformationTO.getDestination());
						retOriginDestinationInformationTO.setDestination(originDestinationInformationTO.getOrigin());
						retOriginDestinationInformationTO.setReturnFlag(true);
						retOriginDestinationInformationTOList.add(retOriginDestinationInformationTO);
					}
					ondSeq++;
				}
			}
		}

		if (isSplitForBusInvoked && !priceQuoteRQ.getTravelPreferences().isOpenReturn()) {
			Map<Integer, FlightSegmentDTO> segMap = new HashMap<Integer, FlightSegmentDTO>();
			Map<Integer, OndFareDTO> ondFareMap = new HashMap<>();

			for (OndFareDTO ondFareDTO : changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs()) {
				LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap = ondFareDTO.getSegmentsMap();
				for (Entry<Integer, FlightSegmentDTO> entry : segmentsMap.entrySet()) {
					segMap.put(entry.getKey(), entry.getValue());
				}

				if (!ondFareMap.containsKey(ondFareDTO.getOndSequence())) {
					ondFareMap.put(ondFareDTO.getOndSequence(), ondFareDTO);
				} else {
					throw new ModuleException("Invalid state");
				}
			}

			if (ondFareMap.keySet().size() > priceQuoteRQ.getOrderedOriginDestinationInformationList().size()) {

				List<OndFareDTO> ondFares = new ArrayList<>(ondFareMap.values());
				Iterator<OndFareDTO> sltItr = null;
				List<OriginDestinationInformationTO> ondNewList = new ArrayList<>();
				List<OriginDestinationInformationTO> nonMatchedList = new ArrayList<>();
				for (OriginDestinationInformationTO requestedOndInfo : priceQuoteRQ
						.getOrderedOriginDestinationInformationList()) {
					sltItr = ondFares.iterator();
					boolean found = false;
					while (sltItr.hasNext()) {
						OndFareDTO selectedOndFare = sltItr.next();
						if (isResponseWithinRequestedOnd(requestedOndInfo, selectedOndFare)) {
							ondNewList.add(requestedOndInfo);
							sltItr.remove();
							found = true;
							break;
						}
					}

					if (!found) {
						nonMatchedList.add(requestedOndInfo);
					}

				}

				for (OndFareDTO ondOption : ondFares) {
					for (OriginDestinationInformationTO ondInfo : nonMatchedList) {
						if (isSameOrigin(ondInfo, ondOption) || isSameDestination(ondInfo, ondOption)
								&& isOnDOptionDatesWithinRequestedDates(ondInfo, ondOption)) {
							OriginDestinationInformationTO newOnd = ondInfo.clone();

							newOnd.setOrigin(getFirstSegment(ondOption).getFromAirport());
							newOnd.setDestination(getLastSegment(ondOption).getToAirport());
							newOnd.setOrignDestinationOptions(new ArrayList<>());
							newOnd.setOriginDestinationSegFareOptions(new ArrayList<>());
							ondNewList.add(newOnd);
							break;
						}
					}
				}

				if (ondNewList.size() == ondFareMap.keySet().size()) {
					priceQuoteRQ.getOriginDestinationInformationList().clear();
					priceQuoteRQ.getOriginDestinationInformationList().addAll(ondNewList);
					priceQuoteRQ.getOrderedOriginDestinationInformationList();
				}
			}

			for (OriginDestinationInformationTO originDestinationInformationTO : priceQuoteRQ
					.getOrderedOriginDestinationInformationList()) {
				for (OriginDestinationOptionTO ond : originDestinationInformationTO.getOrignDestinationOptions()) {
					for (FlightSegmentTO seg : ond.getFlightSegmentList()) {
						if (segMap.get(seg.getFlightSegId()) != null) {
							seg.setOperationType(segMap.get(seg.getFlightSegId()).getOperationTypeID());
						}
					}
				}
			}

		}

		priceQuoteRQ.getOriginDestinationInformationList().addAll(retOriginDestinationInformationTOList);
		flightPriceRS.getOriginDestinationInformationList().addAll(orderedOndInfoList);

		orderedOndInfoList = priceQuoteRQ.getOrderedOriginDestinationInformationList();
		Map<Integer, OriginDestinationInformationTO> ondSeqWiseInfoTo = new HashMap<Integer, OriginDestinationInformationTO>();
		int ondSeq = 0;
		for (OriginDestinationInformationTO originDestinationInformationTO : orderedOndInfoList) {
			ondSeqWiseInfoTo.put(ondSeq, originDestinationInformationTO);
			ondSeqWiseInfoTo.get(ondSeq).setOrignDestinationOptions(new ArrayList<OriginDestinationOptionTO>());
			ondSeq++;
		}

		Map<Integer, Boolean> ondSequenceReturnMap = changeFaresDTO.getOndSequenceReturnMap();
		int ondSize = ondSeqWiseInfoTo.size();
		for (Entry<Integer, OriginDestinationInformationTO> ondInfoEntry : ondSeqWiseInfoTo.entrySet()) {
			int ondSequence = ondInfoEntry.getKey();
			// If only two OndInfo is available do return validation
			if (ondSequence == OndSequence.IN_BOUND && ondSize == 2) {
				if (changeFaresDTO.getSegmentCount(OndSequence.IN_BOUND) == 0
						&& ondSeqWiseInfoTo.get(OndSequence.OUT_BOUND) == null
						&& changeFaresDTO.getSegmentCount(OndSequence.OUT_BOUND) > 0) {
					OndConvertUtil.populateFareSummaryInfoForOndList(ondInfoEntry.getValue(),
							changeFaresDTO.getSegmentIds(OndSequence.OUT_BOUND),
							changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs(), priceQuoteRQ);
				} else {
					OndConvertUtil.populateFareSummaryInfoForOndList(ondInfoEntry.getValue(),
							changeFaresDTO.getSegmentIds(ondSequence), changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs(),
							priceQuoteRQ);
				}
			} else {
				boolean isIBFlight = false;
				if (ondSequenceReturnMap != null && ondSequenceReturnMap.size() > 2
						&& ondSequenceReturnMap.get(ondSequence) != null) {
					isIBFlight = ondSequenceReturnMap.get(ondSequence);
				}
				ondInfoEntry.getValue().setReturnFlag(isIBFlight);
				OndConvertUtil.populateFareSummaryInfoForOndList(ondInfoEntry.getValue(),
						changeFaresDTO.getSegmentIds(ondSequence), changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs(),
						priceQuoteRQ);
			}
		}

		IPaxCountAssembler paxAssm = new PaxCountAssembler(priceQuoteRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		PriceInfoTO priceInfoTO = FareConvertUtil.getChangedPriceFltInfo(changeFaresDTO, paxAssm, agentCode,
				CommonUtil.getAppIndicator(priceQuoteRQ.getAvailPreferences().getAppIndicator().toString()),
				priceQuoteRQ.getAvailPreferences().getQuoteOndFlexi(), priceQuoteRQ);

		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
			priceInfoTO.getFareTypeTO().setOndFlexiSelection(priceQuoteRQ.getAvailPreferences().getOndFlexiSelected());
			priceInfoTO.getFareTypeTO().setPromotionTO(changeFaresDTO.getApplicablePromotionDetails());
		}
		if (changeFaresDTO != null && changeFaresDTO.getLastFareQuotedDate() != null) {
			priceInfoTO.setLastFareQuotedDate(changeFaresDTO.getLastFareQuotedDate());
			priceInfoTO.setFQWithinValidity(changeFaresDTO.isFQWithinValidity());
		}

		flightPriceRS.setSelectedPriceFlightInfo(priceInfoTO);

		OpenReturnPeriodsTO openCloneReturnPeriodsTO = OndFareUtil
				.getOpenReturnPeriods(changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs());
		flightPriceRS.setOpenReturnOptionsTO(OndConvertUtil.getOpenReturnOptions(openCloneReturnPeriodsTO));

		return flightPriceRS;
	}

	private static boolean isResponseWithinRequestedOnd(OriginDestinationInformationTO requestedOndInfo,
			OndFareDTO selectedOndFare) {
		return isSameOrigin(requestedOndInfo, selectedOndFare) && isSameDestination(requestedOndInfo, selectedOndFare)
				&& isOnDOptionDatesWithinRequestedDates(requestedOndInfo, selectedOndFare);
	}

	private static boolean isOnDOptionDatesWithinRequestedDates(OriginDestinationInformationTO ondInfo, OndFareDTO ondOption) {
		return getFirstSegment(ondOption).getDepartureDateTime().after(ondInfo.getDepartureDateTimeStart())
				&& getFirstSegment(ondOption).getDepartureDateTime().before(ondInfo.getDepartureDateTimeEnd());

	}

	private static boolean isSameDestination(OriginDestinationInformationTO ondInfo, OndFareDTO ondOption) {
		return ondInfo.getDestination().equals(getLastSegment(ondOption).getToAirport());
	}

	private static boolean isSameOrigin(OriginDestinationInformationTO ondInfo, OndFareDTO ondOption) {
		return ondInfo.getOrigin().equals(getFirstSegment(ondOption).getFromAirport());
	}

	private static FlightSegmentDTO getLastSegment(OndFareDTO ondOption) {
		return ondOption.getSegmentsMap().values().iterator().next();
	}

	private static FlightSegmentDTO getFirstSegment(OndFareDTO ondOption) {
		List<FlightSegmentDTO> list = new ArrayList<>(ondOption.getSegmentsMap().values());
		return list.get(list.size() - 1);
	}

	public static RollForwardFlightSearchDTO getRollForwardFlightSearch(Collection<LCCClientReservationSegment> colsegs,
			Date startDate, Date endDate, Integer adultCount, Integer childCount, Integer infantCount, boolean overBook,
			String agentCode) {
		RollForwardFlightSearchDTO rollForwardFlightSearchDTO = new RollForwardFlightSearchDTO();

		rollForwardFlightSearchDTO.setStartDate(startDate);
		rollForwardFlightSearchDTO.setEndDate(endDate);
		rollForwardFlightSearchDTO.setAdultCount(adultCount);
		rollForwardFlightSearchDTO.setChildCount(childCount);
		rollForwardFlightSearchDTO.setInfantCount(infantCount);
		rollForwardFlightSearchDTO.setAgentCode(agentCode);
		LCCClientReservationSegment[] sortedArr = SortUtil.sortByDepDate(new HashSet<LCCClientReservationSegment>(colsegs));

		Map<Integer, List<RollForwardFlightDTO>> allFltMap = new HashMap<Integer, List<RollForwardFlightDTO>>();
		if (sortedArr != null) {
			for (LCCClientReservationSegment lccClientResSeg : sortedArr) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(lccClientResSeg.getStatus())) {
					if (allFltMap.get(lccClientResSeg.getJourneySequence() - 1) != null) {
						allFltMap.get(lccClientResSeg.getJourneySequence() - 1)
								.add(populateRollFwdFlightSegmentDTO(lccClientResSeg));
					} else {
						List<RollForwardFlightDTO> flightList = new ArrayList<RollForwardFlightDTO>();
						flightList.add(populateRollFwdFlightSegmentDTO(lccClientResSeg));
						allFltMap.put(lccClientResSeg.getJourneySequence() - 1, flightList);
					}
				}
			}
		}
		rollForwardFlightSearchDTO.setAllFltMap(allFltMap);
		rollForwardFlightSearchDTO.calculateOutboundInboundDifference();
		rollForwardFlightSearchDTO.calculateFirstDeptStartEndGap();
		if (overBook) {
			rollForwardFlightSearchDTO.setBcType(BookingClass.BookingClassType.OVERBOOK);
		}
		return rollForwardFlightSearchDTO;
	}

	private static RollForwardFlightDTO populateRollFwdFlightSegmentDTO(LCCClientReservationSegment resSeg) {
		RollForwardFlightDTO flightDTO = new RollForwardFlightDTO();
		flightDTO.setFlightRefNo(resSeg.getFlightSegmentRefNumber());
		flightDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber()));
		flightDTO.setSegmentCode(resSeg.getSegmentCode());
		flightDTO.setSegmentSeq(resSeg.getSegmentSeq());
		flightDTO.setDepartureDateTime(resSeg.getDepartureDate());
		flightDTO.setBookingClass(resSeg.getFareTO().getBookingClassCode());
		flightDTO.setCabinClass(resSeg.getCabinClassCode());
		flightDTO.setFareId(resSeg.getFareTO().getFareId());
		flightDTO.setFareRuleId(resSeg.getFareTO().getFareRuleID());
		if (BookingClass.BookingClassType.OPEN_RETURN.equals(resSeg.getBookingType())) {
			flightDTO.setOpenReturn(true);
		}
		return flightDTO;
	}

	public static AllFaresDTO getAllFaresDTO(FareAvailRQ fareAvailRQ, FlightBD flightBD) throws ModuleException {
		AllFaresDTO allFaresDTO = new AllFaresDTO();

		int ondSeq = 0;
		for (OriginDestinationInformationTO ondInfo : fareAvailRQ.getOrderedOriginDestinationInformationList()) {
			if (ondInfo.getOrignDestinationOptions() != null) {
				for (OriginDestinationOptionTO ondOptTO : ondInfo.getOrignDestinationOptions()) {
					if (ondOptTO.getFlightSegmentList() != null) {
						for (FlightSegmentTO fltSeg : ondOptTO.getFlightSegmentList()) {
							allFaresDTO.addAllFlightSegmentDTO(ondSeq, getFlightSegmentDTO(fltSeg, flightBD));
						}
					}
				}
			}
			ondSeq++;
		}

		if (fareAvailRQ.getSelectedOBFltRefNo() != null) {
			allFaresDTO.setSelectedSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedOBFltRefNo()));
		}

		if (fareAvailRQ.getSelectedIBFltRefNo() != null) {
			allFaresDTO.setSelectedSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedIBFltRefNo()));
		}

		return allFaresDTO;
	}

	public static FlightSegmentDTO getFlightSegmentDTO(FlightSegmentTO fltTO, FlightBD flightBD) throws ModuleException {
		FlightSegmentDTO fltDTO = new FlightSegmentDTO();
		FlightSegement fltSeg = flightBD
				.getFlightSegment(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltTO.getFlightRefNumber()));
		fltDTO.setSegmentCode(fltSeg.getSegmentCode());
		fltDTO.setFlightNumber(fltTO.getFlightNumber());
		fltDTO.setFromAirport(SegmentUtil.getFromAirport(fltSeg.getSegmentCode()));
		fltDTO.setToAirport(SegmentUtil.getToAirport(fltSeg.getSegmentCode()));
		fltDTO.setDepartureDateTime(fltSeg.getEstTimeDepatureLocal());
		fltDTO.setArrivalDateTime(fltSeg.getEstTimeArrivalLocal());
		fltDTO.setDepartureDateTimeZulu(fltSeg.getEstTimeDepatureZulu());
		fltDTO.setArrivalDateTimeZulu(fltSeg.getEstTimeArrivalZulu());
		fltDTO.setSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltTO.getFlightRefNumber()));
		fltDTO.setFlightId(fltSeg.getFlightId());
		return fltDTO;
	}

	public static FareFilteringCriteria getFareFilteringCriteria(FareAvailRQ fareAvailRQ, UserPrincipal userPrincipal) {
		FareFilteringCriteria ffc = new FareFilteringCriteria();
		TravelPreferencesTO travelPref = fareAvailRQ.getTravelPreferences();
		AvailPreferencesTO availPref = fareAvailRQ.getAvailPreferences();
		IPaxCountAssembler paxAssm = new PaxCountAssembler(fareAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

		ffc.setBookingType(travelPref.getBookingType());
		ffc.setNumberOfAdults(paxAssm.getAdultCount());
		ffc.setNumberOfChilds(paxAssm.getChildCount());
		ffc.setNumberOfInfants(paxAssm.getInfantCount());
		ffc.setBookingPaxType(availPref.getBookingPaxType());
		ffc.setFareCategoryType(availPref.getFareCategoryType());
		// TODO Fix with open return
		// ffc.setReturnFlag(fareAvailRQ.isReturn() || travelPref.isOpenReturn());

		ffc.setAgentCode(availPref.getTravelAgentCode());
		ffc.setExcludeNonLowestPublicFares(fareAvailRQ.isExcludeNonLowestPublicFares());
		ffc.setChannelCode(
				SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.getSalesChannelName(userPrincipal.getSalesChannel())));
		ffc.setOndShowFaresWithFlexi(fareAvailRQ.getAvailPreferences().getOndFlexiSelected());
		ffc.setExcludeOnewayPublicFaresFromReturnJourneys(fareAvailRQ.isExcludeOnewayPublicFaresFromReturnJourneys());
		return ffc;
	}

	public static FareAvailRS getFareAvailRS(AllFaresDTO allFaresDTO, AvailableFlightSearchDTO availableFlightSearchDTO) {
		FareAvailRS fareAvailRS = new FareAvailRS();

		LinkedHashMap<Integer, SegmentFaresDTO> segFareDTOMap = allFaresDTO.getSegmentsFaresDTOMap();
		int paxCount = availableFlightSearchDTO.getAdultCount() + availableFlightSearchDTO.getChildCount();
		boolean isSelectedSegmentWithSameBC = false;

		for (Integer segId : segFareDTOMap.keySet()) {

			String bookedFltBookingClass = availableFlightSearchDTO.getBookedFlightBookingClass(segId);
			SegmentFaresDTO segFareDTO = segFareDTOMap.get(segId);
			Set<Integer> fareTypes = segFareDTO.getSegmentsFares().keySet();
			FlightSegmentDTO fltSegDTO = allFaresDTO.getFlightSegmentDTO(segId);
			// if (allFaresDTO.getInboundFlightSegmentDTOs() != null
			// && allFaresDTO.getInboundFlightSegmentDTOs().keySet().contains(segId)) {
			// fltSegDTO = allFaresDTO.getInboundFlightSegmentDTOs().get(segId);
			// }
			// if (allFaresDTO.getOutboundFlightSegmentDTOs() != null
			// && allFaresDTO.getOutboundFlightSegmentDTOs().keySet().contains(segId)) {
			// fltSegDTO = allFaresDTO.getOutboundFlightSegmentDTOs().get(segId);
			// }
			// Populate segment wise information
			SegmentInvFareTO segmentInvFare = new SegmentInvFareTO();
			segmentInvFare.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(fltSegDTO));

			for (Integer fareType : fareTypes) {
				LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allFareInvSumryMap = allFaresDTO
						.getSegmentFaresOfGivenType(segId, fareType);
				if (allFareInvSumryMap != null && !allFareInvSumryMap.values().isEmpty()) {

					// Fare types
					InvFareTypeTO invFareTypeTO = new InvFareTypeTO();
					invFareTypeTO.setFareOndCode(allFaresDTO.getOndCodeForFareType(fareType));
					invFareTypeTO.setFareType(fareType);

					for (AllFaresBCInventorySummaryDTO allFareInvSummryDTO : allFareInvSumryMap.values()) {
						FCCSegBCInvSummaryDTO bcInvSummary = allFareInvSummryDTO.getBcInventorySummaryDTO();

						// boolean isSameBookingClassBeingSearched =
						// bcInvSummary.getBookingCode().equals(bookedFltBookingClass);
						boolean isNestedSeatsAvailable = false;
						if (AppSysParamsUtil.showNestedAvailableSeats() && bcInvSummary.getAvailableMaxSeats() == 0
								&& bcInvSummary.getAvailableNestedSeats() > 0) {
							isNestedSeatsAvailable = true;
						}

						if (bcInvSummary.getAvailableMaxSeats() > 0 || bcInvSummary.isSameBookingClassAsExisting()
								|| bcInvSummary.isFlownOnd() || isNestedSeatsAvailable) {

							// BC Alloc
							InvFareAllocTO invFareAlloc = new InvFareAllocTO();
							invFareAlloc.setBookingCode(bcInvSummary.getBookingCode());
							invFareAlloc.setCcCode(allFareInvSummryDTO.getCcCode());
							invFareAlloc.setLogicalCabinClass(allFareInvSummryDTO.getLogicalCabinclass());
							BCType bcType = BCType.NONSTD;
							if (bcInvSummary.isFixedFlag()) {
								bcType = BCType.FIXED;
							} else if (bcInvSummary.isStandardFlag()) {
								bcType = BCType.STD;
							}
							if (allFaresDTO.getSelectedSegmentId().equals(segId) && bcInvSummary.isSameBookingClassAsExisting()) {
								isSelectedSegmentWithSameBC = true;
							}

							invFareAlloc.setAvailableNestedSeats(bcInvSummary.getAvailableNestedSeats());
							invFareAlloc.setBcType(bcType);
							invFareAlloc.setBcInvStatus(bcInvSummary.getBcInvStatus());
							invFareAlloc.setAllocatedSeats(bcInvSummary.getAllocatedSeats());
							invFareAlloc.setSoldSeats(bcInvSummary.getSoldSeats());
							invFareAlloc.setOnholdSeats(bcInvSummary.getOnholdSeats());
							invFareAlloc.setAvailableMaxSeats(bcInvSummary.getAvailableMaxSeats());
							invFareAlloc.setAvailableSeats(bcInvSummary.getAvailableSeats());
							invFareAlloc.setOnholdRestricted(bcInvSummary.getOnholdRestricted());
							invFareAlloc.setSameBookingClassBeingSearch(bcInvSummary.isSameBookingClassAsExisting());
							invFareAlloc.setBookedFlightCabinBeingSearched(bcInvSummary.isBookedFlightCabinBeingSearched());
							invFareAlloc.setFlownOnd(bcInvSummary.isFlownOnd());
							invFareAlloc.setNestRank(bcInvSummary.getNestedRank());
							invFareTypeTO.addInvFareAlloc(invFareAlloc);
							invFareAlloc.setActualSeatsOnHold(bcInvSummary.getActualSeatsOnHold());
							invFareAlloc.setActualSeatsSold(bcInvSummary.getActualSeatsSold());
							invFareAlloc.setCommonAllSegments(allFareInvSummryDTO.getFareCommonAllSegments(fareType, bcInvSummary,
									paxCount, isSelectedSegmentWithSameBC));

							Collection<FareSummaryDTO> fareSummaryDTOs = null;
							if (fareType == FareTypes.HALF_RETURN_FARE) {
								fareSummaryDTOs = allFareInvSummryDTO.getFareSummaryDTOs().values();
							} else {
								fareSummaryDTOs = allFareInvSummryDTO.getFareSummaryDTOsApplicable(fareType).values();
							}
							for (FareSummaryDTO fareSummaryDTO : fareSummaryDTOs) {
								// Fare
								FareRuleFareDTO fareRuleFare = new FareRuleFareDTO();

								fareRuleFare.setFareRuleCode(fareSummaryDTO.getFareRuleCode());
								// fareRuleFare.setFareRuleCode(fareSummaryDTO.getMasterFareRuleCode());
								fareRuleFare.setFareBasisCode(fareSummaryDTO.getFareBasisCode());
								fareRuleFare.setBookingClassCode(fareSummaryDTO.getBookingClassCode());
								fareRuleFare.setAdultFareAmount(
										AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)));
								fareRuleFare.setChildFareAmount(
										AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)));
								fareRuleFare.setInfantFareAmount(
										AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)));
								fareRuleFare.setAdultFareApplicable(fareSummaryDTO.isAdultApplicability());
								fareRuleFare.setChildFareApplicable(fareSummaryDTO.isChildApplicability());
								fareRuleFare.setInfantFareApplicable(fareSummaryDTO.isInfantApplicability());
								fareRuleFare.setFareId(fareSummaryDTO.getFareId());
								fareRuleFare.setVisibleAgents(fareSummaryDTO.getVisibleAgents());
								fareRuleFare.setVisibleChannelNames(fareSummaryDTO.getVisibleChannelNames());
								fareRuleFare.setFareRuleId(fareSummaryDTO.getFareRuleID());

								String agentComissionType = fareSummaryDTO.getAgentCommissionType();
								BigDecimal agentCommissionAmount = fareSummaryDTO.getAgentCommissionAmount();
								if (agentComissionType == null || agentCommissionAmount == null) {
									fareRuleFare.setApplicableAgentCommission("0.00");
								} else {
									if (agentComissionType.equals(FareRule.CHARGE_TYPE_PF)) {
										fareRuleFare.setApplicableAgentCommission(
												AccelAeroCalculator.formatAsDecimal(agentCommissionAmount) + "%");
									} else {
										fareRuleFare.setApplicableAgentCommission(
												AccelAeroCalculator.formatAsDecimal(agentCommissionAmount));
									}

								}
								invFareAlloc.addFareRuleFare(fareRuleFare);
							}
						}
					}
					if (invFareTypeTO.hasInvFareAllocations()) {
						segmentInvFare.addInvetoryFareType(invFareTypeTO);
					}
				}
			}
			fareAvailRS.addSegmentInvFare(segmentInvFare);
		}

		return fareAvailRS;
	}

	public static int getAvailabilityRestrictionLevel(boolean viewFlightsWithLesserSeats, String bookingClassType) {
		if (viewFlightsWithLesserSeats || BookingClassUtil.byPassAvailableSeatsCheck(bookingClassType)) {
			return AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION;
		} else {
			return AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;
		}
	}

	private static FareTO getOwnOldPerPaxFareTO(OriginDestinationInformationTO ondInfoTO) throws ModuleException {
		if (ondInfoTO.getOldPerPaxFareTOList() != null) {
			Set<String> ownAirlineCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
			String busCarrier = AirproxyModuleUtils.getFlightBD().getDefaultBusCarrierCode();
			for (FareTO fareTO : ondInfoTO.getOldPerPaxFareTOList()) {
				if (fareTO != null && (ownAirlineCodes.contains(fareTO.getCarrierCode())
						|| (busCarrier != null && busCarrier.equals(fareTO.getCarrierCode()))
						|| ReservationApiUtils.isCodeShareCarrier(fareTO.getCarrierCode()))) {
					return fareTO;
				}
			}
		}
		return null;
	}

	private static Date getFirstDepartureDateTime(List<OriginDestinationOptionTO> originDestinationOptions) {
		if (originDestinationOptions != null && !originDestinationOptions.isEmpty()) {
			List<Date> list = new ArrayList<>();
			for (OriginDestinationOptionTO optionTO : originDestinationOptions) {
				list.add(optionTO.getFirstDepartureDateTimeByRef());

			}
			if (!list.isEmpty()) {
				Collections.sort(list);
				return list.get(0);
			}
		}

		return null;
	}

	private static boolean isSplitOperationforBusInvoked(Collection<OndFareDTO> ondFares) {

		boolean splitOperationForBusInvoked = false;

		for (OndFareDTO fareDto : ondFares) {
			if (fareDto.isSplitOperationForBusInvoked()) {
				splitOperationForBusInvoked = true;
				break;
			}
		}

		return splitOperationForBusInvoked;
	}

}
