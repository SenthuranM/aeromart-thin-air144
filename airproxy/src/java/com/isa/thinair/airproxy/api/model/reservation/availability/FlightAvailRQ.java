package com.isa.thinair.airproxy.api.model.reservation.availability;

public class FlightAvailRQ extends BaseAvailRQ {

	private static final long serialVersionUID = 1L;

	public FlightAvailRQ() {
	}

	public FlightAvailRQ(BaseAvailRQ baseAvailRQ) {
		super(baseAvailRQ);
	}
}
