package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;

public class BlacklisPaxReservationTO implements Serializable {

	/**
	 * @author subash
	 */
	private static final long serialVersionUID = 1L;
	
	private String pnr;
	
	private String fName;
	
	private String lName;
	
	private String fullName;
	
	private String pnrStatus;
	
	private String passportNo;
	
	private int nationalityCode;
	
	private String nationality;
	
	private Date dateOfBirth;
	
	private String blacklistType;
	
	private String effectiveFrom;
	
	private String effectiveTo;
	
	private Date validUntil;
	
	private String flightNo;
	
	private String status;
	
	private String depatureDate;
	
	private String arrivalDate;
	
	private String isActioned;
	
	private Long blacklistReservationId;

	private Integer blacklistPaxId;
	
	private Integer pnrPaxId;
	
	private String reasonToAllow;

	private Integer version;
	
	private String dummyBooking;
	
	private String reasonForWhitelisted;
	
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPnrStatus() {
		return pnrStatus;
	}

	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public int getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(int nationality) {
		this.nationalityCode = nationality;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getBlacklistType() {
		return blacklistType;
	}

	public void setBlacklistType(String blacklistType) {
		this.blacklistType = blacklistType;
	}

	public String getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public String getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(String effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public String getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(String depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsActioned() {
		return isActioned;
	}

	public void setIsActioned(String isActioned) {
		this.isActioned = isActioned;
	}

	public Long getBlacklistReservationId() {
		return blacklistReservationId;
	}

	public void setBlacklistReservationId(Long blacklistReservationId) {
		this.blacklistReservationId = blacklistReservationId;
	}

	public Integer getBlacklistPaxId() {
		return blacklistPaxId;
	}

	public void setBlacklistPaxId(Integer blacklistPaxId) {
		this.blacklistPaxId = blacklistPaxId;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getReasonToAllow() {
		return reasonToAllow;
	}

	public void setReasonToAllow(String reasonToAllow) {
		this.reasonToAllow = reasonToAllow;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getDummyBooking() {
		return dummyBooking;
	}

	public void setDummyBooking(String dummyBooking) {
		this.dummyBooking = dummyBooking;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getReasonForWhitelisted() {
		return reasonForWhitelisted;
	}

	public void setReasonForWhitelisted(String reasonForWhitelisted) {
		this.reasonForWhitelisted = reasonForWhitelisted;
	}
	
}
