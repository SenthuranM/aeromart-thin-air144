package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PrintRecieptUtil {

	public static Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO) throws ModuleException {
		Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackInfoDTO);
		return reservation;
	}

	public static Collection<Integer> getPnrPaxList(Reservation reservation) {
		Collection<Integer> pNRPaxIdCol = null;

		if (reservation.getPassengers() != null && !reservation.getPassengers().isEmpty()) {
			pNRPaxIdCol = new ArrayList<Integer>();
		}

		for (Object pax : reservation.getPassengers()) {
			ReservationPax reservationPax = (ReservationPax) pax;
			pNRPaxIdCol.add(new Integer(reservationPax.getPnrPaxId()));
		}

		return pNRPaxIdCol;
	}

}
