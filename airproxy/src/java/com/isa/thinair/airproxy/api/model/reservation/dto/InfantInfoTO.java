package com.isa.thinair.airproxy.api.model.reservation.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class InfantInfoTO implements Serializable {

	private static final long serialVersionUID = 9115658591248195429L;

	private BigDecimal infantCharge;

	public BigDecimal getInfantCharge() {
		return infantCharge;
	}

	public void setInfantCharge(BigDecimal infantCharge) {
		this.infantCharge = infantCharge;
	}

}
