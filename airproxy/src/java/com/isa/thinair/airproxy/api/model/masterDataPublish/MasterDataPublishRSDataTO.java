package com.isa.thinair.airproxy.api.model.masterDataPublish;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MasterDataPublishRSDataTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private List<String> updatedAirportCodes;

	private List<String> updatedStationCodes;

	private List<String> updatedTerritoryCodes;
	
	private List<String> updatedCityCodes;

	/** The errors. */
	private List<String> errors;

	public List<String> getUpdatedAirportCodes() {
		if (updatedAirportCodes == null) {
			updatedAirportCodes = new ArrayList<String>();
		}
		return updatedAirportCodes;
	}

	public void setUpdatedAirportIds(List<String> updatedAirportIds) {
		this.updatedAirportCodes = updatedAirportIds;
	}

	public List<String> getUpdatedStationCodes() {
		return updatedStationCodes;
	}

	public void setUpdatedStationCodes(List<String> updatedStationCodes) {
		this.updatedStationCodes = updatedStationCodes;
	}

	public List<String> getUpdatedTerritoryCodes() {
		return updatedTerritoryCodes;
	}

	public void setUpdatedTerritoryCodes(List<String> updatedTerritoryCodes) {
		this.updatedTerritoryCodes = updatedTerritoryCodes;
	}

	public List<String> getUpdatedCityCodes() {
		return updatedCityCodes;
	}

	public void setUpdatedCityCodes(List<String> updatedCityCodes) {
		this.updatedCityCodes = updatedCityCodes;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String> getErrors() {
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

	/**
	 * Sets the errors.
	 * 
	 * @param errors
	 *            the new errors
	 */
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
