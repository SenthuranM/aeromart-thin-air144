/**
 * 
 */
package com.isa.thinair.airproxy.api.model.routepublish;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airmaster.api.model.RouteInfo;

/**
 * @author Navod Ediriweera
 * @since Oct 21, 2009
 */
public class RouteInfoPublishRQDataTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<RouteInfo> publishedRouteInfo;

	public List<RouteInfo> getPublishedRouteInfo() {
		return publishedRouteInfo;
	}

	public void setPublishedRouteInfo(List<RouteInfo> publishedRouteInfo) {
		this.publishedRouteInfo = publishedRouteInfo;
	}

}
