package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class LCCReservationBaggageSummaryTo implements Serializable {
	private Map<String, String> bookingClasses;
	private Map<String, String> classesOfService;
	private Map<String, String> logicalCC;
	private List<String> flightNumbers;
	private boolean allowTillFinalCutOver;

	public Map<String, String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Map<String, String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public List<String> getFlightNumbers() {
		return flightNumbers;
	}

	public void setFlightNumbers(List<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	public Map<String, String> getClassesOfService() {
		return classesOfService;
	}

	public void setClassesOfService(Map<String, String> classesOfService) {
		this.classesOfService = classesOfService;
	}

	public boolean isAllowTillFinalCutOver() {
		return allowTillFinalCutOver;
	}

	public void setAllowTillFinalCutOver(boolean allowTillFinalCutOver) {
		this.allowTillFinalCutOver = allowTillFinalCutOver;
	}

	public Map<String, String> getLogicalCC() {
		return logicalCC;
	}

	public void setLogicalCC(Map<String, String> logicalCC) {
		this.logicalCC = logicalCC;
	}
}
