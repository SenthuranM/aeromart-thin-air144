package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;

public class FlightSegmentTO implements Serializable, Comparable<FlightSegmentTO>, IFlightSegment {

	private static final long serialVersionUID = 1L;

	protected String segmentCode;

	protected Date departureDateTime;

	protected Date departureDateTimeZulu;

	protected Date arrivalDateTime;

	protected Date arrivalDateTimeZulu;

	protected String flightNumber;

	protected String operatingAirline;

	protected int operationType;

	protected String flightRefNumber;

	protected boolean returnFlag;

	protected String cabinClassCode;

	protected String logicalCabinClassCode;

	protected Integer flightSegId;

	protected Integer flightId;

	protected int adultCount;

	protected int childCount;

	protected int infantCount;

	protected String subStationShortName;

	/** The departure terminal name for the given flight segment */
	protected String departureTerminalName;

	/** The arrival terminal name for the given flight segment */
	protected String arrivalTerminalName;

	protected String seatAvailCount;

	protected String routeRefNumber;

	protected boolean surfaceSegment;

	// use in airport services to identify the
	// airport where the service is available
	protected String airportCode;

	// Keep whether the airport is Departure(D) or Arrival(A) or Transit(T)
	protected String airportType;

	protected int segmentSequence;

	protected int ondSequence;

	protected String bookingType;

	// TODO Both bookingType and pnrSegId should be moved to booking related FlightSegmentTO object
	protected String pnrSegId;

	protected String baggageONDGroupId;

	private boolean isDomesticFlight;

	private boolean ssrInventoryCheck;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String remarks;

	private String flightStopOverDuration;

	private String flightSegmentStatus;

	private boolean freeSeatEnabled;

	private boolean freeMealEnabled;

	private boolean waitListed;

	private boolean connection;

	// Total available infant seat on Flight CabinClass Segment (irrespective of channel)
	private Map<String, Integer> availableInfantCountMap = new HashMap<String, Integer>();

	// Total available ADULT OR CHILD seat on Flight CabinClass Segment (irrespective of channel)
	private Map<String, Integer> availableAdultCountMap = new HashMap<String, Integer>();

	private boolean forInsuranceAvailability = false;

	private String fareBasisCode;

	private Integer flexiID;

	private int subJourneyGroup = -1;

	private boolean isParticipatingJourneyReturn;

	private String sectorONDCode;

	private Date inverseSegmentDeparture;

	private String bookingClass;

	private boolean isOverbookEnabled;

	private String csOcCarrierCode;

	private boolean flownSegment;

	private int requestedOndSequence = -1;
	
	private String originCountryCode;
	

	@Override
	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	@Override
	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	@Override
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departurDateTimeZulu) {
		this.departureDateTimeZulu = departurDateTimeZulu;
	}

	@Override
	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public int compareTo(FlightSegmentTO o) {
		if (this.departureDateTimeZulu == null || o.departureDateTimeZulu == null) {
			return (this.departureDateTimeZulu == null) ? -1 : 1;
		}
		if (this.departureDateTimeZulu == null && o.departureDateTimeZulu == null) {
			return 0;
		}
		return this.departureDateTimeZulu.compareTo(o.departureDateTimeZulu);
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public String getSubStationShortName() {
		return subStationShortName;
	}

	public void setSubStationShortName(String subStationShortName) {
		this.subStationShortName = subStationShortName;
	}

	/**
	 * @return the departure terminal name for the flight segment.
	 */
	public String getDepartureTerminalName() {
		return departureTerminalName;
	}

	/**
	 * @param departureTerminalName
	 *            Sets the departure terminal name for the flight segment.
	 */
	public void setDepartureTerminalName(String departureTerminalName) {
		this.departureTerminalName = departureTerminalName;
	}

	/**
	 * @return the arrival terminal name for the flight segment.
	 */
	public String getArrivalTerminalName() {
		return arrivalTerminalName;
	}

	/**
	 * @param arrivalTerminalName
	 *            The arrival terminal name for the flight segment.
	 */
	public void setArrivalTerminalName(String arrivalTerminalName) {
		this.arrivalTerminalName = arrivalTerminalName;
	}

	public String getSeatAvailCount() {
		return seatAvailCount;
	}

	public void setSeatAvailCount(String seatAvailCount) {
		this.seatAvailCount = seatAvailCount;
	}

	public void setRouteRefNumber(String routeRefNumber) {
		this.routeRefNumber = routeRefNumber;
	}

	public String getRouteRefNumber() {
		return routeRefNumber;
	}

	public boolean isSurfaceSegment() {
		return surfaceSegment;
	}

	public void setSurfaceSegment(boolean surfaceSegment) {
		this.surfaceSegment = surfaceSegment;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportType() {
		return airportType;
	}

	public void setAirportType(String airportType) {
		this.airportType = airportType;
	}

	public int getSegmentSequence() {
		return segmentSequence;
	}

	public void setSegmentSequence(int segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public boolean isDomesticFlight() {
		return isDomesticFlight;
	}

	public void setDomesticFlight(boolean isDomesticFlight) {
		this.isDomesticFlight = isDomesticFlight;
	}

	public boolean isSsrInventoryCheck() {
		return ssrInventoryCheck;
	}

	public void setSsrInventoryCheck(boolean ssrInventoryCheck) {
		this.ssrInventoryCheck = ssrInventoryCheck;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFlightStopOverDuration() {
		return flightStopOverDuration;
	}

	public void setFlightStopOverDuration(String flightStopOverDuration) {
		this.flightStopOverDuration = flightStopOverDuration;
	}

	public String getFlightSegmentStatus() {
		return flightSegmentStatus;
	}

	public void setFlightSegmentStatus(String flightSegmentStatus) {
		this.flightSegmentStatus = flightSegmentStatus;
	}

	public boolean isFreeSeatEnabled() {
		return freeSeatEnabled;
	}

	public void setFreeSeatEnabled(boolean freeSeatEnabled) {
		this.freeSeatEnabled = freeSeatEnabled;
	}

	public boolean isFreeMealEnabled() {
		return freeMealEnabled;
	}

	public void setFreeMealEnabled(boolean freeMealEnabled) {
		this.freeMealEnabled = freeMealEnabled;
	}

	public boolean hasRequestingNumOfSeats(int adult, int infant) {
		if (getAdultCount() + getChildCount() < adult || getInfantCount() < infant) {
			return false;
		}
		return true;
	}

	public boolean isConnection() {
		return connection;
	}

	public void setConnection(boolean isConnection) {
		this.connection = isConnection;
	}

	public FlightSegmentTO() {
	}

	public FlightSegmentTO(FlightSegement flightSegment) {
		this();
		this.setFlightSegId(flightSegment.getFltSegId());
		this.setSegmentCode(flightSegment.getSegmentCode());
		this.setDepartureDateTimeZulu(flightSegment.getEstTimeDepatureZulu());
		this.setDepartureDateTime(flightSegment.getEstTimeDepatureLocal());
		this.setArrivalDateTimeZulu(flightSegment.getEstTimeArrivalZulu());
		this.setArrivalDateTime(flightSegment.getEstTimeArrivalLocal());
	}

	public FlightSegmentTO clone() {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(this.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(this.getArrivalDateTimeZulu());
		flightSegmentTO.setCabinClassCode(this.getCabinClassCode());
		flightSegmentTO.setCsOcCarrierCode(this.getCsOcCarrierCode());
		flightSegmentTO.setDepartureDateTime(this.getDepartureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(this.getDepartureDateTimeZulu());
		flightSegmentTO.setFlightId(this.getFlightId());
		flightSegmentTO.setFlightNumber(this.getFlightNumber());
		flightSegmentTO.setFlightRefNumber(this.getFlightRefNumber());
		flightSegmentTO.setFlightSegId(this.getFlightSegId());
		flightSegmentTO.setOperatingAirline(this.getOperatingAirline());
		flightSegmentTO.setOperationType(this.getOperationType());
		flightSegmentTO.setAdultCount(this.getAdultCount());
		flightSegmentTO.setChildCount(this.getChildCount());
		flightSegmentTO.setInfantCount(this.getInfantCount());
		flightSegmentTO.setReturnFlag(returnFlag);
		flightSegmentTO.setSegmentCode(this.getSegmentCode());
		flightSegmentTO.setSubStationShortName(this.getSubStationShortName());
		flightSegmentTO.setArrivalTerminalName(this.getArrivalTerminalName());
		flightSegmentTO.setDepartureTerminalName(this.getDepartureTerminalName());
		flightSegmentTO.setRouteRefNumber(this.routeRefNumber);
		flightSegmentTO.setSurfaceSegment(this.surfaceSegment);
		flightSegmentTO.setAirportCode(this.airportCode);
		flightSegmentTO.setAirportType(this.airportType);
		flightSegmentTO.setSegmentSequence(this.segmentSequence);
		flightSegmentTO.setBookingType(this.bookingType);
		flightSegmentTO.setDomesticFlight(this.isDomesticFlight());
		flightSegmentTO.setSsrInventoryCheck(this.ssrInventoryCheck);
		flightSegmentTO.setFlightDuration(this.flightDuration);
		flightSegmentTO.setFlightModelDescription(this.flightModelDescription);
		flightSegmentTO.setFlightModelNumber(this.flightModelNumber);
		flightSegmentTO.setRemarks(this.remarks);
		flightSegmentTO.setFlightStopOverDuration(this.flightStopOverDuration);
		flightSegmentTO.setFlightSegmentStatus(this.flightSegmentStatus);
		flightSegmentTO.setFreeSeatEnabled(this.freeSeatEnabled);
		flightSegmentTO.setFreeMealEnabled(this.freeMealEnabled);
		flightSegmentTO.setPnrSegId(this.getPnrSegId());
		flightSegmentTO.setBaggageONDGroupId(this.getBaggageONDGroupId());
		flightSegmentTO.setFareBasisCode(this.fareBasisCode);
		flightSegmentTO.setFlexiID(this.flexiID);
		flightSegmentTO.setParticipatingJourneyReturn(this.isParticipatingJourneyReturn);
		flightSegmentTO.setSectorONDCode(this.sectorONDCode);
		flightSegmentTO.setInverseSegmentDeparture(this.inverseSegmentDeparture);
		flightSegmentTO.setBookingClass(this.bookingClass);
		flightSegmentTO.setLogicalCabinClassCode(this.logicalCabinClassCode);
		flightSegmentTO.setOndSequence(this.ondSequence);

		return flightSegmentTO;
	}

	@Override
	public String toString() {
		return "FlightSegmentTO [segmentCode=" + segmentCode + ", departureDateTime=" + departureDateTime
				+ ", departureDateTimeZulu=" + departureDateTimeZulu + ", arrivalDateTime=" + arrivalDateTime
				+ ", arrivalDateTimeZulu=" + arrivalDateTimeZulu + ", flightNumber=" + flightNumber + ", operatingAirline="
				+ operatingAirline + ", operationType=" + operationType + ", flightRefNumber=" + flightRefNumber + ", returnFlag="
				+ returnFlag + ", cabinClassCode=" + cabinClassCode + ", flightSegId=" + flightSegId + ", flightId=" + flightId
				+ ", adultCount=" + adultCount + ", childCount=" + childCount + ", infantCount=" + infantCount
				+ ", subStationShortName=" + subStationShortName + ", departureTerminalName=" + departureTerminalName
				+ ", arrivalTerminalName=" + arrivalTerminalName + ", seatAvailCount=" + seatAvailCount + ", routeRefNumber="
				+ routeRefNumber + ", surfaceSegment=" + surfaceSegment + ", airportCode=" + airportCode + ", airportType="
				+ airportType + ", segmentSequence=" + segmentSequence + ", bookingType=" + bookingType + ", isDomesticFlight="
				+ isDomesticFlight + ", ssrInventoryCheck=" + ssrInventoryCheck + ", flightModelNumber=" + flightModelNumber
				+ ", flightModelDescription=" + flightModelDescription + ", flightDuration=" + flightDuration + ", remarks="
				+ remarks + ", flightStopOverDuration=" + flightStopOverDuration + ", flightSegmentStatus=" + flightSegmentStatus
				+ ", freeSeatEnabled=" + freeSeatEnabled + ", freeMealEnabled=" + freeMealEnabled + "]";
	}

	/**
	 * convert the FlightSegmentTO to FlightSegmentDTO
	 * 
	 * @param flightSegmentTO
	 * @return
	 */
	public FlightSegmentDTO convert() {

		FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();

		flightSegmentDTO.setDepartureDateTime(this.getDepartureDateTime());
		flightSegmentDTO.setArrivalDateTime(this.getArrivalDateTime());

		flightSegmentDTO.setDepartureDateTimeZulu(this.getDepartureDateTimeZulu());
		flightSegmentDTO.setArrivalDateTimeZulu(this.getArrivalDateTimeZulu());

		flightSegmentDTO.setSegmentId(this.getFlightSegId());
		flightSegmentDTO.setFlightNumber(this.getFlightNumber());
		flightSegmentDTO.setFlightId(this.getFlightId());
		flightSegmentDTO.setSegmentCode(this.getSegmentCode());

		return flightSegmentDTO;

	}

	public String getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(String pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the baggageONDGroupId
	 */
	public String getBaggageONDGroupId() {
		return baggageONDGroupId;
	}

	/**
	 * @param baggageONDGroupId
	 *            the baggageONDGroupId to set
	 */
	public void setBaggageONDGroupId(String baggageONDGroupId) {
		this.baggageONDGroupId = baggageONDGroupId;
	}

	public Map<String, Integer> getAvailableInfantCountMap() {
		return availableInfantCountMap;
	}

	public void setAvailableInfantCountMap(Map<String, Integer> availableInfantCountMap) {
		this.availableInfantCountMap = availableInfantCountMap;
	}

	public Map<String, Integer> getAvailableAdultCountMap() {
		return availableAdultCountMap;
	}

	public void setAvailableAdultCountMap(Map<String, Integer> availableAdultCountMap) {
		this.availableAdultCountMap = availableAdultCountMap;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public void setWaitListed(boolean waitListed) {
		this.waitListed = waitListed;
	}

	public boolean isForInsuranceAvailability() {
		return forInsuranceAvailability;
	}

	public void setForInsuranceAvailability(boolean forInsuranceAvailability) {
		this.forInsuranceAvailability = forInsuranceAvailability;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public Integer getFlexiID() {
		return flexiID;
	}

	public void setFlexiID(Integer flexiID) {
		this.flexiID = flexiID;
	}

	/**
	 * @return the isParticipatingJourneyReturn
	 */
	public boolean isParticipatingJourneyReturn() {
		return isParticipatingJourneyReturn;
	}

	/**
	 * @param isParticipatingJourneyReturn
	 *            the isParticipatingJourneyReturn to set
	 */
	public void setParticipatingJourneyReturn(boolean isParticipatingJourneyReturn) {
		this.isParticipatingJourneyReturn = isParticipatingJourneyReturn;
	}

	/**
	 * @return the sectorONDCode
	 */
	public String getSectorONDCode() {
		return sectorONDCode;
	}

	/**
	 * @param sectorONDCode
	 *            the sectorONDCode to set
	 */
	public void setSectorONDCode(String sectorONDCode) {
		this.sectorONDCode = sectorONDCode;
	}

	/**
	 * @return the inverseSegmentDeparture
	 */
	public Date getInverseSegmentDeparture() {
		return inverseSegmentDeparture;
	}

	/**
	 * @param inverseSegmentDeparture
	 *            the inverseSegmentDeparture to set
	 */
	public void setInverseSegmentDeparture(Date inverseSegmentDeparture) {
		this.inverseSegmentDeparture = inverseSegmentDeparture;
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;
	}

	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public boolean isOverbookEnabled() {
		return isOverbookEnabled;
	}

	public void setOverbookEnabled(boolean isOverbookEnabled) {
		this.isOverbookEnabled = isOverbookEnabled;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public boolean isFlownSegment() {
		return flownSegment;
	}

	public void setFlownSegment(boolean flownSegment) {
		this.flownSegment = flownSegment;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}
	
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}

}
