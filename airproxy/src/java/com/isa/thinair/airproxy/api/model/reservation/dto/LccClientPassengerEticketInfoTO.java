package com.isa.thinair.airproxy.api.model.reservation.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eric
 * 
 */
public class LccClientPassengerEticketInfoTO implements Comparable<LccClientPassengerEticketInfoTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String travelerRefNumber;

	private Integer ppfsId;

	private Integer eticketId;

	private String segmentCode;

	private String segmentStatus;

	private String paxETicketNo;

	private Integer couponNo;
	
	private String externalPaxETicketNo;

	private Integer externalCouponNo;

	private String externalCouponStatus;

	private String paxETicketStatus;

	private String pnrSegId;

	private int eTicketSequnce;

	private String carrierCode;

	private String flightNo;

	private Date departureDate;

	private Date departureDateZulu;

	private String paxStatus;

	private String flightSegmentRef;

	private String externalCouponControl;

	private String settlementAuthCode;
	
	private String passengerName;

	private String codeShareCarrierCode;

	private String codeShareFlightNumber;

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public Integer getPpfsId() {
		return ppfsId;
	}

	public void setPpfsId(Integer ppfsId) {
		this.ppfsId = ppfsId;
	}

	public Integer getEticketId() {
		return eticketId;
	}

	public void setEticketId(Integer eticketId) {
		this.eticketId = eticketId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentStatus() {
		return segmentStatus;
	}

	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}

	public String getPaxETicketNo() {
		return paxETicketNo;
	}

	public void setPaxETicketNo(String paxETicketNo) {
		this.paxETicketNo = paxETicketNo;
	}

	public Integer getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(Integer couponNo) {
		this.couponNo = couponNo;
	}

	public String getPaxETicketStatus() {
		return paxETicketStatus;
	}

	public void setPaxETicketStatus(String paxETicketStatus) {
		this.paxETicketStatus = paxETicketStatus;
	}

	public String getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(String pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public int geteTicketSequnce() {
		return eTicketSequnce;
	}

	public void seteTicketSequnce(int eTicketSequnce) {
		this.eTicketSequnce = eTicketSequnce;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}
	
	/**
	 * @return the externalPaxETicketNo
	 */
	public String getExternalPaxETicketNo() {
		return externalPaxETicketNo;
	}

	/**
	 * @param externalPaxETicketNo the externalPaxETicketNo to set
	 */
	public void setExternalPaxETicketNo(String externalPaxETicketNo) {
		this.externalPaxETicketNo = externalPaxETicketNo;
	}

	/**
	 * @return the externalCouponNo
	 */
	public Integer getExternalCouponNo() {
		return externalCouponNo;
	}

	/**
	 * @param externalCouponNo the externalCouponNo to set
	 */
	public void setExternalCouponNo(Integer externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	public String getExternalCouponStatus() {
		return externalCouponStatus;
	}

	public void setExternalCouponStatus(String externalCouponStatus) {
		this.externalCouponStatus = externalCouponStatus;
	}

	public String getFlightSegmentRef() {
		return flightSegmentRef;
	}

	public void setFlightSegmentRef(String flightSegmentRef) {
		this.flightSegmentRef = flightSegmentRef;
	}

	@Override
	public int compareTo(LccClientPassengerEticketInfoTO o) {
		return this.getEticketId().compareTo(o.getEticketId());
	}

	public String getExternalCouponControl() {
		return externalCouponControl;
	}

	public void setExternalCouponControl(String externalCouponControl) {
		this.externalCouponControl = externalCouponControl;
	}

	public String getSettlementAuthCode() {
		return settlementAuthCode;
	}

	public void setSettlementAuthCode(String settlementAuthCode) {
		this.settlementAuthCode = settlementAuthCode;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getCodeShareCarrierCode() {
		return codeShareCarrierCode;
	}

	public void setCodeShareCarrierCode(String codeShareCarrierCode) {
		this.codeShareCarrierCode = codeShareCarrierCode;
	}

	public String getCodeShareFlightNumber() {
		return codeShareFlightNumber;
	}

	public void setCodeShareFlightNumber(String codeShareFlightNumber) {
		this.codeShareFlightNumber = codeShareFlightNumber;
	}	
}
