package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Date;

public class LCCClientReservationAdditionalPax implements Serializable {

	private static final long serialVersionUID = 6066882919887873664L;

	private String passportNo;
	private Date passportExpiry;
	private String passportIssuedCntry;
	private String employeeId;
	private Date dateOfJoin;
	private String idCategory;
	private String placeOfBirth; 
	private String travelDocumentType; 
	private String visaDocNumber;
	private String visaDocPlaceOfIssue ;
	private Date visaDocIssueDate; 
	private String visaApplicableCountry;
	private String arrivalIntlFltNo;
	private Date intlFltArrivalDate;
	private String departureIntlFltNo;
	private Date intlFltDepartureDate;
	private String pnrPaxGroupId;
	private String ffid;
	private String nationalIDNo;

	/**
	 * @return the passportNo
	 */
	public String getPassportNo() {
		return passportNo;
	}

	/**
	 * @param passportNo
	 *            the passportNo to set
	 */
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	/**
	 * @return the passportExpiry
	 */
	public Date getPassportExpiry() {
		return passportExpiry;
	}

	/**
	 * @param passportExpiry
	 *            the passportExpiry to set
	 */
	public void setPassportExpiry(Date passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	/**
	 * @return the passportIssuedCntry
	 */
	public String getPassportIssuedCntry() {
		return passportIssuedCntry;
	}

	/**
	 * @param passportIssuedCntry
	 *            the passportIssuedCntry to set
	 */
	public void setPassportIssuedCntry(String passportIssuedCntry) {
		this.passportIssuedCntry = passportIssuedCntry;
	}

	/**
	 * @return employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 *            employeeId to be set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return dateOfJoin
	 */
	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	/**
	 * @param dateOfJoin
	 *            dateOfJoin to be set
	 */
	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	/**
	 * @return idCategory
	 */
	public String getIdCategory() {
		return idCategory;
	}

	/**
	 * @param idCategory
	 *            idCategory to be set
	 */
	public void setIdCategory(String idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * @return the placeOfBirth
	 */
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	/**
	 * @param placeOfBirth the placeOfBirth to set
	 */
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	/**
	 * @return the travelDocumentType
	 */
	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	/**
	 * @param travelDocumentType the travelDocumentType to set
	 */
	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	/**
	 * @return the visaDocNumber
	 */
	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	/**
	 * @param visaDocNumber the visaDocNumber to set
	 */
	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	/**
	 * @return the visaDocPlaceOfIssue
	 */
	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	/**
	 * @param visaDocPlaceOfIssue the visaDocPlaceOfIssue to set
	 */
	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	/**
	 * @return the visaDocIssueDate
	 */
	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	/**
	 * @param visaDocIssueDate the visaDocIssueDate to set
	 */
	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

	/**
	 * @return the visaApplicableCountry
	 */
	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	/**
	 * @param visaApplicableCountry the visaApplicableCountry to set
	 */
	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public String getNationalIDNo() {
		return nationalIDNo;
	}

	public void setNationalIDNo(String nationalIDNo) {
		this.nationalIDNo = nationalIDNo;
	}
	
	public String getArrivalIntlFltNo() {
		return arrivalIntlFltNo;
	}

	public void setArrivalIntlFltNo(String arrivalIntlFltNo) {
		this.arrivalIntlFltNo = arrivalIntlFltNo;
	}

	public Date getIntlFltArrivalDate() {
		return intlFltArrivalDate;
	}

	public void setIntlFltArrivalDate(Date intlFltArrivalDate) {
		this.intlFltArrivalDate = intlFltArrivalDate;
	}

	public String getDepartureIntlFltNo() {
		return departureIntlFltNo;
	}

	public void setDepartureIntlFltNo(String departureIntlFltNo) {
		this.departureIntlFltNo = departureIntlFltNo;
	}

	public Date getIntlFltDepartureDate() {
		return intlFltDepartureDate;
	}

	public void setIntlFltDepartureDate(Date intlFltDepartureDate) {
		this.intlFltDepartureDate = intlFltDepartureDate;
	}

	public String getPnrPaxGroupId() {
		return pnrPaxGroupId;
	}

	public void setPnrPaxGroupId(String pnrPaxGroupId) {
		this.pnrPaxGroupId = pnrPaxGroupId;
	}

	public String getFfid() {
		return ffid;
	}

	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

}
