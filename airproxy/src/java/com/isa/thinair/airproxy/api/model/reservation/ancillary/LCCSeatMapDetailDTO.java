package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCSeatMapDetailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LCCCabinClassDTO> cabinClass;

	public List<LCCCabinClassDTO> getCabinClass() {
		if (cabinClass == null)
			cabinClass = new ArrayList<LCCCabinClassDTO>();
		return cabinClass;
	}

	public void setCabinClass(List<LCCCabinClassDTO> cabinClass) {
		this.cabinClass = cabinClass;
	}
}
