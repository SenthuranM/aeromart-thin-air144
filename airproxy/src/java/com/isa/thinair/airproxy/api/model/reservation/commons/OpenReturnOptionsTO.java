package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;

/**
 * Contain Optional data of open return booking
 * 
 * @author danuruddha
 * 
 */
public class OpenReturnOptionsTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date confirmExpiryDate;
	private Date travelExpiryDate;
	private String airportCode;

	/**
	 * @return the confirmation expiry in the local date time of the reference airport
	 */
	public Date getConfirmExpiryDate() {
		return confirmExpiryDate;
	}

	/**
	 * @param set
	 *            the confirm expiry in the local date time of the reference airport
	 */
	public void setConfirmExpiryDate(Date confirmExpiryDate) {
		this.confirmExpiryDate = confirmExpiryDate;
	}

	/**
	 * @return the travel expiry date in local date time of the reference airport
	 */
	public Date getTravelExpiryDate() {
		return travelExpiryDate;
	}

	/**
	 * @param set
	 *            the travel expiry in local date time of the reference airport
	 */
	public void setTravelExpiryDate(Date travelExpiryDate) {
		this.travelExpiryDate = travelExpiryDate;
	}

	/**
	 * @return the airportCode to which the date times will be referenced
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param set
	 *            the airportCode to which the date times will be referenced
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
