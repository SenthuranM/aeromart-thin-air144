package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.Collection;

public class BaggageRatesDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Collection<FlightSegmentBaggageRate> baggageRates;

	public Collection<FlightSegmentBaggageRate> getBaggageRates() {
		return baggageRates;
	}

	public void setBaggageRates(Collection<FlightSegmentBaggageRate> baggageRates) {
		this.baggageRates = baggageRates;
	}
}
