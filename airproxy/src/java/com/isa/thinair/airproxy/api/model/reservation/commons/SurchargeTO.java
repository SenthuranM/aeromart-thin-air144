package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class SurchargeTO extends BasicChargeTO implements Serializable, Comparable<SurchargeTO> {

	private static final long serialVersionUID = 1L;

	protected String surchargeCode;
	protected String carrierCode;
	protected String segmentCode;
	protected List<String> applicablePassengerTypeCode;
	protected String surchargeName;
	protected Date applicableTime;
	protected String chargeRateId;
	protected String chagrgeGroupCode;
	protected String chargeCode;
	protected String reportingChargeGroupCode;
	protected String chargeCategoryCode;

	public String getSurchargeName() {
		return surchargeName;
	}

	public void setSurchargeName(String surchargeName) {
		this.surchargeName = surchargeName;
	}

	public String getSurchargeCode() {
		return surchargeCode;
	}

	public void setSurchargeCode(String surchargeCode) {
		this.surchargeCode = surchargeCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public List<String> getApplicablePassengerTypeCode() {
		if (applicablePassengerTypeCode == null)
			applicablePassengerTypeCode = new ArrayList<String>();
		return applicablePassengerTypeCode;
	}

	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public Date getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(Date applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(String chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public String getChagrgeGroupCode() {
		return chagrgeGroupCode;
	}

	public void setChagrgeGroupCode(String chagrgeGroupCode) {
		this.chagrgeGroupCode = chagrgeGroupCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getApplicableToDisplay() {
		String applicableTo = "";
		if (applicablePassengerTypeCode != null) {
			for (String applicable : applicablePassengerTypeCode) {
				applicableTo = applicableTo + "," + PaxTypeTO.getPaxTypeDisplayForAAPaxType(applicable);
			}
		}
		if (!applicableTo.equals("")) {
			applicableTo = applicableTo.substring(1);
		}
		return applicableTo;
	}

	@Override
	public SurchargeTO clone() {
		SurchargeTO clone = new SurchargeTO();
		clone.setCarrierCode(this.carrierCode);
		clone.setSegmentCode(this.getSegmentCode());
		clone.setSurchargeCode(this.getSurchargeCode());
		clone.setAmount(this.getAmount());
		clone.getApplicablePassengerTypeCode().addAll(this.getApplicablePassengerTypeCode());
		clone.setChagrgeGroupCode(this.getChagrgeGroupCode());
		clone.setChargeRateId(this.getChargeRateId());
		clone.setChargeCode(this.getChargeCode());
		clone.setLocalCurrencyAmount(this.getLocalCurrencyAmount());
		clone.setLocalCurrencyCode(this.getLocalCurrencyCode());
		clone.setReportingChargeGroupCode(this.getReportingChargeGroupCode());
		clone.setOndSequence(this.getOndSequence());
		clone.setSurchargeName(this.getSurchargeName());
		clone.setChargeCategoryCode(this.getChargeCategoryCode());
		return clone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SurchargeTO [amount=");
		builder.append(amount);
		builder.append(", applicablePassengerTypeCode=");
		builder.append(applicablePassengerTypeCode);
		builder.append(", applicableTime=");
		builder.append(applicableTime);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", segmentCode=");
		builder.append(segmentCode);
		builder.append(", surchargeCode=");
		builder.append(surchargeCode);
		builder.append(", surchargeName=");
		builder.append(surchargeName);
		builder.append(", chargeRateId=");
		builder.append(chargeRateId);
		builder.append(", chargeCode=");
		builder.append(chargeCode);
		builder.append(", chagrgeGroupCode=");
		builder.append(chagrgeGroupCode);
		builder.append(", localCurrencyCode=");
		builder.append(localCurrencyCode);
		builder.append(", localCurrencyAmount=");
		builder.append(localCurrencyAmount);
		builder.append("]");
		builder.append(", reportingChargeGroupCode=");
		builder.append(reportingChargeGroupCode);
		builder.append("]");
		return builder.toString();
	}

	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}

	@Override
	public int compareTo(SurchargeTO o) {
		return o.amount.compareTo(this.amount);
	}

	/**
	 * @return the chargeCategoryCode
	 */
	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	/**
	 * @param chargeCategoryCode
	 *            the chargeCategoryCode to set
	 */
	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}
}
