package com.isa.thinair.airproxy.api.utils.assembler;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * PaxInfomation Assembler Utility
 * 
 * @author Dilan Anuruddha
 * 
 */

public class PaxCountAssembler implements IPaxCountAssembler, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int adultCount = 0;
	private int childCount = 0;
	private int infantCount = 0;

	public PaxCountAssembler(List<PassengerTypeQuantityTO> paxList) {
		for (PassengerTypeQuantityTO pax : paxList) {
			if (PaxTypeTO.ADULT.equals(pax.getPassengerType())) {
				adultCount = pax.getQuantity();
			} else if (PaxTypeTO.CHILD.equals(pax.getPassengerType())) {
				childCount = pax.getQuantity();
			} else if (PaxTypeTO.INFANT.equals(pax.getPassengerType())) {
				infantCount = pax.getQuantity();
			}
		}
	}

	public PaxCountAssembler(int adultCount, int childCount, int infantCount) {
		this.adultCount = adultCount;
		this.childCount = childCount;
		this.infantCount = infantCount;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public int getAdultCount() {
		return adultCount;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public int getChildCount() {
		return childCount;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public int getInfantCount() {
		return infantCount;
	}
}
