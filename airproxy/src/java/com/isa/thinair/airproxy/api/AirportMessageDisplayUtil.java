package com.isa.thinair.airproxy.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.AirportMessageFilter;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;

public class AirportMessageDisplayUtil {

	public static String getAirportMessages(List<OriginDestinationInformationTO> destinationInformationTOs, String salesChannel,
			String stage, String languageCode) throws ModuleException {
		String airportMessage = "";
		List<AirportMessageFilter> airportMsgFilterList = new ArrayList<AirportMessageFilter>();
		AirportMessageFilter airportMessageFilter = null;

		for (OriginDestinationInformationTO destinationInfo : destinationInformationTOs) {
			for (OriginDestinationOptionTO originDestOpt : destinationInfo.getOrignDestinationOptions()) {
				if (originDestOpt.isSelected()) {
					for (FlightSegmentTO flightseg : originDestOpt.getFlightSegmentList()) {
						airportMessageFilter = new AirportMessageFilter();

						String[] splits = flightseg.getSegmentCode().split("/");

						airportMessageFilter.setDepAirport(splits[0]);
						airportMessageFilter.setArrAirport(splits[splits.length - 1]);
						airportMessageFilter.setFlightDepDate(flightseg.getDepartureDateTimeZulu());
						airportMessageFilter.setFlightNumber(flightseg.getFlightNumber());
						airportMessageFilter.setFromDate(flightseg.getDepartureDateTimeZulu());
						airportMessageFilter.setToDate(flightseg.getArrivalDateTimeZulu());
						airportMessageFilter.setSalesChanel(salesChannel);
						airportMessageFilter.setStage(stage);
						airportMessageFilter.setReturnFlag(flightseg.isReturnFlag());
						airportMsgFilterList.add(airportMessageFilter);
					}
				}
			}
		}

		if (airportMsgFilterList.size() == 0) {
			airportMessage = "";
		} else {
			airportMessage = composeMessage(airportMsgFilterList, languageCode);
		}

		return airportMessage;
	}

	public static String getAirportMessagesForFlightSegmentWise(List<FlightSegmentTO> flightSegmentTOs, String salesChannel,
			String stage, String languageCode) throws ModuleException {

		String airportMessage = "";
		List<AirportMessageFilter> airportMsgFilterList = new ArrayList<AirportMessageFilter>();
		AirportMessageFilter airportMessageFilter = null;

		for (FlightSegmentTO flightseg : flightSegmentTOs) {
			airportMessageFilter = new AirportMessageFilter();

			String splits[] = flightseg.getSegmentCode().split("/");

			airportMessageFilter.setDepAirport(splits[0]);
			airportMessageFilter.setArrAirport(splits[splits.length - 1]);
			airportMessageFilter.setFlightDepDate(flightseg.getDepartureDateTimeZulu());
			airportMessageFilter.setFlightNumber(flightseg.getFlightNumber());
			airportMessageFilter.setFromDate(flightseg.getDepartureDateTimeZulu());
			airportMessageFilter.setToDate(flightseg.getArrivalDateTimeZulu());
			airportMessageFilter.setSalesChanel(salesChannel);
			airportMessageFilter.setStage(stage);
			airportMessageFilter.setReturnFlag(flightseg.isReturnFlag());
			airportMsgFilterList.add(airportMessageFilter);
		}

		if (airportMsgFilterList.size() == 0) {
			airportMessage = "";
		} else {
			airportMessage = composeMessage(airportMsgFilterList, languageCode);
		}

		return airportMessage;
	}

	public static List<String> getAirportMessagesForFlightSegmentWise(Set<LCCClientReservationSegment> reservationSegments,
			String salesChannel, String stage, String languageCode) throws ModuleException {

		List<String> airportMsgs = new ArrayList<String>();

		List<AirportMessageFilter> airportMsgFilterList = new ArrayList<AirportMessageFilter>();
		AirportMessageFilter airportMessageFilter = null;

		for (LCCClientReservationSegment flightseg : reservationSegments) {
			airportMessageFilter = new AirportMessageFilter();

			String[] splits = flightseg.getSegmentCode().split("/");

			airportMessageFilter.setDepAirport(splits[0]);
			airportMessageFilter.setArrAirport(splits[splits.length - 1]);
			airportMessageFilter.setFlightDepDate(flightseg.getDepartureDateZulu());
			airportMessageFilter.setFlightNumber(flightseg.getFlightNo());
			airportMessageFilter.setFromDate(flightseg.getDepartureDateZulu());
			airportMessageFilter.setToDate(flightseg.getArrivalDateZulu());
			airportMessageFilter.setSalesChanel(salesChannel);
			airportMessageFilter.setStage(stage);
			airportMessageFilter.setReturnFlag(flightseg.isOpenReturnSegment());

			airportMsgFilterList.add(airportMessageFilter);
		}

		if (!(airportMsgFilterList.size() == 0)) {
			airportMsgs = composeMessageList(airportMsgFilterList, languageCode);
		}

		return airportMsgs;
	}

	private static String composeMessage(List<AirportMessageFilter> airportMsgFilterList, String languageCode)
			throws ModuleException {

		StringBuilder sb = new StringBuilder("");
		List<String> messages = composeMessageList(airportMsgFilterList, languageCode);
		for (String message : messages) {
			sb.append("<ul><li>");
			sb.append(message);
			sb.append("</ul></li>");
		}

		return sb.toString();
	}

	private static List<String> composeMessageList(List<AirportMessageFilter> airportMsgFilterList, String languageCode)
			throws ModuleException {
		List<String> airportMsgs = new ArrayList<String>();
		List<AirportMessageDisplayDTO> airportMessageDisplayDTOs = loadAirportMessages(airportMsgFilterList);

		for (AirportMessageDisplayDTO airportMessageDisplayDTO : airportMessageDisplayDTOs) {
			if (languageCode != null && !languageCode.equals("") && !languageCode.equalsIgnoreCase("en")) {
				String translatedMessageText = loadAirportMessagesTranslation(airportMessageDisplayDTO.getAirportMsgId(),
						airportMessageDisplayDTO.getI18MessageKey(), languageCode);
				if (translatedMessageText != null && !translatedMessageText.equals("")) {
					String unicodeTrnText = StringUtil.getUnicode(translatedMessageText).replace('^', ',');
					airportMsgs.add(unicodeTrnText);
				} else {
					airportMsgs.add(airportMessageDisplayDTO.getAirportMessage());
				}
			} else {
				//In XBE there is no Language support for front end. we can put default message null and set translation in t_airport_message_translations
				if (airportMessageDisplayDTO.getAirportMessage() == null){
					languageCode = "en";
					String translatedMessageText = loadAirportMessagesTranslation(airportMessageDisplayDTO.getAirportMsgId(),
							airportMessageDisplayDTO.getI18MessageKey(), languageCode);
					if (translatedMessageText != null && !translatedMessageText.equals("")) {
						String unicodeTrnText = StringUtil.getUnicode(translatedMessageText).replace('^', ',');
						airportMsgs.add(unicodeTrnText);
					} else {
						airportMsgs.add(airportMessageDisplayDTO.getAirportMessage());
					}
				}else{
					airportMsgs.add(airportMessageDisplayDTO.getAirportMessage());
				}
			}
		}

		return getUniqueMessages(airportMsgs);
	}

	private static List<String> getUniqueMessages(List<String> messages) {
		Collection<String> uniqueMessages = new HashSet<String>();
		uniqueMessages.addAll(messages);

		return new ArrayList<String>(uniqueMessages);
	}

	private static String loadAirportMessagesTranslation(Integer airportMsgId, String i18MessageKey, String slectedLang)
			throws ModuleException {
		ReservationAuxilliaryBD reservationAuxilliaryBD = ReservationModuleUtils.getReservationAuxilliaryBD();
		String translation = "";
		if (i18MessageKey != null && !i18MessageKey.isEmpty()) {
			translation = reservationAuxilliaryBD.loadI18AirportMessagesTranslation(i18MessageKey, slectedLang);
		} else {
			translation = reservationAuxilliaryBD.loadAirportMessagesTranslation(airportMsgId, slectedLang);
		}
		return translation;
	}

	private static List<AirportMessageDisplayDTO> loadAirportMessages(List<AirportMessageFilter> airportMsgFilters)
			throws ModuleException {

		List<AirportFlightDetail> airportMessageDetailList = new ArrayList<AirportFlightDetail>();
		AirportFlightDetail airportMsg = new AirportFlightDetail();

		for (AirportMessageFilter airportMessageFilter : airportMsgFilters) {
			airportMsg = new AirportFlightDetail();
			airportMsg.setAirportCode(airportMessageFilter.getDepAirport());
			airportMsg.setTravelDate(airportMessageFilter.getFromDate());
			airportMsg.setFlightDepDate(airportMessageFilter.getFlightDepDate());
			airportMsg.setFlightNumber(airportMessageFilter.getFlightNumber());
			airportMsg.setFlightIds(airportMessageFilter.getFlightId());
			airportMsg.setTravelWay(ReservationInternalConstants.AirportMessageTravelWay.DEP);
			airportMsg.setSalesChannel(airportMessageFilter.getSalesChanel());
			airportMsg.setStage(airportMessageFilter.getStage());
			airportMsg.setOnd(airportMessageFilter.getDepAirport() + "/" + airportMessageFilter.getArrAirport());
			airportMessageDetailList.add(airportMsg);

			airportMsg = new AirportFlightDetail();
			airportMsg.setAirportCode(airportMessageFilter.getArrAirport());
			airportMsg.setTravelDate(airportMessageFilter.getToDate());
			airportMsg.setFlightDepDate(airportMessageFilter.getFlightDepDate());
			airportMsg.setFlightNumber(airportMessageFilter.getFlightNumber());
			airportMsg.setFlightIds(airportMessageFilter.getFlightId());
			airportMsg.setTravelWay(ReservationInternalConstants.AirportMessageTravelWay.ARR);
			airportMsg.setSalesChannel(airportMessageFilter.getSalesChanel());
			airportMsg.setStage(airportMessageFilter.getStage());
			airportMsg.setOnd(airportMessageFilter.getDepAirport() + "/" + airportMessageFilter.getArrAirport());
			airportMessageDetailList.add(airportMsg);

		}

		return ReservationModuleUtils.getGlobalConfig().airportMessages(airportMessageDetailList);
	}
}
