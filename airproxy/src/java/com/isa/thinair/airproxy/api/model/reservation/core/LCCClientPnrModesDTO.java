/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.commons.api.constants.ApplicationEngine;

/**
 * To hold reservation load modes data transfer information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientPnrModesDTO implements Serializable {

	private static final long serialVersionUID = -5037273089951502606L;

	private String groupPNR;

	/** Holds the reservation number */
	private String pnr;

	/** Holds whether to load the reservation fare and segment information */
	private boolean loadFares;

	/** Holds whether to load all segment with flight information (READ ONLY) */
	private boolean loadSegView;

	/** Holds whether to load all segment with flight information (READ ONLY) with booking types */
	private boolean loadSegViewBookingTypes;

	/** Holds whether to load all segment with flight information (READ ONLY) with fare category types */
	private boolean loadSegViewFareCategoryTypes;

	/** Holds whether to load return group id or not */
	private boolean loadSegViewReturnGroupId;

	/** Holds whether to load the passenger avaliable credit or debit information (READ ONLY) */
	private boolean loadPaxAvaBalance;

	/** Holds whether to load local times or not */
	private boolean loadLocalTimes;

	/** Loads the last user note */
	private boolean loadLastUserNote;

	/** Loads the Ond Charges View */
	private boolean loadOndChargesView;

	/** Loads the pax payment tnx with ond payment breakdown */
	private boolean loadPaxPaymentOndBreakdownView;

	/** Loads the external pax payments done for dry/interline */
	private boolean loadExternalPaxPayments;

	/** To audit or not */
	private boolean recordAudit;

	/** Holds whether to load all seat map info */
	private boolean loadSeatingInfo;

	/** Holds whether to load all meal info */
	private boolean loadMealInfo;
	
	/** Holds whether to load all airport transfer info */
	private boolean loadAirportTransferInfo;

	/** Load pax ssr Info */
	private boolean loadSSRInfo;

	/** Hold AccelAero System IBE /XBE */
	private ApplicationEngine appIndicator;
	/** Hold PrivilegeIDs (Segment Modifiable) **/
	Map<String, String> mapPrivilegeIds;

	/** To hold XBE preferred language */
	private String preferredLanguage;

	private String extRecordLocatorId;

	/** place holder to identify marketing airline */
	private String marketingAirlineCode;

	/** Holds the calling airline of LCC */
	private String callingAirlineCode;

	/** place holder to identify airline */
	private String airlineCode;

	/** place holder for interline agreement id. used for dry bookings */
	private Long interlineAgreementId;

	private String bookingCreationDate;

	/** Holds whether to load all baggage info */
	private boolean loadBaggageInfo;

	/** Holds whether to load passenger e ticket info */
	private boolean loadEtickets;

	/** Holds the switch that determines whether the origin country will be retrieved and set */
	private boolean loadOriginCountry;

	/**
	 * Trigger to tel whether to load refundable tax amounts or not.
	 */
	private boolean loadRefundableTaxInfo;

	/** Skip considering promotion adjustment when displaying total charges in front-end */
	private boolean skipPromoAdjustment;
	
	/** Check privillege to add/view private user notes */
	private boolean loadClassifyUN = false;

	private boolean loadExternalReference;

	private boolean loadGOQUOAmounts = false;

	private String userNote;

	private String agentCode;

	/** Holds whether to load all automatic checkin info */
	private boolean loadAutoCheckinInfo;

	/**
	 * @return the groupPNR
	 */
	public String getGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param groupPNR
	 *            the groupPNR to set
	 */
	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return Returns the loadFares.
	 */
	public boolean isLoadFares() {
		return loadFares;
	}

	/**
	 * @param loadFares
	 *            The loadFares to set.
	 */
	public void setLoadFares(boolean loadFares) {
		this.loadFares = loadFares;
	}

	/**
	 * @return Returns the loadPaxAvaBalance.
	 */
	public boolean isLoadPaxAvaBalance() {
		return loadPaxAvaBalance;
	}

	/**
	 * @param loadPaxAvaBalance
	 *            The loadPaxAvaBalance to set.
	 */
	public void setLoadPaxAvaBalance(boolean loadPaxAvaBalance) {
		this.loadPaxAvaBalance = loadPaxAvaBalance;
	}

	/**
	 * @return Returns the loadSegView.
	 */
	public boolean isLoadSegView() {
		return loadSegView;
	}

	/**
	 * @param loadSegView
	 *            The loadSegView to set.
	 */
	public void setLoadSegView(boolean loadSegView) {
		this.loadSegView = loadSegView;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the loadLocalTimes.
	 */
	public boolean isLoadLocalTimes() {
		return loadLocalTimes;
	}

	/**
	 * @param loadLocalTimes
	 *            The loadLocalTimes to set.
	 */
	public void setLoadLocalTimes(boolean loadLocalTimes) {
		this.loadLocalTimes = loadLocalTimes;
	}

	/**
	 * @return Returns the loadLastUserNote.
	 */
	public boolean isLoadLastUserNote() {
		return loadLastUserNote;
	}

	/**
	 * @param loadLastUserNote
	 *            The loadLastUserNote to set.
	 */
	public void setLoadLastUserNote(boolean loadLastUserNote) {
		this.loadLastUserNote = loadLastUserNote;
	}

	/**
	 * @return Returns the recordAudit.
	 */
	public boolean isRecordAudit() {
		return recordAudit;
	}

	/**
	 * @param recordAudit
	 *            The recordAudit to set.
	 */
	public void setRecordAudit(boolean recordAudit) {
		this.recordAudit = recordAudit;
	}

	/**
	 * @return Returns the loadSegViewBookingTypes.
	 */
	public boolean isLoadSegViewBookingTypes() {
		return loadSegViewBookingTypes;
	}

	/**
	 * @param loadSegViewBookingTypes
	 *            The loadSegViewBookingTypes to set.
	 */
	public void setLoadSegViewBookingTypes(boolean loadSegViewBookingTypes) {
		this.loadSegViewBookingTypes = loadSegViewBookingTypes;
	}

	/**
	 * @return Returns the loadOndChargesView.
	 */
	public boolean isLoadOndChargesView() {
		return loadOndChargesView;
	}

	/**
	 * @param loadOndChargesView
	 *            The loadOndChargesView to set.
	 */
	public void setLoadOndChargesView(boolean loadOndChargesView) {
		this.loadOndChargesView = loadOndChargesView;
	}

	/**
	 * @return the loadPaxPaymentOndBreakdownView
	 */
	public boolean isLoadPaxPaymentOndBreakdownView() {
		return loadPaxPaymentOndBreakdownView;
	}

	/**
	 * @param loadPaxPaymentOndBreakdownView
	 *            the loadPaxPaymentOndBreakdownView to set
	 */
	public void setLoadPaxPaymentOndBreakdownView(boolean loadPaxPaymentOndBreakdownView) {
		this.loadPaxPaymentOndBreakdownView = loadPaxPaymentOndBreakdownView;
	}

	/**
	 * @return the loadExternalPaxPayments
	 */
	public boolean isLoadExternalPaxPayments() {
		return loadExternalPaxPayments;
	}

	/**
	 * @param loadExternalPaxPayments
	 *            the loadExternalPaxPayments to set
	 */
	public void setLoadExternalPaxPayments(boolean loadExternalPaxPayments) {
		this.loadExternalPaxPayments = loadExternalPaxPayments;
	}

	/**
	 * @return Returns the loadSegViewFareCategoryTypes.
	 */
	public boolean isLoadSegViewFareCategoryTypes() {
		return loadSegViewFareCategoryTypes;
	}

	/**
	 * @param loadSegViewFareCategoryTypes
	 *            The loadSegViewFareCategoryTypes to set.
	 */
	public void setLoadSegViewFareCategoryTypes(boolean loadSegViewFareCategoryTypes) {
		this.loadSegViewFareCategoryTypes = loadSegViewFareCategoryTypes;
	}

	public boolean isLoadSeatingInfo() {
		return loadSeatingInfo;
	}

	public void setLoadSeatingInfo(boolean loadSeatingInfo) {
		this.loadSeatingInfo = loadSeatingInfo;
	}

	/**
	 * @return the loadSegViewReturnGroupId
	 */
	public boolean isLoadSegViewReturnGroupId() {
		return loadSegViewReturnGroupId;
	}

	/**
	 * @param loadSegViewReturnGroupId
	 *            the loadSegViewReturnGroupId to set
	 */
	public void setLoadSegViewReturnGroupId(boolean loadSegViewReturnGroupId) {
		this.loadSegViewReturnGroupId = loadSegViewReturnGroupId;
	}

	public boolean isLoadMealInfo() {
		return loadMealInfo;
	}

	public void setLoadMealInfo(boolean loadMealInfo) {
		this.loadMealInfo = loadMealInfo;
	}

	/**
	 * @return Returns the loadSSRInfo.
	 */
	public boolean isLoadSSRInfo() {
		return loadSSRInfo;
	}

	/**
	 * @param loadSSRInfo
	 *            The loadSSRInfo to set.
	 */
	public void setLoadSSRInfo(boolean loadSSRInfo) {
		this.loadSSRInfo = loadSSRInfo;
	}

	public Map<String, String> getMapPrivilegeIds() {
		return mapPrivilegeIds;
	}

	public void setMapPrivilegeIds(Map<String, String> mapPrivilegeIds) {
		this.mapPrivilegeIds = mapPrivilegeIds;
	}

	public ApplicationEngine getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(ApplicationEngine appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public void setExtRecordLocatorId(String extRecordLocator) {
		this.extRecordLocatorId = extRecordLocator;
	}

	public String getExtRecordLocatorId() {
		return extRecordLocatorId;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public String getBookingCreationDate() {
		return bookingCreationDate;
	}

	public void setBookingCreationDate(String bookingCreationDate) {
		this.bookingCreationDate = bookingCreationDate;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public Long getInterlineAgreementId() {
		return interlineAgreementId;
	}

	public void setInterlineAgreementId(Long interlineAgreementId) {
		this.interlineAgreementId = interlineAgreementId;
	}

	/**
	 * @return the loadBaggageInfo
	 */
	public boolean isLoadBaggageInfo() {
		return loadBaggageInfo;
	}

	/**
	 * @param loadBaggageInfo
	 *            the loadBaggageInfo to set
	 */
	public void setLoadBaggageInfo(boolean loadBaggageInfo) {
		this.loadBaggageInfo = loadBaggageInfo;
	}

	/**
	 * @return
	 */
	public boolean isLoadEtickets() {
		return loadEtickets;
	}

	/**
	 * @param loadEtickets
	 */
	public void setLoadEtickets(boolean loadEtickets) {
		this.loadEtickets = loadEtickets;
	}

	public String toString() {
		StringBuffer objDetails = new StringBuffer();
		objDetails.append(" Pnrmodes details - ");
		objDetails.append(" pnr - " + (this.getPnr() == null ? "[null]" : this.getPnr()));
		objDetails.append(" groupPNR - " + (this.getGroupPNR() == null ? "[null]" : this.getGroupPNR()));
		objDetails.append(" marketingAirlineCode - "
				+ (this.getMarketingAirlineCode() == null ? "[null]" : this.getMarketingAirlineCode()));
		objDetails.append(" airlineCode - " + (this.getAirlineCode() == null ? "[null]" : this.getAirlineCode()));
		return objDetails.toString();
	}

	public boolean isLoadRefundableTaxInfo() {
		return loadRefundableTaxInfo;
	}

	public void setLoadRefundableTaxInfo(boolean loadRefundableTaxInfo) {
		this.loadRefundableTaxInfo = loadRefundableTaxInfo;
	}

	/**
	 * @return the loadOriginCountry
	 */
	public boolean isLoadOriginCountry() {
		return loadOriginCountry;
	}

	/**
	 * @param loadOriginCountry
	 *            the loadOriginCountry to set
	 */
	public void setLoadOriginCountry(boolean loadOriginCountry) {
		this.loadOriginCountry = loadOriginCountry;
	}

	/**
	 * @return the callingAirlineCode
	 */
	public String getCallingAirlineCode() {
		return callingAirlineCode;
	}

	/**
	 * @param callingAirlineCode
	 *            the callingAirlineCode to set
	 */
	public void setCallingAirlineCode(String callingAirlineCode) {
		this.callingAirlineCode = callingAirlineCode;
	}

	public boolean isSkipPromoAdjustment() {
		return skipPromoAdjustment;
	}

	public void setSkipPromoAdjustment(boolean skipPromoAdjustment) {
		this.skipPromoAdjustment = skipPromoAdjustment;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public boolean isLoadAirportTransferInfo() {
		return loadAirportTransferInfo;
	}

	public void setLoadAirportTransferInfo(boolean loadAirportTransferInfo) {
		this.loadAirportTransferInfo = loadAirportTransferInfo;
	}

	public boolean isLoadClassifyUN() {
		return loadClassifyUN;
	}

	public void setLoadClassifyUN(boolean loadClassifyUN) {
		this.loadClassifyUN = loadClassifyUN;
	}

	public boolean isLoadExternalReference() {
		return loadExternalReference;
	}

	public void setLoadExternalReference(boolean loadExternalReference) {
		this.loadExternalReference = loadExternalReference;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the loadAutoCheckinInfo
	 */
	public boolean isLoadAutoCheckinInfo() {
		return loadAutoCheckinInfo;
	}

	/**
	 * @param loadAutoCheckinInfo
	 *            the loadAutoCheckinInfo to set
	 */
	public void setLoadAutoCheckinInfo(boolean loadAutoCheckinInfo) {
		this.loadAutoCheckinInfo = loadAutoCheckinInfo;
	}

	public boolean isLoadGOQUOAmounts() {
		return loadGOQUOAmounts;
	}

	public void setLoadGOQUOAmounts(boolean loadGOQUOAmounts) {
		this.loadGOQUOAmounts = loadGOQUOAmounts;
	}



}
