package com.isa.thinair.airproxy.api.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryBookingTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryFinancialTotalsTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryOndChargeTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPassengerTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryPaymentTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class ItineraryValueFormatterUtil {
	public static final String VALUE_SEPERATOR = ".";
	public static final String VALUE_CURRENCY_SEPERATOR = " ";
	public static final String VALUE_ADDER = "+";
	public static final String baseCurrency = AppSysParamsUtil.getBaseCurrency();

	public static ItineraryDTO removeDecimalsInBaseCurrencyValues(ItineraryDTO itineraryDTO) {

		for (ItineraryPassengerTO itineraryPassengerTO : itineraryDTO.getPassengers()) {
			ItineraryFinancialTotalsTO financialTotalsTO = itineraryPassengerTO.getBaseCurrencyFinancials();
			if (financialTotalsTO != null) {
				financialTotalsTO.setTotalBalance(safareDecimalRemover(financialTotalsTO.getTotalBalance()));
				financialTotalsTO.setTotalCharges(safareDecimalRemover(financialTotalsTO.getTotalCharges()));
				financialTotalsTO.setTotalFare(safareDecimalRemover(financialTotalsTO.getTotalFare()));
				financialTotalsTO.setTotalPaidAmount(safareDecimalRemover(financialTotalsTO.getTotalPaidAmount()));
			}

			Map<ItineraryFinancialTotalsTO, List<ItineraryPaymentTO>> payments = itineraryPassengerTO.getPayments();

			if (payments != null && payments.size() > 0) {
				for (ItineraryFinancialTotalsTO paymentTO : payments.keySet()) {

					if (paymentTO != null) {
						for (ItineraryPaymentTO itineraryPaymentTO : payments.get(paymentTO)) {

							if (itineraryPaymentTO != null && (baseCurrencyCheck(itineraryPaymentTO.getPaidCurrency()))) {
								itineraryPaymentTO.setPaidAmount(safareDecimalRemover(itineraryPaymentTO.getPaidAmount()));
							}
						}
						if (baseCurrencyCheck(paymentTO.getCurrency())) {
							paymentTO.setTotalPaidAmount(safareDecimalRemover(paymentTO.getTotalPaidAmount()));
						}
					}
				}
			}
		}
		Collection<ItineraryOndChargeTO> ondChargeTOColl = itineraryDTO.getOndCharges();

		if (ondChargeTOColl != null && ondChargeTOColl.size() > 0) {
			for (ItineraryOndChargeTO ondChargeTOs : ondChargeTOColl) {
				if (ondChargeTOs != null) {
					for (ItineraryChargeTO charges : ondChargeTOs.getCharges()) {
						String amount = charges.getAmount();
						charges.setAmount(safareDecimalRemover(amount));
					}
					ondChargeTOs.setTotal(new BigDecimal(safareDecimalRemover(ondChargeTOs.getTotal())), false);
				}
			}

		}
		ItineraryBookingTO itineraryBookingTO = itineraryDTO.getBooking();
		ItineraryFinancialTotalsTO financialsPaidTO = itineraryBookingTO.getPaidCurrencyFinancials();
		if (financialsPaidTO != null) {
			String totalBalance = financialsPaidTO.getTotalBalance();
			String totalCharges = financialsPaidTO.getTotalCharges();
			String totalFare = financialsPaidTO.getTotalFare();
			String totalPaidAmount = financialsPaidTO.getTotalPaidAmount();
			financialsPaidTO.setTotalBalance(safareDecimalRemover(totalBalance));
			financialsPaidTO.setTotalCharges(safareDecimalRemover(totalCharges));
			financialsPaidTO.setTotalFare(safareDecimalRemover(totalFare));
			financialsPaidTO.setTotalPaidAmount(safareDecimalRemover(totalPaidAmount));
		}

		ItineraryFinancialTotalsTO paidCurrencyFinancials = itineraryBookingTO.getBaseCurrencyFinancials();
		if (paidCurrencyFinancials != null) {
			paidCurrencyFinancials.setTotalBalance(safareDecimalRemover(paidCurrencyFinancials.getTotalBalance()));
			paidCurrencyFinancials.setTotalCharges(safareDecimalRemover(paidCurrencyFinancials.getTotalCharges()));
			paidCurrencyFinancials.setTotalFare(safareDecimalRemover(paidCurrencyFinancials.getTotalFare()));
			paidCurrencyFinancials.setTotalPaidAmount(safareDecimalRemover(paidCurrencyFinancials.getTotalPaidAmount()));
		}
		return itineraryDTO;
	}

	public static String safareDecimalRemover(String inputStr) {
		if (inputStr != null) {
			String returnString = "";
			if (inputStr.indexOf(VALUE_SEPERATOR) == -1) {
				return inputStr;
			} else {
				if (inputStr.indexOf(VALUE_CURRENCY_SEPERATOR) == -1) {
					return inputStr.substring(0, inputStr.indexOf(VALUE_SEPERATOR));
				} else {
					if (inputStr.indexOf(VALUE_ADDER) == -1) {
						if (baseCurrencyCheck(inputStr)) {
							return inputStr.substring(0, inputStr.indexOf(VALUE_SEPERATOR)) + " " + baseCurrency;
						} else {
							return inputStr;
						}
					} else {
						while (inputStr.indexOf(VALUE_ADDER) != -1) {
							String val = inputStr.substring(0, inputStr.indexOf(VALUE_ADDER)).trim();
							inputStr = inputStr.substring(inputStr.indexOf(VALUE_ADDER) + 1).trim();
							if (baseCurrencyCheck(val)) {
								returnString += val.substring(0, val.indexOf(VALUE_SEPERATOR)) + " " + baseCurrency;
							} else {
								returnString += val;
							}
							returnString += VALUE_CURRENCY_SEPERATOR + VALUE_ADDER + VALUE_CURRENCY_SEPERATOR;
						}
						if (baseCurrencyCheck(inputStr)) {
							returnString += inputStr.substring(0, inputStr.indexOf(VALUE_SEPERATOR)) + " " + baseCurrency;
						} else {
							returnString += inputStr;
						}
						return returnString;
					}
				}
			}
		}
		return "";
	}

	private static boolean baseCurrencyCheck(String inputStr) {
		if (inputStr.indexOf(VALUE_CURRENCY_SEPERATOR) == -1) {
			if (baseCurrency.equals(inputStr.trim())) {
				return true;
			}
		} else {
			if (baseCurrency.equals(inputStr.substring(inputStr.indexOf(VALUE_CURRENCY_SEPERATOR), inputStr.length()).trim())) {
				return true;
			}
		}
		return false;
	}
}