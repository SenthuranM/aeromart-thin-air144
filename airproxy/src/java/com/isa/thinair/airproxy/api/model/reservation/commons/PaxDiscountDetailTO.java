package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;

public class PaxDiscountDetailTO implements Serializable {

	private static final long serialVersionUID = 1L;
	List<DiscountChargeTO> paxDiscountChargeTOs;
	List<DiscountChargeTO> infantDiscountChargeTOs;
	private int paxSequence;
	private String paxType;

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public BigDecimal getPaxTotalDiscountAmount() {
		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!getPaxDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discountChargeTO : getPaxDiscountChargeTOs()) {
				totalDiscount = AccelAeroCalculator.add(totalDiscount, discountChargeTO.getDiscountAmount());

			}
		}

		return totalDiscount;
	}

	public BigDecimal getInfantTotalDiscountAmount() {
		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isInfantChargesExist() && !getInfantDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discountChargeTO : getInfantDiscountChargeTOs()) {
				totalDiscount = AccelAeroCalculator.add(totalDiscount, discountChargeTO.getDiscountAmount());

			}
		}

		return totalDiscount;
	}

	public BigDecimal getTotalDiscount() {
		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!getPaxDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discountChargeTO : getPaxDiscountChargeTOs()) {
				totalDiscount = AccelAeroCalculator.add(totalDiscount, discountChargeTO.getDiscountAmount());

			}
		}
		if (isInfantChargesExist() && !getInfantDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discountChargeTO : getInfantDiscountChargeTOs()) {
				totalDiscount = AccelAeroCalculator.add(totalDiscount, discountChargeTO.getDiscountAmount());

			}
		}

		return totalDiscount;
	}

	public boolean isInfantChargesExist() {
		if (getInfantDiscountChargeTOs() != null && !getInfantDiscountChargeTOs().isEmpty()) {
			return true;
		}
		return false;
	}

	public List<DiscountChargeTO> getPaxDiscountChargeTOs() {
		if (paxDiscountChargeTOs == null)
			paxDiscountChargeTOs = new ArrayList<DiscountChargeTO>();

		return paxDiscountChargeTOs;
	}

	public List<DiscountChargeTO> getInfantDiscountChargeTOs() {

		if (infantDiscountChargeTOs == null)
			infantDiscountChargeTOs = new ArrayList<DiscountChargeTO>();

		return infantDiscountChargeTOs;
	}

	public void addDiscountChargeTO(String chargeCode, String chargeGroup, String segment, BigDecimal discount, boolean isInfant,
			Integer chargeRateId, Collection<Integer> flightSegmentIds, String externalChargeCode) {
		DiscountChargeTO dis = new DiscountChargeTO();
		dis.setChargeCode(chargeCode);
		dis.setDiscountAmount(discount);
		dis.setSegmentCode(segment);
		dis.setChargeGroupCode(chargeGroup);
		dis.setRateId(chargeRateId);
		dis.setFlightSegmentIds(flightSegmentIds);
		dis.setExternalChargeCode(externalChargeCode);
		if (!isInfant) {
			getPaxDiscountChargeTOs().add(dis);
		} else {
			getInfantDiscountChargeTOs().add(dis);
		}

	}

	public PaxDiscountDetailTO splitInfant(int infantSequence) {
		PaxDiscountDetailTO paxDiscountDetailTO = null;

		if (getInfantDiscountChargeTOs() != null && !getInfantDiscountChargeTOs().isEmpty()) {
			paxDiscountDetailTO = new PaxDiscountDetailTO();
			paxDiscountDetailTO.setPaxSequence(infantSequence);
			paxDiscountDetailTO.getPaxDiscountChargeTOs().addAll(getInfantDiscountChargeTOs());
			paxDiscountDetailTO.setPaxType(PaxTypeTO.INFANT);

			getInfantDiscountChargeTOs().clear();
		}

		return paxDiscountDetailTO;

	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public Collection<DiscountChargeTO> getDiscountChargeTOsByFltSegId(Collection<Integer> fltSegIds) {
		Collection<DiscountChargeTO> discChargeTOs = new ArrayList<DiscountChargeTO>();
		if (fltSegIds != null && !fltSegIds.isEmpty() && getPaxDiscountChargeTOs() != null
				&& !getPaxDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discChargeTO : getPaxDiscountChargeTOs()) {
				if (discChargeTO.getFlightSegmentIds().containsAll(fltSegIds)) {
					discChargeTOs.add(discChargeTO);
				}
			}
		}
		return discChargeTOs;
	}

	public Collection<DiscountChargeTO> getDiscountTOsBySegmentChargeCode(Collection<Integer> fltSegIds, String chargeCode) {
		Collection<DiscountChargeTO> discChargeTOs = new ArrayList<DiscountChargeTO>();
		if (fltSegIds != null && !fltSegIds.isEmpty() && getPaxDiscountChargeTOs() != null
				&& !getPaxDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discChargeTO : getPaxDiscountChargeTOs()) {
				if (discChargeTO.getFlightSegmentIds().containsAll(fltSegIds) && discChargeTO.getChargeCode().equals(chargeCode)) {

					discChargeTOs.add(discChargeTO);
				}
			}
		}
		return discChargeTOs;
	}

	public Map<Collection<Integer>, BigDecimal> getDiscountTotalMapByFltSegId() {

		Map<Collection<Integer>, BigDecimal> discountByFltSegIdMap = new HashMap<Collection<Integer>, BigDecimal>();
		if (getPaxDiscountChargeTOs() != null && !getPaxDiscountChargeTOs().isEmpty()) {
			for (DiscountChargeTO discChargeTO : getPaxDiscountChargeTOs()) {

				BigDecimal amount = discChargeTO.getDiscountAmount();
				if (discountByFltSegIdMap.get(discChargeTO.getFlightSegmentIds()) != null) {
					amount = AccelAeroCalculator.add(amount, discountByFltSegIdMap.get(discChargeTO.getFlightSegmentIds()));

				}

				discountByFltSegIdMap.put(discChargeTO.getFlightSegmentIds(), amount);

			}
		}
		return discountByFltSegIdMap;
	}
	
	public BigDecimal getDiscountTotalByFlightSegments(Collection<Integer> fltSegIds, boolean withInfant) {
		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (fltSegIds != null && !fltSegIds.isEmpty() && getPaxDiscountChargeTOs() != null
				&& !getPaxDiscountChargeTOs().isEmpty()) {

			for (DiscountChargeTO discChargeTO : getPaxDiscountChargeTOs()) {
				if (discChargeTO.getFlightSegmentIds().containsAll(fltSegIds)) {
					totalDiscount = AccelAeroCalculator.add(totalDiscount, discChargeTO.getDiscountAmount());
				}
			}
			if (withInfant && isInfantChargesExist()) {
				for (DiscountChargeTO infDiscChargeTO : getInfantDiscountChargeTOs()) {
					if (infDiscChargeTO.getFlightSegmentIds().containsAll(fltSegIds)) {
						totalDiscount = AccelAeroCalculator.add(totalDiscount, infDiscChargeTO.getDiscountAmount());
					}
				}
			}

		}
		return totalDiscount;
	}
	
	public BigDecimal getSegmentDiscountTotalByChargeGroup(Collection<Integer> fltSegIds, Collection<String> chargeGroups,
			boolean withInfant) {
		BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (chargeGroups != null && !chargeGroups.isEmpty() && fltSegIds != null && !fltSegIds.isEmpty()
				&& getPaxDiscountChargeTOs() != null && !getPaxDiscountChargeTOs().isEmpty()) {

			for (DiscountChargeTO discChargeTO : getPaxDiscountChargeTOs()) {
				if (discChargeTO.getFlightSegmentIds().containsAll(fltSegIds)
						&& chargeGroups.contains(discChargeTO.getChargeGroupCode())) {
					totalDiscount = AccelAeroCalculator.add(totalDiscount, discChargeTO.getDiscountAmount());
				}
			}
			if (withInfant && isInfantChargesExist()) {
				for (DiscountChargeTO infDiscChargeTO : getInfantDiscountChargeTOs()) {
					if (infDiscChargeTO.getFlightSegmentIds().containsAll(fltSegIds)
							&& chargeGroups.contains(infDiscChargeTO.getChargeGroupCode())) {
						totalDiscount = AccelAeroCalculator.add(totalDiscount, infDiscChargeTO.getDiscountAmount());
					}
				}
			}

		}
		return totalDiscount;
	}
	
	public BigDecimal getTotalDiscountByExternalChargeCode(String externalChargeCode) {
		BigDecimal discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getPaxDiscountChargeTOs() != null && !getPaxDiscountChargeTOs().isEmpty()
				&& !StringUtil.isNullOrEmpty(externalChargeCode)) {

			for (DiscountChargeTO discountChargeTO : getPaxDiscountChargeTOs()) {
				if (externalChargeCode.equals(discountChargeTO.getExternalChargeCode())
						&& discountChargeTO.getDiscountAmount().doubleValue() > 0) {

					discount = AccelAeroCalculator.add(discount, discountChargeTO.getDiscountAmount());
				}
			}

		}

		return discount;
	}

}
