package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class TravelerInfoSummaryTO implements Serializable {

	private final static long serialVersionUID = 1L;
	protected List<PassengerTypeQuantityTO> passengerTypeQuantityList;
	private int payablePaxCount = -1;

	public List<PassengerTypeQuantityTO> getPassengerTypeQuantityList() {
		if (passengerTypeQuantityList == null) {
			passengerTypeQuantityList = new ArrayList<PassengerTypeQuantityTO>();
		}
		return this.passengerTypeQuantityList;
	}

	public PassengerTypeQuantityTO addNewPassengerTypeQuantityTO() {
		PassengerTypeQuantityTO passengerTypeQuantityTO = new PassengerTypeQuantityTO();
		this.getPassengerTypeQuantityList().add(passengerTypeQuantityTO);
		return passengerTypeQuantityTO;
	}

	public int getPayablePaxCount() {
		if (payablePaxCount == -1) {
			payablePaxCount = 0;
			for (PassengerTypeQuantityTO paxTypeTo : passengerTypeQuantityList) {
				if (PaxTypeTO.ADULT.equals(paxTypeTo.getPassengerType()) || PaxTypeTO.CHILD.equals(paxTypeTo.getPassengerType())) {
					payablePaxCount += paxTypeTo.getQuantity();
				}
			}
		}

		return payablePaxCount;
	}
}
