package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ONDExternalChargeTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected boolean flexiSelected = false;
	protected List<ExternalChargeTO> externalCharges = new ArrayList<ExternalChargeTO>();
	protected BigDecimal totalOndExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
	private int ondSequence;

	public List<ExternalChargeTO> getExternalCharges() {
		if (externalCharges == null) {
			externalCharges = new ArrayList<ExternalChargeTO>();
		}
		return externalCharges;
	}

	public boolean isFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(boolean flexiSelected) {
		if (!flexiSelected) {
			externalCharges = new ArrayList<ExternalChargeTO>();
			totalOndExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		}
		this.flexiSelected = flexiSelected;
	}

	public BigDecimal getTotalOndExternalCharges() {
		return totalOndExternalCharges;
	}

	public void addExternalCharges(List<ExternalChargeTO> externalCharges) {
		for (ExternalChargeTO externalCharge : externalCharges) {
			addExternalCharge(externalCharge);
		}
	}

	/**
	 * When an external charge is added to the ONDExternalChargeTO, it will automatically set the flexi selected status
	 * to true as well as add up the total of the external charges
	 * 
	 * @param externalCharge
	 */
	public void addExternalCharge(ExternalChargeTO externalCharge) {
		if (externalCharge != null && externalCharge.getAmount() != null) {
			setFlexiSelected(true);
		}
		getExternalCharges().add(externalCharge);
		totalOndExternalCharges = AccelAeroCalculator.add(totalOndExternalCharges, externalCharge.getAmount());
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}
}
