package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCMealRequestDTO implements Serializable {

	private final static long serialVersionUID = 1L;
	private String transactionIdentifier;

	private String cabinClass;
	private List<FlightSegmentTO> flightSegment;

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public List<FlightSegmentTO> getFlightSegment() {
		if (flightSegment == null) {
			flightSegment = new ArrayList<FlightSegmentTO>();
		}
		return this.flightSegment;
	}
}
