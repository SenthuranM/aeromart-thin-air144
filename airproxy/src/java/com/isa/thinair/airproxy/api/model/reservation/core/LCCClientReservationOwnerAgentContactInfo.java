package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

public class LCCClientReservationOwnerAgentContactInfo implements Serializable {

	private static final long serialVersionUID = 219779388364784949L;
	private String agentCarrier;
	private String agentEmail;
	private String agentName;
	private String agentTelephone;
	private String agentIATANumber;

	/**
	 * @return the agentCarrier
	 */
	public String getAgentCarrier() {
		return agentCarrier;
	}

	/**
	 * @param agentCarrier
	 *            the agentCarrier to set
	 */
	public void setAgentCarrier(String agentCarrier) {
		this.agentCarrier = agentCarrier;
	}

	/**
	 * @return the agentEmail
	 */
	public String getAgentEmail() {
		return agentEmail;
	}

	/**
	 * @param agentEmail
	 *            the agentEmail to set
	 */
	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the agentTelephone
	 */
	public String getAgentTelephone() {
		return agentTelephone;
	}

	/**
	 * @param agentTelephone
	 *            the agentTelephone to set
	 */
	public void setAgentTelephone(String agentTelephone) {
		this.agentTelephone = agentTelephone;
	}

	/**
	 * @return the agentIATANumber
	 */
	public String getAgentIATANumber() {
		return agentIATANumber;
	}

	/**
	 * @param agentIATANumber the agentIATANumber to set
	 */
	public void setAgentIATANumber(String agentIATANumber) {
		this.agentIATANumber = agentIATANumber;
	}
}