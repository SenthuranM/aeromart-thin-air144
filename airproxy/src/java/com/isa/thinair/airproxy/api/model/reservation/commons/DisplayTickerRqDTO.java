package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

/**
 * Class to initiate display request when loading the reservation modified but
 * not syncd prices
 * 
 * @author rajitha
 *
 */
public class DisplayTickerRqDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pnr;
	private Integer gdsId;
	private Set<LCCClientReservationPax> reservationPaxSet;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public Set<LCCClientReservationPax> getReservationPaxSet() {
		return reservationPaxSet;
	}

	public void setReservationPaxSet(Set<LCCClientReservationPax> reservationPaxSet) {
		this.reservationPaxSet = reservationPaxSet;
	}

}
