/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold agent credit data transfer information
 * 
 * @author Nilindra Fernando
 */
public class LCCClientOnAccountPaymentInfo implements Serializable, LCCClientPaymentInfo {

	private static final long serialVersionUID = 1L;

	/** Holds total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String totalAmountCurrencyCode;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	/** Holds the agent code */
	private String agentCode;

	/** Holds the Marketing carrier Agent code.Only relevant for dry bookings */
	private String externalAgentCode;

	/** MC of the dry booking */
	private String externalCarrierCode;

	private Integer paxSequence;

	private Date txnDateTime;

	private String carrierCode;

	private String lccUniqueTnxId;

	private String agentName;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private boolean includeExternalCharges;

	private String payReference;

	private Integer actualPaymentMethod;

	private String originalPayReference;

	private String recieptNumber;

	private String carrierVisePayments;

	private String paymentMethod;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private boolean offlinePayment = false;
	
	private boolean nonRefundable = false;	

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paxSequence
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	@Override
	public LCCClientOnAccountPaymentInfo clone() {
		LCCClientOnAccountPaymentInfo lccClientOnAccountPaymentInfo = new LCCClientOnAccountPaymentInfo();
		lccClientOnAccountPaymentInfo.setTotalAmount(this.getTotalAmount());
		lccClientOnAccountPaymentInfo.setTotalAmountCurrencyCode(this.getTotalAmountCurrencyCode());
		lccClientOnAccountPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) this.getPayCurrencyDTO().clone());
		lccClientOnAccountPaymentInfo.setPaxSequence(this.getPaxSequence());
		lccClientOnAccountPaymentInfo.setAgentCode(this.getAgentCode());
		lccClientOnAccountPaymentInfo.setLccUniqueTnxId(this.getLccUniqueTnxId());
		lccClientOnAccountPaymentInfo.setPaymentTnxReference(this.getPaymentTnxReference());
		lccClientOnAccountPaymentInfo.setOriginalPaymentUID(this.getOriginalPaymentUID());
		lccClientOnAccountPaymentInfo.setPaymentTnxDateTime(this.getPaymentTnxDateTime());

		return lccClientOnAccountPaymentInfo;
	}

	public Date getTxnDateTime() {
		return this.txnDateTime;
	}

	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	/**
	 * @return the totalAmountCurrencyCode
	 */
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	/**
	 * @param totalAmountCurrencyCode
	 *            the totalAmountCurrencyCode to set
	 */
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCClientOnAccountPaymentInfo [agentCode=");
		builder.append(agentCode);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", externalAgentCode=");
		builder.append(externalAgentCode);
		builder.append(", externalCarrierCode=");
		builder.append(externalCarrierCode);
		builder.append(", lccUniqueTnxId=");
		builder.append(lccUniqueTnxId);
		builder.append(", paxSequence=");
		builder.append(paxSequence);
		builder.append(", payCurrencyDTO=");
		builder.append(payCurrencyDTO);
		builder.append(", totalAmount=");
		builder.append(totalAmount);
		builder.append(", totalAmountCurrencyCode=");
		builder.append(totalAmountCurrencyCode);
		builder.append(", txnDateTime=");
		builder.append(txnDateTime);
		builder.append(", agentName=");
		builder.append(agentName);
		builder.append(", paymentTnxReference=");
		builder.append(paymentTnxReference);
		builder.append(", originalPaymentUID=");
		builder.append(originalPaymentUID);
		builder.append(", paymentTnxDateTime=");
		builder.append(paymentTnxDateTime);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;

	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;

	}

	@Override
	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	@Override
	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	public String getPayReference() {
		return payReference;
	}

	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	public String getOriginalPayReference() {
		return originalPayReference;
	}

	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	/**
	 * @return the externalAgentCode
	 */
	public String getExternalAgentCode() {
		return externalAgentCode;
	}

	/**
	 * @param externalAgentCode
	 *            the externalAgentCode to set
	 */
	public void setExternalAgentCode(String externalAgentCode) {
		this.externalAgentCode = externalAgentCode;
	}

	/**
	 * @return the externalCarrierCode
	 */
	public String getExternalCarrierCode() {
		return externalCarrierCode;
	}

	/**
	 * @param externalCarrierCode
	 *            the externalCarrierCode to set
	 */
	public void setExternalCarrierCode(String externalCarrierCode) {
		this.externalCarrierCode = externalCarrierCode;
	}

	public boolean isDryPaymrtInfo() {
		return ((externalAgentCode != null && !externalAgentCode.isEmpty()) && (externalCarrierCode != null && !externalCarrierCode
				.isEmpty()));
	}

	public Integer getActualPaymentMethod() {

		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {

		this.actualPaymentMethod = actualPaymentMethod;
	}

	public String getRecieptNumber() {
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	/**
	 * @return the carrierVisePayments
	 */
	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	/**
	 * @param carrierVisePayments
	 *            the carrierVisePayments to set
	 */
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	public String getOriginalPaymentUID() {
		return originalPaymentUID;
	}

	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	public Date getPaymentTnxDateTime() {
		return this.paymentTnxDateTime;
	}

	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	public boolean isOfflinePayment() {
		return this.offlinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.offlinePayment = isOfflinePayment;
	}

	@Override
	public boolean isNonRefundable() {
		return this.nonRefundable;
	}
	
	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;		
	}
}