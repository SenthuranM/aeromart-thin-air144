package com.isa.thinair.airproxy.api.utils.converters;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AuxillaryHelper {

	private Log logger = LogFactory.getLog(AuxillaryHelper.class);
	private ReservationAuxilliaryBD auxiBD = null;

	public AuxillaryHelper(ReservationAuxilliaryBD auxiBD) {
		this.auxiBD = auxiBD;
	}

	public boolean hasFlownSegments(List<Integer> pnrSegIds) {
		try {
			return auxiBD.hasFlownSegments(pnrSegIds);
		} catch (ModuleException e) {
			logger.error("Flown segment calculatino failed", e);
		}
		return false;
	}

	public Map<Integer, String> getFltSegWiseBkgClasses(List<Integer> pnrSegIds) {
		try {
			return auxiBD.getFltSegWiseBkgClasses(pnrSegIds);
		} catch (ModuleException e) {
			logger.error("Existing flt segment booking classes fetching failed", e);
		}
		return new HashMap<Integer, String>();
	}

	public Map<Integer, String> getFltSegWiseCabinClasses(List<Integer> pnrSegIds) {
		try {
			return auxiBD.getFltSegWiseCabinClasses(pnrSegIds);
		} catch (ModuleException e) {
			logger.error("Existing flt segment booking classes fetching failed", e);
		}
		return new HashMap<Integer, String>();
	}

	public Map<Integer, Date> getFltSegWiseLastFQDates(List<Integer> flownPnrSegIds) {
		try {
			return auxiBD.getFltSegWiseLastFQDates(flownPnrSegIds);
		} catch (ModuleException e) {
			logger.error("Last FQ date for pnr seg ids retrieval failed", e);
		}
		return new HashMap<Integer, Date>();
	}
}
