package com.isa.thinair.airproxy.api.model.reservation.availability;

public class FlightPriceRQ extends BaseAvailRQ {

	private static final long serialVersionUID = -7369226125851266489L;

	public FlightPriceRQ() {
	}

	public FlightPriceRQ(BaseAvailRQ baseAvailRQ) {
		super(baseAvailRQ);
	}
}
