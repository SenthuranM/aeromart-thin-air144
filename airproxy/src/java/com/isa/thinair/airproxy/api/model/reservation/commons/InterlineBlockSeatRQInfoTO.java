package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;

public class InterlineBlockSeatRQInfoTO implements Serializable {

	private static final long serialVersionUID = -8083265001060144571L;

	private String transactionIdentifier;

	private List<String> outFlightRPHList;

	private List<String> retFlightRPHList;

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public List<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(List<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public List<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(List<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}
}
