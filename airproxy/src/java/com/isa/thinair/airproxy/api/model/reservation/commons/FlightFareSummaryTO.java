package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Calendar View- Availability And Fare Display(Right Now IBE Calendar View)
 * 
 * @author pkarunanayake
 * 
 */
public class FlightFareSummaryTO implements Serializable, Cloneable, Comparable<FlightFareSummaryTO> {

	private static final long serialVersionUID = 1L;

	private BigDecimal baseFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean seatAvailable = false;

	private BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal serviceTaxes = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal serviceTaxPercentageForFares = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal calDisplayAmout = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String logicalCCCode;

	private String logicalCCDesc;

	private boolean selected;

	private boolean withFlexi;

	private Integer flexiRuleId;

	private boolean isFreeFlexi;

	private int logicalCCRank;

	private String comment;

	private int noOfAvailableSeats = -1;

	private String visibleChannelName;

	private Integer bundledFarePeriodId;

	private Set<String> bookingCodes;

	private Map<String, String> segmentBookingClasses;

	private Set<String> bundledFareFreeServiceNames;

	private BigDecimal bundleFareFee;

	private Integer rank;
	
	private String imageUrl;

	private Map<FarePriceOnd, BigDecimal> minimumFareByPriceOnd;

	private BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO;

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	@JSON(serialize = false)
	public BigDecimal getBaseFareAmount() {
		return baseFareAmount;
	}

	public void setBaseFareAmount(BigDecimal baseFareAmount) {
		this.baseFareAmount = baseFareAmount;
	}

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	@JSON(serialize = false)
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getCalDisplayAmout() {
		return calDisplayAmout;
	}

	public void setCalDisplayAmout(BigDecimal calDisplayAmout) {
		this.calDisplayAmout = calDisplayAmout;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCDesc() {
		return logicalCCDesc;
	}

	public void setLogicalCCDesc(String logicalCCDesc) {
		this.logicalCCDesc = logicalCCDesc;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isWithFlexi() {
		return withFlexi;
	}

	public void setWithFlexi(boolean withFlexi) {
		this.withFlexi = withFlexi;
	}

	public boolean isFreeFlexi() {
		return isFreeFlexi;
	}

	public void setFreeFlexi(boolean isFreeFlexi) {
		this.isFreeFlexi = isFreeFlexi;
	}

	public int getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(int logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		FlightFareSummaryTO flightFareSummaryTO = (FlightFareSummaryTO) super.clone();
		flightFareSummaryTO.setLogicalCCCode(this.logicalCCCode);
		flightFareSummaryTO.setLogicalCCDesc(this.logicalCCDesc);
		flightFareSummaryTO.setSeatAvailable(this.seatAvailable);
		flightFareSummaryTO.setLogicalCCRank(this.logicalCCRank);
		flightFareSummaryTO.setComment(this.comment);
		flightFareSummaryTO.setNoOfAvailableSeats(getNoOfAvailableSeats());
		flightFareSummaryTO.setVisibleChannelName(this.visibleChannelName);
		flightFareSummaryTO.setMinimumFareByPriceOnd(this.minimumFareByPriceOnd);
		flightFareSummaryTO.setBundleFareDescriptionTemplateDTO(this.bundleFareDescriptionTemplateDTO);
		return flightFareSummaryTO;
	}

	public int getNoOfAvailableSeats() {
		return noOfAvailableSeats;
	}

	public void setNoOfAvailableSeats(int noOfAvailableSeats) {
		this.noOfAvailableSeats = noOfAvailableSeats;
	}

	public void setVisibleChannelName(String visibleChannelName) {
		this.visibleChannelName = visibleChannelName;
	}

	public String getVisibleChannelName() {
		return visibleChannelName;
	}
	
	public Integer getBundledFarePeriodId() {
		return bundledFarePeriodId;
	}

	public void setBundledFarePeriodId(Integer bundledFarePeriodId) {
		this.bundledFarePeriodId = bundledFarePeriodId;
	}

	public Set<String> getBookingCodes() {
		return bookingCodes;
	}

	public void setBookingCodes(Set<String> bookingCodes) {
		this.bookingCodes = bookingCodes;
	}

	public Map<String, String> getSegmentBookingClasses() {
		return segmentBookingClasses;
	}

	public void setSegmentBookingClasses(Map<String, String> segmentBookingClasses) {
		this.segmentBookingClasses = segmentBookingClasses;
	}

	public Set<String> getBundledFareFreeServiceNames() {
		return bundledFareFreeServiceNames;
	}

	public void setBundledFareFreeServiceNames(Set<String> bundledFareFreeServiceNames) {
		this.bundledFareFreeServiceNames = bundledFareFreeServiceNames;
	}

	public BigDecimal getBundleFareFee() {
		return bundleFareFee;
	}

	public void setBundleFareFee(BigDecimal bundleFareFee) {
		this.bundleFareFee = bundleFareFee;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getFlexiRuleId() {
		return flexiRuleId;
	}

	public void setFlexiRuleId(Integer flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	public BigDecimal getServiceTaxPercentageForFares() {
		return serviceTaxPercentageForFares;
	}

	public void setServiceTaxPercentageForFares(BigDecimal serviceTaxPercentageForFares) {
		this.serviceTaxPercentageForFares = serviceTaxPercentageForFares;
	}

	@Override
	public int compareTo(FlightFareSummaryTO obj) {
		int comp = this.rank.compareTo(obj.rank);

		if (comp == 0) {
			if (this.withFlexi) {
				comp = 1;
			} else {
				comp = -1;
			}
		}

		return comp;
	}

	public void setMinimumFareByPriceOnd(Map<FarePriceOnd, BigDecimal> minimumFareByPriceOnd) {
		this.minimumFareByPriceOnd = minimumFareByPriceOnd;
	}

	public Map<FarePriceOnd, BigDecimal> getMinimumFareByPriceOnd() {
		return minimumFareByPriceOnd;
	}

	public BigDecimal getServiceTaxes() {
		return serviceTaxes;
	}

	public void setServiceTaxes(BigDecimal serviceTaxes) {
		this.serviceTaxes = serviceTaxes;
	}

	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateDTO() {
		return bundleFareDescriptionTemplateDTO;
	}

	public void setBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO) {
		this.bundleFareDescriptionTemplateDTO = bundleFareDescriptionTemplateDTO;
	}
}
