package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;

public class CommonAncillaryModifyAssembler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* Holds which system should the reservation be saved to */
	private SYSTEM targetSystem;

	/* Holds the transaction identifier */
	private String transactionIdentifier;

	/* Holds temporary payment map */
	private Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap;

	/* Holds the pnr for the segment */
	private String pnr;

	private Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap;

	private Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxAddAncillaryMap;

	private Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxRemoveAncillaryMap;

	private Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> paxUpdateAncillaryMap;

	private String version;

	/* Holds the lcc payment reference number */
	private String lccPaymentRefNumber;

	private String ownerAgentCode;

	private String reservationStatus;

	private Collection<LCCClientReservationSegment> lccSegments;

	private Collection<LCCClientReservationPax> lccPassengers;

	private boolean autoCnxEnabled;

	private boolean hasBufferTimeAutoCnx;

	private boolean skipOwnAutoCnxCalculation;

	private AutoCancellationInfo autoCancellationInfo;

	private boolean skipCutoverValidation;

	private BigDecimal serviceTaxRatio = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean isPartialPayment = false;
	
	private boolean forceConfirm;

	private boolean applyPenaltyForAnciModification;

	private LoyaltyPaymentInfo loyaltyPaymentInfo;

	public CommonAncillaryModifyAssembler() {
		passengerPaymentMap = new HashMap<Integer, LCCClientPaymentAssembler>();
		paxAddAncillaryMap = new HashMap<Integer, List<LCCSelectedSegmentAncillaryDTO>>();
		paxRemoveAncillaryMap = new HashMap<Integer, List<LCCSelectedSegmentAncillaryDTO>>();
		paxUpdateAncillaryMap = new HashMap<Integer, List<LCCSelectedSegmentAncillaryDTO>>();
		setLccPaymentRefNumber(UniqueIDGenerator.generate());
	}

	public void addPassengerPayment(Integer paxSequence, LCCClientPaymentAssembler payment) {
		passengerPaymentMap.put(paxSequence, payment);
	}

	public void removeAncillary(Integer paxSequence, List<LCCSelectedSegmentAncillaryDTO> ancillary) {
		paxRemoveAncillaryMap.put(paxSequence, ancillary);
	}

	public void addAncillary(Integer paxSequence, List<LCCSelectedSegmentAncillaryDTO> ancillary) {
		paxAddAncillaryMap.put(paxSequence, ancillary);
	}

	public void updateAncillary(Integer seqNumber, List<LCCSelectedSegmentAncillaryDTO> ancillariesToUpdate) {
		paxUpdateAncillaryMap.put(seqNumber, ancillariesToUpdate);
	}

	public SYSTEM getTargetSystem() {
		return targetSystem;
	}

	public void setTargetSystem(SYSTEM targetSystem) {
		this.targetSystem = targetSystem;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public Map<Integer, CommonCreditCardPaymentInfo> getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	public void setTemporyPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<Integer, LCCClientPaymentAssembler> getPassengerPaymentMap() {
		return passengerPaymentMap;
	}

	public void setPassengerPaymentMap(Map<Integer, LCCClientPaymentAssembler> passengerPaymentMap) {
		this.passengerPaymentMap = passengerPaymentMap;
	}

	public Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> getPaxAddAncillaryMap() {
		return paxAddAncillaryMap;
	}

	public Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> getPaxRemoveAncillaryMap() {
		return paxRemoveAncillaryMap;
	}

	public Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> getPaxUpdateAncillaryMap() {
		return paxUpdateAncillaryMap;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLccPaymentRefNumber() {
		return lccPaymentRefNumber;
	}

	public void setLccPaymentRefNumber(String lccPaymentRefNumber) {
		this.lccPaymentRefNumber = lccPaymentRefNumber;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public Collection<LCCClientReservationSegment> getLccSegments() {
		return lccSegments;
	}

	public void setLccSegments(Collection<LCCClientReservationSegment> lccSegments) {
		this.lccSegments = lccSegments;
	}

	public Collection<LCCClientReservationPax> getLccPassengers() {
		return lccPassengers;
	}

	public void setLccPassengers(Collection<LCCClientReservationPax> lccPassengers) {
		this.lccPassengers = lccPassengers;
	}

	public boolean isAutoCnxEnabled() {
		return autoCnxEnabled;
	}

	public void setAutoCnxEnabled(boolean autoCnxEnabled) {
		this.autoCnxEnabled = autoCnxEnabled;
	}

	public boolean isHasBufferTimeAutoCnx() {
		return hasBufferTimeAutoCnx;
	}

	public void setHasBufferTimeAutoCnx(boolean hasBufferTimeAutoCnx) {
		this.hasBufferTimeAutoCnx = hasBufferTimeAutoCnx;
	}

	public boolean isSkipOwnAutoCnxCalculation() {
		return skipOwnAutoCnxCalculation;
	}

	public void setSkipOwnAutoCnxCalculation(boolean skipOwnAutoCnxCalculation) {
		this.skipOwnAutoCnxCalculation = skipOwnAutoCnxCalculation;
	}

	public AutoCancellationInfo getAutoCancellationInfo() {
		return autoCancellationInfo;
	}

	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		this.autoCancellationInfo = autoCancellationInfo;
	}

	public boolean isSkipCutoverValidation() {
		return skipCutoverValidation;
	}

	public void setSkipCutoverValidation(boolean skipCutoverValidation) {
		this.skipCutoverValidation = skipCutoverValidation;
	}

	public BigDecimal getServiceTaxRatio() {
		return serviceTaxRatio;
	}

	public void setServiceTaxRatio(BigDecimal serviceTaxRatio) {
		this.serviceTaxRatio = serviceTaxRatio;
	}

	public boolean isPartialPayment() {
		return isPartialPayment;
	}

	public void setPartialPayment(boolean isPartialPayment) {
		this.isPartialPayment = isPartialPayment;
	}

	public boolean isApplyPenaltyForAnciModification() {
		return applyPenaltyForAnciModification;
	}

	public void setApplyPenaltyForAnciModification(boolean applyPenaltyForAnciModification) {
		this.applyPenaltyForAnciModification = applyPenaltyForAnciModification;
	}

	public LoyaltyPaymentInfo getLoyaltyPaymentInfo() {
		return loyaltyPaymentInfo;
	}

	public void setLoyaltyPaymentInfo(LoyaltyPaymentInfo loyaltyPaymentInfo) {
		this.loyaltyPaymentInfo = loyaltyPaymentInfo;
	}

	public boolean isForceConfirm() {
		return forceConfirm;
	}

	public void setForceConfirm(boolean forceConfirm) {
		this.forceConfirm = forceConfirm;
	}

}
