package com.isa.thinair.airproxy.api;

import java.util.Collection;
import java.util.HashSet;

public interface LccSubOperationConstants {

	class ModifyReservation {
		public static final String ALTER_PAX_INFO =  "ALTER_PAX_INFO";
		public static final String ALTER_USER_NOTE =  "ALTER_USER_NOTE";

		public static Collection<String> getPrivileges(String operation) {
			Collection<String> privileges = null;

			if (operation.equals(ALTER_PAX_INFO)) {
				privileges = new HashSet<String>();
				privileges.add("xbe.res.alt.modify.departed.segment.name");

			} else if (operation.equals(ALTER_USER_NOTE)) {
				privileges = new HashSet<String>();
				privileges.add("xbe.res.alt.modify.departed.segment.usernote");

			}

			return privileges;
		}
	}
}
