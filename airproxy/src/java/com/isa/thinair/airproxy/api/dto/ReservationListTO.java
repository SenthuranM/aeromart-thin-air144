package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class ReservationListTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String rowNo;

	private String pnrNo;

	private String originatorPnr;

	private String paxName;

	private String pnrStatus;

	private Date releaseTimeStamp;

	private String marketingAirlineCode;

	private String airlineCode;

	private Collection<FlightInfoTO> flightInfo;

	private Long interlineAgreementId;

	private String paxEmail;

	private String paxPhone;

	private String paxMobile;
	
	private Boolean sendPromoEmail;

	/**
	 * @return the pnrNo
	 */
	public String getPnrNo() {
		return pnrNo;
	}

	/**
	 * @param pnrNo
	 *            the pnrNo to set
	 */
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the pnrStatus
	 */
	public String getPnrStatus() {
		return pnrStatus;
	}

	/**
	 * @param pnrStatus
	 *            the pnrStatus to set
	 */
	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	/**
	 * @return the flightInfo
	 */
	public Collection<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	/**
	 * @param flightInfo
	 *            the flightInfo to set
	 */
	public void setFlightInfo(Collection<FlightInfoTO> flightInfo) {
		this.flightInfo = flightInfo;
	}

	/**
	 * @return the originatorPnr
	 */
	public String getOriginatorPnr() {
		return originatorPnr;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	public String getRowNo() {
		return rowNo;
	}

	public void setRowNo(String rowNo) {
		this.rowNo = rowNo;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public Long getInterlineAgreementId() {
		return interlineAgreementId;
	}

	public void setInterlineAgreementId(Long interlineAgreementId) {
		this.interlineAgreementId = interlineAgreementId;
	}

	public Date getReleaseTimeStamp() {
		return releaseTimeStamp;
	}

	public void setReleaseTimeStamp(Date releaseTimeStamp) {
		this.releaseTimeStamp = releaseTimeStamp;
	}

	public String getPaxEmail() {
		return paxEmail;
	}

	public void setPaxEmail(String paxEmail) {
		this.paxEmail = paxEmail;
	}

	public String getPaxPhone() {
		return paxPhone;
	}

	public void setPaxPhone(String paxPhone) {
		this.paxPhone = paxPhone;
	}

	public String getPaxMobile() {
		return paxMobile;
	}

	public void setPaxMobile(String paxMobile) {
		this.paxMobile = paxMobile;
	}

	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}

}
