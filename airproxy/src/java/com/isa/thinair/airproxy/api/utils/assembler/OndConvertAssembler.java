package com.isa.thinair.airproxy.api.utils.assembler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Ond Convert Assebler Class
 * 
 * @author Dilan Anuruddha
 * 
 */

public class OndConvertAssembler {
	private OriginDestinationInformationTO outbound = null;
	private OriginDestinationInformationTO inbound = null;

	public OndConvertAssembler(Collection<OriginDestinationInformationTO> ondInfoList) {
		OriginDestinationInformationTO minTO = null;
		OriginDestinationInformationTO maxTO = null;
		for (OriginDestinationInformationTO ondTO : ondInfoList) {
			if (minTO == null || maxTO == null) {
				minTO = ondTO;
				maxTO = ondTO;
			} else {
				if (maxTO.getPreferredDate().compareTo(ondTO.getPreferredDate()) <= 0) {
					maxTO = ondTO;
				}
				if (minTO.getPreferredDate().compareTo(ondTO.getPreferredDate()) > 0) {
					minTO = ondTO;
				}
			}
		}
		if (minTO == maxTO) {
			outbound = minTO;
		} else {
			outbound = minTO;
			inbound = maxTO;
		}
	}

	/**
	 * 
	 * @return
	 */
	public Date getOutboundDateTimeStart() {
		return outbound.getDepartureDateTimeStart();
	}

	/**
	 * 
	 * @return
	 */
	public Date getOutboundDateTimeEnd() {
		return outbound.getDepartureDateTimeEnd();
	}

	public Date getOutboundArrivalDateTimeStart() {
		return outbound.getArrivalDateTimeStart();
	}

	public Date getOutboundArrivalDateTimeEnd() {
		return outbound.getArrivalDateTimeEnd();
	}

	/**
	 * 
	 * @return
	 */
	public Date getInboundDateTimeStart() {
		if (inbound == null)
			return null;
		return inbound.getDepartureDateTimeStart();
	}

	public Date getInboundDateTimeEnd() {
		if (inbound == null)
			return null;
		return inbound.getDepartureDateTimeEnd();
	}

	/**
	 * 
	 * @param selectedDate
	 * @return
	 */
	private Date getSelectedEndTime(Date selectedDate) {
		Date d = new Date(selectedDate.getTime());
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}

	/**
	 * 
	 * @param selectedDate
	 * @param date
	 * @return
	 */
	private Date getSelectedStartTime(Date selectedDate, Date departDate) {
		Date d = new Date(selectedDate.getTime());
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		if (departDate.after(c.getTime())) {
			return departDate;
		}
		return c.getTime();
	}

	/**
	 * 
	 * @return
	 */
	public Date getSelectedInboundDateTimeStart() {
		if (inbound == null)
			return null;
		return getSelectedStartTime(inbound.getPreferredDate(), inbound.getDepartureDateTimeStart());
	}

	/**
	 * 
	 * @return
	 */
	public Date getSelectedInboundDateTimeEnd() {
		if (inbound == null)
			return null;
		return getSelectedEndTime(inbound.getPreferredDate());
	}

	/**
	 * 
	 * @return
	 */
	public Date getSelectedOutboundDateTimeStart() {
		return getSelectedStartTime(outbound.getPreferredDate(), outbound.getDepartureDateTimeStart());
	}

	/**
	 * 
	 * @return
	 */
	public Date getSelectedOutboundDateTimeEnd() {
		return getSelectedEndTime(outbound.getPreferredDate());
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReturnFlight() {
		return (inbound != null);
	}

	/**
	 * 
	 * @return
	 */
	public String getInboundAirport() {
		if (inbound == null) {
			return outbound.getDestination();
		} else {
			return inbound.getOrigin();
		}
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<Integer> extractSegIdsFromOnds(List<OriginDestinationOptionTO> list) {
		List<Integer> segIds = new ArrayList<Integer>();
		for (OriginDestinationOptionTO ondTO : list) {
			for (FlightSegmentTO ondOptTO : ondTO.getFlightSegmentList()) {
				segIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(ondOptTO.getFlightRefNumber()));
			}
		}
		return segIds;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static Date extractFirstDepDateFromOnds(List<OriginDestinationOptionTO> list, Date preferedDate) {
		Date firstDepDate = null;
		for (OriginDestinationOptionTO ondTO : list) {
			for (FlightSegmentTO ondOptTO : ondTO.getFlightSegmentList()) {
				firstDepDate = FlightRefNumberUtil.getDepartureDateFromFlightRPH(ondOptTO.getFlightRefNumber());
				if (CalendarUtil.isEqualStartTimeOfDate(preferedDate, firstDepDate)) {
					return firstDepDate;
				}
				break;
			}
		}
		return firstDepDate;
	}

	/**
	 * 
	 * @return
	 */
	public Collection<Integer> getOutboundSegmentIds() {
		Collection<Integer> outSegIds = new ArrayList<Integer>();
		if (outbound.getOrignDestinationOptions() != null) {
			outSegIds = extractSegIdsFromOnds(outbound.getOrignDestinationOptions());
		}
		return outSegIds;
	}

	/**
	 * 
	 * @return
	 */
	public Collection<Integer> getInboundSegmentIds() {
		Collection<Integer> inSegIds = new ArrayList<Integer>();
		if (inbound != null && inbound.getOrignDestinationOptions() != null) {
			inSegIds = extractSegIdsFromOnds(inbound.getOrignDestinationOptions());
		}
		return inSegIds;
	}

	/**
	 * 
	 * @return
	 */
	public String getOutboundAirport() {
		return outbound.getOrigin();
	}
}
