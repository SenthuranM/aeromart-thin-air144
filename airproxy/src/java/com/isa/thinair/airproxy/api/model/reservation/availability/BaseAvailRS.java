package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BaseAvailRS implements Serializable {

	private static final long serialVersionUID = 1L;

	private PriceInfoTO selectedPriceFlightInfo;

	/**
	 * Segment fare prince information. Only applicable if selectedPriceFlightInfo contains return fare price
	 * information.
	 */
	private PriceInfoTO selectedSegmentFlightPriceInfo;

	private String transactionIdentifier;

	private OpenReturnOptionsTO openReturnOptionsTO;

	private List<OriginDestinationInformationTO> originDestinationInformationList = null;

	private List<String> reservationParms = null;

	private boolean onHoldEnabled;

	private HashMap<String, List<String>> subJourneyGroupMap = new HashMap<String, List<String>>();

	private Set<ServiceTaxContainer> applicableServiceTaxes;

	public List<OriginDestinationInformationTO> getOriginDestinationInformationList() {
		if (this.originDestinationInformationList == null)
			originDestinationInformationList = new ArrayList<OriginDestinationInformationTO>();
		return originDestinationInformationList;
	}

	public void setOriginDestinationInformationList(List<OriginDestinationInformationTO> originDestinationInformationList) {
		this.originDestinationInformationList = originDestinationInformationList;
	}

	private int getOndSequence(OriginDestinationInformationTO x) throws ModuleException {
		if (x.getOrignDestinationOptions() != null && x.getOrignDestinationOptions().size() > 0) {
			if (x.getOrignDestinationOptions() != null && x.getOrignDestinationOptions().size() > 0) {
				return x.getOrignDestinationOptions().get(0).getFlightSegmentList().get(0).getOndSequence();
			}
		}
		throw new ModuleException("Invalid ond result");
	}

	public List<OriginDestinationInformationTO> getOrderedOndListBySequence() {
		List<OriginDestinationInformationTO> list = new ArrayList<OriginDestinationInformationTO>(
				getOriginDestinationInformationList());
		Collections.sort(list, new Comparator<OriginDestinationInformationTO>() {

			@Override
			public int compare(OriginDestinationInformationTO o1, OriginDestinationInformationTO o2) {
				try {
					return getOndSequence(o1) - getOndSequence(o2);
				} catch (ModuleException e) {
					return 0;
				}
			}
		});
		return list;
	}

	public List<OriginDestinationInformationTO> getOrderedOriginDestinationInformationList() {
		List<OriginDestinationInformationTO> list = new ArrayList<OriginDestinationInformationTO>(
				getOriginDestinationInformationList());
		Collections.sort(list);
		return list;
	}

	public PriceInfoTO getSelectedPriceFlightInfo() {
		return selectedPriceFlightInfo;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public void setSelectedPriceFlightInfo(PriceInfoTO selectedPriceFlightInfo) {
		this.selectedPriceFlightInfo = selectedPriceFlightInfo;
	}

	/**
	 * @return the openReturnOptionsTO
	 */
	public OpenReturnOptionsTO getOpenReturnOptionsTO() {
		return openReturnOptionsTO;
	}

	/**
	 * @param openReturnOptionsTO
	 *            the openReturnOptionsTO to set
	 */
	public void setOpenReturnOptionsTO(OpenReturnOptionsTO openReturnOptionsTO) {
		this.openReturnOptionsTO = openReturnOptionsTO;
	}

	public List<String> getReservationParms() {
		return reservationParms;
	}

	public void setReservationParms(List<String> reservationParms) {
		this.reservationParms = reservationParms;
	}

	/**
	 * @return the onHoldEnabled
	 */
	@Deprecated
	public boolean isOnHoldEnabled() {
		return onHoldEnabled;
	}

	/**
	 * @param onHoldEnabled
	 *            the onHoldEnabled to set
	 */
	@Deprecated
	public void setOnHoldEnabled(boolean onHoldEnabled) {
		this.onHoldEnabled = onHoldEnabled;
	}

	public PriceInfoTO getSelectedSegmentFlightPriceInfo() {
		return selectedSegmentFlightPriceInfo;
	}

	public void setSelectedSegmentFlightPriceInfo(PriceInfoTO selectedSegmentFlightPriceInfo) {
		this.selectedSegmentFlightPriceInfo = selectedSegmentFlightPriceInfo;
	}

	public Set<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (applicableServiceTaxes == null) {
			applicableServiceTaxes = new HashSet<ServiceTaxContainer>();
		}
		return applicableServiceTaxes;
	}

	public void setApplicableServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		this.applicableServiceTaxes = applicableServiceTaxes;
	}

	public HashMap<String, List<String>> getSubJourneyGroupMap() {
		return subJourneyGroupMap;
	}

	public void setSubJourneyGroupMap(HashMap<String, List<String>> subJourneyGroupMap) {
		this.subJourneyGroupMap = subJourneyGroupMap;
	}

	public List<FlightSegmentTO> getAllFlightSegments() {
		List<FlightSegmentTO> allFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

		int ondSequence = 0;
		for (OriginDestinationInformationTO ondInfo : getOrderedOriginDestinationInformationList()) {

			if (ondInfo.getOrignDestinationOptions() != null) {
				for (OriginDestinationOptionTO ondOption : ondInfo.getOrignDestinationOptions()) {
					if (ondOption.isSelected()) {
						List<FlightSegmentTO> flightSegmentList = ondOption.getFlightSegmentList();
						if (flightSegmentList != null && !flightSegmentList.isEmpty()) {
							for (FlightSegmentTO flightSegmentTO : flightSegmentList) {
								flightSegmentTO.setOndSequence(ondSequence);
							}

							allFlightSegmentTOs.addAll(flightSegmentList);
						}
					}
				}
			}

			ondSequence++;
		}

		return allFlightSegmentTOs;
	}

	public void fixMissingOndSequence() {
		int ondSequence = 0;
		for (OriginDestinationInformationTO ondInfo : getOrderedOriginDestinationInformationList()) {
			ondInfo.fixMissingOndSequence(ondSequence++);
		}
	}
}
