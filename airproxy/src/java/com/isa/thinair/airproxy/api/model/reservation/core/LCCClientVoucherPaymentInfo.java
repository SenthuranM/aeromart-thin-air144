package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * @author chethiya
 *
 */
public class LCCClientVoucherPaymentInfo implements LCCClientPaymentInfo, Serializable {

	private static final long serialVersionUID = 7728893914026176222L;

	private BigDecimal totalAmount;

	private PayCurrencyDTO payCurrencyDTO;

	private Integer paxSequence;

	private String carrierCode;

	private String totalAmountCurrencyCode;

	private String lccUniqueTnxId;

	private String remarks;

	private BigDecimal payCurrencyAmount;

	private boolean includeExternalCharges = false;

	private String payReference;

	private Integer actualPaymentMethod;

	private String originalPayReference;

	private String recieptNumber;

	private String carrierVisePayments;

	private String paymentTnxReference;

	private String originalPaymentUID;

	private Date paymentTnxDateTime;

	private boolean isOfflinePayment = false;

	private Date txnDateTime;

	private VoucherDTO voucherDTO;
	
	private boolean nonRefundable = false;	

	@Override
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	@Override
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	@Override
	public Integer getPaxSequence() {
		return paxSequence;
	}

	@Override
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	@Override
	public String getCarrierCode() {
		return carrierCode;
	}

	@Override
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	@Override
	public String getTotalAmountCurrencyCode() {
		return totalAmountCurrencyCode;
	}

	@Override
	public void setTotalAmountCurrencyCode(String totalAmountCurrencyCode) {
		this.totalAmountCurrencyCode = totalAmountCurrencyCode;
	}

	@Override
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	@Override
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}

	@Override
	public String getRemarks() {
		return remarks;
	}

	@Override
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	@Override
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	@Override
	public boolean includeExternalCharges() {
		return includeExternalCharges;
	}

	@Override
	public void setIncludeExternalCharges(boolean includeExternalCharges) {
		this.includeExternalCharges = includeExternalCharges;
	}

	@Override
	public String getPayReference() {
		return payReference;
	}

	@Override
	public void setPayReference(String payReference) {
		this.payReference = payReference;
	}

	@Override
	public Integer getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	@Override
	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	@Override
	public String getOriginalPayReference() {
		return originalPayReference;
	}

	@Override
	public void setOriginalPayReference(String originalPayReference) {
		this.originalPayReference = originalPayReference;
	}

	@Override
	public String getRecieptNumber() {
		return recieptNumber;
	}

	@Override
	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	@Override
	public String getCarrierVisePayments() {
		return carrierVisePayments;
	}

	@Override
	public void setCarrierVisePayments(String carrierVisePayments) {
		this.carrierVisePayments = carrierVisePayments;
	}

	@Override
	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	@Override
	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

	@Override
	public String getOriginalPaymentUID() {
		return originalPaymentUID;
	}

	@Override
	public void setOriginalPaymentUID(String originalPaymentUID) {
		this.originalPaymentUID = originalPaymentUID;
	}

	@Override
	public Date getPaymentTnxDateTime() {
		return paymentTnxDateTime;
	}

	@Override
	public void setPaymentTnxDateTime(Date paymentTnxDateTime) {
		this.paymentTnxDateTime = paymentTnxDateTime;
	}

	@Override
	public boolean isOfflinePayment() {
		return isOfflinePayment;
	}

	@Override
	public void setOfflinePayment(boolean isOfflinePayment) {
		this.isOfflinePayment = isOfflinePayment;
	}

	@Override
	public Date getTxnDateTime() {
		return txnDateTime;
	}

	@Override
	public void setTxnDateTime(Date txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}
	
	@Override
	public boolean isNonRefundable() {		
		return this.nonRefundable;
	}
	
	@Override
	public void setNonRefundable(boolean isNonRefundable) {
		this.nonRefundable = isNonRefundable;		
	}

}
