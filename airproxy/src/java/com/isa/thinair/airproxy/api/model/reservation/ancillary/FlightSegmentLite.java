package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

public class FlightSegmentLite implements Serializable {

	private static final long serialVersionUID = 1L;

	private String filghtDesignator;

	private String segmentCode;

	private String flightSegmentRPH;

	public String getFilghtDesignator() {
		return filghtDesignator;
	}

	public void setFilghtDesignator(String filghtDesignator) {
		this.filghtDesignator = filghtDesignator;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightSegmentRPH() {
		return flightSegmentRPH;
	}

	public void setFlightSegmentRPH(String flightSegmentRPH) {
		this.flightSegmentRPH = flightSegmentRPH;
	}
}
