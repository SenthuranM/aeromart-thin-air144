package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

public class DiscountChargeTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String chargeCode;
	private String chargeGroupCode;
	private Integer rateId; // holds charge_rate_id or fare_id
	private BigDecimal discountAmount;
	private String segmentCode;
	private Collection<Integer> flightSegmentIds;
	private boolean isConsumed;
	private String externalChargeCode;

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	public Integer getRateId() {
		return rateId;
	}

	public void setRateId(Integer chargeRateId) {
		this.rateId = chargeRateId;
	}

	public Collection<Integer> getFlightSegmentIds() {
		return flightSegmentIds;
	}

	public void setFlightSegmentIds(Collection<Integer> flightSegmentIds) {
		this.flightSegmentIds = flightSegmentIds;
	}

	public boolean isConsumed() {
		return isConsumed;
	}

	public void setConsumed(boolean isConsumed) {
		this.isConsumed = isConsumed;
	}

	public String getExternalChargeCode() {
		return externalChargeCode;
	}

	public void setExternalChargeCode(String externalChargeCode) {
		this.externalChargeCode = externalChargeCode;
	}
}
