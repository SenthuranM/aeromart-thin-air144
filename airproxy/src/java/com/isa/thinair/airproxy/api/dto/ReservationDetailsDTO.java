/**
 * 
 */
package com.isa.thinair.airproxy.api.dto;

import java.util.Date;

/**
 * This DTO is used to get the details for the grid in self reprotection page
 * 
 * @author suneth
 *
 */
/**
 * @author dev
 *
 */
public class ReservationDetailsDTO {

	private String pnr;
	
	private String paxName;
	
	private String paxType;
	
	private String from;
	
	private String to;
	
	private Date departureDateTime;
	
	private Date arrivalDateTime;
	
	private String status;
	
	private String flightSegId;
	
	public ReservationDetailsDTO(){		
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the departureDateTime
	 */
	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	/**
	 * @param departureDateTime the departureDateTime to set
	 */
	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	/**
	 * @return the arrivalDateTime
	 */
	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	/**
	 * @param arrivalDateTime the arrivalDateTime to set
	 */
	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the flightSegId
	 */
	public String getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId the flightSegId to set
	 */
	public void setFlightSegId(String flightSegId) {
		this.flightSegId = flightSegId;
	}
	
}
