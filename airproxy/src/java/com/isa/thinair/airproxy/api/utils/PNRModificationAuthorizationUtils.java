package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.PNRModifyPreValidationStatesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.mediators.ReservationValidationUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Reservation modification authorization utilities.
 * 
 * @author Duminda G
 * @since April 30, 2008
 */
public class PNRModificationAuthorizationUtils {

	private static Log log = LogFactory.getLog(PNRModificationAuthorizationUtils.class);

	/**
	 * Returns the reservation segment modifiable status
	 * 
	 * @param reservationSegmentDTO
	 * @param reservation
	 * @param privilegesKeys
	 * @param appIndicator
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean getResSegmentModifiable(ReservationSegmentDTO reservationSegmentDTO, Reservation reservation,
			Collection<String> privilegesKeys, String privilegeToCheck, ApplicationEngine appIndicator) throws ModuleException {
		/*
		 * |--------|-------------|--------------|----------------| Res FR Buffer Buffer DEP
		 * 
		 * |---If Res NOT interlined |-------If NOT CNX |-------If NOT restricted |-------If Old Res Modify Privilege
		 * but not a special fare |-------If Old Modify special fare privilege |-------If NOT Flown |-------If
		 * BufferModifiable || FR buffer Modifiable |----------If NOT Buffer privilege |--------------If NOT
		 * BufferModifiable |--------------------isHasFareRuleBuffer
		 */

		if (log.isDebugEnabled()) {
			log.debug("Inside getResSegmentModifiable for PNR [" + reservationSegmentDTO.getPnr() + "] PNR_SEG_ID ["
					+ reservationSegmentDTO.getPnrSegId() + "] ");
		}

		Collection<Integer> colPnrSegId = new ArrayList<Integer>();
		colPnrSegId.add(reservationSegmentDTO.getPnrSegId());
		PNRModifyPreValidationStatesTO statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation,
				colPnrSegId);

		// Interlined Status
		if (statesTO.isInterlinedModifiable()) {
			// Cancel Status
			if (statesTO.isCancelled()) {
				if (log.isDebugEnabled()) {
					log.debug("::::::::: [Cancel Segments cannot be modified] ");
				}
				return false;
			}

			// Restricted status
			if (statesTO.isRestricted()) {
				if (log.isDebugEnabled()) {
					log.debug("::::::::: [Restricted Segments cannot be modified] ");
				}
				return false;
			}

			// open return expiry
			if (reservationSegmentDTO.isOpenReturnSegment()
					&& !AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
				if (reservationSegmentDTO.getOpenRtConfirmBeforeZulu() != null
						&& !reservationSegmentDTO.getOpenRtConfirmBeforeZulu().after(new Date())) {
					if (log.isDebugEnabled()) {
						log.debug("::::::::: [Open return expired]");
					}
					return false;
				}
			}

			if (!isSegmentModifiableAsPerETicketStatus(reservation, reservationSegmentDTO.getPnrSegId())) {
				return false;
			}

			// is flown and has NOSHOW pax
			if (statesTO.isHasNOSHOWPax()
					&& (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator) || AuthorizationUtil.hasPrivilege(privilegesKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NOSHOW))) {

				if (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator) && !AppSysParamsUtil.isAllowModifyNoShowSegmentsIBE()) {
					return false;
				}

				log.debug("::::::::: [Flown Segments Allow noshow] ");
				return true;
			}

			// flown and flight segment with UN status
			if (statesTO.isFlown()
					&& !statesTO.isSpecialFare()
					&& reservationSegmentDTO.getDisplayStatus().equals(
							ReservationInternalConstants.ReservationSegmentStatus.FLIGHT_CANCEL)) {
				return true;
			}

			// flown and open return segment
			if (statesTO.isFlown() && !statesTO.isSpecialFare() && reservationSegmentDTO.isOpenReturnSegment()) {
				return true;
			}

			// flown and with old reservation modify privilege
			if (statesTO.isFlown() && !statesTO.isSpecialFare()
					&& AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
				return true;
			}

			// flown and with Allow Modify Expired Special Fare Segments
			if (statesTO.isFlown()
					&& statesTO.isSpecialFare()
					&& AuthorizationUtil.hasPrivilege(privilegesKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD_SPECIAL_FARE)) {
				log.debug("::::::::: [Flown Segments Allow Res special fare] ");
				return true;
			}

			// Flown
			if (statesTO.isFlown()) {
				if (log.isDebugEnabled()) {
					log.debug("::::::::: [Flown Segments cannot be modified] ");
				}
				return false;
			}

			// // Modify Old Reservation in buffer time
			// AARESAA-18944 Not required to check expire res privi within buffer,if user has buffer mod/cnx privi
			// it should allow,full fix should be to totally remove this privi after re-quote
			// if (statesTO.isInBufferTime()
			// && AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
			// return true;
			// }

			// Modify buffer status
			if (statesTO.isInBufferTime()) {
				if (AuthorizationUtil.hasPrivilege(privilegesKeys, privilegeToCheck)) {
					if (!statesTO.isHasFareRuleBuffer()) {
						return true;
					} else {
						if (AuthorizationUtil.hasPrivilege(privilegesKeys,
								PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_FARE_BUFFER)) {
							return true;
						}
					}
				} else if (statesTO.isHasFareRuleBuffer()
						&& AuthorizationUtil.hasPrivilege(privilegesKeys,
								PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_FARE_BUFFER)) {
					return true;
				} else {

					if (log.isDebugEnabled()) {
						log.debug("::::::::: [xbe.res.alt.modify.buffer does not exist OR xbe.res.alt.modify.buffer.fare does not exist] ");
					}
					return false;
				}
			} else {
				return true;
			}
		} else {
			if (log.isDebugEnabled()) {
				log.debug("::::::::: [Interlined bookings cannot be modified]");
			}
		}

		return false;
	}

	/**
	 * Returns the reservation segment modifiable states
	 * 
	 * @param reservation
	 * @param reservationSegmentDTO
	 * @param appIndicatorEnum
	 * @param mapPrivilegeIds
	 * @return
	 * @throws ModuleException
	 */
	public static boolean getResSegmentModifiable(Reservation reservation, ReservationSegmentDTO reservationSegmentDTO,
			ApplicationEngine appIndicator, Map<String, String> mapPrivilegeIds) throws ModuleException {
		Collection colPrivilegeIds = mapPrivilegeIds != null ? mapPrivilegeIds.keySet() : null;
		if (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator)) {
			colPrivilegeIds = null;
		}
		return getResSegmentModifiable(reservationSegmentDTO, reservation, colPrivilegeIds,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, appIndicator);
	}

	/**
	 * Returns interlined segments transferd
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static boolean getInterlinedTransferd(Collection<ReservationSegmentDTO> segments) throws ModuleException {
		return ReservationValidationUtils.isInterlinedTransferd(segments);
	}

	// TODO Add Privileges
	/**
	 * 
	 * @param modifyTillBufferDateTime
	 * @param modifyTillFlightClosureDateTime
	 * @param appIndicator
	 * @param mapPrivilegeIds
	 * @param b
	 * @return
	 */
	public static boolean isInterlineSegmentModifible(Date modifyTillBufferDateTime, Date modifyTillFlightClosureDateTime,
			ApplicationEngine appIndicator, Map<String, String> mapPrivilegeIds, boolean isSegmentFlown) {
		if (isSegmentFlown) {
			return false;
		}
		Collection privilegesKeys = mapPrivilegeIds != null ? mapPrivilegeIds.keySet() : null;
		if (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator)) {
			privilegesKeys = null;
		}
		String privilegeToCheck = PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX;
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MILLISECOND, -(cal.getTimeZone().getRawOffset() + cal.getTimeZone().getDSTSavings()));

		long sysTime = cal.getTimeInMillis();
		long segBuffStart = 0l;
		long segBuffEnd = 0l;

		segBuffStart = modifyTillBufferDateTime.getTime();
		segBuffEnd = modifyTillFlightClosureDateTime.getTime();

		if (sysTime > segBuffStart) {
			if (sysTime < segBuffEnd) {
				if (AuthorizationUtil.hasPrivilege(privilegesKeys, privilegeToCheck)) {
					return true;
				}
			} else if (AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
				return true;
			}
		} else {
			if (appIndicator == ApplicationEngine.XBE) {
				// segement modification refactored
				if (AuthorizationUtil.hasPrivilege(privilegesKeys,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE)
						&& AuthorizationUtil.hasPrivilege(privilegesKeys,
								PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE)) {
					return true;
				}
			} else {
				return true;
			}
		}

		return false;
	}

	public static boolean isSegmentModifiableAsPerETicketStatus(Reservation reservation, Integer pnrSegId) {
		if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
			for (ReservationPax pax : reservation.getPassengers()) {
				if( pax.geteTickets() != null){
					for (EticketTO eTicket : pax.geteTickets()) {
						if (eTicket.getPnrSegId().intValue() == pnrSegId.intValue()) {
							if (EticketStatus.CHECKEDIN.code().equals(eTicket.getTicketStatus())
									|| EticketStatus.BOARDED.code().equals(eTicket.getTicketStatus())) {
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	public static boolean isSegmentModifiableAsPerETicketStatus(LCCClientReservation reservation, Integer pnrSegId) {
		if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				if( pax.geteTickets() != null){
					for (LccClientPassengerEticketInfoTO eTicket : pax.geteTickets()) {
						if (pnrSegId != null) {
							if (Integer.parseInt(eTicket.getPnrSegId()) == pnrSegId.intValue()) {
								if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
										|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
									return false;
								}
							}
						} else {
							if (EticketStatus.CHECKEDIN.code().equals(eTicket.getPaxETicketStatus())
									|| EticketStatus.BOARDED.code().equals(eTicket.getPaxETicketStatus())) {
								return false;
							}
						}
					}
				} 
			}
		}
		return true;
	}
}
