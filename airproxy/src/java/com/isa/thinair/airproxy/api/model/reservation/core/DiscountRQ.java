package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;

/**
 * 
 * @author M.Rikaz
 * 
 */
public class DiscountRQ implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum DISCOUNT_METHOD {
		FARE_DISCOUNT, DOM_FARE_DISCOUNT, PROMOTION;

		public static DISCOUNT_METHOD getEnum(String strEnum) {
			for (DISCOUNT_METHOD enm : DISCOUNT_METHOD.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return DISCOUNT_METHOD.FARE_DISCOUNT;
		}

	};

	private DiscountedFareDetails discountInfoDTO;
	private Collection<PaxChargesTO> paxChargesList;
	private Collection<PassengerTypeQuantityTO> paxQtyList;
	private boolean calculateTotalOnly;
	private QuotedFareRebuildDTO quotedFareRebuildDTO;
	private Collection<OndFareDTO> ondFareDTOs;
	private boolean validateCriteria;
	private DISCOUNT_METHOD actionType;
	private SYSTEM system = SYSTEM.AA;
	private String transactionIdentifier;

	public DiscountRQ(DISCOUNT_METHOD actionType) {
		this.actionType = actionType;
	}

	public DiscountedFareDetails getDiscountInfoDTO() {
		return discountInfoDTO;
	}

	public void setDiscountInfoDTO(DiscountedFareDetails discountInfoDTO) {
		this.discountInfoDTO = discountInfoDTO;
	}

	public Collection<PaxChargesTO> getPaxChargesList() {
		return paxChargesList;
	}

	public void setPaxChargesList(Collection<PaxChargesTO> paxChargesList) {
		this.paxChargesList = paxChargesList;
	}

	public Collection<PassengerTypeQuantityTO> getPaxQtyList() {
		return paxQtyList;
	}

	public void setPaxQtyList(Collection<PassengerTypeQuantityTO> paxQtyList) {
		this.paxQtyList = paxQtyList;
	}

	public DISCOUNT_METHOD getActionType() {
		return actionType;
	}

	public void setActionType(DISCOUNT_METHOD actionType) {
		this.actionType = actionType;
	}

	public boolean isCalculateTotalOnly() {
		return calculateTotalOnly;
	}

	public void setCalculateTotalOnly(boolean calculateTotalOnly) {
		this.calculateTotalOnly = calculateTotalOnly;
	}

	public QuotedFareRebuildDTO getQuotedFareRebuildDTO() {
		return quotedFareRebuildDTO;
	}

	public void setQuotedFareRebuildDTO(QuotedFareRebuildDTO quotedFareRebuildDTO) {
		this.quotedFareRebuildDTO = quotedFareRebuildDTO;
	}

	public Collection<OndFareDTO> getOndFareDTOs() {
		return ondFareDTOs;
	}

	public void setOndFareDTOs(Collection<OndFareDTO> ondFareDTOs) {
		this.ondFareDTOs = ondFareDTOs;
	}

	public boolean isValidateCriteria() {
		return validateCriteria;
	}

	public void setValidateCriteria(boolean validateCriteria) {
		this.validateCriteria = validateCriteria;
	}

	public SYSTEM getSystem() {
		return system;
	}

	public void setSystem(SYSTEM system) {
		this.system = system;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

}
