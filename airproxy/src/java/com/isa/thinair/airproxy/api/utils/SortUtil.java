package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Sort Utility
 * 
 * @author Zaki
 * @since Nov 03, 2009
 */
public class SortUtil {

	/**
	 * Method to sort LCCClientReservationSegment by departure date
	 * 
	 * @param setReservationSegments
	 *            A set of LCCClientReservationSegment
	 * @return An array of LCCClientReservationSegment
	 */
	public static LCCClientReservationSegment[] sort(Set<LCCClientReservationSegment> setReservationSegments) {
		int size = setReservationSegments.size();
		LCCClientReservationSegment[] arrayToSort = setReservationSegments.toArray(new LCCClientReservationSegment[size]);
		Arrays.sort(arrayToSort, new Comparator<LCCClientReservationSegment>() {
			@Override
			public int compare(LCCClientReservationSegment rs1, LCCClientReservationSegment rs2) {
				// TODO check with nili seg sequence sort is not working reverting to departure date
				return rs1.getSegmentSeq().compareTo(rs2.getSegmentSeq());
			}
		});

		return arrayToSort;
	}

	/**
	 * 
	 * @param setReservationSegments
	 * @return
	 */
	public static LCCClientReservationSegment[] sortByDepDate(Set<LCCClientReservationSegment> setReservationSegments) {
		int size = setReservationSegments.size();
		LCCClientReservationSegment[] arrayToSort = setReservationSegments.toArray(new LCCClientReservationSegment[size]);
		Arrays.sort(arrayToSort, new Comparator<LCCClientReservationSegment>() {
			@Override
			public int compare(LCCClientReservationSegment rs1, LCCClientReservationSegment rs2) {
				return rs1.getDepartureDateZulu().compareTo(rs2.getDepartureDateZulu());
			}
		});

		return arrayToSort;
	}

	/**
	 * Sort LCCClientReservationPax using the following methodology
	 * 
	 * <ol>
	 * <li>Sort by paxSequence if and only if all paxSequence fields are not null</li>
	 * <li>As a fallback will sort all pax by "AD", then "CH", then "IN" (only use for simple display purposes)</li>
	 * </ol>
	 * 
	 * @param setReservationPax
	 *            A set of LCCClientReservationPax
	 * @return An array of LCCClientReservationPax
	 */
	public static LCCClientReservationPax[] sortPax(Set<LCCClientReservationPax> setReservationPax) {

		boolean sortBypaxSequence = true;

		int size = setReservationPax.size();
		LCCClientReservationPax[] arrayToSort = setReservationPax.toArray(new LCCClientReservationPax[size]);

		if (AppSysParamsUtil.isLoadReservationOrderByPaxName()) {

			boolean isAllTba = true;

			for (LCCClientReservationPax pax : arrayToSort) {
				if (!("T B A".equals(pax.getFirstName())) && !("T B A".equals(pax.getFirstName()))) {
					isAllTba = false;
					break;
				}
			}

			if (isAllTba) {
				Arrays.sort(arrayToSort, new Comparator<LCCClientReservationPax>() {
					@Override
					public int compare(LCCClientReservationPax rp1, LCCClientReservationPax rp2) {
						return rp1.getPaxSequence().compareTo(rp2.getPaxSequence());
					}
				});

			} else {

				Arrays.sort(arrayToSort, new Comparator<LCCClientReservationPax>() {
					@Override
					public int compare(LCCClientReservationPax rp1, LCCClientReservationPax rp2) {
						return (rp1.getPaxType() + " " + rp1.getLastName().toUpperCase() + " " + rp1.getFirstName().toUpperCase())
								.compareTo(rp2.getPaxType() + " " + rp2.getLastName().toUpperCase() + " "
										+ rp2.getFirstName().toUpperCase());
					}
				});
			}
		} else {

			for (LCCClientReservationPax lccClientReservationPax : setReservationPax) {
				if (lccClientReservationPax.getPaxSequence() == null) {
					sortBypaxSequence = false;
					break;
				}
			}
			if (sortBypaxSequence) {
				Arrays.sort(arrayToSort, new Comparator<LCCClientReservationPax>() {
					@Override
					public int compare(LCCClientReservationPax rp1, LCCClientReservationPax rp2) {
						return rp1.getPaxSequence() - rp2.getPaxSequence();
					}
				});
			} else {
				Arrays.sort(arrayToSort, new Comparator<LCCClientReservationPax>() {
					@Override
					public int compare(LCCClientReservationPax rp1, LCCClientReservationPax rp2) {
						String paxType1 = rp1.getPaxType();
						String paxType2 = rp2.getPaxType();

						if (paxType1.compareTo(PaxTypeTO.ADULT) == 0 && paxType2.compareTo(PaxTypeTO.CHILD) == 0) {
							return -1;
						} else if (paxType1.compareTo(PaxTypeTO.ADULT) == 0 && paxType2.compareTo(PaxTypeTO.INFANT) == 0) {
							return -1;
						} else if (paxType1.compareTo(PaxTypeTO.CHILD) == 0 && paxType2.compareTo(PaxTypeTO.ADULT) == 0) {
							return 1;
						} else if (paxType1.compareTo(PaxTypeTO.CHILD) == 0 && paxType2.compareTo(PaxTypeTO.INFANT) == 0) {
							return -1;
						} else if (paxType1.compareTo(PaxTypeTO.INFANT) == 0 && paxType2.compareTo(PaxTypeTO.ADULT) == 0) {
							return 1;
						} else if (paxType1.compareTo(PaxTypeTO.INFANT) == 0 && paxType2.compareTo(PaxTypeTO.CHILD) == 0) {
							return 1;
						} else {
							return 0;
						}
					}
				});
			}
		}
		return arrayToSort;
	}

	/**
	 * Method to sort lCCClientAlertTO by alertID
	 * 
	 * @param LCCClientAlertTO
	 *            A set of LCCClientAlertTO
	 * @return An array of LCCClientAlertTO
	 */
	public static List<LCCClientAlertTO> sort(List<LCCClientAlertTO> lCCClientAlertTO) {
		Collections.sort(lCCClientAlertTO, new Comparator<LCCClientAlertTO>() {
			@Override
			public int compare(LCCClientAlertTO at1, LCCClientAlertTO at2) {
				return (new Integer(at2.getAlertId())).compareTo(new Integer(at1.getAlertId()));
			}
		});
		return lCCClientAlertTO;
	}

	public static List<LCCSelectedSegmentAncillaryDTO> sortPaxWiseAnci(List<LCCSelectedSegmentAncillaryDTO> lccPaxAnci) {
		Collections.sort(lccPaxAnci, new Comparator<LCCSelectedSegmentAncillaryDTO>() {
			@Override
			public int compare(LCCSelectedSegmentAncillaryDTO seg1, LCCSelectedSegmentAncillaryDTO seg2) {
				return (seg1.getFlightSegmentTO().getDepartureDateTime().compareTo(seg2.getFlightSegmentTO()
						.getDepartureDateTime()));
			}
		});
		return lccPaxAnci;
	}

	public static void sortFlightSegByDepDate(List<FlightSegmentTO> flightSegmentTOs) {
		Collections.sort(flightSegmentTOs, new Comparator<FlightSegmentTO>() {

			@Override
			public int compare(FlightSegmentTO fltSegFirst, FlightSegmentTO fltSegSecond) {
				return FlightRefNumberUtil.getDepartureDateFromFlightRPH(fltSegFirst.getFlightRefNumber()).compareTo(
						FlightRefNumberUtil.getDepartureDateFromFlightRPH(fltSegSecond.getFlightRefNumber()));
			}

		});
	}

	public static void sortFlightSegByDepZuluDate(List<FlightSegmentTO> flightSegmentTOs) {
		Collections.sort(flightSegmentTOs, new Comparator<FlightSegmentTO>() {

			@Override
			public int compare(FlightSegmentTO fltSegFirst, FlightSegmentTO fltSegSecond) {
				return fltSegFirst.getDepartureDateTimeZulu().compareTo(fltSegSecond.getDepartureDateTimeZulu());
			}

		});
	}

	public static void sortAirportServicesByFltDeparture(List<LCCFlightSegmentAirportServiceDTO> lccAvailabilityList) {
		Collections.sort(lccAvailabilityList, new Comparator<LCCFlightSegmentAirportServiceDTO>() {

			@Override
			public int compare(LCCFlightSegmentAirportServiceDTO obj1, LCCFlightSegmentAirportServiceDTO obj2) {
				return FlightRefNumberUtil
						.getDepartureDateFromFlightRPH(obj1.getFlightSegmentTO().getFlightRefNumber())
						.compareTo(
								FlightRefNumberUtil.getDepartureDateFromFlightRPH(obj2.getFlightSegmentTO().getFlightRefNumber()));
			}

		});
	}

	public static void sortByDepartureDate(List<RollForwardFlightAvailRS> rollForwardFlightAvailRSs) {
		Collections.sort(rollForwardFlightAvailRSs, new Comparator<RollForwardFlightAvailRS>() {
			@Override
			public int compare(RollForwardFlightAvailRS firstObj, RollForwardFlightAvailRS secondObj) {
				return firstObj.getDepartureDate().compareTo(secondObj.getDepartureDate());
			}
		});
	}

	public static List<FlightInfoTO> sortFlightSegBySubJourneyGroup(List<FlightInfoTO> flightInfoList) {
		Collections.sort(flightInfoList, new Comparator<FlightInfoTO>() {
			public int compare(FlightInfoTO fltInfoObj, FlightInfoTO fltInfoObj2) {
				return new Long(fltInfoObj.getSubJourneyGroup()).compareTo(new Long(fltInfoObj2.getSubJourneyGroup()));
			}
		});

		return flightInfoList;
	}

	public static List<FlightInfoTO> sortByDepatureDate(List<FlightInfoTO> flightInfoList) {
		Collections.sort(flightInfoList, new Comparator<FlightInfoTO>() {
			@Override
			public int compare(FlightInfoTO at1, FlightInfoTO at2) {
				return new Long(at1.getDepartureDateLong()).compareTo(new Long(at2.getDepartureDateLong()));
			}
		});
		return flightInfoList;
	}
	
	public static List<LCCClientCarrierOndGroup> sortByDepartureDateTime(List<LCCClientCarrierOndGroup> ondGroups) {

		Collections.sort(ondGroups, new Comparator<LCCClientCarrierOndGroup>() {
			@Override
			public int compare(LCCClientCarrierOndGroup at1, LCCClientCarrierOndGroup at2) {
				try {
					if (!StringUtil.isNullOrEmpty(at1.getDepartureDateTime())
							&& !StringUtil.isNullOrEmpty(at2.getDepartureDateTime())) {
						Date temp1 = CalendarUtil.getParsedTime(at1.getDepartureDateTime(), "yyyy-MM-dd HH:mm");
						Date objDate = CalendarUtil.getParsedTime(at2.getDepartureDateTime(), "yyyy-MM-dd HH:mm");

						if (temp1.compareTo(objDate) == 0) {
							return at1.getSegmentCode().compareTo(at2.getSegmentCode());
						}
						return temp1.compareTo(objDate);
					}
				} catch (Exception e) {
					// handle exception
				}

				return 0;
			}

		});
		return ondGroups;
	}
	
	public static LCCClientReservationSegment[] sortByLocalDepDate(Set<LCCClientReservationSegment> setReservationSegments) {
		int size = setReservationSegments.size();
		LCCClientReservationSegment[] arrayToSort = setReservationSegments.toArray(new LCCClientReservationSegment[size]);
		Arrays.sort(arrayToSort, new Comparator<LCCClientReservationSegment>() {
			@Override
			public int compare(LCCClientReservationSegment rs1, LCCClientReservationSegment rs2) {
				return rs1.getDepartureDate().compareTo(rs2.getDepartureDate());
			}
		});

		return arrayToSort;
	}
	
	/**
	 * Method to sort FlightSegmentTO by segment sequence
	 * 
	 * @param flightSegmentTOs
	 *            A list of FlightSegmentTO
	 * @return An array of FlightSegmentTO
	 */
	public static FlightSegmentTO[] sortFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		int size = flightSegmentTOs.size();
		FlightSegmentTO[] arrayToSort = flightSegmentTOs.toArray(new FlightSegmentTO[size]);
		Arrays.sort(arrayToSort, new Comparator<FlightSegmentTO>() {
			@Override
			public int compare(FlightSegmentTO rs1, FlightSegmentTO rs2) {
				// TODO check with nili seg sequence sort is not working reverting to departure date
				return Integer.valueOf(rs1.getSegmentSequence()).compareTo(Integer.valueOf(rs2.getSegmentSequence()));
			}
		});

		return arrayToSort;
	}
	
	public static List<ServiceTaxTO> sortServiceTaxByFlightRefDepartureDate(List<ServiceTaxTO> serviceTaxTOs){
		
		if(serviceTaxTOs == null){
			return null;
		}
		
		int size = serviceTaxTOs.size();
		ServiceTaxTO[] arrayToSort = serviceTaxTOs.toArray(new ServiceTaxTO[size]);
		Arrays.sort(arrayToSort, new Comparator<ServiceTaxTO>() {
			@Override
			public int compare(ServiceTaxTO rs1, ServiceTaxTO rs2) {
				return FlightRefNumberUtil.getDepartureDateFromFlightRPHString(rs1.getFlightRefNumber()).compareTo(
						FlightRefNumberUtil.getDepartureDateFromFlightRPHString(rs2.getFlightRefNumber()));
			}
		});

		return new ArrayList<>(Arrays.asList(arrayToSort));
	}
	
}