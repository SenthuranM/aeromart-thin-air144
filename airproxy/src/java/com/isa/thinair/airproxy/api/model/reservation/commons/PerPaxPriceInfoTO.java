package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class PerPaxPriceInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String passengerType;
	private String travelerRefNumber;
	private FareTypeTO passengerPrice;

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public FareTypeTO getPassengerPrice() {
		return passengerPrice;
	}

	public void setPassengerPrice(FareTypeTO passengerPrice) {
		this.passengerPrice = passengerPrice;
	}
}
