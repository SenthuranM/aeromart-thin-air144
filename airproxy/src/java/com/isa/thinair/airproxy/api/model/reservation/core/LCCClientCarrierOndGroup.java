package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

public class LCCClientCarrierOndGroup implements Serializable {

	private static final long serialVersionUID = -2136458756213276088L;

	private String carrierCode;

	private String segmentCode;

	private String carrierOndGroupRPH;

	private String departureDateTime;

	private String subStatus;

	private Collection<Integer> carrierPnrSegIds;

	private String totalCharges;

	private BigDecimal totalChargeAmount;

	
	private String status;
	
	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getCarrierOndGroupRPH() {
		return carrierOndGroupRPH;
	}

	public void setCarrierOndGroupRPH(String carrierOndGroupRPH) {
		this.carrierOndGroupRPH = carrierOndGroupRPH;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public Collection<Integer> getCarrierPnrSegIds() {
		return carrierPnrSegIds;
	}

	public void setCarrierPnrSegIds(Collection<Integer> carrierPnrSegIds) {
		this.carrierPnrSegIds = carrierPnrSegIds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}

	public BigDecimal getTotalChargeAmount() {
		return totalChargeAmount;
	}

	public void setTotalChargeAmount(BigDecimal totalChargeAmount) {
		this.totalChargeAmount = totalChargeAmount;
	}

}
