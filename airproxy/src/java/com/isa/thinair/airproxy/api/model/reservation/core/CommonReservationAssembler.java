/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * LCCClientReservationAssembler is the main assembling utility for the LCC Reservation Object
 * 
 * @author Nilindra Fernando
 */
public class CommonReservationAssembler implements Serializable {

	private static final long serialVersionUID = 5641986183365300865L;

	/* Holds the Reservation object */
	private LCCClientReservation lccreservation;

	/* Holds which system should the reservation be saved to */
	private SYSTEM targetSystem;

	/* Holds the Release Time Stamp which is unique for all passengers */
	private Date pnrZuluReleaseTimeStamp;

	/* Holds the Release Time Stamp which is unique for selected pgw */
	private Date pgwWiswZuluReleaseTimeStamp;

	/* Holds the Lcc transaction identifier */
	private String lccTransactionIdentifier;

	/* Holds whether the booking is onHold or not */
	private boolean onHoldBooking = false;

	private boolean waitListingBooking = false;

	/* Holds whether the booking is forceConfirm or not */
	private boolean forceConfirm = false;

	/* Holds whether the booking is split or not */
	private boolean splitBooking = false;

	/*
	 * Holds whether the reservation use other carrier pax credit for payment If yes the reservation need to be saved as
	 * a interline reservation
	 */
	private boolean useOtherCarrierPaxCredit;

	/* Agent code for the booking */
	private String agentCode;

	/* Holds the lcc payment reference number */
	private String lccPaymentRefNumber;

	/* Price Quote Request */
	private FlightPriceRQ flightPriceRQ;

	/* Original price quote information */
	private FareTypeTO selectedFare;

	/* Info to recreate the OndFareDTO .FIXME used common DTO to pass the Original price quote information */
	private FareSegChargeTO selectedFareSegChargeTO;

	private AppIndicatorEnum appIndicator;

	private Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap;
	
	private Map<String, VoucherPaymentInfo> voucherTemporyPaymentMap;

	private String selectedCurrency;

	private boolean isFlexiQuote;

	private int adultCount = 0;

	private int childCount = 0;

	private int infantCount = 0;

	/* Holds booking charges applicability level, make , modify or default */
	private int chargesApplicability = ChargeRateOperationType.ANY;

	private DiscountedFareDetails discountedFareDetails = null;

	private BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/* Holds booking type */
	private String bookingCategory = BookingCategory.STANDARD.getCode();

	private String originCountryOfCall;

	private String bookingType;

	private String itineraryFareMask = "N";

	private boolean skipDuplicationCheck;

	private List<FlightSegmentTO> flightSegmentTOs;

	private boolean allowFlightSearchAfterCutOffTime = false;

	private boolean actualPayment;

	private AgentCommissionDetails agentCommissions;

	private Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplateIDs;

	private BigDecimal serviceTaxRatio = AccelAeroCalculator.getDefaultBigDecimalZero();

	private long groupBookingRequestID;

	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentMap;

	private boolean allowClassifyUN = false;
	
	/*Holds the reason for allow blacklist passenger in a reservation*/
	private String  reasonForAllowBlPaxRes;

	private PayByVoucherInfo payByVoucherInfo;
	
	private boolean isCreateResNPayInitInDifferentFlows;

	public CommonReservationAssembler() {
		setLccreservation(new LCCClientReservation());
	}

	/**
	 * Add Reservation Information
	 * 
	 * @param userNote
	 * @param contactInfo
	 * @param object 
	 * @param userNoteType TODO
	 */
	public void addContactInfo(String userNote, CommonReservationContactInfo contactInfo, String userNoteType) {
		getLccreservation().setLastUserNote(userNote);
		getLccreservation().setUserNoteType(userNoteType);
		getLccreservation().setContactInfo(contactInfo);
		getLccreservation().setZuluBookingTimestamp(new Date());
	}

	/**
	 * Add Preference Information
	 * 
	 * @param CommonReservationPreferenceInfo
	 * @param contactInfo
	 */
	public void addPreferenceInfo(CommonReservationPreferrenceInfo preferenceInfo) {
		getLccreservation().setPreferrenceInfo(preferenceInfo);
	}

	/**
	 * Ammend the user notes
	 * 
	 * @param userNotes
	 */
	public void ammendUserNotes(String userNotes) {
		String newUserNote = BeanUtils.nullHandler(getLccreservation().getLastUserNote()) + " " + userNotes;
		getLccreservation().setLastUserNote(newUserNote);
	}

	/**
	 * Adds a outgoing segment
	 * 
	 * @param segmentSeq
	 * @param flightRefNo
	 * @param logicalCabinClass
	 * @param departureDateZulu
	 * @param arrivalDateZulu
	 * @param routeRefNum
	 * @param flightSegId
	 * @param ondGroupId
	 */
	public void addOutgoingSegment(int segmentSeq, String flightSegRefNumber, String flightRefNo, String flightNo,
			String segmentCode, String cabinClassCode, String logicalCabinClass, Date departureDate, Date arrivalDate,
			String subStationShortName, Date departureDateZulu, Date arrivalDateZulu, String baggageONDGroupId,
			boolean waitListing, String routeRefNum) {

		LCCClientReservationSegment pnrSeg = populateConfirmedReservationSegment(segmentSeq,
				ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE, flightSegRefNumber, flightRefNo, flightNo,
				segmentCode, cabinClassCode, logicalCabinClass, departureDate, arrivalDate, subStationShortName,
				departureDateZulu, arrivalDateZulu, baggageONDGroupId, waitListing, routeRefNum);

		getLccreservation().addSegment(pnrSeg);
	}

	/**
	 * Add a return segment
	 * 
	 * @param segmentSeq
	 * @param logicalCabinClass
	 * @param departureDateZulu
	 * @param arrivalDateZulu
	 * @param routeRefNum
	 * @param flightSegId
	 * @param ondGroupId
	 * @param openRtConfirmBefore
	 */
	public void addReturnSegment(int segmentSeq, String flightSegRefNumber, String flightRefNo, String flightNo,
			String segmentCode, String cabinClassCode, String logicalCabinClass, Date departureDate, Date arrivalDate,
			String subStationShortName, Date departureDateZulu, Date arrivalDateZulu, String baggageONDGroupId,
			boolean waitListing, String routeRefNum) {

		LCCClientReservationSegment pnrSeg = populateConfirmedReservationSegment(segmentSeq,
				ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE, flightSegRefNumber, flightRefNo, flightNo,
				segmentCode, cabinClassCode, logicalCabinClass, departureDate, arrivalDate, subStationShortName,
				departureDateZulu, arrivalDateZulu, baggageONDGroupId, waitListing, routeRefNum);

		getLccreservation().addSegment(pnrSeg);
	}

	private LCCClientReservationSegment populateConfirmedReservationSegment(int segmentSeq, String returnFlag,
			String flightSegRefNumber, String flightRefNo, String flightNo, String segmentCode, String cabinClassCode,
			String logicalCabinClass, Date departureDate, Date arrivalDate, String subStationShortName, Date departureDateZulu,
			Date arrivalDateZulu, String baggageONDGroupId, boolean waitListing, String routeRefNum) {
		LCCClientReservationSegment pnrSeg = new LCCClientReservationSegment();

		pnrSeg.setSegmentSeq(Integer.valueOf(segmentSeq));
		pnrSeg.setReturnFlag(returnFlag);
		pnrSeg.setFlightSegmentRefNumber(flightSegRefNumber);
		pnrSeg.setRouteRefNumber(routeRefNum);
		pnrSeg.setBookingFlightSegmentRefNumber(flightRefNo);
		pnrSeg.setFlightNo(flightNo);
		pnrSeg.setSegmentCode(segmentCode);
		pnrSeg.setCabinClassCode(cabinClassCode);
		pnrSeg.setLogicalCabinClass(logicalCabinClass);
		pnrSeg.setDepartureDate(departureDate);
		pnrSeg.setDepartureDateZulu(departureDateZulu);
		pnrSeg.setArrivalDate(arrivalDate);
		pnrSeg.setArrivalDateZulu(arrivalDateZulu);
		pnrSeg.setSubStationShortName(subStationShortName);
		pnrSeg.setBaggageONDGroupId(baggageONDGroupId);

		if (!waitListing) {
			// Making the segment status active purposely
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		} else {
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING);
		}
		return pnrSeg;
	}

	/**
	 * Adds an adult
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param paxCategory
	 *            TODO
	 * @param selctedAniclaryList
	 *            TODO
	 * @param selctedAniclaryList
	 *            TODO
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 * @param pnrPaxCatFOIDPlace
	 * @param pnrPaxCatEmpId
	 *            TODO
	 * @param pnrPaxCatDOJ
	 *            TODO
	 * @param pnrPaxCatIdCat
	 *            TODO
	 * @param ssrCode
	 * @param ssrRemarks
	 * @param iPayment
	 */
	public void addSingle(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, String travelerRef, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory,
			LCCClientPaymentAssembler lccPaymentAssembler, List<LCCSelectedSegmentAncillaryDTO> selctedAniclaryList,
			String firstNameOl, String lastNameOl, String titleOl, String translationLanguage, String arrivalIntlFltNo,
			Date intlFltArrivalDate, String departureIntlFltNo, Date intlfltDepartureDate, String pnrPaxGroupId) {

		LCCClientReservationPax passenger = populateLCCClientReservationPax(firstName, lastName, title, dateOfBirth,
				nationalityCode, paxSequence, null, travelerRef, paxAdditionalInfoDTO,
				ReservationInternalConstants.PassengerType.ADULT, paxCategory, firstNameOl, lastNameOl, titleOl,
				translationLanguage, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlfltDepartureDate,
				pnrPaxGroupId);

		passenger.setLccClientPaymentAssembler(lccPaymentAssembler);

		if (selctedAniclaryList != null && !selctedAniclaryList.isEmpty()) {
			passenger.setSelectedAncillaries(selctedAniclaryList);
		}

		// Add the adult
		getLccreservation().addPassenger(passenger);
	}

	/**
	 * Add a child
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param paxCategory
	 * @param selctedAniclaryList
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 * @param pnrPaxCatFOIDPlace
	 * @param pnrPaxCatEmpId
	 *            TODO
	 * @param pnrPaxCatDOJ
	 *            TODO
	 * @param pnrPaxCatIdCat
	 *            TODO
	 * @param ssrCode
	 * @param ssrRemarks
	 * @param iPayment
	 */
	public void addChild(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, String travelerRef, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory,
			LCCClientPaymentAssembler lccPaymentAssembler, List<LCCSelectedSegmentAncillaryDTO> selctedAniclaryList,
			String firstNameOl, String lastNameOl, String titleOl, String languageCode, String arrivalIntlFltNo,
			Date intlFltArrivalDate, String departureIntlFltNo, Date intlfltDepartureDate, String paxPnrGroupId) {

		LCCClientReservationPax passenger = populateLCCClientReservationPax(firstName, lastName, title, dateOfBirth,
				nationalityCode, paxSequence, null, travelerRef, paxAdditionalInfoDTO,
				ReservationInternalConstants.PassengerType.CHILD, paxCategory, firstNameOl, lastNameOl, titleOl, languageCode,
				arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlfltDepartureDate, paxPnrGroupId);

		passenger.setLccClientPaymentAssembler(lccPaymentAssembler);

		if (selctedAniclaryList != null && !selctedAniclaryList.isEmpty()) {
			passenger.setSelectedAncillaries(selctedAniclaryList);
		}

		// Add the child
		getLccreservation().addPassenger(passenger);
	}

	/**
	 * Adds an infant
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 *            TODO
	 * @param pnrPaxCatFOIDPlace
	 *            TODO
	 * @param pnrPaxCatEmpId
	 *            TODO
	 * @param pnrPaxCatDOJ
	 *            TODO
	 * @param pnrPaxCatIdCat
	 *            TODO
	 * @param paxCategory
	 *            TODO
	 * @param selctedAniclaryList
	 *            TODO
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param ssrCode
	 * @param ssrRemarks
	 */
	public void addInfant(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int parentSeqId, String travelerRef, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory,
			List<LCCSelectedSegmentAncillaryDTO> selctedAniclaryList, String firstNameOl, String lastNameOl, String languageCode,
			String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String paxPnrGroupId, LCCClientPaymentAssembler lccPaymentAssembler) {

		LCCClientReservationPax passenger = populateLCCClientReservationPax(firstName, lastName, title, dateOfBirth,
				nationalityCode, paxSequence, parentSeqId, travelerRef, paxAdditionalInfoDTO,
				ReservationInternalConstants.PassengerType.INFANT, paxCategory, firstNameOl, lastNameOl, null, languageCode,
				arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, paxPnrGroupId);

		if(lccPaymentAssembler != null && lccPaymentAssembler.getPayments() != null){
			for(LCCClientPaymentInfo lccClientPaymentInfo : lccPaymentAssembler.getPayments()){
				if(lccClientPaymentInfo instanceof LCCClientPaxCreditPaymentInfo){
					LCCClientPaxCreditPaymentInfo lccClientPaxCreditPaymentInfo = (LCCClientPaxCreditPaymentInfo) lccClientPaymentInfo;
					lccClientPaxCreditPaymentInfo.setPaxSequence(paxSequence);
				}
			}
			
		}
		passenger.setLccClientPaymentAssembler(lccPaymentAssembler);
		passenger.setAttachedPaxId(new Integer(parentSeqId));

		if (selctedAniclaryList != null && !selctedAniclaryList.isEmpty()) {
			passenger.setSelectedAncillaries(selctedAniclaryList);
		}

		// Add the infant
		getLccreservation().addPassenger(passenger);
	}

	/**
	 * Add a parent
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 * @param pnrPaxCatFOIDPlace
	 * @param pnrPaxCatEmpId
	 *            TODO
	 * @param pnrPaxCatDOJ
	 *            TODO
	 * @param pnrPaxCatIdCat
	 * @param paxCategory
	 *            TODO
	 * @param selctedAniclaryList
	 *            TODO
	 * @param iPayment
	 * @throws ModuleException
	 */
	public void addParent(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			Integer paxSequence, Integer parentSeqId, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory,
			LCCClientPaymentAssembler lccPaymentAssembler, List<LCCSelectedSegmentAncillaryDTO> selctedAniclaryList,
			String pnrPaxGroupId) throws ModuleException {

		LCCClientReservationPax passenger = populateLCCClientReservationPax(firstName, lastName, title, dateOfBirth,
				nationalityCode, paxSequence, parentSeqId, null, paxAdditionalInfoDTO,
				ReservationInternalConstants.PassengerType.ADULT, paxCategory, null, null, null, null, null, null, null, null,
				pnrPaxGroupId);

		passenger.setLccClientPaymentAssembler(lccPaymentAssembler);

		if (selctedAniclaryList != null && !selctedAniclaryList.isEmpty()) {
			passenger.setSelectedAncillaries(selctedAniclaryList);
		}

		// Add the Parent
		getLccreservation().addPassenger(passenger);
	}

	/**
	 * Add a parent
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param paxCategory
	 *            TODO
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 * @param pnrPaxcatFOIDPlace
	 * @param pnrPaxCatEmpId
	 *            TODO
	 * @param pnrPaxCatDOJ
	 *            TODO
	 * @param pnrPaxCatIdCat
	 *            TODO
	 * @param ssrCode
	 * @param ssrRemarks
	 * @param iPayment
	 */
	public void addParent(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			Integer paxSequence, Integer parentSeqId, String travelerRef, PaxAdditionalInfoDTO paxAdditionalInfoDTO,
			String paxCategory, LCCClientPaymentAssembler lccPaymentAssembler, String firstNameOl, String lastNameOl,
			String titleOl, String languageCode, String ArrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo,
			Date intlFltDepartureDate, String pnrPaxGroupId) {

		LCCClientReservationPax passenger = populateLCCClientReservationPax(firstName, lastName, title, dateOfBirth,
				nationalityCode, paxSequence, parentSeqId, travelerRef, paxAdditionalInfoDTO,
				ReservationInternalConstants.PassengerType.ADULT, paxCategory, firstNameOl, lastNameOl, titleOl, languageCode,
				ArrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);

		passenger.setLccClientPaymentAssembler(lccPaymentAssembler);

		// Add the Parent
		getLccreservation().addPassenger(passenger);
	}

	private LCCClientReservationPax populateLCCClientReservationPax(String firstName, String lastName, String title,
			Date dateOfBirth, Integer nationalityCode, Integer paxSequence, Integer attachedPax, String travelerRef,
			PaxAdditionalInfoDTO paxAdditionalInfoDTO, String passengerType, String paxCategory, String firstNameOl,
			String lastNameOl, String titleOl, String translationLanguage, String arrivalIntlFltNo, Date intlFltArrivalDate,
			String departureIntlFltNo, Date intlFltDepartureDate, String pnrPaxGroupId) {
		LCCClientReservationPax passenger = new LCCClientReservationPax();

		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		if (!"BABY".equals(title)) {
			passenger.setTitle(title);
		}
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(paxSequence);

		passenger.setPaxType(passengerType);
		passenger.setAttachedPaxId(attachedPax);

		if (firstNameOl != null && lastNameOl != null && translationLanguage != null) {
			LCCClientReservationPaxNamesTranslation paxNameTranslations = new LCCClientReservationPaxNamesTranslation();
			paxNameTranslations.setFirstNameOl(firstNameOl);
			paxNameTranslations.setLastNameOl(lastNameOl);
			paxNameTranslations.setLanguageCode(translationLanguage);
			if (titleOl != null) {
				paxNameTranslations.setTitleOl(titleOl);
			}
			passenger.setLccClientPaxNameTranslations(paxNameTranslations);
		}

		// passenger.setPnrPaxCatFOIDType(pnrPaxCatFOIDType);

		/*
		 * As a temporary solution passport #, expiry date & issued place stored in pnrPaxCatFOIDNumber as a comma
		 * separated value.
		 */

		/*
		 * String psptString = pnrPaxCatFOIDNumber + ReservationInternalConstants.PassengerConst.FOID_SEPARATOR +
		 * pnrPaxCatFOIDExpiry + ReservationInternalConstants.PassengerConst.FOID_SEPARATOR + pnrPaxCatFOIDPlace;
		 * 
		 * passenger.setPnrPaxCatFOIDNumber(psptString);
		 */

		LCCClientReservationAdditionalPax additionalPax = new LCCClientReservationAdditionalPax();
		additionalPax.setPassportNo(paxAdditionalInfoDTO.getPassportNo());
		additionalPax.setPassportExpiry(paxAdditionalInfoDTO.getPassportExpiry());
		additionalPax.setPassportIssuedCntry(paxAdditionalInfoDTO.getPassportIssuedCntry());
		additionalPax.setEmployeeId(paxAdditionalInfoDTO.getEmployeeId());
		additionalPax.setDateOfJoin(paxAdditionalInfoDTO.getDateOfJoin());
		additionalPax.setIdCategory(paxAdditionalInfoDTO.getIdCategory());
		additionalPax.setPlaceOfBirth(paxAdditionalInfoDTO.getPlaceOfBirth());
		additionalPax.setTravelDocumentType(paxAdditionalInfoDTO.getTravelDocumentType());
		additionalPax.setVisaDocNumber(paxAdditionalInfoDTO.getVisaDocNumber());
		additionalPax.setVisaDocPlaceOfIssue(paxAdditionalInfoDTO.getVisaDocPlaceOfIssue());
		additionalPax.setVisaDocIssueDate(paxAdditionalInfoDTO.getVisaDocIssueDate());
		additionalPax.setVisaApplicableCountry(paxAdditionalInfoDTO.getVisaApplicableCountry());
		additionalPax.setFfid(paxAdditionalInfoDTO.getFfid());
		additionalPax.setNationalIDNo(paxAdditionalInfoDTO.getNationalIDNo());
		additionalPax.setArrivalIntlFltNo(arrivalIntlFltNo);
		additionalPax.setIntlFltArrivalDate(intlFltArrivalDate);
		additionalPax.setDepartureIntlFltNo(departureIntlFltNo);
		additionalPax.setIntlFltDepartureDate(intlFltDepartureDate);
		additionalPax.setPnrPaxGroupId(pnrPaxGroupId);

		passenger.setLccClientAdditionPax(additionalPax);

		passenger.setPaxCategory(paxCategory);

		passenger.setZuluStartTimeStamp(new Date());

		passenger.setTravelerRefNumber(travelerRef);

		return passenger;
	}

	/**
	 * @return Returns the pnrZuluReleaseTimeStamp.
	 */
	public Date getPnrZuluReleaseTimeStamp() {
		return pnrZuluReleaseTimeStamp;
	}

	/**
	 * @param pnrZuluReleaseTimeStamp
	 *            The pnrZuluReleaseTimeStamp to set.
	 */
	public void setPnrZuluReleaseTimeStamp(Date pnrZuluReleaseTimeStamp) {
		this.pnrZuluReleaseTimeStamp = pnrZuluReleaseTimeStamp;
	}

	/**
	 * @return Returns the pgwWiswZuluReleaseTimeStamp.
	 */
	public Date getPgwWiswZuluReleaseTimeStamp() {
		return pgwWiswZuluReleaseTimeStamp;
	}

	/**
	 * @param pgwWiswZuluReleaseTimeStamp
	 *            The pgwWiswZuluReleaseTimeStamp to set.
	 */
	public void setPgwWiswZuluReleaseTimeStamp(Date pgwWiswZuluReleaseTimeStamp) {
		this.pgwWiswZuluReleaseTimeStamp = pgwWiswZuluReleaseTimeStamp;
	}

	/**
	 * @return the lccreservation
	 */
	public LCCClientReservation getLccreservation() {
		return lccreservation;
	}

	/**
	 * @param lccreservation
	 *            the lccreservation to set
	 */
	public void setLccreservation(LCCClientReservation lccreservation) {
		this.lccreservation = lccreservation;
	}

	/**
	 * @return the lccTransactionIdentifier
	 */
	public String getLccTransactionIdentifier() {
		return lccTransactionIdentifier;
	}

	/**
	 * @param lccTransactionIdentifier
	 *            the lccTransactionIdentifier to set
	 */
	public void setLccTransactionIdentifier(String lccTransactionIdentifier) {
		this.lccTransactionIdentifier = lccTransactionIdentifier;
	}

	/**
	 * @return the onHoldBooking
	 */
	public boolean isOnHoldBooking() {
		return onHoldBooking;
	}

	/**
	 * @param onHoldBooking
	 *            the onHoldBooking to set
	 */
	public void setOnHoldBooking(boolean onHoldBooking) {
		this.onHoldBooking = onHoldBooking;
	}

	/**
	 * @return the targetSystem
	 */
	public SYSTEM getTargetSystem() {
		return targetSystem;
	}

	/**
	 * @param targetSystem
	 *            the targetSystem to set
	 */
	public void setTargetSystem(SYSTEM targetSystem) {
		this.targetSystem = targetSystem;
	}

	/**
	 * @return the flightPriceRQ
	 */
	public FlightPriceRQ getFlightPriceRQ() {
		return flightPriceRQ;
	}

	/**
	 * @param flightPriceRQ
	 *            the flightPriceRQ to set
	 */
	public void setFlightPriceRQ(FlightPriceRQ flightPriceRQ) {
		this.flightPriceRQ = flightPriceRQ;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the originalPriceQuote
	 */
	public FareTypeTO getSelectedFare() {
		return selectedFare;
	}

	/**
	 * @param fareTypeTO
	 *            the originalPriceQuote to set
	 */
	public void setSelectedFare(FareTypeTO fareTypeTO) {
		this.selectedFare = fareTypeTO;
	}

	/**
	 * @return the selectedFareSegChargeTO
	 */
	public FareSegChargeTO getSelectedFareSegChargeTO() {
		return selectedFareSegChargeTO;
	}

	/**
	 * @param selectedFareSegChargeTO
	 *            the selectedFareSegChargeTO to set
	 */
	public void setSelectedFareSegChargeTO(FareSegChargeTO selectedFareSegChargeTO) {
		this.selectedFareSegChargeTO = selectedFareSegChargeTO;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public void setTemporaryPaymentMap(Map<Integer, CommonCreditCardPaymentInfo> temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public Map<Integer, CommonCreditCardPaymentInfo> getTemporaryPaymentMap() {
		return temporyPaymentMap;
	}

	public boolean isForceConfirm() {
		return forceConfirm;
	}

	public void setForceConfirm(boolean forceConfirm) {
		this.forceConfirm = forceConfirm;
	}

	public String getSelectedCurrency() {
		return this.selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	/**
	 * @return the isFlexiQuote
	 */
	@Deprecated
	public boolean isFlexiQuote() {
		return isFlexiQuote;
	}

	/**
	 * @param isFlexiQuote
	 *            the isFlexiQuote to set
	 */
	@Deprecated
	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	/**
	 * 
	 * @return whether the booking is split or not
	 */
	public boolean isSplitBooking() {
		return splitBooking;
	}

	/**
	 * 
	 * @param splitBooking
	 *            the split Booking to set
	 */
	public void setSplitBooking(boolean splitBooking) {
		this.splitBooking = splitBooking;
	}

	/**
	 * @return the adultCount
	 */
	public int getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount
	 *            the adultCount to set
	 */
	public void addAdultCount() {
		this.adultCount++;
	}

	/**
	 * @return the childCount
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount
	 *            the childCount to set
	 */
	public void addChildCount() {
		this.childCount++;
	}

	/**
	 * @return the infantCount
	 */
	public int getInfantCount() {
		return infantCount;
	}

	/**
	 * @param infantCount
	 *            the infantCount to set
	 */
	public void addInfantCount() {
		this.infantCount++;
	}

	/**
	 * @return the useOtherCarrierPaxCredit
	 */
	public boolean isUseOtherCarrierPaxCredit() {
		return useOtherCarrierPaxCredit;
	}

	/**
	 * @param useOtherCarrierPaxCredit
	 *            the useOtherCarrierPaxCredit to set
	 */
	public void setUseOtherCarrierPaxCredit(boolean useOtherCarrierPaxCredit) {
		this.useOtherCarrierPaxCredit = useOtherCarrierPaxCredit;
	}

	/**
	 * @return
	 */
	public int getChargesApplicability() {
		return chargesApplicability;
	}

	/**
	 * @param chargesApplicability
	 */
	public void setChargesApplicability(int chargesApplicability) {
		this.chargesApplicability = chargesApplicability;
	}

	public DiscountedFareDetails getDiscountedFareDetails() {
		return discountedFareDetails;
	}

	public void setDiscountedFareDetails(DiscountedFareDetails discountedFareDetails) {
		this.discountedFareDetails = discountedFareDetails;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getBookingCategory() {
		return bookingCategory;
	}

	public void setBookingCategory(String bookingCategory) {
		this.bookingCategory = bookingCategory;
	}

	public String getItineraryFareMask() {
		return itineraryFareMask;
	}

	public void setItineraryFareMask(String itineraryFareMask) {
		this.itineraryFareMask = itineraryFareMask;
	}

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public void setSkipDuplicateCheck(boolean skipDuplicationCheck) {
		this.skipDuplicationCheck = skipDuplicationCheck;
	}

	public boolean isSkipDuplicationCheck() {
		return skipDuplicationCheck;
	}

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public boolean isAllowFlightSearchAfterCutOffTime() {
		return allowFlightSearchAfterCutOffTime;
	}

	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime) {
		this.allowFlightSearchAfterCutOffTime = allowFlightSearchAfterCutOffTime;
	}

	public boolean isWaitListingBooking() {
		return waitListingBooking;
	}

	public void setWaitListingBooking(boolean waitListingBooking) {
		this.waitListingBooking = waitListingBooking;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public AgentCommissionDetails getAgentCommissions() {
		return agentCommissions;
	}

	public void setAgentCommissions(AgentCommissionDetails agentCommissions) {
		this.agentCommissions = agentCommissions;
	}

	public Map<String, Map<EXTERNAL_CHARGES, Integer>> getAnciOfferTemplateIDs() {
		if (anciOfferTemplateIDs == null) {
			anciOfferTemplateIDs = new HashMap<String, Map<EXTERNAL_CHARGES, Integer>>();
		}
		return anciOfferTemplateIDs;
	}

	public void setAnciOfferTemplateIDs(Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplateIDs) {
		this.anciOfferTemplateIDs = anciOfferTemplateIDs;
	}

	public BigDecimal getServiceTaxRatio() {
		return serviceTaxRatio;
	}

	public void setServiceTaxRatio(BigDecimal serviceTaxRatio) {
		this.serviceTaxRatio = serviceTaxRatio;
	}

	public long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentMap() {
		return carrierWiseLoyaltyPaymentMap;
	}

	public void setCarrierWiseLoyaltyPaymentMap(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentMap) {
		this.carrierWiseLoyaltyPaymentMap = carrierWiseLoyaltyPaymentMap;
	}

	public LoyaltyPaymentInfo getCarrierLoyaltyPaymentInfo(String carrierCode) {
		if (carrierWiseLoyaltyPaymentMap != null && carrierWiseLoyaltyPaymentMap.containsKey(carrierCode)) {
			return carrierWiseLoyaltyPaymentMap.get(carrierCode);
		}

		return null;
	}

	public boolean isAllowClassifyUN() {
		return allowClassifyUN;
	}

	public void setAllowClassifyUN(boolean allowClassifyUN) {
		this.allowClassifyUN = allowClassifyUN;
	}

	public String getReasonForAllowBlPaxRes() {
		return reasonForAllowBlPaxRes;
	}

	public void setReasonForAllowBlPaxRes(String reasonForAllowBlPaxRes) {
		this.reasonForAllowBlPaxRes = reasonForAllowBlPaxRes;
	}

	/**
	 * @return the payByVoucherInfo
	 */
	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	/**
	 * @param payByVoucherInfo
	 *            the payByVoucherInfo to set
	 */
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	public boolean isCreateResNPayInitInDifferentFlows() {
		return isCreateResNPayInitInDifferentFlows;
	}

	public void setCreateResNPayInitInDifferentFlows(boolean isCreateResNPayInitInDifferentFlows) {
		this.isCreateResNPayInitInDifferentFlows = isCreateResNPayInitInDifferentFlows;
	}

	public String getOriginCountryOfCall() {
		return originCountryOfCall;
	}

	public void setOriginCountryOfCall(String originCountryOfCall) {
		this.originCountryOfCall = originCountryOfCall;
	}
	public Map<String, VoucherPaymentInfo> getVoucherTemporyPaymentMap() {
		return voucherTemporyPaymentMap;
	}

	public void setVoucherTemporyPaymentMap(Map<String, VoucherPaymentInfo> voucherTemporyPaymentMap) {
		this.voucherTemporyPaymentMap = voucherTemporyPaymentMap;
	}
}
