package com.isa.thinair.airproxy.api.utils.assembler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;

/**
 * 
 * Prepare Meta info required to processing of AgentCommissionBL (Only common logics should go here)
 * 
 */
public class PreferenceAnalyzer implements Serializable {

	private static final long serialVersionUID = 1L;

	private BaseAvailRQ baseAvailRQ = null;

	private Collection<FlightSegmentDTO> existingSegmentsList = null;

	/** Modification Related **/
	private String modifiedOndCode = null;
	private Integer oldFareId = null; // modifying segs
	private List<String> modifiedSegCodes = new ArrayList<String>();
	private Collection<FlightSegmentDTO> modifiedSegmentsList = null;

	List<String> processingOndCodes = null;

	private MODE operation = null;

	public static enum MODE {
		CREATE_FRESH, CANCEL_RES, CANCEL_OND, MODIFY_OND, ADD_OND, ADD_INF, REMOVE_PAX, VOID_RES
	};

	public PreferenceAnalyzer(BaseAvailRQ baseAvailRQ) {
		this.baseAvailRQ = baseAvailRQ;
	}

	public void anlyze() {

		if (baseAvailRQ != null) {
			AvailPreferencesTO availPreferences = baseAvailRQ.getAvailPreferences();
			SYSTEM searchSystem = availPreferences.getSearchSystem();
			ApplicationEngine appCode = availPreferences.getAppIndicator();

			if (appCode.equals(ApplicationEngine.XBE) && searchSystem.equals(SYSTEM.AA)) {
				boolean isModifyBooking = availPreferences.isModifyBooking();
				boolean addSegment = availPreferences.isAddSegment();

				if (isModifyBooking) {
					existingSegmentsList = availPreferences.getExistingSegmentsList();
					modifiedSegmentsList = availPreferences.getModifiedSegmentsList();
					modifiedOndCode = getModifyingOndCode((List<FlightSegmentDTO>) modifiedSegmentsList);
				} else {
					this.operation = MODE.CREATE_FRESH;
				}

				if (isModifyBooking && !addSegment) {
					oldFareId = availPreferences.getOldFareID();
					this.operation = MODE.MODIFY_OND;
				} else if (isModifyBooking && addSegment) {
					this.operation = MODE.ADD_OND;
				}
			}
		}
	}

	private String getModifyingOndCode(List<FlightSegmentDTO> modifiedSegmentsList) {
		String ondCode = null;
		if (modifiedSegmentsList != null && !modifiedSegmentsList.isEmpty()) {
			Collections.sort(modifiedSegmentsList);
			Iterator<FlightSegmentDTO> it = modifiedSegmentsList.iterator();
			while (it.hasNext()) {
				FlightSegmentDTO temp = it.next();
				modifiedSegCodes.add(temp.getSegmentCode());
			}
			ondCode = SegmentUtil.getOndCode(modifiedSegCodes);

		}
		return ondCode;
	}

	public MODE getOperation() {
		return operation;
	}

	public Integer getOldFareId() {
		return oldFareId;
	}

	public void setOldFareId(Integer oldFareId) {
		this.oldFareId = oldFareId;
	}

	public Collection<FlightSegmentDTO> getModifiedSegmentsList() {
		return modifiedSegmentsList;
	}

	public String getModifiedOndCode() {
		return modifiedOndCode;
	}

	public List<String> getModifiedSegCodes() {
		return modifiedSegCodes;
	}

}
