package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class FareDiscountTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer fareDiscountMin;
	private Integer fareDiscountMax;
	public Integer getFareDiscountMin() {
		return fareDiscountMin;
	}
	public void setFareDiscountMin(Integer fareDiscountMin) {
		this.fareDiscountMin = fareDiscountMin;
	}
	public Integer getFareDiscountMax() {
		return fareDiscountMax;
	}
	public void setFareDiscountMax(Integer fareDiscountMax) {
		this.fareDiscountMax = fareDiscountMax;
	}
	
	public boolean isFareDiscountAvailable(){
		return (fareDiscountMin != null && fareDiscountMax != null);
	}
	

}
