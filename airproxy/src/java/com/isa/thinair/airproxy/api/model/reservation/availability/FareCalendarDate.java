package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.Date;

public class FareCalendarDate implements Serializable, Comparable<FareCalendarDate> {

	public enum FareCalendarDateStatus {
		AVL, NOTAVL
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date date;
	private FareCalendarDateStatus status;
	private FareAvailDetails fareDetails;

	public FareCalendarDate(Date date, FareCalendarDateStatus status) {
		this.date = date;
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public FareCalendarDateStatus getStatus() {
		return status;
	}

	public FareAvailDetails getFareDetails() {
		return fareDetails;
	}

	public void setFareDetails(FareAvailDetails fareDetails) {
		this.fareDetails = fareDetails;
	}

	@Override
	public int compareTo(FareCalendarDate fareCalDate) {
		return this.getDate().compareTo(fareCalDate.getDate());
	}

}
