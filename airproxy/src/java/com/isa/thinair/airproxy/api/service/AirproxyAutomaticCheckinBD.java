package com.isa.thinair.airproxy.api.service;

import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinRequest;
import com.isa.thinair.airproxy.api.model.automaticCheckin.AutomaticCheckinResponse;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AirproxyAutomaticCheckinBD {
	public static final String SERVICE_NAME = "AirproxyAutomaticCheckinService";

	/**
	 * @param automaticCheckinRequest
	 * @return automaticCheckinResponse
	 * @throws ModuleException
	 * @throws Exception
	 */
	public AutomaticCheckinResponse getAutomaticCheckinResponse(AutomaticCheckinRequest automaticCheckinRequest)
			throws ModuleException;

}
