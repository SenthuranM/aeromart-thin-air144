package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;

/**
 * The class representing the charge adjustment privileges for a particular charge adjustment type.
 * @author sanjaya
 *
 */
public class ChargeAdjustmentPrivilege implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/** The charge adjustment type id the privileges are associated with */
	private Integer chargeAdjustmentTypeId;
	
	/** The flag representing whether refundable charge adjustments are allowed */
	private Boolean refundable;
	
	/** The flag representing whether non - refundable charge adjustments are allowed */
	private Boolean nonRefundable;

	/**
	 * @return The charge adjustment type id.
	 */
	public Integer getChargeAdjustmentTypeId() {
		return chargeAdjustmentTypeId;
	}

	/**
	 * @param chargeAdjustmentTypeId : The charge adjsutment type id to set.
	 */
	public void setChargeAdjustmentTypeId(Integer chargeAdjustmentTypeId) {
		this.chargeAdjustmentTypeId = chargeAdjustmentTypeId;
	}

	/**
	 * @return : the refundable flag. true if refundable options are allowed. false otherwise.
	 */
	public Boolean getRefundable() {
		return refundable;
	}

	/**
	 * @param refundable : The refundable flag to set.
	 */
	public void setRefundable(Boolean refundable) {
		this.refundable = refundable;
	}

	/**
	 * @return : the refundable flag. true if non - refundable options are allowed. false otherwise.
	 */
	public Boolean getNonRefundable() {
		return nonRefundable;
	}

	/**
	 * @param nonRefundable : The non refundable flag to set.
	 */
	public void setNonRefundable(Boolean nonRefundable) {
		this.nonRefundable = nonRefundable;
	}
}