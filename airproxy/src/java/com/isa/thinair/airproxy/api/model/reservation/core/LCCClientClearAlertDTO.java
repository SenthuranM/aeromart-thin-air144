package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Map;

/**
 * The Class LCCClientClearAlertDTO.
 */
public class LCCClientClearAlertDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Holds pnrSegId and carrier code. */
	Map<Integer, String> pnrSegIds;

	/** Holds action taken for the alert. */
	String actionTaken;

	/** Holds text provided. */
	String actionText;

	/** The alert content. */
	String alertContent;

	/** Holds PNR. */
	private String PNR;

	/** Holds groupPNR. */
	private String groupPNR;

	/**
	 * Gets the pnrSegIds.
	 * 
	 * @return the pnrSegIds
	 */
	public Map<Integer, String> getPnrSegIds() {
		return pnrSegIds;
	}

	/**
	 * Sets the pnrSegIds.
	 * 
	 * @param pnrSegIds
	 *            the pnrSegIds
	 */
	public void setPnrSegIds(Map<Integer, String> pnrSegIds) {
		this.pnrSegIds = pnrSegIds;
	}

	/**
	 * Gets the action taken.
	 * 
	 * @return the action taken
	 */
	public String getActionTaken() {
		return actionTaken;
	}

	/**
	 * Sets the action taken.
	 * 
	 * @param actionTaken
	 *            the new action taken
	 */
	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	/**
	 * Gets the action text.
	 * 
	 * @return the action text
	 */
	public String getActionText() {
		return actionText;
	}

	/**
	 * Sets the action text.
	 * 
	 * @param actionText
	 *            the new action text
	 */
	public void setActionText(String actionText) {
		this.actionText = actionText;
	}

	/**
	 * Gets the PNR.
	 * 
	 * @return the PNR
	 */

	public String getPNR() {
		return PNR;
	}

	/**
	 * Sets the pNR.
	 * 
	 * @param pNR
	 *            the new PNR
	 */
	public void setPNR(String pNR) {
		PNR = pNR;
	}

	/**
	 * Gets the alert content.
	 * 
	 * @return the alert content
	 */
	public String getAlertContent() {
		return alertContent;
	}

	/**
	 * Sets the alert content.
	 * 
	 * @param alertContent
	 *            the new alert content
	 */
	public void setAlertContent(String alertContent) {
		this.alertContent = alertContent;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

}
