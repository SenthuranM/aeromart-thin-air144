package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

public class PassengerTypeQuantityTO implements Serializable {

	private final static long serialVersionUID = 1L;
	protected String passengerType;
	protected int quantity;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int value) {
		this.quantity = value;
	}

	@Override
	public String toString() {
		StringBuilder toStringBuilder = new StringBuilder("PassengerTypeQuantityTO [");
		toStringBuilder.append("passengerType=").append(passengerType);
		toStringBuilder.append("quantity=").append(quantity);
		toStringBuilder.append("]");
		return toStringBuilder.toString();
	}

	/**
	 * @return the passengerType
	 */
	public String getPassengerType() {
		return passengerType;
	}

	/**
	 * @param passengerType
	 *            the passengerType to set
	 */
	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

}
