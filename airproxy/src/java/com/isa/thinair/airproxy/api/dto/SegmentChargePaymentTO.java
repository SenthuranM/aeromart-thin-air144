package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The payment charge breakdown transfer object.
 * 
 * @author sanjaya
 */
public class SegmentChargePaymentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The payment amount of the charge */
	private BigDecimal amount;

	/** The charge group code of the associated charge */
	private String chargeGroupCode;

	/** Nominal Code of the associated payment */
	private Integer nominalCode;

	/**
	 * @return : The payment amount of the charge
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            : The payment amount of the charge to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return : The charge group code of the associated charge
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            : The charge group code of the associated charge to set
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return : Nominal Code of the associated payment.
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            : Nominal Code of the associated payment to set.
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	@Override
	public String toString() {
		return "SegmentChargePaymentTO [amount=" + amount + ", chargeGroupCode=" + chargeGroupCode + ", nominalCode="
				+ nominalCode + "]";
	}
}
