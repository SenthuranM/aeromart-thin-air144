package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

public class ExternalChargeTO extends SurchargeTO {

	private static final long serialVersionUID = 1L;

	// This is set as FlexiInfoTO collection. We can make this generic by extending from a super class
	private Collection<FlexiInfoTO> additionalDetails = new ArrayList<FlexiInfoTO>();

	private boolean isInbound = false;

	private String comments;

	private EXTERNAL_CHARGES type;

	public Collection<FlexiInfoTO> getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(Collection<FlexiInfoTO> additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public void addAdditionalDetail(FlexiInfoTO additionalDetail) {
		this.getAdditionalDetails().add(additionalDetail);
	}

	@Deprecated
	public boolean isInbound() {
		return isInbound;
	}

	@Deprecated
	public void setInbound(boolean isInbound) {
		this.isInbound = isInbound;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public ExternalChargeTO clone() {
		ExternalChargeTO clone = new ExternalChargeTO();
		clone.setCarrierCode(this.carrierCode);
		clone.setSegmentCode(this.getSegmentCode());
		clone.setSurchargeCode(this.getSurchargeCode());
		clone.setAmount(this.getAmount());
		clone.getApplicablePassengerTypeCode().addAll(this.getApplicablePassengerTypeCode());
		clone.setChagrgeGroupCode(this.getChagrgeGroupCode());
		clone.setChargeRateId(this.getChargeRateId());
		clone.setInbound(this.isInbound());
		clone.setComments(this.getComments());
		Collection<FlexiInfoTO> additionalDetailsClone = new ArrayList<FlexiInfoTO>();
		for (FlexiInfoTO flexiInfoTO : this.getAdditionalDetails()) {
			additionalDetailsClone.add(flexiInfoTO.clone());
		}
		clone.setAdditionalDetails(additionalDetailsClone);
		clone.setType(this.getType());
		clone.setOndSequence(this.getOndSequence());
		return clone;
	}

	public EXTERNAL_CHARGES getType() {
		return type;
	}

	public void setType(EXTERNAL_CHARGES type) {
		this.type = type;
	}

}
