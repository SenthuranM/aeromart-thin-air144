package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FareTO extends BasicChargeTO implements Serializable, Comparable<FareTO> {

	private static final long serialVersionUID = 1L;

	private String description;
	private Date applicableDate;
	private String segmentCode;
	private String carrierCode;
	private String fareCategoryCode;
	private String fareCategoryDescription;
	private String fareRuleComments;
	private boolean showComments;
	private Integer appliedFlightSegId;
	private String logicalCCCode;

	private int fareId;

	private int fareRuleID;

	private int pnrPaxFareId;

	private List<String> pnrSegIds;

	private String fareBasisCode;

	private String bookingClassCode;

	private boolean isModifyToSameFare;

	private String fareType;

	private int returnGroupId;
	
	private boolean bulkTicketFareRule;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Date getApplicableDate() {
		return applicableDate;
	}

	public void setApplicableDate(Date applicableDate) {
		this.applicableDate = applicableDate;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(int fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	public String getFareCategoryDescription() {
		return fareCategoryDescription;
	}

	public void setFareCategoryDescription(String fareCategoryDescription) {
		this.fareCategoryDescription = fareCategoryDescription;
	}

	public String getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(String fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}

	public boolean isShowComments() {
		return showComments;
	}

	public void setShowComments(boolean showComments) {
		this.showComments = showComments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FareTO [amount=");
		builder.append(amount);
		builder.append(", applicableDate=");
		builder.append(applicableDate);
		builder.append(", carrierCode=");
		builder.append(carrierCode);
		builder.append(", description=");
		builder.append(description);
		builder.append(", segmentCode=");
		builder.append(segmentCode);
		builder.append(", localCurrencyCode=");
		builder.append(localCurrencyCode);
		builder.append(", localCurrencyAmount=");
		builder.append(localCurrencyAmount);
		builder.append(", logicalCCCode=");
		builder.append(logicalCCCode);
		builder.append("]");
		return builder.toString();
	}

	public int getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(int pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public List<String> getPnrSegIds() {
		return pnrSegIds;
	}

	public void setPnrSegIds(List<String> pnrSegIds) {
		this.pnrSegIds = pnrSegIds;
	}

	/**
	 * @return the fareBasisCode
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            the fareBasisCode to set
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return the bookingClassCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            the bookingClassCode to set
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	@Override
	public int compareTo(FareTO o) {
		return o.amount.compareTo(this.amount);
	}

	/**
	 * @return the isModifyToSameFare
	 */
	public boolean isModifyToSameFare() {
		return isModifyToSameFare;
	}

	/**
	 * @param isModifyToSameFare
	 *            the isModifyToSameFare to set
	 */
	public void setModifyToSameFare(boolean isModifyToSameFare) {
		this.isModifyToSameFare = isModifyToSameFare;
	}

	public String getFareType() {
		return fareType;
	}

	public void setFareType(String fareType) {
		this.fareType = fareType;
	}

	public int getReturnGroupId() {
		return returnGroupId;
	}

	public void setReturnGroupId(int returnGroupId) {
		this.returnGroupId = returnGroupId;
	}

	public Integer getAppliedFlightSegId() {
		return appliedFlightSegId;
	}

	public void setAppliedFlightSegId(Integer appliedFlightSegId) {
		this.appliedFlightSegId = appliedFlightSegId;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public boolean isBulkTicketFareRule() {
		return bulkTicketFareRule;
	}

	public void setBulkTicketFareRule(boolean bulkTicketFareRule) {
		this.bulkTicketFareRule = bulkTicketFareRule;
	}

}
