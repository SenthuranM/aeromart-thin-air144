package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;

import com.isa.thinair.airinventory.api.model.BookingClass;

public class TravelPreferencesTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String bookingType = BookingClass.BookingClassType.NORMAL;

	private boolean openReturn = false;

	private Integer validityId;

	private String bookingClassCode;

	/** shall we move this to avail pref? **/
	private boolean openReturnConfirm;

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		if (bookingType != null && !("").equals(bookingType.trim())) {
			this.bookingType = bookingType;
		}
	}

	/**
	 * @return the openReturn
	 */
	public boolean isOpenReturn() {
		return openReturn;
	}

	/**
	 * @param openReturn
	 *            the openReturn to set
	 */
	public void setOpenReturn(boolean openReturn) {
		this.openReturn = openReturn;
	}

	/**
	 * @return the validityId
	 */
	public Integer getValidityId() {
		return validityId;
	}

	/**
	 * @param validityId
	 *            the validity to set
	 */
	public void setValidityId(Integer validityId) {
		this.validityId = validityId;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public boolean isOpenReturnConfirm() {
		return openReturnConfirm;
	}

	public void setOpenReturnConfirm(boolean openReturnConfirm) {
		this.openReturnConfirm = openReturnConfirm;
	}

}
