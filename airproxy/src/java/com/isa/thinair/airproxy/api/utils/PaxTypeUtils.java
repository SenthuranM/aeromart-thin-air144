package com.isa.thinair.airproxy.api.utils;

import java.util.regex.Pattern;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PaxTypeUtils {
	private static final String $ = "$";
	private final static String SPLIT_DELIM = "\\$";
	private static final String CARRIER_SEPERATOR = "|";
	private static final String TRAVELLER_SEPERATOR = ",";
	private static final String VERSION_SEPERATOR = "-";

	/**
	 * Returns encoded traveler reference information.
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static String travelerReference(ReservationPax reservationPax) {
		// TODO Seems this is common for LCCClient / AAServices etc. Possibly let's refactor.
		String travelerReference = "";
		if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
			travelerReference = "A" + reservationPax.getPaxSequence() + $ + reservationPax.getPnrPaxId();
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
			travelerReference = "C" + reservationPax.getPaxSequence() + $ + reservationPax.getPnrPaxId();
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())) {
			Integer parentSeq = 0;
			if (reservationPax.getParent() != null) {// there is a possibility of infant alone;
				parentSeq = reservationPax.getParent().getPaxSequence();
			}
			travelerReference = "I" + reservationPax.getPaxSequence() + "/A" + parentSeq + $ + reservationPax.getPnrPaxId();
		}
		return travelerReference;
	}

	/**
	 * 
	 * @param travelerRefNumber
	 * @return
	 */
	public static Integer getPaxSeq(String travelerRefNumber) {
		StringBuilder sequence = new StringBuilder();
		String numbers = "0123456789";

		int indexOf$ = travelerRefNumber.indexOf($.charAt(0));

		for (int i = indexOf$ - 1; i >= 0; i--) {
			char charAt = travelerRefNumber.charAt(i);
			if (numbers.indexOf(charAt) == -1) {
				if (i > 2) {
					if (travelerRefNumber.charAt(i - 1) == '/') {
						sequence = new StringBuilder();
						i--;
						continue;
					}
				}
				break;
			} else {
				sequence.append(charAt);
			}
		}

		return Integer.valueOf(sequence.reverse().toString());
	}

	public static Integer getPnrPaxId(String travelerRefNumber) {
		int pnrPaxIdStartIndex = travelerRefNumber.lastIndexOf(PaxTypeUtils.$);
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = travelerRefNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

	public static Integer getPnrPaxIdFromLccRef(String travelerRefNumber) {
		Integer intVal = null;
		String carrierTravelerRef = null;
		String[] travelRefNumbers = travelerRefNumber.split(TRAVELLER_SEPERATOR);
		for (String carrerRefProtion : travelRefNumbers) {
			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			if (carrerRefProtion.indexOf(defaultCarrier, 0) >= 0) {
				int indexOfAirlineCodeSeparator = carrerRefProtion.lastIndexOf(CARRIER_SEPERATOR);
				carrierTravelerRef = carrerRefProtion.substring(indexOfAirlineCodeSeparator + 1);
				carrierTravelerRef = carrierTravelerRef.trim();
				String arr[] = carrierTravelerRef.split(SPLIT_DELIM);
				intVal = new Integer(arr[1]);
				break;
			}
		}
		return intVal;
	}

	public static String getTravellerRefFromLccRef(String travellerRefNo) {
		return travellerRefNo.split(Pattern.quote(CARRIER_SEPERATOR))[1];
	}
}
