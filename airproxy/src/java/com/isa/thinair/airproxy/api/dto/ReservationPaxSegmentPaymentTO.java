package com.isa.thinair.airproxy.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The transfer object to hold segment wise reservation passenger payment breakdown.
 * 
 * @author sanjaya
 * 
 */
public class ReservationPaxSegmentPaymentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The segment code for which the break down is applicable */
	private String segmentCode;

	/** The carrier code for which the break down is applicable */
	private String carrierCode;

	/** The departure date of the segment */
	private Date departureDateZulu;

	/** The associated transaction ID to which the break down is applicable */
	private String transactionId;

	/** The pax sequence of the passenger */
	private Integer paxSequence;

	/** The collection of segment wise charges for the payment. */
	private Collection<SegmentChargePaymentTO> segmentChargePaymentCollection = new ArrayList<SegmentChargePaymentTO>();

	/**
	 * @return : The segment code for which the break down is applicable.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            : segment code for which the break down is applicable to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return : carrier code for which the break down is applicable
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            : carrier code for which the break down is applicable to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return : The departure date of the segment.
	 */
	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	/**
	 * @param departureDateZulu
	 *            : The departure date of the segment to set.
	 */
	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	/**
	 * @return : The collection of segment payment breakdown objects.
	 */
	public Collection<SegmentChargePaymentTO> getSegmentChargePaymentCollection() {
		return segmentChargePaymentCollection;
	}

	/**
	 * @return : The associated transaction ID to which the break down is applicable
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            : The associated transaction ID to which the break down is applicable to set.
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return : The pax sequence of the passenger
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            : The pax sequence of the passenger to set.
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}
	
	

	@Override
	public String toString() {
		return "ReservationPaxSegmentPaymentTO [segmentCode=" + segmentCode + ", carrierCode=" + carrierCode
				+ ", departureDateZulu=" + departureDateZulu + ", transactionId=" + transactionId + ", paxSequence="
				+ paxSequence + ", segmentChargePaymentCollection=" + segmentChargePaymentCollection + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carrierCode == null) ? 0 : carrierCode.hashCode());
		result = prime * result + ((departureDateZulu == null) ? 0 : departureDateZulu.hashCode());
		result = prime * result + ((paxSequence == null) ? 0 : paxSequence.hashCode());
		result = prime * result + ((segmentCode == null) ? 0 : segmentCode.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReservationPaxSegmentPaymentTO other = (ReservationPaxSegmentPaymentTO) obj;
		if (carrierCode == null) {
			if (other.carrierCode != null)
				return false;
		} else if (!carrierCode.equals(other.carrierCode))
			return false;
		if (departureDateZulu == null) {
			if (other.departureDateZulu != null)
				return false;
		} else if (!departureDateZulu.equals(other.departureDateZulu))
			return false;
		if (paxSequence == null) {
			if (other.paxSequence != null)
				return false;
		} else if (!paxSequence.equals(other.paxSequence))
			return false;
		if (segmentCode == null) {
			if (other.segmentCode != null)
				return false;
		} else if (!segmentCode.equals(other.segmentCode))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}
}
