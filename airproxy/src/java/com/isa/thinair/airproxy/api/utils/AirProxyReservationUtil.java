package com.isa.thinair.airproxy.api.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

public class AirProxyReservationUtil {

	private static Log log = LogFactory.getLog(AirProxyReservationUtil.class);

	/**
	 * Return pnrPaxCharges indexed by pnrPaxId.
	 * 
	 * @param pnrPaxCharges
	 * @return
	 */
	public static Map<Integer, PnrChargesDTO> indexOndChargesByPax(Collection<PnrChargesDTO> pnrPaxCharges) {
		Map<Integer, PnrChargesDTO> pnrPaxChargesMap = new HashMap<Integer, PnrChargesDTO>();
		for (PnrChargesDTO pnrPaxChargesDTO : pnrPaxCharges) {
			pnrPaxChargesMap.put(pnrPaxChargesDTO.getPnrPaxId(), pnrPaxChargesDTO);
		}
		return pnrPaxChargesMap;
	}

	public static Map<Integer, ReservationPax> prepareReservationPaxMap(Reservation res) {
		Map<Integer, ReservationPax> reservationPaxMap = new HashMap<Integer, ReservationPax>();
		for (Iterator paxIt = res.getPassengers().iterator(); paxIt.hasNext();) {
			ReservationPax pax = (ReservationPax) paxIt.next();
			reservationPaxMap.put(pax.getPnrPaxId(), pax);
		}
		return reservationPaxMap;
	}

	/**
	 * 
	 * @param lccContactInfo
	 * @return
	 * @throws ModuleException
	 */
	public static ReservationContactInfo transformContactInfo(CommonReservationContactInfo lccContactInfo) throws ModuleException {
		ReservationContactInfo contactInfo = new ReservationContactInfo();
		contactInfo.setCity(lccContactInfo.getCity());
		contactInfo.setCountryCode(lccContactInfo.getCountryCode());
		contactInfo.setZipCode(lccContactInfo.getZipCode());
		contactInfo.setCustomerId(null);
		contactInfo.setEmail(lccContactInfo.getEmail());
		contactInfo.setFax(lccContactInfo.getFax());
		contactInfo.setFirstName(lccContactInfo.getFirstName());
		contactInfo.setLastName(lccContactInfo.getLastName());
		contactInfo.setMobileNo(lccContactInfo.getMobileNo());
		if (lccContactInfo.getNationalityCode() != null) {
			contactInfo.setNationalityCode(lccContactInfo.getNationalityCode().toString());
		}
		contactInfo.setPhoneNo(lccContactInfo.getPhoneNo());
		contactInfo.setState(lccContactInfo.getState());
		contactInfo.setStreetAddress1(lccContactInfo.getStreetAddress1());
		contactInfo.setStreetAddress2(lccContactInfo.getStreetAddress2());
		contactInfo.setTitle(lccContactInfo.getTitle());
		contactInfo.setPreferredLanguage(lccContactInfo.getPreferredLanguage());
		contactInfo.setTaxRegNo(lccContactInfo.getTaxRegNo());

		// set emergency contact information
		contactInfo.setEmgnTitle(lccContactInfo.getEmgnTitle());
		contactInfo.setEmgnFirstName(lccContactInfo.getEmgnFirstName());
		contactInfo.setEmgnLastName(lccContactInfo.getEmgnLastName());
		contactInfo.setEmgnPhoneNo(lccContactInfo.getEmgnPhoneNo());
		contactInfo.setEmgnEmail(lccContactInfo.getEmgnEmail());
		contactInfo.setSendPromoEmail(lccContactInfo.getSendPromoEmail());

		return contactInfo;
	}

	/**
	 * Return the no ONDs for the reservation.
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static int getUniqueFareIds(Reservation reservation) throws ModuleException {
		Collection<ReservationSegment> pnrSegments = reservation.getSegments();
		Map pnrSegIdAndFareTO = ReservationApiUtils.getFareTOs(reservation, null);

		Collection<Integer> UniqueFareIds = new HashSet<Integer>();

		for (ReservationSegment reservationSegment : pnrSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
				UniqueFareIds.add(reservationSegment.getOndGroupId());
			}

		}

		return UniqueFareIds.size();
	}

	public static String buildOndCode(String ondCode, String segmentCode) {
		if (ondCode == null) {
			ondCode = segmentCode;
		} else {
			String transit = segmentCode.substring(0, 3);
			if (ondCode.endsWith(transit)) {
				ondCode += segmentCode.substring(3);
			} else {
				ondCode += "/" + segmentCode;
			}
		}
		return ondCode;
	}

	public static List<FlightSegmentTO> populateFlightSegmentTO(Collection<LCCClientReservationSegment> existingAllSegs) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (LCCClientReservationSegment resSegment : existingAllSegs) {
			FlightSegmentTO fltSeg = new FlightSegmentTO();
			fltSeg.setSegmentCode(resSegment.getSegmentCode());
			fltSeg.setDepartureDateTime(resSegment.getDepartureDate());
			fltSeg.setDepartureDateTimeZulu(resSegment.getDepartureDateZulu());
			fltSeg.setDepartureTerminalName(resSegment.getDepartureTerminalName());
			fltSeg.setArrivalDateTime(resSegment.getArrivalDate());
			fltSeg.setArrivalDateTimeZulu(resSegment.getArrivalDateZulu());
			fltSeg.setArrivalTerminalName(resSegment.getArrivalTerminalName());
			fltSeg.setFlightNumber(resSegment.getFlightNo());
			fltSeg.setFlightRefNumber(resSegment.getFlightSegmentRefNumber());
			fltSeg.setCabinClassCode(resSegment.getCabinClassCode());
			fltSeg.setCsOcCarrierCode(resSegment.getCsOcCarrierCode());
			fltSeg.setDomesticFlight(resSegment.isDomesticFlight());
			fltSeg.setSegmentSequence(resSegment.getSegmentSeq());
			fltSeg.setOperatingAirline(resSegment.getCarrierCode());
			flightSegmentTOs.add(fltSeg);
		}

		return flightSegmentTOs;
	}

	public static List<FlightSegmentDTO> populateFlightSegmentDTO(Collection<LCCClientReservationSegment> existingAllSegs) {
		List<FlightSegmentDTO> flightSegmentTOs = new ArrayList<FlightSegmentDTO>();

		for (LCCClientReservationSegment resSegment : existingAllSegs) {
			FlightSegmentDTO fltSeg = new FlightSegmentDTO();
			fltSeg.setFromAirport(resSegment.getSegmentCode().substring(0, 3));
			fltSeg.setToAirport(resSegment.getSegmentCode().substring(resSegment.getSegmentCode().length() - 3,
					resSegment.getSegmentCode().length()));
			fltSeg.setSegmentCode(resSegment.getSegmentCode());
			fltSeg.setDepartureDateTime(resSegment.getDepartureDate());
			fltSeg.setDepartureDateTimeZulu(resSegment.getDepartureDateZulu());
			fltSeg.setDepartureTerminal(resSegment.getDepartureTerminalName());
			fltSeg.setArrivalDateTime(resSegment.getArrivalDate());
			fltSeg.setArrivalDateTimeZulu(resSegment.getArrivalDateZulu());
			fltSeg.setArrivalTerminal(resSegment.getArrivalTerminalName());
			fltSeg.setFlightNumber(resSegment.getFlightNo());
			fltSeg.setSegmentId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSegment.getFlightSegmentRefNumber()));
			fltSeg.setDomesticFlight(resSegment.isDomesticFlight());
			flightSegmentTOs.add(fltSeg);
		}

		return flightSegmentTOs;
	}

	public static Date getFirstDepartureFlightDate(Collection<LCCClientReservationSegment> colResSegs) {
		LCCClientReservationSegment firstDeptFltSeg = null;
		for (LCCClientReservationSegment seg : colResSegs) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus())) {
				String segmentCode = seg.getSegmentCode();
				if (segmentCode != null) {
					if (firstDeptFltSeg == null) {
						firstDeptFltSeg = seg;
					} else {
						if (firstDeptFltSeg.getDepartureDateZulu().after(seg.getDepartureDateZulu())) {
							firstDeptFltSeg = seg;
						}
					}
				}
			}
		}
		if (firstDeptFltSeg != null) {
			return firstDeptFltSeg.getDepartureDateZulu();
		}

		return null;
	}

	public static Date getEffectiveFirstDepartureDate(Date firstDepartDateForNewSegments,
			Collection<ReservationSegmentDTO> segments, Collection<Integer> modifyingPnrSegIds) {
		Date effectiveFirstDepartureDate = firstDepartDateForNewSegments;

		for (ReservationSegmentDTO reservationSegment : segments) {
			if ((!ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus()))
					|| (modifyingPnrSegIds != null && modifyingPnrSegIds.contains(reservationSegment.getPnrSegId()))) {
				continue;
			}

			if (effectiveFirstDepartureDate.after(reservationSegment.getZuluDepartureDate())) {
				effectiveFirstDepartureDate = reservationSegment.getZuluDepartureDate();
			}
		}

		return effectiveFirstDepartureDate;
	}

	/**
	 * Remove adjustment amount used for credit promotion, when displaying total charges for front end display purpose
	 * 
	 * @param lccReservation
	 * @throws ModuleException
	 */
	public static void skipPromotionAdjustments(LCCClientReservation lccReservation) throws ModuleException {
		if (lccReservation.getLccPromotionInfoTO() != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(lccReservation.getLccPromotionInfoTO()
						.getDiscountAs())
				&& !ReservationInternalConstants.ReservationStatus.CANCEL.equals(lccReservation.getStatus())) {
			BigDecimal totalAdjWithoutPromoAdj = lccReservation.getTotalTicketAdjustmentCharge();
			BigDecimal totPromoAdj = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (LCCClientReservationPax lccResPax : lccReservation.getPassengers()) {
				BigDecimal paxTotalAdjWithoutPromoAdj = lccResPax.getTotalAdjustmentCharge();
				List<FeeTO> fees = lccResPax.getFees();
				if (fees != null && !fees.isEmpty()) {
					BigDecimal paxTotPromoAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
					for (FeeTO feeTO : fees) {
						if (ChargeCodes.DISCOUNT_PROMO_ADJUSTMENT.equals(feeTO.getChargeCode())
								|| ChargeCodes.DISCOUNT_PROMO_ADJUSTMENT.equals(feeTO.getFeeCode())) {
							paxTotPromoAdj = AccelAeroCalculator.add(paxTotPromoAdj, feeTO.getAmount());
						}
					}

					paxTotalAdjWithoutPromoAdj = AccelAeroCalculator.subtract(paxTotalAdjWithoutPromoAdj, paxTotPromoAdj);
					totPromoAdj = AccelAeroCalculator.add(totPromoAdj, paxTotPromoAdj);
				}

				lccResPax.setTotalAdjustmentCharge(paxTotalAdjWithoutPromoAdj);
			}

			totalAdjWithoutPromoAdj = AccelAeroCalculator.subtract(totalAdjWithoutPromoAdj, totPromoAdj);
			lccReservation.setTotalTicketAdjustmentCharge(totalAdjWithoutPromoAdj);
		}
	}

	public static BigDecimal getTotalRedeemedPoints(Map<Integer, Map<String, BigDecimal>> paxProductRedeemedValues) {
		BigDecimal totalRedeemedTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (paxProductRedeemedValues != null && !paxProductRedeemedValues.isEmpty()) {
			for (Integer paxSeq : paxProductRedeemedValues.keySet()) {
				BigDecimal paxRedeemedTotal = getPaxRedeemedTotal(paxProductRedeemedValues, paxSeq);
				totalRedeemedTotal = AccelAeroCalculator.add(totalRedeemedTotal, paxRedeemedTotal);
			}
		}

		return totalRedeemedTotal;
	}

	public static BigDecimal getPaxRedeemedTotal(Map<Integer, Map<String, BigDecimal>> paxProductRedeemedValues,
			Integer paxSequence) {
		BigDecimal paxRedeemedTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (paxProductRedeemedValues != null && !paxProductRedeemedValues.isEmpty()) {
			Map<String, BigDecimal> productRedeemedTotal = paxProductRedeemedValues.get(paxSequence);
			if (productRedeemedTotal != null && !productRedeemedTotal.isEmpty()) {
				for (BigDecimal productTotal : productRedeemedTotal.values()) {
					paxRedeemedTotal = AccelAeroCalculator.add(paxRedeemedTotal, productTotal);
				}
			}
		}

		return AccelAeroCalculator.parseBigDecimal(paxRedeemedTotal.doubleValue());
	}

	public static Map<Integer, BigDecimal> getPaxRedeemedTotalMap(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments) {
		Map<Integer, BigDecimal> paxRedeemTotalMap = new HashMap<Integer, BigDecimal>();
		if (carrierWiseLoyaltyPayments != null) {
			BigDecimal paxLoyaltyPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (LoyaltyPaymentInfo loyaltyPaymentInfo : carrierWiseLoyaltyPayments.values()) {
				for (Integer paxSeq : loyaltyPaymentInfo.getPaxProductPaymentBreakdown().keySet()) {
					paxLoyaltyPayAmount = getPaxRedeemedTotal(loyaltyPaymentInfo.getPaxProductPaymentBreakdown(), paxSeq);
					if (paxRedeemTotalMap.containsKey(paxSeq)) {
						paxLoyaltyPayAmount = AccelAeroCalculator.add(paxRedeemTotalMap.get(paxSeq), paxLoyaltyPayAmount);
					}

					paxRedeemTotalMap.put(paxSeq, paxLoyaltyPayAmount);
				}
			}
		}
		return paxRedeemTotalMap;
	}
	
	public static Map<Integer, List<ExternalChgDTO>>
			composeHandlingFeeChargesByPaxSequence(LCCClientSegmentAssembler lccClientSegmentAssembler) throws ModuleException {
		if (lccClientSegmentAssembler != null && lccClientSegmentAssembler.getPassengerExtChargeMap() != null
				&& !lccClientSegmentAssembler.getPassengerExtChargeMap().isEmpty()) {
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges = lccClientSegmentAssembler.getPassengerExtChargeMap();

			return convertLCCClientDTOChgToAAChgDTO(paxExternalCharges);

		}
		return null;
	}

	public static Map<Integer, List<ExternalChgDTO>> convertLCCClientDTOChgToAAChgDTO(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException {

		Map<Integer, List<ExternalChgDTO>> paxCharges = new HashMap<>();

		if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

			Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = AirproxyModuleUtils.getReservationBD()
					.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);

			if (extChgMap != null && !extChgMap.isEmpty()) {
				ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(extChgMap.values());
				if (externalChgDTO != null) {
					for (Integer paxSeq : paxExternalCharges.keySet()) {
						List<LCCClientExternalChgDTO> charges = paxExternalCharges.get(paxSeq);
						if (charges != null && !charges.isEmpty()) {
							for (LCCClientExternalChgDTO paxExtChg : charges) {

								externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
								externalChgDTO.setAmountConsumedForPayment(paxExtChg.isAmountConsumedForPayment());
								externalChgDTO.setAmount(paxExtChg.getAmount());
								externalChgDTO.setFlightSegId(
										FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));

								if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()
										&& paxExtChg.getOperationMode() != null) {
									externalChgDTO.setOperationMode(paxExtChg.getOperationMode());
								}

								if (paxCharges.get(paxSeq) == null) {
									paxCharges.put(paxSeq, new ArrayList<>());
								}

								paxCharges.get(paxSeq).add(externalChgDTO);
							}
						}

					}
				}
			}
		}

		return paxCharges;

	}

	public static boolean isOtherExternalChargesExist(LCCClientPaymentAssembler paymentAssembler) {
		if (paymentAssembler.isExternalChargesConsumed() && paymentAssembler.getPerPaxExternalCharges() != null
				&& !paymentAssembler.getPerPaxExternalCharges().isEmpty()) {
			return true;
		}
		return false;
	}
}
