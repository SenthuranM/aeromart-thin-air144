package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.util.ArrayList;
import java.util.List;

public class FlexiFareRulesInformationDTO extends FareRulesInformationDTO {

	private static final long serialVersionUID = -7088371533484356071L;

	private List<String> totalSegmentTicketPrice;
	private List<String> totalSegmentTicketPriceInSelCurr;

	public List<String> getTotalSegmentTicketPrice() {
		if (totalSegmentTicketPrice == null) {
			totalSegmentTicketPrice = new ArrayList<String>();
		}
		return totalSegmentTicketPrice;
	}

	public List<String> getTotalSegmentTicketPriceInSelCurr() {
		if (totalSegmentTicketPriceInSelCurr == null) {
			totalSegmentTicketPriceInSelCurr = new ArrayList<String>();
		}
		return totalSegmentTicketPriceInSelCurr;
	}

}
