package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author rumesh
 * 
 */
public class LogicalCabinClassInfoTO implements Serializable, Comparable<LogicalCabinClassInfoTO> {
	private static final long serialVersionUID = 5690400674711327174L;

	private String logicalCCCode;
	private String logicalCCDesc;
	private String fareBasis;
	private String comment;
	private boolean selected;
	private boolean withFlexi;
	private boolean flexiAvailable;
	private Integer flexiRuleID;
	private Integer rank;
	private Integer bundledFarePeriodId;
	private Set<String> bookingClasses;
	private Map<String, String> segmentBookingClasses;
	private Set<String> bundledFareFreeServiceName;
	private BigDecimal bundledFareFee = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal bundledFareDelta = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String imageUrl;
	private BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO;
	
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCDesc() {
		return logicalCCDesc;
	}

	public void setLogicalCCDesc(String logicalCCDesc) {
		this.logicalCCDesc = logicalCCDesc;
	}

	public String getFareBasis() {
		return fareBasis;
	}

	public void setFareBasis(String fareBasis) {
		this.fareBasis = fareBasis;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isWithFlexi() {
		return withFlexi;
	}

	public void setWithFlexi(boolean withFlexi) {
		this.withFlexi = withFlexi;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
	public Integer getBundledFarePeriodId() {
		return bundledFarePeriodId;
	}

	public void setBundledFarePeriodId(Integer bundledFarePeriodId) {
		this.bundledFarePeriodId = bundledFarePeriodId;
	}

	public Integer getFlexiRuleID() {
		return flexiRuleID;
	}

	public void setFlexiRuleID(Integer flexiRuleID) {
		this.flexiRuleID = flexiRuleID;
	}

	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public Map<String, String> getSegmentBookingClasses() {
		return segmentBookingClasses;
	}

	public void setSegmentBookingClasses(Map<String, String> segmentBookingClasses) {
		this.segmentBookingClasses = segmentBookingClasses;
	}

	public Set<String> getBundledFareFreeServiceName() {
		return bundledFareFreeServiceName;
	}

	public void setBundledFareFreeServiceName(Set<String> bundledFareFreeServiceName) {
		this.bundledFareFreeServiceName = bundledFareFreeServiceName;
	}

	public BigDecimal getBundledFareFee() {
		return bundledFareFee;
	}

	public void setBundledFareFee(BigDecimal bundledFareFee) {
		this.bundledFareFee = bundledFareFee;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public BigDecimal getBundledFareDelta() {
		return bundledFareDelta;
	}

	public void setBundledFareDelta(BigDecimal bundledFareDelta) {
		this.bundledFareDelta = bundledFareDelta;
	}

	public boolean isFlexiAvailable() {
		return flexiAvailable;
	}

	public void setFlexiAvailable(boolean flexiAvailable) {
		this.flexiAvailable = flexiAvailable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((logicalCCCode == null) ? 0 : logicalCCCode.hashCode());
		result = prime * result + (withFlexi ? 1231 : 1237);
		result = prime * result + (bundledFarePeriodId == null ? 0 : bundledFarePeriodId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (!(obj instanceof LogicalCabinClassInfoTO)) {
			return false;
		}
		LogicalCabinClassInfoTO other = (LogicalCabinClassInfoTO) obj;
		if (logicalCCCode == null) {
			if (other.logicalCCCode != null)
				return false;
		} else if (!logicalCCCode.equals(other.logicalCCCode)) {
			return false;
		}
		if (withFlexi != other.withFlexi) {
			return false;
		}
		if (!BeanUtils.nullHandler(bundledFarePeriodId).equals(BeanUtils.nullHandler(other.bundledFarePeriodId))) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(LogicalCabinClassInfoTO obj) {
		int comp = this.rank.compareTo(obj.rank);

		if (comp == 0) {
			if (this.withFlexi) {
				comp = 1;
			} else {
				comp = -1;
			}
		}

		return comp;
	}


	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateDTO() {
		return bundleFareDescriptionTemplateDTO;
	}

	public void setBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO) {
		this.bundleFareDescriptionTemplateDTO = bundleFareDescriptionTemplateDTO;
	}

}
