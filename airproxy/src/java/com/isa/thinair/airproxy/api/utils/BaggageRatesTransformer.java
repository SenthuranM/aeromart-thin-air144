package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRateUnit;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.FlightSegmentBaggageRate;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.FlightSegmentLite;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentBaggagesDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class BaggageRatesTransformer {

	public static BaggageRatesDTO transform(Collection<LCCBaggageResponseDTO> lccBaggageResponseDTOs) {
		BaggageRatesDTO baggageRateRS = new BaggageRatesDTO();

		Map<String, FlightSegmentBaggageRate> flightSegmentBaggageRateMap = new HashMap<>();
		Collection<FlightSegmentBaggageRate> flightSegmentBaggageRateSet = new HashSet<>();

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {

			Map<String, List<LCCBaggageDTO>> ondGroupIDBaggagesMap = new HashMap<String, List<LCCBaggageDTO>>();
			Map<String, List<FlightSegmentTO>> ondGroupIDSegmentsKeyMap = new HashMap<String, List<FlightSegmentTO>>();
			String baggageONDGroupId = null;
			for (LCCBaggageResponseDTO lccBaggageResponseDTO : lccBaggageResponseDTOs) {
				for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
					baggageONDGroupId = lccFlightSegmentBaggagesDTO.getFlightSegmentTO().getBaggageONDGroupId();

					if (baggageONDGroupId != null && !baggageONDGroupId.isEmpty()) {

						if (!ondGroupIDBaggagesMap.containsKey(baggageONDGroupId)) {
							ondGroupIDBaggagesMap.put(baggageONDGroupId, lccFlightSegmentBaggagesDTO.getBaggages());
							List<FlightSegmentTO> flightSegmentTos = new ArrayList<FlightSegmentTO>();
							flightSegmentTos.add(lccFlightSegmentBaggagesDTO.getFlightSegmentTO());
							ondGroupIDSegmentsKeyMap.put(baggageONDGroupId, flightSegmentTos);
						} else {
							ondGroupIDSegmentsKeyMap.get(baggageONDGroupId).add(lccFlightSegmentBaggagesDTO.getFlightSegmentTO());
						}
					}
				}
			}
			for (String key : ondGroupIDBaggagesMap.keySet()) {
				FlightSegmentBaggageRate flightSegmentBaggageRate = new FlightSegmentBaggageRate();
				for (FlightSegmentTO flightSegmentTO : ondGroupIDSegmentsKeyMap.get(key)) {
					flightSegmentBaggageRate.getOndFlights().add(transformFlightSegment(flightSegmentTO));
					ondGroupIDBaggagesMap.get(key).forEach(lccBaggage -> flightSegmentBaggageRate.getBaggageRates()
							.add(transformBaggage(lccBaggage)));

				}
				flightSegmentBaggageRateMap.put(key, flightSegmentBaggageRate);
			}
			flightSegmentBaggageRateSet = flightSegmentBaggageRateMap.values();
		} else {
			for (LCCBaggageResponseDTO lccBaggageResponseDTO : lccBaggageResponseDTOs) {
				for (LCCFlightSegmentBaggagesDTO lccFlightSegmentBaggagesDTO : lccBaggageResponseDTO.getFlightSegmentBaggages()) {
					FlightSegmentTO flightSegmentTO = lccFlightSegmentBaggagesDTO.getFlightSegmentTO();
					FlightSegmentBaggageRate flightSegmentBaggageRate = new FlightSegmentBaggageRate();
					if (lccFlightSegmentBaggagesDTO.getBaggages() != null
							&& !lccFlightSegmentBaggagesDTO.getBaggages().isEmpty()) {
						flightSegmentBaggageRate.getOndFlights().add(transformFlightSegment(flightSegmentTO));
						lccFlightSegmentBaggagesDTO.getBaggages().forEach(
								lccBaggage -> flightSegmentBaggageRate.getBaggageRates().add(transformBaggage(lccBaggage)));
					}
					flightSegmentBaggageRateSet.add(flightSegmentBaggageRate);

				}
			}
		}
		Collection<FlightSegmentBaggageRate> flightSegmentBaggageRates = new ArrayList<>(flightSegmentBaggageRateSet);
		baggageRateRS.setBaggageRates(flightSegmentBaggageRates);
		return baggageRateRS;
	}

	private static BaggageRateUnit transformBaggage(LCCBaggageDTO lccBaggageDTO) {

		BaggageRateUnit baggageRate = new BaggageRateUnit();

		baggageRate.setBaggageName(lccBaggageDTO.getBaggageName());
		baggageRate.setBaggageDescription(lccBaggageDTO.getBaggageDescription());
		baggageRate.setBaggageCharge(lccBaggageDTO.getBaggageCharge());
		baggageRate.setDefaultBggage(CommonsConstants.YES.equals(lccBaggageDTO.getDefaultBaggage()));

		return baggageRate;
	}

	private static FlightSegmentLite transformFlightSegment(FlightSegmentTO flightSegmentTO) {
		FlightSegmentLite flightSegment = new FlightSegmentLite();
		flightSegment.setFilghtDesignator(flightSegmentTO.getFlightNumber());
		flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegment.setFlightSegmentRPH(flightSegmentTO.getFlightRefNumber());
		return flightSegment;
	}

}
