package com.isa.thinair.airproxy.api.utils.converters;

import java.util.Comparator;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;

public class AncillarySegComparator implements Comparator<LCCSelectedSegmentAncillaryDTO> {
	@Override
	public int compare(LCCSelectedSegmentAncillaryDTO anciDTOFirst, LCCSelectedSegmentAncillaryDTO anciDTOSecond) {
		return FlightRefNumberUtil.getDepartureDateFromFlightRPH(anciDTOFirst.getFlightSegmentTO().getFlightRefNumber())
				.compareTo(
						FlightRefNumberUtil
								.getDepartureDateFromFlightRPH(anciDTOSecond.getFlightSegmentTO().getFlightRefNumber()));
	}
}
