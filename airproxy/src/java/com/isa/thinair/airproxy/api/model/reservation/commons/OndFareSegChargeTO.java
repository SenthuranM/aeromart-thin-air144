package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;

public class OndFareSegChargeTO implements Serializable, Comparable<OndFareSegChargeTO> {

	private static final long serialVersionUID = 3488008072141184302L;

	/** For fare rebuild */
	private int fareID;
	private int farePercentage;
	private String fareBasisCode;

	/** Ond wise FareType . This will not indicate the fare type of the total journey */
	private int fareType;

	/** direction indicators */
	private boolean isInbound;
	private boolean isModifyInbound; // used to identify inbound modification

	/** Open return indicator */
	private boolean isOpenReturn;

	/**
	 * Segments the ond is made up of key=fsegmentId, value=Collection of BC with no of allocations. Order of the
	 * fsegmentId : "departure date zulu" Order of BookingClassAllocs : "Nest Rank"
	 */
	private LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc;

	/**
	 * For rebuilding chargers
	 */
	private Collection<Integer> chargeRateIds;

	/**
	 * For rebuilding flexi
	 */
	private Collection<Integer> flexiChargeIds;

	private int ondSequence;

	private int subJourneyGroup = -1;

	private boolean busSegmentExists;

	private int fareRuleId;

	private String ondCode;

	private int requestedOndSequence;
	
	private boolean splitOperationForBusInvoked;

	/**
	 * @return the fareID
	 */
	public int getFareID() {
		return fareID;
	}

	/**
	 * @param fareID
	 *            the fareID to set
	 */
	public void setFareID(int fareID) {
		this.fareID = fareID;
	}

	/**
	 * @return the farePercentage
	 */
	public int getFarePercentage() {
		return farePercentage;
	}

	/**
	 * @param farePercentage
	 *            the farePercentage to set
	 */
	public void setFarePercentage(int farePercentage) {
		this.farePercentage = farePercentage;
	}

	/**
	 * @return the fareType
	 */
	public int getFareType() {
		return fareType;
	}

	/**
	 * @param fareType
	 *            the fareType to set
	 */
	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	/**
	 * @return the isInbound
	 */
	public boolean isInbound() {
		return isInbound;
	}

	/**
	 * @param isInbound
	 *            the isInbound to set
	 */
	public void setInbound(boolean isInbound) {
		this.isInbound = isInbound;
	}

	/**
	 * @return the isModifyInbound
	 */
	public boolean isModifyInbound() {
		return isModifyInbound;
	}

	/**
	 * @param isModifyInbound
	 *            the isModifyInbound to set
	 */
	public void setModifyInbound(boolean isModifyInbound) {
		this.isModifyInbound = isModifyInbound;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	/**
	 * @return the fsegBCAlloc
	 */
	public LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> getFsegBCAlloc() {
		if (fsegBCAlloc == null) {
			fsegBCAlloc = new LinkedHashMap<Integer, LinkedList<BookingClassAlloc>>();
		}
		return fsegBCAlloc;
	}

	/**
	 * @param fsegBCAlloc
	 *            the fsegBCAlloc to set
	 */
	public void setFsegBCAlloc(LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc) {
		this.fsegBCAlloc = fsegBCAlloc;
	}

	public void addFsegBCAlloc(Integer flrSegId, LinkedList<BookingClassAlloc> bookingClassAllocs) {
		getFsegBCAlloc().put(flrSegId, bookingClassAllocs);
	}

	/**
	 * @return the chargeRateIds
	 */
	public Collection<Integer> getChargeRateIds() {
		if (chargeRateIds == null) {
			chargeRateIds = new ArrayList<Integer>();
		}
		return chargeRateIds;
	}

	/**
	 * @param chargeRateIds
	 *            the chargeRateIds to set
	 */
	public void setChargeRateIds(Collection<Integer> chargeRateIds) {
		this.chargeRateIds = chargeRateIds;
	}

	public void addChargeRateIds(Integer chargeRateId) {
		getChargeRateIds().add(chargeRateId);
	}

	/**
	 * @return the flexiChargersId
	 */
	public Collection<Integer> getFlexiChargeIds() {
		if (flexiChargeIds == null) {
			flexiChargeIds = new ArrayList<Integer>();
		}
		return flexiChargeIds;
	}

	/**
	 * @param flexiChargerId
	 *            the flexiChargersId to set
	 */
	public void setFlexiChargeIds(Collection<Integer> flexiChargeIds) {
		this.flexiChargeIds = flexiChargeIds;
	}

	public void addFlexiChargeId(Integer flexiChargeId) {
		getFlexiChargeIds().add(flexiChargeId);
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	@Override
	public int compareTo(OndFareSegChargeTO o) {
		return (this.ondSequence - o.getOndSequence());
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;
	}

	public boolean isBusSegmentExists() {
		return busSegmentExists;
	}

	public void setBusSegmentExists(boolean busSegmentExists) {
		this.busSegmentExists = busSegmentExists;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public boolean isSplitOperationForBusInvoked() {
		return splitOperationForBusInvoked;
	}

	public void setSplitOperationForBusInvoked(boolean isSplitOperationForBusInvoked) {
		this.splitOperationForBusInvoked = isSplitOperationForBusInvoked;
	}

	
}
