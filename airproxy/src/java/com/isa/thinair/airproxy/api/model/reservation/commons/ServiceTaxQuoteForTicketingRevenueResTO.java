package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ServiceTaxQuoteForTicketingRevenueResTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, List<ServiceTaxTO>> paxTypeWiseServiceTaxes;
	
	private Map<Integer, List<ServiceTaxTO>> paxWiseServiceTaxes;
	
	private String serviceTaxDepositeStateCode;
	
	private String serviceTaxDepositeCountryCode;

	public Map<String, List<ServiceTaxTO>> getPaxTypeWiseServiceTaxes() {
		return paxTypeWiseServiceTaxes;
	}

	public Map<Integer, List<ServiceTaxTO>> getPaxWiseServiceTaxes() {
		return paxWiseServiceTaxes;
	}

	public String getServiceTaxDepositeStateCode() {
		return serviceTaxDepositeStateCode;
	}

	public String getServiceTaxDepositeCountryCode() {
		return serviceTaxDepositeCountryCode;
	}

	public void setPaxTypeWiseServiceTaxes(Map<String, List<ServiceTaxTO>> paxTypeWiseServiceTaxes) {
		this.paxTypeWiseServiceTaxes = paxTypeWiseServiceTaxes;
	}

	public void setPaxWiseServiceTaxes(Map<Integer, List<ServiceTaxTO>> paxWiseServiceTaxes) {
		this.paxWiseServiceTaxes = paxWiseServiceTaxes;
	}

	public void setServiceTaxDepositeStateCode(String serviceTaxDepositeStateCode) {
		this.serviceTaxDepositeStateCode = serviceTaxDepositeStateCode;
	}

	public void setServiceTaxDepositeCountryCode(String serviceTaxDepositeCountryCode) {
		this.serviceTaxDepositeCountryCode = serviceTaxDepositeCountryCode;
	}	
}
