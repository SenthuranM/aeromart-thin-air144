package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * @author mano
 * @since May 9, 2011
 */
public class LCCBaggageRequestDTO implements Serializable {

	private String transactionIdentifier;

	private String cabinClass;

	private FlightSegmentTO flightSegment;

	/**
	 * @return the transactionIdentifier
	 */
	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	/**
	 * @param transactionIdentifier
	 *            the transactionIdentifier to set
	 */
	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the flightSegment
	 */
	public FlightSegmentTO getFlightSegment() {
		return flightSegment;
	}

	/**
	 * @param flightSegment
	 *            the flightSegment to set
	 */
	public void setFlightSegment(FlightSegmentTO flightSegment) {
		this.flightSegment = flightSegment;
	}

}
