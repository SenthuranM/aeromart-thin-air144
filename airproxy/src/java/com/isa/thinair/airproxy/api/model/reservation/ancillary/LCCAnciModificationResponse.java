package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

public class LCCAnciModificationResponse implements Serializable {

	private static final long serialVersionUID = -5640337695103090705L;
	private boolean isModifcationSuccess;

	public boolean isModifcationSuccess() {
		return isModifcationSuccess;
	}

	public void setModifcationSuccess(boolean isModifcationSuccess) {
		this.isModifcationSuccess = isModifcationSuccess;
	}

}
