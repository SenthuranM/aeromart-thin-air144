package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * The Class LCCClientTransferSegmentTO.
 */
public class LCCClientTransferSegmentTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The pnr. */
	private String pnr;

	private boolean isGroupPNR;

	/** The reservation version. */
	private String reservationVersion;

	/** The old segment details. */
	private Map<String, SegmentDetails> oldSegmentDetails;

	/** The new segment details. */
	private Map<String, SegmentDetails> newSegmentDetails;

	/** Own Airline Transfer Segment Handling **/
	private Map<String, Integer> oldNewFlightSegId;

	private boolean alert;

	private boolean isSegmentTransferredByNorecProcess = false;

	/** Re protected flight segment IDs */
	private Set<Integer> reprotectFlightSegIds;

	/** Flag to save data for IBE re protection */
	private String IBEVisibleFlag;

	/** Flag for transfer flight selected */
	private String TransferSelectedFlightsFlag;

	/**
	 * Gets the pnr.
	 * 
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * Sets the pnr.
	 * 
	 * @param pnr
	 *            the new pnr
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * Gets the reservation version.
	 * 
	 * @return the reservation version
	 */
	public String getReservationVersion() {
		return reservationVersion;
	}

	/**
	 * Sets the reservation version.
	 * 
	 * @param reservationVersion
	 *            the new reservation version
	 */
	public void setReservationVersion(String reservationVersion) {
		this.reservationVersion = reservationVersion;
	}

	/**
	 * The Class SegmentDetails.
	 */
	public class SegmentDetails implements Serializable {

		private static final long serialVersionUID = -1183567194885783677L;

		/** The pnr seg id. */
		private Integer pnrSegId;

		/** The flight seg id. */
		private Integer flightSegId;

		/** The carrier. */
		private String carrier;

		/** The flight number. */
		private String flightNumber;

		/** The departure date. */
		private Date departureDate;

		/** The departure date zulu. */
		private Date departureDateZulu;

		/** The arrival date. */
		private Date arrivalDate;

		/** The arrival date zulu. */
		private Date arrivalDateZulu;

		/** The segment code. */
		private String segmentCode;

		/** The cabin class. */
		private String cabinClass;

		/** The logical cabin class. */
		private String logicalCabinClass;

		private String status;

		private Integer segmentSeq;

		/**
		 * Gets the pnr seg id.
		 * 
		 * @return the pnr seg id
		 */
		public Integer getPnrSegId() {
			return pnrSegId;
		}

		/**
		 * Sets the pnr seg id.
		 * 
		 * @param pnrSegId
		 *            the new pnr seg id
		 */
		public void setPnrSegId(Integer pnrSegId) {
			this.pnrSegId = pnrSegId;
		}

		/**
		 * Gets the flight seg id.
		 * 
		 * @return the flight seg id
		 */
		public Integer getFlightSegId() {
			return flightSegId;
		}

		/**
		 * Sets the flight seg id.
		 * 
		 * @param flightSegId
		 *            the new flight seg id
		 */
		public void setFlightSegId(Integer flightSegId) {
			this.flightSegId = flightSegId;
		}

		/**
		 * Gets the carrier.
		 * 
		 * @return the carrier
		 */
		public String getCarrier() {
			return carrier;
		}

		/**
		 * Sets the carrier.
		 * 
		 * @param carrier
		 *            the new carrier
		 */
		public void setCarrier(String carrier) {
			this.carrier = carrier;
		}

		/**
		 * Gets the flight number.
		 * 
		 * @return the flight number
		 */
		public String getFlightNumber() {
			return flightNumber;
		}

		/**
		 * Sets the flight number.
		 * 
		 * @param flightNumber
		 *            the new flight number
		 */
		public void setFlightNumber(String flightNumber) {
			this.flightNumber = flightNumber;
		}

		/**
		 * Gets the departure date.
		 * 
		 * @return the departure date
		 */
		public Date getDepartureDate() {
			return departureDate;
		}

		/**
		 * Sets the departure date.
		 * 
		 * @param departureDate
		 *            the new departure date
		 */
		public void setDepartureDate(Date departureDate) {
			this.departureDate = departureDate;
		}

		/**
		 * Gets the arrival date.
		 * 
		 * @return the arrival date
		 */
		public Date getArrivalDate() {
			return arrivalDate;
		}

		/**
		 * Sets the arrival date.
		 * 
		 * @param arrivalDate
		 *            the new arrival date
		 */
		public void setArrivalDate(Date arrivalDate) {
			this.arrivalDate = arrivalDate;
		}

		/**
		 * Gets the segment code.
		 * 
		 * @return the segment code
		 */
		public String getSegmentCode() {
			return segmentCode;
		}

		/**
		 * Sets the segment code.
		 * 
		 * @param segmentCode
		 *            the new segment code
		 */
		public void setSegmentCode(String segmentCode) {
			this.segmentCode = segmentCode;
		}

		/**
		 * Gets the cabin class.
		 * 
		 * @return the cabin class
		 */
		public String getCabinClass() {
			return cabinClass;
		}

		/**
		 * Sets the cabin class.
		 * 
		 * @param cabinClass
		 *            the new cabin class
		 */
		public void setCabinClass(String cabinClass) {
			this.cabinClass = cabinClass;
		}

		/**
		 * @return the logicalCabinClass
		 */
		public String getLogicalCabinClass() {
			return logicalCabinClass;
		}

		/**
		 * @param logicalCabinClass
		 *            the logicalCabinClass to set
		 */
		public void setLogicalCabinClass(String logicalCabinClass) {
			this.logicalCabinClass = logicalCabinClass;
		}

		/**
		 * @return the departureDateZulu
		 */
		public Date getDepartureDateZulu() {
			return departureDateZulu;
		}

		/**
		 * @param departureDateZulu
		 *            the departureDateZulu to set
		 */
		public void setDepartureDateZulu(Date departureDateZulu) {
			this.departureDateZulu = departureDateZulu;
		}

		/**
		 * @return the arrivalDateZulu
		 */
		public Date getArrivalDateZulu() {
			return arrivalDateZulu;
		}

		/**
		 * @param arrivalDateZulu
		 *            the arrivalDateZulu to set
		 */
		public void setArrivalDateZulu(Date arrivalDateZulu) {
			this.arrivalDateZulu = arrivalDateZulu;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Integer getSegmentSeq() {
			return segmentSeq;
		}

		public void setSegmentSeq(Integer segmentSeq) {
			this.segmentSeq = segmentSeq;
		}

	}

	/**
	 * Gets the old segment details.
	 * 
	 * @return the old segment details
	 */
	public Map<String, SegmentDetails> getOldSegmentDetails() {
		return oldSegmentDetails;
	}

	/**
	 * Sets the old segment details.
	 * 
	 * @param oldSegmentDetails
	 *            the new old segment details
	 */
	public void setOldSegmentDetails(Map<String, SegmentDetails> oldSegmentDetails) {
		this.oldSegmentDetails = oldSegmentDetails;
	}

	/**
	 * Gets the new segment details.
	 * 
	 * @return the new segment details
	 */
	public Map<String, SegmentDetails> getNewSegmentDetails() {
		return newSegmentDetails;
	}

	/**
	 * Sets the new segment details.
	 * 
	 * @param newSegmentDetails
	 *            the new new segment details
	 */
	public void setNewSegmentDetails(Map<String, SegmentDetails> newSegmentDetails) {
		this.newSegmentDetails = newSegmentDetails;
	}

	public boolean isGroupPNR() {
		return isGroupPNR;
	}

	public void setGroupPNR(boolean isGroupPNR) {
		this.isGroupPNR = isGroupPNR;
	}

	public Map<String, Integer> getOldNewFlightSegId() {
		return oldNewFlightSegId;
	}

	public void setOldNewFlightSegId(Map<String, Integer> oldNewFlightSegId) {
		this.oldNewFlightSegId = oldNewFlightSegId;
	}

	public boolean isAlert() {
		return alert;
	}

	public void setAlert(boolean alert) {
		this.alert = alert;
	}

	public boolean isSegmentTransferredByNorecProcess() {
		return isSegmentTransferredByNorecProcess;
	}

	public void setSegmentTransferredByNorecProcess(boolean isSegmentTransferredByNorecProcess) {
		this.isSegmentTransferredByNorecProcess = isSegmentTransferredByNorecProcess;
	}

	public Set<Integer> getReprotectFlightSegIds() {
		return reprotectFlightSegIds;
	}

	public void setReprotectFlightSegIds(Set<Integer> reprotectFlightSegIds) {
		this.reprotectFlightSegIds = reprotectFlightSegIds;
	}

	public String getIBEVisibleFlag() {
		return IBEVisibleFlag;
	}

	public void setIBEVisibleFlag(String iBEVisibleFlag) {
		IBEVisibleFlag = iBEVisibleFlag;
	}

	public String getTransferSelectedFlightsFlag() {
		return TransferSelectedFlightsFlag;
	}

	public void setTransferSelectedFlightsFlag(String transferSelectedFlightsFlag) {
		TransferSelectedFlightsFlag = transferSelectedFlightsFlag;
	}

}
