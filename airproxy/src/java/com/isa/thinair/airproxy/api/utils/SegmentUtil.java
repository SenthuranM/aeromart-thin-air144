package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

public class SegmentUtil {

	private static Log log = LogFactory.getLog(SegmentUtil.class);

	private final static String SPLIT_DELIM = "\\$";

	private final static String OND_DELIM = "/";

	/**
	 * Given the Available Flight Segment, with the SelectedFlight Segment, Determine whether they are equals based on
	 * the flight number and departure dates
	 * 
	 * @param availFltSegCol
	 * @param selectedFltSegCol
	 * @return true/false
	 */
	public static boolean isFlightSegmentSelected(AvailableIBOBFlightSegment availableFlightSegment,
			AvailableIBOBFlightSegment selectedFlightSegment) {
		if (availableFlightSegment.isDirectFlight() && !selectedFlightSegment.isDirectFlight()) {
			return false;
		} else if (!availableFlightSegment.isDirectFlight() && selectedFlightSegment.isDirectFlight()) {
			return false;
		}

		for (FlightSegmentDTO availableFlightSegmentDTO : availableFlightSegment.getFlightSegmentDTOs()) {
			boolean found = false;
			for (FlightSegmentDTO selectedFlightSegmentDTO : selectedFlightSegment.getFlightSegmentDTOs()) {
				if (selectedFlightSegmentDTO.getDepartureDateTimeZulu().compareTo(
						availableFlightSegmentDTO.getDepartureDateTimeZulu()) == 0
						&& selectedFlightSegmentDTO.getFlightNumber().equals(availableFlightSegmentDTO.getFlightNumber())
						&& selectedFlightSegmentDTO.getSegmentCode().equals(availableFlightSegmentDTO.getSegmentCode())) {
					found = true;
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns a list of flightSegmentIds for the given flight segment info.
	 * 
	 * @param flightSegment
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static Collection<Integer> getFlightSegmentIds(FlightSegmentTO flightSegment) throws ModuleException {
		Collection<Integer> flightSegmentIds = new ArrayList<Integer>();

		if (flightSegment.getFlightRefNumber() != null) {
			String arr[] = flightSegment.getFlightRefNumber().split(SPLIT_DELIM);
			String segId = (arr.length > 1) ? arr[2] : flightSegment.getFlightRefNumber();
			flightSegmentIds.add(new Integer(segId));
			return flightSegmentIds;
		}

		Integer flightId = AirproxyModuleUtils.getFlightBD().getFlightID(flightSegment.getSegmentCode(),
				flightSegment.getFlightNumber(), flightSegment.getDepartureDateTime());

		if (flightId == null) {
			throw new ModuleException("airinventory.flight.notfound");
		}

		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightId);

		Collection<FlightSeatsDTO> flightSeatsDTOs = AirproxyModuleUtils.getFlightBD().getFlightSegIdsforFlight(flightIds);

		for (FlightSeatsDTO flightSeatsDTO : flightSeatsDTOs) {
			flightSegmentIds.add(new Integer(flightSeatsDTO.getFlightSegmentID()));
		}

		return flightSegmentIds;
	}

	public static Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseClassOfServices(FlightSegmentTO flightSegment,
			Map<Integer, Boolean> csOCFlightMapping) throws ModuleException {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		populateFlightSegWiseCOS(flightSegIdWiseCos, flightSegment, csOCFlightMapping);
		return flightSegIdWiseCos;
	}

	public static Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseClassOfServices(List<FlightSegmentTO> flightSegment)
			throws ModuleException {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		for (FlightSegmentTO flightSegmentTO : flightSegment) {
			populateFlightSegWiseCOS(flightSegIdWiseCos, flightSegmentTO, null);
		}
		return flightSegIdWiseCos;
	}

	private static void populateFlightSegWiseCOS(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			FlightSegmentTO flightSegment, Map<Integer, Boolean> csOCFlightMapping) throws ModuleException {

		ClassOfServiceDTO cosDTO = new ClassOfServiceDTO(flightSegment.getCabinClassCode(),
				flightSegment.getLogicalCabinClassCode(), flightSegment.getSegmentCode());

		if (flightSegment.getFlightRefNumber() != null) {

			Integer segId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber());
			if ((csOCFlightMapping != null && csOCFlightMapping.size() > 0 && !csOCFlightMapping.get(segId))
					|| csOCFlightMapping == null) {
				flightSegIdWiseCos.put(segId, cosDTO);
			}

		} else {

			Integer flightId = AirproxyModuleUtils.getFlightBD().getFlightID(flightSegment.getSegmentCode(),
					flightSegment.getFlightNumber(), flightSegment.getDepartureDateTime());

			if (flightId == null) {
				throw new ModuleException("airinventory.flight.notfound");
			}

			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flightId);

			Collection<FlightSeatsDTO> flightSeatsDTOs = AirproxyModuleUtils.getFlightBD().getFlightSegIdsforFlight(flightIds);

			for (FlightSeatsDTO flightSeatsDTO : flightSeatsDTOs) {
				Integer segId = new Integer(flightSeatsDTO.getFlightSegmentID());
				if ((csOCFlightMapping != null && csOCFlightMapping.size() > 0 && !csOCFlightMapping.get(segId))
						|| csOCFlightMapping == null) {
					flightSegIdWiseCos.put(segId, cosDTO);
				}
			}
		}

	}

	public static void setPNRSegID(LCCClientResAlterModesTO lccClientResAlterQueryModesTO, Reservation reservation) {
		// TODO Auto-generated method stub
		if (lccClientResAlterQueryModesTO.getSelectedPnrSegID() != null) {
			String flightSegID[] = lccClientResAlterQueryModesTO.getSelectedPnrSegID().split(SPLIT_DELIM);

			Collection<ReservationSegment> reservationSegments = reservation.getSegments();

			for (Iterator iterator = reservationSegments.iterator(); iterator.hasNext();) {
				ReservationSegment reservationSegment = (ReservationSegment) iterator.next();
				if (reservationSegment.getFlightSegId().toString().equalsIgnoreCase(flightSegID[0])) {
					lccClientResAlterQueryModesTO.setSelectedPnrSegID(reservationSegment.getPnrSegId().toString());
				}

			}

		}
	}

	/**
	 * Given the interline compatible flight RPH of format G9$SHJ/CMB$205921$20100622215500$20100623043500 return the
	 * flight segment no (205921)
	 * 
	 * @return
	 */
	public static Integer getSegmentIdFromFlightRPH(String flightRPH) {
		Integer intVal = null;
		if (flightRPH == null)
			return intVal;
		String arr[] = flightRPH.split(SPLIT_DELIM);
		if (arr.length >= 5) {
			intVal = new Integer(arr[2]);
		} else if (arr.length == 1) {
			intVal = new Integer(flightRPH);
		} else {
			log.warn("CANNOT GET FLTSEGID FROM FLIGHT RPH " + flightRPH);
		}
		return intVal;
	}

	public static void setFltSegSequence(List<FlightSegmentTO> flightSegmentTOs, Integer startFltSegSeq) {
		boolean isSegSeqAlreadySet = true;
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getSegmentSequence() == 0) {
				isSegSeqAlreadySet = false;
			}
		}

		if (!isSegSeqAlreadySet) {
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);

			int seq = (startFltSegSeq != null) ? (startFltSegSeq + 1) : 1;
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				flightSegmentTO.setSegmentSequence(seq++);
			}
		}
	}

	/**
	 * @param segsSeatsAndFaresCol
	 * @param obFlightSegments
	 * @param ibFlightSegments
	 * @param nextSegmentSequnce
	 */
	@SuppressWarnings("unchecked")
	public static void setSegmentSequnceNumbers(Collection<OndFareDTO> segsSeatsAndFaresCol,
			Collection<ReservationSegmentTO> obFlightSegments, Collection<ReservationSegmentTO> ibFlightSegments,
			int nextSegmentSequnce) {

		List<ReservationSegmentTO> allSegments = new ArrayList<ReservationSegmentTO>(obFlightSegments);
		if (ibFlightSegments != null && !ibFlightSegments.isEmpty()) {
			allSegments.addAll(ibFlightSegments);
		}

		List<FlightSegmentDTO> colNewFlightSegmentDTOs = new ArrayList<FlightSegmentDTO>(
				ReleaseTimeUtil.getFlightDepartureInfo(segsSeatsAndFaresCol));

		Collections.sort(colNewFlightSegmentDTOs);

		for (FlightSegmentDTO fltSegment : colNewFlightSegmentDTOs) {
			for (ReservationSegmentTO resSegment : allSegments) {
				if (resSegment.getFlightSegId() == fltSegment.getSegmentId()) {
					resSegment.setSegmentSeq(nextSegmentSequnce++);
					break;
				}
			}
		}

	}

	public static String getFromAirport(String segmentCode) {
		String airportCode = null;
		if (segmentCode != null) {
			String arr[] = segmentCode.split(OND_DELIM);
			return airportCode = arr[0];
		}
		return airportCode;
	}

	public static String getToAirport(String segmentCode) {
		String airportCode = null;
		if (segmentCode != null) {
			String arr[] = segmentCode.split(OND_DELIM);
			return airportCode = arr[arr.length - 1];
		}
		return airportCode;
	}

	public static String getOndCode(List<String> segCodes) {
		String ondCode = "";
		Pair<String, String> originAndDest = null;
		int a = 0;

		for (; a < segCodes.size() - 1; a++) {
			originAndDest = getOriginAndDestinationAirports(segCodes.get(a));
			ondCode += originAndDest.getLeft() + OND_DELIM;
		}

		originAndDest = getOriginAndDestinationAirports(segCodes.get(a));
		ondCode += originAndDest.getLeft() + OND_DELIM + originAndDest.getRight();

		return ondCode;
	}

	public static Pair<String, String> getOriginAndDestinationAirports(String segmentCode) {
		Pair<String, String> originDestPair = null;
		if (segmentCode != null) {
			String arr[] = segmentCode.split(OND_DELIM);
			originDestPair = Pair.of(arr[0], arr[arr.length - 1]);
		}
		return originDestPair;

	}
}
