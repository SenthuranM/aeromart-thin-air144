package com.isa.thinair.airproxy.api.utils.assembler;

public interface IPaxCountAssembler {

	public abstract int getInfantCount();

	public abstract int getChildCount();

	public abstract int getAdultCount();

}
