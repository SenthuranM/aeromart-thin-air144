package com.isa.thinair.airproxy.api.dto;

public interface ReservationConstants {

	/** Reservation Segments Alteration Operations */
	public static interface AlterationType {
		public static String ALT_CANCEL_RES = "CNXRES";
		public static String ALT_CANCEL_OND = "CNXOND";
		public static String ALT_CANCEL_OND_WITH_MODIFY_CHARGE = "CNXONDWITHMODCHG";
		public static String ALT_MODIFY_OND = "MODOND";
		public static String ALT_ADD_OND = "ADDOND";
		public static String ALT_ADD_INF = "ADDINF";
		public static String ALT_VOID_RES = "VOIDRES";
		public static String ALT_REQUOTE = "REQUOTE";
	}
	
	public static interface SegmentStatus {
		public static String CONFIRMED = "CNF";
		public static String CANCELED = "CNX";
	}

}
