package com.isa.thinair.airproxy.api.utils;

import java.util.Date;

import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class NameChangeUtil {
	public static String populateStationWiseNameChangeInfo(LocationBD locationBD, Reservation reservation)
			throws ModuleException {
		String firstDepartingStation = "";
		if (reservation.getSegmentsView() != null) {
			Date depDate = null;
			Date prevDate = null;
			ReservationSegmentDTO firstSegment = null;
			for (ReservationSegmentDTO reservationSegment : reservation.getSegmentsView()) {
				depDate = reservationSegment.getDepartureDate();
				if ((prevDate == null || (prevDate != null && (prevDate.compareTo(depDate) > 0)))
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
								.equals(reservationSegment.getStatus())) {

					firstDepartingStation = reservationSegment.getSegmentCode().substring(0,
							reservationSegment.getSegmentCode().indexOf("/"));
					firstSegment = reservationSegment;
				}
				prevDate = depDate;
			}
			if (!firstDepartingStation.isEmpty()) {
				String hoursMins = locationBD.getNameChangeThresholTimePerStation(firstDepartingStation);
				if (hoursMins == null || "".equals(hoursMins.trim())) {
					if (firstSegment.isInternationalFlight()) {
						hoursMins = AppSysParamsUtil.getInternationalModBufferDurationInHours() + ":00";
					} else {
						hoursMins = AppSysParamsUtil.getDomesticModBufferDurationInHours() + ":00";
					}
				}
				return hoursMins;
			}
		}
		return AppSysParamsUtil.getInternationalModBufferDurationInHours() + ":00";
	}

	public static String populateStationWiseNameChangeInfo(LocationBD locationBD, String agentStation,
			LCCClientReservation lccReservation) throws ModuleException {
		String hoursMins = locationBD.getNameChangeThresholTimePerStation(agentStation);
		if (hoursMins == null || "".equals(hoursMins.trim())) {
			if (lccReservation != null && lccReservation.getSegments() != null) {
				LCCClientReservationSegment firstSegment = null;
				for (LCCClientReservationSegment segment : lccReservation.getSegments()) {
					if (firstSegment == null
							|| firstSegment.getDepartureDateZulu().compareTo(segment.getDepartureDateZulu()) > 0) {
						firstSegment = segment;
					}
				}

				if (firstSegment != null && firstSegment.isDomesticFlight()) {
					hoursMins = AppSysParamsUtil.getDomesticModBufferDurationInHours() + ":00";
				} else {
					hoursMins = AppSysParamsUtil.getInternationalModBufferDurationInHours() + ":00";
				}
			} else {
				hoursMins = AppSysParamsUtil.getInternationalModBufferDurationInHours() + ":00";
			}
		}
		return hoursMins;
	}

}
