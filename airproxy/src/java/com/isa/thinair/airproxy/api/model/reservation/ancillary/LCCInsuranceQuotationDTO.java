package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;

public class LCCInsuranceQuotationDTO implements Serializable, Comparable<LCCInsuranceQuotationDTO> {

	private static final long serialVersionUID = 1L;

	private String policyCode;
	private String quotedCurrencyCode;
	private BigDecimal quotedTotalPremiumAmount;
	private BigDecimal totalPerPaxPremiumAmount;
	private String operatingAirline;
	private String insuranceRefNumber;
	private LCCInsuredJourneyDTO insuredJourney;

	private BigDecimal displayTotalPremiumAmount;
	private String responseCode;
	private String reponseMessage;
	private int insuranceType;
	private Map<InsuranceTypes, BigDecimal> insTypeCharges;
	private String insuranceProvider;
	private boolean isAllDomastic = true;

	private String ssrFeeCode;
	private String planCode;

	private boolean isEuropianCountry;

	private boolean isAnciOffer = false;
	private String selectedLanguageAnciOfferName;
	private String selectedLanguageAnciOfferDescription;
	private Map<String, String> anciOfferNameTranslations;
	private Map<String, String> anciOfferDescriptionTranslations;

	private int rank;
	private int groupID;
	private int subGroupID;
	private boolean isExternalContent;
	private boolean popup;
	private String displayName;
	private Map<String, String> insuranceExternalContent;

	public void setEuropianCountry(boolean isEuropianCountry) {
		this.isEuropianCountry = isEuropianCountry;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getQuotedCurrencyCode() {
		return quotedCurrencyCode;
	}

	public void setQuotedCurrencyCode(String quotedCurrencyCode) {
		this.quotedCurrencyCode = quotedCurrencyCode;
	}

	public BigDecimal getQuotedTotalPremiumAmount() {
		return quotedTotalPremiumAmount;
	}

	public void setQuotedTotalPremiumAmount(BigDecimal quotedTotalPremiumAmount) {
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getInsuranceRefNumber() {
		return insuranceRefNumber;
	}

	public void setInsuranceRefNumber(String insuranceRefNumber) {
		this.insuranceRefNumber = insuranceRefNumber;
	}

	public LCCInsuredJourneyDTO getInsuredJourney() {
		return insuredJourney;
	}

	public void setInsuredJourney(LCCInsuredJourneyDTO insuredJourney) {
		this.insuredJourney = insuredJourney;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getReponseMessage() {
		return reponseMessage;
	}

	public void setReponseMessage(String reponseMessage) {
		this.reponseMessage = reponseMessage;
	}

	public BigDecimal getDisplayTotalPremiumAmount() {
		return displayTotalPremiumAmount;
	}

	public void setDisplayTotalPremiumAmount(BigDecimal displayTotalPremiumAmount) {
		this.displayTotalPremiumAmount = displayTotalPremiumAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCInsuranceQuotationDTO [insuranceRefNumber=");
		builder.append(insuranceRefNumber);
		builder.append(", insuredJourney=");
		builder.append(insuredJourney);
		builder.append(", policyCode=");
		builder.append(policyCode);
		builder.append(", operatingAirline=");
		builder.append(operatingAirline);
		builder.append(", quotedCurrencyCode=");
		builder.append(quotedCurrencyCode);
		builder.append(", quotedTotalPremiumAmount=");
		builder.append(quotedTotalPremiumAmount);
		builder.append("]");
		return builder.toString();
	}

	public Map<InsuranceTypes, BigDecimal> getInsTypeCharges() {
		return insTypeCharges;
	}

	public void setInsTypeCharges(Map<InsuranceTypes, BigDecimal> insTypeCharges) {
		this.insTypeCharges = insTypeCharges;
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getInsuranceProvider() {
		return insuranceProvider;
	}

	public void setInsuranceProvider(String insuranceProvider) {
		this.insuranceProvider = insuranceProvider;
	}

	public boolean isEuropianCountry() {
		return isEuropianCountry;
	}

	public void setIsEuropianCountry(boolean isEuropianCountry) {
		this.isEuropianCountry = isEuropianCountry;
	}

	public BigDecimal getTotalPerPaxPremiumAmount() {
		return totalPerPaxPremiumAmount;
	}

	public void setTotalPerPaxPremiumAmount(BigDecimal totalPerPaxPremiumAmount) {
		this.totalPerPaxPremiumAmount = totalPerPaxPremiumAmount;
	}

	public boolean isAllDomastic() {
		return isAllDomastic;
	}

	public void setAllDomastic(boolean isAllDomastic) {
		this.isAllDomastic = isAllDomastic;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the anciOfferNameTranslations
	 */
	public Map<String, String> getAnciOfferNameTranslations() {
		return anciOfferNameTranslations;
	}

	/**
	 * @param anciOfferNameTranslations
	 *            the anciOfferNameTranslations to set
	 */
	public void setAnciOfferNameTranslations(Map<String, String> anciOfferNameTranslations) {
		this.anciOfferNameTranslations = anciOfferNameTranslations;
	}

	/**
	 * @return the anciOfferDescriptionTranslations
	 */
	public Map<String, String> getAnciOfferDescriptionTranslations() {
		return anciOfferDescriptionTranslations;
	}

	/**
	 * @param anciOfferDescriptionTranslations
	 *            the anciOfferDescriptionTranslations to set
	 */
	public void setAnciOfferDescriptionTranslations(Map<String, String> anciOfferDescriptionTranslations) {
		this.anciOfferDescriptionTranslations = anciOfferDescriptionTranslations;
	}

	/**
	 * @return the selectedLanguageAnciOfferDescription
	 */
	public String getSelectedLanguageAnciOfferDescription() {
		return selectedLanguageAnciOfferDescription;
	}

	/**
	 * @param selectedLanguageAnciOfferDescription
	 *            the selectedLanguageAnciOfferDescription to set
	 */
	public void setSelectedLanguageAnciOfferDescription(String selectedLanguageAnciOfferDescription) {
		this.selectedLanguageAnciOfferDescription = selectedLanguageAnciOfferDescription;
	}

	/**
	 * @return the selectedLanguageAnciOfferName
	 */
	public String getSelectedLanguageAnciOfferName() {
		return selectedLanguageAnciOfferName;
	}

	/**
	 * @param selectedLanguageAnciOfferName
	 *            the selectedLanguageAnciOfferName to set
	 */
	public void setSelectedLanguageAnciOfferName(String selectedLanguageAnciOfferName) {
		this.selectedLanguageAnciOfferName = selectedLanguageAnciOfferName;
	}

	public String getSsrFeeCode() {
		return ssrFeeCode;
	}

	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}


	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public int getSubGroupID() {
		return subGroupID;
	}

	public void setSubGroupID(int subGroupID) {
		this.subGroupID = subGroupID;
	}

	public boolean isExternalContent() {
		return isExternalContent;
	}

	public void setExternalContent(boolean isExternalContent) {
		this.isExternalContent = isExternalContent;
	}

	@Override
	public int compareTo(LCCInsuranceQuotationDTO lccInsQuoteDTO) {
		if (this.getRank() < lccInsQuoteDTO.getRank()) {
			return -1;
		} else if (this.getRank() > lccInsQuoteDTO.getRank()) {
			return 1;
		} else if (this.getGroupID() < lccInsQuoteDTO.getGroupID()) {
			return -1;
		} else if (this.getGroupID() > lccInsQuoteDTO.getGroupID()) {
			return 1;
		} else if (this.subGroupID < lccInsQuoteDTO.getSubGroupID()) {
			return -1;
		} else if (this.subGroupID > lccInsQuoteDTO.getSubGroupID()) {
			return 1;
		} else {
			return 0;
		}

	}

	public boolean isPopup() {
		return popup;
	}

	public void setPopup(boolean popup) {
		this.popup = popup;
	}
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public Map<String, String> getInsuranceExternalContent() {
		return insuranceExternalContent;
	}

	public void setInsuranceExternalContent(
			Map<String, String> insuranceExternalContent) {
		this.insuranceExternalContent = insuranceExternalContent;
	}
	
}
