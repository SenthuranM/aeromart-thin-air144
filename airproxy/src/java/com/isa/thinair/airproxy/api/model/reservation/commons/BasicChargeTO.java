package com.isa.thinair.airproxy.api.model.reservation.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public abstract class BasicChargeTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected BigDecimal amount;
	protected BigDecimal localCurrencyAmount;
	protected String localCurrencyCode;
	protected int ondSequence;
	protected Date departureDate;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getLocalCurrencyAmount() {
		return localCurrencyAmount;
	}

	public void setLocalCurrencyAmount(BigDecimal localCurrencyAmount) {
		this.localCurrencyAmount = localCurrencyAmount;
	}

	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	public boolean isDefinedInLocal() {
		return getLocalCurrencyCode() != null;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

}
