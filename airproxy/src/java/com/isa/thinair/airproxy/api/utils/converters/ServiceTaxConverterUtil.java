package com.isa.thinair.airproxy.api.utils.converters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.CnxModPenChargeDTO;
import com.isa.thinair.airpricing.api.dto.PaxCnxModPenChargeDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class ServiceTaxConverterUtil {
	
	public static ServiceTaxQuoteForTicketingRevenueRQ createServiceTaxQuoteCriteriaForTicketingRevenueRequest(
			ServiceTaxQuoteCriteriaForTktDTO criteria, UserPrincipal userPrincipal) throws ModuleException {

		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTicketingRevenueRQ = new ServiceTaxQuoteForTicketingRevenueRQ();
		
		if (criteria.getBaseAvailRQ() != null && criteria.getFareSegChargeTO()!=null) {
			OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(criteria.getBaseAvailRQ(), criteria.getFareSegChargeTO());
			Collection<OndFareDTO> collFares = AirproxyModuleUtils.getAirReservationQueryBD().recreateFareSegCharges(
					ondRebuildCriteria);
	
			List<Collection<OndFareDTO>> jouneyONDWiseFareONDs = new ArrayList<Collection<OndFareDTO>>();
			Collections.sort((List<OndFareDTO>) collFares);
			for (OndFareDTO ondFareDTO : collFares) {
				if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
					if (jouneyONDWiseFareONDs.size() == ondFareDTO.getRequestedOndSequence()) {
						jouneyONDWiseFareONDs.add(new ArrayList<OndFareDTO>());
					}
					jouneyONDWiseFareONDs.get(ondFareDTO.getRequestedOndSequence()).add(ondFareDTO);

				}else{
					if (jouneyONDWiseFareONDs.size() == ondFareDTO.getOndSequence()) {
						jouneyONDWiseFareONDs.add(new ArrayList<OndFareDTO>());
					}
					jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
				}
			}
			IPaxCountAssembler paxAssm = new PaxCountAssembler(criteria.getBaseAvailRQ().getTravelerInfoSummary()
					.getPassengerTypeQuantityList());
			FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(jouneyONDWiseFareONDs, paxAssm, userPrincipal.getAgentCode(),
					CommonUtil.getAppIndicator(criteria.getBaseAvailRQ().getAvailPreferences().getAppIndicator().toString()),
					criteria.getBaseAvailRQ());
			serviceTaxQuoteForTicketingRevenueRQ.setPaxTypeWiseCharges(composePaxTypeWiseCharges(fareTypeTO));
		}

		if (criteria.getPaxWiseExternalCharges().values() != null && !criteria.getPaxWiseExternalCharges().values().isEmpty()) {
			Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();
			Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			for (Entry<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalChargeEntry : criteria.getPaxWiseExternalCharges()
					.entrySet()) {
				if (paxWiseExternalChargeEntry.getValue() != null && !paxWiseExternalChargeEntry.getValue().isEmpty()) {
					List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
					SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
					paxDTO.setPaxSequence(paxWiseExternalChargeEntry.getKey());
					if (criteria.getPaxWisePaxTypes() != null
							&& criteria.getPaxWisePaxTypes().get(paxWiseExternalChargeEntry.getKey()) != null) {
						paxDTO.setPaxType(criteria.getPaxWisePaxTypes().get(paxWiseExternalChargeEntry.getKey()));
					}
					for (LCCClientExternalChgDTO externalChgDTO : paxWiseExternalChargeEntry.getValue()) {
						if (externalChgDTO.getAmount() != null && externalChgDTO.getAmount().doubleValue() > 0) {
							SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
							chargeDTO.setAmount(externalChgDTO.getAmount());
							chargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
							chargeDTO.setCarrierCode(externalChgDTO.getCarrierCode());
							chargeDTO.setChargeCode(externalChargesMap.get(externalChgDTO.getExternalCharges().toString()));
							chargeDTO.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
							chargeDTO.setSegmentCode(externalChgDTO.getSegmentCode());
							chargeDTO.setSegmentSequence(externalChgDTO.getSegmentSequence());
							chargesList.add(chargeDTO);
						}
					}
					if (!chargesList.isEmpty()) {
						paxWiseCharges.put(paxDTO, chargesList);
					}
				}
			}
			serviceTaxQuoteForTicketingRevenueRQ.setPaxWiseExternalCharges(paxWiseCharges);
		}

		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
		if (criteria.getFlightSegmentTOs() != null && !criteria.getFlightSegmentTOs().isEmpty()) {
			for (FlightSegmentTO flightSegmentTO : criteria.getFlightSegmentTOs()) {
				SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
				simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
				simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
				String logicalCabinClassCode = flightSegmentTO.getLogicalCabinClassCode();
				if (StringUtil.isNullOrEmpty(logicalCabinClassCode)
						&& !StringUtil.isNullOrEmpty(flightSegmentTO.getCabinClassCode())) {
					logicalCabinClassCode = flightSegmentTO.getCabinClassCode();
				}
				simplifiedFlightSegmentDTO.setLogicalCabinClassCode(logicalCabinClassCode);
				simplifiedFlightSegmentDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				simplifiedFlightSegmentDTO.setOndSequence(flightSegmentTO.getOndSequence());
				simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
				simplifiedFlightSegmentDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				simplifiedFlightSegmentDTO.setReturnFlag(flightSegmentTO.isReturnFlag());
				simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getOperatingAirline());
				simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
			}
		}
		serviceTaxQuoteForTicketingRevenueRQ.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
		serviceTaxQuoteForTicketingRevenueRQ.setPaxState(criteria.getPaxState());
		serviceTaxQuoteForTicketingRevenueRQ.setPaxCountryCode(criteria.getPaxCountryCode());
		serviceTaxQuoteForTicketingRevenueRQ.setPaxTaxRegistered(criteria.isPaxTaxRegistered());
		
		if (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_GDS) {
			serviceTaxQuoteForTicketingRevenueRQ.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		} else {
			serviceTaxQuoteForTicketingRevenueRQ.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil
					.getSalesChannelName(userPrincipal.getSalesChannel())));
		}
		return serviceTaxQuoteForTicketingRevenueRQ;
	}
	
	public static Map<String, List<SimplifiedChargeDTO>> composePaxTypeWiseCharges(FareTypeTO fareTypeTO) {
		
		Map<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges = new HashMap<String, List<SimplifiedChargeDTO>>();
		List<SimplifiedChargeDTO> adultChargeDTOs = new ArrayList<SimplifiedChargeDTO>();
		List<SimplifiedChargeDTO> childChargeDTOs = new ArrayList<SimplifiedChargeDTO>();
		List<SimplifiedChargeDTO> infantChargeDTOs = new ArrayList<SimplifiedChargeDTO>();

		for (PerPaxPriceInfoTO perPaxPriceInfo : fareTypeTO.getPerPaxPriceInfo()) {
			if (paxTypeWiseCharges.get(perPaxPriceInfo.getPassengerType()) == null) {
				for (BaseFareTO baseFareTO : perPaxPriceInfo.getPassengerPrice().getBaseFares()) {
					SimplifiedChargeDTO fareDTO = new SimplifiedChargeDTO();
					fareDTO.setAmount(baseFareTO.getAmount());
					fareDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.FAR);
					if (baseFareTO.getCarrierCode() != null && baseFareTO.getCarrierCode().size() > 0) {
						fareDTO.setCarrierCode(baseFareTO.getCarrierCode().get(0));
					}
					fareDTO.setLogicalCCCode(baseFareTO.getLogicalCCType());
					fareDTO.setOndSequence(baseFareTO.getOndSequence());
					fareDTO.setSegmentCode(baseFareTO.getSegmentCode());
		
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
						adultChargeDTOs.add(fareDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
						childChargeDTOs.add(fareDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
						infantChargeDTOs.add(fareDTO);
					}
				}
		
				for (TaxTO taxTO : perPaxPriceInfo.getPassengerPrice().getTaxes()) {
					SimplifiedChargeDTO taxDTO = new SimplifiedChargeDTO();
					taxDTO.setAmount(taxTO.getAmount());
					taxDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.TAX);
					taxDTO.setCarrierCode(taxTO.getCarrierCode());
					taxDTO.setChargeCode(taxTO.getTaxCode());
					taxDTO.setOndSequence(taxTO.getOndSequence());
					taxDTO.setSegmentCode(taxTO.getSegmentCode());
		
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
						adultChargeDTOs.add(taxDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
						childChargeDTOs.add(taxDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
						infantChargeDTOs.add(taxDTO);
					}
				}
		
				for (SurchargeTO surchargeTO : perPaxPriceInfo.getPassengerPrice().getSurcharges()) {
					SimplifiedChargeDTO surchargeDTO = new SimplifiedChargeDTO();
					surchargeDTO.setAmount(surchargeTO.getAmount());
					surchargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
					surchargeDTO.setCarrierCode(surchargeTO.getCarrierCode());
					surchargeDTO.setChargeCode(surchargeTO.getSurchargeCode());
					surchargeDTO.setOndSequence(surchargeTO.getOndSequence());
					surchargeDTO.setSegmentCode(surchargeTO.getSegmentCode());
		
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
						adultChargeDTOs.add(surchargeDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
						childChargeDTOs.add(surchargeDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
						infantChargeDTOs.add(surchargeDTO);
					}
				}
		
				for (FeeTO feeTO : perPaxPriceInfo.getPassengerPrice().getFees()) {
					SimplifiedChargeDTO feeDTO = new SimplifiedChargeDTO();
					feeDTO.setAmount(feeTO.getAmount());
					feeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
					feeDTO.setCarrierCode(feeTO.getCarrierCode());
					feeDTO.setChargeCode(feeTO.getFeeCode());
					feeDTO.setOndSequence(feeTO.getOndSequence());
					feeDTO.setSegmentCode(feeTO.getSegmentCode());
		
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
						adultChargeDTOs.add(feeDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
						childChargeDTOs.add(feeDTO);
					}
					if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
						infantChargeDTOs.add(feeDTO);
					}
				}
				if (!adultChargeDTOs.isEmpty()) {
					paxTypeWiseCharges.put(PaxTypeTO.ADULT, adultChargeDTOs);
				}
				if (!childChargeDTOs.isEmpty()) {
					paxTypeWiseCharges.put(PaxTypeTO.CHILD, childChargeDTOs);
				}
				if (!infantChargeDTOs.isEmpty()) {
					paxTypeWiseCharges.put(PaxTypeTO.INFANT, infantChargeDTOs);
				}
			}
		}
		return paxTypeWiseCharges;
	}
	
	public static ServiceTaxQuoteForNonTicketingRevenueRQ createServiceTaxQuoteCriteriaForNonTicketingRevenueRequest(
			ServiceTaxQuoteCriteriaForNonTktDTO criteria, UserPrincipal userPrincipal) throws ModuleException {

		ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ = new ServiceTaxQuoteForNonTicketingRevenueRQ();
		serviceTaxQuoteForNonTktRevRQ.setPaxState(criteria.getPaxState());
		serviceTaxQuoteForNonTktRevRQ.setPaxCountryCode(criteria.getPaxCountryCode());
		serviceTaxQuoteForNonTktRevRQ.setModifyingAgentStation(userPrincipal.getAgentStation());
		if (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_GDS) {
			serviceTaxQuoteForNonTktRevRQ.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		} else {
			serviceTaxQuoteForNonTktRevRQ.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil
					.getSalesChannelName(userPrincipal.getSalesChannel())));
		}

		if (criteria.getPaxWiseCharges() != null && criteria.getPaxWiseCharges().values() != null
				&& !criteria.getPaxWiseCharges().values().isEmpty()) {
			List<PaxCnxModPenChargeDTO> paxCnxModPenCharges = new ArrayList<PaxCnxModPenChargeDTO>();
			for (Entry<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxCnxModPenChargeEntry : criteria.getPaxWiseCharges()
					.entrySet()) {
				PaxCnxModPenChargeDTO paxCnxModPenCharge = new PaxCnxModPenChargeDTO();
				paxCnxModPenCharge.setPaxSequence(paxCnxModPenChargeEntry.getKey());
				paxCnxModPenCharge.setTotalAmountIncludingServiceTax(paxCnxModPenChargeEntry.getValue()
						.isTotalAmountIncludingServiceTax());
				List<CnxModPenChargeDTO> cnxModPenChargeDTOs = new ArrayList<CnxModPenChargeDTO>();
				for (LCCClientExternalChgDTO charge : paxCnxModPenChargeEntry.getValue().getCharges()) {
					CnxModPenChargeDTO cnxModPenChargeDTO = new CnxModPenChargeDTO();
					cnxModPenChargeDTO.setAmount(charge.getAmount());
					cnxModPenChargeDTO.setChargeCode(charge.getCode());
					//cnxModPenChargeDTO.setChargeGroupCode(charge.getCode());
					cnxModPenChargeDTOs.add(cnxModPenChargeDTO);
					paxCnxModPenCharge.setCnxModPenChargeDTOs(cnxModPenChargeDTOs);
				}
				paxCnxModPenCharges.add(paxCnxModPenCharge);
			}
			serviceTaxQuoteForNonTktRevRQ.setPaxCnxModPenCharges(paxCnxModPenCharges);
		}
		return serviceTaxQuoteForNonTktRevRQ;
	}
	
	public static ServiceTaxQuoteForTicketingRevenueResTO adaptServiceTaxQuoteForTicketingRevenueRS(
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS) {
		ServiceTaxQuoteForTicketingRevenueResTO serviceTaxResTO = new ServiceTaxQuoteForTicketingRevenueResTO();
		serviceTaxResTO.setServiceTaxDepositeCountryCode(serviceTaxRS.getServiceTaxDepositeCountryCode());
		serviceTaxResTO.setServiceTaxDepositeStateCode(serviceTaxRS.getServiceTaxDepositeStateCode());
		if (serviceTaxRS.getPaxTypeWiseServiceTaxes() != null && !serviceTaxRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
			Map<String, List<ServiceTaxTO>> paxTypeWiseServiceTaxes = new HashMap<String, List<ServiceTaxTO>>();
			for (Entry<String, List<ServiceTaxDTO>> paxTypeWiseServiceTaxesEntry : serviceTaxRS.getPaxTypeWiseServiceTaxes()
					.entrySet()) {
				paxTypeWiseServiceTaxes.put(paxTypeWiseServiceTaxesEntry.getKey(),
						adaptServiceTaxList(paxTypeWiseServiceTaxesEntry.getValue()));
			}
			serviceTaxResTO.setPaxTypeWiseServiceTaxes(paxTypeWiseServiceTaxes);
		}
		if (serviceTaxRS.getPaxWiseServiceTaxes() != null && !serviceTaxRS.getPaxWiseServiceTaxes().isEmpty()) {
			Map<Integer, List<ServiceTaxTO>> paxWiseServiceTaxes = new HashMap<Integer, List<ServiceTaxTO>>();
			for (Entry<Integer, List<ServiceTaxDTO>> paxWiseServiceTaxesEntry : serviceTaxRS.getPaxWiseServiceTaxes().entrySet()) {
				paxWiseServiceTaxes.put(paxWiseServiceTaxesEntry.getKey(),
						adaptServiceTaxList(paxWiseServiceTaxesEntry.getValue()));
			}
			serviceTaxResTO.setPaxWiseServiceTaxes(paxWiseServiceTaxes);
		}
		return serviceTaxResTO;
	}

	public static List<ServiceTaxTO> adaptServiceTaxList(List<ServiceTaxDTO> serviceTaxDTOs) {
		List<ServiceTaxTO> serviceTaxTOs = new ArrayList<ServiceTaxTO>();
		for (ServiceTaxDTO serviceTaxDTO : serviceTaxDTOs) {
			ServiceTaxTO serviceTaxTO = new ServiceTaxTO();
			serviceTaxTO.setAmount(serviceTaxDTO.getAmount());
			serviceTaxTO.setCarrierCode(serviceTaxDTO.getCarrierCode());
			serviceTaxTO.setChargeCode(serviceTaxDTO.getChargeCode());
			serviceTaxTO.setChargeGroupCode(serviceTaxDTO.getChargeGroupCode());
			serviceTaxTO.setFlightRefNumber(serviceTaxDTO.getFlightRefNumber());
			serviceTaxTO.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
			serviceTaxTO.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
			serviceTaxTO.setChargeRateId(serviceTaxDTO.getChargeRateId());
			serviceTaxTOs.add(serviceTaxTO);
		}
		return serviceTaxTOs;
	}
	
	public static ServiceTaxExtCharges addPaxServiceTaxesAsExtCharges(List<ServiceTaxDTO> serviceTaxs,
			String taxDepositCountryCode, String taxDepositStateCode) {
		ServiceTaxExtCharges extChg = null;
		if (serviceTaxs != null && !serviceTaxs.isEmpty()) {
			extChg = new ServiceTaxExtCharges();
			for (ServiceTaxDTO serviceTax : serviceTaxs) {
				extChg.addServiceTaxes(externalChargeAdaptor(serviceTax, taxDepositCountryCode, taxDepositStateCode));
			}
			extChg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
		}
		return extChg;
	}

	public static ServiceTaxExtChgDTO externalChargeAdaptor(ServiceTaxDTO serviceTax, String taxDepositCountryCode,
			String taxDepositStateCode) {
		ServiceTaxExtChgDTO chg = new ServiceTaxExtChgDTO();
		chg.setAmount(serviceTax.getAmount());
		chg.setChargeCode(serviceTax.getChargeCode());
		chg.setChgRateId(serviceTax.getChargeRateId());
		chg.setChgGrpCode(PricingConstants.ChargeGroups.TAX);
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setTaxableAmount(serviceTax.getTaxableAmount());
		chg.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
		chg.setTicketingRevenue(true);
		chg.setTaxDepositCountryCode(taxDepositCountryCode);
		chg.setTaxDepositStateCode(taxDepositStateCode);
		chg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
		return chg;
	}
	
	public static ServiceTaxExtCharges addLccServiceTaxesAsExtCharges(List<LCCClientExternalChgDTO> serviceTaxs) {
		ServiceTaxExtCharges extChg = null;
		if (serviceTaxs != null && !serviceTaxs.isEmpty()) {
			extChg = new ServiceTaxExtCharges();
			for (LCCClientExternalChgDTO serviceTax : serviceTaxs) {
				extChg.addServiceTaxes(lccExternalChargeAdaptor(serviceTax));
			}
			extChg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
		}
		return extChg;
	}

	public static ServiceTaxExtChgDTO lccExternalChargeAdaptor(LCCClientExternalChgDTO serviceTax) {
		ServiceTaxExtChgDTO chg = new ServiceTaxExtChgDTO();
		chg.setAmount(serviceTax.getAmount());
		chg.setChargeCode(serviceTax.getCode());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setTaxableAmount(serviceTax.getTaxableAmount());
		chg.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
		chg.setTicketingRevenue(true);
		chg.setTaxDepositCountryCode(serviceTax.getTaxDepositCountryCode());
		chg.setTaxDepositStateCode(serviceTax.getTaxDepositStateCode());
		chg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
		return chg;
	}
	
	public static LCCClientExternalChgDTO externalChargeAdaptor(ServiceTaxDTO serviceTax, boolean isTicketingRevenue,
			String taxDepositCountryCode, String taxDepositStateCode) {

		LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
		chg.setAmount(serviceTax.getAmount());
		chg.setExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
		chg.setCode(serviceTax.getChargeCode());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(serviceTax.getFlightRefNumber()));
		chg.setTaxableAmount(serviceTax.getTaxableAmount());
		chg.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
		chg.setTicketingRevenue(isTicketingRevenue);
		chg.setTaxDepositCountryCode(taxDepositCountryCode);
		chg.setTaxDepositStateCode(taxDepositStateCode);

		return chg;
	}
	
	public static ServiceTaxQuoteForTicketingRevenueRQ createServiceTaxQuoteForAddInfantRequest(Collection<OndFareDTO> collFares,
			Reservation reservation, UserPrincipal userPrincipal, Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, Integer adultPaxSeq) throws ModuleException {

		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = new ServiceTaxQuoteForTicketingRevenueRQ();
		List<Collection<OndFareDTO>> jouneyONDWiseFareONDs = new ArrayList<Collection<OndFareDTO>>();
		for (OndFareDTO ondFareDTO : collFares) {
			while (jouneyONDWiseFareONDs.size() <= ondFareDTO.getOndSequence()) {
				jouneyONDWiseFareONDs.add(new ArrayList<OndFareDTO>());
			}
			jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
		}
		IPaxCountAssembler paxAssm = new PaxCountAssembler(0, 0, 1);
		FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(jouneyONDWiseFareONDs, paxAssm, null, null, null);
		serviceTaxQuoteForTktRevRQ.setPaxTypeWiseCharges(ServiceTaxConverterUtil.composePaxTypeWiseCharges(fareTypeTO));

		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
		List<String> flightRefNumbers = new ArrayList<String>();
		Map<Integer, SimplifiedFlightSegmentDTO> fltSegIdSegments = new HashMap<>();
		Map<Integer, ReservationSegment> resSegRetuenMap = new HashMap<Integer, ReservationSegment>();
		for (ReservationSegment resSeg : reservation.getSegments()) {
			resSegRetuenMap.put(resSeg.getFlightSegId(), resSeg);
		}
		for (OndFareDTO ondFare : collFares) {
			Map<Integer, SegmentSeatDistsDTO> fltSegIdSD = new HashMap<>();
			for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
				fltSegIdSD.put(segSeatDist.getFlightSegId(), segSeatDist);
			}
			for (FlightSegmentDTO flightSegmentTO : ondFare.getSegmentsMap().values()) {
				SegmentSeatDistsDTO segSeatDist = fltSegIdSD.get(flightSegmentTO.getSegmentId());
				if (segSeatDist != null) {
					SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
					simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
					simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
					simplifiedFlightSegmentDTO.setOndSequence(ondFare.getOndSequence());
					simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
					simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
					simplifiedFlightSegmentDTO.setLogicalCabinClassCode(segSeatDist.getLogicalCabinClass());
					simplifiedFlightSegmentDTO.setReturnFlag("Y".equals(resSegRetuenMap.get(flightSegmentTO.getSegmentId()).getReturnFlag()) ? true : false);
					simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getCarrierCode());
					simplifiedFlightSegmentDTO.setSegmentSequence(resSegRetuenMap.get(flightSegmentTO.getSegmentId()).getSegmentSeq());
					simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
					fltSegIdSegments.put(flightSegmentTO.getSegmentId(), simplifiedFlightSegmentDTO);
					flightRefNumbers.add(simplifiedFlightSegmentDTO.getFlightRefNumber());
				}
			}
		}
		serviceTaxQuoteForTktRevRQ.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
		
		if (externalChargesMap != null) {
			for (Entry<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMapEntry : externalChargesMap.entrySet()) {
				if (EXTERNAL_CHARGES.CREDIT_CARD.equals(externalChargesMapEntry.getKey())
						&& externalChargesMapEntry.getValue() != null) {
					ExternalChgDTO externalChargeDTO = externalChargesMapEntry.getValue();
					List<ExternalChgDTO> chgDTOs = segmentWiseDistribution(flightRefNumbers,
							externalChargeDTO.getAmount(), EXTERNAL_CHARGES.CREDIT_CARD, externalChargeDTO.getChargeCode());
					Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();
					List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
					SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
					paxDTO.setPaxSequence(adultPaxSeq);
					paxDTO.setPaxType(PaxTypeTO.INFANT);
					for (ExternalChgDTO externalChgDTO : chgDTOs) {
						if (externalChgDTO.getAmount() != null && externalChgDTO.getAmount().doubleValue() > 0) {
							SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
							chargeDTO.setAmount(externalChgDTO.getAmount());
							chargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
							chargeDTO.setChargeCode(externalChgDTO.getChargeCode());
							SimplifiedFlightSegmentDTO flightSegmentTO = fltSegIdSegments.get(externalChgDTO.getFlightSegId());
							chargeDTO.setCarrierCode(flightSegmentTO.getOperatingAirline());
							chargeDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
							chargeDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
							chargeDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());
							chargesList.add(chargeDTO);
						}
					}
					if (!chargesList.isEmpty()) {
						paxWiseCharges.put(paxDTO, chargesList);
						serviceTaxQuoteForTktRevRQ.setPaxWiseExternalCharges(paxWiseCharges);
					}
				}
			}
		}
		serviceTaxQuoteForTktRevRQ.setPaxState(reservation.getContactInfo().getState());
		serviceTaxQuoteForTktRevRQ.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
		serviceTaxQuoteForTktRevRQ.setPaxTaxRegistered(reservation.getContactInfo().getTaxRegNo() != null ? true : false);
		if (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_GDS) {
			serviceTaxQuoteForTktRevRQ.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		} else {
			serviceTaxQuoteForTktRevRQ.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil
					.getSalesChannelName(userPrincipal.getSalesChannel())));
		}
		return serviceTaxQuoteForTktRevRQ;
	}
	
	private static List<ExternalChgDTO> segmentWiseDistribution(List<String> flightRefNumbers, BigDecimal amount,
			EXTERNAL_CHARGES externalChargesEnum, String chargeCode) {
		List<ExternalChgDTO> chgDTOs = new ArrayList<>();
		if (flightRefNumbers != null && !flightRefNumbers.isEmpty()) {
			BigDecimal[] paxSegmentWiseCardFee = AccelAeroCalculator.roundAndSplit(amount, flightRefNumbers.size());
			int p = 0;
			for (String flightRefNumber : flightRefNumbers) {
				if (paxSegmentWiseCardFee[p].doubleValue() > 0) {
					ExternalChgDTO chg = new ExternalChgDTO();
					chg.setAmount(paxSegmentWiseCardFee[p]);
					chg.setExternalChargesEnum(externalChargesEnum);
					chg.setChargeCode(chargeCode);
					chg.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
					chgDTOs.add(chg);
				}
				p++;
			}
		}
		return chgDTOs;
	}
	
	public static List<FlightSegmentTO> adaptFlightSegments(Collection<LCCClientReservationSegment> lccSegments) {
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		if (lccSegments != null && !lccSegments.isEmpty()) {
			for (LCCClientReservationSegment lccSegment : lccSegments) {
				FlightSegmentTO flightSegmentDTO = new FlightSegmentTO();
				flightSegmentDTO.setDepartureDateTimeZulu(lccSegment.getDepartureDateZulu());
				flightSegmentDTO.setArrivalDateTimeZulu(lccSegment.getArrivalDateZulu());
				flightSegmentDTO.setLogicalCabinClassCode(lccSegment.getCabinClassCode());
				flightSegmentDTO.setFlightRefNumber(lccSegment.getFlightSegmentRefNumber());
				flightSegmentDTO.setOndSequence(lccSegment.getJourneySequence());
				flightSegmentDTO.setSegmentCode(lccSegment.getSegmentCode());
				flightSegmentDTO.setSegmentSequence(lccSegment.getSegmentSeq());
				flightSegmentDTO.setReturnFlag("Y".equals(lccSegment.getReturnFlag()) ? true : false);
				flightSegmentDTO.setOperatingAirline(lccSegment.getCarrierCode());
				flightSegments.add(flightSegmentDTO);
			}
		}
		return flightSegments;
	}
	
	public static List<FlightSegmentTO> adaptFlightSegmentsFromResSegmentDTOs(Collection<FlightSegmentDTO> segments) {
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		int i = 1;
		if (segments != null && !segments.isEmpty()) {
			for (FlightSegmentDTO segment : segments) {
				FlightSegmentTO flightSegmentDTO = new FlightSegmentTO();
				flightSegmentDTO.setDepartureDateTimeZulu(segment.getDepartureDateTimeZulu());
				flightSegmentDTO.setArrivalDateTimeZulu(segment.getArrivalDateTimeZulu());
				flightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(segment));
				flightSegmentDTO.setOndSequence(1);
				flightSegmentDTO.setSegmentCode(segment.getSegmentCode());
				flightSegmentDTO.setSegmentSequence(i);
				flightSegmentDTO.setReturnFlag(false);
				flightSegmentDTO.setOperatingAirline(segment.getCarrierCode());
				flightSegments.add(flightSegmentDTO);
			}
		}
		return flightSegments;
	}

}
