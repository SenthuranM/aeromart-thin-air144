package com.isa.thinair.airproxy.api.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class CustomizedItineraryUtil {

	private static String ACTION_EMAIL = "EMAIL";
	private static String ACTION_PRINT = "PRINT";

	public static boolean isHeadOfficeAgent(HttpServletRequest request) {
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		boolean headOfficeAgent = false;
		if (userPrincipal != null) {
			String agentTypeCode = userPrincipal.getAgentTypeCode();
			if (agentTypeCode.equals(AppSysParamsUtil.getCarrierAgent())) {
				headOfficeAgent = true;
			}
		}
		return headOfficeAgent;
	}

	public static List<CommonItineraryParamDTO> itineraryCompose(HttpServletRequest request, String action,
			CommonItineraryParamDTO commonItineraryParam, TrackInfoDTO trackInfoDTO, LCCClientPnrModesDTO pnrModesDTO)
			throws ModuleException {

		List<CommonItineraryParamDTO> itineraryDto = new ArrayList<CommonItineraryParamDTO>();
		CommonItineraryParamDTO tempCommonItineraryParamDTO = new CommonItineraryParamDTO();
		tempCommonItineraryParamDTO.setItineraryLanguage(commonItineraryParam.getItineraryLanguage());
		tempCommonItineraryParamDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
		tempCommonItineraryParamDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
		tempCommonItineraryParamDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
		tempCommonItineraryParamDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
		tempCommonItineraryParamDTO.setStation(commonItineraryParam.getStation());
		tempCommonItineraryParamDTO.setAppIndicator(commonItineraryParam.getAppIndicator());
		tempCommonItineraryParamDTO.setAirportMap(commonItineraryParam.getAirportMap());
		tempCommonItineraryParamDTO.setIncludePaxContactDetails(commonItineraryParam.isIncludePaxContactDetails());
		tempCommonItineraryParamDTO.setIncludeStationContactDetails(commonItineraryParam.isIncludeStationContactDetails());

		if (isHeadOfficeAgent(request)) { // HEAD OFFICE AGENT
			// For Head office agents, Depending on there privileges they should allow to add following details as they
			// wont(For any booking. Booking accsess can be controlled from other privileges).
			tempCommonItineraryParamDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
			tempCommonItineraryParamDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
			tempCommonItineraryParamDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
			tempCommonItineraryParamDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
			itineraryDto.add(tempCommonItineraryParamDTO);
		} else if (commonItineraryParam.getAppIndicator().equals(ApplicationEngine.XBE)) { // Agent
			if (action.equals(ACTION_PRINT)) { // PRINT 2Copies of Itenary one for agent and one for customer
				// Hide all payment info from itinerary
				tempCommonItineraryParamDTO.setIncludePaxFinancials(false);
				tempCommonItineraryParamDTO.setIncludePaymentDetails(false);
				tempCommonItineraryParamDTO.setIncludeTicketCharges(false);
				tempCommonItineraryParamDTO.setIncludeTermsAndConditions(true);
				itineraryDto.add(tempCommonItineraryParamDTO);
				// itinerary += getItinerary(tempCommonItineraryParamDTO, pnrModesDTO, trackInfoDTO, individualPax,
				// selectedPax, action);

				// Display all payment info from itinerary
				CommonItineraryParamDTO tempCommonItineraryXBEParamDTO = new CommonItineraryParamDTO();
				tempCommonItineraryXBEParamDTO.setItineraryLanguage(commonItineraryParam.getItineraryLanguage());
				tempCommonItineraryXBEParamDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
				tempCommonItineraryXBEParamDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
				tempCommonItineraryXBEParamDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
				tempCommonItineraryXBEParamDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
				tempCommonItineraryXBEParamDTO.setStation(commonItineraryParam.getStation());
				tempCommonItineraryXBEParamDTO.setAppIndicator(commonItineraryParam.getAppIndicator());
				tempCommonItineraryXBEParamDTO.setAirportMap(commonItineraryParam.getAirportMap());
				tempCommonItineraryXBEParamDTO.setIncludePaxFinancials(true);
				tempCommonItineraryXBEParamDTO.setIncludePaymentDetails(true);
				tempCommonItineraryXBEParamDTO.setIncludeTicketCharges(true);
				tempCommonItineraryXBEParamDTO.setIncludeTermsAndConditions(false);
				itineraryDto.add(tempCommonItineraryXBEParamDTO);
				// itinerary += getItinerary(tempCommonItineraryParamDTO, pnrModesDTO, trackInfoDTO, individualPax,
				// selectedPax, action);
			} else if (action.equals(ACTION_EMAIL)) {
				if (isAgentCreatedBooking(pnrModesDTO, trackInfoDTO, request)) { // ORIGNAL AGENT Created Booking
					tempCommonItineraryParamDTO.setIncludePaxFinancials(commonItineraryParam.isIncludePaxFinancials());
					tempCommonItineraryParamDTO.setIncludePaymentDetails(commonItineraryParam.isIncludePaymentDetails());
					tempCommonItineraryParamDTO.setIncludeTicketCharges(commonItineraryParam.isIncludeTicketCharges());
					tempCommonItineraryParamDTO.setIncludeTermsAndConditions(commonItineraryParam.isIncludeTermsAndConditions());
					itineraryDto.add(tempCommonItineraryParamDTO);
				} else {
					// Hide all payment info from itinerary
					tempCommonItineraryParamDTO.setIncludePaxFinancials(false);
					tempCommonItineraryParamDTO.setIncludePaymentDetails(false);
					tempCommonItineraryParamDTO.setIncludeTicketCharges(false);
					tempCommonItineraryParamDTO.setIncludeTermsAndConditions(true);
					itineraryDto.add(tempCommonItineraryParamDTO);
				}

			}
		} else if (SalesChannelsUtil.isAppEngineWebOrMobile(commonItineraryParam.getAppIndicator())) {// CHECK FOR IBE
			tempCommonItineraryParamDTO.setIncludePaxFinancials(true);
			tempCommonItineraryParamDTO.setIncludePaymentDetails(true);
			tempCommonItineraryParamDTO.setIncludeTicketCharges(true);
			tempCommonItineraryParamDTO.setIncludeTermsAndConditions(true);
			itineraryDto.add(tempCommonItineraryParamDTO);
		}

		// return tempCommonItineraryParamDTO;\
		return itineraryDto;
	}

	private static boolean isAgentCreatedBooking(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO,
			HttpServletRequest request) throws ModuleException {
		boolean agentCreatedBooking = false;
		Reservation reservation = ReservationModuleUtils.getReservationBD().getReservation(
				FindReservationUtil.getLitePnrModesDTO(pnrModesDTO), trackInfoDTO);
		String originAgentCode = reservation.getAdminInfo().getOriginAgentCode();

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (originAgentCode != null && userPrincipal != null) {
			String agentCode = userPrincipal.getAgentCode();
			if (originAgentCode.equals(agentCode)) {
				agentCreatedBooking = true;
			}
		}

		return agentCreatedBooking;
	}

}
