/**
 * 
 */
package com.isa.thinair.airproxy.api.model.reservation.commons;

/**
 * Contain Common Enum Constants across modules
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ProxyConstants {
	/**
	 * Enum to identify the systems 
	 * Interline only - INT 
	 * AccelAero only - AA 
	 * Both system - ALL 
	 * Conditional (INT if no AA results) - COND 
	 * No System - NONE
	 */
	public enum SYSTEM {
		AA, INT, ALL, COND, NONE;

		/**
		 * 
		 * @param strEnum
		 * @param def
		 *            , Default Enum
		 * @return SYSTEM
		 */
		public static SYSTEM getEnum(String strEnum, SYSTEM def) {
			for (SYSTEM enm : SYSTEM.values()) {
				if (enm.toString().equals(strEnum)) {
					return enm;
				}
			}

			return (def != null) ? def : SYSTEM.NONE;
		}

		/**
		 * Return the SYSTEM matched for given string if string is null SYSTEM.NONE will be returned
		 * 
		 * @param strEnum
		 * @return SYSTEM
		 * @see #getEnum(String, SYSTEM)
		 */
		public static SYSTEM getEnum(String strEnum) {
			return getEnum(strEnum, null);
		}
	};

	/**
	 * Enum to identify the payment options
	 * 
	 * TOTAL - total payment in loyalty credit PART - partial payment in loyalty credit NONE - none of the payment in
	 * loyalty credit
	 * 
	 */
	public enum LoyaltyPaymentOption {
		TOTAL, PART, NONE;

		/**
		 * Return the LoyaltyPaymentOption matched for the given string with ignore case
		 * 
		 * @param strEnum
		 * @return LoyaltyPaymentOption
		 */
		public static LoyaltyPaymentOption getEnum(String strEnum) {
			for (LoyaltyPaymentOption enm : LoyaltyPaymentOption.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return LoyaltyPaymentOption.NONE;
		}
	}

	/**
	 * Enum to identify the voucher payment options
	 * 
	 * TOTAL - total payment in loyalty credit PART - partial payment in loyalty credit NONE - none of the payment in
	 * loyalty credit
	 * 
	 */
	public enum VoucherPaymentOption {
		TOTAL, PART, NONE;

		/**
		 * Return the LoyaltyPaymentOption matched for the given string with ignore case
		 * 
		 * @param strEnum
		 * @return LoyaltyPaymentOption
		 */
		public static VoucherPaymentOption getEnum(String strEnum) {
			for (VoucherPaymentOption enm : VoucherPaymentOption.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return VoucherPaymentOption.NONE;
		}
	}

	/**
	 * 
	 * Booking Type
	 * 
	 */
	public enum BookingType {
		ONHOLD, NORMAL, NONE;

		public static BookingType getEnum(String strEnum) {
			for (BookingType enm : BookingType.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return BookingType.NONE;
		}
	}

}