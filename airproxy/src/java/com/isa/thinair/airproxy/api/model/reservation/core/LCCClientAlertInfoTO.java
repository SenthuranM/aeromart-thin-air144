package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.List;

public class LCCClientAlertInfoTO implements Serializable {

	private static final long serialVersionUID = 2261842108506314729L;

	public static interface alertType {
		public static final String FLEXI = "FLEXI";
		public static final String GROUND_SERVICE = "GS";
		public static final String TRANSFER_ALERT = "TRN";
		public static final String OPEN_RETURN = "OPRT";
	}

	private String flightSegmantRefNumber;

	private List<LCCClientAlertTO> alertTO;

	private boolean alertActionOnXBE;

	private String alertCategory;

	/**
	 * @return the flightSegmantRefNumber
	 */
	public String getFlightSegmantRefNumber() {
		return flightSegmantRefNumber;
	}

	/**
	 * @param flightSegmantRefNumber
	 *            the flightSegmantRefNumber to set
	 */
	public void setFlightSegmantRefNumber(String flightSegmantRefNumber) {
		this.flightSegmantRefNumber = flightSegmantRefNumber;
	}

	/**
	 * @return the alertTO
	 */
	public List<LCCClientAlertTO> getAlertTO() {
		return alertTO;
	}

	/**
	 * @param alertTO
	 *            the alertTO to set
	 */
	public void setAlertTO(List<LCCClientAlertTO> alertTO) {
		this.alertTO = alertTO;
	}

	/**
	 * @return the alertCategory
	 */
	public String getAlertCategory() {
		return alertCategory;
	}

	/**
	 * @param alertCategory
	 *            the alertCategory to set
	 */
	public void setAlertCategory(String alertCategory) {
		this.alertCategory = alertCategory;
	}

	/**
	 * @return the isAlertShowOnXBE
	 */
	public boolean isAlertActionOnXBE() {
		return alertActionOnXBE;
	}

	/**
	 * @param alertActionOnXBE
	 *            the isAlertShowOnXBE
	 */
	public void setAlertActionOnXBE(boolean alertActionOnXBE) {
		this.alertActionOnXBE = alertActionOnXBE;
	}

}
