package com.isa.thinair.airproxy.api.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.CodeShareFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.utils.converters.AncillarySegComparator;
import com.isa.thinair.airproxy.core.utils.transformer.PaymentConvertUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.model.IResSegment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypesDesc;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class ReservationAssemblerConvertUtil {

	/**
	 * Given the airproxy common reservation assembler and ondFareDTO Return the air reservation IReservation assembler
	 * 
	 * @param selectedFlightDTO
	 * @param commonReservationAssembler
	 * @return
	 * @throws ModuleException
	 * @see #fillContactInformation(IReservation, CommonReservationContactInfo)
	 * @see #populatePaxInformation(IReservation, Set)
	 * @see #populateInsurance(IReservation, LCCClientReservationInsurance, Set, CommonReservationContactInfo)
	 * @see #populateOndSegments(IReservation, Collection)
	 */
	public static IReservation getReservationAssembler(Collection<OndFareDTO> ondFareDTOs,
			CommonReservationAssembler commonReservationAssembler) throws ModuleException {
		IReservation iReservation = null;
		iReservation = new ReservationAssembler(ondFareDTOs, commonReservationAssembler.getLccreservation().getPNR());
		iReservation.setBookingCategory(commonReservationAssembler.getBookingCategory());
		iReservation.setOriginCountryOfCall(commonReservationAssembler.getOriginCountryOfCall());
		iReservation.setItineraryFareMask(commonReservationAssembler.getItineraryFareMask());
		iReservation.setBookingType(commonReservationAssembler.getBookingType());
		iReservation.setSkipDuplicateCheck(commonReservationAssembler.isSkipDuplicationCheck());
		iReservation.setUsedOtherCarrierCredit(commonReservationAssembler.isUseOtherCarrierPaxCredit());
		iReservation.setAllowFlightSearchAfterCutOffTime(commonReservationAssembler.isAllowFlightSearchAfterCutOffTime());
		// set charges applicablity
		iReservation.setChargesApplicability(commonReservationAssembler.getChargesApplicability());
		// We need to save the reservation as a
		if (commonReservationAssembler.isUseOtherCarrierPaxCredit()) {
			iReservation.setOriginatorPnr(commonReservationAssembler.getLccreservation().getPNR());
		}

		iReservation.setPnrZuluReleaseTimeStamp(commonReservationAssembler.getPnrZuluReleaseTimeStamp());
		fillContactInformation(iReservation, commonReservationAssembler.getLccreservation().getContactInfo());
		populatePaxInformation(iReservation, commonReservationAssembler.getLccreservation().getPassengers());
		iReservation.setLastCurrencyCode(commonReservationAssembler.getSelectedCurrency());

		if (commonReservationAssembler.getLccreservation().getReservationInsurances() != null
				&& !commonReservationAssembler.getLccreservation().getReservationInsurances().isEmpty()) {
			populateInsurance(iReservation, commonReservationAssembler.getLccreservation().getReservationInsurances(),
					commonReservationAssembler.getLccreservation().getPassengers(), commonReservationAssembler
							.getLccreservation().getContactInfo(),
					SalesChannelsUtil.isAppIndicatorWebOrMobile(commonReservationAssembler.getAppIndicator()) ? 4 : 3);
		}
		Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
		Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();
		CommonUtil.mapStationsBySegment(commonReservationAssembler.getLccreservation().getSegments(), groundStationBySegmentMap,
				groundSementFlightByAirFlightMap);

		// injectDiscountedFaresAmts(ondFareDTOs, commonReservationAssembler.getFareDiscountPercentage());
		if (commonReservationAssembler.getDiscountedFareDetails() != null) {
			iReservation.setFareDiscount(commonReservationAssembler.getDiscountedFareDetails().getFarePercentage(),
					commonReservationAssembler.getDiscountedFareDetails().getNotes());
			iReservation.setFareDiscountInfo(commonReservationAssembler.getDiscountedFareDetails());

			if (AppSysParamsUtil.enableFareDiscountCodes()) {
				iReservation.setFareDiscountCode(commonReservationAssembler.getDiscountedFareDetails().getCode());
			}
		}
		iReservation.setApplicableAgentCommissions(commonReservationAssembler.getAgentCommissions());
		Map<Integer, Boolean> flightSegmentWaitListStatusMap = populateFlightSegmentWaitListStatusMap(commonReservationAssembler
				.getFlightSegmentTOs());

		Map<Integer, String> flightSegmentBagggageOndGroupMap = populateFlightSegmentBaggageOndGroupMap(commonReservationAssembler
				.getLccreservation().getSegments());
		// setting the initial segment sequence and ond grouping to one for own bookings
		populateOndSegments(iReservation, ondFareDTOs, groundStationBySegmentMap, groundSementFlightByAirFlightMap, 1, 1, 1, 1,
				flightSegmentBagggageOndGroupMap, flightSegmentWaitListStatusMap, null);
		populateTempPaymentEntries(iReservation, commonReservationAssembler.getTemporaryPaymentMap());

		// if (AppSysParamsUtil.isTicketValidityEnabled()) {
		// iReservation.setTicketValidity(ondFareDTOs);
		// }
		iReservation.setGroupBookingRequestID(commonReservationAssembler.getGroupBookingRequestID());
		iReservation.setLoyaltyPaymentInfo(commonReservationAssembler.getCarrierLoyaltyPaymentInfo(AppSysParamsUtil
				.getDefaultCarrierCode()));
		iReservation.setPayByVoucherInfo(commonReservationAssembler.getPayByVoucherInfo());
		iReservation.setCreateResNPayInitInDifferentFlows(commonReservationAssembler.isCreateResNPayInitInDifferentFlows());

		if(commonReservationAssembler.getReasonForAllowBlPaxRes()!=null && !commonReservationAssembler.getReasonForAllowBlPaxRes().equals("")){
			iReservation.setReasonForAllowBlPaxRes(commonReservationAssembler.getReasonForAllowBlPaxRes());
		}
		iReservation.setPayByVoucherInfo(commonReservationAssembler.getPayByVoucherInfo());

		return iReservation;
	}

	public static IReservation getReservationAssembler(CommonReservationAssembler commonReservationAssembler)
			throws ModuleException {
		IReservation iReservation = null;
		iReservation = new ReservationAssembler(null, null);
		iReservation.setChargesApplicability(commonReservationAssembler.getChargesApplicability());
		fillContactInformation(iReservation, commonReservationAssembler.getLccreservation().getContactInfo());
		populatePaxInformation(iReservation, commonReservationAssembler.getLccreservation().getPassengers());
		iReservation.setLastCurrencyCode(commonReservationAssembler.getSelectedCurrency());
		populateTempPaymentEntries(iReservation, commonReservationAssembler.getTemporaryPaymentMap());
		return iReservation;
	}

	/**
	 * Populate temporary payment entries from common reservation assembler to the iReservation
	 * 
	 * @param iReservation
	 * @param temporaryPaymentMap
	 */
	private static void populateTempPaymentEntries(IReservation iReservation,
			Map<Integer, CommonCreditCardPaymentInfo> temporaryPaymentMap) {
		if (temporaryPaymentMap != null && temporaryPaymentMap.size() > 0) {
			Map<Integer, CardPaymentInfo> cardPayInfoMap = new HashMap<Integer, CardPaymentInfo>();
			Set<Integer> tmpKeySet = temporaryPaymentMap.keySet();
			for (Integer key : tmpKeySet) {
				CommonCreditCardPaymentInfo commonInfo = temporaryPaymentMap.get(key);
				CardPaymentInfo cardPayInfo = new CardPaymentInfo();
				cardPayInfo.setAddress(commonInfo.getAddress());
				cardPayInfo.setAppIndicator(commonInfo.getAppIndicator());
				cardPayInfo.setAuthorizationId(commonInfo.getAuthorizationId());
				cardPayInfo.setEDate(commonInfo.geteDate());
				cardPayInfo.setIpgIdentificationParamsDTO(commonInfo.getIpgIdentificationParamsDTO());
				cardPayInfo.setName(commonInfo.getName());
				cardPayInfo.setNo(commonInfo.getNo());
				cardPayInfo.setNoFirstDigits(commonInfo.getNoFirstDigits());
				cardPayInfo.setNoLastDigits(commonInfo.getNoLastDigits());
				cardPayInfo.setPayCurrencyDTO(commonInfo.getPayCurrencyDTO());
				cardPayInfo.setPaymentBrokerRefNo(commonInfo.getPaymentBrokerId());
				cardPayInfo.setTemporyPaymentId(commonInfo.getTemporyPaymentId());
				cardPayInfo.setTnxMode(commonInfo.getTnxMode());
				cardPayInfo.setType(commonInfo.getType());
				cardPayInfo.setTotalAmount(commonInfo.getTotalAmount());
				cardPayInfo.setSecurityCode(commonInfo.getSecurityCode());
				cardPayInfo.setPnr(commonInfo.getGroupPNR());
				cardPayInfo.setSuccess(commonInfo.isPaymentSuccess());

				cardPayInfoMap.put(key, cardPayInfo);
			}
			iReservation.addTemporyPaymentEntries(cardPayInfoMap);
		}
	}

	private static Map<Integer, Boolean> populateFlightSegmentWaitListStatusMap(Collection<FlightSegmentTO> flightSegmentTOs) {
		Map<Integer, Boolean> filghtSegmentStatusMap = new HashMap<Integer, Boolean>();
		if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				filghtSegmentStatusMap.put(flightSegmentTO.getFlightSegId(), flightSegmentTO.isWaitListed());
			}
		}
		return filghtSegmentStatusMap;

	}

	private static Map<Integer, String> populateFlightSegmentBaggageOndGroupMap(Set<LCCClientReservationSegment> segments) {
		Map<Integer, String> filghtSegmentBaggageOndGroupMap = new HashMap<Integer, String>();
		if (segments != null && !segments.isEmpty()) {
			for (LCCClientReservationSegment segment : segments) {
				if (segment.getBaggageONDGroupId() != null && !segment.getBaggageONDGroupId().isEmpty()) {
					filghtSegmentBaggageOndGroupMap.put(new Integer(segment.getFlightSegmentRefNumber()),
							segment.getBaggageONDGroupId());
				}
			}
		}
		return filghtSegmentBaggageOndGroupMap;

	}

	/**
	 * 
	 * @param isResSegment
	 * @param nextSegSeq
	 * @param nextOndGroup
	 * @param nextJourneyGroup
	 * @param selectedFlightDTO
	 * @throws ModuleException
	 */
	public static void populateOndSegments(IResSegment isResSegment, Collection<OndFareDTO> ondFareDTOs,
			Map<Integer, String> groundStationBySegmentMap, Map<Integer, Integer> linkedGroundSegmentFlightRefMap,
			Integer nextSegSeq, Integer nextOndGroup, Integer nextReturnGroup, Integer nextJourneyGroup,
			Map<Integer, String> flightSegIdWiseBaggageOndGrpId, Map<Integer, Boolean> flightSegmentWaitListStatusMap,
			List<CodeShareFlightDTO> codeShareFlightDTOs) throws ModuleException {

		Map<Integer, Collection<OndFareDTO>> subJourneyGroupMap = new HashMap<Integer, Collection<OndFareDTO>>();
		if (ondFareDTOs != null && ondFareDTOs.size() > 0) {
			boolean isSubJourneyEnabled = AppSysParamsUtil.isSubJourneyDetectionEnabled();
			Map<Integer, Integer> actualSegmentSeqMap = null;
			List<FlightSegmentDTO> flightSegmentColl = new ArrayList<FlightSegmentDTO>();
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				Collection<OndFareDTO> ondFareDTOColl = new ArrayList<OndFareDTO>();
				if (!ondFareDTO.isFlown()) {
					flightSegmentColl.addAll(ondFareDTO.getSegmentsMap().values());
				}

				if (ondFareDTO.getSubJourneyGroup() > 0) {

					if (subJourneyGroupMap.get(ondFareDTO.getSubJourneyGroup()) != null) {
						ondFareDTOColl = subJourneyGroupMap.get(ondFareDTO.getSubJourneyGroup());
					}

					ondFareDTO.setOndSequence(ondFareDTOColl.size());

					ondFareDTOColl.add(ondFareDTO);
					subJourneyGroupMap.put(ondFareDTO.getSubJourneyGroup(), ondFareDTOColl);

				} else {

					if (subJourneyGroupMap.get(0) != null) {
						ondFareDTOColl = subJourneyGroupMap.get(0);
					}
					ondFareDTO.setOndSequence(ondFareDTOColl.size());

					ondFareDTOColl.add(ondFareDTO);
					subJourneyGroupMap.put(0, ondFareDTOColl);
				}

			}

			if (isSubJourneyEnabled && flightSegmentColl != null && !flightSegmentColl.isEmpty()) {
				// when sub-journey is enabled re-arrange OND sequence based on departure date
				Collections.sort(flightSegmentColl);
				actualSegmentSeqMap = new HashMap<Integer, Integer>();
				int tempSegmentSeq = nextSegSeq;
				for (FlightSegmentDTO flightSegmentDTO : flightSegmentColl) {
					actualSegmentSeqMap.put(flightSegmentDTO.getSegmentId(), tempSegmentSeq);
					tempSegmentSeq++;
				}
			}

			if (subJourneyGroupMap != null && subJourneyGroupMap.size() > 0) {
				int segmentSeq = nextSegSeq;
				int intFareOndGrp = nextOndGroup;
				int intFareReturnOndGrp = nextReturnGroup;
				int journeyGroup = nextJourneyGroup;

				for (Integer key : subJourneyGroupMap.keySet()) {
					Collection<OndFareDTO> ondFareDTOSubColl = subJourneyGroupMap.get(key);

					if (ondFareDTOSubColl != null && ondFareDTOSubColl.size() > 0) {

						List<Collection<ReservationSegmentTO>> ondFlightSegments = getOndWiseFlights(ondFareDTOSubColl,
								codeShareFlightDTOs);
						boolean isReturnBooking = OndFareUtil.isReturnBooking(ondFareDTOSubColl);

						Date openRTConfirmExpiry = OndFareUtil.getOpenReturnPeriods(ondFareDTOSubColl).getConfirmBefore();

						int count = 1;

						boolean openRTSegExists = false;

						// FIXME a small hack until the full fix is done for OPENRT
						if (ondFlightSegments.size() == 2 && openRTConfirmExpiry != null) {
							openRTSegExists = true;
						}

						for (Collection<ReservationSegmentTO> perOndFlights : ondFlightSegments) {
							int lastFareTypeId = -1;
							for (ReservationSegmentTO segmentInfo : perOndFlights) {
								Date openRTConfirmBefore = null;
								boolean isReturnOND = false;

								if (openRTSegExists && count == 2) {
									openRTConfirmBefore = openRTConfirmExpiry;
								}
								boolean waitListed = (flightSegmentWaitListStatusMap != null && flightSegmentWaitListStatusMap
										.get(segmentInfo.getFlightSegId()) != null) ? flightSegmentWaitListStatusMap
										.get(segmentInfo.getFlightSegId()) : false;
								if (isReturnBooking && count == 2) {
									isReturnOND = true;
								}
								int fareTypeId = segmentInfo.getOndFareType();
								if (lastFareTypeId != -1 && lastFareTypeId != FareTypes.PER_FLIGHT_FARE
										&& fareTypeId != lastFareTypeId) {
									intFareOndGrp++;
									intFareReturnOndGrp++;
								}

								if (isSubJourneyEnabled) {
									segmentSeq = getActualSegmentSequence(actualSegmentSeqMap, segmentInfo.getFlightSegId(),
											segmentSeq);
								}

								isResSegment.addOndSegment((journeyGroup), segmentSeq++, segmentInfo.getFlightSegId(),
										intFareOndGrp, intFareReturnOndGrp, openRTConfirmBefore, null,
										getBaggageOndGrpId(flightSegIdWiseBaggageOndGrpId, segmentInfo.getFlightSegId()),
										waitListed, isReturnOND, segmentInfo.getSelectedBundledFarePeriodId(),
										segmentInfo.getCodeShareFlightNumber(), segmentInfo.getCodeShareBookingClass(),
										segmentInfo.getCsOcCarrierCode(), segmentInfo.getCsOcFlightNumber());

								if (groundStationBySegmentMap != null && linkedGroundSegmentFlightRefMap != null) {
									isResSegment.addGroundStationDataToPNRSegment(groundStationBySegmentMap,
											linkedGroundSegmentFlightRefMap);
								}

								if (fareTypeId == FareTypes.PER_FLIGHT_FARE) {
									intFareOndGrp++;
									intFareReturnOndGrp++;
								}
								lastFareTypeId = fareTypeId;
							}
							if (lastFareTypeId != FareTypes.PER_FLIGHT_FARE) {
								intFareOndGrp++;
							}
							if (lastFareTypeId == FareTypes.OND_FARE || ondFlightSegments.size() == count) {
								intFareReturnOndGrp++;
							}
							journeyGroup++;
							count++;
						}

					}

				}
			}

		}

	}

	private static Integer getBaggageOndGrpId(Map<Integer, String> flightSegIdWiseBaggageOndGrpId, int flightSegId) {
		if (flightSegIdWiseBaggageOndGrpId != null && flightSegIdWiseBaggageOndGrpId.size() > 0) {
			String baggageOndGrpId = BeanUtils.nullHandler(flightSegIdWiseBaggageOndGrpId.get(flightSegId));
			if (baggageOndGrpId.length() > 0) {
				return new Integer(baggageOndGrpId);
			}
		}
		return null;
	}

	private static void populateInsurance(IReservation iReservation, List<LCCClientReservationInsurance> reservationInsurances,
			Set<LCCClientReservationPax> passengers, CommonReservationContactInfo contactInfo, int channelCode) {

		String phoneNumber = "";

		if (contactInfo.getMobileNo() != null && !contactInfo.getMobileNo().equals("") && !contactInfo.getMobileNo().equals("--")) {
			phoneNumber = contactInfo.getMobileNo();
		} else {
			phoneNumber = contactInfo.getPhoneNo();
		}

		if (reservationInsurances != null && !reservationInsurances.isEmpty()) {
			Iterator<LCCClientReservationInsurance> insIterator = reservationInsurances.iterator();

			while (insIterator.hasNext()) {

				IInsuranceRequest insuranceRequest = new InsuranceRequestAssembler();
				LCCClientReservationInsurance reservationInsurance = insIterator.next();

				insuranceRequest.setInsuranceId(new Integer(reservationInsurance.getInsuranceQuoteRefNumber()));
				insuranceRequest.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedTotalPremium());
				insuranceRequest.setInsuranceType(reservationInsurance.getInsuranceType());
				insuranceRequest.setAllDomastic(reservationInsurance.isAllDomastic());
				insuranceRequest.setPolicyCode(reservationInsurance.getPolicyCode());
				insuranceRequest.setSsrFeeCode(reservationInsurance.getSsrFeeCode());
				insuranceRequest.setPlanCode(reservationInsurance.getPlanCode());

				InsureSegment insSegment = new InsureSegment();
				insSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
				insSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
				insSegment.setFromAirportCode(reservationInsurance.getOrigin());
				insSegment.setToAirportCode(reservationInsurance.getDestination());
				insSegment.setRoundTrip(reservationInsurance.getInsuredJourneyDTO().isRoundTrip());
				insSegment.setSalesChanelCode(channelCode);
				insuranceRequest.addFlightSegment(insSegment);

				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(reservationInsurance.getQuotedTotalPremium(),
						passengers.size());
				int i = 0;
				for (LCCClientReservationPax pax : passengers) {
					if (!pax.getPaxType().equals(PaxTypeTO.INFANT)
							|| (pax.getPaxType().equals(PaxTypeTO.INFANT) && AppSysParamsUtil.allowAddInsurnaceForInfants())) {
						InsurePassenger insurePassenger = new InsurePassenger();

						insurePassenger.setFirstName(pax.getFirstName());
						insurePassenger.setLastName(pax.getLastName());
						insurePassenger.setTitle(pax.getTitle());
						insurePassenger.setDateOfBirth(pax.getDateOfBirth());

						insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insurePassenger.setCity(contactInfo.getCity());
						insurePassenger.setEmail(contactInfo.getEmail());
						insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insurePassenger.setHomePhoneNumber(phoneNumber);

						insuranceRequest.addPassenger(insurePassenger);
						if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
							insuranceRequest.addInsuranceCharge(pax.getPaxSequence(), BigDecimal.ZERO);
						} else {
							insuranceRequest.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
						}
					}
				}

				iReservation.addInsurance(insuranceRequest);
			}

		}

	}

	/**
	 * 
	 * @param iReservation
	 * @param passengers
	 * @throws ModuleException
	 * @see {@link #getReservationAssembler(Collection, CommonReservationAssembler)}
	 */
	private static void populatePaxInformation(IReservation iReservation, Set<LCCClientReservationPax> passengers)
			throws ModuleException {
		// int parentSeqId = 1;
		Map extExternalChgDTOMap = getExternalChargesMap(passengers, iReservation.getChargesApplicability());
		for (LCCClientReservationPax pax : passengers) {
			List<LCCSelectedSegmentAncillaryDTO> paxAncillaries = pax.getSelectedAncillaries();
			SegmentSSRAssembler segmentSSRs = paxAncillaries != null
					? composeSSRAssembler(paxAncillaries)
					: new SegmentSSRAssembler();

			PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
			String arrivalIntlFltNo = "";
			Date intlFltArrivalDate = null;
			String departureIntlFltNo = "";
			Date intlFltDepartureDate = null;
			String pnrPaxGroupId = "";
			if (pax.getLccClientAdditionPax() != null) {
				paxAdditionalInfoDTO.setPassportNo(pax.getLccClientAdditionPax().getPassportNo());
				paxAdditionalInfoDTO.setPassportExpiry(pax.getLccClientAdditionPax().getPassportExpiry());
				paxAdditionalInfoDTO.setPassportIssuedCntry(pax.getLccClientAdditionPax().getPassportIssuedCntry());
				paxAdditionalInfoDTO.setEmployeeId(pax.getLccClientAdditionPax().getEmployeeId());
				paxAdditionalInfoDTO.setDateOfJoin(pax.getLccClientAdditionPax().getDateOfJoin());
				paxAdditionalInfoDTO.setIdCategory(pax.getLccClientAdditionPax().getIdCategory());
				paxAdditionalInfoDTO.setPlaceOfBirth(pax.getLccClientAdditionPax().getPlaceOfBirth());
				paxAdditionalInfoDTO.setTravelDocumentType(pax.getLccClientAdditionPax().getTravelDocumentType());
				paxAdditionalInfoDTO.setVisaDocNumber(pax.getLccClientAdditionPax().getVisaDocNumber());
				paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(pax.getLccClientAdditionPax().getVisaDocPlaceOfIssue());
				paxAdditionalInfoDTO.setVisaDocIssueDate(pax.getLccClientAdditionPax().getVisaDocIssueDate());
				paxAdditionalInfoDTO.setVisaApplicableCountry(pax.getLccClientAdditionPax().getVisaApplicableCountry());
				paxAdditionalInfoDTO.setFfid(pax.getLccClientAdditionPax().getFfid());

				// TODO set these values
				arrivalIntlFltNo = pax.getLccClientAdditionPax().getArrivalIntlFltNo();
				intlFltArrivalDate = pax.getLccClientAdditionPax().getIntlFltArrivalDate();
				departureIntlFltNo = pax.getLccClientAdditionPax().getDepartureIntlFltNo();
				intlFltDepartureDate = pax.getLccClientAdditionPax().getIntlFltDepartureDate();
				pnrPaxGroupId = pax.getLccClientAdditionPax().getPnrPaxGroupId();

				paxAdditionalInfoDTO.setNationalIDNo(pax.getLccClientAdditionPax().getNationalIDNo());
			}

			if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
				if (pax.getAttachedPaxId() != null && pax.getAttachedPaxId() > 0) {
					if (pax.getLccClientPaxNameTranslations() != null) {
						iReservation.addParent(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
								.getNationalityCode(), pax.getPaxSequence(), pax.getAttachedPaxId(), paxAdditionalInfoDTO, pax
								.getPaxCategory(), PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(),
								extExternalChgDTOMap), segmentSSRs, pax.getLccClientPaxNameTranslations().getFirstNameOl(), pax
								.getLccClientPaxNameTranslations().getLastNameOl(), pax.getLccClientPaxNameTranslations()
								.getTitleOl(), pax.getLccClientPaxNameTranslations().getLanguageCode(),
								CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
								intlFltDepartureDate, pnrPaxGroupId);
					} else {
						iReservation.addParent(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
								.getNationalityCode(), pax.getPaxSequence(), pax.getAttachedPaxId(), paxAdditionalInfoDTO, pax
								.getPaxCategory(), PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(),
								extExternalChgDTOMap), segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER,
								arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
					}
				} else {
					if (pax.getLccClientPaxNameTranslations() != null) {
						iReservation
								.addSingle(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
										.getNationalityCode(), pax.getPaxSequence(), paxAdditionalInfoDTO, pax.getPaxCategory(),
										PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(),
												extExternalChgDTOMap), segmentSSRs, pax.getLccClientPaxNameTranslations()
												.getFirstNameOl(), pax.getLccClientPaxNameTranslations().getLastNameOl(), pax
												.getLccClientPaxNameTranslations().getLanguageCode(), pax
												.getLccClientPaxNameTranslations().getTitleOl(),
										CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate,
										departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
					} else {
						iReservation
								.addSingle(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
										.getNationalityCode(), pax.getPaxSequence(), paxAdditionalInfoDTO, pax.getPaxCategory(),
										PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(),
												extExternalChgDTOMap), segmentSSRs, null, null, null, null,
										CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate,
										departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
					}
				}
			} else if (pax.getPaxType().equals(PaxTypeTO.CHILD)) {
				if (pax.getLccClientPaxNameTranslations() != null) {
					iReservation.addChild(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
							.getNationalityCode(), pax.getPaxSequence(), paxAdditionalInfoDTO, pax.getPaxCategory(),
							PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(), extExternalChgDTOMap),
							segmentSSRs, pax.getLccClientPaxNameTranslations().getFirstNameOl(), pax
									.getLccClientPaxNameTranslations().getLastNameOl(), pax.getLccClientPaxNameTranslations()
									.getTitleOl(), pax.getLccClientPaxNameTranslations().getLanguageCode(),
							CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
							intlFltDepartureDate, pnrPaxGroupId);
				} else {
					iReservation.addChild(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(),
							pax.getNationalityCode(), pax.getPaxSequence(), paxAdditionalInfoDTO, pax.getPaxCategory(),
							PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(), extExternalChgDTOMap),
							segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo,
							intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
				}
			} else if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				if (pax.getLccClientPaxNameTranslations() != null) {
					iReservation.addInfant(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(), pax
							.getNationalityCode(), pax.getPaxSequence(), pax.getAttachedPaxId(), paxAdditionalInfoDTO, pax
							.getPaxCategory(), PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(), extExternalChgDTOMap), segmentSSRs, pax.getLccClientPaxNameTranslations()
							.getFirstNameOl(), pax.getLccClientPaxNameTranslations().getLastNameOl(), pax
							.getLccClientPaxNameTranslations().getLanguageCode(), null, CommonsConstants.INDIVIDUAL_MEMBER,
							arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo, intlFltDepartureDate, pnrPaxGroupId);
				} else {
					iReservation.addInfant(pax.getFirstName(), pax.getLastName(), pax.getTitle(), pax.getDateOfBirth(),
							pax.getNationalityCode(), pax.getPaxSequence(), pax.getAttachedPaxId(), paxAdditionalInfoDTO,
							pax.getPaxCategory(), PaymentConvertUtil.populatePerPaxPayment(pax.getLccClientPaymentAssembler(), extExternalChgDTOMap), segmentSSRs, null, null, null, null,
							CommonsConstants.INDIVIDUAL_MEMBER, arrivalIntlFltNo, intlFltArrivalDate, departureIntlFltNo,
							intlFltDepartureDate, pnrPaxGroupId);
				}
			}
		}

	}

	/**
	 * This method use to compose In-flight & airport service to SegmentSSRAssembler
	 * 
	 * @param selectedAncillaries
	 * @return
	 * @throws ModuleException
	 */
	private static SegmentSSRAssembler composeSSRAssembler(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries)
			throws ModuleException {
		SegmentSSRAssembler ssrAsm = new SegmentSSRAssembler();
		int segSequence = 1;
		// sort the segments to get the proper order
		Collections.sort(selectedAncillaries, new AncillarySegComparator());
		for (LCCSelectedSegmentAncillaryDTO segAnci : selectedAncillaries) {
			int correctSegSeq = segSequence;
			if (segAnci.getFlightSegmentTO() != null && segAnci.getFlightSegmentTO().getSegmentSequence() != 0) {
				correctSegSeq = segAnci.getFlightSegmentTO().getSegmentSequence();
			}

			/*
			 * Add In-flight services to Assembler
			 */
			List<LCCSpecialServiceRequestDTO> ssrList = segAnci.getSpecialServiceRequestDTOs();
			if (ssrList != null) {
				for (LCCSpecialServiceRequestDTO ssr : ssrList) {
					SSRInfoDTO ssrInfo = SSRUtil.getSSRInfo(SSRUtil.getSSRId(ssr.getSsrCode()));
					ssrAsm.addPaxSegmentSSR(correctSegSeq, ssrInfo.getSSRId(), ssr.getText(), null, ssr.getCharge(),
							ssrInfo.getSSRCode(), ssrInfo.getSSRDescription(), null, null,
							ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, null, null, null, null);
				}
			}

			/*
			 * Add Airport services to Assembler
			 */
			List<LCCAirportServiceDTO> airportServiceList = segAnci.getAirportServiceDTOs();
			if (airportServiceList != null) {
				for (LCCAirportServiceDTO lccAirportServiceDTO : airportServiceList) {
					SSRInfoDTO ssrInfo = SSRUtil.getSSRInfo(SSRUtil.getSSRId(lccAirportServiceDTO.getSsrCode()));
					ssrAsm.addPaxSegmentSSR(correctSegSeq, ssrInfo.getSSRId(), lccAirportServiceDTO.getServiceNumber(), null,
							lccAirportServiceDTO.getServiceCharge(), ssrInfo.getSSRCode(), ssrInfo.getSSRDescription(), null,
							lccAirportServiceDTO.getAirportCode(), lccAirportServiceDTO.getApplyOn(), null, null, null, null);
				}
			}

			segSequence++;
		}
		return ssrAsm;
	}

	/**
	 * 
	 * @param passengers
	 * @return
	 * @throws ModuleException
	 */
	private static Map getExternalChargesMap(Set<LCCClientReservationPax> passengers, int chargesApplicablity)
			throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		for (LCCClientReservationPax pax : passengers) {
			if (pax.getLccClientPaymentAssembler() != null) {
				Collection<LCCClientExternalChgDTO> extChgColl = pax.getLccClientPaymentAssembler().getPerPaxExternalCharges();
				for (LCCClientExternalChgDTO extChg : extChgColl) {
					colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
				}
			}
		}

		Map extExternalChgDTOMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
				chargesApplicablity);
		return extExternalChgDTOMap;
	}

	/**
	 * Fill the contact information
	 * 
	 * @param reservation
	 * @param reservationContactInfo
	 * @see #getReservationAssembler(Collection, CommonReservationAssembler)
	 */
	private static void fillContactInformation(IReservation reservation, CommonReservationContactInfo reservationContactInfo) {
		ReservationContactInfo contactInfo = new ReservationContactInfo();
		contactInfo.setTitle(reservationContactInfo.getTitle());
		contactInfo.setFirstName(reservationContactInfo.getFirstName());
		contactInfo.setLastName(reservationContactInfo.getLastName());
		contactInfo.setStreetAddress1(reservationContactInfo.getStreetAddress1());
		contactInfo.setStreetAddress2(reservationContactInfo.getStreetAddress2());
		contactInfo.setState(reservationContactInfo.getState());
		contactInfo.setCity(reservationContactInfo.getCity());
		contactInfo.setCountryCode(reservationContactInfo.getCountryCode());
		contactInfo.setZipCode(reservationContactInfo.getZipCode());
		contactInfo.setMobileNo(reservationContactInfo.getMobileNo());
		contactInfo.setPhoneNo(reservationContactInfo.getPhoneNo());
		contactInfo.setFax(reservationContactInfo.getFax());
		contactInfo.setEmail(reservationContactInfo.getEmail());
		contactInfo.setNationalityCode(reservationContactInfo.getNationalityCode() != null ? reservationContactInfo
				.getNationalityCode().toString() : null);
		contactInfo.setCustomerId(reservationContactInfo.getCustomerId());
		String prefLang = (reservationContactInfo.getPreferredLanguage() == null)
				? Locale.ENGLISH.toString()
				: reservationContactInfo.getPreferredLanguage();
		contactInfo.setPreferredLanguage(prefLang);
		contactInfo.setEmgnFirstName(reservationContactInfo.getEmgnFirstName());
		contactInfo.setEmgnLastName(reservationContactInfo.getEmgnLastName());
		contactInfo.setEmgnTitle(reservationContactInfo.getEmgnTitle());
		contactInfo.setEmgnEmail(reservationContactInfo.getEmgnEmail());
		contactInfo.setEmgnPhoneNo(reservationContactInfo.getEmgnPhoneNo());
		contactInfo.setTaxRegNo(reservationContactInfo.getTaxRegNo());
		contactInfo.setSendPromoEmail(reservationContactInfo.getSendPromoEmail());
		reservation.addContactInfo(reservationContactInfo.getUserNotes(), contactInfo, reservationContactInfo.getUserNoteType());
	}

	public static List<Collection<ReservationSegmentTO>> getOndWiseFlights(Collection<OndFareDTO> ondFares,
			List<CodeShareFlightDTO> codeShareFlightDTOs) {
		List<Collection<ReservationSegmentTO>> ondFlightSegments = new ArrayList<Collection<ReservationSegmentTO>>();

		int ondSequence = 0;
		for (OndFareDTO ondFareDTO : ondFares) {
			for (FlightSegmentDTO flightSegmentDTO : ondFareDTO.getSegmentsMap().values()) {

				Collection<ReservationSegmentTO> flights = getOndResSegments(ondFlightSegments, ondFareDTO.getOndSequence());
				String codeShareFlightNumber = null;
				String codeShareBC = null;
				if (codeShareFlightDTOs != null) {
					for (CodeShareFlightDTO codeShareFlightDTO : codeShareFlightDTOs) {
						if (codeShareFlightDTO.getFlightNumber().equals(flightSegmentDTO.getFlightNumber())
								&& CalendarUtil.isEqualStartTimeOfDate(codeShareFlightDTO.getDepartureDate(),
										flightSegmentDTO.getDepatureDate())) {
							codeShareFlightNumber = codeShareFlightDTO.getCodeShareFlightNumber();
							codeShareBC = codeShareFlightDTO.getCodeShareBookingClass();
						}
					}
				}
				ReservationSegmentTO segment = new ReservationSegmentTO(flightSegmentDTO.getSegmentId(),
						ondFareDTO.getFareType(), flightSegmentDTO.getDepartureDateTimeZulu(),
						ondFareDTO.getSelectedBundledFarePeriodId(), codeShareFlightNumber, codeShareBC,
						flightSegmentDTO.getCsOcCarrierCode(), flightSegmentDTO.getCsOcFlightNumber());
				flights.add(segment);
			}
			ondSequence++;
		}
		return ondFlightSegments;
	}

	public static List<LCCClientAlertTO> generatePromotionInfoAlerts(LCCClientReservation reservation) {
		LCCPromotionInfoTO lccPromotionInfoTO = reservation.getLccPromotionInfoTO();
		List<LCCClientAlertTO> alerts = null;
		if (lccPromotionInfoTO != null) {
			alerts = new ArrayList<LCCClientAlertTO>();
			LCCClientAlertTO alert = new LCCClientAlertTO();
			alert.setAlertId(0);
			alert.setContent(lccPromotionInfoTO.getPromoType() + " Promotion");
			alerts.add(alert);

			if (PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA.equals(lccPromotionInfoTO.getPromoType())) {
				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				alert.setContent("Free Pax Count - AD: " + lccPromotionInfoTO.getAppliedAdults() + " CH: "
						+ lccPromotionInfoTO.getAppliedChilds() + " IN: " + lccPromotionInfoTO.getAppliedInfants());
				alerts.add(alert);
			} else if (PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA.equals(lccPromotionInfoTO.getPromoType())
					|| PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(lccPromotionInfoTO.getPromoType())
					|| PromotionCriteriaTypesDesc.SYS_GEN_PROMO.equals(lccPromotionInfoTO.getPromoType())) {

				if (PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA.equals(lccPromotionInfoTO.getPromoType())) {
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					String anciContent = "For ";
					for (Iterator<String> iterator = lccPromotionInfoTO.getAppliedAncis().iterator(); iterator.hasNext();) {
						String anciCode = iterator.next();
						anciContent += PromotionCriteriaConstants.ANCILLARIES.getName(anciCode);

						if (iterator.hasNext()) {
							anciContent += ", ";
						}

					}
					alert.setContent(anciContent);
					alerts.add(alert);
				}

				if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(lccPromotionInfoTO.getDiscountType())
						|| PromotionCriteriaTypesDesc.SYS_GEN_PROMO.equals(lccPromotionInfoTO.getPromoType())) {
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					alert.setContent("Up to " + AppSysParamsUtil.getBaseCurrency() + " "
							+ AccelAeroCalculator.formatAsDecimal(new BigDecimal(lccPromotionInfoTO.getDiscountValue()))
							+ " Discount on " + lccPromotionInfoTO.getDiscountApplyTo());
					alerts.add(alert);
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					BigDecimal discount;
					if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(lccPromotionInfoTO.getDiscountAs())) {
						discount = lccPromotionInfoTO.getCreditDiscountAmount();
					} else {
						discount = reservation.getTotalDiscount();
					}
					alert.setContent("Total Discount of " + AppSysParamsUtil.getBaseCurrency() + " "
							+ AccelAeroCalculator.formatAsDecimal(discount));
					alerts.add(alert);
				} else if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(lccPromotionInfoTO.getDiscountType())) {
					alert = new LCCClientAlertTO();
					alert.setAlertId(0);
					if (lccPromotionInfoTO.getDiscountApplyTo() == null) {
						alert.setContent(lccPromotionInfoTO.getDiscountValue() + "% Discount");
					} else {
						alert.setContent(lccPromotionInfoTO.getDiscountValue() + "% Discount on "
								+ lccPromotionInfoTO.getDiscountApplyTo());
					}
					alerts.add(alert);
				}
			}

			if (!lccPromotionInfoTO.isSystemGenerated()) {
				String optNot = "";
				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				if (!lccPromotionInfoTO.isModificationAllowed()) {
					optNot = "not";
				}
				alert.setContent("Modification " + optNot + " allowed");
				alerts.add(alert);

				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				if (!lccPromotionInfoTO.isCancellationAllowed()) {
					optNot = "not";
				} else {
					optNot = "";
				}
				alert.setContent("Cancellation " + optNot + " allowed");
				alerts.add(alert);

				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				if (!lccPromotionInfoTO.isSplitAllowed()) {
					optNot = "not";
				} else {
					optNot = "";
				}
				alert.setContent("Split reservation " + optNot + " allowed");
				alerts.add(alert);

				alert = new LCCClientAlertTO();
				alert.setAlertId(0);
				if (!lccPromotionInfoTO.isRemovePaxAllowed()) {
					optNot = "not";
				} else {
					optNot = "";
				}
				alert.setContent("Remove pax " + optNot + " allowed");
				alerts.add(alert);
			}

		}
		return alerts;
	}

	// FIXME proper refactor
	// public static Object[] extractNewResSegmentsInfo(Collection<OndFareDTO> ondFares, Integer nextSegSeq, Integer
	// nextOndGroup) {
	//
	// int fareGroup = nextOndGroup.intValue();
	//
	// List<Collection<ReservationSegmentTO>> ondFlightSegments = new ArrayList<Collection<ReservationSegmentTO>>();
	//
	// // Collection<ReservationSegmentTO> obFlightSegments = new LinkedList<ReservationSegmentTO>();
	// // Collection<ReservationSegmentTO> ibFlightSegments = null;
	// Map<Integer, Integer> fareGroupFareTypeMap = new HashMap<Integer, Integer>();
	//
	// List<ReservationSegmentTO> allSegments = new LinkedList<ReservationSegmentTO>();
	// for (OndFareDTO ondFareDTO : ondFares) {
	// for (FlightSegmentDTO flightSegmentDTO : ondFareDTO.getSegmentsMap().values()) {
	//
	// Collection<ReservationSegmentTO> flights = getOndResSegments(ondFlightSegments, ondFareDTO.getOndSequence());
	// // if (!ondFareDTO.isInBoundOnd()) {
	// // obFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, null));
	// // } else {
	// // if (ibFlightSegments == null) {
	// // ibFlightSegments = new LinkedList<ReservationSegmentTO>();
	// // }
	// // ibFlightSegments.add(new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup, null));
	// // }
	// ReservationSegmentTO segment = new ReservationSegmentTO(flightSegmentDTO.getSegmentId(), fareGroup,
	// flightSegmentDTO.getDepartureDateTimeZulu());
	// flights.add(segment);
	// allSegments.add(segment);
	// }
	// fareGroupFareTypeMap.put(fareGroup, ondFareDTO.getTotalJourneyFareType());
	// ++fareGroup;
	// }
	// // set segment sequence
	// SegmentUtil.setSegmentSequnceNumbers(ondFares, allSegments, null, nextSegSeq.intValue());
	// return new Object[] { ondFlightSegments, fareGroupFareTypeMap };
	// }

	private static Collection<ReservationSegmentTO> getOndResSegments(List<Collection<ReservationSegmentTO>> ondFlightSegments,
			int ondSequence) {
		if (ondFlightSegments.size() >= ondSequence) {
			for (int i = ondFlightSegments.size(); i <= ondSequence; i++) {
				ondFlightSegments.add(i, new ArrayList<ReservationSegmentTO>());
			}
		}
		return ondFlightSegments.get(ondSequence);
	}

	private static List<String> getInsReferanceList(String jsonInsuranceRef) {
		String insRefarances = jsonInsuranceRef.replaceAll("[^0-9,]", "");

		return Arrays.asList(insRefarances.split(","));
	}
	
	private static Integer getActualSegmentSequence(Map<Integer, Integer> actualSegmentSeqMap, int flightSegId, int segmentSeq) {

		if (actualSegmentSeqMap != null && !actualSegmentSeqMap.isEmpty() && actualSegmentSeqMap.get(flightSegId) != null) {
			return actualSegmentSeqMap.get(flightSegId);
		}

		return segmentSeq;
	}

}
