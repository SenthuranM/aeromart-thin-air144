package com.isa.thinair.airproxy.api.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.BaggageRatesDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryQuotation;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageResponseDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public interface AirproxyAncillaryBD {

	public static final String SERVICE_NAME = "AirproxyReservationService";

	public boolean blockSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, SYSTEM system,
			BasicTrackInfo trackInfo) throws ModuleException;

	public AnciAvailabilityRS getAncillaryAvailability(LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityDTO,
			BasicTrackInfo trackInfo) throws ModuleException;

	public LCCMealResponceDTO getAvailableMeals(List<LCCMealRequestDTO> lccMealRequestDTOs, BasicTrackInfo trackInfo,
			Map<String, Set<String>> fltSegWiseMeals, List<BundledFareDTO> bundledFareDTOs, SYSTEM system,
			String transactionIdentifier, ApplicationEngine appEngine, String pnr) throws ModuleException;

	public LCCMealResponceDTO getAvailableMealsWithTranslations(List<LCCMealRequestDTO> lccMealRequestDTOs,
			String transactionIdentifier, List<BundledFareDTO> bundledFareDTOs, SYSTEM system, String selectedLanguage,
			BasicTrackInfo trackInfo, ApplicationEngine appEngine, String pnr) throws ModuleException;

	public List<LCCInsuranceQuotationDTO> getInsuranceQuotation(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCInsuredPassengerDTO> insuredPassengerDTOs, LCCInsuredJourneyDTO insuredJourneyDTO,
			String transactionIdentifier, SYSTEM system, String pnr, ClientCommonInfoDTO clientInfoDto,
			BigDecimal totalTicketPriceWithoutExternal, BasicTrackInfo trackInfo, ApplicationEngine appEngine, LCCInsuredContactInfoDTO contactInfo) throws ModuleException;

	public LCCSeatMapDTO getSeatMap(LCCSeatMapDTO lccSeatMapDTO, String selectedLocale, BasicTrackInfo trackInfo,
			ApplicationEngine appEngine) throws ModuleException;

	public List<LCCFlightSegmentSSRDTO> getSpecialServiceRequests(List<FlightSegmentTO> flightSegmentTOs,
			String transactionIdentifier, SYSTEM system, String selectedLanguage, BasicTrackInfo trackInfo,
			Map<String, Set<String>> fltRefWiseSelectedSSR, boolean isModifyAnci, boolean isGdsPnr,
			boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) throws ModuleException;

	public boolean releaseSeats(List<LCCSegmentSeatDTO> blockSeatDTOs, String transactionIdentifier, SYSTEM system,
			BasicTrackInfo trackInfo) throws ModuleException;

	public LCCBaggageResponseDTO getAvailableBaggages(List<LCCBaggageRequestDTO> lccBaggageRequestDTOs,
			String transactionIdentifier, SYSTEM system, String selectedLanguage, boolean isModifyOperation,
			boolean hasFinalCutOverPrivilege, boolean isRequote, ApplicationEngine appEngine,
			LCCReservationBaggageSummaryTo baggageSummaryTo, List<BundledFareDTO> bundledFareDTOs, String pnr, BasicTrackInfo trackInfo) throws ModuleException;

	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAvailableAiportServices(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, int ssrCategory, SYSTEM system,
			String selectedLanguage, String transactionIdentifier, BasicTrackInfo trackInfo, Map<String, Integer> serviceCount,
			boolean isModifyAnci, List<BundledFareDTO> bundledFareDTOs, String pnr) throws ModuleException;
	
	public Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> getAvailableAiportTransfers(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, SYSTEM system,
			Map<String, Integer> serviceCount, boolean isModifyAnci) throws ModuleException;

	// This is for automatic checkin ancillary
	public Map<AirportServiceKeyTO, LCCFlightSegmentAutomaticCheckinDTO> getAvailableAutomaticCheckins(
			List<FlightSegmentTO> flightSegmentTOs, AppIndicatorEnum appIndicator, SYSTEM system,
			Map<String, Integer> serviceCount, boolean isModifyAnci) throws ModuleException;
	
	public List<LCCMealDTO> getMealsForPreference(String selectedLanguage) throws ModuleException;

	public LCCAncillaryQuotation getSelectedAncillaryDetails(LCCAncillaryQuotation selectedAncillary, Map<String, BundledFareDTO> segmentBundledFareMap, UserPrincipal userPrincipal, TrackInfoDTO trackInfo)
			throws ModuleException;

	public BaggageRatesDTO getBaggageRates(TrackInfoDTO trackInfoDTO, BaseAvailRS flightPriceRS,
			String cabinClass, String logicalCabinClass, String selectedSystem, ApplicationEngine appEngine,
			String locale) throws ModuleException;
}

