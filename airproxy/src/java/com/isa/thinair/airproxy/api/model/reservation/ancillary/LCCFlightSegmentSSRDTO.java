package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCFlightSegmentSSRDTO implements Serializable, Comparable<LCCFlightSegmentSSRDTO> {

	private static final long serialVersionUID = 1L;
	private FlightSegmentTO flightSegmentTO;
	private List<LCCSpecialServiceRequestDTO> specialServiceRequest;

	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	public List<LCCSpecialServiceRequestDTO> getSpecialServiceRequest() {
		if (specialServiceRequest == null) {
			specialServiceRequest = new ArrayList<LCCSpecialServiceRequestDTO>();
		}
		return specialServiceRequest;
	}

	public void setSpecialServiceRequest(List<LCCSpecialServiceRequestDTO> specialServiceRequest) {
		this.specialServiceRequest = specialServiceRequest;
	}
	
	@Override
	public int compareTo(LCCFlightSegmentSSRDTO o) {
		if (this.flightSegmentTO.getDepartureDateTimeZulu() == null || o.getFlightSegmentTO().getDepartureDateTimeZulu() == null) {
			return (this.flightSegmentTO.getDepartureDateTimeZulu() == null) ? -1 : 1;
		}
		if (this.flightSegmentTO.getDepartureDateTimeZulu() == null && o.getFlightSegmentTO().getDepartureDateTimeZulu() == null) {
			return 0;
		}
		return this.flightSegmentTO.getDepartureDateTimeZulu().compareTo(o.getFlightSegmentTO().getDepartureDateTimeZulu());
	}
}
