package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LCCAirRowGroupDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String rowGroupId;
	private List<LCCAirColumnGroupDTO> airColumnGroups;

	public String getRowGroupId() {
		return rowGroupId;
	}

	public void setRowGroupId(String rowGroupId) {
		this.rowGroupId = rowGroupId;
	}

	public List<LCCAirColumnGroupDTO> getAirColumnGroups() {
		if (airColumnGroups == null)
			airColumnGroups = new ArrayList<LCCAirColumnGroupDTO>();
		return airColumnGroups;
	}

	public void setAirColumnGroups(List<LCCAirColumnGroupDTO> airColumnGroups) {
		this.airColumnGroups = airColumnGroups;
	}
}
