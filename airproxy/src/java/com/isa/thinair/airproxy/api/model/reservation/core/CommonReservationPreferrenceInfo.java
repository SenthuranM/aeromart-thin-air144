/*
 * =============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;

/**
 * To keep track of Reservation Preferences information
 * 
 * @author Astria Alva
 */
public class CommonReservationPreferrenceInfo implements Serializable {

	private static final long serialVersionUID = 5394037585604901577L;
	private String preferredLanguage;

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

}
