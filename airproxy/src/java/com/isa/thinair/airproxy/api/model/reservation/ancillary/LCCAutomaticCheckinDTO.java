package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author aravinth.r
 */
public class LCCAutomaticCheckinDTO implements Serializable {

	private static final long serialVersionUID = -3045972689076478992L;

	private BigDecimal automaticCheckinCharge;

	private String status;

	private String airportCode;
	
	private String SeatPref;
	
	private Integer autoCheckinId;

	private String email;

	private String seatCode;

	/**
	 * @return the automaticCheckinCharge
	 */
	public BigDecimal getAutomaticCheckinCharge() {
		return automaticCheckinCharge;
	}

	/**
	 * @param automaticCheckinCharge
	 *            the automaticCheckinCharge to set
	 */
	public void setAutomaticCheckinCharge(BigDecimal automaticCheckinCharge) {
		this.automaticCheckinCharge = automaticCheckinCharge;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
		
	}

	/**
	 * @return the seatPref
	 */
	public String getSeatPref() {
		return SeatPref;
	}

	/**
	 * @param seatPref
	 *            the seatPref to set
	 */
	public void setSeatPref(String seatPref) {
		SeatPref = seatPref;
	}

	/**
	 * @return the autoCheckinId
	 */
	public Integer getAutoCheckinId() {
		return autoCheckinId;
	}

	/**
	 * @param autoCheckinId
	 *            the autoCheckinId to set
	 */
	public void setAutoCheckinId(Integer autoCheckinId) {
		this.autoCheckinId = autoCheckinId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

}
