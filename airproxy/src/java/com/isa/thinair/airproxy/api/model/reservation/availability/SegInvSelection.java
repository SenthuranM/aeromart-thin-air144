package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class SegInvSelection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String segmentCode;
	private Date departureDateTime;
	private String bookingCode;
	private String flightNumber;
	private Date arrivalDateTime;
	private String flightRPH;

	public SegInvSelection(FlightSegmentTO fltSegment, String bookingCode) {
		this.segmentCode = fltSegment.getSegmentCode();
		this.departureDateTime = fltSegment.getDepartureDateTime();
		this.bookingCode = bookingCode;
		this.flightNumber = fltSegment.getFlightNumber();
		this.arrivalDateTime = fltSegment.getArrivalDateTime();
		this.flightRPH = fltSegment.getFlightRefNumber();
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public String getFlightRPH() {
		return flightRPH;
	}
}
