package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mano
 * @since May 9, 2011
 */
public class LCCBaggageResponseDTO implements Serializable {

	private List<LCCFlightSegmentBaggagesDTO> flightSegmentBaggages;

	/**
	 * @return the flightSegmentBaggages
	 */
	public List<LCCFlightSegmentBaggagesDTO> getFlightSegmentBaggages() {

		if (flightSegmentBaggages == null) {
			flightSegmentBaggages = new ArrayList<LCCFlightSegmentBaggagesDTO>();
		}
		return flightSegmentBaggages;

	}

}
