package com.isa.thinair.airproxy.api.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class FindReservationUtil {

	private static Log log = LogFactory.getLog(FindReservationUtil.class);

	public static Collection<ReservationListTO> composeReservationsList(Collection<ReservationDTO> colReservationDTO,
			UserPrincipal userPrincipal) throws ModuleException {
		Collection<ReservationListTO> colReservationListTO = new ArrayList<ReservationListTO>();
		SimpleDateFormat smpdtDate = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat smpdtTime = new SimpleDateFormat("HH:mm");
		String strReservationStatus;
		String status;
		boolean waitListedSegmentExists = false;
		
		if (userPrincipal!= null && (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES)) {
			log.info("###WSLOGS### COMPOSE RESERVATIONS LIST STARTED... LIST SIZE : " + colReservationDTO.size());
		}

		if (colReservationDTO != null && colReservationDTO.size() > 0) {
			Iterator<ReservationDTO> iterReservation = colReservationDTO.iterator();

			// Reservation Information
			while (iterReservation.hasNext()) {
				ReservationDTO reservationDTO = iterReservation.next();
				ReservationContactInfo reservationContactInfo = reservationDTO.getReservationContactInfo();

				ReservationListTO reservationListTO = new ReservationListTO();
				if (userPrincipal != null && (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES)) {
					log.info("###WSLOGS### COMPOSE RESERVATION STARTED... PNR : " + reservationDTO.getPnr());
				}
				reservationListTO.setPnrNo(BeanUtils.nullHandler(reservationDTO.getPnr()));
				reservationListTO.setPaxName(BeanUtils.nullHandler(reservationContactInfo.getFirstName()) + ", "
						+ BeanUtils.nullHandler(reservationContactInfo.getLastName()));
				reservationListTO.setPaxEmail(BeanUtils.nullHandler(reservationContactInfo.getEmail()));
				reservationListTO.setPaxMobile(BeanUtils.nullHandler(reservationContactInfo.getMobileNo()));
				reservationListTO.setPaxPhone(BeanUtils.nullHandler(reservationContactInfo.getPhoneNo()));
				reservationListTO.setSendPromoEmail(reservationContactInfo.getSendPromoEmail());
				
				reservationListTO.setOriginatorPnr(BeanUtils.nullHandler(reservationDTO.getOriginatorPnr()));
				reservationListTO.setReleaseTimeStamp(reservationDTO.getZuluReleaseTimeStamp());

				List<FlightInfoTO> lstFlightInfo = new ArrayList<FlightInfoTO>();

				String strPaxCount = reservationDTO.getAdultCount() + "/" + reservationDTO.getChildCount() + "/"
						+ reservationDTO.getInfantCount();
				int i = 0;
				for (Iterator iterator = reservationDTO.getSegments().iterator(); iterator.hasNext();) {
					ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterator.next();
					if(reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)){
						waitListedSegmentExists = true;
					}
					FlightInfoTO flightInfoTO = new FlightInfoTO();

					flightInfoTO.setDepartureDate(smpdtDate.format(reservationSegmentDTO.getDepartureDate()));
					flightInfoTO.setDepartureTime(smpdtTime.format(reservationSegmentDTO.getDepartureDate()));
					flightInfoTO.setDepartureDateLong(reservationSegmentDTO.getDepartureDate().getTime());
					String formatedArrivalDate = reservationSegmentDTO.getArrivalDate() != null ? smpdtDate
							.format(reservationSegmentDTO.getArrivalDate()) : "";
					String formatedArrivalTime = reservationSegmentDTO.getArrivalDate() != null ? smpdtTime
							.format(reservationSegmentDTO.getArrivalDate()) : "";
					flightInfoTO.setArrivalDate(formatedArrivalDate);
					flightInfoTO.setArrivalTime(formatedArrivalTime);
					flightInfoTO.setFlightNo(reservationSegmentDTO.getFlightNo());
					flightInfoTO.setPaxCount(strPaxCount);
					flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(reservationSegmentDTO.getFlightNo()));

					boolean isOpenReturn = reservationSegmentDTO.isOpenReturnSegment();
					flightInfoTO.setOpenReturnSegment(isOpenReturn);
					flightInfoTO.setStatus(reservationSegmentDTO.getStatus());
					flightInfoTO.setSubStatus(reservationSegmentDTO.getSubStatus());
					flightInfoTO.setDisplayStatus(reservationSegmentDTO.getDisplayStatus());
					LogicalCabinClassDTO logicalCC = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap()
							.get(reservationSegmentDTO.getLogicalCCCode());
					if (logicalCC != null) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							flightInfoTO.setCosDes(logicalCC.getDescription());
						} else {
							flightInfoTO.setCosDes(CommonsServices.getGlobalConfig().getActiveCabinClassesMap()
									.get(logicalCC.getCabinClassCode()));
						}
					} else {
						String bookingClass = reservationSegmentDTO.getCodeShareBc();
						if (bookingClass != null) {
							String cabinClass = CommonsServices.getGlobalConfig().getCodeshareBcCosMap().get(bookingClass);
							flightInfoTO.setCosDes(CommonsServices.getGlobalConfig().getActiveCabinClassesMap().get(cabinClass));
						} else {
							flightInfoTO.setCosDes("");
						}
					}

					String segmentCode = BeanUtils.nullHandler(reservationSegmentDTO.getSegmentCode());

					// Haider 4Feb09
					if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
						segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
					}

					flightInfoTO.setOrignNDest(segmentCode);

					String fltDuration = reservationSegmentDTO.getFlightDuration();
					String fltModelDescription = reservationSegmentDTO.getFlightModelDescription();
					String fltModelNo = reservationSegmentDTO.getFlightModelNumber();
					String fltRemarks = reservationSegmentDTO.getRemarks();

					fltDuration = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDuration);
					fltModelDescription = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelDescription);
					fltModelNo = FlightStopOverDisplayUtil.formatNullOrEmpty(fltModelNo);
					fltRemarks = FlightStopOverDisplayUtil.formatNullOrEmpty(fltRemarks);					

					String fltDepartureTerminalName = reservationSegmentDTO.getDepartureTerminalName();
					String fltArrivalTerminalName = reservationSegmentDTO.getArrivalTerminalName();

					fltDepartureTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltDepartureTerminalName);
					fltArrivalTerminalName = FlightStopOverDisplayUtil.formatNullOrEmpty(fltArrivalTerminalName);					
					flightInfoTO.setDepartureTerminal(fltDepartureTerminalName);
					flightInfoTO.setArrivalTerminal(fltArrivalTerminalName);

					flightInfoTO.setFlightDuration(fltDuration);
					flightInfoTO.setFlightModelDescription(fltModelDescription);
					flightInfoTO.setFlightModelNumber(fltModelNo);
					flightInfoTO.setRemarks(fltRemarks);

					flightInfoTO.setFlightStopOverDuration(getStopOverDuration(
							reservationDTO.getSegmentsOrderedByZuluDepartureDate(), reservationSegmentDTO));

					flightInfoTO.setNoOfStops(FlightStopOverDisplayUtil.getStopOverStations(reservationSegmentDTO
							.getSegmentCode()));
					flightInfoTO.setCsOcCarrierCode(reservationSegmentDTO.getCsOcCarrierCode());

					lstFlightInfo.add(flightInfoTO);
					i++;
				}
				
				strReservationStatus = reservationDTO.getStatus();
				if (strReservationStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {

					if (userPrincipal != null) {
						String strStation = userPrincipal.getAgentStation();
						Collection colUserDST = userPrincipal.getColUserDST();
						String localTime = DateUtil.getAgentLocalTime(reservationDTO.getZuluReleaseTimeStamp(), colUserDST,
								strStation, "dd-MM-yyyy HH:mm");
						status = "<font color='#FFB900'>" + strReservationStatus;

						if (!waitListedSegmentExists) {
							status = status + "<BR><font color='RED' size='0' style='font-size: 9px;'>" + localTime
									+ "</font></font>";
						}
					} else {
						status = strReservationStatus;
					}
				} else {
					status = strReservationStatus;
				}
				reservationListTO.setPnrStatus(status);
				if (reservationDTO.getColExternalSegments() != null && reservationDTO.getColExternalSegments().size() > 0) {
					for (ReservationExternalSegmentTO reservationExternalSegmentTO : reservationDTO.getColExternalSegments()) {
						String segmentCode = reservationExternalSegmentTO.getSegmentCode();

						if (AppSysParamsUtil.isHideStopOverEnabled() && segmentCode != null && segmentCode.split("/").length > 2) {
							segmentCode = ReservationApiUtils.hideStopOverSeg(segmentCode);
						}

						FlightInfoTO flightInfoTO = new FlightInfoTO();
						flightInfoTO.setFlightNo(reservationExternalSegmentTO.getFlightNo());
						flightInfoTO.setAirLine(AppSysParamsUtil.extractCarrierCode(reservationExternalSegmentTO.getFlightNo()));
						flightInfoTO.setDepartureDate(smpdtDate.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureTime(smpdtTime.format(reservationExternalSegmentTO.getDepartureDate()));
						flightInfoTO.setDepartureDateLong(reservationExternalSegmentTO.getDepartureDate().getTime());
						flightInfoTO.setArrivalDate(smpdtDate.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setArrivalTime(smpdtTime.format(reservationExternalSegmentTO.getArrivalDate()));
						flightInfoTO.setOrignNDest(segmentCode);
						flightInfoTO.setPaxCount(strPaxCount);
						flightInfoTO.setStatus(reservationExternalSegmentTO.getStatus());
						LogicalCabinClassDTO logicalCC = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap()
								.get(reservationExternalSegmentTO.getLogicalCCCode());
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							flightInfoTO.setCosDes(logicalCC.getDescription());
						} else {
							flightInfoTO.setCosDes(CommonsServices.getGlobalConfig().getActiveCabinClassesMap()
									.get(logicalCC.getCabinClassCode()));
						}
						lstFlightInfo.add(flightInfoTO);
					}
				}

				Collections.sort(lstFlightInfo);

				reservationListTO.setFlightInfo(lstFlightInfo);
				colReservationListTO.add(reservationListTO);
				if (userPrincipal!=null && (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES)) {
					log.info("###WSLOGS### COMPOSE RESERVATION COMPLETED... PNR : " + reservationListTO.getPnrNo());
				}
			}
		}

		return colReservationListTO;
	}

	public static CurrencyCodeGroup getDefaultCurrencyCodeGroup() {
		CurrencyCodeGroup currencyCodeGroup = new CurrencyCodeGroup();
		currencyCodeGroup.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		currencyCodeGroup.setDecimalPlaces(AppSysParamsUtil.getNumberOfDecimalPoints());
		return currencyCodeGroup;
	}

	/**
	 * liter version of the pnrModesDTO so reservatons will be load with minumun info.
	 * 
	 * @param pnrModesDTO
	 * @return
	 */
	public static LCCClientPnrModesDTO getLitePnrModesDTO(LCCClientPnrModesDTO pnrModesDTO) {
		LCCClientPnrModesDTO lccClientPnrModesDTO = new LCCClientPnrModesDTO();
		lccClientPnrModesDTO.setPnr(pnrModesDTO.getPnr());
		lccClientPnrModesDTO.setGroupPNR(pnrModesDTO.getGroupPNR());
		return lccClientPnrModesDTO;
	}

	private static String getStopOverDuration(List<FlightSegmentTO> fltSegmentList, int currentSegIndex) {
		int nextSegmentIndex = currentSegIndex + 1;
		if (nextSegmentIndex < fltSegmentList.size()) {
			IFlightSegment currentSeg = fltSegmentList.get(currentSegIndex);
			IFlightSegment nextSeg = fltSegmentList.get(nextSegmentIndex);

			return getDuration(currentSeg.getArrivalDateTime(), nextSeg.getDepartureDateTime());
		}
		return "-";
	}

	private static String getStopOverDuration(Collection<ReservationSegmentDTO> setReservationSegments,
			ReservationSegmentDTO currentSegment) {

		try {

			ReservationSegmentDTO nextReservSegment = getNextSegment(setReservationSegments, currentSegment.getPnrSegId());

			if (nextReservSegment != null) {

				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(currentSegment.getStatus())
						&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(nextReservSegment.getStatus())) {

					String curreSegAirportArr[] = currentSegment.getSegmentCode().split("/");
					String currSegDepCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(curreSegAirportArr[0]);
					String currSegArrCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(curreSegAirportArr[1]);

					String nextSegAirportArr[] = nextReservSegment.getSegmentCode().split("/");
					String nextSegDepCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(nextSegAirportArr[0]);
					String nextSegArrCityId = AirproxyModuleUtils.getAirportBD().getOwnCityIdForAirport(nextSegAirportArr[1]);

					boolean isReturn = false;

					if (currSegDepCityId != null && nextSegArrCityId != null && currSegDepCityId.equals(nextSegArrCityId)) {
						isReturn = true;
					} else if (curreSegAirportArr[0].equals(nextSegAirportArr[1])) {
						isReturn = true;
					}

					if (!isReturn) {

						String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								nextSegAirportArr[0], null, null);

						if (isWithinTheTransitTime(minMaxTransitDurations, currentSegment.getArrivalDate(),
								nextReservSegment.getDepartureDate())) {
							if (curreSegAirportArr[1].equals(nextSegAirportArr[0])) {
								return getDuration(currentSegment.getArrivalDate(), nextReservSegment.getDepartureDate());
							} else if (currSegArrCityId != null && nextSegDepCityId != null
									&& currSegArrCityId.equals(nextSegDepCityId)) {
								return getDuration(currentSegment.getArrivalDate(), nextReservSegment.getDepartureDate());
							}
						}

					}
				}

			}

		} catch (Exception e) {
			log.error(e);
		}

		return "-";
	}

	private static boolean isWithinTheTransitTime(String[] minMaxTransitDurations, Date curSegArrDate, Date nextSegDepDate) {

		String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
		String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

		GregorianCalendar minCal = new GregorianCalendar();
		minCal.setTime(curSegArrDate);
		minCal.add(GregorianCalendar.HOUR, Integer.parseInt(minTransitHoursMin[0]));
		minCal.add(GregorianCalendar.MINUTE, Integer.parseInt(minTransitHoursMin[1]));

		GregorianCalendar maxCal = new GregorianCalendar();
		maxCal.setTime(curSegArrDate);
		maxCal.add(GregorianCalendar.HOUR, Integer.parseInt(maxTransitHoursMin[0]));
		maxCal.add(GregorianCalendar.MINUTE, Integer.parseInt(maxTransitHoursMin[1]));

		GregorianCalendar nextSegDep = new GregorianCalendar();
		nextSegDep.setTime(nextSegDepDate);

		if (nextSegDep.getTimeInMillis() >= minCal.getTimeInMillis() && nextSegDep.getTimeInMillis() <= maxCal.getTimeInMillis()) {
			return true;
		}

		return false;
	}

	private static ReservationSegmentDTO getNextSegment(Collection<ReservationSegmentDTO> sortedSegmentColl, Integer pnrSegId) {

		if (sortedSegmentColl != null && sortedSegmentColl.size() > 1) {
			Iterator<ReservationSegmentDTO> reserSegItr = sortedSegmentColl.iterator();

			while (reserSegItr.hasNext()) {
				ReservationSegmentDTO segmentDTO = reserSegItr.next();
				if (segmentDTO.getPnrSegId().intValue() == pnrSegId.intValue()) {
					if (reserSegItr.hasNext()) {
						return reserSegItr.next();
					}
				}
			}

		}

		return null;
	}

	private static String getDuration(Date departureDate, Date arrivalDate) {

		NumberFormat nfr = new DecimalFormat("##00");

		Calendar depCal = Calendar.getInstance();
		depCal.setTime(departureDate);

		Calendar arrCal = Calendar.getInstance();
		arrCal.setTime(arrivalDate);

		long diff = arrCal.getTimeInMillis() - depCal.getTimeInMillis();

		long diffMinutes = diff / (60 * 1000);

		String durationHoursStr = nfr.format((diffMinutes / 60)) + ":" + nfr.format((diffMinutes % 60));

		return durationHoursStr;
	}

}
