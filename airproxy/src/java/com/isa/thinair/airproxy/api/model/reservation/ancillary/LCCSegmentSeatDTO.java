package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class LCCSegmentSeatDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private FlightSegmentTO flightSegmentTO;

	private List<LCCAirSeatDTO> airSeatDTOs;

	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	public List<LCCAirSeatDTO> getAirSeatDTOs() {
		return airSeatDTOs;
	}

	public void setAirSeatDTOs(List<LCCAirSeatDTO> airSeatDTOs) {
		this.airSeatDTOs = airSeatDTOs;
	}

	public String toString() {
		StringBuilder toStringBuilder = new StringBuilder("LCCBlockSeatDTO [");
		toStringBuilder.append("airSeatDTOs=").append(airSeatDTOs);
		toStringBuilder.append(", flightSegmentTO=").append(flightSegmentTO);
		toStringBuilder.append("]");
		return toStringBuilder.toString();
	}
}
