/**
 * 
 */
package com.isa.thinair.airproxy.api.utils.assembler;

import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airreservation.api.model.ReservationSegment;

/**
 * @author Indika Athauda
 * 
 */
public class CommonAssemblerUtil {

	public static void addGroundStationDataToPNRSegment(Map<Integer, String> groundStation,
			Map<Integer, Integer> groundSementFlightByAirFlightMap, Map map) {
		if (map != null) {
			for (Entry<Integer, String> stationMapEntry : groundStation.entrySet()) {
				ReservationSegment resSeg = (ReservationSegment) map.get(stationMapEntry.getKey());
				if (resSeg != null) {
					resSeg.setSubStationShortName(stationMapEntry.getValue());
				}
			}
			for (Entry<Integer, Integer> parentEntry : groundSementFlightByAirFlightMap.entrySet()) {
				ReservationSegment resSeg = (ReservationSegment) map.get(parentEntry.getKey());
				if (resSeg != null) {
					Object resSegParentOjb = map.get(parentEntry.getValue());
					if (resSegParentOjb != null) {
						ReservationSegment resSegParent = (ReservationSegment) resSegParentOjb;
						resSegParent.setGroundSegment(resSeg);
					}
					// else ?? Should always have a parent? : Think and Discuss
				}

			}
		}
	}

}
