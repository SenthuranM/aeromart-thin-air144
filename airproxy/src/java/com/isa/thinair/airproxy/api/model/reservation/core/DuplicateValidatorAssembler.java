package com.isa.thinair.airproxy.api.model.reservation.core;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class DuplicateValidatorAssembler implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* Holds which system should the reservation be saved to */
	private SYSTEM targetSystem;

	/* Holds the transaction identifier */
	private String transactionIdentifier;

	/* Holds the flight segments to be checked */
	private List<FlightSegmentTO> flightSegments;

	/* Holds the passenger list */
	private List<LCCClientReservationPax> paxList;

	private String pnr;

	public SYSTEM getTargetSystem() {
		return targetSystem;
	}

	public void setTargetSystem(SYSTEM targetSystem) {
		this.targetSystem = targetSystem;
	}

	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	public List<FlightSegmentTO> getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(List<FlightSegmentTO> flightSegments) {
		this.flightSegments = flightSegments;
	}

	public List<LCCClientReservationPax> getPaxList() {
		return paxList;
	}

	public void setPaxList(List<LCCClientReservationPax> paxList) {
		this.paxList = paxList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

}
