package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

public class LCCAddressDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String streetLnNm1;
	private String streetLnNm2;
	private String cityName;
	private String stateName;
	private String zipCode;
	private String homePhoneNo;
	private String cellPhoneNo;
	private String emailAddr;
	private String countryName;

	public String getStreetLnNm1() {
		return streetLnNm1;
	}

	public void setStreetLnNm1(String streetLnNm1) {
		this.streetLnNm1 = streetLnNm1;
	}

	public String getStreetLnNm2() {
		return streetLnNm2;
	}

	public void setStreetLnNm2(String streetLnNm2) {
		this.streetLnNm2 = streetLnNm2;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getHomePhoneNo() {
		return homePhoneNo;
	}

	public void setHomePhoneNo(String homePhoneNo) {
		this.homePhoneNo = homePhoneNo;
	}

	public String getCellPhoneNo() {
		return cellPhoneNo;
	}

	public void setCellPhoneNo(String cellPhoneNo) {
		this.cellPhoneNo = cellPhoneNo;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}
