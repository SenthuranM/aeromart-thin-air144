package com.isa.thinair.airproxy.api.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.model.AnciOfferCriteria;

public class AnciOfferAuditCreator {
	public static final String ONE_WAY_APPLICABILITY = "oneWayApplicable";
	public static final String RETURN_APPLICABILITY = "returnApplicable";
	public static final String PAX_COUNT = "passengerCountQuantity";
	public static final String OFFERED_TEMPLATE_ID = "offeredAnciTemplateID";
	public static final String TEMPLATE_TYPE = "offeredAnciTemplateType";
	public static final String FARE_BASIS = "fareBasisCodes";
	public static final String LOGICAL_CABIN_CLASS = "logicalCabinClasses";
	public static final String OND = "ONDs";
	public static final String APPLICABLE_FROM = "applicableFrom";
	public static final String APPLICABLE_TO = "applicableTo";
	public static final String FLEXI_ID = "flexiIDs";
	public static final String DAYS_OF_THE_WEEK = "daysOfTheWeek";

	public static Audit generateAudit(AnciOfferCriteria oldCriteria, AnciOfferCriteria newCriteria, UserPrincipal userPrincipal) {
		return new Audit(TasksUtil.ANCI_OFFER_EDIT, new Date(), "airadmin", userPrincipal.getUserId(), generateAudit(oldCriteria,
				newCriteria));
	}

	private static String generateAudit(AnciOfferCriteria oldCriteria, AnciOfferCriteria newCriteria) {
		DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Map<Object, Object>> comparisonDetails = initComparisonMap(new HashMap<String, Map<Object, Object>>());

		if (!oldCriteria.getApplicableForOneWay().equals(newCriteria.getApplicableForOneWay())) {
			comparisonDetails.get(ONE_WAY_APPLICABILITY).put(oldCriteria.getApplicableForOneWay(),
					newCriteria.getApplicableForOneWay());
		}
		if (!oldCriteria.getApplicableForReturn().equals(newCriteria.getApplicableForReturn())) {
			comparisonDetails.get(RETURN_APPLICABILITY).put(oldCriteria.getApplicableForReturn(),
					newCriteria.getApplicableForReturn());
		}
		if (!oldCriteria.getEligiblePaxCounts().equals(newCriteria.getEligiblePaxCounts())) {
			comparisonDetails.get(PAX_COUNT).put(oldCriteria.getEligiblePaxCounts(), newCriteria.getEligiblePaxCounts());
		}
		if (!oldCriteria.getTemplateID().equals(newCriteria.getTemplateID())) {
			comparisonDetails.get(OFFERED_TEMPLATE_ID).put(oldCriteria.getTemplateID(), newCriteria.getTemplateID());
		}
		if (!oldCriteria.getAnciTemplateType().equals(newCriteria.getAnciTemplateType())) {
			comparisonDetails.get(TEMPLATE_TYPE).put(oldCriteria.getAnciTemplateType(), newCriteria.getAnciTemplateType());
		}
		if (!oldCriteria.getBookingClasses().equals(newCriteria.getBookingClasses())) {
			comparisonDetails.get(FARE_BASIS).put(oldCriteria.getBookingClasses(), newCriteria.getBookingClasses());
		}
		if (!oldCriteria.getLccs().equals(newCriteria.getLccs())) {
			comparisonDetails.get(LOGICAL_CABIN_CLASS).put(oldCriteria.getLccs(), newCriteria.getLccs());
		}
		if (!oldCriteria.getOnds().equals(newCriteria.getOnds())) {
			comparisonDetails.get(OND).put(oldCriteria.getOnds(), newCriteria.getOnds());
		}
		if (!oldCriteria.getApplicableFrom().equals(newCriteria.getApplicableFrom())) {
			comparisonDetails.get(APPLICABLE_FROM).put(dateFormatter.format(oldCriteria.getApplicableFrom()),
					dateFormatter.format(newCriteria.getApplicableFrom()));
		}
		if (!oldCriteria.getApplicableTo().equals(newCriteria.getApplicableTo())) {
			comparisonDetails.get(APPLICABLE_TO).put(dateFormatter.format(oldCriteria.getApplicableTo()),
					dateFormatter.format(newCriteria.getApplicableTo()));
		}
		if (!oldCriteria.getApplyToFlexiOnly().equals(newCriteria.getApplyToFlexiOnly())) {
			comparisonDetails.get(FLEXI_ID).put(oldCriteria.getApplyToFlexiOnly(), newCriteria.getApplyToFlexiOnly());
		}
		if (!oldCriteria.getApplicableOutboundDaysOfTheWeek().equals(newCriteria.getApplicableOutboundDaysOfTheWeek())) {
			comparisonDetails.get(DAYS_OF_THE_WEEK).put(oldCriteria.getApplicableOutboundDaysOfTheWeek(),
					newCriteria.getApplicableOutboundDaysOfTheWeek());
		}
		return transformChangesStringRepresentation(comparisonDetails, oldCriteria.getId());
	}

	private static String
			transformChangesStringRepresentation(Map<String, Map<Object, Object>> comparisonDetails, Integer offerID) {
		StringBuilder comparisons = new StringBuilder("Anci Offer ID : ").append(offerID);
		for (Entry<String, Map<Object, Object>> entry : comparisonDetails.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				comparisons.append(" Changed ");
				comparisons.append(entry.getKey());
				comparisons.append(" Old :: ");
				comparisons.append(entry.getValue().entrySet().iterator().next().getKey());
				comparisons.append(" New ::");
				comparisons.append(entry.getValue().entrySet().iterator().next().getValue());
			}
		}
		return comparisons.toString();
	}

	private static Map<String, Map<Object, Object>> initComparisonMap(Map<String, Map<Object, Object>> unInitializedMap) {
		unInitializedMap.put(ONE_WAY_APPLICABILITY, new HashMap<Object, Object>());
		unInitializedMap.put(RETURN_APPLICABILITY, new HashMap<Object, Object>());
		unInitializedMap.put(PAX_COUNT, new HashMap<Object, Object>());
		unInitializedMap.put(OFFERED_TEMPLATE_ID, new HashMap<Object, Object>());
		unInitializedMap.put(TEMPLATE_TYPE, new HashMap<Object, Object>());
		unInitializedMap.put(FARE_BASIS, new HashMap<Object, Object>());
		unInitializedMap.put(LOGICAL_CABIN_CLASS, new HashMap<Object, Object>());
		unInitializedMap.put(OND, new HashMap<Object, Object>());
		unInitializedMap.put(APPLICABLE_FROM, new HashMap<Object, Object>());
		unInitializedMap.put(APPLICABLE_TO, new HashMap<Object, Object>());
		unInitializedMap.put(FLEXI_ID, new HashMap<Object, Object>());
		unInitializedMap.put(DAYS_OF_THE_WEEK, new HashMap<Object, Object>());

		return unInitializedMap;
	}
}
