package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * @author aravinth.r
 * 
 */
public class LCCAutomaticCheckinRequestDTO implements Serializable {

	private static final long serialVersionUID = 821523908346395675L;

	private String transactionIdentifier;

	private String cabinClass;

	private FlightSegmentTO flightSegment;

	/**
	 * @return the transactionIdentifier
	 */
	public String getTransactionIdentifier() {
		return transactionIdentifier;
	}

	/**
	 * @param transactionIdentifier
	 *            the transactionIdentifier to set
	 */
	public void setTransactionIdentifier(String transactionIdentifier) {
		this.transactionIdentifier = transactionIdentifier;
	}

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the flightSegment
	 */
	public FlightSegmentTO getFlightSegment() {
		return flightSegment;
	}

	/**
	 * @param flightSegment
	 *            the flightSegment to set
	 */
	public void setFlightSegment(FlightSegmentTO flightSegment) {
		this.flightSegment = flightSegment;
	}

}
