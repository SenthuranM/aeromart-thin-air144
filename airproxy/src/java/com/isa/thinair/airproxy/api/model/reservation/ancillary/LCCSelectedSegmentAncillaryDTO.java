package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * 
 * @author Abheek Das Gupta
 * @since 1.0
 */
public class LCCSelectedSegmentAncillaryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private FlightSegmentTO flightSegmentTO;
	
	private List<FlightSegmentTO> externalFlightSegments;

	private String travelerRefNumber;

	private LCCAirSeatDTO airSeatDTO;

	private List<LCCAirSeatDTO> extraSeatDTOs;

	private List<LCCInsuranceQuotationDTO> insuranceQuotations;

	private List<LCCMealDTO> mealDTOs;

	private List<LCCSpecialServiceRequestDTO> specialServiceRequestDTOs;

	private String carrierCode;

	private List<LCCBaggageDTO> baggageDTOs;

	private List<LCCAirportServiceDTO> airportServiceDTOs;

	private List<LCCAirportServiceDTO> airportTransferDTOs;

	private List<LCCAutomaticCheckinDTO> automaticCheckinDTOs;

	/** Holds Passenger type */
	private String paxType;

	/**
	 * @return the flightSegmentTO
	 */
	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}

	/**
	 * @param flightSegmentTO
	 *            the flightSegmentTO to set
	 */
	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}

	/**
	 * @return the travelerRefNumber
	 */
	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	/**
	 * @param travelerRefNumber
	 *            the travelerRefNumber to set
	 */
	public void setTravelerRefNumber(String paxRefnumber) {
		this.travelerRefNumber = paxRefnumber;
	}

	/**
	 * @return the airSeatDTOs
	 */
	public LCCAirSeatDTO getAirSeatDTO() {
		return airSeatDTO;
	}

	/**
	 * @param airSeatDTOs
	 *            the airSeatDTOs to set
	 */
	public void setAirSeatDTO(LCCAirSeatDTO airSeatDTO) {
		this.airSeatDTO = airSeatDTO;
	}

	/**
	 * @return the mealDTOs
	 */
	public List<LCCMealDTO> getMealDTOs() {
		return mealDTOs;
	}

	/**
	 * @param mealDTOs
	 *            the mealDTOs to set
	 */
	public void setMealDTOs(List<LCCMealDTO> mealDTOs) {
		this.mealDTOs = mealDTOs;
	}

	/**
	 * @return the specialServiceRequestDTOs
	 */
	public List<LCCSpecialServiceRequestDTO> getSpecialServiceRequestDTOs() {
		return specialServiceRequestDTOs;
	}

	/**
	 * @param specialServiceRequestDTOs
	 *            the specialServiceRequestDTOs to set
	 */
	public void setSpecialServiceRequestDTOs(List<LCCSpecialServiceRequestDTO> specialServiceRequestDTOs) {
		this.specialServiceRequestDTOs = specialServiceRequestDTOs;
	}

	
	public List<LCCInsuranceQuotationDTO> getInsuranceQuotations() {
		return insuranceQuotations;
	}

	public void setInsuranceQuotations(List<LCCInsuranceQuotationDTO> insuranceQuotations) {
		this.insuranceQuotations = insuranceQuotations;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LCCSelectedSegmentAncillaryDTO [airSeatDTO=");
		builder.append(airSeatDTO);
		builder.append(", flightSegmentTO=");
		builder.append(flightSegmentTO);
		builder.append(", insuranceQuotations=");
		builder.append(insuranceQuotations);
		builder.append(", mealDTOs=");
		builder.append(mealDTOs);
		builder.append(", specialServiceRequestDTOs=");
		builder.append(specialServiceRequestDTOs);
		builder.append(", travelerRefNumber=");
		builder.append(travelerRefNumber);
		builder.append(", passenget type=");
		builder.append(paxType);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the baggageDTOs
	 */
	public List<LCCBaggageDTO> getBaggageDTOs() {
		return baggageDTOs;
	}

	/**
	 * @param baggageDTOs
	 *            the baggageDTOs to set
	 */
	public void setBaggageDTOs(List<LCCBaggageDTO> baggageDTOs) {
		this.baggageDTOs = baggageDTOs;
	}

	/**
	 * @return the airportServiceDTOs
	 */
	public List<LCCAirportServiceDTO> getAirportServiceDTOs() {
		return airportServiceDTOs;
	}

	/**
	 * @param airportServiceDTOs
	 *            the airportServiceDTOs to set
	 */
	public void setAirportServiceDTOs(List<LCCAirportServiceDTO> airportServiceDTOs) {
		this.airportServiceDTOs = airportServiceDTOs;
	}

	public List<LCCAirportServiceDTO> getAirportTransferDTOs() {
		return airportTransferDTOs;
	}

	public void setAirportTransferDTOs(List<LCCAirportServiceDTO> airportTransferDTOs) {
		this.airportTransferDTOs = airportTransferDTOs;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the externalFlightSegments
	 */
	public List<FlightSegmentTO> getExternalFlightSegments() {
		return externalFlightSegments;
	}

	/**
	 * @param externalFlightSegments the externalFlightSegments to set
	 */
	public void setExternalFlightSegments(List<FlightSegmentTO> externalFlightSegments) {
		this.externalFlightSegments = externalFlightSegments;
	}
	
	public List<LCCAirSeatDTO> getExtraSeatDTOs() {
		return extraSeatDTOs;
	}

	public void setExtraSeatDTOs(List<LCCAirSeatDTO> extraSeatDTOs) {
		this.extraSeatDTOs = extraSeatDTOs;
	}

	/**
	 * @return the automaticCheckinDTOs
	 */
	public List<LCCAutomaticCheckinDTO> getAutomaticCheckinDTOs() {
		if (automaticCheckinDTOs == null) {
			automaticCheckinDTOs = new ArrayList<LCCAutomaticCheckinDTO>();
		}
		return automaticCheckinDTOs;
	}

	/**
	 * @param automaticCheckinDTOs
	 *            the automaticCheckinDTOs to set
	 */
	public void setAutomaticCheckinDTOs(List<LCCAutomaticCheckinDTO> automaticCheckinDTOs) {
		this.automaticCheckinDTOs = automaticCheckinDTOs;
	}

}