package com.isa.thinair.airproxy.api.model.reservation.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.CodeShareFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

public class RequoteModifyRQ extends RequoteBalanceQueryRQ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, IPayment> passengerPaymentAssemblers = new HashMap<String, IPayment>();

	private Map<String, LCCClientPaymentAssembler> lccPassengerPayments = new HashMap<String, LCCClientPaymentAssembler>();

	// TODO do the generalization
	private Map<Integer, CardPaymentInfo> temporaryTnxMap;

	private Integer paymentType;

	private String userNotes;

	private String endorsementNotes;

	private Date lastModificationTimestamp;

	private String lastCurrencyCode;

	private Date releaseTimeStampZulu;

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap;

	private Map<Integer, CommonCreditCardPaymentInfo> lccTemporaryTnxMap;

	private List<IInsuranceRequest> insurances;

	private String auditDetails;

	private boolean autoCancellationEnabled;

	private boolean hasBufferTimeAutoCnxPrivilege;

	private boolean actualPayment;
	
	private boolean noBalanceToPay;

	private CommonReservationContactInfo contactInfo;

	private Map<String, String> flightRefWiseOndBaggageGroup;
	
	private List<CodeShareFlightDTO> codeShareFlightDTOs;

	private LoyaltyPaymentInfo loyaltyPaymentInfo;
	
	private String reasonForAllowBlPax;
    
    private String userNoteType;

	private Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo;

	private String ownerChannelId;
	
	private PayByVoucherInfo payByVoucherInfo;
	
	private boolean infantPaymentSeparated;

	public void addPassengerPaymentAssembler(String paxUniqueId, IPayment iPayment) {
		passengerPaymentAssemblers.put(paxUniqueId, iPayment);
	}

	public Map<String, IPayment> getPassengerPaymentMap() {
		return passengerPaymentAssemblers;
	}

	public Collection<IPayment> getAllPaymentAssemblers() {
		return passengerPaymentAssemblers.values();
	}

	public void setLccTemporaryTnxMap(Map<Integer, CommonCreditCardPaymentInfo> lccTemporaryTnxMap) {
		this.lccTemporaryTnxMap = lccTemporaryTnxMap;
	}

	public Map<Integer, CommonCreditCardPaymentInfo> getLccTemporaryTnxMap() {
		if (lccTemporaryTnxMap == null) {
			lccTemporaryTnxMap = new HashMap<Integer, CommonCreditCardPaymentInfo>();
		}
		return lccTemporaryTnxMap;
	}

	/**
	 * TODO do the generalization to PaymentInfo and remove unwanted OO dependencies in the form of instanceof
	 * 
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getTemporaryTnxMap() {
		if (temporaryTnxMap == null) {
			temporaryTnxMap = new HashMap<Integer, CardPaymentInfo>();
		}
		return temporaryTnxMap;
	}

	public void setTemporaryTnxMap(Map<Integer, CardPaymentInfo> temporaryTnxMap) {
		this.temporaryTnxMap = temporaryTnxMap;
	}

	public String getUserNotes() {
		return userNotes;
	}

	public String getEndorsementNotes() {
		return endorsementNotes;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public String getLastCurrencyCode() {
		return lastCurrencyCode;
	}

	public boolean isFlexiExists() {
		// TODO Auto-generated method stub
		return false;
	}

	public Map<String, LCCClientPaymentAssembler> getLccPassengerPayments() {
		return lccPassengerPayments;
	}

	public void setLccPassengerPayments(Map<String, LCCClientPaymentAssembler> lccPassengerPayments) {
		this.lccPassengerPayments = lccPassengerPayments;
	}

	public Date getLastModificationTimestamp() {
		return lastModificationTimestamp;
	}

	public void setLastModificationTimestamp(Date lastModificationTimestamp) {
		this.lastModificationTimestamp = lastModificationTimestamp;
	}

	public Date getReleaseTimeStampZulu() {
		return releaseTimeStampZulu;
	}

	public void setReleaseTimeStampZulu(Date releaseTimeStampZulu) {
		this.releaseTimeStampZulu = releaseTimeStampZulu;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public void setEndorsementNotes(String endorsementNotes) {
		this.endorsementNotes = endorsementNotes;
	}

	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.lastCurrencyCode = lastCurrencyCode;
	}

	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap() {
		return externalChargesMap;
	}

	public void setExternalChargesMap(Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap) {
		this.externalChargesMap = externalChargesMap;
	}

	public void addTemporaryTnx(Integer pnrPaxId, CardPaymentInfo cardPaymentInfo) {
		if (cardPaymentInfo != null) {
			getTemporaryTnxMap().put(pnrPaxId, cardPaymentInfo);
		}
	}
	
	public List<IInsuranceRequest> getInsurances() {
		if(insurances == null){
			insurances = new ArrayList<IInsuranceRequest>();
		}
		return insurances;
	}

	public void setInsurances(List<IInsuranceRequest> insurances) {
		this.insurances = insurances;
	}
	
	public void addInsurance(IInsuranceRequest insurance) {
		this.getInsurances().add(insurance);
	}

	public String getAuditDetails() {
		return auditDetails;
	}

	public void setAuditDetails(String auditDetails) {
		this.auditDetails = auditDetails;
	}

	public boolean isAutoCancellationEnabled() {
		return autoCancellationEnabled;
	}

	public void setAutoCancellationEnabled(boolean autoCancellationEnabled) {
		this.autoCancellationEnabled = autoCancellationEnabled;
	}

	public boolean isHasBufferTimeAutoCnxPrivilege() {
		return hasBufferTimeAutoCnxPrivilege;
	}

	public void setHasBufferTimeAutoCnxPrivilege(boolean hasBufferTimeAutoCnxPrivilege) {
		this.hasBufferTimeAutoCnxPrivilege = hasBufferTimeAutoCnxPrivilege;
	}

	public boolean isActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(boolean actualPayment) {
		this.actualPayment = actualPayment;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Map<String, String> getFlightRefWiseOndBaggageGroup() {
		return flightRefWiseOndBaggageGroup;
	}

	public void setFlightRefWiseOndBaggageGroup(Map<String, String> flightRefWiseOndBaggageGroup) {
		this.flightRefWiseOndBaggageGroup = flightRefWiseOndBaggageGroup;
	}

	public List<CodeShareFlightDTO> getCodeShareFlightDTOs() {
		return codeShareFlightDTOs;
	}

	public void setCodeShareFlightDTOs(List<CodeShareFlightDTO> codeShareFlightDTOs) {
		this.codeShareFlightDTOs = codeShareFlightDTOs;
	}

	public LoyaltyPaymentInfo getDefaultCarrierLoyaltyPaymentInfo() {
		LoyaltyPaymentInfo loyaltyPaymentInfo = null;
		if (carrierWiseLoyaltyPaymentInfo != null
				&& carrierWiseLoyaltyPaymentInfo.containsKey(AppSysParamsUtil.getDefaultCarrierCode())) {
			loyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo.get(AppSysParamsUtil.getDefaultCarrierCode());
		}
		return loyaltyPaymentInfo;
	}

	public Map<String, LoyaltyPaymentInfo> getCarrierWiseLoyaltyPaymentInfo() {
		return carrierWiseLoyaltyPaymentInfo;
	}

	public void setCarrierWiseLoyaltyPaymentInfo(Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPaymentInfo) {
		this.carrierWiseLoyaltyPaymentInfo = carrierWiseLoyaltyPaymentInfo;
	}

	public String getOwnerChannelId() {
		return ownerChannelId;
	}

	public void setOwnerChannelId(String ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	public boolean isNoBalanceToPay() {
		return noBalanceToPay;
	}

	public void setNoBalanceToPay(boolean noBalanceToPay) {
		this.noBalanceToPay = noBalanceToPay;
	}

	public String getReasonForAllowBlPax() {
		return reasonForAllowBlPax;
	}

	public void setReasonForAllowBlPax(String reasonForAllowBlPax) {
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}

	public String getUserNoteType() {
		return userNoteType;
	}

	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}
	
	
}
