package com.isa.thinair.airproxy.api.utils.converters;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OpenReturnPeriodsTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class OndConvertUtil {
	private static Log log = LogFactory.getLog(OndConvertUtil.class);

	/**
	 * Compose OriginDestinationInformationTO list based on the all available flights in
	 * AvailableFlightSegment/AvailableConnectedFlight list and selected flight segment(s) in
	 * AvailableFlightSegment/AvailableConnectedFlight
	 * 
	 * @param onewayAvailableFlightsCol
	 * @param selectedAvailableFlightSegment
	 * @param returnFlag
	 * @param isFareQuote
	 * @param getSegONDList
	 *            boolean denoting whether to get segment fare OND list or not.
	 * @param ondSequence
	 *            TODO
	 * @param selectedFlightDTO
	 * 
	 * @return List<OriginDestinationInformationTO>
	 * @throws ModuleException
	 */

	public static void getOndList(OriginDestinationInformationTO originDestinationInformationTO,
			Collection<AvailableIBOBFlightSegment> onewayAvailableFlightsCol,
			AvailableIBOBFlightSegment selectedAvailableFlightSegment, boolean returnFlag, boolean isFareQuote,
			boolean getSegONDList, BaseAvailRQ baseAvailRQ, Integer ondSequence) throws ModuleException {
		if (originDestinationInformationTO != null && !onewayAvailableFlightsCol.isEmpty() && !getSegONDList) {
			originDestinationInformationTO.getOrignDestinationOptions().clear();
		}

		log.info("onewayAvailableFlightsCol size = " + onewayAvailableFlightsCol.size());
		if (originDestinationInformationTO != null) {
			String selectedLanguage = baseAvailRQ.getAvailPreferences().getPreferredLanguage();
			for (AvailableIBOBFlightSegment onewayAvailableFlight : onewayAvailableFlightsCol) {

				if (!returnFlag && onewayAvailableFlight.isInboundFlightSegment()) {
					returnFlag = onewayAvailableFlight.isInboundFlightSegment();
				}

				List<FlightSegmentTO> flightSegmentList = new ArrayList<FlightSegmentTO>();
				List<FlightFareSummaryTO> flightFareSummaryTOs = new ArrayList<FlightFareSummaryTO>();

				boolean isSelected = selectedAvailableFlightSegment != null
						? SegmentUtil.isFlightSegmentSelected(onewayAvailableFlight, selectedAvailableFlightSegment)
						: false;

				if (onewayAvailableFlight.getAvailableLogicalCabinClasses() != null) {
					Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig()
							.getAvailableLogicalCCMap();
					for (String availLogicalCabinClass : onewayAvailableFlight.getAvailableLogicalCabinClasses()) {
						Collection<OndFareDTO> colOndFareDTOForLogicalCC = onewayAvailableFlight
								.getFareChargesSeatsSegmentsDTOs(availLogicalCabinClass);

						if (colOndFareDTOForLogicalCC == null) {
							continue;
						}

						if (ondSequence == null) {
							ondSequence = returnFlag ? OndSequence.IN_BOUND : OndSequence.OUT_BOUND;
						}

						LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(availLogicalCabinClass);
						boolean isOndFlexiSelected = false;

						if (baseAvailRQ.getAvailPreferences().getOndFlexiSelected() != null
								&& baseAvailRQ.getAvailPreferences().getOndFlexiSelected().containsKey(ondSequence)
								&& onewayAvailableFlight.getSelectedBundledFare() == null) {
							isOndFlexiSelected = baseAvailRQ.getAvailPreferences().getOndFlexiSelected().get(ondSequence);
						}

						/*
						 * With new flexi implementation, flexi will be offered as a new logical cabin class in the
						 * front-end. But, in the back-end same logical cabin class will be used with a flexi flag. So,
						 * before sending available logical CC & fares to the front-end, dummy logical cabin class will
						 * populated based on the requirement.
						 * 
						 * Here, if the flexi is defined as a FOC service for a particular logical cc, only the 'logical
						 * cc with flexi' fare details will be added. Otherwise separate fare details will be added for
						 * 'logical cc with flexi'
						 */

						boolean freeFlexiEnabled = false;
						boolean hasFlexi = FareQuoteUtil.isFlexiAvailable(colOndFareDTOForLogicalCC);
						Integer flexiRuleId = FareQuoteUtil.getAvailableFlexiID(colOndFareDTOForLogicalCC);
						freeFlexiEnabled = logicalCabinClassDTO.isFreeFlexiEnabled();

						FlightFareSummaryTO flightFareSummaryTO = new FlightFareSummaryTO();
						flightFareSummaryTO.setFreeFlexi(freeFlexiEnabled);
						flightFareSummaryTO.setMinimumFareByPriceOnd(onewayAvailableFlight.getMinimumFareByPriceOnd());
						boolean hasFlightFare = createDummyFareSummaryObjectForFlexi(flightFareSummaryTO, onewayAvailableFlight,
								logicalCabinClassDTO, freeFlexiEnabled, isOndFlexiSelected, selectedLanguage, flexiRuleId,
								getSegONDList);

						if (freeFlexiEnabled && hasFlightFare) {
							flightFareSummaryTOs.add(flightFareSummaryTO);
						} else {
							if (hasFlexi && hasFlightFare && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
								flightFareSummaryTOs.add(flightFareSummaryTO);
							}

							FlightFareSummaryTO fareWihtoutFlexi = new FlightFareSummaryTO();
							try {	
								fareWihtoutFlexi = (FlightFareSummaryTO) flightFareSummaryTO.clone();
							} catch (CloneNotSupportedException e) {
								throw new ModuleException(e.getCause(), e.getMessage());
							}
							if (!isOndFlexiSelected && isSelected
									&& availLogicalCabinClass
											.equals(selectedAvailableFlightSegment.getSelectedLogicalCabinClass())
									&& (selectedAvailableFlightSegment.getSelectedBundledFare() == null)) {
								fareWihtoutFlexi.setSelected(true);
							} else {
								fareWihtoutFlexi.setSelected(false);
							}
							
							fareWihtoutFlexi.setLogicalCCDesc(logicalCabinClassDTO.getDescription());
							fareWihtoutFlexi.setComment(logicalCabinClassDTO.getComment());
							fareWihtoutFlexi.setWithFlexi(false);
							fareWihtoutFlexi.setFreeFlexi(false);
							
							fareWihtoutFlexi.setBundleFareDescriptionTemplateDTO(logicalCabinClassDTO.getBundleFareDescriptionTemplateDTO());
							fareWihtoutFlexi.setImageUrl(
									generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(), false, selectedLanguage));
							if (isFaresAndChargesAvailable(onewayAvailableFlight, availLogicalCabinClass, fareWihtoutFlexi, false,
									false, null, getSegONDList)) {
								flightFareSummaryTOs.add(fareWihtoutFlexi);
							}

						}
					}
				}

				addBundledFaresToLogicalCabinList(flightFareSummaryTOs, onewayAvailableFlight, getSegONDList);

				if (onewayAvailableFlight.isFlown() && onewayAvailableFlight.getSelectedBundledFare() != null) {
					retainOnlyBundledServiceOption(flightFareSummaryTOs,
							onewayAvailableFlight.getSelectedBundledFare().getBundledFarePeriodId());
				}

				if (log.isDebugEnabled()) {
					logFareClasses(flightFareSummaryTOs, onewayAvailableFlight);
				}

				Collection<OndFareDTO> ondFares = onewayAvailableFlight
						.getFareChargesSeatsSegmentsDTOs(onewayAvailableFlight.getSelectedLogicalCabinClass());

				setOndWiseBundledFare(onewayAvailableFlight.getSelectedBundledFare(), ondFares);

				for (FlightSegmentDTO availableFlightSegmentDTO : onewayAvailableFlight.getFlightSegmentDTOs()) {

					FlightSegmentTO cmnSegDTO = new FlightSegmentTO();
					cmnSegDTO.setArrivalDateTime(availableFlightSegmentDTO.getArrivalDateTime());
					cmnSegDTO.setArrivalDateTimeZulu(availableFlightSegmentDTO.getArrivalDateTimeZulu());
					cmnSegDTO.setCabinClassCode(onewayAvailableFlight.getCabinClassCode());
					cmnSegDTO.setDepartureDateTime(availableFlightSegmentDTO.getDepartureDateTime());
					cmnSegDTO.setDepartureDateTimeZulu(availableFlightSegmentDTO.getDepartureDateTimeZulu());
					cmnSegDTO.setFlightNumber(availableFlightSegmentDTO.getFlightNumber());
					cmnSegDTO.setSegmentCode(availableFlightSegmentDTO.getSegmentCode());
					cmnSegDTO.setOperatingAirline(availableFlightSegmentDTO.getCarrierCode());
					cmnSegDTO.setOperationType(availableFlightSegmentDTO.getOperationTypeID());
					cmnSegDTO.setReturnFlag(returnFlag);
					cmnSegDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(availableFlightSegmentDTO));
					cmnSegDTO.setFlightSegId(availableFlightSegmentDTO.getSegmentId());
					cmnSegDTO.setFlightId(availableFlightSegmentDTO.getFlightId());
					cmnSegDTO.setDomesticFlight(availableFlightSegmentDTO.isDomesticFlight());

					cmnSegDTO.setArrivalTerminalName(availableFlightSegmentDTO.getArrivalTerminal());
					cmnSegDTO.setDepartureTerminalName(availableFlightSegmentDTO.getDepartureTerminal());
					cmnSegDTO.setAdultCount(availableFlightSegmentDTO.getTotalAvailableAdultCount());
					cmnSegDTO.setInfantCount(availableFlightSegmentDTO.getTotalAvailableInfantCount());
					cmnSegDTO.setChildCount(0);
					if (ondSequence != null) {
						cmnSegDTO.setOndSequence(ondSequence);
					}

					cmnSegDTO.setFlightDuration(availableFlightSegmentDTO.getFlightDuration());
					cmnSegDTO.setFlightModelDescription(availableFlightSegmentDTO.getFlightModelDescription());
					cmnSegDTO.setFlightModelNumber(availableFlightSegmentDTO.getFlightModelNumber());
					cmnSegDTO.setRemarks(availableFlightSegmentDTO.getRemarks());
					cmnSegDTO.setFlightSegmentStatus(availableFlightSegmentDTO.getFlightSegmentStatus());
					cmnSegDTO.setAvailableAdultCountMap(availableFlightSegmentDTO.getAvailableAdultCount());
					cmnSegDTO.setAvailableInfantCountMap(availableFlightSegmentDTO.getAvailableInfantCount());
					cmnSegDTO.setSubJourneyGroup(onewayAvailableFlight.getSubJourneyGroup());
					if (onewayAvailableFlight.getRequestedOndSequence() != null) {
						cmnSegDTO.setRequestedOndSequence(onewayAvailableFlight.getRequestedOndSequence());
					} else {
						cmnSegDTO.setRequestedOndSequence(onewayAvailableFlight.getOndSequence());
					}
					cmnSegDTO.setWaitListed(availableFlightSegmentDTO.isWaitListed());
					cmnSegDTO.setOverbookEnabled(availableFlightSegmentDTO.isOverBookEnabled());
					cmnSegDTO.setCsOcCarrierCode(availableFlightSegmentDTO.getCsOcCarrierCode());
					cmnSegDTO.setOriginCountryCode(AirproxyModuleUtils.getFlightBD().getCountryCodeForOrigin(availableFlightSegmentDTO.getFlightId()));

					flightSegmentList.add(cmnSegDTO);
				}

				OriginDestinationOptionTO ondOption = new OriginDestinationOptionTO();
				ondOption.setFlightSegmentList(flightSegmentList);
				ondOption.setSelected(isSelected);
				boolean seatAvailable = false;
				if (!isFareQuote || !onewayAvailableFlight.getAvailableLogicalCabinClasses().isEmpty()) {
					seatAvailable = true;
				}
				ondOption.setSeatAvailable(seatAvailable);
				ondOption.setSystem(SYSTEM.AA);
				ondOption.setFlightFareSummaryList(flightFareSummaryTOs);
				if (getSegONDList) {
					originDestinationInformationTO.getOriginDestinationSegFareOptions().add(ondOption);
				} else {
					originDestinationInformationTO.getOrignDestinationOptions().add(ondOption);
				}
			}
		}
	}

	private static void logFareClasses(List<FlightFareSummaryTO> flightFareSummaryTOs,
			AvailableIBOBFlightSegment onewayAvailableFlight) {
		StringBuilder sb = new StringBuilder();

		sb.append("###FARE_CLASSES_LOG###,ONDFLIGHT:" + onewayAvailableFlight.toString() + ",");
		sb.append("FLTS:[");
		for (FlightSegmentDTO fs : onewayAvailableFlight.getFlightSegmentDTOs()) {
			sb.append(fs.getSegmentCode() + "|" + fs.getFlightNumber() + "|" + fs.getDepartureDateTime() + ",");
		}
		sb.append("],");
		sb.append("FARECLS:[");
		for (FlightFareSummaryTO ffs : flightFareSummaryTOs) {
			sb.append("{price:" + ffs.getTotalPrice() + ",flexi:" + ffs.isWithFlexi() + ",bfid:" + ffs.getBundledFarePeriodId()
					+ ",bffee:" + ffs.getBundleFareFee() + "," + ffs.getBookingCodes() + "},");
		}
		sb.append("]");
		log.debug(sb);
	}

	private static void retainOnlyBundledServiceOption(List<FlightFareSummaryTO> flightFareSummaryTOs, Integer bundledServicePeriodId) {
		if (bundledServicePeriodId != null) {
			Collection<FlightFareSummaryTO> retainElements = new ArrayList<FlightFareSummaryTO>();
			for (FlightFareSummaryTO flightFareSummaryTO : flightFareSummaryTOs) {
				if (bundledServicePeriodId.equals(flightFareSummaryTO.getBundledFarePeriodId())) {
					retainElements.add(flightFareSummaryTO);
				}
			}

			if (!retainElements.isEmpty()) {
				flightFareSummaryTOs.retainAll(retainElements);
				if (!flightFareSummaryTOs.isEmpty()) {
					flightFareSummaryTOs.iterator().next().setSelected(true);
				}
			}
		}
	}

	private static void addBundledFaresToLogicalCabinList(List<FlightFareSummaryTO> flightFareSummaryTOs,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, boolean getSegONDList) {

		Map<String, List<BundledFareLiteDTO>> availableBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		// Collections.sort(flightFareSummaryTOs);
		int lastRank = 0;
		if (flightFareSummaryTOs != null && !flightFareSummaryTOs.isEmpty()) {
			FlightFareSummaryTO lastElement = flightFareSummaryTOs.get(flightFareSummaryTOs.size() - 1);
			lastRank = lastElement.getRank() != null ? lastElement.getRank() : 2;
		}

		if (availableBundledFares != null
				&& availableBundledFares.containsKey(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
				&& !availableIBOBFlightSegment.isFlown()) {

			Integer selectedBundledFarePeriodId = null;
			if (availableIBOBFlightSegment.getSelectedBundledFare() != null) {
				selectedBundledFarePeriodId = availableIBOBFlightSegment.getSelectedBundledFare().getBundledFarePeriodId();
			}

			List<BundledFareLiteDTO> bundledFareLiteDTOs = availableBundledFares
					.get(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
			if (bundledFareLiteDTOs != null && !bundledFareLiteDTOs.isEmpty()) {

				for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareLiteDTOs) {
					FlightFareSummaryTO bundleFare = new FlightFareSummaryTO();
					bundleFare.setBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
					bundleFare.setComment(bundledFareLiteDTO.getDescription());
					bundleFare.setLogicalCCCode(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
					bundleFare.setLogicalCCDesc(bundledFareLiteDTO.getBundledFareName());
					bundleFare.setRank(++lastRank);
					bundleFare.setWithFlexi(bundledFareLiteDTO.isFlexiIncluded());
					bundleFare.setSeatAvailable(true);
					bundleFare.setBundleFareFee(bundledFareLiteDTO.getPerPaxBundledFee());
					bundleFare.setBookingCodes(bundledFareLiteDTO.getBookingClasses());
					bundleFare.setSegmentBookingClasses(bundledFareLiteDTO.getSegmentBookingClass());
					bundleFare.setBundledFareFreeServiceNames(bundledFareLiteDTO.getFreeServices());
					bundleFare.setSelected(bundledFareLiteDTO.getBundleFarePeriodId().equals(selectedBundledFarePeriodId));
					bundleFare.setImageUrl(bundledFareLiteDTO.getImageUrl());
					bundleFare.setBundleFareDescriptionTemplateDTO(bundledFareLiteDTO.getBundleFareDescriptionTemplateDTO());
					bundleFare.setMinimumFareByPriceOnd(availableIBOBFlightSegment.getMinimumFareByPriceOnd());
					isFaresAndChargesAvailable(availableIBOBFlightSegment,
							availableIBOBFlightSegment.getSelectedLogicalCabinClass(), bundleFare,
							bundledFareLiteDTO.isFlexiIncluded(), true, bundledFareLiteDTO, getSegONDList);
					flightFareSummaryTOs.add(bundleFare);
				}
			}
		} else if (availableIBOBFlightSegment.getSelectedBundledFare() != null
				&& (availableIBOBFlightSegment.isFlown() || availableIBOBFlightSegment.isUnTouchedOnd())) {
			BundledFareDTO selectedBundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();
			FlightFareSummaryTO bundledFare = new FlightFareSummaryTO();
			bundledFare.setBundledFarePeriodId(selectedBundledFareDTO.getBundledFarePeriodId());
			bundledFare.setComment(selectedBundledFareDTO.getBundledFareName());
			bundledFare.setLogicalCCCode(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
			bundledFare.setLogicalCCDesc(selectedBundledFareDTO.getBundledFareName());
			bundledFare.setRank(++lastRank);
			bundledFare.setWithFlexi(selectedBundledFareDTO.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()));
			bundledFare.setBookingCodes(selectedBundledFareDTO.getBookingClasses());
			bundledFare.setBundledFareFreeServiceNames(selectedBundledFareDTO.getApplicableServiceNames());
			bundledFare.setBundleFareFee(selectedBundledFareDTO.getPerPaxBundledFee());
			bundledFare.setImageUrl(selectedBundledFareDTO.getImageUrl());
			bundledFare.setBundleFareDescriptionTemplateDTO(selectedBundledFareDTO.getBundleFareDescriptionTemplateDTO());
			bundledFare.setSelected(true);

			flightFareSummaryTOs.add(bundledFare);
		}

	}

	private static void setOndWiseBundledFare(BundledFareDTO bundledFareDTO, Collection<OndFareDTO> ondFares) {
		if (bundledFareDTO != null) {
			BigDecimal[] amountBreakDown = AccelAeroCalculator.roundAndSplit(bundledFareDTO.getPerPaxBundledFee(),
					getAirSegOndFareDTOCount(ondFares));
			int count = 0;
			for (OndFareDTO ondFareDTO : ondFares) {
				if (!InventoryAPIUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
					BundledFareDTO ondBundledFareDTO = bundledFareDTO.clone();
					ondBundledFareDTO.setPerPaxBundledFee(amountBreakDown[count++]);
					ondFareDTO.setSelectedBundledFarePeriodId(ondBundledFareDTO.getBundledFarePeriodId());
				}
			}
		}
	}

	private static int getAirSegOndFareDTOCount(Collection<OndFareDTO> ondFares) {
		int count = 0;
		for (OndFareDTO ondFareDTO : ondFares) {
			if (!InventoryAPIUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
				count++;
			}
		}
		return count == 0 ? 1 : count;
	}

	private static boolean createDummyFareSummaryObjectForFlexi(FlightFareSummaryTO flightFareSummaryTO,
			AvailableIBOBFlightSegment onewayAvailableFlight, LogicalCabinClassDTO logicalCabinClassDTO, boolean freeFlexiEnabled,
			boolean isOndFlexiSelected, String preferredLanguage, Integer flexiRuleId, boolean getSegONDList) {

		flightFareSummaryTO.setLogicalCCCode(logicalCabinClassDTO.getLogicalCCCode());
		if (freeFlexiEnabled) {
			flightFareSummaryTO.setLogicalCCDesc(logicalCabinClassDTO.getDescription());
		} else {
			flightFareSummaryTO.setLogicalCCDesc(logicalCabinClassDTO.getFlexiDescription());
		}
		flightFareSummaryTO.setFlexiRuleId(flexiRuleId);
		flightFareSummaryTO.setLogicalCCRank(logicalCabinClassDTO.getNestRank());
		flightFareSummaryTO.setSeatAvailable(true);
		flightFareSummaryTO.setComment(logicalCabinClassDTO.getFlexiComment());
		flightFareSummaryTO.setSelected(isOndFlexiSelected || freeFlexiEnabled);
		flightFareSummaryTO.setWithFlexi(true);
		flightFareSummaryTO
				.setImageUrl(generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(), true, preferredLanguage));

		boolean hasFlightFare = isFaresAndChargesAvailable(onewayAvailableFlight, logicalCabinClassDTO.getLogicalCCCode(),
				flightFareSummaryTO, true, false, null, getSegONDList);

		return hasFlightFare;
	}

	private static boolean isFaresAndChargesAvailable(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			String availLogicalCabinClass, FlightFareSummaryTO flightFareSummaryTO, boolean withFlexi, boolean withBundleFare,
			BundledFareLiteDTO bundledFareLiteDTO, boolean getSegONDList) {
		boolean faresAvailableForTotalJourney = true;
		BigDecimal fareWithFareBundleFee = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (availableIBOBFlightSegment.getFare(availLogicalCabinClass) != null) {
			BigDecimal availableFlightFare = AccelAeroCalculator
					.parseBigDecimal(availableIBOBFlightSegment.getFare(availLogicalCabinClass).getFareAmount(PaxTypeTO.ADULT));
			BigDecimal availableFlightTotalChargesAndTaxes = BigDecimal.ZERO;
			BundledFareDTO selectedBundledFare = availableIBOBFlightSegment.getSelectedBundledFare();
			if (availableIBOBFlightSegment.getTotalCharges(availLogicalCabinClass) != null) {
				availableFlightTotalChargesAndTaxes = AccelAeroCalculator
						.parseBigDecimal(availableIBOBFlightSegment.getTotalCharges(availLogicalCabinClass).get(0));
				if (getSegONDList && selectedBundledFare != null && (bundledFareLiteDTO == null || (bundledFareLiteDTO != null
						&& !selectedBundledFare.getBundledFarePeriodId().equals(bundledFareLiteDTO.getBundleFarePeriodId())))) {
					BigDecimal bundleFareAmountToBeRemoved = BigDecimal.ZERO;
					bundleFareAmountToBeRemoved = availableIBOBFlightSegment.getSelectedBundleFareAmountWithTaxes();
					availableFlightTotalChargesAndTaxes = AccelAeroCalculator.add(availableFlightTotalChargesAndTaxes,
							bundleFareAmountToBeRemoved.negate());
					// TODO selected bundled fare tax portion removal
				} else if (withBundleFare == false || (withBundleFare == true && !flightFareSummaryTO.isSelected())) {
					BigDecimal bundleFareAmountToBeRemoved = BigDecimal.ZERO;
					bundleFareAmountToBeRemoved = availableIBOBFlightSegment.getSelectedBundleFareAmountWithTaxes();
					availableFlightTotalChargesAndTaxes = AccelAeroCalculator.add(availableFlightTotalChargesAndTaxes,
							bundleFareAmountToBeRemoved.negate());
				}
			}
			BigDecimal availableFlightTotalFlexi = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal availableBundleFare = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (withFlexi && (bundledFareLiteDTO == null || !bundledFareLiteDTO.isFlexiIncluded())) {
				availableFlightTotalFlexi = getTotalFlexiCharges(
						availableIBOBFlightSegment.getEffectiveFlexiCharges(availLogicalCabinClass));
			}

			if (withBundleFare
					&& bundledFareLiteDTO != null
					&& (selectedBundledFare == null || !selectedBundledFare.getBundledFarePeriodId().equals(
							bundledFareLiteDTO.getBundleFarePeriodId()))) {
				availableBundleFare = bundledFareLiteDTO.getPerPaxBundledFee();
			}

			if (bundledFareLiteDTO != null
					&& (selectedBundledFare == null || !selectedBundledFare.getBundledFarePeriodId().equals(
							bundledFareLiteDTO.getBundleFarePeriodId()))) {
				availableBundleFare = AccelAeroCalculator.add(availableBundleFare, bundledFareLiteDTO.getApplicableTaxPortion());
			}

			fareWithFareBundleFee = withBundleFare && bundledFareLiteDTO != null ?
					AccelAeroCalculator.add(availableFlightTotalFlexi,
							AccelAeroCalculator.add(bundledFareLiteDTO.getPerPaxBundledFee(), availableFlightFare)) :
					AccelAeroCalculator.add(availableFlightFare, availableFlightTotalFlexi);

			flightFareSummaryTO.setBaseFareAmount(
									AccelAeroCalculator.add(availableFlightFare, availableFlightTotalFlexi, availableBundleFare));
			flightFareSummaryTO.setTotalPrice(AccelAeroCalculator.add(availableFlightFare, availableFlightTotalChargesAndTaxes,
					availableFlightTotalFlexi, availableBundleFare));

			if (log.isDebugEnabled()) {
				log.debug(availableIBOBFlightSegment.toString() + "," + availableFlightFare + "," + availableFlightTotalFlexi
						+ "," + availableFlightTotalChargesAndTaxes + "," + flightFareSummaryTO.getTotalPrice());
			}

			// Booking class wise allocations
			int numberOfBCWiseAvailableSeats = availableIBOBFlightSegment.getFare(availLogicalCabinClass).getNoOfAvailableSeats();

			// Flights seat allocation
			int numberOfAllocatedAvailableSeats = availableIBOBFlightSegment.getFlightSegmentDTOs().get(0)
					.getAvailableAdultCount().get(availLogicalCabinClass);

			/*
			 * If Booking class wise allocation is less than allocated available seats in the flight it is taken
			 * otherwise, actual allocated number of seats are taken. (lesser of the two)
			 */
			int numberOfAvailableSeats = Math.min(numberOfAllocatedAvailableSeats, numberOfBCWiseAvailableSeats);

			if (numberOfAvailableSeats <= AppSysParamsUtil.getMinimumSeatsCutOverToShowInIbe()) {
				flightFareSummaryTO.setNoOfAvailableSeats(numberOfAvailableSeats);
			}
			if (availableIBOBFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames() != null
					|| availableIBOBFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames().size() > 0) {
				flightFareSummaryTO.setVisibleChannelName(
						(String) ((List) availableIBOBFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames())
								.get(0));
			}

		} else if (!availableIBOBFlightSegment.isDirectFlight()) {
			double availableFlightFare = 0;
			double availableFlightTotalChargesAndTaxes = 0;
			BigDecimal availableFlightTotalFlexi = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal availableBundleFare = AccelAeroCalculator.getDefaultBigDecimalZero();

			String visibleChannelName = null;
			boolean hasOnlyWebFare = true;
			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableIBOBFlightSegment)
					.getAvailableFlightSegments()) {
				if (availableFlightSegment.getFare(availLogicalCabinClass) != null) {
					availableFlightFare += availableFlightSegment.getFare(availLogicalCabinClass).getFareAmount(PaxTypeTO.ADULT);
					if (availableFlightSegment.getTotalCharges(availLogicalCabinClass) != null) {
						availableFlightTotalChargesAndTaxes += availableFlightSegment.getTotalCharges(availLogicalCabinClass)
								.get(0);
					}

					if (withFlexi && (bundledFareLiteDTO == null || !bundledFareLiteDTO.isFlexiIncluded())) {
						availableFlightTotalFlexi = AccelAeroCalculator.add(availableFlightTotalFlexi,
								getTotalFlexiCharges(availableFlightSegment.getEffectiveFlexiCharges(availLogicalCabinClass)));
					}

					BundledFareDTO selectedBundledFare = availableIBOBFlightSegment.getSelectedBundledFare();
					if (withBundleFare
							&& bundledFareLiteDTO != null
							&& (selectedBundledFare == null || !selectedBundledFare.getBundledFarePeriodId().equals(
									bundledFareLiteDTO.getBundleFarePeriodId()))) {
						availableBundleFare = bundledFareLiteDTO.getPerPaxBundledFee();
					}

					if (withBundleFare
							&& bundledFareLiteDTO != null
							&& (selectedBundledFare == null || !selectedBundledFare.getBundledFarePeriodId().equals(
									bundledFareLiteDTO.getBundleFarePeriodId()))) {
						availableBundleFare = AccelAeroCalculator.add(availableBundleFare,
								bundledFareLiteDTO.getApplicableTaxPortion());
					}

					if (availableFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames() != null
							|| availableFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames().size() > 0) {
						if (!SalesChannelsUtil.SALES_CHANNEL_WEB_KEY.equals(
								(String) ((List) availableFlightSegment.getFare(availLogicalCabinClass).getVisibleChannelNames())
										.get(0))) {
							hasOnlyWebFare = false;
							visibleChannelName = (String) ((List) availableFlightSegment.getFare(availLogicalCabinClass)
									.getVisibleChannelNames()).get(0);
						}
					}

				} else {
					faresAvailableForTotalJourney = false;
					break;
				}

			}
			if (faresAvailableForTotalJourney) {
				if (!withBundleFare || (withBundleFare && !flightFareSummaryTO.isSelected())) {
					BigDecimal bundleFareAmountToBeRemoved =
							availableIBOBFlightSegment.getSelectedBundleFareAmountWithTaxes() != null ?
									availableIBOBFlightSegment.getSelectedBundleFareAmountWithTaxes() :
									BigDecimal.ZERO;
					availableFlightTotalChargesAndTaxes =
							availableFlightTotalChargesAndTaxes - bundleFareAmountToBeRemoved.doubleValue();
				}
				fareWithFareBundleFee = withBundleFare && bundledFareLiteDTO != null ?
						AccelAeroCalculator.add(availableFlightTotalFlexi, AccelAeroCalculator
								.add(bundledFareLiteDTO.getPerPaxBundledFee(),
										AccelAeroCalculator.parseBigDecimal(availableFlightFare))) :
						AccelAeroCalculator
								.add(AccelAeroCalculator.parseBigDecimal(availableFlightFare), availableFlightTotalFlexi);
				flightFareSummaryTO
						.setBaseFareAmount(AccelAeroCalculator
								.add(AccelAeroCalculator.parseBigDecimal(availableFlightFare), availableFlightTotalFlexi,
										availableBundleFare));
				flightFareSummaryTO
						.setTotalPrice(AccelAeroCalculator.add(AccelAeroCalculator.parseBigDecimal(availableFlightFare),
								AccelAeroCalculator.parseBigDecimal(availableFlightTotalChargesAndTaxes),
								availableFlightTotalFlexi, availableBundleFare));
				if (hasOnlyWebFare) {
					flightFareSummaryTO.setVisibleChannelName(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY);
				} else {
					flightFareSummaryTO.setVisibleChannelName(visibleChannelName);
				}
			}
		}
		updateServiceTaxes(flightFareSummaryTO, availableIBOBFlightSegment, availLogicalCabinClass, fareWithFareBundleFee);
		return faresAvailableForTotalJourney;
	}

	private static void updateServiceTaxes(FlightFareSummaryTO flightFareSummaryTO,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, String availLogicalCabinClass,
			BigDecimal fareWithBundleFareFee) {
		if (!CollectionUtils.isEmpty(availableIBOBFlightSegment.getServiceTaxes())) {
			double serviceTaxAmount = getServiceTaxAmountForPaxType(fareWithBundleFareFee, availableIBOBFlightSegment,
					availLogicalCabinClass, PaxTypeTO.ADULT);
			flightFareSummaryTO.setServiceTaxes(AccelAeroCalculator.parseBigDecimal(serviceTaxAmount));
			flightFareSummaryTO.setServiceTaxPercentageForFares(getServiceTaxPercentageForFares(availableIBOBFlightSegment));
			flightFareSummaryTO.setTotalPrice(
					AccelAeroCalculator.add(flightFareSummaryTO.getServiceTaxes(), flightFareSummaryTO.getTotalPrice()));
		}
	}

	public static BigDecimal getServiceTaxPercentageForFares(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		double percentage = 0.0;
		if (!CollectionUtils.isEmpty(availableIBOBFlightSegment.getServiceTaxes())) {
			for (QuotedChargeDTO charge : availableIBOBFlightSegment.getServiceTaxes().values()) {
				if (charge.isChargeValueInPercentage()) {
					percentage += charge.getChargeValuePercentage();
				}
			}
		}
		return AccelAeroCalculator.parseBigDecimal(percentage);
	}

	public static double getServiceTaxAmountForPaxType(BigDecimal totalBaseFareAmount,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, String availLogicalCabinClass, String paxType) {
		double serviceTaxAmount = 0;
			Collection<QuotedChargeDTO> effectiveCharges = availableIBOBFlightSegment.getAllEffectiveCharges(availLogicalCabinClass);
			if (!CollectionUtils.isEmpty(effectiveCharges)) {
				for (QuotedChargeDTO effectiveCharge : effectiveCharges) {
					for (Map.Entry<String, QuotedChargeDTO> serviceTaxEntry :
							availableIBOBFlightSegment.getServiceTaxes().entrySet()) {
						// For non percentage taxes logic needs to be revisited
						if (serviceTaxEntry.getValue().isChargeValueInPercentage() && !effectiveCharge.isBundledFareCharge() && (
								CollectionUtils.isEmpty(availableIBOBFlightSegment.getServiceTaxExcludedCharges()) ||
										CollectionUtils.isEmpty(availableIBOBFlightSegment.getServiceTaxExcludedCharges()
												.get(serviceTaxEntry.getKey())) ||
										!availableIBOBFlightSegment.getServiceTaxExcludedCharges().get(serviceTaxEntry.getKey())
												.contains(effectiveCharge.getChargeCode()))) {
							serviceTaxAmount += serviceTaxEntry.getValue().getChargeValuePercentage() / 100.0 * effectiveCharge
									.getEffectiveChargeAmount(paxType);
						}
					}
				}
			}
		serviceTaxAmount += getServiceTaxAmountForFare(totalBaseFareAmount, availableIBOBFlightSegment);
		return serviceTaxAmount;
	}

	public static double getServiceTaxAmountForFare(BigDecimal fareAmount,
			AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		double serviceTaxAmount = 0;
		if (fareAmount != null) {
			for (Map.Entry<String, QuotedChargeDTO> serviceTaxEntry : availableIBOBFlightSegment.getServiceTaxes().entrySet()) {
				if (serviceTaxEntry.getValue().isChargeValueInPercentage()) {
					serviceTaxAmount += fareAmount.doubleValue() * serviceTaxEntry.getValue().getChargeValuePercentage() / 100.0;
				}
			}
		}
		return serviceTaxAmount;
	}

	private static BigDecimal getTotalFlexiCharges(Collection<FlexiRuleDTO> colFlexiRuleDTOs) {
		BigDecimal flexiTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (colFlexiRuleDTOs != null) {
			for (FlexiRuleDTO flexiRuleDTO : colFlexiRuleDTOs) {
				Double flexiVal = flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);
				if (flexiVal != null) {
					flexiTotal = AccelAeroCalculator.add(flexiTotal, AccelAeroCalculator.parseBigDecimal(flexiVal));
				}
			}
		}

		return flexiTotal;
	}

	/***
	 * Given the SelectedFlightDTO return OpenReturnOptionsTO which contain OpenReturn data(expiry dates) and airport
	 * data if available
	 * 
	 * @param selectedFlightDTO
	 * @return OpenReturnOptionsTO if available, null otherwise
	 * @throws ModuleException
	 */
	public static OpenReturnOptionsTO getOpenReturnOptions(SelectedFlightDTO selectedFlightDTO) throws ModuleException {
		OpenReturnOptionsTO orOpt = null;

		if (selectedFlightDTO == null || selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND) == null
				|| selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND).getFare(
						selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND).getSelectedLogicalCabinClass()) == null) {
			return orOpt;
		}

		OpenReturnPeriodsTO orPeriods = selectedFlightDTO.getOpenReturnPeriods();
		if (orPeriods != null) {
			return getOpenReturnOptions(orPeriods);
		}

		return orOpt;
	}

	/***
	 * Given the OpenReturnPeriodsTO return OpenReturnOptionsTO which contain OpenReturn data(expiry dates) and airport
	 * data if available
	 * 
	 * @param openReturnPeriodsTO
	 * @return OpenReturnOptionsTO if available, null otherwise
	 * @throws ModuleException
	 */
	public static OpenReturnOptionsTO getOpenReturnOptions(OpenReturnPeriodsTO orPeriods) throws ModuleException {
		OpenReturnOptionsTO orOpt = null;

		if (orPeriods != null) {

			try {
				Date cnfDate = null;
				Date trvlDate = null;
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				if (orPeriods.getConfirmBefore() != null) {
					cnfDate = sdf.parse(orPeriods.getConvertedConfirmBeforeDateForTheAirport());
					trvlDate = sdf.parse(orPeriods.getConvertedTravelExpiryForTheAirport());
				}
				if (cnfDate != null && trvlDate != null) {
					orOpt = new OpenReturnOptionsTO();
					orOpt.setConfirmExpiryDate(cnfDate);
					orOpt.setTravelExpiryDate(trvlDate);
					orOpt.setAirportCode(orPeriods.getAirportCode());
				}
			} catch (ParseException e) {
				throw new ModuleException("Error occured while parsing dates for open return expiry ", e.getStackTrace());
			}
		}

		return orOpt;
	}

	private static FlightSegmentTO getFlightSegmentTOFromFlightSegmentDTO(OndFareDTO ondFareDTO, int fltSegId,
			BaseAvailRQ priceQuoteRQ) {
		FlightSegmentDTO segDTO = ondFareDTO.getFlightSegmentDTO(fltSegId);
		FlightSegmentTO cmnSegDTO = new FlightSegmentTO();
		cmnSegDTO.setArrivalDateTime(segDTO.getArrivalDateTime());
		cmnSegDTO.setArrivalDateTimeZulu(segDTO.getArrivalDateTimeZulu());
		cmnSegDTO.setDepartureDateTime(segDTO.getDepartureDateTime());
		cmnSegDTO.setDepartureDateTimeZulu(segDTO.getDepartureDateTimeZulu());
		cmnSegDTO.setFlightNumber(segDTO.getFlightNumber());
		cmnSegDTO.setSegmentCode(segDTO.getSegmentCode());
		cmnSegDTO.setOperatingAirline(segDTO.getCarrierCode());
		cmnSegDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(segDTO));
		cmnSegDTO.setFlightSegId(segDTO.getSegmentId());
		cmnSegDTO.setFlightId(segDTO.getFlightId());
		cmnSegDTO.setArrivalTerminalName(segDTO.getArrivalTerminal());
		cmnSegDTO.setDepartureTerminalName(segDTO.getDepartureTerminal());
		cmnSegDTO.setDomesticFlight(segDTO.isDomesticFlight());
		cmnSegDTO.setSubJourneyGroup(ondFareDTO.getSubJourneyGroup());
		cmnSegDTO.setCsOcCarrierCode(segDTO.getCsOcCarrierCode());
		cmnSegDTO.setRequestedOndSequence(ondFareDTO.getRequestedOndSequence());
		cmnSegDTO.setOndSequence(ondFareDTO.getOndSequence());

		if (ondFareDTO.getSegmentSeatDistsDTO() != null) {
			for (SegmentSeatDistsDTO seatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
				if (seatDistsDTO.getFlightSegId() == fltSegId) {
					cmnSegDTO.setCabinClassCode(seatDistsDTO.getCabinClassCode());
					cmnSegDTO.setLogicalCabinClassCode(seatDistsDTO.getLogicalCabinClass());
					cmnSegDTO.setBookingType(seatDistsDTO.getBookingClassType());
				}
			}
		}
		cmnSegDTO.setOperationType(segDTO.getOperationTypeID());
		return cmnSegDTO;
	}

	public static void populateFareSummaryInfoForOndList(OriginDestinationInformationTO ond, Set<Integer> segmentIds,
			Collection<OndFareDTO> colNewFares, BaseAvailRQ priceQuoteRQ) {
		Iterator<OndFareDTO> iterFares = colNewFares.iterator();
		List<FlightSegmentTO> flightSegmentDTOs = new ArrayList<FlightSegmentTO>();
		OriginDestinationOptionTO ondOptionTO = new OriginDestinationOptionTO();
		List<FlightFareSummaryTO> flightFareSummaryList = new ArrayList<FlightFareSummaryTO>();
		boolean blnSeatAvailable = false;
		while (iterFares.hasNext()) {

			OndFareDTO ondFareDTO = iterFares.next();
			FlightFareSummaryTO flightFareSummaryTO = new FlightFareSummaryTO();

			if (segmentIds.containsAll(ondFareDTO.getSegmentsMap().keySet())) {

				blnSeatAvailable = (ondFareDTO.getSegmentSeatDistsDTO() != null && !ondFareDTO.getSegmentSeatDistsDTO().isEmpty())
						? true
						: false;
				flightFareSummaryTO.setSeatAvailable(blnSeatAvailable);
				flightFareSummaryTO.setBaseFareAmount(
						AccelAeroCalculator.parseBigDecimal(ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT)));
				int ondSequence = 0;
				if (ond.isReturnFlag()) {
					ondSequence = 1;
				}
				boolean isFlexiSelected = priceQuoteRQ.getAvailPreferences().getOndFlexiSelected().get(ondSequence) != null
						? priceQuoteRQ.getAvailPreferences().getOndFlexiSelected().get(ondSequence)
						: false;
				flightFareSummaryTO.setWithFlexi(isFlexiSelected);

				Iterator<Integer> iterSegs = ondFareDTO.getSegmentsMap().keySet().iterator();
				while (iterSegs.hasNext()) {
					Integer fltSegId = iterSegs.next();
					FlightSegmentTO fltSegTO = getFlightSegmentTOFromFlightSegmentDTO(ondFareDTO, fltSegId, priceQuoteRQ);
					fltSegTO.setReturnFlag(ond.isReturnFlag());
					for (PassengerTypeQuantityTO paxTypeTO : priceQuoteRQ.getTravelerInfoSummary()
							.getPassengerTypeQuantityList()) {
						if (PaxTypeTO.INFANT.equals(paxTypeTO.getPassengerType())) {
							fltSegTO.setInfantCount(paxTypeTO.getQuantity());
						} else if (PaxTypeTO.ADULT.equals(paxTypeTO.getPassengerType())) {
							fltSegTO.setAdultCount(paxTypeTO.getQuantity());
						} else if (PaxTypeTO.CHILD.equals(paxTypeTO.getPassengerType())) {
							fltSegTO.setChildCount(paxTypeTO.getQuantity());
						}
					}
					flightSegmentDTOs.add(fltSegTO);
				}
			}

			flightFareSummaryList.add(flightFareSummaryTO);
		}
		ondOptionTO.setFlightSegmentList(flightSegmentDTOs);
		ondOptionTO.setFlightFareSummaryList(flightFareSummaryList);
		ondOptionTO.setSelected(true);
		ondOptionTO.setSeatAvailable(blnSeatAvailable);

		ond.getOrignDestinationOptions().add(ondOptionTO);
	}

	public static String generateLogicalCCImageUrl(String logicalCabinCode, boolean isWithFlexi, String selectedLanguage) {
		String strImgpath = AppSysParamsUtil.getImageUploadPath();
		String flexiStr = isWithFlexi ? "Y" : "N";
		selectedLanguage = (selectedLanguage == null) ? Locale.ENGLISH.getLanguage() : selectedLanguage;
		StringBuilder imageUrl = new StringBuilder(strImgpath).append(logicalCabinCode).append(LogicalCabinClassDTO.SEPARATOR)
				.append(flexiStr).append(LogicalCabinClassDTO.SEPARATOR).append(selectedLanguage)
				.append(LogicalCabinClassDTO.IMG_SUFFIX);
		return imageUrl.toString();
	}
}
