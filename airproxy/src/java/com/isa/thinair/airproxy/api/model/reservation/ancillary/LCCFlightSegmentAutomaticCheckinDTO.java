package com.isa.thinair.airproxy.api.model.reservation.ancillary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

/**
 * @author aravinth.r
 * 
 */
public class LCCFlightSegmentAutomaticCheckinDTO implements Serializable, Comparable<LCCFlightSegmentAutomaticCheckinDTO> {

	private static final long serialVersionUID = 6253912902518397977L;
	private FlightSegmentTO flightSegmentTO;
	private List<LCCAutomaticCheckinDTO> automcaticCheckin;

	
	/**
	 * @return the flightSegmentTO
	 */
	public FlightSegmentTO getFlightSegmentTO() {
		return flightSegmentTO;
	}


	/**
	 * @param flightSegmentTO the flightSegmentTO to set
	 */
	public void setFlightSegmentTO(FlightSegmentTO flightSegmentTO) {
		this.flightSegmentTO = flightSegmentTO;
	}


	/**
	 * @return the automcaticCheckin
	 */
	public List<LCCAutomaticCheckinDTO> getAutomcaticCheckin() {
		if(automcaticCheckin == null)  {
			automcaticCheckin = new ArrayList<LCCAutomaticCheckinDTO>();
		}
		return automcaticCheckin;
	}


	/**
	 * @param automcaticCheckin the automcaticCheckin to set
	 */
	public void setAutomcaticCheckin(List<LCCAutomaticCheckinDTO> automcaticCheckin) {
		this.automcaticCheckin = automcaticCheckin;
	}

	@Override
	public int compareTo(LCCFlightSegmentAutomaticCheckinDTO o) {
		return this.getFlightSegmentTO().getDepartureDateTime().compareTo(o.getFlightSegmentTO().getDepartureDateTime());
	}

}
