package com.isa.thinair.airproxy.api.model.reservation.availability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CalendarSearchRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<FareCalendarOND> ondCalendars;

	private String currencyCode;

	public List<FareCalendarOND> getOndCalendars() {
		if (ondCalendars == null) {
			ondCalendars = new ArrayList<FareCalendarOND>();
		}
		return ondCalendars;
	}

	public void addOndCalendar(FareCalendarOND ondCal) {
		getOndCalendars().add(ondCal);
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
