package com.isa.thinair.airproxy.api.model.reservation.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PaymentSummaryTO implements Serializable {

	private static final long serialVersionUID = 9115658591248195429L;

	private Integer tnxId;

	private BigDecimal amount;

	private BigDecimal amountInPayCurrency;

	private String payCurrencyCode;

	private String agencyCode;

	private Date paymentDate;

	private String paymentReference;

	private Integer actualPaymentMethod;

	private String description;

	private String recieptNo;

	private String remarks;
	
	private String paymentTnxReference;

	private String dummyPayment;

	/**
	 * @return the tnxId
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            the tnxId to set
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the amoutInPayCurrency
	 */
	public BigDecimal getAmountInPayCurrency() {
		return amountInPayCurrency;
	}

	/**
	 * @param amountInPayCurrency
	 *            the amoutInPayCurrency to set
	 */
	public void setAmountInPayCurrency(BigDecimal amountInPayCurrency) {
		this.amountInPayCurrency = amountInPayCurrency;
	}

	/**
	 * @return the payCurrencyCode
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the agencyCode
	 */
	public String getAgencyCode() {
		return agencyCode;
	}

	/**
	 * @param agencyCode
	 *            the agencyCode to set
	 */
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public Integer getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	public String getRecieptNo() {
		return recieptNo;
	}

	public void setRecieptNo(String recieptNo) {
		this.recieptNo = recieptNo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDummyPayment() {
		return dummyPayment;
	}

	public void setDummyPayment(String dummyPayment) {
		this.dummyPayment = dummyPayment;
	}
	
	public String getPaymentTnxReference() {
		return paymentTnxReference;
	}

	public void setPaymentTnxReference(String paymentTnxReference) {
		this.paymentTnxReference = paymentTnxReference;
	}

}
