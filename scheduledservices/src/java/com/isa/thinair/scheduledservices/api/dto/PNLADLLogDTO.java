package com.isa.thinair.scheduledservices.api.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

public class PNLADLLogDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7456509720347399278L;
	// String errorLogFileName;
	String messageType;
	String generatedMessageType;
	Date processStartedDate;
	Date historyRecordInsertedTime;
	String flightID;
	String departureAirPort;
	String flightNumber;
	Date departureDate;
	Collection obtainedSitaAddresses;
	Date sitaAddressesObtainedTime;
	Date passengerListobtainedTime;
	String folderGenPNL;
	Date generatedPNLSavedTime; // TODO revise
								// name
								// GenFolderTransferTime
	Collection pnlADLEmailTxStatusDTOs;
	Date historyRecordUpdatedTime;
	String folderSendPNL;
	Date sentPNLSavedTime; // TODO revise
							// name
							// SendFolderTransferTime
	StackTraceElement[] stackTraceElements;
	Date exceptionOccurredTime;

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
	private SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getGeneratedMessageType() {
		return generatedMessageType;
	}

	public void setGeneratedMessageType(String generatedMessageType) {
		this.generatedMessageType = generatedMessageType;
	}

	public boolean isGeneratedMessageTypeDiffer() {
		return !generatedMessageType.equals(messageType);
	}

	public String getProcessStartedDate() {
		return simpleDateFormat.format(processStartedDate);
	}

	public void setProcessStartedDate(Date processStartedDate) {
		this.processStartedDate = processStartedDate;
	}

	public String getDepartureAirPort() {
		return departureAirPort;
	}

	public void setDepartureAirPort(String departureAirPort) {
		this.departureAirPort = departureAirPort;
	}

	public String getDepartureDate() {
		return simpleDateFormat.format(departureDate);
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getExceptionOccurredTime() {
		return simpleTimeFormat.format(exceptionOccurredTime);
	}

	public void setExceptionOccurredTime(Date exceptionOccurredTime) {
		this.exceptionOccurredTime = exceptionOccurredTime;
	}

	public String getFlightID() {
		return flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFolderGenPNL() {
		return folderGenPNL;
	}

	public void setFolderGenPNL(String folderGenPNL) {
		this.folderGenPNL = folderGenPNL;
	}

	public String getFolderSendPNL() {
		return folderSendPNL;
	}

	public void setFolderSendPNL(String folderSendPNL) {
		this.folderSendPNL = folderSendPNL;
	}

	public String getGeneratedPNLSavedTime() {
		return simpleTimeFormat.format(generatedPNLSavedTime);
	}

	public void setGeneratedPNLSavedTime(Date generatedPNLSavedTime) {
		this.generatedPNLSavedTime = generatedPNLSavedTime;
	}

	public String getHistoryRecordInsertedTime() {
		return simpleTimeFormat.format(historyRecordInsertedTime);
	}

	public void setHistoryRecordInsertedTime(Date historyRecordInsertedTime) {
		this.historyRecordInsertedTime = historyRecordInsertedTime;
	}

	public String getHistoryRecordUpdatedTime() {
		return simpleTimeFormat.format(historyRecordUpdatedTime);
	}

	public void setHistoryRecordUpdatedTime(Date historyRecordUpdatedTime) {
		this.historyRecordUpdatedTime = historyRecordUpdatedTime;
	}

	public Collection getObtainedSitaAddresses() {
		return obtainedSitaAddresses;
	}

	public void setObtainedSitaAddresses(Collection obtainedSitaAddresses) {
		this.obtainedSitaAddresses = obtainedSitaAddresses;
	}

	public String getPassengerListobtainedTime() {
		return simpleTimeFormat.format(passengerListobtainedTime);
	}

	public void setPassengerListobtainedTime(Date passengerListobtainedTime) {
		this.passengerListobtainedTime = passengerListobtainedTime;
	}

	public Collection getPnlADLEmailTxStatusDTOs() {
		return pnlADLEmailTxStatusDTOs;
	}

	public void setPnlADLEmailTxStatusDTOs(Collection pnlADLEmailTxStatusDTOs) {
		this.pnlADLEmailTxStatusDTOs = pnlADLEmailTxStatusDTOs;
	}

	public String getSentPNLSavedTime() {
		return simpleTimeFormat.format(sentPNLSavedTime);
	}

	public void setSentPNLSavedTime(Date sentPNLSavedTime) {
		this.sentPNLSavedTime = sentPNLSavedTime;
	}

	public String getSitaAddressesObtainedTime() {
		return simpleTimeFormat.format(sitaAddressesObtainedTime);
	}

	public void setSitaAddressesObtainedTime(Date sitaAddressesObtainedTime) {
		this.sitaAddressesObtainedTime = sitaAddressesObtainedTime;
	}

	public Collection getStackTraceElements() {
		return Arrays.asList(stackTraceElements);
	}

	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}
	// public String getErrorLogFileName() {
	// return errorLogFileName;
	// }
	// public void setErrorLogFileName(String errorLogFileName) {
	// this.errorLogFileName = errorLogFileName;
	// }

}
