package com.isa.thinair.scheduledservices.api.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PNLADLEmailTxStatusDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5792324168180534746L;
	String emailID;
	Date emailSentDate;
	Date historyRecordInsertedTime;

	private SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getEmailSentDate() {
		return simpleTimeFormat.format(emailSentDate);
	}

	public void setEmailSentDate(Date emailSentDate) {
		this.emailSentDate = emailSentDate;
	}

	public String getHistoryRecordInsertedTime() {
		return simpleTimeFormat.format(historyRecordInsertedTime);
	}

	public void setHistoryRecordInsertedTime(Date historyRecordInsertedTime) {
		this.historyRecordInsertedTime = historyRecordInsertedTime;
	}

}