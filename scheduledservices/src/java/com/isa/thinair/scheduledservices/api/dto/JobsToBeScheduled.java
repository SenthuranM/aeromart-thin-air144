package com.isa.thinair.scheduledservices.api.dto;

import java.io.Serializable;
import java.util.Date;

public class JobsToBeScheduled implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6437056944342047902L;

	private String jobId;

	/** A type from SchedulerJobTypes in ScheduleServicesConstants */
	private String jobType;

	private Date executionTime;

	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
}
