package com.isa.thinair.scheduledservices.api.dto;

import java.io.Serializable;
import java.util.Date;

public class JobsToBeScheduledDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7715807477106937661L;
	String jobId;
	String jobType; // A type from SchedulerJobTypes in
					// ScheduleServicesConstants
	Date executionTime;

	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
}
