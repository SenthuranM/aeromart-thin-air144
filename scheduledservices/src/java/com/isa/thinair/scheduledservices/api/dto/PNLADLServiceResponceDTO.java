package com.isa.thinair.scheduledservices.api.dto;

import java.io.Serializable;

public class PNLADLServiceResponceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7883278394936565741L;
	String jobStatus;
	String errorFileName;
	String messageType;

	public String getErrorFileName() {
		return errorFileName;
	}

	public void setErrorFileName(String errorFileName) {
		this.errorFileName = errorFileName;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

}
