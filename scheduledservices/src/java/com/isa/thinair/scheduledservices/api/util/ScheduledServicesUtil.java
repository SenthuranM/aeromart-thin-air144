package com.isa.thinair.scheduledservices.api.util;

import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class ScheduledServicesUtil {

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}
}
