package com.isa.thinair.scheduledservices.api.dto;

public class ReportConfig {

	public static final String DEFUAL_FILE_EXTENSION = ".zip";

	public enum DeliveryMode {
		ATTACHMENT, LINK
	}

	private DeliveryMode deliveryMode;
	private String uploadPath;
	private String fileExtension;
	private boolean copyToPublicDir;

	public ReportConfig() {
		fileExtension = DEFUAL_FILE_EXTENSION;
		copyToPublicDir = true;
	}

	public DeliveryMode getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(DeliveryMode deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public boolean isCopyToPublicDir() {
		return copyToPublicDir;
	}

	public void setCopyToPublicDir(boolean copyToPublicDir) {
		this.copyToPublicDir = copyToPublicDir;
	}
}
