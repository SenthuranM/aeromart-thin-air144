package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author isuru
 */
public class ReservationReleaser {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ReservationReleaser.class);

	public static void releaseOnHoldReservations() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			Date currentDate = new Date();
			ScheduledservicesUtils.getReservationBD().expireOnholdReservations(currentDate, null, null, null);
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
