package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ReleaseFixedAllocationSeatsJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(ReleaseFixedAllocationSeatsJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		synchronized (this) {
			try {
				log.info("############# Starting ReleaseFixedAllocationSeatsJob ");
				CredentialInvokerUtil.invokeCredentials();
				ScheduledservicesUtils.getFlightInventoryBD().releaseFixedAllocations();
				log.info("############# Finished ReleaseFixedAllocationSeatsJob ");
			} catch (Exception ce) {
				log.error("############# Error ReleaseFixedAllocationSeatsJob " + ce);
				throw new JobExecutionException(ce);
			} finally {
				CredentialInvokerUtil.close();
			}
		}

	}

}
