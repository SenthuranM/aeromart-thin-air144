package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class SitaTexMessageRetrieveJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(SitaTexMessageRetrieveJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("######SITATEXT######## Retrieving PFS, ETL, PRL messages Start At" + new Date());
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getMessageBrokerServiceBD().receiveMessage(false);
			log.info("######SITATEXT######## Retrieving PFS, ETL, PRL messages End At" + new Date());
		} catch (Exception e) {
			log.error("######SITATEXT######## Error while retrieving PFS, ETL, PRL messages" + e);
			throw new JobExecutionException(e);
		}
	}
}
