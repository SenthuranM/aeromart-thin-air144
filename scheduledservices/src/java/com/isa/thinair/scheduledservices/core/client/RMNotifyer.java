package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Byorn
 */
public class RMNotifyer {
	private static final Log log = LogFactory.getLog(RMNotifyer.class);

	public static void notifyRM() throws Exception {
		try {
			log.info("#############################################");
			log.info("########## Starting to Notify RM    #########");

			CredentialInvokerUtil.invokeCredentials();

			ScheduledservicesUtils.getWSClientBD().publishRMInventoryAlerts();
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
