package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class CancelLMSBlockedCreditsJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(CancelLMSBlockedCreditsJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("######### CancelLMSBlockedCreditsJob  invoked ##########");
		log.info("######### CancelLMSBlockedCreditsJob started at " + new Date());

		try {
			CredentialInvokerUtil.invokeCredentials();

			ScheduledservicesUtils.getReservationBD().cancelFailedLMSBlockedCreditPayments();
		} catch (ModuleException e) {
			log.error("Error @ CancelLMSBlockedCreditsJob " + e.getMessage());
		}
		log.info("######### CancelLMSBlockedCreditsJob ended at " + new Date());

	}
}
