package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

public class GSTAdditionalReportGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return ScheduledservicesUtils.getDataExtractionBD().getGSTAdditionalReportData(reportsSearchCriteria);
	}

	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}
}
