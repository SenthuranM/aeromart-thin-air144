package com.isa.thinair.scheduledservices.core.client.rak;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class LastPublishRakInsuranceRecoveryJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(LastPublishRakInsuranceRecoveryJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			reschedulePublishingFlightDetails();
		} catch (Exception e) {
			log.error("Recovery Schedule Ancillary Reminders failed", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void reschedulePublishingFlightDetails() throws JobExecutionException {
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		int timeFraction = 5;
		Collection<InsurancePublisherDetailDTO> failedFightSegInfo = ScheduledservicesUtils.getFlightInventoryBD()
				.getFailedInsuredFlightData();
		if (log.isDebugEnabled()) {
			log.debug("Found " + failedFightSegInfo.size() + " failed flight segments to be re-notified");
		}

		for (InsurancePublisherDetailDTO insurancePublisherDTO : failedFightSegInfo) {
			String jobId = "RECLASTPUBLISH" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
					+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
					+ insurancePublisherDTO.getDepartureTimeZulu();

			try {
				if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION_RECOVERY)) {
					log.info("Recovery Schedule Publish Rak insurance [Skipping Scheduling Recovery Job as Scheduled JOB_ID="
							+ jobId + "]");
					continue;
				}

				ScheduledservicesUtils.getFlightBD().updateFlightSegmentNotifyStatus(insurancePublisherDTO.getFlightSegId(),
						insurancePublisherDTO.getFlightSegmentNotificationEventId(), "RESCHEDULED", "", 0, "INS");
				JobDetail notificationJobDetail = new JobDetail();

				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_GROUP,
						SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightId,
						insurancePublisherDTO.getFlightId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DepartureStation,
						insurancePublisherDTO.getFlightOrigin());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_NUMBER,
						insurancePublisherDTO.getFlightNumber());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightSegId,
						insurancePublisherDTO.getFlightSegId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
						insurancePublisherDTO.getFlightSegmentNotificationEventId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_LOCAL,
						insurancePublisherDTO.getDeparturetimeLocal());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_ZULU,
						insurancePublisherDTO.getDepartureTimeZulu());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_STATUS,
						insurancePublisherDTO.getStatus());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFICATION_TYPE, "INS");

				notificationJobDetail.setJobClass(PublishRakInsuranceInfo.class);
				GregorianCalendar calendar1 = new GregorianCalendar();
				calendar1.setTime(insurancePublisherDTO.getDepartureTimeZulu());
				int lastPublishCutover = Integer.parseInt(AppSysParamsUtil.getRakLastPublishCutoverTime());

				calendar1.add(GregorianCalendar.MINUTE, -lastPublishCutover);
				Date notificationStartTime = calendar1.getTime();

				if (notificationStartTime.before(currentTimestamp)) {
					timeFraction = timeFraction + 2;
					// TODO: Clarify how this time should be calculated
					Date adjNotificationStartTime = new Date(new Timestamp(notificationStartTime.getTime() + 60 * timeFraction
							* 1000).getTime());
					log.info("Reschedule Insurance publishing [Scheduling Delivery Passed Job JOB_ID=" + jobId
							+ ", ExpectedSendTime=" + notificationStartTime + ", AdjustedSendTime=" + adjNotificationStartTime
							+ "]");
					notificationStartTime = adjNotificationStartTime;
				}
				try {
					ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
					log.info("Reschedule Insurance Publishing [Scheduled Job JOB_ID=" + jobId + ", Notification Send Timestamp="
							+ notificationStartTime + "]");
				} catch (SchedulerException se) {
					log.error("Reschedule Insurance Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
					// FIXME - handle exception
				} catch (Exception e) {
					log.error("RescheduleInsurance Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
					notificationJobDetail.setRequestsRecovery(true);
				}
			} catch (SchedulerException e) {
				log.error("Recovery Schedule Insurance Publishing Failed", e);
				throw new JobExecutionException(e);
			}

		}
	}

}
