package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class BSPSalesJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(BSPSalesJob.class);

	private int salesFrequencyInDays;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			BSPSales.execute(salesFrequencyInDays);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("BSP Sales job failed", e);
			}
			throw new JobExecutionException(e);
		}
	}

	public void setSalesFrequencyInDays(int salesFrequencyInDays) {
		this.salesFrequencyInDays = salesFrequencyInDays;
	}

	public int getSalesFrequencyInDays() {
		return salesFrequencyInDays;
	}

}
