package com.isa.thinair.scheduledservices.core;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class GoshowBCOpener implements Job {

	private static Log log = LogFactory.getLog(GoshowBCOpener.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("############################## OPENING GOSHO BOOKING CLASS JOB IS STARTED " + new Date());
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();

			String fltSegId = jobDetail.getJobDataMap().get(Job.PROP_FlightSegId).toString();

			FlightSegmentDTO flightSegDTO = new FlightSegmentDTO();
			flightSegDTO.setSegmentId(new Integer(fltSegId));

			ScheduledservicesUtils.getFlightInventoryBD().openGoshowBcInventory(flightSegDTO);
			log.info("############################## OPENING GOSHO BOOKING CLASS JOB IS COMPLETED " + new Date());
		} catch (Exception me) {
			log.error("OPENING GOSHO BOOKING CLASS JOB IS FAILED", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
