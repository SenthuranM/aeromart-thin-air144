package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class SendSSMRecap {
	
	private static final Log log = LogFactory.getLog(SendSSMRecap.class);



	public static void execute(int gdsID, Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress) {	
		log.info("######### SSM Recap Job invoked ##########");
		log.info("######### SSM Recap Job started at " + new Date());
		try {
			ScheduledservicesUtils.getScheduleBD().publishSchedulesToGDS(gdsID,scheduleStartDate,scheduleEndDate,isSelectedSchedules,emailAddress);
			
		} catch (ModuleException e) {
			e.printStackTrace();
		}	
		log.info("######### SSM Recap Job ended at " + new Date());
		
	}
}
