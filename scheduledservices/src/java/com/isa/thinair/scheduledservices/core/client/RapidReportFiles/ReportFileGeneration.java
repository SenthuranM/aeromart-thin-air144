package com.isa.thinair.scheduledservices.core.client.RapidReportFiles;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.invoicing.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.client.BSPSales;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;

public class ReportFileGeneration {
	private static Log log = LogFactory.getLog(BSPSales.class);

	public static void execute(int salesFrequncyInDays, ReportFileTypes.FileTypes file) throws Exception {
		log.info("######### Ret File generation Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();

			Date date = new Date();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
			date = fmt.parse(fmt.format(date)); // Convert the date to yyyy/MM/dd 00:00

			Date toDate = new Date(date.getTime() - TimeUnit.SECONDS.toMillis(1));

			// fromDate = (yyyy/MM/(dd-salesFrequncyInDays) 00:00)
			Date fromDate = new Date(date.getTime() - TimeUnit.DAYS.toMillis(salesFrequncyInDays));
			log.info("######### Ret file generation Job s" + "tarted for RET file generation at " + new Date()
					+ "for sales Frequency" + salesFrequncyInDays);
			ScheduledservicesUtils.getInvoicingBD().generateReportFile(fromDate, toDate, false, file);
			log.info("######### Ret file generation Job ended RET file generation at " + new Date());
		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "RET File creation failed!");
		}

		/*
		 * This is to make the ret file upload recovery try {
		 * log.info("######### Ret file upload Recovery  ##########"); //
		 * ScheduledservicesUtils.getInvoicingBD().uploadPendingRETFiles();
		 * log.info("######### Ret file genetaed Job ended for RET file upload ##########");
		 * 
		 * } catch (Exception e) { log.error(e); CommonUtil.reportSheduleFailure(e,
		 * "Ret file recovery scheduler Failed!"); throw e; } finally { CredentialInvokerUtil.close(); }
		 */
		log.info("#########Ret file generation Job ended at " + new Date());
	}
}
