package com.isa.thinair.scheduledservices.core.client.rak;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

/**
 * RAK Insurance Sub Job Class
 */
public class PublishRakInsuranceInfo implements Job {
	private static Log log = LogFactory.getLog(PublishRakInsuranceInfo.class);

	public void execute(JobExecutionContext executionContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = executionContext.getJobDetail();

			String jobId = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
			String jobGroupName = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
			String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
			Integer flightID = jobDetail.getJobDataMap().getInt(Job.PROP_FlightId);
			Integer flightSegmentId = jobDetail.getJobDataMap().getInt(Job.PROP_FlightSegId);
			Integer flightSegNotiEventId = (jobDetail.getJobDataMap().get(Job.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID) != null)
					? jobDetail.getJobDataMap().getInt(Job.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID)
					: null;
			String flightOriginAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
			Date departureDateTimeLocal = (Date) jobDetail.getJobDataMap().get(Job.PROP_DEPARTURE_DATE_LOCAL);
			Date departureDateTimeZulu = (Date) jobDetail.getJobDataMap().get(Job.PROP_DEPARTURE_DATE_ZULU);
			String status = (jobDetail.getJobDataMap().get(Job.PROP_INSURANCE_PUBLISH_STATUS) != null) ? jobDetail
					.getJobDataMap().getString(Job.PROP_INSURANCE_PUBLISH_STATUS) : null;
			String notifyType = jobDetail.getJobDataMap().getString(Job.PROP_NOTIFICATION_TYPE);
			String notifyResult = jobDetail.getJobDataMap().getString(Job.PROP_NOTIFY_RESULT);
			String publishType = jobDetail.getJobDataMap().getString(Job.PROP_INSURANCE_PUBLISH_TYPE);
			Integer notifyCount = jobDetail.getJobDataMap().getInt(Job.PROP_NOTIFY_COUNT);
			Boolean isRecovery = jobDetail.getJobDataMap().getBoolean(Job.PROP_IS_RECOVERY);

			InsurancePublisherDetailDTO insurancePublisherDetailDTO = new InsurancePublisherDetailDTO();
			insurancePublisherDetailDTO.setJobName(jobId);
			insurancePublisherDetailDTO.setJobGroupName(jobGroupName);
			insurancePublisherDetailDTO.setFlightNumber(flightNumber);
			insurancePublisherDetailDTO.setFlightId(flightID);
			insurancePublisherDetailDTO.setFlightOrigin(flightOriginAirport);
			insurancePublisherDetailDTO.setFlightSegId(flightSegmentId);
			insurancePublisherDetailDTO.setFlightSegmentNotificationEventId(flightSegNotiEventId);
			insurancePublisherDetailDTO.setDeparturetimeLocal(departureDateTimeLocal);
			insurancePublisherDetailDTO.setDepartureTimeZulu(departureDateTimeZulu);
			insurancePublisherDetailDTO.setStatus(status);
			insurancePublisherDetailDTO.setInsResponse(notifyResult);
			insurancePublisherDetailDTO.setRecovery(isRecovery);
			insurancePublisherDetailDTO.setPublishType(publishType);

			log.info(" -------------------------------------------------------------------------------------------------------");
			log.info(" #####################     RAK INSURANCE PUBLICATION SUB JOB EXECUTION STARTED ######################");
			log.info(" #####################     FLIGHT NUMBER		    :     " + flightNumber);
			log.info(" #####################     FLIGHT ID 	            : 	  " + flightID);
			log.info(" #####################     FLIGHT SEG NUMBER   	:     " + flightSegmentId);
			log.info(" #####################     DEPATURE STATION	    :     " + flightOriginAirport);
			log.info(" #####################     PULISH TYPE 			:     " + publishType);
			log.info(" #####################     CURRENT STATUS 		:     " + status);
			log.info(" #####################     IS RECOVERY 			:     " + (isRecovery ? "YES" : "NO"));
			log.info(" -------------------------------------------------------------------------------------------------------");

			log.info(" ----------------------------MAIN JOB EXECUTION STARTED------------------------------------------");
			FlightSegNotificationEvent notifyEvent = publishRakInsuranceFlightData(insurancePublisherDetailDTO);
			log.info(" ----------------------------MAIN JOB EXECUTION COMPLETED------------------------------------------");
			SchedulerBD schedulerBD = ReservationModuleUtils.getSchedulerBD();
			schedulerBD.removeJob(jobId, jobGroupName);

			SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
			Integer minGapInMinsBwJobs = sc.getMinGapInMinitesBWJobs();

			if (status == null || status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT)
					|| status.equals("RAK_INS_" + publishType + "_UPLOAD_FAILED")) {

				if (notifyEvent != null) {

					if (notifyEvent.getAncillaryNotifyStatus() != null
							&& notifyEvent.getAncillaryNotifyStatus().equals("RAK_INS_" + publishType + "_UPLOAD_SENT")) {

						log.info(" ----------------------------SCHEDULING NEW JOB TO CHECK UPLOAD STATUS STARTED------------------------------------------");
						jobId = "RAKPUBLISH_" + publishType + "_RESULT" + "/" + flightNumber + "/" + flightID + "/"
								+ flightSegmentId + "/" + departureDateTimeZulu + "/" + isRecovery;
						Date notificationStartTime = new Date(System.currentTimeMillis()
								+ (60000 * Integer.parseInt(AppSysParamsUtil.getRakUploadStatusCheckGap())));
						ScheduleNewJob(jobId, jobGroupName, flightID, flightNumber, flightOriginAirport, flightSegmentId,
								notifyEvent.getFlightMsgEventId(), departureDateTimeZulu, departureDateTimeLocal,
								notifyEvent.getAncillaryNotifyStatus(), publishType, notifyType, 1,
								notifyEvent.getInsFirstResponse(), isRecovery, notificationStartTime);
						log.info(" ----------------------------SCHEDULING NEW JOB TO CHECK UPLOAD STATUS ENDED------------------------------------------");

					} else if (notifyEvent.getAncillaryNotifyStatus() != null
							&& notifyEvent.getAncillaryNotifyStatus().equals(
									InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT)) {

						// Check max from db then schedule
						notifyCount = ReservationModuleUtils.getFlightBD().getFlightSegNotificationAttempts(flightSegNotiEventId);
						if (notifyCount < Integer.parseInt(BeanUtils.nullHandler(AppSysParamsUtil.getRakMaxPublishRetries()))) {

							log.info("Insurance upload failure... rescheduling upload again");
							log.info(" ----------------------------SCHEDULING SAME JOB AGAIN STARTED------------------------------------------");
							Date notificationStartTime = new Date(System.currentTimeMillis() + (60000 * minGapInMinsBwJobs));
							ScheduleNewJob(jobId, jobGroupName, flightID, flightNumber, flightOriginAirport, flightSegmentId,
									notifyEvent.getFlightMsgEventId(), departureDateTimeZulu, departureDateTimeLocal,
									notifyEvent.getAncillaryNotifyStatus(), publishType, notifyType, notifyCount, null,
									isRecovery, notificationStartTime);
							log.info(" ----------------------------SCHEDULING SAME JOB AGAIN ENDED------------------------------------------");
						}

					}

				} else {

					notifyCount = ReservationModuleUtils.getFlightBD().getFlightSegNotificationAttempts(flightSegNotiEventId);

					if (notifyCount < Integer.parseInt(BeanUtils.nullHandler(AppSysParamsUtil.getRakMaxPublishRetries()))) {

						log.info("Insurance upload failure... rescheduling upload again");
						log.info(" ----------------------------SCHEDULING SAME JOB AGAIN STARTED------------------------------------------");
						Date notificationStartTime = new Date(System.currentTimeMillis() + (60000 * minGapInMinsBwJobs));
						ScheduleNewJob(jobId, jobGroupName, flightID, flightNumber, flightOriginAirport, flightSegmentId,
								flightSegNotiEventId, departureDateTimeZulu, departureDateTimeLocal, status, publishType,
								notifyType, notifyCount, null, isRecovery, notificationStartTime);
						log.info(" ----------------------------SCHEDULING SAME JOB AGAIN ENDED------------------------------------------");

					}

				}

			} else {

				if (notifyEvent != null) {

					if (notifyEvent.getAncillaryNotifyStatus() != null
							&& notifyEvent.getAncillaryNotifyStatus().equals("RAK_INS_" + publishType + "_UPLOAD_FAILED")) {

						notifyCount = ReservationModuleUtils.getFlightBD().getFlightSegNotificationAttempts(flightSegNotiEventId);

						if (notifyCount < Integer.parseInt(BeanUtils.nullHandler(AppSysParamsUtil.getRakMaxPublishRetries()))) {

							log.info("Retrieved insurance upload status is failed... rescheduling upload again");
							log.info(" ----------------------------SCHEDULING NEW JOB TO UPLOAD AGAIN STARTED------------------------------------------");
							Date notificationStartTime = new Date(System.currentTimeMillis() + (60000 * minGapInMinsBwJobs));
							jobId = "RAKPUBLISH_" + publishType + "/" + flightNumber + "/" + flightID + "/" + flightSegmentId
									+ "/" + departureDateTimeZulu + "/" + isRecovery;
							ScheduleNewJob(jobId, jobGroupName, flightID, flightNumber, flightOriginAirport, flightSegmentId,
									flightSegNotiEventId, departureDateTimeZulu, departureDateTimeLocal,
									notifyEvent.getAncillaryNotifyStatus(), publishType, notifyType, notifyCount, null,
									isRecovery, notificationStartTime);
							log.info(" ----------------------------SCHEDULING NEW JOB TO UPLOAD AGAIN ENDED------------------------------------------");
						}

					}

				} else {

					notifyCount++;

					if (notifyCount < Integer.parseInt(BeanUtils.nullHandler(AppSysParamsUtil.getRakMaxPublishRetries()))) {
						log.info("Could not retrieve upload status... rescheduling upload status check again");
						log.info(" ----------------------------SCHEDULING SAME JOB AGAIN STARTED------------------------------------------");
						Date notificationStartTime = new Date(System.currentTimeMillis() + (60000 * minGapInMinsBwJobs));
						ScheduleNewJob(jobId, jobGroupName, flightID, flightNumber, flightOriginAirport, flightSegmentId,
								flightSegNotiEventId, departureDateTimeZulu, departureDateTimeLocal, status, publishType,
								notifyType, notifyCount, null, isRecovery, notificationStartTime);
						log.info(" ----------------------------SCHEDULING SAME JOB AGAIN ENDED------------------------------------------");
					}

				}

			}
			log.info("#################### RAK INSURANCE PUBLICATION SUB JOB EXECUTION ENDED  #############################");

		} catch (Exception e) {
			log.error("PublishRakInsuranceInfo ", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private FlightSegNotificationEvent publishRakInsuranceFlightData(InsurancePublisherDetailDTO insurancePublisherDetailDTO)
			throws ModuleException {
		return ScheduledservicesUtils.getReservationBD().publishRakInuranceData(insurancePublisherDetailDTO);
	}

	private void ScheduleNewJob(String jobId, String jobGroupName, Integer flightID, String flightNumber, String originAirport,
			Integer flightSegId, Integer notifyId, Date DepTimeZulu, Date DepTimeLocal, String insPubStatus, String pubType,
			String notifyType, Integer notifyCount, String notifyResult, Boolean isRecovery, Date notificationStartTime) {

		JobDetail notificationJobDetail = new JobDetail();

		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_GROUP,
				SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightId, flightID);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DepartureStation, originAirport);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_NUMBER, flightNumber);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightSegId, flightSegId);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID, notifyId);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_LOCAL, DepTimeLocal);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_ZULU, DepTimeZulu);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_STATUS, insPubStatus);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_TYPE, pubType);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFICATION_TYPE, "INS");
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_COUNT, notifyCount);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_RESULT, notifyResult);
		notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_IS_RECOVERY, isRecovery);
		notificationJobDetail.setJobClass(PublishRakInsuranceInfo.class);

		try {
			// Schedule the job if its not already scheduled
			if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION)) {

				ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
				log.info("Schedule Insurance Publishing [Scheduled Job JOB_ID=" + jobId + ", Notification Send Timestamp="
						+ notificationStartTime + "]");
			}
		} catch (SchedulerException se) {
			log.error("Schedule Insurance Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
		} catch (Exception e) {
			log.error("ScheduleInsurance Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
		}

	}
}
