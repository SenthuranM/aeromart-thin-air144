package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.CalendarUtil;

public class CreditCardSalesPosterJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		synchronized (this) {
			try {
				Date d = CalendarUtil.getLastMomentOfPreviousDaY();
				CreditCardSalesPoster.transferCreditCardSales(CalendarUtil.formatForSQL(d, "dd-MM-yyyy"));
			} catch (Exception e) {
				throw new JobExecutionException(e);
			}
		}
	}
}
