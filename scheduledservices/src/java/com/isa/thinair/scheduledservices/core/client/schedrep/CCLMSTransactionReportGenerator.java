package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class CCLMSTransactionReportGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		ResultSet resultSet = null;
		String reportOption = reportsSearchCriteria.getReportOption();
		Boolean isDetailReport = reportsSearchCriteria.isDetailReport();

		if (reportOption.equals("CC_LMS_TRAN") && isDetailReport) {

			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionsLMSPointsDetailQuery(reportsSearchCriteria);

		} else if (reportOption.equals("CC_LMS_TRAN")) {

			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionsLMSPointsData(reportsSearchCriteria);

		} else if (reportOption.equals("CC_LMS_TRAN_DETAIL")) {

			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionsLMSPointsDetailQuery(reportsSearchCriteria);

		}

		return resultSet;
	}

	@Override
	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy");
	}
}
