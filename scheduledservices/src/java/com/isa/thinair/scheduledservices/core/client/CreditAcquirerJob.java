package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class CreditAcquirerJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CreditAcquirer.acquireExpiredCredit();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
}
