package com.isa.thinair.scheduledservices.core.client;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PalCalResender {

	public static void checkAndResend() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationAuxilliaryBD().checkAndResendPALCAL();
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
