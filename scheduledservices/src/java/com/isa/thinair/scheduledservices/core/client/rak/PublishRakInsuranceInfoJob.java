package com.isa.thinair.scheduledservices.core.client.rak;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class PublishRakInsuranceInfoJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(PublishRakInsuranceInfoJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
			log.info("###################### START	- Main Scheduler Job for RAK Insurace Publication [CurrentTime = "
					+ currentTimestamp + "] ######################");
			schedulePublishingFlightDetails();
			log.info("###################### END 	- Main Scheduler Job for RAK Insurace Publication [CurrentTime = "
					+ currentTimestamp + "] ######################");
		} catch (Exception e) {
			log.error("Main Scheduler Job for RAK Insurace Publication Failed", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void schedulePublishingFlightDetails() throws JobExecutionException {
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		int timeFraction = 5;
		try {

			Collection<InsurancePublisherDetailDTO> flightInfo = ScheduledservicesUtils.getFlightInventoryBD()
					.getInsuredFlightData();
			if (log.isDebugEnabled()) {
				log.debug("Found " + flightInfo.size() + " flight segments to be notified");
			}
			
			for (InsurancePublisherDetailDTO insurancePublisherDTO : flightInfo) {

				log.info(" ---------------------------------------------------------------------------------------");
				log.info(" #####################     RAK INSURANCE SUB JOB SCHEDULING STARTED FOR ######################");
				log.info(" #####################     FLIGHT NUMBER          :     " + insurancePublisherDTO.getFlightNumber());
				log.info(" #####################     FLIGHT ID              : 	  " + insurancePublisherDTO.getFlightId());
				log.info(" #####################     FLIGHT SEG NUMBER      :     " + insurancePublisherDTO.getFlightSegId());
				log.info(" #####################     DEPATURE STATION       :     " + insurancePublisherDTO.getFlightOrigin());
				log.info(" ---------------------------------------------------------------------------------------");

				// =================================== First Publish Sub Job Scheduling Segment Start
				// ================================================
				log.info(" #####################     FIRST PUBLISH SUB JOB SCHEDULING STARTED    ######################");

				String jobId = "RAKPUBLISH_FIRST" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();

				JobDetail notificationJobDetail = new JobDetail();
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_GROUP,
						SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightId,
						insurancePublisherDTO.getFlightId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DepartureStation,
						insurancePublisherDTO.getFlightOrigin());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_NUMBER,
						insurancePublisherDTO.getFlightNumber());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightSegId,
						insurancePublisherDTO.getFlightSegId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
						insurancePublisherDTO.getFlightSegmentNotificationEventId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_LOCAL,
						insurancePublisherDTO.getDeparturetimeLocal());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_ZULU,
						insurancePublisherDTO.getDepartureTimeZulu());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_STATUS,
						insurancePublisherDTO.getStatus());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_TYPE,
						InsurancePublisherDetailDTO.PUBLISH_FIRST);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFICATION_TYPE, "INS");
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_COUNT, 1);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_RESULT, "");
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_IS_RECOVERY,
						insurancePublisherDTO.isRecovery());
				notificationJobDetail.setJobClass(PublishRakInsuranceInfo.class);

				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(insurancePublisherDTO.getDepartureTimeZulu());
				int firstPublishCutover = Integer.parseInt(AppSysParamsUtil.getRakFirstPublishCutoverTime());
				calendar.add(GregorianCalendar.MINUTE, -firstPublishCutover);
				Date notificationStartTime = calendar.getTime();

				if (notificationStartTime.before(currentTimestamp)) {
					timeFraction = timeFraction + 2;
					Date adjNotificationStartTime = new Date(
							new Timestamp(currentTimestamp.getTime() + 60 * timeFraction * 1000).getTime());
					log.info("Schedule Insurance publishing [Scheduling Delivery Passed Job JOB_ID=" + jobId
							+ ", ExpectedSendTime=" + notificationStartTime + ", AdjustedSendTime=" + adjNotificationStartTime
							+ "]");
					notificationStartTime = adjNotificationStartTime;
				}
				try {

					if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION)) {
						ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
						log.info("Schedule Insurance Publishing [Scheduled Job JOB_ID=" + jobId
								+ ", Notification Send Timestamp=" + notificationStartTime + "]");
					} else {
						log.info("Job is already scheduled. Skipping scheduling ... [JOB_ID=" + jobId + "]");
					}

				} catch (SchedulerException se) {
					log.error("Schedule Insurance Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
				} catch (Exception e) {
					log.error("ScheduleInsurance Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
					notificationJobDetail.setRequestsRecovery(true);
				}
				log.info(" #####################     FIRST PUBLISH SUB JOB SCHEDULING ENDED    ######################");
				// =================================== First Publish Sub Job Scheduling Segment End
				// ================================================

				// =================================== Last Publish Sub Job Scheduling Segment Start
				// ================================================
				log.info(" #####################     LAST PUBLISH SUB JOB SCHEDULING STARTED    ######################");
				jobId = "RAKPUBLISH_LAST" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();

				if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION)) {
					log.info("Job is already scheduled. Skipping scheduling ... [JOB_ID=" + jobId + "]");
					log.info(" #####################     LAST PUBLISH SUB JOB SCHEDULING ENDED    ######################");
					continue;
				}

				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_TYPE,
						InsurancePublisherDetailDTO.PUBLISH_LAST);

				int lastPublishCutover = Integer.parseInt(AppSysParamsUtil.getRakLastPublishCutoverTime());
				calendar.add(GregorianCalendar.MINUTE, (firstPublishCutover - lastPublishCutover));
				notificationStartTime = calendar.getTime();

				if (notificationStartTime.before(currentTimestamp)) {
					timeFraction = timeFraction + 5;
					Date adjNotificationStartTime = new Date(
							new Timestamp(currentTimestamp.getTime() + 60 * timeFraction * 1000).getTime());
					log.info("Schedule Insurance for last publishing [Scheduling Delivery Passed Job JOB_ID=" + jobId
							+ ", ExpectedSendTime=" + notificationStartTime + ", AdjustedSendTime=" + adjNotificationStartTime
							+ "]");
					notificationStartTime = adjNotificationStartTime;
				}

				try {

					ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
					log.info("Schedule Insurance for last Publishing [Scheduled Job JOB_ID=" + jobId
							+ ", Notification Send Timestamp=" + notificationStartTime + "]");

				} catch (SchedulerException se) {
					log.error("Schedule Insurance for last Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
					// FIXME - handle exception
				} catch (Exception e) {
					log.error("ScheduleInsurance for last Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
					notificationJobDetail.setRequestsRecovery(true);
				}
				log.info(" #####################     LAST PUBLISH SUB JOB SCHEDULING ENDED    ######################");
				// =================================== Last Publish Sub Job Scheduling Segment End
				// ================================================

			}
		} catch (Exception ex) {
			log.error("Schedule Insurance Publishings failed", ex);
			throw new JobExecutionException(ex);
		}
	}

}
