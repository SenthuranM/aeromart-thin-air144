package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ONDReportGenerator extends CompleteReportGenerator {

	@Override
	protected void generateReportFile(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		ScheduledservicesUtils.getDataExtractionBD().getONDData(reportsSearchCriteria);

	}

	@Override
	protected ResultSet getReportDataForAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

}
