package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PromotionRequestsCleanUpJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(PromotionRequestsCleanUpJob.class);

	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

		try {
			log.info(" ---- promo requests clean-up start");
			CredentialInvokerUtil.invokeCredentials();

			JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
			PromotionType promotionType = PromotionType.valueOf((String) BeanUtils.getFirstElement(jobDataMap.values()));
			ScheduledservicesUtils.getPromotionAdministrationBD().cleanUpPromotionRequests(promotionType);
			log.info(" ---- promo requests clean-up end");
		} catch (Exception e) {
			log.error("PromotionsNotifierJob ", e);
			throw new JobExecutionException(e);
		}

	}
}
