package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class OnlineCheckInReminderJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(OnlineCheckInReminderJob.class);

	@Override
	public void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		CredentialInvokerUtil.invokeCredentials();

		OnlineCheckInReminder onlineCheckInReminder = new OnlineCheckInReminder();

		try {
			onlineCheckInReminder.execute();
		} catch (Exception e) {
			log.error("Error in executing online checking reminder job", e);
			throw new JobExecutionException(e);
		}

	}

}
