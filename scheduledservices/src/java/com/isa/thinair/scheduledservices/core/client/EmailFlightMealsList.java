package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class EmailFlightMealsList {

	private static final Log log = LogFactory.getLog(EmailFlightMealsList.class);

	public static void sendMealsList() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("----- Sending meal notification for catering started --------");
			ScheduledservicesUtils.getFlightInventoryBD().sendMealNotification();
			log.info("----- Sending meal notification for catering ended -----------");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
