package com.isa.thinair.scheduledservices.core.client.cf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Scheduler to Publish flight changes to cargo flash
 * 
 * @author Nafly Azhar
 *
 */
public class FlightDetailsPublisherJob extends QuartzJobBean{
	
	private static Log log = LogFactory.getLog(FlightDetailsPublisherJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0)throws JobExecutionException {
		
		log.info("==========================Flight Changes Publisher for Cargo Flash Started============================");
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationAuxilliaryBD().publishPendingFlights(null, null);
			
		log.info("==========================Flight Changes Publisher for Cargo Flash Completed===========================");
		
		} catch (ModuleException e) {
			log.error("=============ERROR in publishing flight details=====================",e);
			CommonUtil.reportSheduleFailure(e, "Flight Details Publishing Scheduler Failed!");
			throw new JobExecutionException(e);
		}
		
	}

}
