package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ReservationReleaserJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ReservationReleaser.releaseOnHoldReservations();
		} catch (Exception me) {
			throw new JobExecutionException(me);
		}
	}
}