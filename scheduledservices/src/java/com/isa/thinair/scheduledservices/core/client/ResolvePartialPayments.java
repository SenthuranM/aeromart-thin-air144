package com.isa.thinair.scheduledservices.core.client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import org.springframework.util.CollectionUtils;

/**
 * @author IndikaA
 */
public class ResolvePartialPayments {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ResolvePartialPayments.class);

	public static void resolvePartialPayments() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();

			Collection refundBySchedulerEnabledIPGs = ScheduledservicesUtils.getPaymetBrokerBD().getSchedulerRefundEnabledIPGs();

			// Perform refund for one IPG at a time
			if (refundBySchedulerEnabledIPGs != null) {
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
				for (Iterator ipgsIt = refundBySchedulerEnabledIPGs.iterator(); ipgsIt.hasNext();) {
					ipgIdentificationParamsDTO = (IPGIdentificationParamsDTO) ipgsIt.next();
					Collection ipgIdentificationDTOs = new ArrayList();
					ipgIdentificationDTOs.add(ipgIdentificationParamsDTO);
					resolvePartialPaymentsForCarrier(ipgIdentificationDTOs);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private static void resolvePartialPaymentsForCarrier(Collection ipgIdentificationDTOCol) throws ModuleException {

		// Get Collection from RES
		log.info("#######################################################################");
		log.info("########## Starting Resolving Payments - "
				+ ((IPGIdentificationParamsDTO) ipgIdentificationDTOCol.iterator().next()).getFQIPGConfigurationName()
				+ " ######");
		Calendar oCal = Calendar.getInstance();
		log.info("########## Started " + oCal + " #######################################");
		oCal.add(Calendar.MINUTE, -1 * AppSysParamsUtil.getTimeForQueryDROperation());

		Collection colIPGQueryDTO = ScheduledservicesUtils.getReservationBD().getInitiatedPaymentInfo(oCal.getTime(),
				ipgIdentificationDTOCol);

		Collection<IPGQueryDTO> voucherRelatedQueryDTOS = getVoucherRelatedQueryDTOS(colIPGQueryDTO);

		// VOUCHER RELATED PAYMENTS ARE REMOVED FROM INCONSISTENT PAYMENTS
		colIPGQueryDTO.removeAll(voucherRelatedQueryDTOS);

		PaymentBrokerBD oPaymentBrokerBD = ScheduledservicesUtils.getPaymetBrokerBD();
		
		Collection<Integer> tempTnxIdsIS = new ArrayList<>();
		
		if (colIPGQueryDTO.size() > 0) {
			// Get Query Results
			// Do reversal
			ServiceResponce sr = oPaymentBrokerBD.resolvePartialPayments(colIPGQueryDTO, AppSysParamsUtil.isQueryDRRefund());

			// Update Status
			if (sr.isSuccess()) {
				Collection oResultIF = (Collection) sr.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF);
				Collection oResultIP = (Collection) sr.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP);
				Collection oResultII = (Collection) sr.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II);
				Collection oResultIS = (Collection) sr.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS);

				if (oResultIF != null && !oResultIF.isEmpty())
					updateStatus(oResultIF, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED);
				if (oResultIP != null && !oResultIP.isEmpty())
					updateStatus(oResultIP, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST);
				if (oResultII != null && !oResultII.isEmpty())
					updateStatus(oResultII, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT);
				if (oResultIS != null && !oResultIS.isEmpty()) {
					updateStatus(oResultIS, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS);
					tempTnxIdsIS.addAll(oResultIS);
				}
			}
			
			//cancel & update LMS block credit payments
			cancelLMSBlockedCredits(tempTnxIdsIS);
		}

		if (!CollectionUtils.isEmpty(voucherRelatedQueryDTOS)) {
			log.info("########## Started REVERSING VOUCHER PAYMENTS #######################################");
			ServiceResponce serviceResponce = oPaymentBrokerBD.resolvePartialPaymentsForVouchers(voucherRelatedQueryDTOS,
					AppSysParamsUtil.isQueryDRRefund());

			Collection oResultIF = (Collection) serviceResponce.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF);
			Collection oResultIS = (Collection) serviceResponce.getResponseParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS);

			if (!CollectionUtils.isEmpty(oResultIF)) {
				updateStatus(oResultIF, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED);
			}

			if (!CollectionUtils.isEmpty(oResultIS)) {
				updateStatus(oResultIS, ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS);
			}
			log.info("########## End REVERSING VOUCHER PAYMENTS #######################################");
		}

		log.info("########## End Resolving Payments " + ipgIdentificationDTOCol.toString() + "###########");
		log.info("####################################################################");

	}

	private static Collection<IPGQueryDTO> getVoucherRelatedQueryDTOS(Collection colIPGQueryDTO) {
		Collection<IPGQueryDTO> voucherPayments = new ArrayList<>();
		if (!CollectionUtils.isEmpty(colIPGQueryDTO)) {
			for (Object ipgQueryDTO : colIPGQueryDTO) {
				IPGQueryDTO queryDTO = (IPGQueryDTO) ipgQueryDTO;
				if (PromotionCriteriaConstants.ProductType.GIFT_VOUCHER
						.equals(CharUtils.toCharacterObject(((IPGQueryDTO) ipgQueryDTO).getProductType()))) {
					voucherPayments.add(queryDTO);
				}
			}
		}
		return voucherPayments;
	}

	/**
	 * Updates the tempory payment states
	 * 
	 * @param arrList
	 * @param txnstate
	 * @throws ModuleException
	 */
	private static void updateStatus(Collection arrList, String txnstate) throws ModuleException {
		ScheduledservicesUtils.getReservationBD().updateTempPaymentEntriesForBulkRecords(arrList, txnstate);
	}
	
	private static void cancelLMSBlockedCredits(Collection<Integer> tempTnxIds) {
		log.info("########## Starting Cancel LMSBlocked Credits ###########");
		try {

			if (tempTnxIds != null && !tempTnxIds.isEmpty()) {
				ScheduledservicesUtils.getReservationBD().cancelFailedLMSBlockedCreditPaymentsByTempIds(tempTnxIds);
			}

		} catch (Exception e) {
			log.error("Cancel LMSBlocked Credits Operation ::: FAILED ");
		}
		log.info("########## End Cancel LMSBlocked Credits ###########");
	}
}
