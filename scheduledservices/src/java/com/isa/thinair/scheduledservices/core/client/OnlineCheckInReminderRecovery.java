package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class OnlineCheckInReminderRecovery {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(OnlineCheckInReminderRecovery.class);

	public void execute() throws JobExecutionException {

		if (AppSysParamsUtil.isOnlineCheckInReminderEnable()) {

			try {
				ReservationBD reservationBD = ScheduledservicesUtils.getReservationBD();
				FlightBD flightBD = ScheduledservicesUtils.getFlightBD();

				Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
				log.info("START - Recovery Schedule Online CheckIn Reminders [CurrentTime = "
						+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(currentTimestamp) + "]");

				List<OnlineCheckInReminderDTO> onlineCheckInReminderDTOs = reservationBD
.getFlightSegsForOnlineCheckInReminder(
						currentTimestamp, ReservationInternalConstants.SchedulerType.RECOVERY_SCHEDULER);

				log.info("Recovery Schedule Online CheckIn Reminders::: " + "[No of Reminders Fetched = "
						+ onlineCheckInReminderDTOs.size() + "]");

				int timeFraction = 5;
				String flightNumber;
				String originAirport;
				Date departureDate;
				String segCode;
				Integer flightSegId;
				Date expectedSendNotificationTime;
				Integer flightSegNotifEventId;

				for (OnlineCheckInReminderDTO onlineCheckinReminderDTO : onlineCheckInReminderDTOs) {

					flightNumber = onlineCheckinReminderDTO.getFlightNo();
					originAirport = onlineCheckinReminderDTO.getOrigin();
					departureDate = onlineCheckinReminderDTO.getDepartureDate();
					segCode = onlineCheckinReminderDTO.getSegCode();
					flightSegId = onlineCheckinReminderDTO.getFltSegId();
					expectedSendNotificationTime = onlineCheckinReminderDTO.getSendNotificationTime();
					Date onlineCheckInRemainderSendTime = expectedSendNotificationTime;
					flightSegNotifEventId = onlineCheckinReminderDTO.getFlightSegmentNotificationId();

					String jobId = "RECOVERY/ONLINECHECKINREM/" + "/" + segCode + "/" + flightNumber + "/"
							+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(departureDate) + "/" + flightSegId;

					if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.ONLINE_CHECKING_REMINDER)) {
						log.info("Recovery Schedule Online CheckIn Reminders [Skipping Scheduling Job as Scheduled JOB_ID="
								+ jobId + "]");
						continue;
					}

					log.info("Recovery Schedule Online CheckIn Reminders [Scheduling Job For JOB_ID = " + jobId + "]");
					log.info("Recovery Schedule Online Checkin Reminders ::: [Update T_FLIGHT_SEG_NOTIFICATION Table Wtih Status 'RESCHEDULED' ");
					flightBD.updateFlightSegmentNotifyStatus(flightSegId, flightSegNotifEventId, "RESCHEDULED", "", 0,
							ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER);

					timeFraction = timeFraction + 2;
					onlineCheckInRemainderSendTime = new Date(
							new Timestamp(new Date().getTime() + 60 * timeFraction * 1000).getTime());

					log.info("Recovery Schedule Online Checkin Reminders ::: [New Schedule Recovery time = "
							+ onlineCheckInRemainderSendTime + "]");

					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(onlineCheckInRemainderSendTime);

					int onlineCheckInReminderYear = calendar.get(Calendar.YEAR);
					int onlineCheckInReminderMonth = calendar.get(Calendar.MONTH) + 1;
					int onlineCheckInReminderDate = calendar.get(Calendar.DATE);
					int onlineCheckInReminderHour = calendar.get(Calendar.HOUR);
					int onlineCheckInReminderMintes = calendar.get(Calendar.MINUTE);
					int onlineCheckInReminderSeconds = calendar.get(Calendar.SECOND);

					JobDetail notificationJobDetail = new JobDetail();

					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_JOB_ID, jobId);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_JOB_GROUP,
							SSInternalConstants.JOB_TYPE.ONLINE_CHECKING_REMINDER);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_DepartureStation, originAirport);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FLIGHT_NUMBER, flightNumber);
					notificationJobDetail.getJobDataMap()
							.put(OnlineCheckInReminderSender.PROP_DEPARTURE_DATE_ZULU, departureDate);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FlightSegId, flightSegId);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
							flightSegNotifEventId);

					notificationJobDetail.setJobClass(OnlineCheckInReminderSender.class);

					java.util.Date notificationRecoveryTime = CommonUtil.getDateof(onlineCheckInReminderSeconds,
							onlineCheckInReminderMintes, onlineCheckInReminderHour, onlineCheckInReminderDate,
							onlineCheckInReminderMonth, onlineCheckInReminderYear);

					try {

						ScheduleManager.scheduleJob(notificationJobDetail, notificationRecoveryTime, null, 0, 0);
						log.info("Recovery Schedule Online CheckIn Reminders [Scheduled Recovery Job JOB_ID = " + jobId
								+ ", Notification Recovery Send Timestamp = " + notificationRecoveryTime + "]");
					} catch (SchedulerException se) {
						log.error("Recovery Schedule Online CheckIn Reminders [Scheduling Recovery Job FAILED for JOB_ID="
								+ jobId + "]", se);
					} catch (Exception e) {
						log.error("Recovery Schedule Online CheckIn Reminders [Scheduling Recovery Job FAILED for JOB_ID="
								+ jobId + "]", e);
						notificationJobDetail.setRequestsRecovery(true);
					}

				}
			} catch (Exception ex) {
				log.error("Recovery Schedule Online CheckIn Reminders failed", ex);
				throw new JobExecutionException(ex);
			} finally {
				CredentialInvokerUtil.close();
			}
			log.info("END - Recovery Schedule Online CheckIn Reminders [CurrentTime = "
					+ CalendarUtil.getCurrentSystemTimeInZulu() + "]");
		}
	}

}
