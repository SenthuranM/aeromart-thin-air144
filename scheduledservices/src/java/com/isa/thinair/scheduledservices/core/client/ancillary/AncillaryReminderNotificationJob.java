package com.isa.thinair.scheduledservices.core.client.ancillary;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class AncillaryReminderNotificationJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(AncillaryReminderNotificationJob.class);

	public void executeInternal(JobExecutionContext executionContext) throws JobExecutionException {

		if (AppSysParamsUtil.isAncillaryReminderEnable()) {
			try {
				CredentialInvokerUtil.invokeCredentials();
				SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
				Integer minimumGapInMins = sc.getMinGapInMinitesBWJobs();

				ReservationBD reservationBD = ScheduledservicesUtils.getReservationBD();
				Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
				log.info("START - Schedule Ancillary Reminders [CurrentTime = "
						+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(currentTimestamp) + "]");
				List<AncillaryReminderDetailDTO> ancilaryFlightSegmentsToRemind = reservationBD
						.getFlightSegmentsToScheduleReminder(currentTimestamp, "MASTER_SCHEDULER");
				log.info("Schedule Ancillary Reminders " + "[No of flight Segments Fetched = "
						+ ancilaryFlightSegmentsToRemind.size() + "]");

				Integer flightId;
				String flightNumber;
				Integer flightSegmentId;
				Integer flightSegNotifEventId;
				Integer airportCutOverStartTime;
				String originAirport;
				String onlineCheckin;
				Date departureTimeLocal;
				Date departureTimeZulu;
				Date expectedSendNotificationTime;
				Date previousSendNotificationTime = currentTimestamp;

				for (AncillaryReminderDetailDTO anciDetailDTO : ancilaryFlightSegmentsToRemind) {
					String jobId = "ANCIREM/" + anciDetailDTO.getSegmentCode() + "/" + anciDetailDTO.getFlightNumber() + "/"
							+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(anciDetailDTO.getDepartureTimeZulu()) + "/" + anciDetailDTO.getFlightSegId();

					flightId = anciDetailDTO.getFlightId();
					flightNumber = anciDetailDTO.getFlightNumber();
					flightSegmentId = anciDetailDTO.getFlightSegId();
					flightSegNotifEventId = anciDetailDTO.getFlightSegmentNotificationEventId();
					airportCutOverStartTime = anciDetailDTO.getAnciNotificationStartCutOverTime();
					originAirport = anciDetailDTO.getFlightOrigin();
					onlineCheckin = anciDetailDTO.getAirportOnlineCheckin();
					departureTimeLocal = anciDetailDTO.getDeparturetimeLocal();
					departureTimeZulu = anciDetailDTO.getDepartureTimeZulu();
					expectedSendNotificationTime = anciDetailDTO.getNotificationReminderTime();

					Date ancilaryRemainderSendTime = expectedSendNotificationTime;
					Date ancillaryNotifyEndCutoverTimestamp = null;
					if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.ANCILLARY_NOTIFICATION)) {
						log.info("Schedule Ancillary Reminders [Skipping Scheduling Job as Scheduled JOB_ID=" + jobId + "]");
						continue;
					}

					log.info("Schedule Ancillary Reminders [Scheduling Job For JOB_ID = " + jobId + "]");

					if ((ancilaryRemainderSendTime.getTime() - previousSendNotificationTime.getTime()) < minimumGapInMins) {
						// Inject minimum gap
						GregorianCalendar calAdjustNextExecutionTime = new GregorianCalendar();
						calAdjustNextExecutionTime.setTime(previousSendNotificationTime);
						calAdjustNextExecutionTime.add(Calendar.MINUTE, minimumGapInMins);

						ancilaryRemainderSendTime = calAdjustNextExecutionTime.getTime();

						GregorianCalendar calNotifyEndCutover = new GregorianCalendar();
						calNotifyEndCutover.setTime(departureTimeZulu);
						calNotifyEndCutover.add(Calendar.MINUTE, -anciDetailDTO.getAnciNotificationEndCutOverTime());

						ancillaryNotifyEndCutoverTimestamp = calNotifyEndCutover.getTime();

						if (ancilaryRemainderSendTime.after(ancillaryNotifyEndCutoverTimestamp)) {
							ancilaryRemainderSendTime = ancillaryNotifyEndCutoverTimestamp;
						}

						log.warn("Schedule Ancillary Reminders [Revising execution time Job JOB_ID=" + jobId
								+ ", ExpectedSendTime=" + new SimpleDateFormat("ddMMyy-HH:mm:ss").format(expectedSendNotificationTime) + ", AdjustedSendTime="
								+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(ancilaryRemainderSendTime) + ", Ancillary Notify End Cutover ="
								+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(ancillaryNotifyEndCutoverTimestamp) + "]");
					}

					JobDetail notificationJobDetail = new JobDetail();

					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_JOB_ID, jobId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_JOB_GROUP,
							SSInternalConstants.JOB_TYPE.ANCILLARY_NOTIFICATION);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FlightId, flightId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DepartureStation, originAirport);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FLIGHT_NUMBER, flightNumber);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FlightSegId, flightSegmentId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
							flightSegNotifEventId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DEPARTURE_DATE_LOCAL,
							departureTimeLocal);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DEPARTURE_DATE_ZULU,
							departureTimeZulu);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_AIRPORT_ONLINE_CHECKIN,
							onlineCheckin);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_ANCILLARY_CUT_OVER_TIME,
							airportCutOverStartTime);

					notificationJobDetail.setJobClass(AncillaryNotificationSender.class);

					try {

						ScheduleManager.scheduleJob(notificationJobDetail, ancilaryRemainderSendTime, null, 0, 0);
						previousSendNotificationTime = ancilaryRemainderSendTime;
						log.info("Schedule Ancillary Reminders [Scheduled Job JOB_ID=" + jobId + ", Notification Send Timestamp="
								+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(ancilaryRemainderSendTime) + "]");
					} catch (SchedulerException se) {
						log.error("Schedule Ancillary Reminders [Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
					} catch (Exception e) {
						log.error("Schedule Ancillary Reminders [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
						notificationJobDetail.setRequestsRecovery(true);
					}
				}
			} catch (Exception ex) {
				log.error("Schedule Ancillary Reminders failed", ex);
				throw new JobExecutionException(ex);
			} finally {
				CredentialInvokerUtil.close();
			}
			log.info("END - Schedule Ancillary Reminders [CurrentTime = " + CalendarUtil.getCurrentSystemTimeInZulu() + "]");
		}
	}
}
