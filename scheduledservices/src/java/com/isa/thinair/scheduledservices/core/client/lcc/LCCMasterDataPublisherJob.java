package com.isa.thinair.scheduledservices.core.client.lcc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Scheduled Job of MasterData Publishing to LCC.
 * 
 * @author Charith
 * @since 2010-03-22
 */
public class LCCMasterDataPublisherJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(LCCMasterDataPublisherJob.class);

	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			LCCMasterDataPublisher.execute();
		} catch (Exception e) {
			log.error("LCCMasterDataPublisherJob ", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
