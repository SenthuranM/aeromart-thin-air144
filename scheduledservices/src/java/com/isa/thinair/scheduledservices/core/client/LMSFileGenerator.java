package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.promotion.api.model.LoyaltyFileLog.FILE_TYPE;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class LMSFileGenerator {
	private static Log log = LogFactory.getLog(LMSFileGenerator.class);

	public static void generateProductFile() throws Exception {
		log.info("######### LMS Generate Product File Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();

			log.info("######### LMS Generate Product File Job started at " + new Date());
			ScheduledservicesUtils.getLoyaltyManagementBD().generateProductFile();
			log.info("######### LMS Generate Product File Job ended at " + new Date());
		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "LMS Product File creation failed!");
		}

		try {
			log.info("######### LMS Upload Product File Job started ##########");
			ScheduledservicesUtils.getLoyaltyManagementBD().updatePendingFiles(FILE_TYPE.PRODUCT_FILE);
			log.info("######### LMS Upload Product File Job Job ended ##########");
		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "LMS Product File Generate Scheduler Failed!");
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
		log.info("######### LMS Generate Product File Job ended at " + new Date());
	}

	public static void generateFlownFile(int salesFrequncyInDays) throws Exception {
		log.info("######### LMS Generate Flown File Job invoked ##########");

		try {
			CredentialInvokerUtil.invokeCredentials();

			Date date = new Date();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
			date = fmt.parse(fmt.format(date)); // Convert the date to yyyy/MM/dd 00:00

			// toDate = yyyy/MM/(dd-1) 11:59
			Date toDate = new Date(date.getTime() - TimeUnit.SECONDS.toMillis(1));

			// fromDate = (yyyy/MM/(dd-salesFrequncyInDays) 00:00)
			Date fromDate = new Date(date.getTime() - TimeUnit.DAYS.toMillis(salesFrequncyInDays));

			log.info("######### LMS Generate Flown File Job started at " + new Date() + "for sales Frequency"
					+ salesFrequncyInDays);
			ScheduledservicesUtils.getLoyaltyManagementBD().generateFlownFile(fromDate, toDate, false);
			log.info("######### LMS Generate Flown File Job ended at " + new Date());

		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "LMS Flown File creation failed!");
		}

		try {
			log.info("######### LMS Upload Flown File Job started ##########");
			ScheduledservicesUtils.getLoyaltyManagementBD().updatePendingFiles(FILE_TYPE.FLOWN_FILE);
			log.info("######### LMS Upload Flown File Job Job ended ##########");
		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "LMS Flown File Generate Scheduler Failed!");
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}

		log.info("######### LMS Generate Flown File Job ended at " + new Date());
	}
}
