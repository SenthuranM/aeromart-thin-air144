package com.isa.thinair.scheduledservices.core.client;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.GoshowBCOpener;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class GoshowBCOpenerSchedulerJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(GoshowBCOpenerSchedulerJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		CredentialInvokerUtil.invokeCredentials();
		log.info("############################## SCHEDULING GOSHO BOOKING CLASS OPENING JOB IS STARTED " + new Date());
		ArrayList<FlightSegmentDTO> flightSegList = ScheduledservicesUtils.getFlightBD().getGoshowBCOpeningFilghtList(new Date());
		String jobId = "";
		for (FlightSegmentDTO fltSeg : flightSegList) {
			jobId = fltSeg.getSegmentId() + "/" + fltSeg.getDepartureDateTimeZulu();
			try {
				if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.GOSHOW_BC_OPENER)) {
					scheduleJob(jobId, SSInternalConstants.JOB_TYPE.GOSHOW_BC_OPENER, fltSeg.getFlightId().toString(),
							fltSeg.getDepartureDateTimeZulu(), fltSeg.getSegmentId(), fltSeg.getFlightType());
				}
			} catch (SchedulerException e) {
				log.error("GoshowBCOpenerSchedulerJob", e);
			}
		}
	}

	private void scheduleJob(String jobId, String jobGroup, String flightId, Date departureDate, Integer flightSegId,
			String flightType) throws SchedulerException {

		long reservationCutOffTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
		long reservationCutOffTimeForDomestic = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();

		JobDetail jobDetail = new JobDetail();
		Job goshowBCOpener = new GoshowBCOpener();

		jobDetail.getJobDataMap().put(goshowBCOpener.PROP_JOB_ID, jobId);
		jobDetail.getJobDataMap().put(goshowBCOpener.PROP_JOB_GROUP, jobGroup);
		jobDetail.getJobDataMap().put(goshowBCOpener.PROP_FlightId, flightId);
		jobDetail.getJobDataMap().put(goshowBCOpener.PROP_DEPARTURE_DATE_ZULU, departureDate);
		jobDetail.getJobDataMap().put(goshowBCOpener.PROP_FlightSegId, flightSegId);
		jobDetail.setJobClass(GoshowBCOpener.class);

		Date goshowBCOpeningTime = null;
		if (FlightUtil.FLIGHT_TYPE_INT.equals(flightType)) {
			goshowBCOpeningTime = CommonUtil.getDateof(departureDate.getTime() - reservationCutOffTime);
		} else {
			goshowBCOpeningTime = CommonUtil.getDateof(departureDate.getTime() - reservationCutOffTimeForDomestic);
		}
		ScheduleManager.scheduleJob(jobDetail, goshowBCOpeningTime, null, 0, 0);
	}
}
