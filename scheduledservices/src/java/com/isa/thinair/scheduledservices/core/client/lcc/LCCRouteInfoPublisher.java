/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client.lcc;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.routepublish.RouteInfoPublishRSDataTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * Scheduled Job of RouteInfo Publishing to LCC
 * 
 * @author Navod Ediriweera
 * @since 2009-10-20
 */
public class LCCRouteInfoPublisher {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(LCCRouteInfoPublisher.class);

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute() throws ModuleException {
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			log.info("#######################################################");
			log.info("########## Starting AA-LCC RouteInfo Publishing #######");

			publishRouteInfoUpdates();

			log.info("########## Completed AA-LCC RouteInfo Publishing ######");
			log.info("#######################################################");
		}
	}

	/**
	 * Perform the Taks
	 * 
	 * @throws ModuleException
	 */
	private static void publishRouteInfoUpdates() throws ModuleException {
		List<RouteInfo> unPublishedRouteInfoList = getUnPublishedData();
		RouteInfoPublishRSDataTO responseDTO = doWSCall(createRequestDTO(unPublishedRouteInfoList));
		updateRoutes(responseDTO, unPublishedRouteInfoList);
		processErrors(responseDTO);
	}

	/**
	 * Creates the RequestDTO to be send to the LC client
	 * 
	 * @return
	 */
	private static RouteInfoPublishRQDataTO createRequestDTO(List<RouteInfo> unPublishedRouteInfoList) {
		RouteInfoPublishRQDataTO routeInfoRS = new RouteInfoPublishRQDataTO();
		routeInfoRS.setPublishedRouteInfo(unPublishedRouteInfoList);
		return routeInfoRS;
	}

	/**
	 * Retrives unpublished RouteInfo
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static List<RouteInfo> getUnPublishedData() throws ModuleException {
		return ScheduledservicesUtils.getCommonMasterBD().getLCCUnPublishedRouteInfo();
	}

	/**
	 * Connect to LCC Client and make the WS call.
	 * 
	 * @param routeInfoRS
	 * @return
	 * @throws ModuleException
	 */
	private static RouteInfoPublishRSDataTO doWSCall(RouteInfoPublishRQDataTO routeInfoRS) throws ModuleException {
		try {
			return ScheduledservicesUtils.getLCCMasterDataPublisherBD().publishRouteInfoToLCC(routeInfoRS);
		} catch (Exception e) {
			log.error("Failed while Publishing Routes");
			throw new ModuleException(e, "lccclient.routeinfo.invocation.error");
		}
	}

	/**
	 * Roll back LCC_PUBLISHED_STATUS if the publishing has failed in LCC side
	 * 
	 * @param responseData
	 * @throws ModuleException
	 */
	private static void updateRoutes(RouteInfoPublishRSDataTO responseData, List<RouteInfo> unPublishedRouteInfoList)
			throws ModuleException {
		// update RouteInfo Table
		List<String> updatedRIList = responseData.getUpdatedRInfoIDs();
		ScheduledservicesUtils.getCommonMasterBD().updateRouteInfoLCCPublishStatus(unPublishedRouteInfoList, updatedRIList);
	}

	/**
	 * Process Error Messages
	 * 
	 * @param responseDTO
	 */
	private static void processErrors(RouteInfoPublishRSDataTO responseDTO) {
		if (responseDTO.getErrors().size() > 0) {
			log.info("############# Errors Present in LCC Publishing - Displaying- ##################");
			for (Iterator<String> iterator = responseDTO.getErrors().iterator(); iterator.hasNext();) {
				String error = iterator.next();
				log.info("ERROR Returned From LCC - " + error);
			}
			log.info("################## End Displaying Errors #########################");
		}

	}
}
