package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class CachingServicegenerator {
	private static Log log = LogFactory.getLog(CachingServicegenerator.class);

	public static void exportIpToCountryData() {
		log.info("######### Export ip to country data Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();

			log.info("######### Export ip to country data Job started at " + new Date());
			CommonsServices.getAerospikeCachingService().syncIpToCountryData();
			log.info("######### Export ip to country data Job ended at " + new Date());
		} catch (Exception e) {
			log.error(e);
		}
		log.info("######### export ip to country data Job ended at " + new Date());
	}
}
