package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Manoj Dhanushka
 */
public class AviatorInventoryFilePublisher {
	
	static Log log = LogFactory.getLog(AviatorInventoryFilePublisher.class);
	
	/**
	 * Main execution
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static void execute() throws Exception {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("#####AVIATOR##### AviatorInventoryFilePublisherJob Started ");
			ScheduledservicesUtils.getDataExtractionBD().generateAviatorFile();
			log.info("#####AVIATOR##### AviatorInventoryFilePublisherJob Ended");
		} catch (Exception e) {
			log.error("#####AVIATOR##### Error in Aviator file generation", e);
			CommonUtil.reportSheduleFailure(e, "Error in Aviator file generation!");
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
