/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Job activate scheduled reports.
 * 
 * This will activate scheduled reports prior to one date to the start date by adding a quartz job and will inactivate
 * the scheduled report within one date from the end date by removing the quartz job.
 * 
 * @author Lasantha Pambagoda
 */
public class ScheduledReportActivationJob extends QuartzJobBean {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ScheduledReportActivationJob.class);

	/**
	 * Execute method to be called by the scheduler
	 */
	@Override
	public void executeInternal(JobExecutionContext context) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			if (AppSysParamsUtil.isScheduledReportsAllowed()) {
				if (log.isInfoEnabled()) {
					log.info("Executing the ScheduledReportActivationJob");
				}

				// Retrieve all the scheduled jobs not started within one day to start
				Collection<ScheduledReport> reportList = ScheduledservicesUtils.getScheduledReportBD()
						.getNotActivatedScheduleReports();
				for (ScheduledReport scheduledReport : reportList) {
					ScheduledservicesUtils.getScheduledReportBD().activateScheduledReport(scheduledReport);
				}

				// Complete the finished scheduled jobs
				Collection<ScheduledReport> compNeedList = ScheduledservicesUtils.getScheduledReportBD()
						.getCompletionRequiredScheduleReports();
				for (ScheduledReport scheduledReport : compNeedList) {
					ScheduledservicesUtils.getScheduledReportBD().finishScheduledReport(scheduledReport);
				}

				// recreate the deleted quarts
				Collection<Integer> deletedSRIDList = ScheduledservicesUtils.getScheduledReportBD()
						.getStartedScheduledReportIdsWithNoTriggers();
				for (Integer scheduledReportId : deletedSRIDList) {
					ScheduledservicesUtils.getScheduledReportBD().recreateScheduledReportTrigger(scheduledReportId);
				}
			} else {
				if (log.isInfoEnabled()) {
					log.info("Skipping the report Activation Job");
				}
			}
		} catch (Exception re) {
			log.error("ScheduledReportActivationJob ", re);
			throw new JobExecutionException(re);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
