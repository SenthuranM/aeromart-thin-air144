package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class RevenueTaxReportGenerator extends CompleteReportGenerator {

	@Override
	protected void generateReportFile(ReportsSearchCriteria reportSearchCriteria) throws ModuleException {

		if (reportSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_DETAIL)) {
			ScheduledservicesUtils.getDataExtractionBD().getROPRevenue(reportSearchCriteria);
		} else if (reportSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY)) {
			ScheduledservicesUtils.getDataExtractionBD().getROPRevenueDataforReports(reportSearchCriteria);
		} else if (reportSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY)) {
			ScheduledservicesUtils.getDataExtractionBD().getROPRevenueDataforReports(reportSearchCriteria);
		}
		generatedReportFileNamePrefix = this.reportConfigurationMap.get(reportSearchCriteria.getReportOption() + "_PREFIX")
				.toString();
		setGeneratedFileFormat(this.reportConfigurationMap.get(reportSearchCriteria.getReportOption() + "_FILETYPE").toString());

	}

	@Override
	protected ResultSet getReportDataForAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {

			return ScheduledservicesUtils.getDataExtractionBD().getROPRevenueDataforReports(reportsSearchCriteria);

		}

		return null;
	}

}
