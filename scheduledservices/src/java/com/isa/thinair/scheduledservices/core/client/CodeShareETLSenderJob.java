package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class CodeShareETLSenderJob extends QuartzJobBean {
	
	private static Log log = LogFactory.getLog(GDSAVSSenderJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
            ScheduledservicesUtils.getMessageBrokerServiceBD().sendETLMessages();						
			log.info("############## Sending ETL messages" + new Date());
		} catch (Exception e) {
			log.error("Error while sending ETL Messages" + e);
			throw new JobExecutionException(e);
		}
		
	}

}
