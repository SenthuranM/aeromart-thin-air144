package com.isa.thinair.scheduledservices.core.client;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AgentTokenPurgeJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getTravelAgentBD().purgeExpiredAgentTokens();
		} catch (ModuleException e) {
			throw new JobExecutionException(e);
		}
	}
}
