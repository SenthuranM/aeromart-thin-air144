package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class PromotionInfoReportGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return ScheduledservicesUtils.getDataExtractionBD().getPromoCodeDetailsData(reportsSearchCriteria);
	}

	@Override
	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy");
	}

}
