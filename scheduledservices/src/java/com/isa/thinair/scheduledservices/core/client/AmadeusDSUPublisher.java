package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Manoj Dhanushka
 */
public class AmadeusDSUPublisher {
	static Log log = LogFactory.getLog(AmadeusDSUPublisher.class);

	/**
	 * Main execution
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static void execute() throws Exception {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("########## AmadeusDSUPublisherJob Started ");
			ScheduledservicesUtils.getGDServicesBD().publishDailyScheduleUpdateMessages();
			log.info("########## AmadeusDSUPublisherJob Ended");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
