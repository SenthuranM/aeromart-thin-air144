package com.isa.thinair.scheduledservices.core.client.ssmasm;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Job to release schedules that locked more than 30mins
 * @author rajitha
 *
 */
public class ReleaseLockInMessageProcessingQueueJob  extends QuartzJobBean{

	private static final Log log = LogFactory.getLog(ReleaseLockInMessageProcessingQueueJob.class);
	
	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			ReleaseLockInMessageProcessingQueue.executeJob();
		} catch (Exception e) {
			log.error("ReleaseLockInMessageProcessingQueueJob ", e);
			throw new JobExecutionException(e);
		}

	}

}
