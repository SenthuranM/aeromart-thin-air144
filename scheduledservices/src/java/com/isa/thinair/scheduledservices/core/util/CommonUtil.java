package com.isa.thinair.scheduledservices.core.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.DateBuilder;

import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;

/**
 * 
 * @author nafly
 *
 */
public class CommonUtil {

	private static Log log = LogFactory.getLog(CommonUtil.class);

	/**
	 * Notify scheduler job failures
	 * 
	 * @param throwable
	 * @param subject
	 */
	public static void reportSheduleFailure(Throwable throwable, String subject) {
		try {
			SchedulerConfig schedulerConfig = (SchedulerConfig) SchedulerUtils
					.getInstance().getModuleConfig();
			if (schedulerConfig.getErrorNotifyingEmail() != null) {
				SimpleEmailDTO emailDTO = new SimpleEmailDTO();
				emailDTO.setToEmailAddress(schedulerConfig
						.getErrorNotifyingEmail());
				emailDTO.setSubject(subject);
				String str = ExceptionUtils.getStackTrace(throwable);
				emailDTO.setContent(str);
				ScheduledservicesUtils.getMessagingServiceBD().sendSimpleEmail(
						emailDTO);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	public static Date getDateof(Timestamp timestamp) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(new Date(timestamp.getTime()));

		int scheduledYear = gregorianCalendar.get(Calendar.YEAR);
		int scheduledMonth = gregorianCalendar.get(Calendar.MONTH) + 1;
		int scheduledDay = gregorianCalendar.get(Calendar.DATE);
		int scheduledHour = gregorianCalendar.get(Calendar.HOUR_OF_DAY);
		int scheduledMinutes = gregorianCalendar.get(Calendar.MINUTE);
		int scheduledSeconds = gregorianCalendar.get(Calendar.SECOND);

		return DateBuilder.dateOf(scheduledHour, scheduledMinutes,
				scheduledSeconds, scheduledDay, scheduledMonth, scheduledYear);

	}

	public static Date getDateof(Long timestamp) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(new Date(timestamp));

		int scheduledYear = gregorianCalendar.get(Calendar.YEAR);
		int scheduledMonth = gregorianCalendar.get(Calendar.MONTH) + 1;
		int scheduledDay = gregorianCalendar.get(Calendar.DATE);
		int scheduledHour = gregorianCalendar.get(Calendar.HOUR_OF_DAY);
		int scheduledMinutes = gregorianCalendar.get(Calendar.MINUTE);
		int scheduledSeconds = gregorianCalendar.get(Calendar.SECOND);

		return DateBuilder.dateOf(scheduledHour, scheduledMinutes,
				scheduledSeconds, scheduledDay, scheduledMonth, scheduledYear);
	}

	public static Date getDateof(int scheduledSeconds, int scheduledMinutes,
			int scheduledHour, int scheduledDay, int scheduledMonth,
			int scheduledYear) {

		return DateBuilder.dateOf(scheduledHour, scheduledMinutes,
				scheduledSeconds, scheduledDay, scheduledMonth, scheduledYear);
	}

}
