/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public class ReconcileOpenReturnSegments {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ReconcileOpenReturnSegments.class);

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute(Date executionDate) throws Exception {

		try {
			CredentialInvokerUtil.invokeCredentials();

			if (AppSysParamsUtil.isCancelConfirmExpiredOpenReturnSegments()) {
				log.info("###############################################################");
				log.info("########## Starting Reconciling Open Return Segments ##########");

				if (executionDate == null) {
					executionDate = new Date();
				}

				log.info("########## Started " + executionDate + " ######");

				ScheduledservicesUtils.getSegmentBD().expireOpenReturnSegments(executionDate, null, null, null);

				log.info("########## Completed Reconciling Open Return Segments ##########");
				log.info("################################################################");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
