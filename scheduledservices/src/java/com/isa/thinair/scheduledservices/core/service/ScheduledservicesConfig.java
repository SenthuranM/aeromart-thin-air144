package com.isa.thinair.scheduledservices.core.service;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author nasly All scheduler client specific configurations go here
 */
public class ScheduledservicesConfig extends DefaultModuleConfig {

}
