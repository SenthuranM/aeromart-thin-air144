package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL") public class GSTR3Generator extends BaseRowSetWithSubReportsGenerator {

	private static Log log = LogFactory.getLog(GSTR3Generator.class);

	@Override public Map<String, ResultSet> getResultSetMap(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException {
		return ScheduledservicesUtils.getDataExtractionBD().getGstr3(reportsSearchCriteria);
	}

	@Override public void setParameters(HashMap mainReportParameterMap, Map<String, Object> parameters)
			throws ModuleException {

		State state = WSClientModuleUtils.getCommonServiceBD().getState(String.valueOf(parameters.get("STATE_CODE")));

		int yearMonthDay[] = CalendarUtil.getYearMonthDay(String.valueOf(parameters.get("FROM_DATE")));
		//yearMonthDay[0] = year; yearMonthDay[1] = month; yearMonthDay[2] = day
		Date date = CalendarUtil.getDate(yearMonthDay[0], yearMonthDay[1] - 1, 1);

		mainReportParameterMap.put("GSTIN", Arrays.asList(state.getAirlineOfficeTaxRegNo().split("")));
		mainReportParameterMap.put("PERSON_NAME", AppSysParamsUtil.getDefaultCarrierName());
		mainReportParameterMap.put("ADDRESS",
				state.getAirlineOfficeSTAddress1() + ", " + state.getAirlineOfficeSTAddress2() + ", " + state
						.getAirlineOfficeCity() + ".");
		mainReportParameterMap.put("MONTH", CalendarUtil.getMonthName(date));
		mainReportParameterMap.put("YEAR", Arrays.asList(Integer.toString(yearMonthDay[0]).split("")));

		parameters.put("mainReportParameterMap", mainReportParameterMap);
		parameters.put("SUBREPORT_DIR", PlatformConstants.getConfigRootAbsPath() + "/templates/reports/GSTR3/");

	}

}
