/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.PerfUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Duminda G
 * @since 2.0
 */
public class UpdateAgentCredit {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(UpdateAgentCredit.class);

	/**
	 * Process UpdateAgentCredit
	 * 
	 * @param executionDate
	 * @throws ModuleException
	 */
	private static void adjustCreditLimitLocalCurrencyAgents(Date date) throws ModuleException {
		ScheduledservicesUtils.getTravelAgentBD().adjustCreditLimitForAgents(date);
	}

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute(Date executionDate) throws Exception {
		try {
			log.info("#################################################################");
			log.info("######### Initiated adjustCreditLimitLocalCurrencyAgents  #######");

			CredentialInvokerUtil.invokeCredentials();

			if (executionDate == null) {
				executionDate = getCurrentDate();
			}

			adjustCreditLimitLocalCurrencyAgents(executionDate);

			log.info("######## Finished adjustCreditLimitLocalCurrencyAgents "
					+ PerfUtil.getExecutionTime(executionDate, new Date()) + "#######");
			log.info("##############################################################");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	/**
	 * Get current Sysdate
	 * 
	 * @return
	 */
	private static Date getCurrentDate() {
		String strTimeZone = "GMT";
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

}
