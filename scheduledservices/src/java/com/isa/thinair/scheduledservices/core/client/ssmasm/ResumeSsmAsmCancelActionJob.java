package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ResumeSsmAsmCancelActionJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(ResumeSsmAsmCancelActionJob.class);
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("######### ResumeSsmAsmCancelActionJob  invoked ##########");
		log.info("######### ResumeSsmAsmCancelActionJob started at " + new Date());

		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getScheduleBD().resumeSsmAsmWaitingCancelActions();

		} catch (ModuleException e) {
			log.error("Error @ ResumeSsmAsmCancelActionJob " + e.getMessage());
		}
		log.info("######### ResumeSsmAsmCancelActionJob ended at " + new Date());
		
	}
}
