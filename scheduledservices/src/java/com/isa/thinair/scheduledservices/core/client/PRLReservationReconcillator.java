package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PRLReservationReconcillator {

	private static Log log = LogFactory.getLog(PRLReservationReconcillator.class);

	public static void reconcilePRLReservations() throws Exception {
		try {
			log.info("Before Reconciling the PRL ");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getPRLBD().processPRLMessage();
			log.info("After Reconciling the PRL ");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
