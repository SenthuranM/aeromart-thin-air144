package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class IBEOnholdReservationEmailNotifierJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(IBEOnholdReservationEmailNotifierJob.class);

	private int createdBeforeXMinutes;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			// the window size should be at least the size of the scheduler repetition period
			int windowSizeInMinutes = CalendarUtil.getTimeDifferenceInMinutes(arg0.getFireTime(), arg0.getNextFireTime());
			// add a buffer time because it is possible that the difference between fireTime and nextFireTime
			// is not exactly the repetition interval.
			windowSizeInMinutes = windowSizeInMinutes + 1;
			IBEOnholdReservationEmailNotifier notifier = new IBEOnholdReservationEmailNotifier(createdBeforeXMinutes,
					windowSizeInMinutes);
			notifier.sendEmailNotification();
		} catch (Exception ex) {
			log.error("Schedule IBE OnHold notifications failed", ex);
			throw new JobExecutionException(ex);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	public int getCreatedBeforeXMinutes() {
		return createdBeforeXMinutes;
	}

	public void setCreatedBeforeXMinutes(int createdBeforeXMinutes) {
		this.createdBeforeXMinutes = createdBeforeXMinutes;
	}
}
