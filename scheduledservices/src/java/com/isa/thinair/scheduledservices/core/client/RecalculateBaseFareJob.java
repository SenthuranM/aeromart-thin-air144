package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Re-calculate base fare with exchange rate change
 * 
 * @author Noshani Perera
 */
public class RecalculateBaseFareJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(RecalculateBaseFareJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			RecalculateBaseFare.execute(null);
		} catch (Exception e) {
			log.error("Error in RecalculateBaseFare", e);
			throw new JobExecutionException(e);
		}
	}

}
