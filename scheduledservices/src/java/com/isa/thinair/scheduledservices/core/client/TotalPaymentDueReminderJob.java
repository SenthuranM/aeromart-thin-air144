package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class TotalPaymentDueReminderJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(TotalPaymentDueReminderJob.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		CredentialInvokerUtil.invokeCredentials();
		TotalPaymentNotification totalPaymentNotify = new TotalPaymentNotification();
		try {
			totalPaymentNotify.execute();
		} catch (Exception e) {
			log.error("Error in executing total payment due reminder job", e);
			throw new JobExecutionException(e);
		}

	}

}
