package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ReSendPALCALJob extends QuartzJobBean {

	private Log log = LogFactory.getLog(ReSendPALCALJob.class);

	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
		try {
			log.info("######[PAL CAL MESSAGE RESENDER IS STARTING]");
			log.info("###### ------------------------------------");
			PalCalResender.checkAndResend();
			log.info("###### [PAL CAL MESSAGE RESENDER HAS COMPLETED]");
		} catch (Exception e) {
			log.error("### ERROR occured in JOb: " + e);
			throw new JobExecutionException("job.resend.failed");
		}
	}

}
