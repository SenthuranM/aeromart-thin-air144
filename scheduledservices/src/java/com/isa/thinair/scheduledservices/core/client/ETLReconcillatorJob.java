package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ETLReconcillatorJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ETLReservationReconcillator.reconcileETLReservations();
		} catch (Exception ex) {
			throw new JobExecutionException(ex);
		}
	}
}
