package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author isuru
 */
public class CreditAcquirer {

	public static void acquireExpiredCredit() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			Date today = new Date();
			ScheduledservicesUtils.getPassengerBD().expirePassengerCredits(today);
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
