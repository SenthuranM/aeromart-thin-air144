package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.CalendarUtil;

public class CashSalesPosterJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(CashSalesPosterJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		synchronized (this) {
			try {
				Date d = CalendarUtil.getLastMomentOfPreviousDaY();
				CashSalesPoster.transferCashSales(CalendarUtil.formatForSQL(d, "dd-MM-yyyy"));
			} catch (Exception ce) {
				throw new JobExecutionException(ce);
			}
		}
	}

}
