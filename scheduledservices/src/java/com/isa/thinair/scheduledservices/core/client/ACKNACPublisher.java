package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ACKNACPublisher {
	private static final Log log = LogFactory.getLog(ACKNACPublisher.class);

	public static void execute() throws ModuleException {
		log.info("######### ACK NAC Publisher Job invoked ##########");
		log.info("######### ACK NAC Publisher Job started at " + new Date());
		ScheduledservicesUtils.getMessageBrokerServiceBD().sendACKNACMessages();
		log.info("######### ACK NAC Publisher Job ended at " + new Date());
	}
}
