package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Updates agent credit limit with exchange rate change
 * 
 * @author Duminda G
 * @since 19-Jan-2009
 */
public class UpdateAgentCreditJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(UpdateAgentCreditJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			UpdateAgentCredit.execute(null);
		} catch (Exception e) {
			log.error("Error in UpdateAgentCredit", e);
			throw new JobExecutionException(e);
		}
	}

}
