/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Noshani Perera
 * 
 */
public class RecalculateChargesInLocalCurr {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(RecalculateChargesInLocalCurr.class);

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute(Date executionDate) throws Exception {
		try {
			log.info("#######################################################");
			log.info("########## reCalcChargesInLocalCurrency #######");

			CredentialInvokerUtil.invokeCredentials();

			if (executionDate == null) {
				executionDate = getCurrentDate();
			}

			log.info("########## Started " + executionDate + " ######");

			if ("Y".equals(ScheduledservicesUtils.getGlobalConfig().getBizParam(
					SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY))) {
				ScheduledservicesUtils.getchargeBD().reCalcChargesInLocalCurr(executionDate);
			}

			log.info("########## Finished reCalcChargesInLocalCurrency ######");
			log.info("###############################################################");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	/**
	 * Get current Sysdate
	 * 
	 * @return
	 */
	private static Date getCurrentDate() {
		String strTimeZone = "GMT";
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

}
