/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Duminda G
 */
public class EmailFlightSchedule {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(EmailFlightSchedule.class);

	/**
	 * Main execution
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static void execute() throws Exception {

		try {
			log.info("#######################################################");
			log.info("########## Starting Sending Flight Schedule ###########");

			CredentialInvokerUtil.invokeCredentials();

			log.info("########## Sending Flight Schedule Initiated ##########");
			ScheduledservicesUtils.getScheduleBD().sendFlightSchedules();
			log.info("########## Sending Flight Schedule Completed ##########");

			log.info("########## Sending Flight Schedule ####################");
			log.info("#######################################################");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
