package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ParsedPFSReconcilator {
	private static Log log = LogFactory.getLog(ParsedPFSReconcilator.class);

	public static void reconcileParsedPFS() throws Exception {
		try {
			log.info("Before Reconcile parsed PFS");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationBD().processParsedPFSReconcilation();
			log.info("After Reconcile parsed PFS");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}