package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author isuru
 * @author Chamindap modified and changed. Module = Reservation Interface = ReservationAuxilliaryBD
 */
public class CreditCardSalesPoster {

	/**
	 * Method to check transfer credit sales.
	 * 
	 * @throws ModuleException
	 */
	public static void transferCreditCardSales(Date date) throws Exception {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			// get the credit card payments frrom t_pax_transaction.

			// save to t_credit_Sales in internal sys datablase.

			// save list to external t_int_credit_sales datablse.

			// update status of t_Credit_sales if external was updated.

			ScheduledservicesUtils.getReservationAuxilliaryBD().transferCreditCardSales(date);
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
