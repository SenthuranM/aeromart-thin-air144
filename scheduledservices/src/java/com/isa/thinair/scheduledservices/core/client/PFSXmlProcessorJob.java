package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class PFSXmlProcessorJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			PFSXmlProcessor.processPFSXmls(null);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}
