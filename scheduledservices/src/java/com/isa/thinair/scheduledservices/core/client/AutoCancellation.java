package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class AutoCancellation {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(AutoCancellation.class);

	public boolean execute() throws ModuleException {
		log.info("#######################################################");
		log.info("###### Starting Auto Cancellation Scheduler job #######");

		CredentialInvokerUtil.invokeCredentials();

		if (AppSysParamsUtil.isAutoCancellationEnabled()) {
			log.info("Auto Cancellation Enabled");
			ScheduledservicesUtils.getReservationBD().executeAutoCancellation(CalendarUtil.getCurrentSystemTimeInZulu(), null,
					null, null);
		} else {
			log.info("Auto Cancellation Disabled");
		}
		log.info("############# Completed Auto Cancellation #############");
		log.info("#######################################################");
		return true;
	}
}
