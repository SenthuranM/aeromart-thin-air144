package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.IBEOnHoldPassengersDTO;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class IBEOnHoldReportGenerator extends ReportGeneratorCommon {
	
	private static Log log = LogFactory.getLog(IBEOnHoldReportGenerator.class);
	
	@Override
	public String generateReport(ScheduledReport scheduledReport)
			throws ModuleException {


		String fileName = null;
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream.fromXML(scheduledReport
				.getCriteriaTemplete());
		Map<String, Object> parameters = (Map<String, Object>) xstream.fromXML(scheduledReport.getParamTemplete());

		// inject the date range
		injectRecalulatedParams(reportsSearchCriteria, parameters, scheduledReport);

		// register the template
		String reportPath = PlatformConstants.getConfigRootAbsPath() + scheduledReport.getReportTempleteRelativePath();
		ScheduledservicesUtils.getReportingFrameworkBD().storeJasperTemplateRefs(scheduledReport.getReportName(), reportPath);

		// Retrieve the result set based on the parameters regenerated
		List<IBEOnHoldPassengersDTO> colReportData = getResultSet(reportsSearchCriteria);

		// generate the report and save it in the report store and email it
		if (colReportData != null) {

			byte[] reportBytes = this.createReport(scheduledReport, parameters, colReportData, new HashMap<Object, Object>());

			// finding the file name
			fileName = this.createFileName(scheduledReport);

			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			// Code to FTP
			serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			}
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
			}
				ftpUtil.uploadAttachmentWithOutFTP(fileName, reportBytes);
		}

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK && getReportConfig().isCopyToPublicDir()) {
			copyReportsToReportsStore(fileName);
		}

		return fileName;
	
	}
	
	
	public List<IBEOnHoldPassengersDTO> getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		
		ResultSet resultSet = ScheduledservicesUtils.getDataExtractionBD().getIBEOnholdPassengersData(reportsSearchCriteria);
		return  createIBEOnHoldPassengersDTOList(resultSet,
				(reportsSearchCriteria.getStation()), reportsSearchCriteria.isShowInLocalTime());
		
		
	}

	@Override
	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy");
	}
	private static List<IBEOnHoldPassengersDTO> createIBEOnHoldPassengersDTOList(ResultSet ibeOnHoldPaxDetailsResults,
			String agentStation, boolean showInLocal) {

		List<IBEOnHoldPassengersDTO> ibeOnHoldPassengerList = new ArrayList<IBEOnHoldPassengersDTO>();
		try {

			if (showInLocal) {
				while (ibeOnHoldPaxDetailsResults.next()) {
					IBEOnHoldPassengersDTO ibeOnholdPassenger = new IBEOnHoldPassengersDTO();
					try {

						ibeOnholdPassenger.setReservationDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("RESERVATION_DATE"), "dd/MM/yy"),
								agentStation), "dd/MM/yy"));
						ibeOnholdPassenger.setReleaseDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("RELEASE_DATE"), "dd/MM/yy HH:mm:ss"),
								agentStation), "dd/MM/yy HH:mm:ss"));
						ibeOnholdPassenger.setFlightDate(ibeOnHoldPaxDetailsResults.getString("FLIGHT_DATE"));
						ibeOnholdPassenger.setFlightNumber(ibeOnHoldPaxDetailsResults.getString("FLIGHT_NUMBER"));
						ibeOnholdPassenger.setPnr(ibeOnHoldPaxDetailsResults.getString("PNR"));
						ibeOnholdPassenger.setNoOfPaxOnHold(String.valueOf(ibeOnHoldPaxDetailsResults
								.getString("NO_OF_PAX_ONHOLD")));
						ibeOnholdPassenger.setContactName(ibeOnHoldPaxDetailsResults.getString("CONTACT_NAME"));
						ibeOnholdPassenger.setSector(ibeOnHoldPaxDetailsResults.getString("SECTOR"));
						ibeOnholdPassenger.setResTel(ibeOnHoldPaxDetailsResults.getString("RES_TEL"));
						ibeOnholdPassenger.setMobile(ibeOnHoldPaxDetailsResults.getString("MOBILE"));
						ibeOnholdPassenger.setSegStatus(ibeOnHoldPaxDetailsResults.getString("SEG_STATUS"));
						ibeOnholdPassenger.setMxTime(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("MX_TIME"), "dd/MM/yy HH:mm"),
								agentStation), "dd/MM/yy HH:mm"));
						ibeOnholdPassenger.setModDate(DateUtil.formatDate(DateUtil.getLocalDateTime(
								DateUtil.parseDate(ibeOnHoldPaxDetailsResults.getString("MOD_DATE"), "dd/MM/yy HH:mm"),
								agentStation), "dd/MM/yy HH:mm"));

						ibeOnHoldPassengerList.add(ibeOnholdPassenger);
					} catch (ParseException e) {
						log.error(e);
					} catch (ModuleException e1) {
						log.error(e1);
					}
				}
			} else {
				while (ibeOnHoldPaxDetailsResults.next()) {
					IBEOnHoldPassengersDTO ibeOnholdPassenger = new IBEOnHoldPassengersDTO();
					ibeOnholdPassenger.setReservationDate(ibeOnHoldPaxDetailsResults.getString("RESERVATION_DATE"));
					ibeOnholdPassenger.setReleaseDate(ibeOnHoldPaxDetailsResults.getString("RELEASE_DATE"));
					ibeOnholdPassenger.setFlightDate(ibeOnHoldPaxDetailsResults.getString("FLIGHT_DATE"));
					ibeOnholdPassenger.setFlightNumber(ibeOnHoldPaxDetailsResults.getString("FLIGHT_NUMBER"));
					ibeOnholdPassenger.setPnr(ibeOnHoldPaxDetailsResults.getString("PNR"));
					ibeOnholdPassenger.setNoOfPaxOnHold(String.valueOf(ibeOnHoldPaxDetailsResults.getString("NO_OF_PAX_ONHOLD")));
					ibeOnholdPassenger.setContactName(ibeOnHoldPaxDetailsResults.getString("CONTACT_NAME"));
					ibeOnholdPassenger.setSector(ibeOnHoldPaxDetailsResults.getString("SECTOR"));
					ibeOnholdPassenger.setResTel(ibeOnHoldPaxDetailsResults.getString("RES_TEL"));
					ibeOnholdPassenger.setMobile(ibeOnHoldPaxDetailsResults.getString("MOBILE"));
					ibeOnholdPassenger.setSegStatus(ibeOnHoldPaxDetailsResults.getString("SEG_STATUS"));
					ibeOnholdPassenger.setMxTime(ibeOnHoldPaxDetailsResults.getString("MX_TIME"));
					ibeOnholdPassenger.setModDate(ibeOnHoldPaxDetailsResults.getString("MOD_DATE"));
					ibeOnHoldPassengerList.add(ibeOnholdPassenger);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ibeOnHoldPassengerList;
	}

	public void copyReportsToReportsStore(String fileName) throws ModuleException {

		String reportPath = PlatformConstants.getAbsAttachmentsPath() + File.separator + fileName;
		File src = new File(reportPath);

		String destPath = AppSysParamsUtil.getReportsStorageLocation() + File.separator + fileName;
		File dest = new File(destPath);

		try {
			Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			log.error("file copy failed -- src : " + src.getAbsolutePath() + " -- dest : " + dest.getAbsolutePath() , e);
			throw new ModuleException("file copy failed", e);
		}
	}
}
