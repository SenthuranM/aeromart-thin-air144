package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class generalizes report options - No FTP option is enabled in this
 * 
 * @author jpadukka
 * 
 */
public abstract class CompleteReportGenerator extends ReportGeneratorCommon {

	private static final Log log = LogFactory.getLog(CompleteReportGenerator.class);

	protected String generatedReportFileUniqueID;
	protected String generatedReportFileNamePrefix;
	protected Map reportConfigurationMap;

	@Override
	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {

		String reportFileName = null;

		generatedReportFileUniqueID = this.generateUniqueID();
		ReportsSearchCriteria reportSearchCriteria = extractReportSearhCriteria(scheduledReport);
		Map<String, Object> parameters = extractParamsMap(scheduledReport);
		injectRecalulatedParams(reportSearchCriteria, parameters, scheduledReport);
		reportSearchCriteria.setUserId(generatedReportFileUniqueID);

		// Load report name + report option, check what is in configuration, and then decide

		if (reportConfigurationMap != null) {

			if (reportConfigurationMap.containsKey(reportSearchCriteria.getReportOption() + "_DOWNLOAD")) {
				String isDownloadLink = reportConfigurationMap.get(reportSearchCriteria.getReportOption() + "_DOWNLOAD")
						.toString();

				if (isDownloadLink != null && isDownloadLink.equals("true")) {
					// Returns link of the remotely generated locally mounted file
					generateReportFile(reportSearchCriteria);
					setDownloadable(true);
					reportFileName = generatedReportFileNamePrefix + generatedReportFileUniqueID
							+ getGeneratedFileFormat();
				} else {
					setDownloadable(false);
					reportFileName = generateAttachementReport(scheduledReport, reportSearchCriteria, parameters);
				}
			} else {
				// Choose generalized option

				if (isDownloadable() != null) {
					if (isDownloadable()) {
						generateReportFile(reportSearchCriteria);
						reportFileName = generatedReportFileNamePrefix
								+ generatedReportFileUniqueID + getGeneratedFileFormat();
					} else {
						reportFileName = generateAttachementReport(scheduledReport, reportSearchCriteria, parameters);
					}

				} else {
					throw new ModuleException("scheduled report download option not defined ");
				}

			}
		} else {
			// Directly calls generalized option
			if (isDownloadable() != null) {
				if (isDownloadable()) {
					generateReportFile(reportSearchCriteria);
					reportFileName = AppSysParamsUtil.getReportURLPRefix() + generatedReportFileNamePrefix + generatedReportFileUniqueID
							+ getGeneratedFileFormat();
				} else {
					reportFileName = generateAttachementReport(scheduledReport, reportSearchCriteria, parameters);
				}

			} else {
				throw new ModuleException("scheduled report download option not defined ");
			}
		}

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK && getReportConfig().isCopyToPublicDir()) {
			copyReportsToReportsStore(reportFileName);
		}

		return reportFileName;
	}

	protected abstract void generateReportFile(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	protected abstract ResultSet getReportDataForAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	protected ReportsSearchCriteria extractReportSearhCriteria(ScheduledReport scheduledReport) {
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream.fromXML(scheduledReport
				.getCriteriaTemplete());
		return reportsSearchCriteria;
	}

	protected Map<String, Object> extractParamsMap(ScheduledReport scheduledReport) {
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		Map<String, Object> parameters = (Map<String, Object>) xstream.fromXML(scheduledReport.getParamTemplete());

		return parameters;
	}

	protected String generateAttachementReport(ScheduledReport scheduledReport, ReportsSearchCriteria reportSearchCriteria,
			Map parameters) throws ModuleException {
		// Attachment
		String fileName = null;

		// register the template
		String reportPath = PlatformConstants.getConfigRootAbsPath() + scheduledReport.getReportTempleteRelativePath();
		ScheduledservicesUtils.getReportingFrameworkBD().storeJasperTemplateRefs(scheduledReport.getReportName(), reportPath);

		// Retrieve the result set based on the parameters regenerated
		ResultSet resultSet = getReportDataForAttachment(reportSearchCriteria);

		// generate the report and save it in the report store and email it
		if (resultSet != null) {

			byte[] reportBytes = this.createReport(scheduledReport, parameters, resultSet, new HashMap<Object, Object>());

			// finding the file name
			fileName = this.createFileName(scheduledReport);

			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			// Code to FTP
			serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			}
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
			}
			ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());
			ftpUtil.uploadAttachment(fileName, reportBytes, serverProperty);
		}

		return fileName;
	}

	public String getGeneratedReportFileUniqueID() {
		return generatedReportFileUniqueID;
	}

	public void setGeneratedReportFileUniqueID(String generatedReportFileUniqueID) {
		this.generatedReportFileUniqueID = generatedReportFileUniqueID;
	}

	public String getGeneratedReportFileNamePrefix() {
		return generatedReportFileNamePrefix;
	}

	public void setGeneratedReportFileNamePrefix(String generatedReportFileNamePrefix) {
		this.generatedReportFileNamePrefix = generatedReportFileNamePrefix;
	}

	public Map getReportConfigurationMap() {
		return reportConfigurationMap;
	}

	public void setReportConfigurationMap(Map reportConfigurationMap) {
		this.reportConfigurationMap = reportConfigurationMap;
	}

	public Boolean isDownloadable() {
		return getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK;
	}

	public void setDownloadable(boolean downloadable) {
		getReportConfig().setDeliveryMode(downloadable ? ReportConfig.DeliveryMode.LINK : ReportConfig.DeliveryMode.ATTACHMENT);
	}

	public String getGeneratedFileFormat() {
		return getReportConfig().getFileExtension();
	}

	public void setGeneratedFileFormat(String fileFormat) {
		getReportConfig().setFileExtension(fileFormat);
	}


	@Override
	public Boolean isEmailAttachmentReport() throws ModuleException {

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK) {
			return false;
		} else {
			return true;
		}

	}

	public void copyReportsToReportsStore(String fileName) throws ModuleException {

		String reportPath = PlatformConstants.getAbsAttachmentsPath() + File.separator + fileName;
		File src = new File(reportPath);

		String destPath = AppSysParamsUtil.getReportsStorageLocation() + File.separator + fileName;
		File dest = new File(destPath);

		try {
			Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			log.error("file copy failed -- src : " + src.getAbsolutePath() + " -- dest : " + dest.getAbsolutePath() , e);
			throw new ModuleException("file copy failed", e);
		}
	}

}
