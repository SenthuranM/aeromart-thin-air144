package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class BSPSales {
	private static Log log = LogFactory.getLog(BSPSales.class);

	public static void execute(int salesFrequncyInDays) throws Exception {

		log.info("######### BSP Sales Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();

			Date date = new Date();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
			date = fmt.parse(fmt.format(date)); // Convert the date to yyyy/MM/dd 00:00

			// toDate = yyyy/MM/(dd-1) 11:59
			Date toDate = new Date(date.getTime() - TimeUnit.SECONDS.toMillis(1));

			// fromDate = (yyyy/MM/(dd-salesFrequncyInDays) 00:00)
			Date fromDate = new Date(date.getTime() - TimeUnit.DAYS.toMillis(salesFrequncyInDays));
			log.info("######### BSP Sales Job started for RET file generation at " + new Date() + "for sales Frequency"
					+ salesFrequncyInDays);
			ScheduledservicesUtils.getInvoicingBD().publishDailySalesToBSP(fromDate, toDate, false, null, null);
			log.info("######### BSP Sales Job ended RET file generation at " + new Date());
		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "BSP RET File creation failed!");
		}

		try {
			log.info("######### BSP Sales Job started RET file upload ##########");
			ScheduledservicesUtils.getInvoicingBD().uploadPendingRETFiles();
			log.info("######### BSP Sales Job ended for RET file upload ##########");

		} catch (Exception e) {
			log.error(e);
			CommonUtil.reportSheduleFailure(e, "BSP Scheduler Failed!");
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
		log.info("######### BSP Sales Job ended at " + new Date());
	}

	/**
	 * Method used for bsp transfer to external (X3) system Consist of logic to find the start and end date
	 * 
	 * @throws Exception
	 */
	public static void transferBSPSales() throws Exception {

		log.info("######### BSP Details transfer Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			Calendar aCalendar = Calendar.getInstance();
			Date startDate = new Date();
			Date endDate = new Date();
			boolean flag = false;
			SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
			String dayNum = sdfDay.format(startDate);
			int dayNumber = Integer.valueOf(dayNum);
			if (dayNumber == 1) {// need to get the bsp sales from previous
									// month 24th to 30th 0r 31th
				aCalendar.add(Calendar.MONTH, -1);
				aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				endDate = CalendarUtil.getLastMomentOfDate(aCalendar.getTime());
				aCalendar.set(Calendar.DATE, 24);
				startDate = CalendarUtil.getStartTimeOfDate(aCalendar.getTime());
				flag = true;
			} else if (dayNumber == 8) {// need to get the bsp sales from 1 to
										// 7th of current month
				aCalendar.set(Calendar.DATE, 1);
				startDate = CalendarUtil.getStartTimeOfDate(aCalendar.getTime());
				aCalendar.set(Calendar.DATE, 7);
//				endDate = aCalendar.getTime();
				endDate = CalendarUtil.getLastMomentOfDate(aCalendar.getTime());
				flag = true;
			} else if (dayNumber == 16) {// need to get the bsp sales from 8 to
											// 15th of current month
				aCalendar.set(Calendar.DATE, 8);
				startDate = CalendarUtil.getStartTimeOfDate(aCalendar.getTime());
				aCalendar.set(Calendar.DATE, 15);
//				endDate = aCalendar.getTime();
				endDate = CalendarUtil.getLastMomentOfDate(aCalendar.getTime());
				flag = true;
			} else if (dayNumber == 24) {// need to get the bsp sales from from
											// 17 to 23rd of current month
				aCalendar.set(Calendar.DATE, 16);
				startDate = CalendarUtil.getStartTimeOfDate(aCalendar.getTime());
				aCalendar.set(Calendar.DATE, 23);
//				endDate = aCalendar.getTime();
				endDate = CalendarUtil.getLastMomentOfDate(aCalendar.getTime());
				flag = true;
			}

			if (flag) {
				SearchInvoicesDTO searchDTO = new SearchInvoicesDTO();
				searchDTO.setInvoiceDateFrom(startDate);
				searchDTO.setInvoiceDateTo(endDate);
				ScheduledservicesUtils.getInvoicingBD().transferBSPsToQueue(searchDTO);
			}
		} catch (Exception exp) {
			throw exp;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
