package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author byorn
 */
public class TransferInvoicesJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(TransferInvoicesJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();

			Date currentDate = new Date();
			SimpleDateFormat dateFormatDay = new SimpleDateFormat("dd");
			String dayNum = dateFormatDay.format(currentDate);

			if (Integer.valueOf(dayNum).intValue() == 16 || Integer.valueOf(dayNum).intValue() == 1) {

				GlobalConfig globalConfig = (GlobalConfig) LookupServiceFactory.getInstance().getBean(
						PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
				if ("Y".equals(globalConfig.getBizParam(SystemParamKeys.TRANSFER_INVOICES))) {

					log.info("############## Auto Transfering Invoicing Started for " + new Date());
					GregorianCalendar gc = new GregorianCalendar();
					int year = gc.get(GregorianCalendar.YEAR);
					int month = gc.get(GregorianCalendar.MONTH);
					int period = 0;

					if (Integer.valueOf(dayNum).intValue() == 16) {
						period = 1;
						month = month + 1;
					} else {
						period = 2;
					}

					SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
					searchInvoicesDTO.setInvoicePeriod(period);
					searchInvoicesDTO.setYear(year);
					searchInvoicesDTO.setMonth(month);

					ScheduledservicesUtils.getInvoicingBD().transferToInterfaceTables(searchInvoicesDTO);
				}
			}
			log.info("############## Auto Transfering Invoicing Ended for " + new Date());
		} catch (Exception me) {
			log.error("Errror when auto invoicing" + me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
