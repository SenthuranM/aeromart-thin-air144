/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

/**
 * @author panchatcharam.s
 *
 */
public class AutomaticCheckinDetailsSender implements Job {
	private Log log = LogFactory.getLog(AutomaticCheckinDetailsSender.class);
	private final String INITIAL_ATTEMPT = "IA";
	private final Integer DCS_FIRST_ATTEMPT = 1;

	/**
	 * JOB EXCECUTION CONTEXT
	 */
	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
		String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
		this.executeAction(flightId);
	}

	public void executeAction(String flightId) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			if (AppSysParamsUtil.isAutomaticCheckinEnabled()) {
				Integer pnrpaxId = 0;
				log.info(" #####################     AutomaticCheckinDetailsSender JOB STARTED ######################");
				log.info(" ----------------------------------------------------------------");
				log.info(" #####################     AutomaticCheckinDetailsSender FOR FLIGHT ID : " + flightId);
				log.info(" ----------------------------------------------------------------");
				sendAutomaticCheckinDetails(Integer.parseInt(flightId), pnrpaxId);
				log.info("#################### AutomaticCheckinDetailsSender Completed  #############################");
			} else {
				log.info("#################### Disabled AutomaticCheckinDetailsSender   #############################");
			}

		} catch (Exception me) {
			log.error("AutomaticCheckinDetailsSender failed", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	/**
	 * 
	 * @param flightId
	 * @throws ModuleException
	 * @throws Exception
	 */
	public void sendAutomaticCheckinDetails(Integer flightId, Integer pnrpaxId) throws ModuleException, Exception {
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendAutomaticCheckinDetails(pnrpaxId, flightId, DCS_FIRST_ATTEMPT,
				INITIAL_ATTEMPT, null);
		ScheduledservicesUtils.getReservationAuxilliaryBD().updateCreiditNoteforPassenger(flightId);
	}
}
