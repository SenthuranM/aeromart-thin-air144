package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionException;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author isuru
 */
public class IntermediateReservationTrasher {

	public static void clearTempBCAlloc() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();

			ScheduledservicesUtils.getFlightInventoryResBD().cleanupBlockedSeats(true);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
