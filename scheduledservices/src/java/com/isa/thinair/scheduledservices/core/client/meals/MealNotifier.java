package com.isa.thinair.scheduledservices.core.client.meals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

/**
 * @author Indika Athauda
 */
public class MealNotifier implements Job {

	private Log log = LogFactory.getLog(MealNotifier.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();

			String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
			String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);

			String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
			
			log.info(" #####################     MEAL NOTIFICATION JOB STARTED ######################");
			log.info(" ----------------------------------------------------------------");
			log.info(" #####################     NOTIFICATION FOR FLIGHT ID : " + flightId);
			log.info(" #####################     DEPATURE STATION : " + depAirport);
			log.info(" #####################     FLIGHT NUMBER : " + flightNumber);
			log.info(" ----------------------------------------------------------------");
			sendMealNotification(Integer.parseInt(flightId));

			log.info("#################### Meal Notification Completed  #############################");
		} catch (Exception me) {
			log.error("MealNotifier ", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void sendMealNotification(int flightId) throws ModuleException {
		List<Integer> listFlightIds = new ArrayList<Integer>();
		listFlightIds.add(flightId);
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendMealNotificaions(listFlightIds);
	}

}
