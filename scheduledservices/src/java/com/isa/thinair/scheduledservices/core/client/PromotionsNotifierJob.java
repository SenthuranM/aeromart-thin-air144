package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PromotionsNotifierJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(PromotionsNotifierJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

		try {
			log.info(" ---- promo notifier start");
			CredentialInvokerUtil.invokeCredentials();

			JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
			PromotionType promotionType = (PromotionType) PromotionType.valueOf((String) BeanUtils.getFirstElement(jobDataMap.values()));
			ScheduledservicesUtils.getPromotionAdministrationBD().notifyPromotionOffer(promotionType);
			log.info(" ---- promo notifier end");
		} catch (Exception e) {
			log.error("PromotionsNotifierJob ", e);
			throw new JobExecutionException(e);
		}

	}
}
