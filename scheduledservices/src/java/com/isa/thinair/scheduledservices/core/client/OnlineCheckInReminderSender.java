package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class OnlineCheckInReminderSender implements Job {

	private Log log = LogFactory.getLog(OnlineCheckInReminderSender.class);

	@Override
	public void execute(JobExecutionContext executionContext) throws JobExecutionException {

		String flightNumber = null;
		String originAirport = null;
		Integer flightSegId = null;
		Integer flightSegNotificationId = null;
		try {
			CredentialInvokerUtil.invokeCredentials();
			JobDetail jobDetail = executionContext.getJobDetail();
			OnlineCheckInReminderDTO onlineCheckInReminderDTO = new OnlineCheckInReminderDTO();

			String jobName = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
			String JobGroupName = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
			flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
			originAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
			Date flightDepartureDateTimeZulu = (Date) jobDetail.getJobDataMap().get(Job.PROP_DEPARTURE_DATE_ZULU);
			flightSegId = jobDetail.getJobDataMap().getInt(Job.PROP_FlightSegId);
			flightSegNotificationId = jobDetail.getJobDataMap().getInt(Job.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID);

			onlineCheckInReminderDTO.setJobName(jobName);
			onlineCheckInReminderDTO.setJobGroupName(JobGroupName);
			onlineCheckInReminderDTO.setFlightNo(flightNumber);
			onlineCheckInReminderDTO.setOrigin(originAirport);
			onlineCheckInReminderDTO.setDepartureDate(flightDepartureDateTimeZulu);
			onlineCheckInReminderDTO.setFltSegId(flightSegId);
			onlineCheckInReminderDTO.setFlightSegmentNotificationId(flightSegNotificationId);

			log.debug(" ---------------------------------------------------------------------------------------");
			log.info(" #####################     ONLINE CHECKIN REMINDER SUB JOB STARTED ######################");
			log.debug(" #####################     NOTIFICATION FOR FLIGHT NUMBER :     " + flightNumber);
			log.debug(" #####################     NOTIFICATION FOR FLIGHT SEG ID	: 	  " + flightSegId);
			log.debug(" #####################     DEPATURE STATION 				:     " + originAirport);
			log.debug(" ---------------------------------------------------------------------------------------");

			sendNotification(onlineCheckInReminderDTO);
			log.info("#################### ONLINE CHECKIN REMINDER SUB JOB Completed  #############################");

		} catch (Exception exception) {
			log.error("###################  EXCEPTION ARAISED IN SUB JOB WHILE SENDING REMINDER ONLINE CHECKIN #########");
			log.error("###################  DEPATURE STATION 				:   " + originAirport);
			log.error("###################  FLIGHT NUMBER 					:   " + flightNumber);
			log.error(" ################### FLIGHT SEG ID 							: 	" + flightSegId);
			log.error("###################  EXCEPTION MESSAGE 				:   " + exception.getMessage());
			log.error("###################  EXCEPTION DETAILS 				:   ");
			log.error(exception);
			throw new JobExecutionException(exception);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

	private void sendNotification(OnlineCheckInReminderDTO onlineCheckInReminderDTO) throws ModuleException {
		log.debug(" #####################     Giving Call to AirReservation    ######################");
		ScheduledservicesUtils.getReservationBD().sendOnlineCheckInReminder(onlineCheckInReminderDTO);
		log.debug(" #####################     AirReservation Process Completed ######################");
	}

}
