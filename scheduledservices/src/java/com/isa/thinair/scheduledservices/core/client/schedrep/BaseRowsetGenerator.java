package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseRowsetGenerator extends ReportGeneratorCommon {

	private static final Log log = LogFactory.getLog(BaseRowsetGenerator.class);

	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {

		String fileName = null;
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream.fromXML(scheduledReport
				.getCriteriaTemplete());
		Map<String, Object> parameters = (Map<String, Object>) xstream.fromXML(scheduledReport.getParamTemplete());

		// inject the date range
		injectRecalulatedParams(reportsSearchCriteria, parameters, scheduledReport);

		// register the template
		String reportPath = PlatformConstants.getConfigRootAbsPath() + scheduledReport.getReportTempleteRelativePath();
		ScheduledservicesUtils.getReportingFrameworkBD().storeJasperTemplateRefs(scheduledReport.getReportName(), reportPath);

		// Retrieve the result set based on the parameters regenerated
		ResultSet resultSet = getResultSet(reportsSearchCriteria);

		// generate the report and save it in the report store and email it
		if (resultSet != null) {

			byte[] reportBytes = this
					.createReport(scheduledReport, parameters, resultSet, new HashMap<Object, Object>());

			// finding the file name
			fileName = this.createFileName(scheduledReport);

			uploadOrAttachFile(scheduledReport, fileName, reportBytes);

		}

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK && getReportConfig().isCopyToPublicDir()) {
			copyReportsToReportsStore(fileName);
		}

		return fileName;
	}

	public abstract ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

}
