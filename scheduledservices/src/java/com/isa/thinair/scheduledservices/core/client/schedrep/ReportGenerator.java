package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.ScheduledReport;

public interface ReportGenerator {

	public String generateReport(ScheduledReport scheduledReport) throws ModuleException;

	public Boolean isEmailAttachmentReport() throws ModuleException;
}
