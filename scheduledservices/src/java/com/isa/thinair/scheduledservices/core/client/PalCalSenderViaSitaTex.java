package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PalCalSenderViaSitaTex extends QuartzJobBean {

	private static Log log = LogFactory.getLog(PalCalSenderViaSitaTex.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getMessageBrokerServiceBD().sendPALCALViaSitaTex();
			log.info("############## Sending PAL CAL via SitaTex" + new Date());

		} catch (Exception e) {
			log.error("Error while sending GDS emails" + e);
			CommonUtil.reportSheduleFailure(e, "Sending PAL CAL via SitaTex Failed!");
			throw new JobExecutionException(e);
		}

	}
	
	public void execute() {
		try {
			ScheduledservicesUtils.getMessageBrokerServiceBD().sendPALCALViaSitaTex();
			log.info("############## Sending PNL ADL via SitaTex" + new Date());
		} catch (Exception e) {
			log.error("Error while sending GDS emails" + e);
			CommonUtil.reportSheduleFailure(e, "Sending PNL ADL via SitaTex Failed!");
		}	
	}

}
