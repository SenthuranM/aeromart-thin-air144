package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ModeOfPaymentGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return ScheduledservicesUtils.getDataExtractionBD().getCallCentreModeOfPaymentsData(reportsSearchCriteria);
	}

}
