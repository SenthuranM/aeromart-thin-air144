/*
 * ==============================================================================
 * ISA Software License, Version 1.0s
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */

package com.isa.thinair.scheduledservices.core.util;

/**
 * To keep track of Reservation Internal Constants
 * 
 * @author Byorn de Silva
 * @since 1.0
 */
public interface SSInternalConstants {
	/** Denotes Transaction Types */
	public static interface CHECK_CONSTRAINT {
		/** Denotes credit transaction type */
		public static final String TRUE = "YES";

		/** Denotes debit transaction type */
		public static final String FALSE = "NO";
	}

	/** Denotes Schedule Service Job Types */
	public static interface JOB_TYPE {
		/** Denotes PNL msg */
		public static final String PNL = "PNL";

		/** Denotes ADL msg */
		public static final String ADL = "ADL";

		public static final String PAL = "PAL";

		public static final String CAL = "CAL";

		/** Denotes Flight Closing Job */
		public static final String FLIGHT_CLOSER = "FLIGHT_CLOSER";

		/** Denotes ANCILLARY_NOTIFICATIO Job */
		public static final String ANCILLARY_NOTIFICATION = "ANCILLARY_NOTIFICATION";

		public static final String ANCILLARY_RECOVERY_NOTIFICATION = "ANCILLARY_RECOVERY_NOTIFICATION";

		public static final String RAK_INSURANCE_PUBLICATION = "RAK_INSURANCE_PUBLICATION";

		public static final String RAK_INSURANCE_PUBLICATION_RECOVERY = "RAK_INSURANCE_PUBLICATION_RECOVERY";

		public static final String MEAL_NOTIFICATION = "MEAL_NOTIFICATION";

		public static final String ONLINE_CHECKING_REMINDER = "ONLINE_CHECKING_REMINDER";

		public static final String GOSHOW_BC_OPENER = "GOSHOW_BC_OPENER";
		
		public static final String ONLINE_CHECKING_REMINDER_RECOVERY = "ONLINE_CHECKING_REMINDER_RECOVERY";
		
		public static final String PNRGOV = "PNRGOV";
		
		public static final String FLIGHT_PNR_FLOWN = "FLIGHT_PNR_FLOWN";
		
		public static final String EMAIL_PROMO_CODES_CAMPAIGN = "EMAIL_PROMO_CODES_CAMPAIGN";
		
		public static final String AUTOMATIC_CHECKIN_SEND_DETAILS = "AUTOMATIC_CHECKIN_SEND_DETAILS";

		public static final String AUTO_CHECKIN_SEAT_MAP = "AUTOMATIC_CHECKIN_SEAT_MAP";
	}
}
