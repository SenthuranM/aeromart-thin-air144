package com.isa.thinair.scheduledservices.core.client;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class IBEOnholdReservationEmailNotifier {
	int confirmedBeforeXMinutes;
	int windowSizeInMinutes;

	public IBEOnholdReservationEmailNotifier(int confirmedBeforeXMinutes, int windowSizeInMinutes) {
		super();
		this.windowSizeInMinutes = windowSizeInMinutes;
		this.confirmedBeforeXMinutes = confirmedBeforeXMinutes;
	}

	public void sendEmailNotification() throws ModuleException {
		OnholdReservatoinSearchDTO onHoldReservationSearchDTO = new OnholdReservatoinSearchDTO();
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.MINUTE, -confirmedBeforeXMinutes);
		onHoldReservationSearchDTO.setBookingTimeStampBeforeEnd(calendarEnd.getTime());
		Calendar calendarStart = Calendar.getInstance();
		// the window should be at least the size of the scheduler repetition interval
		calendarStart.add(Calendar.MINUTE, -confirmedBeforeXMinutes - windowSizeInMinutes);
		onHoldReservationSearchDTO.setBookingTimeStampBeforeStart(calendarStart.getTime());
		onHoldReservationSearchDTO.setOriginCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		
		
		Map<Integer, OnHold> webAndMobileChannels = new HashMap<Integer, OnHold>();
		
		webAndMobileChannels.put(ReservationInternalConstants.SalesChannel.WEB,OnHold.IBEPAYMENT);
		webAndMobileChannels.put(ReservationInternalConstants.SalesChannel.IOS,OnHold.IOS);
		webAndMobileChannels.put(ReservationInternalConstants.SalesChannel.ANDROID,OnHold.ANDROID);

		for (Integer channelCode : webAndMobileChannels.keySet()) {

			if (AppSysParamsUtil.isOnHoldEnable(webAndMobileChannels.get(channelCode))) {
				onHoldReservationSearchDTO.setOwnerChannelCode(channelCode);
				ScheduledservicesUtils.getReservationBD().sendIBEOnholdReservationEmail(onHoldReservationSearchDTO);
			}
		}
	}
}
