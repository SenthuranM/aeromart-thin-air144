package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

/**
 * @author Mohamed Rizwan
 *
 */
public class AutoCheckinSeatMapJob implements Job {

	private Log log = LogFactory.getLog(AutoCheckinSeatMapJob.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
		String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
		executeAction(flightId);

	}

	private void assignSeatByPAX(int flightId) throws ModuleException, Exception {
		log.info(" ----------------------------------------------------------------");
		log.info(" ------Method for derive the Autocheckin seat for Flight-" + flightId);
		ScheduledservicesUtils.getReservationAuxilliaryBD().deriveAutoCheckinSeatMap(flightId);

	}

	public void executeAction(String flightId) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();

			boolean isAutoCheckin = AppSysParamsUtil.isAutomaticCheckinEnabled();

			if (isAutoCheckin) {
				log.info(" #####################     Auto Checkin Seat Map JOB STARTED ######################");
				log.info(" ----------------------------------------------------------------");
				log.info(" #####################     Auto Checkin Seat Map FOR FLIGHT ID : " + flightId);
				log.info(" ----------------------------------------------------------------");
				assignSeatByPAX(Integer.parseInt(flightId));

				log.info("#################### Auto Checkin Seat Map JOB Completed  #############################");
			} else {
				log.info("#################### Auto Checkin app paramater is Disabled #############################");
			}

		} catch (Exception me) {
			log.error("Auto Checkin Seat Selection Failed ", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
