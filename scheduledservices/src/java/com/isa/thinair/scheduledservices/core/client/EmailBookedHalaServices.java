package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airreservation.api.dto.HalaServiceDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.SSRCategoryDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.dto.UpdateEmailStatusInfoDTO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Before 24 hours (parameter) of flight departure, a consolidated report by flight by concerned airport including all
 * the HALA services booked should go out to a pre-stated email id.
 * 
 * @author lalanthi
 */
public class EmailBookedHalaServices {
	private static Log log = LogFactory.getLog(EmailBookedHalaServices.class);

	public static void execute() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			sendBookedHalaServiceNotification();
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	/**
	 * Sends a notification message to a prestated email for the hala services booked.
	 * 
	 * @throws ModuleException
	 */
	private static void sendBookedHalaServiceNotification() throws ModuleException {
		log.info("Started sending notifications for Hala services booked......");
		Collection<HalaServiceDTO> bookedHalaServices = getBookedHalaServicesForPeriod();
		if (bookedHalaServices != null && bookedHalaServices.size() > 0) {
			HashMap<String, ArrayList<HalaServiceDTO>> halaServicesForFlightSegMap = getHalaServicesPerFlightSegment(new ArrayList<HalaServiceDTO>(
					bookedHalaServices));
			composeNotificationForFlightSegment(halaServicesForFlightSegMap);
		}
		log.info("Stopped sending notifications for Hala services booked");
	}

	/**
	 * Gets a collection of Hala services for a given time period.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<HalaServiceDTO> getBookedHalaServicesForPeriod() throws ModuleException {
		String hubAirPort = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		Map<Integer, SSRCategoryDTO> ssrCategorMap = CommonsServices.getGlobalConfig().getSsrCategoryMap();
		SSRCategoryDTO ssrCategory = (SSRCategoryDTO) ssrCategorMap.get(SSRCategory.ID_AIRPORT_SERVICE);
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		Collection<HalaServiceDTO> halaServicesCollection = new ArrayList<HalaServiceDTO>();
		int beginTime = ssrCategory.getNotifyStartCutoverTime();// 1440 - 24 hrs
		int endTime = ssrCategory.getNotifyStopCutoverTime();// 960 - 16 hrs

		Date currentSysDate = CalendarUtil.getCurrentSystemTimeInZulu();
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.setTime(currentSysDate);
		calendar1.add(GregorianCalendar.MINUTE, beginTime);
		// Current time + 24 hrs
		search.setDateRangeTo(CalendarUtil.formatForSQL_toDate(new Timestamp(calendar1.getTime().getTime())));

		GregorianCalendar calendar2 = new GregorianCalendar();
		calendar2.setTime(currentSysDate);
		calendar2.add(GregorianCalendar.MINUTE, endTime);
		// Current time + 16 hrs
		search.setDateRangeFrom(CalendarUtil.formatForSQL_toDate(new Timestamp(calendar2.getTime().getTime())));
		// search.setDateRangeFrom("'2009-12-10 00:00:00','YYYY-MM-DDHH24:MI:SS'");
		// search.setDateRangeTo("'2009-12-13 23:50:00','YYYY-MM-DDHH24:MI:SS'");
		// 20-Sep-2011 01:03:11 - 22-Sep-2011 09:03:11
		search.setStatus(SSRCategory.NOTIFICATION_EMAIL_NOT_SENT);
		search.setSsrCategory(SSRCategory.ID_AIRPORT_SERVICE.toString());
		// search.setStation(hubAirPort); //to retreive respective airports booked Airport service notifications to be
		// sent
		search.setNoOfAttempts(new Integer(ssrCategory.getNotificationAttempts()));
		halaServicesCollection = ScheduledservicesUtils.getDataExtractionBD().getBookedHalaServiceSummaryForNotification(search);
		return halaServicesCollection;
	}

	/**
	 * Sends notification mail for each booked hala service for a given email address.
	 * 
	 * @param topic
	 * @param email
	 */
	private static void sendHalaNotificationMessage(Topic topic, String email) throws ModuleException {

		// Email id list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(email);
		messageList.add(user);

		// Profile List
		List messageProfiles = new ArrayList();
		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);
		profile.setTopic(topic);
		messageProfiles.add(profile);
		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Arranges booked Hala services per airport.
	 * 
	 * @param allHalaServices
	 * @return halaServicesMap
	 */

	private static HashMap<String, HalaServiceDTO> getHalaServicesSummaryByFlightSegment(List<HalaServiceDTO> allHalaServices) {

		HashMap<String, HalaServiceDTO> halaServicesSummaryMap = new HashMap<String, HalaServiceDTO>();

		if (allHalaServices != null && allHalaServices.size() > 0) {
			Iterator<HalaServiceDTO> halaServicesIT = allHalaServices.iterator();

			while (halaServicesIT.hasNext()) {
				HalaServiceDTO halaServiceDTO = halaServicesIT.next();

				String fltSegmentId = halaServiceDTO.getFlightSegmentId();
				String ssrId = halaServiceDTO.getSsrId();
				String reqAirportCode = halaServiceDTO.getStation();

				String key = fltSegmentId + "_" + ssrId + "_" + reqAirportCode;

				HalaServiceDTO halaSummaryFltSegDTO = null;

				Integer ssrServiceCount = 1;
				if (halaServicesSummaryMap != null && halaServicesSummaryMap.size() > 0) {

					halaSummaryFltSegDTO = (HalaServiceDTO) halaServicesSummaryMap.get(key);

					if ((halaSummaryFltSegDTO != null) && (halaSummaryFltSegDTO.getSsrCode() != null)
							&& (halaSummaryFltSegDTO.getSsrCode().equals(halaServiceDTO.getSsrCode()))) {
						// same ssr code already requested in the same flight segment
						ssrServiceCount = halaSummaryFltSegDTO.getNoOfPax();
						ssrServiceCount++;
					}
				}

				halaSummaryFltSegDTO = setHalaSummaryDTO(halaServiceDTO, halaSummaryFltSegDTO, ssrServiceCount);

				halaServicesSummaryMap.put(key, halaSummaryFltSegDTO);
			}

			return halaServicesSummaryMap;
		} else {
			return null;
		}

	}

	private static HalaServiceDTO setHalaSummaryDTO(HalaServiceDTO halaServiceDTO, HalaServiceDTO halaSummaryDTO,
			Integer ssrServiceCount) {

		if (halaSummaryDTO == null) {
			halaSummaryDTO = new HalaServiceDTO();
		}

		halaSummaryDTO.setDepartureDate(halaServiceDTO.getDepartureDate());
		halaSummaryDTO.setArrivalDate(halaServiceDTO.getArrivalDate());
		halaSummaryDTO.setFlightNo(halaServiceDTO.getFlightNo());
		halaSummaryDTO.setSsrCode(halaServiceDTO.getSsrCode());
		halaSummaryDTO.setSsrDescription(halaServiceDTO.getSsrDescription());
		halaSummaryDTO.setStation(halaServiceDTO.getStation());
		halaSummaryDTO.setNoOfPax(ssrServiceCount);
		return halaSummaryDTO;
	}

	private static HashMap<String, ArrayList<HalaServiceDTO>> getHalaServicesPerFlightSegment(
			ArrayList<HalaServiceDTO> allHalaServices) {
		HashMap<String, ArrayList<HalaServiceDTO>> halaServicesMap = new HashMap<String, ArrayList<HalaServiceDTO>>();

		if (allHalaServices != null && allHalaServices.size() > 0) {
			Iterator<HalaServiceDTO> halaServicesIT = allHalaServices.iterator();
			ArrayList<HalaServiceDTO> valueList = null;

			while (halaServicesIT.hasNext()) {
				HalaServiceDTO halaServiceDTO = halaServicesIT.next();
				String flightSegmentId = halaServiceDTO.getFlightSegmentId();
				String airport = halaServiceDTO.getStation();
				String key = flightSegmentId + "_" + airport;

				if (halaServicesMap != null && halaServicesMap.size() > 0) {

					if (halaServicesMap.containsKey(key)) {
						valueList = halaServicesMap.get(key);
						valueList.add(halaServiceDTO);
					} else {
						valueList = new ArrayList<HalaServiceDTO>();
						valueList.add(halaServiceDTO);
					}

				} else {
					valueList = new ArrayList<HalaServiceDTO>();
					valueList.add(halaServiceDTO);
				}
				halaServicesMap.put(key, valueList);
			}

			return halaServicesMap;
		} else {
			return null;
		}

	}

	private static void
			composeNotificationForFlightSegment(HashMap<String, ArrayList<HalaServiceDTO>> halaServicesForFlightSegMap)
					throws ModuleException {

		if (halaServicesForFlightSegMap != null && halaServicesForFlightSegMap.size() > 0) {
			String hubAirPort = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
			String emailAddresses = ReservationModuleUtils.getGlobalConfig().getSSRCategoryEMailMap()
					.get(SSRCategory.ID_AIRPORT_SERVICE);

			for (String flightSegId : halaServicesForFlightSegMap.keySet()) {

				List<HalaServiceDTO> servicesDTOList = halaServicesForFlightSegMap.get(flightSegId);
				HalaServiceDTO halaServiceDTO = servicesDTOList.get(0);

				HashMap<String, HalaServiceDTO> halaServicesSummary = getHalaServicesSummaryByFlightSegment(servicesDTOList);

				String mailSubject = "BOOKED HALA SERVICES NOTIFICATION : " + halaServiceDTO.getFlightNo();
				Topic topic = new Topic();
				HashMap map = new HashMap();
				map.put("subject", mailSubject);
				map.put("header", halaServiceDTO);
				map.put("servicesSummary", halaServicesSummary);
				map.put("hub", hubAirPort);

				List<UpdateEmailStatusInfoDTO> updateEmailStatusInfoDTOList = new ArrayList<UpdateEmailStatusInfoDTO>();
				if (servicesDTOList != null && servicesDTOList.size() > 0) {
					for (HalaServiceDTO serviceDTO : servicesDTOList) {
						UpdateEmailStatusInfoDTO updateEmailStatusInfoDTO = new UpdateEmailStatusInfoDTO();
						updateEmailStatusInfoDTO.setPpssId(serviceDTO.getPpssId());

						updateEmailStatusInfoDTOList.add(updateEmailStatusInfoDTO);
					}
				}

				map.put("detailsList", servicesDTOList);
				topic.setTopicParams(map);
				topic.setObjectInfo(updateEmailStatusInfoDTOList);
				topic.setTopicName(AirinventoryCustomConstants.HALA_NOTIFICATION_MAIL_TEMPLATE);

				sendHalaNotificationMessage(topic, emailAddresses);
				// sendHalaNotificationMessage(topic, "mrikaz@isaaviation.ae");
			}

		}
	}

}
