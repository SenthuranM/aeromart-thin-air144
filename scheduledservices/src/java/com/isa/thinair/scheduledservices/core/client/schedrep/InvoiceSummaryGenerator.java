package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class InvoiceSummaryGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		ResultSet resultSet = null;
		String rptView = reportsSearchCriteria.getReportType();
		if (ReportsSearchCriteria.AGENT_INVOICE_SUMMARY.equals(rptView)) {
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getInvoiceSummaryDataByCurrency(reportsSearchCriteria);
		} else {
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getInvoiceSummaryDataByCurrency(reportsSearchCriteria);
		}
		return resultSet;
	}

}
