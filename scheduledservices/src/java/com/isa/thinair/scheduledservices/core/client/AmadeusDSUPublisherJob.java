package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Manoj Dhanushka
 */
public class AmadeusDSUPublisherJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			AmadeusDSUPublisher.execute();
		} catch (Exception ex) {
			throw new JobExecutionException(ex);
		}
	}
}
