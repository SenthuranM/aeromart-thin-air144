package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class PALSender implements Job {
	private Log log = LogFactory.getLog(PALSender.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {

		try {
			//TODO when PNL ADL notifier integrate done move this app apram value to pal cal observer
			if(AppSysParamsUtil.isEnablePalCalMessage()){
				CredentialInvokerUtil.invokeCredentials();
				org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
	
				String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
				String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
	
				String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
	
				log.info(" #####################     PAL JOB STARTED ######################");
				log.info(" ----------------------------------------------------------------");
				log.info(" #####################     PAL FOR FLIGHT ID : " + flightId);
				log.info(" #####################     DEPATURE STATION : " + depAirport);
				log.info(" #####################     FLIGHT NUMBER : " + flightNumber);
				log.info(" ----------------------------------------------------------------");
				sendPALMessage(Integer.parseInt(flightId), depAirport);
	
				log.info("#################### PALSender Completed  #############################");
			}
		} catch (Exception me) {
			log.error("send PAL failed", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

	public void sendPALMessage(int flightId, String departureStation) throws ModuleException, Exception {
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendPALMessage(flightId, departureStation);
	}

}
