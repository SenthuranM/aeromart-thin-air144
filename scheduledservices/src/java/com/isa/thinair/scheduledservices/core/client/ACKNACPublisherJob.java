package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ACKNACPublisherJob extends QuartzJobBean {
	private static final Log log = LogFactory.getLog(ACKNACPublisherJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ACKNACPublisher.execute();
		} catch (Exception e) {
			log.error("ACKNACPublisherJob ", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
