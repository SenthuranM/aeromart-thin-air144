package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.scheduledservices.api.util.ScheduledServicesUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class ADL implements Job {

	private static Log log = LogFactory.getLog(ADL.class);

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub

	}

	private void setJobData(JobDetail jobDetail, String jobId, String jobGroup, int flightId, String departureStation,
			String flightNumber) {
		jobDetail.getJobDataMap().put(Job.PROP_JOB_ID, jobId);
		jobDetail.getJobDataMap().put(Job.PROP_JOB_GROUP, jobGroup);
		jobDetail.getJobDataMap().put(Job.PROP_FlightId, String.valueOf(flightId));
		jobDetail.getJobDataMap().put(Job.PROP_DepartureStation, departureStation);
		jobDetail.getJobDataMap().put(Job.PROP_FLIGHT_NUMBER, flightNumber);
	}

	/**
	 * check if pnl already sent / has a history
	 * 
	 * @param flightNumber
	 * @param departureStation
	 * @param departuretimeZulu
	 * @return
	 */
	private boolean pnlAlreadySent(String flightNumber, String departureStation, Timestamp departuretimeZulu) {
		try {
			return ScheduledservicesUtils.getReservationAuxilliaryBD().hasPnlSentHistory(flightNumber, departuretimeZulu,
					departureStation);
		} catch (Exception exception) {
			log.error(
					"Error when checking pnl already sent! This will not interupt operation " + exception.getLocalizedMessage(),
					exception);
		}
		return false;
	}

	private class LatestFirstSortComparator implements Comparator<PNLTransMissionDetailsDTO> {
		@Override
		public int compare(PNLTransMissionDetailsDTO first, PNLTransMissionDetailsDTO second) {
			PNLTransMissionDetailsDTO transMissionDetailsDTO = first;
			PNLTransMissionDetailsDTO compareTo = second;
			return (transMissionDetailsDTO.getDepartureTimeZulu() == null || compareTo.getDepartureTimeZulu() == null)
					? 0
					: transMissionDetailsDTO.getDepartureTimeZulu().compareTo(compareTo.getDepartureTimeZulu()) * -1;
		}
	}

	private boolean flightIsSystemAirLine(String flightNumber) {
		String[] carrierCodes = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CARRIERS_TO_SEND_PNL_ADL)
				.trim().split(",");
		for (String carrierCode : carrierCodes) {
			if (carrierCode.equals(AppSysParamsUtil.extractCarrierCode(flightNumber))) {
				return true;
			}
		}
		return false;
	}

	private void addDebugLog(String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	public void execute() throws JobExecutionException {

		try {
			addDebugLog("############################## SCHEDULING PNLS, ADLS AND FLIGHT CLOSURE JOB IS STARTED "
					+ CalendarUtil.getCurrentSystemTimeInZulu());

			CredentialInvokerUtil.invokeCredentials();
			ReservationAuxilliaryBD auxilliaryBD = ScheduledservicesUtils.getReservationAuxilliaryBD();

			Timestamp currentTime = new Timestamp((CalendarUtil.getCurrentSystemTimeInZulu()).getTime());
			addDebugLog("RETREIVING FLIGHT PNL ADL TRNSMSN TIMES" + currentTime);
			ArrayList<PNLTransMissionDetailsDTO> list = auxilliaryBD.getFlightForPnlAdlScheduling();
			addDebugLog("RETREIVING FLIGHT PNL ADL TRNSMSN TIMES SUCCESSFUL !!!" + currentTime);

			Collections.sort(list, new LatestFirstSortComparator());

			// populating configuration data.
			GlobalConfig globalConfig = ScheduledServicesUtil.getGlobalConfig();
			int flightClosureDepartureGapMins = AppSysParamsUtil.getFlightClosureGapInMins();
			int pnlDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PNL_DEPARTURE_GAP));
			int lastADLDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.LAST_ADL));
			int adlRepeatIntervalMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ADL_INTERVAL));

			Integer effectivePNLDepGapInMins = null;
			Integer effectiveADLRepeatIntervalInMins = null;
			Integer effectiveLastADLDepGapInMins = null;

			String jobId = null;
			int newTime = 5;
			String previousDepartureStation = "";
			int previousFlightId = -1;

			boolean isScheduledPnl = false;
			PNLTransMissionDetailsDTO dto = list.get(4);
			jobId = dto.getFlightNumber() + "/" + dto.getDepartureStation() + "/" + dto.getDepartureTimeZulu();
			String departureStation = dto.getDepartureStation();

			// Note: 7th June - For Merging G9 and 0Y No need to check the carrier coce.
			// PNL Should be sent to all the valid flights in the System.
			// Checking the carrier code again as blocked seats interlining is started with Maroc - 08/Jul

			int fid = dto.getFlightId();
			Timestamp zuluFlightDepartureTime = dto.getDepartureTimeZulu();

			previousDepartureStation = departureStation;
			previousFlightId = fid;

			Timestamp flightClosureTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - flightClosureDepartureGapMins
					* 60 * 1000);

			effectivePNLDepGapInMins = dto.getPnlDepartureGap() != 0 ? dto.getPnlDepartureGap() : pnlDepartureGapMins;

			Timestamp pnlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - 60 * effectivePNLDepGapInMins
					* 1000);

			// If PNL scheduled time is in the past, re-schedule if eligible
			if (pnlScheduledTimeStamp.getTime() < new Date().getTime()) {
				if (!pnlAlreadySent(dto.getFlightNumber(), dto.getDepartureStation(), dto.getDepartureTimeZulu())) {
					if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.PNL)) {
						addDebugLog("PNL WAS NOT SCHEDULED FOR " + jobId + " AND PNL SHOULD HAVE SENT AT :"
								+ pnlScheduledTimeStamp.toString());
						newTime = newTime + 2;
						// Adding 2 mins to avoid possible errors from simultaneous PNL sending
						pnlScheduledTimeStamp = new Timestamp(new Date().getTime() + 60 * newTime * 1000);
						addDebugLog("NEW PNL TIME FOR " + jobId + " IS : " + pnlScheduledTimeStamp.toString());
					} else {
						log.warn("PNL WAS ALREADY SCHEDULED AND WILL NOT SCHEDULE:" + jobId);
					}
				} else {
					log.warn("PNL WAS ALREADY SENT AND WILL NOT SCHEDULE:" + jobId);

				}
			}

			if (pnlScheduledTimeStamp.getTime() > currentTime.getTime()) {
				JobDetail pnlJobDetail = new JobDetail();
				setJobData(pnlJobDetail, jobId, SSInternalConstants.JOB_TYPE.PNL, fid, departureStation, dto.getFlightNumber());
				pnlJobDetail.setJobClass(PNLSender.class);
				Date pnlstartTime = CommonUtil.getDateof(pnlScheduledTimeStamp);
				try {
					addDebugLog("JOB: " + jobId + "SCHEDULING PNL. FLIGHT NUMBER: " + dto.getFlightNumber() + " PNL TIME : "
							+ pnlstartTime);
					ScheduleManager.scheduleJob(pnlJobDetail, pnlstartTime, null, 0, 0);
				} catch (SchedulerException exception) {
					log.error("JOB: " + jobId + " DID NOT SCHEDULE PNL. POSSIBLLY PNL TIME ALREADY SCHEDULED. ", exception);
				} catch (Exception e) {
					log.error("JOB: " + jobId + " DID NOT SCHEDULE PNL. POSSIBLLY PNL TIME ALREADY SCHEDULED. ", e);
					pnlJobDetail.setRequestsRecovery(true);
				}
				isScheduledPnl = true;
			}

			if (isScheduledPnl) {
				effectiveLastADLDepGapInMins = dto.getLastAdlGap() > 0 ? dto.getLastAdlGap() : lastADLDepartureGapMins;
				effectiveADLRepeatIntervalInMins = dto.getAdlRepeatInterval() != 0
						? dto.getAdlRepeatInterval()
						: adlRepeatIntervalMins;
				Timestamp lastADLScheduledTimestamp = new Timestamp(zuluFlightDepartureTime.getTime()
						- effectiveLastADLDepGapInMins * 60 * 1000);
				int totalADLToSend = 0;
				Timestamp adlScheduledTimeStamp = null;
				// Number of ADLs to send
				if (AppSysParamsUtil.isMaintainSeperateADLTransmissionIntervalAfterCutOffTime()) {
					int adlRepeatIntervalMinsAfterCutoffTime = Integer.parseInt(globalConfig
							.getBizParam(SystemParamKeys.ADL_INTERVAL_AFTER_CUTOFF_TIME));
					Date flightCutoffTime = CalendarUtil.addMilliSeconds(dto.getDepartureTimeZulu(), -dto.getFlightCutoffTime());
					int adlCountBeforeCutOffTime = 0, adlCountAfterCutOffTime = 0;
					Integer effectiveADLRepeatIntervalAfterCutoffTimeInMins = dto.getAdlRepeatIntervalAfterCutoffTime() != 0
							? dto.getAdlRepeatIntervalAfterCutoffTime()
							: adlRepeatIntervalMinsAfterCutoffTime;

					if (effectiveADLRepeatIntervalAfterCutoffTimeInMins != 0) {
						adlCountBeforeCutOffTime = (int) Math.ceil(((float) (flightCutoffTime.getTime() - pnlScheduledTimeStamp
								.getTime())) / (effectiveADLRepeatIntervalInMins * 60 * 1000));
						adlCountAfterCutOffTime = (int) Math
								.ceil(((float) (lastADLScheduledTimestamp.getTime() - flightCutoffTime.getTime()))
										/ (effectiveADLRepeatIntervalAfterCutoffTimeInMins * 60 * 1000));
					} else {
						adlCountBeforeCutOffTime = (int) Math
								.ceil(((float) (lastADLScheduledTimestamp.getTime() - pnlScheduledTimeStamp.getTime()))
										/ (effectiveADLRepeatIntervalInMins * 60 * 1000));

					}

					totalADLToSend = adlCountBeforeCutOffTime + adlCountAfterCutOffTime;
					for (int k = 1; k <= totalADLToSend; k++) {
						if (k < adlCountBeforeCutOffTime) {
							adlScheduledTimeStamp = new Timestamp(pnlScheduledTimeStamp.getTime() + (k)
									* effectiveADLRepeatIntervalInMins * 60 * 1000);
						} else if (k < totalADLToSend) {
							adlScheduledTimeStamp = new Timestamp(pnlScheduledTimeStamp.getTime() + (k)
									* effectiveADLRepeatIntervalAfterCutoffTimeInMins * 60 * 1000);
						} else {
							adlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime()
									- effectiveLastADLDepGapInMins * 60 * 1000);
						}

						if (adlScheduledTimeStamp.getTime() > currentTime.getTime()) {
							JobDetail adlJobDetail = new JobDetail();
							setJobData(adlJobDetail, jobId, SSInternalConstants.JOB_TYPE.ADL + "/" + k, fid, departureStation,
									dto.getFlightNumber());
							adlJobDetail.setJobClass(ADLSender.class);
							java.util.Date adlstartTime = CommonUtil.getDateof(adlScheduledTimeStamp);

							try {
								addDebugLog("JOB : " + jobId + " SCHEDULING ADL. ADL TIME : " + adlstartTime);
								ScheduleManager.scheduleJob(adlJobDetail, adlstartTime, null, 0, 0);
							} catch (SchedulerException exception) {
								log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY ADL-" + k
										+ " TIME ALREADY SCHEDULED : ", exception);
							}
						}
					}

				} else {
					totalADLToSend = (int) Math.ceil(((float) (lastADLScheduledTimestamp.getTime() - pnlScheduledTimeStamp
							.getTime())) / (effectiveADLRepeatIntervalInMins * 60 * 1000));

					for (int k = 1; k <= totalADLToSend; k++) {
						if (k < totalADLToSend) {
							adlScheduledTimeStamp = new Timestamp(pnlScheduledTimeStamp.getTime() + (k)
									* effectiveADLRepeatIntervalInMins * 60 * 1000);
						} else {
							adlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime()
									- effectiveLastADLDepGapInMins * 60 * 1000);
						}

						if (adlScheduledTimeStamp.getTime() > flightClosureTimeStamp.getTime()) {
							log.warn("JOB : " + jobId + " ADL DELIVERY IS WITHIN FLIGHT CLOSURE TIME. ADL TIME :"
									+ adlScheduledTimeStamp.toString());
						}

						if (adlScheduledTimeStamp.getTime() > currentTime.getTime()) {
							JobDetail adlJobDetail = new JobDetail();
							setJobData(adlJobDetail, jobId, SSInternalConstants.JOB_TYPE.ADL + "/" + k, fid, departureStation,
									dto.getFlightNumber());
							adlJobDetail.setJobClass(ADLSender.class);
							java.util.Date adlstartTime = CommonUtil.getDateof(adlScheduledTimeStamp);

							try {
								addDebugLog("JOB : " + jobId + " SCHEDULING ADL. ADL TIME : " + adlstartTime);
								ScheduleManager.scheduleJob(adlJobDetail, adlstartTime, null, 0, 0);
							} catch (SchedulerException exception) {
								log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY ADL-" + k
										+ " TIME ALREADY SCHEDULED : ", exception);
							}
						}
					}
				}

				if (flightClosureTimeStamp.getTime() > currentTime.getTime()) {
					JobDetail flightCloseJobDetail = new JobDetail();
					setJobData(flightCloseJobDetail, jobId, SSInternalConstants.JOB_TYPE.FLIGHT_CLOSER, fid, departureStation,
							dto.getFlightNumber());
					flightCloseJobDetail.setJobClass(FlightClosurer.class);
					java.util.Date flightclosestartTime = CommonUtil.getDateof(flightClosureTimeStamp);
					addDebugLog("JOB: " + jobId + " SCHEDULING FLIGHT CLOSURE. CLOSURE TIME : " + flightclosestartTime
							+ ", FLIGHT DEP ZULU : " + dto.getDepartureTimeZulu());
					try {
						ScheduleManager.scheduleJob(flightCloseJobDetail, flightclosestartTime, null, 0, 0);
					} catch (SchedulerException exception) {
						log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY FLIGHT CLOSURE ALREADY SCHEDULED", exception);
					}
				} else {
					log.warn("JOB: " + "NOT SCHEDULING FLIGHT CLOSURE. CLOSURE TIME ALREADY PASS. CLOSURE TIME : "
							+ flightClosureTimeStamp + ", FLIGHT DEP ZULU : " + dto.getDepartureTimeZulu());
				}
			}
			addDebugLog(" =================================================================================== ");

			addDebugLog("#######COMPLETED SCHEDULING PNLS, ADLS AND FLIGHT CLOSURES WITHOUT ANY ERRORS##################");
		} catch (Exception e) {
			log.error("ERROR OCCURED IN SCHEDULING PNL, ADL AND FLIGHT CLOSURE JOBS", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
