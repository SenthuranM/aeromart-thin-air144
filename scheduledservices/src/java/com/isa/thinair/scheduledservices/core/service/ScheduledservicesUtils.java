package com.isa.thinair.scheduledservices.core.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airmaster.core.remoting.ejb.AirportServiceBean;
import com.isa.thinair.airpricing.api.service.AnciChargeLocalCurrBD;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.lccclient.api.service.LCCMasterDataPublisherBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.service.PFSXmlBD;
import com.isa.thinair.messagepasser.api.service.PRLBD;
import com.isa.thinair.messagepasser.api.service.SSIMBD;
import com.isa.thinair.messagepasser.api.service.XApnlBD;
import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.service.AVSServiceBD;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;
import com.isa.thinair.msgbroker.api.service.SSMASMServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.service.ScheduledReportBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;
import com.isa.thinair.scheduledservices.core.client.schedrep.ReportGenerator;
import com.isa.thinair.wsclient.api.service.InsuranceClientBD;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

/**
 * @author nasly
 * 
 *         Utility for looking up service delegates offered by the modules
 */
public class ScheduledservicesUtils {
	private static final Log log = LogFactory.getLog(ScheduledservicesUtils.class);

	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("scheduledservices");
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getScheduledservicesConfig(),
				"schedulerclient", "schedulerclient.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getScheduledservicesConfig(),
				"schedulerclient", "schedulerclient.config.dependencymap.invalid");
	}

	public static ScheduledservicesConfig getScheduledservicesConfig() {
		return (ScheduledservicesConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/scheduledservices?id=scheduledservicesModuleConfig");
	}

	public static ReportGenerator getReportGenerator(String beanId) {
		return (ReportGenerator) LookupServiceFactory.getInstance().getBean("isa:base://modules/scheduledservices?id=" + beanId);
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Returns transparent method to get the ReservationBD.
	 * 
	 * @return SeatMapBD
	 */
	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	/**
	 * Returns transparent method to get the ReservationBD.
	 * 
	 * @return ReservationBD
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	/**
	 * Returns transparent method to get the SegmentBD.
	 * 
	 * @return SegmentBD
	 */
	public static SegmentBD getSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	/**
	 * Returns transparent to get the PaymentBrokersBD.
	 * 
	 * @return PaymentBrokerBD
	 */
	public static PaymentBrokerBD getPaymetBrokerBD() {
		return (PaymentBrokerBD) lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME, PaymentBrokerBD.SERVICE_NAME);
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	/**
	 * Returns transparent method to get the SegmentBD.
	 * 
	 * @return SegmentBD
	 */
	public static ScheduleBD getScheduleBD() {
		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static InvoicingBD getInvoicingBD() {
		return (InvoicingBD) lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	public static GDSServicesBD getGDServicesBD() {
		return (GDSServicesBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSServicesBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static ScheduledReportBD getScheduledReportBD() {
		return (ScheduledReportBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, ScheduledReportBD.SERVICE_NAME);
	}

	public static XApnlBD getXApnlBD() {
		return (XApnlBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, XApnlBD.SERVICE_NAME);
	}

	public static ETLBD getETLBD() {
		return (ETLBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
	}

	public static PRLBD getPRLBD() {
		return (PRLBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PRLBD.SERVICE_NAME);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}

	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	public static FareBD getFareBD() {
		return (FareBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public static ChargeBD getchargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}
	
	public static AnciChargeLocalCurrBD getAnciChargeLocalCurrBD(){
		return (AnciChargeLocalCurrBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, AnciChargeLocalCurrBD.SERVICE_NAME);
	}

	public static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, DataExtractionBD.SERVICE_NAME);
	}

	public static ReportingFrameworkBD getReportingFrameworkBD() {
		return (ReportingFrameworkBD) lookupServiceBD(ReportingframeworkConstants.MODULE_NAME,
				ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static LCCMasterDataPublisherBD getLCCMasterDataPublisherBD() {
		return (LCCMasterDataPublisherBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCMasterDataPublisherBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportServiceBean.SERVICE_NAME);
	}

	public static SSMASMServiceBD getSSMASMServiceBD() {
		return (SSMASMServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, SSMASMServiceBD.SERVICE_NAME);
	}

	public static AVSServiceBD getAVSServiceBD() {
		return (AVSServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, AVSServiceBD.SERVICE_NAME);
	}

	public static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		return (MessageBrokerServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, MessageBrokerServiceBD.SERVICE_NAME);
	}

	public static PFSXmlBD getPfsXmlBD() {
		return (PFSXmlBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PFSXmlBD.SERVICE_NAME);
	}

	public static PromotionAdministrationBD getPromotionAdministrationBD() {
		return (PromotionAdministrationBD) lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionAdministrationBD.SERVICE_NAME);
	}

	public static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, PromotionManagementBD.SERVICE_NAME);
	}
	
	public static SSIMBD getSSIMBD() {
		return (SSIMBD) lookupEJB3Service(MessagepasserConstants.MODULE_NAME, SSIMBD.SERVICE_NAME);
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, LoyaltyManagementBD.SERVICE_NAME);
	}
	
	public final static VoucherBD getVoucherBD() {
		return (VoucherBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, VoucherBD.SERVICE_NAME);
	}
	
	public static InsuranceClientBD getInsuranceClientBD(){
		return (InsuranceClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, InsuranceClientBD.SERVICE_NAME);
	}

}
