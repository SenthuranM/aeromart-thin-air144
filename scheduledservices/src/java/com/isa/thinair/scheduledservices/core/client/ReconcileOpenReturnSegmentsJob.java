package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public class ReconcileOpenReturnSegmentsJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ReconcileOpenReturnSegments.execute(null);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
}
