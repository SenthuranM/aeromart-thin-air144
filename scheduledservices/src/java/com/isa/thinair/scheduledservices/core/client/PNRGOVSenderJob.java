package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class PNRGOVSenderJob implements Job {
	private static Log log = LogFactory.getLog(PNRGOVSenderJob.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();

			String fltSegID = jobDetail.getJobDataMap().getString(Job.PROP_FlightSegId);
			String countryCode = jobDetail.getJobDataMap().getString(Job.PROP_COUNTRY_CODE);
			String airportCode = jobDetail.getJobDataMap().getString(Job.PROP_AIRPORT_CODE);
			String timeGap = jobDetail.getJobDataMap().getString(Job.PROP_TIME_GAP);
			String inboundOutbound = jobDetail.getJobDataMap().getString(Job.PROP_INBOUND_OUTBOUND);

			log.info("PNRGOV SENDER JOB STARTED FOR : " + fltSegID);
			ScheduledservicesUtils.getReservationBD().publishPNRGOV(Integer.parseInt(fltSegID), countryCode, airportCode,
					Integer.parseInt(timeGap), inboundOutbound);
			log.info("PNRGOV SENDER JOB COMPLETED FOR : " + fltSegID);
		} catch (ModuleException e) {
			log.error(e.getMessage());
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
