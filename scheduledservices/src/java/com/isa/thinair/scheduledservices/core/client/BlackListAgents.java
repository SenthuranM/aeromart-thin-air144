/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Duminda G
 * @since 2.0
 */
public class BlackListAgents {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(BlackListAgents.class);

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute(Date executionDate) throws Exception {

		try {
			log.info("########## BlackListAgentsJob Invoked #######");
			CredentialInvokerUtil.invokeCredentials();

			log.info("##############BlackListAgentsJob Started for " + new Date());
			ScheduledservicesUtils.getTravelAgentBD().blockNonPayingAgents();
			log.info("##############BlackListAgentsJob Ended for " + new Date());
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
