package com.isa.thinair.scheduledservices.core.client.cf;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Scheduler of flight load publishing to cargo flash.
 * 
 * @author vidula
 *
 */
public class FlightLoadPublisherJob extends QuartzJobBean {
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			sendLoadInfo();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
	
	private void sendLoadInfo() throws Exception {
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendFlightLoadInfo();
	}
}
