package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * ReservationReconcillator
 * 
 * @author isuru
 * @since 1.0
 */
public class ReservationReconcillator {

	private static Log log = LogFactory.getLog(ReservationReconcillator.class);

	public static void reconcileReservations() throws Exception {
		try {
			log.info("Before Reconciling the reservations");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationBD().processAutomaticReconcilation();
			log.info("After Reconciling the reservations");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
