package com.isa.thinair.scheduledservices.core.client.lcc;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Scheduled Job of RouteInfo Publishing to LCC
 * 
 * @author Navod Ediriweera
 * @since 2009-10-20
 */
public class LCCRouteInfoPublisherJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			LCCRouteInfoPublisher.execute();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
