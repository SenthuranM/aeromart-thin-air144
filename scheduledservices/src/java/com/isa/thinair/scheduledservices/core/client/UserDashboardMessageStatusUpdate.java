package com.isa.thinair.scheduledservices.core.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgent;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author rumesh
 * 
 *         DB Operations are broken into chunks of 1000 to avoid transaction timeout when running long operations. This
 *         is not something that is likely to happen at production, it popped up during testing due to all the agents
 *         being selected when creating a message and this process was followed to create about 15 messages resulting a
 *         excessive amount of data to crunch. @author Thihara
 */
public class UserDashboardMessageStatusUpdate {
	private Log log = LogFactory.getLog(UserDashboardMessageStatusUpdate.class);

	private static int RECORDS_PER_OPERATION = 1000;

	private enum MESSAGE_TYPE {
		USER_ID, AGENT_TYPE, AGENT_CODE, POS
	};

	private Set<String> includeUserIds = new HashSet<String>();
	private Integer currentUserIDRecordSet = 0;
	private Boolean userIDRecordSetFinished = Boolean.FALSE;

	private Set<String> includeAgentCodes = new HashSet<String>();
	private Integer currentAgentCodeRecordSet = 0;
	private Boolean agentCodeRecordSetFinished = Boolean.FALSE;

	private Set<String> includeAgentTypeCodes = new HashSet<String>();
	private Integer currentAgentTypeRecordSet = 0;
	private Boolean agentTypeRecordSetFinished = Boolean.FALSE;

	private Set<String> includePosCodes = new HashSet<String>();
	private Integer currentPosCodeRecordSet = 0;
	private Boolean posRecordSetFinished = Boolean.FALSE;

	private Integer currentDashBoardMessageRecordSet = 0;
	private Boolean dashBoardRecordSetFinished = Boolean.FALSE;

	public void execute() throws ModuleException {

		CredentialInvokerUtil.invokeCredentials();

		if (AppSysParamsUtil.isDBMessageSynchronise()) {
			log.info("Dashboard Message Synchronize Enabled");
			updateUserMessageStatus();
		} else {
			log.info("Dashboard Message Synchronize Disabled");
		}
	}

	private void updateUserMessageStatus() throws ModuleException {
		log.info("########################################################################");
		log.info("######### Initiated Updating New Message Exist Status for Users  #######");

		List<DashboardMessage> newDashboardMsgs = AirSchedulesUtil.getCommonMasterBD().getNewDashbrdMsgIDs();

		if (newDashboardMsgs != null && newDashboardMsgs.size() > 0) {
			// This will populate including users, agents, agent types & agent pos
			getIncludingUserList(newDashboardMsgs);

			if (this.includeUserIds.size() > 0 || this.includeAgentCodes.size() > 0 || this.includeAgentTypeCodes.size() > 0
					|| this.includePosCodes.size() > 0) {

				/*
				 * Records are broken down into smaller sub sets to avoid transaction timeout.
				 */
				int iterationCount = 1;
				do {
					log.debug("######### Begining New Message Exists Iteration : " + iterationCount + " #######");
					AirSchedulesUtil.getSecurityBD().setUserNewMessageStatus(getIncludeUserIDs(), getIncludeAgentCodes(),
							getIncludeAgentTypeCodes(), getIncludePOSCodes());
					log.debug("######### End New Message Exists Iteration  #######");
					iterationCount++;
				} while (!userIDRecordSetFinished || !agentCodeRecordSetFinished || !agentTypeRecordSetFinished
						|| !posRecordSetFinished);
			}

			do {
				AirSchedulesUtil.getCommonMasterBD().updateDashboardNewMessageStatus(getMessageIdList(newDashboardMsgs),
						DashboardMessage.NO_NEW_MESSAGE);
			} while (!dashBoardRecordSetFinished);
		}

		log.info("######### Completed Updating New Message Exist Status for Users  #######");
		log.info("########################################################################");
	}

	private Set<String> getIncludeUserIDs() {
		return getNextSubSet(this.includeUserIds, currentUserIDRecordSet, MESSAGE_TYPE.USER_ID);
	}

	private Set<String> getIncludeAgentCodes() {
		return getNextSubSet(this.includeAgentCodes, currentAgentCodeRecordSet, MESSAGE_TYPE.AGENT_CODE);
	}

	private Set<String> getIncludeAgentTypeCodes() {
		return getNextSubSet(this.includeAgentTypeCodes, currentAgentTypeRecordSet, MESSAGE_TYPE.AGENT_TYPE);
	}

	private Set<String> getIncludePOSCodes() {
		return getNextSubSet(this.includePosCodes, currentPosCodeRecordSet, MESSAGE_TYPE.POS);

	}

	private Set<String> getNextSubSet(Set<String> originalSet, Integer currentRecordSet, MESSAGE_TYPE messageType) {

		log.debug("######### " + messageType + " | Current Rec Set : " + currentRecordSet + " Rec Finished : "
				+ hasProcessingCompleted(messageType) + " #######");

		if (hasProcessingCompleted(messageType)) {
			return Collections.emptySet();
		}

		if (checkInitialRecordSizeExceeded(currentRecordSet, originalSet, messageType)) {
			return originalSet;
		}

		List<String> tmpList = new ArrayList<String>(originalSet);
		int currentRecordIndex = currentRecordSet * RECORDS_PER_OPERATION;
		int nextRecordSetEndingIndex = (currentRecordSet + 1) * RECORDS_PER_OPERATION;

		if (nextRecordSetEndingIndex > tmpList.size()) {
			nextRecordSetEndingIndex = tmpList.size();
			updateProcessingStatus(Boolean.TRUE, messageType);
		}

		incrementCurrentRecordPointer(messageType);

		Set<String> subSet = new HashSet<String>(tmpList.subList(currentRecordIndex, nextRecordSetEndingIndex));
		log.debug("#########" + messageType + " | Sub set size " + subSet.size() + " #######");
		return subSet;
	}

	private boolean checkInitialRecordSizeExceeded(Integer currentRecordSet, Set<String> originalSet, MESSAGE_TYPE messageType) {
		if (originalSet.size() <= RECORDS_PER_OPERATION) {

			updateProcessingStatus(Boolean.TRUE, messageType);
			return true;
		} else {
			return false;
		}
	}

	private boolean hasProcessingCompleted(MESSAGE_TYPE messageType) {
		switch (messageType) {
		case AGENT_CODE:
			return agentCodeRecordSetFinished;
		case AGENT_TYPE:
			return agentTypeRecordSetFinished;
		case POS:
			return posRecordSetFinished;
		case USER_ID:
			return userIDRecordSetFinished;
		default:
			throw new RuntimeException("Invalid Message Type");
		}
	}

	private void updateProcessingStatus(Boolean status, MESSAGE_TYPE messageType) {
		switch (messageType) {
		case AGENT_CODE:
			this.agentCodeRecordSetFinished = status;
			break;
		case AGENT_TYPE:
			this.agentTypeRecordSetFinished = status;
			break;
		case POS:
			this.posRecordSetFinished = status;
			break;
		case USER_ID:
			this.userIDRecordSetFinished = status;
			break;
		}
	}

	private void incrementCurrentRecordPointer(MESSAGE_TYPE messageType) {
		switch (messageType) {
		case AGENT_CODE:
			this.currentAgentCodeRecordSet++;
			break;
		case AGENT_TYPE:
			this.currentAgentTypeRecordSet++;
			break;
		case POS:
			this.currentPosCodeRecordSet++;
			break;
		case USER_ID:
			this.currentUserIDRecordSet++;
			break;
		}
	}

	private List<Integer> getMessageIdList(List<DashboardMessage> newDashboardMsgs) {
		List<Integer> msgIds = new ArrayList<Integer>();

		log.debug("######### Dashboard Message List | Current Rec Set : " + this.currentDashBoardMessageRecordSet
				+ " Rec Finished : " + this.dashBoardRecordSetFinished + " #######");

		int startingIndex = this.currentDashBoardMessageRecordSet * RECORDS_PER_OPERATION;
		int endingIndex = (this.currentDashBoardMessageRecordSet + 1) * RECORDS_PER_OPERATION;

		if (endingIndex >= newDashboardMsgs.size()) {
			endingIndex = newDashboardMsgs.size();
			this.dashBoardRecordSetFinished = Boolean.TRUE;
		}

		for (; startingIndex < endingIndex; startingIndex++) {
			msgIds.add(newDashboardMsgs.get(startingIndex).getMessageID());
		}

		this.currentDashBoardMessageRecordSet++;

		log.debug("######### Dashboard Message | Sub set size " + msgIds.size() + " #######");

		return msgIds;
	}

	private void getIncludingUserList(List<DashboardMessage> newDashboardMsgs) {
		for (DashboardMessage dashboardMessage : newDashboardMsgs) {
			Set<String> includedUsers = getIncludedUsers(dashboardMessage.getUser());
			Set<String> includedAgents = getIncludedAgents(dashboardMessage.getAgents());
			Set<String> includedAgentTypes = getIncludedAgentTypes(dashboardMessage.getAgentType());
			Set<String> includedPos = getIncludedPos(dashboardMessage.getPos());

			if (includedUsers != null && includedUsers.size() > 0) {
				this.includeUserIds.addAll(includedUsers);
			}

			if (includedAgents != null && includedAgents.size() > 0) {
				this.includeAgentCodes.addAll(includedAgents);
			}

			if (includedAgentTypes != null && includedAgentTypes.size() > 0) {
				this.includeAgentTypeCodes.addAll(includedAgentTypes);
			}

			if (includedPos != null && includedPos.size() > 0) {
				this.includePosCodes.addAll(includedPos);
			}
		}
	}

	/*
	 * returns User IDs included for Dashboard Messages
	 */
	private Set<String> getIncludedUsers(Set<DashbrdMsgUser> allUsers) {
		Set<String> includedUsers = null;

		if (allUsers != null && allUsers.size() > 0) {
			includedUsers = new HashSet<String>();
			for (DashbrdMsgUser dashbrdMsgUser : allUsers) {
				if (DashboardMessage.APPLY_STATUS_Y.equals(dashbrdMsgUser.getApplyStatus())) {
					includedUsers.add(dashbrdMsgUser.getUserID());
				}
			}
		}

		return includedUsers;
	}

	/*
	 * returns Agent Codes included for Dashboard Messages
	 */
	private Set<String> getIncludedAgents(Set<DashbrdMsgAgent> allAgents) {
		Set<String> includedAgents = null;

		if (allAgents != null && allAgents.size() > 0) {
			includedAgents = new HashSet<String>();
			for (DashbrdMsgAgent dashbrdMsgAgent : allAgents) {
				if (DashboardMessage.APPLY_STATUS_Y.equals(dashbrdMsgAgent.getApplyStatus())) {
					includedAgents.add(dashbrdMsgAgent.getAgentCode());
				}
			}
		}

		return includedAgents;
	}

	/*
	 * returns Agent Types included for Dashboard Messages
	 */
	private Set<String> getIncludedAgentTypes(Set<DashbrdMsgAgentType> allAgentType) {
		Set<String> includedAgentTypes = null;

		if (allAgentType != null && allAgentType.size() > 0) {
			includedAgentTypes = new HashSet<String>();
			for (DashbrdMsgAgentType dashbrdMsgAgentType : allAgentType) {
				if (DashboardMessage.APPLY_STATUS_Y.equals(dashbrdMsgAgentType.getApplyStatus())) {
					includedAgentTypes.add(dashbrdMsgAgentType.getAgentTypeCode());
				}
			}
		}

		return includedAgentTypes;
	}

	/*
	 * returns Agent POS included for Dashboard Messages
	 */
	private Set<String> getIncludedPos(Set<DashbrdMsgAgentPOS> allPos) {
		Set<String> includedPos = null;

		if (allPos != null && allPos.size() > 0) {
			includedPos = new HashSet<String>();
			for (DashbrdMsgAgentPOS dashbrdMsgAgentPOS : allPos) {
				if (DashboardMessage.APPLY_STATUS_Y.equals(dashbrdMsgAgentPOS.getApplyStatus())) {
					includedPos.add(dashbrdMsgAgentPOS.getPosCode());
				}
			}
		}

		return includedPos;
	}
}
