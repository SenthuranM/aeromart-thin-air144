/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 */

package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * This class initiated the transfer of bsp sales summary to external finance system (X3). This class is triggered using
 * cron expression in scheduler-context-mod.xml
 * 
 * @author
 *
 */
public class TransferBSPsalesJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(TransferBSPsalesJob.class);

	/**
	 * Method executes when scheduler job triggers.
	 * 
	 * @param arg0
	 * @throws JobExecutionException
	 */

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		synchronized (this) {
			try {
				BSPSales.transferBSPSales();
			} catch (Exception exp) {
				if (log.isErrorEnabled()) {
					log.error("BSP Details transfer job failed", exp);
				}
				throw new JobExecutionException(exp);
			}
		}

	}
}
