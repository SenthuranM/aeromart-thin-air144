/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.scheduledservices.api.util.ScheduledServicesUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;

/**
 * @author Isuru/Byorn
 * @modified 28/07/08
 * 
 */
public class FlightClosurer implements Job {

	private static Log log = LogFactory.getLog(FlightClosurer.class);

	private GlobalConfig globalConfig = ScheduledServicesUtil.getGlobalConfig();

	public void executeAction(String flightId, String depAirport, String flightNumber, org.quartz.JobDetail jobDetail) {
		try {
			CredentialInvokerUtil.invokeCredentials();
			Timestamp estimatedDepartureTimeZulu = getFlightDepartureTime(Integer.parseInt(flightId), depAirport);

			if (estimatedDepartureTimeZulu != null) {
				int closureTimeDiff = compareAgainstFlightClosure(estimatedDepartureTimeZulu, getTriggerTimeUptoLastMinutes());

				// Triggered at estimated closureTime
				if (closureTimeDiff == 0) {
					if (log.isInfoEnabled()) {
						log.info(" ##################### Closure Time Is Accurate #####################");
						log.info(" #####################     [FLIGHT CLOSING]     #####################");
						log.info(" ----------------------------------------------------------------");
						log.info(" #####################     PNL FOR FLIGHT ID : " + flightId);
						log.info(" #####################     DEPATURE STATION : " + depAirport);
						log.info(" #####################     FLIGHT NUMBER : " + flightNumber);
						log.info(" ----------------------------------------------------------------");
					}
					closeFlightForReservation(Integer.parseInt(flightId), depAirport); // close flight
					// Triggered after estimated closureTime [This is a rare scenario]
				} else if (closureTimeDiff > 0) {
					if (log.isInfoEnabled()) {
						log.info(" ##################### Scheduler Trying To Close Flight LATE #####################");
						log.info(" This feature is currently not supported Flight will be CLOSED NOW");
					}
					closeFlightForReservation(Integer.parseInt(flightId), depAirport); // close flight
					// Triggered before estimated closureTime
				} else if (closureTimeDiff < 0) {
					Date updatedClosureTime = new Date(getFlightClosuringTime(estimatedDepartureTimeZulu).getTime());
					if (log.isInfoEnabled()) {
						log.info(" ##################### Scheduler Trying To Close Flight EARLY #####################");
						log.info(" ##################### Updated closure time is " + updatedClosureTime.toString());
					}
					if (jobDetail != null) {
						// create new schedule
						String strJobID = "";
						String jobGroup = "";

						try {
							log.info("##############################################################");
							log.info("##################### [CREATING NEW JOB] #####################");
							log.info("##############################################################");
							JobDetailImpl flightCloseJobDetail = new JobDetailImpl();
							Job jobClose = new FlightClosurer();
							flightCloseJobDetail.getJobDataMap().put(jobClose.PROP_JOB_ID,
									getUpdatedJobID(flightNumber, depAirport, updatedClosureTime.toString()));

							flightCloseJobDetail.getJobDataMap().put(jobClose.PROP_JOB_GROUP,
									SSInternalConstants.JOB_TYPE.FLIGHT_CLOSER);

							flightCloseJobDetail.getJobDataMap().put(jobClose.PROP_FlightId, String.valueOf(flightId));

							flightCloseJobDetail.getJobDataMap().put(jobClose.PROP_DepartureStation, depAirport);

							flightCloseJobDetail.getJobDataMap().put(jobClose.PROP_FLIGHT_NUMBER, flightNumber);

							flightCloseJobDetail.setJobClass(FlightClosurer.class);

							strJobID = flightCloseJobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
							jobGroup = flightCloseJobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
							flightCloseJobDetail.setName(strJobID);
							flightCloseJobDetail.setGroup(jobGroup);
							flightCloseJobDetail.setDurability(true);

							Trigger trigger = new SimpleTriggerImpl(strJobID, flightCloseJobDetail.getJobDataMap().getString(
									Job.PROP_JOB_GROUP)
									+ "/" + updatedClosureTime.toString(), updatedClosureTime, null, 0, 0);
							SchedulerUtils.getScheduler().scheduleJob(flightCloseJobDetail, trigger);
							if (log.isInfoEnabled()) {
								log.info("#########################################################################################");
								log.info("Scheduling new Job for JobID  " + strJobID + " Scheduled For :"
										+ updatedClosureTime.toString());
								log.info("#########################################################################################");
							}
						} catch (org.quartz.ObjectAlreadyExistsException e) {
							log.error(" JOB ID " + strJobID + " " + jobGroup + " Already Exists ", e);
							throw e;
						} catch (SchedulerException e2) {
							log.error("JOB: " + strJobID + " DID NOT SCHEDULE ! " + e2);
							throw e2;
						} catch (Exception e1) {
							log.error(" JOB ID " + strJobID + " Already Exists A ", e1);
							throw e1;
						}
					}
				}
			} else {
				if (log.isInfoEnabled()) {
					String msg = " Oops I can't find the flight for flight id " + flightId + " and departure airport "
							+ depAirport;
					log.info("##################### " + msg + " #####################");
				}
			}
		} catch (Exception me) {
			log.error("#####################################################################################");
			log.error("Error Occured in Closing the flight... " + flightNumber, me);
			log.error("##################### FLIGHT WILL CLOSE NOW AT OLD CLOSURE TIME #####################");
			log.error("##################### Flight Number " + flightNumber);
			closeFlightForReservation(Integer.parseInt(flightId), depAirport); // close flight
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {

		org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
		String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
		String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
		String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);

		this.executeAction(flightId, depAirport, flightNumber, jobDetail);
	}

	/**
	 * Test Method for Flight Closure
	 * 
	 * @throws JobExecutionException
	 */
	public void executeForTesting() throws JobExecutionException {
		String flightId = "3069";
		String depAirport = "CMB";
		String flightNumber = "M81000";

		this.executeAction(flightId, depAirport, flightNumber, null);
	}

	/**
	 * Get current Sysdate
	 * 
	 * @return
	 */
	private Date getTriggerTimeUptoLastMinutes() {
		String strTimeZone = "GMT";
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Update Job ID with new Departure Time
	 * 
	 * @param flightNumber
	 * @param depStation
	 * @param depTimeZulu
	 * @return
	 */
	private String getUpdatedJobID(String flightNumber, String depStation, String depTimeZulu) {
		return flightNumber + "/" + depStation + "/" + depTimeZulu;
	}

	/**
	 * Calculate Flight Closure Time
	 * 
	 * @param ZuluDepTime
	 * @return
	 */
	private Timestamp getFlightClosuringTime(Timestamp ZuluDepTime) {
		long flightclosurerdeparturegap = Long.parseLong(globalConfig.getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		return new Timestamp(ZuluDepTime.getTime() - flightclosurerdeparturegap * 60 * 1000);
	}

	/**
	 * Calculate ClosureTime difference
	 * 
	 * @param ZuluDepTime
	 * @param TriggerTime
	 * @return
	 */
	private int compareAgainstFlightClosure(Timestamp zuluDepTime, Date triggerTime) {
		triggerTime.setSeconds(0);
		int timeDiff = 0;
		Date closureTime = new Date(getFlightClosuringTime(zuluDepTime).getTime());
		timeDiff = triggerTime.compareTo(closureTime);
		// if(log.isDebugEnabled()){
		// log.debug(" ################## VALIDATING CLOSURE TIME ##################");
		// log.debug(" ###### Estimated Closure Time is : " + closureTime.toString());
		// log.debug(" ###### Triggered Time is         : " + triggerTime.toString());
		// }
		if (log.isDebugEnabled()) {
			log.info(" ################## VALIDATING CLOSURE TIME ######################################################");
			log.info(" ###### Estimated Closure Time is : " + closureTime.toString() + " "
					+ String.valueOf(closureTime.getTime()));
			log.info(" ###### Triggered Time is         : " + triggerTime.toString() + " "
					+ String.valueOf(triggerTime.getTime()));
			log.info(" ###### Time Diff        			: " + String.valueOf(timeDiff));
			log.info(" #################################################################################################");
		}
		return timeDiff;
	}

	/**
	 * Get Flight Departure Timestamp Zulu
	 * 
	 * @param flightId
	 * @param departureStation
	 * @return
	 */
	private Timestamp getFlightDepartureTime(int flightId, String departureStation) {
		Timestamp ts = null;
		try {
			ts = getFlightBD().getFlightForDepartureStation(flightId, departureStation);
		} catch (ModuleException me) {
			log.error(me);
		}
		return ts;
	}

	/**
	 * Flight is closed
	 * 
	 * @param flightId
	 * @param departureStation
	 * @return
	 */
	private boolean closeFlightForReservation(int flightId, String departureStation) {
		boolean isClosed = false;
		FlightBD fBD = getFlightBD();
		try {
			isClosed = fBD.closeFlightForReservation(flightId, departureStation);
			if (isClosed) {
				publishFlightClosureForGDS(flightId);
			}
		} catch (ModuleException me) {
			isClosed = false;
		}
		return isClosed;
	}

	private void publishFlightClosureForGDS(int flightId) {
		NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
		NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
		newFlightEnvent.setFlightEventCode(FlightEventCode.FLIGHT_CLOSED);
		newFlightEnvent.addFlightId(flightId);
		
		try {
		Flight	flight =	getFlightBD().getFlight(flightId) ;
		Set<Integer> gDSIds = flight.getGdsIds();		
		newFlightEnvent.setGdsIdsRemoved(gDSIds);
		notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);		
		ScheduledservicesUtils.getFlightInventoryBD().notifyFlightEvent(notifyFlightEventsRQ) ;
		
		} catch (Exception e) {
			log.error("Pubilishing flight closure to GDS fails") ;			
		}				
	}

	/**
	 * Private method to get the ReservationAuxilliary BD.
	 * 
	 * @return ReservationAuxilliaryBD
	 */
	private FlightBD getFlightBD() {
		return ScheduledservicesUtils.getFlightBD();
	}
	
}
