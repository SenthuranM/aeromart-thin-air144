package com.isa.thinair.scheduledservices.core.client.lcc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class LCCUserInfoPublisher {

	private static final Log log = LogFactory.getLog(LCCUserInfoPublisher.class);

	public static void execute() throws ModuleException {

		if (AppSysParamsUtil.isLCCConnectivityEnabled() && AppSysParamsUtil.isLCCDataSyncEnabled()) {
			log.info("#######################################################");
			log.info("########## Starting AA-LCC User Info sync job #######");

			ScheduledservicesUtils.getCommonMasterBD().publishUserInfoUpdates();

			log.info("########## Completed AA-LCC User Info sync job ######");
			log.info("#######################################################");
		}

	}

}
