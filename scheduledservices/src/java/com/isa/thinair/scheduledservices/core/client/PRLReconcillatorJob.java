package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class PRLReconcillatorJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			PRLReservationReconcillator.reconcilePRLReservations();
		} catch (Exception re) {
			throw new JobExecutionException(re);
		}
	}
}
