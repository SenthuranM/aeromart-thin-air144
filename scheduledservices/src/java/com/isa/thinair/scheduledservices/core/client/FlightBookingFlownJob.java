package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class FlightBookingFlownJob implements Job {
	private static Log log = LogFactory.getLog(FlightBookingFlownJob.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {

		org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
		String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
		String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
		String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);

		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationBD().updatePassengerCoupons(new Integer(flightId), depAirport);
		} catch (Exception me) {
			log.error("#####################################################################################");
			log.error("ERROR OCCURED in Mark Booking flown in the flight = " + flightNumber, me);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
