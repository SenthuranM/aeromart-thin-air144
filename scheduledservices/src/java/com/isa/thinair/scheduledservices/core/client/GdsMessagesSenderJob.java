package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class GdsMessagesSenderJob
		extends QuartzJobBean
{
	
	private static Log log = LogFactory.getLog(GdsMessagesSenderJob.class);

//	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		execute();
	}

	public void execute() {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getMessageBrokerServiceBD().sendMessages();
			log.error("---- Sending GDS Msgs" + new Date());
		} catch (Exception e) {
			log.error("Error while Sending GDS Msgs" + e);
		}
	}

}
