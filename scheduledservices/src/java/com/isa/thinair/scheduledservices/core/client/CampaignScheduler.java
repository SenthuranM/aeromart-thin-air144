package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

/**
 * @author chanaka
 *
 */
public class CampaignScheduler implements Job{
	
	private Log log = LogFactory.getLog(CampaignScheduler.class);
	
	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();

			String campaignID = jobDetail.getJobDataMap().getString(Job.PROP_CAMP_ID);
			
			log.info(" #####################     CampaignScheduler JOB STARTED ######################");
			log.info(" ----------------------------------------------------------------");
			log.info(" #####################     CampaignScheduler FOR FLIGHT ID : " + campaignID);
			log.info(" ----------------------------------------------------------------");
			startCampaign(Integer.parseInt(campaignID));

			log.info("#################### CampaignScheduler Completed  #############################");
		} catch (Exception me) {
			log.error("CampaignScheduler failed", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	public void startCampaign(int campaignID) throws ModuleException, Exception {
		ScheduledservicesUtils.getPromotionManagementBD().sendEmailsForScheduledCampaigns(campaignID);
	}

}
