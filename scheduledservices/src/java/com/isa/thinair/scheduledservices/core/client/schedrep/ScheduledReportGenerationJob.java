/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client.schedrep;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Job to generate scheduled reports.
 * 
 * @author Lasantha Pambagoda
 */
public class ScheduledReportGenerationJob implements Job {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ScheduledReportGenerationJob.class);

	/**
	 * Execute method to be called by the scheduler
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			if (AppSysParamsUtil.isScheduledReportsAllowed()) {
				String jobName = context.getJobDetail().getKey().getName();
				if (log.isInfoEnabled()) {
					log.info("Executing the ScheduledReportGenerationJob" + jobName);
				}

				// get the job name and find the scheduled report id
				// (job name is having the format of
				// "scheduledReport:scheduledReportId")
				int seperetorIndex = jobName.indexOf(":");

				if (seperetorIndex != -1) {

					Integer scheduledReportId = new Integer(jobName.substring(seperetorIndex + 1));

					// Execute Report Generation logic
					ScheduledReportGenerationBL reportGenerator = new ScheduledReportGenerationBL();
					reportGenerator.generateReport(scheduledReportId);

				}
			}
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
