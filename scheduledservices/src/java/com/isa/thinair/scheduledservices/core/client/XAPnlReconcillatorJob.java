package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Byorn
 */
public class XAPnlReconcillatorJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			XAPnlReservationReconcillator.reconcileReservations();
		} catch (Exception re) {
			throw new JobExecutionException(re);
		}
	}
}
