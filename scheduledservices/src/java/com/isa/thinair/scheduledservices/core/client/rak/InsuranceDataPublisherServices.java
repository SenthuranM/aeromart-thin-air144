package com.isa.thinair.scheduledservices.core.client.rak;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * @author Nilindra Fernando
 */
public class InsuranceDataPublisherServices {

	private Log log = LogFactory.getLog(InsuranceDataPublisherServices.class);

	public void execute() {

		Collection<InsurancePublisherDetailDTO> flightInfo = ReservationModuleUtils.getFlightInventoryBD().getInsuredFlightData();

		if (flightInfo != null && flightInfo.size() > 0) {
			for (InsurancePublisherDetailDTO insurancePublisherDetailDTO : flightInfo) {
				try {
					insurancePublisherDetailDTO.setPublishType(InsurancePublisherDetailDTO.PUBLISH_FIRST);
					ScheduledservicesUtils.getReservationBD().publishRakInuranceData(insurancePublisherDetailDTO);
				} catch (Exception e) {
					log.error("Error occur in InsuranceDataPublisherServices ", e);
				}
			}

		}

	}

}
