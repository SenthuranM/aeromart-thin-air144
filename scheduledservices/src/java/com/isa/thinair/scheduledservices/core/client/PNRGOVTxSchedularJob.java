package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class PNRGOVTxSchedularJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(PNRGOVTxSchedularJob.class);

	private int pnrGovTrasmissionGap;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		addDebugLog("############################## SCHEDULING PNRGOV JOB IS STARTED "
				+ CalendarUtil.getCurrentSystemTimeInZulu());
		try {
			CredentialInvokerUtil.invokeCredentials();

			addDebugLog("######## GETTING PNRGOV ENABLED COUNTRIES");
			Collection<Country> pnrGovEnabledCountries = ScheduledservicesUtils.getCommonMasterBD().getPnrGovEnabledCountries();
			Collection<Country> pnrGovInboundEnabledCountries = filterPnrGovInboundEnabledValidCountries(pnrGovEnabledCountries);
			Collection<Country> pnrGovOutboundEnabledCountries = filterPnrGovOutboundEnabledValidCountries(pnrGovEnabledCountries);
			addDebugLog("######## COMPLETED GETTING PNRGOV ENABLED COUNTRIES.");

			log.debug("###### START SCHEDULING PNRGOV MESSAGES ######");
			Map<Country, List<String>> countryAirportMapForInbound = getCountryAirportMapForCountries(pnrGovInboundEnabledCountries);
			Map<Country, List<String>> countryAirportMapForOutbound = getCountryAirportMapForCountries(pnrGovOutboundEnabledCountries);
			schedulePnrGovMessages(countryAirportMapForInbound, false);
			schedulePnrGovMessages(countryAirportMapForOutbound, true);
			log.debug("######## SCHEDULING PNRGOV MESSAGES COMPLETED ###########");

		} catch (Exception e) {
			log.debug("ERROR IN SCHEDULING PNRGOV" + e.getMessage());
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

	private Collection<Country> filterPnrGovInboundEnabledValidCountries(Collection<Country> pnrGovEnabledCountries) {
		Collection<Country> inboundEnabledCountries = new ArrayList<Country>();
		for (Country country : pnrGovEnabledCountries) {
			if (country.getPnrGovInboundEnabled().equals("Y") && country.getPnrGovTiming() != null
					&& !country.getPnrGovTiming().equals("")) {
				inboundEnabledCountries.add(country);
			} else {
				log.error("Invalid Inbound PNRGOV Enabled Country. PNRGOV Timing missing" + country.getCountryCode());
			}
		}

		return inboundEnabledCountries;
	}

	private Collection<Country> filterPnrGovOutboundEnabledValidCountries(Collection<Country> pnrGovEnabledCountries) {
		Collection<Country> outboundEnabledCountries = new ArrayList<Country>();
		for (Country country : pnrGovEnabledCountries) {
			if (country.getPnrGovOutboundEnabled().equals("Y") && country.getPnrGovTiming() != null
					&& !country.getPnrGovTiming().equals("")) {
				outboundEnabledCountries.add(country);
			} else {
				log.error("Invalid Outbound PNRGOV Enabled Country. PNRGOV Timing missing" + country.getCountryCode());
			}
		}

		return outboundEnabledCountries;
	}

	private Map<Country, List<String>> getCountryAirportMapForCountries(Collection<Country> countries) throws ModuleException {
		Map<Country, List<String>> countryAirportMap = new HashMap<Country, List<String>>();
		for (Country country : countries) {
			String countryCode = country.getCountryCode();
			List<String> airportCodes = ScheduledservicesUtils.getAirportBD().getActiveAirportsForCountry(countryCode);
			countryAirportMap.put(country, airportCodes);
		}

		return countryAirportMap;
	}

	private void schedulePnrGovMessages(Map<Country, List<String>> countryAirportMap, boolean isOutbound) throws ModuleException,
			SchedulerException {
		Set<Country> keySet = countryAirportMap.keySet();

		for (Country country : keySet) {
			List<String> airportCodes = countryAirportMap.get(country);
			String countryCode = country.getCountryCode();
			String pnrGovTiming = country.getPnrGovTiming();
			List<String> pnrGovTimingList = getPnrGovTimingHours(pnrGovTiming);
			getFlightSegmentDetailsAndSchedule(pnrGovTimingList, airportCodes, countryCode, isOutbound);
		}
	}

	private List<String> getPnrGovTimingHours(String pnrGovTiming) {
		List<String> timingList = new ArrayList<String>();
		if (pnrGovTiming.contains(",")) {
			String[] timingArray = pnrGovTiming.split(",");
			if (timingArray != null && timingArray.length > 0) {
				for (String time : timingArray) {
					if (StringUtils.isNumeric(time)) {
						timingList.add(time);
					} else {
						log.error("############Invalid Time. " + time);
					}
				}
			}
		} else if (StringUtils.isNumeric(pnrGovTiming)) {
			timingList.add(pnrGovTiming);
		} else {
			log.error("############Invalid Time. " + pnrGovTiming);
		}

		return timingList;
	}

	private void getFlightSegmentDetailsAndSchedule(List<String> pnrGovTimingList, List<String> airportCodes, String countryCode,
			boolean isOutbound) throws ModuleException, SchedulerException {
		ReservationAuxilliaryBD auxilliaryBD = ScheduledservicesUtils.getReservationAuxilliaryBD();
		for (String airportCode : airportCodes) {
			for (String time : pnrGovTimingList) {
				int timePeriod = Integer.parseInt(time);
				List<PNRGOVTrasmissionDetailsDTO> flightDetails = auxilliaryBD.getFlightForPnrGovScheduling(airportCode,
						timePeriod, getPnrGovTrasmissionGap(), isOutbound);
				schedulePnrGovMessages(flightDetails, countryCode, timePeriod, isOutbound);

			}
		}
	}

	private void schedulePnrGovMessages(List<PNRGOVTrasmissionDetailsDTO> flightDetails, String countryCode, int timePeriod,
			boolean isOutbound) throws SchedulerException {
		String jobID = null;
		String airportCode = null;
		int newTime = 5;
		for (PNRGOVTrasmissionDetailsDTO transmissionDetail : flightDetails) {
			if (isOutbound) {
				airportCode = transmissionDetail.getDepartureAirport();
				jobID = String.valueOf(transmissionDetail.getFltSegID()) + "/" + countryCode + "/"
						+ transmissionDetail.getDepartureAirport() + "/" + transmissionDetail.getFlightNo() + "/"
						+ transmissionDetail.getDepartureTimeZulu() + "/" + String.valueOf(timePeriod) + "/" + String.valueOf(0);
			} else {
				airportCode = transmissionDetail.getArrivalAirport();
				jobID = String.valueOf(transmissionDetail.getFltSegID()) + "/" + countryCode + "/"
						+ transmissionDetail.getArrivalAirport() + "/" + transmissionDetail.getFlightNo() + "/"
						+ transmissionDetail.getDepartureTimeZulu() + "/" + String.valueOf(timePeriod) + "/" + String.valueOf(1);
			}
			Date zuluFlightDepartureDate = transmissionDetail.getDepartureTimeZulu();
			Timestamp pnrGovScheduledTimeStamp = new Timestamp(zuluFlightDepartureDate.getTime() - timePeriod * 3600 * 1000);

			if (pnrGovScheduledTimeStamp.getTime() < new Date().getTime()) {
				if (!hasPnrGovAlreadySent(transmissionDetail.getFltSegID(), countryCode, airportCode, isOutbound, timePeriod)) {
					if (!ScheduleManager.jobWasScheduled(jobID, SSInternalConstants.JOB_TYPE.PNRGOV)) {
						log.debug("####RESCHEDULING PNRGOV MESSAGE FOR : " + jobID);
						newTime = newTime + 2;
						pnrGovScheduledTimeStamp = new Timestamp(new Date().getTime() + 60 * newTime * 1000);
						log.debug("#### NEW TIME FOR " + jobID + " is " + pnrGovScheduledTimeStamp.toString());
					} else {
						log.error("##### PNRGOV ALREADY SCHEDULED FOR THIS FLIGHT... #####");
						continue;
					}
				} else {
					log.error("#### PNRGOV MESSAGE IS ALREADY SENT FOR THIS FLIGHT. ####");
					continue;
				}
			}
			if (pnrGovScheduledTimeStamp.getTime() > new Date().getTime()) {
				JobDetail pnrGovJobDetail = new JobDetail();
				setJobData(pnrGovJobDetail, jobID, SSInternalConstants.JOB_TYPE.PNRGOV, transmissionDetail.getFltSegID(),
						airportCode, countryCode, isOutbound, timePeriod);
				pnrGovJobDetail.setJobClass(PNRGOVSenderJob.class);
				Date pnrGovStartTime = CommonUtil.getDateof(pnrGovScheduledTimeStamp);
				try {
					addDebugLog("JOB: " + jobID + "SCHEDULING PNRGOV. FLIGHT SEGMENT ID: " + transmissionDetail.getFltSegID()
							+ " PNRGOV TIME : " + pnrGovStartTime + " FOR PERIOD " + String.valueOf(timePeriod));
					ScheduleManager.scheduleJob(pnrGovJobDetail, pnrGovStartTime, null, 0, 0);
				} catch (SchedulerException exception) {
					log.error("JOB: " + jobID + " DID NOT SCHEDULE PNRGOV Message. POSSIBLLY PNRGOV TIME ALREADY SCHEDULED. ",
							exception);
				} catch (Exception e) {
					log.error("JOB: " + jobID + " DID NOT SCHEDULE PNRGOV MESSAGE. POSSIBLLY PNRGOV TIME ALREADY SCHEDULED. ", e);
					pnrGovJobDetail.setRequestsRecovery(true);
				}
			}
		}
	}

	private boolean
			hasPnrGovAlreadySent(int fltSegId, String countryCode, String airportCode, boolean isOutbound, int timePeriod) {
		try {
			return ScheduledservicesUtils.getReservationAuxilliaryBD().hasPnrGovSentHistory(fltSegId, countryCode, airportCode,
					isOutbound, timePeriod);
		} catch (ModuleException exception) {
			log.error(
					"Error when checking pnrgov message already sent! This will not interupt operation "
							+ exception.getLocalizedMessage(), exception);
		}
		return false;
	}

	private void setJobData(JobDetail jobDetail, String jobId, String jobGroup, int fltSegId, String airportCode,
			String countryCode, boolean isOutbound, int timeGap) {
		jobDetail.getJobDataMap().put(Job.PROP_JOB_ID, jobId);
		jobDetail.getJobDataMap().put(Job.PROP_JOB_GROUP, jobGroup);
		jobDetail.getJobDataMap().put(Job.PROP_FlightSegId, String.valueOf(fltSegId));
		jobDetail.getJobDataMap().put(Job.PROP_COUNTRY_CODE, countryCode);
		jobDetail.getJobDataMap().put(Job.PROP_AIRPORT_CODE, airportCode);
		jobDetail.getJobDataMap().put(Job.PROP_TIME_GAP, String.valueOf(timeGap));
		if (isOutbound) {
			jobDetail.getJobDataMap().put(Job.PROP_INBOUND_OUTBOUND, "OUTBOUND");
		} else {
			jobDetail.getJobDataMap().put(Job.PROP_INBOUND_OUTBOUND, "INBOUND");
		}
	}

	private void addDebugLog(String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	public int getPnrGovTrasmissionGap() {
		return pnrGovTrasmissionGap;
	}

	public void setPnrGovTrasmissionGap(int pnrGovTrasmissionGap) {
		this.pnrGovTrasmissionGap = pnrGovTrasmissionGap;
	}

}
