package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class OnlineCheckInReminder {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(OnlineCheckInReminder.class);

	public void execute() throws JobExecutionException {

		if (AppSysParamsUtil.isOnlineCheckInReminderEnable()) {

			try {
				ReservationBD reservationBD = ScheduledservicesUtils.getReservationBD();
				SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
				Integer minimumGapInMins = sc.getMinGapInMinitesBWJobs();

				Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
				log.debug("****************************************************************************************************************");
				log.debug("****************************************************************************************************************");
				log.debug("****************************************************************************************************************");
				log.info("START - Schedule Online CheckIn Reminders [CurrentTime = "
						+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(currentTimestamp) + "]");

				List<OnlineCheckInReminderDTO> onlineCheckInReminderDTOs = reservationBD.getFlightSegsForOnlineCheckInReminder(
						currentTimestamp, ReservationInternalConstants.SchedulerType.MASTER_SCHEDULER);

				log.debug("Schedule Online Reminders " + "[No of Reminders Fetched = " + onlineCheckInReminderDTOs.size() + "]");

				String flightNumber;
				String originAirport;
				Date departureDate;
				String segCode;
				Integer flightSegId;
				Date sendNotificationTime;
				Date previousSendNotificationTime = currentTimestamp;
				Date expectedSendNotificationTime;
				Integer flightSegNotifEventId;

				for (OnlineCheckInReminderDTO onlineCheckinReminderDTO : onlineCheckInReminderDTOs) {

					flightNumber = onlineCheckinReminderDTO.getFlightNo();
					originAirport = onlineCheckinReminderDTO.getOrigin();
					departureDate = onlineCheckinReminderDTO.getDepartureDate();
					segCode = onlineCheckinReminderDTO.getSegCode();
					flightSegId = onlineCheckinReminderDTO.getFltSegId();
					expectedSendNotificationTime = onlineCheckinReminderDTO.getSendNotificationTime();
					Date onlineCheckInRemainderSendTime = expectedSendNotificationTime;
					flightSegNotifEventId = onlineCheckinReminderDTO.getFlightSegmentNotificationId();

					String jobId = "ONLINECHECKINREM/" + "/" + segCode + "/" + flightNumber + "/"
							+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(departureDate) + "/" + flightSegId;

					if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.ONLINE_CHECKING_REMINDER)) {
						log.debug("Schedule Online CheckIn Reminders [Skipping Scheduling Job as Scheduled JOB_ID=" + jobId + "]");
						continue;
					}

					log.debug("Schedule Online CheckIn Reminders [Scheduling Job For JOB_ID = " + jobId + "]");

					JobDetail notificationJobDetail = new JobDetail();

					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_JOB_ID, jobId);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_JOB_GROUP,
							SSInternalConstants.JOB_TYPE.ONLINE_CHECKING_REMINDER);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_DepartureStation, originAirport);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FLIGHT_NUMBER, flightNumber);
					notificationJobDetail.getJobDataMap()
							.put(OnlineCheckInReminderSender.PROP_DEPARTURE_DATE_ZULU, departureDate);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FlightSegId, flightSegId);
					notificationJobDetail.getJobDataMap().put(OnlineCheckInReminderSender.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
							flightSegNotifEventId);

					notificationJobDetail.setJobClass(OnlineCheckInReminderSender.class);

					try {

						ScheduleManager.scheduleJob(notificationJobDetail, onlineCheckInRemainderSendTime, null, 0, 0);
						previousSendNotificationTime = onlineCheckInRemainderSendTime;
						log.debug("Schedule Online CheckIn Reminders [Scheduled Job JOB_ID=" + jobId
								+ ", Notification Send Timestamp="
								+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(onlineCheckInRemainderSendTime) + "]");
						log.debug("****************************************************************************************************************");
						log.debug("****************************************************************************************************************");
						log.debug("****************************************************************************************************************");
					} catch (SchedulerException se) {
						log.error("Schedule Online CheckIn Reminders [Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
					} catch (Exception e) {
						log.error("Schedule Online CheckIn Reminders [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
						notificationJobDetail.setRequestsRecovery(true);
					}

				}
			} catch (Exception ex) {
				log.error("Schedule Online CheckIn Reminders failed", ex);
				throw new JobExecutionException(ex);
			} finally {
				CredentialInvokerUtil.close();
			}
			log.info("END - Schedule Online CheckIn Reminders [CurrentTime = " + CalendarUtil.getCurrentSystemTimeInZulu() + "]");
			log.debug("****************************************************************************************************************");
			log.debug("****************************************************************************************************************");
			log.debug("****************************************************************************************************************");
		}
	}

}
