/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client.agentnotify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author nafly
 */
public class AgentNotifierJob extends QuartzJobBean {

	private Log log = LogFactory.getLog(AgentNotifierJob.class);

	@Override
	public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		
		try {
			log.info("#####################  BANK GUARANTEE EXPIRY/UTILIZATION NOTIFICATION JOB STARTED ######################");
			CredentialInvokerUtil.invokeCredentials();

			if (AppSysParamsUtil.isBankGuaranteeNotificationEnabled()) {
				ScheduledservicesUtils.getTravelAgentFinanceBD().sendBankGuaranteeNotificationEmail();

				ScheduledservicesUtils.getTravelAgentFinanceBD().sendCreditLimitUtilizationEmail();
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Bank Guarantee Notification Email is Disabled. No Processing done");
				}
			}

			log.info("#####################  BANK GUARANTEE EXPIRY/UTILIZATION NOTIFICATION JOB COMPLETED ######################");
		} catch (Exception me) {
			if (log.isErrorEnabled()) {
				log.error("send Bank Gurantee Notification failed", me);
			}
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
