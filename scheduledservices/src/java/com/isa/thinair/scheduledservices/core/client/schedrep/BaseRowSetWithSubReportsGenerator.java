package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseRowSetWithSubReportsGenerator extends ReportGeneratorCommon {

	private static final Log log = LogFactory.getLog(BaseRowSetWithSubReportsGenerator.class);

	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {

		String fileName = null;
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream
				.fromXML(scheduledReport.getCriteriaTemplete());
		@SuppressWarnings("unchecked") Map<String, Object> parameters = (Map<String, Object>) xstream
				.fromXML(scheduledReport.getParamTemplete());

		// inject the date range
		injectRecalulatedParams(reportsSearchCriteria, parameters, scheduledReport);

		// register the template
		String reportPath = PlatformConstants.getConfigRootAbsPath() + scheduledReport.getReportTempleteRelativePath();
		ScheduledservicesUtils.getReportingFrameworkBD()
				.storeJasperTemplateRefs(scheduledReport.getReportName(), reportPath);

		// Retrieve the result set map based on the parameters regenerated
		Map<String, ResultSet> resultSetMap = getResultSetMap(reportsSearchCriteria);
		HashMap mainReportParameterMap = new HashMap();

		if (resultSetMap != null) {

			for (Object o : resultSetMap.entrySet()) {
				Map.Entry pair = (Map.Entry) o;
				//noinspection unchecked
				mainReportParameterMap.put(pair.getKey(), new JRResultSetDataSource((ResultSet) pair.getValue()));
			}

			//set the parameters those need to be transferred to the report
			setParameters(mainReportParameterMap, parameters);

			byte[] reportBytes = this.createReport(scheduledReport, parameters, resultSetMap);

			// finding the file name
			fileName = this.createFileName(scheduledReport);

			uploadOrAttachFile(scheduledReport, fileName, reportBytes);
		}

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK && getReportConfig()
				.isCopyToPublicDir()) {
			copyReportsToReportsStore(fileName);
		}

		return fileName;

	}

	public abstract Map<String, ResultSet> getResultSetMap(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException;

	public abstract void setParameters(HashMap mainReportParameterMap, Map<String, Object> parameters)
			throws ModuleException;

}
