package com.isa.thinair.scheduledservices.core.client.ccc;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Daily flown segments insurance data
 * 
 * @author pradeep
 */
public class PublishCCCInsuranceDailyFlownSalesJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(PublishCCCInsuranceDailyFlownSalesJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
			log.info("###################### START	- Main Scheduler Job for CCC Insurace Publication daily flown sales [CurrentTime = "
					+ currentTimestamp + "] ######################");
			schedulePublishingSaleInsuranceData();
			log.info("###################### END 	- Main Scheduler Job for CCC Insurace Publication daily flown sales [CurrentTime = "
					+ currentTimestamp + "] ######################");
		} catch (Exception e) {
			log.error("Main Scheduler Job for CCC Insurace Publication Failed", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void schedulePublishingSaleInsuranceData() throws JobExecutionException {
		boolean isUpload = true;
		Exception tempEx = null;

		try {
			isUpload = ScheduledservicesUtils.getReservationBD().publishInsuranceSalesData(InsuranceSalesType.DAILY_FLOWN_SALES);
			log.info(" #####################     LAST PUBLISH SUB JOB SCHEDULING ENDED    ######################");
		} catch (Exception ex) {
			isUpload = false;
			tempEx = ex;
			log.error("Schedule Insurance Publishings failed CCC daily flown sales", ex);
		}
		// Send E-mail to inform upload fail
		if (isUpload == false) {
			Map config = ReservationModuleUtils.getAirReservationConfig().getCccInsuranceConfigMap();
			SimpleEmailDTO emailDTO = new SimpleEmailDTO();
			emailDTO.setToEmailAddress((String) config.get("supportEmail"));
			emailDTO.setSubject("Insurance daily FTP flown sales data upload fail - Cabinet Chaubet Courtage (AirMed)");
			emailDTO.setContent("Insurance daily FTP flown sales data upload fail - Cabinet Chaubet Courtage (AirMed)");
			try {
				ScheduledservicesUtils.getMessagingServiceBD().sendSimpleEmail(emailDTO);
			} catch (Exception e) {
				log.error(e);
			}
			if (tempEx != null) {
				throw new JobExecutionException(tempEx);
			}
		}
	}

}
