package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class CALSender implements Job {

	private static Log log = LogFactory.getLog(CALSender.class);

	@Override
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			if(AppSysParamsUtil.isEnablePalCalMessage()){
				org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
	
				String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
	
				String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
	
				String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
	
				CredentialInvokerUtil.invokeCredentials();
	
				log.info(" #####################     CAL JOB STARTED ######################");
				log.info(" ----------------------------------------------------------------");
				log.info(" #####################     CAL FOR FLIGHT ID : " + flightId);
				log.info(" #####################     DEPATURE STATION : " + depAirport);
				log.info(" #####################     FLIGHT NUMBER : " + flightNumber);
				log.info(" ----------------------------------------------------------------");
				sendCALMessage(Integer.parseInt(flightId), depAirport);
			}

		} catch (Exception me) {
			log.error("send CAL failed", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

	private void sendCALMessage(int flightId, String departureStation) throws ModuleException {
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendCALMessage(flightId, departureStation);
	}

}
