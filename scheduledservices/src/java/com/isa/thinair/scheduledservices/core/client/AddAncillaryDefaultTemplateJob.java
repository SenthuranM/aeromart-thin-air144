/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class AddAncillaryDefaultTemplateJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(AddAncillaryDefaultTemplateJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			AddAncillaryDefaultTemplate.execute();
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Add Ancillary Default Templates job failed");
			}
			throw new JobExecutionException(e);
		}
	}

}
