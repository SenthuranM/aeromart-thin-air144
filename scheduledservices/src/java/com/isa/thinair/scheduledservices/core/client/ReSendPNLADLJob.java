package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author byorn
 */
public class ReSendPNLADLJob extends QuartzJobBean {

	private Log log = LogFactory.getLog(ReSendPNLADLJob.class);

	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
		try {
			log.info("######[PNL ADL MESSAGE RESENDER IS STARTING]");
			log.info("###### ------------------------------------");
			PnlAdlResender.checkAndResend();
			log.info("###### [PNL ADL MESSAGE RESENDER HAS COMPLETED]");
		} catch (Exception e) {
			log.error("### ERROR occured in JOb: " + e);
			throw new JobExecutionException("job.resend.failed");
		}
	}

}
