package com.isa.thinair.scheduledservices.core.client;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class DeactivateTravelAgents {

	public void execute() throws ModuleException {
		ScheduledservicesUtils.getTravelAgentBD().deactivateTravelAgents();
	}
}
