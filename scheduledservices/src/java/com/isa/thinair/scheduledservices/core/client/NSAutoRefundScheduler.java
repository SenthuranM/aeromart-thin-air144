/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author jagath
 * 
 */
public class NSAutoRefundScheduler {
	private static Log log = LogFactory.getLog(NSAutoRefundScheduler.class);

	public static void procesNSAutoRefundPNRs() throws Exception {
		try {
			log.info("Before start NOSHOW Auto Refund Process");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationBD().processNSAutoRefundPNRs();
			log.info("Finished NOSHOW Auto Refund Process");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
