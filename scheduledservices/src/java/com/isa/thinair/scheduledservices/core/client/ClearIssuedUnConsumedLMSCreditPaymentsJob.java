package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ClearIssuedUnConsumedLMSCreditPaymentsJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(ClearIssuedUnConsumedLMSCreditPaymentsJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("######### Cancel isssued unconsumed LMS Credit Job invoked ##########");
		log.info("######### Cancel isssued unconsumed LMS Credit Job  started at " + new Date());

		try {
			CredentialInvokerUtil.invokeCredentials();

			ScheduledservicesUtils.getReservationBD().clearIssuedUnConsumedLMSCreditPayments();
		} catch (ModuleException e) {
			log.error("Error @ Cancel isssued unconsumed LMS Credit Job  " + e.getMessage());
		}
		log.info("######### Cancel isssued unconsumed LMS Credit Job b ended at " + new Date());

	}
}
