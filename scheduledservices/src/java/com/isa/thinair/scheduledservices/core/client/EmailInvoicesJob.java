package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author byorn
 * 
 */
public class EmailInvoicesJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(EmailInvoicesJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("############## Auto Invoicing Started for " + new Date());
			Date currentDate = new Date();
			SimpleDateFormat dateFormatDay = new SimpleDateFormat("dd");
			String dayNum = dateFormatDay.format(currentDate);

			if (Integer.valueOf(dayNum).intValue() == 16 || Integer.valueOf(dayNum).intValue() == 01) {
				if (log.isDebugEnabled()) {
					log.info("############## Auto Invoicing Started for " + new Date());
				}
			}
			ScheduledservicesUtils.getInvoicingBD().autoEmailInvoices();

			log.info("############## Auto Invoicing Ended for " + new Date());
		} catch (Exception me) {
			log.error("Errror when auto invoicing" + me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
