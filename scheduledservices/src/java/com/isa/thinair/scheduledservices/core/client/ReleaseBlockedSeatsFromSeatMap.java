package com.isa.thinair.scheduledservices.core.client;

import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author indika
 */
public class ReleaseBlockedSeatsFromSeatMap {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ReleaseBlockedSeatsFromSeatMap.class);

	public static void releaseBlockedSeatsFromSeatMap() throws Exception {

		try {
			CredentialInvokerUtil.invokeCredentials();
			// Get Collection from RES
			log.info("############################################################");
			log.info("#### Starting Releasing Blocked Seats from SeatMap    ######");
			Calendar oCal = Calendar.getInstance();
			log.info("#### Started " + oCal + "                             ######");

			ServiceResponce sr = ScheduledservicesUtils.getSeatMapBD().releaseBlockedSeats();

			if (sr.isSuccess()) {
				log.info("#### Relese Success                                 ######");
			} else {
				log.info("#### Relese Fail                                    ######");
			}

			log.info("#### End Resolving Releasing Blocked Seats from SeatMap ###########");
			log.info("############################################################");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
