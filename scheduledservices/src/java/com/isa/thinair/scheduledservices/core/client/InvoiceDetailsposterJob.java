package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class InvoiceDetailsposterJob extends QuartzJobBean {

	public Log log = LogFactory.getLog(InvoiceDetailsposterJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		synchronized (this) {

			try {
				CredentialInvokerUtil.invokeCredentials();
				Date currentDate = new Date();
				SimpleDateFormat dateFormatDay = new SimpleDateFormat("dd");
				String dayNum = dateFormatDay.format(currentDate);
				int dayNumber = Integer.valueOf(dayNum).intValue();

				if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
					Calendar date = Calendar.getInstance();
					int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
					if (AppSysParamsUtil.getDayNumberToGenerateInvoices() == dayOfWeek) {
						int period = 0;

						int year = date.get(GregorianCalendar.YEAR);
						int month = date.get(GregorianCalendar.MONTH);
						if (dayNumber >= 1 && dayNumber < 8) {
							// This is the last invoice of the previous month. Can be 4 or 5
							period = BLUtil.getLastMonthFinalInvoicePeriod(month, year);
						} else if (dayNumber >= 8 && dayNumber < 15) {
							period = 1;
							month = month + 1;
						} else if (dayNumber >= 15 && dayNumber < 22) {
							period = 2;
							month = month + 1;
						} else if (dayNumber >= 22 && dayNumber < 29) {
							period = 3;
							month = month + 1;
						} else {
							period = 4;
							month = month + 1;
						}
						SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
						searchInvoicesDTO.setFilterZeroInvoices(true);
						searchInvoicesDTO.setMonth(month);
						searchInvoicesDTO.setYear(year);
						searchInvoicesDTO.setInvoicePeriod(period);
						searchInvoicesDTO.setAgentCodes(null);

						if (log.isDebugEnabled()) {
							log.debug("######################## WILL START INVOICE GENERATION");
						}

						ScheduledservicesUtils.getInvoicingBD().generateInvoices(searchInvoicesDTO,
								GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
					}

				} else {
					if (dayNumber == 16 || dayNumber == 01) {

						int period = 0;

						GregorianCalendar gc = new GregorianCalendar();
						int year = gc.get(GregorianCalendar.YEAR);
						int month = gc.get(GregorianCalendar.MONTH);
						if (dayNumber == 16) {
							period = 1;
							month = month + 1;
						} else {
							period = 2;
						}
						SearchInvoicesDTO searchInvoicesDTO = new SearchInvoicesDTO();
						searchInvoicesDTO.setFilterZeroInvoices(true);
						searchInvoicesDTO.setMonth(month);
						searchInvoicesDTO.setYear(year);
						searchInvoicesDTO.setInvoicePeriod(period);
						searchInvoicesDTO.setAgentCodes(null);

						if (log.isDebugEnabled()) {
							log.debug("######################## WILL START INVOICE GENERATION");
						}

						ScheduledservicesUtils.getInvoicingBD().generateInvoices(searchInvoicesDTO,
								GenerateInvoiceOption.IF_EXISTS_DO_NOT_PROCEED);
					}
				}

			} catch (Exception e) {
				throw new JobExecutionException(e);
			} finally {
				CredentialInvokerUtil.close();
			}
		}
	}
}
