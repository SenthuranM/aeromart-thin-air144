/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class AddAncillaryDefaultTemplate {
	private static Log log = LogFactory.getLog(AddAncillaryDefaultTemplate.class);

	public static void execute() throws Exception {

		try {
			CredentialInvokerUtil.invokeCredentials();
			log.info("#######################################################");
			log.info("########## Starting Sending Flight Schedule ###########");

			if (AppSysParamsUtil.isAssignDefaultMealChargesTemplate()) {
				log.info("######### Add Ancillary Default Meal Tempaltes Job Started " + new Date());
				ScheduledservicesUtils.getFlightBD().assignDefaultMealChargesTemplate();
				log.info("######### Add Ancillary Default Meal Tempaltes Job End " + new Date());
			}
			if (AppSysParamsUtil.isAssignDefaultSeatChargesTemplate()) {
				log.info("######### Add Ancillary Default Seat Map Tempaltes Job Started " + new Date());
				ScheduledservicesUtils.getFlightBD().assignDefaultSeatMapChargesTemplate();
				log.info("######### Add Ancillary Default Seat Tempaltes Job End " + new Date());
			}
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
