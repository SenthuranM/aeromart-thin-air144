package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author byorn
 */
public class RMNotifyerJob extends QuartzJobBean {
	private static final Log log = LogFactory.getLog(RMNotifyerJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			RMNotifyer.notifyRM();
		} catch (Exception e) {
			log.error(e);
			throw new JobExecutionException(e);
		}
	}

}
