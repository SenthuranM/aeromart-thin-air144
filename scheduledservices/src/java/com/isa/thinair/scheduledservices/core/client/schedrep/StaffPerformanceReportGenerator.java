/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * @author Janaka Padukka
 * 
 */
public class StaffPerformanceReportGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		DateFormat inputFormatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
		DateFormat outputFormatter = new SimpleDateFormat("dd-MMM-yyyy");

		Date from;
		Date to;
		try {
			from = inputFormatter.parse(reportsSearchCriteria.getDateRangeFrom());
			to = inputFormatter.parse(reportsSearchCriteria.getDateRangeTo());
			reportsSearchCriteria.setDateRangeFrom(outputFormatter.format(from));
			reportsSearchCriteria.setDateRangeTo(outputFormatter.format(to));
			return ScheduledservicesUtils.getDataExtractionBD().getPerformanceOfSalesStaffSummaryData(
					reportsSearchCriteria);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new ModuleException(e, "reporting.schedreports.data.extraction.failed");
		}

	}
}
