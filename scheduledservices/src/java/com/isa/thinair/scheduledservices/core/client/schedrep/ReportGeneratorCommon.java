package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.ReportFormatTypes;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.IBEOnHoldPassengersDTO;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.reportingframework.api.dto.ReportParam;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import static com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduledReportMetaDataIDs.*;
/**
 * Common Report Generation Generalization
 * 
 * @author Navod Ediriweera
 * 
 */
public abstract class ReportGeneratorCommon implements ReportGenerator, ReportFormatTypes.ReportTypes {
	protected String strLogo = AppSysParamsUtil.getReportLogo(false);
	protected String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
	private ReportConfig reportConfig;
	private static final Log log = LogFactory.getLog(ReportGeneratorCommon.class);

	protected byte[] createReport(ScheduledReport scheduledReport, Map<String, Object> parameters, ResultSet resultSet,
			Map<Object, Object> imagesMap) throws ModuleException {
		this.setReportLogoLocation(scheduledReport.getReportFormat(), parameters);
		if (HTML.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createHTMLReport(scheduledReport.getReportName(), parameters,
					resultSet, imagesMap, null);
		} else if (CSV.equals(scheduledReport.getReportFormat()) || CSV_AGENT.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createCSVReport(scheduledReport.getReportName(), parameters,
					resultSet);
		} else if (EXCEL.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createXLSReport(scheduledReport.getReportName(), parameters,
					resultSet);
		} else {
			return ScheduledservicesUtils.getReportingFrameworkBD().createPDFReport(scheduledReport.getReportName(), parameters,
					resultSet);
		}
	}

	protected byte[] createReport(ScheduledReport scheduledReport, Map<String, Object> parameters,
			Map<String, ResultSet> resultSetMap) throws ModuleException {
		this.setReportLogoLocation(scheduledReport.getReportFormat(), parameters);
		if (HTML.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD()
					.createReport(ReportParam.ReportType.HTML, scheduledReport.getReportName(), parameters,
							resultSetMap);
		} else if (CSV.equals(scheduledReport.getReportFormat()) || CSV_AGENT
				.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD()
					.createReport(ReportParam.ReportType.CSV, scheduledReport.getReportName(), parameters,
							resultSetMap);
		} else if (EXCEL.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD()
					.createReport(ReportParam.ReportType.XSL, scheduledReport.getReportName(), parameters,
							resultSetMap);
		} else {
			return ScheduledservicesUtils.getReportingFrameworkBD()
					.createReport(ReportParam.ReportType.PDF, scheduledReport.getReportName(), parameters,
							resultSetMap);
		}
	}

	protected String createFileName(ScheduledReport scheduledReport) {
		String fileExtension = ".pdf";
		if (HTML.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".html";
		} else if (CSV.equals(scheduledReport.getReportFormat()) || CSV_AGENT.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".csv";
		} else if (EXCEL.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".xls";
		}
		return scheduledReport.getReportName() + "_" + generateUniqueID() + fileExtension;
	}

	protected String generateUniqueID() {

		return UniqueIDGenerator.generate();
	}

	protected void setReportLogoLocation(String reportFormat, Map<String, Object> parameters) {

		if (HTML.equals(reportFormat)) {
			// reportsRootDir = "../../images/" + strLogo;
			// reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/"
			// + AppSysParamsUtil.getReportLogo(false);
			reportsRootDir = AppSysParamsUtil.getImageUploadPath() + AppSysParamsUtil.getReportLogo(false);
			parameters.put("IMG", reportsRootDir);

		} else if (PDF.equals(reportFormat)) {
			String strLogo = AppSysParamsUtil.getReportLogo(false);// ScheduledservicesUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsIMGDir = getReportTemplate(strLogo);
			parameters.put("IMG", reportsIMGDir);

		}
	}

	/**
	 * Retrive the actual report Template path
	 * 
	 * @param fileName
	 * @return
	 */
	protected String getReportTemplate(String fileName) {
		String reportsRootDir = null;
		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;
		return reportsRootDir;
	}

	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	}

	protected SimpleDateFormat getReportParametersDateFormat() {
		return new SimpleDateFormat("dd/MM/yyyy");
	}

	protected void injectRecalulatedParams(ReportsSearchCriteria reportsSearchCriteria, Map<String, Object> parameters,
			ScheduledReport scheduledReport) throws ModuleException {

		SimpleDateFormat format = getReportsSearchCriteriaDateFormat();
		SimpleDateFormat paramFormat = getReportParametersDateFormat();
		Date startDate = null;
		Date endDate = null;

		try {

			if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_ADJUSTED_RANGE)) {

				String startDateInTemplete = reportsSearchCriteria.getDateRangeFrom();
				startDate = format.parse(startDateInTemplete);

				String endDateInTemplete = reportsSearchCriteria.getDateRangeTo();
				endDate = format.parse(endDateInTemplete);

				int dayDiff = CalendarUtil.daysUntil(scheduledReport.getCreatedTime(), new Date());
				startDate = CalendarUtil.addDateVarience(startDate, dayDiff);
				endDate = CalendarUtil.addDateVarience(endDate, dayDiff);

			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_LAST_WEEK)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.WEEK_OF_MONTH, -1);
				cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.add(Calendar.DAY_OF_MONTH, 6);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();

			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_LAST_HALF_MONTH)) {

				Calendar cal = new GregorianCalendar();
				if (cal.get(Calendar.DAY_OF_MONTH) >= 15) {

					cal.set(Calendar.DAY_OF_MONTH, 1);
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					startDate = cal.getTime();

					cal.set(Calendar.DAY_OF_MONTH, 14);
					cal.set(Calendar.HOUR_OF_DAY, 23);
					cal.set(Calendar.MINUTE, 59);
					cal.set(Calendar.SECOND, 59);
					endDate = cal.getTime();

				} else {
					// getting last date for last month
					int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH)));

					cal.set(Calendar.DAY_OF_MONTH, 15);
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					startDate = cal.getTime();

					cal.set(Calendar.DAY_OF_MONTH, lastDate);
					cal.set(Calendar.HOUR_OF_DAY, 23);
					cal.set(Calendar.MINUTE, 59);
					cal.set(Calendar.SECOND, 59);
					endDate = cal.getTime();
				}

			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_LAST_MONTH)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, -1);

				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH) + 1));
				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_END_OF_THIS_MONTH)) {

				Calendar cal = new GregorianCalendar();

				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_NEXT_MONTH)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, 1);

				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_NEXT_2_MONTHS)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, 1);

				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.add(Calendar.MONTH, 1);
				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_NEXT_3_MONTHS)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, 1);

				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.add(Calendar.MONTH, 2);
				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			} else if (scheduledReport.getDateCalculationMechanism().equals(ScheduledReport.PERIOD_NEXT_6_MONTHS)) {

				Calendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, 1);

				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				startDate = cal.getTime();

				cal.add(Calendar.MONTH, 5);
				int lastDate = CalendarUtil.getLastDayOfYearAndMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
				cal.set(Calendar.DAY_OF_MONTH, lastDate);
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
			}

			reportsSearchCriteria.setDateRangeFrom(format.format(startDate));
			reportsSearchCriteria.setDateRangeTo(format.format(endDate));

			Date currentTime = CalendarUtil.getCurrentZuluDateTime();

			parameters.put("FROM_DATE", paramFormat.format(startDate));
			parameters.put("TO_DATE", paramFormat.format(endDate));
			parameters.put("ZULU_PRINT_DATE", paramFormat.format(currentTime));
			parameters.put("ZULU_PRINT_TIME", DateUtil.formatDate(currentTime, "HH:mm:ss"));

		} catch (ParseException e) {
			throw new ModuleException(e, "reporting.date.invalid");
		}
	}

	@Override
	public Boolean isEmailAttachmentReport() throws ModuleException {
		// Defaults to true
		return getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.ATTACHMENT;
	}
	protected byte[] createReport(ScheduledReport scheduledReport, Map<String, Object> parameters, List<IBEOnHoldPassengersDTO> colData,
			Map<Object, Object> imagesMap) throws ModuleException {
		this.setReportLogoLocation(scheduledReport.getReportFormat(), parameters);
		if (HTML.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createHTMLReport(scheduledReport.getReportName(), parameters,
					colData, imagesMap, null);
		} else if (CSV.equals(scheduledReport.getReportFormat()) || CSV_AGENT.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createCSVReport(scheduledReport.getReportName(), parameters,
					colData);
		} else if (EXCEL.equals(scheduledReport.getReportFormat())) {
			return ScheduledservicesUtils.getReportingFrameworkBD().createXLSReport(scheduledReport.getReportName(), parameters,
					colData);
		} else {
			return ScheduledservicesUtils.getReportingFrameworkBD().createPDFReport(scheduledReport.getReportName(), parameters,
					colData);
		}
		
	}

	public ReportConfig getReportConfig() {
		return reportConfig;
	}

	public void setReportConfig(ReportConfig reportConfig) {
		this.reportConfig = reportConfig;
	}

	// Code to FTP
	protected void uploadOrAttachFile(ScheduledReport scheduledReport, String fileName, byte[] reportBytes) {
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
		serverProperty.setUserName(
				StringUtils.defaultString(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName()));
		serverProperty.setPassword(
				StringUtils.defaultString(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword()));
		ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());
		String schedReptName = scheduledReport.getReportName();
		if (GSTR1_REPORT.equalsIgnoreCase(schedReptName) || GSTR3_REPORT.equalsIgnoreCase(schedReptName) ||
				GST_ADDITIONAL_REPORT.equalsIgnoreCase(schedReptName) ||
				NIL_TRANSACTION_AGENTS.equalsIgnoreCase(schedReptName) || TRANSACTION_REPORT.equalsIgnoreCase(schedReptName)) {
			ftpUtil.uploadAttachmentWithOutFTP(fileName, reportBytes);
		} else {
			ftpUtil.uploadAttachment(fileName, reportBytes, serverProperty);
		}
	}

	protected void copyReportsToReportsStore(String fileName) throws ModuleException {

		String reportPath = PlatformConstants.getAbsAttachmentsPath() + File.separator + fileName;
		File src = new File(reportPath);

		String destPath = AppSysParamsUtil.getReportsStorageLocation() + File.separator + fileName;
		File dest = new File(destPath);

		try {
			Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			log.error("file copy failed -- src : " + src.getAbsolutePath() + " -- dest : " + dest.getAbsolutePath(), e);
			throw new ModuleException("file copy failed", e);
		}
	}

}
