package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * This is a scheduler job to send notifications for booked hala services for each flight, each airport.
 * 
 * @author lalanthi
 * 
 */
public class EmailBookedHalaServicesJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			EmailBookedHalaServices.execute();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}
