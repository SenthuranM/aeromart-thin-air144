package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class FareUpdateOnExchangeRateChangeAutomationJob extends QuartzJobBean {
	private static final Log log = LogFactory.getLog(FareUpdateOnExchangeRateChangeAutomationJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			FareUpdateOnExchangeRateChangeAutomation.execute();
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fare and Charge Update On Exchange Rate Change Automation job failed");
			}
			throw new JobExecutionException(e);
		}
	}

}
