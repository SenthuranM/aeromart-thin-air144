package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * Scheduled Agent Sale/Modify/Refund Report
 * 
 * @author M.Rikaz
 * 
 */
public class AgentSalesModifyReportGenerator extends CompleteReportGenerator {

	@Override
	protected void generateReportFile(ReportsSearchCriteria reportSearchCriteria) throws ModuleException {

		ScheduledservicesUtils.getDataExtractionBD().getAgentSalesModifyRefundReports(reportSearchCriteria);

		generatedReportFileNamePrefix = this.reportConfigurationMap.get("REPORT_DETAIL_PREFIX").toString();

		setGeneratedFileFormat(this.reportConfigurationMap.get("REPORT_DETAIL_FILETYPE").toString());

	}

	@Override
	protected ResultSet getReportDataForAttachment(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
}
