package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class CompPaymentDetailGenerator extends BaseRowsetGenerator {
	
	private static Log log = LogFactory.getLog(CompPaymentDetailGenerator.class);
	
	
	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {

		String zipFileName = null;
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream
				.fromXML(scheduledReport.getCriteriaTemplete());
		Map<String, Object> parameters = (Map<String, Object>) xstream.fromXML(scheduledReport.getParamTemplete());

		// inject the date range

			injectRecalulatedParams(reportsSearchCriteria, parameters,
					scheduledReport);

			// register the template
			String reportPath = PlatformConstants.getConfigRootAbsPath()
					+ scheduledReport.getReportTempleteRelativePath();
			ScheduledservicesUtils.getReportingFrameworkBD()
					.storeJasperTemplateRefs(scheduledReport.getReportName(),
							reportPath);

			Collection<Agent> allAgents = new ArrayList<Agent>();
			allAgents = ScheduledservicesUtils.getTravelAgentBD().getAgents(
					reportsSearchCriteria.getAgents());
			Collection<String> allDetailReports = new ArrayList<String>();
			
			for (Agent agent : allAgents) {
				String fileName = null;
				Collection<String> agentCodes = new ArrayList<String>();
				agentCodes.add(agent.getAgentCode());
				reportsSearchCriteria.setAgents(agentCodes);
				reportsSearchCriteria.setCurrencies(agent.getCurrencyCode());
				fileName = getDetailReportForAgent(scheduledReport, parameters,
						reportsSearchCriteria, agent.getAgentCode());
				allDetailReports.add(fileName);
			}
			
			if(reportsSearchCriteria.isOnlyReportingAgents()){
				
				Collection<Agent> gsaAgents = ScheduledservicesUtils.getTravelAgentBD().getActiveAgentsForGSA(reportsSearchCriteria.getGsaCode());
				
				for (Agent agent : gsaAgents) {
					String fileName = null;
					Collection<String> agentCodes = new ArrayList<String>();
					agentCodes.add(agent.getAgentCode());
					reportsSearchCriteria.setAgents(agentCodes);
					reportsSearchCriteria.setCurrencies(agent.getCurrencyCode());
					fileName = getDetailReportForAgent(scheduledReport, parameters,
							reportsSearchCriteria, agent.getAgentCode());
					allDetailReports.add(fileName);
				}
			}
			
			zipFileName = generateZipFile(allDetailReports,scheduledReport.getReportName());

		if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK) {
			copyReportsToReportsStore(zipFileName);
		}
			return zipFileName;
		
	}

	private String generateZipFile(Collection<String> filesNames, String reportName) {
		
		String zipFileName = null;		
	    String attachmentName = null;
		try {
			attachmentName = reportName + generateUniqueID() + ".zip";
			zipFileName =  PlatformConstants.getAbsAttachmentsPath() + "/"+ attachmentName;
			FileOutputStream fos = new FileOutputStream(zipFileName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			
			for(String fileName: filesNames){
				fileName = PlatformConstants.getAbsAttachmentsPath() + "/" + fileName;
				addToZipFile(fileName,zos);
			}
			zos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			 log.error("Error while creating zip file, fnf",e);
		} catch (IOException e) {
			log.error("Error while creating zip file, io",e);
		}	
		 
		uploadZipFileToFtpServer(attachmentName,zipFileName);
		
		return attachmentName;
	}

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		
		return ScheduledservicesUtils.getDataExtractionBD().getCompanyPaymentCurrencyData(reportsSearchCriteria);
	}

	@Override
	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy");
	}
	
	private String getDetailReportForAgent(ScheduledReport scheduledReport,Map<String, Object> parameters, ReportsSearchCriteria reportsSearchCriteria, String agentCode ) throws ModuleException{
		
		ResultSet resultSet = getResultSet(reportsSearchCriteria);
		String fileName = null;
		
		byte[] reportBytes = this.createReport(scheduledReport, parameters, resultSet, new HashMap<Object, Object>());

		fileName =  agentCode + "_" + this.createFileName(scheduledReport);

		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		}
		serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
		}

		ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());
		ftpUtil.uploadAttachment(fileName, reportBytes, serverProperty);		
		return fileName;
	}
	
	private void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
		
		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(fileName);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}
		file.delete();
		zos.closeEntry();
		fis.close();
	}
	
	private void uploadZipFileToFtpServer(String remote,String local){
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		}
		serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
		}
		
		ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());
	    ftpUtil.uploadZipFile(remote, local,serverProperty);
		
	}
	
}
