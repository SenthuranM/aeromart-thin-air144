package com.isa.thinair.scheduledservices.core.client.lcc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class LCCUserInfoPublisherJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(LCCUserInfoPublisherJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			LCCUserInfoPublisher.execute();
		} catch (Exception e) {
			log.error("LCCUserInfoPublisherJob ", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
