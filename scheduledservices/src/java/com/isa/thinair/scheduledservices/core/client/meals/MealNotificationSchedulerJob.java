package com.isa.thinair.scheduledservices.core.client.meals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

/**
 * @author Indika Athauda
 */
public class MealNotificationSchedulerJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(MealNotificationSchedulerJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();

			log.info("############################## SCHEDULING MEAL NOTIFICATION JOB IS STARTED " + new Date());
			ReservationAuxilliaryBD auxilliaryBD = ScheduledservicesUtils.getReservationAuxilliaryBD();

			log.debug("############################## RETREIVING FLIGHT MEAL NOTIFICATION TRNSMSN TIMES");
			// FIXME Indika to take flights only relative to Meals
			ArrayList<MealNotificationDetailsDTO> list = auxilliaryBD.getFlightForMealNotificationScheduling();
			log.debug("############################## RETREIVING FLIGHT MEAL NOTIFICATION TRNSMSN TIMES SUCCESSFUL !!!");

			Collections.sort(list, new LatestFirstSortComparator());

			for (MealNotificationDetailsDTO mealNotificationDetailsDTO : list) {
				// String jobId=null;
				int notifyCount;
				int lastNotification;
				if (mealNotificationDetailsDTO.getNotifyFrequency() > 0) {
					notifyCount = (mealNotificationDetailsDTO.getNotifyStartTime() - mealNotificationDetailsDTO
							.getLastNotification()) / mealNotificationDetailsDTO.getNotifyFrequency();
					lastNotification = (mealNotificationDetailsDTO.getNotifyStartTime() - mealNotificationDetailsDTO
							.getLastNotification()) % mealNotificationDetailsDTO.getNotifyFrequency();
				} else {
					notifyCount = 0;
					lastNotification = 1;
				}

				int actualcount = 0;
				if (lastNotification > 0) {
					actualcount = notifyCount + 2;
				} else {
					actualcount = notifyCount + 1;
				}

				for (int i = 0; i <= notifyCount; i++) {
					String jobId = "MEAL_NOTIFY_" + mealNotificationDetailsDTO.getFlightNumber() + "/"
							+ mealNotificationDetailsDTO.getDepartureStation() + "/"
							+ mealNotificationDetailsDTO.getDepartureTimeZulu() + "_" + (i + 1) + "OF" + actualcount + "";

					/*
					 * Formula: Notify time = Flight_departure - ( start - ( frequency x count ))
					 * 
					 * count = ((start - last)/frequency ) + 1
					 */
					Timestamp mealNotificationScheduledTimeStamp = new Timestamp(
							mealNotificationDetailsDTO.getDepartureTimeZulu().getTime()
									- 60
									* (mealNotificationDetailsDTO.getNotifyStartTime() - (mealNotificationDetailsDTO
											.getNotifyFrequency() * i)) * 1000);

					if (mealNotificationScheduledTimeStamp.getTime() > new Date().getTime()) {
						if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.MEAL_NOTIFICATION)) {
							log.info("MEAL NOTIFICATION WAS NOT SCHEDULED FOR " + jobId
									+ " AND NOTIFICATION SHOULD HAVE SENT AT :" + mealNotificationScheduledTimeStamp.toString());
							scheduleMealNotification(mealNotificationScheduledTimeStamp, jobId, mealNotificationDetailsDTO);
							log.info("NEW MEAL NOTIFICAITON TIME FOR " + jobId + " IS : "
									+ mealNotificationScheduledTimeStamp.toString());
						} else {
							log.warn("MEAL NOTIFICATION WAS ALREADY SCHEDULED AND WILL NOT SCHEDULE:" + jobId);
							continue;
						}
					} else {
						log.info("NOTIFICATION TIME PASSED");
					}

				}

				// Last notification should send where (start-last)%frequency > 0 , means remainder is more than 0. So
				// the last notification does not fall in to frequency cycle
				if (lastNotification > 0) {
					// Send seperate last notification JOB
					String jobId = "MEAL_NOTIFY_" + mealNotificationDetailsDTO.getFlightNumber() + "/"
							+ mealNotificationDetailsDTO.getDepartureStation() + "/"
							+ mealNotificationDetailsDTO.getDepartureTimeZulu() + "_" + actualcount + "OF" + actualcount + "";
					Timestamp mealNotificationScheduledTimeStamp = new Timestamp(mealNotificationDetailsDTO
							.getDepartureTimeZulu().getTime() - 60 * mealNotificationDetailsDTO.getLastNotification() * 1000);

					if (mealNotificationScheduledTimeStamp.getTime() > new Date().getTime()) {
						if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.MEAL_NOTIFICATION)) {
							log.info("MEAL NOTIFICATION WAS NOT SCHEDULED FOR " + jobId
									+ " AND NOTIFICATION SHOULD HAVE SENT AT :" + mealNotificationScheduledTimeStamp.toString());
							scheduleMealNotification(mealNotificationScheduledTimeStamp, jobId, mealNotificationDetailsDTO);
							log.info("NEW MEAL NOTIFICAITON TIME FOR " + jobId + " IS : "
									+ mealNotificationScheduledTimeStamp.toString());
						} else {
							log.warn("MEAL NOTIFICATION WAS ALREADY SCHEDULED AND WILL NOT SCHEDULE:" + jobId);
							continue;
						}
					} else {
						log.info("NOTIFICATION TIME PASSED");
					}

				}
			}
		} catch (Exception e) {
			log.error("MealNotificationSchedulerJob ", e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void scheduleMealNotification(Timestamp mealNotificationScheduledTimeStamp, String jobId,
			MealNotificationDetailsDTO mealNotificationDetailsDTO) throws SchedulerException {

		JobDetail mealNotifyJobDetail = new JobDetail();
		Job malNotifyJob = new MealNotifier();

		mealNotifyJobDetail.getJobDataMap().put(malNotifyJob.PROP_JOB_ID, jobId);
		mealNotifyJobDetail.getJobDataMap().put(malNotifyJob.PROP_JOB_GROUP, SSInternalConstants.JOB_TYPE.MEAL_NOTIFICATION);
		mealNotifyJobDetail.getJobDataMap().put(malNotifyJob.PROP_FlightId,
				String.valueOf(mealNotificationDetailsDTO.getFlightId()));
		mealNotifyJobDetail.getJobDataMap().put(malNotifyJob.PROP_DepartureStation,
				mealNotificationDetailsDTO.getDepartureStation());
		mealNotifyJobDetail.getJobDataMap().put(malNotifyJob.PROP_FLIGHT_NUMBER, mealNotificationDetailsDTO.getFlightNumber());
		mealNotifyJobDetail.setJobClass(MealNotifier.class);

		java.util.Date notificationStartTime = CommonUtil.getDateof(mealNotificationScheduledTimeStamp);

		try {
			log.info("JOB: " + jobId + "SCHEDULING MAEL_NOTIFICATION. FLIGHT NUMBER: "
					+ mealNotificationDetailsDTO.getFlightNumber() + " NOTIFICATION TIME : " + notificationStartTime);
			ScheduleManager.scheduleJob(mealNotifyJobDetail, notificationStartTime, null, 0, 0);
		} catch (SchedulerException exception) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE MEAL NOTIFICATION. POSSIBLLY NOTIFICATION TIME ALREADY SCHEDULED. ",
					exception);
		} catch (Exception e) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE MEAL NOTIFICATION. POSSIBLLY NOTIFICATION TIME ALREADY SCHEDULED. ", e);
			mealNotifyJobDetail.setRequestsRecovery(true);
		}
	}

	private class LatestFirstSortComparator implements Comparator {

		public int compare(Object first, Object second) {
			MealNotificationDetailsDTO transMissionDetailsDTO = (MealNotificationDetailsDTO) first;
			MealNotificationDetailsDTO compareTo = (MealNotificationDetailsDTO) second;

			return (transMissionDetailsDTO.getDepartureTimeZulu() == null || compareTo.getDepartureTimeZulu() == null)
					? 0
					: transMissionDetailsDTO.getDepartureTimeZulu().compareTo(compareTo.getDepartureTimeZulu()) * -1;
		}
	}

}
