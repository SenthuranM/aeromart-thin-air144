package com.isa.thinair.scheduledservices.core.client.schedrep;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

import java.sql.ResultSet;


public class OriginCountryOfCallGenerator extends BaseRowsetGenerator{

	@Override public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		ResultSet resultSet = ScheduledservicesUtils.getDataExtractionBD()
				.getOriginCountryOfCallData(reportsSearchCriteria);
		return resultSet;
	}
}
