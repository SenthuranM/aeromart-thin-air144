package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class AutoCancellationJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(AutoCancellationJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExeContext) throws JobExecutionException {
		CredentialInvokerUtil.invokeCredentials();

		AutoCancellation autoCancellation = new AutoCancellation();

		try {
			autoCancellation.execute();
		} catch (Exception e) {
			log.error("Error in executing auto cancellation", e);
			throw new JobExecutionException(e);
		}

	}

}
