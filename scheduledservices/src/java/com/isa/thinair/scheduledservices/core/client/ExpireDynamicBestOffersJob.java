package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ExpireDynamicBestOffersJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ExpireDynamicBestOffers.expireDynamicBestOffers();
		} catch (Exception me) {
			throw new JobExecutionException(me);
		}
	}

}
