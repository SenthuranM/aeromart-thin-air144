package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ExpireDynamicBestOffers {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ExpireDynamicBestOffers.class);

	public static void expireDynamicBestOffers() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getFlightInventoryResBD().removeExpiredBestOffers();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
