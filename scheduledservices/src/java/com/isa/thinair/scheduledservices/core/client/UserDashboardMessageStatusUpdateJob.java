package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * This is a scheduler job which will update existence of new dashboard messages for users if any new or unread
 * dashboard messages exist
 * 
 * @author rumesh
 */
public class UserDashboardMessageStatusUpdateJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			UserDashboardMessageStatusUpdate messageUpdater = new UserDashboardMessageStatusUpdate();
			messageUpdater.execute();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
