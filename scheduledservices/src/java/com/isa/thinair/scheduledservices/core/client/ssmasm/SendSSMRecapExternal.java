package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class SendSSMRecapExternal {
	
	private static final Log log = LogFactory.getLog(SendSSMRecapExternal.class);

	public static void execute(Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress, String sitaAddress) {	
		log.info("######### SSM Recap External Job invoked ##########");
		log.info("######### SSM Recap External Job started at " + new Date());
		try {
			ScheduledservicesUtils.getScheduleBD().publishSchedulesToExternal(scheduleStartDate, scheduleEndDate,
					isSelectedSchedules, emailAddress, sitaAddress);

		} catch (ModuleException e) {
			e.printStackTrace();
		}	
		log.info("######### SSM Recap External Job ended at " + new Date());
		
	}

}
