package com.isa.thinair.scheduledservices.core.client.rak;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class PublishRakInsuranceRecoveryJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(PublishRakInsuranceRecoveryJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
			log.info("###################### START - Recovery Job for RAK Insurace Publication [CurrentTime = "
					+ currentTimestamp + "] ######################");
			reschedulePublishingFlightDetails();
			log.info("###################### END -  Recovery Job for RAK Insurace Publication [CurrentTime = " + currentTimestamp
					+ "] ######################");
		} catch (Exception e) {
			log.error("Recovery Schedule RAK Insurance Publish failed", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void reschedulePublishingFlightDetails() throws JobExecutionException {
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		int timeFraction = 5;
		Collection<InsurancePublisherDetailDTO> failedFightSegInfo = ScheduledservicesUtils.getFlightInventoryBD()
				.getFailedInsuredFlightData();
		log.info("Found " + failedFightSegInfo.size() + " failed flight segments to be re-notified");
		for (InsurancePublisherDetailDTO insurancePublisherDTO : failedFightSegInfo) {

			insurancePublisherDTO.setRecovery(true);
			String status = insurancePublisherDTO.getStatus();
			String jobId;
			String publishType;

			if (status == null || status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SENT)) {
				jobId = "RAKPUBLISH_FIRST_RESULT" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();
				publishType = InsurancePublisherDetailDTO.PUBLISH_FIRST;
			} else if (status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_FAILED)
					|| (status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT) && insurancePublisherDTO
							.getInsResponse() == null)) {
				jobId = "RAKPUBLISH_FIRST" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();
				publishType = InsurancePublisherDetailDTO.PUBLISH_FIRST;
			} else if (status.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SENT)) {
				jobId = "RAKPUBLISH_LAST_RESULT" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();
				publishType = InsurancePublisherDetailDTO.PUBLISH_LAST;
			} else {
				jobId = "RAKPUBLISH_LAST" + "/" + insurancePublisherDTO.getFlightNumber() + "/"
						+ insurancePublisherDTO.getFlightId() + "/" + insurancePublisherDTO.getFlightSegId() + "/"
						+ insurancePublisherDTO.getDepartureTimeZulu() + "/" + insurancePublisherDTO.isRecovery();
				publishType = InsurancePublisherDetailDTO.PUBLISH_LAST;
			}

			log.info(" ---------------------------------------------------------------------------------------");
			log.info(" #####################     RAK INSURANCE RECOVERY SUB JOB SCHEDULING STARTED FOR    ######################");
			log.info(" #####################     FLIGHT NUMBER 			:     " + insurancePublisherDTO.getFlightNumber());
			log.info(" #####################     FLIGHT ID 				: 	  " + insurancePublisherDTO.getFlightId());
			log.info(" #####################     FLIGHT SEG NUMBER   	:     " + insurancePublisherDTO.getFlightSegId());
			log.info(" #####################     DEPATURE STATION 		:     " + insurancePublisherDTO.getFlightOrigin());
			log.info(" #####################     PULISH TYPE 			:     " + publishType);
			log.info(" #####################     CURRENT STATUS 		:     " + status);
			log.info(" ---------------------------------------------------------------------------------------");

			try {
				if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION_RECOVERY)) {
					log.info("Job is already scheduled. Skipping scheduling ... [JOB_ID=" + jobId + "]");
					continue;
				}

				JobDetail notificationJobDetail = new JobDetail();

				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_GROUP,
						SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightId,
						insurancePublisherDTO.getFlightId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DepartureStation,
						insurancePublisherDTO.getFlightOrigin());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_NUMBER,
						insurancePublisherDTO.getFlightNumber());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FlightSegId,
						insurancePublisherDTO.getFlightSegId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
						insurancePublisherDTO.getFlightSegmentNotificationEventId());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_LOCAL,
						insurancePublisherDTO.getDeparturetimeLocal());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_DEPARTURE_DATE_ZULU,
						insurancePublisherDTO.getDepartureTimeZulu());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_STATUS,
						insurancePublisherDTO.getStatus());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_TYPE, publishType);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFICATION_TYPE, "INS");
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_COUNT, 0);
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_RESULT,
						insurancePublisherDTO.getInsResponse());
				notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_IS_RECOVERY,
						insurancePublisherDTO.isRecovery());
				notificationJobDetail.setJobClass(PublishRakInsuranceInfo.class);

				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(insurancePublisherDTO.getDepartureTimeZulu());
				int firstPublishCutover = Integer.parseInt(AppSysParamsUtil.getRakFirstPublishCutoverTime());

				calendar.add(GregorianCalendar.MINUTE, -firstPublishCutover);
				Date notificationStartTime = calendar.getTime();

				if (notificationStartTime.before(currentTimestamp)) {
					timeFraction = timeFraction + 5;
					// TODO: Clarify how this time should be calculated
					Date adjNotificationStartTime = new Date(
							new Timestamp(currentTimestamp.getTime() + 60 * timeFraction * 1000).getTime());
					log.info("Reschedule Insurance publishing [Scheduling Delivery Passed Job JOB_ID=" + jobId
							+ ", ExpectedSendTime=" + notificationStartTime + ", AdjustedSendTime=" + adjNotificationStartTime
							+ "]");
					notificationStartTime = adjNotificationStartTime;
				}

				try {
					ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
					log.info("Reschedule Insurance Publishing [Scheduled Job JOB_ID=" + jobId + ", Notification Send Timestamp="
							+ notificationStartTime + "]");
				} catch (SchedulerException se) {
					log.error("Reschedule Insurance Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
					// FIXME - handle exception
				} catch (Exception e) {
					log.error("RescheduleInsurance Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
					notificationJobDetail.setRequestsRecovery(true);
				}
			} catch (SchedulerException e) {
				log.error("Recovery Schedule Insurance Publishing Failed", e);
				throw new JobExecutionException(e);
			}

		}
	}
}