package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Duminda G
 * @since
 */
public class EmailFlightScheduleJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			EmailFlightSchedule.execute();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
}
