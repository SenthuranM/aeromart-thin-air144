package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author IndikaA
 */
public class ResolvePartialPaymentsJob extends QuartzJobBean {

	@Override
	public void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ResolvePartialPayments.resolvePartialPayments();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}
