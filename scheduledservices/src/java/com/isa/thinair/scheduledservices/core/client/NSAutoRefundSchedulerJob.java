/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author jagath
 * 
 */
public class NSAutoRefundSchedulerJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			NSAutoRefundScheduler.procesNSAutoRefundPNRs();
		} catch (Exception re) {
			throw new JobExecutionException(re);
		}
	}

}
