package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Manoj Dhanushka
 */
public class AviatorInventoryFilePublisherJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			AviatorInventoryFilePublisher.execute();
		} catch (Exception ex) {
			throw new JobExecutionException(ex);
		}
		
	}

}
