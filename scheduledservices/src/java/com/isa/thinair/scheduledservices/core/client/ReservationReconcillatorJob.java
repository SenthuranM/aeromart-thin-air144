package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ReservationReconcillatorJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ReservationReconcillator.reconcileReservations();
		} catch (Exception re) {
			throw new JobExecutionException(re);
		}
	}
}
