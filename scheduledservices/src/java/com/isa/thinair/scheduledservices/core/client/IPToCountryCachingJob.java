package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * export IP to Country details in caching DB
 * 
 * @author rumesh
 * 
 */
public class IPToCountryCachingJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(IPToCountryCachingJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg) throws JobExecutionException {
		try {
			if (AppSysParamsUtil.isPosWebFaresEnabled() && CommonsServices.getAerospikeCachingService().isCachingDBEnabled()) {
				CachingServicegenerator.exportIpToCountryData();
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Export ip to country job failed", e);
			}
			throw new JobExecutionException(e);
		}

	}

}
