package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Scheduled Report DownLoader
 * 
 * @author Navod Ediriweera
 * 
 */
public abstract class BaseReportDownloadGenerator extends ReportGeneratorCommon {
	protected Log log = LogFactory.getLog(BaseReportDownloadGenerator.class);
	private static final String ISBOOLEAN_STR = "true";

	protected String isDownloadReport;
	protected FTPUtil ftpUtil;
	// Local file location to down load the file from FTP
	protected String localFileLocation = PlatformConstants.getConfigRootAbsPath() + "/templates/";
	// Location of FTP file in the FTP server
	protected String ftpFileLocation = AppSysParamsUtil.getFTPLocationForRevenueReports();
	protected String ftpServerName;
	protected String ftpServerUserName;
	protected String ftpPassword;

	protected int threshHoldMax = 16; // 8
										// hours
	protected int thread_sleep_time;

	// Name of the generated report
	protected String generatedReportFileName;
	// Condition to check if a .log or a similar file has to be present before
	// downloading the actual file
	protected Boolean doVerifyFileCheck;
	// name of the generated file in the FTP location
	protected String generaedFileNameInFTPLocation;
	// name of the generated log file in the ftp location
	protected String generatedFileNameLogInFTPLocation;
	protected String generatedFileFormat;
	protected String generatedFileNamePrefixInProc;

	protected void setUpAppParamValuesForFTP() {
		String serverLoginDetailsFromApp[] = AppSysParamsUtil.getFTPProcGeneratedServerLoginDetails().split(",");
		ftpServerName = serverLoginDetailsFromApp[0];
		ftpServerUserName = serverLoginDetailsFromApp[1];
		ftpPassword = serverLoginDetailsFromApp[2];
	}

	@Override
	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {
		try {
			generatedReportFileName = this.generateUniqueID();
			ReportsSearchCriteria reportsSearchCriteria = this.extractReportData(scheduledReport);
			// Executing ResultSet
			this.executeResultSetExtraction(reportsSearchCriteria);
			this.setFileNamesForGenerated();
			// if report should not be downloaded -- Return the url directly
			if (ISBOOLEAN_STR.equalsIgnoreCase(isDownloadReport)) {
				return this.doFTPDownLoad();
			} else {
				return AppSysParamsUtil.getReportURLPRefix() + generatedFileNamePrefixInProc + generatedReportFileName
						+ generatedFileFormat;
			}
		} finally {

			if (ftpUtil != null) {
				try {
					if (ftpUtil.isConnected()) {
						ftpUtil.disconnect();
					}
				} catch (Exception e) {
					if (log.isErrorEnabled())
						log.error("Error Disconecting from FTP [" + e.getMessage() + "]", e);
					throw new ModuleException("Error Disconecting from FTP");
				}
			}
		}
	}

	private String doFTPDownLoad() throws ModuleException {
		this.setUpAppParamValuesForFTP();
		ftpUtil = this.connectAndLoginToFTP();
		String localFilePathForDownLoaded = localFileLocation + generatedFileNamePrefixInProc + generatedReportFileName
				+ generatedFileFormat;

		// Checking for log file to verify actual file is finished in the
		// FTP location
		boolean isPresent = false;
		if (doVerifyFileCheck) {
			String localFilePathForLog = localFileLocation + generatedReportFileName;
			isPresent = checkForGenerateCompletion(generatedFileNameLogInFTPLocation, localFilePathForLog);
		} else
			isPresent = true;
		// IF verification is not present-->Assuming the file is created
		// downloading actual file.
		boolean isDownloaded = false;

		if (isPresent) {
			isDownloaded = downLoadActualReportFromFTPLocation(generaedFileNameInFTPLocation, localFilePathForDownLoaded);
		}

		if (isDownloaded) {
			return AppSysParamsUtil.getReportURLPRefix() + localFilePathForDownLoaded;
		}
		return null;
	}

	protected FTPUtil connectAndLoginToFTP() throws ModuleException {
		FTPServerProperty serverProperty = this.setUpFTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();
		try {
			if (!ftpUtil.isConnected()) {
				if (ftpUtil.connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(),
						serverProperty.getPassword())) {
					ftpUtil.binary();
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled())
				log.error("Connecting to FTP Failed [" + e.getMessage() + "]", e);
			throw new ModuleException("Connecting to FTP Failed");
		}
		return ftpUtil;
	}

	protected boolean downLoadActualReportFromFTPLocation(String fileName, String localFile) throws ModuleException {
		boolean downloaded = false;
		try {
			ftpUtil = this.connectAndLoginToFTP();
			downloaded = ftpUtil.downloadFile(fileName, localFile);
			if (downloaded) {
				downloaded = new File(localFile).exists();
			}
		} catch (Exception e) {
			if (log.isErrorEnabled())
				log.error("Error Downloading Generated  file", e);
			throw new ModuleException("Error Downloading Generated ");
		}
		return downloaded;
	}

	protected boolean checkForGenerateCompletion(String serverFile, String localFile) throws ModuleException {

		boolean isPresent = false;
		int threshholdCount = 0;
		boolean isThreshholdPass = true;
		try {
			ftpUtil = this.connectAndLoginToFTP();
			isPresent = ftpUtil.downloadFile(serverFile, localFile);
			while (!isPresent && isThreshholdPass) {
				Thread.sleep(thread_sleep_time);
				isPresent = ftpUtil.downloadFile(serverFile, localFile);
				threshholdCount++;
				if (!isPresent && threshholdCount > threshHoldMax) {// greater
																	// than 8
																	// hours
					isThreshholdPass = false;
					if (log.isInfoEnabled())
						log.info("Revenue Report Generation--> Exit --> Was Waiting for More than 8 hours");
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled())
				log.error("Error Checking Generated Log completion file", e);
			throw new ModuleException("Error Checking Generated Log completion file");
		}
		return isPresent;
	}

	protected ReportsSearchCriteria extractReportData(ScheduledReport scheduledReport) {
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream.fromXML(scheduledReport
				.getCriteriaTemplete());
		return reportsSearchCriteria;
	}

	protected FTPServerProperty setUpFTPServerProperty() {
		FTPServerProperty serverProperty = new FTPServerProperty();
		// Code to FTP
		serverProperty.setServerName(ftpServerName);
		if (this.ftpServerUserName == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(this.ftpServerUserName);
		}
		if (this.ftpPassword == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(this.ftpPassword);
		}
		return serverProperty;
	}

	protected abstract void executeResultSetExtraction(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException;

	protected abstract void setFileNamesForGenerated();

	public String getIsDownloadReport() {
		return isDownloadReport;
	}

	public void setIsDownloadReport(String isDownloadReport) {
		this.isDownloadReport = isDownloadReport;
	}

	public String getGeneratedFileFormat() {
		return generatedFileFormat;
	}

	public void setGeneratedFileFormat(String generatedFileFormat) {
		this.generatedFileFormat = generatedFileFormat;
	}

	public int getThread_sleep_time() {
		return thread_sleep_time;
	}

	public void setThread_sleep_time(int thread_sleep_time) {
		this.thread_sleep_time = thread_sleep_time;
	}

	public String getGeneratedFileNamePrefixInProc() {
		return generatedFileNamePrefixInProc;
	}

	public void setGeneratedFileNamePrefixInProc(String generatedFileNamePrefixInProc) {
		this.generatedFileNamePrefixInProc = generatedFileNamePrefixInProc;
	}

	public String getLocalFileLocation() {
		return localFileLocation;
	}

	public void setLocalFileLocation(String localFileLocation) {
		this.localFileLocation = localFileLocation;
	}

	public int getThreshHoldMax() {
		return threshHoldMax;
	}

	public void setThreshHoldMax(int threshHoldMax) {
		this.threshHoldMax = threshHoldMax;
	}

	public String getGeneratedReportFileName() {
		return generatedReportFileName;
	}

	public void setGeneratedReportFileName(String generatedReportFileName) {
		this.generatedReportFileName = generatedReportFileName;
	}

	public String getGeneraedFileNameInFTPLocation() {
		return generaedFileNameInFTPLocation;
	}

	public void setGeneraedFileNameInFTPLocation(String generaedFileNameInFTPLocation) {
		this.generaedFileNameInFTPLocation = generaedFileNameInFTPLocation;
	}

	public String getGeneratedFileNameLogInFTPLocation() {
		return generatedFileNameLogInFTPLocation;
	}

	public void setGeneratedFileNameLogInFTPLocation(String generatedFileNameLogInFTPLocation) {
		this.generatedFileNameLogInFTPLocation = generatedFileNameLogInFTPLocation;
	}

}
