package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class GdsMessagesRetrieverJob
		extends QuartzJobBean
{
	
	private static Log log = LogFactory.getLog(GdsMessagesRetrieverJob.class);

//	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		execute();
	}

	public void execute() {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getMessageBrokerServiceBD().receiveMessage(true);
			log.error("---- Retrieving GDS Msgs" + new Date());
		} catch (Exception e) {
			log.error("Error while Retrieving GDS Msgs" + e);
		}
	}

}
