package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class InMessageSequentialProcessingJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(InMessageSequentialProcessingJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		log.info("######### InMessageSequentialProcessingJob  invoked ##########");
		log.info("######### InMessageSequentialProcessingJob started at " + new Date());

		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getMessageBrokerServiceBD().processMessagesSequentially();

		} catch (Exception e) {
			log.error("Error @ InMessageSequentialProcessingJob " + e.getMessage());
		}
		log.info("######### InMessageSequentialProcessingJob ended at " + new Date());

	}
}
