package com.isa.thinair.scheduledservices.core.service;



import java.util.Date;

import junit.framework.TestCase;

import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.scheduledservices.core.client.CashSalesPoster;
import com.isa.thinair.scheduledservices.core.client.ResolvePartialPayments;


public class TestScheduledservicesUtils extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void jtestTransferCashSales() throws ModuleException
	{
		
	//Date today=new Date();
	//	LookupServiceBD().transferCashSales(today);
	CashSalesPoster poster=new CashSalesPoster();
	Date  today=new Date();
	poster.transferCashSales(today);
	}
	
	
	public ReservationAuxilliaryBD LookupServiceBD(){
		//MessagingServiceBD messagingServiceBD = (MessagingServiceBD)ScheduledservicesUtils.lookupServiceBD("messaging","messaging.bdimpl");
		//assertNotNull(messagingServiceBD);
		
		ReservationAuxilliaryBD resauuxBD = (ReservationAuxilliaryBD)ScheduledservicesUtils.lookupServiceBD("airreservation", "reservationauxilliary.service");
	return resauuxBD;
	}
	
	public void testResolvePartialPayments() throws com.isa.thinair.commons.api.exception.ModuleException{
		System.out.println("Start");
		ResolvePartialPayments oNew = new ResolvePartialPayments();
		assertTrue(oNew.resolvePartialPayments());
		System.out.println("OK");
	}
	
}
