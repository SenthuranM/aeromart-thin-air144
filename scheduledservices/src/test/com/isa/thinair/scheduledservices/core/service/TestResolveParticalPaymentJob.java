package com.isa.thinair.scheduledservices.core.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.scheduledservices.core.client.ResolvePartialPayments;

public class TestResolveParticalPaymentJob extends PlatformTestCase {
	
	protected void setUp() throws Exception {
		System.setProperty("repository.home", "/usr/isa/aares/config-test");
		super.setUp();
	}
	
	public void testResolvePartialPayments(){
		ResolvePartialPayments resolvePartialPayments = new ResolvePartialPayments();
		try {
			resolvePartialPayments.resolvePartialPayments();
		} catch (ModuleException e) {
			e.printStackTrace();
		}
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
