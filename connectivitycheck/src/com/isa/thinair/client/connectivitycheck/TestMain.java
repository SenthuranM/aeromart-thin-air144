/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck;

import com.isa.thinair.client.connectivitycheck.core.TestReport;
import com.isa.thinair.client.connectivitycheck.tests.TestSuite;

/**
 *
 * @author dilan
 */
public class TestMain {

    public static void main(String args[]) {
        TestSuite s = null;
        try {
            s = new TestSuite();
        } catch (Exception ex) {
            System.out.println("System is not configured property! Check configuration");
        }
        TestReport report = s.run();
        System.out.println(report.toString());

    }
}
