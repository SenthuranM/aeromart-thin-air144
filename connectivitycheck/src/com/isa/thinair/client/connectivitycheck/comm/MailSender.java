/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.isa.thinair.client.connectivitycheck.comm;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author danuruddha
 */
public class MailSender {

    public enum MailStatus {ERROR,NORMAL};

    private String FROM_EMAIL = "danuruddha@isaaviation.ae";
    private String ERROR_EMAIL = "mjirjees@isaaviation.ae";
    private String NORMAL_EMAIL = "mjirjees@isaaviation.ae";

    private String SMTP_HOST = "smtp.isaaviation.ae";
    private String SMTP_AUTH = "true";
    private String SMTP_USER = "danuruddha@isaaviation.ae";
    private String SMTP_PASS = "j30oSY8x";

    

    public void sendMail(String subject, String body,MailStatus status){
        try {
			InternetAddress toAddress =null;

            if(status == MailStatus.ERROR){
                toAddress = new InternetAddress(
					ERROR_EMAIL, ERROR_EMAIL);
            }else{
                toAddress = new InternetAddress(NORMAL_EMAIL,NORMAL_EMAIL);
            }
			InternetAddress fromAddress = new InternetAddress(
					FROM_EMAIL, FROM_EMAIL);

			Properties props = new Properties();
			props.put("mail.smtp.host", SMTP_HOST);
			props.put("mail.smtp.auth", SMTP_AUTH);
			props.put("mail.smtp.user", SMTP_USER);
			props.put("mail.smtp.password", SMTP_PASS);
			Session mailsession = Session.getDefaultInstance(props,
					new SimpleSmtpAuthenticator(SMTP_USER,SMTP_PASS));
			Message msg = new MimeMessage(mailsession);
			msg.addRecipient(Message.RecipientType.TO, toAddress);
			msg.setSubject(subject);
			msg.setFrom(fromAddress);
			msg.setText(body);
			Transport.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
