/*
 * AgentView.java
 */
package com.isa.thinair.client.connectivitycheck;

import com.isa.thinair.client.connectivitycheck.dao.FixedConfig;
import com.isa.thinair.client.connectivitycheck.core.TestReport;
import com.isa.thinair.client.connectivitycheck.tests.TestSuite;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;

import java.awt.AWTException;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The application's main frame.
 */
public class AgentView extends FrameView {

    public AgentView(SingleFrameApplication app) {
        super(app);
        getFrame().setAlwaysOnTop(true);


        initComponents();
        getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        getFrame().setResizable(false);
        getFrame().addWindowListener(new WindowListener() {

            public void windowOpened(WindowEvent e) {
            }

            public void windowClosing(WindowEvent e) {
            }

            public void windowClosed(WindowEvent e) {
            }

            public void windowIconified(WindowEvent e) {
                toSysTray();

            }

            public void windowDeiconified(WindowEvent e) {
            }

            public void windowActivated(WindowEvent e) {
            }

            public void windowDeactivated(WindowEvent e) {
            }
        });

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });

        int delay = new FixedConfig().getCheckInterval();
        java.util.Timer timer = new java.util.Timer();
        timer.schedule(new TimerTask() {

            public void run() {
                serviceCheckDaemon();
            }
        }, new Date(), delay);

    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = AgentApp.getApplication().getMainFrame();
            aboutBox = new AgentAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        AgentApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        panelStatReport = new javax.swing.JPanel();
        lblDate = new javax.swing.JLabel();
        lblConnectionSpeed = new javax.swing.JLabel();
        lblConnStat = new javax.swing.JLabel();
        btnCheck = new javax.swing.JButton();
        lblDateInput = new javax.swing.JLabel();
        lblConnStatInput = new javax.swing.JLabel();
        lblConnectionSpeedInput = new javax.swing.JLabel();
        lblAppStatus = new javax.swing.JLabel();
        lblAppStatusInput = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        settingsMenuItem = new javax.swing.JMenuItem();
        proxyMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setName("mainPanel"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.isa.thinair.client.connectivitycheck.AgentApp.class).getContext().getResourceMap(AgentView.class);
        panelStatReport.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("statusPanel.border.title"))); // NOI18N
        panelStatReport.setName("statusPanel"); // NOI18N

        lblDate.setText(resourceMap.getString("lblDate.text")); // NOI18N
        lblDate.setName("lblDate"); // NOI18N

        lblConnectionSpeed.setText(resourceMap.getString("lblConnectionSpeed.text")); // NOI18N
        lblConnectionSpeed.setName("lblConnectionSpeed"); // NOI18N

        lblConnStat.setText(resourceMap.getString("lblConnStat.text")); // NOI18N
        lblConnStat.setName("lblConnStat"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.isa.thinair.client.connectivitycheck.AgentApp.class).getContext().getActionMap(AgentView.class, this);
        btnCheck.setAction(actionMap.get("checkServiceClicked")); // NOI18N
        btnCheck.setText(resourceMap.getString("btnCheck.text")); // NOI18N
        btnCheck.setName("btnCheck"); // NOI18N

        lblDateInput.setText(resourceMap.getString("lblDateInput.text")); // NOI18N
        lblDateInput.setName("lblDateInput"); // NOI18N

        lblConnStatInput.setText(resourceMap.getString("lblConnStatInput.text")); // NOI18N
        lblConnStatInput.setName("lblConnStatInput"); // NOI18N

        lblConnectionSpeedInput.setText(resourceMap.getString("lblConnectionSpeedInput.text")); // NOI18N
        lblConnectionSpeedInput.setName("lblConnectionSpeedInput"); // NOI18N

        lblAppStatus.setText(resourceMap.getString("lblAppStatus.text")); // NOI18N
        lblAppStatus.setName("lblAppStatus"); // NOI18N

        lblAppStatusInput.setText(resourceMap.getString("lblAppStatusInput.text")); // NOI18N
        lblAppStatusInput.setName("lblAppStatusInput"); // NOI18N

        javax.swing.GroupLayout panelStatReportLayout = new javax.swing.GroupLayout(panelStatReport);
        panelStatReport.setLayout(panelStatReportLayout);
        panelStatReportLayout.setHorizontalGroup(
            panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatReportLayout.createSequentialGroup()
                .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStatReportLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDate, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblConnStat, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblConnectionSpeed, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblAppStatus, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAppStatusInput)
                            .addComponent(lblConnectionSpeedInput, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblConnStatInput, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDateInput, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelStatReportLayout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(btnCheck)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        panelStatReportLayout.setVerticalGroup(
            panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStatReportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDate)
                    .addComponent(lblDateInput))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConnStat)
                    .addComponent(lblConnStatInput))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConnectionSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblConnectionSpeedInput))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelStatReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAppStatus)
                    .addComponent(lblAppStatusInput))
                .addGap(54, 54, 54)
                .addComponent(btnCheck)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStatReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelStatReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        settingsMenuItem.setAction(actionMap.get("showSettings")); // NOI18N
        settingsMenuItem.setText(resourceMap.getString("settingsMenuItem.text")); // NOI18N
        settingsMenuItem.setName("settingsMenuItem"); // NOI18N
        settingsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(settingsMenuItem);

        proxyMenuItem.setAction(actionMap.get("showProxyConfig")); // NOI18N
        proxyMenuItem.setText(resourceMap.getString("proxyMenuItem.text")); // NOI18N
        proxyMenuItem.setName("proxyMenuItem"); // NOI18N
        fileMenu.add(proxyMenuItem);

        exitMenuItem.setAction(actionMap.get("toSysTray")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 210, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void settingsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingsMenuItemActionPerformed
    }//GEN-LAST:event_settingsMenuItemActionPerformed
    private Task checkTask = null;

    private void serviceCheckDaemon() {
        try {
            TestSuite testSuite = new TestSuite();
            TestReport report = testSuite.run();
            lblAppStatusInput.setText(report.getAgentStatus());
            lblConnStatInput.setText(report.getConnectionStatus());
            lblConnectionSpeedInput.setText(report.getSpeed());
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            lblDateInput.setText(dateFormat.format(date));

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());

            JOptionPane.showMessageDialog(getFrame(), "Running tests failed! Check your settings", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    @Action
    public Task checkServiceClicked() {
        if (checkTask == null) {
            System.out.println("No task");
            checkTask = new CheckServiceClickedTask(getApplication());
            btnCheck.setText("Stop Check");
        } else {
            System.out.println("Existing task");
            checkTask.cancel(true);
            checkTask = null;
            btnCheck.setText("Check Status");
        }
        return checkTask;
    }

    private class CheckServiceClickedTask extends org.jdesktop.application.Task<Object, Void> {

        CheckServiceClickedTask(org.jdesktop.application.Application app) {
            // Runs on the EDT.  Copy GUI state that
            // doInBackground() depends on from parameters
            // to CheckServiceClickedTask fields, here.
            super(app);
        }

        @Override
        protected Object doInBackground() {
            clearStatusInput();
            System.out.println("Running tests");
            TestSuite testSuite = null;
            try {
                testSuite = new TestSuite();
                TestReport report = testSuite.run();
                lblAppStatusInput.setText(report.getAgentStatus());
                lblConnStatInput.setText(report.getConnectionStatus());
                lblConnectionSpeedInput.setText(report.getSpeed());
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                lblDateInput.setText(dateFormat.format(report.getDate()));

            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(getFrame(), "Running tests failed! Check your settings", "Error", JOptionPane.ERROR_MESSAGE);
            }
            return null;  // return your result
        }

        @Override
        protected void succeeded(Object result) {
            checkTask = null;
            btnCheck.setText("Check Status");
        }
    }

    private void clearStatusInput() {
        this.lblConnStatInput.setText("--");
        this.lblConnectionSpeedInput.setText("--");
        this.lblAppStatusInput.setText("--");
        this.lblDateInput.setText("--");
    }

    @Action
    public void toSysTray() {
        getFrame().setVisible(false);
        Runnable runner = new Runnable() {

            SystemTray tray;
            TrayIcon trayIcon;

            public void run() {
                if (SystemTray.isSupported()) {
                    tray = SystemTray.getSystemTray();
                    Image image = Toolkit.getDefaultToolkit().getImage(StaticFileNameUtil.getCorrected("grabber.gif"));
                    PopupMenu popup = new PopupMenu();

                    MenuItem item = new MenuItem("AccelAero Agent");

                    popup.add(item);
                    trayIcon = new TrayIcon(image, "AccelAero Monitoring Tool", popup);
                    item.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            tray.remove(trayIcon);
                            getFrame().setVisible(true);
                            getFrame().setState(JFrame.NORMAL);
                        }
                    });
                    trayIcon.addMouseListener(new MouseListener() {

                        public void mouseClicked(MouseEvent e) {
                            tray.remove(trayIcon);
                            getFrame().setVisible(true);
                            getFrame().setState(JFrame.NORMAL);
                        }

                        public void mousePressed(MouseEvent e) {
                            //throw new UnsupportedOperationException("Not supported yet.");
                        }

                        public void mouseReleased(MouseEvent e) {
                            //throw new UnsupportedOperationException("Not supported yet.");
                        }

                        public void mouseEntered(MouseEvent e) {
                            // throw new UnsupportedOperationException("Not supported yet.");
                        }

                        public void mouseExited(MouseEvent e) {
                            //throw new UnsupportedOperationException("Not supported yet.");
                        }
                    });
                    trayIcon.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            System.out.println(e.getActionCommand());
                        }
                    });
                    try {
                        tray.add(trayIcon);
                    } catch (AWTException ew) {
                        System.err.println("Can't add to tray");
                    }
                } else {
                    System.err.println("Tray unavailable");
                }
            }
        };

        EventQueue.invokeLater(runner);

    }

    @Action
    public void showSettings() {
        JDialog d = new JDialog(getFrame(), "Settings");
        SettingsPanel p = new SettingsPanel(d);
        d.getContentPane().add(p);
        d.pack();
        d.setModal(false);
        d.setVisible(true);
        JFrame mainFrame = AgentApp.getApplication().getMainFrame();
        d.setLocationRelativeTo(mainFrame);
    }

    @Action
    public void showProxyConfig() {
        JDialog d = new JDialog(getFrame(), "Proxy Configuration");
        ProxyPanel p = new ProxyPanel(d);
        d.getContentPane().add(p);
        d.pack();
        d.setModal(false);
        d.setVisible(true);
        JFrame mainFrame = AgentApp.getApplication().getMainFrame();
        d.setLocationRelativeTo(mainFrame);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheck;
    private javax.swing.JLabel lblAppStatus;
    private javax.swing.JLabel lblAppStatusInput;
    private javax.swing.JLabel lblConnStat;
    private javax.swing.JLabel lblConnStatInput;
    private javax.swing.JLabel lblConnectionSpeed;
    private javax.swing.JLabel lblConnectionSpeedInput;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDateInput;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel panelStatReport;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JMenuItem proxyMenuItem;
    private javax.swing.JMenuItem settingsMenuItem;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
