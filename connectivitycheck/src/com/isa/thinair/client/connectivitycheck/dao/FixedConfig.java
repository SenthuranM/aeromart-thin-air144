/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.dao;

import com.isa.thinair.client.connectivitycheck.core.AirlineApplication;
import com.isa.thinair.client.connectivitycheck.core.URLAccount;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author danuruddha
 */
public class FixedConfig {

    private double SPEED_IMAGE_SIZE = 7.31; /*in kilo bytes*/

    private int CHECK_INTERVAL = 1000 * 60 * 15; /* 5 mins -  in mili seconds */

    private String LOG_FILE = "agent.log";
    private String[] countries = {
        "Sri Lanka",
        "UAE",
        "India",
        "Pakistan",
        "Iraq",
        "UK",
        "Russia",
        "Kenya"
    };
    private Map<String, AirlineApplication> appStore = new HashMap<String, AirlineApplication>();

    public FixedConfig() {
        /**
         * first parameter should be the airline
         * Airline application can have upto 3 url accounts one each for agent, cc, ibe
         * Each url account can have login url, targert url, username and password
         * If username and password is not set default username and password specified in URLAccount will be taken instead
         */
        appStore.put("G9", new AirlineApplication("https://airarabia29.isaaviations.com:8443", new URLAccount(URLAccount.SYSTEM_TYPE.ADMIN, "SYSTEM", "G9December09"), new URLAccount(URLAccount.SYSTEM_TYPE.XBE, "SYSTEM", "G9December09")));
        appStore.put("MA", new AirlineApplication("https://airarabiama.isaaviations.com:8443", new URLAccount(URLAccount.SYSTEM_TYPE.ADMIN, "SYSTEM", "password")));



    }

    public String[] getCountries() {
        return countries;
    }

    public String[] getAirlines() {
        Set<String> airlineSet = appStore.keySet();
        String[] airlines = new String[airlineSet.size()];
        return airlineSet.toArray(airlines);
    }

    public double getSpeedImageSize() {
        return SPEED_IMAGE_SIZE;
    }

    public AirlineApplication getAirlineApplication(String airline) {
        return appStore.get(airline);
    }

    public int getCheckInterval() {
        return CHECK_INTERVAL;
    }

    public String getLogFile() {
        return LOG_FILE;
    }
}
