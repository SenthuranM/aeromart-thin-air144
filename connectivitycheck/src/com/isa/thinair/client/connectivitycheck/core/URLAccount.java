/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.core;

import com.isa.thinair.commons.core.util.StaticFileNameUtil;

/**
 *
 * @author dilan
 */
public class URLAccount {

    private final static String ADMIN_LOGIN = "/airadmin/public/master/../public/j_security_check";
    private final static String ADMIN_MAIN = "/airadmin/private/showXBEMain.action";
    private final static String XBE_LOGIN = "/xbe/public/master/../public/j_security_check";
    private final static String XBE_MAIN = "/xbe/private/showXBEMain.action";
    private final static String CC_LOGIN = "/xbe/public/master/../public/j_security_check";
    private final static String CC_MAIN = "/xbe/private/showXBEMain.action";
    private final static String IBE_MAIN = "/ibe/js/indexDummy_GA.jsp";
    private final static String SPEED_IMAGE = StaticFileNameUtil.getCorrected("/xbe/images/AA110_3.jpg");

    public enum SYSTEM_TYPE {

        XBE, CC, ADMIN, IBE, SPEED_IMAGE
    }
    private String DEF_USERNAME = "default_user";
    private String DEF_PASSWORD = "default_password";
    private String prefix = null;
    private String username;
    private String password;
    private boolean enabled;
    private SYSTEM_TYPE sysType;

    public URLAccount() {
        enabled = false;
    }

    public URLAccount(SYSTEM_TYPE type) {
        this.sysType = type;
        this.username = DEF_USERNAME;
        this.password = DEF_PASSWORD;
        enabled = true;
    }

    public URLAccount(SYSTEM_TYPE type, String username, String password) {
        this.sysType = type;
        if (username == null || username.equals("")) {
            throw new IllegalArgumentException("Username cannot be null or empty");
        }

        if (password == null || password.equals("")) {
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
        this.username = username;
        this.password = password;
        enabled = true;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getPassword() {
        return password;
    }

    public String getLoginUrl() {
        StringBuffer urlBuff = new StringBuffer();
        if (prefix != null) {
            urlBuff.append(prefix);
        }
        switch (sysType) {
            case ADMIN:
                urlBuff.append(ADMIN_LOGIN);
                break;
            case XBE:
                urlBuff.append(XBE_LOGIN);
                break;
            case CC:
                urlBuff.append(CC_LOGIN);
                break;
            case IBE:
                urlBuff.append(IBE_MAIN);
                break;
            case SPEED_IMAGE:
                urlBuff.append(SPEED_IMAGE);
                break;
            default:
                return null;
        }
        return urlBuff.toString();

    }

    public String getTargetUrl() {
        StringBuffer urlBuff = new StringBuffer();
        if (prefix != null) {
            urlBuff.append(prefix);
        }
        switch (sysType) {
            case ADMIN:
                urlBuff.append(ADMIN_MAIN);
                break;
            case XBE:
                urlBuff.append(XBE_MAIN);
                break;
            case CC:
                urlBuff.append(CC_MAIN);
                break;
            case IBE:
                urlBuff.append(IBE_MAIN);
                break;
            case SPEED_IMAGE:
                urlBuff.append(SPEED_IMAGE);
                break;
            default:
                return null;
        }
        return urlBuff.toString();
    }

    public String getUsername() {
        return username;
    }

    public SYSTEM_TYPE getSystemType() {
        return sysType;
    }
}
