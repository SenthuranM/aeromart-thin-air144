/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.core;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dilan
 */
public class TestReport {

    private Date date;
    private boolean speedChecked;
    private String speed = "n/a";
    private boolean agentChecked;
    private String agentStatus = "n/a";
    private boolean callCenterChecked;
    private String callCenterStatus = "n/a";
    private boolean ibeChecked;
    private String ibeStatus = "n/a";
    private boolean connectionChecked;
    private String connectionStatus = "n/a";
    private Settings settings;
    private ProxyConfig proxyConfig;
    private AirlineApplication airlineApp;

    public boolean isAgentChecked() {
        return agentChecked;
    }

    public void setAgentChecked(boolean agentChecked) {
        this.agentChecked = agentChecked;
    }

    public String getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(String agentStatus) {
        this.agentStatus = agentStatus;
    }

    public AirlineApplication getAirlineApp() {
        return airlineApp;
    }

    public void setAirlineApp(AirlineApplication airlineApp) {
        this.airlineApp = airlineApp;
    }

    public boolean isCallCenterChecked() {
        return callCenterChecked;
    }

    public void setCallCenterChecked(boolean callCenterChecked) {
        this.callCenterChecked = callCenterChecked;
    }

    public String getCallCenterStatus() {
        return callCenterStatus;
    }

    public void setCallCenterStatus(String callCenterStatus) {
        this.callCenterStatus = callCenterStatus;
    }

    public boolean isConnectionChecked() {
        return connectionChecked;
    }

    public void setConnectionChecked(boolean connectionChecked) {
        this.connectionChecked = connectionChecked;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isIbeChecked() {
        return ibeChecked;
    }

    public void setIbeChecked(boolean ibeChecked) {
        this.ibeChecked = ibeChecked;
    }

    public String getIbeStatus() {
        return ibeStatus;
    }

    public void setIbeStatus(String ibeStatus) {
        this.ibeStatus = ibeStatus;
    }

    public ProxyConfig getProxyConfig() {
        return proxyConfig;
    }

    public void setProxyConfig(ProxyConfig proxyConfig) {
        this.proxyConfig = proxyConfig;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public boolean isSpeedChecked() {
        return speedChecked;
    }

    public void setSpeedChecked(boolean speedChecked) {
        this.speedChecked = speedChecked;
    }

    public String toMail() {
        StringBuffer buff = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
        buff.append("Date         \t\t: " + sdf.format(getDate()) + "\n");
        buff.append("Airline      \t\t: " + settings.getAirline() + "\n");
        buff.append("Agent        \t\t: " + settings.getAgentCode() + "\n");
        buff.append("Station      \t\t: " + settings.getStation() + "\n");
        buff.append("Location     \t\t: " + settings.getLocation() + "\n");
        buff.append("Country      \t\t: " + settings.getCountry() + "\n");
        if (proxyConfig.isNoProxy()) {
            buff.append("Proxy        \t\t: No Proxy\n");
        } else {
            buff.append("Proxy Host  \t\t: " + proxyConfig.getProxyHost() + "\n");
            buff.append("Proxy Port  \t\t: " + proxyConfig.getProxyPort() + "\n");
            if (proxyConfig.isAuthRequired()) {
                buff.append("Proxy Authenticated\t\t: Yes\n");
            } else {
                buff.append("Proxy Host      \t\t: No\n");
            }
        }
        buff.append("Connection status  \t\t: " + getConnectionStatus() + "\n");
        buff.append("Speed              \t\t: " + getSpeed() + "\n");
        buff.append("Agent App          \t\t: " + getAgentStatus() + "\n");
        buff.append("Call Center App    \t\t: " + getCallCenterStatus() + "\n");
        buff.append("IBE App            \t\t: " + getIbeStatus() + "\n");

        return buff.toString();
    }

    @Override
    public String toString() {
        return toMail();
    }

    public String toLog() {
        StringBuffer buff = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
        buff.append(sdf.format(date) + ",");
        buff.append(settings.getAirline() + ",");
        buff.append(settings.getAgentCode() + ",");
        buff.append(settings.getCountry() + ",");
        buff.append(settings.getStation() + ",");
        buff.append(settings.getLocation() + ",");
        buff.append("agent:" + isAgentChecked() + ";" + getAgentStatus() + ",");
        buff.append("cc:" + isCallCenterChecked() + ";" + getCallCenterStatus() + ",");
        buff.append("ibe:" + isIbeChecked() + ";" + getIbeStatus() + ",");
        buff.append("speed:" + isSpeedChecked() + ";" + getSpeed() + ",");
        buff.append("conn:" + isConnectionChecked() + ";" + getConnectionStatus() + ",");
        buff.append("proxy:" + (proxyConfig.isNoProxy() ? "No proxy" : "Proxy Configured") + ";" + (proxyConfig.isAuthRequired() ? "With Authentication" : "Without Authentication"));
        return buff.toString();
    }

    public String toWeb() {
        return toMail();
    }
}
