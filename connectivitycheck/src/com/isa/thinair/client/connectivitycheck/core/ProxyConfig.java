/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.core;

import java.io.Serializable;

/**
 *
 * @author dilan
 */
public class ProxyConfig implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 517626092731071835L;
	private boolean noProxy = true;
    private String proxyHost;
    private int proxyPort;
    private boolean authRequired;
    private String username;
    private String password;

    public boolean isAuthRequired() {
        return authRequired;
    }

    public void setAuthRequired(boolean authRequired) {
        this.authRequired = authRequired;
    }

    public boolean isNoProxy() {
        return noProxy;
    }

    public void setNoProxy(boolean noProxy) {
        this.noProxy = noProxy;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
