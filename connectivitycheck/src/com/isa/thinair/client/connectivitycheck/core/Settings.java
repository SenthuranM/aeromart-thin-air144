/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.isa.thinair.client.connectivitycheck.core;

import java.io.Serializable;

/**
 *
 * @author danuruddha
 */
public class Settings implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8153956267391306938L;
	private String airline;
    private String country;
    private String station;
    private String location;
    private String agentCode;

    public String getAirline() {
        return airline;
    }

    public String getCountry() {
        return country;
    }

    public String getLocation() {
        return location;
    }

    public String getStation() {
        return station;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    
}
