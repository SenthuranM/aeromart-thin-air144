/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isa.thinair.client.connectivitycheck.tests;

import com.isa.thinair.client.connectivitycheck.comm.NotificationStub;
import com.isa.thinair.client.connectivitycheck.dao.FixedConfig;
import com.isa.thinair.client.connectivitycheck.dao.Persistance;
import com.isa.thinair.client.connectivitycheck.core.AirlineApplication;
import com.isa.thinair.client.connectivitycheck.core.ProxyConfig;
import com.isa.thinair.client.connectivitycheck.core.Settings;
import com.isa.thinair.client.connectivitycheck.core.TestReport;
import com.isa.thinair.client.connectivitycheck.core.URLAccount;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

/**
 *
 * @author danuruddha
 */
public class TestSuite {

    private final int MAX_RETRIES = 1;
    private final String STAT_OK = "OK";
    private final String STAT_ERROR = "ERROR";
    private final String STAT_CONN_ERROR = "CONN. ERROR";
    private final String TEST_SKIPPED = "SKIPPED";
    private final String TEST_FAILED = "FAILED";
    private final String BLANK = "--";
    private Settings settings = null;
    private ProxyConfig proxyConfig = null;
    private FixedConfig fixedConfig = null;
    private AirlineApplication airlineApp = null;

    public TestSuite() throws Exception {
        Persistance p = new Persistance();
        settings = p.readSettings();
        proxyConfig = p.readProxyConfig();
        fixedConfig = new FixedConfig();
        if (fixedConfig == null) {
            throw new Exception("static configuration is null");
        }
        if (settings == null) {
            throw new Exception("settings is null");
        }
        if (settings.getAirline() == null) {
            throw new Exception("irline is null");
        }
        airlineApp = fixedConfig.getAirlineApplication(settings.getAirline());
        if (airlineApp == null) {
            throw new Exception("airline app is null");
        }
    }

    private void setRetryHandler(GetMethod method) {
        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(MAX_RETRIES, false));
    }

    private void printHeaders(Header arr[]) {
        for (Header h : arr) {
            System.out.print(h);
        }
    }

    private String urlCheck(HttpClient client, URLAccount account) {
        String status = STAT_ERROR;
        GetMethod method = new GetMethod(account.getTargetUrl());
        method.setFollowRedirects(false);
        setRetryHandler(method);

        int statusCode = 0;
        try {
            statusCode = client.executeMethod(method);
            if (statusCode != HttpStatus.SC_OK) {
                status = STAT_ERROR;
            } else {
                status = STAT_OK;
            }

        } catch (HttpException ex) {
            status = STAT_CONN_ERROR;
        } catch (IOException ex) {
            status = STAT_CONN_ERROR;
        } finally {
            method.releaseConnection();
            return status;
        }
    }

    private String loginCheck(HttpClient client, URLAccount account) {
        String status = STAT_ERROR;
        GetMethod method = new GetMethod(account.getTargetUrl());
        PostMethod post = null;
        method.setFollowRedirects(true);
        setRetryHandler(method);

        int statusCode = 0;
        try {
            statusCode = client.executeMethod(method);
            printHeaders(method.getResponseHeaders());
            String sessionCookie = method.getResponseHeader("Set-Cookie").getValue();
            method.releaseConnection();

            post = new PostMethod(account.getLoginUrl());
            post.setRequestHeader("Cookie", sessionCookie);

            NameValuePair[] data = {
                new NameValuePair("j_username", account.getUsername()),
                new NameValuePair("j_password", account.getPassword()),
                new NameValuePair("j_token", "1")
            };
            post.setRequestBody(data);
            client.executeMethod(post);

            statusCode = post.getStatusCode();
            printHeaders(post.getRequestHeaders());
            post.releaseConnection();
            if (statusCode != HttpStatus.SC_OK) {
                method = new GetMethod(account.getTargetUrl());
                method.setRequestHeader("Cookie", sessionCookie);

                setRetryHandler(method);
                statusCode = client.executeMethod(method);
                status = STAT_OK;
                method.releaseConnection();
            }

        } catch (HttpException ex) {
            status = STAT_CONN_ERROR;
        } catch (IOException ex) {
            status = STAT_CONN_ERROR;
        } finally {
            if (post != null) {
                post.releaseConnection();
            }
            if (method != null) {
                method.releaseConnection();
            }
            return status;
        }
    }

    public TestReport run() {

        HttpClient client = new HttpClient();

        if (!proxyConfig.isNoProxy()) {
            client.getHostConfiguration().setProxy(proxyConfig.getProxyHost(), proxyConfig.getProxyPort());
        }

        TestReport report = new TestReport();
        report.setAirlineApp(airlineApp);
        report.setSettings(settings);
        report.setProxyConfig(proxyConfig);
        report.setDate(new Date());


        URLAccount account;
        int statusCode;



        /**
         * Login check
         */
        // agent check
        account = airlineApp.getAgent();
        if (account.isEnabled()) {
            report.setAgentChecked(true);
            report.setAgentStatus(loginCheck(client, account));
        } else {
            report.setAgentChecked(false);
            report.setAgentStatus(TEST_SKIPPED);
        }

        //call center check
        account = airlineApp.getCallCenter();
        if (account.isEnabled()) {
            report.setCallCenterChecked(true);
            report.setCallCenterStatus(loginCheck(client, account));
        } else {
            report.setCallCenterChecked(false);
            report.setCallCenterStatus(TEST_SKIPPED);
        }
        //ibe check
        account = airlineApp.getIbe();
        if (account.isEnabled()) {
            report.setIbeChecked(true);
            report.setIbeStatus(urlCheck(client, account));
        } else {
            report.setIbeChecked(false);
            report.setIbeStatus(TEST_SKIPPED);
        }

        /**
         * Speed Check
         */
        report.setConnectionChecked(true);
        GetMethod method = null;
        String speedStat = STAT_ERROR;
        String connStat = STAT_ERROR;
        try {

            account = airlineApp.getSpeedAccount();
            if(account == null){
                System.out.println("account is null");
            }else{
                System.out.println("accoutn is not null");
            }
            System.out.println(account.getTargetUrl());
            method = new GetMethod(account.getTargetUrl());
            setRetryHandler(method);
            Calendar st = Calendar.getInstance();
            statusCode = client.executeMethod(method);
            connStat = STAT_OK;
            Calendar et = Calendar.getInstance();
            report.setSpeedChecked(true);
            if (statusCode == HttpStatus.SC_OK) {
                double netSpeed = fixedConfig.getSpeedImageSize() * 1000 / (et.getTimeInMillis() - st.getTimeInMillis());
                NumberFormat nf = new DecimalFormat("#.00 kbps");
                speedStat = nf.format(netSpeed);
            }

        } catch (HttpException e) {
            speedStat = STAT_CONN_ERROR;
            connStat = STAT_ERROR;
        } catch (IOException e) {
            speedStat = STAT_CONN_ERROR;
            connStat = STAT_ERROR;
        } finally {
            // Release the connection.
            report.setSpeed(speedStat);
            report.setConnectionStatus(connStat);
            method.releaseConnection();
        }
        //notify the reporting stub 
        new NotificationStub().notifyAll(report);
        return report;
    }
}
