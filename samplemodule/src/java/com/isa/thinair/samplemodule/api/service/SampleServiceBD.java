/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.api.service;

import java.util.Collection;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;

/**
 * @author Nasly
 */
public interface SampleServiceBD {

	public static final String SERVICE_NAME = "SampleService";

	public void saveOrUpdate(SampleOjbect obj) throws ModuleException;

	public void delete(String key) throws ModuleException;

	public Collection getRecords() throws ModuleException;

	public SampleOjbect getRecord(String key) throws ModuleException;
}
