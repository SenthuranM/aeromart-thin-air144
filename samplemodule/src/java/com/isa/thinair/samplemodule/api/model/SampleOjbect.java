/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nasly
 * @hibernate.class table = "t_sampleobject"
 */
public class SampleOjbect extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3950409106519291933L;
	private String id;
	private String message;
	private Date testdate;
	private int testint;
	private double testdouble;

	/**
	 * 
	 * @hibernate.id column = "ID" generator-class = "assigned"
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @hibernate.property column = "MESSAGE"
	 */
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @hibernate.property column = "TESTDATE"
	 */
	public Date getTestdate() {
		return testdate;
	}

	public void setTestdate(Date testdate) {
		this.testdate = testdate;
	}

	/**
	 * @hibernate.property column = "TESTDOUBLE"
	 */
	public double getTestdouble() {
		return testdouble;
	}

	public void setTestdouble(double testdouble) {
		this.testdouble = testdouble;
	}

	/**
	 * @hibernate.property column = "TESTINT"
	 */
	public int getTestint() {
		return testint;
	}

	public void setTestint(int testint) {
		this.testint = testint;
	}
}
