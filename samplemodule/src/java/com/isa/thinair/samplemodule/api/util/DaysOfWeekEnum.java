/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.samplemodule.api.util;

/**
 * sample enum for days of week
 * 
 * @author Lasantha Pambagoda
 */
public final class DaysOfWeekEnum {

	private String day;

	private DaysOfWeekEnum(String day) {
		this.day = day;
	}

	public String toString() {
		return this.day;
	}

	public static final DaysOfWeekEnum SUNDAY = new DaysOfWeekEnum("SUNDAY");
	public static final DaysOfWeekEnum MONDAY = new DaysOfWeekEnum("MONDAY");
	public static final DaysOfWeekEnum TUESDAY = new DaysOfWeekEnum("TUESDAY");
	public static final DaysOfWeekEnum WEDNESDAY = new DaysOfWeekEnum("WEDNESDAY");
	public static final DaysOfWeekEnum THURSDAY = new DaysOfWeekEnum("THURSDAY");
	public static final DaysOfWeekEnum FRIDAY = new DaysOfWeekEnum("FRIDAY");
	public static final DaysOfWeekEnum SATURDAY = new DaysOfWeekEnum("SATURDAY");

}
