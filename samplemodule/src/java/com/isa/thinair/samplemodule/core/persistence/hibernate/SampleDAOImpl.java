/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.persistence.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessResourceFailureException;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.core.persistence.dao.SampleDAO;

/**
 * @author Nasly
 * @isa.module.dao-impl dao-name="sampleDAO"
 */
public class SampleDAOImpl extends PlatformHibernateDaoSupport implements SampleDAO {
	private Log log = LogFactory.getLog(getClass());

	public SampleDAOImpl() {
	}

	public void saveOrUpdate(SampleOjbect obj) {

		// try{
		// getSessionFactory().getSQLExceptionConverter().
		super.hibernateSaveOrUpdate(obj);
		// }catch (Throwable daex){
		// System.out.println("EXXXXXXXXXXXXXXXXXXXXXXXXX");
		// }

		// throw new RuntimeException();

	}

	public List getRecords() {
		return find("from SampleOjbect", SampleOjbect.class);

	}

	public SampleOjbect getRecord(String id) {
		return (SampleOjbect) get(SampleOjbect.class, id);

	}

	public void delete(String primaryKey) {
		SampleOjbect sampleObj = (SampleOjbect) load(SampleOjbect.class, primaryKey);
		
		if (log.isDebugEnabled()) {
			log.debug("Primary key = " + primaryKey + " Version = " + sampleObj.getVersion());
		}
		delete(sampleObj);
	}

	public List search(List criteria) throws CommonsDataAccessException {
		try {
			return CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(SampleOjbect.class)).list();
		} catch (DataAccessResourceFailureException e) {
			throw new CommonsDataAccessException(e, "");
		} catch (HibernateException e) {
			throw new CommonsDataAccessException(e, "");
		} catch (IllegalStateException e) {
			throw new CommonsDataAccessException(e, "");
		}
	}

}
