/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.samplemodule.api.service.SampleServiceBD;

/**
 * @author Nasly
 */

@Local
public interface SampleServiceLocalDelegateImpl extends SampleServiceBD {

}
