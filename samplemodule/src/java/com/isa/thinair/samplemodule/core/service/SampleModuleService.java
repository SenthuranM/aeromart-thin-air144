/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * 
 *         SampleModule's service interface
 * @isa.module.service-interface module-name="samplemodule" description="a sample module"
 */
public class SampleModuleService extends DefaultModule {
}
