/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.samplemodule.core.remoting.ejb;

import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.core.persistence.dao.SampleDAO;
import com.isa.thinair.samplemodule.core.service.bd.SampleServiceDelegateImpl;
import com.isa.thinair.samplemodule.core.service.bd.SampleServiceLocalDelegateImpl;

/**
 * @author Nasly
 */
@Stateless
@RemoteBinding(jndiBinding = "SampleService.remote")
@LocalBinding(jndiBinding = "SampleService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Clustered
public class SampleServiceBean extends PlatformBaseSessionBean implements SampleServiceDelegateImpl,
		SampleServiceLocalDelegateImpl {

	/**
	 * @throws ModuleException
	 */
	public void saveOrUpdate(SampleOjbect obj) throws CommonsDataAccessException {

		lookupSampleDAO().saveOrUpdate(obj);

	}

	public SampleOjbect getRecord(String key) throws CommonsDataAccessException {
		return lookupSampleDAO().getRecord(key);
	}

	public Collection getRecords() throws CommonsDataAccessException {
		return lookupSampleDAO().getRecords();
	}

	public void delete(String key) throws CommonsDataAccessException {
		lookupSampleDAO().delete(key);
	}

	private SampleDAO lookupSampleDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (SampleDAO) lookupService.getBean("isa:base://modules/samplemodule?id=sampleDAOProxy");
	}
}
