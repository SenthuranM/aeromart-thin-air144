/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestSampleDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.samplemodule.core.persistence.hibernate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.core.persistence.dao.SampleDAO;

/**
 * @author Nasly
 *
 */
public class TestSampleDAO extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testSave1()   {
		

		try{
		SampleDAO dao = getDAO();
		assertNotNull(dao);
		SampleOjbect obj = new SampleOjbect();
		String key = "324";
		String msg = "`sss!&%$@(  )-+";
		obj.setId(key);
		obj.setMessage(msg + "1");
		obj.setTestdate(new Date());
		obj.setTestdouble(11.12);
		obj.setTestint(3);
		//System.out.println(obj.getTestint());
		
		dao.saveOrUpdate(obj);
		}
		catch(CommonsDataAccessException e)
		{
			System.out.println("EERRRROOOOOOOOOOORRRRRRROR :   " + e.getModuleCode() + "   " + e.getExceptionCode() + " " + e);
		}
		catch(Exception e)
		{
			System.out.println("EERRRROOOOOOOOOOORRRRRRROR :   " + e);
		}
	}
	
	public void testDelete()   {
		
		try{
			SampleDAO dao = getDAO();
			dao.delete("329");
		}
		catch(CommonsDataAccessException e)
		{
			System.out.println("EERRRROOOOOOOOOOORRRRRRROR :   " + e.getModuleCode() + "   " + e.getExceptionCode() + " " + e);
		}
	}
	
	public void testGetRecord()   {
		
		try{
			SampleDAO dao = getDAO();
			dao.getRecord("888");
		}
		catch(CommonsDataAccessException e)
		{
			System.out.println("EERRRROOOOOOOOOOORRRRRRROR :   " + e.getModuleCode() + "   " + e.getExceptionCode() + " " + e);
		}
	}
	
	public void testGetRecords()   {
		
		try{
			SampleDAO dao = getDAO();
			List l = dao.getRecords();
		}
		catch(CommonsDataAccessException e)
		{
			System.out.println("EERRRROOOOOOOOOOORRRRRRROR :   " + e.getModuleCode() + "   " + e.getExceptionCode() + " " + e);
		}
	}
	
	

	public void testSearchIfTIMESTAMPSareEqual() throws Exception{
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
		List criteriaList = new ArrayList();
		
		//testing - greaterthan
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterion.setFieldName("testdate");
		List values = new ArrayList();
		
		//give year/month/day as a string will also work
		
		//Date startdate = new Date("1979/5/5");
		//Date enddate = new Date("1979/5/7");
			
		GregorianCalendar gc = new GregorianCalendar();
		//gc.set(2005,7,11,2,41,39);
			
		//working when giving the 24 hr clock
		gc.setTime(new Date("2005/8/11 14:41:39"));
			
	
		
		
		// THE FORMATTER METHODS DONT WORK UNFORTUNATELY
		//DateFormat formatter = new SimpleDateFormat("dd-mm-yyyy HH:mm:ss");

        //formatter = new SimpleDateFormat();
        //Date d = (Date)formatter.parse("11-08-2005 14:41:39");
		
		

		//DateFormat.g
			
			
		//WONT WORK
		//gc.setTime(new Date("2005/8/11 2:41:39PM"));
			
		Date d = gc.getTime();
		System.out.println(d);
		values.add(d);
						
		criterion.setValue(values);
		//adding the criterias to the list	
		criteriaList.add(criterion);
		
		SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
		List result = dao.search(criteriaList);
		assertTrue("invalid search timestamp", result.size()>0);
		System.out.println(result);
			
						
	}
	public void testSearchBETWEEN_DATE_Conditions(){
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_BETWEEN);
			criterion.setFieldName("testdate");
			List values = new ArrayList();
			

			//give year/month/day as a string will also work
			Date startdate = new Date("1979/5/5");
			Date enddate = new Date("1979/5/7");
			/////////////////////////////////
			
			
			//problems exist in using this way....
			//Date startdate = new  Date(5,4,1979);
			//Date enddate = new Date(5,7,1979);
			
					
			
			System.out.println("startdate: - " + startdate +"       end date:--" + enddate); 
			
			
			values.add(startdate);
			values.add(enddate);
			
			
			
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid Between condition search", result.size()>0);
			System.out.println(result);
			
						
	}
	
	public void testSave(){
		SampleDAO dao = getDAO();
		assertNotNull(dao);
		SampleOjbect obj = new SampleOjbect();
		String key = "555";
		String msg = "test hibernate";
		obj.setId(key);
		obj.setMessage(msg);
		//dao.saveOrUpdate(obj);
		dao.delete(key);
		//following should not throw an exception if deletion successful
		//dao.saveOrUpdate(obj);
		dao.delete(key);
	}
	
	
	
	public void testSearch(){
		
		
		SampleDAO dao = getDAO();
		//add a record
		
			SampleOjbect obj = new SampleOjbect();
			String key = "255";
			
			String msg = "test hibernate";
			String msg1 = "test hibernate1";
			
			obj.setId(key);
			obj.setMessage(msg1);
			obj.setTestdate(new Date());
			obj.setTestdouble(234.232);
			obj.setTestint(12);
						
		//dao.saveOrUpdate(obj);
		
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
		List criteriaList = new ArrayList();
		
			//criteria 1 - message
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterion.setFieldName("message");
					
			List values = new ArrayList();
			
			values.add(msg);
			criterion.setValue(values);
			
			//criteria 2 - testing INT
			ModuleCriterion criterion1 = new ModuleCriterion();
			criterion1.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterion1.setFieldName("testint");
			List values1 = new ArrayList();
			values1.add(new Integer("11"));
			criterion1.setValue(values1);
			
			//criteria 3 testing DOUBLE
			ModuleCriterion criterion2 = new ModuleCriterion();
			criterion2.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterion2.setFieldName("testdouble");
			List values2 = new ArrayList();
			values2.add(new Double(111.11));
			criterion2.setValue(values2);
			
						
			
		//adding the criterias to the list	
			criteriaList.add(criterion);
			criteriaList.add(criterion1);
			criteriaList.add(criterion2);
			
		//pass the List criteriaList and get the result	
		List result = dao.search(criteriaList);
		assertTrue("invalid search", result.size()>0);
		System.out.println("RESULTS OF CRIETERIA SELECTION" +  result);
		//dao.delete(key);
	}
	
	public void testSearchGreaterThanOrEqualsConditions(){
				
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_GREATER_THAN);
			criterion.setFieldName("testint");
			List values = new ArrayList();
			values.add(new Integer(12));
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid greater than search", result.size()>0);
			
			
			//testing - greaterthanorequals
			criterion.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			criteriaList.add(criterion);
			List result1 = dao.search(criteriaList);
			assertTrue("invalid greater or equal search", result1.size()>0);
			
	}
	
	
	public void testSearchLessThanOrEqualsConditions(){
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_LESS_THAN);
			criterion.setFieldName("testint");
			List values = new ArrayList();
			values.add(new Integer(12));
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid Less than search", result.size()>0);
			
			
			//testing - greaterthanorequals
			criterion.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			criteriaList.add(criterion);
			List result1 = dao.search(criteriaList);
			assertTrue("invalid lesser than or equal search", result1.size()>0);
			
	}
	
	
	public void testSearchBETWEENConditions(){
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_BETWEEN);
			criterion.setFieldName("testint");
			List values = new ArrayList();
			values.add(new Integer(10));
			values.add(new Integer(12));
			 //the values 10 and 12 will also be included 
			
			
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid Between condition search", result.size()>0);
			System.out.println("*************************BETWEEN 10 AND 12" + result);
			
						
	}
//	
//	
//	
//	
//	
	public void testSearchINConditions(){
		
		//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_IN);
			criterion.setFieldName("testint");
			List values = new ArrayList();
			values.add(new Integer(1));
			values.add(new Integer(12));
			
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid Between condition search", result.size()>0);
			
			
						
	}
	
	public void testSearchLIKEConditions(){
		
			//create a criteria from ModuleCriterion and add it to a list ....i.e  a list of criterias 
			List criteriaList = new ArrayList();
		
			//testing - greaterthan
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
			criterion.setFieldName("message");
			List values = new ArrayList();
			values.add("yo dude");
			//values.add(new Integer(12));
			
			criterion.setValue(values);
			//adding the criterias to the list	
			criteriaList.add(criterion);
			SampleDAO dao = getDAO();
			//pass the List criteriaList and get the result	
			List result = dao.search(criteriaList);
			assertTrue("invalid LIKE condition search", result.size()>0);
			
			
	}

	

	
private SampleDAO getDAO(){
		LookupService lookup = LookupServiceFactory.getInstance();
		return (SampleDAO) lookup.getBean("isa:base://modules/samplemodule?id=sampleDAOProxy");
}

protected void tearDown()
{	
}

}
