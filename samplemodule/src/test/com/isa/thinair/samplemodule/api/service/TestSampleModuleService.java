package com.isa.thinair.samplemodule.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.core.persistence.dao.SampleDAO;

public class TestSampleModuleService extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Test method for 'com.isa.thinair.platform.api.DefaultModule.getServiceDelegate(String)'
	 */
	public void testGetServiceDelegateString() throws ModuleException {
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule sampleModule = lookup.getModule("samplemodule");
		assertNotNull(sampleModule);
		SampleServiceBD sampleServiceBD = (SampleServiceBD)sampleModule.getServiceBD("sample.service.remote");
		SampleOjbect obj = new SampleOjbect();
		String key = "555";
		String msg = "test hibernate";
		obj.setId(key);
		obj.setMessage(msg);
		sampleServiceBD.saveOrUpdate(obj);
		sampleServiceBD.delete(key);
		//following should not throw an exception if deletion successful
		sampleServiceBD.saveOrUpdate(obj);
		sampleServiceBD.delete(key);
	}

}
