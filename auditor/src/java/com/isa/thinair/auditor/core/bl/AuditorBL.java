/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * ===============================================================================
 */
package com.isa.thinair.auditor.core.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.auditor.api.model.AgentAudit;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.AuditTemplate;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorModuleUtils;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.auditor.core.config.AuditorConfig;
import com.isa.thinair.auditor.core.persistence.dao.AuditorDAO;
import com.isa.thinair.auditor.core.util.AuditorInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Util;

public class AuditorBL {

	private Log log = LogFactory.getLog(AuditorBL.class);

	private static final String PARAM_SEPARATOR = "||";

	private static final String VALUE_SEPARATOR = "::";

	private static final String PARAM_VALUE_SEPARATOR = ":=";

	private static final String AUDIT_PLACEHOLDER_MARK = "{}";

	private static final String AUDIT_INTERNAL_PLACEHOLDER_MARK = "~|";

	private static final String AUDIT_BREAKLINES_START = "<br>";

	private static final String AUDIT_BREAKLINES_END = "</br>";

	private static final String AUDIT_BOLD_START = "<b>";

	private static final String AUDIT_BOLD_END = "</b>";

	// the database constraint in auditor table.
	private static final int MAX_LENGHT_FOR_CONTENT = 2000;

	// Reservation audit max length
	private static final int MAX_LENGHT_FOR_AUDIT_CONTENT = 4000;

	private AuditorDAO auditorDAO;

	public AuditorBL() {
		auditorDAO = (AuditorDAO) AuditorModuleUtils.getInstance().getLocalBean(
				AuditorInternalConstants.DAOProxyNames.AUDITOR_DAO);
	}

	/**
	 * 
	 * @param audit
	 * @param contents
	 * @throws ModuleException
	 */
	public void doAudit(Audit audit, LinkedHashMap contents) throws ModuleException {

		if (contents != null) {
			audit.setContent(constructContent(contents));
		}

		StringBuilder tmpStringBuffer = new StringBuilder();
		String contentAudit = audit.getContent();

		contentAudit = (contentAudit == null) ? "" : contentAudit;

		int baseWidth = ((AuditorConfig) AuditorModuleUtils.getInstance().getModuleConfig()).getAuditWidth();
		int width = baseWidth;

		int lengthOfContent = contentAudit.length();
		int tmpStartWidth = 0;
		int tempSeparator = 0;

		while (width < lengthOfContent) {

			String tmpSplitAudit = contentAudit.substring(tmpStartWidth, width);

			if (tmpSplitAudit.endsWith("||")) {

				tmpStringBuffer.append(tmpSplitAudit);
				tmpStartWidth = width;
				width = baseWidth + width;

			} else {
				tempSeparator = tmpSplitAudit.lastIndexOf("||");
				if (tempSeparator == -1) {
					tmpStringBuffer.append(tmpSplitAudit);
					tmpStartWidth = width;
					width = baseWidth + width;
				} else {
					String splStr = contentAudit.substring(tmpStartWidth, tempSeparator + tmpStartWidth + 2);
					tmpStringBuffer.append(splStr);
					tmpStartWidth = tmpStartWidth + tempSeparator + 2;
					width = tmpStartWidth + baseWidth;
				}
			}
		}

		tmpStringBuffer.append(contentAudit.substring(tmpStartWidth, lengthOfContent));
		audit.setContent(tmpStringBuffer.toString());

		if (audit.getContent().length() < MAX_LENGHT_FOR_CONTENT) {
			auditorDAO.createAudit(audit);
		} else {
			// AuditBLUtil will return a collection of strings to fit the
			// audit.setContent. where each wont exceed the max length.
			Collection splitedContents = null;
			try {
				splitedContents = new AuditorBLUtil(MAX_LENGHT_FOR_CONTENT).splitString(audit.getContent());
			} catch (Exception e) {
				log.info("ERROR WHEN TRYING TO SPLIT LARGE AUDIT CONTENT...THE MAXIMUM LENGTH WILL BE TAKEN");
			}

			if (splitedContents != null) {
				Iterator iterator = splitedContents.iterator();
				while (iterator.hasNext()) {
					Audit auditNew = new Audit();
					auditNew.setAppCode(audit.getAppCode());
					auditNew.setTaskCode(audit.getTaskCode());
					auditNew.setTimestamp(audit.getTimestamp());
					auditNew.setUserId(audit.getUserId());
					auditNew.setContent(iterator.next().toString());

					auditorDAO.createAudit(auditNew);
				}
			} else {
				audit.setContent(audit.getContent().substring(0, MAX_LENGHT_FOR_CONTENT));
				auditorDAO.createAudit(audit);
			}
		}
	}

	/**
	 * Carry out all audits
	 * 
	 * @param collectionOfAudits
	 * @throws ModuleException
	 */
	public void doAudit(Collection collectionOfAudits) throws ModuleException {
		if (collectionOfAudits == null || collectionOfAudits.isEmpty()) {
			throw new ModuleException("auditor.arg.dao.auditcontent.null");
		}
		ReservationAudit reservationAudit;
		AgentAudit agentAudit;

		for (Object object : collectionOfAudits) {

			// IF the object is Reservation Audit, call the reservation audit method
			if (object instanceof ReservationAudit) {
				reservationAudit = (ReservationAudit) object;
				this.doAudit(reservationAudit, reservationAudit.getContentMap());
			} else if (object instanceof AgentAudit) {
				// IF the object is Agent Audit, call the Agent audit method
				agentAudit = (AgentAudit) object;
				this.doAudit(agentAudit, agentAudit.getContentMap());
			}
		}
	}

	/**
	 * Carry out a reservation audit
	 * 
	 * @param audit
	 * @param contents
	 * @throws ModuleException
	 */
	public void doAudit(ReservationAudit reservationAudit, LinkedHashMap contents) throws ModuleException {
		if (reservationAudit == null || contents == null || reservationAudit.getModificationType() == null) {
			throw new ModuleException("auditor.arg.dao.auditcontent.null");
		}

		String auditContent = getAuditContent(reservationAudit.getModificationType(), contents);

		// auditContent = auditContent.replaceAll(RESERVAION_AUDIT_PLACEHOLDER_MARK.substring(1,2),"");
		ReservationAudit newReservationAudit;

		Collection<String> trunkatedStings = getTrunkatedStingsList(auditContent);
		for (String trunkatedSting : trunkatedStings) {
			newReservationAudit = copyReservationAudit(reservationAudit);
			newReservationAudit.setDescription(trunkatedSting);
			auditorDAO.createAudit(newReservationAudit);
		}
	}

	public void doAuditInventory(Collection inventoryAudits) {
		if (inventoryAudits != null) {
			Iterator inventoryAuditsIt = inventoryAudits.iterator();
			while (inventoryAuditsIt.hasNext()) {
				auditorDAO.createAudit((InventoryAudit) inventoryAuditsIt.next());
			}
		}
	}

	private ReservationAudit copyReservationAudit(ReservationAudit reservationAudit) {
		ReservationAudit newReservationAudit = new ReservationAudit();
		newReservationAudit.setAppCode(reservationAudit.getAppCode());
		newReservationAudit.setAuditId(reservationAudit.getAuditId());
		newReservationAudit.setCustomerId(reservationAudit.getCustomerId());
		newReservationAudit.setDescription(reservationAudit.getDescription());
		newReservationAudit.setModificationType(reservationAudit.getModificationType());
		newReservationAudit.setPnr(reservationAudit.getPnr());
		newReservationAudit.setSalesChannelCode(reservationAudit.getSalesChannelCode());
		newReservationAudit.setUserId(reservationAudit.getUserId());
		newReservationAudit.setUserNote(reservationAudit.getUserNote());
		newReservationAudit.setUserNoteType(reservationAudit.getUserNoteType());
		newReservationAudit.setVersion(reservationAudit.getVersion());
		newReservationAudit.setZuluModificationDate(reservationAudit.getZuluModificationDate());
		newReservationAudit.setDisplayName(reservationAudit.getDisplayName());
		return newReservationAudit;
	}

	private List<String> getTrunkatedStingsList(String source) {
		int continuationSignlength = 4;
		String lastString;
		List<String> trunkatedStingsList = new ArrayList<String>();
		int splitPosition = 0;
		if (source != null) {
			lastString = source.trim();
			while (lastString.length() > MAX_LENGHT_FOR_AUDIT_CONTENT) {
				splitPosition = lastString.substring(0, MAX_LENGHT_FOR_AUDIT_CONTENT - continuationSignlength).lastIndexOf(" ");
				if (splitPosition < 0) {
					splitPosition = MAX_LENGHT_FOR_AUDIT_CONTENT - continuationSignlength;
				}
				trunkatedStingsList.add(lastString.substring(0, splitPosition) + " ...");
				lastString = "..." + lastString.substring(splitPosition).trim();
			}
			trunkatedStingsList.add(lastString);
		}
		return trunkatedStingsList;
	}

	/**
	 * The constructed content would be as paramkey1:=oldvalue1<>oldvalue2::newvalue1<>newvalue2||paramkey2:=newvalue3
	 * 
	 * @param contents
	 * @return
	 */
	private String constructContent(LinkedHashMap contents) throws ModuleException {
		Object obj;
		Iterator it;
		String[] values;
		String paramKey;
		String auditContent = "";
		String paramValue = "";
		int noOfRecs = 0;

		try {
			if ((contents != null) && (!contents.isEmpty())) {
				it = contents.keySet().iterator();
				while (it.hasNext()) {
					paramKey = (String) it.next();
					paramValue = "";
					if ((paramKey != null) && (!paramKey.trim().equals(""))) {
						obj = contents.get(paramKey);
						if ((obj instanceof String)) {
							paramValue = (String) obj;
						} else if (obj instanceof String[]) {
							values = (String[]) obj;
							if ((values != null) && (values.length > 0)) {
								noOfRecs = values.length;
								for (int i = (noOfRecs - 1); i >= 0; i--) {
									paramValue += values[i];
									if (i != 0) {
										paramValue += VALUE_SEPARATOR;
									}
								}
							}
						} else if ((obj instanceof Integer)) {

							paramValue = ((Integer) obj).toString();

						} else if ((obj instanceof Date)) {

							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String strDate = dateFormat.format((Date) obj);
							paramValue = strDate;
						}
						if ((paramValue != null) && (!paramValue.trim().equals(""))) {
							if ((auditContent != null) && (!auditContent.trim().equalsIgnoreCase(""))) {
								auditContent += PARAM_SEPARATOR;
							}
							auditContent += paramKey + PARAM_VALUE_SEPARATOR + paramValue;
						}
					}
				}
			}
		} catch (Exception e) {
			throw new ModuleException(e, "audit.error");

		}
		return auditContent;
	}

	public void doAudit(PFSAudit pfsAudit) throws ModuleException {
		if (pfsAudit == null) {
			throw new ModuleException("auditor.arg.dao.auditcontent.null");
		}
		auditorDAO.createAudit(pfsAudit);
	}

	public void deleteAuditsByID(long pfsID) {
		Collection<PFSAudit> auditDTOs = auditorDAO.getPFSAudits(pfsID);
		auditorDAO.deleteAudits(auditDTOs);
	}

	public void doAudit(NameChangeChargeAudit nameChangeChargeAudit) throws ModuleException {
		if (nameChangeChargeAudit == null) {
			throw new ModuleException("auditor.arg.dao.auditcontent.null");
		}
		auditorDAO.createAudit(nameChangeChargeAudit);
	}

	/**
	 * Audit the agent audit DTO
	 * 
	 * @param agentAudit
	 * @param contents
	 * @throws ModuleException
	 */
	public void doAudit(AgentAudit agentAudit, LinkedHashMap contents) throws ModuleException {
		if (agentAudit == null || contents == null || agentAudit.getTemplateCode() == null) {
			throw new ModuleException("auditor.arg.dao.auditcontent.null");
		}

		// Get the Audit Content
		String auditContent = getAuditContent(agentAudit.getTemplateCode(), contents);
		// Get the truncated String
		Collection<String> trunkatedStings = getTrunkatedStingsList(auditContent);

		AgentAudit newAgentAudit;
		for (String trunkatedSting : trunkatedStings) {
			newAgentAudit = copyAgentAudit(agentAudit);
			newAgentAudit.setContent(trunkatedSting);
			auditorDAO.createAudit(newAgentAudit);
		}

	}

	/**
	 * When the template code and contents are passed, this method will give you the audit content (description)
	 * 
	 * @param templateCode
	 * @param contents
	 * @return
	 * @throws ModuleException
	 */
	private String getAuditContent(String templateCode, LinkedHashMap contents) throws ModuleException {

		AuditTemplate auditTemplate = auditorDAO.getAuditTemplate(templateCode);

		// Replace <> tags by the sent content hashmap values
		String auditContent = Util.setStringToPlaceHolderIndex(auditTemplate.getTemplateContent(), contents,
				AUDIT_INTERNAL_PLACEHOLDER_MARK);

		// Remove {} tags that contains <> tags (Un-used tags)
		auditContent = Util.removeUnUsedTagsFromString(auditContent, AUDIT_PLACEHOLDER_MARK,
				AUDIT_INTERNAL_PLACEHOLDER_MARK.substring(0, 1));

		auditContent = Util.replacePlaceHolderValues(auditContent, "", "[" + AUDIT_PLACEHOLDER_MARK + "]",
				AUDIT_BREAKLINES_START, AUDIT_BREAKLINES_END, AUDIT_BOLD_START, AUDIT_BOLD_END);

		return auditContent;
	}

	/**
	 * 
	 * @param agentAudit
	 * @return
	 */
	private AgentAudit copyAgentAudit(AgentAudit agentAudit) {
		AgentAudit newAgentAudit = new AgentAudit();
		newAgentAudit.setAuditId(agentAudit.getAuditId());
		newAgentAudit.setTemplateCode(agentAudit.getTemplateCode());
		newAgentAudit.setTimestamp(agentAudit.getTimestamp());
		newAgentAudit.setUserId(agentAudit.getUserId());
		newAgentAudit.setContent(agentAudit.getContent());
		newAgentAudit.setAgentCode(agentAudit.getAgentCode());
		newAgentAudit.setDaysPriorToExpire(agentAudit.getDaysPriorToExpire());
		newAgentAudit.setCreditLimitUtilization(agentAudit.getCreditLimitUtilization());
		newAgentAudit.setVersion(agentAudit.getVersion());
		newAgentAudit.setDefaultMessageValidity(agentAudit.getDefaultMessageValidity());
		newAgentAudit.setPaymentCode(agentAudit.getPaymentCode());
		return newAgentAudit;
	}

	// Method to add new user note with user note type
	public void doAuditUN(UserNoteTO userNoteTO, CredentialsDTO credentialDTO) throws ModuleException {
		ReservationAudit newReservationAudit = new ReservationAudit();
		String userNote = userNoteTO.getUserNotes().get(0);
		newReservationAudit.setUserNoteType(userNoteTO.getUserNoteType());
		newReservationAudit.setUserNote(userNote);
		newReservationAudit.setPnr(userNoteTO.getPnr());
		newReservationAudit.setModificationType(AuditTemplateEnum.ADD_USER_NOTE.getCode());
		newReservationAudit.setUserId(credentialDTO.getUserId());
		newReservationAudit.setVersion(-1l);
		newReservationAudit.setCustomerId(credentialDTO.getCustomerId());
		newReservationAudit.setDisplayName(credentialDTO.getDisplayName());
		newReservationAudit.setSalesChannelCode(credentialDTO.getSalesChannelCode());
		newReservationAudit.setZuluModificationDate(new Date());

		// Setting the ip address
		if (credentialDTO.getTrackInfoDTO() != null && credentialDTO.getTrackInfoDTO().getIpAddress() != null) {
			newReservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.IP_ADDDRESS, credentialDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialDTO.getTrackInfoDTO() != null && credentialDTO.getTrackInfoDTO().getCarrierCode() != null) {
			newReservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.ORIGIN_CARRIER, credentialDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		String auditContent = getAuditContent(newReservationAudit.getModificationType(), newReservationAudit.getContentMap());

		Collection<String> trunkatedStings = getTrunkatedStingsList(auditContent);
		for (String trunkatedSting : trunkatedStings) {
			newReservationAudit.setDescription(trunkatedSting);
			auditorDAO.createAudit(newReservationAudit);
		}
	}

	/**
	 * Audit tax invoice(s)
	 * 
	 * @param pnr
	 * @param invoiceNumbers
	 * @param credentialDTO
	 * @return
	 * @throws ModuleException
	 */
	public void doTaxInvoiceAudit(String pnr, List<String> invoiceNumbers, CredentialsDTO credentialDTO) throws ModuleException {
		ReservationAudit newReservationAudit = new ReservationAudit();

		String taxInvoicesStr = String.join(", ", invoiceNumbers);
		newReservationAudit.setPnr(pnr);
		newReservationAudit.setModificationType(AuditTemplateEnum.GENERATE_TAX_INVOICE.getCode());
		newReservationAudit.setUserId(credentialDTO.getUserId());
		newReservationAudit.setVersion(-1l);
		newReservationAudit.setCustomerId(credentialDTO.getCustomerId());
		newReservationAudit.setDisplayName(credentialDTO.getDisplayName());
		newReservationAudit.setSalesChannelCode(credentialDTO.getSalesChannelCode());
		newReservationAudit.setZuluModificationDate(new Date());

		// Setting the ip address
		if (credentialDTO.getTrackInfoDTO() != null && credentialDTO.getTrackInfoDTO().getIpAddress() != null) {
			newReservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.IP_ADDDRESS, credentialDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialDTO.getTrackInfoDTO() != null && credentialDTO.getTrackInfoDTO().getCarrierCode() != null) {
			newReservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ViewedReservation.ORIGIN_CARRIER, credentialDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		// Setting the tax invoice(s)
		if (taxInvoicesStr != null && !taxInvoicesStr.isEmpty()) {
			newReservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TaxInvoiceGenerated.TAX_INVOICE, taxInvoicesStr);
		}

		String auditContent = getAuditContent(newReservationAudit.getModificationType(), newReservationAudit.getContentMap());

		Collection<String> trunkatedStings = getTrunkatedStingsList(auditContent);
		for (String trunkatedSting : trunkatedStings) {
			newReservationAudit.setDescription(trunkatedSting);
			auditorDAO.createAudit(newReservationAudit);
		}
	}
}
