package com.isa.thinair.auditor.core.service.bd;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationAuditDTO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.service.AuditorModuleUtils;
import com.isa.thinair.auditor.core.bl.AuditorBL;
import com.isa.thinair.auditor.core.bl.PFSAuditBL;
import com.isa.thinair.auditor.core.persistence.dao.AuditorDAO;
import com.isa.thinair.auditor.core.persistence.dao.AuditorJdbcDAO;
import com.isa.thinair.auditor.core.util.AuditorInternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;

/**
 * @isa.module.bd-impl id="auditor.service.local" description="Locally accessible auditoring service BD"
 */
public class AuditorBDLocalImpl extends PlatformBaseServiceDelegate implements AuditorBD {

	public void audit(Audit audit, LinkedHashMap contents) throws ModuleException {
		try {
			new AuditorBL().doAudit(audit, contents);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception exception) {
			throw new ModuleException(exception, "auditor.unhandled.exception");
		}
	}

	public void audit(ReservationAudit reservationAudit, LinkedHashMap contents) throws ModuleException {
		try {
			new AuditorBL().doAudit(reservationAudit, contents);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public void audit(Collection collectionOfAudits) throws ModuleException {
		try {
			new AuditorBL().doAudit(collectionOfAudits);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public void audit(PFSAudit pfsAudit) throws ModuleException {
		try {
			new AuditorBL().doAudit(pfsAudit);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public void audit(NameChangeChargeAudit nameChangeChargeAudit) throws ModuleException {
		try {
			new AuditorBL().doAudit(nameChangeChargeAudit);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}
	
	public void doTaxInvoiceAudit(String pnr, List<String> invoiceNumbers, CredentialsDTO credentialDTO) throws ModuleException {
		try {
			new AuditorBL().doTaxInvoiceAudit(pnr, invoiceNumbers, credentialDTO);
		} catch (ModuleException e) {
			throw e;
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	/**
	 * Audits inventory seat movements
	 * 
	 * @param inventoryAudits
	 *            Collection of InventoryAudits
	 */
	public void auditInventory(Collection inventoryAudits) throws ModuleException {
		try {
			new AuditorBL().doAuditInventory(inventoryAudits);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public Page getAudits(List criteria, int startIndex, int pageSize, List orderByFields) throws ModuleException {
		try {
			return getAuditorDAO().getAudits(criteria, startIndex, pageSize, orderByFields);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public Page getReservationAudits(List criteria, int startIndex, int pageSize, List orderByFields) throws ModuleException {

		try {
			return getAuditorDAO().getReservationAudits(criteria, startIndex, pageSize, orderByFields);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public List getAudits(String pnr) throws ModuleException {
		try {
			return getAuditorDAO().getAudits(pnr);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}
	}

	public Collection retrieveAudits(AuditDTO auditSearchCriteria) throws ModuleException {
		try {
			return getAuditorJdbcDAO().retrieveAudits(auditSearchCriteria);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}
	}

	public Collection retrieveAudits(ReservationAuditDTO reservationAuditSearchCriteria) throws ModuleException {
		try {
			return getAuditorJdbcDAO().retrieveAudits(reservationAuditSearchCriteria);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}
	}

	/**
	 * Return the history
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection getHistory(String pnr) throws ModuleException {
		try {
			return getAuditorJdbcDAO().getHistory(pnr);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}
	}


	/**
	 * Return the user notes history
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection getUserNotes(String pnr, boolean isClassifyUN, String agentCode) throws ModuleException {
		try {
			return getAuditorJdbcDAO().getUserNotes(pnr, isClassifyUN, agentCode);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}
	}

	public Collection<InventoryAudit> getInventoryAudit(Integer flightId) {
		return getAuditorDAO().getInventoryAudit(flightId);
	}

	/**
	 * Return the last user note
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public String getLastUserNote(String pnr, boolean classifyFlag, String agent) throws ModuleException {
		try {
			return getAuditorJdbcDAO().getLastUserNote(pnr, classifyFlag, agent);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		} catch (Exception e) {
			throw new ModuleException(e, e.getLocalizedMessage());
		}
	}

	/**
	 * 
	 * @return
	 */
	private AuditorDAO getAuditorDAO() {
		return (AuditorDAO) AuditorModuleUtils.getInstance().getLocalBean(AuditorInternalConstants.DAOProxyNames.AUDITOR_DAO);
	}

	/**
	 * 
	 * @return
	 */
	private AuditorJdbcDAO getAuditorJdbcDAO() {
		return (AuditorJdbcDAO) AuditorModuleUtils.getInstance().getLocalBean(
				AuditorInternalConstants.DAOProxyNames.AUDITOR_JDBC_DAO);
	}

	@Override
	public Collection<PFSAuditDTO> retrievePFSAudits(long pfsID) throws ModuleException {
		try {
			return getAuditorDAO().retrievePFSAudits(pfsID);
		} catch (CommonsDataAccessException cde) {
			throw new ModuleException(cde, cde.getExceptionCode());
		}

	}

	@Override
	public void deleteAuditsByID(long pfsID) {
		new AuditorBL().deleteAuditsByID(pfsID);
	}

	@Override
	public void preparePFSAudit(PFSAuditDTO pfsAuditDTO, long pfsID, String action, String systemNote) throws ModuleException {
		PFSAuditBL.preparePFSAudit(pfsAuditDTO, pfsID, action, systemNote);
	}

	@Override
	public ReservationAudit getLastReservationAudit(String pnr){
		return getAuditorDAO().getLastReservationAudit(pnr);
	}
	
	@Override
	public void updateReservationAudit(ReservationAudit reservationAudit){
		getAuditorDAO().updateReservationAudit(reservationAudit);
	}
}
