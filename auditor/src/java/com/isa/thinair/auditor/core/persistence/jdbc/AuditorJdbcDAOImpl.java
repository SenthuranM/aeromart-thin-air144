package com.isa.thinair.auditor.core.persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationAuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.auditor.core.persistence.dao.AuditorJdbcDAO;
import com.isa.thinair.auditor.core.util.AuditorInternalConstants;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

public class AuditorJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements AuditorJdbcDAO {

	private final Log log = LogFactory.getLog(AuditorJdbcDAOImpl.class);

	public Collection retrieveAudits(AuditDTO auditSearchCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Collection) jdbcTemplate.query(new RetreiveAuditsPrepareStmtCreator(auditSearchCriteria),
				new RetreiveAuditsResultSetExtractor());
	}

	public Collection retrieveAudits(ReservationAuditDTO reservationAuditSearchCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Collection) jdbcTemplate.query(new RetreiveAuditsPrepareStmtCreator(reservationAuditSearchCriteria),
				new RetreiveReservationAuditsResultSetExtractor());
	}

	public class RetreiveAuditsPrepareStmtCreator implements PreparedStatementCreator {
		private String queryString;
		private List queryValues;

		public RetreiveAuditsPrepareStmtCreator(AuditDTO criteria) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuffer sql = new StringBuffer("SELECT tAudit.audit_id, tAudit.task_code, "
					+ " tAudit.timestamp, tAudit.app_code, tAudit.user_id," + " tAudit.content, tTask.task_group_code, "
					+ " tTask.severity_level FROM t_audit tAudit, t_task tTask " + " WHERE tAudit.task_code = tTask.task_code ");

			if ((criteria.getFromDate() != null) && (criteria.getToDate() != null)) {
				criteriaList.add("tAudit.timestamp BETWEEN ? and ? ");
				values.add(criteria.getFromDate());
				values.add(criteria.getToDate());
			}
			if ((criteria.getFromDate() != null) && (criteria.getToDate() == null)) {
				criteriaList.add("tAudit.timestamp >= ? ");
				values.add(criteria.getFromDate());
			}
			if ((criteria.getFromDate() == null) && (criteria.getToDate() != null)) {
				criteriaList.add("tAudit.timestamp <= ? ");
				values.add(criteria.getToDate());
			}
			if ((criteria.getAppCode() != null) && (!criteria.getAppCode().trim().equals(""))) {
				criteriaList.add("tAudit.app_code = ? ");
				values.add(criteria.getAppCode());
			}
			if ((criteria.getUserId() != null) && (!criteria.getUserId().trim().equals(""))) {
				criteriaList.add("tAudit.user_id = ? ");
				values.add(criteria.getUserId());
			}
			if ((criteria.getTaskCode() != null) && (!criteria.getTaskCode().trim().equals(""))) {
				criteriaList.add("tAudit.task_code = ? ");
				values.add(criteria.getTaskCode());
			}
			if ((criteria.getSeverityLevel() != null) && (!criteria.getSeverityLevel().trim().equals(""))) {
				criteriaList.add("tTask.severity_level = ? ");
				values.add(criteria.getSeverityLevel());
			}
			if ((criteria.getTaskGroup() != null) && (!criteria.getTaskGroup().trim().equals(""))) {
				criteriaList.add("tTask.task_group_code = ? ");
				values.add(criteria.getTaskGroup());
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append("AND ");
				sql.append(criteriaList.get(i));
			}
			sql.append("ORDER BY tAudit.audit_id, tAudit.timestamp ");

			this.queryString = sql.toString();
			this.queryValues = values;

			log.debug("Retrieve Audits - Query String [ " + queryString + " ]");
			log.debug("Retrieve Audits - Query Values " + queryValues);
		}

		public RetreiveAuditsPrepareStmtCreator(ReservationAuditDTO criteria) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuffer sql = new StringBuffer("SELECT tAudit.audit_id, tAudit.task_code, "
					+ " tAudit.timestamp, tAudit.app_code, tAudit.user_id," + " tAudit.content, tAudit.user_notes, tAudit.pnr, "
					+ " tAudit.sales_channel_code, tAudit.customer_id, " + " tTask.task_group_code, tTask.severity_level "
					+ " FROM t_reservation_audit tAudit, t_task tTask " + " WHERE tAudit.task_code = tTask.task_code ");

			if ((criteria.getFromDate() != null) && (criteria.getToDate() != null)) {
				criteriaList.add("tAudit.timestamp BETWEEN ? and ? ");
				values.add(criteria.getFromDate());
				values.add(criteria.getToDate());
			}
			if ((criteria.getAppCode() != null) && (!criteria.getAppCode().trim().equals(""))) {
				criteriaList.add("tAudit.app_code = ? ");
				values.add(criteria.getAppCode());
			}
			if ((criteria.getUserId() != null) && (!criteria.getUserId().trim().equals(""))) {
				criteriaList.add("tAudit.user_id = ? ");
				values.add(criteria.getUserId());
			}
			if ((criteria.getTaskCode() != null) && (!criteria.getTaskCode().trim().equals(""))) {
				criteriaList.add("tAudit.task_code = ? ");
				values.add(criteria.getTaskCode());
			}
			if ((criteria.getPnr() != null) && (!criteria.getPnr().equals(""))) {
				criteriaList.add("tAudit.pnr = ? ");
				values.add(criteria.getPnr());
			}
			if (criteria.getCustomerId() != null) {
				criteriaList.add("tAudit.customer_id = ? ");
				values.add(criteria.getCustomerId());
			}
			if (criteria.getSalesChannelCode() != null) {
				criteriaList.add("tAudit.sales_channel_code = ? ");
				values.add(criteria.getSalesChannelCode());
			}
			if ((criteria.getSeverityLevel() != null) && (!criteria.getSeverityLevel().trim().equals(""))) {
				criteriaList.add("tTask.severity_level = ? ");
				values.add(criteria.getSeverityLevel());
			}
			if ((criteria.getTaskGroup() != null) && (!criteria.getTaskGroup().trim().equals(""))) {
				criteriaList.add("tTask.task_group_code = ? ");
				values.add(criteria.getTaskGroup());
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append("AND ");
				sql.append(criteriaList.get(i));
			}
			sql.append("ORDER BY tAudit.audit_id, tAudit.timestamp ");

			this.queryString = sql.toString();
			this.queryValues = values;

			log.debug("Retrieve Reservation Audits - Query String [ " + queryString + " ]");
			log.debug("Retrieve Reservation Audits - Query Values " + queryValues);
		}

		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof Date) {
					// prepStmt.setTimestamp((i + 1),
					// new Timestamp(((Date) value).getTime()));
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}
			return prepStmt;
		}

	}

	public class RetreiveAuditsResultSetExtractor implements ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List audits = new ArrayList();
			while (rs.next()) {
				AuditDTO auditDTO = new AuditDTO();
				auditDTO.setAuditId(new Long(rs.getLong("audit_id")));
				auditDTO.setAuditDate(rs.getDate("timestamp"));
				auditDTO.setAppCode(rs.getString("app_code"));
				auditDTO.setSeverityLevel(rs.getString("severity_level"));
				auditDTO.setTaskCode(rs.getString("task_code"));
				auditDTO.setTaskGroup(rs.getString("task_group_code"));
				auditDTO.setUserId(rs.getString("user_id"));
				audits.add(auditDTO);
			}
			return audits;
		}
	}

	public class RetreiveReservationAuditsResultSetExtractor implements ResultSetExtractor {
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List reservationAudits = new ArrayList();
			while (rs.next()) {
				ReservationAuditDTO reservationAuditDTO = new ReservationAuditDTO();
				reservationAuditDTO.setAuditId(new Long(rs.getLong("audit_id")));
				reservationAuditDTO.setAuditDate(rs.getDate("timestamp"));
				reservationAuditDTO.setAppCode(rs.getString("app_code"));
				reservationAuditDTO.setSeverityLevel(rs.getString("severity_level"));
				reservationAuditDTO.setTaskCode(rs.getString("task_code"));
				reservationAuditDTO.setTaskGroup(rs.getString("task_group_code"));
				reservationAuditDTO.setUserId(rs.getString("user_id"));
				reservationAuditDTO.setPnr(rs.getString("pnr"));
				reservationAuditDTO.setSalesChannelCode(new Long(rs.getLong("sales_channel_code")));
				reservationAuditDTO.setCustomerId(new Long(rs.getLong("customer_id")));
				reservationAudits.add(reservationAuditDTO);
			}
			return reservationAudits;
		}
	}

	/**
	 * Returns modification history
	 * 
	 * @param pnr
	 * @return
	 */
	public Collection getHistory(String pnr) {
		log.debug("Inside getHistory");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection collection = (Collection) jt.query(
				"SELECT mod.content, mod.user_notes, mod.sales_channel_code, tpl.template_desc, mod.user_id, "
						+ " mod.customer_id, mod.timestamp, mod.display_name, tpl.TEMPLATE_CODE "
						+ " FROM T_RESERVATION_AUDIT mod, T_AUDIT_TEMPLATE tpl "
						+ " WHERE mod.template_code = tpl.template_code AND mod.pnr = ? ORDER BY mod.audit_id DESC ",
				new Object[] { pnr }, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Collection collection = new ArrayList();
						ReservationModificationDTO reservationModification;

						if (rs != null) {
							while (rs.next()) {
								reservationModification = new ReservationModificationDTO();

								reservationModification.setDescription(BeanUtils.nullHandler(rs.getString(1)));
								reservationModification.setUserNote(BeanUtils.nullHandler(rs.getString(2)));
								reservationModification.setSalesChannelCode(BeanUtils.parseInteger(rs.getInt(3)));
								reservationModification.setModificationTypeDesc(BeanUtils.nullHandler(rs.getString(4)));
								reservationModification.setUserId(BeanUtils.nullHandler(rs.getString(5)));
								reservationModification.setCustomerId(BeanUtils.parseInteger(rs.getInt(6)));
								reservationModification.setZuluModificationDate(rs.getTimestamp(7));
								reservationModification.setDisplayName(BeanUtils.nullHandler(rs.getString(8)));
								reservationModification.setModificationTypeCode(BeanUtils.nullHandler(rs.getString(9)));
								collection.add(reservationModification);
							}
						}
						return collection;
					}
				});

		log.debug("Exit getHistory");
		return collection;
	}
	
	/**
	 * Return reservation user notes
	 * 
	 * @param pnr
	 * @return collection of reservationModification
	 */
	public Collection getUserNotes(String pnr, boolean isClassifyUN, String agentCode) {
		log.debug("Inside getUserNotes");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		StringBuffer sqlb = new StringBuffer(" SELECT tpl.template_desc, mod.content, mod.timestamp, mod.sales_channel_code, mod.user_id, "
				+ " mod.customer_id, mod.user_notes, mod.user_note_type,mod.display_name "
				+ " FROM T_RESERVATION_AUDIT mod, T_AUDIT_TEMPLATE tpl WHERE mod.template_code = tpl.template_code AND mod.pnr = ? AND mod.user_notes IS NOT NULL ");

		sqlb.append(" AND ((mod.user_note_type='" + AuditorInternalConstants.UserNoteParams.PUBLIC
				+ "' OR mod.user_note_type IS NULL) "); // Includes all public user notes

		if (isClassifyUN) { // Includes private user notes of same agent if privileged
			sqlb.append(" OR ((select usr.agent_code  from t_user usr where usr.user_id = mod.user_id) ='" + agentCode
					+ "' AND mod.user_note_type='" + AuditorInternalConstants.UserNoteParams.PRIVATE + "')");
		}
		sqlb.append(") ORDER BY mod.audit_id DESC ");
		String sql = sqlb.toString();

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + pnr);
		log.debug("############################################");

		Collection userNotes = (Collection) jt.query(sql, new Object[] { pnr }, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection userNotes = new ArrayList();
				ReservationModificationDTO reservationModification;

				if (rs != null) {
					while (rs.next()) {
						reservationModification = new ReservationModificationDTO();

						reservationModification.setModificationTypeDesc(BeanUtils.nullHandler(rs.getString("template_desc")));
						reservationModification.setDescription(BeanUtils.nullHandler(rs.getString("content")));
						reservationModification.setZuluModificationDate(rs.getTimestamp("timestamp"));
						reservationModification.setSalesChannelCode(new Integer(rs.getInt("sales_channel_code")));
						reservationModification.setUserId(BeanUtils.nullHandler(rs.getString("user_id")));
						reservationModification.setCustomerId(new Integer(rs.getInt("customer_id")));
						reservationModification.setUserNote(BeanUtils.nullHandler(rs.getString("user_notes")));
						reservationModification.setUserNoteType(rs.getString("user_note_type"));
						reservationModification.setDisplayName(BeanUtils.nullHandler(rs.getString("display_name")));

						userNotes.add(reservationModification);
					}
				}

				return userNotes;
			}
		});

		log.debug("Exit getUserNotes");
		return userNotes;
	}

	/**
	 * Return the last user note
	 * 
	 * @param pnr
	 * @return
	 */
	public String getLastUserNote(String pnr, boolean classifyFlag, String agentCode) {
		log.debug("Inside getLastUserNote");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
        
		StringBuffer sqlb = new StringBuffer(
				"SELECT mod.user_notes FROM T_RESERVATION_AUDIT mod WHERE mod.pnr = ? AND mod.user_notes IS NOT NULL");

		sqlb.append(" AND  ((mod.user_note_type='" + AuditorInternalConstants.UserNoteParams.PUBLIC
				+ "' OR mod.user_note_type IS NULL) "); // include public user notes of all agents

		if (classifyFlag) { // If classify privileged then select private user notes of same agent
			sqlb.append("OR ((select usr.agent_code  from t_user usr WHERE usr.user_id = mod.user_id) ='" + agentCode
					+ "' AND mod.user_note_type='" + AuditorInternalConstants.UserNoteParams.PRIVATE + "')");
		}
		
		sqlb.append(") ORDER BY audit_id DESC ");
		
		String sql = sqlb.toString();
		
		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + pnr + "," + agentCode);
		log.debug("############################################");

		String userNote = (String) jt.query(sql, new Object[] { pnr }, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String userNote = "";

				if (rs != null) {
					if (rs.next()) {
						userNote = BeanUtils.nullHandler(rs.getString("user_notes"));
					}
				}

				return userNote;
			}
		});

		log.debug("Exit getLastUserNote");
		return userNote;
	}

}
