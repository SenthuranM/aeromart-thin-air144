package com.isa.thinair.auditor.core.bl;

import java.util.ArrayList;
import java.util.Collection;

public class AuditorBLUtil {

	public static int maxlength = 2000;

	public static String PARAM_SEPARATOR = "||";

	public AuditorBLUtil(int maxlengthConstraint) {
		maxlength = maxlengthConstraint;
	}

	public Collection splitString(String content) {

		Collection list = new ArrayList();

		if (content.length() > maxlength) {

			String data[] = getEndIndex(content, maxlength);
			list.add(data[0]);

			if (data[1].length() > maxlength) {
				list.addAll(splitString(data[1]));
			} else {
				list.add(data[1]);
			}
		} else {
			list.add(content);
		}

		return list;
	}

	private String[] getEndIndex(String newContent, int strlength) {

		String dividedString = newContent.substring(0, strlength);
		int endIndex = dividedString.lastIndexOf(PARAM_SEPARATOR);

		// if(endIndex<0){
		// strlength = strlength+1;
		// return getEndIndex(newContent, strlength);
		// }
		if (endIndex != -1 && (endIndex + PARAM_SEPARATOR.length()) != strlength) {
			dividedString = newContent.substring(0, endIndex + PARAM_SEPARATOR.length());
			strlength = endIndex + PARAM_SEPARATOR.length();
		}

		String balanceString = newContent.substring(strlength);
		return new String[] { dividedString, balanceString };

	}

}
