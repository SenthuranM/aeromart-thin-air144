package com.isa.thinair.auditor.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nasly
 * @isa.module.config-bean
 */
public class AuditorConfig extends DefaultModuleConfig {

	private int auditWidth;

	/**
	 * returns the auditWidth
	 * 
	 * @return Returns the auditWidth.
	 * 
	 */
	public int getAuditWidth() {
		return auditWidth;
	}

	/**
	 * sets the auditWidth
	 * 
	 * @param auditWidth
	 *            The auditWidth to set.
	 */
	public void setAuditWidth(int auditWidth) {
		this.auditWidth = auditWidth;
	}

}
