/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.auditor.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.model.AgentAudit;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.AuditTemplate;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.dto.Page;

public interface AuditorDAO {

	public void createAudit(Audit audit);

	public void createAudit(ReservationAudit reservationAudit);

	public void createAudit(PFSAudit pFSAudit);

	public void deleteAudits(Collection<PFSAudit> pFSAudits);

	public void createAudit(NameChangeChargeAudit nameChangeChargeAudit);

	public void createAudit(InventoryAudit inventoryAudit);

	public Page getAudits(List criteria, int startIndex, int pageSize, List orderByFields);

	public Page getReservationAudits(List criteria, int startIndex, int pageSize, List orderByFields);

	public List getAudits(String pnr);

	public AuditTemplate getAuditTemplate(String auditorCode);

	public Collection<InventoryAudit> getInventoryAudit(Integer flightId);

	public void createAudit(AgentAudit agentAudit);

	public Collection<PFSAuditDTO> retrievePFSAudits(long pfsID);

	public Collection<PFSAudit> getPFSAudits(long pfsID);
	
	// Added for AARESAA-24971 implementation requirement
	public void updateReservationAudit(ReservationAudit reservationAudit);

	public ReservationAudit getLastReservationAudit(String pnr);
}
