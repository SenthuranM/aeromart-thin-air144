package com.isa.thinair.auditor.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationAuditDTO;

public interface AuditorJdbcDAO {

	public Collection retrieveAudits(AuditDTO auditSearchCriteria);

	public Collection retrieveAudits(ReservationAuditDTO reservationAuditSearchCriteria);

	/**
	 * Returns modification history (FAST LANE READER)
	 * 
	 * @param pnr
	 * @return
	 */
	public Collection getHistory(String pnr);

	/**
	 * Returns modifications that has only user notes (FAST LANE READER)
	 * 
	 * @param pnr
	 * @param isClassifyUN TODO
	 * @param agentCode TODO
	 * @return
	 */
	public Collection getUserNotes(String pnr, boolean isClassifyUN, String agentCode);

	/**
	 * Return the last user note (FAST LANE READER)
	 * 
	 * @param pnr
	 * @param classifyFlag TODO
	 * @param agentCode TODO
	 * @return
	 */
	public String getLastUserNote(String pnr, boolean classifyFlag, String agentCode);
}
