/**
 * 
 */
package com.isa.thinair.auditor.api.model;

import java.util.Date;
import java.util.LinkedHashMap;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author nafly
 * @hibernate.class table = "T_AGENT_AUDIT"
 */
public class AgentAudit extends Persistent {

	private static final long serialVersionUID = 5079090397522491795L;

	private Long auditId;

	private String templateCode;

	private Date timestamp;

	private String userId;

	private String content;

	private String agentCode;

	private Integer daysPriorToExpire;

	private Double creditLimitUtilization;

	private String paymentCode;

	private LinkedHashMap contentMap = new LinkedHashMap();

	private int defaultMessageValidity;

	/**
	 * @hibernate.id column = "AGENT_AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AGENT_AUDIT"
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the templateCode
	 * @hibernate.property column = "TEMPLATE_CODE"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the timestamp
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the content
	 * @hibernate.property column = "CONTENT"
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the daysPriorToExpire
	 * @hibernate.property column = "DAYS_PRIOR_EXP"
	 */
	public Integer getDaysPriorToExpire() {
		return daysPriorToExpire;
	}

	/**
	 * @param daysPriorToExpire
	 *            the daysPriorToExpire to set
	 */
	public void setDaysPriorToExpire(Integer daysPriorToExpire) {
		this.daysPriorToExpire = daysPriorToExpire;
	}

	/**
	 * @return the creditLimitUtilization
	 * @hibernate.property column = "CREDIT_LIMIT_PERCENTAGE"
	 */
	public Double getCreditLimitUtilization() {
		return creditLimitUtilization;
	}

	/**
	 * @param creditLimitUtilization
	 *            the creditLimitUtilization to set
	 */
	public void setCreditLimitUtilization(Double creditLimitUtilization) {
		this.creditLimitUtilization = creditLimitUtilization;
	}

	/**
	 * @return the paymentCode
	 * @hibernate.property column = "PAYMENT_CODE"
	 */
	public String getPaymentCode() {
		return paymentCode;
	}

	/**
	 * @param paymentCode
	 *            the paymentCode to set
	 */
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	/**
	 * @return Returns the contentMap.
	 */
	public LinkedHashMap getContentMap() {
		return contentMap;
	}

	/**
	 * @param contentMap
	 *            The contentMap to set.
	 */
	public void addContentMap(String key, String description) {
		this.getContentMap().put(key, description);
	}

	/**
	 * @param contentMap
	 *            The contentMap to set.
	 */
	private void setContentMap(LinkedHashMap contentMap) {
		this.contentMap = contentMap;
	}

	/**
	 * @return the defaultMessageValidity
	 */
	public int getDefaultMessageValidity() {
		return defaultMessageValidity;
	}

	/**
	 * @param defaultMessageValidity
	 *            the defaultMessageValidity to set
	 */
	public void setDefaultMessageValidity(int defaultMessageValidity) {
		this.defaultMessageValidity = defaultMessageValidity;
	}

}
