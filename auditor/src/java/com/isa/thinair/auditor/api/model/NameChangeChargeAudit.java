package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * To keep track of Name change charge details
 * 
 * @author Jagath
 * @hibernate.class table = "T_NAME_CHANGE_CHARGE_AUDIT"
 */
public class NameChangeChargeAudit implements Serializable {

	private static final long serialVersionUID = 4625861044066933131L;

	private Long nameChangeChargeAuditId;

	private Date createdDate;

	private String pnr;

	private Integer salesChannelCode;

	private String agentCode;

	private Integer nameChangePaxCount;

	private Set<Integer> pnrSegmentIds;

	/**
	 * @hibernate.id column = "NAME_CHANGE_CHARGE_AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_NAME_CHANGE_CHARGE_AUDIT"
	 */
	public Long getNameChangeChargeAuditId() {
		return nameChangeChargeAuditId;
	}

	public void setNameChangeChargeAuditId(Long nameChangeChargeAuditId) {
		this.nameChangeChargeAuditId = nameChangeChargeAuditId;
	}

	/**
	 * @return Returns the pnr
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the createdDate
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return Returns the salesChannelCode
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the nameChangePaxCount
	 * @hibernate.property column = "NAME_CHANGE_PAX_COUNT"
	 */
	public Integer getNameChangePaxCount() {
		return nameChangePaxCount;
	}

	public void setNameChangePaxCount(Integer nameChangePaxCount) {
		this.nameChangePaxCount = nameChangePaxCount;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_NAME_CHANGE_CHARGE_AUDIT_SEG"
	 * @hibernate.collection-element column="PNR_SEG_ID" type="java.lang.Integer"
	 * @hibernate.collection-key column="NAME_CHANGE_CHARGE_AUDIT_ID"
	 */
	public Set<Integer> getPnrSegmentIds() {
		return pnrSegmentIds;
	}

	public void setPnrSegmentIds(Set<Integer> pnrSegmentIds) {
		this.pnrSegmentIds = pnrSegmentIds;
	}

}
