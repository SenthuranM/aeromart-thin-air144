package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author MN
 * @hibernate.class table="T_TASK_GROUP"
 */
public class TaskGroup implements Serializable {

	private static final long serialVersionUID = -3697803228090086223L;

	private Long taskGroupCode;
	private String description;
	private Set tasks;

	public TaskGroup(Long taskGroupCode, String description, Set tasks) {
		this.taskGroupCode = taskGroupCode;
		this.description = description;
		this.tasks = tasks;
	}

	public TaskGroup() {
	}

	public TaskGroup(Long taskGroupCode, Set tasks) {
		this.taskGroupCode = taskGroupCode;
		this.tasks = tasks;
	}

	/**
	 * @hibernate.id generator-class="assigned" type="java.lang.Long" column="TASK_GROUP_CODE"
	 */
	public Long getTaskGroupCode() {
		return this.taskGroupCode;
	}

	public void setTaskGroupCode(Long taskGroupCode) {
		this.taskGroupCode = taskGroupCode;
	}

	/**
	 * @hibernate.property column="DESCRIPTION"
	 */
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @hibernate.set lazy="false" cascade="none"
	 * @hibernate.collection-key column="TASK_GROUP_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.auditor.api.model.Task"
	 */
	public Set getTasks() {
		return this.tasks;
	}

	public void setTasks(Set tasks) {
		this.tasks = tasks;
	}

	public String toString() {
		return new ToStringBuilder(this).append("taskGroupCode", getTaskGroupCode()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof TaskGroup))
			return false;
		TaskGroup castOther = (TaskGroup) other;
		return new EqualsBuilder().append(this.getTaskGroupCode(), castOther.getTaskGroupCode()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTaskGroupCode()).toHashCode();
	}
}
