package com.isa.thinair.auditor.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.ancilaryreminder.FlightSegReminderNotificationDTO;

public class AncillaryReminderAuditDTO implements Serializable {

	private static final long serialVersionUID = -5314195721433604700L;

	private String pnr;
	private String qrtzJobName;
	private String qrtzJobGroupName;
	private String userId;
	private Integer flightSegmentId;
	private Integer flightSegNotificationId;
	private Integer salesChannelCode;
	private List<FlightSegReminderNotificationDTO> flightSegNotifyDTOList = new ArrayList<FlightSegReminderNotificationDTO>();

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the flightSegmentId
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            the flightSegmentId to set
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return the flightSegNotificationId
	 */
	public Integer getFlightSegNotificationId() {
		return flightSegNotificationId;
	}

	/**
	 * @param flightSegNotificationId
	 *            the flightSegNotificationId to set
	 */
	public void setFlightSegNotificationId(Integer flightSegNotificationId) {
		this.flightSegNotificationId = flightSegNotificationId;
	}

	/**
	 * @return the flightSegNotifyDTOList
	 */
	public List<FlightSegReminderNotificationDTO> getFlightSegNotifyDTOList() {
		return flightSegNotifyDTOList;
	}

	/**
	 * @param flightSegNotifyDTOList
	 *            the flightSegNotifyDTOList to set
	 */
	public void setFlightSegNotifyDTOList(List<FlightSegReminderNotificationDTO> flightSegNotifyDTOList) {
		this.flightSegNotifyDTOList = flightSegNotifyDTOList;
	}

	/**
	 * @return the qrtzJobName
	 */
	public String getQrtzJobName() {
		return qrtzJobName;
	}

	/**
	 * @param qrtzJobName
	 *            the qrtzJobName to set
	 */
	public void setQrtzJobName(String qrtzJobName) {
		this.qrtzJobName = qrtzJobName;
	}

	/**
	 * @return the qrtzJobGroupName
	 */
	public String getQrtzJobGroupName() {
		return qrtzJobGroupName;
	}

	/**
	 * @param qrtzJobGroupName
	 *            the qrtzJobGroupName to set
	 */
	public void setQrtzJobGroupName(String qrtzJobGroupName) {
		this.qrtzJobGroupName = qrtzJobGroupName;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the salesChannelCode
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

}
