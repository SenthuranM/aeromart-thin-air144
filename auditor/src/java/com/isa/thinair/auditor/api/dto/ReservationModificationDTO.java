/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.auditor.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;

/**
 * To keep track of reservation modifications
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationModificationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1322072676247207828L;

	/** Holds the pnr number */
	private String pnr;

	/** Holds the modification description */
	private String description;

	/** Holds additional user note */
	private String userNote;
	
	/** Holds user note type*/
	private String userNoteType;

	/** Holds the customer (web) id */
	private Integer customerId;

	/** Holds the modification id */
	private Integer modificationId;

	/** Holds the sales channel code */
	private Integer salesChannelCode;

	/** Holds the user (Air Line) id */
	private String userId;

	/** Holds the zulu modification date */
	private Date zuluModificationDate;

	/** Holds the local modification date */
	private Date localModificationDate;

	/** Holds the modification type description */
	private String modificationTypeDesc;

	/** Holds the modification type code */
	private String modificationTypeCode;

	/** Holds the display name of the user who is responsible for the audit */
	private String displayName;
	/**
	 * @return Returns the customerId.
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the modificationId.
	 */
	public Integer getModificationId() {
		return modificationId;
	}

	/**
	 * @param modificationId
	 *            The modificationId to set.
	 */
	public void setModificationId(Integer modificationId) {
		this.modificationId = modificationId;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the salesChannelCode.
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the userNote.
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            The userNote to set.
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}
	
	/**
	 * @return Returns the userNoteType.
	 */
	public String getUserNoteType() {
		return userNoteType;
	}
    
	/**
	 * @param userNoteType
	 *            The userNoteType to set.
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}


	/**
	 * @return Returns the localModificationDate.
	 */
	public Date getLocalModificationDate() {
		return localModificationDate;
	}

	/**
	 * @param localModificationDate
	 *            The localModificationDate to set.
	 */
	public void setLocalModificationDate(Date localModificationDate) {
		this.localModificationDate = localModificationDate;
	}

	/**
	 * @return Returns the zuluModificationDate.
	 */
	public Date getZuluModificationDate() {
		return zuluModificationDate;
	}

	/**
	 * @param zuluModificationDate
	 *            The zuluModificationDate to set.
	 */
	public void setZuluModificationDate(Date zuluModificationDate) {
		this.zuluModificationDate = zuluModificationDate;
	}

	/**
	 * Return the modification date
	 * 
	 * @param dateFormat
	 * @param displayConversion
	 * @return
	 */
	public String getModificationDateString(String dateFormat, boolean displayConversion) {
		return ReservationApiUtils.getConvertedDate(this.getZuluModificationDate(), this.getLocalModificationDate(), dateFormat,
				displayConversion);
	}

	/**
	 * @return Returns the modificationTypeCode.
	 */
	public String getModificationTypeCode() {
		return modificationTypeCode;
	}

	/**
	 * @param modificationTypeCode
	 *            The modificationTypeCode to set.
	 */
	public void setModificationTypeCode(String modificationTypeCode) {
		this.modificationTypeCode = modificationTypeCode;
	}

	/**
	 * @return Returns the modificationTypeDesc.
	 */
	public String getModificationTypeDesc() {
		return modificationTypeDesc;
	}

	/**
	 * @param modificationTypeDesc
	 *            The modificationTypeDesc to set.
	 */
	public void setModificationTypeDesc(String modificationTypeDesc) {
		this.modificationTypeDesc = modificationTypeDesc;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
