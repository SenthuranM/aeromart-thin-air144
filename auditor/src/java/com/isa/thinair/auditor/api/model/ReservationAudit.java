package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * To keep track of reservation audits
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_RESERVATION_AUDIT"
 */
public class ReservationAudit extends Persistent implements Serializable {

	private static final long serialVersionUID = 5079090391582091795L;

	/** Holds the audit id */
	private Long auditId;

	/** Holds the modification type */
	private String modificationType;

	/** Holds additional user note */
	private String userNote;

	/** Holds the pnr number */
	private String pnr;

	/** Holds the customer (web) id */
	private Integer customerId;

	/** Holds the user (Air Line) id */
	private String userId;

	/** Holds the modification description */
	private String description;

	/** Holds the sales channel code */
	private Integer salesChannelCode;

	/** Holds the zulu modification date */
	private Date zuluModificationDate;

	/** Holds the application code */
	private String appCode;

	/** Holds the current display name of the user **/
	private String displayName;

	/** Holds the content map */
	private LinkedHashMap contentMap = new LinkedHashMap();
	
	/** Holds the user note type **/
	private String userNoteType;

	private static final String REMOTE_USER_KEY = "remoteUser";

	/**
	 * Creates a Reservation Audit with default values
	 * 
	 * @param reservationAudit
	 * @param pnr
	 * @param userNotes
	 * @param credentialsDTO
	 */
	public static void createReservationAudit(ReservationAudit reservationAudit, String pnr, String userNotes,
			CredentialsDTO credentialsDTO) {
		reservationAudit.setPnr(pnr);
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setZuluModificationDate(new Date());
		reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
		reservationAudit.setUserId(credentialsDTO.getUserId());
		reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));

		if (SalesChannelsUtil.isSalesChannelNOTWebOrMobile(credentialsDTO.getSalesChannelCode())) {
			addRemoteUserAudit(reservationAudit, credentialsDTO);
		}
	}

	/**
	 * Creates a Reservation Audit with default values
	 * 
	 * @param reservationAudit
	 * @param credentialsDTO
	 * @param isGoShowProcess
	 *            TODO
	 */
	public static void createReservationAudit(ReservationAudit reservationAudit, CredentialsDTO credentialsDTO,
			Boolean isGoShowProcess) {
		reservationAudit.setZuluModificationDate(new Date());
		reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
		reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));

		if (isGoShowProcess && AppSysParamsUtil.isAllowSetOriginUserAsSchedulerForGoShowBookings()) {
			reservationAudit.setUserId(User.SCHEDULER_USER_ID);
		} else if(reservationAudit.getUserId() == null || reservationAudit.getUserId().isEmpty()){
			reservationAudit.setUserId(credentialsDTO.getUserId());
		}
		if (SalesChannelsUtil.isSalesChannelNOTWebOrMobile(credentialsDTO.getSalesChannelCode())) {
			addRemoteUserAudit(reservationAudit, credentialsDTO);
		}
	}

	private static void addRemoteUserAudit(ReservationAudit reservationAudit, CredentialsDTO credentialsDTO) {
		if (credentialsDTO != null) {
			reservationAudit.getContentMap().put(
					REMOTE_USER_KEY,
					credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
							+ credentialsDTO.getUserId() + "]");
		}
	}

	/**
	 * @hibernate.id column = "AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_RESERVATION_AUDIT"
	 */
	public Long getAuditId() {
		return this.auditId;
	}

	/**
	 * 
	 * @param auditId
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	public String toString() {
		return new ToStringBuilder(this).append("auditId", getAuditId()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Audit))
			return false;
		ReservationAudit castOther = (ReservationAudit) other;
		return new EqualsBuilder().append(this.getAuditId(), castOther.getAuditId()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAuditId()).toHashCode();
	}

	/**
	 * @return Returns the appCode.
	 * @hibernate.property column = "APP_CODE"
	 */
	public String getAppCode() {
		return appCode;
	}

	/**
	 * @param appCode
	 *            The appCode to set.
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "CONTENT"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the modificationType.
	 * @hibernate.property column = "TEMPLATE_CODE" not-null = "true"
	 */
	public String getModificationType() {
		return modificationType;
	}

	/**
	 * @param modificationType
	 *            The modificationType to set.
	 */
	public void setModificationType(String modificationType) {
		this.modificationType = modificationType;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the userNote.
	 * @hibernate.property column = "USER_NOTES"
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            The userNote to set.
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @return Returns the zuluModificationDate.
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getZuluModificationDate() {
		return zuluModificationDate;
	}

	/**
	 * @param zuluModificationDate
	 *            The zuluModificationDate to set.
	 */
	public void setZuluModificationDate(Date zuluModificationDate) {
		this.zuluModificationDate = zuluModificationDate;
	}

	/**
	 * @return Returns the contentMap.
	 */
	public LinkedHashMap getContentMap() {
		return contentMap;
	}

	/**
	 * @param contentMap
	 *            The contentMap to set.
	 */
	public void addContentMap(String key, String description) {
		this.getContentMap().put(key, description);
	}

	/**
	 * @param contentMap
	 *            The contentMap to set.
	 */
	private void setContentMap(LinkedHashMap contentMap) {
		this.contentMap = contentMap;
	}

	/**
	 * @return Returns the Display Name
	 * @hibernate.property column = "DISPLAY_NAME"
	 */
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
    
	/**
	 * @return Returns the userNoteType
	 * @hibernate.property column = "USER_NOTE_TYPE"
	 */
	public String getUserNoteType() {
		return userNoteType;
	}
	/**
	 * @param userNoteType
	 *            The userNoteType to set.
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

}
