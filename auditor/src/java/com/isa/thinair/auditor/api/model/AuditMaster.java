/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

public class AuditMaster extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5040661400071656838L;

	private Long auditId;

	private String taskCode;

	private Date timestamp;

	private String appCode;

	private String userId;

	private String content;

	public AuditMaster() {

	}

	/**
	 * 
	 * @param taskCode
	 * @param timestamp
	 * @param appCode
	 * @param userId
	 * @param content
	 */
	public AuditMaster(String taskCode, Date timestamp, String appCode, String userId, String content) {
		setTaskCode(taskCode);
		setTimestamp(timestamp);
		setAppCode(appCode);
		setUserId(userId);
		setContent(content);
	}

	/**
	 * @hibernate.id column = "AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AUDIT"
	 * 
	 */
	public Long getAuditId() {
		return this.auditId;
	}

	/**
	 * 
	 * @param auditId
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimestamp() {
		return this.timestamp;
	}

	/**
	 * 
	 * @param timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * 
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "APP_CODE"
	 */
	public String getAppCode() {
		return this.appCode;
	}

	/**
	 * 
	 * @param appCode
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TASK_CODE" not-null = "true"
	 */
	public String getTaskCode() {
		return this.taskCode;
	}

	/**
	 * 
	 * @param taskCode
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CONTENT"
	 */
	public String getContent() {
		return this.content;
	}

	/**
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
