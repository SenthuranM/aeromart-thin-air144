package com.isa.thinair.auditor.api.model;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @hibernate.class table = "T_AUDIT"
 */
public class Audit extends AuditMaster {

	private static final long serialVersionUID = -5392641732837666461L;

	public Audit() {
		super();
	}

	/**
	 * 
	 * @param taskCode
	 * @param timestamp
	 * @param appCode
	 * @param userId
	 * @param content
	 */
	public Audit(String taskCode, Date timestamp, String appCode, String userId, String content) {
		super(taskCode, timestamp, appCode, userId, content);
	}

	public String toString() {
		return new ToStringBuilder(this).append("auditId", getAuditId()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Audit))
			return false;
		Audit castOther = (Audit) other;
		return new EqualsBuilder().append(this.getAuditId(), castOther.getAuditId()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAuditId()).toHashCode();
	}
}
