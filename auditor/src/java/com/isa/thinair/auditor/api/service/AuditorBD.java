/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.auditor.api.service;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationAuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.model.NameChangeChargeAudit;
import com.isa.thinair.auditor.api.model.PFSAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @isa.module.bd-intf id="auditor.service"
 */
public interface AuditorBD {

	public void audit(Audit audit, LinkedHashMap contents) throws ModuleException;

	public void audit(PFSAudit pfsAudit) throws ModuleException;

	public void audit(NameChangeChargeAudit nameChangeChargeAudit) throws ModuleException;

	public Collection<PFSAuditDTO> retrievePFSAudits(long pfsID) throws ModuleException;

	public Collection retrieveAudits(AuditDTO auditSearchCriteria) throws ModuleException;

	public Page getAudits(List criteria, int startIndex, int pageSize, List orderByFields) throws ModuleException;

	public Page getReservationAudits(List criteria, int startIndex, int pageSize, List orderByFields) throws ModuleException;

	public List getAudits(String pnr) throws ModuleException;

	public void audit(ReservationAudit reservationAudit, LinkedHashMap contents) throws ModuleException;

	/**
	 * Audit Reservation information
	 * 
	 * @param collectionOfAudits
	 * @throws ModuleException
	 */
	public void audit(Collection collectionOfAudits) throws ModuleException;

	public Collection retrieveAudits(ReservationAuditDTO reservationAuditSearchCriteria) throws ModuleException;

	/**
	 * Return the last user note
	 * 
	 * @param pnr
	 * @param classifyFlag TODO
	 * @param agent TODO
	 * @return
	 */
	public String getLastUserNote(String pnr, boolean classifyFlag, String agent) throws ModuleException;

	/**
	 * Return last user notes
	 * 
	 * @param pnr
	 * @param isClassifyUN TODO
	 * @param agentCode TODO
	 * @return
	 */
	public Collection<ReservationModificationDTO> getUserNotes(String pnr, boolean isClassifyUN, String agentCode)
			throws ModuleException;

	/**
	 * Return the pnr history
	 * 
	 * @param pnr
	 * @return
	 */
	public Collection getHistory(String pnr) throws ModuleException;

	/**
	 * Audits inventory seat movements
	 * 
	 * @param inventoryAudits
	 */
	public void auditInventory(Collection inventoryAudits) throws ModuleException;

	public Collection<InventoryAudit> getInventoryAudit(Integer flightId);

	public void deleteAuditsByID(long pfsID);

	public void preparePFSAudit(PFSAuditDTO pfsAuditDTO, long pfsID, String action, String systemNote) throws ModuleException;

	public void updateReservationAudit(ReservationAudit reservationAudit);

	public ReservationAudit getLastReservationAudit(String pnr);
	
	public void doTaxInvoiceAudit(String pnr, List<String> invoiceNumbers, CredentialsDTO credentialDTO) throws ModuleException;
}
