package com.isa.thinair.auditor.api.service;

import javax.sql.DataSource;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class AuditorModuleUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("auditor");
	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}
}
