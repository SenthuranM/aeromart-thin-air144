package com.isa.thinair.auditor.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table="T_AUDIT_TEMPLATE"
 * 
 */
public class AuditTemplate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -697412085184883686L;
	String templateCode;
	String templateDesc;
	String templateContent;

	/**
	 * returns the alertTemplateId
	 * 
	 * @return Returns the alertTemplateId.
	 * 
	 * @hibernate.id column = "TEMPLATE_CODE" generator-class = "assigned"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * sets the alertTemplateId
	 * 
	 * @param alertTemplateId
	 *            The alertTemplateId to set.
	 */
	private void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * returns the alertTemplateContent
	 * 
	 * @return Returns the alertTemplateContent.
	 * 
	 * @hibernate.property column = "TEMPLATE_CONTENT"
	 */
	public String getTemplateContent() {
		return templateContent;
	}

	/**
	 * sets the alertTemplateContent
	 * 
	 * @param alertTemplateContent
	 *            The alertTemplateContent to set.
	 */
	private void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	/**
	 * returns the alertTemplateDesc
	 * 
	 * @return Returns the alertTemplateDesc.
	 * 
	 * @hibernate.property column = "TEMPLATE_DESC"
	 */
	public String getTemplateDesc() {
		return templateDesc;
	}

	/**
	 * sets the alertTemplateDesc
	 * 
	 * @param alertTemplateDesc
	 *            The alertTemplateDesc to set.
	 */
	private void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	}
}