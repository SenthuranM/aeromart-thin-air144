package com.isa.thinair.auditor.api.dto;

import java.io.Serializable;

public class OnlineCheckInReminderAuditDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pnr;
	private String qrtzJobName;
	private String qrtzJobGroupName;
	private String userId;
	private Integer flightSegmentId;
	private Integer flightSegNotificationId;
	private Integer salesChannelCode;
	private Integer pnrSegId;
	private Integer pnrSegNotificationEventId;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getQrtzJobName() {
		return qrtzJobName;
	}

	public void setQrtzJobName(String qrtzJobName) {
		this.qrtzJobName = qrtzJobName;
	}

	public String getQrtzJobGroupName() {
		return qrtzJobGroupName;
	}

	public void setQrtzJobGroupName(String qrtzJobGroupName) {
		this.qrtzJobGroupName = qrtzJobGroupName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	public Integer getFlightSegNotificationId() {
		return flightSegNotificationId;
	}

	public void setFlightSegNotificationId(Integer flightSegNotificationId) {
		this.flightSegNotificationId = flightSegNotificationId;
	}

	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegNotificationEventId(Integer pnrSegNotificationEventId) {
		this.pnrSegNotificationEventId = pnrSegNotificationEventId;
	}

	public Integer getPnrSegNotificationEventId() {
		return pnrSegNotificationEventId;
	}

}
