package com.isa.thinair.auditor.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_PFS_AUDIT"
 */
public class PFSAudit extends Persistent implements Serializable {

	private long pfsAuditID;
	private long pfsID;
	private Date timeStampZulu;
	private String action;
	private String userNote;
	private String systemNote;
	private String userName;
	private String ipAddress;

	public void setPfsAuditID(long pfsAuditID) {
		this.pfsAuditID = pfsAuditID;
	}

	/**
	 * @hibernate.id column = "PFS_AUDIT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PFS_AUDIT"
	 */
	public long getPfsAuditID() {
		return pfsAuditID;
	}

	public void setPfsID(long pfsID) {
		this.pfsID = pfsID;
	}

	/**
	 * @hibernate.property column = "PFS_ID"
	 */
	public long getPfsID() {
		return pfsID;
	}

	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @hibernate.property column = "ACTION"
	 */
	public String getAction() {
		return action;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @hibernate.property column = "USER_NOTE"
	 */
	public String getUserNote() {
		return userNote;
	}

	public void setSystemNote(String systemNote) {
		this.systemNote = systemNote;
	}

	/**
	 * @hibernate.property column = "SYSTEM_NOTE"
	 */
	public String getSystemNote() {
		return systemNote;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @hibernate.property column = "USER_NAME"
	 */
	public String getUserName() {
		return userName;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @hibernate.property column = "IP_ADDRESS"
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return the timeStampZulu
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimeStampZulu() {
		return timeStampZulu;
	}

	/**
	 * @param timeStampZulu
	 *            the timeStampZulu to set
	 */
	public void setTimeStampZulu(Date timeStampZulu) {
		this.timeStampZulu = timeStampZulu;
	}

}