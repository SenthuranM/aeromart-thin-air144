package com.isa.thinair.auditor.api.dto;

import java.io.Serializable;

/**
 * @author eric
 * 
 */
public class PassengerTicketCouponAuditDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pnr;

	private String pnrPaxId;

	private String passengerName;

	private String remark;
	
	private Integer eTicketId;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer geteTicketId() {
		return eTicketId;
	}

	public void seteTicketId(Integer eTicketId) {
		this.eTicketId = eTicketId;
	}

}
