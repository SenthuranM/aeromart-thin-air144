package com.isa.thinair.auditor.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorModuleUtils;
import com.isa.thinair.auditor.core.persistence.dao.AuditorDAO;
import com.isa.thinair.auditor.core.util.AuditorInternalConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAuditorDAO extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

    public void testCreateAudit() {
        Audit audit = new Audit("TSK002", new Date(System.currentTimeMillis()), 
                "APPCODE_TEST_2", null, "CONTENT_TEST_2");
        getAuditorDAO().createAudit(audit);
    }
        
    public void testCreateReservationAudit() {        
        //FIXME
//        ReservationAudit reservationAudit = new ReservationAudit("TSK002", 
//                new Date(System.currentTimeMillis()), 
//                "APPCODE_RESTEST_1", null, "CONTENT_RESTEST_1", 
//                "USER_NOTES_TEST_1", "QWERTY", null, null);        
//        getAuditorDAO().createAudit(reservationAudit);
    }
   
    public void testGetAudits() {
        ModuleCriterion criterion = new ModuleCriterion();
        criterion.setFieldName("appCode");
        criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
        List values = new ArrayList();
        values.add("APPCODE%");
        criterion.setValue(values);
        
        List criteria = new ArrayList();
        criteria.add(criterion);
        
        List list = new ArrayList();
        list.add("auditId");
        
        Collection data = getAuditorDAO().getAudits(
                criteria, 0, 3, list).getPageData();
        assertNotNull(data);
        assertEquals(4, data.size());
    }
    
    public void testGetReservationAudits() {
        ModuleCriterion criterion = new ModuleCriterion();
        criterion.setFieldName("appCode");
        criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
        List values = new ArrayList();
        values.add("APPCODE%");
        criterion.setValue(values);
        
        List criteria = new ArrayList();
        criteria.add(criterion);
        
        List list = new ArrayList();
        list.add("timestamp");
        
        Collection data = getAuditorDAO().getReservationAudits(
                criteria, 0, 3, list).getPageData();
        
        assertNotNull(data);
        assertEquals(4, data.size());
    }
    
    public void testGetAuditsForPnr() {
        String pnr = "QWERTY";
        List listData = getAuditorDAO().getAudits(pnr);
        assertNotNull(listData);        
        assertEquals(4, listData.size());
    }
    
	private AuditorDAO getAuditorDAO() {
		return (AuditorDAO) AuditorModuleUtils.getInstance().getLocalBean(
                AuditorInternalConstants.DAOProxyNames.AUDITOR_DAO);
	}
}
