package com.isa.thinair.auditor.core.persistence.jdbc;

import java.util.Collection;
import java.util.GregorianCalendar;

import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.dto.ReservationAuditDTO;
import com.isa.thinair.auditor.api.service.AuditorModuleUtils;
import com.isa.thinair.auditor.core.persistence.dao.AuditorJdbcDAO;
import com.isa.thinair.auditor.core.util.AuditorInternalConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAuditorJdbcDAO extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

    public void testRetrieveAudits() {
        AuditDTO criteria = new AuditDTO();
        GregorianCalendar fromDateG = new GregorianCalendar(2005, 11, 01, 0, 0);
        GregorianCalendar toDateG = new GregorianCalendar(2005, 11, 20, 0, 15);
        criteria.setFromDate(fromDateG.getTime());
        criteria.setToDate(toDateG.getTime());
        criteria.setAppCode("APPCODE_TEST_2");
        criteria.setUserId(null);
        criteria.setSeverityLevel("INFO");
        criteria.setTaskCode("TSK002");
        criteria.setTaskGroup("TSKG001");        
        Collection results = getAuditorJdbcDAO().retrieveAudits(criteria);
        assertNotNull(results);
        assertEquals(0, results.size());
    }
    
    public void testReservationRetrieveAudits() {
        ReservationAuditDTO criteria = new ReservationAuditDTO();
        GregorianCalendar fromDateG = new GregorianCalendar(2005, 11, 01, 0, 0);
        GregorianCalendar toDateG = new GregorianCalendar(2005, 11, 20, 0, 15);
        /*criteria.setFromDate(fromDateG.getTime());
        criteria.setToDate(toDateG.getTime());
        criteria.setAppCode("APPCODE_RESTEST_1");
        criteria.setUserId(null);
        criteria.setSeverityLevel("INFO");
        criteria.setTaskCode("TSK002");
        criteria.setTaskGroup("TSKG001");*/
        criteria.setPnr("QWERTY");
        criteria.setSalesChannelCode(new Long("1"));        
        criteria.setCustomerId(null);
        Collection results = getAuditorJdbcDAO().retrieveAudits(criteria);
        assertNotNull(results);
        assertEquals(4, results.size());
    }   
    
	private AuditorJdbcDAO getAuditorJdbcDAO() {
		return (AuditorJdbcDAO) AuditorModuleUtils.getInstance().getLocalBean(
                AuditorInternalConstants.DAOProxyNames.AUDITOR_JDBC_DAO);
	}
}
