package com.isa.thinair.auditor.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.dto.AuditDTO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAuditorServiceRemote extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

    public void testCreateAudit() throws Exception {
        LinkedHashMap contents = new LinkedHashMap();
        String singleContent = "value single content";
        contents.put("key audit test", singleContent);
        
        Audit audit = new Audit("TSK002", new Date(System.currentTimeMillis()), 
                "APPCODE_TEST_3", null, null);
        getAuditorService().audit(audit, contents);
    }
        
    public void testCreateReservationAudit() throws Exception { 
        
        LinkedHashMap contents = new LinkedHashMap();
        
        String[] contentSeg = new String[] {"new seg 1 <> new seg 2", "old seg 1 <> old seg 2"};
        String[] contentPax = new String[] {"new name" , "old name"};
        contents.put("Seg", contentSeg);
        contents.put("Pax", contentPax);
        
        //FIXME
//        ReservationAudit reservationAudit = new ReservationAudit("TSK002", 
//                new Date(System.currentTimeMillis()), 
//                "APPCODE_RESTEST_3", null, null, 
//                "USER_NOTES_TEST_3", "QWERTY", null, null);        
//        getAuditorService().audit(reservationAudit, contents);
    }
   
    public void testGetAudits() throws Exception {
        ModuleCriterion criterion = new ModuleCriterion();
        criterion.setFieldName("appCode");
        criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
        List values = new ArrayList();
        values.add("APPCODE%");
        criterion.setValue(values);
        
        List criteria = new ArrayList();
        criteria.add(criterion);
        
        List list = new ArrayList();
        list.add("auditId");
        
        Collection data = getAuditorService().getAudits(
                criteria, 0, 5, list).getPageData();
        assertNotNull(data);
        assertEquals(5, data.size());
    }
    
    public void testGetReservationAudits() throws Exception {
        ModuleCriterion criterion = new ModuleCriterion();
        criterion.setFieldName("appCode");
        criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
        List values = new ArrayList();
        values.add("APPCODE%");
        criterion.setValue(values);
        
        List criteria = new ArrayList();
        criteria.add(criterion);
        
        List list = new ArrayList();
        list.add("timestamp");
        
        Collection data = getAuditorService().getReservationAudits(
                criteria, 0, 3, list).getPageData();
        
        assertNotNull(data);
        assertEquals(3, data.size());
    }
    
    public void testGetAuditsForPnr() throws Exception {
        String pnr = "QWERTY";
        List listData = getAuditorService().getAudits(pnr);
        assertNotNull(listData);        
        assertEquals(5, listData.size());
    }
    
    private AuditorBD getAuditorService() { 
        LookupService lookup = LookupServiceFactory.getInstance();
        IModule auditorModule = lookup.getModule("auditor");
        AuditorBD delegate = null;
        try {
            delegate = (AuditorBD) auditorModule.getServiceBD("auditor.service.remote");
            return delegate;
        } catch (Exception e) {
            System.out.println("Exception in creating delegate");
            e.printStackTrace();
        }
        return delegate;
    }

}
