package com.isa.thinair.wsclient.core.service.blockspace;

import java.util.Collection;
import java.util.Iterator;

import junit.framework.TestCase;

import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.util.CommonUtil;

public class TestBlockSpaceWebservicesClientImpl extends TestCase {
    
	private static OTAAirBookRS otaAirBookRS;
    private static OTAAirAvailRS otaAirAvailRS;
    private static OTAAirPriceRS otaAirPriceRS;
	private AvailableFlightSearchDTO afsDTO;
                
    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testAvailabilitySearch() throws Exception {
    	
    	afsDTO = new AvailableFlightSearchDTO();
    	
    	afsDTO.setFromAirport("SHJ");
    	afsDTO.setToAirport("CMB");
    	afsDTO.setDepatureDate(CommonUtil.parseDate("2008-02-20T00:00:00").getTime());
        //afsDTO(CommonUtil.parseDate("2008-02-25T00:00:00").getTime());
    	afsDTO.setAdultCount(2);
    	afsDTO.setInfantCount(1);        
                
		otaAirAvailRS = new BlockSpaceWebservicesClientImpl().getSeatAvailability(afsDTO, null);
        
        System.out.println("EchoToken:" + otaAirAvailRS.getEchoToken());
        System.out.println(getAvailabilityResultsSummary(otaAirAvailRS));
        
    }
    
    public void testFareQuote() throws Exception {
//        FareQuoteRequestDTO fqrDTO = new FareQuoteRequestDTO();
//        Collection<String> colFlightId = null;
//        String flightId1 = "127951";
//        
//        fqrDTO.setFlightAvailabilityRequestDTO(farDTO);
//        fqrDTO.setAvailabilityResponse(otaAirAvailRS);
//        
//        colFlightId = new ArrayList<String>();
//        colFlightId.add(flightId1);
//        fqrDTO.setFlightIds(colFlightId);
        
        AvailableFlightSearchDTO availableFlightSearchDTO = null;
		otaAirPriceRS = new BlockSpaceWebservicesClientImpl().getPriceQuote(availableFlightSearchDTO, otaAirAvailRS, null);
        
        System.out.println(getPriceQuoteResultsSummary(otaAirPriceRS));
    }
    
    
    public void testMakeBooking() throws Exception {
//        MakeBookingRequestDTO bookingDTO = new MakeBookingRequestDTO();
//        ContactPerson contactPerson = null;
//        
//        bookingDTO.setFareQuoteResponse(otaAirPriceRS); 
//        
//        contactPerson = new ContactPerson();
        
        //bookingDTO.setContactPerson(contactPerson);
		Reservation reservation = null;
		String agentCode = null;
		otaAirBookRS = new BlockSpaceWebservicesClientImpl().bookReservation(otaAirPriceRS, reservation,agentCode, null);
        
        System.out.println(getReservationSummary(otaAirBookRS));
    }
    
    
    public void testTransferReservation() throws Exception {
//        MakeBookingRequestDTO bookingDTO = new MakeBookingRequestDTO();
//        ContactPerson contactPerson = null;
//        
//        bookingDTO.setFareQuoteResponse(otaAirPriceRS); 
//        
//        contactPerson = new ContactPerson();
        
        //bookingDTO.setContactPerson(contactPerson);
		Reservation reservation = null;
		String agentCode = null;
		Collection pnrSegmentIDs = null;
		ServiceResponce sr = new BlockSpaceWebservicesClientImpl().transferReservation(reservation, pnrSegmentIDs, agentCode, null);
        
        System.out.println(getReservationSummary(otaAirBookRS));
    }
    
    private static StringBuffer getAvailabilityResultsSummary(OTAAirAvailRS otaAirAvailRS) throws Exception {
        StringBuffer summary = new StringBuffer();
        String nl = "\n\r \t";
        summary.append("Number of search results found = " + otaAirAvailRS.getOriginDestinationInformation().size());
        for (OriginDestinationInformation originDestinationInformationRS : otaAirAvailRS.getOriginDestinationInformation()){
            summary.append(nl + "Flight Details [" +
                    "Origin= " + originDestinationInformationRS.getOriginLocation().getLocationCode() +
                    ",Destination= " + originDestinationInformationRS.getDestinationLocation().getLocationCode() + 
                    ",DepDateTime= " + originDestinationInformationRS.getDepartureDateTime().getValue() + 
                    ",ArrDateTime="+originDestinationInformationRS.getArrivalDateTime().getValue() + "]");
            for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption()){
                for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption.getFlightSegment()){
                    summary.append(nl + "Segment Info [fltNumber=" + flightSegment.getFlightNumber() +
                            ",FlightId=" + flightSegment.getRPH() +
                            ",Origin=" + flightSegment.getDepartureAirport().getLocationCode() +
                            ",Destination=" + flightSegment.getArrivalAirport().getLocationCode() +
                            ",DepDateTime=" + CommonUtil.getFormattedDate(flightSegment.getDepartureDateTime().toGregorianCalendar().getTime()) + 
                            ",ArrDateTime=" + CommonUtil.getFormattedDate(flightSegment.getArrivalDateTime().toGregorianCalendar().getTime()) + "]");
                }
            }
        }
        return summary;
    }
    
    private static StringBuffer getPriceQuoteResultsSummary(OTAAirPriceRS otaAirPriceRS) {
        StringBuffer summary = new StringBuffer();
        String nl = "\n\r \t";
        if(otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries() != null){
            PricedItinerariesType pricedItineraries = (PricedItinerariesType) otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().get(0);
            AirItineraryPricingInfoType airItineraryPricingInfo = pricedItineraries.getPricedItinerary().get(0).getAirItineraryPricingInfo();
            summary.append(nl + "Pricing Details [" +
                    "Fare= " + airItineraryPricingInfo.getItinTotalFare().getBaseFare().getAmount().doubleValue() +
                    ",Tax= " + (airItineraryPricingInfo.getItinTotalFare().getTotalFare().getAmount().doubleValue() - airItineraryPricingInfo.getItinTotalFare().getBaseFare().getAmount().doubleValue()) +
                    ",Total= " + airItineraryPricingInfo.getItinTotalFare().getTotalFare().getAmount().doubleValue() + "]");
        }
        return summary;
    }
    
    private static StringBuffer getReservationSummary(OTAAirBookRS otaAirBookRS) throws ModuleException {
        StringBuffer summary = new StringBuffer();
        String nl = "\n\r \t";
        summary.append("Number of reservations found = " + otaAirBookRS
                .getSuccessAndWarningsAndAirReservation().size());
        if (otaAirBookRS != null && otaAirBookRS.getSuccessAndWarningsAndAirReservation() != null) {
            for (Iterator i = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator(); i.hasNext();) {             
                Object o = i.next();
                if (o instanceof AirReservation) {
                    AirReservation reservation = (AirReservation)o;                                 
                    summary.append("BookingReferenceID = " + reservation.getBookingReferenceID().get(0).getID());
                    
                    // Itinerary
                    AirItineraryType airItinerary = (AirItineraryType) reservation.getAirItinerary();
                    if (airItinerary != null) {
                        Iterator odo = airItinerary.getOriginDestinationOptions().getOriginDestinationOption().iterator();                      
                        while (odo.hasNext()){
                            OriginDestinationOptionType odot = (OriginDestinationOptionType) odo.next();                            
                            Iterator fsg = odot.getFlightSegment().iterator();                          
                            while (fsg.hasNext()){      
                                BookFlightSegmentType bfst = (BookFlightSegmentType)fsg.next();                                                         
                                summary.append(nl + "Flight Details [" +
                                        "Flight Number= " + bfst.getFlightNumber() +
                                        "Origin= " + bfst.getDepartureAirport().getLocationCode() +
                                        ",Destination= " +  bfst.getArrivalAirport().getLocationCode() + 
                                        ",DepDateTime= " + bfst.getDepartureDateTime().toString() + 
                                        ",ArrDateTime= "+ bfst.getArrivalDateTime().toString() + "]");
                            }                           
                        }
                    }                   
                }               
            }
        }
        
        return summary;
    }        

}
