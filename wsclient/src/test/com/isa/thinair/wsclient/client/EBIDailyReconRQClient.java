package com.isa.thinair.wsclient.client;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.wsclient.core.remoting.ejb.WSClientService;
import com.isa.thinair.wsclient.core.remoting.ejb.WSClientServiceHome;

public class EBIDailyReconRQClient {
	
	private static SimpleDateFormat DATE_FORMAT_FOR_LOGGING = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	protected static final Log log = LogFactory.getLog(EBIDailyReconRQClient.class);
	
	public static void main(String[] args) throws Exception {
		
		log.info("Begin calling daily EBI transaction recon [timestamp=" + DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
		
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		hashtable.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		hashtable.put("java.naming.provider.url", "jnp://localhost:1099");
		InitialContext initialContext = new InitialContext(hashtable);
		
		WSClientServiceHome wsClientHome = (WSClientServiceHome) initialContext.lookup("ejb/WSClientService");
		WSClientService wsClientService = wsClientHome.create();

		int dayOffset = decideDayOffset();
		
		Calendar startTimestamp = new GregorianCalendar();
		startTimestamp.add(Calendar.DAY_OF_MONTH, dayOffset);
		startTimestamp.set(Calendar.HOUR_OF_DAY, 0);
		startTimestamp.set(Calendar.MINUTE, 0);
		startTimestamp.set(Calendar.SECOND, 0);
		startTimestamp.set(Calendar.MILLISECOND, 0);
		
		Calendar endTimestmap = new GregorianCalendar();
		endTimestmap.add(Calendar.DAY_OF_MONTH, dayOffset);
		endTimestmap.set(Calendar.HOUR_OF_DAY, 23);
		endTimestmap.set(Calendar.MINUTE, 59);
		endTimestmap.set(Calendar.SECOND, 59);
		endTimestmap.set(Calendar.MILLISECOND, 999);
		
		log.info("Begin daily EBI transaction recon [timestamp=" + DATE_FORMAT_FOR_LOGGING.format(new Date()) + 
							",periodFrom=" + startTimestamp.getTime() + 
							",periodTo=" + endTimestmap.getTime()+ "]");
		
		wsClientService.requestDailyEBITnxReconcilation(startTimestamp.getTime(), endTimestmap.getTime());
		
		log.info("End calling daily EBI transaction recon [timestamp=" + DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
	}
	
	private static int decideDayOffset() {
		int dayOffset = -1;
		return dayOffset;
	}
	
}
