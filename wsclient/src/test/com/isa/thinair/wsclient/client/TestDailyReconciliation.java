package com.isa.thinair.wsclient.client;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.isa.thinair.wsclient.core.remoting.ejb.WSClientService;
import com.isa.thinair.wsclient.core.remoting.ejb.WSClientServiceHome;

public class TestDailyReconciliation {

	/**
	 * @param args
	 * @throws NamingException 
	 */
	public static void main(String[] args) throws Exception {
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		hashtable.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		hashtable.put("java.naming.provider.url", "jnp://localhost:1099");
		InitialContext initialContext = new InitialContext(hashtable);
		
		WSClientServiceHome wsClientHome = (WSClientServiceHome) initialContext.lookup("ejb/WSClientService");
		WSClientService wsClientService = wsClientHome.create();

		Calendar fromTimestampCal = new GregorianCalendar();
		fromTimestampCal.set(Calendar.HOUR_OF_DAY, 0);
		fromTimestampCal.set(Calendar.MINUTE, 0);
		fromTimestampCal.set(Calendar.SECOND, 0);
		fromTimestampCal.set(Calendar.MILLISECOND, 0);
		
		Calendar toTimestampCal = new GregorianCalendar();
		toTimestampCal.set(Calendar.HOUR_OF_DAY, 23);
		toTimestampCal.set(Calendar.MINUTE, 59);
		toTimestampCal.set(Calendar.SECOND, 59);
		toTimestampCal.set(Calendar.MILLISECOND, 999);
		
		wsClientService.requestDailyEBITnxReconcilation(fromTimestampCal.getTime(), toTimestampCal.getTime());
	}

}
