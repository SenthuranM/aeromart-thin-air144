package com.isa.thinair.wsclient.api.dto.aig;

import java.math.BigDecimal;

public class InsuranceCountryTO {

	private String countryCodeAIG;
	private String currencyCode;
	private BigDecimal insurancePercentage;
	private boolean isEuropianCountry;

	/**
	 * @return the countryCodeAIG
	 */
	public String getCountryCodeAIG() {
		return countryCodeAIG;
	}

	/**
	 * @param countryCodeAIG
	 *            the countryCodeAIG to set
	 */
	public void setCountryCodeAIG(String countryCodeAIG) {
		this.countryCodeAIG = countryCodeAIG;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * 
	 * @param insurancePercentage
	 */
	public void setInsurancePercentage(BigDecimal insurancePercentage) {
		this.insurancePercentage = insurancePercentage;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getInsurancePercentage() {
		return insurancePercentage;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEuropianCountry() {
		return isEuropianCountry;
	}

	/**
	 * 
	 * @param insuranceViewChange
	 */
	public void setEuropianCountry(boolean insuranceViewChange) {
		this.isEuropianCountry = insuranceViewChange;
	}

}
