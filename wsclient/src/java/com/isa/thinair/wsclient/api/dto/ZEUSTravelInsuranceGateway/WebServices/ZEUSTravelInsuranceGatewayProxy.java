package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ZEUSTravelInsuranceGatewayProxy implements com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType {
  private String _endpoint = null;
  private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType zEUSTravelInsuranceGateway_PortType = null;
  
  public ZEUSTravelInsuranceGatewayProxy() {
    _initZEUSTravelInsuranceGatewayProxy();
  }
  
  public ZEUSTravelInsuranceGatewayProxy(String endpoint) {
    _endpoint = endpoint;
    _initZEUSTravelInsuranceGatewayProxy();
  }
  
  private void _initZEUSTravelInsuranceGatewayProxy() {
    try {
      zEUSTravelInsuranceGateway_PortType = (new com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_ServiceLocator()).getZEUSTravelInsuranceGateway();
      if (zEUSTravelInsuranceGateway_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)zEUSTravelInsuranceGateway_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)zEUSTravelInsuranceGateway_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (zEUSTravelInsuranceGateway_PortType != null)
      ((javax.xml.rpc.Stub)zEUSTravelInsuranceGateway_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType getZEUSTravelInsuranceGateway_PortType() {
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType;
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans getAvailablePlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.getAvailablePlans(genericRequest);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders getAvailablePlansWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.getAvailablePlansWithRiders(genericRequest);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA getAvailablePlansOTA(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestOTA genericRequestOTALite) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.getAvailablePlansOTA(genericRequestOTALite);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse confirmPurchase(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.confirmPurchase(genericRequest);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders confirmPurchaseWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders genericRequest) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.confirmPurchaseWithRiders(genericRequest);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse getPolicyDetails(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, java.lang.String PNR) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.getPolicyDetails(itineraryAuthentication, PNR);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse cancelPolicy(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.cancelPolicy(itineraryAuthentication, policyHeader);
  }
  
  public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse reissuePolicyCertificate(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException{
    if (zEUSTravelInsuranceGateway_PortType == null)
      _initZEUSTravelInsuranceGatewayProxy();
    return zEUSTravelInsuranceGateway_PortType.reissuePolicyCertificate(itineraryAuthentication, policyHeader);
  }
  
  
}