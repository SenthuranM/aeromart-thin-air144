package com.isa.thinair.wsclient.api.dto;

import java.io.Serializable;

public class InsuranceProductConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	private String providerName;
	private String productCode;
	private String displayName;
	private int rank;
	private int groupID;
	private int subGroupID;
	private int saleChannelCode;
	private boolean isActive;
	private boolean isExternalContent;
	private boolean isPopup;
	private int insuranceProductConfigID;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public int getSubGroupID() {
		return subGroupID;
	}

	public void setSubGroupID(int subGroupID) {
		this.subGroupID = subGroupID;
	}

	public int getSaleChannelCode() {
		return saleChannelCode;
	}

	public void setSaleChannelCode(int saleChannelCode) {
		this.saleChannelCode = saleChannelCode;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isExternalContent() {
		return isExternalContent;
	}

	public void setExternalContent(boolean isExternalContent) {
		this.isExternalContent = isExternalContent;
	}

	public boolean isPopup() {
		return isPopup;
	}

	public void setPopup(boolean isPopup) {
		this.isPopup = isPopup;
	}

	public int getInsuranceProductConfigID() {
		return insuranceProductConfigID;
	}

	public void setInsuranceProductConfigID(int insuranceProductConfigID) {
		this.insuranceProductConfigID = insuranceProductConfigID;
	}

}
