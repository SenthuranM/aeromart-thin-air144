package com.isa.thinair.wsclient.api.service;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

public interface InsuranceClientBD {

	public static final String SERVICE_NAME = "InsuranceClientService";

	List<InsuranceResponse> quoteInsurancePolicy(IInsuranceRequest iQuoteRequest) throws ModuleException;

	List<InsuranceResponse> sellInsurancePolicy(List<IInsuranceRequest> iRequests) throws ModuleException;

}
