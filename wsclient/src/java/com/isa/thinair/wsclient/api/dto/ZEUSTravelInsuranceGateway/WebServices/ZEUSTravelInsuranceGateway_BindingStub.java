/**
 * ZEUSTravelInsuranceGateway_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ZEUSTravelInsuranceGateway_BindingStub extends org.apache.axis.client.Stub implements com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[8];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailablePlans");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequest"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlans"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailablePlansWithRiders");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequest"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansWithRiders"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailablePlansOTA");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericRequestOTALite"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestOTA"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestOTA.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansOTA"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConfirmPurchase");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequest"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseResponse"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConfirmPurchaseWithRiders");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenericRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestWithRiders"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseResponseWithRiders"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseResponseWithRiders"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPolicyDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetPolicyDetailsResponse"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ExistingPolicy"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelPolicy");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ReissuePolicyCertificate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader"), com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse"));
        oper.setReturnClass(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

    }

    public ZEUSTravelInsuranceGateway_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ZEUSTravelInsuranceGateway_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ZEUSTravelInsuranceGateway_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfConfirmedPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassenger");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassenger");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfItineraryFlight");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryFlight");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Flight");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfItineraryPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryPassenger");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Passenger");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlan");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Plan");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlan");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlan1");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Plan");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailableRiderPlan");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlanMarketingPointer");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlanOTA");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTA");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlan");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlanPricingBreakdown");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanPricingBreakdown");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PricingBreakdown");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPlanQualifiedPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassenger");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassenger");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPurchaseMain");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseMain");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseMain");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrayOfPurchaseRider");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseRider");
            qName2 = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseRider");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseResponse");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseResponseWithRiders");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenderType");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetPolicyDetailsResponse");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IdentificationType");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequest");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestOTA");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestOTA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestWithRiders");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryContact");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryFlight");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeader");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeaderOTA");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderOTA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeaderWithRiders");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlans");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansOTA");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansWithRiders");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Plan");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanChargeType");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanChargeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTA");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTAChargeType");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTAChargeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanPricingBreakdown");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassenger");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyStatus");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ProposalStatus");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ProposalStatusWithRiders");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatusWithRiders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseMain");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseRider");
            cachedSerQNames.add(qName);
            cls = com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans getAvailablePlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/GetAvailablePlans");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetAvailablePlans"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {genericRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders getAvailablePlansWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/GetAvailablePlansWithRiders");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetAvailablePlansWithRiders"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {genericRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA getAvailablePlansOTA(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestOTA genericRequestOTALite) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/GetAvailablePlansOTA");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetAvailablePlansOTA"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {genericRequestOTALite});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse confirmPurchase(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/ConfirmPurchase");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchase"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {genericRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders confirmPurchaseWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders genericRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/ConfirmPurchaseWithRiders");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseWithRiders"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {genericRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse getPolicyDetails(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, java.lang.String PNR) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/GetPolicyDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GetPolicyDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {itineraryAuthentication, PNR});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse cancelPolicy(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/CancelPolicy");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CancelPolicy"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {itineraryAuthentication, policyHeader});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse reissuePolicyCertificate(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ZEUSTravelInsuranceGateway/WebServices/ReissuePolicyCertificate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ReissuePolicyCertificate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {itineraryAuthentication, policyHeader});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
