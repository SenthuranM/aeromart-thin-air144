/**
 * InputRequestWithRiders.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class InputRequestWithRiders  implements java.io.Serializable {
    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders header;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact contactDetails;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight[] flights;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger[] passengers;

    public InputRequestWithRiders() {
    }

    public InputRequestWithRiders(
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders header,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact contactDetails,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight[] flights,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger[] passengers) {
           this.authentication = authentication;
           this.header = header;
           this.contactDetails = contactDetails;
           this.flights = flights;
           this.passengers = passengers;
    }


    /**
     * Gets the authentication value for this InputRequestWithRiders.
     * 
     * @return authentication
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this InputRequestWithRiders.
     * 
     * @param authentication
     */
    public void setAuthentication(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication) {
        this.authentication = authentication;
    }


    /**
     * Gets the header value for this InputRequestWithRiders.
     * 
     * @return header
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders getHeader() {
        return header;
    }


    /**
     * Sets the header value for this InputRequestWithRiders.
     * 
     * @param header
     */
    public void setHeader(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders header) {
        this.header = header;
    }


    /**
     * Gets the contactDetails value for this InputRequestWithRiders.
     * 
     * @return contactDetails
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact getContactDetails() {
        return contactDetails;
    }


    /**
     * Sets the contactDetails value for this InputRequestWithRiders.
     * 
     * @param contactDetails
     */
    public void setContactDetails(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact contactDetails) {
        this.contactDetails = contactDetails;
    }


    /**
     * Gets the flights value for this InputRequestWithRiders.
     * 
     * @return flights
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight[] getFlights() {
        return flights;
    }


    /**
     * Sets the flights value for this InputRequestWithRiders.
     * 
     * @param flights
     */
    public void setFlights(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight[] flights) {
        this.flights = flights;
    }


    /**
     * Gets the passengers value for this InputRequestWithRiders.
     * 
     * @return passengers
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger[] getPassengers() {
        return passengers;
    }


    /**
     * Sets the passengers value for this InputRequestWithRiders.
     * 
     * @param passengers
     */
    public void setPassengers(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger[] passengers) {
        this.passengers = passengers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InputRequestWithRiders)) return false;
        InputRequestWithRiders other = (InputRequestWithRiders) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication()))) &&
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.contactDetails==null && other.getContactDetails()==null) || 
             (this.contactDetails!=null &&
              this.contactDetails.equals(other.getContactDetails()))) &&
            ((this.flights==null && other.getFlights()==null) || 
             (this.flights!=null &&
              java.util.Arrays.equals(this.flights, other.getFlights()))) &&
            ((this.passengers==null && other.getPassengers()==null) || 
             (this.passengers!=null &&
              java.util.Arrays.equals(this.passengers, other.getPassengers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getContactDetails() != null) {
            _hashCode += getContactDetails().hashCode();
        }
        if (getFlights() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFlights());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFlights(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InputRequestWithRiders.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestWithRiders"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeaderWithRiders"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contactDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ContactDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryContact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flights");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Flights"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryFlight"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Flight"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Passengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryPassenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Passenger"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
