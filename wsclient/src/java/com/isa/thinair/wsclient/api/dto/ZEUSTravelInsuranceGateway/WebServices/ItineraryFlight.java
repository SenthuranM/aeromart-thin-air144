/**
 * ItineraryFlight.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ItineraryFlight  implements java.io.Serializable {
    private java.lang.String departCountryCode;

    private java.lang.String departStationCode;

    private java.lang.String arrivalCountryCode;

    private java.lang.String arrivalStationCode;

    private java.lang.String departAirlineCode;

    private java.lang.String departDateTime;

    private java.lang.String returnAirlineCode;

    private java.lang.String returnDateTime;

    private java.lang.String departFlightNo;

    private java.lang.String returnFlightNo;

    public ItineraryFlight() {
    }

    public ItineraryFlight(
           java.lang.String departCountryCode,
           java.lang.String departStationCode,
           java.lang.String arrivalCountryCode,
           java.lang.String arrivalStationCode,
           java.lang.String departAirlineCode,
           java.lang.String departDateTime,
           java.lang.String returnAirlineCode,
           java.lang.String returnDateTime,
           java.lang.String departFlightNo,
           java.lang.String returnFlightNo) {
           this.departCountryCode = departCountryCode;
           this.departStationCode = departStationCode;
           this.arrivalCountryCode = arrivalCountryCode;
           this.arrivalStationCode = arrivalStationCode;
           this.departAirlineCode = departAirlineCode;
           this.departDateTime = departDateTime;
           this.returnAirlineCode = returnAirlineCode;
           this.returnDateTime = returnDateTime;
           this.departFlightNo = departFlightNo;
           this.returnFlightNo = returnFlightNo;
    }


    /**
     * Gets the departCountryCode value for this ItineraryFlight.
     * 
     * @return departCountryCode
     */
    public java.lang.String getDepartCountryCode() {
        return departCountryCode;
    }


    /**
     * Sets the departCountryCode value for this ItineraryFlight.
     * 
     * @param departCountryCode
     */
    public void setDepartCountryCode(java.lang.String departCountryCode) {
        this.departCountryCode = departCountryCode;
    }


    /**
     * Gets the departStationCode value for this ItineraryFlight.
     * 
     * @return departStationCode
     */
    public java.lang.String getDepartStationCode() {
        return departStationCode;
    }


    /**
     * Sets the departStationCode value for this ItineraryFlight.
     * 
     * @param departStationCode
     */
    public void setDepartStationCode(java.lang.String departStationCode) {
        this.departStationCode = departStationCode;
    }


    /**
     * Gets the arrivalCountryCode value for this ItineraryFlight.
     * 
     * @return arrivalCountryCode
     */
    public java.lang.String getArrivalCountryCode() {
        return arrivalCountryCode;
    }


    /**
     * Sets the arrivalCountryCode value for this ItineraryFlight.
     * 
     * @param arrivalCountryCode
     */
    public void setArrivalCountryCode(java.lang.String arrivalCountryCode) {
        this.arrivalCountryCode = arrivalCountryCode;
    }


    /**
     * Gets the arrivalStationCode value for this ItineraryFlight.
     * 
     * @return arrivalStationCode
     */
    public java.lang.String getArrivalStationCode() {
        return arrivalStationCode;
    }


    /**
     * Sets the arrivalStationCode value for this ItineraryFlight.
     * 
     * @param arrivalStationCode
     */
    public void setArrivalStationCode(java.lang.String arrivalStationCode) {
        this.arrivalStationCode = arrivalStationCode;
    }


    /**
     * Gets the departAirlineCode value for this ItineraryFlight.
     * 
     * @return departAirlineCode
     */
    public java.lang.String getDepartAirlineCode() {
        return departAirlineCode;
    }


    /**
     * Sets the departAirlineCode value for this ItineraryFlight.
     * 
     * @param departAirlineCode
     */
    public void setDepartAirlineCode(java.lang.String departAirlineCode) {
        this.departAirlineCode = departAirlineCode;
    }


    /**
     * Gets the departDateTime value for this ItineraryFlight.
     * 
     * @return departDateTime
     */
    public java.lang.String getDepartDateTime() {
        return departDateTime;
    }


    /**
     * Sets the departDateTime value for this ItineraryFlight.
     * 
     * @param departDateTime
     */
    public void setDepartDateTime(java.lang.String departDateTime) {
        this.departDateTime = departDateTime;
    }


    /**
     * Gets the returnAirlineCode value for this ItineraryFlight.
     * 
     * @return returnAirlineCode
     */
    public java.lang.String getReturnAirlineCode() {
        return returnAirlineCode;
    }


    /**
     * Sets the returnAirlineCode value for this ItineraryFlight.
     * 
     * @param returnAirlineCode
     */
    public void setReturnAirlineCode(java.lang.String returnAirlineCode) {
        this.returnAirlineCode = returnAirlineCode;
    }


    /**
     * Gets the returnDateTime value for this ItineraryFlight.
     * 
     * @return returnDateTime
     */
    public java.lang.String getReturnDateTime() {
        return returnDateTime;
    }


    /**
     * Sets the returnDateTime value for this ItineraryFlight.
     * 
     * @param returnDateTime
     */
    public void setReturnDateTime(java.lang.String returnDateTime) {
        this.returnDateTime = returnDateTime;
    }


    /**
     * Gets the departFlightNo value for this ItineraryFlight.
     * 
     * @return departFlightNo
     */
    public java.lang.String getDepartFlightNo() {
        return departFlightNo;
    }


    /**
     * Sets the departFlightNo value for this ItineraryFlight.
     * 
     * @param departFlightNo
     */
    public void setDepartFlightNo(java.lang.String departFlightNo) {
        this.departFlightNo = departFlightNo;
    }


    /**
     * Gets the returnFlightNo value for this ItineraryFlight.
     * 
     * @return returnFlightNo
     */
    public java.lang.String getReturnFlightNo() {
        return returnFlightNo;
    }


    /**
     * Sets the returnFlightNo value for this ItineraryFlight.
     * 
     * @param returnFlightNo
     */
    public void setReturnFlightNo(java.lang.String returnFlightNo) {
        this.returnFlightNo = returnFlightNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItineraryFlight)) return false;
        ItineraryFlight other = (ItineraryFlight) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.departCountryCode==null && other.getDepartCountryCode()==null) || 
             (this.departCountryCode!=null &&
              this.departCountryCode.equals(other.getDepartCountryCode()))) &&
            ((this.departStationCode==null && other.getDepartStationCode()==null) || 
             (this.departStationCode!=null &&
              this.departStationCode.equals(other.getDepartStationCode()))) &&
            ((this.arrivalCountryCode==null && other.getArrivalCountryCode()==null) || 
             (this.arrivalCountryCode!=null &&
              this.arrivalCountryCode.equals(other.getArrivalCountryCode()))) &&
            ((this.arrivalStationCode==null && other.getArrivalStationCode()==null) || 
             (this.arrivalStationCode!=null &&
              this.arrivalStationCode.equals(other.getArrivalStationCode()))) &&
            ((this.departAirlineCode==null && other.getDepartAirlineCode()==null) || 
             (this.departAirlineCode!=null &&
              this.departAirlineCode.equals(other.getDepartAirlineCode()))) &&
            ((this.departDateTime==null && other.getDepartDateTime()==null) || 
             (this.departDateTime!=null &&
              this.departDateTime.equals(other.getDepartDateTime()))) &&
            ((this.returnAirlineCode==null && other.getReturnAirlineCode()==null) || 
             (this.returnAirlineCode!=null &&
              this.returnAirlineCode.equals(other.getReturnAirlineCode()))) &&
            ((this.returnDateTime==null && other.getReturnDateTime()==null) || 
             (this.returnDateTime!=null &&
              this.returnDateTime.equals(other.getReturnDateTime()))) &&
            ((this.departFlightNo==null && other.getDepartFlightNo()==null) || 
             (this.departFlightNo!=null &&
              this.departFlightNo.equals(other.getDepartFlightNo()))) &&
            ((this.returnFlightNo==null && other.getReturnFlightNo()==null) || 
             (this.returnFlightNo!=null &&
              this.returnFlightNo.equals(other.getReturnFlightNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepartCountryCode() != null) {
            _hashCode += getDepartCountryCode().hashCode();
        }
        if (getDepartStationCode() != null) {
            _hashCode += getDepartStationCode().hashCode();
        }
        if (getArrivalCountryCode() != null) {
            _hashCode += getArrivalCountryCode().hashCode();
        }
        if (getArrivalStationCode() != null) {
            _hashCode += getArrivalStationCode().hashCode();
        }
        if (getDepartAirlineCode() != null) {
            _hashCode += getDepartAirlineCode().hashCode();
        }
        if (getDepartDateTime() != null) {
            _hashCode += getDepartDateTime().hashCode();
        }
        if (getReturnAirlineCode() != null) {
            _hashCode += getReturnAirlineCode().hashCode();
        }
        if (getReturnDateTime() != null) {
            _hashCode += getReturnDateTime().hashCode();
        }
        if (getDepartFlightNo() != null) {
            _hashCode += getDepartFlightNo().hashCode();
        }
        if (getReturnFlightNo() != null) {
            _hashCode += getReturnFlightNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItineraryFlight.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryFlight"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DepartCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departStationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DepartStationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrivalCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrivalStationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ArrivalStationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departAirlineCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DepartAirlineCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DepartDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnAirlineCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ReturnAirlineCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ReturnDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departFlightNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DepartFlightNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnFlightNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ReturnFlightNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
