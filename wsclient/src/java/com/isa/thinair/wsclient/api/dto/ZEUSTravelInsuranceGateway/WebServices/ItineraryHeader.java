/**
 * ItineraryHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ItineraryHeader  implements java.io.Serializable {
    private java.lang.String channel;

    private java.lang.String itineraryID;

    private java.lang.String PNR;

    private java.lang.String policyNo;

    private java.lang.String purchaseDate;

    private java.lang.String SSRFeeCode;

    private java.lang.String feeDescription;

    private java.lang.String currency;

    private double totalPremium;

    private java.lang.String countryCode;

    private java.lang.String cultureCode;

    private int totalAdults;

    private int totalChild;

    private int totalInfants;

    public ItineraryHeader() {
    }

    public ItineraryHeader(
           java.lang.String channel,
           java.lang.String itineraryID,
           java.lang.String PNR,
           java.lang.String policyNo,
           java.lang.String purchaseDate,
           java.lang.String SSRFeeCode,
           java.lang.String feeDescription,
           java.lang.String currency,
           double totalPremium,
           java.lang.String countryCode,
           java.lang.String cultureCode,
           int totalAdults,
           int totalChild,
           int totalInfants) {
           this.channel = channel;
           this.itineraryID = itineraryID;
           this.PNR = PNR;
           this.policyNo = policyNo;
           this.purchaseDate = purchaseDate;
           this.SSRFeeCode = SSRFeeCode;
           this.feeDescription = feeDescription;
           this.currency = currency;
           this.totalPremium = totalPremium;
           this.countryCode = countryCode;
           this.cultureCode = cultureCode;
           this.totalAdults = totalAdults;
           this.totalChild = totalChild;
           this.totalInfants = totalInfants;
    }


    /**
     * Gets the channel value for this ItineraryHeader.
     * 
     * @return channel
     */
    public java.lang.String getChannel() {
        return channel;
    }


    /**
     * Sets the channel value for this ItineraryHeader.
     * 
     * @param channel
     */
    public void setChannel(java.lang.String channel) {
        this.channel = channel;
    }


    /**
     * Gets the itineraryID value for this ItineraryHeader.
     * 
     * @return itineraryID
     */
    public java.lang.String getItineraryID() {
        return itineraryID;
    }


    /**
     * Sets the itineraryID value for this ItineraryHeader.
     * 
     * @param itineraryID
     */
    public void setItineraryID(java.lang.String itineraryID) {
        this.itineraryID = itineraryID;
    }


    /**
     * Gets the PNR value for this ItineraryHeader.
     * 
     * @return PNR
     */
    public java.lang.String getPNR() {
        return PNR;
    }


    /**
     * Sets the PNR value for this ItineraryHeader.
     * 
     * @param PNR
     */
    public void setPNR(java.lang.String PNR) {
        this.PNR = PNR;
    }


    /**
     * Gets the policyNo value for this ItineraryHeader.
     * 
     * @return policyNo
     */
    public java.lang.String getPolicyNo() {
        return policyNo;
    }


    /**
     * Sets the policyNo value for this ItineraryHeader.
     * 
     * @param policyNo
     */
    public void setPolicyNo(java.lang.String policyNo) {
        this.policyNo = policyNo;
    }


    /**
     * Gets the purchaseDate value for this ItineraryHeader.
     * 
     * @return purchaseDate
     */
    public java.lang.String getPurchaseDate() {
        return purchaseDate;
    }


    /**
     * Sets the purchaseDate value for this ItineraryHeader.
     * 
     * @param purchaseDate
     */
    public void setPurchaseDate(java.lang.String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }


    /**
     * Gets the SSRFeeCode value for this ItineraryHeader.
     * 
     * @return SSRFeeCode
     */
    public java.lang.String getSSRFeeCode() {
        return SSRFeeCode;
    }


    /**
     * Sets the SSRFeeCode value for this ItineraryHeader.
     * 
     * @param SSRFeeCode
     */
    public void setSSRFeeCode(java.lang.String SSRFeeCode) {
        this.SSRFeeCode = SSRFeeCode;
    }


    /**
     * Gets the feeDescription value for this ItineraryHeader.
     * 
     * @return feeDescription
     */
    public java.lang.String getFeeDescription() {
        return feeDescription;
    }


    /**
     * Sets the feeDescription value for this ItineraryHeader.
     * 
     * @param feeDescription
     */
    public void setFeeDescription(java.lang.String feeDescription) {
        this.feeDescription = feeDescription;
    }


    /**
     * Gets the currency value for this ItineraryHeader.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this ItineraryHeader.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }


    /**
     * Gets the totalPremium value for this ItineraryHeader.
     * 
     * @return totalPremium
     */
    public double getTotalPremium() {
        return totalPremium;
    }


    /**
     * Sets the totalPremium value for this ItineraryHeader.
     * 
     * @param totalPremium
     */
    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }


    /**
     * Gets the countryCode value for this ItineraryHeader.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this ItineraryHeader.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the cultureCode value for this ItineraryHeader.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this ItineraryHeader.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }


    /**
     * Gets the totalAdults value for this ItineraryHeader.
     * 
     * @return totalAdults
     */
    public int getTotalAdults() {
        return totalAdults;
    }


    /**
     * Sets the totalAdults value for this ItineraryHeader.
     * 
     * @param totalAdults
     */
    public void setTotalAdults(int totalAdults) {
        this.totalAdults = totalAdults;
    }


    /**
     * Gets the totalChild value for this ItineraryHeader.
     * 
     * @return totalChild
     */
    public int getTotalChild() {
        return totalChild;
    }


    /**
     * Sets the totalChild value for this ItineraryHeader.
     * 
     * @param totalChild
     */
    public void setTotalChild(int totalChild) {
        this.totalChild = totalChild;
    }


    /**
     * Gets the totalInfants value for this ItineraryHeader.
     * 
     * @return totalInfants
     */
    public int getTotalInfants() {
        return totalInfants;
    }


    /**
     * Sets the totalInfants value for this ItineraryHeader.
     * 
     * @param totalInfants
     */
    public void setTotalInfants(int totalInfants) {
        this.totalInfants = totalInfants;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItineraryHeader)) return false;
        ItineraryHeader other = (ItineraryHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.channel==null && other.getChannel()==null) || 
             (this.channel!=null &&
              this.channel.equals(other.getChannel()))) &&
            ((this.itineraryID==null && other.getItineraryID()==null) || 
             (this.itineraryID!=null &&
              this.itineraryID.equals(other.getItineraryID()))) &&
            ((this.PNR==null && other.getPNR()==null) || 
             (this.PNR!=null &&
              this.PNR.equals(other.getPNR()))) &&
            ((this.policyNo==null && other.getPolicyNo()==null) || 
             (this.policyNo!=null &&
              this.policyNo.equals(other.getPolicyNo()))) &&
            ((this.purchaseDate==null && other.getPurchaseDate()==null) || 
             (this.purchaseDate!=null &&
              this.purchaseDate.equals(other.getPurchaseDate()))) &&
            ((this.SSRFeeCode==null && other.getSSRFeeCode()==null) || 
             (this.SSRFeeCode!=null &&
              this.SSRFeeCode.equals(other.getSSRFeeCode()))) &&
            ((this.feeDescription==null && other.getFeeDescription()==null) || 
             (this.feeDescription!=null &&
              this.feeDescription.equals(other.getFeeDescription()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            this.totalPremium == other.getTotalPremium() &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode()))) &&
            this.totalAdults == other.getTotalAdults() &&
            this.totalChild == other.getTotalChild() &&
            this.totalInfants == other.getTotalInfants();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChannel() != null) {
            _hashCode += getChannel().hashCode();
        }
        if (getItineraryID() != null) {
            _hashCode += getItineraryID().hashCode();
        }
        if (getPNR() != null) {
            _hashCode += getPNR().hashCode();
        }
        if (getPolicyNo() != null) {
            _hashCode += getPolicyNo().hashCode();
        }
        if (getPurchaseDate() != null) {
            _hashCode += getPurchaseDate().hashCode();
        }
        if (getSSRFeeCode() != null) {
            _hashCode += getSSRFeeCode().hashCode();
        }
        if (getFeeDescription() != null) {
            _hashCode += getFeeDescription().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        _hashCode += new Double(getTotalPremium()).hashCode();
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        _hashCode += getTotalAdults();
        _hashCode += getTotalChild();
        _hashCode += getTotalInfants();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItineraryHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Channel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRFeeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "SSRFeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("feeDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "FeeDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPremium");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalPremium"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAdults");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalAdults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalChild");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalChild"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalInfants");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalInfants"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
