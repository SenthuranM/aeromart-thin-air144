package com.isa.thinair.wsclient.api.dto.lms;

import java.io.Serializable;
import java.util.Map;

/**
 * Internal response Object for IssueVariableReward WS API
 * 
 * @author rumesh
 * 
 */
public class IssueRewardResponse implements Serializable {

	private static final long serialVersionUID = 7996169662176737617L;

	private String[] issuedRewardIds;

	private Map<String, Double> productLoyaltyPointCost;

	public String[] getIssuedRewardIds() {
		return issuedRewardIds;
	}

	public void setIssuedRewardIds(String[] issuedRewardIds) {
		this.issuedRewardIds = issuedRewardIds;
	}

	public Map<String, Double> getProductLoyaltyPointCost() {
		return productLoyaltyPointCost;
	}

	public void setProductLoyaltyPointCost(Map<String, Double> productLoyaltyPointCost) {
		this.productLoyaltyPointCost = productLoyaltyPointCost;
	}

}
