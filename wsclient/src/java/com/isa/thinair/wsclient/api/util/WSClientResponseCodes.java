package com.isa.thinair.wsclient.api.util;

/**
 * Holds api related WS Client constants
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class WSClientResponseCodes {

	public static interface InterlinedTypes {
		public static final String TRANSFER_PNR = "RESPONSE_PNR";
		public static final String TOTAL_PRICE_AMOUNT = "TOTAL_PRICE_AMOUNT";
		public static final String TOTAL_PRICE_CURRENCY = "TOTAL_PRICE_CURRENCY";
		public static final String TOTAL_PAYMENT_AMOUNT = "TOTAL_PAYMENT_AMOUNT";
		public static final String TOTAL_PAYMENT_CURRENCY = "TOTAL_PAYMENT_CURRENCY";
		public static final String SEGMENT_INFORMATION = "SEGMENT_INFO";
		public static final String PROCESS_DESC = "PROCESS_DESC";
		public static final String SUCCESSFUL_TRANSFER_SEGMENT_DTOS = "SUCCESSFUL_TRANSFER_SEGMENT_DTOS";
		public static final String FAILED_TRANSFER_SEGMENT_DTOS = "FAILED_TRANSFER_SEGMENT_DTOS";
	}
}
