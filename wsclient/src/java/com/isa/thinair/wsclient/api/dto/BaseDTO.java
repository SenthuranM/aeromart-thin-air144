package com.isa.thinair.wsclient.api.dto;

import java.io.Serializable;

/**
 * Holds this generic informatin through out web services client invocations
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class BaseDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 636808661020539580L;

	private String terminalId;

	private String onAccountAgentCode;

	private String userId;

	private String salesChannelCode;

	private String carrierCode;

	/**
	 * @return Returns the agentCode.
	 */
	public String getOnAccountAgentCode() {
		return onAccountAgentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setOnAccountAgentCode(String agentCode) {
		this.onAccountAgentCode = agentCode;
	}

	/**
	 * @return Returns the bookingChanelType.
	 */
	public String getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param bookingChanelType
	 *            The bookingChanelType to set.
	 */
	public void setSalesChannelCode(String bookingChanelType) {
		this.salesChannelCode = bookingChanelType;
	}

	/**
	 * @return Returns the terminalId.
	 */
	public String getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId
	 *            The terminalId to set.
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return Returns the carrierCode.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
