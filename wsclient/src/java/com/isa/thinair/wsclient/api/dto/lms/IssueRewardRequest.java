package com.isa.thinair.wsclient.api.dto.lms;

import java.io.Serializable;
import java.util.List;

/**
 * Internal request Object for IssueVariableReward WS API
 * 
 * @author rumesh
 * 
 */
public class IssueRewardRequest implements Serializable {

	private static final long serialVersionUID = 14800561294735794L;

	private String memberAccountId;

	private List<RewardData> rewards;

	private String locationExternalReference;
	
	private String memberEnrollingCarrier;

	private String memberExternalId;

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

	public List<RewardData> getRewards() {
		return rewards;
	}

	public void setRewards(List<RewardData> rewards) {
		this.rewards = rewards;
	}

	public String getLocationExternalReference() {
		return locationExternalReference;
	}

	public void setLocationExternalReference(String locationExternalReference) {
		this.locationExternalReference = locationExternalReference;
	}

	public String getMemberEnrollingCarrier() {
		return memberEnrollingCarrier;
	}

	public void setMemberEnrollingCarrier(String memberEnrollingCarrier) {
		this.memberEnrollingCarrier = memberEnrollingCarrier;
	}

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}

}
