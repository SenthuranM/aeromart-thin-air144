package com.isa.thinair.wsclient.api.dto.aig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AigTOUtils {

	public static String getIBEDate(Date date) throws ParseException {
		return new SimpleDateFormat("dd/MM/yyyy").format(date);

	}

	public static Date getIBEDate(String date) throws ParseException {
		return new SimpleDateFormat("dd/MM/yyyy").parse(date);

	}

	public static String getAIGDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		return sdf.format(date);
	}

	public static String getAIGBirthDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		return sdf.format(date);
	}

	public static String getMessageIdDateDetails(Date d) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(d);
		StringBuilder sb = new StringBuilder();
		sb.append(gc.get(Calendar.YEAR));
		sb.append(gc.get(Calendar.MONTH));
		sb.append(gc.get(Calendar.DATE));
		sb.append(gc.get(Calendar.HOUR));
		sb.append(gc.get(Calendar.MINUTE));
		sb.append(gc.get(Calendar.SECOND));
		sb.append(gc.get(Calendar.MILLISECOND) * 1000);
		return sb.toString();
	}
}
