/**
 * PolicyHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class PolicyHeader  implements java.io.Serializable {
    private java.lang.String PNR;

    private java.lang.String itineraryID;

    private java.lang.String policyNo;

    private java.lang.String purchaseDate;

    public PolicyHeader() {
    }

    public PolicyHeader(
           java.lang.String PNR,
           java.lang.String itineraryID,
           java.lang.String policyNo,
           java.lang.String purchaseDate) {
           this.PNR = PNR;
           this.itineraryID = itineraryID;
           this.policyNo = policyNo;
           this.purchaseDate = purchaseDate;
    }


    /**
     * Gets the PNR value for this PolicyHeader.
     * 
     * @return PNR
     */
    public java.lang.String getPNR() {
        return PNR;
    }


    /**
     * Sets the PNR value for this PolicyHeader.
     * 
     * @param PNR
     */
    public void setPNR(java.lang.String PNR) {
        this.PNR = PNR;
    }


    /**
     * Gets the itineraryID value for this PolicyHeader.
     * 
     * @return itineraryID
     */
    public java.lang.String getItineraryID() {
        return itineraryID;
    }


    /**
     * Sets the itineraryID value for this PolicyHeader.
     * 
     * @param itineraryID
     */
    public void setItineraryID(java.lang.String itineraryID) {
        this.itineraryID = itineraryID;
    }


    /**
     * Gets the policyNo value for this PolicyHeader.
     * 
     * @return policyNo
     */
    public java.lang.String getPolicyNo() {
        return policyNo;
    }


    /**
     * Sets the policyNo value for this PolicyHeader.
     * 
     * @param policyNo
     */
    public void setPolicyNo(java.lang.String policyNo) {
        this.policyNo = policyNo;
    }


    /**
     * Gets the purchaseDate value for this PolicyHeader.
     * 
     * @return purchaseDate
     */
    public java.lang.String getPurchaseDate() {
        return purchaseDate;
    }


    /**
     * Sets the purchaseDate value for this PolicyHeader.
     * 
     * @param purchaseDate
     */
    public void setPurchaseDate(java.lang.String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PolicyHeader)) return false;
        PolicyHeader other = (PolicyHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PNR==null && other.getPNR()==null) || 
             (this.PNR!=null &&
              this.PNR.equals(other.getPNR()))) &&
            ((this.itineraryID==null && other.getItineraryID()==null) || 
             (this.itineraryID!=null &&
              this.itineraryID.equals(other.getItineraryID()))) &&
            ((this.policyNo==null && other.getPolicyNo()==null) || 
             (this.policyNo!=null &&
              this.policyNo.equals(other.getPolicyNo()))) &&
            ((this.purchaseDate==null && other.getPurchaseDate()==null) || 
             (this.purchaseDate!=null &&
              this.purchaseDate.equals(other.getPurchaseDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPNR() != null) {
            _hashCode += getPNR().hashCode();
        }
        if (getItineraryID() != null) {
            _hashCode += getItineraryID().hashCode();
        }
        if (getPolicyNo() != null) {
            _hashCode += getPolicyNo().hashCode();
        }
        if (getPurchaseDate() != null) {
            _hashCode += getPurchaseDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PolicyHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
