/**
 * PlanOTA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class PlanOTA  implements java.io.Serializable {
    private java.lang.String planCode;

    private java.lang.String SSRFeeCode;

    private java.lang.String currencyCode;

    private double totalPremiumAmount;

    private double totalCoverageAmount;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTAChargeType planPremiumChargeType;

    private java.lang.String planTitle;

    private java.lang.String planDesc;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer[] planMarketingPointers;

    private java.lang.String planAdditionalInfoTitle;

    private java.lang.String planAdditionalInfoDesc;

    private java.lang.String planYesDesc;

    private java.lang.String planNoDesc;

    private java.lang.String planNoConsideration;

    private java.lang.String planTnC;

    private boolean isDefaultPlan;

    private java.lang.String planContent;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger[] planQualifiedPassengers;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown[] planPricingBreakdown;

    public PlanOTA() {
    }

    public PlanOTA(
           java.lang.String planCode,
           java.lang.String SSRFeeCode,
           java.lang.String currencyCode,
           double totalPremiumAmount,
           double totalCoverageAmount,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTAChargeType planPremiumChargeType,
           java.lang.String planTitle,
           java.lang.String planDesc,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer[] planMarketingPointers,
           java.lang.String planAdditionalInfoTitle,
           java.lang.String planAdditionalInfoDesc,
           java.lang.String planYesDesc,
           java.lang.String planNoDesc,
           java.lang.String planNoConsideration,
           java.lang.String planTnC,
           boolean isDefaultPlan,
           java.lang.String planContent,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger[] planQualifiedPassengers,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown[] planPricingBreakdown) {
           this.planCode = planCode;
           this.SSRFeeCode = SSRFeeCode;
           this.currencyCode = currencyCode;
           this.totalPremiumAmount = totalPremiumAmount;
           this.totalCoverageAmount = totalCoverageAmount;
           this.planPremiumChargeType = planPremiumChargeType;
           this.planTitle = planTitle;
           this.planDesc = planDesc;
           this.planMarketingPointers = planMarketingPointers;
           this.planAdditionalInfoTitle = planAdditionalInfoTitle;
           this.planAdditionalInfoDesc = planAdditionalInfoDesc;
           this.planYesDesc = planYesDesc;
           this.planNoDesc = planNoDesc;
           this.planNoConsideration = planNoConsideration;
           this.planTnC = planTnC;
           this.isDefaultPlan = isDefaultPlan;
           this.planContent = planContent;
           this.planQualifiedPassengers = planQualifiedPassengers;
           this.planPricingBreakdown = planPricingBreakdown;
    }


    /**
     * Gets the planCode value for this PlanOTA.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this PlanOTA.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the SSRFeeCode value for this PlanOTA.
     * 
     * @return SSRFeeCode
     */
    public java.lang.String getSSRFeeCode() {
        return SSRFeeCode;
    }


    /**
     * Sets the SSRFeeCode value for this PlanOTA.
     * 
     * @param SSRFeeCode
     */
    public void setSSRFeeCode(java.lang.String SSRFeeCode) {
        this.SSRFeeCode = SSRFeeCode;
    }


    /**
     * Gets the currencyCode value for this PlanOTA.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this PlanOTA.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the totalPremiumAmount value for this PlanOTA.
     * 
     * @return totalPremiumAmount
     */
    public double getTotalPremiumAmount() {
        return totalPremiumAmount;
    }


    /**
     * Sets the totalPremiumAmount value for this PlanOTA.
     * 
     * @param totalPremiumAmount
     */
    public void setTotalPremiumAmount(double totalPremiumAmount) {
        this.totalPremiumAmount = totalPremiumAmount;
    }


    /**
     * Gets the totalCoverageAmount value for this PlanOTA.
     * 
     * @return totalCoverageAmount
     */
    public double getTotalCoverageAmount() {
        return totalCoverageAmount;
    }


    /**
     * Sets the totalCoverageAmount value for this PlanOTA.
     * 
     * @param totalCoverageAmount
     */
    public void setTotalCoverageAmount(double totalCoverageAmount) {
        this.totalCoverageAmount = totalCoverageAmount;
    }


    /**
     * Gets the planPremiumChargeType value for this PlanOTA.
     * 
     * @return planPremiumChargeType
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTAChargeType getPlanPremiumChargeType() {
        return planPremiumChargeType;
    }


    /**
     * Sets the planPremiumChargeType value for this PlanOTA.
     * 
     * @param planPremiumChargeType
     */
    public void setPlanPremiumChargeType(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTAChargeType planPremiumChargeType) {
        this.planPremiumChargeType = planPremiumChargeType;
    }


    /**
     * Gets the planTitle value for this PlanOTA.
     * 
     * @return planTitle
     */
    public java.lang.String getPlanTitle() {
        return planTitle;
    }


    /**
     * Sets the planTitle value for this PlanOTA.
     * 
     * @param planTitle
     */
    public void setPlanTitle(java.lang.String planTitle) {
        this.planTitle = planTitle;
    }


    /**
     * Gets the planDesc value for this PlanOTA.
     * 
     * @return planDesc
     */
    public java.lang.String getPlanDesc() {
        return planDesc;
    }


    /**
     * Sets the planDesc value for this PlanOTA.
     * 
     * @param planDesc
     */
    public void setPlanDesc(java.lang.String planDesc) {
        this.planDesc = planDesc;
    }


    /**
     * Gets the planMarketingPointers value for this PlanOTA.
     * 
     * @return planMarketingPointers
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer[] getPlanMarketingPointers() {
        return planMarketingPointers;
    }


    /**
     * Sets the planMarketingPointers value for this PlanOTA.
     * 
     * @param planMarketingPointers
     */
    public void setPlanMarketingPointers(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanMarketingPointer[] planMarketingPointers) {
        this.planMarketingPointers = planMarketingPointers;
    }


    /**
     * Gets the planAdditionalInfoTitle value for this PlanOTA.
     * 
     * @return planAdditionalInfoTitle
     */
    public java.lang.String getPlanAdditionalInfoTitle() {
        return planAdditionalInfoTitle;
    }


    /**
     * Sets the planAdditionalInfoTitle value for this PlanOTA.
     * 
     * @param planAdditionalInfoTitle
     */
    public void setPlanAdditionalInfoTitle(java.lang.String planAdditionalInfoTitle) {
        this.planAdditionalInfoTitle = planAdditionalInfoTitle;
    }


    /**
     * Gets the planAdditionalInfoDesc value for this PlanOTA.
     * 
     * @return planAdditionalInfoDesc
     */
    public java.lang.String getPlanAdditionalInfoDesc() {
        return planAdditionalInfoDesc;
    }


    /**
     * Sets the planAdditionalInfoDesc value for this PlanOTA.
     * 
     * @param planAdditionalInfoDesc
     */
    public void setPlanAdditionalInfoDesc(java.lang.String planAdditionalInfoDesc) {
        this.planAdditionalInfoDesc = planAdditionalInfoDesc;
    }


    /**
     * Gets the planYesDesc value for this PlanOTA.
     * 
     * @return planYesDesc
     */
    public java.lang.String getPlanYesDesc() {
        return planYesDesc;
    }


    /**
     * Sets the planYesDesc value for this PlanOTA.
     * 
     * @param planYesDesc
     */
    public void setPlanYesDesc(java.lang.String planYesDesc) {
        this.planYesDesc = planYesDesc;
    }


    /**
     * Gets the planNoDesc value for this PlanOTA.
     * 
     * @return planNoDesc
     */
    public java.lang.String getPlanNoDesc() {
        return planNoDesc;
    }


    /**
     * Sets the planNoDesc value for this PlanOTA.
     * 
     * @param planNoDesc
     */
    public void setPlanNoDesc(java.lang.String planNoDesc) {
        this.planNoDesc = planNoDesc;
    }


    /**
     * Gets the planNoConsideration value for this PlanOTA.
     * 
     * @return planNoConsideration
     */
    public java.lang.String getPlanNoConsideration() {
        return planNoConsideration;
    }


    /**
     * Sets the planNoConsideration value for this PlanOTA.
     * 
     * @param planNoConsideration
     */
    public void setPlanNoConsideration(java.lang.String planNoConsideration) {
        this.planNoConsideration = planNoConsideration;
    }


    /**
     * Gets the planTnC value for this PlanOTA.
     * 
     * @return planTnC
     */
    public java.lang.String getPlanTnC() {
        return planTnC;
    }


    /**
     * Sets the planTnC value for this PlanOTA.
     * 
     * @param planTnC
     */
    public void setPlanTnC(java.lang.String planTnC) {
        this.planTnC = planTnC;
    }


    /**
     * Gets the isDefaultPlan value for this PlanOTA.
     * 
     * @return isDefaultPlan
     */
    public boolean isIsDefaultPlan() {
        return isDefaultPlan;
    }


    /**
     * Sets the isDefaultPlan value for this PlanOTA.
     * 
     * @param isDefaultPlan
     */
    public void setIsDefaultPlan(boolean isDefaultPlan) {
        this.isDefaultPlan = isDefaultPlan;
    }


    /**
     * Gets the planContent value for this PlanOTA.
     * 
     * @return planContent
     */
    public java.lang.String getPlanContent() {
        return planContent;
    }


    /**
     * Sets the planContent value for this PlanOTA.
     * 
     * @param planContent
     */
    public void setPlanContent(java.lang.String planContent) {
        this.planContent = planContent;
    }


    /**
     * Gets the planQualifiedPassengers value for this PlanOTA.
     * 
     * @return planQualifiedPassengers
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger[] getPlanQualifiedPassengers() {
        return planQualifiedPassengers;
    }


    /**
     * Sets the planQualifiedPassengers value for this PlanOTA.
     * 
     * @param planQualifiedPassengers
     */
    public void setPlanQualifiedPassengers(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanQualifiedPassenger[] planQualifiedPassengers) {
        this.planQualifiedPassengers = planQualifiedPassengers;
    }


    /**
     * Gets the planPricingBreakdown value for this PlanOTA.
     * 
     * @return planPricingBreakdown
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown[] getPlanPricingBreakdown() {
        return planPricingBreakdown;
    }


    /**
     * Sets the planPricingBreakdown value for this PlanOTA.
     * 
     * @param planPricingBreakdown
     */
    public void setPlanPricingBreakdown(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanPricingBreakdown[] planPricingBreakdown) {
        this.planPricingBreakdown = planPricingBreakdown;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PlanOTA)) return false;
        PlanOTA other = (PlanOTA) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            ((this.SSRFeeCode==null && other.getSSRFeeCode()==null) || 
             (this.SSRFeeCode!=null &&
              this.SSRFeeCode.equals(other.getSSRFeeCode()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            this.totalPremiumAmount == other.getTotalPremiumAmount() &&
            this.totalCoverageAmount == other.getTotalCoverageAmount() &&
            ((this.planPremiumChargeType==null && other.getPlanPremiumChargeType()==null) || 
             (this.planPremiumChargeType!=null &&
              this.planPremiumChargeType.equals(other.getPlanPremiumChargeType()))) &&
            ((this.planTitle==null && other.getPlanTitle()==null) || 
             (this.planTitle!=null &&
              this.planTitle.equals(other.getPlanTitle()))) &&
            ((this.planDesc==null && other.getPlanDesc()==null) || 
             (this.planDesc!=null &&
              this.planDesc.equals(other.getPlanDesc()))) &&
            ((this.planMarketingPointers==null && other.getPlanMarketingPointers()==null) || 
             (this.planMarketingPointers!=null &&
              java.util.Arrays.equals(this.planMarketingPointers, other.getPlanMarketingPointers()))) &&
            ((this.planAdditionalInfoTitle==null && other.getPlanAdditionalInfoTitle()==null) || 
             (this.planAdditionalInfoTitle!=null &&
              this.planAdditionalInfoTitle.equals(other.getPlanAdditionalInfoTitle()))) &&
            ((this.planAdditionalInfoDesc==null && other.getPlanAdditionalInfoDesc()==null) || 
             (this.planAdditionalInfoDesc!=null &&
              this.planAdditionalInfoDesc.equals(other.getPlanAdditionalInfoDesc()))) &&
            ((this.planYesDesc==null && other.getPlanYesDesc()==null) || 
             (this.planYesDesc!=null &&
              this.planYesDesc.equals(other.getPlanYesDesc()))) &&
            ((this.planNoDesc==null && other.getPlanNoDesc()==null) || 
             (this.planNoDesc!=null &&
              this.planNoDesc.equals(other.getPlanNoDesc()))) &&
            ((this.planNoConsideration==null && other.getPlanNoConsideration()==null) || 
             (this.planNoConsideration!=null &&
              this.planNoConsideration.equals(other.getPlanNoConsideration()))) &&
            ((this.planTnC==null && other.getPlanTnC()==null) || 
             (this.planTnC!=null &&
              this.planTnC.equals(other.getPlanTnC()))) &&
            this.isDefaultPlan == other.isIsDefaultPlan() &&
            ((this.planContent==null && other.getPlanContent()==null) || 
             (this.planContent!=null &&
              this.planContent.equals(other.getPlanContent()))) &&
            ((this.planQualifiedPassengers==null && other.getPlanQualifiedPassengers()==null) || 
             (this.planQualifiedPassengers!=null &&
              java.util.Arrays.equals(this.planQualifiedPassengers, other.getPlanQualifiedPassengers()))) &&
            ((this.planPricingBreakdown==null && other.getPlanPricingBreakdown()==null) || 
             (this.planPricingBreakdown!=null &&
              java.util.Arrays.equals(this.planPricingBreakdown, other.getPlanPricingBreakdown())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        if (getSSRFeeCode() != null) {
            _hashCode += getSSRFeeCode().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        _hashCode += new Double(getTotalPremiumAmount()).hashCode();
        _hashCode += new Double(getTotalCoverageAmount()).hashCode();
        if (getPlanPremiumChargeType() != null) {
            _hashCode += getPlanPremiumChargeType().hashCode();
        }
        if (getPlanTitle() != null) {
            _hashCode += getPlanTitle().hashCode();
        }
        if (getPlanDesc() != null) {
            _hashCode += getPlanDesc().hashCode();
        }
        if (getPlanMarketingPointers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlanMarketingPointers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlanMarketingPointers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPlanAdditionalInfoTitle() != null) {
            _hashCode += getPlanAdditionalInfoTitle().hashCode();
        }
        if (getPlanAdditionalInfoDesc() != null) {
            _hashCode += getPlanAdditionalInfoDesc().hashCode();
        }
        if (getPlanYesDesc() != null) {
            _hashCode += getPlanYesDesc().hashCode();
        }
        if (getPlanNoDesc() != null) {
            _hashCode += getPlanNoDesc().hashCode();
        }
        if (getPlanNoConsideration() != null) {
            _hashCode += getPlanNoConsideration().hashCode();
        }
        if (getPlanTnC() != null) {
            _hashCode += getPlanTnC().hashCode();
        }
        _hashCode += (isIsDefaultPlan() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getPlanContent() != null) {
            _hashCode += getPlanContent().hashCode();
        }
        if (getPlanQualifiedPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlanQualifiedPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlanQualifiedPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPlanPricingBreakdown() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlanPricingBreakdown());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlanPricingBreakdown(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PlanOTA.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTA"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSRFeeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "SSRFeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPremiumAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalPremiumAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCoverageAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalCoverageAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planPremiumChargeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanPremiumChargeType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTAChargeType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planMarketingPointers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planAdditionalInfoTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanAdditionalInfoTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planAdditionalInfoDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanAdditionalInfoDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planYesDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanYesDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planNoDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanNoDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planNoConsideration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanNoConsideration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planTnC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanTnC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isDefaultPlan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IsDefaultPlan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planContent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanContent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planQualifiedPassengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanQualifiedPassenger"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planPricingBreakdown");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanPricingBreakdown"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanPricingBreakdown"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PricingBreakdown"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
