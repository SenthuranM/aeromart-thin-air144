package com.isa.thinair.wsclient.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Holds the flight search transfer information
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class FlightSearchTransferDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2924561954062566375L;

	private String fromAirport;

	private String toAirport;

	private Date depatureDate;

	private Date returnDate;

	private int adultCount;

	private int childCount;

	private int infantCount;

	private String cabinClassCode;

	private String flightNumber;

	/**
	 * @return Returns the adultCount.
	 */
	public int getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount
	 *            The adultCount to set.
	 */
	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the childCount.
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount
	 *            The childCount to set.
	 */
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return Returns the depatureDate.
	 */
	public Date getDepatureDate() {
		return depatureDate;
	}

	/**
	 * @param depatureDate
	 *            The depatureDate to set.
	 */
	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	/**
	 * @return Returns the fromAirport.
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the infantCount.
	 */
	public int getInfantCount() {
		return infantCount;
	}

	/**
	 * @param infantCount
	 *            The infantCount to set.
	 */
	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	/**
	 * @return Returns the returnDate.
	 */
	public Date getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate
	 *            The returnDate to set.
	 */
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * @return Returns the toAirport.
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
}
