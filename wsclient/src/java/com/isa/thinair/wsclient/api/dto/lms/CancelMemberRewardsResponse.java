package com.isa.thinair.wsclient.api.dto.lms;

import java.io.Serializable;

/**
 * Internal DTO to transfer CancelMemberRewards API response
 * 
 * @author rumesh
 * 
 */
public class CancelMemberRewardsResponse implements Serializable {

	private static final long serialVersionUID = -7234685334492859229L;

}
