package com.isa.thinair.wsclient.api.dto.netcast;

import java.io.Serializable;

public class NetCastRequest implements Serializable {

	private String mobile;

	private String message;

	private String netCastId;

	private String mask;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNetCastId() {
		return netCastId;
	}

	public void setNetCastId(String netCastId) {
		this.netCastId = netCastId;
	}

	public String getMask() {
		return mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

}
