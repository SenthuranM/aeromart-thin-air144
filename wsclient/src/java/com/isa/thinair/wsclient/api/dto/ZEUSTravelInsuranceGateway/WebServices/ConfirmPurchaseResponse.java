/**
 * ConfirmPurchaseResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ConfirmPurchaseResponse  implements java.io.Serializable {
    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatus proposalState;

    private java.lang.String itineraryID;

    private java.lang.String PNR;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private java.lang.String policyNo;

    private java.lang.String policyPurchasedDateTime;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger[] confirmedPassengers;

    public ConfirmPurchaseResponse() {
    }

    public ConfirmPurchaseResponse(
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatus proposalState,
           java.lang.String itineraryID,
           java.lang.String PNR,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           java.lang.String policyNo,
           java.lang.String policyPurchasedDateTime,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger[] confirmedPassengers) {
           this.proposalState = proposalState;
           this.itineraryID = itineraryID;
           this.PNR = PNR;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.policyNo = policyNo;
           this.policyPurchasedDateTime = policyPurchasedDateTime;
           this.confirmedPassengers = confirmedPassengers;
    }


    /**
     * Gets the proposalState value for this ConfirmPurchaseResponse.
     * 
     * @return proposalState
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatus getProposalState() {
        return proposalState;
    }


    /**
     * Sets the proposalState value for this ConfirmPurchaseResponse.
     * 
     * @param proposalState
     */
    public void setProposalState(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ProposalStatus proposalState) {
        this.proposalState = proposalState;
    }


    /**
     * Gets the itineraryID value for this ConfirmPurchaseResponse.
     * 
     * @return itineraryID
     */
    public java.lang.String getItineraryID() {
        return itineraryID;
    }


    /**
     * Sets the itineraryID value for this ConfirmPurchaseResponse.
     * 
     * @param itineraryID
     */
    public void setItineraryID(java.lang.String itineraryID) {
        this.itineraryID = itineraryID;
    }


    /**
     * Gets the PNR value for this ConfirmPurchaseResponse.
     * 
     * @return PNR
     */
    public java.lang.String getPNR() {
        return PNR;
    }


    /**
     * Sets the PNR value for this ConfirmPurchaseResponse.
     * 
     * @param PNR
     */
    public void setPNR(java.lang.String PNR) {
        this.PNR = PNR;
    }


    /**
     * Gets the errorCode value for this ConfirmPurchaseResponse.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this ConfirmPurchaseResponse.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this ConfirmPurchaseResponse.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this ConfirmPurchaseResponse.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the policyNo value for this ConfirmPurchaseResponse.
     * 
     * @return policyNo
     */
    public java.lang.String getPolicyNo() {
        return policyNo;
    }


    /**
     * Sets the policyNo value for this ConfirmPurchaseResponse.
     * 
     * @param policyNo
     */
    public void setPolicyNo(java.lang.String policyNo) {
        this.policyNo = policyNo;
    }


    /**
     * Gets the policyPurchasedDateTime value for this ConfirmPurchaseResponse.
     * 
     * @return policyPurchasedDateTime
     */
    public java.lang.String getPolicyPurchasedDateTime() {
        return policyPurchasedDateTime;
    }


    /**
     * Sets the policyPurchasedDateTime value for this ConfirmPurchaseResponse.
     * 
     * @param policyPurchasedDateTime
     */
    public void setPolicyPurchasedDateTime(java.lang.String policyPurchasedDateTime) {
        this.policyPurchasedDateTime = policyPurchasedDateTime;
    }


    /**
     * Gets the confirmedPassengers value for this ConfirmPurchaseResponse.
     * 
     * @return confirmedPassengers
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger[] getConfirmedPassengers() {
        return confirmedPassengers;
    }


    /**
     * Sets the confirmedPassengers value for this ConfirmPurchaseResponse.
     * 
     * @param confirmedPassengers
     */
    public void setConfirmedPassengers(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmedPassenger[] confirmedPassengers) {
        this.confirmedPassengers = confirmedPassengers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConfirmPurchaseResponse)) return false;
        ConfirmPurchaseResponse other = (ConfirmPurchaseResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.proposalState==null && other.getProposalState()==null) || 
             (this.proposalState!=null &&
              this.proposalState.equals(other.getProposalState()))) &&
            ((this.itineraryID==null && other.getItineraryID()==null) || 
             (this.itineraryID!=null &&
              this.itineraryID.equals(other.getItineraryID()))) &&
            ((this.PNR==null && other.getPNR()==null) || 
             (this.PNR!=null &&
              this.PNR.equals(other.getPNR()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.policyNo==null && other.getPolicyNo()==null) || 
             (this.policyNo!=null &&
              this.policyNo.equals(other.getPolicyNo()))) &&
            ((this.policyPurchasedDateTime==null && other.getPolicyPurchasedDateTime()==null) || 
             (this.policyPurchasedDateTime!=null &&
              this.policyPurchasedDateTime.equals(other.getPolicyPurchasedDateTime()))) &&
            ((this.confirmedPassengers==null && other.getConfirmedPassengers()==null) || 
             (this.confirmedPassengers!=null &&
              java.util.Arrays.equals(this.confirmedPassengers, other.getConfirmedPassengers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProposalState() != null) {
            _hashCode += getProposalState().hashCode();
        }
        if (getItineraryID() != null) {
            _hashCode += getItineraryID().hashCode();
        }
        if (getPNR() != null) {
            _hashCode += getPNR().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getPolicyNo() != null) {
            _hashCode += getPolicyNo().hashCode();
        }
        if (getPolicyPurchasedDateTime() != null) {
            _hashCode += getPolicyPurchasedDateTime().hashCode();
        }
        if (getConfirmedPassengers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConfirmedPassengers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConfirmedPassengers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConfirmPurchaseResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmPurchaseResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proposalState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ProposalState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ProposalStatus"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyPurchasedDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyPurchasedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("confirmedPassengers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassengers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassenger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ConfirmedPassenger"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
