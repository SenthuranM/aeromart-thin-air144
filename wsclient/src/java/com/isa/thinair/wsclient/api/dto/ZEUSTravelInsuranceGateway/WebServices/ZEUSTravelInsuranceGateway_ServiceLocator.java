/**
 * ZEUSTravelInsuranceGateway_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;

public class ZEUSTravelInsuranceGateway_ServiceLocator extends org.apache.axis.client.Service implements com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_Service {

    public ZEUSTravelInsuranceGateway_ServiceLocator() {
    }


    public ZEUSTravelInsuranceGateway_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ZEUSTravelInsuranceGateway_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ZEUSTravelInsuranceGateway
    private java.lang.String ZEUSTravelInsuranceGateway_address = WSClientModuleUtils.getModuleConfig().getTuneClientConfig().getUrl();
    		//"http://uat1.tunedirectonline.com/zeusws_airarabia/ZEUS.asmx";, "http://202.76.235.108/ZEUSWS/ZEUS.asmx";


    public java.lang.String getZEUSTravelInsuranceGatewayAddress() {
        return ZEUSTravelInsuranceGateway_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ZEUSTravelInsuranceGatewayWSDDServiceName = "ZEUSTravelInsuranceGateway";

    public java.lang.String getZEUSTravelInsuranceGatewayWSDDServiceName() {
        return ZEUSTravelInsuranceGatewayWSDDServiceName;
    }

    public void setZEUSTravelInsuranceGatewayWSDDServiceName(java.lang.String name) {
        ZEUSTravelInsuranceGatewayWSDDServiceName = name;
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType getZEUSTravelInsuranceGateway() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ZEUSTravelInsuranceGateway_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getZEUSTravelInsuranceGateway(endpoint);
    }

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType getZEUSTravelInsuranceGateway(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_BindingStub _stub = new com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_BindingStub(portAddress, this);
            _stub.setPortName(getZEUSTravelInsuranceGatewayWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setZEUSTravelInsuranceGatewayEndpointAddress(java.lang.String address) {
        ZEUSTravelInsuranceGateway_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_BindingStub _stub = new com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_BindingStub(new java.net.URL(ZEUSTravelInsuranceGateway_address), this);
                _stub.setPortName(getZEUSTravelInsuranceGatewayWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ZEUSTravelInsuranceGateway".equals(inputPortName)) {
            return getZEUSTravelInsuranceGateway();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ZEUSTravelInsuranceGateway");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ZEUSTravelInsuranceGateway"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ZEUSTravelInsuranceGateway".equals(portName)) {
            setZEUSTravelInsuranceGatewayEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
