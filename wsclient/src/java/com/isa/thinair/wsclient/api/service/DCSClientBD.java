package com.isa.thinair.wsclient.api.service;

import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRS;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRS;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface DCSClientBD {
	public static final String SERVICE_NAME = "DCSClientService";

	public void ping(String text) throws ModuleException;

	public DCSPNLInfoRS sendPNLToDCS(DCSPNLInfoRQ pnlInfo) throws ModuleException;
	
	public DCSADLInfoRS sendADLToDCS(DCSADLInfoRQ adlInfo) throws ModuleException;
}
