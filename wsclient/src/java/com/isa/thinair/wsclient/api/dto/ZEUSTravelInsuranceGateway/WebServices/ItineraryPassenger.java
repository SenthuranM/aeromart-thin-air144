/**
 * ItineraryPassenger.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ItineraryPassenger  implements java.io.Serializable {
    private java.lang.String isInfant;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType gender;

    private java.lang.String DOB;

    private java.lang.String age;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType identityType;

    private java.lang.String identityNo;

    private boolean isQualified;

    private java.lang.String nationality;

    private java.lang.String countryOfResidence;

    private java.lang.String selectedPlanCode;

    private java.lang.String selectedSSRFeeCode;

    private java.lang.String currencyCode;

    private double passengerPremiumAmount;

    public ItineraryPassenger() {
    }

    public ItineraryPassenger(
           java.lang.String isInfant,
           java.lang.String firstName,
           java.lang.String lastName,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType gender,
           java.lang.String DOB,
           java.lang.String age,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType identityType,
           java.lang.String identityNo,
           boolean isQualified,
           java.lang.String nationality,
           java.lang.String countryOfResidence,
           java.lang.String selectedPlanCode,
           java.lang.String selectedSSRFeeCode,
           java.lang.String currencyCode,
           double passengerPremiumAmount) {
           this.isInfant = isInfant;
           this.firstName = firstName;
           this.lastName = lastName;
           this.gender = gender;
           this.DOB = DOB;
           this.age = age;
           this.identityType = identityType;
           this.identityNo = identityNo;
           this.isQualified = isQualified;
           this.nationality = nationality;
           this.countryOfResidence = countryOfResidence;
           this.selectedPlanCode = selectedPlanCode;
           this.selectedSSRFeeCode = selectedSSRFeeCode;
           this.currencyCode = currencyCode;
           this.passengerPremiumAmount = passengerPremiumAmount;
    }


    /**
     * Gets the isInfant value for this ItineraryPassenger.
     * 
     * @return isInfant
     */
    public java.lang.String getIsInfant() {
        return isInfant;
    }


    /**
     * Sets the isInfant value for this ItineraryPassenger.
     * 
     * @param isInfant
     */
    public void setIsInfant(java.lang.String isInfant) {
        this.isInfant = isInfant;
    }


    /**
     * Gets the firstName value for this ItineraryPassenger.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this ItineraryPassenger.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this ItineraryPassenger.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this ItineraryPassenger.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the gender value for this ItineraryPassenger.
     * 
     * @return gender
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this ItineraryPassenger.
     * 
     * @param gender
     */
    public void setGender(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType gender) {
        this.gender = gender;
    }


    /**
     * Gets the DOB value for this ItineraryPassenger.
     * 
     * @return DOB
     */
    public java.lang.String getDOB() {
        return DOB;
    }


    /**
     * Sets the DOB value for this ItineraryPassenger.
     * 
     * @param DOB
     */
    public void setDOB(java.lang.String DOB) {
        this.DOB = DOB;
    }


    /**
     * Gets the age value for this ItineraryPassenger.
     * 
     * @return age
     */
    public java.lang.String getAge() {
        return age;
    }


    /**
     * Sets the age value for this ItineraryPassenger.
     * 
     * @param age
     */
    public void setAge(java.lang.String age) {
        this.age = age;
    }


    /**
     * Gets the identityType value for this ItineraryPassenger.
     * 
     * @return identityType
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType getIdentityType() {
        return identityType;
    }


    /**
     * Sets the identityType value for this ItineraryPassenger.
     * 
     * @param identityType
     */
    public void setIdentityType(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType identityType) {
        this.identityType = identityType;
    }


    /**
     * Gets the identityNo value for this ItineraryPassenger.
     * 
     * @return identityNo
     */
    public java.lang.String getIdentityNo() {
        return identityNo;
    }


    /**
     * Sets the identityNo value for this ItineraryPassenger.
     * 
     * @param identityNo
     */
    public void setIdentityNo(java.lang.String identityNo) {
        this.identityNo = identityNo;
    }


    /**
     * Gets the isQualified value for this ItineraryPassenger.
     * 
     * @return isQualified
     */
    public boolean isIsQualified() {
        return isQualified;
    }


    /**
     * Sets the isQualified value for this ItineraryPassenger.
     * 
     * @param isQualified
     */
    public void setIsQualified(boolean isQualified) {
        this.isQualified = isQualified;
    }


    /**
     * Gets the nationality value for this ItineraryPassenger.
     * 
     * @return nationality
     */
    public java.lang.String getNationality() {
        return nationality;
    }


    /**
     * Sets the nationality value for this ItineraryPassenger.
     * 
     * @param nationality
     */
    public void setNationality(java.lang.String nationality) {
        this.nationality = nationality;
    }


    /**
     * Gets the countryOfResidence value for this ItineraryPassenger.
     * 
     * @return countryOfResidence
     */
    public java.lang.String getCountryOfResidence() {
        return countryOfResidence;
    }


    /**
     * Sets the countryOfResidence value for this ItineraryPassenger.
     * 
     * @param countryOfResidence
     */
    public void setCountryOfResidence(java.lang.String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }


    /**
     * Gets the selectedPlanCode value for this ItineraryPassenger.
     * 
     * @return selectedPlanCode
     */
    public java.lang.String getSelectedPlanCode() {
        return selectedPlanCode;
    }


    /**
     * Sets the selectedPlanCode value for this ItineraryPassenger.
     * 
     * @param selectedPlanCode
     */
    public void setSelectedPlanCode(java.lang.String selectedPlanCode) {
        this.selectedPlanCode = selectedPlanCode;
    }


    /**
     * Gets the selectedSSRFeeCode value for this ItineraryPassenger.
     * 
     * @return selectedSSRFeeCode
     */
    public java.lang.String getSelectedSSRFeeCode() {
        return selectedSSRFeeCode;
    }


    /**
     * Sets the selectedSSRFeeCode value for this ItineraryPassenger.
     * 
     * @param selectedSSRFeeCode
     */
    public void setSelectedSSRFeeCode(java.lang.String selectedSSRFeeCode) {
        this.selectedSSRFeeCode = selectedSSRFeeCode;
    }


    /**
     * Gets the currencyCode value for this ItineraryPassenger.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this ItineraryPassenger.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the passengerPremiumAmount value for this ItineraryPassenger.
     * 
     * @return passengerPremiumAmount
     */
    public double getPassengerPremiumAmount() {
        return passengerPremiumAmount;
    }


    /**
     * Sets the passengerPremiumAmount value for this ItineraryPassenger.
     * 
     * @param passengerPremiumAmount
     */
    public void setPassengerPremiumAmount(double passengerPremiumAmount) {
        this.passengerPremiumAmount = passengerPremiumAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItineraryPassenger)) return false;
        ItineraryPassenger other = (ItineraryPassenger) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.isInfant==null && other.getIsInfant()==null) || 
             (this.isInfant!=null &&
              this.isInfant.equals(other.getIsInfant()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.DOB==null && other.getDOB()==null) || 
             (this.DOB!=null &&
              this.DOB.equals(other.getDOB()))) &&
            ((this.age==null && other.getAge()==null) || 
             (this.age!=null &&
              this.age.equals(other.getAge()))) &&
            ((this.identityType==null && other.getIdentityType()==null) || 
             (this.identityType!=null &&
              this.identityType.equals(other.getIdentityType()))) &&
            ((this.identityNo==null && other.getIdentityNo()==null) || 
             (this.identityNo!=null &&
              this.identityNo.equals(other.getIdentityNo()))) &&
            this.isQualified == other.isIsQualified() &&
            ((this.nationality==null && other.getNationality()==null) || 
             (this.nationality!=null &&
              this.nationality.equals(other.getNationality()))) &&
            ((this.countryOfResidence==null && other.getCountryOfResidence()==null) || 
             (this.countryOfResidence!=null &&
              this.countryOfResidence.equals(other.getCountryOfResidence()))) &&
            ((this.selectedPlanCode==null && other.getSelectedPlanCode()==null) || 
             (this.selectedPlanCode!=null &&
              this.selectedPlanCode.equals(other.getSelectedPlanCode()))) &&
            ((this.selectedSSRFeeCode==null && other.getSelectedSSRFeeCode()==null) || 
             (this.selectedSSRFeeCode!=null &&
              this.selectedSSRFeeCode.equals(other.getSelectedSSRFeeCode()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            this.passengerPremiumAmount == other.getPassengerPremiumAmount();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIsInfant() != null) {
            _hashCode += getIsInfant().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getDOB() != null) {
            _hashCode += getDOB().hashCode();
        }
        if (getAge() != null) {
            _hashCode += getAge().hashCode();
        }
        if (getIdentityType() != null) {
            _hashCode += getIdentityType().hashCode();
        }
        if (getIdentityNo() != null) {
            _hashCode += getIdentityNo().hashCode();
        }
        _hashCode += (isIsQualified() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getNationality() != null) {
            _hashCode += getNationality().hashCode();
        }
        if (getCountryOfResidence() != null) {
            _hashCode += getCountryOfResidence().hashCode();
        }
        if (getSelectedPlanCode() != null) {
            _hashCode += getSelectedPlanCode().hashCode();
        }
        if (getSelectedSSRFeeCode() != null) {
            _hashCode += getSelectedSSRFeeCode().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        _hashCode += new Double(getPassengerPremiumAmount()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItineraryPassenger.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryPassenger"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isInfant");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IsInfant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "GenderType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("age");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Age"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identityType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IdentityType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IdentificationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identityNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IdentityNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isQualified");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "IsQualified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationality");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Nationality"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryOfResidence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CountryOfResidence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selectedPlanCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "SelectedPlanCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selectedSSRFeeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "SelectedSSRFeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passengerPremiumAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PassengerPremiumAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
