/**
 * InputRequestOTA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class InputRequestOTA  implements java.io.Serializable {
    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderOTA header;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight flights;

    public InputRequestOTA() {
    }

    public InputRequestOTA(
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderOTA header,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight flights) {
           this.authentication = authentication;
           this.header = header;
           this.flights = flights;
    }


    /**
     * Gets the authentication value for this InputRequestOTA.
     * 
     * @return authentication
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this InputRequestOTA.
     * 
     * @param authentication
     */
    public void setAuthentication(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication authentication) {
        this.authentication = authentication;
    }


    /**
     * Gets the header value for this InputRequestOTA.
     * 
     * @return header
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderOTA getHeader() {
        return header;
    }


    /**
     * Sets the header value for this InputRequestOTA.
     * 
     * @param header
     */
    public void setHeader(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderOTA header) {
        this.header = header;
    }


    /**
     * Gets the flights value for this InputRequestOTA.
     * 
     * @return flights
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight getFlights() {
        return flights;
    }


    /**
     * Sets the flights value for this InputRequestOTA.
     * 
     * @param flights
     */
    public void setFlights(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight flights) {
        this.flights = flights;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InputRequestOTA)) return false;
        InputRequestOTA other = (InputRequestOTA) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication()))) &&
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.flights==null && other.getFlights()==null) || 
             (this.flights!=null &&
              this.flights.equals(other.getFlights())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getFlights() != null) {
            _hashCode += getFlights().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InputRequestOTA.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InputRequestOTA"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryAuthentication"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeaderOTA"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flights");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Flights"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryFlight"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
