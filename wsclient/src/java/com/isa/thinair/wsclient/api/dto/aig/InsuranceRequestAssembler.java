package com.isa.thinair.wsclient.api.dto.aig;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Insurance for AIG ToAssembler Design Pattern.
 * 
 * @author Byorn.
 * 
 */
public class InsuranceRequestAssembler implements IInsuranceRequest, Serializable {

	private static final long serialVersionUID = 1L;

	private static Log log = LogFactory.getLog(InsuranceRequestAssembler.class);

	private InsureSegment insureSegment;
	private Collection<InsurePassenger> passengers;
	private Integer insuranceId;

	private Date now;
	private String messageId;
	private Map<Integer, BigDecimal> paxSequenceInsuranceChargeMap;
	private BigDecimal totalPremiumAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String pnr;
	private String policyCode;
	private BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal quotedTotalPremiumAmount = null;
	private int insuranceType;
	private Integer autoCancellationId;
	private boolean isAllDomastic = true;
	private LCCInsuredContactInfoDTO contactInfo;
	private String prefLanguage;
	private String channel;
	private String currency;
	private String ssrFeeCode;
	private String planCode;
	private List<InsurableFlightSegment> flightSegments;
	private String operatingAirline;

	public InsuranceRequestAssembler() {
		now = new Date();
	}

	/** Use by the Front End **/
	public void addFlightSegment(InsureSegment insureSegment) {
		if (this.insureSegment == null) {
			this.insureSegment = insureSegment;
		}
	}

	/** Use by the Front End **/
	public void addPassenger(InsurePassenger passenger) {

		if (passengers == null) {
			passengers = new ArrayList<InsurePassenger>();
		}
		if (passenger.isInfant()) {
			// do nothing
		} else {
			passengers.add(passenger);
		}
	}

	public String getDateTimeNow() {
		return AigTOUtils.getAIGDate(now);
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void constructNewMessageId(String gdsCode, String iataCountryCode) {
		setMessageId(gdsCode + iataCountryCode + AigTOUtils.getMessageIdDateDetails(now));
	}

	public Collection<InsurePassenger> getPassengers() {
		return passengers;
	}

	public InsureSegment getInsureSegment() {
		return this.insureSegment;
	}

	public String getInsuranceEndDate(String hoursToAdd) {
		if (this.insureSegment.getArrivalDate() == null
				|| (this.insureSegment.getArrivalDate() != null && // in case of reSellingInsurance Schedule job
						this.insureSegment.getDepartureDate() != null && this.insureSegment.getArrivalDate().getTime() == this.insureSegment
						.getDepartureDate().getTime())) {

			this.insureSegment.setArrivalDate(insureSegment.getDepartureDate());
			hoursToAdd = "24";
		}
		return AigTOUtils.getAIGDate(CalendarUtil.addHours(this.insureSegment.getArrivalDate(), Integer.parseInt(hoursToAdd)));
	}

	public void addInsuranceCharge(Integer paxSequenceId, BigDecimal insuranceAmount) {
		if (paxSequenceInsuranceChargeMap == null) {
			paxSequenceInsuranceChargeMap = new HashMap<Integer, BigDecimal>();
		}
		paxSequenceInsuranceChargeMap.put(paxSequenceId, insuranceAmount);
		this.totalPremiumAmount = AccelAeroCalculator.add(totalPremiumAmount, insuranceAmount);
	}

	public void prepareOriginAndDestinationInfo(Collection ondFareDtos) {
		InsureSegment insureSegment = null;
		if (this.insureSegment == null) {
			insureSegment = new InsureSegment();
		} else {
			insureSegment = this.insureSegment;
		}

		String origin = null;
		String furthestCity = null;

		if (this.insureSegment != null) {
			origin = this.insureSegment.getFromAirportCode();
			furthestCity = this.insureSegment.getToAirportCode();
		}

		try {
			Iterator iter = ondFareDtos.iterator();
			while (iter.hasNext()) {
				OndFareDTO ondFareDTO = (OndFareDTO) iter.next();
				// if outward segment ?
				if (!ondFareDTO.isInBoundOnd()) {
					String ondCode = ondFareDTO.getOndCode();

					// if single leg?
					if (ondCode == null) {
						List<SegmentSeatDistsDTO> segmentSeatDistDTO = (List<SegmentSeatDistsDTO>) ondFareDTO
								.getSegmentSeatDistsDTO();
						if (segmentSeatDistDTO != null || segmentSeatDistDTO.size() == 1) {
							SegmentSeatDistsDTO segmentseatdistdto = segmentSeatDistDTO.get(0);
							origin = segmentseatdistdto.getSegmentCode().substring(0, 3);
							furthestCity = segmentseatdistdto.getSegmentCode().substring(
									segmentseatdistdto.getSegmentCode().length() - 3);
						} else {

						}
					}

					// multiple legs - MCT/SHJ/CMB
					else {
						if (ondFareDTO.getOndSequence() == 0) {
							origin = ondCode.substring(0, 3);
							furthestCity = ondCode.substring(ondCode.length() - 3);
						}
					}
				}

			}
		} catch (Throwable e) {
			log.error(e);
		}

		insureSegment.setFromAirportCode(origin);
		insureSegment.setToAirportCode(furthestCity);
	}

	public void prepareFlightSegmentsInfo(Collection ondFareDtos) {
		try {
			Iterator iter = ondFareDtos.iterator();
			while (iter.hasNext()) {
				OndFareDTO ondFareDTO = (OndFareDTO) iter.next();
				InsurableFlightSegment flgtSegInfo = new InsurableFlightSegment();
				List<InsurableFlightSegment> flightSegments = new ArrayList<InsurableFlightSegment>();
				HashMap<Integer, FlightSegmentDTO> segmentMap = ondFareDTO.getSegmentsMap();
				Iterator<FlightSegmentDTO> it = segmentMap.values().iterator();
				while (it.hasNext()) {
					FlightSegmentDTO flightSegmentDTO = it.next();
					flgtSegInfo.setDepartureStationCode(flightSegmentDTO.getSegmentCode().substring(0, 3));
					flgtSegInfo.setDepartureDateTimeLocal(flightSegmentDTO.getDepartureDateTime());
					flgtSegInfo.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
					flgtSegInfo.setArrivalStationCode(flightSegmentDTO.getSegmentCode().substring(
							flightSegmentDTO.getSegmentCode().length() - 3, flightSegmentDTO.getSegmentCode().length()));
					flgtSegInfo.setFlightNo(flightSegmentDTO.getFlightNumber());
					flightSegments.add(flgtSegInfo);
				}
				this.setFlightSegments(flightSegments);
			}
		} catch (Throwable e) {
			log.error(e);
		}

	}

	public void setTotalPremiumAmount(BigDecimal totalpremAmt) {
		this.totalPremiumAmount = totalpremAmt;
	}

	/**
	 * @return the totalPremiumAmount
	 */
	public BigDecimal getTotalPremiumAmount() throws ModuleException {

		// FIXME RaK insurance fail with modification flow with zero totalPremiumAmount
		// Create flow should should check this validation and Rak Modification flow shoud avoid this validation

		/*
		 * if(totalPremiumAmount==null||totalPremiumAmount.doubleValue()==0){ throw new
		 * ModuleException("airreservation.aig.sell.zero"); }
		 */
		return totalPremiumAmount;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}

	/**
	 * @return the quotedTotalPremiumAmount
	 */
	public BigDecimal getQuotedTotalPremiumAmount() {
		return quotedTotalPremiumAmount;
	}

	/**
	 * @param quotedTotalPremiumAmount
	 *            the quotedTotalPremiumAmount to set
	 */
	public void setQuotedTotalPremiumAmount(BigDecimal quotedTotalPremiumAmount) {
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
	}

	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

	public void setTotalTicketPrice(BigDecimal totalTicketAmount) {
		this.totalTicketPrice = totalTicketAmount;

	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

	public int getNoOfPax() {
		if (passengers != null)
			return passengers.size();
		else
			return 0;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public boolean isAllDomastic() {
		return isAllDomastic;
	}

	public void setAllDomastic(boolean isAllDomastic) {
		this.isAllDomastic = isAllDomastic;
	}

	public void setLccInsuranceContactInfo(LCCInsuredContactInfoDTO contactInfo) {
		// TODO Auto-generated method stub
		this.contactInfo = contactInfo;

	}

	public LCCInsuredContactInfoDTO getLccInsuranceContactInfo() {
		return this.contactInfo;
	}

	@Override
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getPolicyCode() {
		return this.policyCode;
	}

	public String getPrefLanguage() {
		return prefLanguage;
	}

	public void setPrefLanguage(String prefLanguage) {
		this.prefLanguage = prefLanguage;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSsrFeeCode() {
		return ssrFeeCode;
	}

	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public List<InsurableFlightSegment> getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(List<InsurableFlightSegment> insurableFlightSegments) {
		this.flightSegments = insurableFlightSegments;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

}
