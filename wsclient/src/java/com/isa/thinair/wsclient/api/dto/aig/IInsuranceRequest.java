package com.isa.thinair.wsclient.api.dto.aig;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;

/**
 * 
 * @author Byorn.
 * 
 */
public interface IInsuranceRequest {

	public static enum REQ_TYPE {
		SELL, QUOTE
	};

	public void setInsuranceId(Integer insuranceId);

	public void addFlightSegment(InsureSegment insureSegment);

	public void addPassenger(InsurePassenger passenger);

	public void addInsuranceCharge(Integer paxSequenceId, BigDecimal insuranceAmount);

	public void prepareOriginAndDestinationInfo(Collection ondFareDtos);

	public void prepareFlightSegmentsInfo(Collection ondFareDtos);

	public void setQuotedTotalPremiumAmount(BigDecimal quotedTotalPremiumAmount);

	public void setTotalTicketPrice(BigDecimal totalTicketPrice);

	public void setInsuranceType(int insType);

	public void setPnr(String pnr);

	public void setPolicyCode(String policyCode);

	public int getNoOfPax();

	public void setAutoCancellationId(Integer autoCancellationId);

	public boolean isAllDomastic();

	public void setAllDomastic(boolean isAllDomastic);

	public void setLccInsuranceContactInfo(LCCInsuredContactInfoDTO contactInfo);

	public void setCurrency(String currency);

	public void setChannel(String channel);

	public void setPrefLanguage(String prefLanguage);

	public void setSsrFeeCode(String ssrFeeCode);

	public void setPlanCode(String planCode);

	public void setFlightSegments(List<InsurableFlightSegment> insurableFlightSegments);

	public void setOperatingAirline(String operatingAirline);
	
	public Integer getInsuranceId();

}
