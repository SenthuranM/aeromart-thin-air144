package com.isa.thinair.wsclient.api.dto.rak;

import java.io.Serializable;

public class Travelers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 796662344303868166L;

	private String bookingNo;

	private String sequenceNo;

	private String title;

	private String firstName;

	private String lastName;

	private String dob;

	private boolean oneWay;

	private String destinationAirport;

	private String returnDate;

	private String premium;

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public boolean isOneWay() {
		return oneWay;
	}

	public void setOneWay(boolean oneWay) {
		this.oneWay = oneWay;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String toXML() {
		StringBuilder xml = new StringBuilder();

		xml.append("<Travelers>");
		xml.append("<BookingNo>" + getBookingNo() + "</BookingNo>");
		xml.append("<SeqNo>" + getSequenceNo() + "</SeqNo>");
		xml.append("<Title>" + getTitle() + "</Title>");
		xml.append("<FirstName>" + getFirstName() + "</FirstName>");
		xml.append("<FamilyName>" + getLastName() + "</FamilyName>");
		xml.append("<BirthDate>" + getDob() + "</BirthDate>");
		xml.append("<OneWay>" + isOneWay() + "</OneWay>");
		xml.append("<DestinationAirport>" + getDestinationAirport() + "</DestinationAirport>");
		xml.append("<ReturnDate>" + getReturnDate() + "</ReturnDate>");
		xml.append("<Premium>" + getPremium() + "</Premium>");
		xml.append("</Travelers>");

		return xml.toString();
	}

}
