package com.isa.thinair.wsclient.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.model.ReservationInsurance;

/**
 * This is for Handle multiple insurance quotes.
 *  
 * @author primal
 * 
 */

public class InsuranceQuote implements Serializable{

	private static final long serialVersionUID = 2088361985042827131L;
	
	private String ssrFeeCode;
	private String planCode;
	private String planDesc;
	private BigDecimal quotedTotalPremiumAmount;
	private BigDecimal totalPremiumAmount;
	private String quotedCurrencyCode;
	private String termNCondition;
	private String planCoverdInfoDescription;
	private ReservationInsurance reservationInsurance;
	
	
	public String getSsrFeeCode() {
		return ssrFeeCode;
	}
	
	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}
	
	public String getPlanCode() {
		return planCode;
	}
	
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	
	public String getPlanDesc() {
		return planDesc;
	}
	
	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}
	
	public BigDecimal getQuotedTotalPremiumAmount() {
		return quotedTotalPremiumAmount;
	}
	
	public void setQuotedTotalPremiumAmount(BigDecimal quotedTotalPremiumAmount) {
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
	}
	
	public BigDecimal getTotalPremiumAmount() {
		return totalPremiumAmount;
	}
	
	public void setTotalPremiumAmount(BigDecimal totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}
	
	public String getQuotedCurrencyCode() {
		return quotedCurrencyCode;
	}
	
	public void setQuotedCurrencyCode(String quotedCurrencyCode) {
		this.quotedCurrencyCode = quotedCurrencyCode;
	}
	
	public String getTermNCondition() {
		return termNCondition;
	}
	
	public void setTermNCondition(String termNCondition) {
		this.termNCondition = termNCondition;
	}
	
	public String getPlanCoverdInfoDescription() {
		return planCoverdInfoDescription;
	}
	
	public void setPlanCoverdInfoDescription(String planCoverdInfoDescription) {
		this.planCoverdInfoDescription = planCoverdInfoDescription;
	}
	
	public ReservationInsurance getReservationInsurance() {
		return reservationInsurance;
	}
	
	public void setReservationInsurance(ReservationInsurance reservationInsurance) {
		this.reservationInsurance = reservationInsurance;
	}
	
}
