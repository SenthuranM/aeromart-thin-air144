package com.isa.thinair.wsclient.api.dto.rak;

import java.io.Serializable;
import java.util.List;

public class Flight implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8174882906842791425L;

	private String recordsNo;

	private String flightNo;

	private String originAirport;

	private String flightDepartureDate;

	private String notificationSendingTime;

	private List<Travelers> travelers;

	public String getRecordsNo() {
		return recordsNo;
	}

	public void setRecordsNo(String recordsNo) {
		this.recordsNo = recordsNo;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getFlightDepartureDate() {
		return flightDepartureDate;
	}

	public void setFlightDepartureDate(String flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}

	public String getNotificationSendingTime() {
		return notificationSendingTime;
	}

	public void setNotificationSendingTime(String notificationSendingTime) {
		this.notificationSendingTime = notificationSendingTime;
	}

	public List<Travelers> getTravelers() {
		return travelers;
	}

	public void setTravelers(List<Travelers> travelers) {
		this.travelers = travelers;
	}

	public String toXML() {
		StringBuilder xml = new StringBuilder();

		xml.append("<RecordsNo>" + getRecordsNo() + "</RecordsNo>");
		xml.append("<FlightNo>" + getFlightNo() + "</FlightNo>");
		xml.append("<OriginAirport>" + getOriginAirport() + "</OriginAirport>");
		xml.append("<FlightDateTime>" + getFlightDepartureDate() + "</FlightDateTime>");
		xml.append("<SendingDateTime>" + getNotificationSendingTime() + "</SendingDateTime>");

		if (this.getTravelers() != null && this.getTravelers().size() > 0) {
			for (Travelers travelers : this.getTravelers()) {
				xml.append(travelers.toXML());
			}
		}

		return xml.toString();
	}

}
