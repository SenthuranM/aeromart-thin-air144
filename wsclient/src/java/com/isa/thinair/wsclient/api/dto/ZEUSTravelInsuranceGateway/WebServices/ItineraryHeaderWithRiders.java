/**
 * ItineraryHeaderWithRiders.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class ItineraryHeaderWithRiders  implements java.io.Serializable {
    private java.lang.String channel;

    private java.lang.String itineraryID;

    private java.lang.String PNR;

    private java.lang.String policyNo;

    private java.lang.String purchaseDate;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain[] purchasesMains;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider[] purchasesRiders;

    private java.lang.String countryCode;

    private java.lang.String cultureCode;

    private int totalAdults;

    private int totalChild;

    private int totalInfants;

    public ItineraryHeaderWithRiders() {
    }

    public ItineraryHeaderWithRiders(
           java.lang.String channel,
           java.lang.String itineraryID,
           java.lang.String PNR,
           java.lang.String policyNo,
           java.lang.String purchaseDate,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain[] purchasesMains,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider[] purchasesRiders,
           java.lang.String countryCode,
           java.lang.String cultureCode,
           int totalAdults,
           int totalChild,
           int totalInfants) {
           this.channel = channel;
           this.itineraryID = itineraryID;
           this.PNR = PNR;
           this.policyNo = policyNo;
           this.purchaseDate = purchaseDate;
           this.purchasesMains = purchasesMains;
           this.purchasesRiders = purchasesRiders;
           this.countryCode = countryCode;
           this.cultureCode = cultureCode;
           this.totalAdults = totalAdults;
           this.totalChild = totalChild;
           this.totalInfants = totalInfants;
    }


    /**
     * Gets the channel value for this ItineraryHeaderWithRiders.
     * 
     * @return channel
     */
    public java.lang.String getChannel() {
        return channel;
    }


    /**
     * Sets the channel value for this ItineraryHeaderWithRiders.
     * 
     * @param channel
     */
    public void setChannel(java.lang.String channel) {
        this.channel = channel;
    }


    /**
     * Gets the itineraryID value for this ItineraryHeaderWithRiders.
     * 
     * @return itineraryID
     */
    public java.lang.String getItineraryID() {
        return itineraryID;
    }


    /**
     * Sets the itineraryID value for this ItineraryHeaderWithRiders.
     * 
     * @param itineraryID
     */
    public void setItineraryID(java.lang.String itineraryID) {
        this.itineraryID = itineraryID;
    }


    /**
     * Gets the PNR value for this ItineraryHeaderWithRiders.
     * 
     * @return PNR
     */
    public java.lang.String getPNR() {
        return PNR;
    }


    /**
     * Sets the PNR value for this ItineraryHeaderWithRiders.
     * 
     * @param PNR
     */
    public void setPNR(java.lang.String PNR) {
        this.PNR = PNR;
    }


    /**
     * Gets the policyNo value for this ItineraryHeaderWithRiders.
     * 
     * @return policyNo
     */
    public java.lang.String getPolicyNo() {
        return policyNo;
    }


    /**
     * Sets the policyNo value for this ItineraryHeaderWithRiders.
     * 
     * @param policyNo
     */
    public void setPolicyNo(java.lang.String policyNo) {
        this.policyNo = policyNo;
    }


    /**
     * Gets the purchaseDate value for this ItineraryHeaderWithRiders.
     * 
     * @return purchaseDate
     */
    public java.lang.String getPurchaseDate() {
        return purchaseDate;
    }


    /**
     * Sets the purchaseDate value for this ItineraryHeaderWithRiders.
     * 
     * @param purchaseDate
     */
    public void setPurchaseDate(java.lang.String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }


    /**
     * Gets the purchasesMains value for this ItineraryHeaderWithRiders.
     * 
     * @return purchasesMains
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain[] getPurchasesMains() {
        return purchasesMains;
    }


    /**
     * Sets the purchasesMains value for this ItineraryHeaderWithRiders.
     * 
     * @param purchasesMains
     */
    public void setPurchasesMains(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain[] purchasesMains) {
        this.purchasesMains = purchasesMains;
    }


    /**
     * Gets the purchasesRiders value for this ItineraryHeaderWithRiders.
     * 
     * @return purchasesRiders
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider[] getPurchasesRiders() {
        return purchasesRiders;
    }


    /**
     * Sets the purchasesRiders value for this ItineraryHeaderWithRiders.
     * 
     * @param purchasesRiders
     */
    public void setPurchasesRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider[] purchasesRiders) {
        this.purchasesRiders = purchasesRiders;
    }


    /**
     * Gets the countryCode value for this ItineraryHeaderWithRiders.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this ItineraryHeaderWithRiders.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the cultureCode value for this ItineraryHeaderWithRiders.
     * 
     * @return cultureCode
     */
    public java.lang.String getCultureCode() {
        return cultureCode;
    }


    /**
     * Sets the cultureCode value for this ItineraryHeaderWithRiders.
     * 
     * @param cultureCode
     */
    public void setCultureCode(java.lang.String cultureCode) {
        this.cultureCode = cultureCode;
    }


    /**
     * Gets the totalAdults value for this ItineraryHeaderWithRiders.
     * 
     * @return totalAdults
     */
    public int getTotalAdults() {
        return totalAdults;
    }


    /**
     * Sets the totalAdults value for this ItineraryHeaderWithRiders.
     * 
     * @param totalAdults
     */
    public void setTotalAdults(int totalAdults) {
        this.totalAdults = totalAdults;
    }


    /**
     * Gets the totalChild value for this ItineraryHeaderWithRiders.
     * 
     * @return totalChild
     */
    public int getTotalChild() {
        return totalChild;
    }


    /**
     * Sets the totalChild value for this ItineraryHeaderWithRiders.
     * 
     * @param totalChild
     */
    public void setTotalChild(int totalChild) {
        this.totalChild = totalChild;
    }


    /**
     * Gets the totalInfants value for this ItineraryHeaderWithRiders.
     * 
     * @return totalInfants
     */
    public int getTotalInfants() {
        return totalInfants;
    }


    /**
     * Sets the totalInfants value for this ItineraryHeaderWithRiders.
     * 
     * @param totalInfants
     */
    public void setTotalInfants(int totalInfants) {
        this.totalInfants = totalInfants;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItineraryHeaderWithRiders)) return false;
        ItineraryHeaderWithRiders other = (ItineraryHeaderWithRiders) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.channel==null && other.getChannel()==null) || 
             (this.channel!=null &&
              this.channel.equals(other.getChannel()))) &&
            ((this.itineraryID==null && other.getItineraryID()==null) || 
             (this.itineraryID!=null &&
              this.itineraryID.equals(other.getItineraryID()))) &&
            ((this.PNR==null && other.getPNR()==null) || 
             (this.PNR!=null &&
              this.PNR.equals(other.getPNR()))) &&
            ((this.policyNo==null && other.getPolicyNo()==null) || 
             (this.policyNo!=null &&
              this.policyNo.equals(other.getPolicyNo()))) &&
            ((this.purchaseDate==null && other.getPurchaseDate()==null) || 
             (this.purchaseDate!=null &&
              this.purchaseDate.equals(other.getPurchaseDate()))) &&
            ((this.purchasesMains==null && other.getPurchasesMains()==null) || 
             (this.purchasesMains!=null &&
              java.util.Arrays.equals(this.purchasesMains, other.getPurchasesMains()))) &&
            ((this.purchasesRiders==null && other.getPurchasesRiders()==null) || 
             (this.purchasesRiders!=null &&
              java.util.Arrays.equals(this.purchasesRiders, other.getPurchasesRiders()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.cultureCode==null && other.getCultureCode()==null) || 
             (this.cultureCode!=null &&
              this.cultureCode.equals(other.getCultureCode()))) &&
            this.totalAdults == other.getTotalAdults() &&
            this.totalChild == other.getTotalChild() &&
            this.totalInfants == other.getTotalInfants();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChannel() != null) {
            _hashCode += getChannel().hashCode();
        }
        if (getItineraryID() != null) {
            _hashCode += getItineraryID().hashCode();
        }
        if (getPNR() != null) {
            _hashCode += getPNR().hashCode();
        }
        if (getPolicyNo() != null) {
            _hashCode += getPolicyNo().hashCode();
        }
        if (getPurchaseDate() != null) {
            _hashCode += getPurchaseDate().hashCode();
        }
        if (getPurchasesMains() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPurchasesMains());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPurchasesMains(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPurchasesRiders() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPurchasesRiders());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPurchasesRiders(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getCultureCode() != null) {
            _hashCode += getCultureCode().hashCode();
        }
        _hashCode += getTotalAdults();
        _hashCode += getTotalChild();
        _hashCode += getTotalInfants();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItineraryHeaderWithRiders.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryHeaderWithRiders"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Channel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchaseDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchasesMains");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchasesMains"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseMain"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseMain"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchasesRiders");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchasesRiders"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseRider"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PurchaseRider"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cultureCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "CultureCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAdults");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalAdults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalChild");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalChild"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalInfants");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalInfants"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
