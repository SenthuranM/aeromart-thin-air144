package com.isa.thinair.wsclient.api.dto.aig;

import java.io.Serializable;
import java.util.Date;

public class InsureSegment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3537430461517909961L;
	private String fromAirportCode;
	private String toAirportCode;
	private Date departureDate;
	private String departureDateOffset;
	private Date arrivalDate;
	private String arrivalDateOffset;
	private boolean roundTrip;
	private Integer salesChanelCode;
	private String accountNumber;
	private String departureFlightNo;
	private String arrivalFlightNo;

	public String getFromAirportCode() {
		return fromAirportCode;
	}

	public void setFromAirportCode(String fromAirportCode) {
		this.fromAirportCode = fromAirportCode;
	}

	public String getToAirportCode() {
		return toAirportCode;
	}

	public void setToAirportCode(String toAirportCode) {
		this.toAirportCode = toAirportCode;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Integer getSalesChanelCode() {
		return salesChanelCode;
	}

	public void setSalesChanelCode(Integer salesChanelCode) {
		this.salesChanelCode = salesChanelCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDepartureDateOffset() {
		return departureDateOffset;
	}

	public void setDepartureDateOffset(String departureDateOffset) {
		this.departureDateOffset = departureDateOffset;
	}

	public String getArrivalDateOffset() {
		return arrivalDateOffset;
	}

	public void setArrivalDateOffset(String arrivalDateOffset) {
		this.arrivalDateOffset = arrivalDateOffset;
	}

	public String getDepartureFlightNo() {
		return departureFlightNo;
	}

	public void setDepartureFlightNo(String departureFlightNo) {
		this.departureFlightNo = departureFlightNo;
	}

	public String getArrivalFlightNo() {
		return arrivalFlightNo;
	}

	public void setArrivalFlightNo(String arrivalFlightNo) {
		this.arrivalFlightNo = arrivalFlightNo;
	}

}
