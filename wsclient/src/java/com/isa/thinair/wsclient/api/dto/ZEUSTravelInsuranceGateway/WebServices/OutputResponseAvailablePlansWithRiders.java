/**
 * OutputResponseAvailablePlansWithRiders.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class OutputResponseAvailablePlansWithRiders  implements java.io.Serializable {
    private java.lang.String insuranceTitle;

    private java.lang.String insuranceDesc;

    private int totalPlansAvailable;

    private int totalRiderPlansAvailable;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availablePlans;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availableRiderPlans;

    public OutputResponseAvailablePlansWithRiders() {
    }

    public OutputResponseAvailablePlansWithRiders(
           java.lang.String insuranceTitle,
           java.lang.String insuranceDesc,
           int totalPlansAvailable,
           int totalRiderPlansAvailable,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availablePlans,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availableRiderPlans) {
           this.insuranceTitle = insuranceTitle;
           this.insuranceDesc = insuranceDesc;
           this.totalPlansAvailable = totalPlansAvailable;
           this.totalRiderPlansAvailable = totalRiderPlansAvailable;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.availablePlans = availablePlans;
           this.availableRiderPlans = availableRiderPlans;
    }


    /**
     * Gets the insuranceTitle value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return insuranceTitle
     */
    public java.lang.String getInsuranceTitle() {
        return insuranceTitle;
    }


    /**
     * Sets the insuranceTitle value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param insuranceTitle
     */
    public void setInsuranceTitle(java.lang.String insuranceTitle) {
        this.insuranceTitle = insuranceTitle;
    }


    /**
     * Gets the insuranceDesc value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return insuranceDesc
     */
    public java.lang.String getInsuranceDesc() {
        return insuranceDesc;
    }


    /**
     * Sets the insuranceDesc value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param insuranceDesc
     */
    public void setInsuranceDesc(java.lang.String insuranceDesc) {
        this.insuranceDesc = insuranceDesc;
    }


    /**
     * Gets the totalPlansAvailable value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return totalPlansAvailable
     */
    public int getTotalPlansAvailable() {
        return totalPlansAvailable;
    }


    /**
     * Sets the totalPlansAvailable value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param totalPlansAvailable
     */
    public void setTotalPlansAvailable(int totalPlansAvailable) {
        this.totalPlansAvailable = totalPlansAvailable;
    }


    /**
     * Gets the totalRiderPlansAvailable value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return totalRiderPlansAvailable
     */
    public int getTotalRiderPlansAvailable() {
        return totalRiderPlansAvailable;
    }


    /**
     * Sets the totalRiderPlansAvailable value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param totalRiderPlansAvailable
     */
    public void setTotalRiderPlansAvailable(int totalRiderPlansAvailable) {
        this.totalRiderPlansAvailable = totalRiderPlansAvailable;
    }


    /**
     * Gets the errorCode value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the availablePlans value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return availablePlans
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] getAvailablePlans() {
        return availablePlans;
    }


    /**
     * Sets the availablePlans value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param availablePlans
     */
    public void setAvailablePlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availablePlans) {
        this.availablePlans = availablePlans;
    }


    /**
     * Gets the availableRiderPlans value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @return availableRiderPlans
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] getAvailableRiderPlans() {
        return availableRiderPlans;
    }


    /**
     * Sets the availableRiderPlans value for this OutputResponseAvailablePlansWithRiders.
     * 
     * @param availableRiderPlans
     */
    public void setAvailableRiderPlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan[] availableRiderPlans) {
        this.availableRiderPlans = availableRiderPlans;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputResponseAvailablePlansWithRiders)) return false;
        OutputResponseAvailablePlansWithRiders other = (OutputResponseAvailablePlansWithRiders) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.insuranceTitle==null && other.getInsuranceTitle()==null) || 
             (this.insuranceTitle!=null &&
              this.insuranceTitle.equals(other.getInsuranceTitle()))) &&
            ((this.insuranceDesc==null && other.getInsuranceDesc()==null) || 
             (this.insuranceDesc!=null &&
              this.insuranceDesc.equals(other.getInsuranceDesc()))) &&
            this.totalPlansAvailable == other.getTotalPlansAvailable() &&
            this.totalRiderPlansAvailable == other.getTotalRiderPlansAvailable() &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.availablePlans==null && other.getAvailablePlans()==null) || 
             (this.availablePlans!=null &&
              java.util.Arrays.equals(this.availablePlans, other.getAvailablePlans()))) &&
            ((this.availableRiderPlans==null && other.getAvailableRiderPlans()==null) || 
             (this.availableRiderPlans!=null &&
              java.util.Arrays.equals(this.availableRiderPlans, other.getAvailableRiderPlans())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInsuranceTitle() != null) {
            _hashCode += getInsuranceTitle().hashCode();
        }
        if (getInsuranceDesc() != null) {
            _hashCode += getInsuranceDesc().hashCode();
        }
        _hashCode += getTotalPlansAvailable();
        _hashCode += getTotalRiderPlansAvailable();
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getAvailablePlans() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailablePlans());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailablePlans(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAvailableRiderPlans() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailableRiderPlans());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailableRiderPlans(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputResponseAvailablePlansWithRiders.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansWithRiders"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuranceTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InsuranceTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuranceDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InsuranceDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPlansAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalPlansAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalRiderPlansAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalRiderPlansAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availablePlans");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Plan"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlan"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableRiderPlans");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailableRiderPlans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "Plan"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailableRiderPlan"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
