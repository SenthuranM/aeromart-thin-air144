package com.isa.thinair.wsclient.api.service;

import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;
import com.isa.thinair.wsclient.core.config.WSClientConfig;

public class WSClientModuleUtils {
	private static final String LOYALTY_MANAGMENT_DAO = "LoyaltyManagementDAOProxy";

	/**
	 * Returns Module service interface.
	 * 
	 * @return
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(WsclientConstants.MODULE_NAME);
	}

	/**
	 * Returns Module Config.
	 * 
	 * @return
	 */
	public static WSClientConfig getModuleConfig() {
		return (WSClientConfig) getInstance().getModuleConfig();
	}

	/**
	 * Returns ReservationQueryBD stub.
	 * 
	 * @return
	 */
	public static ReservationQueryBD getResQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Returns FlightInventoryBD stub.
	 * 
	 * @return
	 */
	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	/**
	 * Returns getAirportBD stub.
	 * 
	 * @return
	 */
	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public final static CommonMasterBD getCommonServiceBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getModuleConfig(),
				WsclientConstants.MODULE_NAME, "module.dependencymap.invalid");
	}
	
	public final static LmsMemberServiceBD getLmsMemberServiceBD() {
		return (LmsMemberServiceBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LmsMemberServiceBD.SERVICE_NAME);
	}

}
