package com.isa.thinair.wsclient.api.dto.rak;

import java.io.Serializable;

public class TravelSchema implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4061761023759936356L;
	private Flight flight;

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public String toXML(boolean isJustString) {
		StringBuilder xml = new StringBuilder();

		xml.append("<TravelSchema>");
		xml.append("<Flight>");
		xml.append(flight.toXML());
		xml.append("</Flight>");
		xml.append("</TravelSchema>");

		if (isJustString) {
			return xml.toString();
		} else {
			return "<![CDATA[" + xml.toString() + "]]>";
		}
	}

}
