/**
 * 
 */
package com.isa.thinair.wsclient.api.dto.aig;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Janaka Padukka
 *
 */
public class InsurableFlightSegment implements Serializable, Comparable<InsurableFlightSegment> {
	
	private static final long serialVersionUID = 1L;
	
	String departureStationCode;
	
	String arrivalStationCode;
	
	String flightNo;
	
	Date departureDateTimeLocal;
	
	Date departureDateTimeZulu;
	
	private boolean domesticFlight;
	
	public String getDepartureStationCode() {
		return departureStationCode;
	}
	public void setDepartureStationCode(String departureStationCode) {
		this.departureStationCode = departureStationCode;
	}
	public String getArrivalStationCode() {
		return arrivalStationCode;
	}
	public void setArrivalStationCode(String arrivalStationCode) {
		this.arrivalStationCode = arrivalStationCode;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}
	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}
	
	

	public boolean isDomesticFlight() {
		return domesticFlight;
	}
	public void setDomesticFlight(boolean domesticFlight) {
		this.domesticFlight = domesticFlight;
	}
	@Override
	public int compareTo(InsurableFlightSegment insurableSegment) {

		return this.getDepartureDateTimeZulu().compareTo(insurableSegment.getDepartureDateTimeZulu());
	}
	
	
}
