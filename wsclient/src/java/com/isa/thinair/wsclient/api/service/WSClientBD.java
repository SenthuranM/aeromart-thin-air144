/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.wsclient.api.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckRequestDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakFlightInsuranceInfoDTO;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.paymentbroker.api.dto.PAYPALRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentFieldsResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentIdRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtInquiryRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtPayRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtRefundRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtReversalRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtSettleRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtVerifyRequest;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundServiceResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundStatusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRs;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.reporting.api.model.InventoryAllocationTO;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.dto.netcast.NetCastRequest;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestType;
import com.paypal.soap.api.DoExpressCheckoutPaymentResponseType;
import com.paypal.soap.api.GetExpressCheckoutDetailsRequestType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseDetailsType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.RefundTransactionRequestType;
import com.paypal.soap.api.RefundTransactionResponseType;
import com.paypal.soap.api.SetExpressCheckoutRequestType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

/**
 * @author Mohamed Nasly
 */
public interface WSClientBD {

	public static final String SERVICE_NAME = "WSClientService";

	public String sendNetCastSMSMessage(NetCastRequest request) throws ModuleException;

	/**
	 * Requests external party transaction statuses for transactions of a given PNR.
	 * 
	 * @param transactionStatusEnquiry
	 * @return
	 * @throws ModuleException
	 */
	public PNRExtTransactionsTO getEBITransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO) throws ModuleException;

	/**
	 * Requests external party transaction statuses for transactions of a given PNR.
	 * 
	 * @param transactionStatusEnquiry
	 * @return
	 * @throws ModuleException
	 */
	public PNRExtTransactionsTO getPNRTransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO, String serviceProvider)
			throws ModuleException;

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at external party.
	 * 
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 */
	public void requestDailyBankTnxReconcilation(Date startTimestamp, Date endTimestamp, String serviceProvider)
			throws ModuleException;

	/**
	 * Publish Above Threshold/Below Threshold inventories to RM.
	 * 
	 * @throws ModuleException
	 */
	public void publishRMInventoryAlerts() throws ModuleException;

	/**
	 * Transfer Reservation
	 * 
	 * @param reservation
	 * @param pnrSegIDs
	 * @param interlinedAirLineTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferReservation(Reservation reservation, Collection pnrSegIDs,
			InterlinedAirLineTO interlinedAirLineTO) throws ModuleException;

	/**
	 * Returns the flight inventory allocation information
	 * 
	 * @param allocationTO
	 * @param interLineAirlines
	 * @return
	 * @throws ModuleException
	 */
	public List getFlightInventoryAllocations(InventoryAllocationTO allocationTO, Map interLineAirlines) throws ModuleException;

	/**
	 * Validate CC payment for fraud check using eurocommerce
	 * 
	 * @param paymentRQ
	 * @return
	 * @throws ModuleException
	 */
	public CCFraudCheckResponseDTO validateCCPaymentForFraud(CCFraudCheckRequestDTO paymentRQ) throws ModuleException;

	/** RAK Insurance related methods */
	public InsuranceResponse quoteRAKInsurancePolicy(IInsuranceRequest iQuoteRequest) throws ModuleException;

	public InsuranceResponse sellRAKInsurancePolicy(IInsuranceRequest isellReq) throws ModuleException;

	public InsuranceResponse reSellRAKInsurancePolicy(IInsuranceRequest reSellReq) throws ModuleException;

	public String publishRakInsuranceData(RakFlightInsuranceInfoDTO rakFlightInsuranceInfoDTO) throws ModuleException;

	public String[] getInsurancePubStatus(String response) throws ModuleException;

	public CyberplusResponseDTO cancel(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException;

	public CyberplusResponseDTO modify(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException;

	public CyberplusTransactionInfoDTO getInfo(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException;

	public CyberplusTransactionInfoDTO refund(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException;

	public CyberplusTransactionInfoDTO create(CyberplusRequestDTO paymentInfo) throws ModuleException;

	/*
	 * PAYPAL Transactions
	 */
	public DoDirectPaymentRequestType prepareDoDirectPaymentRequestType(PAYPALRequestDTO paypalRequestDTO) throws ModuleException;

	public DoDirectPaymentResponseType doDirectPaymentWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoDirectPaymentRequestType pprequest) throws ModuleException;

	public RefundTransactionRequestType prepareRefund(PAYPALRequestDTO paypalRequestDTO) throws ModuleException;

	public RefundTransactionResponseType doRefundWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			RefundTransactionRequestType pprequest) throws ModuleException;

	public GetTransactionDetailsRequestType prepareDoGetTransactionDetailsCode(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException;

	public GetTransactionDetailsResponseType doGetTransactionDetailsCode(GetTransactionDetailsRequestType pprequest,
			PAYPALRequestDTO paypalRequestDTO) throws ModuleException;

	public SetExpressCheckoutRequestType prepareSetExpressCheckoutRequestType(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException;

	public SetExpressCheckoutResponseType doSetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			SetExpressCheckoutRequestType pprequest) throws ModuleException;

	public GetExpressCheckoutDetailsRequestType prepareGetExpressCheckoutDetailsRequestType(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException;

	public GetExpressCheckoutDetailsResponseType doGetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			GetExpressCheckoutDetailsRequestType pprequest) throws ModuleException;

	public DoExpressCheckoutPaymentRequestType prepareDoExpressCheckoutPaymentRequestType(
			GetExpressCheckoutDetailsResponseDetailsType response) throws ModuleException;

	public DoExpressCheckoutPaymentResponseType doDoExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoExpressCheckoutPaymentRequestType pprequest) throws ModuleException;

	public SamanResponseDTO verifyTransaction(SamanRequestDTO verifyRequest) throws ModuleException;

	public SamanResponseDTO reverseTransaction(SamanRequestDTO refundRequest) throws ModuleException;

	public String reverseTransactionSRTM(SamanRequestDTO refundRequest) throws ModuleException;

	public SRTMRefundServiceResponseDTO reverseTransactionRegSRTM(SamanRequestDTO refundRequest, String transRefNum, Long tptID)
			throws ModuleException;

	public SRTMRefundServiceResponseDTO reverseTransactionExcSRTM(SamanRequestDTO refundRequest, Long refundRegID)
			throws ModuleException;

	public SRTMRefundStatusResponseDTO getSRTMRefundStatus(SamanRequestDTO refundRequest) throws ModuleException;

	public boolean publishFlightLoadInformation(List<FlightLoad> flightLoadInformationDTOs) throws ModuleException;

	public String postRefundRequest(MIGSRequest refundReuest) throws ModuleException;

	public String verifyTransaction(MIGSRequest refundReuest) throws ModuleException;

	public boolean publishFlightChangeInformation(List<FlightsToPublish> flightsToPublish) throws ModuleException;

	public TypeBRS processTypeBResMessage(TypeBRQ typebReq) throws ModuleException;

	public Pay_AT_StoreResponseDTO payAtStorePaymentTransaction(Pay_AT_StoreRequestDTO payRequest) throws ModuleException;

	public CheckOrderResponseDTO checkTransaction(CheckOrderDTO checkOrderDTO) throws ModuleException;

	public CheckNotificationResponseDTO checkNotification(CheckNotificationDTO checkNotificationDTO) throws ModuleException;

	public Pay_AT_HomeResponseDTO payAtHomePaymentTransaction(Pay_AT_HomeRequestDTO payRequest) throws ModuleException;

	/*
	 * LMS API services
	 */

	public LoyaltyMemberStateDTO getLoyaltyMemberState(String memberAccountId) throws ModuleException;

	public String enrollMemberWithEmail(Customer customer) throws ModuleException;

	public LoyaltyMemberCoreDTO getLoyaltyMemberCoreDetails(String memberAccountId) throws ModuleException;

	public LoyaltyPointDetailsDTO getLoyaltyMemberPointBalances(String memberAccountId, String memberExternalId) throws ModuleException;

	public boolean saveMemberCustomFields(Customer customer) throws ModuleException;

	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException;

	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber) throws ModuleException;
	
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException;
	
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException;

	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode) throws ModuleException;

	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException;

	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException;

	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException;

	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef) throws ModuleException;

	public IssueRewardResponse issueVariableRewards(IssueRewardRequest issueRewardRequest, String pnr, Map<String, Double> productPoints) throws ModuleException;

	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException;

	public boolean cancelMemberRewards(String pnr, String[] rewardIds, String memberAccountId) throws ModuleException;

	boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException;

	/*
	 * Behpardakht Payment Gateway
	 */

	public BehpardakhtResponse verifyRequest(BehpardakhtVerifyRequest verifyRequest) throws ModuleException;

	public BehpardakhtResponse payRequest(BehpardakhtPayRequest payRequest) throws ModuleException;

	public BehpardakhtResponse settleRequest(BehpardakhtSettleRequest settleRequest) throws ModuleException;

	public BehpardakhtResponse inquiryRequest(BehpardakhtInquiryRequest inquiryRequest) throws ModuleException;

	public BehpardakhtResponse reversalRequest(BehpardakhtReversalRequest reversalRequest) throws ModuleException;

	public BehpardakhtResponse refundRequest(BehpardakhtRefundRequest refundRequest) throws ModuleException;
	
	public int saveMemberName(Customer customer) throws ModuleException;
	
	public ParsianPaymentResponseDTO pinPaymentRequest(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException;
	
	public ParsianPaymentResponseDTO pinPaymentEnquiry(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException;

	public ParsianPaymentResponseDTO pinVoidPayment(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException;
	
	public ParsianPaymentResponseDTO pinReversal(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException;
	
	public ParsianRefundResponseDTO doRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException;
	/*
	 * Ameria Bank vPOS (Armenian Payment Gateway)
	 */

	public AmeriaBankCommonResponse getAmeriaBankPaymentId(AmeriaBankPaymentIdRequest payRequest) throws ModuleException;

	public AmeriaBankPaymentFieldsResponse getAmeriaBankPaymentFields(AmeriaBankCommonRequest ameriaBankPaymentFieldsRequest)
			throws ModuleException;

	public AmeriaBankCommonResponse refundAmeriaBankPayment(AmeriaBankCommonRequest refundAmeriaBankPaymentRequest)
			throws ModuleException;

	public AmeriaBankCommonResponse reverseAmeriaBankPayment(AmeriaBankCommonRequest ameriaRefundRequest) throws ModuleException;

	public ParsianRefundResponseDTO approveRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException;
	
	public ParsianRefundResponseDTO cancleSuspendedRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException;
	
	/*
	 * Tap Payment Gateway
	 */
	public TapPaymentRequestCallRs paymentRequest(TapPaymentRequestCallRq paymentRequest) throws ModuleException;
	
	public TapCaptureRequestCallRs capturePayment(TapCaptureRequestCallRq captureRequest) throws ModuleException;
	
	public TapGetOrderStatusRequestCallRs getOrderStatus(TapGetOrderStatusRequestCallRq getOrderRequest) throws ModuleException;
	
	public TapRefundCallRs refund(TapRefundCallRq refundRequest) throws ModuleException;
	
	public TapRefundStatusCallRs getRefundStatus(TapRefundStatusCallRq refundStatusRequest) throws ModuleException;

	public MemberRemoteLoginResponseDTO memberLogin(String userName, String password) throws ModuleException;

	public void updateMember(Customer customer) throws ModuleException;
	
	public String generateS3SignedUrl(String fileName, String mimeType) throws ModuleException;	
}