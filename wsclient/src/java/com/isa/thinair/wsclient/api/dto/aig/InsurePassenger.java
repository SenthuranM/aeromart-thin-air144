package com.isa.thinair.wsclient.api.dto.aig;

import java.io.Serializable;
import java.util.Date;

public class InsurePassenger implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5553710357833067598L;
	private String title;
	private String firstName;
	private String lastName;
	private String addressLn1;
	private String addressLn2;
	private String city;
	private String email;
	private String homePhoneNumber;
	private String countryOfAddress;
	private String nationality;
	private Date dateOfBirth;
	private boolean infant;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressLn1() {
		return addressLn1;
	}

	public void setAddressLn1(String addressLn1) {
		this.addressLn1 = addressLn1;
	}

	public String getAddressLn2() {
		return addressLn2;
	}

	public void setAddressLn2(String addressLn2) {
		this.addressLn2 = addressLn2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getCountryOfAddress() {
		return countryOfAddress;
	}

	public void setCountryOfAddress(String countryOfAddress) {
		this.countryOfAddress = countryOfAddress;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isInfant() {
		return infant;
	}

	public void setInfant(boolean infant) {
		this.infant = infant;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
}
