package com.isa.thinair.wsclient.core.service.aig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.HttpClient;
import com.isa.thinair.wsclient.core.config.AIGClientConfig;
import com.isaaviation.thinair.webservices.api.airinsurance.TINSXMLDATA;

/**
 * 
 * @author Byorn.
 * 
 */
public class AIGProxy {

	private static final Log log = LogFactory.getLog(AIGProxy.class);

	/**
	 * Call AIG External Resource
	 * 
	 * @param requestObject
	 * @return
	 * @throws ModuleException
	 */
	public static TINSXMLDATA callService(TINSXMLDATA requestObject) throws ModuleException {
		log.debug("Entered  callService aig");
		AIGClientConfig aigClientConfig = WSClientModuleUtils.getModuleConfig().getAigClientConfig();

		String _url = aigClientConfig.getUrl();
		String _method = aigClientConfig.getMethod();
		String _timeOut = aigClientConfig.getConnectionTimeOutInSeconds();
		// time out in second..i.e.*1000
		HttpClient httpClient = new HttpClient(_url, _method, Integer.valueOf(_timeOut) * 1000);

		AIGExecutionStrategry aigExecutionStrategry = new AIGExecutionStrategry(requestObject, aigClientConfig.getUrl());

		httpClient.executeOperation(aigExecutionStrategry);

		log.debug("Exit callService aig");
		return aigExecutionStrategry.get_responseTINSXMLDATA();
	}
}