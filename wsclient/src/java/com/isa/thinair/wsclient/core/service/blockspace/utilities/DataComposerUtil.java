package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SourceType.RequestorID;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isa.thinair.wsclient.api.dto.FlightSearchTransferDTO;
import com.isa.thinair.wsclient.api.util.WSClientResponseCodes;

/**
 * WS Client block space data composing utilites make Reservation
 * 
 * @author dumindag
 */
public class DataComposerUtil {

	/**
	 * Prepare the BaseDTO
	 * 
	 * @param agentCode
	 * @return
	 */
	public static BaseDTO prepareBaseDTO(InterlinedAirLineTO interlinedAirLineTO) {
		BaseDTO baseDTO = new BaseDTO();

		baseDTO.setCarrierCode(interlinedAirLineTO.getCarrierCode());
		baseDTO.setUserId(interlinedAirLineTO.getUserId());
		baseDTO.setOnAccountAgentCode(interlinedAirLineTO.getAgentCode());
		baseDTO.setSalesChannelCode(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA));
		baseDTO.setTerminalId("Interlined Booking");

		return baseDTO;
	}

	/**
	 * Prepare the Source Type information
	 * 
	 * @param baseDTO
	 * @return
	 */
	public static SourceType preparePOS(BaseDTO baseDTO) {
		if (baseDTO != null) {
			SourceType sourceType = new SourceType();
			sourceType.setTerminalID(baseDTO.getTerminalId());

			RequestorID requestorID = new RequestorID();
			sourceType.setRequestorID(requestorID);

			requestorID.setID(baseDTO.getUserId());
			requestorID.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_COMPANY));

			SourceType.BookingChannel bookingChannel = new SourceType.BookingChannel();
			sourceType.setBookingChannel(bookingChannel);

			bookingChannel.setType(baseDTO.getSalesChannelCode());

			return sourceType;
		} else {
			return null;
		}
	}

	/**
	 * Returns the airport code
	 * 
	 * @param segmentCode
	 * @param isFromAirport
	 * @return
	 */
	public static String getAirportCode(String segmentCode, boolean isFromAirport) {
		if (isFromAirport) {
			return segmentCode.substring(0, segmentCode.indexOf("/"));
		} else {
			return segmentCode.substring(segmentCode.lastIndexOf("/") + 1, segmentCode.length());
		}
	}

	/**
	 * Returns the external segments
	 * 
	 * @param reservation
	 * @param interlinedAirLineTO
	 * @param reservationSegmentDTO
	 * @return
	 */
	public static Collection<ReservationSegmentDTO> getExternalSegments(Reservation reservation,
			InterlinedAirLineTO interlinedAirLineTO, ReservationSegmentDTO reservationSegmentDTO) {
		Collection<ReservationSegmentDTO> externalSegments = new ArrayList<ReservationSegmentDTO>();
		Integer ondGroupId = reservationSegmentDTO.getFareGroupId();
		ReservationSegmentDTO reservationSegmentDTOObj;

		for (Iterator iter = reservation.getSegmentsView().iterator(); iter.hasNext();) {
			reservationSegmentDTOObj = (ReservationSegmentDTO) iter.next();

			if (reservationSegmentDTOObj.getFareGroupId().intValue() == ondGroupId.intValue()
					&& reservationSegmentDTOObj.getPnrSegId().intValue() != reservationSegmentDTO.getPnrSegId().intValue()
					&& !AppSysParamsUtil.extractCarrierCode(reservationSegmentDTOObj.getFlightNo()).equals(
							interlinedAirLineTO.getCarrierCode())) {
				externalSegments.add(reservationSegmentDTOObj);
			}
		}

		return externalSegments;
	}

	/**
	 * Return the external segments
	 * 
	 * @param reservation
	 * @param interlinedAirLineTO
	 * @param colReservationSegmentDTO
	 * @return
	 */
	public static Collection<ReservationSegmentDTO> getExternalSegments(Reservation reservation,
			InterlinedAirLineTO interlinedAirLineTO, Collection colReservationSegmentDTO) {
		Collection<ReservationSegmentDTO> externalSegments = new ArrayList<ReservationSegmentDTO>();
		Collection<Integer> ondGroupIds = ReservationApiUtils.getONDGroupIds(colReservationSegmentDTO);
		Collection<Integer> pnrSegIds = ReservationApiUtils.getPnrSegIds(colReservationSegmentDTO);
		ReservationSegmentDTO reservationSegmentDTOObj;

		for (Iterator iter = reservation.getSegmentsView().iterator(); iter.hasNext();) {
			reservationSegmentDTOObj = (ReservationSegmentDTO) iter.next();

			if (ondGroupIds.contains(reservationSegmentDTOObj.getFareGroupId())
					&& !pnrSegIds.contains(reservationSegmentDTOObj.getPnrSegId())
					&& !AppSysParamsUtil.extractCarrierCode(reservationSegmentDTOObj.getFlightNo()).equals(
							interlinedAirLineTO.getCarrierCode())) {
				externalSegments.add(reservationSegmentDTOObj);
			}
		}

		return externalSegments;
	}

	/**
	 * Returns the reservation segments
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<ReservationSegmentDTO> getReservationSegments(Reservation reservation, Collection pnrSegIds)
			throws ModuleException {
		Map<Integer, Map<Integer, ReservationSegmentDTO>> ondGrpIdAndSegmentsMap = new HashMap<Integer, Map<Integer, ReservationSegmentDTO>>();
		ReservationSegmentDTO reservationSegmentDTO;
		Map<Integer, ReservationSegmentDTO> connectionSegmentsMap;
		Integer ondGrpId;

		for (Iterator iter = reservation.getSegmentsView().iterator(); iter.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

			if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {

				if (ondGrpIdAndSegmentsMap.containsKey(reservationSegmentDTO.getFareGroupId())) {
					connectionSegmentsMap = ondGrpIdAndSegmentsMap.get(reservationSegmentDTO.getFareGroupId());
					connectionSegmentsMap.put(reservationSegmentDTO.getSegmentSeq(), reservationSegmentDTO);
					;
				} else {
					connectionSegmentsMap = new TreeMap<Integer, ReservationSegmentDTO>();
					connectionSegmentsMap.put(reservationSegmentDTO.getSegmentSeq(), reservationSegmentDTO);
					ondGrpIdAndSegmentsMap.put(reservationSegmentDTO.getFareGroupId(), connectionSegmentsMap);
				}
			}
		}

		// Many connection(s) can exit. Only transfering one connection segment at this time of invocation
		// These connections are same destinations which was added SHJ/KTM Practically users won't enter like this
		// In our reservation we allow to do the same segment twice.
		for (Iterator iter = ondGrpIdAndSegmentsMap.keySet().iterator(); iter.hasNext();) {
			ondGrpId = (Integer) iter.next();
			connectionSegmentsMap = ondGrpIdAndSegmentsMap.get(ondGrpId);
			return connectionSegmentsMap.values();
		}

		throw new ModuleException("wsclient.pnrTransfer.invalid.invalidSegments");
	}

	/**
	 * Compose the FlightSearchTransferDTO object
	 * 
	 * @param reservation
	 * @param reservationSegmentDTO
	 * @return
	 * @throws ModuleException
	 */
	public static FlightSearchTransferDTO composeFlightSearchTransferData(Reservation reservation,
			ReservationSegmentDTO reservationSegmentDTO) throws ModuleException {

		FlightSearchTransferDTO flightSearchTransferDTO = new FlightSearchTransferDTO();

		flightSearchTransferDTO.setFromAirport(getAirportCode(reservationSegmentDTO.getSegmentCode(), true));
		flightSearchTransferDTO.setToAirport(getAirportCode(reservationSegmentDTO.getSegmentCode(), false));
		flightSearchTransferDTO.setDepatureDate(reservationSegmentDTO.getDepartureDate());
		flightSearchTransferDTO.setReturnDate(reservationSegmentDTO.getArrivalDate());
		flightSearchTransferDTO.setFlightNumber(reservationSegmentDTO.getFlightNo());

		flightSearchTransferDTO.setAdultCount(reservation.getTotalPaxAdultCount());
		flightSearchTransferDTO.setChildCount(reservation.getTotalPaxChildCount());
		flightSearchTransferDTO.setInfantCount(reservation.getTotalPaxInfantCount());

		return flightSearchTransferDTO;
	}

	/**
	 * Compose the response segment
	 * 
	 * @param serviceResponce
	 * @param failed_transfer_segment_dtos
	 * @param reservationSegmentDTO
	 */
	private static void composeResponseSegment(DefaultServiceResponse serviceResponce, String key,
			ReservationSegmentDTO reservationSegmentDTO) {

		Collection colReservationSegmentDTO = new ArrayList();

		if (reservationSegmentDTO != null) {
			colReservationSegmentDTO.add(reservationSegmentDTO);
		}

		serviceResponce.addResponceParam(key, colReservationSegmentDTO);
	}

	/**
	 * Compose the response segment
	 * 
	 * @param serviceResponce
	 * @param reservationSegmentDTO
	 */
	public static void assembleResponseSegment(DefaultServiceResponse serviceResponce,
			ReservationSegmentDTO reservationSegmentDTO, boolean isSuccess) {
		if (isSuccess) {
			composeResponseSegment(serviceResponce, WSClientResponseCodes.InterlinedTypes.FAILED_TRANSFER_SEGMENT_DTOS, null);
			composeResponseSegment(serviceResponce, WSClientResponseCodes.InterlinedTypes.SUCCESSFUL_TRANSFER_SEGMENT_DTOS,
					reservationSegmentDTO);
		} else {
			composeResponseSegment(serviceResponce, WSClientResponseCodes.InterlinedTypes.FAILED_TRANSFER_SEGMENT_DTOS,
					reservationSegmentDTO);
			composeResponseSegment(serviceResponce, WSClientResponseCodes.InterlinedTypes.SUCCESSFUL_TRANSFER_SEGMENT_DTOS, null);
		}
	}

	/**
	 * Bundles the responses
	 * 
	 * @param serviceResponce
	 * @param successRecords
	 * @param failedRecords
	 */
	public static void
			buddleResponses(DefaultServiceResponse serviceResponce, Collection successRecords, Collection failedRecords) {

		serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.SUCCESSFUL_TRANSFER_SEGMENT_DTOS, successRecords);
		serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.FAILED_TRANSFER_SEGMENT_DTOS, failedRecords);
	}

	/**
	 * Prepares dummy response
	 * 
	 * @param messageString
	 * @return
	 */
	public static DefaultServiceResponse prepareFailedDummyResponse(String messageString) {
		messageString = BeanUtils.nullHandler(messageString);

		if (messageString.length() == 0) {
			messageString = "Generic Booking Creation Error";
		}

		DefaultServiceResponse serviceResponce = new DefaultServiceResponse(false);
		serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC, messageString);

		return serviceResponce;
	}

	/**
	 * Copy the error
	 * 
	 * @param successServiceResponce
	 * @param tmpServiceResponce
	 */
	public static void copyError(DefaultServiceResponse successServiceResponce, DefaultServiceResponse tmpServiceResponce) {
		String procesDesc = (String) tmpServiceResponce.getResponseParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC);
		successServiceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC, procesDesc);
	}
}
