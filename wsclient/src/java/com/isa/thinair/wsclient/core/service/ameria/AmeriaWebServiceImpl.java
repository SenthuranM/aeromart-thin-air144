package com.isa.thinair.wsclient.core.service.ameria;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentFieldsResponse;
import com.isa.thinair.wsclient.api.ameriabank.IAmeria;
import com.isa.thinair.wsclient.api.ameriabank.PaymentClientClass;
import com.isa.thinair.wsclient.api.ameriabank.PaymentFields;
import com.isa.thinair.wsclient.api.ameriabank.RespMessage;
import com.isa.thinair.wsclient.api.ameriabank.ResultPaymentClass;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.util.SOAPLoggingHandler;

public class AmeriaWebServiceImpl {

	private static Log log = LogFactory.getLog(AmeriaWebServiceImpl.class);

	com.isa.thinair.wsclient.core.config.AmeriaClientConfig ameriaClientConfig = WSClientModuleUtils.getModuleConfig()
			.getAmeriaClientConfig();

	/**
	 * Payment verification method for AmeriaBank Payment gateway
	 * 
	 * @throws ModuleException
	 */
	public AmeriaBankCommonResponse getPaymentId(
			com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentIdRequest ameriaPaymentIdRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[AmeriaBankWebServiceImpl::payment ID request Transaction()] Begin");

		AmeriaBankCommonResponse ameriaBankPaymentIdResponse = new AmeriaBankCommonResponse();

		try {

			IAmeria serviceStub = getClientStub();
			com.isa.thinair.wsclient.api.ameriabank.ObjectFactory objectFactory = new com.isa.thinair.wsclient.api.ameriabank.ObjectFactory();

			try {

				PaymentClientClass getPaymentIdRequest = new PaymentClientClass();
				buildAmeriaBankCommonRequest(ameriaPaymentIdRequest, getPaymentIdRequest);
				getPaymentIdRequest.setDescription(objectFactory.createPaymentClientClassDescription(ameriaPaymentIdRequest
						.getDescription()));
				getPaymentIdRequest
						.setBackURL(objectFactory.createPaymentClientClassBackURL(ameriaPaymentIdRequest.getBackURL()));
				getPaymentIdRequest.setOpaque(objectFactory.createPaymentClientClassOpaque(ameriaPaymentIdRequest.getOpaque()));
				getPaymentIdRequest.setCurrency(objectFactory.createPaymentClientClassCurrency(ameriaPaymentIdRequest
						.getCurrency()));

				ResultPaymentClass paymentIdResponse = serviceStub.getPaymentID(getPaymentIdRequest);

				ameriaBankPaymentIdResponse.setPaymentId(paymentIdResponse.getPaymentID().getValue());
				ameriaBankPaymentIdResponse.setRespCode(String.valueOf(paymentIdResponse.getRespcode().intValue()));
				ameriaBankPaymentIdResponse.setRespMessage(paymentIdResponse.getRespmessage().getValue());

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (ameriaClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[AmeriaBankWebServiceImpl::verifyTransaction()] End");
		return ameriaBankPaymentIdResponse;
	}

	public AmeriaBankPaymentFieldsResponse getPaymentFields(AmeriaBankCommonRequest ameriaBankPaymentFieldsRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {
		log.debug("[AmeriaBankWebServiceImpl::payment fields request transaction] Begin");

		AmeriaBankPaymentFieldsResponse ameriaBankPaymentFieldsResponse = new AmeriaBankPaymentFieldsResponse();

		try {

			IAmeria serviceStub = getClientStub();

			try {

				PaymentClientClass getPaymentFieldsRequest = new PaymentClientClass();
				buildAmeriaBankCommonRequest(ameriaBankPaymentFieldsRequest, getPaymentFieldsRequest);

				PaymentFields paymentFieldsResponse = serviceStub.getPaymentFields(getPaymentFieldsRequest);

				ameriaBankPaymentFieldsResponse.setPaymentAmount(paymentFieldsResponse.getAmount().getValue());
				ameriaBankPaymentFieldsResponse.setApprovedAmount(paymentFieldsResponse.getApproveamount());
				ameriaBankPaymentFieldsResponse.setAuthCode(paymentFieldsResponse.getAuthcode().getValue());
				ameriaBankPaymentFieldsResponse.setCardNumber(paymentFieldsResponse.getCardnumber().getValue());
				ameriaBankPaymentFieldsResponse.setClientName(paymentFieldsResponse.getClientname().getValue());
				ameriaBankPaymentFieldsResponse.setCurrency(paymentFieldsResponse.getCurrency().getValue());
				ameriaBankPaymentFieldsResponse.setDateTime(paymentFieldsResponse.getDatetime().getValue());
				ameriaBankPaymentFieldsResponse.setDepositAmount(paymentFieldsResponse.getDepositamount());
				ameriaBankPaymentFieldsResponse.setRespMessage(paymentFieldsResponse.getDescr().getValue());
				ameriaBankPaymentFieldsResponse.setMerchantId(paymentFieldsResponse.getMerchantid().getValue());
				ameriaBankPaymentFieldsResponse.setOpaque(paymentFieldsResponse.getOpaque().getValue());
				ameriaBankPaymentFieldsResponse.setOrderId(paymentFieldsResponse.getOrderid().getValue());
				ameriaBankPaymentFieldsResponse.setPaymentState(paymentFieldsResponse.getPaymentState().getValue());
				ameriaBankPaymentFieldsResponse.setPaymentType(paymentFieldsResponse.getPaymenttype());
				ameriaBankPaymentFieldsResponse.setRespCode(paymentFieldsResponse.getRespcode().getValue());
				ameriaBankPaymentFieldsResponse.setRrn(paymentFieldsResponse.getRrn().getValue());
				ameriaBankPaymentFieldsResponse.setStan(paymentFieldsResponse.getStan().getValue());
				ameriaBankPaymentFieldsResponse.setTerminalId(paymentFieldsResponse.getTerminalid().getValue());
				ameriaBankPaymentFieldsResponse.setTrxnDetails(paymentFieldsResponse.getTrxnDetails().getValue());
				ameriaBankPaymentFieldsResponse.setRefundAmount(paymentFieldsResponse.getRefundamount());
				ameriaBankPaymentFieldsResponse.setRewardPoint(paymentFieldsResponse.getRewardpoint());
				ameriaBankPaymentFieldsResponse.setOrderStatusCode(paymentFieldsResponse.getOrderstatuscode());
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (ameriaClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[AmeriaBankWebServiceImpl:: payment fields request transaction] End");
		return ameriaBankPaymentFieldsResponse;
	}

	public AmeriaBankCommonResponse refundAmeriaBankPayment(AmeriaBankCommonRequest refundAmeriaBankRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {
		log.debug("[AmeriaBankWebServiceImpl::payment fields request transaction] Begin");

		AmeriaBankCommonResponse refundAmeriaBankPaymentResponse = new AmeriaBankCommonResponse();

		try {

			IAmeria serviceStub = getClientStub();

			PaymentClientClass refundPaymentRequest = new PaymentClientClass();
			buildAmeriaBankCommonRequest(refundAmeriaBankRequest, refundPaymentRequest);

			RespMessage refundResponseMessage = serviceStub.refund(refundPaymentRequest);

			refundAmeriaBankPaymentResponse.setRespMessage(refundResponseMessage.getRespmessage().getValue());
			refundAmeriaBankPaymentResponse.setOpaque(refundResponseMessage.getOpaque().getValue());
			refundAmeriaBankPaymentResponse.setRespCode(refundResponseMessage.getRespcode().getValue());

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (ameriaClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[AmeriaBankWebServiceImpl:: payment fields request transaction] End");

		return refundAmeriaBankPaymentResponse;
	}

	public AmeriaBankCommonResponse reverseAmeriaBankPayment(AmeriaBankCommonRequest reverseAmeriaBankRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {
		log.debug("[AmeriaBankWebServiceImpl::payment fields request transaction] Begin");

		AmeriaBankCommonResponse reverseAmeriaBankPaymentResponse = new AmeriaBankCommonResponse();

		try {

			IAmeria serviceStub = getClientStub();

			PaymentClientClass reversePaymentRequest = new PaymentClientClass();
			buildAmeriaBankCommonRequest(reverseAmeriaBankRequest, reversePaymentRequest);

			RespMessage refundResponseMessage = serviceStub.reversePayment(reversePaymentRequest);

			reverseAmeriaBankPaymentResponse.setRespMessage(refundResponseMessage.getRespmessage().getValue());
			reverseAmeriaBankPaymentResponse.setOpaque(refundResponseMessage.getOpaque().getValue());
			reverseAmeriaBankPaymentResponse.setRespCode(refundResponseMessage.getRespcode().getValue());

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (ameriaClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[AmeriaBankWebServiceImpl:: payment fields request transaction] End");

		return reverseAmeriaBankPaymentResponse;
	}

	private void buildAmeriaBankCommonRequest(
			com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest ameriaPaymentCommonRequest,
			PaymentClientClass getPaymentRequest) {
		getPaymentRequest.setClientID(ameriaPaymentCommonRequest.getClientID());
		getPaymentRequest.setOrderID(ameriaPaymentCommonRequest.getOrderId());
		getPaymentRequest.setPassword(ameriaPaymentCommonRequest.getUserPassword());
		getPaymentRequest.setPaymentAmount(ameriaPaymentCommonRequest.getPaymentAmount());
		getPaymentRequest.setUsername(ameriaPaymentCommonRequest.getUserName());
	}

	private IAmeria getClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (ameriaClientConfig.isUseProxy()) {
				setProxy(ameriaClientConfig.getHttpProxy(), ameriaClientConfig.getHttpProxyPort());
			}
			if (ameriaClientConfig.isUseSSL()) {
				setSSL(ameriaClientConfig.getTrustStoreFile(), ameriaClientConfig.getKeyStoreFile());
			}

			URL wsdlURL = new URL(ameriaClientConfig.getWsdlUrl());
			QName qname = new QName("http://tempuri.org/", "PaymentService");
			Service service = Service.create(wsdlURL, qname);
			IAmeria port = service.getPort(IAmeria.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			return port;

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.create.stub.ameriaBankWebService.failed");
		}
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
		System.setProperty("http.proxyHost", host);
		System.setProperty("http.proxyPort", port);
	}

	private void setSSL(String trustStore, String keyStore) {
		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.trustStore", trustStore);
	}

}
