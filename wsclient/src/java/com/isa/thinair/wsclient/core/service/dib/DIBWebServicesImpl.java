package com.isa.thinair.wsclient.core.service.dib;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openuri.PNRTransactionsStatusCheckRQType;

import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.PNRTransactionsStatusCheckRQ;
import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.PNRTransactionsStatusCheckRS;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.CommonUtil;

public class DIBWebServicesImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_DIB;

	private static Log log = LogFactory.getLog(DIBWebServicesImpl.class);

	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	/**
	 * Returns the status at DIB for a given set of transactions.
	 * 
	 * @param pnrExtTransactionsTO
	 * @return
	 */
	public PNRExtTransactionsTO getTransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO) throws ModuleException {
		org.openuri.StatusChecker statusChkr = new org.openuri.StatusChecker();
		statusChkr.setPNRTransactionsStatusCheckRQ(preparePNRTransactionsStatusCheckRQ(pnrExtTransactionsTO));
		PNRTransactionsStatusCheckRQType wsPNRTnxs = (PNRTransactionsStatusCheckRQType) ExternalWebServiceInvoker.invokeService(
				SERVICE_PROVIDER, "statusChecker", true, new Param(statusChkr));
		PNRExtTransactionsTO aaPNRTnxs = null;
		if (wsPNRTnxs != null) {
			aaPNRTnxs = getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(wsPNRTnxs);
		}
		return aaPNRTnxs;
	}

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at DIB.
	 * 
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 */
	public void requestDailyTnxReconcilation(Date startTimestamp, Date endTimestamp) throws ModuleException {
		log.debug("BEGIN:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
		String agent = ExternalWebServiceInvoker.getServiceAgent(SERVICE_PROVIDER);
		org.openuri.CheckStatus chkStatus = new org.openuri.CheckStatus();
		chkStatus.setTransactionsReconRQ(prepareTnxsForRecon(startTimestamp, endTimestamp, agent));
		ExternalWebServiceInvoker.invokeService(getServiceProvider(null), "checkStatus", false, new Param(chkStatus));
		log.debug("END:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
	}

	/**
	 * Prepares {@link PNRExtTransactionsTO} from {@link PNRTransactionsStatusCheckRS}
	 * 
	 * @param wsPNRTnxs
	 * @return
	 */
	private static PNRExtTransactionsTO getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(
			PNRTransactionsStatusCheckRQType pnrTransactionsStatusCheckRS) throws ModuleException {
		PNRExtTransactionsTO aaPNRTnxs = null;

		if (pnrTransactionsStatusCheckRS != null && pnrTransactionsStatusCheckRS.getPNRPaymentTransactions().size() > 0) {
			aaPNRTnxs = new PNRExtTransactionsTO();
			PNRTransactionsStatusCheckRQType.PNRPaymentTransactions wsPNRTnxs = pnrTransactionsStatusCheckRS
					.getPNRPaymentTransactions().get(0);
			aaPNRTnxs.setPnr(wsPNRTnxs.getPNRNo());

			for (Iterator it = wsPNRTnxs.getPaymentTransaction().iterator(); it.hasNext();) {
				PNRTransactionsStatusCheckRQType.PNRPaymentTransactions.PaymentTransaction wsTnx = (PNRTransactionsStatusCheckRQType.PNRPaymentTransactions.PaymentTransaction) it
						.next();

				ExternalPaymentTnx aaTnx = new ExternalPaymentTnx();

				aaTnx.setPnr(wsPNRTnxs.getPNRNo());
				aaTnx.setBalanceQueryKey(wsTnx.getAARefNo());
				aaTnx.setAmount(AccelAeroCalculator.parseBigDecimal(Double.parseDouble(wsTnx.getAmount())));
				aaTnx.setChannel(wsTnx.getBankChannel());
				aaTnx.setExternalPayStatus(WSClientModuleUtils.getModuleConfig().getAAExtPayTnxStatus(wsTnx.getBankStatus()));
				aaTnx.setExternalPayId(wsTnx.getBankRefNo());
				aaTnx.setExternalTnxEndTimestamp(CommonUtil.getDate(wsTnx.getTimestamp()));

				aaPNRTnxs.addExtPayTransactions(aaTnx);
			}
		}
		return aaPNRTnxs;
	}

	/**
	 * Prepares {@link PNRTransactionsStatusCheckRQ} from {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static PNRTransactionsStatusCheckRQType preparePNRTransactionsStatusCheckRQ(PNRExtTransactionsTO aaPNRTnxs)
			throws ModuleException {
		PNRTransactionsStatusCheckRQType pnrTransactionsStatusCheckRQ = new PNRTransactionsStatusCheckRQType();

		PNRTransactionsStatusCheckRQType.PNRPaymentTransactions wsPNRTnxs = new PNRTransactionsStatusCheckRQType.PNRPaymentTransactions();
		pnrTransactionsStatusCheckRQ.getPNRPaymentTransactions().add(wsPNRTnxs);

		wsPNRTnxs.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			PNRTransactionsStatusCheckRQType.PNRPaymentTransactions.PaymentTransaction wsExtTnx = new PNRTransactionsStatusCheckRQType.PNRPaymentTransactions.PaymentTransaction();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			// wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setAmount(String.valueOf(aaExtTnx.getAmount().doubleValue()));
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()).toString());

			wsPNRTnxs.getPaymentTransaction().add(wsExtTnx);
		}
		return pnrTransactionsStatusCheckRQ;
	}

	/**
	 * Set external payment transactions done between given start and end timestamps.
	 * 
	 * @param reconRQ
	 * @param startTimestamp
	 * @param endTimestamp
	 * @throws ModuleException
	 */
	private static org.openuri.TransactionsReconRQType prepareTnxsForRecon(Date startTimestamp, Date endTimestamp, String agent)
			throws ModuleException {
		org.openuri.TransactionsReconRQType reconRQ = new org.openuri.TransactionsReconRQType();
		reconRQ.setStartTimestamp(CommonUtil.parse(startTimestamp).toString());
		reconRQ.setEndTimestamp(CommonUtil.parse(endTimestamp).toString());

		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setStartTimestamp(startTimestamp);
		criteriaDTO.setEndTimestamp(endTimestamp);
		criteriaDTO.setAgentCode(agent);
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);

		Map pnrTnxsMap = WSClientModuleUtils.getResQueryBD().getExtPayTransactions(criteriaDTO);

		if (pnrTnxsMap != null) {
			for (Iterator pnrsIt = pnrTnxsMap.keySet().iterator(); pnrsIt.hasNext();) {
				PNRExtTransactionsTO pnrTnxs = (PNRExtTransactionsTO) pnrTnxsMap.get(pnrsIt.next());
				if (pnrTnxs != null && pnrTnxs.getExtPayTransactions() != null) {
					reconRQ.getPNRPaymentTransactions().add(getWSPNRExtTnxsForTnxReconRQ(pnrTnxs));
				}
			}
		}
		return reconRQ;
	}

	/**
	 * Prepares {@link org.openuri.TransactionsReconRQType.PNRPaymentTransactions} from {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static org.openuri.TransactionsReconRQType.PNRPaymentTransactions getWSPNRExtTnxsForTnxReconRQ(
			PNRExtTransactionsTO aaPNRTnxs) throws ModuleException {
		log.debug("BEGIN getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		org.openuri.TransactionsReconRQType.PNRPaymentTransactions wsPNRTnxs = new org.openuri.TransactionsReconRQType.PNRPaymentTransactions();
		wsPNRTnxs.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			if (log.isDebugEnabled())
				log.debug(aaExtTnx.getSummary().toString());

			org.openuri.TransactionsReconRQType.PNRPaymentTransactions.PaymentTransaction wsExtTnx = new org.openuri.TransactionsReconRQType.PNRPaymentTransactions.PaymentTransaction();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			// wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setAmount(aaExtTnx.getAmount().toString());
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()).toString());

			wsPNRTnxs.getPaymentTransaction().add(wsExtTnx);
		}
		log.debug("END getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		return wsPNRTnxs;
	}

}
