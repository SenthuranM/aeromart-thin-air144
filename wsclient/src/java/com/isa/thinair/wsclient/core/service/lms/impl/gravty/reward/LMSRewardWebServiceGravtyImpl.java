package com.isa.thinair.wsclient.core.service.lms.impl.gravty.reward;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.dto.lms.RewardData;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.GravtyConfig;
import com.isa.thinair.wsclient.core.service.lms.base.LMSServiceFactory;
import com.isa.thinair.wsclient.core.service.lms.base.reward.LMSRewardWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.CancelRedemptionRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.CancelRedemptionResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsDetailsRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsDetailsResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyRestServiceClient;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.LMSGravtyWebServiceInvoker;

public class LMSRewardWebServiceGravtyImpl implements LMSRewardWebService {
	
	public static final GravtyConfig gravtyConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig().getGravtyConfig();
	
	private final static String ACCRUAL = "ACCRUAL";
	private final static String PAYMENT_WITH_POINTS = "PAYMENT_WITH_POINTS";
	private final static String REVERSAL ="REVERSAL";
	private final static String CANCELLATION ="CANCELLATION";
	
	private static Log log = LogFactory.getLog(LMSRewardWebServiceGravtyImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public IssueRewardResponse issueVariableRewards(IssueRewardRequest request, String pnr, Map<String, Double> productPoints) throws ModuleException {
	
		log.info("=================Starting rewards points issue service====");
				
		GravtyRestServiceClient<RedeemPointsResponse, RedeemPointsRequest> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.REDEEM_POINTS);
		
		RedeemPointsRequest requestToGravty = new RedeemPointsRequest();

		String memberExternalId;

		if (StringUtils.isEmpty(request.getMemberExternalId())) {
			MemberFetchDataResponse memberDetails = LMSServiceFactory.getLMSMemberWebService().getMemberFromEmail(
					request.getMemberAccountId());
			memberExternalId = memberDetails.getMember_id();
		} else {
			memberExternalId = request.getMemberExternalId();
		}

		
		
		Integer property = (Integer) gravtyConfig.getProgramId();
		
		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		int sponserId = externalConfig.getSponserId();
		
		int totalPoints = 0;
		for (Entry<String, Double> e: productPoints.entrySet()) {
			totalPoints += e.getValue();
		}
		
		List<RewardData> requestRewardDataList = request.getRewards();
		
		List<RedeemPointsDetailsRequest> redeemLines = new ArrayList<RedeemPointsDetailsRequest>();
		
		for (RewardData rewardData : requestRewardDataList) {
			RedeemPointsDetailsRequest redeemPointDetailRequest = new RedeemPointsDetailsRequest();
			redeemPointDetailRequest.setL_product_external_id(rewardData.getRewardTypeExtReference());
			redeemPointDetailRequest.setL_product_amount(rewardData.getRewardAmount().doubleValue());
			redeemPointDetailRequest.setL_pay_in_points(productPoints.get(rewardData.getRewardTypeExtReference()));
			redeemLines.add(redeemPointDetailRequest);
		}
				
		requestToGravty.setH_bit_category(ACCRUAL);
		requestToGravty.setH_bit_type(PAYMENT_WITH_POINTS);
		requestToGravty.setH_program_id(property);
		requestToGravty.setH_sponsor_id(sponserId);
		requestToGravty.setH_bit_date(OffsetDateTime.now().toString());
		requestToGravty.setH_bit_source_generated_id(pnr);
		requestToGravty.setH_member_id(memberExternalId);
		requestToGravty.setPay_in_points(totalPoints);
		requestToGravty.setLines(redeemLines);

		RedeemPointsResponse redeemResponseData = serviceClent.post(requestToGravty);
		IssueRewardResponse issueRewardResponse = new IssueRewardResponse();
		
		if (redeemResponseData != null) {
			List<RedeemPointsDetailsResponse> responseRedeemLines = redeemResponseData.getOriginal_bit().getLines();
			ArrayList<String> rewardId = new ArrayList<String>();
 			Map<String, Double> loyaltyPoint = new HashMap<String, Double>();
					
			for (RedeemPointsDetailsResponse red : responseRedeemLines) {
				rewardId.add(red.getL_product_external_id());
				loyaltyPoint.put(red.getL_product_external_id(), red.getL_product_amount());
			}
			
			String[] rewardIdArray = new String[1];
			rewardIdArray[0] = redeemResponseData.getBit_id();
			
			issueRewardResponse.setIssuedRewardIds(rewardIdArray);
			issueRewardResponse.setProductLoyaltyPointCost(loyaltyPoint);
		}
		
		log.info("=================Redeem points issued successfully====");
		return issueRewardResponse;
	}

	@Override
	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean cancelMemberRewards(String pnr,String[] memberRewardIds, String memberAccountId) throws ModuleException {
		
		log.info("=================Reverting redemption points====");
		
		//LMSServiceFactory.getLMSMemberWebService().generateNewSystemToken();
		
		GravtyRestServiceClient<CancelRedemptionResponse, CancelRedemptionRequest> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.CANCEL_REDEMPTION);
		
		CancelRedemptionRequest request = new CancelRedemptionRequest();
		
		
		Integer property = (Integer) gravtyConfig.getProgramId();
		
		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		
		int sponserId = externalConfig.getSponserId();
		String rewardId = null;
		
		for (String id : memberRewardIds) {
			rewardId = id; 
		}

		String memberExternalId;
		LmsMemberServiceBD lmsMemberServiceBD = (LmsMemberServiceBD) WSClientModuleUtils.getLmsMemberServiceBD();
		LmsMember lmsMember = lmsMemberServiceBD.getLmsMember(memberAccountId);

		if (memberAccountId != null && lmsMember != null && lmsMember.getMemberExternalId() != null) {
			memberExternalId = lmsMember.getMemberExternalId();
		} else {
			MemberFetchDataResponse member = LMSServiceFactory.getLMSMemberWebService().getMemberFromEmail(
					memberAccountId);
			memberExternalId = member.getMember_id();
		}

		request.setH_bit_category(CANCELLATION);
		request.setH_bit_type(REVERSAL);
		request.setH_program_id(property);
		request.setH_sponsor_id(sponserId);
		request.setH_bit_date(OffsetDateTime.now().toString());
		request.setH_member_id(memberExternalId);
		request.setCancel_bit_id(rewardId);
		request.setH_bit_source_generated_id(pnr);
		CancelRedemptionResponse sysreturns = serviceClent.post(request);
		
		log.info("=================Redeem points cancellation successfull====");
		
		return true;
	}

}
