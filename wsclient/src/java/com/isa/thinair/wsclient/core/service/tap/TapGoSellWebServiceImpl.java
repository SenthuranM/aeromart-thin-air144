package com.isa.thinair.wsclient.core.service.tap;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRs;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.tap.CaptureRequestDC;
import com.isa.thinair.wsclient.api.tap.IPayGatewayService;
import com.isa.thinair.wsclient.core.config.TapGoSellConfig;
import com.isa.thinair.wsclient.core.util.SOAPLoggingHandler;

public class TapGoSellWebServiceImpl {

	private static Log log = LogFactory.getLog(TapGoSellWebServiceImpl.class);

	com.isa.thinair.wsclient.core.config.TapGoSellConfig tapGoSellConfig = null;
	private static TapGoSellAdaptor adaptor = null;

	private TapGoSellAdaptor getAdaptor() {
		if (adaptor == null) {
			adaptor = new TapGoSellAdaptor();
		}
		return adaptor;
	}

	private TapGoSellConfig getTapConfig() {
		if (tapGoSellConfig == null) {
			tapGoSellConfig = WSClientModuleUtils.getModuleConfig().getTapGoSellConfig();
		}
		return tapGoSellConfig;
	}

	/**
	 * Payment verification method for TapGoSell Payment gateway
	 * 
	 * @param paymentRequest
	 * @return
	 * @throws ModuleException
	 */
	public TapPaymentRequestCallRs paymentRequest(TapPaymentRequestCallRq paymentRequest) throws ModuleException {

		log.debug("[TapGoSellWebServiceImpl::paymentRequest()] Begin");
		TapPaymentRequestCallRs response = new TapPaymentRequestCallRs();
		try {

			IPayGatewayService serviceStub = getClientStub();

			try {
				response = getAdaptor().transformeToTapPaymentRequestCallRs(
						serviceStub.paymentRequest(getAdaptor().transformeToPayRequestDC(paymentRequest)));
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (getTapConfig().isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[TapGoSellWebServiceImpl::paymentRequest()] End");
		return response;
	}

	public TapCaptureRequestCallRs capturePayment(TapCaptureRequestCallRq captureRequest) throws ModuleException {

		log.debug("[TapGoSellWebServiceImpl::paymentRequest()] Begin");
		TapCaptureRequestCallRs response = new TapCaptureRequestCallRs();
		try {

			IPayGatewayService serviceStub = getClientStub();

			try {
				response = (TapCaptureRequestCallRs) getAdaptor().transformeToTapGetOrderStatusResponseCallRs(serviceStub
						.captureAmount((CaptureRequestDC) getAdaptor().transformeToGetOrderStatusRequestDC(captureRequest)));
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (getTapConfig().isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[TapGoSellWebServiceImpl::capturePayment()] End");
		return response;
	}

	public TapGetOrderStatusRequestCallRs getOrderStatus(TapGetOrderStatusRequestCallRq getOrderRequest) throws ModuleException {

		log.debug("[TapGoSellWebServiceImpl::paymentRequest()] Begin");
		TapGetOrderStatusRequestCallRs response = new TapGetOrderStatusRequestCallRs();
		try {

			IPayGatewayService serviceStub = getClientStub();

			try {
				response = getAdaptor().transformeToTapGetOrderStatusResponseCallRs(
						serviceStub.getOrderStatusRequest(getAdaptor().transformeToGetOrderStatusRequestDC(getOrderRequest)));
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (getTapConfig().isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[TapGoSellWebServiceImpl::getOrderStatus()] End");
		return response;
	}

	public TapRefundCallRs refund(TapRefundCallRq refundRequest) throws ModuleException {

		log.debug("[TapGoSellWebServiceImpl::refund()] Begin");
		TapRefundCallRs response = new TapRefundCallRs();
		try {

			IPayGatewayService serviceStub = getClientStub();

			try {
				response = getAdaptor()
						.transformeToTapRefundCallRs(serviceStub.refund(getAdaptor().transformeToRefundRequestDC(refundRequest)));
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (getTapConfig().isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[TapGoSellWebServiceImpl::getOrderStatus()] End");
		return response;
	}

	public TapRefundStatusCallRs getRefundStatus(TapRefundStatusCallRq refundStatusRequest) throws ModuleException {

		log.debug("[TapGoSellWebServiceImpl::getRefundStatus()] Begin");
		TapRefundStatusCallRs response = new TapRefundStatusCallRs();
		try {

			IPayGatewayService serviceStub = getClientStub();

			try {
				response = getAdaptor().transformeToTapRefundStatusCallRs(
						serviceStub.getRefundStatus(getAdaptor().transformeToGetRefundStatusRequestDC(refundStatusRequest)));
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (getTapConfig().isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[TapGoSellWebServiceImpl::getOrderStatus()] End");
		return response;
	}

	private IPayGatewayService getClientStub() throws ModuleException {
		try {

			if (getTapConfig().isUseProxy()) {
				setProxy(getTapConfig().getHttpProxy(), getTapConfig().getHttpProxyPort());
			}
			if (getTapConfig().isUseSSL()) {
				setSSL(getTapConfig().getTrustStoreFile(), getTapConfig().getKeyStoreFile());
			}

			URL wsdlURL = new URL(getTapConfig().getWsdlUrl());
			QName qname = new QName("http://tempuri.org/", "PayGatewayServiceCall");
			Service service = Service.create(wsdlURL, qname);
			IPayGatewayService port = service.getPort(IPayGatewayService.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			return port;

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.create.stub.TapGoSellWebService.failed");
		}
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
		System.setProperty("http.proxyHost", host);
		System.setProperty("http.proxyPort", port);
	}

	private void setSSL(String trustStore, String keyStore) {
		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.trustStore", trustStore);
	}

}
