package com.isa.thinair.wsclient.core.service.lms.impl.gravty;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.http.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;

public class GravtyRestServiceClient<RESPONSE, REQUEST> {

	public static final String CONTENT_TYPE = "Content-type";
	public static final String COOKIE = "Cookie";

	private static final String PROXY_HOST = CommonsServices.getGlobalConfig().getHttpProxy();
	private static final int PROXY_PORT = CommonsServices.getGlobalConfig().getHttpPort();
	private static final boolean IS_USE_PROXY = LMSGravtyWebServiceInvoker.gravtyConfig.isApplyProxy();

	private static final String X_API_KEY = LMSGravtyWebServiceInvoker.gravtyConfig.getxApiKey();

	private static Log log = LogFactory.getLog(GravtyRestServiceClient.class);

	private Class<RESPONSE> responseClass;
	private String url;
	
	private boolean authTokenRequired= true;

	private static Gson gson = new Gson();

	public GravtyRestServiceClient(Class<RESPONSE> responseClass, String url, boolean authTokenRequired) {
		this.url = url;
		this.responseClass = responseClass;
		this.authTokenRequired = authTokenRequired;
	}

	public RESPONSE post(REQUEST request) throws ModuleException {

		HttpClient httpClient = getHttpClient();

		try {

			HttpPost post = getHttpPostRequest(request);
			HttpResponse response = httpClient.execute(post);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK
					&& response.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED) {
				handleErrorResponse(response);
			}

			return (RESPONSE) gson.fromJson(EntityUtils.toString(response.getEntity()), responseClass);

		} catch (IOException ce) {
			log.error("================================================================");
			log.error("GRAVTY INVOCATION ERROR  >>  ",ce);
			log.error("================================================================");
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	public RESPONSE put(REQUEST request) throws ModuleException {

		HttpClient httpClient = getHttpClient();

		try {

			HttpPut post = getHttpPutRequest(request);
			HttpResponse response = httpClient.execute(post);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK
					&& response.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED) {
				handleErrorResponse(response);
			}

			return (RESPONSE) gson.fromJson(EntityUtils.toString(response.getEntity()), responseClass);

		} catch (IOException ce) {

			log.error("================================================================");
			log.error("GRAVTY INVOCATION ERROR  >>  ",ce);
			log.error("================================================================");
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	private HttpClient getHttpClient() {
		HttpClient httpClient;
		if (IS_USE_PROXY) {
			HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
			httpClient = HttpClientBuilder.create().setProxy(proxy).build();
		} else {
			httpClient = HttpClientBuilder.create().build();
		}
		//log.info(url);
		return httpClient;
	}

	private HttpPost getHttpPostRequest(REQUEST request) throws UnsupportedEncodingException, ModuleException {

		String requestJson = gson.toJson(request);
		StringEntity postData = new StringEntity(requestJson);
		log.info("POST :: URL/Request  >> " + url +" / " + requestJson);

		HttpPost post = new HttpPost(url);
		post.setEntity(postData);
		post.setHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		post.setHeader("x-api-key", X_API_KEY);
		if(authTokenRequired){
			post.setHeader("Authorization", "JWT " + getAuthToken());
		}
		return post;
	}
	
	private HttpPut getHttpPutRequest(REQUEST request) throws UnsupportedEncodingException, ModuleException {

		String requestJson = gson.toJson(request);
		StringEntity postData = new StringEntity(requestJson);
		log.info("PUT :: URL/Request  >> " + url +" / " + requestJson);

		HttpPut post = new HttpPut(url);
		post.setEntity(postData);
		post.setHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		post.setHeader("x-api-key", X_API_KEY);
		if(authTokenRequired){
			post.setHeader("Authorization", "JWT " + getAuthToken());
		}
		return post;
	}


	private void handleErrorResponse(HttpResponse response) throws ModuleException {

		try {
			String errorJson = EntityUtils.toString(response.getEntity());
			log.error("SERVICE URL :: "+ url +"  /  Gravty Service Invocation Error >> " + errorJson);
			throw new ModuleException("reward.service.client");

		} catch (JsonSyntaxException | ParseException | IOException e) {
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	public RESPONSE get() throws ModuleException {

		HttpClient httpClient = getHttpClient();

		try {
			log.info("GET :: URL Request  >> " + url);
			HttpGet get = getHttpGetRequest();
			HttpResponse response = httpClient.execute(get);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				handleErrorResponse(response);
			}

			return (RESPONSE) gson.fromJson(EntityUtils.toString(response.getEntity()), responseClass);

		} catch (IOException ce) {

			log.error("================================================================");
			log.error("GRAVTY INVOCATION ERROR  >>  ",ce);
			log.error("================================================================");
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	public HttpResponse getHttpResponse() throws ModuleException {

		HttpClient httpClient = getHttpClient();

		try {

			log.info("GET :: URL Request  >> " + url);
			HttpGet get = getHttpGetRequest();
			HttpResponse response = httpClient.execute(get);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				handleErrorResponse(response);
			}
			return response;

		} catch (IOException ce) {

			log.error("================================================================");
			log.error("GRAVTY INVOCATION ERROR  >>  ",ce);
			log.error("================================================================");
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	private HttpGet getHttpGetRequest() throws UnsupportedEncodingException, ModuleException {
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		httpget.setHeader("x-api-key", X_API_KEY);
		if(authTokenRequired){
			httpget.setHeader("Authorization", "JWT " + getAuthToken());
		}
		return httpget;

	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static String getAuthToken() throws ModuleException {
		return GravtyTokenHandler.getAuthToken();
	}

}
