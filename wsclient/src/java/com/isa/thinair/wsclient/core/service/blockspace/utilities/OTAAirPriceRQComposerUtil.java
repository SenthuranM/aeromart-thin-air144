package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.math.BigInteger;
import java.util.Date;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebservicesConstants;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isa.thinair.wsclient.api.dto.FlightSearchTransferDTO;

/**
 * Compose the OTAAirPriceRQ object
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class OTAAirPriceRQComposerUtil {

	public static OTAAirPriceRQ compose(FlightSearchTransferDTO flightSearchTransferDTO, OTAAirAvailRS oTAAirAvailRS,
			BaseDTO baseDTO) throws ModuleException {

		OTAAirPriceRQ oTAAirPriceRQ = null;

		for (OriginDestinationInformation originDestinationInformationRS : oTAAirAvailRS.getOriginDestinationInformation()) {
			if (originDestinationInformationRS.getOriginLocation().getLocationCode()
					.equals(flightSearchTransferDTO.getFromAirport())
					&& originDestinationInformationRS.getDestinationLocation().getLocationCode()
							.equals(flightSearchTransferDTO.getToAirport())
					&& (CommonUtil.parseDate(originDestinationInformationRS.getDepartureDateTime().getValue()).getTime()
							.compareTo(flightSearchTransferDTO.getDepatureDate()) == 0)
					&& (CommonUtil.parseDate(originDestinationInformationRS.getArrivalDateTime().getValue()).getTime()
							.compareTo(flightSearchTransferDTO.getReturnDate()) == 0)
					&& (originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption().size() > 0)
					&& (originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption().get(0)
							.getFlightSegment().size() > 0)
					&& (originDestinationInformationRS.getOriginDestinationOptions().getOriginDestinationOption().get(0)
							.getFlightSegment().get(0).getFlightNumber().equals(flightSearchTransferDTO.getFlightNumber()))) {

				oTAAirPriceRQ = new OTAAirPriceRQ();
				oTAAirPriceRQ.setPOS(new POSType());
				oTAAirPriceRQ.getPOS().getSource().add(DataComposerUtil.preparePOS(baseDTO));

				oTAAirPriceRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
				oTAAirPriceRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
				oTAAirPriceRQ.setSequenceNmbr(new BigInteger("1"));
				oTAAirPriceRQ.setEchoToken(UID.generate());
				oTAAirPriceRQ.setTimeStamp(CommonUtil.parse(new Date()));
				oTAAirPriceRQ.setTransactionIdentifier(oTAAirAvailRS.getTransactionIdentifier());

				AirItineraryType airItineraryType = new AirItineraryType();
				oTAAirPriceRQ.setAirItinerary(airItineraryType);
				airItineraryType.setDirectionInd(AirTripType.ONE_WAY);// FIXME

				AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
				airItineraryType.setOriginDestinationOptions(originDestinationOptions);

				for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS
						.getOriginDestinationOptions().getOriginDestinationOption()) {

					OriginDestinationOptionType originDestinationOptionType = new OriginDestinationOptionType();
					airItineraryType.getOriginDestinationOptions().getOriginDestinationOption().add(originDestinationOptionType);

					for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption
							.getFlightSegment()) {
						BookFlightSegmentType bookFlightSegmentType = new BookFlightSegmentType();
						originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
						bookFlightSegmentType.setDepartureAirport(flightSegment.getDepartureAirport());
						bookFlightSegmentType.setArrivalAirport(flightSegment.getArrivalAirport());
						bookFlightSegmentType.setDepartureDateTime(flightSegment.getDepartureDateTime());
						bookFlightSegmentType.setArrivalDateTime(flightSegment.getArrivalDateTime());
						bookFlightSegmentType.setFlightNumber(flightSegment.getFlightNumber());
					}
				}

				TravelerInfoSummaryType travellerInfoSummary1 = new TravelerInfoSummaryType();
				oTAAirPriceRQ.setTravelerInfoSummary(travellerInfoSummary1);

				TravelerInformationType travelerInfo1 = new TravelerInformationType();
				travellerInfoSummary1.getAirTravelerAvail().add(travelerInfo1);

				PassengerTypeQuantityType paxAdultQuantity = new PassengerTypeQuantityType();
				paxAdultQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
				paxAdultQuantity.setQuantity(flightSearchTransferDTO.getAdultCount());
				travelerInfo1.getPassengerTypeQuantity().add(paxAdultQuantity);

				PassengerTypeQuantityType paxChildQuantity = new PassengerTypeQuantityType();
				paxChildQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
				paxChildQuantity.setQuantity(flightSearchTransferDTO.getChildCount());
				travelerInfo1.getPassengerTypeQuantity().add(paxChildQuantity);

				PassengerTypeQuantityType paxInfantQuantity = new PassengerTypeQuantityType();
				paxInfantQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
				paxInfantQuantity.setQuantity(flightSearchTransferDTO.getInfantCount());
				travelerInfo1.getPassengerTypeQuantity().add(paxInfantQuantity);
			}
		}

		return oTAAirPriceRQ;
	}
}