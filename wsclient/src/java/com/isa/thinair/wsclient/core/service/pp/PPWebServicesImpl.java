package com.isa.thinair.wsclient.core.service.pp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.PAYPALRequestDTO;
import com.isa.thinair.paymentbroker.core.bl.paypal.PAYPALPaymentUtils;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.BasicAmountType;
import com.paypal.soap.api.CreditCardDetailsType;
import com.paypal.soap.api.CreditCardTypeType;
import com.paypal.soap.api.CurrencyCodeType;
import com.paypal.soap.api.DoDirectPaymentRequestDetailsType;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestDetailsType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestType;
import com.paypal.soap.api.DoExpressCheckoutPaymentResponseType;
import com.paypal.soap.api.GetExpressCheckoutDetailsRequestType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseDetailsType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.PayerInfoType;
import com.paypal.soap.api.PaymentDetailsType;
import com.paypal.soap.api.RefundTransactionRequestType;
import com.paypal.soap.api.RefundTransactionResponseType;
import com.paypal.soap.api.RefundType;
import com.paypal.soap.api.SetExpressCheckoutRequestDetailsType;
import com.paypal.soap.api.SetExpressCheckoutRequestType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

public class PPWebServicesImpl {

	private Log log = LogFactory.getLog(PPWebServicesImpl.class);

	public static final String VERSION = "63.0";

	public static final String API_SIGNATURE = "0";

	public static final String API_CERTIFICATE = "1";

	private static final String TRUE = "true";

	private APIProfile createProfile(PAYPALRequestDTO paypalRequestDTO) throws PayPalException {
		// construct and set the profile, these are the credentials we establish
		// as "the shop" with Paypal
		if (paypalRequestDTO.getProfileDTO().getUseProxy().equalsIgnoreCase(TRUE)) {
			setProxy(paypalRequestDTO.getProfileDTO().getProxyHost(), paypalRequestDTO.getProfileDTO().getProxyPort());
		}
	    APIProfile profile = ProfileFactory.createSignatureAPIProfile();

		profile.setAPIUsername(paypalRequestDTO.getProfileDTO().getApiUsername());
		profile.setAPIPassword(paypalRequestDTO.getProfileDTO().getApiPassword());
		profile.setEnvironment(paypalRequestDTO.getProfileDTO().getEnvironment());

		if (paypalRequestDTO.getProfileDTO().getMode().equals(API_SIGNATURE)) {
			profile.setSignature(paypalRequestDTO.getProfileDTO().getSignature());
		} else if (paypalRequestDTO.getProfileDTO().getMode().equals(API_CERTIFICATE)) {
			profile.setCertificateFile(paypalRequestDTO.getProfileDTO().getCertificateFilePath());
			profile.setPrivateKeyPassword(paypalRequestDTO.getProfileDTO().getPrivateKeyPassword());
		} else {
			throw new IllegalArgumentException("Invalid Mode Type");
		}
		
		return profile;
	}

	private SetExpressCheckoutResponseType setExpressCheckout(PAYPALRequestDTO paypalRequestDTO, APIProfile profile,
			SetExpressCheckoutRequestType pprequest) throws PayPalException {
		CallerServices caller = new CallerServices();
		caller.setAPIProfile(profile);
		// call the actual webservice, passing the constructed request
		SetExpressCheckoutResponseType ppresponse = (SetExpressCheckoutResponseType) caller.call("SetExpressCheckout", pprequest);
		return ppresponse;
	}

	public SetExpressCheckoutRequestType prepareSetExpressCheckoutRequestType(PAYPALRequestDTO paypalRequestDTO) {

		// construct the request
		SetExpressCheckoutRequestType pprequest = new SetExpressCheckoutRequestType();
		pprequest.setVersion(VERSION);

		// construct the details for the request
		SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();

		PaymentDetailsType paymentDetails = new PaymentDetailsType();
		paymentDetails.setOrderDescription(paypalRequestDTO.getProfileDTO().getOrderDescription());
		paymentDetails.setInvoiceID("INVOICE-" + paypalRequestDTO.getUserInputDTO().getInvoiceId());
		BasicAmountType orderTotal = new BasicAmountType(paypalRequestDTO.getUserInputDTO().getAmount());
		orderTotal.setCurrencyID(paypalRequestDTO.getUserInputDTO().getCurrencyCodeType());

		paymentDetails.setOrderTotal(orderTotal);
		paymentDetails.setPaymentAction(paypalRequestDTO.getUserInputDTO().getPaymentAction());
		details.setPaymentDetails(new PaymentDetailsType[] { paymentDetails });
		details.setNoShipping(paypalRequestDTO.getUserInputDTO().getNoShipping());
		details.setReqConfirmShipping(paypalRequestDTO.getUserInputDTO().getTypeOfGoods());
		details.setReturnURL(paypalRequestDTO.getUserInputDTO().getReturnUrl());
		details.setCancelURL(paypalRequestDTO.getProfileDTO().getCancelUrl());
		details.setCustom(paypalRequestDTO.getUserInputDTO().getUserId());

		// set the details on the request
		pprequest.setSetExpressCheckoutRequestDetails(details);
		return pprequest;
	}

	private GetExpressCheckoutDetailsResponseType getExpressCheckoutDetails(PAYPALRequestDTO paypalRequestDTO, APIProfile profile,
			GetExpressCheckoutDetailsRequestType pprequest) throws PayPalException {
		CallerServices caller = new CallerServices();
		caller.setAPIProfile(profile);
		GetExpressCheckoutDetailsResponseType ppresponse = (GetExpressCheckoutDetailsResponseType) caller.call(
				"GetExpressCheckoutDetails", pprequest);
		log.debug("getExpressCheckoutDetails web service call");
		return ppresponse;
	}

	public GetExpressCheckoutDetailsRequestType prepareGetExpressCheckoutDetailsRequestType(PAYPALRequestDTO paypalRequestDTO) {
		GetExpressCheckoutDetailsRequestType pprequest = new GetExpressCheckoutDetailsRequestType();
		pprequest.setVersion(VERSION);
		pprequest.setToken(paypalRequestDTO.getUserInputDTO().getToken());
		log.debug("Prepare GetExpressCheckoutDetailsRequestType");
		return pprequest;
	}

	private DoExpressCheckoutPaymentResponseType doExpressCheckoutService(APIProfile profile, PAYPALRequestDTO paypalRequestDTO,
			DoExpressCheckoutPaymentRequestType pprequest) throws PayPalException {
		CallerServices caller = new CallerServices();
		caller.setAPIProfile(profile);
		DoExpressCheckoutPaymentResponseType ppresponse = new DoExpressCheckoutPaymentResponseType();
		ppresponse = (DoExpressCheckoutPaymentResponseType) caller.call("DoExpressCheckoutPayment", pprequest);
		log.debug("doExpressCheckoutService web service call");
		return ppresponse;
	}

	public DoExpressCheckoutPaymentRequestType prepareDoExpressCheckoutPaymentRequestType(
			GetExpressCheckoutDetailsResponseDetailsType response) {
		DoExpressCheckoutPaymentRequestType pprequest = new DoExpressCheckoutPaymentRequestType();
		pprequest.setVersion(VERSION);

		DoExpressCheckoutPaymentRequestDetailsType paymentDetailsRequestType = new DoExpressCheckoutPaymentRequestDetailsType();
		paymentDetailsRequestType.setToken(response.getToken());

		PayerInfoType payerInfo = response.getPayerInfo();
		paymentDetailsRequestType.setPayerID(payerInfo.getPayerID());

		PaymentDetailsType paymentDetails = response.getPaymentDetails(0);
		paymentDetailsRequestType.setPaymentAction(paymentDetails.getPaymentAction());

		paymentDetailsRequestType.setPaymentDetails(response.getPaymentDetails());
		pprequest.setDoExpressCheckoutPaymentRequestDetails(paymentDetailsRequestType);
		log.debug("Prepare DoExpressCheckoutPaymentRequestType");
		return pprequest;
	}

	public RefundTransactionResponseType refund(PAYPALRequestDTO paypalRequestDTO, RefundTransactionRequestType pprequest) {
		RefundTransactionResponseType ppresponse = null;
		try {
			CallerServices caller = new CallerServices();
			// Set up your API credentials, PayPal end point, and API version.
			APIProfile profile = createProfile(paypalRequestDTO);
			caller.setAPIProfile(profile);
			// Execute the API operation and obtain the response.
			ppresponse = (RefundTransactionResponseType) caller.call("RefundTransaction", pprequest);

		} catch (Exception ex) {
			log.error(ex);
		}
		
		return ppresponse;
	}

	public RefundTransactionRequestType prepareRefund(PAYPALRequestDTO paypalRequestDTO) {
		RefundTransactionRequestType pprequest = new RefundTransactionRequestType();
		if ((paypalRequestDTO.getUserInputDTO().getAmount() != null && paypalRequestDTO.getUserInputDTO().getAmount().length() > 0)
				&& (paypalRequestDTO.getUserInputDTO().getRefundType().equals(PAYPALPaymentUtils.PARTIAL_REFUND))) {

			BasicAmountType amtType = new BasicAmountType();
			amtType.set_value(paypalRequestDTO.getUserInputDTO().getAmount());
			amtType.setCurrencyID(paypalRequestDTO.getUserInputDTO().getCurrencyCodeType());
			pprequest.setAmount(amtType);
		}
		pprequest.setVersion(VERSION);

		// Add request-specific fields to the request.
		pprequest.setTransactionID(paypalRequestDTO.getUserInputDTO().getTransactionId());
		pprequest.setMemo(paypalRequestDTO.getProfileDTO().getNote());
		pprequest.setRefundType(RefundType.fromString(paypalRequestDTO.getUserInputDTO().getRefundType()));

		return pprequest;
	}

	private DoDirectPaymentResponseType doDirectPaymentCode(APIProfile profile, DoDirectPaymentRequestType pprequest)
			throws PayPalException {
		DoDirectPaymentResponseType ppresponse = null;
		try {
			CallerServices caller = new CallerServices();
			// Set up your API credentials, PayPal end point, and API version.
			caller.setAPIProfile(profile);

			// Execute the API operation and obtain the response.
			ppresponse = (DoDirectPaymentResponseType) caller.call("DoDirectPayment", pprequest);
		} catch (Exception ex) {
			log.error(ex);
		}
		
		return ppresponse;
	}

	public DoDirectPaymentRequestType prepareDoDirectPaymentRequestType(PAYPALRequestDTO paypalRequestDTO) {
		DoDirectPaymentRequestType pprequest = new DoDirectPaymentRequestType();
		pprequest.setVersion(VERSION);

		// Add request-specific fields to the request.
		DoDirectPaymentRequestDetailsType details = new DoDirectPaymentRequestDetailsType();
		PaymentDetailsType paymentDetails = new PaymentDetailsType();
		BasicAmountType amount = new BasicAmountType();
		amount.set_value(paypalRequestDTO.getUserInputDTO().getAmount());
		amount.setCurrencyID(CurrencyCodeType.USD);
		paymentDetails.setOrderTotal(amount);

		details.setPaymentDetails(paymentDetails);

		CreditCardDetailsType cardDetails = new CreditCardDetailsType();
		
		//Fix for Master to MasterCard in the PayPal Payment gateway
		if("Master".equals(paypalRequestDTO.getUserInputDTO().getCreditCardType())){
			paypalRequestDTO.getUserInputDTO().setCreditCardType("MasterCard");
		}
		
		cardDetails.setCreditCardType(CreditCardTypeType.fromString(paypalRequestDTO.getUserInputDTO().getCreditCardType()));
		cardDetails.setCreditCardNumber(paypalRequestDTO.getUserInputDTO().getCreditCardNumber());
		cardDetails.setExpMonth(new Integer(paypalRequestDTO.getUserInputDTO().getExpMonth()));
		cardDetails.setExpYear(new Integer(paypalRequestDTO.getUserInputDTO().getExpYear()));
		cardDetails.setCVV2(paypalRequestDTO.getUserInputDTO().getCVV2());

		PayerInfoType payer = new PayerInfoType();
		com.paypal.soap.api.PersonNameType name = new com.paypal.soap.api.PersonNameType();
		name.setFirstName(paypalRequestDTO.getUserInputDTO().getBuyerFirstName());
		name.setLastName(paypalRequestDTO.getUserInputDTO().getBuyerLastName());
		payer.setPayerName(name);
		payer.setPayerCountry(paypalRequestDTO.getUserInputDTO().getCountryCodeType());

		com.paypal.soap.api.AddressType billingAddress = new com.paypal.soap.api.AddressType();
		billingAddress.setCityName(paypalRequestDTO.getUserInputDTO().getBuyerCity());
		billingAddress.setStreet1(paypalRequestDTO.getUserInputDTO().getBuyerAddress1());
		billingAddress.setStreet2(paypalRequestDTO.getUserInputDTO().getBuyerAddress2());
		billingAddress.setCountry(paypalRequestDTO.getUserInputDTO().getCountryCodeType());
		billingAddress.setPostalCode(paypalRequestDTO.getUserInputDTO().getPostalCode());
		billingAddress.setStateOrProvince(paypalRequestDTO.getUserInputDTO().getBuyerState());

		payer.setAddress(billingAddress);

		cardDetails.setCardOwner(payer);
		details.setCreditCard(cardDetails);
		details.setIPAddress(paypalRequestDTO.getUserInputDTO().getIPAddress());
		details.setPaymentAction(paypalRequestDTO.getUserInputDTO().getPaymentAction());
		pprequest.setDoDirectPaymentRequestDetails(details);
		return pprequest;
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
	}

	public SetExpressCheckoutResponseType doSetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			SetExpressCheckoutRequestType pprequest) throws ModuleException {
		SetExpressCheckoutResponseType ppresponse = null;
		try {
			APIProfile profile = createProfile(paypalRequestDTO);
			ppresponse = setExpressCheckout(paypalRequestDTO, profile, pprequest);
		} catch (PayPalException e) {
			log.error(e);
		}
		return ppresponse;
	}

	public GetExpressCheckoutDetailsResponseType doGetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			GetExpressCheckoutDetailsRequestType pprequest) throws ModuleException {
		GetExpressCheckoutDetailsResponseType ppresponse = null;
		try {
			APIProfile profile = createProfile(paypalRequestDTO);
			ppresponse = getExpressCheckoutDetails(paypalRequestDTO, profile, pprequest);
		} catch (Exception e) {
			log.error(e);
		}
		return ppresponse;
	}

	public DoExpressCheckoutPaymentResponseType doDoExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoExpressCheckoutPaymentRequestType pprequest) throws ModuleException {
		DoExpressCheckoutPaymentResponseType ppresponse = null;
		try {
			APIProfile profile = createProfile(paypalRequestDTO);
			ppresponse = doExpressCheckoutService(profile, paypalRequestDTO, pprequest);
		} catch (Exception e) {
			log.error(e);
		}
		return ppresponse;
	}

	public DoDirectPaymentResponseType doDirectPaymentWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoDirectPaymentRequestType pprequest) throws ModuleException {
		DoDirectPaymentResponseType ppresponse = null;

		try {
			APIProfile profile = createProfile(paypalRequestDTO);
			ppresponse = doDirectPaymentCode(profile, pprequest);
		} catch (PayPalException e) {
			log.error(e);
		}
		return ppresponse;
	}

	public GetTransactionDetailsRequestType prepareDoGetTransactionDetailsCode(PAYPALRequestDTO paypalRequestDTO) {
		GetTransactionDetailsRequestType pprequest = new GetTransactionDetailsRequestType();
		pprequest.setVersion(VERSION);
		// Add request-specific fields to the request.
		pprequest.setTransactionID(paypalRequestDTO.getUserInputDTO().getTransactionId());

		return pprequest;
	}

	public GetTransactionDetailsResponseType doGetTransactionDetailsCode(GetTransactionDetailsRequestType pprequest,
			PAYPALRequestDTO paypalRequestDTO) {
		GetTransactionDetailsResponseType ppresponse = null;
		try {
			CallerServices caller = new CallerServices();
			APIProfile profile = createProfile(paypalRequestDTO);
			caller.setAPIProfile(profile);
			// Execute the API operation and obtain the response.
			ppresponse = (GetTransactionDetailsResponseType) caller.call("GetTransactionDetails", pprequest);
		} catch (Exception ex) {
			log.error(ex);
		}
		return ppresponse;
	}

}