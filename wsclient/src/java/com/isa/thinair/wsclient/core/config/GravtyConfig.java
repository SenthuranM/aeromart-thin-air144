package com.isa.thinair.wsclient.core.config;

import java.util.Properties;

public class GravtyConfig {

	private Properties gravtyURL;

	private String authToken;

	private String tenantCode;

	private boolean applyProxy;

	private String username;

	private String password;

	private String xApiKey;

	private Properties dashboardUrls;

	private Properties supportingLanguages;

	private String defaultLanguage;
	
	private Integer programId; 
	
	private Integer expirationBuffer;

	public Properties getGravtyURL() {
		return gravtyURL;
	}

	public void setGravtyURL(Properties gravtyURL) {
		this.gravtyURL = gravtyURL;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public boolean isApplyProxy() {
		return applyProxy;
	}

	public void setApplyProxy(boolean applyProxy) {
		this.applyProxy = applyProxy;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getxApiKey() {
		return xApiKey;
	}

	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}

	public Properties getDashboardUrls() {
		return dashboardUrls;
	}

	public void setDashboardUrls(Properties dashboardUrls) {
		this.dashboardUrls = dashboardUrls;
	}

	public Properties getSupportingLanguages() {
		return supportingLanguages;
	}

	public void setSupportingLanguages(Properties supportingLanguages) {
		this.supportingLanguages = supportingLanguages;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	public Integer getExpirationBuffer() {
		return expirationBuffer;
	}

	public void setExpirationBuffer(Integer expirationBuffer) {
		this.expirationBuffer = expirationBuffer;
	}

	

}
