package com.isa.thinair.wsclient.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class SamanClientConfig extends DefaultModuleConfig {

	private String wsdlUrl;

	private String httpProxy;

	private String httpProxyPort;

	private boolean useProxy;
	
	private String srtmWsdlUrl;

	public String getWsdlUrl() {
		return wsdlUrl;
	}

	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}

	public String getHttpProxy() {
		return httpProxy;
	}

	public void setHttpProxy(String httpProxy) {
		this.httpProxy = httpProxy;
	}

	public String getHttpProxyPort() {
		return httpProxyPort;
	}

	public void setHttpProxyPort(String httpProxyPort) {
		this.httpProxyPort = httpProxyPort;
	}

	public boolean isUseProxy() {
		return useProxy;
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	public String getSrtmWsdlUrl() {
		return srtmWsdlUrl;
	}

	public void setSrtmWsdlUrl(String srtmWsdlUrl) {
		this.srtmWsdlUrl = srtmWsdlUrl;
	}

}
