package com.isa.thinair.wsclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.wsclient.api.service.WSClientBD;

@Remote
public interface WSClientBDImpl extends WSClientBD {

}
