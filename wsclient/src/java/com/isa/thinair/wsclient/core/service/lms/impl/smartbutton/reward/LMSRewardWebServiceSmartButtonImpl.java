package com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.reward;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.dto.lms.RewardData;
import com.isa.thinair.wsclient.api.lms.offer.CancelMemberRewardsReturn;
import com.isa.thinair.wsclient.api.lms.offer.OfferSoapStub;
import com.isa.thinair.wsclient.api.lms.offer.RedeemMemberRewardsReturn;
import com.isa.thinair.wsclient.api.lms.offer.RewardCancelResultItem;
import com.isa.thinair.wsclient.api.lms.offer.RewardRedeemResultItem;
import com.isa.thinair.wsclient.api.lms.reward.ReqIssueVariableReward;
import com.isa.thinair.wsclient.api.lms.reward.RespIssueVariableReward;
import com.isa.thinair.wsclient.api.lms.reward.RewardSoapStub;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.service.lms.base.reward.LMSRewardWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.common.LMSCommonServiceSmartButtonImpl;

public class LMSRewardWebServiceSmartButtonImpl implements DefaultWebserviceClient, LMSRewardWebService {

	private static Log log = LogFactory.getLog(LMSRewardWebServiceSmartButtonImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		return LMSSmartButtonWebServiceInvoker.SERVICE_PROVIDER;
	}

	/**
	 * Get issuing rewards for a loyalty member before actually redeeming from available loyalty points
	 * 
	 * @param request
	 *            Internal request object to hold required data
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public IssueRewardResponse issueVariableRewards(IssueRewardRequest request, String pnr, Map<String, Double> productPoints) throws ModuleException {

		// Validate rewards issuing
		// validateVariableRewardAmount(request);

		IssueRewardResponse response = null;

		RewardSoapStub clientStub = (RewardSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.REWARD);

		List<RewardData> rewards = request.getRewards();
		if (rewards != null) {
			response = new IssueRewardResponse();
			Set<Integer> rewardIds = new HashSet<Integer>();
			Map<String, Double> productRewardCost = new HashMap<String, Double>();
			for (RewardData rewardData : rewards) {
				ReqIssueVariableReward lmsRewardReq = LMSCommonUtil.populateReqIssueVariableReward(rewardData);
				lmsRewardReq.setLocationExternalReference(request.getLocationExternalReference());
				lmsRewardReq.setCardNumber(request.getMemberAccountId());
				try {
					RespIssueVariableReward lmsRewardRes = clientStub.issueVariableReward(lmsRewardReq);
					int returnCode = (lmsRewardRes == null) ? -1 : lmsRewardRes.getReturnCode();

					if (lmsRewardRes == null || lmsRewardRes.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {

						// If error occurred, revert already issued rewards
						if (!rewardIds.isEmpty()) {
							int[] rewardsArr = new int[rewardIds.size()];
							int counter = 0;
							for (Integer rewardId : rewardIds) {
								rewardsArr[counter++] = rewardId;
							}
							cancelMemberRewards(null, convertIntArrayToStringArray(rewardsArr), null);
						}

						StringBuilder sb = new StringBuilder("Error in issueVariableReward");
						sb.append(" for Reward: ").append(rewardData.getRewardTypeExtReference());
						sb.append(" , member: ").append(request.getMemberAccountId());
						sb.append(" , Return Code: ").append(returnCode);
						LMSCommonServiceSmartButtonImpl.wrapErrorResponse(lmsRewardRes, returnCode, sb.toString(),
								"wsclient.loyalty.redemption.failed");
					} else {
						rewardIds.add(lmsRewardRes.getMemberIssuedRewardId());
						productRewardCost.put(rewardData.getRewardTypeExtReference(), lmsRewardRes.getVariableRewardPointCost());
					}

					log.info("LMS member issue variable rewards for " + request.getMemberAccountId() + " response code "
							+ returnCode);
				} catch (RemoteException e) {
					log.error("Error in issueVariableRewards for " + request.getMemberAccountId(), e);
					throw new ModuleException(e, "wsclient.loyalty.redemption.failed");
				}
			}
			response.setIssuedRewardIds(convertIntArrayToStringArray(LMSCommonUtil.converttoReponseRewardIds(rewardIds)));
			response.setProductLoyaltyPointCost(productRewardCost);
		}

		return response;
	}
	private  String[] convertIntArrayToStringArray(int[] rewardsIds){
		String[] convertedStringArray = new String[rewardsIds.length];
		for (int i = 0; i < rewardsIds.length; i++)
			convertedStringArray[i] = String.valueOf(rewardsIds[i]);
		return convertedStringArray;
	}
	private  int[] convertStringArrayToIntArray(String[] rewardsIds){
		int[] convertedIntArray = new int[rewardsIds.length];
		for (int i = 0; i < rewardsIds.length; i++)
			convertedIntArray[i] = Integer.parseInt(rewardsIds[i]);
		return convertedIntArray;
	}

	@Override
	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException {
		OfferSoapStub clientStub = (OfferSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.OFFER);

		RedeemMemberRewardsReturn lmsResponse = null;

		try {
			lmsResponse = clientStub.redeemMemberRewards(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(),
					locationExtReference, convertStringArrayToIntArray(rewardIds));
		} catch (RemoteException e) {
			log.error("Error in redeemMemberRewards: " + rewardIds, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		handleRedeemResponse(lmsResponse, rewardIds, locationExtReference, memberAccountId);

		return true;
	}

	@Override
	public boolean cancelMemberRewards(String pnr,String[] memberRewardIds, String memberAccountId) throws ModuleException {

		OfferSoapStub clientStub = (OfferSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.OFFER);

		
		CancelMemberRewardsReturn response = null;
		try {
			response = clientStub.cancelMemberRewards(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), convertStringArrayToIntArray(memberRewardIds));
		} catch (RemoteException e) {
			log.error("Error in cancelMemberRewards: " + memberRewardIds + " for member " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		handleCancelResponse(response, memberRewardIds, memberAccountId);

		return true;
	}

	private void handleCancelResponse(CancelMemberRewardsReturn response, String[] memberRewardIds, String memberAccountId)
			throws ModuleException {
		int returnCode = (response == null) ? -1 : response.getReturnCode();
		StringBuilder sb = new StringBuilder("Error in cancelMemberRewards");
		sb.append(" member account ID: ").append(memberAccountId);
		sb.append(" , reward IDs: ").append(memberRewardIds);
		sb.append(" , return code: ").append(returnCode);
		LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, returnCode, sb.toString(), "wsclient.loyalty.revert.redemption.failed");

		log.info("LMS member cancel rewards for " + memberAccountId + " response code " + returnCode);

		log.info("Cancelled issued Rewards IDs: " + Arrays.toString(memberRewardIds) + " for Loyalty Member: " + memberAccountId);

		if (response.getRewardCancelResults() != null) {
			for (RewardCancelResultItem rewardItem : response.getRewardCancelResults()) {
				returnCode = (rewardItem == null) ? -1 : rewardItem.getReturnCode();
				sb = new StringBuilder("Error in cancelMemberRewards");
				sb.append(" member account ID: ").append(memberAccountId);
				sb.append(" , reward ID: ").append(rewardItem.getMemberRewardId());
				sb.append(" , return code: ").append(rewardItem.getReturnCode());
				LMSCommonServiceSmartButtonImpl.wrapErrorResponse(rewardItem, returnCode, sb.toString(),
						"wsclient.loyalty.redemption.failed");
			}
		}
	}

	private void handleRedeemResponse(RedeemMemberRewardsReturn lmsResponse, String[] memberRewardIds, String locationExtReference,
			String memberAccountId) throws ModuleException {
		int returnCode = (lmsResponse == null) ? -1 : lmsResponse.getReturnCode();

		log.info("LMS member redeem rewards for " + memberAccountId + " response code " + returnCode);

		if (returnCode != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
			cancelMemberRewards(null, memberRewardIds, memberAccountId);
		} else {
			log.info("Redeemed reward IDs: " + Arrays.toString(memberRewardIds) + " by Loyalty Member: " + memberAccountId);
		}

		StringBuilder sb = new StringBuilder("Error in redeemMemberRewards");
		sb.append(" member account ID: ").append(memberAccountId);
		sb.append(" Location Ext Ref: ").append(locationExtReference);
		sb.append(" , reward IDs: ").append(memberRewardIds);
		sb.append(" , return code: ").append(returnCode);
		LMSCommonServiceSmartButtonImpl.wrapErrorResponse(lmsResponse, returnCode, sb.toString(), "wsclient.loyalty.redemption.failed");

		if (lmsResponse.getRewardRedeemResults() != null) {
			for (RewardRedeemResultItem rewardItem : lmsResponse.getRewardRedeemResults()) {
				returnCode = (rewardItem == null) ? -1 : rewardItem.getReturnCode();

				if (returnCode != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					cancelMemberRewards(null, memberRewardIds, memberAccountId);
				}

				sb = new StringBuilder("Error in redeemMemberRewards");
				sb.append(" member account ID: ").append(memberAccountId);
				sb.append(" , reward ID: ").append(rewardItem.getMemberRewardId());
				sb.append(" , return code: ").append(rewardItem.getReturnCode());
				LMSCommonServiceSmartButtonImpl.wrapErrorResponse(rewardItem, returnCode, sb.toString(),
						"wsclient.loyalty.redemption.failed");
			}
		}
	}
}
