package com.isa.thinair.wsclient.core.service.typeb;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRQ;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRS;

/**
 * @author Manoj Dhanushka
 */
public class TypeBResWebServicesClientImpl {
	
	private static Log log = LogFactory.getLog(TypeBResWebServicesClientImpl.class);
	
	public TypeBRS processTypeBResMessage(TypeBRQ typebMsgReq) {

		TypeBRS typeBResponse = new TypeBRS();
		typeBResponse.setSynchronousCommunication(true);
		typeBResponse.setResponseStatus(TypeBRS.ResponseStatus.FAILED);

		try {
			AATypeBReservationRQ typeBReservationRQ = new AATypeBReservationRQ();
			typeBReservationRQ.setMessage(typebMsgReq.getTypeBMessage());
			AATypeBReservationRS typeBReservationRS = TypeBWebServiceInvoker.getTypeBExposedWS(typebMsgReq.getWsServerUrl())
					.processTypeBResMessage(typeBReservationRQ);
			if (typeBReservationRS.isSuccess()) {
				typeBResponse.setResponseStatus(TypeBRS.ResponseStatus.SUCCESSFUL);
			}
			typeBResponse.setResponseMessage(typeBReservationRS.getResMessage());
		} catch (Exception e) {
			log.info(e);
		}
		return typeBResponse;
	}
}
