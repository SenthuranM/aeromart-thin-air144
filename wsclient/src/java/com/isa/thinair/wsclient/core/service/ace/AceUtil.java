package com.isa.thinair.wsclient.core.service.ace;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import ws.acord.schemas.crs.travel.global.ace.GetTravelPolicy;
import ws.acord.schemas.crs.travel.global.ace.GetTravelQuote;
import acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq;
import acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem;
import acord_policyreq.schemas.crs.travel.global.ace.ObjectFactory;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo.Communications;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo.Communications.EmailInfo;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo.Communications.PhoneInfo;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.PersPolicy;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.PersPolicy.ComAcegroupDestination;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.PersPolicy.ComAcegroupInsuredPackage;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.PersPolicy.ComAcegroupPlan;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyQuoteInqRq.PersPolicy.ContractTerm;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.SignonRq;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.SignonRq.ClientApp;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd.CustId;
import acord_quotereq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd.CustPswd;
import acord_quotereq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyQuoteInqRqDataItem;
import acord_quotereq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyQuoteInqRqDataItem.DataItem;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.ACEClientConfig;
import com.isa.thinair.wsclient.core.util.CommonUtil;

public class AceUtil {
	
	private static String PNR = "BookingReference";

	private static String BASE_PREMIUM = "BasePremium";

	private static String DEPARTURE = "DepartureCity";

	private static String ARRIVAL = "ArrivalCity";

	private static String ARRIVAL_DATE = "ArrivalDate";

	private static String DATE_FMT = "yyyy-MM-dd";

	private static String CALLCENTER = "CallCentre";

	private static Integer XBE = 3;

	static GetTravelQuote prepareQuoteRequest(IInsuranceRequest iRequest) throws DatatypeConfigurationException, ModuleException {
		GetTravelQuote getTravelQuote = new GetTravelQuote();

		ACORD acord = getAcord(iRequest);
		getTravelQuote.setACORD(acord);
		return getTravelQuote;
	}

	static GetTravelPolicy prepareTravelPolicyRequest(IInsuranceRequest iRequest) throws DatatypeConfigurationException,
			ModuleException {
		GetTravelPolicy travelPolicy = new GetTravelPolicy();

		acord_policyreq.schemas.crs.travel.global.ace.ACORD acord = getTravelPolicyAcord(iRequest);
		travelPolicy.setACORD(acord);
		return travelPolicy;
	}

	private static ACORD getAcord(IInsuranceRequest iRequest) throws ModuleException, DatatypeConfigurationException {
		ACEClientConfig aceClientConfig = WSClientModuleUtils.getModuleConfig().getAceClientConfig();
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;

		ACORD acord = new ACORD();

		SignonRq signonRq = new SignonRq();
		SignonPswd signonPswd = new SignonPswd();
		CustId custId = new CustId();
		custId.setSPName(aceClientConfig.getSpName());
		custId.setCustLoginId(aceClientConfig.getCustLoginId());
		signonPswd.setCustId(custId);
		CustPswd custPswd = new CustPswd();
		custPswd.setEncryptionTypeCd(aceClientConfig.getEncryptionTypeCd());
		custPswd.setPswd(aceClientConfig.getPswd());
		signonPswd.setCustPswd(custPswd);
		signonRq.setSignonPswd(signonPswd);

		XMLGregorianCalendar clientDate = CommonUtil.getXMLGregorianCalendar(Calendar.getInstance());
		signonRq.setClientDt(clientDate);
		signonRq.setCustLangPref(aceClientConfig.getCustLangPref());

		ClientApp clientApp = new ClientApp();
		clientApp.setOrg(aceClientConfig.getSpName());
		clientApp.setName(aceClientConfig.getClientAppName());
		clientApp.setVersion(new BigDecimal(aceClientConfig.getVersion()));
		signonRq.setClientApp(clientApp);

		InsuranceSvcRq insuranceSvcRq = new InsuranceSvcRq();
		if (insReqAssemebler.getInsuranceId() != null && !insReqAssemebler.getInsuranceId().equals("")) {
			insuranceSvcRq.setRqUID(String.valueOf(insReqAssemebler.getInsuranceId()));
		} else {
			insuranceSvcRq.setRqUID(aceClientConfig.getDefaultQuoteId());
		}

		PersPkgPolicyQuoteInqRq persPkgPolicyQuoteInqRq = new PersPkgPolicyQuoteInqRq();
		persPkgPolicyQuoteInqRq.setTransactionRequestDt(clientDate);
		InsuredOrPrincipal insuredOrPrincipal = new InsuredOrPrincipal();
		GeneralPartyInfo generalPartyInfo = new GeneralPartyInfo();

		int count = 1;
		XMLGregorianCalendar defBD = DatatypeFactory.newInstance().newXMLGregorianCalendar(1900, 01, 01, 0, 0, 0, 0, 0);
		for (InsurePassenger insurePassenger : insReqAssemebler.getPassengers()) {
			if (!insurePassenger.isInfant() || (insurePassenger.isInfant() && AppSysParamsUtil.allowAddInsurnaceForInfants())) {

				if (count == 1) {
					NameInfo nameInfo = new NameInfo();
					nameInfo.setId(String.valueOf(count));
					PersonName personName = new PersonName();
					personName.setSurname(insurePassenger.getLastName());
					// personName.setGivenName(insurePassenger.getFirstName());
					personName.setTitlePrefix(insurePassenger.getTitle());

					nameInfo.setPersonName(personName);
					if (insurePassenger.getDateOfBirth() != null) {
						nameInfo.setBirthDt(CalendarUtil.asXMLGregorianCalendar(insurePassenger.getDateOfBirth()));
					} else {
						nameInfo.setBirthDt(defBD);
					}

					generalPartyInfo.getNameInfo().add(nameInfo);

					Communications communications = new Communications();
					communications.setId(count + "0000");
					communications.setNameInfoRef(String.valueOf(count));
					PhoneInfo phoneInfo = new PhoneInfo();
					phoneInfo.setPhoneTypeCd("Telephone");
					String homePhoneNum = "NULL";
					if (insurePassenger.getHomePhoneNumber() != null || !insurePassenger.getHomePhoneNumber().equals("")) {
						homePhoneNum = insurePassenger.getHomePhoneNumber();
					}
					phoneInfo.setPhoneNumber(homePhoneNum);
					communications.getPhoneInfo().add(phoneInfo);

					EmailInfo emailInfo = new EmailInfo();
					if (insurePassenger.getEmail() != null && !insurePassenger.getEmail().equals("")) {
						emailInfo.setEmailAddr(insurePassenger.getEmail());
					} else {
						emailInfo.setEmailAddr("Contact_or_PolicyHolder@email.com");
					}
					communications.getEmailInfo().add(emailInfo);

					generalPartyInfo.getCommunications().add(communications);
					count++;
				}
				NameInfo nameInfo = new NameInfo();
				nameInfo.setId(String.valueOf(count));
				PersonName personName = new PersonName();
				personName.setSurname(insurePassenger.getLastName());
				personName.setTitlePrefix("NA");

				nameInfo.setPersonName(personName);
				if (insurePassenger.getDateOfBirth() != null) {
					nameInfo.setBirthDt(CalendarUtil.asXMLGregorianCalendar(insurePassenger.getDateOfBirth()));
				} else {
					nameInfo.setBirthDt(defBD);
				}

				generalPartyInfo.getNameInfo().add(nameInfo);
				count++;
			}
		}

		insuredOrPrincipal.setGeneralPartyInfo(generalPartyInfo);
		persPkgPolicyQuoteInqRq.setInsuredOrPrincipal(insuredOrPrincipal);

		PersPolicy persPolicy = new PersPolicy();
		persPolicy.setCompanyProductCd(aceClientConfig.getCompanyProductCd());
		ContractTerm contractTerm = new ContractTerm();

		Date depDate = insReqAssemebler.getInsureSegment().getDepartureDate();
		Date arriveDate = insReqAssemebler.getInsureSegment().getArrivalDate();
		boolean roundTrip = insReqAssemebler.getInsureSegment().isRoundTrip();

		contractTerm.setEffectiveDt(CommonUtil.parse(depDate));
		contractTerm.setExpirationDt(CommonUtil.parse(arriveDate));
		persPolicy.setContractTerm(contractTerm);

		ComAcegroupDestination comAcegroupDestination = new ComAcegroupDestination();
		ComAcegroupPlan comAcegroupPlan = new ComAcegroupPlan();
		if (iRequest.isAllDomastic()) {
			comAcegroupDestination.setRqUID(aceClientConfig.getDestination());
			comAcegroupDestination.setDestinationDesc(aceClientConfig.getDestinationDesc());
			if (!roundTrip) {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeOneWay());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeOneWayDes());
			} else {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeReturn());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeReturnDes());
			}
		} else {
			comAcegroupDestination.setRqUID(aceClientConfig.getDestinationInternational());
			comAcegroupDestination.setDestinationDesc(aceClientConfig.getDestinationInternationalDesc());
			if (!roundTrip) {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeInternationalOneWay());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeInternationalOneWayDes());
			} else {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeInternationalReturn());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeInternationalReturnDes());
			}
		}
		persPolicy.setComAcegroupDestination(comAcegroupDestination);
		persPolicy.setComAcegroupPlan(comAcegroupPlan);

		ComAcegroupInsuredPackage comAcegroupInsuredPackage = new ComAcegroupInsuredPackage();
		comAcegroupInsuredPackage.setRqUID(aceClientConfig.getInsuredPackage());
		comAcegroupInsuredPackage.setInsuredPackageDesc(aceClientConfig.getInsuredPackageDesc());
		persPolicy.setComAcegroupInsuredPackage(comAcegroupInsuredPackage);

		persPkgPolicyQuoteInqRq.setPersPolicy(persPolicy);

		ArrayOfACORDInsuranceSvcRqPersPkgPolicyQuoteInqRqDataItem arrayOfACORDDataItems = new ArrayOfACORDInsuranceSvcRqPersPkgPolicyQuoteInqRqDataItem();
		DataItem pnrDetails = new DataItem();
		pnrDetails.setKey(PNR);
		pnrDetails.setType(aceClientConfig.getStringType());
		pnrDetails.setValue((insReqAssemebler.getPnr() != null && !insReqAssemebler.getPnr().equals("")) ? insReqAssemebler
				.getPnr() : aceClientConfig.getDefaultQuoteId());
		arrayOfACORDDataItems.getDataItem().add(pnrDetails);

		DataItem departure = new DataItem();
		departure.setKey(DEPARTURE);
		departure.setType(aceClientConfig.getStringType());
		departure.setValue(insReqAssemebler.getInsureSegment().getFromAirportCode());
		arrayOfACORDDataItems.getDataItem().add(departure);

		DataItem arrival = new DataItem();
		arrival.setKey(ARRIVAL);
		arrival.setType(aceClientConfig.getStringType());
		arrival.setValue(insReqAssemebler.getInsureSegment().getToAirportCode());
		arrayOfACORDDataItems.getDataItem().add(arrival);

		DataItem arrivalDate = new DataItem();
		arrivalDate.setKey(ARRIVAL_DATE);
		arrivalDate.setType(aceClientConfig.getStringType());
		arrivalDate.setValue(CommonUtil.formatDate(insReqAssemebler.getInsureSegment().getArrivalDate(), DATE_FMT));
		arrayOfACORDDataItems.getDataItem().add(arrivalDate);

		DataItem channelCode = new DataItem();
		channelCode.setKey(CALLCENTER);
		channelCode.setType(aceClientConfig.getStringType());
		if (XBE.equals(insReqAssemebler.getInsureSegment().getSalesChanelCode())) {
			channelCode.setValue("Y");
		} else {
			channelCode.setValue("N");
		}
		arrayOfACORDDataItems.getDataItem().add(channelCode);

		persPkgPolicyQuoteInqRq.setComAcegroupDataExtensions(arrayOfACORDDataItems);
		insuranceSvcRq.setPersPkgPolicyQuoteInqRq(persPkgPolicyQuoteInqRq);

		acord.setSignonRq(signonRq);
		acord.setInsuranceSvcRq(insuranceSvcRq);

		return acord;
	}

	private static acord_policyreq.schemas.crs.travel.global.ace.ACORD getTravelPolicyAcord(IInsuranceRequest iRequest)
			throws ModuleException, DatatypeConfigurationException {
		ACEClientConfig aceClientConfig = WSClientModuleUtils.getModuleConfig().getAceClientConfig();
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		ObjectFactory objectFactory = new ObjectFactory();

		acord_policyreq.schemas.crs.travel.global.ace.ACORD acord = objectFactory.createACORD();

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.SignonRq signonRq = objectFactory.createACORDSignonRq();
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd signonPswd = objectFactory
				.createACORDSignonRqSignonPswd();
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd.CustId custId = objectFactory
				.createACORDSignonRqSignonPswdCustId();
		custId.setSPName(aceClientConfig.getSpName());
		custId.setCustLoginId(aceClientConfig.getCustLoginId());
		signonPswd.setCustId(custId);
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.SignonRq.SignonPswd.CustPswd custPswd = objectFactory
				.createACORDSignonRqSignonPswdCustPswd();
		custPswd.setEncryptionTypeCd(aceClientConfig.getEncryptionTypeCd());
		custPswd.setPswd(aceClientConfig.getPswd());
		signonPswd.setCustPswd(custPswd);
		signonRq.setSignonPswd(signonPswd);

		XMLGregorianCalendar clientDate = CommonUtil.getXMLGregorianCalendar(Calendar.getInstance());
		signonRq.setClientDt(clientDate);
		signonRq.setCustLangPref(aceClientConfig.getCustLangPref());

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.SignonRq.ClientApp clientApp = objectFactory
				.createACORDSignonRqClientApp();
		clientApp.setOrg(aceClientConfig.getSpName());
		clientApp.setName(aceClientConfig.getClientAppName());
		clientApp.setVersion(new BigDecimal(aceClientConfig.getVersion()));
		signonRq.setClientApp(clientApp);

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq insuranceSvcRq = objectFactory
				.createACORDInsuranceSvcRq();
		if (insReqAssemebler.getInsuranceId() != null && !insReqAssemebler.getInsuranceId().equals("")) {
			insuranceSvcRq.setRqUID(String.valueOf(insReqAssemebler.getInsuranceId()));
		} else {
			insuranceSvcRq.setRqUID(aceClientConfig.getDefaultQuoteId());
		}

		PersPkgPolicyAddRq persPkgPolicyAddRq = objectFactory.createACORDInsuranceSvcRqPersPkgPolicyAddRq();
		persPkgPolicyAddRq.setTransactionRequestDt(clientDate);
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal insuredOrPrincipal = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipal();
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo generalPartyInfo = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfo();

		int count = 1;
		XMLGregorianCalendar defBD = DatatypeFactory.newInstance().newXMLGregorianCalendar(1900, 01, 01, 0, 0, 0, 0, 0);
		for (InsurePassenger insurePassenger : insReqAssemebler.getPassengers()) {
			if (!insurePassenger.isInfant() || (insurePassenger.isInfant() && AppSysParamsUtil.allowAddInsurnaceForInfants())) {
				if (count == 1) {
					acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo nameInfo = objectFactory
							.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoNameInfo();
					nameInfo.setId(String.valueOf(count));
					acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName personName = objectFactory
							.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoNameInfoPersonName();
					personName.setSurname(insurePassenger.getLastName());
					personName.setGivenName(insurePassenger.getFirstName());
					personName.setTitlePrefix(insurePassenger.getTitle());

					nameInfo.setPersonName(personName);
					if (insurePassenger.getDateOfBirth() != null) {
						nameInfo.setBirthDt(CalendarUtil.asXMLGregorianCalendar(insurePassenger.getDateOfBirth()));
					} else {
						nameInfo.setBirthDt(defBD);
					}

					generalPartyInfo.getNameInfo().add(nameInfo);

					if (insurePassenger.getEmail() != null && !insurePassenger.getEmail().equals("")) {
						acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.Communications communications = objectFactory
								.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoCommunications();
						communications.setId(count + "0000");
						communications.setNameInfoRef(String.valueOf(count));
						acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.Communications.PhoneInfo phoneInfo = objectFactory
								.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoCommunicationsPhoneInfo();
						phoneInfo.setPhoneTypeCd("Telephone");
						String homePhoneNum = "NULL";
						if (insurePassenger.getHomePhoneNumber() != null || !insurePassenger.getHomePhoneNumber().equals("")) {
							homePhoneNum = insurePassenger.getHomePhoneNumber();
						}
						phoneInfo.setPhoneNumber(homePhoneNum);
						communications.getPhoneInfo().add(phoneInfo);

						acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.Communications.EmailInfo emailInfo = objectFactory
								.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoCommunicationsEmailInfo();
						emailInfo.setEmailAddr(insurePassenger.getEmail());
						communications.getEmailInfo().add(emailInfo);

						generalPartyInfo.getCommunications().add(communications);
					}
					count++;
				}
				acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo nameInfo = objectFactory
						.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoNameInfo();
				nameInfo.setId(String.valueOf(count));
				acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.InsuredOrPrincipal.GeneralPartyInfo.NameInfo.PersonName personName = objectFactory
						.createACORDInsuranceSvcRqPersPkgPolicyAddRqInsuredOrPrincipalGeneralPartyInfoNameInfoPersonName();
				personName.setSurname(insurePassenger.getLastName());
				personName.setGivenName(insurePassenger.getFirstName());
				personName.setTitlePrefix(insurePassenger.getTitle());

				nameInfo.setPersonName(personName);
				if (insurePassenger.getDateOfBirth() != null) {
					nameInfo.setBirthDt(CalendarUtil.asXMLGregorianCalendar(insurePassenger.getDateOfBirth()));
				} else {
					nameInfo.setBirthDt(defBD);
				}

				generalPartyInfo.getNameInfo().add(nameInfo);
				count++;
			}
		}

		insuredOrPrincipal.setGeneralPartyInfo(generalPartyInfo);
		persPkgPolicyAddRq.setInsuredOrPrincipal(insuredOrPrincipal);

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.PersPolicy persPolicy = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqPersPolicy();
		persPolicy.setCompanyProductCd(aceClientConfig.getCompanyProductCd());
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.PersPolicy.ContractTerm contractTerm = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqPersPolicyContractTerm();

		Date depDate = insReqAssemebler.getInsureSegment().getDepartureDate();
		Date arriveDate = insReqAssemebler.getInsureSegment().getArrivalDate();
		boolean roundTrip = insReqAssemebler.getInsureSegment().isRoundTrip();

		contractTerm.setEffectiveDt(CommonUtil.parse(depDate));
		contractTerm.setExpirationDt(CommonUtil.parse(arriveDate));
		persPolicy.setContractTerm(contractTerm);

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.PersPolicy.ComAcegroupDestination comAcegroupDestination = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqPersPolicyComAcegroupDestination();
		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.PersPolicy.ComAcegroupPlan comAcegroupPlan = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqPersPolicyComAcegroupPlan();
		if (iRequest.isAllDomastic()) {
			comAcegroupDestination.setRqUID(aceClientConfig.getDestination());
			comAcegroupDestination.setDestinationDesc(aceClientConfig.getDestinationDesc());
			if (!roundTrip) {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeOneWay());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeOneWayDes());
			} else {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeReturn());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeReturnDes());
			}
		} else {
			comAcegroupDestination.setRqUID(aceClientConfig.getDestinationInternational());
			comAcegroupDestination.setDestinationDesc(aceClientConfig.getDestinationInternationalDesc());
			if (!roundTrip) {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeInternationalOneWay());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeInternationalOneWayDes());
			} else {
				comAcegroupPlan.setRqUID(aceClientConfig.getPlanCodeInternationalReturn());
				comAcegroupPlan.setPlanDesc(aceClientConfig.getPlanCodeInternationalReturnDes());
			}
		}
		persPolicy.setComAcegroupDestination(comAcegroupDestination);
		persPolicy.setComAcegroupPlan(comAcegroupPlan);

		acord_policyreq.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRq.PersPkgPolicyAddRq.PersPolicy.ComAcegroupInsuredPackage comAcegroupInsuredPackage = objectFactory
				.createACORDInsuranceSvcRqPersPkgPolicyAddRqPersPolicyComAcegroupInsuredPackage();
		comAcegroupInsuredPackage.setRqUID(aceClientConfig.getInsuredPackage());
		comAcegroupInsuredPackage.setInsuredPackageDesc(aceClientConfig.getInsuredPackageDesc());
		persPolicy.setComAcegroupInsuredPackage(comAcegroupInsuredPackage);

		persPkgPolicyAddRq.setPersPolicy(persPolicy);

		ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem arrayOfACORDDataItems = new ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem();
		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem pnrDetails = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		pnrDetails.setKey(PNR);
		pnrDetails.setType(aceClientConfig.getStringType());
		pnrDetails.setValue(!insReqAssemebler.getPnr().equals("") ? insReqAssemebler.getPnr() : aceClientConfig
				.getDefaultQuoteId());
		arrayOfACORDDataItems.getDataItem().add(pnrDetails);

		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem basePrimium = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		basePrimium.setKey(BASE_PREMIUM);
		basePrimium.setType("System.Double");
		basePrimium.setValue(String.valueOf(insReqAssemebler.getQuotedTotalPremiumAmount()));
		arrayOfACORDDataItems.getDataItem().add(basePrimium);

		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem departure = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		departure.setKey(DEPARTURE);
		departure.setType(aceClientConfig.getStringType());
		departure.setValue(insReqAssemebler.getInsureSegment().getFromAirportCode());
		arrayOfACORDDataItems.getDataItem().add(departure);

		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem arrival = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		arrival.setKey(ARRIVAL);
		arrival.setType(aceClientConfig.getStringType());
		arrival.setValue(insReqAssemebler.getInsureSegment().getToAirportCode());
		arrayOfACORDDataItems.getDataItem().add(arrival);

		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem arrivalDate = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		arrivalDate.setKey(ARRIVAL_DATE);
		arrivalDate.setType(aceClientConfig.getStringType());
		arrivalDate.setValue(CommonUtil.formatDate(insReqAssemebler.getInsureSegment().getArrivalDate(), DATE_FMT));
		arrayOfACORDDataItems.getDataItem().add(arrivalDate);

		acord_policyreq.schemas.crs.travel.global.ace.ArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItem.DataItem channelCode = objectFactory
				.createArrayOfACORDInsuranceSvcRqPersPkgPolicyAddRqDataItemDataItem();
		channelCode.setType(aceClientConfig.getStringType());
		channelCode.setKey(CALLCENTER);
		if (XBE.equals(insReqAssemebler.getInsureSegment().getSalesChanelCode())) {
			channelCode.setValue("Y");
		} else {
			channelCode.setValue("N");
		}
		arrayOfACORDDataItems.getDataItem().add(channelCode);

		persPkgPolicyAddRq.setComAcegroupDataExtensions(arrayOfACORDDataItems);
		insuranceSvcRq.setPersPkgPolicyAddRq(persPkgPolicyAddRq);

		acord.setSignonRq(signonRq);
		acord.setInsuranceSvcRq(insuranceSvcRq);

		return acord;
	}

}
