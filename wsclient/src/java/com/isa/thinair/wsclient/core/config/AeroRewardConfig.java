package com.isa.thinair.wsclient.core.config;

import java.util.Properties;

public class AeroRewardConfig {

	private Properties rewardApiServiceUrl;

	private String authToken;

	private String tenantCode;
	
	private boolean applyProxy;

	public Properties getRewardApiServiceUrl() {
		return rewardApiServiceUrl;
	}

	public void setRewardApiServiceUrl(Properties rewardApiServiceUrl) {
		this.rewardApiServiceUrl = rewardApiServiceUrl;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getTenantCode() {
		return tenantCode;
	}

	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	public boolean isApplyProxy() {
		return applyProxy;
	}

	public void setApplyProxy(boolean applyProxy) {
		this.applyProxy = applyProxy;
	}
	
	

}
