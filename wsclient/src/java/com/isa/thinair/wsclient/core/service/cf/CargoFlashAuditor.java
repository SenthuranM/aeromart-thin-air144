package com.isa.thinair.wsclient.core.service.cf;

import org.apache.commons.logging.Log;

import com.thoughtworks.xstream.XStream;

public class CargoFlashAuditor {

	public static <K, V> void log(Log log, String identifier, K request, V response) {

		if (log.isDebugEnabled()) {
			XStream xtream = new XStream();
			StringBuilder sbxml = new StringBuilder(identifier);
			sbxml.append(" Request Data \n");

			if (request != null) {
				sbxml.append(xtream.toXML(request));
			} else {
				sbxml.append("null");
			}

			sbxml.append("\n Response \n");

			if (response != null) {
				sbxml.append(xtream.toXML(response));
			} else {
				sbxml.append("null");
			}

			log.debug(sbxml);
		}
	}

}
