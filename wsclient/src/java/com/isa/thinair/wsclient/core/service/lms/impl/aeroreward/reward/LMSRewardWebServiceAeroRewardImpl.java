package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.reward;

import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.core.service.lms.base.reward.LMSRewardWebService;

public class LMSRewardWebServiceAeroRewardImpl implements LMSRewardWebService {

	@Override
	public IssueRewardResponse issueVariableRewards(IssueRewardRequest request, String pnr, Map<String, Double> productPoints) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean cancelMemberRewards(String pnr,String[] memberRewardIds, String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

}
