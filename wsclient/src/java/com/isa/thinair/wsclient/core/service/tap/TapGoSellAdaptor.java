package com.isa.thinair.wsclient.core.service.tap;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRs;
import com.isa.thinair.wsclient.api.tap.ArrayOfGateWayDC;
import com.isa.thinair.wsclient.api.tap.ArrayOfProductDC;
import com.isa.thinair.wsclient.api.tap.CustomerDC;
import com.isa.thinair.wsclient.api.tap.GateWayDC;
import com.isa.thinair.wsclient.api.tap.GetOrderStatusRequestDC;
import com.isa.thinair.wsclient.api.tap.GetRefundStatusRequestDC;
import com.isa.thinair.wsclient.api.tap.MerMastDC;
import com.isa.thinair.wsclient.api.tap.ObjectFactory;
import com.isa.thinair.wsclient.api.tap.OrderStatusResponseDC;
import com.isa.thinair.wsclient.api.tap.PayRequestDC;
import com.isa.thinair.wsclient.api.tap.PayResponseDC;
import com.isa.thinair.wsclient.api.tap.ProductDC;
import com.isa.thinair.wsclient.api.tap.RefundRequestDC;
import com.isa.thinair.wsclient.api.tap.RefundResponseDC;

public class TapGoSellAdaptor {

	private static ObjectFactory factory = new ObjectFactory();

	public PayRequestDC transformeToPayRequestDC(TapPaymentRequestCallRq request) {

		PayRequestDC response = new PayRequestDC();

		// Customer details
		CustomerDC customer = new CustomerDC();
		customer.setName(factory.createCustomerDCName(request.getName()));
		customer.setEmail(factory.createCustomerDCEmail(request.getEmail()));
		customer.setMobile(factory.createCustomerDCMobile(request.getMobileNo()));

		// Product Details
		ProductDC product = new ProductDC();
		product.setUnitName(factory.createProductDCUnitName(request.getUnitName()));
		product.setUnitDesc(factory.createProductDCUnitDesc(request.getUnitName()));
		product.setUnitID(factory.createProductDCUnitID(Integer.toString(request.getQuantity())));
		product.setQuantity(request.getQuantity());
		product.setUnitPrice(request.getUnitPrice());
		product.setTotalPrice(request.getTotalPrice());
		product.setCurrencyCode(factory.createProductDCCurrencyCode(request.getCurrencyCode()));

		ArrayOfProductDC productArray = new ArrayOfProductDC();
		productArray.getProductDC().add(product);

		// Merchant Details
		MerMastDC merchant = new MerMastDC();
		merchant.setMerchantID(request.getMerchantID());
		merchant.setUserName(factory.createMerMastDCUserName(request.getUserName()));
		merchant.setReturnURL(factory.createMerMastDCReturnURL(request.getReturnURL()));
		merchant.setReferenceID(factory.createMerMastDCReferenceID(request.getReferenceID()));
		merchant.setHashString(factory.createMerMastDCHashString(request.getHashString()));
		merchant.setAutoReturn(factory.createMerMastDCAutoReturn("Y")); 

		// Gateway List
		ArrayOfGateWayDC gatewayList = new ArrayOfGateWayDC();
		for(String pgw : request.getGatewayList()){
			GateWayDC gateWay = new GateWayDC();
			gateWay.setName(factory.createGateWayDCName(pgw));
			gatewayList.getGateWayDC().add(gateWay);
		}			
		
		response.setCustomerDC(factory.createCustomerDC(customer));
		response.setMerMastDC(factory.createMerMastDC(merchant));
		response.setLstProductDC(new JAXBElement(new QName("http://schemas.datacontract.org/2004/07/Tap.PayServiceContract", "lstProductDC"), ArrayOfProductDC.class, productArray));
		response.setLstGateWayDC(new JAXBElement(new QName("http://schemas.datacontract.org/2004/07/Tap.PayServiceContract", "lstGateWayDC"), ArrayOfGateWayDC.class, gatewayList));

		return response;
	}

	public TapPaymentRequestCallRs transformeToTapPaymentRequestCallRs(PayResponseDC request) {

		TapPaymentRequestCallRs response = new TapPaymentRequestCallRs();
		response.setPaymentURL(request.getPaymentURL().getValue());
		response.setReferenceID(request.getReferenceID().getValue());
		response.setResponseCode(request.getResponseCode().getValue());
		response.setResponseMessage(request.getResponseMessage().getValue());
		response.setTapPaymentURL(request.getTapPayURL().getValue());

		return response;
	}

	public GetOrderStatusRequestDC transformeToGetOrderStatusRequestDC(TapGetOrderStatusRequestCallRq request) {

		GetOrderStatusRequestDC response = new GetOrderStatusRequestDC();
		response.setMerchantID(request.getMerchantID());
		response.setReferenceID(factory.createGetOrderStatusRequestDCReferenceID(request.getReferenceID()));
		response.setPassword(factory.createGetOrderStatusRequestDCPassword(request.getPassword()));
		response.setUserName(factory.createGetOrderStatusRequestDCUserName(request.getUserName()));

		return response;
	}

	public TapGetOrderStatusRequestCallRs transformeToTapGetOrderStatusResponseCallRs(OrderStatusResponseDC request) {

		TapCaptureRequestCallRs response = new TapCaptureRequestCallRs();
		response.setPaymode(request.getPaymode().getValue());
		response.setPayTxnID(request.getPayTxnID().getValue());
		response.setResponseCode(request.getResponseCode().getValue());
		response.setResponseMessage(request.getResponseMessage().getValue());
		response.setTrackID(request.getTrackID().getValue());

		return response;
	}

	public RefundRequestDC transformeToRefundRequestDC(TapRefundCallRq request) {

		RefundRequestDC response = factory.createRefundRequestDC();
		response.setAmount(request.getAmount());
		response.setCurrencyCode(factory.createRefundRequestDCCurrencyCode(request.getCurrencyCode()));
		response.setMerchantID(request.getMerchantID());
		response.setOrdID(factory.createRefundRequestDCOrdID(request.getOrderID()));
		response.setReferenceID(factory.createRefundRequestDCReferenceID(request.getReferenceID()));
		response.setUserName(factory.createRefundRequestDCUserName(request.getUserName()));

		return response;
	}

	public TapRefundCallRs transformeToTapRefundCallRs(RefundResponseDC request) {

		TapRefundCallRs response = new TapRefundCallRs();
		response.setResponseCode(request.getResponseCode().getValue());
		response.setResponseMessage(request.getResponseMessage().getValue());

		return response;
	}

	public GetRefundStatusRequestDC transformeToGetRefundStatusRequestDC(TapRefundStatusCallRq request) {

		GetRefundStatusRequestDC response = factory.createGetRefundStatusRequestDC();
		response.setMerchantID(request.getMerchantID());
		response.setReferenceID(factory.createGetRefundStatusRequestDCReferenceID(request.getReferenceID()));
		response.setUserName(factory.createGetRefundStatusRequestDCUserName(request.getUserName()));

		return response;
	}

	public TapRefundStatusCallRs transformeToTapRefundStatusCallRs(RefundResponseDC request) {

		TapRefundStatusCallRs response = new TapRefundStatusCallRs();
		response.setResponseCode(request.getResponseCode().getValue());
		response.setResponseMessage(request.getResponseMessage().getValue());

		return response;
	}

}
