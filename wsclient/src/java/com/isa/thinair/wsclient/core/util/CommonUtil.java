package com.isa.thinair.wsclient.core.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.isa.thinair.commons.api.exception.ModuleException;

public class CommonUtil {
	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parse(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLGregorianCalendar(cal);
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to %Y-%M-%DT%h:%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar conforms to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00]
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendar(Calendar cal) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
					DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			throw new ModuleException(e, "");
		}
	}

	/**
	 * Prepare java.util.Date conforming to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00] from java.lang.String
	 * 
	 * @param cal
	 *            Calendar
	 * @return Date conforms to %Y-%M-%DT%h:%m%s format
	 * @throws ModuleException
	 */
	public static Date getDate(String xmlFormattedDate) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendar(xmlFormattedDate).toGregorianCalendar().getTime();
		} catch (DatatypeConfigurationException e) {
			throw new ModuleException(e, "");
		}
	}

	/**
	 * Formats specified date with specified format
	 * 
	 * @return String the formated date
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = "";

		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}
	
	/**
	 * 
	 * @param segmentCode
	 * @param isOrigin
	 * @return
	 */
	public static String getOriginOrDestinationFromSegment(String segmentCode,boolean isOrigin){
		String airport = null;
		
		String[] airports = segmentCode.split("/");
		
		if(isOrigin){
			airport = airports[0];
		}else{
			//If not origin, it is destination
			airport = airports[1];
		}
		
		return airport;
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static Calendar parseDateToCal(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal;
	}

}
