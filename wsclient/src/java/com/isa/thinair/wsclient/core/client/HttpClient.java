package com.isa.thinair.wsclient.core.client;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.service.aig.AIGExecutionStrategry;
import com.isa.thinair.wsclient.core.service.aig.IExecutionStrategy;

public class HttpClient extends org.apache.commons.httpclient.HttpClient {

	private HttpMethod httpMethod;
	private static final Log log = LogFactory.getLog(HttpClient.class);

	public HttpClient(String url, String method, int timeOut) {

		// set the time out
		HttpConnectionManagerParams hc = new HttpConnectionManagerParams();
		hc.setConnectionTimeout(timeOut);
		GlobalConfig globalconfig = WSClientModuleUtils.getGlobalConfig();
		if (globalconfig.getUseProxy()) {
			this.getHostConfiguration().setProxy(globalconfig.getHttpProxy(), globalconfig.getHttpPort());
		}
		this.getHttpConnectionManager().setParams(hc);

		// decide the httm method and set the url
		if ("POST".equals(method)) {
			httpMethod = new PostMethod(url);

		} else if ("GET".equals(method)) {
			httpMethod = new GetMethod(url);

		}
	}

	/**
	 * 
	 * @param executionStrategy
	 */
	public final void executeOperation(IExecutionStrategy executionStrategy) throws ModuleException {

		try {
			log.debug("HttpClient AIG executeOperation going to call");
			if (executionStrategy instanceof AIGExecutionStrategry) {
				AIGExecutionStrategry aigExecutionStrategry = (AIGExecutionStrategry) executionStrategy;
				Map<String, String> params = aigExecutionStrategry.getParams();
				Iterator keys = params.keySet().iterator();

				while (keys.hasNext()) {
					String key = (String) keys.next();
					String value = params.get(key);
					if (httpMethod instanceof PostMethod) {
						PostMethod postMethod = (PostMethod) httpMethod;
						postMethod.setParameter(key, value);
					}
				}
			}

			this.executeMethod(httpMethod);
			executionStrategy.handleResponse(httpMethod.getResponseBodyAsStream());
			log.debug("HttpClient AIG executeOperation completed");
		} catch (ConnectTimeoutException ct) {
			log.error("Http Connection Timed when Calling External Url", ct);
			throw new ModuleException("wsclient.httpclient.timeout");
		} catch (HttpException e) {
			log.error("HTTP Exceptio : ", e);
			throw new ModuleException("wsclient.httpclient.error");
		} catch (IOException e) {
			log.error("IO Oxception : ", e);
			throw new ModuleException("wsclient.io.error");
		} catch (Exception e) {
			log.error("Generic Exception Caught :", e);
			throw new ModuleException("wsclient.io.error");
		} finally {
			httpMethod.releaseConnection();
		}

	}

}
