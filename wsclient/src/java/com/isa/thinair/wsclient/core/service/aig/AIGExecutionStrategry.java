package com.isa.thinair.wsclient.core.service.aig;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isaaviation.thinair.webservices.api.airinsurance.TINSXMLDATA;

/**
 * 
 * @author Byorn.
 * 
 */
public class AIGExecutionStrategry implements IExecutionStrategy {

	private Map params;
	private TINSXMLDATA _responseTINSXMLDATA;
	private JAXBContext jc;
	private static final Log log = LogFactory.getLog(AIGExecutionStrategry.class);

	public AIGExecutionStrategry(TINSXMLDATA tinsxmldata, String url) throws ModuleException {
		log.debug("AIGExecutionStrategry constuctor called");
		String strXml = null;
		ByteArrayOutputStream b = new ByteArrayOutputStream();

		try {
			jc = JAXBContext.newInstance(tinsxmldata.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(tinsxmldata, b);
			strXml = IOUtils.toString(b.toByteArray());
			if (log.isDebugEnabled()) {
				log.debug(strXml);
			}
			log.debug("AIGExecutionStrategry completed");
		} catch (JAXBException e) {
			log.error("AIGExecutionStrategry JaxB Exception", e);
			throw new ModuleException("wsclient.aigexec.marshal");
		} catch (IOException e) {
			log.error("AIGExecutionStrategry IOException ", e);
			throw new ModuleException("wsclient.aigexec.marshal");
		}

		try {
			params = new HashMap<String, String>();
			params.put("MessageId", tinsxmldata.getHeader().getMessageId());
			params.put("MessageType", tinsxmldata.getHeader().getMessageType());
			params.put("URL", url);
			params.put("MessageText", strXml);
			if (log.isDebugEnabled()) {

			}
		} catch (Exception e) {
			log.error("unstructeded request object", e);
			throw new ModuleException("wsclient.aig.unstruct.response");
		}

	}

	/**
	 * Concrete Method for IExecutionStrategy
	 * 
	 * @throws ModuleException
	 */
	public void handleResponse(InputStream inputStream) throws ModuleException {
		log.debug("AIGExecutionStrategry handleResponse called");
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		StringBuffer sb = new StringBuffer();
		String line = null;
		try {
			while ((line = in.readLine()) != null) {
				CharSequence charSw = new StringBuffer("TINS_XML_DATA");

				if (line.contains(charSw)) {
					line = line.replace("<!DOCTYPE TINS_XML_DATA SYSTEM \"Group1.dtd\">", " ");
					line = line.replace("<!DOCTYPE TINS_XML_DATA SYSTEM \"Policy1.dtd\">", " ");
					line = line.replace("<TINS_XML_DATA>",
							"<TINS_XML_DATA xmlns=\"http://www.isaaviation.com/thinair/webservices/api/airinsurance\">");
					sb.append(line);
				} else {
					line = null;
				}
			}
		} catch (IOException e) {
			log.error("AIGExecutionStrategry handleResponse filtering InputStream", e);
			throw new ModuleException("wsclient.aig.handleresponse");
		}

		try {
			Unmarshaller unm = jc.createUnmarshaller();
			_responseTINSXMLDATA = (TINSXMLDATA) unm.unmarshal(IOUtils.toInputStream(sb.toString()));
			if (log.isDebugEnabled()) {
				displayTinsResponse(_responseTINSXMLDATA);
			}
		} catch (JAXBException e) {
			log.error("AIGExecutionStrategry handleResponse unmarshalling InputStream", e);
			throw new ModuleException("wsclient.aig.unmarshal");
		}

	}

	// will be called only if debug is enabled.
	private static void displayTinsResponse(TINSXMLDATA tinsResponse) {

		ByteArrayOutputStream b = new ByteArrayOutputStream();
		try {
			JAXBContext jc = JAXBContext.newInstance(tinsResponse.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(tinsResponse, b);
			String strXml = IOUtils.toString(b.toByteArray());
			log.debug(strXml);
		} catch (Throwable e) {
			log.error("Debug displayTinsResponse Operation Faild", e);
		}

	}

	/** get the final response **/
	public TINSXMLDATA get_responseTINSXMLDATA() {
		return _responseTINSXMLDATA;
	}

	public Map getParams() {
		return params;
	}

	public void setParams(Map params) {
		this.params = params;
	}

}
