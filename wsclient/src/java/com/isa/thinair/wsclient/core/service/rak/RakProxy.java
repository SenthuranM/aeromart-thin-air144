package com.isa.thinair.wsclient.core.service.rak;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakFlightInsuranceInfoDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.wsclient.api.dto.rak.Flight;
import com.isa.thinair.wsclient.api.dto.rak.TravelSchema;
import com.isa.thinair.wsclient.api.dto.rak.Travelers;
import com.isa.thinair.wsclient.api.rakins.ITravelService;
import com.isa.thinair.wsclient.api.rakins.TravelServiceClient;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.RAKClientConfig;

public class RakProxy {

	private static Log log = LogFactory.getLog(RakProxy.class);

	/**
	 * Publishes insurance information encapsulated in the RakFlightInsuranceInfoDTO
	 * 
	 * @param flightInsuranceInfo
	 * @return
	 */
	public static String publishInsuranceData(RakFlightInsuranceInfoDTO flightInsuranceInfo) {
		RAKClientConfig rakClientConfig = WSClientModuleUtils.getModuleConfig().getRakClientConfig();

		return pulishInsuranceData(flightInsuranceInfo, rakClientConfig);

	}

	/**
	 * Retrieves status of a uploaded insurance specified by the input parameter response
	 * 
	 * @param response
	 * @return
	 */
	public static String[] getInsurancePubStatus(String response) {
		RAKClientConfig rakClientConfig = WSClientModuleUtils.getModuleConfig().getRakClientConfig();

		return retriveUploadStatus(response, rakClientConfig);

	}

	private static String pulishInsuranceData(RakFlightInsuranceInfoDTO flightInsuranceInfo, RAKClientConfig rakClientConfig) {

		String flightContents = XMLStreamer.compose(flightInsuranceInfo);
		if (log.isDebugEnabled()) {
			log.debug("Flight Ins Data to be published================>>");
			log.debug(flightContents);
			log.debug("Starting to publish Rak Insurance data to insurance provider for flight : "
					+ flightInsuranceInfo.getFlightNo());
		}

		String response = uploadFileData(flightInsuranceInfo, rakClientConfig);

		if (log.isDebugEnabled()) {
			log.debug("End publishing Rak Insurance data for flight : " + flightInsuranceInfo.getFlightNo());
			log.debug("Rak Insurance file upload response : " + response);
		}

		return response;
	}

	private static String uploadFileData(RakFlightInsuranceInfoDTO flightInsuranceInfo, RAKClientConfig rakClientConfig) {

		try {
			TravelSchema travelSchema = prepareInsuranceFileData(flightInsuranceInfo);
			String flightContents = travelSchema.toXML(true);

			ITravelService iTravelService = getTravelServiceClient(rakClientConfig);

			String message = BeanUtils.nullHandler(iTravelService.uploadFile(flightContents, rakClientConfig.getUserName(),
					rakClientConfig.getPassword()));
			return message;
		} catch (Exception e) {
			log.error("uploadFileData", e);
			return null;
		}
	}

	private static ITravelService getTravelServiceClient(RAKClientConfig rakClientConfig) {

		TravelServiceClient tsClient = new TravelServiceClient();
		ITravelService iTravelService = tsClient.getBasicHttpBinding_ITravelService(BeanUtils.nullHandler(rakClientConfig
				.getUrl()));
		GlobalConfig globalConfig = WSClientModuleUtils.getGlobalConfig();
		String proxy = BeanUtils.nullHandler(globalConfig.getHttpProxy());
		String port = BeanUtils.nullHandler(globalConfig.getHttpPort());

		Client clientx = ((XFireProxy) Proxy.getInvocationHandler(iTravelService)).getClient();
		clientx.addInHandler(new org.codehaus.xfire.util.dom.DOMInHandler());
		clientx.addInHandler(new org.codehaus.xfire.util.LoggingHandler());
		clientx.addOutHandler(new org.codehaus.xfire.util.LoggingHandler());
		clientx.addOutHandler(new org.codehaus.xfire.util.dom.DOMOutHandler());

		if (globalConfig.getUseProxy() && proxy.length() > 0 && port.length() > 0) {
			clientx.setProperty(CommonsHttpMessageSender.HTTP_PROXY_HOST, proxy);
			clientx.setProperty(CommonsHttpMessageSender.HTTP_PROXY_PORT, port);
			clientx.setProperty(CommonsHttpMessageSender.DISABLE_PROXY_UTILS, "true");
			clientx.setProperty(CommonsHttpMessageSender.DISABLE_EXPECT_CONTINUE, "true");
		} else if (proxy.length() == 0 && port.length() == 0) {

		} else {
			throw new IllegalArgumentException("Invalid Proxy Information specified in the rakclient-config.mod.xml");
		}

		return iTravelService;

	}

	/**
	 * Web service call to retrieve file upload status
	 * 
	 * @param responseTimeStamp
	 * @param rakClientConfig
	 * @return
	 */
	private static String[] retriveUploadStatus(String responseTimeStamp, RAKClientConfig rakClientConfig) {

		try {

			ITravelService travelService = getTravelServiceClient(rakClientConfig);
			String result = BeanUtils.nullHandler(travelService.getResult(responseTimeStamp, rakClientConfig.getUserName(),
					rakClientConfig.getPassword()));
			if (log.isDebugEnabled()) {
				log.debug("GetStatus Response :\n" + result);

			}

			return processResultResponse(result);

		} catch (Exception e) {
			log.error("getResult", e);
			return new String[] { InsurancePublisherDetailDTO.RAK_INS_UPLOAD_WS_ERROR };
		}
	}

	private static String[] processResultResponse(String response) {

		if (response.contains("Succeeded") && response.contains("Error")) {
			return new String[] { InsurancePublisherDetailDTO.RAK_INS_UPLOAD_ERRONEOUS, response };
		} else if (response.contains("Succeeded")) {
			return new String[] { InsurancePublisherDetailDTO.RAK_INS_UPLOAD_SUCCESS, response };
		} else {
			return new String[] { InsurancePublisherDetailDTO.RAK_INS_UPLOAD_FAILED, response };
		}

	}

	private static TravelSchema prepareInsuranceFileData(RakFlightInsuranceInfoDTO flightInsuranceInfo) {
		TravelSchema travelSchema = new TravelSchema();
		Flight flight = new Flight();
		flight.setFlightNo(BeanUtils.nullHandler(flightInsuranceInfo.getFlightNo()));
		flight.setFlightDepartureDate(BeanUtils.nullHandler(BeanUtils.parseDateFormat(
				flightInsuranceInfo.getFlightDepartureDate(), "MM/dd/yyyy")));
		flight.setNotificationSendingTime(BeanUtils.nullHandler(BeanUtils.parseDateFormat(
				flightInsuranceInfo.getNotificationSendingTime(), "MM/dd/yyyy")));
		flight.setOriginAirport(BeanUtils.nullHandler(flightInsuranceInfo.getOriginAirport()));
		flight.setRecordsNo(BeanUtils.nullHandler(flightInsuranceInfo.getRecordsNo()));

		if (flightInsuranceInfo.getTravellers() != null) {
			List<Travelers> paxList = new ArrayList<Travelers>();
			for (RakInsuredTraveler rakTraveler : flightInsuranceInfo.getTravellers()) {
				Travelers travelers = new Travelers();
				travelers.setSequenceNo(BeanUtils.nullHandler(rakTraveler.getSequenceNo()));
				travelers.setBookingNo(BeanUtils.nullHandler(rakTraveler.getBookingNo()));
				travelers.setDestinationAirport(BeanUtils.nullHandler(rakTraveler.getDestinationAirport()));
				travelers.setDob(BeanUtils.nullHandler(BeanUtils.parseDateFormat(rakTraveler.getDob(), "MM/dd/yyyy")));
				travelers.setTitle(BeanUtils.nullHandler(rakTraveler.getTitle()));
				travelers.setFirstName(BeanUtils.nullHandler(rakTraveler.getFirstName()));
				travelers.setLastName(BeanUtils.nullHandler(rakTraveler.getLastName()));
				travelers.setOneWay(rakTraveler.isOneWay());
				travelers.setPremium(BeanUtils.nullHandler(rakTraveler.getPremium()));
				travelers
						.setReturnDate(BeanUtils.nullHandler(BeanUtils.parseDateFormat(rakTraveler.getReturnDate(), "MM/dd/yyyy")));

				paxList.add(travelers);
			}

			flight.setTravelers(paxList);
		}

		travelSchema.setFlight(flight);
		return travelSchema;
	}

	private static String getEmailAddresses(Properties properties) {
		StringBuilder emailAddresses = new StringBuilder();
		for (Object entry : (Collection<Object>) properties.values()) {
			if (emailAddresses.length() == 0) {
				emailAddresses.append(BeanUtils.nullHandler(entry));
			} else {
				emailAddresses.append("," + BeanUtils.nullHandler(entry));
			}
		}

		return emailAddresses.toString();
	}

	private static boolean sendEmailForInsuredData(RakFlightInsuranceInfoDTO rakFlightInsuranceInfoDTO,
			RAKClientConfig rakClientConfig) {

		MessagingServiceBD messagingServiceBD = WSClientModuleUtils.getMessagingServiceBD();
		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setLastName("rak");
		userMessaging.setToAddres(getEmailAddresses(rakClientConfig.getEmailNotifyTo()));
		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);
		String title = "Rak Insurance flight data for flight : " + rakFlightInsuranceInfoDTO.getFlightNo();

		HashMap salesMap = new HashMap();
		salesMap.put("flightData", rakFlightInsuranceInfoDTO);
		salesMap.put("subject", "Rak Insurance Data");
		salesMap.put("title", title);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(salesMap);
		topic.setTopicName("insurance_data_email");
		topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		try {
			messagingServiceBD.sendMessage(messageProfile);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

}