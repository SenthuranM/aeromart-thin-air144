package com.isa.thinair.wsclient.core.service.lms.base.reward;

import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;

public interface LMSRewardWebService {

	public IssueRewardResponse issueVariableRewards(IssueRewardRequest request, String pnr, Map<String, Double> productPoints) throws ModuleException;

	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException;

	public boolean cancelMemberRewards(String pnr, String[] memberRewardIds, String memberAccountId) throws ModuleException;

}
