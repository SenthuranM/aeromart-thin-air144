package com.isa.thinair.wsclient.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.wsclient.api.dto.InsuranceProductConfig;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;

/**
 * Re-factor this to a spring bean
 * 
 */
public class InsuranceDAO {

	public static final String INS_CONTENT_EXTERNAL = "EXTERNAL";

	public static String getAigCountryCode(String airportCode) {

		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = " select aig.aig_country_code from t_airport a, t_station s, t_country c, t_country_aig aig "
				+ " where a.station_code=s.station_code " + " and s.country_code=c.country_code "
				+ " and c.country_code=aig.country_code " + " and a.airport_code='" + airportCode + "'"
				+ " and aig.status='ACT' ";

		return (String) templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String aigCountryCode = null;

				while (rs.next()) {
					aigCountryCode = rs.getString("aig_country_code");

				}

				return aigCountryCode;
			}
		});

	}

	public static InsuranceCountryTO getCountryInformation(String airportCode) {

		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = " select aig.aig_country_code, aig.currency_code, aig.is_europian_country"
				+ " from t_airport a, t_station s, t_country c, t_country_aig aig " + " where a.station_code=s.station_code "
				+ " and s.country_code=c.country_code " + " and c.country_code=aig.country_code " + " and a.airport_code='"
				+ airportCode + "'" + " and aig.status='ACT' ";

		return (InsuranceCountryTO) templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				InsuranceCountryTO aigCountryTO = null;

				while (rs.next()) {
					aigCountryTO = new InsuranceCountryTO();
					aigCountryTO.setCountryCodeAIG(rs.getString("aig_country_code"));
					aigCountryTO.setCurrencyCode(rs.getString("currency_code"));
					if ("Y".equals(rs.getString("is_europian_country"))) {
						aigCountryTO.setEuropianCountry(true);
					} else {
						aigCountryTO.setEuropianCountry(false);
					}
				}
				return aigCountryTO;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, InsuranceProductConfig> getInsuraceProductConfig() {
		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT p.insurance_provider_name provider, ");
		sb.append("   p.insurance_product_code product_code, ");
		sb.append("   pc.display_name, ");
		sb.append("   p.ranking rank , ");
		sb.append("   p.group_id groupid , ");
		sb.append("   p.sub_group_id sub_group_id, ");
		sb.append("   pc.sales_channel_code sale_chanel_code , ");
		sb.append("   pc.display_content external_internal , ");
		sb.append("   pc.insurance_product_config_id ins_product_conf_id ");
		sb.append(" FROM t_insurance_product p , ");
		sb.append("   t_insurance_product_config pc ");
		sb.append(" WHERE pc.insurance_product_id = p.insurance_product_id ");
		sb.append(" AND pc.status                 = 'ACT' ");

		return (HashMap<String, InsuranceProductConfig>) templete.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<String, InsuranceProductConfig> insProductConfigMap = new HashMap<String, InsuranceProductConfig>();

				while (rs.next()) {
					InsuranceProductConfig insuranceProductConfig = new InsuranceProductConfig();
					String provider = rs.getString("provider");
					String providerCode = rs.getString("product_code");
					int salesChannelCode = rs.getInt("sale_chanel_code");
					boolean isExternalContent = INS_CONTENT_EXTERNAL.equals(rs.getString("external_internal")) ? true : false;
					insuranceProductConfig.setProviderName(provider);
					insuranceProductConfig.setProductCode(providerCode);
					insuranceProductConfig.setDisplayName(rs.getString("display_name"));
					insuranceProductConfig.setSaleChannelCode(salesChannelCode);
					insuranceProductConfig.setActive(true);
					insuranceProductConfig.setRank(rs.getInt("rank"));
					insuranceProductConfig.setGroupID(rs.getInt("groupID"));
					insuranceProductConfig.setSubGroupID(rs.getInt("sub_group_id"));
					insuranceProductConfig.setExternalContent(isExternalContent);
					insuranceProductConfig.setInsuranceProductConfigID(rs.getInt("ins_product_conf_id"));
					insProductConfigMap.put(provider + "_" + providerCode + "_" + salesChannelCode, insuranceProductConfig);

				}
				return insProductConfigMap;
			}
		});
	}

	public static String getInsuraceProductCode(Integer insuranceID) {
		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT p.insurance_product_code ins_product_code ");
		sb.append(" FROM t_pnr_insurance i , ");
		sb.append("   t_insurance_product_config c, ");
		sb.append("   t_insurance_product p ");
		sb.append(" WHERE i.ins_id                    =");
		sb.append(insuranceID);
		sb.append(" AND i.insurance_product_config_id = c.insurance_product_config_id ");
		sb.append(" AND p.insurance_product_id        = c.insurance_product_id ");

		return (String) templete.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String aigCountryCode = null;

				while (rs.next()) {
					aigCountryCode = rs.getString("ins_product_code");

				}

				return aigCountryCode;
			}
		});
	}

	public static InsuranceProductConfig getInsuranceProvider(Integer insuranceId) {
		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ip.insurance_provider_name AS provider_name, ");
		sb.append("   ip.insurance_product_code       AS product_code ");
		sb.append(" FROM t_pnr_insurance ri, ");
		sb.append("   t_insurance_product ip, ");
		sb.append("   t_insurance_product_config pc ");
		sb.append(" WHERE ri.ins_id                    ='");
		sb.append(insuranceId).append("'");
		sb.append(" AND ri.insurance_product_config_id = pc.insurance_product_config_id ");
		sb.append(" AND pc.insurance_product_id        = ip.insurance_product_id");

		return (InsuranceProductConfig) templete.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				InsuranceProductConfig insuranceProductConfig = null;
				while (rs.next()) {
					insuranceProductConfig = new InsuranceProductConfig();
					insuranceProductConfig.setProviderName(rs.getString("PROVIDER_NAME"));
					insuranceProductConfig.setProductCode(rs.getString("PRODUCT_CODE"));
					break;

				}

				return insuranceProductConfig;
			}
		});

	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}
}
