package com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.member;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.lms.customfield.CustomFieldSoapStub;
import com.isa.thinair.wsclient.api.lms.customfield.ReqSaveMemberCustomFieldCollection;
import com.isa.thinair.wsclient.api.lms.customfield.RespSaveMemberCustomFieldCollection;
import com.isa.thinair.wsclient.api.lms.member.AddMemberAccountIdReturn;
import com.isa.thinair.wsclient.api.lms.member.AssignMemberToHeadOfHouseholdReturn;
import com.isa.thinair.wsclient.api.lms.member.ClearDependentFromHeadOfHouseholdReturn;
import com.isa.thinair.wsclient.api.lms.member.EnrollMemberReturn;
import com.isa.thinair.wsclient.api.lms.member.FetchMemberCoreReturn;
import com.isa.thinair.wsclient.api.lms.member.FetchMemberStateReturn;
import com.isa.thinair.wsclient.api.lms.member.MemberSoapStub;
import com.isa.thinair.wsclient.api.lms.member.SaveMemberNameReturn;
import com.isa.thinair.wsclient.api.lms.member.SaveMemberPrefCommLanguageReturn;
import com.isa.thinair.wsclient.api.lms.memberactivity.FetchMemberPointBalancesReturn;
import com.isa.thinair.wsclient.api.lms.memberactivity.MemberActivitySoapStub;
import com.isa.thinair.wsclient.api.lms.membercontact.MemberContactSoapStub;
import com.isa.thinair.wsclient.api.lms.membercontact.SaveMemberEmailAddressReturn;
import com.isa.thinair.wsclient.api.lms.membercontact.SaveMemberPhoneNumbersReturn;
import com.isa.thinair.wsclient.api.lms.membersecurity.MemberSecuritySoapStub;
import com.isa.thinair.wsclient.api.lms.membersecurity.SetPasswordReturn;
import com.isa.thinair.wsclient.api.lms.message.MessageSoapStub;
import com.isa.thinair.wsclient.api.lms.message.SendWelcomeMessageReturn;
import com.isa.thinair.wsclient.api.lms.portal.CrossPortalLoginReturn;
import com.isa.thinair.wsclient.api.lms.portal.PortalSoapStub;
import com.isa.thinair.wsclient.api.lms.referrer.ReferrerSoapStub;
import com.isa.thinair.wsclient.api.lms.referrer.ReqProcessReferredBy;
import com.isa.thinair.wsclient.api.lms.referrer.RespProcessReferredBy;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.service.lms.base.member.LMSMemberWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Country;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.common.LMSCommonServiceSmartButtonImpl;

/**
 * LMS Member Module Web Service Implementation
 * 
 * @author rumesh
 * 
 */
public class LMSMemberWebServiceSmartButtonImpl implements DefaultWebserviceClient, LMSMemberWebService {

	private static Log log = LogFactory.getLog(LMSMemberWebServiceSmartButtonImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		return LMSSmartButtonWebServiceInvoker.SERVICE_PROVIDER;
	}

	/**
	 * get status attributes of a loyalty member
	 * 
	 * @param memberAccountId
	 *            This is the email address of the loyalty member
	 * @param sbInternaMemberId
	 *            This is generated ID from SB side. optional value. send 0 if not exists in AA
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public LoyaltyMemberStateDTO getMemberState(String memberAccountId, int sbInternaMemberId) throws ModuleException {
		LoyaltyMemberStateDTO response = null;
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		try {
			FetchMemberStateReturn wsResponse = clientStub.fetchMemberState(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, sbInternaMemberId);

			response = LMSCommonUtil.populateLoyaltyMemberStateDTO(memberAccountId, wsResponse);

			LMSCommonServiceSmartButtonImpl.logUnsuccessfulResponse(response, "LMS API response getMemberState: ");

			Integer returnCode = (response != null) ? response.getReturnCode() : null;
			log.info("Get LMS member state for " + memberAccountId + " response code " + returnCode);

		} catch (RemoteException e) {
			log.error("Error in retrieving member state for " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return response;
	}

	/**
	 * 
	 * Method use to register member in the LMS side
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public String enrollMemberWithEmail(Customer customer) throws ModuleException {

		Integer sbInternalMemberId = null;
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		LmsMember lmsMember = customer.getLMSMemberDetails();
		String memberAccountId = lmsMember.getFfid();

		Date dateOfBirth = lmsMember.getDateOfBirth();
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(dateOfBirth);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		try {
			EnrollMemberReturn enrollMemberReturn = clientStub.enrollMemberWithEmail(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), lmsMember.getFirstName(), null,
					lmsMember.getLastName(), null, month, day, year, lmsMember.getGenderTypeId(),
					lmsMember.getEnrollmentLocExtRef(), 0, memberAccountId);

			Integer responseCode = (enrollMemberReturn != null) ? enrollMemberReturn.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(enrollMemberReturn, responseCode, "Unable to enroll a member "
					+ memberAccountId, "wsclient.loyalty.Enroll.member.failed");

			log.info("LMS Enrollment Response: " + responseCode + " for user " + memberAccountId);

			sbInternalMemberId = enrollMemberReturn.getSBInternalMemberId();
		} catch (RemoteException e) {
			log.error("Error in enrolling loyalty member " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return String.valueOf(sbInternalMemberId);
	}

	/**
	 * Method to link Mashreq account with lms account
	 * 
	 * @param memberAccountId
	 *            This is the loyalty member account ID i.e email address
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException {
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		Integer SBInternalMemberId = Integer.parseInt(memberExternalId);		
		try {
			AddMemberAccountIdReturn response = clientStub.addMemberAccountId(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, SBInternalMemberId,
					idTypeExternalReference, memberAccountIdToAdd);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode,
					"Link Mashreq Bank Account Id with Air Arabia Loyalty Account failed for " + memberAccountId
							+ ". Reponse Code: " + responseCode, "wsclient.loyalty.link.mashreq.failed");

			log.info("Link Mashreq Bank Account Id with Air Arabia Loyalty Account for Mashreq account " + memberAccountId
					+ " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}
		} catch (RemoteException e) {
			log.error("Error in link Mashreq Bank Account Id with Air Arabia Loyalty Account " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}
		return false;
	}

	/**
	 * Get Loyalty member details
	 * 
	 * @param memberAccountId
	 *            This is the email address of the loyalty member
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public LoyaltyMemberCoreDTO getMemberCoreDetails(String memberAccountId) throws ModuleException {
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);
		LoyaltyMemberCoreDTO response = null;

		try {
			FetchMemberCoreReturn wsResponse = clientStub.fetchMemberCore(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0);

			response = LMSCommonUtil.populateLoyaltyMemberCoreDTO(memberAccountId, wsResponse);

			LMSCommonServiceSmartButtonImpl.logUnsuccessfulResponse(response, "LMS API response getMemberCoreDetails: ");

			Integer returnCode = (response != null) ? response.getReturnCode() : null;
			log.info("Get LMS member core details for " + memberAccountId + " response code " + returnCode);

		} catch (RemoteException e) {
			log.error("Error in retrieving member core details of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return response;
	}

	/**
	 * Get point balances of a loyalty member
	 * 
	 * @param memberAccountId
	 *            This is the email address of the loyalty member
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public LoyaltyPointDetailsDTO getMemberPointBalances(String memberAccountId,  String memberExternalId) throws ModuleException {
		MemberActivitySoapStub clientStub = (MemberActivitySoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.MEMBER_ACTIVITY);

		LoyaltyPointDetailsDTO response = null;

		try {
			FetchMemberPointBalancesReturn wsResponse = clientStub.fetchMemberPointBalances(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0);

			response = LMSCommonUtil.populateLoyaltyPointDetailsDTO(memberAccountId, wsResponse);

			LMSCommonServiceSmartButtonImpl.logUnsuccessfulResponse(response, "LMS API response getMemberPointBalances: ");

			Integer returnCode = (response != null) ? response.getReturnCode() : null;
			log.info("Get LMS member points for " + memberAccountId + " response code " + returnCode);

		} catch (RemoteException e) {
			log.error("Error in retrieving available balances of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return response;
	}

	/**
	 * 
	 * Method use to save custom fields of a loyalty member. This method should call after enrolling a new member to LMS
	 * 
	 * @param customer
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean saveMemberCustomFields(Customer customer) throws ModuleException {

		String memberAccountId = customer.getEmailId();

		CustomFieldSoapStub clientStub = (CustomFieldSoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.CUSTOMER_FIELD);
		ReqSaveMemberCustomFieldCollection request = LMSCommonUtil.populateMemberCustomFieldCollection(customer);

		if (request != null) {
			try {
				RespSaveMemberCustomFieldCollection response = clientStub.saveMemberCustomFieldCollection(request);

				int responseCode = response != null ? response.getReturnCode() : -1;
				LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save custom fields failed for " + memberAccountId
						+ ". Reponse Code: " + responseCode, "wsclient.loyalty.custom.field.set.failed");

				log.info("save LMS member custom fields for " + memberAccountId + " response code " + responseCode);

				if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					return true;
				}

			} catch (RemoteException e) {
				log.error("Error in saving custom fields of " + memberAccountId, e);
				throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
			}
		}
		return false;
	}

	/**
	 * Method sets the password of a loyalty member which uses to log into LMS web portal
	 * 
	 * @param memberAccountId
	 *            This is the loyalty member account ID i.e email address
	 * @param password
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException {

		MemberSecuritySoapStub clientStub = (MemberSecuritySoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.MEMBER_SECURITY);

		try {
			SetPasswordReturn response = clientStub.setPassword(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(),
					memberAccountId, 0, password);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save password failed for " + memberAccountId
					+ ". Reponse Code: " + responseCode, "wsclient.loyalty.password.set.failed");

			log.info("save LMS member password for " + memberAccountId + " response code " + responseCode);

			if (response != null && response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}

		} catch (RemoteException e) {
			log.error("Error in saving password of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}

	/**
	 * Method use to save/update email address of a loyalty member.
	 * 
	 * @param memberAccountId
	 *            This is the loyalty member account ID i.e email address
	 * @param emailAddress
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException {

		MemberContactSoapStub clientStub = (MemberContactSoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.MEMBER_CONTACT);

		try {
			SaveMemberEmailAddressReturn response = clientStub.saveMemberEmailAddress(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0, emailAddress,
					LMSConstants.MemberContactField.VALID);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update Email address failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.email.set.failed");

			log.info("save LMS member email address for " + memberAccountId + " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}
		} catch (RemoteException e) {
			log.error("Error in save/update email address of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}
	
	
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException {

		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		try {
			AssignMemberToHeadOfHouseholdReturn response = clientStub.assignMemberToHeadOfHousehold(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), headOfHosueHold, -1, memberAccountId, -1);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update head of house failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.headofhouse.set.failed");

			log.info("save LMS member head of house address for " + memberAccountId + " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}

		} catch (RemoteException e) {
			log.error("Error in save/update head of household of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}
	
	
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException {

		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		try {
			ClearDependentFromHeadOfHouseholdReturn response = clientStub.clearDependentFromHeadOfHousehold(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, -1);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "removal of head of house failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.headofhouse.removal.failed");

			log.info("Remove LMS member head of house address for " + memberAccountId + " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}

		} catch (RemoteException e) {
			log.error("Error in removeal of head of household of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}
	

	/**
	 * Method use to save/update phone number and/or mobile number of a loyalty member in a single request.
	 * 
	 * @param memberAccountId
	 *            This is the loyalty member account ID i.e email address
	 * @param phoneNumer
	 * @param mobileNumber
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber) throws ModuleException {
		MemberContactSoapStub clientStub = (MemberContactSoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.MEMBER_CONTACT);

		try {

			SaveMemberPhoneNumbersReturn response;
			response = clientStub.saveMemberPhoneNumbers(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(),
					memberAccountId, 0, phoneNumber, null, LMSConstants.MemberContactField.VALID, mobileNumber,
					LMSConstants.MemberContactField.VALID);

			int responseCode = -1;
			if (phoneNumber != null && mobileNumber != null) {
				if (response != null && response.getAccountPhoneNumberReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK
						&& response.getMobilePhoneNumberReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					return true;
				} else if (response != null
						&& response.getAccountPhoneNumberReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					responseCode = response != null ? response.getAccountPhoneNumberReturnCode() : -1;
					String erroMsgCode = responseCode != LMSConstants.WSReponse.DUPLICATE_PHONE_NUM ? "wsclient.loyalty.phone.set.failed"
							: "wsclient.loyalty.duplicate.phone.number";
					LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update phone number failed for  "
							+ memberAccountId + ". Reponse Code: " + responseCode, erroMsgCode);
				} else if (response != null
						&& response.getMobilePhoneNumberReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					responseCode = response != null ? response.getMobilePhoneNumberReturnCode() : -1;
					String erroMsgCode = responseCode != LMSConstants.WSReponse.DUPLICATE_PHONE_NUM ? "wsclient.loyalty.mobile.set.failed"
							: "wsclient.loyalty.duplicate.phone.number";
					LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update mobile number failed for  "
							+ memberAccountId + ". Reponse Code: " + responseCode, erroMsgCode);
				}
			} else if (phoneNumber != null) {
				if (response != null && response.getAccountPhoneNumberReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					return true;
				} else {
					responseCode = response != null ? response.getAccountPhoneNumberReturnCode() : -1;
					String erroMsgCode = responseCode != LMSConstants.WSReponse.DUPLICATE_PHONE_NUM ? "wsclient.loyalty.phone.set.failed"
							: "wsclient.loyalty.duplicate.phone.number";
					LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update phone number failed for  "
							+ memberAccountId + ". Reponse Code: " + responseCode, erroMsgCode);
				}
			} else if (mobileNumber != null) {
				if (response != null && response.getMobilePhoneNumberReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
					return true;
				} else {
					responseCode = response != null ? response.getMobilePhoneNumberReturnCode() : -1;
					String erroMsgCode = responseCode != LMSConstants.WSReponse.DUPLICATE_PHONE_NUM ? "wsclient.loyalty.mobile.set.failed"
							: "wsclient.loyalty.duplicate.phone.number";
					LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save/Update mobile number failed for  "
							+ memberAccountId + ". Reponse Code: " + responseCode, erroMsgCode);
				}
			}

			log.info("save LMS member mobile/phone number for " + memberAccountId + " response code " + responseCode);

		} catch (RemoteException e) {
			log.error("Error in save/update phone number of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}

	/**
	 * Method to save preferred communication language of loyalty member
	 * 
	 * @param memberAccountId
	 *            This is the loyalty member account ID i.e email address
	 * @param languageCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode) throws ModuleException {
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		try {
			SaveMemberPrefCommLanguageReturn response = clientStub.saveMemberPrefCommLanguage(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0, languageCode);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save member communication language failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.pref.lang.set.failed");

			log.info("save LMS member preferred language for " + memberAccountId + " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}
		} catch (RemoteException e) {
			log.error("Error in save member communication language of " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}
		return false;
	}

	/**
	 * Method to save member who introduce to Loyalty Program
	 * 
	 * @param memberAccountId
	 *            This is the new loyalty member account ID i.e email address
	 * @param referredByMemberAccountId
	 *            This is the loyalty member who introduced the new member into loyalty program
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException {

		ReferrerSoapStub clientStub = (ReferrerSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.REFERRER);
		ReqProcessReferredBy reqProcessReferredBy = LMSCommonUtil.populateReqProcessReferredBy(memberAccountId,
				referredByMemberAccountId);

		try {
			RespProcessReferredBy response = clientStub.processReferredBy(reqProcessReferredBy);

			int responseCode = response != null ? response.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "Save LoyaltySystemReferredByUser failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.referred.by.set.failed");

			log.info("save LMS member referred by user for " + memberAccountId + " response code " + responseCode);

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return true;
			}
		} catch (RemoteException e) {
			log.error("Error in save LoyaltySystemReferredByUser for " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return false;
	}

	/**
	 * Method to get the cross portal login URL
	 * 
	 * @param memberAccountId
	 *            This is the new loyalty member account ID i.e email address
	 * @param password
	 * @param menuItemIntRef
	 *            Internal reference menu link should retrieve. This is optional
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef) throws ModuleException {

		PortalSoapStub clientStub = (PortalSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.PORTAL);

		try {
			CrossPortalLoginReturn response = clientStub.crossPortalLogin(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0, password,
					LMSCommonUtil.getMenuItemExternalReference(menuItemIntRef));

			int responseCode = response != null ? response.getReturnCode() : -1;

			log.info("LMS member cross portal login for " + memberAccountId + " response code " + responseCode);

			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(response, responseCode, "LMS Cross portal log in failed for  "
					+ memberAccountId + ". Reponse Code: " + responseCode, "wsclient.loyalty.cross.portal.login.failed");

			if (response.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				return response.getCrossLoginLink();
			}
		} catch (RemoteException e) {
			log.error("Error in getting cross portal logging URL for " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return null;
	}
	@Override
	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException {

		MessageSoapStub clientStub = (MessageSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MESSAGE);
		boolean welcomeResponse = false;
		try {
			SendWelcomeMessageReturn response = clientStub.sendWelcomeMessage(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0);

			Integer returnCode = (response != null) ? response.getReturnCode() : null;
			log.info("Send LMS member welcome message for " + memberAccountId + " response code " + returnCode);

			welcomeResponse = (returnCode == LMSConstants.WSReponse.RESPONSE_CODE_OK) ? true : false;

		} catch (RemoteException e) {
			log.error("Error in sending member welcome message for " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return welcomeResponse;
	}
	@Override
	public int saveMemberName(Customer customer) throws ModuleException {

		Integer sbInternalMemberId = null;
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);

		LmsMember lmsMember = customer.getLMSMemberDetails();
		String memberAccountId = lmsMember.getFfid();
		int SBInternalMemberId = lmsMember.getLmsMemberId();

		try {
			SaveMemberNameReturn saveMemberNameReturn = clientStub.saveMemberName(
					LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, SBInternalMemberId,
					lmsMember.getGenderTypeId(), lmsMember.getFirstName(), null, lmsMember.getLastName(), null);

			Integer responseCode = (saveMemberNameReturn != null) ? saveMemberNameReturn.getReturnCode() : -1;
			LMSCommonServiceSmartButtonImpl.wrapErrorResponse(saveMemberNameReturn, responseCode, "Unable to save member name "
					+ memberAccountId, "wsclient.loyalty.save.name.member.failed");

			log.info("LMS Member Name Response: " + responseCode + " for user " + memberAccountId);

			sbInternalMemberId = SBInternalMemberId;
		} catch (RemoteException e) {
			log.error("Error in save loyalty member name " + memberAccountId, e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}

		return sbInternalMemberId;
	}

	@Override
	public MemberRemoteLoginResponseDTO memberLogin(String userName,String password) throws ModuleException {
		return null;
	}

	@Override
	public List<Country> getCountriesList() throws ModuleException {
		return null;
	}

	@Override
	public void updateMember(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
	}

	@Override
	public MemberFetchDataResponse getMemberFromEmail(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
}
