package com.isa.thinair.wsclient.core.service.aig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.core.service.InsuranceTemplate;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;
import com.isaaviation.thinair.webservices.api.airinsurance.Insured;
import com.isaaviation.thinair.webservices.api.airinsurance.TINSXMLDATA;

public class AigBL implements InsuranceTemplate {

	private static final Log log = LogFactory.getLog(AigBL.class);

	public static final String SERVICE_PROVIDER = "AIG";

	public enum RequestType {
		SELL, QUOTE, RESELL
	};



	
	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}
	
	@Override
	public List<InsuranceResponse> quote(IInsuranceRequest iQuoteRequest)
			throws ModuleException {
		log.debug("Begin quoteInsurancePolicy");

		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		InsuranceResponse insResponse = null;
		ModuleException mex = null;
		try {
			InsuranceCountryTO aigCountryTO = InsuranceUtil.getCountryTO(((InsuranceRequestAssembler) iQuoteRequest).getInsureSegment()
					.getFromAirportCode());
			TINSXMLDATA tinsReq = AIGUtil.prepareAIGObject(RequestType.QUOTE, iQuoteRequest, aigCountryTO);

			TINSXMLDATA tinsRes = AIGProxy.callService(tinsReq);

			BigDecimal quotedTotalPremium = tinsRes.getSegment().getPolicyOut() == null ? null : new BigDecimal(tinsRes
					.getSegment().getPolicyOut().getTotalPremium());
			// Haider 16Mar09 get the tax amount
			BigDecimal quotedTotalTaxAmount = tinsRes.getSegment().getPolicyOut() == null ? null : new BigDecimal(tinsRes
					.getSegment().getPolicyOut().getTotalTaxAmt());

			// Haider 31Mar09 get the net amount
			Collection<Insured> col = tinsRes.getSegment().getInsured();
			BigDecimal quotedTotalNetAmount = new BigDecimal(0);
			double netAmount = 0.0;
			String premiumAmt = null;
			if (col != null) {
				Iterator<Insured> iter = col.iterator();
				Insured ins = null;
				while (iter.hasNext()) {
					ins = iter.next();
					premiumAmt = ins.getTransactionPremAmt();
					netAmount += Double.valueOf(premiumAmt).doubleValue();
				}
				quotedTotalNetAmount = new BigDecimal(netAmount);
			}

			BigDecimal[] baseCurrencyAmounts = null;

			if (quotedTotalPremium != null) {
				insResponse = new InsuranceResponse();
				insResponse.setSuccess(true);
				insResponse.setErrorCode(tinsRes.getHeader().getErrorCode());
				insResponse.setErrorMessage(tinsRes.getHeader().getErrorMessage());
				insResponse.setMessageId(tinsRes.getHeader().getMessageId());
				insResponse.setTotalPremiumAmount(baseCurrencyAmounts[0]);
				insResponse.setQuotedTotalPremiumAmount(quotedTotalPremium);
				insResponse.setQuotedCurrencyCode(aigCountryTO.getCurrencyCode());
				insResponse.setEuropianCountry(aigCountryTO.isEuropianCountry());

				baseCurrencyAmounts = AIGUtil.getBaseCurrencyAmount(aigCountryTO.getCurrencyCode(), new BigDecimal[] {
						quotedTotalPremium, quotedTotalNetAmount, quotedTotalTaxAmount });

				insResponse.setAmount(baseCurrencyAmounts[0]);
				insResponse.setQuotedAmount(quotedTotalPremium);

				insResponse.setNetAmount(baseCurrencyAmounts[1]);
				insResponse.setQuotedNetAmount(quotedTotalNetAmount);

				insResponse.setTaxAmount(baseCurrencyAmounts[2]);
				insResponse.setQuotedTaxAmout(quotedTotalTaxAmount);
			} else {
				insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
						.getErrorMessage(), tinsRes.getHeader().getMessageId(), null, quotedTotalPremium,
						aigCountryTO.getCurrencyCode(), aigCountryTO.isEuropianCountry());

				if (log.isDebugEnabled()) {
					log.debug("AIG Says - I cannot give you a insurance quote. Because of the following reason.  "
							+ tinsRes.getSegment().getErrorMessage().toString());
				}
			}
			lstInsuranceResponses.add(insResponse);

		} catch (ModuleException ex) {
			mex = ex;
		} catch (Throwable e) {
			log.error("quoteInsurancePolicy failed", e);
			mex = new ModuleException("wsclient.aig.general");
		} finally {
			if (insResponse == null) {
				insResponse = new InsuranceResponse(false, mex.getExceptionCode(), mex.getMessageString(), null, null, null,
						null, false);
				lstInsuranceResponses.add(insResponse);
			}
		}
		log.debug("End quoteInsurancePolicy");
		return lstInsuranceResponses;
	}

	@Override
	public List<InsuranceResponse> sell(List<IInsuranceRequest> iRequests)
			throws ModuleException {
		log.debug("Begin sellInsurancePolicy");

		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		TINSXMLDATA tinsRes = null;
		InsuranceResponse insResponse = null;
		String policyCode = null;
		BigDecimal quotedTotalPremiumAmount = null;
		BigDecimal totalPremiumAmount = null;

		InsuranceCountryTO aigCountryTO = AIGUtil.getCountryTO(((InsuranceRequestAssembler) iRequests).getInsureSegment()
				.getFromAirportCode());
		TINSXMLDATA tinsReq = AIGUtil.prepareAIGObject(RequestType.SELL, iRequests.get(0), aigCountryTO);
		tinsRes = AIGProxy.callService(tinsReq);

		try {
			// if success
			if (tinsRes.getSegment().getPolicyOut() != null && "0".equals(tinsRes.getHeader().getErrorCode())) {

				quotedTotalPremiumAmount = new BigDecimal(tinsRes.getSegment().getPolicyOut().getTotalPremium());
				totalPremiumAmount = AIGUtil.getBaseCurrencyAmount(aigCountryTO.getCurrencyCode(), quotedTotalPremiumAmount)[0];
				policyCode = tinsRes.getSegment().getPolicyOut().getPolicyNumber();

				// check segment level - success
				if ("0".equals(tinsRes.getSegment().getErrorCode())) {

					insResponse = new InsuranceResponse(true, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else { // segment level - fail
					insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				}
				insResponse.setPolicyCode(policyCode);
			}

			// failed
			if (tinsRes.getSegment().getPolicyOut() == null) {
				// fail - in header level
				if ((!"0".equals(tinsRes.getHeader().getErrorCode()))) {
					insResponse = new InsuranceResponse(false, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else {
					insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				}
			}
		} catch (Exception e) {
			// if a valid WS response received from AIG , send it
			quotedTotalPremiumAmount = ((InsuranceRequestAssembler) iRequests.get(0)).getTotalPremiumAmount();
			if (quotedTotalPremiumAmount != null) {
				totalPremiumAmount = AIGUtil.getBaseCurrencyAmount(aigCountryTO.getCurrencyCode(), quotedTotalPremiumAmount)[0];
			}
			if (tinsRes != null && tinsRes.getHeader() != null) {
				// if there is an error code in the header show it, else show the error in the segment area.
				if (tinsRes.getHeader().getErrorCode() != null && tinsRes.getHeader().getErrorCode().length() > 0) {

					insResponse = new InsuranceResponse(true, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else if (tinsRes.getSegment() != null && tinsRes.getSegment().getErrorCode() != null) {
					insResponse = new InsuranceResponse(true, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else {
					log.error("Neither a Header nor Segment Displayed the Error of AIG Response");
					throw new ModuleException("wsclient.aig.unstruct.response");
				}
			} else {
				log.error("sellInsurancePolicy failed", e);
				throw new ModuleException("wsclient.aig.unstruct.response");
			}
		}
		log.debug("End sellInsurancePolicy");
		lstInsuranceResponses.add(insResponse);
		return lstInsuranceResponses;
	}
	
	/**
	 * 
	 * @param iRequest
	 * @return
	 * @throws ModuleException
	 */
	public InsuranceResponse resSellInsurancePolicy(IInsuranceRequest iRequest) throws ModuleException {
		log.debug("Begin resellInsurancePolicy");

		InsuranceCountryTO aigCountryTO = AIGUtil.getCountryTO(((InsuranceRequestAssembler) iRequest).getInsureSegment()
				.getFromAirportCode());
		TINSXMLDATA tinsReq = AIGUtil.prepareAIGObject(RequestType.RESELL, iRequest, aigCountryTO);
		TINSXMLDATA tinsRes = AIGProxy.callService(tinsReq);
		InsuranceResponse insResponse = null;
		BigDecimal quotedTotalPremiumAmount = null;
		BigDecimal totalPremiumAmount = null;

		try {
			quotedTotalPremiumAmount = tinsRes.getSegment().getPolicyOut() == null ? null : new BigDecimal(tinsRes.getSegment()
					.getPolicyOut().getTotalPremium());
			String policyCode = tinsRes.getSegment().getPolicyOut().getPolicyNumber();

			// if success
			if (tinsRes.getSegment().getPolicyOut() != null && "0".equals(tinsRes.getHeader().getErrorCode())) {

				quotedTotalPremiumAmount = new BigDecimal(tinsRes.getSegment().getPolicyOut().getTotalPremium());
				totalPremiumAmount = AIGUtil.getBaseCurrencyAmount(aigCountryTO.getCurrencyCode(), quotedTotalPremiumAmount)[0];
				policyCode = tinsRes.getSegment().getPolicyOut().getPolicyNumber();

				// check segment level - success
				if ("0".equals(tinsRes.getSegment().getErrorCode())) {

					insResponse = new InsuranceResponse(true, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else {// segment level - fail
					insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				}
				insResponse.setPolicyCode(policyCode);

			}

			// failed
			if (tinsRes.getSegment().getPolicyOut() == null) {
				// fail - in header level
				if ((!"0".equals(tinsRes.getHeader().getErrorCode()))) {
					insResponse = new InsuranceResponse(false, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else {
					insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				}
			}
			insResponse.setPolicyCode(policyCode);
		} catch (Exception e) {
			// if a valid ws response received from AIG , send it
			if (tinsRes != null && tinsRes.getHeader() != null) {
				quotedTotalPremiumAmount = ((InsuranceRequestAssembler) iRequest).getTotalPremiumAmount();
				if (quotedTotalPremiumAmount != null) {
					totalPremiumAmount = AIGUtil.getBaseCurrencyAmount(aigCountryTO.getCurrencyCode(), quotedTotalPremiumAmount)[0];
				}
				// if there is an error code in the header show it, else show the error in the segment area.
				if (tinsRes.getHeader().getErrorCode() != null && !"0".equals(tinsRes.getHeader().getErrorCode())) {
					insResponse = new InsuranceResponse(false, tinsRes.getHeader().getErrorCode(), tinsRes.getHeader()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else if (tinsRes.getSegment() != null && tinsRes.getSegment().getErrorCode() != null) {
					insResponse = new InsuranceResponse(false, tinsRes.getSegment().getErrorCode(), tinsRes.getSegment()
							.getErrorMessage(), tinsRes.getHeader().getMessageId(), totalPremiumAmount, quotedTotalPremiumAmount,
							aigCountryTO.getCurrencyCode());
				} else {
					log.error("Neither a Header nor Segment Displayed the Error of AIG Response");
					throw new ModuleException("wsclient.aig.unstruct.response");
				}
			} else {
				log.error("resellInsurancePolicy failed", e);
				throw new ModuleException("wsclient.aig.unstruct.response");
			}
		}
		log.debug("End resellInsurancePolicy");
		return insResponse;
	}

	
	
	
}
