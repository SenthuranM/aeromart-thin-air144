package com.isa.thinair.wsclient.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.core.service.db.DBWebServicesImpl;
import com.isa.thinair.wsclient.core.service.dib.DIBWebServicesImpl;
import com.isa.thinair.wsclient.core.service.ebi.EBIWebervicesImpl;
import com.isa.thinair.wsclient.core.service.ep.EPWebServicesImpl;

public class BankWebServicesInvoker {

	private static Log log = LogFactory.getLog(BankWebServicesInvoker.class);

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at Banks.
	 * 
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 */
	public void requestDailyTnxReconcilation(Date startTimestamp, Date endTimestamp, String serviceProvider)
			throws ModuleException {
		log.debug("BEGIN:: requestDailyTnxReconcilation(startTimestamp,endTimestamp)");

		if (serviceProvider != null && ExternalWebServiceInvoker.SERVICE_PROVIDER_EBI.equals(serviceProvider)) {
			new EBIWebervicesImpl().requestDailyTnxReconcilation(startTimestamp, endTimestamp);
		} else if (serviceProvider != null && ExternalWebServiceInvoker.SERVICE_PROVIDER_DIB.equals(serviceProvider)) {
			new DIBWebServicesImpl().requestDailyTnxReconcilation(startTimestamp, endTimestamp);
		} else if (serviceProvider != null && ExternalWebServiceInvoker.SERVICE_PROVIDER_DB.equals(serviceProvider)) {
			new DBWebServicesImpl().requestDailyTnxReconcilation(startTimestamp, endTimestamp);
		} else if (serviceProvider != null && ExternalWebServiceInvoker.SERVICE_PROVIDER_EP.equals(serviceProvider)) {
			new EPWebServicesImpl().requestDailyTnxReconcilation(startTimestamp, endTimestamp);
		}
		log.debug("END:: requestDailyTnxReconcilation(startTimestamp,endTimestamp)");
	}

}
