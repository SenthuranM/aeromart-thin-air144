/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.wsclient.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * 
 * @isa.module.service-interface module-name="wsclient" description="WS Client APIs"
 */
public class WSClientService extends DefaultModule {
}