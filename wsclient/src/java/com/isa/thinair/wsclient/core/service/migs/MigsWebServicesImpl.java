package com.isa.thinair.wsclient.core.service.migs;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;

public class MigsWebServicesImpl {

	private static Log log = LogFactory.getLog(MigsWebServicesImpl.class);

	public String postRefundRequest(MIGSRequest migsRequest) throws ModuleException {
		org.tempuri.DirectDebit_WS.TesteSettlement.ESettlementSoapStub serviceStub = getClientStub();
		try {
			return serviceStub.postRefundRequest(migsRequest.getRefundRequest(), migsRequest.getUser(),
					migsRequest.getPassword(), migsRequest.getRefNo());
		} catch (RemoteException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}
	}

	private org.tempuri.DirectDebit_WS.TesteSettlement.ESettlementSoapStub getClientStub() throws ModuleException {
		try {
			URL wsdlUrl = new URL("http://test.cbdonline.ae/directdebit_ws/eSettlement.asmx?WSDL");
			QName qName = new QName("http://tempuri.org/DirectDebit_WS/TesteSettlement", "eSettlement");
			ServiceFactory serviceFactory = new ServiceFactory();
			javax.xml.rpc.Service paymentService = serviceFactory.createService(qName);
			return new org.tempuri.DirectDebit_WS.TesteSettlement.ESettlementSoapStub(wsdlUrl, paymentService);
		} catch (MalformedURLException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		} catch (ServiceException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		} catch (AxisFault e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}
	}

	public String verifyTransaction(MIGSRequest refundReuest) throws ModuleException {
		org.tempuri.DirectDebit_WS.TesteSettlement.ESettlementSoapStub serviceStub = getClientStub();
		try {
			return serviceStub.getTransResult(refundReuest.getQueryRequest(), refundReuest.getUser(), refundReuest.getPassword());
		} catch (RemoteException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}
	}
}
