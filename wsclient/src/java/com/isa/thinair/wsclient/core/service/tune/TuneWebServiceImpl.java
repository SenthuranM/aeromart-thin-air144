package com.isa.thinair.wsclient.core.service.tune;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGatewayProxy;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.InsuranceTemplate;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;

/**
 * Tune insurance Web Service Implemantaion.
 * 
 * @author Primal Suaris.
 * @date 22-01-2014
 * 
 */

public class TuneWebServiceImpl implements InsuranceTemplate {

	public static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_TUNE;

	public static final String QUOTE_CURRENCY = "USD";

	private static String SUCCESS = "CONFIRMED";

	private String defaultPlanInfoDescription;

	private String pushPlanInfoDescription;

	private static Log log = LogFactory.getLog(TuneWebServiceImpl.class);

	private List<InsuranceResponse> avilableInsuranceQuotes;

	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	public enum TunePlanType {
		DEFAULT, PUSH, RIDER
	};

	@Override
	public List<InsuranceResponse> quote(IInsuranceRequest iRequest) throws ModuleException {

		log.debug("Begin TUNE - quote");

		ModuleException mex = null;

		avilableInsuranceQuotes = new ArrayList<InsuranceResponse>();
		OutputResponseAvailablePlansWithRiders avilPlanWithRiderPlans = null;

		try {
			TuneUtil tuneUtil = new TuneUtil();
			InputRequest tuneRequest = tuneUtil.prepareQuoteRequest(iRequest);

			try {
				avilPlanWithRiderPlans = new ZEUSTravelInsuranceGatewayProxy().getAvailablePlansWithRiders(tuneRequest);

				if (avilPlanWithRiderPlans != null && avilPlanWithRiderPlans.getAvailablePlans() != null
						&& avilPlanWithRiderPlans.getAvailablePlans().length > 0) {
					String errorMSGAvilPlan = "0".equalsIgnoreCase(avilPlanWithRiderPlans.getErrorCode().trim())
							|| (avilPlanWithRiderPlans.getErrorMessage() != null && avilPlanWithRiderPlans.getErrorMessage()
									.isEmpty()) ? "OK" : avilPlanWithRiderPlans.getErrorMessage();
					String errorCodeAvilPlan = avilPlanWithRiderPlans.getErrorCode();

					tuneUtil.updateAvailableInsuranceQuotes(avilableInsuranceQuotes, avilPlanWithRiderPlans.getAvailablePlans(),
							iRequest, errorCodeAvilPlan, errorMSGAvilPlan, false);

				} else {
					if (log.isDebugEnabled()) {
						log.debug("No plans found for TUNE insurance Default/Push Plan");
					}
				}

				if (avilPlanWithRiderPlans != null && avilPlanWithRiderPlans.getAvailableRiderPlans() != null
						&& avilPlanWithRiderPlans.getAvailableRiderPlans().length > 0) {
					String errorMSGRiderPlan = "0".equalsIgnoreCase(avilPlanWithRiderPlans.getErrorCode().trim())
							|| (avilPlanWithRiderPlans.getErrorMessage() != null && avilPlanWithRiderPlans.getErrorMessage()
									.isEmpty()) ? "OK" : avilPlanWithRiderPlans.getErrorMessage();
					String errorCodeRiderPlan = avilPlanWithRiderPlans.getErrorCode();

					tuneUtil.updateAvailableInsuranceQuotes(avilableInsuranceQuotes,
							avilPlanWithRiderPlans.getAvailableRiderPlans(), iRequest, errorCodeRiderPlan, errorMSGRiderPlan,
							true);

				} else {
					if (log.isDebugEnabled()) {
						log.debug("TNo plans found for TUNE insurance Rider Plan");
					}
				}
			} catch (RemoteException ex) {
				log.error("quoteInsurancePolicy failed", ex);
				mex = new ModuleException("wsclient.ins.general");
			} finally {
				InsuranceUtil.log(log, SERVICE_PROVIDER, tuneRequest, avilPlanWithRiderPlans);
			}
		} catch (DatatypeConfigurationException ex) {
			log.error("quoteInsurancePolicy failed", ex);
			mex = new ModuleException("wsclient.ins.general");
		}
		log.debug("End TUNE - quote");

		return avilableInsuranceQuotes;

	}

	@Override
	public List<InsuranceResponse> sell(List<IInsuranceRequest> iRequests) throws ModuleException {
		log.debug("Begin TUNE - sell");

		for (IInsuranceRequest iRequest : iRequests) {
			log.info("###############################################################################");
			log.info("INSURANCE SELL REQUEST");
			InsuranceRequestAssembler insuranceRequest = (InsuranceRequestAssembler) iRequest;
			log.info("Insured PNR := " + insuranceRequest.getPnr());
			log.info("Insurance plan code := " + insuranceRequest.getPlanCode());
			log.info("Insurance premium amount := " + insuranceRequest.getQuotedTotalPremiumAmount() + " "
					+ insuranceRequest.getCurrency());
			log.info("###############################################################################");

		}

		List<InsuranceResponse> insResponses = new ArrayList<InsuranceResponse>();

		InputRequestWithRiders selPolicyReq = null;
		ConfirmPurchaseResponseWithRiders confirmPurchseResponse = null;

		try {
			TuneUtil tuneUtil = new TuneUtil();
			selPolicyReq = tuneUtil.prepareSellWithRiderPolicyRequest(iRequests);
			confirmPurchseResponse = new ZEUSTravelInsuranceGatewayProxy().confirmPurchaseWithRiders(selPolicyReq);

			InsuranceResponse insResponse = null;

			for (IInsuranceRequest iRequest : iRequests) {

				InsuranceRequestAssembler iRequestAssembler = (InsuranceRequestAssembler) iRequest;
				if (confirmPurchseResponse != null
						&& SUCCESS.equalsIgnoreCase(confirmPurchseResponse.getProposalState().getValue())) {
					// unfortunately cannot validate <quoted amount> against <sold amount> as API is not supporting
					insResponse = new InsuranceResponse(true, "0", "OK", confirmPurchseResponse.getItineraryID(),
							iRequestAssembler.getTotalPremiumAmount(), iRequestAssembler.getQuotedTotalPremiumAmount(),
							AppSysParamsUtil.getBaseCurrency());
					insResponse.setPolicyCode(confirmPurchseResponse.getPolicyNo());
				} else {
					insResponse = new InsuranceResponse(false, "Error", "Insurance Is Not Available", "",
							AccelAeroCalculator.getDefaultBigDecimalZero(), AccelAeroCalculator.getDefaultBigDecimalZero(),
							AppSysParamsUtil.getBaseCurrency());
				}

				insResponse.setInsuranceRefId(iRequestAssembler.getInsuranceId());
				insResponses.add(insResponse);
				if ("0".equals(insResponse.getErrorCode())) {
					log.info("###############################################################################");
					log.info("INSURANCE SELL RESPONSE RECIEVED");
					log.info("Insurance policy code := " + insResponse.getPolicyCode());
					log.info("Insurance response message ID:= " + insResponse.getMessageId());
					log.info("###############################################################################");
				}
			}

		} catch (RemoteException re) {
			log.error("sellTuneInsurancePolicy failed", re);
			throw new ModuleException("wsclient.ins.unstruct.response");
		} catch (Exception e) {
			log.error("sellTuneInsurancePolicy failed", e);
			throw new ModuleException("wsclient.ins.unstruct.response");
		} finally {
			InsuranceUtil.log(log, SERVICE_PROVIDER + "-sell ", selPolicyReq, confirmPurchseResponse);
		}

		log.debug("End TUNE -sell");
		return insResponses;
	}

	public String getDefaultPlanInfoDescription() {
		return defaultPlanInfoDescription;
	}

	public void setDefaultPlanInfoDescription(String defaultPlanInfoDescription) {
		this.defaultPlanInfoDescription = defaultPlanInfoDescription;
	}

	public String getPushPlanInfoDescription() {
		return pushPlanInfoDescription;
	}

	public void setPushPlanInfoDescription(String pushPlanInfoDescription) {
		this.pushPlanInfoDescription = pushPlanInfoDescription;
	}

	public List<InsuranceResponse> getAvilableInsuranceQuotes() {
		return avilableInsuranceQuotes;
	}

	public void setAvilableInsuranceQuotes(List<InsuranceResponse> avilableInsuranceQuotes) {
		this.avilableInsuranceQuotes = avilableInsuranceQuotes;
	}

}
