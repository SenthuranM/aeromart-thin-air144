package com.isa.thinair.wsclient.core.service.typeb;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.typeb.AATypeBResWebServices;
import com.isa.thinair.wsclient.core.security.WSClientSecurityHandler;

/**
 * @author Manoj Dhanushka
 */
public class TypeBWebServiceInvoker {

	private static final Log log = LogFactory.getLog(TypeBWebServiceInvoker.class);

	private static final String defaultNamespaceURI = "http://www.opentravel.org/OTA/2003/05";

	public static AATypeBResWebServices getTypeBExposedWS(String hostname) throws ModuleException {

		String typeBWsURL = hostname + WSClientModuleUtils.getModuleConfig().getTypeBWsUrl();
		String typeBWsUserName = WSClientModuleUtils.getModuleConfig().getTypeBWsUserName();
		String typeBWsPassword = WSClientModuleUtils.getModuleConfig().getTypeBWsPassword();
		
		log.info("TypeB Exposed Services URL = " + typeBWsURL);
		AATypeBResWebServices aaTypeBResWebServices = null;

		try {
			URL wsdlURL = new URL(typeBWsURL);
			QName qname = new QName(defaultNamespaceURI, "AATypeBResWebServices");
			Service service = Service.create(wsdlURL, qname);
			aaTypeBResWebServices = service.getPort(AATypeBResWebServices.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new WSClientSecurityHandler(typeBWsUserName, typeBWsPassword));
			((BindingProvider) aaTypeBResWebServices).getBinding().setHandlerChain(handlerChain);

		} catch (Exception e) {
			log.error("wsclient.serviceinvocation.failed", e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}
		return aaTypeBResWebServices;
	}
}
