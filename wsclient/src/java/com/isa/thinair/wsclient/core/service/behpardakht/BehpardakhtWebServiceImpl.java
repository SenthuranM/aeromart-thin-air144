package com.isa.thinair.wsclient.core.service.behpardakht;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse;
import com.isa.thinair.wsclient.api.behpardakht.IPaymentGateway;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.util.SOAPLoggingHandler;


public class BehpardakhtWebServiceImpl {

	private static Log log = LogFactory.getLog(BehpardakhtWebServiceImpl.class);
	
	com.isa.thinair.wsclient.core.config.BehpardakhtClientConfig behpardakhtClientConfig = WSClientModuleUtils.getModuleConfig()
			.getBehpardakhtClientConfig();
	
	/**
	 * Payment verification method for Behpardakht Payment gateway
	 * 
	 * @param verifyRequest
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse verifyRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtVerifyRequest verifyRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::verifyTransaction()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		try {

			IPaymentGateway serviceStub = getClientStub();

			try {
				String verifyResponse = serviceStub.bpVerifyRequest(verifyRequest.getTerminalId(), verifyRequest.getUserName(), verifyRequest.getUserPassword() , verifyRequest.getOrderId() , verifyRequest.getSaleOrderId() , verifyRequest.getSaleReferenceId());

				response.setResponse(verifyResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::verifyTransaction()] End");
		return response;
	}
	
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse payRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtPayRequest payRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::payRequest()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		try {

			IPaymentGateway serviceStub = getClientStub();

			try {
				String verifyResponse = serviceStub.bpPayRequest(payRequest.getTerminalId(), payRequest.getUserName() , payRequest.getUserPassword() , payRequest.getOrderId() , payRequest.getAmount() , payRequest.getLocalDate() , payRequest.getLocalTime() , payRequest.getAdditionalData() , payRequest.getCallBackUrl() , payRequest.getPayerId());

				response.setResponse(verifyResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::payRequest()] End");
		return response;
	}
	
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse settleRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtSettleRequest settleRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::settleRequest()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		

		try {
			
			IPaymentGateway serviceStub = getClientStub();
			
			try {
				String settleResponse = serviceStub.bpSettleRequest(settleRequest.getTerminalId(), settleRequest.getUserName(), settleRequest.getUserPassword(), settleRequest.getOrderId(), settleRequest.getSaleOrderId(), settleRequest.getSaleReferenceId());

				response.setResponse(settleResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::settleRequest()] End");
		return response;
	}
	
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse inquiryRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtInquiryRequest inquiryRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::inquiryRequest()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		try {
			
			IPaymentGateway serviceStub = getClientStub();
			
			try {
				String verifyResponse = serviceStub.bpInquiryRequest(inquiryRequest.getTerminalId(), inquiryRequest.getUserName(), inquiryRequest.getUserPassword(), inquiryRequest.getOrderId(), inquiryRequest.getSaleOrderId(), inquiryRequest.getSaleReferenceId());

				response.setResponse(verifyResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::inquiryRequest()] End");
		return response;
	}
	
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse reversalRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtReversalRequest reversalRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::reversalRequest()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		try {
			
			IPaymentGateway serviceStub = getClientStub();
			
			try {
				String verifyResponse = serviceStub.bpReversalRequest(reversalRequest.getTerminalId() , reversalRequest.getUserName() , reversalRequest.getUserPassword() , reversalRequest.getOrderId() , reversalRequest.getSaleOrderId() , reversalRequest.getSaleReferenceId());

				response.setResponse(verifyResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::reversalRequest()] End");
		return response;
	}
	
	public com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse refundRequest(
			com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtRefundRequest refundRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[BehpardakhtWebServiceImpl::refundRequest()] Begin");
		BehpardakhtResponse response = new BehpardakhtResponse();
		try {
			
			IPaymentGateway serviceStub = getClientStub();
			
			try {
				String verifyResponse = serviceStub.bpRefundRequest(refundRequest.getTerminalId() , refundRequest.getUserName() , refundRequest.getUserPassword() , refundRequest.getOrderId() , refundRequest.getSaleOrderId() , refundRequest.getSaleReferenceId() , refundRequest.getRefundAmount());

				response.setResponse(verifyResponse);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[BehpardakhtWebServiceImpl::refundRequest()] End");
		return response;
	}
	
	private IPaymentGateway getClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (behpardakhtClientConfig.isUseProxy()) {
				setProxy(behpardakhtClientConfig.getHttpProxy(), behpardakhtClientConfig.getHttpProxyPort());
			}
			if (behpardakhtClientConfig.isUseSSL()) {
				setSSL(behpardakhtClientConfig.getTrustStoreFile(), behpardakhtClientConfig.getKeyStoreFile());
			}
			
			URL wsdlURL = new URL(behpardakhtClientConfig.getWsdlUrl());
			QName qname = new QName("http://service.pgw.sw.bps.com/", "PaymentGatewayImplService");
			Service service = Service.create(wsdlURL, qname);
			IPaymentGateway port = service.getPort(IPaymentGateway.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);
			
			return port;

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.create.stub.behpardakhtWebService.failed");
		}
	}
	
	
	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
		System.setProperty("http.proxyHost", host);
		System.setProperty("http.proxyPort", port);
	}
	
	private void setSSL(String trustStore, String keyStore) {
		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.trustStore", trustStore);
	}
}
