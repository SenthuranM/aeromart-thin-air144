package com.isa.thinair.wsclient.core.service.rm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.rms.webservices.dto.InventoryAllocationType;
import com.isa.rms.webservices.dto.InventoryAllocationsType;
import com.isa.rms.webservices.dto.OptimisationMessageDTO;
import com.isa.rms.webservices.dto.OptimisationMessageType;
import com.isa.rms.webservices.dto.OptimisationMessagesType;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryRMTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.CommonUtil;

/**
 * RM webservices client logic implementation.
 * 
 * @author Nasly
 * 
 */
public class RMWebservicesClientImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_RM;

	private static Log log = LogFactory.getLog(RMWebservicesClientImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		if (!PlatformUtiltiies.nullHandler(carrierCode).equals("")) {
			return SERVICE_PROVIDER + ExternalWebServiceInvoker.SERVICE_PROVIDER_SEPERATOR + carrierCode;
		}

		return SERVICE_PROVIDER;
	}

	/**
	 * Publishes alerts to RM.
	 * 
	 * @throws Exception
	 */
	public void publishAlerts() throws ModuleException {
		log.debug("Begin publish optimization alerts");
		Map<Integer, FCCSegBCInventoryRMTO> flights = WSClientModuleUtils.getFlightInventoryBD().getFlightsForRMAlert();

		if (flights != null && flights.size() > 0) {

			int total = flights.size();
			int maxFlightsPerReq = WSClientModuleUtils.getModuleConfig().getFlightsPerAerlt();
			Collection<Integer> segInvIds = new ArrayList<Integer>();
			Iterator<Integer> segInvIdsIt = flights.keySet().iterator();

			for (int i = 0; i < (total / maxFlightsPerReq) + 1; ++i) {
				if (i * maxFlightsPerReq >= total) {
					break;
				}
				for (int j = i * maxFlightsPerReq; j < (i + 1) * maxFlightsPerReq; ++j) {
					segInvIds.add(segInvIdsIt.next());
					if (j == total - 1) {
						break;
					}
				}

				try {
					Map<Integer, Collection<FCCSegBCInventoryTO>> inventories = WSClientModuleUtils.getFlightInventoryBD()
							.getInvenotriesForRMAlert(segInvIds);

					OptimisationMessageDTO data = prepareInvForRMAlert(segInvIds, flights, inventories);

					if (data != null) {
						log.debug("Invoking save optimization data[Number of Flights="
								+ data.getOptimisationMessages().getOptimisationMessage().size() + "]");
						ExternalWebServiceInvoker.invokeService(getServiceProvider(null), "saveOptimisationMessage", true,
								new Param(data));

						log.debug("Updating bc inventory optimization status");
						Collection<Integer> fccsbInvIdsToUpdate = new ArrayList<Integer>();
						for (OptimisationMessageType optMsg : data.getOptimisationMessages().getOptimisationMessage()) {
							for (InventoryAllocationType inv : optMsg.getInventoryAllocations().getInventoryAllocation()) {
								if (RMWebservicesClientUtil.WSInventoryRMThresholdStatus.ABOVE_THRESHOLD.equals(inv
										.getMessageType())
										|| RMWebservicesClientUtil.WSInventoryRMThresholdStatus.BELOW_THRESHOLD.equals(inv
												.getMessageType())) {
									fccsbInvIdsToUpdate.add(inv.getInventoryAllocationId());
								}
							}
						}
						WSClientModuleUtils.getFlightInventoryBD().updateOptimizationStatus(fccsbInvIdsToUpdate,
								AirinventoryCustomConstants.InventoryRMOptimizationStatus.OPTIMIZATION_PENDING);
					}
				} catch (Exception ex) {
					log.error(ex);
				}
				segInvIds.clear();
			}
		}
		log.debug("End publish optimization alerts");
	}

	/**
	 * Prepare alert data.
	 * 
	 * @param segInvIds
	 * @param flights
	 * @param inventories
	 * @return
	 * @throws ModuleException
	 */
	private static OptimisationMessageDTO prepareInvForRMAlert(Collection<Integer> segInvIds,
			Map<Integer, FCCSegBCInventoryRMTO> flights, Map<Integer, Collection<FCCSegBCInventoryTO>> inventories)
			throws ModuleException {
		OptimisationMessageDTO optimisationMessageDTO = new OptimisationMessageDTO();

		List<OptimisationMessageType> optimizationMessagesList = new ArrayList<OptimisationMessageType>();

		for (Integer segInvId : segInvIds) {
			FCCSegBCInventoryRMTO flightDetails = flights.get(segInvId);

			OptimisationMessageType optimisationMessage = new OptimisationMessageType();

			optimisationMessage.setCabinClass(flightDetails.getCabinClassCode());
			optimisationMessage.setFlightNumber(flightDetails.getFlightNumber());
			optimisationMessage.setDepatureDate(CommonUtil.parse(flightDetails.getDepartureDateZulu()));
			optimisationMessage.setSegmentCode(flightDetails.getSegmentCode());
			optimisationMessage.setMessageDate(CommonUtil.parse(new Date()));
			optimisationMessage.setProcessingStatus(RMWebservicesClientUtil.getMessageProcessingStatus());
			optimisationMessage.setOptimisationHeaderId(-1);
			optimisationMessage.setVersion(-1);

			List<InventoryAllocationType> inventoryAllocationsList = new ArrayList<InventoryAllocationType>();

			Collection<FCCSegBCInventoryTO> aaInventoryAllocations = inventories.get(segInvId);

			boolean isEligible = false;
			for (FCCSegBCInventoryTO aaInvAllocation : aaInventoryAllocations) {
				InventoryAllocationType inventoryAlloc = new InventoryAllocationType();

				inventoryAlloc.setInventoryAllocationId(aaInvAllocation.getFccsbInvId());
				inventoryAlloc.setBookingCode(aaInvAllocation.getBookingClass());
				inventoryAlloc.setSeatAllocation(aaInvAllocation.getAllocation());
				inventoryAlloc.setConfirmedSeats(aaInvAllocation.getSoldSeats());
				inventoryAlloc.setOnHoldSeats(aaInvAllocation.getOnholdSeats());
				inventoryAlloc.setCancelledSeats(aaInvAllocation.getCancelledSeats());

				String messageType = RMWebservicesClientUtil.getRMAlertMessageType(aaInvAllocation.getThresholdStatus(),
						aaInvAllocation.getOptimizationStatus());

				if (messageType != null) {
					isEligible = true;
					inventoryAlloc.setMessageType(messageType);
				}

				inventoryAlloc.setFare(aaInvAllocation.getMinFare().doubleValue());
				inventoryAlloc.setVersion(-1);
				inventoryAlloc.setOptimisationDetailId(-1);

				inventoryAllocationsList.add(inventoryAlloc);
			}

			if (isEligible) {
				optimisationMessage.setInventoryAllocations(new InventoryAllocationsType());
				optimisationMessage.getInventoryAllocations().getInventoryAllocation().addAll(inventoryAllocationsList);
				optimizationMessagesList.add(optimisationMessage);
			}
		}

		if (optimizationMessagesList.size() > 0) {
			optimisationMessageDTO.setOptimisationMessages(new OptimisationMessagesType());
			optimisationMessageDTO.getOptimisationMessages().getOptimisationMessage().addAll(optimizationMessagesList);
			return optimisationMessageDTO;
		} else {
			return null;
		}
	}
}
