package com.isa.thinair.wsclient.core.config;

import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nilindra Fernando
 * 
 * @isa.module.config-bean
 */
public class RAKClientConfig extends DefaultModuleConfig {

	private String userName;

	private String password;

	private String sendNotifyEmail;

	private String performWSIntegration;

	private Properties emailNotifyTo;

	private String url;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the sendNotifyEmail
	 */
	public String getSendNotifyEmail() {
		return sendNotifyEmail;
	}

	/**
	 * @param sendNotifyEmail
	 *            the sendNotifyEmail to set
	 */
	public void setSendNotifyEmail(String sendNotifyEmail) {
		this.sendNotifyEmail = sendNotifyEmail;
	}

	/**
	 * @return the performWSIntegration
	 */
	public String getPerformWSIntegration() {
		return performWSIntegration;
	}

	/**
	 * @param performWSIntegration
	 *            the performWSIntegration to set
	 */
	public void setPerformWSIntegration(String performWSIntegration) {
		this.performWSIntegration = performWSIntegration;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the emailNotifyTo
	 */
	public Properties getEmailNotifyTo() {
		return emailNotifyTo;
	}

	/**
	 * @param emailNotifyTo
	 *            the emailNotifyTo to set
	 */
	public void setEmailNotifyTo(Properties emailNotifyTo) {
		this.emailNotifyTo = emailNotifyTo;
	}

}
