package com.isa.thinair.wsclient.core.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.dcs.v1.exposed.services.DCSExposedWS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.security.WSClientSecurityHandler;

public class DCSConnectorUtil {

	private static final Log log = LogFactory.getLog(DCSConnectorUtil.class);
	private static DCSExposedWS dcsWS;

	public static DCSExposedWS getDCSExposedWS() throws ModuleException {
		
		if (dcsWS == null) {
			String dcsURL = WSClientModuleUtils.getModuleConfig().getDcsWsUrl();
			// String dcsURL = "http://dcs.cmb.isaaviations.com/dcs-service/dcs/v1/exposed/services?wsdl";
			if (log.isInfoEnabled()) {
				log.info("DCS Exposed Services URL = " + dcsURL);
			}

			URL url;
			try {
				url = new URL(dcsURL);
			} catch (MalformedURLException e) {
				log.error(e);
				throw new ModuleException("lccclient.invalid.lcconnect.url");
			}

			try {
				QName qname = new QName(DCSExposedWS.targetNamespace, DCSExposedWS.serviceName);

				Service service = Service.create(url, qname);

				dcsWS = (DCSExposedWS) service.getPort(DCSExposedWS.class);

				// TODO get credentials from DCS app param
				String[] credentials = AppSysParamsUtil.getDCSConnectivityCredentials();
				if (log.isInfoEnabled()) {
					log.info("DCS Connectivity Username = " + credentials[0] + ", password length = "
							+ (credentials[1] != null ? credentials[1].length() : 0));
				}

				List<Handler> handlerChain = new ArrayList<Handler>();
				handlerChain.add(new WSClientSecurityHandler(credentials[0], credentials[1]));
				((BindingProvider) dcsWS).getBinding().setHandlerChain(handlerChain);

				((BindingProvider) dcsWS).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
			} catch (javax.xml.ws.WebServiceException e) {
				log.error("Error in dynamically binding DCS Exposed webservices", e);
				throw e;
			}
		}

		return dcsWS;
	}
}
