package com.isa.thinair.wsclient.core.service;

/**
 * @author Nasly
 * 
 *         Interface that all the webservice client implementations need to implement.
 * 
 */
public interface DefaultWebserviceClient {

	public String getServiceProvider(String carrierCode);

}
