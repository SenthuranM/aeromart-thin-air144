package com.isa.thinair.wsclient.core.bl;

import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRS;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRS;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPingRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPingRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.core.util.DCSConnectorUtil;

public class PNLADLPublisherBL {

	public static void ping(String text) throws ModuleException {
		DCSPingRQ dcsPingRQ = new DCSPingRQ();
		dcsPingRQ.setToken(text);
		DCSPingRS dcsPingRS = DCSConnectorUtil.getDCSExposedWS().ping(dcsPingRQ);
		System.out.println(dcsPingRS.getResult());
	}

	public static DCSPNLInfoRS sendPNLToDCS(DCSPNLInfoRQ pnlInfo) throws ModuleException {
		
		DCSPNLInfoRS dcsPnlRS = DCSConnectorUtil.getDCSExposedWS().submitPNLInfo(pnlInfo);

		return dcsPnlRS;
		
	}
	
	public static DCSADLInfoRS sendADLToDCS(DCSADLInfoRQ adlInfo) throws ModuleException {
		
		DCSADLInfoRS dcsadlInfoRS = DCSConnectorUtil.getDCSExposedWS().submitADLInfo(adlInfo);
		
		return dcsadlInfoRS;
	}

}
