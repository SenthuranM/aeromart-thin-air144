package com.isa.thinair.wsclient.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.wsclient.api.service.InsuranceClientBD;

@Local
public interface InsuranceClientBDLocal extends InsuranceClientBD {

}
