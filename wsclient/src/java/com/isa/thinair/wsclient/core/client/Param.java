package com.isa.thinair.wsclient.core.client;

public class Param {
	private final Object param;
	private final Class paramType;

	public Param(Object first, Class second) {
		super();
		this.param = first;
		this.paramType = second;
	}

	public Param(Object first) {
		super();
		this.param = first;
		this.paramType = null;
	}

	public int hashCode() {
		int hashFirst = param != null ? param.hashCode() : 0;
		int hashSecond = paramType != null ? paramType.hashCode() : 0;

		return (hashFirst + hashSecond) * hashSecond + hashFirst;
	}

	public boolean equals(Object other) {
		if (other instanceof Param) {
			Param otherPair = (Param) other;
			return ((this.param == otherPair.param || (this.param != null && otherPair.param != null && this.param
					.equals(otherPair.param))) && (this.paramType == otherPair.paramType || (this.paramType != null
					&& otherPair.paramType != null && this.paramType.equals(otherPair.paramType))));
		}

		return false;
	}

	public String toString() {
		return "(" + param + ", " + paramType + ")";
	}

	public Object getParam() {
		return param;
	}

	public Class getParamClass() {
		if (paramType == null) {
			return param.getClass();
		}
		return paramType;
	}
}
