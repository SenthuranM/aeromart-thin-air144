package com.isa.thinair.wsclient.core.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationInsurancePremium;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PricingBasis;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class InsurancePremiumDAO {

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/***
	 * Saves the insurance data to a new insurance table
	 */
	public static void saveInsuranceData() {

	}

	@SuppressWarnings("unchecked")
	public static Map<Integer, ReservationInsurancePremium> getInsurancePremiumData() {
		DataSource ds = getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Map<Integer, ReservationInsurancePremium> premiumsCol = null;
		String sql = " SELECT insurance_premium_id, duration_from, duration_to, premium_amount, premium_currency, pricing_basis "
				+ "FROM t_insurance_premium  ";

		premiumsCol = (Map<Integer, ReservationInsurancePremium>) templete.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, ReservationInsurancePremium> col = new HashMap<Integer, ReservationInsurancePremium>();
				String amount = null;
				String pricingBasis = null;
				while (rs.next()) {
					ReservationInsurancePremium premiumObj = new ReservationInsurancePremium();
					premiumObj.setPremiumId(rs.getLong("insurance_premium_id"));
					premiumObj.setFromDays(rs.getInt("duration_from"));
					premiumObj.setToDays(rs.getInt("duration_to"));
					premiumObj.setCurrency(rs.getString("premium_currency"));
					pricingBasis = rs.getString("pricing_basis");
					amount = rs.getString("premium_amount");
					Map<InsuranceTypes, BigDecimal> insChargesMap = createInsuranceChargesMap(amount);
					if (PricingBasis.Value.code().equalsIgnoreCase(pricingBasis)) {
						premiumObj.setAmount(insChargesMap.get(InsuranceTypes.GENERAL).toString());
					} else {
						premiumObj.setInsTypeCharges(insChargesMap);
					}
					col.put(rs.getInt("insurance_premium_id"), premiumObj);

				}
				return col;
			}
		});

		return premiumsCol;
	}

	private static Map<InsuranceTypes, BigDecimal> createInsuranceChargesMap(String chargesValues) {
		Map<InsuranceTypes, BigDecimal> charges = new HashMap<InsuranceTypes, BigDecimal>();
		String separator = ",";
		String valueSeparator = "=";

		if (chargesValues != null) {
			String[] arrInsTypeValues = chargesValues.split(separator);
			String[] arrKeyValue = null;
			for (String insType : arrInsTypeValues) {
				arrKeyValue = insType.split(valueSeparator);
				if (arrKeyValue[0].equals("1")) {
					charges.put(InsuranceTypes.GENERAL, new BigDecimal(arrKeyValue[1]));
				} else if (arrKeyValue[0].equals("2")) {
					charges.put(InsuranceTypes.CANCELATION, new BigDecimal(arrKeyValue[1]));
				} else if (arrKeyValue[0].equals("3")) {
					charges.put(InsuranceTypes.MULTIRISK, new BigDecimal(arrKeyValue[1]));
				} else if (arrKeyValue[0].equals("0")) {
					charges.put(InsuranceTypes.NONE, new BigDecimal(arrKeyValue[1]));
				}
			}
		}

		return charges;
	}

}
