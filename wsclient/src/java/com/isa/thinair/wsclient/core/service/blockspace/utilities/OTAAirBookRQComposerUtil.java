package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType.PTCFareBreakdowns;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.Address;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.CountryNameType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.ErrorType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests.SpecialServiceRequest;
import org.opentravel.ota._2003._05.TravelerInfoType;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebservicesConstants;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isa.thinair.wsclient.api.util.WSClientResponseCodes;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAddressType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAContactInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AACountryType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPersonNameType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AATelephoneType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNoteType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAUserNotesType;

/**
 * @author Noshani
 * 
 */
public class OTAAirBookRQComposerUtil {

	public static interface SSR_CODES {
		public static final String CODE_TKNA = "TKNA";
	}

	public static Object[] compose(OTAAirPriceRS oTAAirPriceRS, Reservation reservation, Collection externalSegments,
			BaseDTO baseDTO) throws ModuleException {

		CountryNameType countryNameType = null;
		PaymentDetailType paymentDetailType = null;
		DirectBillType directBillType = null;
		CompanyName directBillCompanyName = null;
		PaymentAmount paymentAmount = null;
		Address address = null;
		AirTravelerType airTravelerType = null;
		OTAAirBookRQ oTAAirBookRQ = new OTAAirBookRQ();

		oTAAirBookRQ.setPOS(new POSType());
		oTAAirBookRQ.getPOS().getSource().add(DataComposerUtil.preparePOS(baseDTO));

		oTAAirBookRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		oTAAirBookRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_Book);
		oTAAirBookRQ.setSequenceNmbr(new BigInteger("1"));
		oTAAirBookRQ.setEchoToken(UID.generate());
		oTAAirBookRQ.setTimeStamp(CommonUtil.parse(new Date()));
		oTAAirBookRQ.setTransactionIdentifier(oTAAirPriceRS.getTransactionIdentifier());

		directBillType = new DirectBillType();
		directBillType.setCompanyName(directBillCompanyName = new CompanyName());
		directBillCompanyName.setCode(baseDTO.getOnAccountAgentCode());

		TravelerInfoType trInfo = new TravelerInfoType();
		oTAAirBookRQ.setTravelerInfo(trInfo);

		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		oTAAirBookRQ.setPriceInfo(new PriceInfo());
		oTAAirBookRQ.getPriceInfo().setPTCFareBreakdowns(new PTCFareBreakdowns());

		for (Object o : oTAAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {
			if (o instanceof PricedItinerariesType) {
				for (PricedItineraryType itinsType : ((PricedItinerariesType) o).getPricedItinerary()) {
					oTAAirBookRQ.setAirItinerary(ammendExternalSegments(itinsType.getAirItinerary(), externalSegments));
					oTAAirBookRQ.getPriceInfo().setItinTotalFare(itinsType.getAirItineraryPricingInfo().getItinTotalFare());
					totalPrice = AccelAeroCalculator.add(totalPrice, itinsType.getAirItineraryPricingInfo().getItinTotalFare()
							.getTotalFare().getAmount());
					oTAAirBookRQ.getPriceInfo().setFareInfos(itinsType.getAirItineraryPricingInfo().getFareInfos());
					oTAAirBookRQ.getPriceInfo().setPTCFareBreakdowns(
							itinsType.getAirItineraryPricingInfo().getPTCFareBreakdowns());

					Set passengers = reservation.getPassengers();
					Iterator itr = passengers.iterator();

					while (itr.hasNext()) {
						ReservationPax reservationPax = (ReservationPax) itr.next();

						// If the passenger is adult
						if (ReservationApiUtils.isAdultType(reservationPax)) {

							airTravelerType = new AirTravelerType();
							airTravelerType.setPassengerTypeCode(CommonUtil
									.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));

							airTravelerType.setPersonName(new PersonNameType());
							airTravelerType.getPersonName().getNameTitle()
									.add(CommonUtil.getWSPaxTitle(reservationPax.getTitle()));
							airTravelerType.getPersonName().getGivenName().add(reservationPax.getFirstName());
							airTravelerType.getPersonName().setSurname(reservationPax.getLastName());

							if (reservationPax.getDateOfBirth() != null) {
								airTravelerType.setBirthDate(CommonUtil.parse(reservationPax.getDateOfBirth()));
							}

							AirTravelerType.Telephone telephone = new AirTravelerType.Telephone();
							telephone.setCountryAccessCode(reservation.getContactInfo().getCountryCode());
							telephone.setAreaCityCode(reservation.getContactInfo().getCity());
							telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());

							airTravelerType.getTelephone().add(telephone);

							address = new Address();
							address.setCountryName(countryNameType = new CountryNameType());
							countryNameType.setCode(reservation.getContactInfo().getCountryCode());

							airTravelerType.getAddress().add(address);
							airTravelerType.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
							airTravelerType.getTravelerRefNumber().setRPH("A" + reservationPax.getPaxSequence());
							trInfo.getAirTraveler().add(airTravelerType);

							String[] ssrData = ReservationSSRUtil.getPaxSSR(reservationPax);
							updateSSRInfo(oTAAirBookRQ, reservation.getPnr(), ssrData[0], ssrData[1], airTravelerType
									.getTravelerRefNumber().getRPH());
							// If the passenger is a child
						} else if (ReservationApiUtils.isChildType(reservationPax)) {
							airTravelerType = new AirTravelerType();
							airTravelerType.setPassengerTypeCode(CommonUtil
									.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));

							airTravelerType.setPersonName(new PersonNameType());
							airTravelerType.getPersonName().getNameTitle()
									.add(CommonUtil.getWSPaxTitle(reservationPax.getTitle()));
							airTravelerType.getPersonName().getGivenName().add(reservationPax.getFirstName());
							airTravelerType.getPersonName().setSurname(reservationPax.getLastName());

							if (reservationPax.getDateOfBirth() != null) {
								airTravelerType.setBirthDate(CommonUtil.parse(reservationPax.getDateOfBirth()));
							}

							AirTravelerType.Telephone telephone = new AirTravelerType.Telephone();

							telephone.setCountryAccessCode(reservation.getContactInfo().getCountryCode());
							telephone.setAreaCityCode(reservation.getContactInfo().getCity());
							telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());

							airTravelerType.getTelephone().add(telephone);

							address = new Address();
							address.setCountryName(countryNameType = new CountryNameType());
							countryNameType.setCode(reservation.getContactInfo().getCountryCode());

							airTravelerType.getAddress().add(address);
							airTravelerType.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
							airTravelerType.getTravelerRefNumber().setRPH("C" + reservationPax.getPaxSequence());
							trInfo.getAirTraveler().add(airTravelerType);

							String[] ssrData = ReservationSSRUtil.getPaxSSR(reservationPax);

							updateSSRInfo(oTAAirBookRQ, reservation.getPnr(), ssrData[0], ssrData[1], airTravelerType
									.getTravelerRefNumber().getRPH());
							// If the passenger is an infant
						} else if (ReservationApiUtils.isInfantType(reservationPax)) {
							airTravelerType = new AirTravelerType();
							airTravelerType.setPassengerTypeCode(CommonUtil
									.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
							airTravelerType.setPersonName(new PersonNameType());
							airTravelerType.getPersonName().getNameTitle()
									.add(CommonUtil.getWSPaxTitle(reservationPax.getTitle()));
							airTravelerType.getPersonName().getGivenName().add(reservationPax.getFirstName());
							airTravelerType.getPersonName().setSurname(reservationPax.getLastName());

							if (reservationPax.getDateOfBirth() != null) {
								airTravelerType.setBirthDate(CommonUtil.parse(reservationPax.getDateOfBirth()));
							}

							AirTravelerType.Telephone telephone = new AirTravelerType.Telephone();
							telephone.setCountryAccessCode(reservation.getContactInfo().getCountryCode());
							telephone.setAreaCityCode(reservation.getContactInfo().getCity());
							telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());

							airTravelerType.getTelephone().add(telephone);

							address = new Address();
							address.setCountryName(countryNameType = new CountryNameType());
							countryNameType.setCode(reservation.getContactInfo().getCountryCode());

							airTravelerType.getAddress().add(address);
							airTravelerType.setTravelerRefNumber(new AirTravelerType.TravelerRefNumber());
							airTravelerType.getTravelerRefNumber().setRPH(
									"I" + reservationPax.getPaxSequence() + "/A" + reservationPax.getParent().getPaxSequence());

							trInfo.getAirTraveler().add(airTravelerType);
						}
					}
				}
			}
		}

		// ################ PAYMENT DETAILS (DIRECT BILL)
		oTAAirBookRQ.setFulfillment(new OTAAirBookRQ.Fulfillment());
		oTAAirBookRQ.getFulfillment().setPaymentDetails(new OTAAirBookRQ.Fulfillment.PaymentDetails());

		// Total Reservation Payments
		oTAAirBookRQ.getFulfillment().getPaymentDetails().getPaymentDetail().add(paymentDetailType = new PaymentDetailType());
		paymentDetailType.setDirectBill(directBillType);
		paymentDetailType.setPaymentAmount(paymentAmount = new PaymentAmount());
		paymentAmount.setAmount(totalPrice);

		// set contact information
		AAContactInfoType wsContactInfo = new AAContactInfoType();
		AAPersonNameType name = new AAPersonNameType();
		wsContactInfo.setPersonName(name);
		name.setTitle(CommonUtil.getWSPaxTitle(reservation.getContactInfo().getTitle()));
		name.setFirstName(reservation.getContactInfo().getFirstName());
		name.setLastName(reservation.getContactInfo().getLastName());

		AATelephoneType telephone = new AATelephoneType();
		wsContactInfo.setTelephone(telephone);
		telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());
		wsContactInfo.setEmail(reservation.getContactInfo().getEmail());

		AAAddressType caddress = new AAAddressType();
		wsContactInfo.setAddress(caddress);

		AATelephoneType mobile = new AATelephoneType();
		wsContactInfo.setMobile(mobile);
		mobile.setPhoneNumber(reservation.getContactInfo().getMobileNo());

		AATelephoneType fax = new AATelephoneType();
		wsContactInfo.setFax(fax);
		fax.setPhoneNumber(reservation.getContactInfo().getFax());

		AACountryType ccountry = new AACountryType();
		caddress.setAddressLine1(reservation.getContactInfo().getStreetAddress1());
		caddress.setAddressLine2(reservation.getContactInfo().getStreetAddress2());
		caddress.setCityName(reservation.getContactInfo().getCity());
		caddress.setCountryName(ccountry);

		wsContactInfo.setPreferredLanguage(reservation.getContactInfo().getPreferredLanguage());

		ccountry.setCountryCode(reservation.getContactInfo().getCountryCode());
		ccountry.setCountryName(reservation.getContactInfo().getCountryCode());

		AAAirBookRQExt aaAirBookRQExt = new AAAirBookRQExt();
		aaAirBookRQExt.setContactInfo(wsContactInfo);

		AAUserNotesType aaUserNotesType = new AAUserNotesType();
		AAUserNoteType userNoteType = new AAUserNoteType();

		String lastUserNote = BeanUtils.nullHandler(reservation.getUserNote());

		if (lastUserNote.length() > 0) {
			lastUserNote = " LAST USER NOTE [ " + lastUserNote + " ] ";
		}

		userNoteType.setNoteText(lastUserNote + " Booking transfered from [Carrier " + AppSysParamsUtil.getDefaultCarrierCode()
				+ "] [PNR " + reservation.getPnr() + "] [On " + BeanUtils.parseDateFormat(new Date(), "E, dd MMM yyyy HH:mm:ss")
				+ "] [Charges/Payments " + totalPrice.toString() + " " + AppSysParamsUtil.getBaseCurrency() + " ] ");

		aaUserNotesType.getUserNote().add(userNoteType);
		aaAirBookRQExt.setUserNotes(aaUserNotesType);

		return new Object[] { oTAAirBookRQ, aaAirBookRQExt };
	}

	/**
	 * Ammend external segments
	 * 
	 * @param airItinerary
	 * @param externalSegments
	 * @return
	 * @throws ModuleException
	 */
	private static AirItineraryType ammendExternalSegments(AirItineraryType airItinerary, Collection externalSegments)
			throws ModuleException {
		ReservationSegmentDTO reservationSegmentDTO;
		BookFlightSegmentType bookFlightSegmentType;
		DepartureAirport departureAirport;
		ArrivalAirport arrivalAirport;

		for (Iterator itr = externalSegments.iterator(); itr.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itr.next();
			bookFlightSegmentType = new BookFlightSegmentType();

			departureAirport = new DepartureAirport();
			departureAirport.setLocationCode(DataComposerUtil.getAirportCode(reservationSegmentDTO.getSegmentCode(), true));
			bookFlightSegmentType.setDepartureAirport(departureAirport);

			arrivalAirport = new ArrivalAirport();
			arrivalAirport.setLocationCode(DataComposerUtil.getAirportCode(reservationSegmentDTO.getSegmentCode(), false));
			bookFlightSegmentType.setArrivalAirport(arrivalAirport);

			bookFlightSegmentType.setDepartureDateTime(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
			bookFlightSegmentType.setArrivalDateTime(CommonUtil.parse(reservationSegmentDTO.getArrivalDate()));
			bookFlightSegmentType.setFlightNumber(reservationSegmentDTO.getFlightNo());

			airItinerary.getOriginDestinationOptions().getOriginDestinationOption().get(0).getFlightSegment()
					.add(bookFlightSegmentType);
		}

		return airItinerary;
	}

	/**
	 * Updates the SSR information
	 * 
	 * @param airBookRQ
	 * @param ssrCode
	 * @param ssrRemarks
	 * @param rph
	 */
	private static void updateSSRInfo(OTAAirBookRQ oTAAirBookRQ, String pnr, String ssrCode, String ssrRemarks, String rph) {
		SpecialReqDetailsType specialReqDetailType = new SpecialReqDetailsType();
		SpecialServiceRequests specialReqs = new SpecialServiceRequests();
		SpecialServiceRequest specialReq = new SpecialServiceRequest();

		specialReq.setSSRCode(SSR_CODES.CODE_TKNA);
		String combinedSSRRemarks = BeanUtils.nullHandler(BeanUtils.nullHandler(pnr) + " " + BeanUtils.nullHandler(ssrCode) + " "
				+ BeanUtils.nullHandler(ssrRemarks));
		specialReq.setText(combinedSSRRemarks);

		specialReq.getTravelerRefNumberRPHList().clear();
		specialReq.getTravelerRefNumberRPHList().add(rph);

		specialReqs.getSpecialServiceRequest().clear();
		specialReqs.getSpecialServiceRequest().add(specialReq);
		specialReqDetailType.setSpecialServiceRequests(specialReqs);

		oTAAirBookRQ.getTravelerInfo().getSpecialReqDetails().add(specialReqDetailType);
	}

	/**
	 * Returns the total price information
	 * 
	 * @param airReservation
	 * @return
	 */
	public static Object[] getTotalPriceInfo(AirReservation airReservation) {
		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		String currencyCode = "";

		if (airReservation.getPriceInfo().getItinTotalFare() != null
				&& airReservation.getPriceInfo().getItinTotalFare().getTotalFare() != null) {
			totalPrice = airReservation.getPriceInfo().getItinTotalFare().getTotalFare().getAmount();
			currencyCode = airReservation.getPriceInfo().getItinTotalFare().getTotalFare().getCurrencyCode();
		}

		return new Object[] { totalPrice.toString(), currencyCode };
	}

	/**
	 * Returns the total payment information
	 * 
	 * @param airReservation
	 * @return
	 */
	public static Object[] getTotalPaymentInfo(AirReservation airReservation) {
		BigDecimal totalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection currencies = new ArrayList();
		PaymentDetailType pdt;

		if (airReservation.getFulfillment() != null && airReservation.getFulfillment().getPaymentDetails() != null) {
			for (Iterator pmt = airReservation.getFulfillment().getPaymentDetails().getPaymentDetail().iterator(); pmt.hasNext();) {
				pdt = (PaymentDetailType) pmt.next();
				totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, pdt.getPaymentAmount().getAmount());
				currencies.add(pdt.getPaymentAmount().getCurrencyCode());
			}
		}

		return new Object[] { totalPaymentAmount.toString(), BeanUtils.constructINStringForInts(currencies) };
	}

	/**
	 * Returns the segment information
	 * 
	 * @param airReservation
	 * @return
	 */
	public static String getSegmentInformation(AirReservation airReservation, String currentSegInfo) {
		StringBuilder stringBuilder = new StringBuilder();
		if (airReservation.getAirItinerary().getOriginDestinationOptions() != null) {
			for (Iterator itr = airReservation.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()
					.iterator(); itr.hasNext();) {
				OriginDestinationOptionType originDestinationOptionType = (OriginDestinationOptionType) itr.next();

				for (Iterator iter = originDestinationOptionType.getFlightSegment().iterator(); iter.hasNext();) {
					BookFlightSegmentType bookFlightSegmentType = (BookFlightSegmentType) iter.next();
					stringBuilder.append(bookFlightSegmentType.getFlightNumber()
							+ " "
							+ bookFlightSegmentType.getDepartureAirport().getLocationCode()
							+ "/"
							+ bookFlightSegmentType.getArrivalAirport().getLocationCode()
							+ " "
							+ BeanUtils.parseDateFormat(bookFlightSegmentType.getDepartureDateTime().toGregorianCalendar()
									.getTime(), "E, dd MMM yyyy HH:mm:ss")
							+ " / "
							+ BeanUtils.parseDateFormat(bookFlightSegmentType.getArrivalDateTime().toGregorianCalendar()
									.getTime(), "E, dd MMM yyyy HH:mm:ss"));
				}
			}
		}

		return " Current segment(s) (" + currentSegInfo + ") transfered to (" + stringBuilder.toString() + ") ";
	}

	/**
	 * Prepares the service response
	 * 
	 * @param oTAAirBookRS
	 * @param currentSegInfo
	 * @return
	 */
	public static DefaultServiceResponse prepareServiceResponse(OTAAirBookRS oTAAirBookRS, String currentSegInfo) {
		DefaultServiceResponse serviceResponce = new DefaultServiceResponse(false);
		serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC, "Generic Booking Creation Error");

		if (oTAAirBookRS != null) {
			if (oTAAirBookRS.getSuccessAndWarningsAndAirReservation() != null
					&& oTAAirBookRS.getSuccessAndWarningsAndAirReservation().size() > 0) {
				for (Iterator i = oTAAirBookRS.getSuccessAndWarningsAndAirReservation().iterator(); i.hasNext();) {
					Object o = i.next();
					if (o instanceof AirReservation) {
						serviceResponce = new DefaultServiceResponse(true);
						AirReservation airReservation = (AirReservation) o;
						serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.TRANSFER_PNR, airReservation
								.getBookingReferenceID().get(0).getID());

						Object[] priceInfo = getTotalPriceInfo(airReservation);
						serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PRICE_AMOUNT, priceInfo[0]);
						serviceResponce
								.addResponceParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PRICE_CURRENCY, priceInfo[1]);

						Object[] paymentInfo = getTotalPaymentInfo(airReservation);
						serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PAYMENT_AMOUNT,
								paymentInfo[0]);
						serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.TOTAL_PAYMENT_CURRENCY,
								paymentInfo[1]);

						serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.SEGMENT_INFORMATION,
								getSegmentInformation(airReservation, currentSegInfo));
					}
				}
			} else if (oTAAirBookRS.getErrors() != null && oTAAirBookRS.getErrors().getError() != null
					&& oTAAirBookRS.getErrors().getError().size() > 0) {
				// 1st error
				ErrorType error = oTAAirBookRS.getErrors().getError().iterator().next();
				String errorStr = error.getType() + ":" + error.getCode() + " [" + error.getShortText() + "] ";

				serviceResponce.addResponceParam(WSClientResponseCodes.InterlinedTypes.PROCESS_DESC, errorStr);
			}
		}

		return serviceResponce;
	}

}
