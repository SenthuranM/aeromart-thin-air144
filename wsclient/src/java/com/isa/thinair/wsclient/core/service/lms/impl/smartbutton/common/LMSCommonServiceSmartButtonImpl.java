package com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.common;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyBaseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.lms.member.FetchTypesMemberGenderReturn;
import com.isa.thinair.wsclient.api.lms.member.GenderTypeItem;
import com.isa.thinair.wsclient.api.lms.member.MemberSoapStub;
import com.isa.thinair.wsclient.api.lms.membercontact.CountryListItem;
import com.isa.thinair.wsclient.api.lms.membercontact.FetchAddressCountryListReturn;
import com.isa.thinair.wsclient.api.lms.membercontact.MemberContactSoapStub;
import com.isa.thinair.wsclient.api.lms.message.MessageSoapStub;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.service.lms.base.common.LMSCommonService;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSSmartButtonWebServiceInvoker;

public class LMSCommonServiceSmartButtonImpl implements DefaultWebserviceClient, LMSCommonService {

	private static Log log = LogFactory.getLog(LMSCommonServiceSmartButtonImpl.class);

	private static Map<String, Integer> genderTypeIds = null;

	private static Map<String, Integer> countryInternalIds = null;

	/**
	 * This method send message notification for set password
	 * 
	 * @param memberAccountId
	 * @throws ModuleException
	 */
	@Override
	public void sendPasswordMessage(String memberAccountId) throws ModuleException {
		MessageSoapStub clientStub = (MessageSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MESSAGE);

		try {
			clientStub.sendPasswordMessage(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken(), memberAccountId, 0);
		} catch (RemoteException e) {
			log.error("Error in sending password message to  loyalty member " + memberAccountId);
			throw new ModuleException("wsclient.loyalty.serviceinvocation.failed",
					"Error in sending password message to  loyalty member " + memberAccountId);
		}
	}

	/**
	 * This method will return java.util.Map containing gender string as a key and SB internal ID for gender as value
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Integer> getLMSInternalGenderTypeIds() throws ModuleException {
		if (genderTypeIds == null) {
			// Load gender types
			genderTypeIds = new HashMap<String, Integer>();
			setLMSInternalGenderTypes();
		}
		return genderTypeIds;
	}

	/**
	 * This method will return java.util.Map containing country code as a key and SB internal ID for country as value
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Integer> getLMSInternalCountryIds() throws ModuleException {
		if (countryInternalIds == null) {
			// Load Country IDs
			countryInternalIds = new HashMap<String, Integer>();
			setLMSInternalCountryIDs();
		}
		return countryInternalIds;
	}

	/**
	 * Handle LMS WS API response error
	 * 
	 * @param response
	 * @param responseCode
	 * @param errorMsg
	 * @param errorMsgCode
	 * @throws ModuleException
	 */
	public static void wrapErrorResponse(Object response, Integer responseCode, String errorMsg, String errorMsgCode)
			throws ModuleException {
		errorMsgCode = (errorMsgCode == null) ? "wsclient.loyalty.serviceinvocation.failed" : errorMsgCode;
		if (response == null) {
			log.error("response is null");
			throw new ModuleException(errorMsgCode, "Service Response is null");
		}else if(responseCode == LMSConstants.WSReponse.DUPLICATE_ACCOUNT_ID){
			errorMsg = "Loyalty account number already linked with a customer"; 
			errorMsgCode = "aircustomer.loyalty.account.already.linked";
			log.error(errorMsg);
			throw new ModuleException(errorMsgCode, errorMsg + ". service method response code "
					+ responseCode);
		} else if (responseCode != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
			log.error(errorMsg);
			throw new ModuleException(errorMsgCode, errorMsg + ". service method response code "
					+ responseCode);
		}
	}

	public static void logUnsuccessfulResponse(LoyaltyBaseDTO response, String customMessage) {
		if (log.isErrorEnabled()) {
			StringBuilder sb = new StringBuilder(customMessage);
			if (response == null) {
				sb.append("Response is null");
				log.error(sb.toString());
			} else if (response.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
				sb.append(" Member ID: ").append(response.getMemberAccountId());
				sb.append(" , Return Code: ").append(response.getReturnCode());
				log.error(sb.toString());
			}
		}
	}

	@Override
	public String getServiceProvider(String carrierCode) {
		return LMSSmartButtonWebServiceInvoker.SERVICE_PROVIDER;
	}

	private static void setLMSInternalGenderTypes() throws ModuleException {
		MemberSoapStub clientStub = (MemberSoapStub) LMSSmartButtonWebServiceInvoker.getClientStub(LMSConstants.Module.MEMBER);
		try {
			FetchTypesMemberGenderReturn fetchTypesMemberGenderReturn = clientStub
					.fetchTypesMemberGender(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWsSecurityToken());

			wrapErrorResponse(fetchTypesMemberGenderReturn,
					(fetchTypesMemberGenderReturn != null ? fetchTypesMemberGenderReturn.getReturnCode() : -1),
					"Error in retrieving member gender types", null);

			GenderTypeItem[] genderTypeArray = fetchTypesMemberGenderReturn.getMemberGenderTypes();
			if (genderTypeArray != null) {
				for (GenderTypeItem genderTypeItem : genderTypeArray) {
					genderTypeIds.put(genderTypeItem.getMemberGenderTypeString(), genderTypeItem.getMemberGenderTypeInternalId());
				}
			}
		} catch (RemoteException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}
	}

	private static void setLMSInternalCountryIDs() throws ModuleException {
		MemberContactSoapStub clientStub = (MemberContactSoapStub) LMSSmartButtonWebServiceInvoker
				.getClientStub(LMSConstants.Module.MEMBER_CONTACT);

		try {
			FetchAddressCountryListReturn response = clientStub.fetchAddressCountryList(LMSSmartButtonWebServiceInvoker.lmsClientConfig
					.getWsSecurityToken());

			wrapErrorResponse(response, (response != null ? response.getReturnCode() : -1),
					"Error in retrieving country internal IDs", null);

			CountryListItem[] addressCountryList = response.getAddressCountryList();
			if (addressCountryList != null) {
				for (CountryListItem countryListItem : addressCountryList) {
					countryInternalIds.put(countryListItem.getCountryItemAbbreviation(),
							countryListItem.getCountryItemInternalId());
				}
			}
		} catch (RemoteException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.loyalty.serviceinvocation.failed");
		}
	}

	@Override
	public String generateS3SignedUrl(String fileName, String mimeType) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
}
