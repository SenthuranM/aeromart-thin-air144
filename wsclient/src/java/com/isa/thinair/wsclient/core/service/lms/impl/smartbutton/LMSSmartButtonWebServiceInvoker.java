package com.isa.thinair.wsclient.core.service.lms.impl.smartbutton;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;

import org.apache.axis.AxisProperties;
import org.apache.axis.client.ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.wsclient.api.lms.customfield.CustomFieldSoapStub;
import com.isa.thinair.wsclient.api.lms.member.MemberSoapStub;
import com.isa.thinair.wsclient.api.lms.memberactivity.MemberActivitySoapStub;
import com.isa.thinair.wsclient.api.lms.membercontact.MemberContactSoapStub;
import com.isa.thinair.wsclient.api.lms.membersecurity.MemberSecuritySoapStub;
import com.isa.thinair.wsclient.api.lms.message.MessageSoapStub;
import com.isa.thinair.wsclient.api.lms.offer.OfferSoapStub;
import com.isa.thinair.wsclient.api.lms.portal.PortalSoapStub;
import com.isa.thinair.wsclient.api.lms.referrer.ReferrerSoapStub;
import com.isa.thinair.wsclient.api.lms.reward.RewardSoapStub;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.config.SmartButtonConfig;

/**
 * LMS Web Service invocations
 * 
 * @author rumesh
 * 
 */
public class LMSSmartButtonWebServiceInvoker {
	private static Log log = LogFactory.getLog(LMSSmartButtonWebServiceInvoker.class);

	public static final SmartButtonConfig lmsClientConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig()
			.getSmartButtonConfig();
	public static final String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_LMS;

	private static final String defaultNamespaceURI = "LoyaltyPlatformWS";

	public static Object getClientStub(String moduleName) throws ModuleException {
		String serviceURL = lmsClientConfig.getModuleUrl().getProperty(moduleName);
		Object service = null;
		String localNamespaceURI = defaultNamespaceURI;
		try {
			String localPart = "";
			if (LMSConstants.Module.CUSTOMER_FIELD.equals(moduleName)) {
				localPart = "CustomField";
				localNamespaceURI = "https://app.smartbutton.com/Services/";
			} else if (LMSConstants.Module.MEMBER.equals(moduleName)) {
				localPart = "Member";
			} else if (LMSConstants.Module.MEMBER_ACTIVITY.equals(moduleName)) {
				localPart = "MemberActivity";
			} else if (LMSConstants.Module.MEMBER_CONTACT.equals(moduleName)) {
				localPart = "MemberContact";
			} else if (LMSConstants.Module.MEMBER_SECURITY.equals(moduleName)) {
				localPart = "MemberSecurity";
			} else if (LMSConstants.Module.OFFER.equals(moduleName)) {
				localPart = "Offer";
			} else if (LMSConstants.Module.PORTAL.equals(moduleName)) {
				localPart = "Portal";
			} else if (LMSConstants.Module.REFERRER.equals(moduleName)) {
				localPart = "Referrer";
				localNamespaceURI = "https://app.smartbutton.com/Services/";
			} else if (LMSConstants.Module.REWARD.equals(moduleName)) {
				localPart = "Reward";
				localNamespaceURI = "https://app.smartbutton.com/Services/";
			} else if (LMSConstants.Module.MESSAGE.equals(moduleName)) {
				localPart = "Message";
			} else {
				throw new ModuleException("wsclient.serviceinvocation.invalid.provider", "Invalid service provider ID ["
						+ moduleName + "]");
			}

			URL wsdlURL = new URL(serviceURL);
			QName qname = new QName(localNamespaceURI, localPart);

			ServiceFactory serviceFactory = new ServiceFactory();
			Service lmsService = serviceFactory.createService(qname);

			if (LMSConstants.Module.CUSTOMER_FIELD.equals(moduleName)) {
				service = new CustomFieldSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.MEMBER.equals(moduleName)) {
				service = new MemberSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.MEMBER_ACTIVITY.equals(moduleName)) {
				service = new MemberActivitySoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.MEMBER_CONTACT.equals(moduleName)) {
				service = new MemberContactSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.MEMBER_SECURITY.equals(moduleName)) {
				service = new MemberSecuritySoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.OFFER.equals(moduleName)) {
				service = new OfferSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.PORTAL.equals(moduleName)) {
				service = new PortalSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.REFERRER.equals(moduleName)) {
				service = new ReferrerSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.REWARD.equals(moduleName)) {
				service = new RewardSoapStub(wsdlURL, lmsService);
			} else if (LMSConstants.Module.MESSAGE.equals(moduleName)) {
				service = new MessageSoapStub(wsdlURL, lmsService);
			}

			setProxySettings();

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}

		return service;
	}

	private static void setProxySettings() {
		GlobalConfig globalconfig = WSClientModuleUtils.getGlobalConfig();
		if (globalconfig.getUseProxy()) {
			String proxyPort = String.valueOf(globalconfig.getHttpPort());
			AxisProperties.setProperty("http.proxySet", "true");
			AxisProperties.setProperty("http.proxyHost", globalconfig.getHttpProxy());
			AxisProperties.setProperty("http.proxyPort", proxyPort);
			AxisProperties.setProperty("https.proxySet", "true");
			AxisProperties.setProperty("https.proxyHost", globalconfig.getHttpProxy());
			AxisProperties.setProperty("https.proxyPort", proxyPort);
		}
	}
}
