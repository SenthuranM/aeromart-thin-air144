package com.isa.thinair.wsclient.core.service.aig;

import java.io.InputStream;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Created to use with HttpClient.
 * 
 * @author Byorn.
 * 
 */
public interface IExecutionStrategy {

	/** Handle the response inputStream **/
	public void handleResponse(InputStream inputStream) throws ModuleException;

}
