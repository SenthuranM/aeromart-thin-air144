package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebservicesConstants;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isa.thinair.wsclient.api.dto.FlightSearchTransferDTO;

/**
 * @author Noshani
 * 
 */
public class OTAAirAvailRQComposerUtil {

	/**
	 * Compose the OTAAirAvailRQ object
	 * 
	 * @param flightSearchTransferDTO
	 * @param baseDTO
	 * @param transactionIdentifier
	 * @return
	 * @throws ModuleException
	 */
	public static OTAAirAvailRQ compose(FlightSearchTransferDTO flightSearchTransferDTO, BaseDTO baseDTO,
			String transactionIdentifier) throws ModuleException {

		OTAAirAvailRQ oTAAirAvailRQ = new OTAAirAvailRQ();

		oTAAirAvailRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		oTAAirAvailRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		oTAAirAvailRQ.setTimeStamp(CommonUtil.parse(new Date()));
		oTAAirAvailRQ.setSequenceNmbr(new BigInteger("1"));
		oTAAirAvailRQ.setEchoToken(UID.generate());
		oTAAirAvailRQ.setTarget("Interlined");
		oTAAirAvailRQ.setTransactionIdentifier(transactionIdentifier);

		POSType pos = new POSType();
		oTAAirAvailRQ.setPOS(pos);
		pos.getSource().add(DataComposerUtil.preparePOS(baseDTO));

		// if (flightSearchTransferDTO.getReturnDate() != null){
		// setReturnSegments(oTAAirAvailRQ, flightSearchTransferDTO);
		// } else {
		setOnewaySegment(oTAAirAvailRQ, flightSearchTransferDTO);
		// }

		TravelerInfoSummaryType travellerInfoSummary = new TravelerInfoSummaryType();
		oTAAirAvailRQ.setTravelerInfoSummary(travellerInfoSummary);

		TravelerInformationType travelerInfo = new TravelerInformationType();
		travellerInfoSummary.getAirTravelerAvail().add(travelerInfo);

		// adult
		PassengerTypeQuantityType paxAdultQuantity = new PassengerTypeQuantityType();
		paxAdultQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		paxAdultQuantity.setQuantity(flightSearchTransferDTO.getAdultCount());
		travelerInfo.getPassengerTypeQuantity().add(paxAdultQuantity);

		// child
		PassengerTypeQuantityType paxChildQuantity = new PassengerTypeQuantityType();
		paxChildQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		paxChildQuantity.setQuantity(flightSearchTransferDTO.getChildCount());
		travelerInfo.getPassengerTypeQuantity().add(paxChildQuantity);

		// infant
		PassengerTypeQuantityType paxInfantQuantity = new PassengerTypeQuantityType();
		paxInfantQuantity.setCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		paxInfantQuantity.setQuantity(flightSearchTransferDTO.getInfantCount());
		travelerInfo.getPassengerTypeQuantity().add(paxInfantQuantity);

		return oTAAirAvailRQ;

	}

	/**
	 * Set the return segments
	 * 
	 * @param otaAirAvailRQ
	 * @param flightSearchTransferDTO
	 * @throws ModuleException
	 */
	private static void setReturnSegments(OTAAirAvailRQ otaAirAvailRQ, FlightSearchTransferDTO flightSearchTransferDTO)
			throws ModuleException {

		Calendar obDeparuteDateCal = new GregorianCalendar();
		obDeparuteDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getDay(), 0, 0, 0);

		Calendar obArrivalDateCal = new GregorianCalendar();
		obArrivalDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getDay(), 0, 0, 0);

		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation(flightSearchTransferDTO.getFromAirport(), flightSearchTransferDTO.getToAirport(),
						obDeparuteDateCal, obArrivalDateCal));

		Calendar ibDeparuteDateCal = new GregorianCalendar();
		ibDeparuteDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getDay(), 0, 0, 0);

		Calendar ibArrivalDateCal = new GregorianCalendar();
		ibArrivalDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getDay(), 0, 0, 0);

		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation(flightSearchTransferDTO.getToAirport(), flightSearchTransferDTO.getFromAirport(),
						ibDeparuteDateCal, ibArrivalDateCal));
	}

	/**
	 * Set One way segments
	 * 
	 * @param otaAirAvailRQ
	 * @param flightSearchTransferDTO
	 * @throws ModuleException
	 */
	private static void setOnewaySegment(OTAAirAvailRQ otaAirAvailRQ, FlightSearchTransferDTO flightSearchTransferDTO)
			throws ModuleException {
		Calendar deparuteDateCal = new GregorianCalendar();
		deparuteDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getDepatureDate()).getDay(), 0, 0, 0);

		Calendar arrivalDateCal = new GregorianCalendar();
		arrivalDateCal.set(CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getYear(),
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getMonth() - 1,
				CommonUtil.parse(flightSearchTransferDTO.getReturnDate()).getDay(), 0, 0, 0);

		otaAirAvailRQ.getOriginDestinationInformation().add(
				getOriginDestinationInformation(flightSearchTransferDTO.getFromAirport(), flightSearchTransferDTO.getToAirport(),
						deparuteDateCal, arrivalDateCal));
	}

	/**
	 * Compose OriginDestinationInformation Information
	 * 
	 * @param originAirport
	 * @param destAirport
	 * @param depDate
	 * @param arrDate
	 * @return
	 * @throws ModuleException
	 */
	private static OTAAirAvailRQ.OriginDestinationInformation getOriginDestinationInformation(String originAirport,
			String destAirport, Calendar depDate, Calendar arrDate) throws ModuleException {

		OTAAirAvailRQ.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRQ.OriginDestinationInformation();

		OriginDestinationInformationType.OriginLocation origin = new OriginDestinationInformationType.OriginLocation();
		origin.setLocationCode(originAirport);
		OriginDestinationInformationType.DestinationLocation destination = new OriginDestinationInformationType.DestinationLocation();
		destination.setLocationCode(destAirport);

		originDestinationInformation.setOriginLocation(origin);
		originDestinationInformation.setDestinationLocation(destination);

		TimeInstantType departureDate = new TimeInstantType();
		departureDate.setValue(CommonUtil.getFormattedDate(depDate.getTime()));

		TimeInstantType arrivalDate = new TimeInstantType();
		arrivalDate.setValue(CommonUtil.getFormattedDate(arrDate.getTime()));

		originDestinationInformation.setDepartureDateTime(departureDate);
		originDestinationInformation.setArrivalDateTime(arrivalDate);

		return originDestinationInformation;
	}
}
