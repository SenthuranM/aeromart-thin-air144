package com.isa.thinair.wsclient.core.service.saman;

import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;

import org.apache.axis.client.ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.datacontract.schemas._2004._07.Refund_DTO.ResponseModel;
import org.datacontract.schemas._2004._07.Refund_DTO.TypeRefundAction;
import org.tempuri.BasicHttpBinding_IsrvRefundStub;

import Foo.PaymentIFBindingSoapStub;
import RefundReportWSDL_pkg.RefundReportWSDLBindingStub;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundServiceResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundStatusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;

/**
 * Saman Payment Gateway Web Service Client Implementation
 * 
 * @author Janaka Padukka
 */
public class SamanWebServicesImpl {
	private static Log log = LogFactory.getLog(SamanWebServicesImpl.class);

	com.isa.thinair.wsclient.core.config.SamanClientConfig samanClientConfig = WSClientModuleUtils.getModuleConfig()
			.getSamanClientConfig();

	/**
	 * Payment verification method for Saman Payment gateway
	 * 
	 * @param verifyRequest
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO verifyTransaction(
			com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO verifyRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::verifyTransaction()] Begin");
		SamanResponseDTO response = new SamanResponseDTO();
		try {
			PaymentIFBindingSoapStub serviceStub = getClientStub();
			try {
				double verifyResponse = serviceStub.verifyTransaction(verifyRequest.getReferenceNo(),
						verifyRequest.getMerchantId());

				response.setVerifiactionResult(verifyResponse);

			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::verifyTransaction()] End");
		return response;
	}

	/**
	 * Refunding method for Saman Payment Gateway
	 * 
	 * @param reverseRequest
	 *            TODO
	 * @return TODO
	 * @throws ModuleException
	 */
	public com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO reverseTransaction(
			com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO reverseRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::reverseTransaction()] Begin");
		SamanResponseDTO response = new SamanResponseDTO();
		try {
			PaymentIFBindingSoapStub serviceStub = getClientStub();
			try {

				double revResult = serviceStub.reverseTransaction1(reverseRequest.getReferenceNo(), reverseRequest
						.getMerchantId(), reverseRequest.getMerchantPassword(), reverseRequest.getRefundAmount().doubleValue());
				response.setRefundResult(new Integer((int) revResult));
			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::reverseTransaction()] End");
		return response;

	}

	private Foo.PaymentIFBindingSoapStub getClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (samanClientConfig.isUseProxy()) {
				setProxy(samanClientConfig.getHttpProxy(), samanClientConfig.getHttpProxyPort());
			}

			URL wsdlURL = new URL(samanClientConfig.getWsdlUrl());
			QName qname = new QName("urn:Foo", "ReferencePayment");

			ServiceFactory serviceFactory = new ServiceFactory();
			Service paymentService = serviceFactory.createService(qname);
			return new PaymentIFBindingSoapStub(wsdlURL, paymentService);

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
	}

	/**
	 * Refunding method for Non-Saman Bank credit card Payment via SRTM
	 * 
	 */
	public String reverseTransactionSRTM(com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO reverseRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::reverseTransactionSRTM()] Begin");
		String responseStr = null;
		try {
			RefundReportWSDLBindingStub serviceStub = getSRTMClientStub();
			try {

				responseStr = serviceStub.refundReport(reverseRequest.getMerchantId(), reverseRequest.getMerchantPassword(),
						reverseRequest.getRefundAmount().doubleValue(), reverseRequest.getRefundDateTimeStr(),
						reverseRequest.getTransResNo(), reverseRequest.getReferenceNo(), reverseRequest.getRefundResNo(),
						reverseRequest.getRefundReferenceNo(), reverseRequest.getRefundErrorCode(),
						reverseRequest.getRefundErrorDesc(), reverseRequest.getCusEmail(), reverseRequest.getCusCell());

			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::reverseTransactionSRTM()] End");
		return responseStr;

	}
	
	public SRTMRefundServiceResponseDTO reverseTransactionRegSRTM(com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO reverseRequest ,String transRefNum , Long tptID)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::regReverseTransactionSRTM()] Begin");
		ResponseModel responseStrforRFNDReg = null;
		String userName =  reverseRequest.getRefundUser();
		String password =  reverseRequest.getRefundPassword();
		Long merchentID =  new Long(reverseRequest.getMerchantId());
		try {
			BasicHttpBinding_IsrvRefundStub serviceStub = getSRTMV2ClientStub();
			try {
				//(userName, password, refNum, resNum, transactionTermId, refundTermId,
				//amount, requestId, exeTime, email, cellNumber});
				
				responseStrforRFNDReg = serviceStub.refund_Reg(userName, password,
						reverseRequest.getReferenceNo(), transRefNum,
						merchentID, merchentID,
						reverseRequest.getRefundAmount().longValue(),tptID,
						reverseRequest.getExeTime(), reverseRequest.getCusEmail(), reverseRequest.getCusCell());
				
				
			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::reverseTransactionRegSRTM()] End");
		return getSRTMRefundServiceResponse(responseStrforRFNDReg);

	}
	
	public SRTMRefundServiceResponseDTO reverseTransactionExcSRTM(com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO reverseRequest,Long refundRegID)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::regReverseTransactionSRTM()] Begin");
		ResponseModel responseStrforRFNDExc = null;
		String userName =  reverseRequest.getRefundUser();
		String password =  reverseRequest.getRefundPassword();
		Long merchentID =  new Long(reverseRequest.getMerchantId());
		try {
			BasicHttpBinding_IsrvRefundStub serviceStub = getSRTMV2ClientStub();
			try {	
				responseStrforRFNDExc =  serviceStub.refund_Exec(userName, password, refundRegID, TypeRefundAction.Approve, merchentID);

			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::reverseTransactionExcSRTM()] End");
		return getSRTMRefundServiceResponse(responseStrforRFNDExc);

	}

	/**
	 * Refund request status check method for Non-Saman Bank credit card Payment via SRTM
	 * 
	 */
	public com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundStatusResponseDTO getSRTMRefundReportStatus(
			com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO reverseRequest)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[SamanWebServicesImpl::getSRTMRefundReportStatus()] Begin");

		SRTMRefundStatusResponseDTO response = new SRTMRefundStatusResponseDTO();

		try {
			RefundReportWSDLBindingStub serviceStub = getSRTMClientStub();
			try {
				String responseStr = null;
				responseStr = serviceStub.refundReport(reverseRequest.getMerchantId(), reverseRequest.getMerchantPassword(),
						reverseRequest.getRefundAmount(), reverseRequest.getRefundDateTimeStr(), reverseRequest.getTransResNo(),
						reverseRequest.getReferenceNo(), reverseRequest.getRefundResNo(), reverseRequest.getRefundReferenceNo(),
						reverseRequest.getRefundErrorCode(), reverseRequest.getRefundErrorDesc(), reverseRequest.getCusEmail(),
						reverseRequest.getCusCell());

				response.setReferenceNo(reverseRequest.getReferenceNo());

				if (responseStr != null) {

					response.setFullRespStr(responseStr);

					String ressultArray[] = responseStr.split(",");
					if (ressultArray.length > 1) {
						String refundRegArray[] = ressultArray[0].split(";");
						String refundTraceArray[] = ressultArray[1].split(";");

						response.setRefundReg(refundRegArray[0]);
						response.setRefundRegDesc(refundRegArray[1]);
						response.setRefundTrace(refundTraceArray[0]);
						response.setRefundTraceDesc(refundTraceArray[1]);
					}
				}

			} catch (RemoteException e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (samanClientConfig.isUseProxy())
				setProxy("", "");
		}

		log.debug("[SamanWebServicesImpl::getSRTMRefundReportStatus()] End");
		return response;

	}

	private RefundReportWSDLBindingStub getSRTMClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (samanClientConfig.isUseProxy()) {
				setProxy(samanClientConfig.getHttpProxy(), samanClientConfig.getHttpProxyPort());
			}

			URL wsdlURL = new URL(samanClientConfig.getSrtmWsdlUrl());
			QName qname = new QName("urn:RefundReportWSDL_pkg", "RefundReportWSDL");

			ServiceFactory serviceFactory = new ServiceFactory();
			Service paymentService = serviceFactory.createService(qname);
			return new RefundReportWSDLBindingStub(wsdlURL, paymentService);

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}
	
	private BasicHttpBinding_IsrvRefundStub getSRTMV2ClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (samanClientConfig.isUseProxy()) {
				setProxy(samanClientConfig.getHttpProxy(), samanClientConfig.getHttpProxyPort());
			}

			URL wsdlURL = new URL(samanClientConfig.getSrtmWsdlUrl());
			QName qname = new QName("http://tempuri.org/", "SrvRefund");

			ServiceFactory serviceFactory = new ServiceFactory();
			Service paymentService = serviceFactory.createService(qname);
			return new BasicHttpBinding_IsrvRefundStub(wsdlURL, paymentService);

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}
	
	private SRTMRefundServiceResponseDTO getSRTMRefundServiceResponse(ResponseModel responseModule){
		SRTMRefundServiceResponseDTO rfndRes =  new SRTMRefundServiceResponseDTO();
		rfndRes.setActionName(responseModule.getActionName());
		rfndRes.setDescription(responseModule.getDescription());
		rfndRes.setErrorCode(responseModule.getErrorCode().getValue());
		rfndRes.setErrorMessage(responseModule.getErrorMessage());
		rfndRes.setReferenceId(responseModule.getReferenceId());
		rfndRes.setRequestStatus(responseModule.getRequestStatus());
		
		return rfndRes;
	}

}