package com.isa.thinair.wsclient.core.service;

import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

public interface InsuranceTemplate extends DefaultWebserviceClient {

	List<InsuranceResponse> quote(IInsuranceRequest iQuoteRequest) throws ModuleException;

	List<InsuranceResponse> sell(List<IInsuranceRequest> iRequests) throws ModuleException;

}
