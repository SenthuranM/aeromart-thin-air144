package com.isa.thinair.wsclient.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * @author Mohamed Nasly
 */
@Local
public interface WSClientBDLocalImpl extends WSClientBD {

}
