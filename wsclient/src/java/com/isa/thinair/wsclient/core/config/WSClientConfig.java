package com.isa.thinair.wsclient.core.config;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nasly/Byorn/mekanayake
 * @isa.module.config-bean
 */
public class WSClientConfig extends DefaultModuleConfig {

	private Map<String, String> wsExtPayTnxStatusMap = null;

	private Map<String, String> serviceURLsMap = null;

	private int flightsPerAerlt;

	private Collection<String> securityEnabledURLs = null;

	private Collection<String> proxyEnabledURLs = null;

	private AIGClientConfig aigClientConfig;

	private ACEClientConfig aceClientConfig;

	private TuneClientConfig tuneClientConfig;

	private RAKClientConfig rakClientConfig;

	private SamanClientConfig samanClientConfig;

	private PayfortClientConfig payfortClientConfig;

	private LMSClientConfig lmsClientConfig;

	private ParsianClientConfig parsianClientConfig;

	private Map<String, String> serviceAgentsMap = null;

	private Map<String, String> customTimeOutSetUrls = null;

	private String dcsWsUrl = null;

	private String dumpMessages = null;

	private String typeBWsUrl = null;

	private String typeBWsUserName = null;

	private String typeBWsPassword = null;

	private BehpardakhtClientConfig behpardakhtClientConfig;

	private AmeriaClientConfig ameriaClientConfig;
	
	private TapGoSellConfig tapGoSellConfig;

	private Map<String, String> getWsExtPayTnxStatusMap() {
		return wsExtPayTnxStatusMap;
	}

	public void setWsExtPayTnxStatusMap(Map<String, String> wsExtPayTnxStatusMap) {
		this.wsExtPayTnxStatusMap = wsExtPayTnxStatusMap;
	}

	public Map<String, String> getServiceURLsMap() {
		return serviceURLsMap;
	}

	public void setServiceURLsMap(Map<String, String> serviceURLsMap) {
		this.serviceURLsMap = serviceURLsMap;
	}

	public int getFlightsPerAerlt() {
		return flightsPerAerlt;
	}

	public void setFlightsPerAerlt(int flightsPerAerlt) {
		this.flightsPerAerlt = flightsPerAerlt;
	}

	public Map<String, String> getCustomTimeOutSetUrls() {
		return customTimeOutSetUrls;
	}

	public void setCustomTimeOutSetUrls(Map<String, String> customTimeOutSetUrls) {
		this.customTimeOutSetUrls = customTimeOutSetUrls;
	}

	// ------------------------------Derived Methods------------------------------

	/**
	 * Returns WS External Payment Transaction status mapped to AA External Payment Transaction status.
	 * 
	 * @param aaExtPayTnxStatus
	 * @return
	 */
	public String getWSExtPayTnxStatus(String aaExtPayTnxStatus) {
		String wsExtPayTnxStatus = null;
		if (getWsExtPayTnxStatusMap() != null) {
			wsExtPayTnxStatus = (String) getWsExtPayTnxStatusMap().get(aaExtPayTnxStatus);
		}
		return wsExtPayTnxStatus;
	}

	/**
	 * Returns AA External Payment Transaction status mapped to WS External Payment Transaction status.
	 * 
	 * @param wsExtPayTnxStatus
	 * @return
	 */
	public String getAAExtPayTnxStatus(String wsExtPayTnxStatus) {
		String aaExtPayTnxStatus = null;
		if (getWsExtPayTnxStatusMap() != null) {
			String tmpAAStatus;
			for (Iterator statusIt = getWsExtPayTnxStatusMap().keySet().iterator(); statusIt.hasNext();) {
				tmpAAStatus = (String) statusIt.next();
				if (wsExtPayTnxStatus.equals(getWsExtPayTnxStatusMap().get(tmpAAStatus))) {
					aaExtPayTnxStatus = tmpAAStatus;
					break;
				}
			}
		}
		return aaExtPayTnxStatus;
	}

	/**
	 * @return Returns the securityEnabledURLs.
	 */
	public Collection<String> getSecurityEnabledURLs() {
		return securityEnabledURLs;
	}

	/**
	 * @param securityEnabledURLs
	 *            The securityEnabledURLs to set.
	 */
	public void setSecurityEnabledURLs(Collection<String> securityEnabledURLs) {
		this.securityEnabledURLs = securityEnabledURLs;
	}

	public AIGClientConfig getAigClientConfig() {
		return aigClientConfig;
	}

	public void setAigClientConfig(AIGClientConfig aigClientConfig) {
		this.aigClientConfig = aigClientConfig;
	}

	public ACEClientConfig getAceClientConfig() {
		return aceClientConfig;
	}

	public void setAceClientConfig(ACEClientConfig aceClientConfig) {
		this.aceClientConfig = aceClientConfig;
	}

	public Map<String, String> getServiceAgentsMap() {
		return serviceAgentsMap;
	}

	public void setServiceAgentsMap(Map<String, String> serviceAgentsMap) {
		this.serviceAgentsMap = serviceAgentsMap;
	}

	public Collection<String> getProxyEnabledURLs() {
		return proxyEnabledURLs;
	}

	public void setProxyEnabledURLs(Collection<String> proxyEnabledURLs) {
		this.proxyEnabledURLs = proxyEnabledURLs;
	}

	/**
	 * @return the rakClientConfig
	 */
	public RAKClientConfig getRakClientConfig() {
		return rakClientConfig;
	}

	/**
	 * @param rakClientConfig
	 *            the rakClientConfig to set
	 */
	public void setRakClientConfig(RAKClientConfig rakClientConfig) {
		this.rakClientConfig = rakClientConfig;
	}

	public SamanClientConfig getSamanClientConfig() {
		return samanClientConfig;
	}

	public void setSamanClientConfig(SamanClientConfig samanClientConfig) {
		this.samanClientConfig = samanClientConfig;
	}

	public LMSClientConfig getLmsClientConfig() {
		return lmsClientConfig;
	}

	public void setLmsClientConfig(LMSClientConfig lmsClientConfig) {
		this.lmsClientConfig = lmsClientConfig;
	}

	public String getDcsWsUrl() {
		return dcsWsUrl;
	}

	public void setDcsWsUrl(String dcsWsUrl) {
		this.dcsWsUrl = dcsWsUrl;
	}

	public String getDumpMessages() {
		return dumpMessages;
	}

	public void setDumpMessages(String dumpMessages) {
		this.dumpMessages = dumpMessages;
	}

	public TuneClientConfig getTuneClientConfig() {
		return tuneClientConfig;
	}

	public void setTuneClientConfig(TuneClientConfig tuneClientConfig) {
		this.tuneClientConfig = tuneClientConfig;
	}

	public String getTypeBWsUrl() {
		return typeBWsUrl;
	}

	public void setTypeBWsUrl(String typeBWsUrl) {
		this.typeBWsUrl = typeBWsUrl;
	}

	public String getTypeBWsUserName() {
		return typeBWsUserName;
	}

	public String getTypeBWsPassword() {
		return typeBWsPassword;
	}

	public void setTypeBWsUserName(String typeBWsUserName) {
		this.typeBWsUserName = typeBWsUserName;
	}

	public void setTypeBWsPassword(String typeBWsPassword) {
		this.typeBWsPassword = typeBWsPassword;
	}

	public BehpardakhtClientConfig getBehpardakhtClientConfig() {
		return behpardakhtClientConfig;
	}

	public void setBehpardakhtClientConfig(BehpardakhtClientConfig behpardakhtClientConfig) {
		this.behpardakhtClientConfig = behpardakhtClientConfig;
	}

	public PayfortClientConfig getPayfortClientConfig() {
		return payfortClientConfig;
	}

	public void setPayfortClientConfig(PayfortClientConfig payfortClientConfig) {
		this.payfortClientConfig = payfortClientConfig;
	}

	
	public ParsianClientConfig getParsianClientConfig() {
		return parsianClientConfig;
	}

	public void setParsianClientConfig(ParsianClientConfig parsianClientConfig) {
		this.parsianClientConfig = parsianClientConfig;
	}

	public AmeriaClientConfig getAmeriaClientConfig() {
		return ameriaClientConfig;
	}

	public void setAmeriaClientConfig(AmeriaClientConfig ameriaClientConfig) {
		this.ameriaClientConfig = ameriaClientConfig;
	}

	public TapGoSellConfig getTapGoSellConfig() {
		return tapGoSellConfig;
	}

	public void setTapGoSellConfig(TapGoSellConfig tapGoSellConfig) {
		this.tapGoSellConfig = tapGoSellConfig;
	}
	
}
