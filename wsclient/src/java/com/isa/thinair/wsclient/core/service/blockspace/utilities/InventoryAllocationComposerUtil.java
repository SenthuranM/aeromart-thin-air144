package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.reporting.api.model.InventoryAllocationDTO;
import com.isa.thinair.reporting.api.model.InventoryAllocationTO;
import com.isa.thinair.wsclient.core.util.CommonUtil;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;
import com.isaaviation.thinair.webservices.api.airinventory.AASegmentSeatInventoriesType;

/**
 * @author Ruckman Colins S.
 * @since Feb 18, 2008 , 10:36:11 AM.
 */
public class InventoryAllocationComposerUtil {

	/**
	 * build webswervice search criteria using InventoryAllocationTO
	 * 
	 * @param allocationTO
	 * @return AAFlightInventorySearchRQ flightInventorySearchRQ
	 * @throws ModuleException
	 */
	public static AAFlightInventorySearchRQ compose(InventoryAllocationTO allocationTO) throws ModuleException {

		AAFlightInventorySearchRQ flightInventorySearchRQ = new AAFlightInventorySearchRQ();
		flightInventorySearchRQ.setCabinClass(allocationTO.getCabinClass());
		flightInventorySearchRQ.setFlightNumber(allocationTO.getFlightNumber());
		flightInventorySearchRQ.setDepatureDate(CommonUtil.getXMLGregorianCalendar(allocationTO.getDepatureDate()));
		flightInventorySearchRQ.setMessageDate(CommonUtil.parse(Calendar.getInstance().getTime()));
		flightInventorySearchRQ.setChannelId(Integer.toString(SalesChannelsUtil.SALES_CHANNEL_INTERLINED));

		return flightInventorySearchRQ;
	}

	/**
	 * @param dataRS
	 * @return list of InventoryAllocationDTO
	 */
	public static List extract(AAFlightInventoryDataRS dataRS, String carrierCode) {

		InventoryAllocationDTO dto = null;
		List inventioryAllocList = dataRS.getSegmentSeatInventories();
		List allocations = new ArrayList();

		for (Iterator inventioryAllocIter = inventioryAllocList.iterator(); inventioryAllocIter.hasNext();) {
			AASegmentSeatInventoriesType allocation = (AASegmentSeatInventoriesType) inventioryAllocIter.next();
			dto = new InventoryAllocationDTO();

			int actAllocatedSeats = allocation.getAllocatedSeats() + allocation.getOversellSeats()
					- allocation.getCurtailedSeats();
			dto.setAllocation(actAllocatedSeats);

			dto.setOnHoldSeat(allocation.getOnHoldSeats());
			dto.setSold_seat(allocation.getSeatsSold());
			dto.setAvailableSeat(allocation.getAvailableSeats());
			dto.setMarkettingCarrierCode(carrierCode);

			allocations.add(dto);
		}
		return allocations;
	}
}
