package com.isa.thinair.wsclient.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.dto.InsuranceProductConfig;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.core.service.InsuranceTemplate;
import com.isa.thinair.wsclient.core.service.ace.AceWebServiceImpl;
import com.isa.thinair.wsclient.core.service.aig.AigBL;
import com.isa.thinair.wsclient.core.service.bd.InsuranceClientBDLocal;
import com.isa.thinair.wsclient.core.service.bd.InsuranceClientBDRemote;
import com.isa.thinair.wsclient.core.service.ccc.CCCBL;
import com.isa.thinair.wsclient.core.service.tune.TuneWebServiceImpl;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;

@Stateless
@RemoteBinding(jndiBinding = "InsuranceClientService.remote")
@LocalBinding(jndiBinding = "InsuranceClientService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class InsuranceClientServiceBean extends PlatformBaseSessionBean implements InsuranceClientBDLocal,
		InsuranceClientBDRemote {

	@Override
	public List<InsuranceResponse> quoteInsurancePolicy(IInsuranceRequest insuranceRequest) throws ModuleException {

		List<InsuranceResponse> insuranceResponses = new ArrayList<InsuranceResponse>();

		List<INSURANCEPROVIDER> insuranceProviders = AppSysParamsUtil.getInsuranceProvidersList();
		if (insuranceProviders != null && !insuranceProviders.isEmpty()) {
			Iterator<INSURANCEPROVIDER> providers = insuranceProviders.iterator();

			while (providers.hasNext()) {

				InsuranceTemplate template = null;
				INSURANCEPROVIDER provider = providers.next();

				if (provider == (INSURANCEPROVIDER.TUNE)) {
					template = new TuneWebServiceImpl();
				} else if (provider == (INSURANCEPROVIDER.AIG)) {
					template = new AigBL();
				} else if (provider == (INSURANCEPROVIDER.ACE)) {
					template = new AceWebServiceImpl();
				} else if (provider == (INSURANCEPROVIDER.CCC)) {
					template = new CCCBL();
				}

				// selection logic of provider, should be introduced here if there are mutiple providers active in
				// future

				List<InsuranceResponse> responses = template.quote(insuranceRequest);
				if (responses != null && !responses.isEmpty()) {
					for (InsuranceResponse response : responses) {
						insuranceResponses.add(response);
					}
				}

			}
		}

		return insuranceResponses;
	}

	@Override
	public List<InsuranceResponse> sellInsurancePolicy(List<IInsuranceRequest> insuranceRequests) throws ModuleException {
		List<InsuranceResponse> insuranceResponses = new ArrayList<InsuranceResponse>();

		List<INSURANCEPROVIDER> insuranceProviders = AppSysParamsUtil.getInsuranceProvidersList();

		if (isNotEmpty(insuranceProviders) && isNotEmpty(insuranceRequests)) {
			Iterator<INSURANCEPROVIDER> providers = insuranceProviders.iterator();

			Map<String, List<IInsuranceRequest>> requestsByProvider = filterByProvider(insuranceRequests);

			while (providers.hasNext()) {

				INSURANCEPROVIDER provider = providers.next();

				InsuranceTemplate template = null;
				List<IInsuranceRequest> providerRequests = null;

				if (provider == INSURANCEPROVIDER.TUNE) {
					template = new TuneWebServiceImpl();
				} else if (provider == INSURANCEPROVIDER.AIG) {
					template = new AigBL();
				} else if (provider == INSURANCEPROVIDER.ACE) {
					template = new AceWebServiceImpl();
				} else if (provider == INSURANCEPROVIDER.CCC) {
					template = new AceWebServiceImpl();
				}

				if (requestsByProvider.containsKey(provider.toString())) {
					providerRequests = requestsByProvider.get(provider.toString());
				}

				if (isNotEmpty(providerRequests)) {
					List<InsuranceResponse> provderResponses = template.sell(insuranceRequests);
					insuranceResponses.addAll(provderResponses);
				}

			}
		}

		return insuranceResponses;
	}

	private Map<String, List<IInsuranceRequest>> filterByProvider(List<IInsuranceRequest> iRequests) {

		Map<String, List<IInsuranceRequest>> filteredRequests = new HashMap<String, List<IInsuranceRequest>>();
		Iterator<IInsuranceRequest> it = iRequests.iterator();

		while (it.hasNext()) {
			IInsuranceRequest iRequest = it.next();
			Integer insuranceId = iRequest.getInsuranceId();
			InsuranceProductConfig productConfig = InsuranceUtil.getInsuranceProvider(insuranceId);

			if (productConfig != null) {
				String providerName = productConfig.getProviderName();
				if (filteredRequests.containsKey(providerName)) {
					List<IInsuranceRequest> requests = filteredRequests.get(providerName);
					requests.add(iRequest);
				} else {
					List<IInsuranceRequest> requests = new ArrayList<IInsuranceRequest>();
					requests.add(iRequest);
					filteredRequests.put(providerName, requests);
				}

			}
		}

		return filteredRequests;
	}

	private <T> boolean isNotEmpty(Collection<T> collection) {
		return collection != null && !collection.isEmpty();
	}

}
