package com.isa.thinair.wsclient.core.service.lms.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;
import com.isa.thinair.wsclient.core.service.lms.base.common.LMSCommonService;
import com.isa.thinair.wsclient.core.service.lms.base.member.LMSMemberWebService;
import com.isa.thinair.wsclient.core.service.lms.base.reward.LMSRewardWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.common.LMSCommonServiceAeroRewardImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.member.LMSMemberWebServiceAeroRewardImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.reward.LMSRewardWebServiceAeroRewardImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.common.LMSCommonServiceGravtyImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.member.LMSMemberWebServiceGravtyImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.reward.LMSRewardWebServiceGravtyImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.common.LMSCommonServiceSmartButtonImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.member.LMSMemberWebServiceSmartButtonImpl;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.reward.LMSRewardWebServiceSmartButtonImpl;

public class LMSServiceFactory {

	private static Log log = LogFactory.getLog(LMSServiceFactory.class);

	private static final LMS_SERVICE_PROVIDER serviceProvider = WSClientModuleUtils.getModuleConfig().getLmsClientConfig()
			.getDefaultLmsProviderCode();

	public static LMSMemberWebService getLMSMemberWebService() throws ModuleException {

		LMSMemberWebService lmsMemberWebService;

		if (serviceProvider.equals(LMS_SERVICE_PROVIDER.SMART_BUTTON)) {
			lmsMemberWebService = new LMSMemberWebServiceSmartButtonImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.AERO_REWARD)) {
			lmsMemberWebService = new LMSMemberWebServiceAeroRewardImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.GRAVTY)) {
			lmsMemberWebService = new LMSMemberWebServiceGravtyImpl();
		} else {
			log.error("Loyalty service unsupported Lms service provider code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}

		return lmsMemberWebService;
	}

	public static LMSRewardWebService getLMSRewardWebService() throws ModuleException {

		LMSRewardWebService lmsRewardWebService;

		if (serviceProvider.equals(LMS_SERVICE_PROVIDER.SMART_BUTTON)) {
			lmsRewardWebService = new LMSRewardWebServiceSmartButtonImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.AERO_REWARD)) {
			lmsRewardWebService = new LMSRewardWebServiceAeroRewardImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.GRAVTY)) {
			lmsRewardWebService = new LMSRewardWebServiceGravtyImpl();
		} else {
			log.error("Loyalty service unsupported Lms service provider code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}

		return lmsRewardWebService;
	}

	public static LMSCommonService getLMSCommonService() throws ModuleException {

		LMSCommonService lmsCommonService;
		if (serviceProvider.equals(LMS_SERVICE_PROVIDER.SMART_BUTTON)) {
			lmsCommonService = new LMSCommonServiceSmartButtonImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.AERO_REWARD)) {
			lmsCommonService = new LMSCommonServiceAeroRewardImpl();
		} else if (serviceProvider.equals(LMS_SERVICE_PROVIDER.GRAVTY)) {
			lmsCommonService = new LMSCommonServiceGravtyImpl();
		} else {
			log.error("Loyalty service unsupported Lms service provider code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}
		return lmsCommonService;
	}

}
