package com.isa.thinair.wsclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.wsclient.api.service.DCSClientBD;

@Remote
public interface DCSClientBDRemote extends DCSClientBD {

}
