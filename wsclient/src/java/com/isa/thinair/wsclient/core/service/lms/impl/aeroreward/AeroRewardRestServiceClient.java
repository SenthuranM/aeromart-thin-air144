package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.http.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;

public class AeroRewardRestServiceClient<RESPONSE, REQUEST> {

	public static final String ERROR_CODE = "errorCode";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String COOKIE = "Cookie";

	private static final String PROXY_HOST = CommonsServices.getGlobalConfig().getHttpProxy();
	private static final int PROXY_PORT = CommonsServices.getGlobalConfig().getHttpPort();
	private static final boolean IS_USE_PROXY = LMSAeroRewardWebServiceInvoker.aeroRewardConfig.isApplyProxy();

	private static Log log = LogFactory.getLog(AeroRewardRestServiceClient.class);

	private Class<RESPONSE> responseClass;
	private final String url;

	private static Gson gson = new Gson();

	public AeroRewardRestServiceClient(Class<RESPONSE> responseClass, String url) {
		this.url = url;
		this.responseClass = responseClass;
	}

	public RESPONSE post(REQUEST request) throws ModuleException {

		HttpClient httpClient = getHttpClient();

		try {

			HttpPost post = getHttpPostRequest(request);
			HttpResponse response = httpClient.execute(post);

			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				handleErrorResponse(response);
			}

			return (RESPONSE) gson.fromJson(EntityUtils.toString(response.getEntity()), responseClass);

		} catch (IOException ce) {
			
			log.error(ce.getMessage());
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	private HttpClient getHttpClient() {
		HttpClient httpClient;
		if (IS_USE_PROXY) {
			HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
			httpClient = HttpClientBuilder.create().setProxy(proxy).build();
		} else {
			httpClient = HttpClientBuilder.create().build();
		}
		return httpClient;
	}

	private HttpPost getHttpPostRequest(REQUEST request) throws UnsupportedEncodingException {
		StringEntity postData = new StringEntity(gson.toJson(request));

		HttpPost post = new HttpPost(url);
		post.setEntity(postData);
		post.setHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		updateCookieDetails(post);
		return post;
	}

	private void handleErrorResponse(HttpResponse response) throws ModuleException {

		try {
			String errorCode = gson.fromJson(EntityUtils.toString(response.getEntity()), AeroRewardDto.RewardsError.class)
					.getErrorCode();
			log.error(errorCode);
			throw new ModuleException(errorCode, "reward.service.client");

		} catch (JsonSyntaxException | ParseException | IOException e) {
			log.error(e.getMessage());
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}
	}

	private void updateCookieDetails(HttpPost post) {
		String cookie = "ls.tenantCode=" + LMSAeroRewardWebServiceInvoker.aeroRewardConfig.getTenantCode() + ";ls.authToken="
				+ LMSAeroRewardWebServiceInvoker.aeroRewardConfig.getAuthToken() + ";";
		post.setHeader("Cookie", cookie);
	}

}
