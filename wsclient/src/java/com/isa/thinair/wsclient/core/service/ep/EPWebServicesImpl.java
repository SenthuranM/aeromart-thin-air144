package com.isa.thinair.wsclient.core.service.ep;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.PNRTransactionsStatusCheckRQ;
import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.PNRTransactionsStatusCheckRS;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.CommonUtil;

public class EPWebServicesImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_EP;

	private static Log log = LogFactory.getLog(EPWebServicesImpl.class);

	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	/**
	 * Returns the status at DIB for a given set of transactions.
	 * 
	 * @param pnrExtTransactionsTO
	 * @return
	 */
	public PNRExtTransactionsTO getTransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO) throws ModuleException {
		org.tempuri.ArrayOfAAPNRPaymentTransactionType1 wsPNRTnxs = (org.tempuri.ArrayOfAAPNRPaymentTransactionType1) ExternalWebServiceInvoker
				.invokeService(SERVICE_PROVIDER, "getTransactionStatus", true, new Param(
						preparePNRTransactionsStatusCheckRQ(pnrExtTransactionsTO)));
		PNRExtTransactionsTO aaPNRTnxs = null;
		if (wsPNRTnxs != null) {
			aaPNRTnxs = getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(wsPNRTnxs);
		}
		return aaPNRTnxs;
	}

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at Emirates Post.
	 * 
	 * @param pnrExtTransactionsTOs
	 *            collection of PNRExtTransactionsTO
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 */
	public void requestDailyTnxReconcilation(Date startTimestamp, Date endTimestamp) throws ModuleException {
		log.debug("BEGIN:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
		String agencyCode = ExternalWebServiceInvoker.getServiceAgent(SERVICE_PROVIDER);
		ExternalWebServiceInvoker.invokeService(getServiceProvider(null), "getReconciliationReport", false, new Param(
				prepareTnxsForRecon(startTimestamp, endTimestamp, agencyCode)));
		log.debug("END:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
	}

	/**
	 * Prepares {@link PNRExtTransactionsTO} from {@link PNRTransactionsStatusCheckRS}
	 * 
	 * @param wsPNRTnxs
	 * @return
	 */
	private static PNRExtTransactionsTO getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(
			org.tempuri.ArrayOfAAPNRPaymentTransactionType1 pnrTransactionsStatusCheckRS) throws ModuleException {
		PNRExtTransactionsTO aaPNRTnxs = null;

		if (pnrTransactionsStatusCheckRS != null && pnrTransactionsStatusCheckRS.getAAPNRPaymentTransactionType1().size() > 0) {
			aaPNRTnxs = new PNRExtTransactionsTO();
			org.tempuri.AAPNRPaymentTransactionType1 wsPNRTnxs = pnrTransactionsStatusCheckRS.getAAPNRPaymentTransactionType1()
					.get(0);
			aaPNRTnxs.setPnr(wsPNRTnxs.getPNRNo());

			for (Iterator it = wsPNRTnxs.getPaymentTransaction().iterator(); it.hasNext();) {
				org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction1 wsTnx = (org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction1) it
						.next();

				ExternalPaymentTnx aaTnx = new ExternalPaymentTnx();

				aaTnx.setPnr(wsPNRTnxs.getPNRNo());
				aaTnx.setBalanceQueryKey(wsTnx.getAARefNo());
				aaTnx.setAmount(AccelAeroCalculator.parseBigDecimal(wsTnx.getAmount()));
				aaTnx.setChannel(wsTnx.getBankChannel());
				aaTnx.setExternalPayStatus(WSClientModuleUtils.getModuleConfig().getAAExtPayTnxStatus(wsTnx.getBankStatus()));
				aaTnx.setExternalPayId(wsTnx.getBankRefNo());
				aaTnx.setExternalTnxEndTimestamp(wsTnx.getTimestamp().toGregorianCalendar().getTime());

				aaPNRTnxs.addExtPayTransactions(aaTnx);
			}
		}
		return aaPNRTnxs;
	}

	/**
	 * Prepares {@link PNRTransactionsStatusCheckRQ} from {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static org.tempuri.AAPNRPaymentTransactionType2 preparePNRTransactionsStatusCheckRQ(PNRExtTransactionsTO aaPNRTnxs)
			throws ModuleException {
		org.tempuri.AAPNRPaymentTransactionType2 pnrTransactionsStatusCheckRQ = new org.tempuri.AAPNRPaymentTransactionType2();
		pnrTransactionsStatusCheckRQ.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction2 wsExtTnx = new org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction2();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()));

			pnrTransactionsStatusCheckRQ.getPaymentTransaction().add(wsExtTnx);
		}
		return pnrTransactionsStatusCheckRQ;
	}

	/**
	 * Prepares {@link org.tempuri.AAPNRPaymentTransactionType} from {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static org.tempuri.AAPNRPaymentTransactionType getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO aaPNRTnxs)
			throws ModuleException {
		log.debug("BEGIN getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		org.tempuri.AAPNRPaymentTransactionType wsPNRTnxs = new org.tempuri.AAPNRPaymentTransactionType();
		wsPNRTnxs.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			if (log.isDebugEnabled())
				log.debug(aaExtTnx.getSummary().toString());

			org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction wsExtTnx = new org.tempuri.AAPNRPaymentTransactionTypePaymentTransaction();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()));

			wsPNRTnxs.getPaymentTransaction().add(wsExtTnx);
		}
		log.debug("END getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		return wsPNRTnxs;
	}

	/**
	 * Set external payment transactions done between given start and end timestamps.
	 * 
	 * @param reconRQ
	 * @param startTimestamp
	 * @param endTimestamp
	 * @throws ModuleException
	 */
	private static org.tempuri.TransactionsReconRQ prepareTnxsForRecon(Date startTimestamp, Date endTimestamp, String agencyCode)
			throws ModuleException {
		org.tempuri.TransactionsReconRQ reconRQ = new org.tempuri.TransactionsReconRQ();
		reconRQ.setStartTimestamp(CommonUtil.parse(startTimestamp));
		reconRQ.setEndTimestamp(CommonUtil.parse(endTimestamp));

		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setStartTimestamp(startTimestamp);
		criteriaDTO.setEndTimestamp(endTimestamp);
		criteriaDTO.setAgentCode(agencyCode);
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);

		Map pnrTnxsMap = WSClientModuleUtils.getResQueryBD().getExtPayTransactions(criteriaDTO);

		if (pnrTnxsMap != null) {
			org.tempuri.ArrayOfAAPNRPaymentTransactionType arrPnrPaymentTransactionType = new org.tempuri.ArrayOfAAPNRPaymentTransactionType();
			for (Iterator pnrsIt = pnrTnxsMap.keySet().iterator(); pnrsIt.hasNext();) {
				PNRExtTransactionsTO pnrTnxs = (PNRExtTransactionsTO) pnrTnxsMap.get(pnrsIt.next());
				if (pnrTnxs != null && pnrTnxs.getExtPayTransactions() != null) {

					arrPnrPaymentTransactionType.getAAPNRPaymentTransactionType().add(getWSPNRExtTnxsForTnxReconRQ(pnrTnxs));
				}
			}
			reconRQ.setPNRPaymentTransactions(arrPnrPaymentTransactionType);
		}
		return reconRQ;
	}

}
