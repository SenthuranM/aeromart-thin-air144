package com.isa.thinair.wsclient.core.service.ccc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationInsurancePremium;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.dao.InsurancePremiumDAO;
import com.isa.thinair.wsclient.core.service.InsuranceTemplate;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;

/**
 * 
 * @author pkarunanayake
 * 
 */
public class CCCBL implements InsuranceTemplate  {

	private static final Log log = LogFactory.getLog(CCCBL.class);

	public enum RequestType {
		SELL, QUOTE, RESELL
	};

	public static final String CREATE_INS_CODE = "01";
	
	@Override
	public List<InsuranceResponse> quote(IInsuranceRequest iQuoteRequest)
			throws ModuleException {
		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		InsuranceResponse insResponse = null;
		ModuleException mex = null;
		List<ReservationInsurance> insurance = null;
		try {
			InsuranceRequestAssembler insuranceRequestAssembler = (InsuranceRequestAssembler) iQuoteRequest;
			InsuranceCountryTO aigCountryTO = InsuranceUtil.getCountryTO((insuranceRequestAssembler).getInsureSegment()
					.getFromAirportCode());
			BigDecimal totalTicketPrice = insuranceRequestAssembler.getTotalTicketPrice();
			Map<Integer, ReservationInsurancePremium> insurancePremiumCol = InsurancePremiumDAO.getInsurancePremiumData();
			Map<InsuranceTypes, BigDecimal> insTypeCharges = null;
			// Insurance view only functionality
			if (insuranceRequestAssembler.getPnr() != null) {
				insurance = WSClientModuleUtils.getResQueryBD().getReservationInsuranceByPnr(insuranceRequestAssembler.getPnr());
				insTypeCharges = InsuranceUtil.getInsCharges(insurance);
			} else
			// CCC expect as one record
			if (insurancePremiumCol != null && insurancePremiumCol.size() == 1) {
				insTypeCharges = InsuranceUtil.getInsCharges(insurancePremiumCol);
			}

			BigDecimal quotedTotalPremium = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (totalTicketPrice != null) {
				insTypeCharges = calculateTotalPremium(insTypeCharges, totalTicketPrice);
			}

			BigDecimal quotedTotalTaxAmount = new BigDecimal(0);
			BigDecimal quotedTotalNetAmount = quotedTotalPremium;
			BigDecimal baseCurrencyAmounts = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (insTypeCharges != null && totalTicketPrice != null) {
				insResponse = new InsuranceResponse();
				insResponse.setSuccess(true);
				insResponse.setErrorCode("0");
				insResponse.setErrorMessage("OK");
				insResponse.setMessageId("");
				insResponse.setTotalPremiumAmount(baseCurrencyAmounts);
				insResponse.setQuotedTotalPremiumAmount(quotedTotalPremium);
				insResponse.setQuotedCurrencyCode(aigCountryTO.getCurrencyCode());
				insResponse.setEuropianCountry(aigCountryTO.isEuropianCountry());

				insTypeCharges = InsuranceUtil.getBaseCurrencyAmount(insTypeCharges, aigCountryTO.getCurrencyCode());

				insResponse.setInsTypeCharges(insTypeCharges);
			} else {
				insResponse = new InsuranceResponse(false, "Error", "NA", "", null, quotedTotalPremium,
						AppSysParamsUtil.getBaseCurrency(), aigCountryTO.isEuropianCountry());

				if (log.isDebugEnabled()) {
					log.debug("There is an error occurred while calculating total premium for CCC insurance");
				}
			}

			if (log.isDebugEnabled()) {
				log.debug("End quote CCC Insurance Policy");
			}

		} catch (ModuleException ex) {
			mex = ex;
		} catch (Throwable e) {
			log.error("quoteInsurancePolicy failed", e);
			mex = new ModuleException("wsclient.aig.general");
		} finally {
			if (insResponse == null) {
				insResponse = new InsuranceResponse(false, mex.getExceptionCode(), mex.getMessageString(), null, null, null,
						null, false);
			}
		}
		lstInsuranceResponses.add(insResponse);
		return lstInsuranceResponses;
	}

	@Override
	public List<InsuranceResponse> sell(List<IInsuranceRequest> iRequests)
			throws ModuleException {
		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		for (IInsuranceRequest insuranceRequest : iRequests) {
			StringBuffer policyCodeBuf = new StringBuffer();
			InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) insuranceRequest;
			BigDecimal totalPremiumAmount = insReqAssemebler.getTotalPremiumAmount();
			BigDecimal quotedTotalPremiumAmount = insReqAssemebler.getQuotedTotalPremiumAmount();
			Map config = ReservationModuleUtils.getAirReservationConfig().getCccInsuranceConfigMap();
			InsuranceCountryTO aigCountryTO = InsuranceUtil.getCountryTO((insReqAssemebler).getInsureSegment().getFromAirportCode());

			InsuranceResponse insResponse = new InsuranceResponse(true, "0", "OK", "", totalPremiumAmount, quotedTotalPremiumAmount,
				aigCountryTO.getCurrencyCode(),aigCountryTO.isEuropianCountry());
			policyCodeBuf.append(config.get("agencyCode"));
			policyCodeBuf.append("_");
			policyCodeBuf.append(insReqAssemebler.getPnr());
			policyCodeBuf.append("_");
			policyCodeBuf.append(CREATE_INS_CODE);
			insResponse.setPolicyCode(policyCodeBuf.toString());
			lstInsuranceResponses.add(insResponse);
		}
		return lstInsuranceResponses;
		
	}

	

	private static Map<InsuranceTypes, BigDecimal> calculateTotalPremium(Map<InsuranceTypes, BigDecimal> insTypeCharges,
			BigDecimal totalTicketPrice) {
		Map<InsuranceTypes, BigDecimal> insTypeChargesCalculatedAmount = new HashMap<InsuranceTypes, BigDecimal>();

		if (insTypeCharges != null) {

			Set<InsuranceTypes> keyset = insTypeCharges.keySet();
			for (InsuranceTypes key : keyset) {
				insTypeChargesCalculatedAmount.put(key,
						AccelAeroCalculator.divide(AccelAeroCalculator.multiply(totalTicketPrice, insTypeCharges.get(key)), 100));
			}

		}

		return insTypeChargesCalculatedAmount;
	}

	@Override
	public String getServiceProvider(String carrierCode) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
