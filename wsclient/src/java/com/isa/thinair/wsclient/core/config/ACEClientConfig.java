package com.isa.thinair.wsclient.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class ACEClientConfig extends DefaultModuleConfig {

	private String spName;
	private String custLoginId;
	private String encryptionTypeCd;
	private String pswd;
	private String clientAppName;
	private String version;
	private String defaultBirthDt;
	private String companyProductCd;
	private String destination;
	private String destinationDesc;
	private String destinationInternational;
	private String destinationInternationalDesc;
	private String insuredPackage;
	private String insuredPackageDesc;
	private String planCodeOneWay;
	private String planCodeOneWayDes;
	private String planCodeReturn;
	private String planCodeReturnDes;
	private String planCodeInternationalOneWay;
	private String planCodeInternationalOneWayDes;
	private String planCodeInternationalReturn;
	private String planCodeInternationalReturnDes;
	private String url;
	private String custLangPref;
	private String stringType;
	private String defaultQuoteId;

	public String getSpName() {
		return spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public String getCustLoginId() {
		return custLoginId;
	}

	public void setCustLoginId(String custLoginId) {
		this.custLoginId = custLoginId;
	}

	public String getEncryptionTypeCd() {
		return encryptionTypeCd;
	}

	public void setEncryptionTypeCd(String encryptionTypeCd) {
		this.encryptionTypeCd = encryptionTypeCd;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public String getClientAppName() {
		return clientAppName;
	}

	public void setClientAppName(String clientAppName) {
		this.clientAppName = clientAppName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDefaultBirthDt() {
		return defaultBirthDt;
	}

	public void setDefaultBirthDt(String defaultBirthDt) {
		this.defaultBirthDt = defaultBirthDt;
	}

	public String getCompanyProductCd() {
		return companyProductCd;
	}

	public void setCompanyProductCd(String companyProductCd) {
		this.companyProductCd = companyProductCd;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationDesc() {
		return destinationDesc;
	}

	public void setDestinationDesc(String destinationDesc) {
		this.destinationDesc = destinationDesc;
	}

	public String getInsuredPackage() {
		return insuredPackage;
	}

	public void setInsuredPackage(String insuredPackage) {
		this.insuredPackage = insuredPackage;
	}

	public String getInsuredPackageDesc() {
		return insuredPackageDesc;
	}

	public void setInsuredPackageDesc(String insuredPackageDesc) {
		this.insuredPackageDesc = insuredPackageDesc;
	}

	public String getPlanCodeOneWay() {
		return planCodeOneWay;
	}

	public void setPlanCodeOneWay(String planCodeOneWay) {
		this.planCodeOneWay = planCodeOneWay;
	}

	public String getPlanCodeReturn() {
		return planCodeReturn;
	}

	public void setPlanCodeReturn(String planCodeReturn) {
		this.planCodeReturn = planCodeReturn;
	}

	public String getPlanCodeReturnDes() {
		return planCodeReturnDes;
	}

	public void setPlanCodeReturnDes(String planCodeReturnDes) {
		this.planCodeReturnDes = planCodeReturnDes;
	}

	public String getPlanCodeOneWayDes() {
		return planCodeOneWayDes;
	}

	public void setPlanCodeOneWayDes(String planCodeOneWayDes) {
		this.planCodeOneWayDes = planCodeOneWayDes;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCustLangPref() {
		return custLangPref;
	}

	public void setCustLangPref(String custLangPref) {
		this.custLangPref = custLangPref;
	}

	public String getStringType() {
		return stringType;
	}

	public void setStringType(String stringType) {
		this.stringType = stringType;
	}

	public String getDefaultQuoteId() {
		return defaultQuoteId;
	}

	public void setDefaultQuoteId(String defaultQuoteId) {
		this.defaultQuoteId = defaultQuoteId;
	}

	public String getDestinationInternational() {
		return destinationInternational;
	}

	public void setDestinationInternational(String destinationInternational) {
		this.destinationInternational = destinationInternational;
	}

	public String getDestinationInternationalDesc() {
		return destinationInternationalDesc;
	}

	public void setDestinationInternationalDesc(String destinationInternationalDesc) {
		this.destinationInternationalDesc = destinationInternationalDesc;
	}

	public String getPlanCodeInternationalOneWay() {
		return planCodeInternationalOneWay;
	}

	public void setPlanCodeInternationalOneWay(String planCodeInternationalOneWay) {
		this.planCodeInternationalOneWay = planCodeInternationalOneWay;
	}

	public String getPlanCodeInternationalOneWayDes() {
		return planCodeInternationalOneWayDes;
	}

	public void setPlanCodeInternationalOneWayDes(String planCodeInternationalOneWayDes) {
		this.planCodeInternationalOneWayDes = planCodeInternationalOneWayDes;
	}

	public String getPlanCodeInternationalReturn() {
		return planCodeInternationalReturn;
	}

	public void setPlanCodeInternationalReturn(String planCodeInternationalReturn) {
		this.planCodeInternationalReturn = planCodeInternationalReturn;
	}

	public String getPlanCodeInternationalReturnDes() {
		return planCodeInternationalReturnDes;
	}

	public void setPlanCodeInternationalReturnDes(String planCodeInternationalReturnDes) {
		this.planCodeInternationalReturnDes = planCodeInternationalReturnDes;
	}

}
