package com.isa.thinair.wsclient.core.config;

import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class SmartButtonConfig extends DefaultModuleConfig {

	private Properties moduleUrl;

	private String webServiceToken;

	private String wsSecurityToken;

	public Properties getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(Properties moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getWebServiceToken() {
		return webServiceToken;
	}

	public void setWebServiceToken(String webServiceToken) {
		this.webServiceToken = webServiceToken;
	}

	public String getWsSecurityToken() {
		return wsSecurityToken;
	}

	public void setWsSecurityToken(String wsSecurityToken) {
		this.wsSecurityToken = wsSecurityToken;
	}
	
	
}
