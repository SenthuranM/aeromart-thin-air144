package com.isa.thinair.wsclient.core.service.lms.base.member;

import java.util.List;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Country;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;

public interface LMSMemberWebService {

	public LoyaltyMemberStateDTO getMemberState(String memberAccountId, int sbInternaMemberId) throws ModuleException;

	public String enrollMemberWithEmail(Customer customer) throws ModuleException;

	public boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException;

	public LoyaltyMemberCoreDTO getMemberCoreDetails(String memberAccountId) throws ModuleException;

	public LoyaltyPointDetailsDTO getMemberPointBalances(String memberAccountId,  String memberExternalId) throws ModuleException;

	public boolean saveMemberCustomFields(Customer customer) throws ModuleException;

	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException;

	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException;

	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber) throws ModuleException;

	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode) throws ModuleException;

	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException;

	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef) throws ModuleException;

	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException;

	public int saveMemberName(Customer customer) throws ModuleException;
	
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException;
	
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException;

	public MemberRemoteLoginResponseDTO memberLogin(String userName,String password) throws ModuleException;

	public List<Country> getCountriesList() throws ModuleException;

	public void updateMember(Customer customer) throws ModuleException;

	MemberFetchDataResponse getMemberFromEmail(String memberAccountId) throws ModuleException;

}
