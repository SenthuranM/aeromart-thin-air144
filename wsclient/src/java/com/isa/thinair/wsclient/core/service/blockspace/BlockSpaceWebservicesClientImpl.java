package com.isa.thinair.wsclient.core.service.blockspace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isa.thinair.wsclient.api.dto.FlightSearchTransferDTO;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.AddONDComposerUtil;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.DataComposerUtil;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.OTAAirAvailRQComposerUtil;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.OTAAirBookRQComposerUtil;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.OTAAirPriceRQComposerUtil;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;

/**
 * BlockSpace webservices client logic implementation.
 * 
 * @author Dumindag
 * @version 1.0
 */
public class BlockSpaceWebservicesClientImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_AA;
	private static Log log = LogFactory.getLog(BlockSpaceWebservicesClientImpl.class);

	public String getServiceProvider(String carrierCode) {
		if (!PlatformUtiltiies.nullHandler(carrierCode).equals("")) {
			return SERVICE_PROVIDER + ExternalWebServiceInvoker.SERVICE_PROVIDER_SEPERATOR + carrierCode;
		}

		return SERVICE_PROVIDER;
	}

	/**
	 * Perform Availability Search
	 * 
	 * @param flightSearchTransferDTO
	 * @param baseDTO
	 * @param transactionIdentifier
	 * @return
	 * @throws ModuleException
	 */
	public OTAAirAvailRS getSeatAvailability(FlightSearchTransferDTO flightSearchTransferDTO, BaseDTO baseDTO,
			String transactionIdentifier) throws ModuleException {
		log.debug("Begin getSeatAvailability");
		OTAAirAvailRQ oTAAirAvailRQ = OTAAirAvailRQComposerUtil.compose(flightSearchTransferDTO, baseDTO, transactionIdentifier);
		OTAAirAvailRS otaAirAvailRS = (OTAAirAvailRS) ExternalWebServiceInvoker.invokeService(
				getServiceProvider(baseDTO.getCarrierCode()), "getAvailability", true, new Param(oTAAirAvailRQ));

		log.debug("End getSeatAvailability");
		if (otaAirAvailRS.getSuccess() != null) {
			return otaAirAvailRS;
		}

		throw new ModuleException("wsclient.pnrTransfer.invalid.flightsNotAvailable");
	}

	/**
	 * Perform Price Quote
	 * 
	 * @param flightSearchTransferDTO
	 * @param otaAirAvailRS
	 * @param baseDTO
	 * @return
	 * @throws ModuleException
	 */
	public OTAAirPriceRS getPriceQuote(FlightSearchTransferDTO flightSearchTransferDTO, OTAAirAvailRS otaAirAvailRS,
			BaseDTO baseDTO) throws ModuleException {
		log.debug("Begin getPriceQuote");

		OTAAirPriceRQ oTAAirPriceRQ = OTAAirPriceRQComposerUtil.compose(flightSearchTransferDTO, otaAirAvailRS, baseDTO);

		if (oTAAirPriceRQ == null) {
			throw new ModuleException("wsclient.pnrTransfer.invalid.identicalFlightsNotAvailable");
		}

		OTAAirPriceRS oTAAirPriceRS = (OTAAirPriceRS) ExternalWebServiceInvoker.invokeService(
				getServiceProvider(baseDTO.getCarrierCode()), "getPrice", true, new Param(oTAAirPriceRQ));

		log.debug("End getPriceQuote");
		return oTAAirPriceRS;
	}

	/**
	 * Perform the booking
	 * 
	 * @param oTAAirPriceRS
	 * @param reservation
	 * @param externalSegments
	 * @param baseDTO
	 * @return
	 * @throws ModuleException
	 */
	public OTAAirBookRS bookReservation(OTAAirPriceRS oTAAirPriceRS, Reservation reservation, Collection externalSegments,
			BaseDTO baseDTO) throws ModuleException {
		log.debug("Begin bookReservation");

		Object obj[] = OTAAirBookRQComposerUtil.compose(oTAAirPriceRS, reservation, externalSegments, baseDTO);
		OTAAirBookRQ oTAAirBookRQ = (OTAAirBookRQ) obj[0];
		AAAirBookRQExt aaAirBookRQExt = (AAAirBookRQExt) obj[1];

		OTAAirBookRS oTAAirBookRS = (OTAAirBookRS) ExternalWebServiceInvoker.invokeService(
				getServiceProvider(baseDTO.getCarrierCode()), "book", true, new Param(oTAAirBookRQ), new Param(aaAirBookRQExt));

		log.debug("End bookReservation");
		return oTAAirBookRS;
	}

	/**
	 * Adds a new ond
	 * 
	 * @param otaAirBookRS
	 * @param otaAirPriceRS
	 * @param baseDTO
	 * @return
	 * @throws ModuleException
	 */
	public OTAAirBookRS addNewOND(OTAAirBookRS otaAirBookRS, OTAAirPriceRS otaAirPriceRS, BaseDTO baseDTO) throws ModuleException {
		log.debug("Begin modifyReservation");
		Object obj[] = AddONDComposerUtil.compose(otaAirBookRS, otaAirPriceRS, baseDTO);
		OTAAirBookModifyRQ otaAirBookModRQ = (OTAAirBookModifyRQ) obj[0];
		AAAirBookModifyRQExt aaAirBookModifyRQExt = (AAAirBookModifyRQExt) obj[1];

		OTAAirBookRS oTAAirBookRS = (OTAAirBookRS) ExternalWebServiceInvoker.invokeService(getServiceProvider(baseDTO
				.getCarrierCode()), "modifyReservation", true, new Param(otaAirBookModRQ), new Param(aaAirBookModifyRQExt));

		log.debug("End modifyReservation");
		return oTAAirBookRS;
	}

	/**
	 * Transfer the booking
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @param interlinedAirLineTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferReservation(Reservation reservation, Collection pnrSegIds,
			InterlinedAirLineTO interlinedAirLineTO) throws ModuleException {
		BaseDTO baseDTO = DataComposerUtil.prepareBaseDTO(interlinedAirLineTO);
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = DataComposerUtil.getReservationSegments(reservation,
				pnrSegIds);
		ReservationSegmentDTO reservationSegmentDTO;

		// More than one segments so need to transfer the booking first and do an add segment
		if (colReservationSegmentDTO.size() > 1) {
			Collection externalSegments = DataComposerUtil.getExternalSegments(reservation, interlinedAirLineTO,
					colReservationSegmentDTO);
			DefaultServiceResponse successServiceResponce = new DefaultServiceResponse(false);
			Collection successRecords = new ArrayList();
			Collection failedRecords = new ArrayList();
			OTAAirBookRS oTAAirBookRS = null;
			DefaultServiceResponse tmpServiceResponce;
			String currentSegInfo = " ";
			int index = 0;

			for (Iterator itr = colReservationSegmentDTO.iterator(); itr.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itr.next();

				if (index == 0) {
					oTAAirBookRS = transferBookingMacro(reservation, reservationSegmentDTO, externalSegments, baseDTO);
					tmpServiceResponce = OTAAirBookRQComposerUtil.prepareServiceResponse(oTAAirBookRS,
							reservationSegmentDTO.toSimpleString());

					if (!tmpServiceResponce.isSuccess()) {
						failedRecords.add(reservationSegmentDTO);
						DataComposerUtil.buddleResponses(tmpServiceResponce, successRecords, failedRecords);
						return tmpServiceResponce;
					} else {
						successRecords.add(reservationSegmentDTO);
						successServiceResponce = tmpServiceResponce;
					}
				} else {
					currentSegInfo = currentSegInfo + reservationSegmentDTO.toSimpleString();

					try {
						oTAAirBookRS = ammendNewSegmentMacro(oTAAirBookRS, reservation, reservationSegmentDTO, baseDTO);
						tmpServiceResponce = OTAAirBookRQComposerUtil.prepareServiceResponse(oTAAirBookRS, currentSegInfo);
					} catch (ModuleException e) {
						tmpServiceResponce = DataComposerUtil.prepareFailedDummyResponse(e.getMessageString());
					} catch (Exception e) {
						tmpServiceResponce = DataComposerUtil.prepareFailedDummyResponse(e.getMessage());
					}

					if (!tmpServiceResponce.isSuccess()) {
						failedRecords.add(reservationSegmentDTO);
						DataComposerUtil.copyError(successServiceResponce, tmpServiceResponce);
						DataComposerUtil.buddleResponses(successServiceResponce, successRecords, failedRecords);
						return successServiceResponce;
					} else {
						successRecords.add(reservationSegmentDTO);
						successServiceResponce = tmpServiceResponce;
					}
				}

				index++;
			}

			DataComposerUtil.buddleResponses(successServiceResponce, successRecords, failedRecords);
			return successServiceResponce;
			// Only one segment so directly transfer
		} else if (colReservationSegmentDTO.size() == 1) {
			reservationSegmentDTO = (ReservationSegmentDTO) BeanUtils.getFirstElement(colReservationSegmentDTO);
			Collection externalSegments = DataComposerUtil.getExternalSegments(reservation, interlinedAirLineTO,
					reservationSegmentDTO);
			OTAAirBookRS oTAAirBookRS = transferBookingMacro(reservation, reservationSegmentDTO, externalSegments, baseDTO);
			DefaultServiceResponse serviceResponce = OTAAirBookRQComposerUtil.prepareServiceResponse(oTAAirBookRS,
					reservationSegmentDTO.toSimpleString());
			DataComposerUtil.assembleResponseSegment(serviceResponce, reservationSegmentDTO, serviceResponce.isSuccess());

			return serviceResponce;
		} else {
			throw new ModuleException("wsclient.pnrTransfer.invalid.noRecordsFound");
		}
	}

	/**
	 * Transfer booking macro
	 * 
	 * @param reservation
	 * @param reservationSegmentDTO
	 * @param externalSegments
	 * @param baseDTO
	 * @return
	 * @throws ModuleException
	 */
	private OTAAirBookRS transferBookingMacro(Reservation reservation, ReservationSegmentDTO reservationSegmentDTO,
			Collection externalSegments, BaseDTO baseDTO) throws ModuleException {
		FlightSearchTransferDTO flightSearchTransferDTO = DataComposerUtil.composeFlightSearchTransferData(reservation,
				reservationSegmentDTO);
		OTAAirAvailRS oTAAirAvailRS = getSeatAvailability(flightSearchTransferDTO, baseDTO, null);
		OTAAirPriceRS oTAAirPriceRS = getPriceQuote(flightSearchTransferDTO, oTAAirAvailRS, baseDTO);
		OTAAirBookRS oTAAirBookRS = bookReservation(oTAAirPriceRS, reservation, externalSegments, baseDTO);

		return oTAAirBookRS;
	}

	/**
	 * Ammend the new segment macro
	 * 
	 * @param otaAirBookRS
	 * @param reservation
	 * @param reservationSegmentDTO
	 * @param baseDTO
	 * @return
	 * @throws ModuleException
	 */
	private OTAAirBookRS ammendNewSegmentMacro(OTAAirBookRS otaAirBookRS, Reservation reservation,
			ReservationSegmentDTO reservationSegmentDTO, BaseDTO baseDTO) throws ModuleException {

		if (otaAirBookRS == null) {
			throw new ModuleException("wsclient.pnrTransfer.error.cannotAddSegments");
		}

		FlightSearchTransferDTO flightSearchTransferDTO = DataComposerUtil.composeFlightSearchTransferData(reservation,
				reservationSegmentDTO);
		OTAAirAvailRS oTAAirAvailRS = getSeatAvailability(flightSearchTransferDTO, baseDTO,
				otaAirBookRS.getTransactionIdentifier());
		OTAAirPriceRS oTAAirPriceRS = getPriceQuote(flightSearchTransferDTO, oTAAirAvailRS, baseDTO);
		OTAAirBookRS oTAAirBookRS = addNewOND(otaAirBookRS, oTAAirPriceRS, baseDTO);

		return oTAAirBookRS;
	}
}
