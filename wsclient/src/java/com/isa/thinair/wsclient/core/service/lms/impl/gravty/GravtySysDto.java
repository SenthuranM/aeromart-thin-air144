package com.isa.thinair.wsclient.core.service.lms.impl.gravty;
import java.util.HashMap;
import java.util.Map;

public class GravtySysDto {

	public static class SystemAuthTokenResponse{
		private String expires_in;
		private Role role;
		private Integer sponsor_id;
		private Boolean success;
		private String token_key;
		private String sponsor_name;
		private String mobile;
		private String token;
		private User user;
		private String role_name;
		private Boolean active;
		private Integer id;
		private String profile_image;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();
		public String getExpires_in() {
			return expires_in;
		}
		public void setExpires_in(String expires_in) {
			this.expires_in = expires_in;
		}
		public Role getRole() {
			return role;
		}
		public void setRole(Role role) {
			this.role = role;
		}
		public Integer getSponsor_id() {
			return sponsor_id;
		}
		public void setSponsor_id(Integer sponsor_id) {
			this.sponsor_id = sponsor_id;
		}
		public Boolean getSuccess() {
			return success;
		}
		public void setSuccess(Boolean success) {
			this.success = success;
		}
		public String getToken_key() {
			return token_key;
		}
		public void setToken_key(String token_key) {
			this.token_key = token_key;
		}
		public String getSponsor_name() {
			return sponsor_name;
		}
		public void setSponsor_name(String sponsor_name) {
			this.sponsor_name = sponsor_name;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public String getRole_name() {
			return role_name;
		}
		public void setRole_name(String role_name) {
			this.role_name = role_name;
		}
		
		public Boolean getActive() {
			return active;
		}
		public void setActive(Boolean active) {
			this.active = active;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getProfile_image() {
			return profile_image;
		}
		public void setProfile_image(String profile_image) {
			this.profile_image = profile_image;
		}
		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}
		public void setAdditional_properties(Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}


	}
	public static class URL{
		private String url;

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		
	}
	public static class Credentials{
		
		private String username;
		private String password;
		
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}

		
		
	}
	public static class Role {

		private Integer id;
		private Integer versionCounter;
		private String name;
		private Boolean isProgramUser;
		private Boolean canViewOffer;
		private Boolean canEditOffer;
		private Boolean canViewSponsor;
		private Boolean canEditSponsor;
		private Boolean canViewProgram;
		private Boolean canEditProgram;
		private Boolean canViewBit;
		private Boolean canEditBit;
		private Boolean canViewDashboard;
		private Boolean canViewMember;
		private Boolean canEditMember;
		private Boolean canViewMemberDetails;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public Integer getId() {
		return id;
		}

		public void setId(Integer id) {
		this.id = id;
		}

		public Integer getVersionCounter() {
		return versionCounter;
		}

		public void setVersionCounter(Integer versionCounter) {
		this.versionCounter = versionCounter;
		}

		public String getName() {
		return name;
		}

		public void setName(String name) {
		this.name = name;
		}

		public Boolean getIsProgramUser() {
		return isProgramUser;
		}

		public void setIsProgramUser(Boolean isProgramUser) {
		this.isProgramUser = isProgramUser;
		}

		public Boolean getCanViewOffer() {
		return canViewOffer;
		}

		public void setCanViewOffer(Boolean canViewOffer) {
		this.canViewOffer = canViewOffer;
		}

		public Boolean getCanEditOffer() {
		return canEditOffer;
		}

		public void setCanEditOffer(Boolean canEditOffer) {
		this.canEditOffer = canEditOffer;
		}

		public Boolean getCanViewSponsor() {
		return canViewSponsor;
		}

		public void setCanViewSponsor(Boolean canViewSponsor) {
		this.canViewSponsor = canViewSponsor;
		}

		public Boolean getCanEditSponsor() {
		return canEditSponsor;
		}

		public void setCanEditSponsor(Boolean canEditSponsor) {
		this.canEditSponsor = canEditSponsor;
		}

		public Boolean getCanViewProgram() {
		return canViewProgram;
		}

		public void setCanViewProgram(Boolean canViewProgram) {
		this.canViewProgram = canViewProgram;
		}

		public Boolean getCanEditProgram() {
		return canEditProgram;
		}

		public void setCanEditProgram(Boolean canEditProgram) {
		this.canEditProgram = canEditProgram;
		}

		public Boolean getCanViewBit() {
		return canViewBit;
		}

		public void setCanViewBit(Boolean canViewBit) {
		this.canViewBit = canViewBit;
		}

		public Boolean getCanEditBit() {
		return canEditBit;
		}

		public void setCanEditBit(Boolean canEditBit) {
		this.canEditBit = canEditBit;
		}

		public Boolean getCanViewDashboard() {
		return canViewDashboard;
		}

		public void setCanViewDashboard(Boolean canViewDashboard) {
		this.canViewDashboard = canViewDashboard;
		}

		public Boolean getCanViewMember() {
		return canViewMember;
		}

		public void setCanViewMember(Boolean canViewMember) {
		this.canViewMember = canViewMember;
		}

		public Boolean getCanEditMember() {
		return canEditMember;
		}

		public void setCanEditMember(Boolean canEditMember) {
		this.canEditMember = canEditMember;
		}

		public Boolean getCanViewMemberDetails() {
		return canViewMemberDetails;
		}

		public void setCanViewMemberDetails(Boolean canViewMemberDetails) {
		this.canViewMemberDetails = canViewMemberDetails;
		}

		public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		}

		}
	public static class User {

		private Integer id;
		private Object lastLogin;
		private String username;
		private String firstName;
		private String lastName;
		private String email;
		private Boolean isActive;
		private String dateJoined;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public Integer getId() {
		return id;
		}

		public void setId(Integer id) {
		this.id = id;
		}

		public Object getLastLogin() {
		return lastLogin;
		}

		public void setLastLogin(Object lastLogin) {
		this.lastLogin = lastLogin;
		}

		public String getUsername() {
		return username;
		}

		public void setUsername(String username) {
		this.username = username;
		}

		public String getFirstName() {
		return firstName;
		}

		public void setFirstName(String firstName) {
		this.firstName = firstName;
		}

		public String getLastName() {
		return lastName;
		}

		public void setLastName(String lastName) {
		this.lastName = lastName;
		}

		public String getEmail() {
		return email;
		}

		public void setEmail(String email) {
		this.email = email;
		}

		public Boolean getIsActive() {
		return isActive;
		}

		public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
		}

		public String getDateJoined() {
		return dateJoined;
		}

		public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
		}

		public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		}

		}
}
