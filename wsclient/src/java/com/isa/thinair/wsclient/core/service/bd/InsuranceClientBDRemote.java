package com.isa.thinair.wsclient.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.wsclient.api.service.InsuranceClientBD;

@Remote
public interface InsuranceClientBDRemote extends InsuranceClientBD {

}
