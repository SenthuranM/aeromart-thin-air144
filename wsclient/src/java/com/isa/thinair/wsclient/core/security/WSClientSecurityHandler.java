package com.isa.thinair.wsclient.core.security;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.sun.xml.wss.ProcessingContext;
import com.sun.xml.wss.XWSSProcessor;
import com.sun.xml.wss.XWSSProcessorFactory;
import com.sun.xml.wss.XWSSecurityException;

public class WSClientSecurityHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Log log = LogFactory.getLog(WSClientSecurityHandler.class);

	private static final String DUMP_MESSAGES = WSClientModuleUtils.getModuleConfig().getDumpMessages();

	private XWSSProcessor cprocessor = null;

	public WSClientSecurityHandler(String userName, String password) {
		InputStream in = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Authenticating with DCS, UserName [" + userName + "], Password Length ["
						+ PlatformUtiltiies.nullHandler(password).length() + "] ");
			}

			XWSSProcessorFactory factory = XWSSProcessorFactory.newInstance();
			in = getStream();
			cprocessor = factory.createProcessorForSecurityConfiguration(in, new WSClientSecurityEnvHandler(userName, password));
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}

	public Set<QName> getHeaders() {
		QName securityHeader = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
				"Security", "wsse");
		HashSet<QName> headers = new HashSet<QName>();
		headers.add(securityHeader);
		return headers;
	}

	public boolean handleFault(SOAPMessageContext messageContext) {
		return true;
	}

	public boolean handleMessage(SOAPMessageContext messageContext) {
		secureClient(messageContext);
		return true;
	}

	public void close(MessageContext messageContext) {

	}

	private void secureClient(SOAPMessageContext messageContext) {
		Boolean outMessageIndicator = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		SOAPMessage message = messageContext.getMessage();

		if (outMessageIndicator.booleanValue()) {
			if (log.isDebugEnabled()) {
				log.debug("\nClient Outbound SOAP : ");
			}

			try {
				ProcessingContext context = cprocessor.createProcessingContext(message);
				context.setSOAPMessage(message);
				SOAPMessage secureMsg = cprocessor.secureOutboundMessage(context);
				messageContext.setMessage(secureMsg);
			} catch (XWSSecurityException ex) {
				log.error(ex);
				throw new RuntimeException(ex);
			} catch (Exception e) {
				log.error(e);
				throw new RuntimeException(e);
			}
		}

		return;
	}

	private InputStream getStream() {
		String text = "<xwss:SecurityConfiguration xmlns:xwss=\"http://java.sun.com/xml/ns/xwss/config\" dumpMessages=\""
				+ DUMP_MESSAGES + "\"> " + "<xwss:UsernameToken digestPassword=\"false\"/> " + "</xwss:SecurityConfiguration>";
		InputStream is = null;

		try {
			is = new ByteArrayInputStream(text.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error(e);
		}

		return is;
	}
}
