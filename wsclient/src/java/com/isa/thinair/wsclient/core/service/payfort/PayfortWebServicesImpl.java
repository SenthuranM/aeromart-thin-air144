package com.isa.thinair.wsclient.core.service.payfort;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.GetCitiesDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.GetCitiesResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.wsclient.api.payfort.MsgHeaderType;
import com.isa.thinair.wsclient.api.payfort.MsgHeaderTypeServices;
import com.isa.thinair.wsclient.api.payfort.MsgType;
import com.isa.thinair.wsclient.api.payfort.PAYMENTMETHOD;
import com.isa.thinair.wsclient.api.payfort.PAYMENTMETHODResponse;
import com.isa.thinair.wsclient.api.payfort.PAYMENTMETHODSERVICES;
import com.isa.thinair.wsclient.api.payfort.PAYMENTMETHODSERVICESResponse;
import com.isa.thinair.wsclient.api.payfort.PAYMENTMETHOD_Type;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.util.SOAPLoggingHandler;



public class PayfortWebServicesImpl {

	private static Log log = LogFactory.getLog(PayfortWebServicesImpl.class);

	com.isa.thinair.wsclient.core.config.PayfortClientConfig payfortClientConfig = WSClientModuleUtils.getModuleConfig()
			.getPayfortClientConfig();

	/**
	 * Payment verification method for payFort Payment gateway
	 * 
	 * @param payTransaction
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Pay_AT_StoreResponseDTO payAtStorePaymentTransaction(Pay_AT_StoreRequestDTO payAtStoreDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[PayfortWebServicesImpl::payTransaction()] Begin");
		Pay_AT_StoreResponseDTO responseDTO = new Pay_AT_StoreResponseDTO();
		try {
			// method initailizations for pay fort

			PAYMENTMETHOD serviceStub = getClientStub();
			MsgHeaderType header = new MsgHeaderType();
			PAYMENTMETHOD_Type.Message message = new PAYMENTMETHOD_Type.Message();
			PAYMENTMETHODResponse.Return response = new PAYMENTMETHODResponse.Return();
			PAYMENTMETHOD_Type.Message.PAYatSTORE payAtStore = new PAYMENTMETHOD_Type.Message.PAYatSTORE();
			// set data for saop msg
			header.setMsgType(MsgType.PA_YAT_STORE);
			header.setMsgDate(payAtStoreDTO.getMsgDate());

			message.setHeader(header);
			message.setPAYatSTORE(setMessageDataForPayATStore(payAtStoreDTO, payAtStore));
			message.setSignature(payAtStoreDTO.getSignature());

			try {
				response = serviceStub.paymentMETHOD(message);

				// setting response to DTO
				responseDTO = setMessageDataForPayATStore(responseDTO, response);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (payfortClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[PayfortWebServicesImpl::payAtStorePaymentTransaction()] End");
		return responseDTO;
	}

	/**
	 * Payment checkTransaction method for payFort Payment gateway
	 * 
	 * @param CheckOrderDTO
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public CheckOrderResponseDTO checkTransaction(CheckOrderDTO checkOrderDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[PayfortWebServicesImpl::checkTransaction()] Begin");
		CheckOrderResponseDTO responseDTO = new CheckOrderResponseDTO();
		try {
			// method initailizations for pay fort
			PAYMENTMETHOD serviceStub = getClientStub();
			MsgHeaderTypeServices header = new MsgHeaderTypeServices();
			PAYMENTMETHODSERVICES.Message message = new PAYMENTMETHODSERVICES.Message();
			PAYMENTMETHODSERVICESResponse.Return response = new PAYMENTMETHODSERVICESResponse.Return();
			PAYMENTMETHODSERVICES.Message.CHECKORDER checkOder = new PAYMENTMETHODSERVICES.Message.CHECKORDER();
			// set data for saop msg
			header.setMsgType("CHECK_ORDER");
			header.setMsgDate(checkOrderDTO.getMsgDate());

			message.setHeader(header);
			message.setCHECKORDER(setCheckOrder(checkOrderDTO, checkOder));
			message.setSignature(checkOrderDTO.getSignature());

			try {
				response = serviceStub.paymentMETHODSERVICES(message);

				// setting response to DTO
				responseDTO = getCheckResponseToDTO(responseDTO, response);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (payfortClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[PayfortWebServicesImpl::checkTransaction()] End");
		return responseDTO;
	}

	/**
	 * Payment getCities method for payFort Payment gateway
	 * 
	 * @param GetCitiesDTO
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public GetCitiesResponseDTO getCities(GetCitiesDTO getCitiesDTO) throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[PayfortWebServicesImpl::checkTransaction()] Begin");
		GetCitiesResponseDTO responseDTO = new GetCitiesResponseDTO();
		try {
			// method initailizations for pay fort
			PAYMENTMETHOD serviceStub = getClientStub();
			MsgHeaderTypeServices header = new MsgHeaderTypeServices();
			PAYMENTMETHODSERVICES.Message message = new PAYMENTMETHODSERVICES.Message();
			PAYMENTMETHODSERVICESResponse.Return response = new PAYMENTMETHODSERVICESResponse.Return();
			PAYMENTMETHODSERVICES.Message.PAYATHOMEGETCITIES getCities = new PAYMENTMETHODSERVICES.Message.PAYATHOMEGETCITIES();
			// set data for saop msg
			header.setMsgType("PAYATHOME_GET_CITIES");
			header.setMsgDate(getCitiesDTO.getMsgDate());

			message.setHeader(header);
			message.setPAYATHOMEGETCITIES(setCities(getCitiesDTO, getCities));
			message.setSignature(getCitiesDTO.getSignature());

			try {
				response = serviceStub.paymentMETHODSERVICES(message);

				// setting response to DTO
				responseDTO = getCitiesResponseToDTO(responseDTO, response);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (payfortClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[PayfortWebServicesImpl::getCities()] End");
		return responseDTO;
	}

	/**
	 * Payment checkNotification method for payFort Payment gateway
	 * 
	 * @param CheckNotificationDTO
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public CheckNotificationResponseDTO checkNotification(CheckNotificationDTO checkNotificationDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[PayfortWebServicesImpl::checkNotification()] Begin");
		CheckNotificationResponseDTO responseDTO = new CheckNotificationResponseDTO();
		try {
			// method initailizations for pay fort
			PAYMENTMETHOD serviceStub = getClientStub();
			MsgHeaderTypeServices header = new MsgHeaderTypeServices();
			PAYMENTMETHODSERVICES.Message message = new PAYMENTMETHODSERVICES.Message();
			PAYMENTMETHODSERVICESResponse.Return response = new PAYMENTMETHODSERVICESResponse.Return();
			PAYMENTMETHODSERVICES.Message.PAYMENTNOTIFICATION getNotification = new PAYMENTMETHODSERVICES.Message.PAYMENTNOTIFICATION();
			// set data for saop msg
			header.setMsgType("PAYMENT_NOTIFICATION");
			header.setMsgDate(checkNotificationDTO.getMsgDate());

			message.setHeader(header);
			message.setPAYMENTNOTIFICATION(setNotification(checkNotificationDTO, getNotification));
			message.setSignature(checkNotificationDTO.getSignature());

			try {
				response = serviceStub.paymentMETHODSERVICES(message);

				// setting response to DTO
				responseDTO = getNotificationResponseToDTO(responseDTO, response);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (payfortClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[PayfortWebServicesImpl::checkNotification()] End");
		return responseDTO;
	}

	private PAYMENTMETHOD_Type.Message.PAYatSTORE setMessageDataForPayATStore(Pay_AT_StoreRequestDTO payAtStoreDTO,
			PAYMENTMETHOD_Type.Message.PAYatSTORE payAtStore) {

		payAtStore.setAmount(payAtStoreDTO.getAmount());
		payAtStore.setClientEmail(payAtStoreDTO.getClientEmail());
		payAtStore.setClientName(payAtStoreDTO.getClientName());
		payAtStore.setCurrency(payAtStoreDTO.getCurrency());
		payAtStore.setExpiryDate(payAtStoreDTO.getExpiryDate());
		payAtStore.setClientMobile(payAtStoreDTO.getClientMobile());
		payAtStore.setItemName(payAtStoreDTO.getItemName());
		payAtStore.setMerchantID(payAtStoreDTO.getMerchantID());
		payAtStore.setOrderID(payAtStoreDTO.getOrderID());
		payAtStore.setTicketNumber(payAtStoreDTO.getTicketNumber());
		payAtStore.setServiceName(payAtStoreDTO.getServiceName());

		return payAtStore;
	}

	private PAYMENTMETHODSERVICES.Message.CHECKORDER setCheckOrder(CheckOrderDTO checkOrderDTO,
			PAYMENTMETHODSERVICES.Message.CHECKORDER checkOrder) {

		checkOrder.setCurrency(checkOrderDTO.getCurrency());
		checkOrder.setMerchantID(checkOrderDTO.getMerchantID());
		checkOrder.setOrderID(checkOrderDTO.getOrderID());
		checkOrder.setPayment(checkOrderDTO.getPayment());
		checkOrder.setServiceName(checkOrderDTO.getServiceName());

		return checkOrder;
	}

	private PAYMENTMETHODSERVICES.Message.PAYATHOMEGETCITIES setCities(GetCitiesDTO getCitiesDTO,
			PAYMENTMETHODSERVICES.Message.PAYATHOMEGETCITIES getCities) {

		getCities.setCountryCode(getCitiesDTO.getCountryCode());
		getCities.setLanguage(getCitiesDTO.getLanguage());
		getCities.setMerchantID(getCitiesDTO.getMerchantID());
		getCities.setServiceName(getCitiesDTO.getServiceName());

		return getCities;
	}

	private PAYMENTMETHODSERVICES.Message.PAYMENTNOTIFICATION setNotification(CheckNotificationDTO checkNotificationDTO,
			PAYMENTMETHODSERVICES.Message.PAYMENTNOTIFICATION getNotification) {

		getNotification.setCurrency(checkNotificationDTO.getCurrency());
		getNotification.setMerchantID(checkNotificationDTO.getMerchantID());
		getNotification.setOrderID(checkNotificationDTO.getOrderID());
		getNotification.setPayment(checkNotificationDTO.getPayment());
		getNotification.setServiceName(checkNotificationDTO.getServiceName());
		getNotification.setStatus(checkNotificationDTO.getStatus());
		getNotification.setTicketNumber(checkNotificationDTO.getTicketNumber());

		return getNotification;
	}

	private Pay_AT_StoreResponseDTO setMessageDataForPayATStore(Pay_AT_StoreResponseDTO payAtStoreResponseDTO,
			PAYMENTMETHODResponse.Return response) {

		payAtStoreResponseDTO.setMsgDate(response.getHeader().getMsgDate());
		payAtStoreResponseDTO.setSignature(response.getSignature());

		if (response.getError() == null) {
			if (response.getResponseStatus() != null) {
				payAtStoreResponseDTO.setStatus(response.getResponseStatus().getStatus());
				if (response.getResponseStatus().getAdditionalNote() != null)
					payAtStoreResponseDTO.setAdditionalNote(response.getResponseStatus().getAdditionalNote());
			}

			if (response.getPAYatSTORE() != null) {
				payAtStoreResponseDTO.setOrderID(response.getPAYatSTORE().getOrderID());
				payAtStoreResponseDTO.setRequestID(response.getPAYatSTORE().getRequestID());
				payAtStoreResponseDTO.setStatusCode(Integer.parseInt(response.getPAYatSTORE().getStatusCode()));
				payAtStoreResponseDTO.setVoucherNumber(response.getPAYatSTORE().getVoucherNumber());
			}

		} else {
			payAtStoreResponseDTO.setErrorCode(response.getError().getErrorCode());
			payAtStoreResponseDTO.setErrorDesc(response.getError().getErrorDesc());
		}

		return payAtStoreResponseDTO;
	}

	private CheckOrderResponseDTO getCheckResponseToDTO(CheckOrderResponseDTO responseDTO,
			PAYMENTMETHODSERVICESResponse.Return response) {

		responseDTO.setMsgDate(response.getHeader().getMsgDate());
		responseDTO.setSignature(response.getSignature());

		if (response.getError() == null) {
			if (response.getResponseStatus() != null) {
				responseDTO.setStatus(response.getResponseStatus().getStatus());
				if (response.getResponseStatus().getAdditionalNote() != null)
					responseDTO.setAdditionalNote(response.getResponseStatus().getAdditionalNote());
			}

			if (response.getCHECKORDER() != null) {
				responseDTO.setCurrency(response.getCHECKORDER().getCurrency());
				responseDTO.setOrderID(response.getCHECKORDER().getOrderID());
				responseDTO.setPayment(response.getCHECKORDER().getPayment());
				responseDTO.setOrderStatus(response.getCHECKORDER().getOrderStatus());
			}

		} else {
			responseDTO.setErrorCode(response.getError().getErrorCode());
			responseDTO.setErrorDesc(response.getError().getErrorDesc());
		}

		return responseDTO;
	}

	private GetCitiesResponseDTO getCitiesResponseToDTO(GetCitiesResponseDTO responseDTO,
			PAYMENTMETHODSERVICESResponse.Return response) {

		responseDTO.setMsgDate(response.getHeader().getMsgDate());
		responseDTO.setSignature(response.getSignature());

		if (response.getError() == null) {
			if (response.getResponseStatus() != null) {
				responseDTO.setStatus(response.getResponseStatus().getStatus());
				if (response.getResponseStatus().getAdditionalNote() != null)
					responseDTO.setAdditionalNote(response.getResponseStatus().getAdditionalNote());
			}

			if (response.getPAYATHOMEGETCITIES() != null) {
				responseDTO.setCurrency(response.getPAYATHOMEGETCITIES().getCurrency());

				responseDTO.setCountryCode(response.getPAYATHOMEGETCITIES().getCountryCode());
				// need to change accordingly
				responseDTO.setCities(response.getPAYATHOMEGETCITIES().getCities());
			}

		} else {
			responseDTO.setErrorCode(response.getError().getErrorCode());
			responseDTO.setErrorDesc(response.getError().getErrorDesc());
		}

		return responseDTO;
	}

	private CheckNotificationResponseDTO getNotificationResponseToDTO(CheckNotificationResponseDTO responseDTO,
			PAYMENTMETHODSERVICESResponse.Return response) {

		responseDTO.setMsgDate(response.getHeader().getMsgDate());
		responseDTO.setSignature(response.getSignature());

		if (response.getError() == null) {
			if (response.getResponseStatus() != null) {
				responseDTO.setStatus(response.getResponseStatus().getStatus());
				if (response.getResponseStatus().getAdditionalNote() != null)
					responseDTO.setAdditionalNote(response.getResponseStatus().getAdditionalNote());
			}

			if (response.getPAYMENTNOTIFICATION() != null) {
				responseDTO.setMerchantID(response.getPAYMENTNOTIFICATION().getMerchantID());
				responseDTO.setOrderID(response.getPAYMENTNOTIFICATION().getOrderID());
				responseDTO.setPayment(response.getPAYMENTNOTIFICATION().getPayment());
				responseDTO.setCurrency(response.getPAYMENTNOTIFICATION().getCurrency());
			}

		} else {
			responseDTO.setErrorCode(response.getError().getErrorCode());
			responseDTO.setErrorDesc(response.getError().getErrorDesc());
		}

		return responseDTO;
	}

	/**
	 * Payment verification method for payFort pay@home
	 * 
	 * @param payTransaction
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Pay_AT_HomeResponseDTO payAtHomePaymentTransaction(Pay_AT_HomeRequestDTO payAtHomeDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[PayfortWebServicesImpl::payTransaction pay@home()] Begin");
		Pay_AT_HomeResponseDTO responseDTO = new Pay_AT_HomeResponseDTO();
		try {
			// method initailizations for pay fort
			PAYMENTMETHOD serviceStub = getClientStub();
			MsgHeaderType header = new MsgHeaderType();
			PAYMENTMETHOD_Type.Message message = new PAYMENTMETHOD_Type.Message();
			PAYMENTMETHODResponse.Return response = new PAYMENTMETHODResponse.Return();
			PAYMENTMETHOD_Type.Message.PAYatHOME payAtHome = new PAYMENTMETHOD_Type.Message.PAYatHOME();
			// set data for saop msg
			header.setMsgType(MsgType.PA_YAT_HOME);
			header.setMsgDate(payAtHomeDTO.getMsgDate());

			message.setHeader(header);
			message.setPAYatHOME(setMessageDataForPayATHome(payAtHomeDTO, payAtHome));
			message.setSignature(payAtHomeDTO.getSignature());

			try {
				response = serviceStub.paymentMETHOD(message);

				// setting response to DTO
				responseDTO = setMessageDataForPayATHome(responseDTO, response);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			if (payfortClientConfig.isUseProxy()) {
				setProxy("", "");
			}
		}

		log.debug("[PayfortWebServicesImpl::payAtStorePaymentTransaction()] End");
		return responseDTO;
	}

	private PAYMENTMETHOD_Type.Message.PAYatHOME setMessageDataForPayATHome(Pay_AT_HomeRequestDTO payAtHomeDTO,
			PAYMENTMETHOD_Type.Message.PAYatHOME payAtHome) {

		payAtHome.setAmount(payAtHomeDTO.getAmount());
		payAtHome.setClientCountry(payAtHomeDTO.getClientCountry());
		payAtHome.setClientEmail(payAtHomeDTO.getClientEmail());
		payAtHome.setClientMobile(payAtHomeDTO.getClientMobile());
		payAtHome.setClientName(payAtHomeDTO.getClientName());
		payAtHome.setCurrency(payAtHomeDTO.getCurrency());
		payAtHome.setExpiryDate(payAtHomeDTO.getExpiryDate());
		payAtHome.setItemName(payAtHomeDTO.getItemName());
		payAtHome.setMerchantID(payAtHomeDTO.getMerchantID());
		payAtHome.setOrderID(payAtHomeDTO.getOrderID());
		payAtHome.setServiceName(payAtHomeDTO.getServiceName());
		payAtHome.setTicketNumber(payAtHomeDTO.getTicketNumber());

		return payAtHome;
	}

	private Pay_AT_HomeResponseDTO setMessageDataForPayATHome(Pay_AT_HomeResponseDTO payAtHomeResponseDTO,
			PAYMENTMETHODResponse.Return response) {

		payAtHomeResponseDTO.setMsgDate(response.getHeader().getMsgDate());
		payAtHomeResponseDTO.setSignature(response.getSignature());

		if (response.getError() == null) {
			if (response.getResponseStatus() != null) {
				payAtHomeResponseDTO.setStatus(response.getResponseStatus().getStatus());
				if (response.getResponseStatus().getAdditionalNote() != null)
					payAtHomeResponseDTO.setAdditionalNote(response.getResponseStatus().getAdditionalNote());
			}

			if (response.getPAYatHOME() != null) {
				payAtHomeResponseDTO.setStatusCode(response.getPAYatHOME().getStatusCode());
				payAtHomeResponseDTO.setOrderReference(response.getPAYatHOME().getOrderReference());
				payAtHomeResponseDTO.setOrderID(response.getPAYatHOME().getOrderID());
				payAtHomeResponseDTO.setTrackingNumber(response.getPAYatHOME().getTrackingNumber());
			}

		} else {
			payAtHomeResponseDTO.setErrorCode(response.getError().getErrorCode());
			payAtHomeResponseDTO.setErrorDesc(response.getError().getErrorDesc());
		}

		return payAtHomeResponseDTO;
	}

	private PAYMENTMETHOD getClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {

			if (payfortClientConfig.isUseProxy()) {
				setProxy(payfortClientConfig.getHttpProxy(), payfortClientConfig.getHttpProxyPort());
			}
			
			URL wsdlURL = new URL(payfortClientConfig.getWsdlUrl());
			QName qname = new QName("http://www.payfort.com/", "payments");
			Service service = Service.create(wsdlURL, qname);
			PAYMENTMETHOD port = service.getPort(PAYMENTMETHOD.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			return port;

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}
	
	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
		System.setProperty("http.proxyHost", host);
		System.setProperty("http.proxyPort", port);
	}

}
