package com.isa.thinair.wsclient.core.service.aig;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.AigTOUtils;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.AIGClientConfig;
import com.isa.thinair.wsclient.core.dao.InsuranceDAO;
import com.isa.thinair.wsclient.core.service.aig.AigBL.RequestType;
import com.isaaviation.thinair.webservices.api.airinsurance.Address;
import com.isaaviation.thinair.webservices.api.airinsurance.BenefitIn;
import com.isaaviation.thinair.webservices.api.airinsurance.Header;
import com.isaaviation.thinair.webservices.api.airinsurance.Insured;
import com.isaaviation.thinair.webservices.api.airinsurance.PolicyIn;
import com.isaaviation.thinair.webservices.api.airinsurance.Segment;
import com.isaaviation.thinair.webservices.api.airinsurance.TINSXMLDATA;

public class AIGUtil {
	private static final Log log = LogFactory.getLog(AIGUtil.class);
	
	private static final String ONE_WAY = "OneWay";
	private static final String ROUND_TRIP = "RoundTrip";
	
	private static final String DIRECT_SALES = "0";
	private static final String AGENCY_SALES = "2";

	/**
	 * Prepare the Request Object for AIG
	 * 
	 * @param type
	 * @param insuranceRequest
	 * @return
	 * @throws ModuleException
	 */
	public static TINSXMLDATA prepareAIGObject(RequestType type, IInsuranceRequest insuranceRequest, InsuranceCountryTO insuranceCountryTO)
			throws ModuleException {
		log.debug("Begin prepareAIGObject [Request Type = " + type.toString() + "]");
		AIGClientConfig aigClientConfig = WSClientModuleUtils.getModuleConfig().getAigClientConfig();

		TINSXMLDATA tinsReq = new TINSXMLDATA();
		InsuranceRequestAssembler insuranceRequestAssembler = (InsuranceRequestAssembler) insuranceRequest;

		if (RequestType.QUOTE.equals(type)) {
			insuranceRequestAssembler.constructNewMessageId(aigClientConfig.getGdsCode(), insuranceCountryTO.getCountryCodeAIG());
		}

		Header header = new Header();
		header.setMessageType(aigClientConfig.getMessageType());
		header.setVersion(aigClientConfig.getVersion());
		header.setSourceId(aigClientConfig.getGdsCode());
		header.setMessageId(insuranceRequestAssembler.getMessageId());

		Segment segment = new Segment();
		// decide txn type
		if (RequestType.QUOTE.equals(type)) {
			segment.setTransactionType(aigClientConfig.getTransactionTypeQuote());
			segment.setTransactionId(insuranceRequestAssembler.getMessageId());
		} else if (RequestType.SELL.equals(type)) {
			segment.setTransactionType(aigClientConfig.getTransactionTypeSell());
			segment.setTransactionId(insuranceRequestAssembler.getMessageId());

		} else {
			segment.setTransactionType(aigClientConfig.getTransactionTypeSell());
			segment.setUniqueTransactionChk(aigClientConfig.getRetryFlag());
			segment.setTransactionId(insuranceRequestAssembler.getMessageId());
		}

		PolicyIn policyIn = new PolicyIn();
		policyIn.setGDSCode(aigClientConfig.getGdsCode());
		policyIn.setAgencyPCC(aigClientConfig.getAgencyPCC());
		policyIn.setAgencyCode(aigClientConfig.getAgencyCode());
		policyIn.setIATACntryCd(insuranceCountryTO.getCountryCodeAIG());

		if (RequestType.SELL.equals(type)) {
			policyIn.setTransactionReferenceNumber(insuranceRequestAssembler.getPnr());
			policyIn.setVoucherNo(insuranceRequestAssembler.getPnr());
		}

		// for resell no need to set the following
		if (!RequestType.RESELL.equals(type)) {
			policyIn.setTransactionApplDate(insuranceRequestAssembler.getDateTimeNow());
		}

		// TODO
		// ADD Trip Type --- Round Trip or Single Trip
		policyIn.setInceptionDate(AigTOUtils.getAIGDate(insuranceRequestAssembler.getInsureSegment().getDepartureDate()));
		if (insuranceRequestAssembler.getInsureSegment().getArrivalDateOffset() != null) {
			policyIn.setInceptionDateOffset(insuranceRequestAssembler.getInsureSegment().getArrivalDateOffset());
			policyIn.setTransactionEffDateOffset(insuranceRequestAssembler.getInsureSegment().getArrivalDateOffset());
		} else {
			policyIn.setInceptionDateOffset(getCountryGMTOffset(insuranceRequestAssembler.getInsureSegment().getFromAirportCode()));
			policyIn.setTransactionEffDateOffset(getCountryGMTOffset(insuranceRequestAssembler.getInsureSegment().getFromAirportCode()));
		}
		policyIn.setExpirationDate(insuranceRequestAssembler.getInsuranceEndDate("0"));
		if (insuranceRequestAssembler.getInsureSegment().getDepartureDateOffset() != null) {
			policyIn.setExpirationDateOffset(insuranceRequestAssembler.getInsureSegment().getDepartureDateOffset());
			policyIn.setTransactionExpDateOffset(insuranceRequestAssembler.getInsureSegment().getDepartureDateOffset());
		} else {
			policyIn.setExpirationDateOffset(getCountryGMTOffset(insuranceRequestAssembler.getInsureSegment().getToAirportCode()));
			policyIn.setTransactionExpDateOffset(getCountryGMTOffset(insuranceRequestAssembler.getInsureSegment().getToAirportCode()));
		}

		policyIn.setTransactionEffDate(policyIn.getInceptionDate());
		policyIn.setTransactionExpDate(policyIn.getExpirationDate());

		policyIn.setOriginatingCity(insuranceRequestAssembler.getInsureSegment().getFromAirportCode());
		policyIn.setFurthestCity(insuranceRequestAssembler.getInsureSegment().getToAirportCode());
		policyIn.setGDSProductCode(aigClientConfig.getGdsProductCode());

		if (insuranceRequestAssembler.getInsureSegment().isRoundTrip()) {
			policyIn.setTourName(ROUND_TRIP);
		} else {
			policyIn.setTourName(ONE_WAY);
		}

		if (insuranceRequestAssembler.getInsureSegment().getSalesChanelCode() == null
				|| insuranceRequestAssembler.getInsureSegment().getSalesChanelCode() == 4) {
			policyIn.setPackageFg(DIRECT_SALES);
		} else {
			policyIn.setPackageFg(AGENCY_SALES);
		}

		segment.setPolicyIn(policyIn);

		// add passenger information
		boolean policyHolderFound = false;
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		String carrierName = AppSysParamsUtil.getCarrierDesc(carrierCode);

		Collection<InsurePassenger> insurePassengers = insuranceRequestAssembler.getPassengers();
		for (InsurePassenger insurePassenger : insurePassengers) {
			Insured insured = new Insured();
			insured.setFirstNm(insurePassenger.getFirstName());
			insured.setLastNm(insurePassenger.getLastName());
			insured.setPartnerCompanyCd(carrierCode + " - " + carrierName);

			Address add = new Address();
			add.setStreetLn1Nm(insurePassenger.getAddressLn1());
			add.setStreetLn2Nm(insurePassenger.getAddressLn2());
			add.setCityNm(insurePassenger.getCity());
			add.setStateNm(insurePassenger.getCity());
			add.setZipCd(null);
			add.setEmailAddr(insurePassenger.getEmail());
			add.setHomePhoneNo(insurePassenger.getHomePhoneNumber());
			add.setCellPhoneNo(insurePassenger.getHomePhoneNumber());
			add.setCntryNm(insurePassenger.getCountryOfAddress());
			insured.setAddress(add);

			// @assumption: first adult is the policy holder
			if (!insurePassenger.isInfant() && !policyHolderFound) {
				insured.setIsInsuredFlag(aigClientConfig.getInsuredPolicyHolderFlag());
				policyHolderFound = true;
			} else {
				insured.setIsInsuredFlag(aigClientConfig.getInsuredFlag());
			}
			insured.setBirthDt(getAigDOBWithRulesApplied(insurePassenger.getDateOfBirth()));
			if (insuranceRequestAssembler.getInsureSegment().isRoundTrip()) {
				insured.setPlanCode(aigClientConfig.getPlanCodeReturn());
			} else {
				insured.setPlanCode(aigClientConfig.getPlanCodeOneWay());
			}
			BenefitIn bf = new BenefitIn();
			bf.setBenefitCd(aigClientConfig.getBenefitCode());
			insured.setBenefitIn(bf);
			segment.getInsured().add(insured);
		}

		tinsReq.setHeader(header);
		tinsReq.setSegment(segment);
		log.debug("Exit prepareAIGObject");
		return tinsReq;
	}

	/**
	 * Returns AIG Country data for specified airport
	 * 
	 * @param originAirport
	 * @return
	 * @throws ModuleException
	 */
	static InsuranceCountryTO getCountryTO(String originAirport) throws ModuleException {
		// NOTE - Removed cashing part as we can disable insurance runtime.
		InsuranceCountryTO aigCountryTO = null;
		aigCountryTO = InsuranceDAO.getCountryInformation(originAirport);
		if (aigCountryTO == null) {
			throw new ModuleException("wsclient.aig.origin.notsupported");
		}
		return aigCountryTO;
	}

	private static String getAigDOBWithRulesApplied(Date ibeDOB) {
		AIGClientConfig aigClientConfig = WSClientModuleUtils.getModuleConfig().getAigClientConfig();
		String defaultAigDate = aigClientConfig.getPassengerDefaultBirthday();

		if (ibeDOB == null) {
			return defaultAigDate;
		}
		try {
			GregorianCalendar aigGC = new GregorianCalendar();
			aigGC.setTime(ibeDOB);

			GregorianCalendar nowDateOfBirth = new GregorianCalendar();
			nowDateOfBirth.setTime(new Date());
			nowDateOfBirth.set(Calendar.MONTH, nowDateOfBirth.get(Calendar.MONTH) - 4);

			if (CalendarUtil.isGreaterThan(aigGC.getTime(), nowDateOfBirth.getTime())) {
				return defaultAigDate;
			} else {
				return AigTOUtils.getAIGBirthDate(ibeDOB);
			}
		} catch (Throwable e) {
			log.error(e);
			return defaultAigDate;
		}
	}

	static BigDecimal[] getBaseCurrencyAmount(String currencyCode, BigDecimal... values) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal[] convertedValues = null;
		if (!baseCurrencyCode.equals(currencyCode) && values != null && values.length > 0) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			convertedValues = new BigDecimal[values.length];
			for (int i = 0; i < values.length; i++) {
				convertedValues[i] = AccelAeroRounderPolicy.convertAndRound(values[i], exchangeRate, null, null);
			}
		} else {
			convertedValues = values;
		}
		return convertedValues;
	}

	private static String getCountryGMTOffset(String airportCode) throws ModuleException {
		CachedAirportDTO airport = WSClientModuleUtils.getAirportBD().getAllAirportOperatorMap().get(airportCode);
		if (airport.getGmtOffsetHours() % 60 != 0) {
			return airport.getGmtOffsetAction() + (double) airport.getGmtOffsetHours() / 60;
		} else {
			return airport.getGmtOffsetAction() + airport.getGmtOffsetHours() / 60;
		}
	}

}
