package com.isa.thinair.wsclient.core.service.lms.impl.gravty;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.config.GravtyConfig;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.CancelRedemptionRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.CancelRedemptionResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Country;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Empty;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.EnrollmentChannel;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberByIDResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberData;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberRequestData;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Nationality;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.PasswordSet;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.PrefferedLanguage;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.SignedUrlRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.SignedUrlResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.userCredentials;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtySysDto.Credentials;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtySysDto.SystemAuthTokenResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.RedeemPointsResponse;

public class LMSGravtyWebServiceInvoker {

	private static Log log = LogFactory.getLog(LMSGravtyWebServiceInvoker.class);

	public static final GravtyConfig gravtyConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig()
			.getGravtyConfig();
	public static final String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_LMS;

	public static final String ERROR_CODE = "errorCode";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String COOKIE = "Cookie";

	public static final String SYS_LOGIN = "SYS_LOGIN";
	public static final String LOGIN_MEMBER = "LOGIN_MEMBER";
	public static final String FETCH_MEMBER = "FETCH_MEMBER";
	public static final String MEMBER_PASSWORD = "MEMBER_PASSWORD";
	public static final String FETCH_MEMBER_BY_EMAIL = "FETCH_MEMBER_BY_EMAIL";
	public static final String CREATE_MEMBER = "CREATE_MEMBER";
	public static final String UPDATE_MEMBER = "UPDATE_MEMBER";
	public static final String COUNTRY = "COUNTRY";
	public static final String NATIONALITY = "NATIONALITY";
	public static final String ENROLLMENT_CHANNEL = "ENROLLMENT_CHANNEL";
	public static final String REDEEM_POINTS = "REDEEM_POINTS";
	public static final String CANCEL_REDEMPTION = "CANCEL_REDEMPTION";
	public static final String PREFFERED_LANGUAGE = "PREFFERED_LANGUAGE";
	public static final String GENERATE_SIGNED_URL = "GENERATE_SIGNED_URL";

	public static String MEMBER_ID = "MEMBER_ID";

	@SuppressWarnings({ "rawtypes" })
	public static GravtyRestServiceClient getServiceCleint(final String serviceCode) throws ModuleException {

		GravtyRestServiceClient stub;
		String url = getServiceUrl(serviceCode);

		switch (serviceCode) {

		case SYS_LOGIN:
			stub = new GravtyRestServiceClient<SystemAuthTokenResponse, Credentials>(SystemAuthTokenResponse.class,
					url, false);
			break;

		case LOGIN_MEMBER:
			stub = new GravtyRestServiceClient<MemberRemoteLoginResponseDTO, userCredentials>(
					MemberRemoteLoginResponseDTO.class, url, false);
			break;

		case FETCH_MEMBER:
			stub = new GravtyRestServiceClient<MemberByIDResponse, String>(MemberByIDResponse.class, url, true);
			break;
		case FETCH_MEMBER_BY_EMAIL:
			stub = new GravtyRestServiceClient<MemberFetchDataResponse, String>(MemberFetchDataResponse.class, url, true);
			break;
		case MEMBER_PASSWORD:
			stub = new GravtyRestServiceClient<PasswordSet, userCredentials>(PasswordSet.class, url, true);
			break;
		case CREATE_MEMBER:
			stub = new GravtyRestServiceClient<MemberData, MemberRequestData>(MemberData.class, url, true);
			break;
		case UPDATE_MEMBER:
			stub = new GravtyRestServiceClient<MemberData, MemberRequestData>(MemberData.class, url, true);
			break;
		case COUNTRY:
			stub = new GravtyRestServiceClient<Country[], Empty>(Country[].class, url,true);
			break;
		case NATIONALITY:
			stub = new GravtyRestServiceClient<Nationality[], Empty>(Nationality[].class, url, true);
			break;
		case ENROLLMENT_CHANNEL:
			stub = new GravtyRestServiceClient<EnrollmentChannel[], Empty>(EnrollmentChannel[].class, url, true);
			break;
		case REDEEM_POINTS:
			stub = new GravtyRestServiceClient<RedeemPointsResponse, RedeemPointsRequest>(RedeemPointsResponse.class, url,true);
			break;
		case CANCEL_REDEMPTION:
			stub = new GravtyRestServiceClient<CancelRedemptionResponse,CancelRedemptionRequest>(CancelRedemptionResponse.class, url,true);
			break;
		case PREFFERED_LANGUAGE :
			stub = new GravtyRestServiceClient<PrefferedLanguage[], Void>(PrefferedLanguage[].class, url,true);
			break;
		case GENERATE_SIGNED_URL :
			stub = new GravtyRestServiceClient<SignedUrlResponse, SignedUrlRequest>(SignedUrlResponse.class, url, true);
			break;
		default:
			log.error("Loyalty service unsupported Lms service code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}

		return stub;
	}

	private static String getServiceUrl(String serviceCode) throws ModuleException {
		String property = null;
		property = (String) gravtyConfig.getGravtyURL().getProperty(serviceCode);
		if (property == null) {
			property = (String) gravtyConfig.getDashboardUrls().getProperty(serviceCode);
		}
		if (property == null) {
			property = (String) gravtyConfig.getSupportingLanguages().getProperty(serviceCode);
		}

		if (property == null) {
			log.error("Loyalty service unsupported Lms service code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}
		return property;
	}

}
