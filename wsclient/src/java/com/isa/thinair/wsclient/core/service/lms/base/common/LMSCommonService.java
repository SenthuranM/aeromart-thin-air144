package com.isa.thinair.wsclient.core.service.lms.base.common;

import com.isa.thinair.commons.api.exception.ModuleException;

public interface LMSCommonService {

	public void sendPasswordMessage(String memberAccountId) throws ModuleException;
	
	public String generateS3SignedUrl(String fileName, String mimeType) throws ModuleException;

}
