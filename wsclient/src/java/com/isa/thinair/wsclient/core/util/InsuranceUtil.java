package com.isa.thinair.wsclient.core.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationInsurancePremium;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.InsuranceProductConfig;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;
import com.isa.thinair.wsclient.core.dao.InsuranceDAO;
import com.thoughtworks.xstream.XStream;

public class InsuranceUtil {

	public static InsuranceCountryTO getCountryTO(String originAirport) throws ModuleException {
		InsuranceCountryTO aigCountryTO = null;
		aigCountryTO = InsuranceDAO.getCountryInformation(originAirport);
		if (aigCountryTO == null) {
			throw new ModuleException("wsclient.aig.origin.notsupported");
		}
		return aigCountryTO;
	}

	public static Map<InsuranceTypes, BigDecimal> getBaseCurrencyAmount(Map<InsuranceTypes, BigDecimal> insTypeCharges,
			String currencyCode) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		Map<InsuranceTypes, BigDecimal> insTypeChargesNew = new HashMap<InsuranceTypes, BigDecimal>();

		if (!baseCurrencyCode.equals(currencyCode) && !insTypeCharges.isEmpty()) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);

			Set<InsuranceTypes> keyset = insTypeCharges.keySet();

			for (InsuranceTypes key : keyset) {
				insTypeChargesNew.put(key,
						AccelAeroRounderPolicy.convertAndRound(insTypeCharges.get(key), exchangeRate, null, null));
			}

		} else {
			insTypeChargesNew = insTypeCharges;
		}
		return insTypeChargesNew;
	}

	public static ReservationInsurance createReservationInsurance(String msgId, IInsuranceRequest insuranceRequest,
			BigDecimal quotedTotalPremiumAmount, BigDecimal quotedTotalNetAmount, BigDecimal quotedTotalTaxAmount,
			BigDecimal[] baseCurrencyAmounts, String quotedCurrencyCode) {
		InsuranceRequestAssembler insReqAss = (InsuranceRequestAssembler) insuranceRequest;
		ReservationInsurance reservationInsurance = new ReservationInsurance();
		reservationInsurance.setAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[0] : AccelAeroCalculator
				.getDefaultBigDecimalZero());
		reservationInsurance.setQuotedAmount(quotedTotalPremiumAmount);

		reservationInsurance.setNetAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[1] : AccelAeroCalculator
				.getDefaultBigDecimalZero());
		reservationInsurance.setQuotedNetAmount(quotedTotalNetAmount);

		reservationInsurance.setTaxAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[2] : AccelAeroCalculator
				.getDefaultBigDecimalZero());// Haider
		reservationInsurance.setQuotedTaxAmout(quotedTotalTaxAmount);

		reservationInsurance.setQuotedCurrencyCode(quotedCurrencyCode);

		InsureSegment insureSegment = insReqAss.getInsureSegment();
		reservationInsurance.setDateOfReturn(insureSegment.getArrivalDate());
		reservationInsurance.setDateOfTravel(insureSegment.getDepartureDate());

		reservationInsurance.setMessageId(msgId);
		reservationInsurance.setOrigin(insureSegment.getFromAirportCode());
		reservationInsurance.setDestination(insureSegment.getToAirportCode());
		reservationInsurance.setRoundTrip(insureSegment.isRoundTrip() == true ? "Y" : "N");
		reservationInsurance.setTotalPaxCount(insReqAss.getPassengers().size());

		return reservationInsurance;
	}

	public static Map<InsuranceTypes, BigDecimal> getInsCharges(Map<Integer, ReservationInsurancePremium> insurancePremiumCol) {

		Map<InsuranceTypes, BigDecimal> insTypeCharges = null;

		if (insurancePremiumCol != null) {
			Set<Integer> keyset = insurancePremiumCol.keySet();

			for (int key : keyset) {
				insTypeCharges = insurancePremiumCol.get(key).getInsTypeCharges();
				break;
			}
		}

		return insTypeCharges;
	}

	public static Map<InsuranceTypes, BigDecimal> getInsCharges(List<ReservationInsurance> insurances) {
		Map<InsuranceTypes, BigDecimal> insTypeCharges = new HashMap<InsuranceTypes, BigDecimal>();

		if (insurances != null && !insurances.isEmpty()) {
			// introduce the filter by insurance type
			for (ReservationInsurance insurance : insurances) {
				if (insurance != null && insurance.getInsuranceType() != null) {
					InsuranceTypes type = getInsuranceType(insurance.getInsuranceType().intValue());
					if (insTypeCharges.containsKey(type)) {
						BigDecimal amount = insurance.getAmount();
						amount = amount.add(insTypeCharges.get(type));
						insTypeCharges.put(type, amount);
					} else {
						insTypeCharges.put(type, insurance.getAmount());
					}
				}
			}
		}

		return insTypeCharges;
	}

	private static InsuranceTypes getInsuranceType(int code) {
		InsuranceTypes insuranceType = InsuranceTypes.NONE;
		switch (code) {
		case 0:
			insuranceType = InsuranceTypes.NONE;
			break;
		case 1:
			insuranceType = InsuranceTypes.GENERAL;
			break;
		case 2:
			insuranceType = InsuranceTypes.CANCELATION;
			break;
		case 3:
			insuranceType = InsuranceTypes.MULTIRISK;
			break;
		default:
		}
		return insuranceType;
	}

	public static BigDecimal[] getBaseCurrencyAmount(String currencyCode, BigDecimal... values) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal[] convertedValues = null;
		if (!baseCurrencyCode.equals(currencyCode) && values != null && values.length > 0) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			convertedValues = new BigDecimal[values.length];
			for (int i = 0; i < values.length; i++) {
				convertedValues[i] = AccelAeroRounderPolicy.convertAndRound(values[i], exchangeRate, null, null);
			}
		} else {
			convertedValues = values;
		}
		return convertedValues;
	}

	public static HashMap<String, InsuranceProductConfig> getInsuranceProductConfigs() {

		HashMap<String, InsuranceProductConfig> insuranceProductConfigMAp = null;
		insuranceProductConfigMAp = InsuranceDAO.getInsuraceProductConfig();

		return insuranceProductConfigMAp;
	}

	public static String getInsuranceProductCode(Integer insuranceID) {
		return InsuranceDAO.getInsuraceProductCode(insuranceID);
	}

	public static InsuranceProductConfig getInsuranceProvider(Integer insuranceId) {
		return InsuranceDAO.getInsuranceProvider(insuranceId);
	}

	public static <K, V> void log(Log log, String identifier, K request, V response) {

		if (log.isDebugEnabled()) {
			XStream xtream = new XStream();
			StringBuilder sbxml = new StringBuilder(identifier);
			sbxml.append(" Request Data \n");

			if (request != null) {
				sbxml.append(xtream.toXML(request));
			} else {
				sbxml.append("null");
			}

			sbxml.append("\n Response \n");

			if (response != null) {
				sbxml.append(xtream.toXML(response));
			} else {
				sbxml.append("null");
			}

			log.debug(sbxml);
		}
	}
}
