package com.isa.thinair.wsclient.core.service.lms.impl.gravty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;

public class GravtyDto{
	public static class MemberRequestData{
		private User user;
		private Object profile_image;
		private String salutation;
		private String member_name;
		private String middle_name;
		private String date_of_birth;
		private Object gender;
		private String mobile;
		private String address_line1;
		private String address_line2;
		private String area;
		private String city;
		private String region;
		private String zipcode;
		private Integer country;
		private Integer enrolling_sponsor;
		private String enrolling_location;
		private String enrollment_channel;
		private String family_head;
		private Object facebook_id;
		private String enrollment_referrer;
		private String nationality;
		private String passport_number;

		private Map<String, Object> extra_data = new HashMap<String, Object>();
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public Object getProfile_image() {
			return profile_image;
		}
		public void setProfile_image(Object profile_image) {
			this.profile_image = profile_image;
		}
		public String getSalutation() {
			return salutation;
		}
		public void setSalutation(String salutation) {
			this.salutation = salutation;
		}
		public String getMember_name() {
			return member_name;
		}
		public void setMember_name(String member_name) {
			this.member_name = member_name;
		}
		public String getMiddle_name() {
			return middle_name;
		}
		public void setMiddle_name(String middle_name) {
			this.middle_name = middle_name;
		}
		public String getDate_of_birth() {
			return date_of_birth;
		}
		public void setDate_of_birth(String date_of_birth) {
			this.date_of_birth = date_of_birth;
		}
		public Object getGender() {
			return gender;
		}
		public void setGender(Object gender) {
			this.gender = gender;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public Object getAddress_line1() {
			return address_line1;
		}
		public void setAddress_line1(String address_line1) {
			this.address_line1 = address_line1;
		}
		public String getAddress_line2() {
			return address_line2;
		}
		public void setAddress_line2(String address_line2) {
			this.address_line2 = address_line2;
		}
		public Object getArea() {
			return area;
		}
		
		public String getNationality() {
			return nationality;
		}
		public void setNationality(String nationality) {
			this.nationality = nationality;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public Object getRegion() {
			return region;
		}
		public void setRegion(String region) {
			this.region = region;
		}
		public String getZipcode() {
			return zipcode;
		}
		public void setZipcode(String zipcode) {
			this.zipcode = zipcode;
		}
		public Integer getCountry() {
			return country;
		}
		public void setCountry(Integer country) {
			this.country = country;
		}
		public Integer getEnrolling_sponsor() {
			return enrolling_sponsor;
		}
		public void setEnrolling_sponsor(Integer enrolling_sponsor) {
			this.enrolling_sponsor = enrolling_sponsor;
		}
		public String getEnrolling_location() {
			return enrolling_location;
		}
		public void setEnrolling_location(String enrolling_location) {
			this.enrolling_location = enrolling_location;
		}
		public String getEnrollment_channel() {
			return enrollment_channel;
		}
		public void setEnrollment_channel(String enrollment_channel) {
			this.enrollment_channel = enrollment_channel;
		}
		public String getFamily_head() {
			return family_head;
		}
		public void setFamily_head(String family_head) {
			this.family_head = family_head;
		}
		public Object getFacebook_id() {
			return facebook_id;
		}
		public void setFacebook_id(Object facebook_dd) {
			this.facebook_id = facebook_dd;
		}
		public String getEnrollment_referrer() {
			return enrollment_referrer;
		}
		public void setEnrollment_referrer(String enrollment_referrer) {
			this.enrollment_referrer = enrollment_referrer;
		}
		public Map<String, Object> getExtra_data() {
			return extra_data;
		}
		public void setExtra_data(Map<String, Object> extra_data) {
			this.extra_data = extra_data;
		}

		public String getPassport_number() {
			return passport_number;
		}

		public void setPassport_number(String passport_number) {
			this.passport_number = passport_number;
		}

		public void addCommunicationLanguage(String languageCode) throws ModuleException {
			if (!StringUtil.isNullOrEmpty(languageCode)) {
				this.extra_data.put("preferred_language", languageCode);
			}
		}
		
		public void addAgentCode(String agentcode)throws ModuleException {
			if (!StringUtil.isNullOrEmpty(agentcode)) {
				this.extra_data.put("agent_code", agentcode);
			}
		}

		@Override
		public String toString() {
			return "MemberRequestData [user=" + user + ", profile_image=" + profile_image + ", salutation=" + salutation
					+ ", member_name=" + member_name + ", middle_name=" + middle_name + ", date_of_birth=" + date_of_birth
					+ ", gender=" + gender + ", mobile=" + mobile + ", address_line1=" + address_line1 + ", address_line2="
					+ address_line2 + ", area=" + area + ", city=" + city + ", region=" + region + ", zipcode=" + zipcode
					+ ", country=" + country + ", enrolling_sponsor=" + enrolling_sponsor + ", enrolling_location="
					+ enrolling_location + ", enrollment_channel=" + enrollment_channel + ", family_head=" + family_head
					+ ", facebook_id=" + facebook_id + ", enrollment_referrer=" + enrollment_referrer + ", nationality="
					+ nationality + ", passport_number=" + passport_number + ", extra_data=" + extra_data + "]";
		}		
	}
	
	//for response
	public static class MemberData {
		private Integer id;
		private User user;
		private String member_id;
		private String salutation;
		private Object alias;
		private String date_of_birth;
		private String nationality;
		private Object creditcard_number;
		private String mobile;
		private Integer country;
		private String country_name;
		private Object favorite_store;
		private Object enrollment_touchpoint;
		private String enrollment_channel;
		private Integer enrolling_sponsor;
		private String date_of_joining;
		private Integer membership_tenure;
		private List<Object> favorite_sponsors = null;
		private ExtraData extra_data;
		private String last_activity_date;
		private Integer days_since_bit;
		private Object last_accrual_date;
		private Object last_redemption_date;
		private Object merged_member;
		private Object enrollment_referrer;
		private Boolean validated;
		private Long version_counter;
		private Object member_type;
		
	
		@Override
		public String toString() {
			return "MemberData [id=" + id + ", user=" + user + ", member_id="
					+ member_id + ", salutation=" + salutation + ", alias="
					+ alias + ", date_of_birth=" + date_of_birth
					+ ", nationality=" + nationality + ", creditcard_number="
					+ creditcard_number + ", mobile=" + mobile + ", country="
					+ country + ", country_name=" + country_name
					+ ", favorite_store=" + favorite_store
					+ ", enrollment_touchpoint=" + enrollment_touchpoint
					+ ", enrollment_channel=" + enrollment_channel
					+ ", enrolling_sponsor=" + enrolling_sponsor
					+ ", date_of_joining=" + date_of_joining
					+ ", membership_tenure=" + membership_tenure
					+ ", favorite_sponsors=" + favorite_sponsors
					+ ", extra_data=" + extra_data + ", last_activity_date="
					+ last_activity_date + ", days_since_bit=" + days_since_bit
					+ ", last_accrual_date=" + last_accrual_date
					+ ", last_redemption_date=" + last_redemption_date
					+ ", merged_member=" + merged_member
					+ ", enrollment_referrer=" + enrollment_referrer
					+ ", validated=" + validated + ", version_counter="
					+ version_counter + ", member_type=" + member_type + "]";
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public String getMember_id() {
			return member_id;
		}
		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}
		public String getSalutation() {
			return salutation;
		}
		public void setSalutation(String salutation) {
			this.salutation = salutation;
		}
		public Object getAlias() {
			return alias;
		}
		public void setAlias(Object alias) {
			this.alias = alias;
		}
		public String getDate_of_birth() {
			return date_of_birth;
		}
		public void setDate_of_birth(String date_of_birth) {
			this.date_of_birth = date_of_birth;
		}
		public String getNationality() {
			return nationality;
		}
		public void setNationality(String nationality) {
			this.nationality = nationality;
		}
		public Object getCreditcard_number() {
			return creditcard_number;
		}
		public void setCreditcard_number(Object creditcard_number) {
			this.creditcard_number = creditcard_number;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public Integer getCountry() {
			return country;
		}
		public void setCountry(Integer country) {
			this.country = country;
		}
		public String getCountry_name() {
			return country_name;
		}
		public void setCountry_name(String country_name) {
			this.country_name = country_name;
		}
		public Object getFavorite_store() {
			return favorite_store;
		}
		public void setFavorite_store(Object favorite_store) {
			this.favorite_store = favorite_store;
		}
		public Object getEnrollment_touchpoint() {
			return enrollment_touchpoint;
		}
		public void setEnrollment_touchpoint(Object enrollment_touchpoint) {
			this.enrollment_touchpoint = enrollment_touchpoint;
		}
		public String getEnrollment_channel() {
			return enrollment_channel;
		}
		public void setEnrollment_channel(String enrollment_channel) {
			this.enrollment_channel = enrollment_channel;
		}
		public Integer getEnrolling_sponsor() {
			return enrolling_sponsor;
		}
		public void setEnrolling_sponsor(Integer enrolling_sponsor) {
			this.enrolling_sponsor = enrolling_sponsor;
		}
		public String getDate_of_joining() {
			return date_of_joining;
		}
		public void setDate_of_joining(String date_of_joining) {
			this.date_of_joining = date_of_joining;
		}
		public Integer getMembership_tenure() {
			return membership_tenure;
		}
		public void setMembership_tenure(Integer membership_tenure) {
			this.membership_tenure = membership_tenure;
		}
		public List<Object> getFavorite_sponsors() {
			return favorite_sponsors;
		}
		public void setFavorite_sponsors(List<Object> favorite_sponsors) {
			this.favorite_sponsors = favorite_sponsors;
		}
		public ExtraData getExtra_data() {
			return extra_data;
		}
		public void setExtra_data(ExtraData extra_data) {
			this.extra_data = extra_data;
		}
		public String getLast_activity_date() {
			return last_activity_date;
		}
		public void setLast_activity_date(String last_activity_date) {
			this.last_activity_date = last_activity_date;
		}
		public Integer getDays_since_bit() {
			return days_since_bit;
		}
		public void setDays_since_bit(Integer days_since_bit) {
			this.days_since_bit = days_since_bit;
		}
		public Object getLast_accrual_date() {
			return last_accrual_date;
		}
		public void setLast_accrual_date(Object last_accrual_date) {
			this.last_accrual_date = last_accrual_date;
		}
		public Object getLast_redemption_date() {
			return last_redemption_date;
		}
		public void setLast_redemption_date(Object last_redemption_date) {
			this.last_redemption_date = last_redemption_date;
		}
		public Object getMerged_member() {
			return merged_member;
		}
		public void setMerged_member(Object merged_member) {
			this.merged_member = merged_member;
		}
		public Object getEnrollment_referrer() {
			return enrollment_referrer;
		}
		public void setEnrollment_referrer(Object enrollment_referrer) {
			this.enrollment_referrer = enrollment_referrer;
		}
		public Boolean getValidated() {
			return validated;
		}
		public void setValidated(Boolean validated) {
			this.validated = validated;
		}
		public Long getVersion_counter() {
			return version_counter;
		}
		public void setVersion_counter(Long version_counter) {
			this.version_counter = version_counter;
		}
		public Object getMember_type() {
			return member_type;
		}
		public void setMember_type(Object member_type) {
			this.member_type = member_type;
		}
		


		}
	public static class userCredentials{
		private String username;
		private String password;
		
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
	}
	public static class User {

		private String email;
		private String first_name;
		private String last_name;
		

		@Override
		public String toString() {
			return "User [email=" + email + ", first_name=" + first_name
					+ ", last_name=" + last_name + "]";
		}

		public String getEmail() {
		return email;
		}

		public void setEmail(String email) {
		this.email = email;
		}

		public String getFirstName() {
		return first_name;
		}

		public void setFirstName(String firstName) {
		this.first_name = firstName;
		}

		public String getLastName() {
		return last_name;
		}

		public void setLastName(String lastName) {
		this.last_name = lastName;
		}

	}

	public class Error {

		private String type;
		private String status_code;
		private String code;
		private String message;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getStatus_code() {
			return status_code;
		}

		public void setStatus_code(String status_code) {
			this.status_code = status_code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		@Override
		public String toString() {
			return "Error [type=" + type + ", status_code=" + status_code + ", code=" + code + ", message=" + message + "]";
		}
	}

	public class GravtyServiceErrorResponse {

		private Error error;
		
		private List<Error> errors;

		public Error getError() {
			return error;
		}

		public void setError(Error error) {
			this.error = error;
		}

		public List<Error> getErrors() {
			return errors;
		}

		public void setErrors(List<Error> errors) {
			this.errors = errors;
		}
	}
	
	public static  class ExtraData {

		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Map<String, Object> getAdditionalProperties() {
		return this.additional_properties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additional_properties.put(name, value);
		}

		}
	public  static class Balance {

		private String loyalty_account;
		private Double balance;
		
		public String getLoyalty_account() {
			return loyalty_account;
		}

		public void setLoyalty_account(String loyalty_account) {
			this.loyalty_account = loyalty_account;
		}

		public Double getBalance() {
		return balance;
		}

		public void setBalance(Double balance) {
		this.balance = balance;
		}

		}
	public static class PointsExpiration {

		private Double points;
		private String loyaltyAccount;
		private String expirationDate;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public Double getPoints() {
		return points;
		}

		public void setPoints(Double points) {
		this.points = points;
		}

		public String getLoyaltyAccount() {
		return loyaltyAccount;
		}

		public void setLoyaltyAccount(String loyaltyAccount) {
		this.loyaltyAccount = loyaltyAccount;
		}

		public String getExpirationDate() {
		return expirationDate;
		}

		public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
		}

		public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		}

		}
	
	public static class MemberByIDResponse{
		MemberFetchDataResponse data;

		public MemberFetchDataResponse getData() {
			return data;
		}

		public void setData(MemberFetchDataResponse data) {
			this.data = data;
		}

	}
	
	public static class MemberFetchDataResponse {

		private Integer id;
		private User user;
		private String member_id;
		private String salutation;
//		private Object alias;
		private String date_of_birth;
		private String nationality;
//		private Object creditcard_number;
		private String mobile;
		private Integer country;
//		private String country_name;
//		private List<Object> favorite_store = null;
//		private Integer enrollment_touchpoint;
		private String enrollment_channel;
		private Integer enrolling_sponsor;
//		private String date_of_joining;
//		private Integer membership_tenure;
//		private List<Object> favorite_sponsors = null;
		private ExtraData extra_data;
//		private String last_activity_date;
//		private Integer days_since_bit;
//		private String last_accrual_date;
//		private Object last_redemption_date;
//		private Object merged_member;
		private String enrollment_referrer;
		private Boolean validated;
		private Long version_counter;
//		private String member_type;
		private List<Balance> balances = null;
		private String membership_stage;
		private Object family_type;
		private String family_head;
//		private Object do_not_call;
//		private Object do_not_email;
//		private Object do_not_mail;
//		private Object do_not_text;
//		private List<PointsExpiration> points_expiration = null;
//		private Map<String, Object> additional_properties = new HashMap<String, Object>();
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public String getMember_id() {
			return member_id;
		}
		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}
		public String getSalutation() {
			return salutation;
		}
		public void setSalutation(String salutation) {
			this.salutation = salutation;
		}
/*		public Object getAlias() {
			return alias;
		}
		public void setAlias(Object alias) {
			this.alias = alias;
		}*/
		public String getDate_of_birth() {
			return date_of_birth;
		}
		public void setDate_of_birth(String date_of_birth) {
			this.date_of_birth = date_of_birth;
		}
		public String getNationality() {
			return nationality;
		}
		public void setNationality(String nationality) {
			this.nationality = nationality;
		}
/*		public Object getCreditcard_number() {
			return creditcard_number;
		}
		public void setCreditcard_number(Object creditcard_number) {
			this.creditcard_number = creditcard_number;
		}*/
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public Integer getCountry() {
			return country;
		}
		public void setCountry(Integer country) {
			this.country = country;
		}
/*		public String getCountry_name() {
			return country_name;
		}
		public void setCountry_name(String country_name) {
			this.country_name = country_name;
		}
		public List<Object> getFavorite_store() {
			return favorite_store;
		}
		public void setFavorite_store(List<Object> favorite_store) {
			this.favorite_store = favorite_store;
		}
		public Integer getEnrollment_touchpoint() {
			return enrollment_touchpoint;
		}
		public void setEnrollment_touchpoint(Integer enrollment_touchpoint) {
			this.enrollment_touchpoint = enrollment_touchpoint;
		}*/
		public String getEnrollment_channel() {
			return enrollment_channel;
		}
		public void setEnrollment_channel(String enrollment_channel) {
			this.enrollment_channel = enrollment_channel;
		}
		public Integer getEnrolling_sponsor() {
			return enrolling_sponsor;
		}
		public void setEnrolling_sponsor(Integer enrolling_sponsor) {
			this.enrolling_sponsor = enrolling_sponsor;
		}
/*		public String getDate_of_joining() {
			return date_of_joining;
		}
		public void setDate_of_joining(String date_of_joining) {
			this.date_of_joining = date_of_joining;
		}
		public Integer getMembership_tenure() {
			return membership_tenure;
		}
		public void setMembership_tenure(Integer membership_tenure) {
			this.membership_tenure = membership_tenure;
		}
		public List<Object> getFavorite_sponsors() {
			return favorite_sponsors;
		}
		public void setFavorite_sponsors(List<Object> favorite_sponsors) {
			this.favorite_sponsors = favorite_sponsors;
		}*/
		public ExtraData getExtra_data() {
			return extra_data;
		}
		public void setExtra_data(ExtraData extra_data) {
			this.extra_data = extra_data;
		}
/*		public String getLast_activity_date() {
			return last_activity_date;
		}
		public void setLast_activity_date(String last_activity_date) {
			this.last_activity_date = last_activity_date;
		}
		public Integer getDays_since_bit() {
			return days_since_bit;
		}
		public void setDays_since_bit(Integer days_since_bit) {
			this.days_since_bit = days_since_bit;
		}
		public String getLast_accrual_date() {
			return last_accrual_date;
		}
		public void setLast_accrual_date(String last_accrual_date) {
			this.last_accrual_date = last_accrual_date;
		}
		public Object getLast_redemption_date() {
			return last_redemption_date;
		}
		public void setLast_redemption_date(Object last_redemption_date) {
			this.last_redemption_date = last_redemption_date;
		}
		public Object getMerged_member() {
			return merged_member;
		}
		public void setMerged_member(Object merged_member) {
			this.merged_member = merged_member;
		}*/
		public String getEnrollment_referrer() {
			return enrollment_referrer;
		}
		public void setEnrollment_referrer(String enrollment_referrer) {
			this.enrollment_referrer = enrollment_referrer;
		}
		public Boolean getValidated() {
			return validated;
		}
		public void setValidated(Boolean validated) {
			this.validated = validated;
		}
		public Long getVersion_counter() {
			return version_counter;
		}
		public void setVersion_counter(Long version_counter) {
			this.version_counter = version_counter;
		}
/*		public String getMember_type() {
			return member_type;
		}
		public void setMember_type(String member_type) {
			this.member_type = member_type;
		}*/
		public List<Balance> getBalances() {
			return balances;
		}
		public void setBalances(List<Balance> balances) {
			this.balances = balances;
		}
		public String getMembership_stage() {
			return membership_stage;
		}
		public void setMembership_stage(String membership_stage) {
			this.membership_stage = membership_stage;
		}
		public Object getFamily_type() {
			return family_type;
		}
		public void setFamily_type(Object family_type) {
			this.family_type = family_type;
		}
/*		public Object getDo_not_call() {
			return do_not_call;
		}
		public void setDo_not_call(Object do_not_call) {
			this.do_not_call = do_not_call;
		}
		public Object getDo_not_email() {
			return do_not_email;
		}
		public void setDo_not_email(Object do_not_email) {
			this.do_not_email = do_not_email;
		}
		public Object getDo_not_mail() {
			return do_not_mail;
		}
		public void setDo_not_mail(Object do_not_mail) {
			this.do_not_mail = do_not_mail;
		}
		public Object getDo_not_text() {
			return do_not_text;
		}
		public void setDo_not_text(Object do_not_text) {
			this.do_not_text = do_not_text;
		}
		public List<PointsExpiration> getPoints_expiration() {
			return points_expiration;
		}
		public void setPoints_expiration(List<PointsExpiration> points_expiration) {
			this.points_expiration = points_expiration;
		}
		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}
		public void setAdditional_properties(Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}*/
		public String getFamily_head() {
			return family_head;
		}
		public void setFamily_head(String family_head) {
			this.family_head = family_head;
		}
	}
	public static class PasswordSet {

		private String status;
		private String message;
		private String token;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public String getStatus() {
		return status;
		}

		public void setStatus(String status) {
		this.status = status;
		}

		public String getMessage() {
		return message;
		}

		public void setMessage(String message) {
		this.message = message;
		}

		public String getToken() {
		return token;
		}

		public void setToken(String token) {
		this.token = token;
		}

		public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		}

		}
	public static class Country {

		private Integer id;
		private String name;
		private String iso_code;
		private Long version_counter;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getIso_code() {
			return iso_code;
		}
		public void setIso_code(String iso_code) {
			this.iso_code = iso_code;
		}
		public Long getVersion_counter() {
			return version_counter;
		}
		public void setVersion_counter(Long version_counter) {
			this.version_counter = version_counter;
		}
		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}
		public void setAdditional_properties(Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}
		
	}
	public static class Empty{
		
	}
	public class Countries{
	    Country country; 
	}
	public static class Nationality {

		private Integer id;
		private String type;
		private String description;
		private String code;
		private String name;
		private Boolean active;
		private Long version_counter;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Boolean getActive() {
			return active;
		}

		public void setActive(Boolean active) {
			this.active = active;
		}

		public Long getVersionCounter() {
			return version_counter;
		}

		public void setVersionCounter(Long versionCounter) {
			this.version_counter = versionCounter;
		}

		public Map<String, Object> getAdditionalProperties() {
			return this.additional_properties;
		}

		public void setAdditionalProperty(String name, Object value) {
			this.additional_properties.put(name, value);
		}

	}
	public static class EnrollmentChannel {

		private Integer id;
		private String type;
		private String description;
		private String code;
		private String name;
		private Boolean active;
		private Long version_counter;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Boolean getActive() {
			return active;
		}

		public Long getVersionCounter() {
			return version_counter;
		}

		public void setVersionCounter(Long versionCounter) {
			this.version_counter = versionCounter;
		}

		public Map<String, Object> getAdditionalProperties() {
			return additional_properties;
		}

		public void setAdditionalProperties(Map<String, Object> additionalProperties) {
			this.additional_properties = additionalProperties;
		}

	}

	public class Salutation {

		private Integer id;
		private String type;
		private String description;
		private String code;
		private String name;
		private Boolean active;
		private Integer version_counter;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Boolean getActive() {
			return active;
		}

		public void setActive(Boolean active) {
			this.active = active;
		}

		public Integer getVersionCounter() {
			return version_counter;
		}

		public void setVersionCounter(Integer versionCounter) {
			this.version_counter = versionCounter;
		}

		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

	}

	public static class RedeemPointsRequest {

		private String h_bit_category;
		private String h_bit_type;
		private Integer h_program_id;
		private Integer h_sponsor_id;
		private String h_bit_date;
		private String h_bit_source_generated_id;
		private String h_member_id;
		private Integer pay_in_points;
		private List<RedeemPointsDetailsRequest> lines = null;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getH_bit_category() {
			return h_bit_category;
		}

		public void setH_bit_category(String h_bit_category) {
			this.h_bit_category = h_bit_category;
		}

		public String getH_bit_type() {
			return h_bit_type;
		}

		public void setH_bit_type(String h_bit_type) {
			this.h_bit_type = h_bit_type;
		}

		public Integer getH_program_id() {
			return h_program_id;
		}

		public void setH_program_id(Integer h_program_id) {
			this.h_program_id = h_program_id;
		}

		public Integer getH_sponsor_id() {
			return h_sponsor_id;
		}

		public void setH_sponsor_id(Integer h_sponsor_id) {
			this.h_sponsor_id = h_sponsor_id;
		}

		public String getH_bit_date() {
			return h_bit_date;
		}

		public void setH_bit_date(String h_bit_date) {
			this.h_bit_date = h_bit_date;
		}

		public String getH_bit_source_generated_id() {
			return h_bit_source_generated_id;
		}

		public void setH_bit_source_generated_id(
				String h_bit_source_generated_id) {
			this.h_bit_source_generated_id = h_bit_source_generated_id;
		}

		public String getH_member_id() {
			return h_member_id;
		}

		public void setH_member_id(String h_member_id) {
			this.h_member_id = h_member_id;
		}

		public Integer getPay_in_points() {
			return pay_in_points;
		}

		public void setPay_in_points(Integer pay_in_points) {
			this.pay_in_points = pay_in_points;
		}

		public List<RedeemPointsDetailsRequest> getLines() {
			return lines;
		}

		public void setLines(List<RedeemPointsDetailsRequest> lines) {
			this.lines = lines;
		}

		public Map<String, Object> getRedeem_points_details_request() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemPointsDetailsRequest {

		private String l_product_external_id;
		private Double l_product_amount;
		private Double l_pay_in_points;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getL_product_external_id() {
			return l_product_external_id;
		}

		public void setL_product_external_id(String l_external_product_id) {
			this.l_product_external_id = l_external_product_id;
		}

		public Double getL_product_amount() {
			return l_product_amount;
		}

		public void setL_product_amount(Double product_amount) {
			this.l_product_amount = product_amount;
		}

		public Double getL_pay_in_points() {
			return l_pay_in_points;
		}

		public void setL_pay_in_points(Double l_pay_in_points) {
			this.l_pay_in_points = l_pay_in_points;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemError {

		private String code;
		private String message;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemPointsResponse {

		private RedeemResponseOriginalBit original_bit;
		private RedeemResponseMember member;
		private List<RedeemResponseOfferAction> offer_actions = null;
		private String processing_date;
		private String bit_id;
		private String status;
		private List<Object> availed_privileges = null;
		private Object errors;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public RedeemResponseOriginalBit getOriginal_bit() {
			return original_bit;
		}

		public void setOriginal_bit(RedeemResponseOriginalBit original_bit) {
			this.original_bit = original_bit;
		}

		public RedeemResponseMember getMember() {
			return member;
		}

		public void setMember(RedeemResponseMember member) {
			this.member = member;
		}

		public List<RedeemResponseOfferAction> getOffer_actions() {
			return offer_actions;
		}

		public void setOffer_actions(
				List<RedeemResponseOfferAction> offer_actions) {
			this.offer_actions = offer_actions;
		}

		public String getProcessing_date() {
			return processing_date;
		}

		public void setProcessing_date(String processing_date) {
			this.processing_date = processing_date;
		}

		public String getBit_id() {
			return bit_id;
		}

		public void setBit_id(String bit_id) {
			this.bit_id = bit_id;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public List<Object> getAvailed_privileges() {
			return availed_privileges;
		}

		public void setAvailed_privileges(List<Object> availed_privileges) {
			this.availed_privileges = availed_privileges;
		}

		public Object getErrors() {
			return errors;
		}

		public void setErrors(Object errors) {
			this.errors = errors;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseOriginalBit {

		private RedeemResponseHeader header;
		private List<RedeemPointsDetailsResponse> lines = null;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public RedeemResponseHeader getHeader() {
			return header;
		}

		public void setHeader(RedeemResponseHeader header) {
			this.header = header;
		}

		public List<RedeemPointsDetailsResponse> getLines() {
			return lines;
		}

		public void setLines(List<RedeemPointsDetailsResponse> lines) {
			this.lines = lines;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseHeader {

		private String h_bit_category;
		private String h_bit_type;
		private Integer h_program_id;
		private Integer h_sponsor_id;
		private String h_bit_date;
		private String h_bit_source_generated_id;
		private String h_member_id;
		private Integer pay_in_points;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getH_bit_category() {
			return h_bit_category;
		}

		public void setH_bit_category(String h_bit_category) {
			this.h_bit_category = h_bit_category;
		}

		public String getH_bit_type() {
			return h_bit_type;
		}

		public void setH_bit_type(String h_bit_type) {
			this.h_bit_type = h_bit_type;
		}

		public Integer getH_program_id() {
			return h_program_id;
		}

		public void setH_program_id(Integer h_program_id) {
			this.h_program_id = h_program_id;
		}

		public Integer getH_sponsor_id() {
			return h_sponsor_id;
		}

		public void setH_sponsor_id(Integer h_sponsor_id) {
			this.h_sponsor_id = h_sponsor_id;
		}

		public String getH_bit_date() {
			return h_bit_date;
		}

		public void setH_bit_date(String h_bit_date) {
			this.h_bit_date = h_bit_date;
		}

		public String getH_bit_source_generated_id() {
			return h_bit_source_generated_id;
		}

		public void setH_bit_source_generated_id(
				String h_bit_source_generated_id) {
			this.h_bit_source_generated_id = h_bit_source_generated_id;
		}

		public String getH_member_id() {
			return h_member_id;
		}

		public void setH_member_id(String h_member_id) {
			this.h_member_id = h_member_id;
		}

		public Integer getPay_in_points() {
			return pay_in_points;
		}

		public void setPay_in_points(Integer pay_in_points) {
			this.pay_in_points = pay_in_points;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemPointsDetailsResponse {

		private String l_product_external_id;
		private Double l_product_amount;
		private Double l_pay_in_points;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getL_product_external_id() {
			return l_product_external_id;
		}

		public void setL_product_external_id(String l_external_product_id) {
			this.l_product_external_id = l_external_product_id;
		}

		public Double getL_product_amount() {
			return l_product_amount;
		}

		public void setL_product_amount(Double product_amount) {
			this.l_product_amount = product_amount;
		}

		public Double getL_pay_in_points() {
			return l_pay_in_points;
		}

		public void setL_pay_in_points(Double l_pay_in_points) {
			this.l_pay_in_points = l_pay_in_points;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseAffiliatedSponsors {

		private Boolean _2;
		private Boolean _13;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Boolean get2() {
			return _2;
		}

		public void set2(Boolean _2) {
			this._2 = _2;
		}

		public Boolean get13() {
			return _13;
		}

		public void set13(Boolean _13) {
			this._13 = _13;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}
	
	public static class RedeemResponseAttributes {

		private String member_id;
		private String mobile;
		private String last_name;
		private String last_activity_date;
		private String membership_stage;
		private String enrolling_sponsor;
		private String nationality;
		private String date_of_joining;
		private String relationship_cost;
		private String salutation;
		private String first_name;
		private String last_accrual_date;
		private String email;
		private String last_redemption_date;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getMember_id() {
			return member_id;
		}

		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getLast_name() {
			return last_name;
		}

		public void setLast_name(String last_name) {
			this.last_name = last_name;
		}

		public String getLast_activity_date() {
			return last_activity_date;
		}

		public void setLast_activity_date(String last_activity_date) {
			this.last_activity_date = last_activity_date;
		}

		public String getMembership_stage() {
			return membership_stage;
		}

		public void setMembership_stage(String membership_stage) {
			this.membership_stage = membership_stage;
		}

		public String getEnrolling_sponsor() {
			return enrolling_sponsor;
		}

		public void setEnrolling_sponsor(String enrolling_sponsor) {
			this.enrolling_sponsor = enrolling_sponsor;
		}

		public String getNationality() {
			return nationality;
		}

		public void setNationality(String nationality) {
			this.nationality = nationality;
		}

		public String getDate_of_joining() {
			return date_of_joining;
		}

		public void setDate_of_joining(String date_of_joining) {
			this.date_of_joining = date_of_joining;
		}

		public String getRelationship_cost() {
			return relationship_cost;
		}

		public void setRelationship_cost(String relationship_cost) {
			this.relationship_cost = relationship_cost;
		}

		public String getSalutation() {
			return salutation;
		}

		public void setSalutation(String salutation) {
			this.salutation = salutation;
		}

		public String getFirst_name() {
			return first_name;
		}

		public void setFirst_name(String first_name) {
			this.first_name = first_name;
		}

		public String getLast_accrual_date() {
			return last_accrual_date;
		}

		public void setLast_accrual_date(String last_accrual_date) {
			this.last_accrual_date = last_accrual_date;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getLast_redemption_date() {
			return last_redemption_date;
		}

		public void setLast_redemption_date(String last_redemption_date) {
			this.last_redemption_date = last_redemption_date;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class LoyaltyBalances {

		private Integer _1;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Integer get1() {
			return _1;
		}

		public void set1(Integer _1) {
			this._1 = _1;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseMember {

		private Integer version;
		private String member_id;
		private Integer joining_date;
		private Integer last_bit_date;
		private Integer sponsor_id;
		private RedeemResponseAttributes attributes;
		private RedeemResponseMultiValueAttributes multi_value_attributes;
		private RedeemResponseMto mto;
		private LoyaltyBalances loyalty_balances;
		private RedeemResponseAffiliatedSponsors affiliated_sponsors;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Integer getVersion() {
			return version;
		}

		public void setVersion(Integer version) {
			this.version = version;
		}

		public String getMember_id() {
			return member_id;
		}

		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}

		public Integer getJoining_date() {
			return joining_date;
		}

		public void setJoining_date(Integer joining_date) {
			this.joining_date = joining_date;
		}

		public Integer getLast_bit_date() {
			return last_bit_date;
		}

		public void setLast_bit_date(Integer last_bit_date) {
			this.last_bit_date = last_bit_date;
		}

		public Integer getSponsor_id() {
			return sponsor_id;
		}

		public void setSponsor_id(Integer sponsor_id) {
			this.sponsor_id = sponsor_id;
		}

		public RedeemResponseAttributes getAttributes() {
			return attributes;
		}

		public void setAttributes(RedeemResponseAttributes attributes) {
			this.attributes = attributes;
		}

		public RedeemResponseMultiValueAttributes getMulti_value_attributes() {
			return multi_value_attributes;
		}

		public void setMulti_value_attributes(
				RedeemResponseMultiValueAttributes multi_value_attributes) {
			this.multi_value_attributes = multi_value_attributes;
		}

		public RedeemResponseMto getMto() {
			return mto;
		}

		public void setMto(RedeemResponseMto mto) {
			this.mto = mto;
		}

		public LoyaltyBalances getLoyalty_balances() {
			return loyalty_balances;
		}

		public void setLoyalty_balances(LoyaltyBalances loyalty_balances) {
			this.loyalty_balances = loyalty_balances;
		}

		public RedeemResponseAffiliatedSponsors getAffiliated_sponsors() {
			return affiliated_sponsors;
		}

		public void setAffiliated_sponsors(
				RedeemResponseAffiliatedSponsors affiliated_sponsors) {
			this.affiliated_sponsors = affiliated_sponsors;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseMto {

		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseMultiValueAttributes {

		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

	public static class RedeemResponseOfferAction {

		private String offer_id;
		private String rule_id;
		private String type;
		private String subject;
		private Integer value_n;
		private Object value_s;
		private Object operator;
		private Integer privilege_quantity;
		private Object attribute_type;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();

		public String getOffer_id() {
			return offer_id;
		}

		public void setOffer_id(String offer_id) {
			this.offer_id = offer_id;
		}

		public String getRule_id() {
			return rule_id;
		}

		public void setRule_id(String rule_id) {
			this.rule_id = rule_id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public Integer getValue_n() {
			return value_n;
		}

		public void setValue_n(Integer value_n) {
			this.value_n = value_n;
		}

		public Object getValue_s() {
			return value_s;
		}

		public void setValue_s(Object value_s) {
			this.value_s = value_s;
		}

		public Object getOperator() {
			return operator;
		}

		public void setOperator(Object operator) {
			this.operator = operator;
		}

		public Integer getPrivilege_quantity() {
			return privilege_quantity;
		}

		public void setPrivilege_quantity(Integer privilege_quantity) {
			this.privilege_quantity = privilege_quantity;
		}

		public Object getAttribute_type() {
			return attribute_type;
		}

		public void setAttribute_type(Object attribute_type) {
			this.attribute_type = attribute_type;
		}

		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}

		public void setAdditional_properties(
				Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}

	}

		public static class CancelRedemptionRequest{
		
		private String h_bit_category;
		private String h_bit_type;
		private Integer h_program_id;
		private Integer h_sponsor_id;
		private String h_bit_date;
		private String h_bit_source_generated_id;
		private String h_member_id;
		private String cancel_bit_id;
		private Map<String, Object> additional_properties = new HashMap<String, Object>();
		public String getH_bit_category() {
			return h_bit_category;
		}
		public void setH_bit_category(String h_bit_category) {
			this.h_bit_category = h_bit_category;
		}
		public String getH_bit_type() {
			return h_bit_type;
		}
		public void setH_bit_type(String h_bit_type) {
			this.h_bit_type = h_bit_type;
		}
		public Integer getH_program_id() {
			return h_program_id;
		}
		public void setH_program_id(Integer h_program_id) {
			this.h_program_id = h_program_id;
		}
		public Integer getH_sponsor_id() {
			return h_sponsor_id;
		}
		public void setH_sponsor_id(Integer h_sponsor_id) {
			this.h_sponsor_id = h_sponsor_id;
		}
		public String getH_bit_date() {
			return h_bit_date;
		}
		public void setH_bit_date(String h_bit_date) {
			this.h_bit_date = h_bit_date;
		}
		public String getH_bit_source_generated_id() {
			return h_bit_source_generated_id;
		}
		public void setH_bit_source_generated_id(String h_bit_source_generated_id) {
			this.h_bit_source_generated_id = h_bit_source_generated_id;
		}
		public String getH_member_id() {
			return h_member_id;
		}
		public void setH_member_id(String h_member_id) {
			this.h_member_id = h_member_id;
		}
		public String getCancel_bit_id() {
			return cancel_bit_id;
		}
		public void setCancel_bit_id(String cancel_bit_id) {
			this.cancel_bit_id = cancel_bit_id;
		}
		public Map<String, Object> getAdditional_properties() {
			return additional_properties;
		}
		public void setAdditional_properties(Map<String, Object> additional_properties) {
			this.additional_properties = additional_properties;
		}
	
}
		public static class CancelRedemptionResponse {

			private OriginalBit originalBit;
			private String processingDate;
			private String bitId;
			private Object errors;
			private String status;
			private Map<String, Object> additionalProperties = new HashMap<String, Object>();

			public OriginalBit getOriginalBit() {
			return originalBit;
			}

			public void setOriginalBit(OriginalBit originalBit) {
			this.originalBit = originalBit;
			}

			public String getProcessingDate() {
			return processingDate;
			}

			public void setProcessingDate(String processingDate) {
			this.processingDate = processingDate;
			}

			public String getBitId() {
			return bitId;
			}

			public void setBitId(String bitId) {
			this.bitId = bitId;
			}

			public Object getErrors() {
			return errors;
			}

			public void setErrors(Object errors) {
			this.errors = errors;
			}

			public String getStatus() {
			return status;
			}

			public void setStatus(String status) {
			this.status = status;
			}

			public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
			}

			public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
			}

			}
		public static class OriginalBit {

			private Header header;
			private List<Object> lines = null;
			private Map<String, Object> additionalProperties = new HashMap<String, Object>();

			public Header getHeader() {
			return header;
			}

			public void setHeader(Header header) {
			this.header = header;
			}

			public List<Object> getLines() {
			return lines;
			}

			public void setLines(List<Object> lines) {
			this.lines = lines;
			}

			public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
			}

			public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
			}

			}
		public static class Header {

			private String hBitCategory;
			private String hBitType;
			private Integer hProgramId;
			private Integer hSponsorId;
			private String hBitDate;
			private String hBitSourceGeneratedId;
			private String hMemberId;
			private String cancelBitId;
			private Map<String, Object> additionalProperties = new HashMap<String, Object>();

			public String getHBitCategory() {
			return hBitCategory;
			}

			public void setHBitCategory(String hBitCategory) {
			this.hBitCategory = hBitCategory;
			}

			public String getHBitType() {
			return hBitType;
			}

			public void setHBitType(String hBitType) {
			this.hBitType = hBitType;
			}

			public Integer getHProgramId() {
			return hProgramId;
			}

			public void setHProgramId(Integer hProgramId) {
			this.hProgramId = hProgramId;
			}

			public Integer getHSponsorId() {
			return hSponsorId;
			}

			public void setHSponsorId(Integer hSponsorId) {
			this.hSponsorId = hSponsorId;
			}

			public String getHBitDate() {
			return hBitDate;
			}

			public void setHBitDate(String hBitDate) {
			this.hBitDate = hBitDate;
			}

			public String getHBitSourceGeneratedId() {
			return hBitSourceGeneratedId;
			}

			public void setHBitSourceGeneratedId(String hBitSourceGeneratedId) {
			this.hBitSourceGeneratedId = hBitSourceGeneratedId;
			}

			public String getHMemberId() {
			return hMemberId;
			}

			public void setHMemberId(String hMemberId) {
			this.hMemberId = hMemberId;
			}

			public String getCancelBitId() {
			return cancelBitId;
			}

			public void setCancelBitId(String cancelBitId) {
			this.cancelBitId = cancelBitId;
			}

			public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
			}

			public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
			}

			}
	

	public static class PrefferedLanguage {
		private int id;
		private String type;
		private String code;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

	}

	public static class SignedUrlRequest {
		private String file_name;
		private String file_type;
		private int sponsor_id;
		private int batch_id;

		public String getFile_name() {
			return file_name;
		}

		public void setFile_name(String file_name) {
			this.file_name = file_name;
		}

		public String getFile_type() {
			return file_type;
		}

		public void setFile_type(String file_type) {
			this.file_type = file_type;
		}

		public int getSponsor_id() {
			return sponsor_id;
		}

		public void setSponsor_id(int sponsor_id) {
			this.sponsor_id = sponsor_id;
		}

		public int getBatch_id() {
			return batch_id;
		}

		public void setBatch_id(int batch_id) {
			this.batch_id = batch_id;
		}

	}
	
	public static class SignedUrlResponse {
		private String signed_request;
		private String url;

		public String getSigned_request() {
			return signed_request;
		}

		public void setSigned_request(String signed_request) {
			this.signed_request = signed_request;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
	}

}