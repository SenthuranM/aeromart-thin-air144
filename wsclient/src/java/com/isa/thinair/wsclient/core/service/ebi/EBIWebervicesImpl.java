package com.isa.thinair.wsclient.core.service.ebi;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.PNRTransactionsStatusCheckRQ;
import com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.PNRTransactionsStatusCheckRS;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.EBIWebervices;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.CommonUtil;

/**
 * Services made available by EBI.
 * 
 * @author Mohamed Nasly
 */
public class EBIWebervicesImpl implements EBIWebervices, DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_EBI;

	private static Log log = LogFactory.getLog(EBIWebervicesImpl.class);

	public String getServiceProvider(String carrierCode) {
		if (!PlatformUtiltiies.nullHandler(carrierCode).equals("")) {
			return SERVICE_PROVIDER + ExternalWebServiceInvoker.SERVICE_PROVIDER_SEPERATOR + carrierCode;
		}

		return SERVICE_PROVIDER;
	}

	/**
	 * Returns the status at EBI for a given set of transactions.
	 * 
	 * @param pnrExtTransactionsTO
	 * @return
	 */
	public PNRExtTransactionsTO getTransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO) throws ModuleException {
		PNRTransactionsStatusCheckRS wsPNRTnxs = (PNRTransactionsStatusCheckRS) ExternalWebServiceInvoker.invokeService(
				SERVICE_PROVIDER, "getTransactionStatus", true, new Param(
						preparePNRTransactionsStatusCheckRQ(pnrExtTransactionsTO)));
		PNRExtTransactionsTO aaPNRTnxs = null;
		if (wsPNRTnxs != null) {
			aaPNRTnxs = getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(wsPNRTnxs);
		}
		return aaPNRTnxs;
	}

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at EBI.
	 * 
	 * @param pnrExtTransactionsTOs
	 *            collection of PNRExtTransactionsTO
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 */
	public void requestDailyTnxReconcilation(Date startTimestamp, Date endTimestamp) throws ModuleException {
		log.debug("BEGIN:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
		String agencyCode = ExternalWebServiceInvoker.getServiceAgent(SERVICE_PROVIDER);
		ExternalWebServiceInvoker.invokeService(getServiceProvider(null), "getReconciliationReport", false, new Param(
				prepareTnxsForRecon(startTimestamp, endTimestamp, agencyCode)));
		log.debug("END:: requestDailyTnxReconcilation(startTimestamp,endTimestamp) [serviceProvider=" + SERVICE_PROVIDER + "]");
	}

	/**
	 * Prepares {@link PNRExtTransactionsTO} from {@link PNRTransactionsStatusCheckRS}
	 * 
	 * @param wsPNRTnxs
	 * @return
	 */
	private static PNRExtTransactionsTO getAAPNRExtTnxsFromPNRTransactionsStatusCheckRS(
			PNRTransactionsStatusCheckRS pnrTransactionsStatusCheckRS) {
		PNRExtTransactionsTO aaPNRTnxs = null;

		if (pnrTransactionsStatusCheckRS != null && pnrTransactionsStatusCheckRS.getPNRPaymentTransactions().size() > 0) {
			aaPNRTnxs = new PNRExtTransactionsTO();
			com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.AAPNRPaymentTransactionType wsPNRTnxs = pnrTransactionsStatusCheckRS
					.getPNRPaymentTransactions().get(0);
			aaPNRTnxs.setPnr(wsPNRTnxs.getPNRNo());

			for (Iterator it = wsPNRTnxs.getPaymentTransaction().iterator(); it.hasNext();) {
				com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.AAPNRPaymentTransactionType.PaymentTransaction wsTnx = (com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrs.AAPNRPaymentTransactionType.PaymentTransaction) it
						.next();

				ExternalPaymentTnx aaTnx = new ExternalPaymentTnx();

				aaTnx.setPnr(wsPNRTnxs.getPNRNo());
				aaTnx.setBalanceQueryKey(wsTnx.getAARefNo());
				aaTnx.setAmount(AccelAeroCalculator.parseBigDecimal(wsTnx.getAmount()));
				aaTnx.setChannel(wsTnx.getBankChannel());
				aaTnx.setExternalPayStatus(WSClientModuleUtils.getModuleConfig().getAAExtPayTnxStatus(wsTnx.getBankStatus()));
				aaTnx.setExternalPayId(wsTnx.getBankRefNo());
				aaTnx.setExternalTnxEndTimestamp(wsTnx.getTimestamp().toGregorianCalendar().getTime());

				aaPNRTnxs.addExtPayTransactions(aaTnx);
			}
		}
		return aaPNRTnxs;
	}

	/**
	 * Prepares {@link PNRTransactionsStatusCheckRQ} from {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static PNRTransactionsStatusCheckRQ preparePNRTransactionsStatusCheckRQ(PNRExtTransactionsTO aaPNRTnxs)
			throws ModuleException {
		PNRTransactionsStatusCheckRQ pnrTransactionsStatusCheckRQ = new PNRTransactionsStatusCheckRQ();

		com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.AAPNRPaymentTransactionType wsPNRTnxs = new com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.AAPNRPaymentTransactionType();
		pnrTransactionsStatusCheckRQ.getPNRPaymentTransactions().add(wsPNRTnxs);

		wsPNRTnxs.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.AAPNRPaymentTransactionType.PaymentTransaction wsExtTnx = new com.emiratesbank.ebgservice.pnrtransactionsstatuscheckrq.AAPNRPaymentTransactionType.PaymentTransaction();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()));

			wsPNRTnxs.getPaymentTransaction().add(wsExtTnx);
		}
		return pnrTransactionsStatusCheckRQ;
	}

	/**
	 * Prepares {@link com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType} from
	 * {@link PNRExtTransactionsTO}
	 * 
	 * @param aaPNRTnxs
	 * @return
	 * @throws ModuleException
	 */
	private static com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType getWSPNRExtTnxsForTnxReconRQ(
			PNRExtTransactionsTO aaPNRTnxs) throws ModuleException {
		log.debug("BEGIN getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType wsPNRTnxs = new com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType();
		wsPNRTnxs.setPNRNo(aaPNRTnxs.getPnr());

		for (Iterator it = aaPNRTnxs.getExtPayTransactions().iterator(); it.hasNext();) {
			ExternalPaymentTnx aaExtTnx = (ExternalPaymentTnx) it.next();

			if (log.isDebugEnabled())
				log.debug(aaExtTnx.getSummary().toString());

			com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType.PaymentTransaction wsExtTnx = new com.emiratesbank.ebgservice.transactionsreconrq.AAPNRPaymentTransactionType.PaymentTransaction();
			wsExtTnx.setAARefNo(aaExtTnx.getBalanceQueryKey());
			wsExtTnx.setAAStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getStatus()));
			wsExtTnx.setAmount(aaExtTnx.getAmount().doubleValue());
			wsExtTnx.setBankChannel(aaExtTnx.getChannel());
			wsExtTnx.setBankRefNo(aaExtTnx.getExternalPayId() == null ? "" : aaExtTnx.getExternalPayId());
			wsExtTnx.setBankStatus(WSClientModuleUtils.getModuleConfig().getWSExtPayTnxStatus(aaExtTnx.getExternalPayStatus()));
			wsExtTnx.setTimestamp(CommonUtil.parse(aaExtTnx.getInternalTnxStartTimestamp()));

			wsPNRTnxs.getPaymentTransaction().add(wsExtTnx);
		}
		log.debug("END getWSPNRExtTnxsForTnxReconRQ(PNRExtTransactionsTO)");
		return wsPNRTnxs;
	}

	/**
	 * Set external payment transactions done between given start and end timestamps.
	 * 
	 * @param reconRQ
	 * @param startTimestamp
	 * @param endTimestamp
	 * @throws ModuleException
	 */
	private static com.emiratesbank.ebgservice.transactionsreconrq.TransactionsReconRQ prepareTnxsForRecon(Date startTimestamp,
			Date endTimestamp, String agencyCode) throws ModuleException {
		com.emiratesbank.ebgservice.transactionsreconrq.TransactionsReconRQ reconRQ = new com.emiratesbank.ebgservice.transactionsreconrq.TransactionsReconRQ();
		reconRQ.setStartTimestamp(CommonUtil.parse(startTimestamp));
		reconRQ.setEndTimestamp(CommonUtil.parse(endTimestamp));

		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setStartTimestamp(startTimestamp);
		criteriaDTO.setEndTimestamp(endTimestamp);
		criteriaDTO.setAgentCode(agencyCode);
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);

		Map pnrTnxsMap = WSClientModuleUtils.getResQueryBD().getExtPayTransactions(criteriaDTO);

		if (pnrTnxsMap != null) {
			for (Iterator pnrsIt = pnrTnxsMap.keySet().iterator(); pnrsIt.hasNext();) {
				PNRExtTransactionsTO pnrTnxs = (PNRExtTransactionsTO) pnrTnxsMap.get(pnrsIt.next());
				if (pnrTnxs != null && pnrTnxs.getExtPayTransactions() != null) {
					reconRQ.getPNRPaymentTransactions().add(getWSPNRExtTnxsForTnxReconRQ(pnrTnxs));
				}
			}
		}
		return reconRQ;
	}
}
