package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward;

import java.util.List;
import java.util.Set;

public class AeroRewardDto {

	public static class Member {
		private String title;
		private String firstName;
		private String lastName;
		private String fullName;
		private String nationality;
		private String gender;
		private String dateOfBirth;
		private String accountID;
		private String loginName;
		private String version;
		private String status;
		private String password;
		private String reTypePassword;
		private boolean resetPassword;
		private String memberType;
		private String referredMemberId;
		private String communicationLanguage;
		private String enrollLocation;
		private String businessUnit;
		private boolean welcomeEmailSent;
		private String profPicUrl;
		private String profPicImageName;
		private String phoneNumber;
		private String countryOfResidence;
		private String email;
		private String mobile;
		private String passportNumber;
		private String issuedCountry;
		private String issuedDate;
		private String passportExpireDate;
		private String issuingAuthority;
		private String regionCode;
		private String streetAddress;
		private String streetAddressContinue;
		private String city;
		private String state;
		private String zipCode;
		private MemberPoints loyaltyPoints;
		private String headOfFamilyAccountId;
		private Set<FamilyMemberDto> pendingFamilyMembers;
	    private Set<FamilyMemberDto> confirmedFamilyMembers;
		private List<MemberPartnerAccountIdDto> memberPartnerAccountIdList;
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getFullName() {
			return fullName;
		}
		public void setFullName(String fullName) {
			this.fullName = fullName;
		}
		public String getNationality() {
			return nationality;
		}
		public void setNationality(String nationality) {
			this.nationality = nationality;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(String dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getAccountID() {
			return accountID;
		}
		public void setAccountID(String accountID) {
			this.accountID = accountID;
		}
		public String getLoginName() {
			return loginName;
		}
		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getReTypePassword() {
			return reTypePassword;
		}
		public void setReTypePassword(String reTypePassword) {
			this.reTypePassword = reTypePassword;
		}
		public boolean isResetPassword() {
			return resetPassword;
		}
		public void setResetPassword(boolean resetPassword) {
			this.resetPassword = resetPassword;
		}
		public String getMemberType() {
			return memberType;
		}
		public void setMemberType(String memberType) {
			this.memberType = memberType;
		}
		public String getReferredMemberId() {
			return referredMemberId;
		}
		public void setReferredMemberId(String referredMemberId) {
			this.referredMemberId = referredMemberId;
		}
		public String getCommunicationLanguage() {
			return communicationLanguage;
		}
		public void setCommunicationLanguage(String communicationLanguage) {
			this.communicationLanguage = communicationLanguage;
		}
		public String getEnrollLocation() {
			return enrollLocation;
		}
		public void setEnrollLocation(String enrollLocation) {
			this.enrollLocation = enrollLocation;
		}
		public String getBusinessUnit() {
			return businessUnit;
		}
		public void setBusinessUnit(String businessUnit) {
			this.businessUnit = businessUnit;
		}
		public boolean isWelcomeEmailSent() {
			return welcomeEmailSent;
		}
		public void setWelcomeEmailSent(boolean welcomeEmailSent) {
			this.welcomeEmailSent = welcomeEmailSent;
		}
		public String getProfPicUrl() {
			return profPicUrl;
		}
		public void setProfPicUrl(String profPicUrl) {
			this.profPicUrl = profPicUrl;
		}
		public String getProfPicImageName() {
			return profPicImageName;
		}
		public void setProfPicImageName(String profPicImageName) {
			this.profPicImageName = profPicImageName;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getCountryOfResidence() {
			return countryOfResidence;
		}
		public void setCountryOfResidence(String countryOfResidence) {
			this.countryOfResidence = countryOfResidence;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getPassportNumber() {
			return passportNumber;
		}
		public void setPassportNumber(String passportNumber) {
			this.passportNumber = passportNumber;
		}
		public String getIssuedCountry() {
			return issuedCountry;
		}
		public void setIssuedCountry(String issuedCountry) {
			this.issuedCountry = issuedCountry;
		}
		public String getIssuedDate() {
			return issuedDate;
		}
		public void setIssuedDate(String issuedDate) {
			this.issuedDate = issuedDate;
		}
		public String getPassportExpireDate() {
			return passportExpireDate;
		}
		public void setPassportExpireDate(String passportExpireDate) {
			this.passportExpireDate = passportExpireDate;
		}
		public String getIssuingAuthority() {
			return issuingAuthority;
		}
		public void setIssuingAuthority(String issuingAuthority) {
			this.issuingAuthority = issuingAuthority;
		}
		public String getRegionCode() {
			return regionCode;
		}
		public void setRegionCode(String regionCode) {
			this.regionCode = regionCode;
		}
		public String getStreetAddress() {
			return streetAddress;
		}
		public void setStreetAddress(String streetAddress) {
			this.streetAddress = streetAddress;
		}
		public String getStreetAddressContinue() {
			return streetAddressContinue;
		}
		public void setStreetAddressContinue(String streetAddressContinue) {
			this.streetAddressContinue = streetAddressContinue;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getZipCode() {
			return zipCode;
		}
		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}
		public MemberPoints getLoyaltyPoints() {
			return loyaltyPoints;
		}
		public void setLoyaltyPoints(MemberPoints loyaltyPoints) {
			this.loyaltyPoints = loyaltyPoints;
		}
		public String getHeadOfFamilyAccountId() {
			return headOfFamilyAccountId;
		}
		public void setHeadOfFamilyAccountId(String headOfFamilyAccountId) {
			this.headOfFamilyAccountId = headOfFamilyAccountId;
		}
		
		public Set<FamilyMemberDto> getPendingFamilyMembers() {
			return pendingFamilyMembers;
		}
		public void setPendingFamilyMembers(Set<FamilyMemberDto> pendingFamilyMembers) {
			this.pendingFamilyMembers = pendingFamilyMembers;
		}
		public Set<FamilyMemberDto> getConfirmedFamilyMembers() {
			return confirmedFamilyMembers;
		}
		public void setConfirmedFamilyMembers(Set<FamilyMemberDto> confirmedFamilyMembers) {
			this.confirmedFamilyMembers = confirmedFamilyMembers;
		}
		public List<MemberPartnerAccountIdDto> getMemberPartnerAccountIdList() {
			return memberPartnerAccountIdList;
		}
		public void setMemberPartnerAccountIdList(List<MemberPartnerAccountIdDto> memberPartnerAccountIdList) {
			this.memberPartnerAccountIdList = memberPartnerAccountIdList;
		}
		
		
		
		
	}

	public static class MemberPoints {
		private double pointsEarnedTotal;
		private double pointsAvailable;
		private double pointsBurnedTotal;
		private double pointsDeducted;
		private double pointsExpired;
		private double pointsSystemAdded;
		private double pointsTransferred;
		private double pointsSystemBlocked;
		private double tierPoints;
		
		public double getPointsEarnedTotal() {
			return pointsEarnedTotal;
		}
		public void setPointsEarnedTotal(double pointsEarnedTotal) {
			this.pointsEarnedTotal = pointsEarnedTotal;
		}
		public double getPointsAvailable() {
			return pointsAvailable;
		}
		public void setPointsAvailable(double pointsAvailable) {
			this.pointsAvailable = pointsAvailable;
		}
		public double getPointsBurnedTotal() {
			return pointsBurnedTotal;
		}
		public void setPointsBurnedTotal(double pointsBurnedTotal) {
			this.pointsBurnedTotal = pointsBurnedTotal;
		}
		public double getPointsDeducted() {
			return pointsDeducted;
		}
		public void setPointsDeducted(double pointsDeducted) {
			this.pointsDeducted = pointsDeducted;
		}
		public double getPointsExpired() {
			return pointsExpired;
		}
		public void setPointsExpired(double pointsExpired) {
			this.pointsExpired = pointsExpired;
		}
		public double getPointsSystemAdded() {
			return pointsSystemAdded;
		}
		public void setPointsSystemAdded(double pointsSystemAdded) {
			this.pointsSystemAdded = pointsSystemAdded;
		}
		public double getPointsTransferred() {
			return pointsTransferred;
		}
		public void setPointsTransferred(double pointsTransferred) {
			this.pointsTransferred = pointsTransferred;
		}
		public double getPointsSystemBlocked() {
			return pointsSystemBlocked;
		}
		public void setPointsSystemBlocked(double pointsSystemBlocked) {
			this.pointsSystemBlocked = pointsSystemBlocked;
		}
		public double getTierPoints() {
			return tierPoints;
		}
		public void setTierPoints(double tierPoints) {
			this.tierPoints = tierPoints;
		}
				
	}

	static class MemberPartnerAccountIdDto {
		private String memberPartnerId;
		private String partnerId;
		public String getMemberPartnerId() {
			return memberPartnerId;
		}
		public void setMemberPartnerId(String memberPartnerId) {
			this.memberPartnerId = memberPartnerId;
		}
		public String getPartnerId() {
			return partnerId;
		}
		public void setPartnerId(String partnerId) {
			this.partnerId = partnerId;
		}
		
		
	}

	public static class RewardsError {
		private String errorMsg;
		private String errorCode;
		public String getErrorMsg() {
			return errorMsg;
		}
		public void setErrorMsg(String errorMsg) {
			this.errorMsg = errorMsg;
		}
		public String getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}		
	}
	
	public static class MemberRetrieveByIdReq {
		private String accountID;

		public String getAccountID() {
			return accountID;
		}

		public void setAccountID(String accountID) {
			this.accountID = accountID;
		}

	}
	
	public static class MemberSearchByIdRequest {
		private String accountID;

		public String getAccountID() {
			return accountID;
		}

		public void setAccountID(String accountID) {
			this.accountID = accountID;
		}
			
	}
	
	public static class FamilyMemberDto {
		private String memberId;

	    private String relationshipToHead;

	    private String passportScreenCaptureUrl;

		public String getMemberId() {
			return memberId;
		}

		public void setMemberId(String memberId) {
			this.memberId = memberId;
		}

		public String getRelationshipToHead() {
			return relationshipToHead;
		}

		public void setRelationshipToHead(String relationshipToHead) {
			this.relationshipToHead = relationshipToHead;
		}

		public String getPassportScreenCaptureUrl() {
			return passportScreenCaptureUrl;
		}

		public void setPassportScreenCaptureUrl(String passportScreenCaptureUrl) {
			this.passportScreenCaptureUrl = passportScreenCaptureUrl;
		}
	    
	    
	}
}
