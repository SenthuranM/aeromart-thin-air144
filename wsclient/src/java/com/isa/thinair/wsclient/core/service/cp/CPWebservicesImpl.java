package com.isa.thinair.wsclient.core.service.cp;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.Handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.wsclient.api.cp.CreatePaiementInfo;
import com.isa.thinair.wsclient.api.cp.Standard;
import com.isa.thinair.wsclient.api.cp.StandardResponse;
import com.isa.thinair.wsclient.api.cp.TransactionInfo;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.SOAPLoggingHandler;

public class CPWebservicesImpl implements DefaultWebserviceClient {

	private static Log log = LogFactory.getLog(CPWebservicesImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		return null;
	}

	public CyberplusResponseDTO cancel(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling webservice API cancel CC payment for %s", cyberplusRequestDTO.getTransactionId()));

		StandardResponse response = new StandardResponse();
		try {
			setProxy(cyberplusRequestDTO.getProxyHost(), cyberplusRequestDTO.getProxyPort());

			URL wsdlURL = new URL("https://systempay.cyberpluspaiement.com/vads-ws/v2?wsdl");
			QName qname = new QName("http://v2.ws.vads.lyra.com/", "StandardWS");
			Service service = Service.create(wsdlURL, qname);
			Standard port = service.getPort(Standard.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			response = port.cancel(cyberplusRequestDTO.getShopId(), cyberplusRequestDTO.getTransmissionDate(),
					cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(),
					cyberplusRequestDTO.getCtxMode(), cyberplusRequestDTO.getComment(), cyberplusRequestDTO.getWsSignature());

		} catch (Exception ex) {
			log.error("The cancel method call failed", ex);
			throw new ModuleException("paymentbroker.error.unknown");
		} finally {
			setProxy("", "");
		}
		return assembleCyberplusResponseDTO(response);
	}

	public CyberplusResponseDTO modify(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling webservice API modify CC payment for %s", cyberplusRequestDTO.getTransactionId()));

		StandardResponse response = new StandardResponse();
		try {
			setProxy(cyberplusRequestDTO.getProxyHost(), cyberplusRequestDTO.getProxyPort());

			URL wsdlURL = new URL("https://systempay.cyberpluspaiement.com/vads-ws/v2?wsdl");
			QName qname = new QName("http://v2.ws.vads.lyra.com/", "StandardWS");
			Service service = Service.create(wsdlURL, qname);
			Standard port = service.getPort(Standard.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			response = port.modify(cyberplusRequestDTO.getShopId(), cyberplusRequestDTO.getTransmissionDate(),
					cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(),
					cyberplusRequestDTO.getCtxMode(), cyberplusRequestDTO.getAmount(), cyberplusRequestDTO.getCurrency(),
					cyberplusRequestDTO.getPresentationDate(), cyberplusRequestDTO.getComment(),
					cyberplusRequestDTO.getWsSignature());

		} catch (Exception ex) {
			log.error("The modify method call failed", ex);
			throw new ModuleException("paymentbroker.error.unknown");
		} finally {
			setProxy("", "");
		}
		return assembleCyberplusResponseDTO(response);
	}

	public CyberplusTransactionInfoDTO getInfo(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling webservice API getInfo CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		TransactionInfo response = new TransactionInfo();
		try {
			setProxy(cyberplusRequestDTO.getProxyHost(), cyberplusRequestDTO.getProxyPort());

			URL wsdlURL = new URL("https://systempay.cyberpluspaiement.com/vads-ws/v2?wsdl");
			QName qname = new QName("http://v2.ws.vads.lyra.com/", "StandardWS");
			Service service = Service.create(wsdlURL, qname);
			Standard port = service.getPort(Standard.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			response = port.getInfo(cyberplusRequestDTO.getShopId(), cyberplusRequestDTO.getTransmissionDate(),
					cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(),
					cyberplusRequestDTO.getCtxMode(), cyberplusRequestDTO.getWsSignature());

		} catch (Exception ex) {
			log.error("The getInfo method call failed", ex);
			throw new ModuleException("paymentbroker.error.unknown");
		} finally {
			setProxy("", "");
		}
		return assembleTransactionInfo(response);
	}

	public CyberplusTransactionInfoDTO refund(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling webservice API refund CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		TransactionInfo transactionInfo = new TransactionInfo();
		try {
			setProxy(cyberplusRequestDTO.getProxyHost(), cyberplusRequestDTO.getProxyPort());

			URL wsdlURL = new URL("https://systempay.cyberpluspaiement.com/vads-ws/v2?wsdl");
			QName qname = new QName("http://v2.ws.vads.lyra.com/", "StandardWS");
			Service service = Service.create(wsdlURL, qname);
			Standard port = service.getPort(Standard.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			transactionInfo = port.refund(cyberplusRequestDTO.getShopId(), cyberplusRequestDTO.getTransmissionDate(),
					cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(),
					cyberplusRequestDTO.getCtxMode(), cyberplusRequestDTO.getNewTransactionId(), cyberplusRequestDTO.getAmount(),
					cyberplusRequestDTO.getCurrency(), cyberplusRequestDTO.getPresentationDate(),
					cyberplusRequestDTO.getValidationMode(), cyberplusRequestDTO.getComment(),
					cyberplusRequestDTO.getWsSignature());

		} catch (Exception ex) {
			log.error("The refund method call failed", ex);
			throw new ModuleException("paymentbroker.error.unknown");
		} finally {
			setProxy("", "");
		}
		return assembleTransactionInfo(transactionInfo);
	}

	public CyberplusTransactionInfoDTO create(CyberplusRequestDTO paymentInfo) throws ModuleException {
		log.debug(String.format("calling webservice API create CC payment for %s", paymentInfo.getTransactionId()));
		CreatePaiementInfo paiementInfo = new CreatePaiementInfo();
		paiementInfo.setShopId(paymentInfo.getShopId());
		paiementInfo.setTransmissionDate(paymentInfo.getTransmissionDate());
		paiementInfo.setTransactionId(paymentInfo.getTransactionId());
		paiementInfo.setPaymentMethod(paymentInfo.getPaymentMethod());
		paiementInfo.setOrderId(paymentInfo.getOrderId());
		paiementInfo.setOrderInfo(paymentInfo.getOrderInfo());
		paiementInfo.setOrderInfo2(paymentInfo.getOrderInfo2());
		paiementInfo.setOrderInfo3(paymentInfo.getOrderInfo3());
		paiementInfo.setAmount(paymentInfo.getAmount());
		paiementInfo.setDevise(paymentInfo.getCurrency());
		paiementInfo.setPresentationDate(paymentInfo.getPresentationDate());
		paiementInfo.setValidationMode(paymentInfo.getValidationMode());
		paiementInfo.setCardNumber(paymentInfo.getCardNumber());
		paiementInfo.setCardNetwork(paymentInfo.getCardNetwork());
		paiementInfo.setCardExpirationDate(paymentInfo.getCardExpirationDate());
		paiementInfo.setCvv(paymentInfo.getCvv());
		paiementInfo.setContractNumber(paymentInfo.getContractNumber());
		paiementInfo.setCustomerId(paymentInfo.getCustomerId());
		paiementInfo.setCustomerTitle(paymentInfo.getCustomerTitle());
		paiementInfo.setCustomerName(paymentInfo.getCustomerName());
		paiementInfo.setCustomerPhone(paymentInfo.getCustomerPhone());
		paiementInfo.setCustomerMail(paymentInfo.getCustomerMail());
		paiementInfo.setCustomerAddress(paymentInfo.getCustomerAddress());
		paiementInfo.setCustomerZipCode(paymentInfo.getCustomerZipCode());
		paiementInfo.setCustomerCity(paymentInfo.getCustomerCity());
		paiementInfo.setCustomerCountry(paymentInfo.getCustomerCountry());
		paiementInfo.setCustomerLanguage(paymentInfo.getCustomerLanguage());
		paiementInfo.setCustomerIP(paymentInfo.getCustomerIP());
		paiementInfo.setCustomerSendEmail(paymentInfo.isCustomerSendEmail());
		paiementInfo.setCtxMode(paymentInfo.getCtxMode());
		paiementInfo.setComment(paymentInfo.getComment());

		TransactionInfo transactionInfo = new TransactionInfo();
		try {
			setProxy(paymentInfo.getProxyHost(), paymentInfo.getProxyPort());

			URL wsdlURL = new URL("https://systempay.cyberpluspaiement.com/vads-ws/v2?wsdl");
			QName qname = new QName("http://v2.ws.vads.lyra.com/", "StandardWS");
			Service service = Service.create(wsdlURL, qname);
			Standard port = service.getPort(Standard.class);

			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new SOAPLoggingHandler());
			((BindingProvider) port).getBinding().setHandlerChain(handlerChain);

			transactionInfo = port.create(paiementInfo, paymentInfo.getWsSignature());

		} catch (Exception ex) {
			log.error("The create method call failed", ex);
			throw new ModuleException("paymentbroker.error.unknown");
		} finally {
			setProxy("", "");
		}

		return assembleTransactionInfo(transactionInfo);
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
	}

	private CyberplusTransactionInfoDTO assembleTransactionInfo(TransactionInfo transactionInfo) {
		CyberplusTransactionInfoDTO transactionInfoDTO = new CyberplusTransactionInfoDTO();
		transactionInfoDTO.setErrorCode(transactionInfo.getErrorCode());
		transactionInfoDTO.setTransactionStatus(transactionInfo.getTransactionStatus());
		transactionInfoDTO.setAuthNb(transactionInfo.getAuthNb());
		transactionInfoDTO.setAuthResult(transactionInfo.getAuthResult());
		transactionInfoDTO.setAmount(transactionInfo.getAmount());
		return transactionInfoDTO;
	}

	private CyberplusResponseDTO assembleCyberplusResponseDTO(StandardResponse standardResponse) {
		CyberplusResponseDTO cyberplusResponse = new CyberplusResponseDTO();
		cyberplusResponse.setErrorCode(standardResponse.getErrorCode());
		cyberplusResponse.setExtendedErrorCode(standardResponse.getExtendedErrorCode());
		cyberplusResponse.setTransactionStatus(standardResponse.getTransactionStatus());
		return cyberplusResponse;
	}
}