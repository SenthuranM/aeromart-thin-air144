package com.isa.thinair.wsclient.core.service.lms.impl.gravty.common;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.core.service.lms.base.common.LMSCommonService;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.SignedUrlRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.SignedUrlResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyRestServiceClient;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.LMSGravtyWebServiceInvoker;

public class LMSCommonServiceGravtyImpl implements LMSCommonService {

	@Override
	public void sendPasswordMessage(String memberAccountId) throws ModuleException {

	}

	@SuppressWarnings("unchecked")
	@Override
	public String generateS3SignedUrl(String fileName, String mimeType) throws ModuleException {

		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();

		int sponserId = externalConfig.getSponserId();
		int batchId = externalConfig.getBatchId();

		SignedUrlRequest signedUrlRequest = new SignedUrlRequest();
		signedUrlRequest.setBatch_id(batchId);
		signedUrlRequest.setSponsor_id(sponserId);
		signedUrlRequest.setFile_name(fileName);
		signedUrlRequest.setFile_type(mimeType);

		GravtyRestServiceClient<SignedUrlResponse, SignedUrlRequest> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.GENERATE_SIGNED_URL);

		SignedUrlResponse signedUrlResponse = serviceClent.post(signedUrlRequest);

		return signedUrlResponse.getSigned_request();
	}

}
