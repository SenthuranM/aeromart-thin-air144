package com.isa.thinair.wsclient.core.service.ace;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ws.acord.schemas.crs.travel.global.ace.GetTravelPolicy;
import ws.acord.schemas.crs.travel.global.ace.GetTravelPolicyResponse;
import ws.acord.schemas.crs.travel.global.ace.GetTravelQuote;
import ws.acord.schemas.crs.travel.global.ace.GetTravelQuoteResponse;
import acord_quoteresp.schemas.crs.travel.global.ace.ACORD.InsuranceSvcRs.PersPkgPolicyQuoteInqRs.PersPolicy.QuoteInfo;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.InsuranceTemplate;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;

public class AceWebServiceImpl implements InsuranceTemplate {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_ACE;

	private static String SUCCESS = "Success";

	private static String ACCEPTED = "Accepted";

	private static Log log = LogFactory.getLog(AceWebServiceImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	public enum RequestType {
		SELL, QUOTE, RESELL
	};

	@Override
	public List<InsuranceResponse> quote(IInsuranceRequest iQuoteRequest)
			throws ModuleException {
		log.debug("Begin quoteAceInsurancePolicy");
		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		InsuranceResponse insResponse = null;
		ModuleException mex = null;
		try {
			GetTravelQuote quoteReq = AceUtil
					.prepareQuoteRequest(iQuoteRequest);

			GetTravelQuoteResponse quoteRes = (GetTravelQuoteResponse) ExternalWebServiceInvoker
					.invokeService(getServiceProvider(null), "getTravelQuote",
							true, new Param(quoteReq));

			QuoteInfo quoteInfo = quoteRes.getACORD().getInsuranceSvcRs()
					.getPersPkgPolicyQuoteInqRs().getPersPolicy()
					.getQuoteInfo();
			BigDecimal quotedTotalPremium = null;

			if (quoteInfo != null
					&& quoteInfo.getInsuredFullToBePaidAmt() != null
					&& quoteInfo.getInsuredFullToBePaidAmt().getAmt() != null) {
				quotedTotalPremium = quoteInfo.getInsuredFullToBePaidAmt()
						.getAmt();
			}
			BigDecimal quotedTotalTaxAmount = new BigDecimal(0);
			BigDecimal quotedTotalNetAmount = quotedTotalPremium;
			BigDecimal[] baseCurrencyAmounts = null;

			if (quotedTotalPremium != null) {
				baseCurrencyAmounts = InsuranceUtil.getBaseCurrencyAmount(
						quoteInfo.getInsuredFullToBePaidAmt().getCurCd(),
						new BigDecimal[] { quotedTotalPremium,
								quotedTotalNetAmount, quotedTotalTaxAmount });

				insResponse = new InsuranceResponse();
				insResponse.setSuccess(true);
				insResponse.setErrorCode("0");
				insResponse.setErrorMessage("OK");
				insResponse.setMessageId("");
				insResponse.setEuropianCountry(false);

				insResponse.setQuotedCurrencyCode(quoteInfo
						.getInsuredFullToBePaidAmt().getCurCd());

				insResponse.setTotalPremiumAmount(baseCurrencyAmounts[0]);
				insResponse.setQuotedTotalPremiumAmount(quotedTotalPremium);

				insResponse
						.setAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[0]
								: AccelAeroCalculator
										.getDefaultBigDecimalZero());
				insResponse.setQuotedAmount(quotedTotalPremium);

				insResponse
						.setNetAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[1]
								: AccelAeroCalculator
										.getDefaultBigDecimalZero());
				insResponse.setQuotedNetAmount(quotedTotalNetAmount);

				insResponse
						.setTaxAmount(baseCurrencyAmounts != null ? baseCurrencyAmounts[2]
								: AccelAeroCalculator
										.getDefaultBigDecimalZero());// Haider
				insResponse.setQuotedTaxAmout(quotedTotalTaxAmount);

			} else {
				insResponse = new InsuranceResponse(false, "Error",
						"Insurance Is Not Available", quoteRes.getACORD()
								.getInsuranceSvcRs().getRqUID(), null,
						quotedTotalPremium, AppSysParamsUtil.getBaseCurrency(),
						false);

				if (log.isDebugEnabled()) {
					log.debug("There is an error occurred while calculating total premium for ACE insurance");
				}
			}

		} catch (ModuleException ex) {
			mex = ex;
		} catch (Throwable e) {
			log.error("quoteInsurancePolicy failed", e);
			mex = new ModuleException("wsclient.ins.general");
		} finally {
			if (insResponse == null) {
				insResponse = new InsuranceResponse(false,
						mex.getExceptionCode(), mex.getMessageString(), null,
						null, null, null, false);
			}
		}
		log.debug("End quoteAceInsurancePolicy");
		lstInsuranceResponses.add(insResponse);
		return lstInsuranceResponses;
	}

	@Override
	public List<InsuranceResponse> sell(List<IInsuranceRequest> iRequests)
			throws ModuleException {
		log.debug("Begin sellAceInsurancePolicy");

		List<InsuranceResponse> lstInsuranceResponses = new ArrayList<InsuranceResponse>();
		InsuranceResponse insResponse = null;
		String policyCode = null;
		BigDecimal quotedTotalPremiumAmount = null;
		BigDecimal totalPremiumAmount = null;
		try {
			for (IInsuranceRequest iInsuranceRequest : iRequests) {
				GetTravelPolicy travelPolicy = AceUtil
						.prepareTravelPolicyRequest(iInsuranceRequest);

				GetTravelPolicyResponse response = (GetTravelPolicyResponse) ExternalWebServiceInvoker
						.invokeService(getServiceProvider(null),
								"getTravelPolicy", true,
								new Param(travelPolicy));
				// if success
				if (response.getACORD().getInsuranceSvcRs()
						.getPersPkgPolicyAddRs().getMsgStatus()
						.getMsgStatusCd().equalsIgnoreCase(SUCCESS)
						&& response.getACORD().getInsuranceSvcRs()
								.getPersPkgPolicyAddRs().getPersPolicy()
								.getPolicyStatusCd().equalsIgnoreCase(ACCEPTED)) {

					quotedTotalPremiumAmount = new BigDecimal(response
							.getACORD().getInsuranceSvcRs()
							.getPersPkgPolicyAddRs().getPersPolicy()
							.getCurrentTermAmt().getAmt().doubleValue());
					totalPremiumAmount = InsuranceUtil.getBaseCurrencyAmount(
							response.getACORD().getInsuranceSvcRs()
									.getPersPkgPolicyAddRs().getPersPolicy()
									.getCurrentTermAmt().getCurCd(),
							quotedTotalPremiumAmount)[0];
					policyCode = response.getACORD().getInsuranceSvcRs()
							.getPersPkgPolicyAddRs().getPersPolicy()
							.getPolicyNumber();

					insResponse = new InsuranceResponse(true, "0", "OK",
							response.getACORD().getInsuranceSvcRs().getRqUID(),
							totalPremiumAmount, quotedTotalPremiumAmount,
							response.getACORD().getInsuranceSvcRs()
									.getPersPkgPolicyAddRs().getPersPolicy()
									.getCurrentTermAmt().getCurCd());

					insResponse.setPolicyCode(policyCode);
				} else {
					insResponse = new InsuranceResponse(false, "Error",
							"Insurance Is Not Available", response.getACORD()
									.getInsuranceSvcRs().getRqUID(),
							totalPremiumAmount, quotedTotalPremiumAmount,
							AppSysParamsUtil.getBaseCurrency());
				}
				lstInsuranceResponses.add(insResponse);
			}
		} catch (Exception e) {
			log.error("sellAceInsurancePolicy failed", e);
			throw new ModuleException("wsclient.ins.unstruct.response");
		}
		log.debug("End sellAceInsurancePolicy");

		return lstInsuranceResponses;
	}

}
