package com.isa.thinair.platform.api.interceptor;

public class LifeCycleException extends RuntimeException {

	public LifeCycleException(String msg) {
		super(msg);
	}

	public LifeCycleException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
