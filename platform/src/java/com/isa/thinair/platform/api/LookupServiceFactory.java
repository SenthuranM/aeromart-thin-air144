/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.platform.api;

import com.isa.thinair.platform.core.controller.DefaultLookupService;

/**
 * @author Nasly Factory for initializing lookup service
 */
public class LookupServiceFactory {
	private static LookupService lookupService = null;
	private static Object initializeLock = new Object();
	private static boolean isInitialized = false;

	private LookupServiceFactory() {

	}

	public static LookupService getInstance() {
		if (!isInitialized) {
			synchronized (initializeLock) {
				if (!isInitialized) {
					if (lookupService == null) {
						lookupService = new DefaultLookupService();
						isInitialized = true;
					}
				}
			}
		}

		return lookupService;
	}
}
