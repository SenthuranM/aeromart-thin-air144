/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0
 *
 * Copyright (c) 2005-07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.platform.api;

/**
 * @author Nasly Marker interface that all the business delegates must implement
 */
public interface IServiceDelegate {

	public void setConfigurationSource(String configSource);

	public String getConfigSource();
}
