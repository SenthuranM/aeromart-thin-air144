package com.isa.thinair.platform.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.sql.DataSource;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class PlatformUtiltiies {

	private static ResourceBundle platformMessagesBundle = null;

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public static String getMessage(String messageCode) {
		if (platformMessagesBundle == null) {
			loadResourceBundle();
		}

		if (messageCode == null || messageCode.equals("")) {
			return platformMessagesBundle.getString(DEFULT_MODULE_DESC_CODE);
		} else {
			String message = platformMessagesBundle.getString(messageCode);
			if (message != null)
				return message;
		}
		return platformMessagesBundle.getString(DEFULT_MODULE_DESC_CODE);
	}

	private static void loadResourceBundle() {
		synchronized (lock) {
			Locale locale = new Locale("en");
			platformMessagesBundle = ResourceBundle.getBundle(PLATFORM_MESSAGES_CODES_RESOURCE_BUNDLE_NAME, locale);
		}
	}

	public static String replaceCharAt(String s, int pos, char c) {
		if (c == ' ') {
			return s.substring(0, pos) + s.substring(pos + 1);
		}
		return s.substring(0, pos) + c + s.substring(pos + 1);
	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param string
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	/**
	 * Handles null value and check for empty string.
	 * 
	 * @param string
	 * @return true if not empty string. false otherwise
	 */
	public static Boolean isNotEmptyString(Object object) {
		if (object == null) {
			return false;
		} else {
			if ("".equals(object))
				return false;
			else
				return true;
		}
	}

	public static String prepareSegmentCode(String origin, String destination, List viaPoints, boolean matchExactCombinationOnly) {
		if ((origin == null || origin.equals("")) && (viaPoints == null || viaPoints.size() == 0)
				&& (destination == null || destination.equals(""))) {
			return "%";
		}
		// if (origin!=null && !origin.equals("") && (viaPoints == null || viaPoints.size() == 0) && destination!=null
		// && !destination.equals("")){
		// return origin +"%/" + destination;
		// }

		StringBuffer segmentCode = new StringBuffer("");
		if (origin == null || origin.equals("")) {
			segmentCode.append("___");
		} else {
			segmentCode.append(origin);
		}

		if (!matchExactCombinationOnly)
			segmentCode.append("%");
		if (viaPoints != null && viaPoints.size() > 0) {
			for (int i = 0; i < viaPoints.size(); i++) {
				String viaPoint = (String) viaPoints.get(i);
				if (viaPoint != null) {
					segmentCode.append("/" + viaPoint);
				} else {
					segmentCode.append("/___");
				}
			}
			if (!matchExactCombinationOnly)
				segmentCode.append("%");
		}

		if (destination == null || destination.equals("")) {
			segmentCode.append("/___");
		} else {
			segmentCode.append("/" + destination);
		}

		return segmentCode.toString();
	}

	public static String getRouteCode(String strOri, String strDes, List<String> viaPoints) {
		int maxViaPoint = viaPoints.size();
		String via1 = "", via2 = "", via3 = "", via4 = "", ori = "", des = "", searchStr = "";

		String strVia1 = (viaPoints.size() > 0 ? viaPoints.get(0) : null);

		String strVia2 = (viaPoints.size() > 1 ? viaPoints.get(1) : null);

		String strVia3 = (viaPoints.size() > 2 ? viaPoints.get(2) : null);

		String strVia4 = (viaPoints.size() > 3 ? viaPoints.get(3) : null);

		if ((strDes != null) && !(strDes.equals(""))) {
			ori = "%" + (((strOri != null) && !(strOri.equals(""))) ? (strOri + "/" + "%") : "___/");

		} else {

			ori = ((strOri != null) && !(strOri.equals(""))) ? (strOri + "/" + "%") : "___/";
		}

		des = (((strDes != null) && !(strDes.equals(""))) ? (strDes) : "___");

		if (maxViaPoint >= 1) {
			via1 = ((strVia1 != null) && !(strVia1.equals(""))) ? (strVia1 + "/" + "%") : "___/";
		}

		if (maxViaPoint >= 2) {
			via2 = ((strVia2 != null) && !(strVia2.equals(""))) ? (strVia2 + "/" + "%") : "___/";
		}

		if (maxViaPoint >= 3) {
			via3 = ((strVia3 != null) && !(strVia3.equals(""))) ? (strVia3 + "/" + "%") : "___/";
		}

		if (maxViaPoint == 4) {
			via4 = ((strVia4 != null) && !(strVia4.equals(""))) ? (strVia4 + "/" + "%") : "___/";
		}

		searchStr = ori;
		searchStr += via1;
		searchStr += via2;
		searchStr += via3;
		searchStr += via4;
		searchStr += des;
		return searchStr;
	}

	/**
	 * Returns the first element
	 * 
	 * @param <T>
	 * 
	 * @param collection
	 * @return
	 */
	public static <T> T getFirstElement(Collection<T> collection) {
		Iterator<T> itCollection = collection.iterator();
		T element = null;

		if (itCollection.hasNext()) {
			element = itCollection.next();
		}

		return element;
	}

	/**
	 * When you send a list of codes the method will return a string like: --> 2,3,4,1,2,678 ...
	 * 
	 * @param codes
	 * @return
	 */
	public static String constructINStringForInts(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? iter.next().toString() : inStr + "," + iter.next().toString();

		}
		return inStr;
	}

	/**
	 * When you send a list of codes the method will return a string like: --> '2','4', '67', 'code1'...
	 * 
	 * @param codes
	 * @return
	 */
	public static String constructINStringForCharactors(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? "'" + iter.next().toString() + "'" : inStr + "," + "'" + iter.next().toString() + "'";

		}
		return inStr;
	}

	/**
	 * Method to return the cookie value by passing the server ip address
	 * 
	 * @param ipAddress
	 * @return
	 */
	public static String getCookieValue(String ipAddress) {
		String cookievalue = "";
		if ((ipAddress != null) && (!ipAddress.trim().equals(""))) {
			StringTokenizer cookieToken = new StringTokenizer(ipAddress, ".");
			while (cookieToken.hasMoreTokens()) {
				cookievalue += cookieToken.nextToken();
			}
			cookievalue = Integer.toHexString(Integer.parseInt(cookievalue.trim()));
		}
		return cookievalue.toUpperCase();
	}

	/**
	 * Returns the IP number for the given IP address
	 * 
	 * @param ip
	 * @return
	 */
	public static long getOriginIP(String ip) {
		ip = PlatformUtiltiies.nullHandler(ip);
		if (ip.length() > 0) {
			String[] splitIP = ip.split("\\.");

			try {
				return (Long.parseLong(splitIP[0]) * 16777216) + (Long.parseLong(splitIP[1]) * 65536)
						+ (Long.parseLong(splitIP[2]) * 256) + (Long.parseLong(splitIP[3]));
			} catch (Exception e) {
				return 0;
			}
		}
		return 0;
	}
	
	/**
	 * Returns the set of possible combinations for segment codes
	 * A to D via B & C => [AB,BC,CD]
	 * 
	 * @param combinations
	 * @return
	 */
	public static String getCombinationsForViaPoints(Collection<String> points){
		List<String> combinations = new ArrayList<String>();
		
		StringBuffer aCombination = null;
		String lastPoint = null;
		for (Iterator<String> iterator = points.iterator(); iterator.hasNext();) {
			aCombination = new StringBuffer();
			String aPoint = (String) iterator.next();
			
			if(lastPoint == null){
				lastPoint = aPoint;
				continue;
			}
			
			aCombination.append(lastPoint + "/" + aPoint);
			combinations.add(aCombination.toString());
			
			lastPoint = aPoint;
		}
		
		StringBuffer sb = new StringBuffer("(");
		for (Iterator<String> iterator = combinations.iterator(); iterator.hasNext();) {
			String aSegment = (String) iterator.next();
			sb.append("'" + aSegment + "',");
		}
		sb.delete(sb.length()-1, sb.length());
		sb.append(")");
		
		return sb.toString();
	}

	private static Object lock = new Object();
	private static final String PLATFORM_MESSAGES_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/platformmessages";
	private static final String DEFULT_MODULE_DESC_CODE = "module.desc.code.empty";

}
