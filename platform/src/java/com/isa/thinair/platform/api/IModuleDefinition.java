/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * IModuleDefinition.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.api;

/**
 * @author Nasly Forms a module's definition
 */
public interface IModuleDefinition {
	public void setModuleName(String moduleName);

	public void setImplementation(IModule moduleImpl);

	public void setProxy(IModule moduleProxy);

	public void setDescription(String description);

	public String getModuleName();

	public IModule getImplementation();

	public IModule getProxy();

	public String getDescription();
}
