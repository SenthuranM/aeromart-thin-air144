package com.isa.thinair.platform.api.util;

import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * All module test cases should extends this base class. This uses junit 4
 * 
 * @author rumesh
 * 
 */
public class BaseTestCase {
	protected static final String USER_NAME = "ABYSYSTEM";
	protected static final String PASSWORD = "3O#pass123";
	protected static final String LOGIN_CONFIG_FILE = "/home/rumesh/Documents/ISA/AccelAero/Head/TA/login/src/resources/client/client_jass_login.config";
	private static final String REPO_HOME = "/home/rumesh/isaconfig/config-G9";

	public static void initMethod() {
		System.setProperty("repository.home", REPO_HOME);
		ModuleFramework.startup();
	}
}
