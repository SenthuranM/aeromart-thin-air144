/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

public interface ILifeCycle extends IInitializable, IStartable, ISuspendable, IReconfigurable, IDisposable {

}
