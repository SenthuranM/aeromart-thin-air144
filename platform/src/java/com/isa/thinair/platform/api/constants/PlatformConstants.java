package com.isa.thinair.platform.api.constants;

import java.io.File;
import java.util.Locale;

public abstract class PlatformConstants {

	/** Default locations for storing configurations */
	private static final String DEFAULT_CONFIG_ROOT_PATH_WINDOWS = "c:/isaconfig";
	private static final String DEFAULT_CONFIG_ROOT_PATH_UNIX = "/usr/isa/isaconfig";
	private static final boolean IS_JNDI_OBJECTS_CASHE = true;
	private static final String AUDITS_DIR = "audits";
	private static final String ATTACHMENTS_DIR = "attachments";
	private static final String PNLADL_DIR = "pnl_adl";
	private static final String PALCAL_DIR = "pnl_adl";
	private static final String XAPNL_DIR = "xa_pnl";
	private static final String BSP_DIR = "bsp";
	private static final String DIU_DIR = "diu";
	private static final String RET_DIR = "ret";
	private static final String BSP_UPLOAD_DIR = "upload";
	private static final String BSP_DOWNLOAD_DIR = "download";
	private static final String PNRGOV_SAVE_DIR = "pnrgov";
	private static final String LOYALTY_DIR = "lms";
	private static final String LOYALTY_PRODUCT_DIR = "product_file";
	private static final String LOYALTY_FLOWN_FILE_DIR = "flown_file";

	/** Priviledges */
	public static final String ANY_PNR_SEARCH = "xbe.res.alt.find.any";

	public static final String PRIVI_ACC_XBE_RES = "xbe.res.allow.xbe";
	public static final String PRIVI_ACC_IBE_RES = "xbe.res.allow.ibe";
	public static final String PRIVI_ACC_HOLIDAY_RES = "xbe.res.allow.holiday";
	public static final String PRIVI_ACC_ALL_RES = "xbe.res.allow.all";
	public static final String PRIVI_ACC_DNATA_RES = "xbe.res.allow.dnata";
	public static final String PRIVI_ACC_INTERLINED_RES = "xbe.res.allow.interlined";

	public static final String KEY_UNAUTHORIZED_OPERATION = "unauthorized.operation";

	public static String getConfigRootAbsPath() {
		if (System.getProperty("repository.home") != null && !System.getProperty("repository.home").equals("")) {
			return System.getProperty("repository.home");
		}

		if (System.getProperty("os.name").toLowerCase(Locale.US).indexOf("windows") > -1) {
			return DEFAULT_CONFIG_ROOT_PATH_WINDOWS;
		} else {
			return DEFAULT_CONFIG_ROOT_PATH_UNIX;
		}
	}

	public static String getAbsAuditLogsPath() {
		return getConfigRootAbsPath() + File.separator + AUDITS_DIR;
	}

	public static String getAbsPnlAdlAuditLogsPath() {
		return getAbsAuditLogsPath() + File.separator + PNLADL_DIR;
	}

	public static String getAbsPalCalAuditLogsPath() {
		return getAbsAuditLogsPath() + File.separator + PALCAL_DIR;
	}

	public static String getAbsXAPnlAuditLogsPath() {
		return getAbsAuditLogsPath() + File.separator + XAPNL_DIR;
	}

	public static String getAbsAttachmentsPath() {
		return getConfigRootAbsPath() + File.separator + ATTACHMENTS_DIR;
	}

	public static String getAbsBSPUploadPath() {
		return getConfigRootAbsPath() + File.separator + BSP_DIR + File.separatorChar + BSP_UPLOAD_DIR;
	}

	public static String getAbsRETFileUploadPath() {
		return getConfigRootAbsPath() + File.separator + RET_DIR + File.separatorChar + BSP_UPLOAD_DIR;
	}

	public static String getAbsBSPDownloadPath() {
		return getConfigRootAbsPath() + File.separator + BSP_DIR + File.separatorChar + BSP_DOWNLOAD_DIR;
	}

	public static String getAbsPNRGOVSavePath() {
		return getConfigRootAbsPath() + File.separator + PNRGOV_SAVE_DIR;
	}
	
	public static String getAbsDIUSavePath() {
		return getConfigRootAbsPath() + File.separator + DIU_DIR;
	}

	public static String getAbsLoyaltyProductFileUploadPath() {
		return getConfigRootAbsPath() + File.separator + LOYALTY_DIR + File.separatorChar + LOYALTY_PRODUCT_DIR;
	}

	public static String getAbsLoyaltyFlownFileUploadPath() {
		return getConfigRootAbsPath() + File.separator + LOYALTY_DIR + File.separatorChar + LOYALTY_FLOWN_FILE_DIR;
	}

	public static String getBeanURIPrefix() {
		return "isa:base://";
	}

	public static boolean isJndiObjectsCashe() {
		if (System.getProperty("enable.jndi.object.cache") != null && !System.getProperty("enable.jndi.object.cache").equals("")) {
			return new Boolean(System.getProperty("enable.jndi.object.cache")).booleanValue();
		}
		return IS_JNDI_OBJECTS_CASHE;
	}

}
