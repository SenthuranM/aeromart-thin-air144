/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

/**
 * The <code>IStartable</code> interface is supported by modules that that need to be started or stopped. For example, a
 * module that manages its own threads would need to implement this interface.
 * 
 */
public interface IStartable {

	/**
	 * Start the module. On return, framework will forward client requests to the module.
	 * 
	 * @throws Exception
	 *             if some failure prevents starting the module.
	 */
	public void start() throws Exception;

	/**
	 * Prior to entry, framework will stop forwarding client requests to the module. This gives the module the
	 * opportunity to complete any outstanding work.
	 * 
	 * @throws Exception
	 *             if some failure occurs while stopping the module.
	 */
	public void stop() throws Exception;
}
