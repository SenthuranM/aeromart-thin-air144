/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

public interface IInitializable {

	/**
	 * Initializes the module. Called immediately after the module's constructor. On return, the framework may start the
	 * module.
	 * 
	 * @throws Exception
	 *             if <code>initialize</code> fails.
	 */
	void initialize() throws Exception;

}
