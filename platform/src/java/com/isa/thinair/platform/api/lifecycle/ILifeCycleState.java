/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

public interface ILifeCycleState {

	static final int CREATING = 1;
	static final int INITIALIZING = 2;
	static final int STARTING = 4;
	static final int RUNNING = 8;
	static final int STOPPING = 16;
	static final int STOPPED = 32;
	static final int DESTROYING = 64;
	static final int DESTROYED = 128;
	static final int SUSPENDING = 256;
	static final int SUSPENDED = 512;
	static final int RESUMING = 1024;
	static final int RECONFIGURING = 2048;

}
