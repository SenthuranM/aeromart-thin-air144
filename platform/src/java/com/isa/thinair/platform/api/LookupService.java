/*
 * Created on 06-Jun-2005
 *
 */
package com.isa.thinair.platform.api;

import java.util.Properties;

/**
 * @author Nasly Servicies for looking up modules, business delegates and beans configured
 */
public interface LookupService {
	/**
	 * Retrieves the reference for the module's service implemenation Throws ModuleRuntimeException if module is not
	 * found
	 * 
	 * @param moduleName
	 *            name of the modules as per configurations
	 * @return module's service implementation
	 */
	public IModule getModule(String moduleName);

	/**
	 * Get a bd from a module specifying the full bd key Throws runtime exception if the requested BD is not found
	 * 
	 * @param targetModuleName
	 * @param delegateID
	 * @return
	 * @throws ModuleException
	 */
	public IServiceDelegate getServiceBD(String targetModuleName, String fullBDKey);

	/**
	 * Retrieves a bean configured Throws runtime exception if the requested bean is not found
	 * 
	 * @param uri
	 *            the full qualified bean name
	 * @return bean object
	 */
	public Object getBean(String uri);

	public DefaultModuleConfig getModuleConfig(String moduleName);

	public Object getService(Properties jndiProperties, String serviceName, boolean isNoCaching);
}
