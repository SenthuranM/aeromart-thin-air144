/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.platform.api.constants;

/**
 * 
 * @author Lasantha Pambagoda
 * 
 */
public abstract class PlatformBeanUrls {

	public static interface Commons {
		public static final String GLOBAL_CONFIG_BEAN = "isa:base://modules/commons?id=globalConfig";
		public static final String DEV_CONFIG_BEAN = "isa:base://modules/commons?id=devConfig";
		public static final String COMMONS_DAO_BEAN = "isa:base://modules/commons?id=commonsDAO";
		public static final String AEROSPIKE_CONFIG_BEAN = "isa:base://modules/commons?id=aerospikeCachingService";
		public static final String DEFAULT_DATA_SOURCE_BEAN = "isa:base://modules?id=myDataSource";
		public static final String DEFAULT_LCC_DATA_SOURCE_BEAN = "isa:base://modules?id=mylccDataSource";
		public static final String CLUSTER_DATA_SOURCE_BEAN = "isa:base://modules?id=aaactiveDataSource";
		public static final String REPORT_DATA_SOURCE_BEAN = "isa:base://modules?id=rptDataSource";
		public static final String DEFAULT_CACHE_MANAGER_BEAN = "isa:base://modules?id=cacheManager";
	}
}
