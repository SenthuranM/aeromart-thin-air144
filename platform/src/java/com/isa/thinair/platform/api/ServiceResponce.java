/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.platform.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * responce interface to return responce form the service
 * 
 * @author Lasantha Pambagoda
 */
public interface ServiceResponce extends Serializable {

	public String getResponseCode();

	public Collection<String> getResponseParamNames();

	public Object getResponseParam(String key);

	public void addResponceParam(String key, Object param);

	public void addResponceParams(Map<String, Object> parms);

	public boolean isSuccess();
}
