/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.platform.api.lifecycle;

/**
 * <code>ISuspendable</code> should be implemented by Modules that can be suspended and resumed. One possible use of
 * this capability is to support dynamic reconfiguration.
 * 
 */
public interface ISuspendable {

	/**
	 * Suspend the module. On entry, framework has stopped forwarding requests to the module. Clients that call the
	 * module while it is in the suspended state, will block until the module resumes, or until a timeout occurs.
	 * 
	 * @throws Exception
	 *             if <code>suspend</code> fails.
	 */
	void suspend() throws Exception;

	/**
	 * Resume the module. framework will start forwarding client requests to the module on return of this method. Any
	 * blocked clients, will pick up where they left off.
	 * 
	 * @throws Exception
	 *             if <code>resume</code> fails.
	 */
	void resume() throws Exception;

}
