package com.isa.thinair.platform.api;

/**
 * Marker interface that module implemtation configs need to implement.
 * 
 * @author Nasly
 * 
 */
public interface IConfig {

}
