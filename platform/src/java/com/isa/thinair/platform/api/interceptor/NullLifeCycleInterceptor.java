package com.isa.thinair.platform.api.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.isa.thinair.platform.api.lifecycle.ILifeCycle;

/**
 * Trivial interceptor that does nothing.
 * 
 */
public class NullLifeCycleInterceptor implements MethodInterceptor {

	/**
	 * invoke method of the MethodInterceptor
	 */
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object ret = null;
		Class declaringClass = invocation.getMethod().getDeclaringClass();
		// evaluates to true for non LifeCycle methods
		if (declaringClass.isAssignableFrom(ILifeCycle.class) == false) {
			// allow invocation to proceed for business methods.
			ret = invocation.proceed();
		}
		return ret;
	}

}
