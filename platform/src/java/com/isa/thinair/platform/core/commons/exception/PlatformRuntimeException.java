/*
 * ============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-07 Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.commons.exception;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nasly
 */
public class PlatformRuntimeException extends RuntimeException {

	private String exceptionCode;
	private String moduleCode;
	private Object exceptionDetails;

	public PlatformRuntimeException(Throwable cause, String exceptionCode, String moduleCode) {
		super(cause);
		setExceptionCode(exceptionCode);
		setModuleCode(moduleCode);
	}

	public PlatformRuntimeException(Throwable cause, String exceptionCode) {
		super(cause);
		setExceptionCode(exceptionCode);
	}

	public PlatformRuntimeException(String exceptionCode, String moduleCode) {
		setExceptionCode(exceptionCode);
		setModuleCode(moduleCode);
	}

	public PlatformRuntimeException(String exceptionCode, Object exceptionDetails) {
		setExceptionCode(exceptionCode);
		setExceptionDetails(exceptionDetails);
	}

	public PlatformRuntimeException(String exceptionCode) {
		setExceptionCode(exceptionCode);
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Object getExceptionDetails() {
		return exceptionDetails;
	}

	public void setExceptionDetails(Object exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}

	public String getMessageString() {
		return PlatformUtiltiies.getMessage(getExceptionCode());
	}

	public String getModuleDesc() {
		return PlatformUtiltiies.getMessage(getModuleCode());
	}
}
