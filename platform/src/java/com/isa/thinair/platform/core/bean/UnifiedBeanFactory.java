/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * BeanRegistryBuilder.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.bean;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xerces.util.URI;
import org.apache.xmlbeans.impl.common.ConcurrentReaderHashMap;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

/**
 * @author Nasly
 * 
 */
public class UnifiedBeanFactory implements IUnifiedBeanFactory {
	private static final String ISA_PREFIX = "isa:";

	protected final Log log = LogFactory.getLog(getClass());

	private List _allFactories = new ArrayList();

	File _configRootDirFile;

	private Map _resolvedBeanMap = new ConcurrentReaderHashMap();

	private Map _uriToFactoryMap = new HashMap();

	private List _allFactoryUris = new ArrayList();

	public static final String URI_SCHEME_PLUS_SLASHES = PlatformConstants.getBeanURIPrefix();

	public UnifiedBeanFactory(String absRootPath) {
		try {
			Resource resource = new FileSystemResource(absRootPath);
			_configRootDirFile = resource.getFile();
			initFactories(_configRootDirFile);
		} catch (IOException e) {
			String errMsg = "Configuration repository path is invalid. [absolute configuration repository path provided ="
					+ absRootPath + "]";
			log.error(errMsg, e);
			throw new PlatformRuntimeException(e, "platform.arg.repository.path.invalid", "platform.code");
		}
	}

	private void initFactories(File configRootDirFile) {
		boolean debugOn = log.isDebugEnabled();
		HierachicalBeanFactoryBuilder builder = new HierachicalBeanFactoryBuilder();
		Map factoryMap = builder.build(_configRootDirFile);
		Iterator it = factoryMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			BeanFactory factory = (BeanFactory) entry.getKey();
			_allFactories.add(factory);
			String factoryUri = makeFactoryUri((File) entry.getValue());
			if (debugOn) {
				log.debug("bean factory registered for URI = " + factoryUri);
			}
			_uriToFactoryMap.put(factoryUri, factory);
			_allFactoryUris.add(factoryUri);
		}
	}

	public Object getBean(String uri) {
		Object bean;
		ResolvedEntry entry;
		entry = (ResolvedEntry) _resolvedBeanMap.get(uri);
		if (entry != null) {
			bean = entry.getBean();
			if (log.isDebugEnabled()) {
				log.debug("Bean found in cache, uri = " + uri);
			}
		} else {
			bean = resolveBean(uri);
		}
		return bean;
	}

	public Map getBeansOfType(java.lang.Class type) {
		Map beans = new HashMap();
		Iterator it = _allFactories.iterator();
		while (it.hasNext()) {

			DefaultListableBeanFactory factory = (DefaultListableBeanFactory) it
					.next();
			beans.putAll(factory.getBeansOfType(type));
			
		}
		return beans;
	}

	private String makeFactoryUri(File beanFile) {
		String absConfigRootPath = _configRootDirFile.getAbsolutePath();
		String absBeanFileName = beanFile.getAbsolutePath();
		String beanFileName = absBeanFileName.substring(absConfigRootPath.length() + 1);
		String beanFileNoExt = beanFileName;
		int lastIndx = beanFileName.lastIndexOf(".mod.xml");
		if (lastIndx > -1) {
			beanFileNoExt = beanFileName.substring(0, lastIndx);
		}
		String normalized = beanFileNoExt.replace(File.separatorChar, '/');
		return URI_SCHEME_PLUS_SLASHES + normalized;
	}

	private Object resolveBean(String uriString) {
		Parameter param = getFirstParameter(uriString);
		// TODO: Only support the "id" parameter for now
		if (param == null) {
			String errMsg = "\"id\" parameter is required Bean URI, uri = " + uriString;
			log.error(errMsg);
			throw new PlatformRuntimeException("platform.arg.bean.uri.invalid", "platform.desc");
		}
		if (param.getName().equals("id") == false) {
			String errMsg = "invalid parameter - " + param.getName();
			log.error(errMsg);
			throw new PlatformRuntimeException("platform.arg.bean.uri.invalid", "platform.desc");
		}
		List factories = getFactories(uriString);
		Iterator it = factories.iterator();
		if (!it.hasNext()) {
			String errMsg = "no resource found for uri - " + uriString;
			log.error(errMsg);
			throw new PlatformRuntimeException("platform.arg.bean.notfound", "platform.desc");
		}
		String beanId = param.getValue();
		boolean factoryWasFound = false;
		BeanFactory factory = null;
		while (it.hasNext() && !factoryWasFound) {
			factory = (BeanFactory) it.next();
			if (factory.containsBean(beanId)) {
				ResolvedEntry entry = new ResolvedEntry(factory, beanId);
				_resolvedBeanMap.put(uriString, entry);
				factoryWasFound = true;
			}
		}
		if (factoryWasFound) {
			if (log.isDebugEnabled()) {
				log.debug("bean resolved for uri - " + uriString);
			}
			return factory.getBean(beanId);
		} else {
			String msg = "bean could not be resolved for uri " + uriString;
			log.error(msg);
			throw new PlatformRuntimeException("platform.arg.bean.notfound", "platform.desc");
		}
	}

	private Parameter getFirstParameter(String uriValue) {

		try {
			String uriNoPrefix = getUriNoPrefix(uriValue);
			URI uri = new URI(uriNoPrefix);
			String query = uri.getQueryString();
			if (StringUtils.isEmpty(query)) {
				return null;
			}
			Parameter param = null;
			QueryParser parser = new QueryParser(query);
			if (parser.hasMore()) {
				return parser.next();
			}
			return param;
		} catch (IOException e) {
			// TODO: handle properly
			throw new IllegalArgumentException(e.toString());
		}
	}

	private String getUriNoPrefix(String uriValue) {
		return uriValue.startsWith(ISA_PREFIX) ? uriValue.substring(ISA_PREFIX.length()) : uriValue;
	}

	private List getFactories(String uri) {
		boolean debugOn = log.isDebugEnabled();
		List resolvedFactories = new ArrayList();
		String uriNoQuery = getUriWithoutQuery(uri);
		Iterator it = _allFactoryUris.iterator();
		while (it.hasNext()) {
			String factoryUri = (String) it.next();
			if (factoryUri.startsWith(uriNoQuery)) {
				Object factory = _uriToFactoryMap.get(factoryUri);
				resolvedFactories.add(factory);
				if (debugOn) {
					log.debug("found factory " + factoryUri + ", for uri " + uri);
				}
			}
		}
		return resolvedFactories;
	}

	private String getUriWithoutQuery(String uri) {
		String uriNoQuery = uri;
		int indx = uri.lastIndexOf('?');
		if (indx > -1) {
			uriNoQuery = uri.substring(0, indx);
		}
		return uriNoQuery;
	}

	private static class ResolvedEntry {

		private BeanFactory factory;

		private String beanId;

		ResolvedEntry() {
		}

		ResolvedEntry(BeanFactory bf, String beanIdentifier) {
			factory = bf;
			beanId = beanIdentifier;
		}

		Object getBean() {
			return factory.getBean(beanId);
		}

		void setBeanFactory(BeanFactory bf) {
			factory = bf;
		}

		BeanFactory getBeanFactory() {
			return factory;
		}

		void setBeanId(String beanIdentifier) {
			beanId = beanIdentifier;
		}

		String getBeanId() {
			return beanId;
		}
	}

	public File getResourceAsFile(String uri) {
		Parameter param = getFirstParameter(uri);
		if (!param.getName().equals("file")) {
			String errMsg = "\"file\" parameter is missing in URI: " + uri;
			log.error(errMsg);
			throw new PlatformRuntimeException("platform.arg.file.uri.invalid", "platform.desc");
		}
		File resourceDir = new File(uriToFilePath(uri));
		return new File(resourceDir, param.getValue());
	}

	private String uriToFilePath(String uriValue) {
		try {
			String uriNoPrefix = getUriNoPrefix(uriValue);
			String uriNoQuery = getUriWithoutQuery(uriNoPrefix);
			URI uri = new URI(uriNoQuery);
			return _configRootDirFile.getAbsolutePath() + File.separator + uri.getHost() + File.separator + uri.getPath();
		} catch (URI.MalformedURIException e) {
			throw new PlatformRuntimeException(e, "platform.arg.bean.uri.invalid", "platform.desc");
		}
	}

	public InputStream getResourceAsInputStream(String uri) throws IOException {
		File file = getResourceAsFile(uri);
		return new BufferedInputStream(new FileInputStream(file));
	}

	public void dispose() {
		Iterator it = _allFactories.iterator();
		while (it.hasNext()) {
			DefaultListableBeanFactory factory = (DefaultListableBeanFactory) it.next();
			factory.destroySingletons();
		}
		_allFactories.clear();
		_allFactoryUris.clear();
		_resolvedBeanMap.clear();
		_uriToFactoryMap.clear();
	}
}
