/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * HierachicalBeanFactoryBuilder.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.bean;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.FileSystemResource;

import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

/**
 * @author Nasly
 * 
 */
public class HierachicalBeanFactoryBuilder {

	private final Log log = LogFactory.getLog(getClass());
	private final Map<BeanFactory, File> allFactories = new HashMap<BeanFactory, File>();
	private static final FrameworkFileFilter frameworkFileFilter = new FrameworkFileFilter();
	private static final DirectoryFilter directoryFilter = new DirectoryFilter();
	private File absRepositoryDir = null;
	private final HashMap<String, Integer> moduleLoadRanks = new HashMap<String, Integer>();

	public Map<BeanFactory, File> build(File dir) {
		if (log.isDebugEnabled()) {
			log.debug("searching for bean definition files in " + dir.getAbsolutePath());
		}
		File absDir = dir.getAbsoluteFile();
		if (!absDir.isDirectory()) {
			String errMsg = "Repository root should be a directory [ abs path = " + dir.getAbsolutePath() + "]";
			log.error(errMsg);
			throw new PlatformRuntimeException("platform.arg.repository.path.invalid", "platform.desc");
		}
		this.absRepositoryDir = absDir;

		// ---------------- FIXME: TO BE MADE CONFIGURABLE
		moduleLoadRanks.put("commons", new Integer(1));
		// -----------------------------------------------

		buildFactoryHierarchy(absDir, null);
		return allFactories;
	}

	protected void buildFactoryHierarchy(File dir, BeanFactory parent) {
		File[] beanFiles = getBeanFiles(dir);
		BeanFactory myFactory = parent;
		if (beanFiles != null && beanFiles.length > 0) {
			try {
				myFactory = createBeanFactory(beanFiles, parent);
			} catch (Exception e) {
				log.error("Invalid bean configurations found in [location =" + dir.getAbsolutePath() + "]", e);
				throw new PlatformRuntimeException(e, "platform.config.bean.definition.invalid", "platform.desc");
			}
			allFactories.put(myFactory, dir);
		}
		File[] childDirs = getSubDirectories(dir);
		for (File childDir : childDirs) {
			buildFactoryHierarchy(childDir, myFactory);
		}
	}

	protected BeanFactory createBeanFactory(File[] beanFiles, BeanFactory parent) {
		BeanFactory factory = null;
		XmlBeanDefinitionReader reader = null;
		for (File bfile : beanFiles) {
			FileSystemResource res = new FileSystemResource(bfile);
			if (factory == null) {
				factory = new DefaultListableBeanFactory(parent);
				if (log.isInfoEnabled()) {
					log.info("loaded bean definition file [file=" + bfile.getAbsolutePath() + "]");
				}
			}
			if (reader == null) {
				reader = new XmlBeanDefinitionReader((BeanDefinitionRegistry)factory );
			}
			reader.loadBeanDefinitions(res);
			if (log.isInfoEnabled()) {
				log.info("loaded bean definition file [file=" + bfile.getAbsolutePath() + "]");
			}
		}
		return factory;
	}

	private static class FrameworkFileFilter implements FilenameFilter {

		private static final String FRAMEWORK_FILE_EXT = ".mod.xml";

		public FrameworkFileFilter() {
		}

		@Override
		public boolean accept(File dir, String name) {
			if (name != null && name.endsWith(FRAMEWORK_FILE_EXT)) {
				return true;
			}
			return false;
		}
	}

	private static class DirectoryFilter implements FileFilter {

		public DirectoryFilter() {
		}

		@Override
		public boolean accept(File file) {
			return file.isDirectory();
		}
	}

	protected File[] getBeanFiles(File dir) {
		File[] result = dir.listFiles(frameworkFileFilter);
		return result != null ? result : new File[] {};
	}

	protected File[] getSubDirectories(File dir) {
		File[] result = dir.listFiles(directoryFilter);
		ArrayList<File> resultsList = null;
		if (dir.equals(new File(absRepositoryDir.getAbsolutePath() + File.separator + "modules"))) {
			// ordering is enforced only for the top-level module folders
			if (result != null) {
				resultsList = new ArrayList<File>();
				for (File element : result) {
					if (getLoadRank(element.getName()) == -1) {
						resultsList.add(element);// add to the end
					} else {
						resultsList.add(getIndex(resultsList, element.getName()), element);
					}
				}
			}
		}
		if (resultsList != null) {
			result = new File[resultsList.size()];
			resultsList.toArray(result);
		}
		return result != null ? result : new File[] {};
	}

	private int getIndex(ArrayList<File> resultsList, String moduleName) {
		int index = 0;
		int curPosLoadRank = -1;
		for (File result : resultsList) {
			if (result.isDirectory()) {
				curPosLoadRank = getLoadRank(result.getName());
				if (curPosLoadRank == -1 || (curPosLoadRank >= getLoadRank(moduleName))) {
					break;
				}
			}
			++index;
		}
		return index;
	}

	private int getLoadRank(String moduleName) {
		if (moduleLoadRanks.get(moduleName) != null) {
			return moduleLoadRanks.get(moduleName).intValue();
		}
		return -1;
	}
}
