package com.isa.thinair.platform.core.controller;

import java.util.HashMap;

import com.isa.thinair.platform.api.IServiceDelegate;

public class BDCahche {

	private static HashMap bdCache = new HashMap();

	public static IServiceDelegate getServiceDelegate(String fullyQualifiedClassName) {
		synchronized (bdCache) {
			IServiceDelegate bd = (IServiceDelegate) bdCache.get(fullyQualifiedClassName);
			return bd;
		}
	}

	public static void addServiceDelegate(String fullyQualifiedClassName, IServiceDelegate serviceDelegate) {
		synchronized (bdCache) {
			bdCache.put(fullyQualifiedClassName, serviceDelegate);
		}
	}

	public static boolean containsServiceDelegate(String fullyQualifiedClassName) {
		synchronized (bdCache) {
			return bdCache.containsKey(fullyQualifiedClassName);
		}
	}
}
