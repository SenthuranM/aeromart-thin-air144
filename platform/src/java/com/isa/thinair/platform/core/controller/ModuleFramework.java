/*
 * ============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 Information System Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * ModuleFramework.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.controller;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.isa.thinair.platform.api.DefaultModule;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IModuleDefinition;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.lifecycle.ILifeCycle;
import com.isa.thinair.platform.core.bean.IUnifiedBeanFactory;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

/**
 * @author Nasly 1. Controls the bean initialization, module lifecycle events 2. Facilitates module and bean lookups
 */
public class ModuleFramework {

	private final Log log = LogFactory.getLog(getClass());

	private static ModuleFramework instance;

	private static boolean isInitialized = false;

	private static Object initializeLock = new Object();

	private static String relativeRootPath = null;

	private IUnifiedBeanFactory frameworkFactory;

	private ModuleDefinitionCollection moduleDefinitions;

	private ModuleFramework() {
	}

	public static ModuleFramework getInstance() {
		startup();
		return instance;
	}

	public IModule getModule(String moduleName) {
		if (moduleName == null || moduleName.trim().length() == 0) {
			log.error("Module Name parameter passed is invalid");
			throw new PlatformRuntimeException("platform.arg.module.id.empty", "platform.desc");
		}
		IModuleDefinition def = moduleDefinitions.get(moduleName);
		if (def != null) {
			return def.getProxy();
		} else {
			log.error("Requested module not found [ moduleName = " + moduleName + "]");
			throw new PlatformRuntimeException("platform.arg.module.notfound", "platform.desc");
		}
	}

	public Object getBean(String uri) {
		if (uri == null || uri.trim().length() == 0) {
			log.error("Bean uri is invalid [uri =" + uri + "]");
			throw new PlatformRuntimeException("platform.arg.bean.uri.empty", "platform.desc");
		}
		return frameworkFactory.getBean(uri);
	}

	public Object getBean(String moduleName, String beanName) {
		if (moduleName == null || moduleName.trim().length() == 0 || beanName == null || beanName.trim().length() == 0) {
			log.error("Invalid bean request [moduleName =" + moduleName + ",beanName=" + beanName + "]");
			throw new PlatformRuntimeException("platform.arg.bean.uri.empty", "platform.desc");
		}

		String beanUri = PlatformConstants.getBeanURIPrefix() + "modules/" + moduleName + "?id=" + beanName;

		return getBean(beanUri);
	}

	public File getFile(String uri) {
		if (uri == null || uri.trim().length() == 0) {
			log.error("Resource uri is invalid [uri =" + uri + "]");
			throw new PlatformRuntimeException("platform.arg.resource.uri.empty", "platform.desc");
		}
		return frameworkFactory.getResourceAsFile(uri);
	}

	public static void startup() {
		if (!isInitialized) {
			synchronized (initializeLock) {
				if (!isInitialized) {
					instance = new ModuleFramework();
					instance.initialize();
				}
			}
		}
	}

	public static void startup(String relativeConfigurationRoot) {
		if (!isInitialized) {
			synchronized (initializeLock) {
				if (!isInitialized) {
					relativeRootPath = relativeConfigurationRoot;
					instance = new ModuleFramework();
					instance.initialize();
				}
			}
		}
	}

	private void initialize() {
		try {
			initBeanFactory();
			initModules();
			isInitialized = true;
		} catch (Exception e) {
			if (!(e instanceof PlatformRuntimeException))
				throw new PlatformRuntimeException(e, "platform.config.init.failed", "platform.desc");
		}
	}

	private void initBeanFactory() {

		try {
			String absConfigRootPath = System.getProperty("repository.home", PlatformConstants.getConfigRootAbsPath())
					+ "/repository";
			if (relativeRootPath != null) {
				Resource res = new ClassPathResource(relativeRootPath);
				absConfigRootPath = res.getFile().getAbsolutePath();
			}
			if (log.isInfoEnabled()) {
				log.info("Going to Initialize bean configurations ..." + "\nAbsolute configuration repository location ="
						+ absConfigRootPath);
			}
			frameworkFactory = new UnifiedBeanFactory(absConfigRootPath);
		} catch (IOException ioEx) {
			log.error("Invalid configuration repository path [path =" + relativeRootPath + "]");
			throw new PlatformRuntimeException(ioEx, "platform.arg.repository.path.invalid", "platform.desc");
		}
	}

	private void initModules() {
		initializeModuleDefinitions();
		initializeModules();
		startModules();
	}

	private void initializeModuleDefinitions() {
		moduleDefinitions = new ModuleDefinitionCollection();
		// find the module definition files
		List moduleDefs = getModuleDefinitions();
		// for each module definition
		Iterator it = moduleDefs.iterator();
		while (it.hasNext()) {
			IModuleDefinition moduleDef;
			moduleDef = (IModuleDefinition) it.next();
			((DefaultModule) moduleDef.getImplementation()).setModuleName(moduleDef.getModuleName());
			// add the ModuleContext entry to the ModuleContextMap
			moduleDefinitions.put(moduleDef.getModuleName(), moduleDef);
		}
		log.info("Completed Initializing module definitions...");
	}

	private List getModuleDefinitions() {
		Map defMap = null;
		try {
			defMap = frameworkFactory.getBeansOfType(IModuleDefinition.class);
		} catch (BeansException ex) {
			log.error("Found invalid module defintion configuration", ex);
			throw new PlatformRuntimeException(ex, "platform.config.bean.definition.invalid", "platform.desc");
		}
		Iterator it = defMap.values().iterator();
		List modDefs = new LinkedList();
		while (it.hasNext()) {
			modDefs.add(it.next());
		}
		return modDefs;
	}

	private void initializeModules() {
		Iterator it = moduleDefinitions.iterator();
		while (it.hasNext()) {
			IModuleDefinition moduleDef;
			moduleDef = (IModuleDefinition) it.next();
			ILifeCycle lc = (ILifeCycle) moduleDef.getProxy();
			try {
				lc.initialize();
			} catch (Exception e) {
				String errMsg = "Initialization of module definition failed [module name= " + moduleDef.getModuleName() + "]";
				log.error(errMsg, e);
				throw new PlatformRuntimeException(e, "platform.config.mod.def.init.failed", "platform.desc");
			}
		}
	}

	private void startModules() {
		Iterator it = moduleDefinitions.iterator();
		while (it.hasNext()) {
			IModuleDefinition moduleDef;
			moduleDef = (IModuleDefinition) it.next();
			ILifeCycle lc = (ILifeCycle) moduleDef.getProxy();
			try {
				lc.start();
			} catch (Exception e) {
				String errMsg = "Starting " + moduleDef.getModuleName() + " failed.";
				log.error(errMsg, e);
				throw new PlatformRuntimeException(e, "platform.module.impl.startup.failed", "platform.desc");
			}
		}
	}

	protected class ModuleDefinitionCollection {

		private Map _defMap = new HashMap();

		private List _moduleNames = new LinkedList();

		ModuleDefinitionCollection() {
		}

		void put(String name, IModuleDefinition moduleDef) {
			if (!_defMap.containsKey(name)) {
				_moduleNames.add(name);
			}
			_defMap.put(name, moduleDef);
		}

		IModuleDefinition get(String moduleName) {
			return (IModuleDefinition) _defMap.get(moduleName);
		}

		List getModuleNames() {
			return Collections.unmodifiableList(_moduleNames);
		}

		Iterator iterator() {
			return _defMap.values().iterator();
		}
	}

}
