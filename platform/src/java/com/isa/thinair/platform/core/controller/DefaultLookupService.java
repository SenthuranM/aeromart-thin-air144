/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * DefaultLookupService.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.controller;

import java.io.File;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.api.DefaultModuleConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

/**
 * @author Nasly
 */
public class DefaultLookupService implements LookupService {

	private final Log log = LogFactory.getLog(getClass());

	// This will cache the Jndi objects.
	private Hashtable<String, Object> serviceLookups = new Hashtable<String, Object>();

	public DefaultLookupService() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.platform.api.LookupService#getModule(java.lang.String)
	 */
	public IModule getModule(String moduleName) {
		try {
			ModuleFramework framework = ModuleFramework.getInstance();
			return framework.getModule(moduleName);
		} catch (Exception e) {
			if (e instanceof PlatformRuntimeException) {
				throw (PlatformRuntimeException) e;
			} else {
				String errMsg = "Could not lookup module [module name = " + moduleName + "]";
				log.error(errMsg);
				throw new PlatformRuntimeException(e, "platform.module.lookup.failed", "platform.desc");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.platform.api.LookupService#getBean(java.lang.String)
	 */
	public Object getBean(String uri) {
		ModuleFramework framework = ModuleFramework.getInstance();
		return framework.getBean(uri);
	}

	public File getFile(String uri) {
		ModuleFramework framework = ModuleFramework.getInstance();
		return framework.getFile(uri);
	}

	public IServiceDelegate getServiceBD(String targetModuleName, String fullBDKey) {
		ModuleFramework framework = ModuleFramework.getInstance();
		return framework.getModule(targetModuleName).getServiceBD(fullBDKey);
	}

	public Object getService(Properties jndiProperties, String serviceName, boolean isNoCaching) {
		try {
			Object object;
			
			if (isNoCaching) {
				InitialContext initialContext = new InitialContext(jndiProperties);
				object = initialContext.lookup(serviceName);
			} else {
				object = serviceLookups.get(serviceName);
				if (object == null) {
					InitialContext initialContext = new InitialContext(jndiProperties);
					object = initialContext.lookup(serviceName);
					serviceLookups.put(serviceName, object);
				}
			}

			return object;
		} catch (NamingException e) {
			throw new PlatformRuntimeException(e, "platform.module.bd.impl.invalid", "platform.desc");
		}
	}

	public DefaultModuleConfig getModuleConfig(String moduleName) {
		return (DefaultModuleConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/" + moduleName + "?id=" + moduleName + "ModuleConfig");
	}

}
