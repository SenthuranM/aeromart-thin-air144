/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * InvalidURIException.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.bean;

/**
 * @author Nasly
 * 
 */
public class InvalidURIException extends RuntimeException {
	public InvalidURIException() {
		super();
	}

	public InvalidURIException(String str) {
		super(str);
	}
}
