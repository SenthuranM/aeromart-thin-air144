/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 6, 2005
 * 
 * QueryParser.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.bean;

/**
 * @author Nasly
 * 
 */
public class QueryParser {

	private String _query;
	private int _currIndx = 0;
	private int _mark = -1;

	public QueryParser(String query) {
		_query = query;
	}

	public boolean hasMore() {
		return _currIndx < _query.length();
	}

	public Parameter next() {
		String key = null;
		String value = null;
		Parameter param = null;
		for (; _currIndx < _query.length() && param == null; _currIndx++) {
			char c = _query.charAt(_currIndx);
			if (c == '&') {
				value = _query.substring(_mark + 1, _currIndx);
				_mark = _currIndx;
				if (key != null) {
					param = new Parameter(key, value);
					key = null;
				}
			} else if (c == '=') {
				if (key == null) {
					key = _query.substring(_mark + 1, _currIndx);
					_mark = _currIndx;
				}
			} else if (c == '%') {
				// TODO: handle encoding
			}
		}

		if (param == null) {
			if (key != null) {
				value = _query.substring(_mark + 1);
				param = new Parameter(key, value);
			} else if (_mark < _query.length()) {
				key = _query.substring(_mark + 1);
				param = new Parameter(key, "");
			}
		}
		return param;
	}

}