/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * IBeanRegistry.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.core.bean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author Nasly
 * 
 */
public interface IUnifiedBeanFactory {
	Object getBean(String uri);

	Map getBeansOfType(java.lang.Class type);

	File getResourceAsFile(String uri);

	InputStream getResourceAsInputStream(String uri) throws IOException;

	void dispose();
}
