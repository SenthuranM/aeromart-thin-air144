/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestModuleFramework.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.controller;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.platform.example.ExampleModuleConfig;


import junit.framework.TestCase;

/**
 * @author Nasly
 *
 */
public class TestModuleFramework extends TestCase {

	public TestModuleFramework() {
		super();
	}
	
	protected void setUp(){
		ModuleFramework.startup("moduleconfigs");
	}
	
	public void testStartUp(){
		LookupService lookupService = LookupServiceFactory.getInstance();
		assertNotNull(lookupService.getModule("exampleModule"));
		ExampleModuleConfig config = (ExampleModuleConfig)lookupService.getBean("isa:base://modules/test?id=exampleModuleConfig");
		assertNotNull(config);
		assertNotNull(config.getMessage());
	}
	
	protected void tearDown(){
	}
}
