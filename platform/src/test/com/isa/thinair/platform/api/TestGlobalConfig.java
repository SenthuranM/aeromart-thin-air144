package com.isa.thinair.platform.api;

import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.core.controller.ModuleFramework;

import junit.framework.TestCase;

public class TestGlobalConfig extends TestCase{
	public TestGlobalConfig() {
		super();
	}
	
	protected void setUp(){
		ModuleFramework.startup("moduleconfigs");
	}
	
	public void testExceptionMessage(){
		LookupService lookup = LookupServiceFactory.getInstance();
		GlobalConfig config = (GlobalConfig)lookup.getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
		assertNotNull(config);
        //FIXME
		//assertNotNull(config.getMessage("module.exception.code.empty"));
	}
}
