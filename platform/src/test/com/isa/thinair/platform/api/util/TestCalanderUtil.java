/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.platform.api.util;

import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.core.util.CalanderUtil;

import junit.framework.TestCase;

/**
 * test case to test CalanderUtil
 * @author Lasantha Pambagoda
 */
public class TestCalanderUtil extends TestCase {

	
	public void testGetDayNumber(){
		
		assertEquals("Sart day should be saturday",0,CalanderUtil.getDayNumber(DayOfWeek.SATURDAY));
	}
	
}
