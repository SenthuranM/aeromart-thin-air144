/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * ExampleModuleConfig.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.example;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nasly
 *
 */
public class ExampleModuleConfig extends DefaultModuleConfig {

	private String message;
	
	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	public ExampleModuleConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

}
