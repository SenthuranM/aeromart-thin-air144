/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * ExampleModuleServcie.java
 * 
 * ============================================================================
 */
package com.isa.thinair.platform.example;

import com.isa.thinair.platform.api.DefaultModule;


/**
 * @author Nasly
 *
 */
public class ExampleModuleServcie  extends DefaultModule{

}
