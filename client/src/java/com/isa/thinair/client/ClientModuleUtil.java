package com.isa.thinair.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class ClientModuleUtil {
	private static Log log = LogFactory.getLog(ClientModuleUtil.class);

	/**
	 * Lookup an exernal module
	 * 
	 * @param moduleName
	 * @return IModule Module Implemenation
	 */
	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	public static ClientConfig getClientConfig() {
		return (ClientConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/client?id=clientModuleConfig");
	}

	/**
	 * Lookup an external module's BD
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return IServiceDelegate BD implementation
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getClientConfig(), "webservices",
				"webservices.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getClientConfig(), "webservices",
				"webservices.config.dependencymap.invalid");
	}

	/**
	 * Retrieves application's global configurations
	 * 
	 * @return GlobalConfig
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}
}
