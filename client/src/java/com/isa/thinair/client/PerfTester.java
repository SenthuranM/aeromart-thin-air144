package com.isa.thinair.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.login.client.ClientLoginModule;

public class PerfTester implements Runnable {

	private static Log log = LogFactory.getLog(ClientModuleUtil.class);

	String[] pnrs = null;
	int iterations = 0;

	public PerfTester(String[] pnrs, int iterations) {
		this.pnrs = pnrs;
		this.iterations = iterations;
	}

	public void run() {
		long start;
		long end;
		for (int count = 0; count < pnrs.length; ++count) {
			for (int i = 1; i <= iterations; ++i) {
				try {
					start = System.currentTimeMillis();
					log.info("RETRIVERES START," + Thread.currentThread().getName() + "," + start + "," + pnrs[count] + "," + i);
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(pnrs[count]);
					pnrModesDTO.setLoadFares(false);
					pnrModesDTO.setLoadSegView(true);
					pnrModesDTO.setLoadPaxAvaBalance(true);
					pnrModesDTO.setRecordAudit(true);

					new ClientLoginModule().login("SYSTEM", "password", null, null);

					Reservation reservation = ClientModuleUtil.getReservationBD().getReservation(pnrModesDTO, null);
					log.debug("RETRIVERES," + Thread.currentThread().getName() + ",pnr=" + reservation.getPnr() + ",balance="
							+ reservation.getTotalAvailableBalance() + ",status=" + reservation.getStatus());

					end = System.currentTimeMillis();
					log.info("RETRIVERES END," + Thread.currentThread().getName() + "," + end + "," + pnrs[count] + "," + i + ","
							+ (end - start));
				} catch (ModuleException me) {
					log.error("", me);
				}
			}
		}
	}
}
