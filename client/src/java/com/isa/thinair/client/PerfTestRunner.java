package com.isa.thinair.client;

import com.isa.thinair.platform.core.controller.ModuleFramework;

public class PerfTestRunner {

	public static void main(String[] args) {
		System.setProperty("repository.home", "c:/isaconfig-01");
		ModuleFramework.startup();

		String[] pnrs = { "10586526" };
		int iterations = 5;

		for (int count = 0; count < pnrs.length; ++count) {
			new Thread(new PerfTester(pnrs, iterations)).start();
		}
	}
}
