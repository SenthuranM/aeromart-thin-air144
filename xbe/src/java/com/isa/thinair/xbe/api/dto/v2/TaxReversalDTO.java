package com.isa.thinair.xbe.api.dto.v2;

import java.util.List;


public class TaxReversalDTO {
	
	private String pnr;
	
	private boolean groupPNR;
	
	private boolean hasRefundablePax; 
	
	private List<NoShowPassenger> noShowPaxDetails;
	
	private String currencyCode;
	
	public String getPnr() {
		return pnr;
	}
	

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	

	public boolean isGroupPNR() {
		return groupPNR;
	}
	

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}	


	public String getCurrencyCode() {
		return currencyCode;
	}
	


	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}


	public boolean isHasRefundablePax() {
		return hasRefundablePax;
	}


	public void setHasRefundablePax(boolean hasRefundablePax) {
		this.hasRefundablePax = hasRefundablePax;
	}


	public List<NoShowPassenger> getNoShowPaxDetails() {
		return noShowPaxDetails;
	}


	public void setNoShowPaxDetails(List<NoShowPassenger> noShowPaxDetails) {
		this.noShowPaxDetails = noShowPaxDetails;
	}
	
}
