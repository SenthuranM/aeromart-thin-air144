package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

public class PaxAccountChargesTO {
	private String totalCharges;

	private String balance;

	private String currency;

	private String agentCurrency;

	private Collection<ChargesListTO> chargesList;
	
	private String actualCredit;

	/**
	 * @return the totalCharges
	 */
	public String getTotalCharges() {
		return totalCharges;
	}

	/**
	 * @param totalCharges
	 *            the totalCharges to set
	 */
	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the chargesList
	 */
	public Collection<ChargesListTO> getChargesList() {
		return chargesList;
	}

	/**
	 * @param chargesList
	 *            the chargesList to set
	 */
	public void setChargesList(Collection<ChargesListTO> chargesList) {
		this.chargesList = chargesList;
	}

	public String getAgentCurrency() {
		return agentCurrency;
	}

	public void setAgentCurrency(String agentCurrency) {
		this.agentCurrency = agentCurrency;
	}

	public String getActualCredit() {
		return actualCredit;
	}

	public void setActualCredit(String actualCredit) {
		this.actualCredit = actualCredit;
	}

}
