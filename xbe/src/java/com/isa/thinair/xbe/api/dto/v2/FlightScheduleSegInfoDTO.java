package com.isa.thinair.xbe.api.dto.v2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author eric
 * 
 */
public class FlightScheduleSegInfoDTO {

	private String segment;

	private Collection<FlightScheduleDTO> flightSchedules;

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public Collection<FlightScheduleDTO> getFlightSchedules() {
		if (flightSchedules == null) {
			flightSchedules = new ArrayList<FlightScheduleDTO>();
		}
		return flightSchedules;
	}
}
