package com.isa.thinair.xbe.api.dto;

import java.io.Serializable;
import java.util.Set;

import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class UserAgentInfoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String agentCode;

	private String agentType;

	private String agentLanguage;

	private String agentsCurrType;

	private String agentsCurrCode;

	private String airlineCode;

	/** User has Add GSA privilage or Add any privilege and is not a TA */
	private boolean isPrivilegedUser;

	private Set<String> userPaymentInfo;
	
	private boolean sharedCreditBasis;

	/**
	 * @return Returns the agentLanguage.
	 */
	public String getAgentLanguage() {
		return agentLanguage;
	}

	/**
	 * @param agentLanguage
	 *            The agentLanguage to set.
	 */
	public void setAgentLanguage(String agentLanguage) {
		this.agentLanguage = agentLanguage;
	}

	/**
	 * @return Returns the agentType.
	 */
	public String getAgentType() {
		return agentType;
	}

	/**
	 * @param agentType
	 *            The agentType to set.
	 */
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the isGSA.
	 */
	public boolean isGSA() {
		return this.agentType.equals(WebConstants.AGENT_TYPE_GSA);
	}

	/**
	 * @return Returns the isPrivilegedUser.
	 */
	public boolean isPrivilegedUser() {
		return isPrivilegedUser;
	}

	/**
	 * @param isPrivilegedUser
	 *            The isPrivilegedUser to set.
	 */
	public void setPrivilegedUser(boolean isPrivilegedUser) {
		this.isPrivilegedUser = isPrivilegedUser;
	}

	/**
	 * @return Returns the agentsCurrType.
	 */
	public String getAgentsCurrType() {
		return agentsCurrType;
	}

	/**
	 * @param agentsCurrType
	 *            The agentsCurrType to set.
	 */
	public void setAgentsCurrType(String agentsCurrType) {
		this.agentsCurrType = agentsCurrType;
	}

	/**
	 * @return Returns the agentsCurrCode.
	 */
	public String getAgentsCurrCode() {
		return agentsCurrCode;
	}

	/**
	 * @param agentsCurrCode
	 *            The agentsCurrCode to set.
	 */
	public void setAgentsCurrCode(String agentsCurrCode) {
		this.agentsCurrCode = agentsCurrCode;
	}

	/**
	 * @return Returns the userPaymentInfo.
	 */
	public Set<String> getUserPaymentInfo() {
		return userPaymentInfo;
	}

	/**
	 * @param userPaymentInfo
	 *            The userPaymentInfo to set.
	 */
	public void setUserPaymentInfo(Set<String> userPaymentInfo) {
		this.userPaymentInfo = userPaymentInfo;
	}

	/**
	 * @return the airlineCode
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */
	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public boolean isSharedCreditBasis() {
		return sharedCreditBasis;
	}

	public void setSharedCreditBasis(String hasCreditLimit) {
		boolean sharedCreditBasis = false;
		if (!"Y".equalsIgnoreCase(hasCreditLimit)) {
			sharedCreditBasis = true;
		}
		this.sharedCreditBasis = sharedCreditBasis;
	}	

}
