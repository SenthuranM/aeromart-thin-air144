package com.isa.thinair.xbe.api.dto.v2;

public class ExternalPaymentsTO {

	private String displayID;

	private String displayChannel;

	private String displayTimeStamp;

	private String displayStatus;

	private String displayAmount;

	/**
	 * @return the displayID
	 */
	public String getDisplayID() {
		return displayID;
	}

	/**
	 * @param displayID
	 *            the displayID to set
	 */
	public void setDisplayID(String displayID) {
		this.displayID = displayID;
	}

	/**
	 * @return the displayChannel
	 */
	public String getDisplayChannel() {
		return displayChannel;
	}

	/**
	 * @param displayChannel
	 *            the displayChannel to set
	 */
	public void setDisplayChannel(String displayChannel) {
		this.displayChannel = displayChannel;
	}

	/**
	 * @return the displayTimeStamp
	 */
	public String getDisplayTimeStamp() {
		return displayTimeStamp;
	}

	/**
	 * @param displayTimeStamp
	 *            the displayTimeStamp to set
	 */
	public void setDisplayTimeStamp(String displayTimeStamp) {
		this.displayTimeStamp = displayTimeStamp;
	}

	/**
	 * @return the displayStatus
	 */
	public String getDisplayStatus() {
		return displayStatus;
	}

	/**
	 * @param displayStatus
	 *            the displayStatus to set
	 */
	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	/**
	 * @return the displayAmount
	 */
	public String getDisplayAmount() {
		return displayAmount;
	}

	/**
	 * @param displayAmount
	 *            the displayAmount to set
	 */
	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}
}
