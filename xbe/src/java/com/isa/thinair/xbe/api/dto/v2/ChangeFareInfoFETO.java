package com.isa.thinair.xbe.api.dto.v2;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;

public class ChangeFareInfoFETO {
	private FareRuleDTO fareRule;
	private InvAllocationDTO seatInfo;
	private FlightInfoFETO flightInfo;
	private int fareType;

	public FareRuleDTO getFareRule() {
		return fareRule;
	}

	public void setFareRule(FareRuleDTO fareRule) {
		this.fareRule = fareRule;
	}

	public InvAllocationDTO getSeatInfo() {
		return seatInfo;
	}

	public void setSeatInfo(InvAllocationDTO seatInfo) {
		this.seatInfo = seatInfo;
	}

	public FlightInfoFETO getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(FlightInfoFETO flightInfo) {
		this.flightInfo = flightInfo;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

}
