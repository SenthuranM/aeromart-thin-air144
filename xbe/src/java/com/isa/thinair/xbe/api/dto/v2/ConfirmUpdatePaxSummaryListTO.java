package com.isa.thinair.xbe.api.dto.v2;

public class ConfirmUpdatePaxSummaryListTO {
	private String displayPaxName;

	private String displayTotalChargesCurrent;

	private String displayTotalChargesNew;

	private String displayTotalPaymentsCurrent;

	private String displayTotalCFNew;

	private String displayBalance;

	/**
	 * @return the displayPaxName
	 */
	public String getDisplayPaxName() {
		return displayPaxName;
	}

	/**
	 * @param displayPaxName
	 *            the displayPaxName to set
	 */
	public void setDisplayPaxName(String displayPaxName) {
		this.displayPaxName = displayPaxName;
	}

	/**
	 * @return the displayTotalChargesCurrent
	 */
	public String getDisplayTotalChargesCurrent() {
		return displayTotalChargesCurrent;
	}

	/**
	 * @param displayTotalChargesCurrent
	 *            the displayTotalChargesCurrent to set
	 */
	public void setDisplayTotalChargesCurrent(String displayTotalChargesCurrent) {
		this.displayTotalChargesCurrent = displayTotalChargesCurrent;
	}

	/**
	 * @return the displayTotalChargesNew
	 */
	public String getDisplayTotalChargesNew() {
		return displayTotalChargesNew;
	}

	/**
	 * @param displayTotalChargesNew
	 *            the displayTotalChargesNew to set
	 */
	public void setDisplayTotalChargesNew(String displayTotalChargesNew) {
		this.displayTotalChargesNew = displayTotalChargesNew;
	}

	/**
	 * @return the displayTotalPaymentsCurrent
	 */
	public String getDisplayTotalPaymentsCurrent() {
		return displayTotalPaymentsCurrent;
	}

	/**
	 * @param displayTotalPaymentsCurrent
	 *            the displayTotalPaymentsCurrent to set
	 */
	public void setDisplayTotalPaymentsCurrent(String displayTotalPaymentsCurrent) {
		this.displayTotalPaymentsCurrent = displayTotalPaymentsCurrent;
	}

	/**
	 * @return the displayTotalCFNew
	 */
	public String getDisplayTotalCFNew() {
		return displayTotalCFNew;
	}

	/**
	 * @param displayTotalCFNew
	 *            the displayTotalCFNew to set
	 */
	public void setDisplayTotalCFNew(String displayTotalCFNew) {
		this.displayTotalCFNew = displayTotalCFNew;
	}

	/**
	 * @return the displayBalance
	 */
	public String getDisplayBalance() {
		return displayBalance;
	}

	/**
	 * @param displayBalance
	 *            the displayBalance to set
	 */
	public void setDisplayBalance(String displayBalance) {
		this.displayBalance = displayBalance;
	}
}
