package com.isa.thinair.xbe.api.dto.v2;

import java.math.BigDecimal;

public class PnrCreditFETO {
	private BigDecimal usedAmount = null;
	private String carrier = null;
	private Integer paxCreditId = null;
	private BigDecimal residingCarrierAmount = null;
	private String pnr = null;

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public Integer getPaxCreditId() {
		return paxCreditId;
	}

	public void setPaxCreditId(Integer paxCreditId) {
		this.paxCreditId = paxCreditId;
	}

	public BigDecimal getResidingCarrierAmount() {
		return residingCarrierAmount;
	}

	public void setResidingCarrierAmount(BigDecimal residingCarrierAmount) {
		this.residingCarrierAmount = residingCarrierAmount;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public BigDecimal getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(BigDecimal usedAmount) {
		this.usedAmount = usedAmount;
	}

}
