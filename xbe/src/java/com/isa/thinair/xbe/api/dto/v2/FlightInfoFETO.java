package com.isa.thinair.xbe.api.dto.v2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FlightInfoFETO {
	private Log logger = LogFactory.getLog(FlightInfoFETO.class);

	private String ondGroup;

	private boolean returnFlag;

	private String fltSegRefNo;

	private Date departureDateTimeZulu;

	private Date arrivalDateTimeZulu;

	private String flightNo;

	private String orignNDest;

	private String ondCode;

	private int ondSequence;

	private int requestedOndSequence = -1;

	public String getOndGroup() {
		return ondGroup;
	}

	public void setOndGroup(String ondGroup) {
		this.ondGroup = ondGroup;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getFltSegRefNo() {
		return fltSegRefNo;
	}

	public void setFltSegRefNo(String fltSegRefNo) {
		this.fltSegRefNo = fltSegRefNo;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * Accept date format of yyMMddHHmm E.g. 1202182015 - 2012 Feb 18 - 20:15
	 * 
	 * @param departureDateTimeZulu
	 */
	private Date parseDate(String date) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyMMddHHmm");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			logger.warn("Error parsing date", e);
		}
		return null;
	}

	/**
	 * Accept date format of yyMMddHHmm E.g. 1202182015 - 2012 Feb 18 - 20:15
	 * 
	 * @param departureDateTimeZulu
	 */
	public void setDepartureDateTimeZ(String departureDateTimeZulu) {
		this.departureDateTimeZulu = parseDate(departureDateTimeZulu);
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	/**
	 * Accept date format of yyMMddHHmm E.g. 1202182015 - 2012 Feb 18 - 20:15
	 * 
	 * @param departureDateTimeZulu
	 */
	public void setArrivalDateTimeZ(String arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = parseDate(arrivalDateTimeZulu);
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		if (flightRefNumber != null && flightRefNumber.indexOf("#") != -1) {
			flightRefNumber = flightRefNumber.split("#")[0];
		}
		this.fltSegRefNo = flightRefNumber;
	}

	/**
	 * @return the orignNDest
	 */
	public String getOrignNDest() {
		return orignNDest;
	}

	/**
	 * @param orignNDest
	 *            the orignNDest to set
	 */
	public void setOrignNDest(String orignNDest) {
		this.orignNDest = orignNDest;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

}
