package com.isa.thinair.xbe.api.dto.v2;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;

public class AncillaryPaxTO implements Comparable<AncillaryPaxTO> {
	private String paxType;
	private Integer paxSequence;
	private String title;
	private String firstName;
	private String lastName;
	private Integer nationalityCode;
	private Date dateOfBirth;
	private List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries;
	private boolean isParent;
	private Integer infantWith;

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public Integer getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		return selectedAncillaries;
	}

	public void setSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}
	
	public boolean isParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public Integer getInfantWith() {
		return infantWith;
	}

	public void setInfantWith(Integer infantWith) {
		this.infantWith = infantWith;
	}

	@Override
	public int compareTo(AncillaryPaxTO o) {
		if (this.paxSequence < o.paxSequence) {
			return -1;
		} else if (this.paxSequence > o.paxSequence) {
			return 1;
		} else {
			return 0;
		}
	}

}
