package com.isa.thinair.xbe.api.dto.v2;

public class ChargesListTO {
	private String displaySegmentID;

	private String displaySegment;

	private String displayDate;

	private String displayDepartureDate;

	private String displayDescription;

	private String displayAmount;

	private String agentCurrencyAmount;

	private String displayCarrierCode;

	/**
	 * @return the displaySegmentID
	 */
	public String getDisplaySegmentID() {
		return displaySegmentID;
	}

	/**
	 * @param displaySegmentID
	 *            the displaySegmentID to set
	 */
	public void setDisplaySegmentID(String displaySegmentID) {
		this.displaySegmentID = displaySegmentID;
	}

	/**
	 * @return the displaySegment
	 */
	public String getDisplaySegment() {
		return displaySegment;
	}

	/**
	 * @param displaySegment
	 *            the displaySegment to set
	 */
	public void setDisplaySegment(String displaySegment) {
		this.displaySegment = displaySegment;
	}

	/**
	 * @return the displayDate
	 */
	public String getDisplayDate() {
		return displayDate;
	}

	/**
	 * @param displayDate
	 *            the displayDate to set
	 */
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}

	/**
	 * @return
	 */
	public String getDisplayDepartureDate() {
		return displayDepartureDate;
	}

	/**
	 * @param displayDepartureDate
	 */
	public void setDisplayDepartureDate(String displayDepartureDate) {
		this.displayDepartureDate = displayDepartureDate;
	}

	/**
	 * @return the displayDescription
	 */
	public String getDisplayDescription() {
		return displayDescription;
	}

	/**
	 * @param displayDescription
	 *            the displayDescription to set
	 */
	public void setDisplayDescription(String displayDescription) {
		this.displayDescription = displayDescription;
	}

	/**
	 * @return the displayAmount
	 */
	public String getDisplayAmount() {
		return displayAmount;
	}

	/**
	 * @param displayAmount
	 *            the displayAmount to set
	 */
	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}

	public String getDisplayCarrierCode() {
		return displayCarrierCode;
	}

	public void setDisplayCarrierCode(String displayCarrierCode) {
		this.displayCarrierCode = displayCarrierCode;
	}

	public String getAgentCurrencyAmount() {
		if (agentCurrencyAmount == null)
			return "";
		return agentCurrencyAmount;
	}

	public void setAgentCurrencyAmount(String agentCurrencyAmount) {
		this.agentCurrencyAmount = agentCurrencyAmount;
	}

}
