package com.isa.thinair.xbe.api.dto.v2;

public class ContactInfoTO {
	private String title;

	private String firstName;

	private String lastName;

	private String street;

	private String address;

	private String city;

	private String country;

	private String zipCode;

	private String nationality;

	private String mobileCountry;

	private String mobileArea;

	private String mobileNo;

	private String phoneCountry;

	private String phoneArea;

	private String phoneNo;

	private String faxCountry;

	private String faxArea;

	private String faxNo;

	private String email;

	private String userNotes;
	
	private String userNoteType;

	private String preferredLang;

	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneCountry;

	private String emgnPhoneArea;

	private String emgnPhoneNo;

	private String emgnEmail;
	
	private String taxRegNo;
	
	private String state;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry() {
		return mobileCountry;
	}

	/**
	 * @param mobileCountry
	 *            the mobileCountry to set
	 */
	public void setMobileCountry(String mobileCountry) {
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the mobileArea
	 */
	public String getMobileArea() {
		return mobileArea;
	}

	/**
	 * @param mobileArea
	 *            the mobileArea to set
	 */
	public void setMobileArea(String mobileArea) {
		this.mobileArea = mobileArea;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the phoneCountry
	 */
	public String getPhoneCountry() {
		return phoneCountry;
	}

	/**
	 * @param phoneCountry
	 *            the phoneCountry to set
	 */
	public void setPhoneCountry(String phoneCountry) {
		this.phoneCountry = phoneCountry;
	}

	/**
	 * @return the phoneArea
	 */
	public String getPhoneArea() {
		return phoneArea;
	}

	/**
	 * @param phoneArea
	 *            the phoneArea to set
	 */
	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}

	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo
	 *            the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the faxCountry
	 */
	public String getFaxCountry() {
		return faxCountry;
	}

	/**
	 * @param faxCountry
	 *            the faxCountry to set
	 */
	public void setFaxCountry(String faxCountry) {
		this.faxCountry = faxCountry;
	}

	/**
	 * @return the faxArea
	 */
	public String getFaxArea() {
		return faxArea;
	}

	/**
	 * @param faxArea
	 *            the faxArea to set
	 */
	public void setFaxArea(String faxArea) {
		this.faxArea = faxArea;
	}

	/**
	 * @return the faxNo
	 */
	public String getFaxNo() {
		return faxNo;
	}

	/**
	 * @param faxNo
	 *            the faxNo to set
	 */
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the preferredLang
	 */
	public String getPreferredLang() {
		return preferredLang;
	}

	/**
	 * @param preferredLang
	 *            the preferredLang to set
	 */
	public void setPreferredLang(String preferredLang) {
		this.preferredLang = preferredLang;
	}

	/**
	 * @return the userNotes
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes
	 *            the userNotes to set
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	/**
	 * @return the emgnTitle
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnPhoneCountry
	 */
	public String getEmgnPhoneCountry() {
		return emgnPhoneCountry;
	}

	/**
	 * @param emgnPhoneCountry
	 *            the emgnPhoneCountry to set
	 */
	public void setEmgnPhoneCountry(String emgnPhoneCountry) {
		this.emgnPhoneCountry = emgnPhoneCountry;
	}

	/**
	 * @return the emgnPhoneArea
	 */
	public String getEmgnPhoneArea() {
		return emgnPhoneArea;
	}

	/**
	 * @param emgnPhoneArea
	 *            the emgnPhoneArea to set
	 */
	public void setEmgnPhoneArea(String emgnPhoneArea) {
		this.emgnPhoneArea = emgnPhoneArea;
	}

	/**
	 * @return the emgnPhoneNo
	 */
	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	/**
	 * @param emgnPhoneNo
	 *            the emgnPhoneNo to set
	 */
	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	/**
	 * @return the emgnEmail
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}
    
	/**
	 * @return the userNotesType
	 */
	public String getUserNoteType() {
		return userNoteType;
	}
    
	/**
	 * @param userNotesType
	 *            the userNotesType to set
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public String getTaxRegNo() {
		return taxRegNo;
	}

	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}