package com.isa.thinair.xbe.api.dto.v2;

import java.util.Map;

import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;

public class ReservationPostPaymentDTO {
	private Map temporyPaymentMap;

	private String ipgRefenceNo;

	private IPGResponseDTO ipgResponseDTO;

	private Integer ipgId;

	private Integer paymentBrokerRefNo;

	private String currencyCode;

	public Map getTemporyPaymentMap() {
		return temporyPaymentMap;
	}

	public void setTemporyPaymentMap(Map temporyPaymentMap) {
		this.temporyPaymentMap = temporyPaymentMap;
	}

	public String getIpgRefenceNo() {
		return ipgRefenceNo;
	}

	public void setIpgRefenceNo(String ipgRefenceNo) {
		this.ipgRefenceNo = ipgRefenceNo;
	}

	public IPGResponseDTO getIpgResponseDTO() {
		return ipgResponseDTO;
	}

	public void setIpgResponseDTO(IPGResponseDTO ipgResponseDTO) {
		this.ipgResponseDTO = ipgResponseDTO;
	}

	public Integer getIpgId() {
		return ipgId;
	}

	public void setIpgId(Integer ipgId) {
		this.ipgId = ipgId;
	}

	public Integer getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setPaymentBrokerRefNo(Integer paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
