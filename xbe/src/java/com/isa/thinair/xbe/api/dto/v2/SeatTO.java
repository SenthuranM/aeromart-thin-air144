package com.isa.thinair.xbe.api.dto.v2;

public class SeatTO {
	private String id;

	private String no;

	private String st;

	private String chg;

	private String chgID;

	private String fltSgID;

	private String type;

	private String paxType;

	private String cls;

	private String uti;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the no
	 */
	public String getNo() {
		return no;
	}

	/**
	 * @param no
	 *            the no to set
	 */
	public void setNo(String no) {
		this.no = no;
	}

	/**
	 * @return the st
	 */
	public String getSt() {
		return st;
	}

	/**
	 * @param st
	 *            the st to set
	 */
	public void setSt(String st) {
		this.st = st;
	}

	/**
	 * @return the chg
	 */
	public String getChg() {
		return chg;
	}

	/**
	 * @param chg
	 *            the chg to set
	 */
	public void setChg(String chg) {
		this.chg = chg;
	}

	/**
	 * @return the chgID
	 */
	public String getChgID() {
		return chgID;
	}

	/**
	 * @param chgID
	 *            the chgID to set
	 */
	public void setChgID(String chgID) {
		this.chgID = chgID;
	}

	/**
	 * @return the fltSgID
	 */
	public String getFltSgID() {
		return fltSgID;
	}

	/**
	 * @param fltSgID
	 *            the fltSgID to set
	 */
	public void setFltSgID(String fltSgID) {
		this.fltSgID = fltSgID;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the cls
	 */
	public String getCls() {
		return cls;
	}

	/**
	 * @param cls
	 *            the cls to set
	 */
	public void setCls(String cls) {
		this.cls = cls;
	}

	/**
	 * @return the uti
	 */
	public String getUti() {
		return uti;
	}

	/**
	 * @param uti
	 *            the uti to set
	 */
	public void setUti(String uti) {
		this.uti = uti;
	}
}
