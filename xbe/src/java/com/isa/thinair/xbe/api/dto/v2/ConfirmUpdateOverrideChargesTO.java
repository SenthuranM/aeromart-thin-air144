package com.isa.thinair.xbe.api.dto.v2;

import java.math.BigDecimal;
import java.text.Collator;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ConfirmUpdateOverrideChargesTO implements Comparable<ConfirmUpdateOverrideChargesTO> {
	private String description;

	private String id;

	private String type;

	private String value;

	private String chargeType;
	private String min;
	private String max;
	private String precentageVal;
	private String paxType;
	private boolean blnShowChargeTypeOveride;

	public ConfirmUpdateOverrideChargesTO() {
		this.chargeType = AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes();
		this.min = AccelAeroCalculator.formatAsDecimal(new BigDecimal(0.0));
		this.max = AccelAeroCalculator.formatAsDecimal(new BigDecimal(0.0));
		this.precentageVal = AccelAeroCalculator.formatAsDecimal(new BigDecimal(0.0));
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public int compareTo(ConfirmUpdateOverrideChargesTO confirmDto) {
		Collator scol = Collator.getInstance();
		return scol.compare(this.getDescription(), confirmDto.getDescription());

	}

	public String getChargeType() {
		if (chargeType == null)
			return "";
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getMin() {
		if (min == null)
			return "";
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		if (max == null)
			return "";
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getPrecentageVal() {
		if (precentageVal == null)
			return "";
		return precentageVal;
	}

	public void setPrecentageVal(String precentageVal) {
		this.precentageVal = precentageVal;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public boolean isBlnShowChargeTypeOveride() {
		return blnShowChargeTypeOveride;
	}

	public void setBlnShowChargeTypeOveride(boolean blnShowChargeTypeOveride) {
		this.blnShowChargeTypeOveride = blnShowChargeTypeOveride;
	}

}