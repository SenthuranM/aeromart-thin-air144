package com.isa.thinair.xbe.api.dto.v2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PaxCreditFETO {
	private Log logger = LogFactory.getLog(PaxCreditFETO.class);
	private BigDecimal displayToPay = null;
	private String displayTravelerRefNo = null;
	private List<PnrCreditFETO> paxCreditInfo = new ArrayList<PnrCreditFETO>();

	public BigDecimal getDisplayToPay() {
		return displayToPay;
	}

	public void setDisplayToPay(BigDecimal displayToPay) {
		this.displayToPay = displayToPay;
	}

	public boolean hasCreditPayment() {
		if (getPaxCreditInfo() != null) {
			for (PnrCreditFETO paxCredit : getPaxCreditInfo()) {
				if (paxCredit.getUsedAmount() != null && paxCredit.getUsedAmount().compareTo(BigDecimal.ZERO) > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public String getDisplayTravelerRefNo() {
		return displayTravelerRefNo;
	}

	public void setDisplayTravelerRefNo(String displayTravelerRefNo) {
		this.displayTravelerRefNo = displayTravelerRefNo;
	}

	public List<PnrCreditFETO> getPaxCreditInfo() {
		return paxCreditInfo;
	}

	public void setPaxCreditInfo(List<PnrCreditFETO> paxCreditInfo) {
		this.paxCreditInfo = paxCreditInfo;
	}


}
