package com.isa.thinair.xbe.api.dto.v2;

/**
 * @author eric
 * 
 */
public class FlightScheduleDTO {

	private int id;

	private String flightNo;

	private String departureTime;
	private String arrivalTime;
	private String daysOfOperation;
	private String fromDate;
	private String toDate;
	private String noOfStopovers;
	private String flightModel;

	private String totalFlyingTime;
	private String transitDuration;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDaysOfOperation() {
		return daysOfOperation;
	}

	public void setDaysOfOperation(String daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getNoOfStopovers() {
		return noOfStopovers;
	}

	public void setNoOfStopovers(String noOfStopovers) {
		this.noOfStopovers = noOfStopovers;
	}

	public String getFlightModel() {
		return flightModel;
	}

	public void setFlightModel(String flightModel) {
		this.flightModel = flightModel;
	}

	public String getTotalFlyingTime() {
		return totalFlyingTime;
	}

	public void setTotalFlyingTime(String totalFlyingTime) {
		this.totalFlyingTime = totalFlyingTime;
	}

	public String getTransitDuration() {
		return transitDuration;
	}

	public void setTransitDuration(String transitDuration) {
		this.transitDuration = transitDuration;
	}
}
