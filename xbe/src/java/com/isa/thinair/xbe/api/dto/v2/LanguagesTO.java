package com.isa.thinair.xbe.api.dto.v2;

/**
 * 
 * @author Indika Athauda
 * 
 */
public class LanguagesTO {

	/** Language ID consist locale code ex: en,fr,etc. */
	private String id;
	private String desc;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

}
