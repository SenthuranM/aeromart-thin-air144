package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

public class PaxAccountCreditTO {

	private String displayAvailableCredit;

	private String displayExpiredCredit;

	private String currency;

	private Collection<CreditsListTO> credits;
	
	private String displayActualCredit;

	/**
	 * @return the displayAvailableCredit
	 */
	public String getDisplayAvailableCredit() {
		return displayAvailableCredit;
	}

	/**
	 * @param displayAvailableCredit
	 *            the displayAvailableCredit to set
	 */
	public void setDisplayAvailableCredit(String displayAvailableCredit) {
		this.displayAvailableCredit = displayAvailableCredit;
	}

	/**
	 * @return the displayExpiredCredit
	 */
	public String getDisplayExpiredCredit() {
		return displayExpiredCredit;
	}

	/**
	 * @param displayExpiredCredit
	 *            the displayExpiredCredit to set
	 */
	public void setDisplayExpiredCredit(String displayExpiredCredit) {
		this.displayExpiredCredit = displayExpiredCredit;
	}

	/**
	 * @return the credits
	 */
	public Collection<CreditsListTO> getCredits() {
		return credits;
	}

	/**
	 * @param credits
	 *            the credits to set
	 */
	public void setCredits(Collection<CreditsListTO> credits) {
		this.credits = credits;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDisplayActualCredit() {
		return displayActualCredit;
	}

	public void setDisplayActualCredit(String displayActualCredit) {
		this.displayActualCredit = displayActualCredit;
	}
	
}
