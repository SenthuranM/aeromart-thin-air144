package com.isa.thinair.xbe.api.dto.v2;

public class PaxCreditTO {

	private String paxID;

	private String paxName;

	private String availableCredit;

	private String utilizedCredit;

	private String originalCredit;

	private String carrier;

	private String pnr;

	private boolean selected = false;

	private String residingCarrierAmount;

	/**
	 * @return the paxID
	 */
	public String getPaxID() {
		return paxID;
	}

	/**
	 * @param paxID
	 *            the paxID to set
	 */
	public void setPaxID(String paxID) {
		this.paxID = paxID;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the availableCredit
	 */
	public String getAvailableCredit() {
		return availableCredit;
	}

	/**
	 * @param availableCredit
	 *            the availableCredit to set
	 */
	public void setAvailableCredit(String availableCredit) {
		this.availableCredit = availableCredit;
	}

	/**
	 * @return the utilizedCredit
	 */
	public String getUtilizedCredit() {
		return utilizedCredit;
	}

	/**
	 * @param utilizedCredit
	 *            the utilizedCredit to set
	 */
	public void setUtilizedCredit(String utilizedCredit) {
		this.utilizedCredit = utilizedCredit;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getOriginalCredit() {
		return originalCredit;
	}

	public void setOriginalCredit(String originalCredit) {
		this.originalCredit = originalCredit;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier
	 *            the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getResidingCarrierAmount() {
		return residingCarrierAmount;
	}

	public void setResidingCarrierAmount(String residingCarrierAmount) {
		this.residingCarrierAmount = residingCarrierAmount;
	}

}
