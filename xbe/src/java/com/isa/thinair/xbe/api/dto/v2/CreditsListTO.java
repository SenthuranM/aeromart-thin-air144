package com.isa.thinair.xbe.api.dto.v2;

public class CreditsListTO {

	private String displayTxnID;

	private String displayAmount;

	private String displayExpiryDate;

	private String displayNotes;

	private String displayCreditID;

	private String tnxStatus;

	private String calendarDate;

	private String carrierCode;
	
	private String mcCreditAmount;
	
	private String lccUniqueTnxId;
	
	/**
	 * @return the displayTxnID
	 */
	public String getDisplayTxnID() {
		return displayTxnID;
	}

	/**
	 * @param displayTxnID
	 *            the displayTxnID to set
	 */
	public void setDisplayTxnID(String displayTxnID) {
		this.displayTxnID = displayTxnID;
	}

	/**
	 * @return the displayAmount
	 */
	public String getDisplayAmount() {
		return displayAmount;
	}

	/**
	 * @param displayAmount
	 *            the displayAmount to set
	 */
	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}

	/**
	 * @return the displayExpiryDate
	 */
	public String getDisplayExpiryDate() {
		return displayExpiryDate;
	}

	/**
	 * @param displayExpiryDate
	 *            the displayExpiryDate to set
	 */
	public void setDisplayExpiryDate(String displayExpiryDate) {
		this.displayExpiryDate = displayExpiryDate;
	}

	/**
	 * @return the displayNotes
	 */
	public String getDisplayNotes() {
		return displayNotes;
	}

	/**
	 * @param displayNotes
	 *            the displayNotes to set
	 */
	public void setDisplayNotes(String displayNotes) {
		this.displayNotes = displayNotes;
	}

	public String getDisplayCreditID() {
		return displayCreditID;
	}

	public void setDisplayCreditID(String displayCreditID) {
		this.displayCreditID = displayCreditID;
	}

	/**
	 * @return the tnxStatus
	 */
	public String getTnxStatus() {
		return tnxStatus;
	}

	/**
	 * @param tnxStatus
	 *            the tnxStatus to set
	 */
	public void setTnxStatus(String tnxStatus) {
		this.tnxStatus = tnxStatus;
	}

	/**
	 * @return the calendarDate
	 */
	public String getCalendarDate() {
		return calendarDate;
	}

	/**
	 * @param calendarDate
	 *            the calendarDate to set
	 */
	public void setCalendarDate(String calendarDate) {
		this.calendarDate = calendarDate;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getMcCreditAmount() {
		return mcCreditAmount;
	}

	public void setMcCreditAmount(String mcCreditAmount) {
		this.mcCreditAmount = mcCreditAmount;
	}

	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}
	
}
