package com.isa.thinair.xbe.api.dto.v2;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class CustomerRegPrivillagesDTO {

	private boolean addCustomer = false;

	private boolean editCustomer = false;

	private boolean viewCustomer = false;

	private boolean viewCustomerFullSearchAccess = false;
	
	private boolean editName = false;

	public CustomerRegPrivillagesDTO(HttpServletRequest request) {
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_ADD)) {
			this.addCustomer = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_EDIT)) {
			this.editCustomer = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_VIEW)) {
			this.viewCustomer = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_FULL_SEARCH_ACCESS)) {
			this.viewCustomerFullSearchAccess = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.XBECustomerPrivilageKeys.XBE_CUSTOMER_NAME_EDIT)) {
			this.setEditName(true);
		}
	}

	public boolean isAddCustomer() {
		return addCustomer;
	}

	public void setAddCustomer(boolean addCustomer) {
		this.addCustomer = addCustomer;
	}

	public boolean isEditCustomer() {
		return editCustomer;
	}

	public void setEditCustomer(boolean editCustomer) {
		this.editCustomer = editCustomer;
	}

	public boolean isViewCustomer() {
		return viewCustomer;
	}

	public void setViewCustomer(boolean viewCustomer) {
		this.viewCustomer = viewCustomer;
	}

	public boolean isViewCustomerFullSearchAccess() {
		return viewCustomerFullSearchAccess;
	}

	public void setViewCustomerFullSearchAccess(boolean viewCustomerGrid) {
		this.viewCustomerFullSearchAccess = viewCustomerGrid;
	}

	public boolean isEditName() {
		return editName;
	}

	public void setEditName(boolean editName) {
		this.editName = editName;
	}
}
