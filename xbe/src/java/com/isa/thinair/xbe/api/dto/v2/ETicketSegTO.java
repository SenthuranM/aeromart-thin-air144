/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author M.Rikaz
 */
package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.util.Date;

public class ETicketSegTO implements Comparable<ETicketSegTO>, Serializable {

	private static final long serialVersionUID = 1L;

	private String travelerRefNumber;

	private Integer ppfsId;

	private Integer eticketId;

	private String segmentCode;

	private String segmentStatus;

	private String flightNo;

	private String carrierCode;

	private String departureTime;
	
	private Date departureDateTime;

	private String paxETicketNo;

	private String couponNo;

	private String paxETicketStatus;

	private String paxStatus;
	
	private String couponControl;

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public Integer getPpfsId() {
		return ppfsId;
	}

	public void setPpfsId(Integer ppfsId) {
		this.ppfsId = ppfsId;
	}

	public Integer getEticketId() {
		return eticketId;
	}

	public void setEticketId(Integer eticketId) {
		this.eticketId = eticketId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentStatus() {
		return segmentStatus;
	}

	public void setSegmentStatus(String segmentStatus) {
		this.segmentStatus = segmentStatus;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getPaxETicketNo() {
		return paxETicketNo;
	}

	public void setPaxETicketNo(String paxETicketNo) {
		this.paxETicketNo = paxETicketNo;
	}

	public String getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(String couponNo) {
		this.couponNo = couponNo;
	}

	public String getPaxETicketStatus() {
		return paxETicketStatus;
	}

	public void setPaxETicketStatus(String paxETicketStatus) {
		this.paxETicketStatus = paxETicketStatus;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	@Override
	public int compareTo(ETicketSegTO o) {
		return this.getDepartureDateTime().compareTo(o.getDepartureDateTime());
	}

	public String getCouponControl() {
		return couponControl;
	}

	public void setCouponControl(String couponControl) {
		this.couponControl = couponControl;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}	
}
