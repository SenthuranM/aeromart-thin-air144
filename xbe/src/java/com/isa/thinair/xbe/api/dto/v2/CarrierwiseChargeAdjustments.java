package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;

/**
 * Holds charge adjustment types collection for a given carrier.
 * @author sanjaya
 *
 */
public class CarrierwiseChargeAdjustments implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** The carrier code this collection applies to	 */
	private String carrierCode;
	
	/** The collection of charge adjustment types */
	private Collection<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes = new ArrayList<ChargeAdjustmentTypeDTO>();

	/**
	 * @return : The carrier code of the collection of privileges.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode : The carrier code to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return : A collection of {@link ChargeAdjustmentTypeDTO} 
	 */
	public Collection<ChargeAdjustmentTypeDTO> getChargeAdjustmentTypes() {
		return chargeAdjustmentTypes;
	}
}