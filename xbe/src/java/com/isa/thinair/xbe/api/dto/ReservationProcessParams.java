package com.isa.thinair.xbe.api.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airproxy.api.dto.ChargeAdjustmentPrivilege;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;
import com.isa.thinair.airproxy.api.utils.LccOperationsUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * The container for reservation process controlling parameters. The values in the {@link ReservationProcessParams}
 * object will be used to control the reservation creation and modify flows for privilege checks, modify eligibility
 * etc.
 * 
 */
public class ReservationProcessParams {

	private String defaultCurrencyCode;

	private final String baseCurrencyCode;

	private boolean triggerExternalSearch = false;

	private boolean cancelSegment = false;
	/* segment modification */
	private boolean modifySegmentByDate;

	private boolean modifySegmentByRoute;

	private boolean addSegment = false;

	private boolean overideFare = false;

	private boolean standByBooking = false;

	private boolean allowWaitListBooking = false;

	private boolean addSSR = false;

	private boolean seatBlock = false;

	private boolean forceConfirm = false;

	private boolean loadProfile = false;

	private boolean tbaBookings = false;

	private boolean buffertimeModification = false;

	private boolean fareRuleBufferTimeModification = false;

	private boolean onHold = false;

	private boolean bufferTimeOnHold = false;

	private boolean addInsurance = false;

	private boolean transferOwnership = false;

	private boolean transferOwnershipToAnyCarrier = false;

	private boolean cashPayments = false;

	private boolean bspPayments = false;

	private boolean voucherPayments = false;

	private boolean bspPaymentsForAlterRes = false;

	private boolean creditPayments = false;

	private boolean onAccountPayments = false;

	private boolean offlinePayments = false;

	private boolean onAccountMultiAgentPayments = false;

	private boolean onAccountPaymentsReporting = false;

	private boolean nameChange = false;

	private boolean onholdNameChange = false;

	private boolean overrideNameChangeCharge = false;

	private boolean BufferTimeNameChange = false;

	private boolean printItineraryForPartialPayment = false;

	private boolean removePassenger = false;

	private boolean groupChargeAdjustment = false;

	private boolean splitPassenger = false;

	private boolean addInfant = false;

	private boolean addUserNotes = false;

	private boolean cancelReservation = false;

	private boolean currencyManupulation = false;

	private boolean accessAnyBooking = false;

	private boolean accessAPIBookings = false;

	private boolean accessHolidaysBookings = false;

	private boolean accessXBEBookings = false;

	private boolean accessIBEBookings = false;

	private boolean partialPayments = false;

	private boolean onholdInsurance = false;

	private boolean adjustCharges = false;

	private boolean adjustOtherCarrierCharges = false;

	private boolean refundableChargeAdjust = false;

	private boolean nonRefundableChargeAdjust = false;

	private boolean viewReservationHistoryForOtherCarriers = false;

	private boolean tbaNameChange = false;

	private boolean postDepature = false;

	private boolean isFlownSegmentExists = false;

	private boolean allPaxNoshowInTheSegment = false;

	private boolean isUnSegmentExists = false;

	private boolean inBufferTime = false;

	private boolean viewItinerary = true;

	private boolean viewItineraryWithCharges = false;

	private boolean viewItineraryWithPaymentInfo = false;

	private boolean viewItineraryWithoutPaxCharges = false;

	private boolean notTransfered = false;

	private boolean clearAlert = false;

	private boolean transferAlert = false;

	private boolean modifyFlown = false;

	private boolean modifyNoshow = false;

	private boolean modifyCnxResAllowed = false;

	private boolean cancelledReservation = false;

	private boolean partialPaymentModify = false;

	private boolean modifyOnholdConfirm = false;

	private boolean allowOverBook = false;

	private boolean allowRefundNoCredit = false;

	private boolean allowCashRefund = false;

	public boolean isAllowOfflineRefund() {
		return allowOfflineRefund;
	}

	public void setAllowOfflineRefund(boolean allowOfflineRefund) {
		this.allowOfflineRefund = allowOfflineRefund;
	}

	private boolean allowCreditCardRefund = false;

	private boolean allowAnyCreditCardRefund = false;

	private boolean allowAnyCarrierRefund = false;

	private boolean allowOfflineRefund = false;

	private boolean allowOnAccountRefund = false; // refund to own account only

	private boolean allowOnAccountRptRefund = false; // refund to reporting carrier.

	private boolean allowBspRefund = false; // refund to BSP account

	private boolean allowAnyOnAccountRefund = false; // refund to any account

	private boolean allowAnyOpCarrierOnAccountRefund = false; // only relevant in dry users. (Allow to refund
																// to Op Carrier agents as well)
	private boolean allowKeepCommissionOnRefund = false;

	private boolean allowLCCAnyOndModify = false;

	private boolean overrideOnHoldRestricted = false;

	private boolean viewAvailableCredit = false;

	private boolean allowPaxCreditExpiryDateChange = false;

	private boolean allowReinstatePassengerCredits = false;

	private boolean viewReservationAuditAllowed = false;

	private boolean allowNamedCreditTransfer = false;

	private boolean allowAnyCreditTransfer = false;

	private boolean viewReservationDetailsAllowed = false;

	private boolean viewPassengerAccountAllowed = false;

	private boolean openReturnEnabled = false;

	private boolean bufferTimeModAccountAllowed = false;

	private boolean allowCOSSearch = false;

	private boolean allowCapturePayRef = false;

	private boolean groundServiceEnabled = false;

	private boolean customizedItineraryView = false;

	private boolean allowRecieptPrinting = false;

	private boolean displayContDetailsInPNRSummary = false;

	private boolean extendOnhold = false;

	private boolean changeWaitListingPriority = false;

	private boolean modifyContactDetails = false;

	private boolean viewExternalPayments = false;

	private boolean viewCredit = false;

	private boolean allowRefund = false;

	private boolean hasOverrideCnxModChargePrivilege = false;

	private boolean blnShowChargeTypeOveride = false;

	private boolean multiMealEnabled = false;

	private boolean voidReservation = false;

	private boolean bCSelectionAtAvailabilitySearchEnabled = false;

	private boolean endorsementsAllowed = false;

	private boolean countryPaxConfigEnabled = false;

	private boolean itineraryFareMaskPrivilege = false;

	private boolean itineraryFareMaskModifyPrivilege = false;

	private boolean originCountryOfCallPrivilege = false;

	private boolean originCountryModifyPrivilege = false;

	private boolean printPNRHistoryAllow = false;

	private boolean fltStopOverDurInfoToolTipEnabled = false;

	private boolean allowOverrideAnyFare = false;

	private boolean allowOverrideVisibleFare = false;

	private boolean allowOverrideAgentFareOnly = false;

	private boolean allowViewPublicOnewayFaresForReturn = false;

	private boolean allowOnholdRollForward = false;

	private boolean duplicateNameCheckEnabled = false;
	
	private boolean checkBlackListPax = false;

	private boolean duplicateNameSkipPrivilege = false;

	private boolean duplicateNameSkipModifyPrivilege = false;

	private boolean baggageMandatory = false;

	private boolean allowCountryPaxConfigOverride = false;

	private boolean allCoSChangeFareEnabled = false;

	private boolean allowChangePassengerCouponStatus = false;

	private boolean allowExceptionalEticketCouponModifications = false;

	private boolean requoteEnabled = false;

	private boolean allowDisplayPaxNames = false;

	private boolean allowVoidReservation = false;

	private boolean allowOverrideSameFare = false;

	private boolean enforceSameOrHigherFare = false;
	/**
	 * The charge adjustment privilege container holding the charge adjustment privileges for each carrier involved in
	 * the reservation
	 */
	private final ChargeAdjustPrivilegeContainer chargeAdjustPrivileges = new ChargeAdjustPrivilegeContainer();

	private final boolean requoteAutoFQEnabled;

	private final boolean requoteAutoAddOndFQEnabled;

	private final boolean requoteAutoCnxOndFQEnabled;

	private final boolean requoteAutoFltSelectEnabled;

	private boolean confirmButtonForOHDModification;

	private final boolean requoteSetBCForExistingSegments;

	private final boolean userNotesMandatory;

	private boolean tbaGroupNameUpload = false;

	private boolean reservationModificationsAllowed = false;

	private boolean gdsReservationModAllowed = false;

	private boolean autoCancellationEnable = false;

	private boolean allowExcludeCharges = false;

	private boolean allowViewAllBCFares = false;

	private boolean allowModifyToAnyBCWithinSameCOS = false;

	private boolean allowOverrideSameOrHigherFare = false;

	private boolean promoCodeEnabled = false;

	private boolean reQuoteOnlyRequiredONDs = false;

	private boolean allowPassengerDetailCSVUpload = false;

	private boolean allowExtendedPassengerNameModificationWithinBuffer = false;

	private boolean allowCreditUtilizedPassengerNameModification = false;

	private boolean allowPassengerNameModificationHavingDepatedSegments = false;

	private boolean allowPassengerUserNoteModificationHavingDepartedSegments = false;

	private boolean allowExtendedPassengerNameModification = false;

	private boolean allowCreateUserAlert = false;

	private boolean groupBookingSplitPassengers = false;

	private boolean agentCommissionEnabled = false;

	private boolean allowOverBookAfterCutoffTime = false;

	private boolean allowCaptureExternalInternationalFlightDetails = false;

	private boolean allowVoidOwnReservation = false;

	private boolean allowMultiCitySearch = false;

	private boolean onlyUNSegmentExist = true;

	private boolean createNewPnrFromOld = false;

	private boolean allowOnlySplitChgAdjustRefund = false;

	private boolean restrictModForCnxFlown = false;

	private boolean enableFareRuleNCC = false;

	private boolean enableFareRuleNCCForFlown = false;

	private int nameChangeMaxCount = 0;

	private Collection<String> allowedSubOperations;

	private boolean allowAddInfantToPartiallyFlownBookings = true;

	private boolean extendedNameChangeEnabled = false;

	private boolean allowCouponControlRequest = false;

	private boolean allowAddUserNotes = false;

	private boolean allowClassifyUserNotes = false;

	private boolean allowSkipNic = false;

	private boolean allowExternalTicketExchange = false;

	private boolean allowExtendedNameChange = false;
	// TODO in the next phase break this to two constructors one for make
	// reservation one for edit

	private boolean allowOverrideRequoteNameChange = false;

	private boolean clearAlertAfterTransfer = false;

	private boolean goquoValid = false;

	private boolean goquoBooking = false;

	private boolean allowViewMCO = false;

	private boolean allowRouteSelectionForAgents = false;

	private String taxRegistrationNumberEnabledCountries = "";
	
	private boolean allowToRemoveReprotectedAnciForOHD = true;

	// TODO in the next phase break this to two constructors one for make reservation one for edit

	public ReservationProcessParams(HttpServletRequest request, String status, Collection<LCCClientReservationSegment> resSegs,
			boolean reservationModificationsAllowed, boolean gdsReservationModAllowed, LCCPromotionInfoTO lccPromotionInfoTO,
			String ownerAgent, boolean isGdsPnr, boolean isGoquo) {

		this.reservationModificationsAllowed = reservationModificationsAllowed;
		this.gdsReservationModAllowed = gdsReservationModAllowed;
		if (gdsReservationModAllowed) {
			this.gdsReservationModAllowed = BasicRH.hasPrivilege(request,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_MODIFY_GDS_CS);
		}
		if (AppSysParamsUtil.restrictModificationForCancelledOrTotallyFlownBookings()) {
			this.restrictModForCnxFlown = true;
			this.allowOnlySplitChgAdjustRefund = reservationModificationsAllowed;
			reservationModificationsAllowed = !isReservationTotallyFlownOrCancelled(resSegs) && reservationModificationsAllowed;
			this.reservationModificationsAllowed = reservationModificationsAllowed;
		}

		// check modifications are allowed for
		this.modifyCnxResAllowed = BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_CNX_MODIFY)
				&& (reservationModificationsAllowed || gdsReservationModAllowed || this.allowOnlySplitChgAdjustRefund);
		this.modifyFlown = BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)
				&& reservationModificationsAllowed;
		this.modifyNoshow = BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NOSHOW);

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ADD_SEGMENT)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.addSegment = true;

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentCurrencyCode = userPrincipal.getAgentCurrencyCode();
		String agentCode = userPrincipal.getAgentCode();
		this.baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		this.defaultCurrencyCode = baseCurrencyCode;

		this.goquoValid = ((BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_AEROMART_PAY)
				&& isGoquo) || !isGoquo) ? true : false;

		this.setGoquoBooking(isGoquo);

		boolean bulkTicketBooking = isBulkTicketReservation(resSegs);

		if (agentCurrencyCode != null) {
			this.defaultCurrencyCode = agentCurrencyCode;
		}

		if (status != null && status.equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
			// do nothing, use the privilege as it is
			this.cancelReservation = false;
			this.cancelledReservation = true;
			this.nameChange = false;
			if (!BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_VIEW_CNX_ITEN)) {
				this.viewItinerary = false;
			}
			if (!modifyCnxResAllowed) {
				this.addSegment = false;
			}
		} else {
			// if this is not a cancel reservation modifyCnxResAllowed privilage can consider as true
			this.modifyCnxResAllowed = true;
		}

		if (status != null && status.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
			if (!BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ONHOLD)) {
				this.viewItinerary = false;
			}
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITH_PAYMENT_INFO)) {
			this.viewItineraryWithPaymentInfo = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITHOUT_PAX_CHARGES)) {
			this.viewItineraryWithoutPaxCharges = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITH_CHARGES)) {
			this.viewItineraryWithCharges = true;
		}
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERBOOK)) {
			this.allowOverBook = true;
		}

		if (resSegs != null) {
			checkBufferTimes(resSegs);
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.RES_TRANSFER)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.transferOwnership = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.RES_ANYCARRIER_TRANSFER)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.transferOwnershipToAnyCarrier = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.CHANGE_CURRENCY)
				&& reservationModificationsAllowed)
			this.currencyManupulation = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_VISIBLE_FARE)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_NAME_CHANGE_FARE_ANY)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_AGENT_FARE_ONLY))
			this.overideFare = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.MAKE_STANDBY_BOOKING))
			this.standByBooking = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.MAKE_WAITLIST_BOOKING))
			this.allowWaitListBooking = true;
		if ((BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_ADD_INF)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_ADD_INFANT))
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.addInfant = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_SSR_ADD)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SSR)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_SSR_CHANGE))
			this.addSSR = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_SEATBLOCKING))
			this.seatBlock = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.RES_ACCEPT_NO_PAY))
			this.forceConfirm = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_TBA))
			this.tbaBookings = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.LOAD_PROFILE))
			this.loadProfile = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_ALLOW_BUFFERTIME_OHD)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.RES_MAKE_ONHOLD))
			this.onHold = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_ALLOW_BUFFERTIME_OHD))
			this.bufferTimeOnHold = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD))
			this.overrideOnHoldRestricted = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANY_CHANNEL))
			this.accessAnyBooking = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_DNATA_CHANNEL))
			this.accessAPIBookings = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_HOLIDAY_CHANNEL))
			this.accessHolidaysBookings = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_IBE_CHANNEL))
			this.accessIBEBookings = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_XBE_CHANNEL))
			this.accessXBEBookings = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_INSURANCE))
			this.addInsurance = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_PARTIAL_PAYMNETS))
			this.partialPayments = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS))
			this.cashPayments = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_OFFLINE_PAYMNETS))
			this.offlinePayments = (status != null && status.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD))
					? BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OHD_CONF)
					: true;
		// bsp enable only to pay for fresh booking payment and onhold bookings.
		if (AppSysParamsUtil.isEnableTransactionGranularity()
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_BSP_PAYMNETS))
			if ((status != null && status.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) || status == null) {
				if (AppSysParamsUtil.isBspPaymentsAcceptedForMCCreateReservation()) {
					this.bspPayments = true;
				}
			} else {
				if (AppSysParamsUtil.isBspPaymentsAcceptedForMCAlterReservation()) {
					this.bspPayments = true;
					this.bspPaymentsForAlterRes = true;
				}
			}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS)
				|| BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_ANY))
			this.onAccountPayments = true;

		if (this.onAccountPayments
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_MULTIPLE_AGENT_ONACCOUNT_PAYMNETS))
			this.onAccountMultiAgentPayments = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_CARD_PAYMNETS))
			this.creditPayments = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_VOUCHER_PAYMNETS))
			this.voucherPayments = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING))
			this.onAccountPaymentsReporting = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_INSURANCE_OHD))
			this.onholdInsurance = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ADD_USER_NOTE))
			this.allowAddUserNotes = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.CLASSIFY_USER_NOTE))
			this.allowClassifyUserNotes = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_RES_HISTORY_OTHERS))
			this.viewReservationHistoryForOtherCarriers = true;

		if (!(this.postDepature || this.inBufferTime)
				&& (!bulkTicketBooking && (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_REMOVE_PAX))
						|| (bulkTicketBooking
								&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX)))
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.removePassenger = true;
		if (!this.isFlownSegmentExists && !this.postDepature && !this.inBufferTime
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.CANCEL_RES)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.cancelReservation = true;
		if (!this.postDepature && this.inBufferTime
				&& (!bulkTicketBooking
						&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX)
						|| (bulkTicketBooking
								&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX)))
				&& (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.cancelReservation = false;
			this.removePassenger = false;
		}
		if (this.postDepature && this.modifyFlown && (reservationModificationsAllowed || gdsReservationModAllowed))
			this.cancelReservation = false;
		if (!this.isFlownSegmentExists && this.modifyFlown && (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.cancelReservation = true;
		}

		if (!this.isFlownSegmentExists && this.modifyFlown
				&& (!bulkTicketBooking && (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_REMOVE_PAX))
						|| (bulkTicketBooking
								&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX)))
				&& (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.removePassenger = true;
		}
		// If NOSHOW segment there, alllow to modify the reservation if no flown segments
		if (!this.isFlownSegmentExists && this.modifyNoshow && this.allPaxNoshowInTheSegment
				&& (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.cancelReservation = true;
		}
		// If UN (Flight cnx and res segment cnf) segment there, alllow to
		// modify the reservation if no flown segments
		if (!this.isFlownSegmentExists && !this.postDepature && this.isUnSegmentExists
				&& (reservationModificationsAllowed || gdsReservationModAllowed)
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
			this.cancelReservation = true;
		}

		if (!this.isFlownSegmentExists && this.postDepature && this.isUnSegmentExists
				&& (onlyUNSegmentExist || (!onlyUNSegmentExist && modifyFlown))
				&& (reservationModificationsAllowed || gdsReservationModAllowed)
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
			this.cancelReservation = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.cancelSegment = true;
		// if(BasicRH.hasPrivilege(request, "xbe.res.alt.seg.mod")) this.modifySegment = true;
		// /* segment modification */
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.modifySegmentByDate = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.modifySegmentByRoute = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX)
				&& reservationModificationsAllowed)
			this.buffertimeModification = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_FARE_BUFFER)
				&& reservationModificationsAllowed)
			this.fareRuleBufferTimeModification = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_NAME_CHANGE)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.nameChange = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ONHOlD_NAME_CHANGE)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.onholdNameChange = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME)
				&& reservationModificationsAllowed)
			this.BufferTimeNameChange = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_TBA_NAME_CHANGE)
				&& reservationModificationsAllowed)
			this.tbaNameChange = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT)
				&& ((this.modifyCnxResAllowed && (reservationModificationsAllowed
						|| (!reservationModificationsAllowed && this.allowOnlySplitChgAdjustRefund)))))
			this.adjustCharges = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ADJUST_OTHER_CARRIER_CHARGES)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.adjustOtherCarrierCharges = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_REFUND)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.refundableChargeAdjust = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_NONREFUND)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.nonRefundableChargeAdjust = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_UPDATE))
			this.addUserNotes = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.CLEAR_ALERT) && reservationModificationsAllowed)
			this.clearAlert = true;
		this.clearAlertAfterTransfer = AppSysParamsUtil.removeActionAlertInTransfer();
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_TRANSFER_SEGMENT)
				&& (reservationModificationsAllowed || isGdsPnr))
			this.transferAlert = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.partialPaymentModify = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OHD_CONF)
				&& (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.modifyOnholdConfirm = true;
		}

		// <<<<<<<<<<refunds section start
		// enable the refund privileges for void reservations
		setVoidReservation(false);
		if (status != null && status.equals(ReservationInternalConstants.ReservationStatus.VOID)) {
			setVoidReservation(true);
			cancelReservation = false;
			addSegment = false;
			transferOwnership = false;
			transferOwnershipToAnyCarrier = false;
		}

		// refund options enabled for void reservations.
		setRefundOptions(request);
		// >>>>>>>>>>>refunds section end

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ANY_OPCARRIER_ON_ACC_REFUND)
				&& (this.allowOnlySplitChgAdjustRefund))
			this.allowAnyOpCarrierOnAccountRefund = true;// Allow ANY Operation Carrier Refund for dry users
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_PAX_REFUND_CCARD)
				&& (this.allowOnlySplitChgAdjustRefund))
			this.allowCreditCardRefund = true;// Allow Credit Card Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_PAX_REFUND_CCARD_ANY)
				&& (this.allowOnlySplitChgAdjustRefund))
			this.allowAnyCreditCardRefund = true; // Allow ANY Credit Card Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ON_ACC_ANY)
				&& (this.allowOnlySplitChgAdjustRefund))
			this.allowAnyOnAccountRefund = true;// Allow ANY On Account Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_LCC_ANY_OND_MODIFY))
			this.allowLCCAnyOndModify = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VIEW_CREDIT))
			this.viewAvailableCredit = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS))
			this.allowNamedCreditTransfer = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS_ANY))
			this.allowAnyCreditTransfer = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_EXP_DATE_CHANGE))
			this.allowPaxCreditExpiryDateChange = true;// Allow change passenger credit expiry date
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_REINSTATE))
			this.allowReinstatePassengerCredits = true;// Allow reinstate passenger credits
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_RES_HISTORY)
		/* && reservationModificationsAllowed */)
			this.viewReservationAuditAllowed = true;// Allow to view reservation history
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.VIEW_RES_DETAILS))
			this.viewReservationDetailsAllowed = true;// Allow to view reservation details by clicking on search result
														// row
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_VIEW_PAX_ACC))
			this.viewPassengerAccountAllowed = true;// Allow to view passenger account
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_CHARGES)
				&& reservationModificationsAllowed)
			this.bufferTimeModAccountAllowed = true;// Allow to view and edit account during buffer time.
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_LCC_ANY_OND_MODIFY)
				&& reservationModificationsAllowed)
			this.allowLCCAnyOndModify = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_COS))
			this.allowCOSSearch = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CUST_ITI))
			this.customizedItineraryView = true; // Allow user to display customized Itinerary
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_RCPT_PRINT))
			this.allowRecieptPrinting = true; // Allow user to Allow Print Reciept
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VIEW_CREDIT))
			this.viewAvailableCredit = true;
		if ((!bulkTicketBooking && (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_SPLIT))
				|| (bulkTicketBooking && BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_SPLIT)))
				&& (reservationModificationsAllowed || gdsReservationModAllowed || this.allowOnlySplitChgAdjustRefund))
			this.splitPassenger = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_GROUP_BOOKING_SPLIT))
			this.groupBookingSplitPassengers = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_EXTEND)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.extendOnhold = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.RES_CHANGE_WL_PRIORITY)
				&& reservationModificationsAllowed)
			this.changeWaitListingPriority = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.MULTIPLE_CHARGE_ADJUSTMENT)
				&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund))
			this.groupChargeAdjustment = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.CHANGE_CONTACT_DETAILS)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.modifyContactDetails = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRINT_ITN_FOR_PARTIAL_PAYMENTS))
			this.printItineraryForPartialPayment = true; // Allow user print Itinerary if partial payment happened
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_EXT_PAY_VIEW))
			this.viewExternalPayments = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_VIEW_CREDIT))
			this.viewCredit = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PAX_REFUND))
			this.allowRefund = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ENDORSEMENTS))
			this.endorsementsAllowed = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_ITINERARY_FARE_MASK))
			this.itineraryFareMaskPrivilege = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ITINERARY_FARE_MASK_MODIFY))
			this.itineraryFareMaskModifyPrivilege = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRINT_PNR_HISTORY_ALLOW))
			this.printPNRHistoryAllow = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_VISIBLE_FARE))
			this.allowOverrideVisibleFare = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_NAME_CHANGE_FARE_ANY))
			this.allowOverrideAnyFare = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_AGENT_FARE_ONLY))
			this.allowOverrideAgentFareOnly = true;
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_VIEW_PUBLIC_ONEWAY_FOR_RETURNS))
			this.allowViewPublicOnewayFaresForReturn = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ONHOLD_ROLL_FORWARD)
				&& reservationModificationsAllowed)
			this.allowOnholdRollForward = true;
		this.overrideNameChangeCharge = (BasicRH.hasPrivilege(request,
				PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_OVERRIDE_NAME_CHANGE));

		this.duplicateNameCheckEnabled = AppSysParamsUtil.isDuplicateNameCheckEnabled();
		
		this.checkBlackListPax = AppSysParamsUtil.isBlacklistpassengerCheckEnabled();		

		this.tbaGroupNameUpload = (BasicRH.hasPrivilege(request,
				PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_TBA_GROUP_NAME_UPLOAD));

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP))
			this.duplicateNameSkipPrivilege = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP))
			this.duplicateNameSkipModifyPrivilege = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG))
			this.allowCountryPaxConfigOverride = true;

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.VIEW_RES_PAX_NAMES)
				&& reservationModificationsAllowed) {
			this.allowDisplayPaxNames = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VOID_RESERVATION)
				&& (reservationModificationsAllowed || gdsReservationModAllowed || this.allowOnlySplitChgAdjustRefund)) {
			this.allowVoidReservation = true;
		}

		openReturnEnabled = AppSysParamsUtil.isOpenEndedReturnEnabled();

		allowCapturePayRef = AppSysParamsUtil.isAllowCapturePayRef();

		groundServiceEnabled = AppSysParamsUtil.isGroundServiceEnabled();

		displayContDetailsInPNRSummary = AppSysParamsUtil.isDisplayContactDetailOnPNRSummary();

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.OVERRIDE_CHARGES))
			this.hasOverrideCnxModChargePrivilege = true;

		blnShowChargeTypeOveride = AppSysParamsUtil.isAllowPercentageWiseModificationCharges();

		multiMealEnabled = AppSysParamsUtil.isMultipleMealSelectionEnabled();

		bCSelectionAtAvailabilitySearchEnabled = AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled();

		countryPaxConfigEnabled = (AppSysParamsUtil.isCountryPaxConfigEnabled() && !allowCountryPaxConfigOverride);

		baggageMandatory = AppSysParamsUtil.isBaggageMandatory();

		allCoSChangeFareEnabled = AppSysParamsUtil.isAllCoSChangeFareEnabled();

		fltStopOverDurInfoToolTipEnabled = AppSysParamsUtil.isShowFlightStopOvrDurInfo();

		// allowChangePassengerCouponStatus = BasicRH.hasPrivilege(request,
		// PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_PASSENGER_COUPON_STATUS);
		boolean isCodeShareBooking = false;
		if (request.getParameter("gdsId") != null && !("").equals(request.getParameter("gdsId"))) {
			isCodeShareBooking = ReservationApiUtils.isCodeShareReservation(Integer.parseInt(request.getParameter("gdsId")));
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_PASSENGER_COUPON_STATUS)
				&& (reservationModificationsAllowed || isCodeShareBooking)) {
			this.allowChangePassengerCouponStatus = true;
		}

		requoteEnabled = AppSysParamsUtil.isRequoteEnabled();

		requoteAutoFQEnabled = AppSysParamsUtil.isRequoteAutoFareQuoteEnabled();

		requoteAutoAddOndFQEnabled = AppSysParamsUtil.isRequoteAutoAddOndFareQuoteEnabled();

		requoteAutoCnxOndFQEnabled = AppSysParamsUtil.isRequoteAutoCancelOndFareQuoteEnabled();

		requoteAutoFltSelectEnabled = AppSysParamsUtil.isRequoteAutoFlightSelectionEnabled();

		requoteSetBCForExistingSegments = AppSysParamsUtil.setBCForExistingSegments();

		userNotesMandatory = AppSysParamsUtil.isUserNoteMandatory();

		// adds the own airline charge adjustment privileges.
		addOwnAirlineChargeAdjustmentTypePrivileges(request);

		this.confirmButtonForOHDModification = BasicRH.hasPrivilege(request,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OHD_NO_PAY);

		autoCancellationEnable = AppSysParamsUtil.isAutoCancellationEnabled()
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX);

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_EXCLUDE_CHARGES)) {
			this.allowExcludeCharges = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_EXCEPTIONAL_ETICKET_COUPON_MODIFICATIONS)) {
			this.allowExceptionalEticketCouponModifications = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_VIEW_ALL_BC_FARE)) {
			allowViewAllBCFares = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_MODIFY_ANY_BC_ON_SAME_COS)) {
			this.allowModifyToAnyBCWithinSameCOS = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE)) {
			this.allowOverrideSameOrHigherFare = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_OVERRIDE_SAME_FARE_MODIFICATION)) {
			this.allowOverrideSameFare = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CSV_DETAIL_UPLOAD)) {
			this.allowPassengerDetailCSVUpload = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CREATE_USER_ALERT)) {
			this.allowCreateUserAlert = true;
		}

		if (AppSysParamsUtil.applyExtendedNameModificationFunctionality()) {
			if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_EXTENDED_NAME_WITHIN_BUFFER)) {
				this.allowExtendedPassengerNameModificationWithinBuffer = true;
			}

			if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_EXTENDED_NAME)) {
				this.allowExtendedPassengerNameModification = true;
			}

			if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_CREDITUTILIZED_PNR)) {
				this.allowCreditUtilizedPassengerNameModification = true;
			}

			if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_WITH_DEPARTED_SEGMENTS)) {
				this.allowPassengerNameModificationHavingDepatedSegments = true;
			}

			if (allowExtendedPassengerNameModificationWithinBuffer || allowCreditUtilizedPassengerNameModification
					|| allowExtendedPassengerNameModification || allowPassengerNameModificationHavingDepatedSegments) {
				this.nameChange = true;
				this.allowExtendedNameChange = true;
			}
		} else {
			if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_WITH_DEPARTED_SEGMENTS)) {
				if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
					this.setAllowPassengerNameModificationHavingDepatedSegments(true);
					this.nameChange = true;
				}
			}
		}
	
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_USER_NOTE_WITH_DEPARTED_SEGMENTS)) {
			this.setAllowPassengerUserNoteModificationHavingDepartedSegments(true);
			this.addUserNotes = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_EXTENDED_NAME)) {
			this.allowExtendedPassengerNameModification = true;
		}

		if (AppSysParamsUtil.isAgentCommmissionEnabled()) {
			this.agentCommissionEnabled = true;
		}

		if (BasicRH.hasPrivilege(request,
				PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME)) {
			this.allowOverBookAfterCutoffTime = true;
		}

		if (ownerAgent != null && agentCode.equals(ownerAgent)
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VOID_OWN_RESERVATION)
				&& (reservationModificationsAllowed || gdsReservationModAllowed)) {
			this.allowVoidOwnReservation = true;
		}

		if (!this.isFlownSegmentExists && !this.postDepature && this.inBufferTime
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALT_CNX_RES_AFTER_CUTOFF_TIME)
				&& (reservationModificationsAllowed || gdsReservationModAllowed))
			this.cancelReservation = true;

		reQuoteOnlyRequiredONDs = AppSysParamsUtil.isReQuoteOnlyRequiredONDs();
		enforceSameOrHigherFare = AppSysParamsUtil.isEnforceSameHigher();
		overrideModificationOperations(lccPromotionInfoTO, resSegs);

		if (AppSysParamsUtil.isOpenJawMultiCitySearchEnabled()) {
			if (BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_MULTICITY_SEARCH)
					|| AppSysParamsUtil.isAllowMulticitySearchForAllXbeUsers()) {
				this.allowMultiCitySearch = true;
			}
		}

		if (AppSysParamsUtil.isExternalInternationalFlightInfoCaptureEnabled() && BasicRH.hasPrivilege(request,
				PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CAPTURE_EXTERNAL_INTERNATIONAL_FLT_DETAILS)) {
			allowCaptureExternalInternationalFlightDetails = true;
		}

		if ((BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.CREATE_NEW_PNR_FROM_OLD))) {
			this.createNewPnrFromOld = true;
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_SKIP_NIC)) {
			this.setAllowSkipNic(true);
		}

		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_EXTERNAL_TICKET_EXCHANGE)) {
			this.setAllowExternalTicketExchange(true);
		}

		this.enableFareRuleNCC = AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.XBE);

		this.enableFareRuleNCCForFlown = AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.XBE);

		this.nameChangeMaxCount = AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.XBE);

		this.allowedSubOperations = LccOperationsUtil.getGrantedOperations(BasicRH.getPrivileges(request));

		this.allowAddInfantToPartiallyFlownBookings = AppSysParamsUtil.isAllowAddInfantToPartiallyFlownBookings();

		this.extendedNameChangeEnabled = AppSysParamsUtil.applyExtendedNameModificationFunctionality();

		this.allowOverrideRequoteNameChange = BasicRH.hasPrivilege(request,
				PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_OVERRIDE_REQUOTE_NAME_CHANGE);

		this.setAllowViewMCO(AppSysParamsUtil.isEnableElectronicMCO()
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VIEW_MCO));

		this.setAllowRouteSelectionForAgents(AppSysParamsUtil.isEnableRouteSelectionForAgents());

		this.taxRegistrationNumberEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries();
		setGoquoPrivilage();
		
		this.allowToRemoveReprotectedAnciForOHD = AppSysParamsUtil.shouldRemoveReprotectedBundledAnciForOHD();

	}

	/**
	 * Sets options for refund parameters
	 * 
	 * @param request
	 * @param reservationModificationsAllowed
	 */
	private void setRefundOptions(HttpServletRequest request) {
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_REFUND_NO_CREDIT))
			this.allowRefundNoCredit = true;// Allow Refund for no credit
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_CASH_REFUND))
			this.allowCashRefund = true;// Allow Cash Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ON_ACC_REFUND))
			this.allowOnAccountRefund = true;// Allow On Account Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_OFFLINE_REFUND))
			this.allowOfflineRefund = true;
		if (AppSysParamsUtil.isBspPaymentsAcceptedForMCCreateReservation()
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_BSP_REFUND))
			this.allowBspRefund = true;// Allow BSP Account Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ON_ACCOUNT_RPT_REFUND))
			this.allowOnAccountRptRefund = true;// Allow Reporting On Account Refund
		if (BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ANY_CARRIER_REFUND))
			this.allowAnyCarrierRefund = true;// Allow ANY Carrier Refund
		if (AppSysParamsUtil.isAgentCommmissionEnabled()
				&& BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_KEEP_AGENT_COMMISSION_ON_REFUND))
			this.allowKeepCommissionOnRefund = true;// Allow keep to agent commission on Refund
	}

	public void enableInterlineModificationParams(List<String> modificationParams, String status,
			Collection<LCCClientReservationSegment> resSegs, boolean reservationModificationsAllowed,
			LCCPromotionInfoTO lccPromotionInfoTO) {

		enableInterlineModificationParams(modificationParams, status, resSegs, reservationModificationsAllowed,
				lccPromotionInfoTO, true);

	}

	public void enableInterlineModificationParams(List<String> modificationParams, String status,
			Collection<LCCClientReservationSegment> resSegs, boolean reservationModificationsAllowed,
			LCCPromotionInfoTO lccPromotionInfoTO, boolean isTransferable) {

		if (modificationParams != null && modificationParams.size() > 0) {

			boolean bulkTicketBooking = isBulkTicketReservation(resSegs);

			this.reservationModificationsAllowed = reservationModificationsAllowed;

			if (modificationParams.contains(ModifcationParamTypes.RESTRICT_MODIFICATION_CNX_FLOWN_BOOKINGS)) {
				this.restrictModForCnxFlown = true;
				this.allowOnlySplitChgAdjustRefund = reservationModificationsAllowed;
				reservationModificationsAllowed = !isReservationTotallyFlownOrCancelled(resSegs)
						&& reservationModificationsAllowed;
				this.reservationModificationsAllowed = reservationModificationsAllowed;
			}

			this.modifyCnxResAllowed = (modificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_CNX_RESERVATION)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.modifyFlown = (modificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_FLOWN)
					&& reservationModificationsAllowed) ? true : false;

			this.modifySegmentByDate = (modificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_DATE)
					&& reservationModificationsAllowed) ? true : false;
			this.modifySegmentByRoute = (modificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_ROUTE)
					&& reservationModificationsAllowed) ? true : false;

			this.cancelSegment = (modificationParams.contains(ModifcationParamTypes.CANCEL_SEGMENT)
					&& reservationModificationsAllowed) ? true : false;
			this.addInfant = (modificationParams.contains(ModifcationParamTypes.ADD_INFANT) && reservationModificationsAllowed)
					? true
					: false;
			this.transferOwnership = (modificationParams.contains(ModifcationParamTypes.TRANSFER_OWNERSHIP)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.transferOwnershipToAnyCarrier = (modificationParams
					.contains(ModifcationParamTypes.TRANSFER_OWNERSHIP_TO_ANY_CARRIER)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			groundServiceEnabled = modificationParams.contains(ModifcationParamTypes.GROUND_SERVICE_ENABLED) ? true : false;
			// this.addSegment = (modificationParams.contains(ModifcationParamTypes.ADD_SEGMENT))?true:false;
			this.viewItinerary = true;

			if (status != null && status.equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				// do nothing, use the privilege as it is
				this.cancelReservation = false;
				this.cancelledReservation = true;
				this.nameChange = false;
				if (!modificationParams.contains(ModifcationParamTypes.VIEW_CNX_ITEN)) {
					this.viewItinerary = false;
				}
				// if(!modifyCnxResAllowed){
				// this.addSegment = false;
				// }
			} else {
				// if this is not a cancel reservation modifyCnxResAllowed privilage can consider as true
				this.modifyCnxResAllowed = true;
			}

			if (status != null && status.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				if (!modificationParams.contains(ModifcationParamTypes.PRIVI_ONHOLD)) {
					this.viewItinerary = false;
				}
			}

			if (modificationParams.contains(ModifcationParamTypes.VIEW_ITN_WITH_PAYMENT_INFO)) {
				this.viewItineraryWithPaymentInfo = true;
			} else {
				this.viewItineraryWithPaymentInfo = false;
			}
			if (modificationParams.contains(ModifcationParamTypes.VIEW_ITN_WITHOUT_PAX_CHARGES)) {
				this.viewItineraryWithoutPaxCharges = true;
			} else {
				this.viewItineraryWithoutPaxCharges = false;
			}
			if (modificationParams.contains(ModifcationParamTypes.VIEW_ITN_WITH_CHARGES)) {
				this.viewItineraryWithCharges = true;
			} else {
				this.viewItineraryWithCharges = false;
			}

			if (resSegs != null) {
				checkBufferTimes(resSegs);
			}

			this.cancelReservation = false;
			if (!this.isFlownSegmentExists && !this.postDepature && !this.inBufferTime
					&& modificationParams.contains(ModifcationParamTypes.CANCEL_ALLOW) && reservationModificationsAllowed) {
				this.cancelReservation = true;
			}
			if (!this.isFlownSegmentExists && !this.postDepature && this.inBufferTime
					&& modificationParams.contains(ModifcationParamTypes.ALLOW_MODFIFY_BUFFERTIME_SEG_OR_PAX)) {
				this.cancelReservation = true;
			}
			if (!this.isFlownSegmentExists && this.postDepature && this.modifyFlown) {
				this.cancelReservation = true;
			}

			this.buffertimeModification = (modificationParams.contains(ModifcationParamTypes.BUFFER_TIME_MODIFICATION)
					&& reservationModificationsAllowed) ? true : false;
			this.fareRuleBufferTimeModification = (modificationParams.contains(ModifcationParamTypes.BUFFER_TIME_MODIFICATION)
					&& reservationModificationsAllowed) ? true : false;

			if (bulkTicketBooking) {
				this.splitPassenger = (modificationParams.contains(ModifcationParamTypes.SPLIT_BULK_RESERVATION)
						&& reservationModificationsAllowed) ? true : false;
				this.removePassenger = (modificationParams.contains(ModifcationParamTypes.BULK_TKT_REMOVE_PAX)
						&& reservationModificationsAllowed) ? true : false;
			} else {
				this.splitPassenger = (modificationParams.contains(ModifcationParamTypes.SPLIT_RESERVATION)
						&& reservationModificationsAllowed) ? true : false;
				this.removePassenger = (modificationParams.contains(ModifcationParamTypes.REMOVE_PAX)
						&& reservationModificationsAllowed) ? true : false;
			}

			this.extendOnhold = (modificationParams.contains(ModifcationParamTypes.EXTEND_ONHOLD)
					&& reservationModificationsAllowed) ? true : false;

			if (!this.postDepature && this.inBufferTime
					&& !(!bulkTicketBooking
							&& modificationParams.contains(ModifcationParamTypes.ALLOW_MODFIFY_BUFFERTIME_SEG_OR_PAX)
							|| bulkTicketBooking && modificationParams.contains(ModifcationParamTypes.BULK_TKT_REMOVE_PAX))) {
				this.removePassenger = false;
			}
			this.groupChargeAdjustment = (modificationParams.contains(ModifcationParamTypes.MULTIPLE_CHARGE_ADJUSTMENT)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.addUserNotes = (modificationParams.contains(ModifcationParamTypes.ADD_USER_NOTES)) ? true : false;
			this.nameChange = (modificationParams
					.contains(ModifcationParamTypes.ALLOW_NAME_CHANGE) /*
																		 * && reservationModificationsAllowed
																		 */) ? true : false;
			this.onholdNameChange = (modificationParams.contains(ModifcationParamTypes.ONHOLD_NAME_CHANGE)) ? true : false;

			this.tbaNameChange = (modificationParams.contains(ModifcationParamTypes.ALLOW_TBA_NAME_CHANGE)
					&& reservationModificationsAllowed) ? true : false;
			this.BufferTimeNameChange = (modificationParams.contains(ModifcationParamTypes.ALLOW_BUFFERTIME_NAME_CHANGE)
					&& reservationModificationsAllowed) ? true : false;
			this.modifyContactDetails = (modificationParams.contains(ModifcationParamTypes.ALLOW_MODIFY_CONTACT_DETAILS)
					&& reservationModificationsAllowed) ? true : false;
			this.viewReservationAuditAllowed = (modificationParams.contains(ModifcationParamTypes.VIEW_RESERVATION_HISTORY))
					? true
					: false;
			this.viewReservationHistoryForOtherCarriers = (modificationParams
					.contains(ModifcationParamTypes.VIEW_RESERVATION_HISTORY_OTHER_CARRIERS)) ? true : false;
			/* payment related params */
			this.partialPaymentModify = (modificationParams.contains(ModifcationParamTypes.PARTIAL_PAYMENT_MODIFY)
					&& reservationModificationsAllowed) ? true : false;
			this.modifyOnholdConfirm = (modificationParams.contains(ModifcationParamTypes.MODIFY_ONHOLD_CONFIM)
					&& reservationModificationsAllowed) ? true : false;
			this.cashPayments = (modificationParams.contains(ModifcationParamTypes.CASH_PAYMENTS)) ? true : false;
			this.offlinePayments = (modificationParams.contains(ModifcationParamTypes.OFFLINE_PAYMENTS)) ? true : false;
			this.creditPayments = (modificationParams.contains(ModifcationParamTypes.CREDIT_CARD_PAYMENTS)) ? true : false;
			this.voucherPayments = (modificationParams.contains(ModifcationParamTypes.VOUCHER_PAYMENTS)) ? true : false;
			this.onAccountPayments = (modificationParams.contains(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS)
					|| modificationParams.contains(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS_ANY)) ? true : false;

			// this.onAccountMultiAgentPayments = (this.onAccountPayments && modificationParams
			// .contains(ModifcationParamTypes.MULTIPLE_AGENT_ON_ACCOUNT_PAYMENTS)) ? true : false;

			this.onAccountMultiAgentPayments = false;

			if (modificationParams.contains(ModifcationParamTypes.BSP_PAYMENTS)) {
				this.bspPayments = this.bspPayments && true;
				this.bspPaymentsForAlterRes = this.bspPaymentsForAlterRes && true;

			}

			this.onAccountPaymentsReporting = (modificationParams.contains(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS_REPORTING))
					? true
					: false;
			this.forceConfirm = (modificationParams.contains(ModifcationParamTypes.FORCE_CONFIRM)) ? true : false;
			this.loadProfile = (modificationParams.contains(ModifcationParamTypes.LOAD_PROFILE)) ? true : false;
			this.allowCapturePayRef = (modificationParams.contains(ModifcationParamTypes.ALLOW_CAPTURE_PAY_REFERENCE))
					? true
					: false;
			this.allowNamedCreditTransfer = (modificationParams.contains(ModifcationParamTypes.ALLOW_NAMED_CREDIT_TRANSFER))
					? true
					: false;
			this.allowAnyCreditTransfer = (modificationParams.contains(ModifcationParamTypes.ALLOW_ANY_CREDIT_TRANSFER))
					? true
					: false;
			/* payment related params */
			/* charges related info */
			this.adjustCharges = (modificationParams.contains(ModifcationParamTypes.ADJUST_CHARGES)
					&& ((this.modifyCnxResAllowed && (reservationModificationsAllowed
							|| (!reservationModificationsAllowed && this.allowOnlySplitChgAdjustRefund))))) ? true : false;
			this.refundableChargeAdjust = (modificationParams.contains(ModifcationParamTypes.REFUNDABLE_CHARGE_ADJUST)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowPaxCreditExpiryDateChange = (modificationParams
					.contains(ModifcationParamTypes.ALLOW_PAX_CREDIT_EXP_DATE_CHANGE)) ? true : false;
			this.allowReinstatePassengerCredits = (modificationParams.contains(ModifcationParamTypes.ALLOW_REINSTATE_PAX_CREDITS)
					&& reservationModificationsAllowed) ? true : false;
			this.adjustOtherCarrierCharges = (modificationParams.contains(ModifcationParamTypes.ADJUST_OTHER_CARRIER_CHARGES)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.nonRefundableChargeAdjust = (modificationParams.contains(ModifcationParamTypes.NON_REFFUNDABLE_CHARGE_ADJUST)
					&& (reservationModificationsAllowed || this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowRefundNoCredit = (modificationParams.contains(ModifcationParamTypes.ALLOW_REFUND_NO_CREDIT)) ? true : false;
			this.allowCashRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_CASH_REFUND)
					&& (this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowOnAccountRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_ON_ACCOUNT_REFUND)
					&& (this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowBspRefund = modificationParams.contains(ModifcationParamTypes.ALLOW_BSP_REFUND);
			this.allowOnAccountRptRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_ON_ACCOUNT_RPT_REFUND))
					? true
					: false;
			this.allowAnyCarrierRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_ANY_CARRIER_REFUND)
					&& (this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowAnyOpCarrierOnAccountRefund = (modificationParams
					.contains(ModifcationParamTypes.ALLOW_ANY_OPCARRIER_ON_ACC_REFUND)) ? true : false;
			this.allowCreditCardRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_CREDIT_CARD_REFUND)
					&& (this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.allowAnyCreditCardRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_ANY_CREDIT_CARD_REFUND)
					&& (this.allowOnlySplitChgAdjustRefund)) ? true : false;
			this.bufferTimeModAccountAllowed = (modificationParams.contains(ModifcationParamTypes.BUFFERTIME_MOD_ACCOUNT_ALLOWED)
					&& reservationModificationsAllowed) ? true : false;
			this.viewExternalPayments = (modificationParams.contains(ModifcationParamTypes.VIEW_EXTERNAL_PAYMENTS))
					? true
					: false;
			this.viewCredit = (modificationParams.contains(ModifcationParamTypes.VIEW_CREDIT)) ? true : false;
			this.allowRefund = (modificationParams.contains(ModifcationParamTypes.ALLOW_REFUND)) ? true : false;
			/* charges related info */
			this.allowLCCAnyOndModify = (modificationParams.contains(ModifcationParamTypes.ALLOW_LCC_ANY_OND_MODIFY)
					&& reservationModificationsAllowed) ? true : false;
			this.printItineraryForPartialPayment = (modificationParams
					.contains(ModifcationParamTypes.PRINT_ITN_FOR_PARTIAL_PAYMENTS)) ? true : false;
			this.clearAlert = (modificationParams.contains(ModifcationParamTypes.CLEAR_ALERT) && reservationModificationsAllowed)
					? true
					: false;
            if(isTransferable){
			this.transferAlert = (modificationParams.contains(ModifcationParamTypes.TRANSFER_ALERT)
					&& reservationModificationsAllowed) ? true : false;
            } else{
                this.transferAlert = isTransferable;
            }
			this.hasOverrideCnxModChargePrivilege = (modificationParams
					.contains(ModifcationParamTypes.HAS_OVERRIDE_CNX_MOD_CHARGE)) ? true : false;
			this.blnShowChargeTypeOveride = (modificationParams.contains(ModifcationParamTypes.SHOW_CHARGE_TYPE_OVERRIDE))
					? true
					: false;
			this.overideFare = (modificationParams.contains(ModifcationParamTypes.CHANGE_FARE_ANY)
					|| modificationParams.contains(ModifcationParamTypes.OVERRIDE_AGENT_FARE_ONLY)
					|| modificationParams.contains(ModifcationParamTypes.OVERRIDE_VISIBLE_FARE)) ? true : false;
			this.allowOverrideAgentFareOnly = (modificationParams.contains(ModifcationParamTypes.OVERRIDE_AGENT_FARE_ONLY))
					? true
					: false;

			this.allowViewPublicOnewayFaresForReturn = (modificationParams
					.contains(ModifcationParamTypes.VIEW_PUBLIC_ONEWAY_FOR_RETURNS)) ? true : false;

			this.allowOverrideAnyFare = (modificationParams.contains(ModifcationParamTypes.CHANGE_FARE_ANY)) ? true : false;
			this.allowOverrideVisibleFare = (modificationParams.contains(ModifcationParamTypes.OVERRIDE_VISIBLE_FARE))
					? true
					: false;
			this.onHold = (modificationParams.contains(ModifcationParamTypes.ALLOW_ON_HOLD)
					|| modificationParams.contains(ModifcationParamTypes.ALLOW_ON_HOLD_BUFFER_TIME)) ? true : false;
			this.seatBlock = (modificationParams.contains(ModifcationParamTypes.ALLOW_BLOCK_SEATS)) ? true : false;
			this.multiMealEnabled = (modificationParams.contains(ModifcationParamTypes.MULTI_MEAL_ENABLED)) ? true : false;
			this.endorsementsAllowed = false; // TODO : enable through the interline flow
			this.countryPaxConfigEnabled = false; // TODO : not enabled through the interline yet
			this.printPNRHistoryAllow = false; // TODO : nor enable through the interline yet
			this.itineraryFareMaskPrivilege = false;// TODO : enable through the interline flow
			this.itineraryFareMaskModifyPrivilege = false;// TODO : enable through the interline flow
			
			this.duplicateNameCheckEnabled = (modificationParams.contains(ModifcationParamTypes.DUPLICATE_NAME_CHECK_ENABLED)) ? true : false;
			
			this.duplicateNameSkipPrivilege = (modificationParams.contains(ModifcationParamTypes.DUPLICATE_NAME_SKIP)) ? true : false;
			
			this.duplicateNameSkipModifyPrivilege = (modificationParams.contains(ModifcationParamTypes.DUPLICATE_NAME_SKIP_MODIFY)) ? true : false;
			
			this.fltStopOverDurInfoToolTipEnabled = false;// TODO : enable through the interline flow

			this.duplicateNameCheckEnabled = (modificationParams.contains(ModifcationParamTypes.DUPLICATE_NAME_CHECK_ENABLED))
					? true
					: false;

			this.duplicateNameSkipPrivilege = (modificationParams.contains(ModifcationParamTypes.DUPLICATE_NAME_SKIP))
					? true
					: false;

			this.duplicateNameSkipModifyPrivilege = (modificationParams
					.contains(ModifcationParamTypes.DUPLICATE_NAME_SKIP_MODIFY)) ? true : false;

			this.fltStopOverDurInfoToolTipEnabled = false;// TODO : enable through the interline flow
			this.reQuoteOnlyRequiredONDs = AppSysParamsUtil.isReQuoteOnlyRequiredONDs(); // TODO : enable through the
																							// interline flow
			this.enforceSameOrHigherFare = AppSysParamsUtil.isEnforceSameHigher();

			this.allowMultiCitySearch = AppSysParamsUtil.isOpenJawMultiCitySearchEnabled();

			this.baggageMandatory = (modificationParams.contains(ModifcationParamTypes.BAGGAGE_MANDATORY_ENABLED)) ? true : false;

			this.allowChangePassengerCouponStatus = modificationParams
					.contains(ModifcationParamTypes.CHANGE_PASSENGER_COUPON_STATUS) && reservationModificationsAllowed;

			this.allowExceptionalEticketCouponModifications = modificationParams.contains(
					ModifcationParamTypes.ALLOW_EXCEPTIONAL_ETICKET_COUPON_MODIFICATIONS) && reservationModificationsAllowed;

			this.allowOverrideSameFare = (modificationParams.contains(ModifcationParamTypes.OVERRIDE_SAME_FARE_MODIFICATION))
					? true
					: false;

			this.allowPassengerDetailCSVUpload = (modificationParams.contains(ModifcationParamTypes.ALLOW_CSV_DETAIL_UPLOAD))
					? true
					: false;

			if (AppSysParamsUtil.applyExtendedNameModificationFunctionality()) {

				this.allowExtendedPassengerNameModificationWithinBuffer = (modificationParams
						.contains(ModifcationParamTypes.MODIFY_EXTENDED_NAME_WITHIN_BUFFER)) ? true : false;

				this.allowCreditUtilizedPassengerNameModification = (modificationParams
						.contains(ModifcationParamTypes.MODIFY_CREDIT_UTILIZED_PAX_NAME)) ? true : false;

				this.allowPassengerNameModificationHavingDepatedSegments = (modificationParams
						.contains(ModifcationParamTypes.MODIFY_PAX_NAME_HAVING_DEPARTED_SEGMENTS)) ? true : false;

				this.allowExtendedPassengerNameModification = (modificationParams
						.contains(ModifcationParamTypes.MODIFY_EXTENDED_NAME)) ? true : false;

				if (allowExtendedPassengerNameModificationWithinBuffer || allowCreditUtilizedPassengerNameModification
						|| allowExtendedPassengerNameModification || allowPassengerNameModificationHavingDepatedSegments) {
					this.nameChange = true;
					this.allowExtendedNameChange = true;
				}
			}

			this.allowPassengerUserNoteModificationHavingDepartedSegments = (modificationParams
					.contains(ModifcationParamTypes.MODIFY_USER_NOTE_HAVING_DEPARTED_SEGMENTS)) ? true : false;

			this.allowExtendedPassengerNameModification = (modificationParams
					.contains(ModifcationParamTypes.MODIFY_EXTENDED_NAME)) ? true : false;

			if (allowExtendedPassengerNameModificationWithinBuffer || allowExtendedPassengerNameModification
					|| allowExtendedPassengerNameModification) {
				this.nameChange = true;
			}

			if (allowPassengerUserNoteModificationHavingDepartedSegments) {
				this.addUserNotes = true;
			}

			this.allowCreateUserAlert = modificationParams.contains(ModifcationParamTypes.ALLOW_CREATE_USER_ALERT) ? true : false;
			overrideModificationOperations(lccPromotionInfoTO, resSegs);

			this.allowMultiCitySearch = (modificationParams.contains(ModifcationParamTypes.ALLOW_MULTICITY_SEARCH))
					? true
					: false;

			this.enableFareRuleNCC = modificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_ENABLED) ? true : false;
			this.enableFareRuleNCCForFlown = modificationParams.contains(ModifcationParamTypes.FARE_RULE_NCC_FOR_FLOWN_ENABLED)
					? true
					: false;
			this.nameChangeMaxCount = AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.XBE);
			this.allowExternalTicketExchange = false;// TODO : enable through the interline flow

			this.allowOverrideRequoteNameChange = (modificationParams
					.contains(ModifcationParamTypes.ALLOW_OVERRIDE_REQUOTE_NAME_CHANGE) && this.allowOverrideRequoteNameChange)
							? true
							: false;

			this.allowAddUserNotes = modificationParams.contains(ModifcationParamTypes.ADD_USR_NOTE) ? true : false;
			this.allowClassifyUserNotes = modificationParams.contains(ModifcationParamTypes.CLASSIFY_USR_NOTE) ? true : false;
		}
	}

	private void checkBufferTimes(Collection<LCCClientReservationSegment> resSegs) {
		long sysTime = new Date().getTime();
		long firstDeparture = 0;
		for (LCCClientReservationSegment seg : resSegs) {
			long segCancelBufferStart = seg.getCancelTillBufferDateTime().getTime();
			long segBufferEnd = seg.getModifyTillFlightClosureDateTime().getTime();
			long departuretime = seg.getDepartureDate().getTime();
			if (!seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				if (firstDeparture == 0 || firstDeparture > departuretime) {
					firstDeparture = departuretime;
				}

				// Removed checking modification param coz inBufferTime is used only in CNX scenarios
				if (sysTime > segCancelBufferStart) {
					this.inBufferTime = true;
				}
				if (sysTime > segBufferEnd) {
					this.postDepature = true;
				}
				if (sysTime > firstDeparture) {
					this.postDepature = true;
				}

				if (seg.isFlownSegment()) {
					this.isFlownSegmentExists = true;
				}
				if (seg.isAllPaxNoShow()) {
					this.allPaxNoshowInTheSegment = true;
				}
				if (seg.isUnSegment()) {
					this.isUnSegmentExists = true;
				} else {
					this.onlyUNSegmentExist = false;
				}
			}
		}
	}

	private boolean isBulkTicketReservation(Collection<LCCClientReservationSegment> resSegs) {
		if (resSegs != null) {
			for (LCCClientReservationSegment seg : resSegs) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus()) && seg != null
						&& seg.getFareTO() != null && seg.getFareTO().isBulkTicketFareRule()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isReservationTotallyFlownOrCancelled(Collection<LCCClientReservationSegment> resSegs) {

		boolean totalFlownOrCancelled = false;

		if (resSegs != null && !resSegs.isEmpty()) {
			boolean flownAll = true;
			int flownSegCount = 0;
			int cnxSegCount = 0;
			int totalSegCount = resSegs.size();

			for (LCCClientReservationSegment seg : resSegs) {
				if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					cnxSegCount++;
				} else {
					flownAll = flownAll && seg.isFlownSegment();
					flownSegCount++;
				}
			}

			totalFlownOrCancelled = (totalSegCount == cnxSegCount) || (flownSegCount > 0 && flownAll);
		}

		return totalFlownOrCancelled;
	}

	private void overrideModificationOperations(LCCPromotionInfoTO lccPromotionInfoTO,
			Collection<LCCClientReservationSegment> resSegs) {
		if (lccPromotionInfoTO != null) {
			if (!lccPromotionInfoTO.isModificationAllowed()) {
				this.modifyCnxResAllowed = false;
				this.modifyFlown = false;
				this.modifyNoshow = false;
				this.modifySegmentByDate = false;
				this.modifySegmentByRoute = false;
			}

			if (!lccPromotionInfoTO.isCancellationAllowed()) {
				this.cancelSegment = false;
			}

			if (!lccPromotionInfoTO.isSplitAllowed()) {
				this.splitPassenger = false;
			}

			if (!lccPromotionInfoTO.isRemovePaxAllowed()) {
				this.removePassenger = false;
			}

			// In Re-quote tab consequence modification will requires to check segment level values.
			if (resSegs != null && !resSegs.isEmpty()) {
				for (LCCClientReservationSegment seg : resSegs) {
					if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(seg.getStatus())) {
						seg.setModifible(modifyCnxResAllowed && seg.isModifible());
						seg.setModifyByDate(modifySegmentByDate && seg.isModifyByDate());
						seg.setModifyByRoute(modifySegmentByRoute && seg.isModifyByRoute());
						seg.setCancellable(cancelSegment && seg.isCancellable());
					}
				}
			}

		}
	}

	/**
	 * Adds the own airline charge adjustment privileges to the {@link ChargeAdjustPrivilegeContainer}.
	 * 
	 * @param request
	 *            : the {@link HttpServletRequest}.
	 */
	private void addOwnAirlineChargeAdjustmentTypePrivileges(HttpServletRequest request) {

		List<ChargeAdjustmentPrivilege> ownAirlineChargeAdjustmentPrivileges = new ArrayList<ChargeAdjustmentPrivilege>();

		List<ChargeAdjustmentTypeDTO> ownAirlineTypes = CommonsServices.getGlobalConfig().getChargeAdjustmentTypes();
		for (ChargeAdjustmentTypeDTO ownAirLineAdjustmentType : ownAirlineTypes) {
			ChargeAdjustmentPrivilege privilege = new ChargeAdjustmentPrivilege();
			privilege.setChargeAdjustmentTypeId(ownAirLineAdjustmentType.getChargeAdjustmentTypeId());

			if (BasicRH.hasPrivilege(request, ownAirLineAdjustmentType.getRefundablePrivilegeId())) {
				privilege.setRefundable(true);
			} else {
				privilege.setRefundable(false);
			}

			if (BasicRH.hasPrivilege(request, ownAirLineAdjustmentType.getNonRefundablePrivilegeId())) {
				privilege.setNonRefundable(true);
			} else {
				privilege.setNonRefundable(false);
			}
			ownAirlineChargeAdjustmentPrivileges.add(privilege);
		}

		this.getChargeAdjustPrivileges().getCarrierWiseChargeAdjustPrivileges().put(AppSysParamsUtil.getDefaultCarrierCode(),
				ownAirlineChargeAdjustmentPrivileges);
		this.setPromoCodeEnabled(AppSysParamsUtil.isPromoCodeEnabled());
	}

	/**
	 * Adds the interline charge adjustment privileges to the {@link ChargeAdjustPrivilegeContainer}.
	 * 
	 * @param chargeAdjustmentPrivileges
	 *            : the privileges to be added.
	 */
	public void addInterlineChargeAdjustmentPrivileges(Map<String, List<ChargeAdjustmentPrivilege>> chargeAdjustmentPrivileges) {
		if (chargeAdjustmentPrivileges != null && chargeAdjustmentPrivileges.size() > 0) {
			for (String carrierCode : chargeAdjustmentPrivileges.keySet()) {
				this.getChargeAdjustPrivileges().getCarrierWiseChargeAdjustPrivileges().put(carrierCode,
						chargeAdjustmentPrivileges.get(carrierCode));
			}
		}
	}

	private void setGoquoPrivilage() {
		if (!goquoValid) {
			cancelReservation = false;
			viewItineraryWithPaymentInfo = false;
			viewItineraryWithoutPaxCharges = true;
			viewItineraryWithCharges = false;
			buffertimeModification = false;
			fareRuleBufferTimeModification = false;
			splitPassenger = false;
			extendOnhold = false;
			removePassenger = false;
			groupChargeAdjustment = false;
			addUserNotes = false;
			nameChange = false;
			onholdNameChange = false;
			tbaNameChange = false;
			modifyContactDetails = false;
			partialPaymentModify = false;
			modifyOnholdConfirm = false;
			cashPayments = false;
			creditPayments = false;
			onAccountPayments = false;
			onAccountPaymentsReporting = false;
			forceConfirm = false;
			loadProfile = false;
			allowNamedCreditTransfer = false;
			allowAnyCreditTransfer = false;
			adjustCharges = false;
			refundableChargeAdjust = false;
			allowPaxCreditExpiryDateChange = false;
			allowReinstatePassengerCredits = false;
			adjustOtherCarrierCharges = false;
			clearAlert = false;
			transferAlert = false;
			allowAnyOpCarrierOnAccountRefund = false;
			allowCreditCardRefund = false;
			allowAnyCreditCardRefund = false;
			allowAnyOnAccountRefund = false;
			allowLCCAnyOndModify = false;
			viewAvailableCredit = false;
			viewReservationAuditAllowed = false;
			viewReservationDetailsAllowed = false;
			viewPassengerAccountAllowed = false;
			bufferTimeModAccountAllowed = false;
			allowCOSSearch = false;
			customizedItineraryView = false;
			allowRecieptPrinting = false;
			printItineraryForPartialPayment = false;
			viewExternalPayments = false;
			viewCredit = false;
			allowRefund = false;
			itineraryFareMaskPrivilege = false;
			itineraryFareMaskModifyPrivilege = false;
			printPNRHistoryAllow = false;
			allowOnholdRollForward = false;
			overrideNameChangeCharge = false;
			tbaGroupNameUpload = false;
			duplicateNameSkipModifyPrivilege = false;
			allowDisplayPaxNames = false;
			allowVoidReservation = false;
			allowExcludeCharges = false;
			allowExceptionalEticketCouponModifications = false;
			allowViewAllBCFares = false;
			allowModifyToAnyBCWithinSameCOS = false;
			allowOverrideSameOrHigherFare = false;
			allowOverrideSameFare = false;
			allowPassengerDetailCSVUpload = false;
			allowCreateUserAlert = false;
			allowExtendedPassengerNameModificationWithinBuffer = false;
			allowCreditUtilizedPassengerNameModification = false;
			allowExtendedPassengerNameModification = false;
			agentCommissionEnabled = false;
			createNewPnrFromOld = false;
			requoteEnabled = false;
			addInfant = false;
		}
	}

	public boolean isAllowAnyCarrierRefund() {
		return allowAnyCarrierRefund;
	}

	public void setAllowAnyCarrierRefund(boolean allowAnyCarrierRefund) {
		this.allowAnyCarrierRefund = allowAnyCarrierRefund;
	}

	public String getDefaultCurrencyCode() {
		return defaultCurrencyCode;
	}

	public boolean isAllowExceptionalEticketCouponModifications() {
		return allowExceptionalEticketCouponModifications;
	}

	public void setAllowExceptionalEticketCouponModifications(boolean allowExceptionalEticketCouponModifications) {
		this.allowExceptionalEticketCouponModifications = allowExceptionalEticketCouponModifications;
	}

	/**
	 * @return the baseCurrencyCode
	 */
	public String getBaseCurrencyCode() {
		return baseCurrencyCode;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public boolean isOnholdNameChange() {
		return onholdNameChange;
	}

	public void setOnholdNameChange(boolean onholdNameChange) {
		this.onholdNameChange = onholdNameChange;
	}

	/**
	 * @return the reservationModificationsAllowed
	 */
	public boolean isReservationModificationsAllowed() {
		return reservationModificationsAllowed;
	}

	/**
	 * @param reservationModificationsAllowed
	 *            the reservationModificationsAllowed to set
	 */
	public void setReservationModificationsAllowed(boolean reservationModificationsAllowed) {
		this.reservationModificationsAllowed = reservationModificationsAllowed;
	}

	public boolean isOverideFare() {
		return overideFare;
	}

	public boolean isStandByBooking() {
		return standByBooking;
	}

	public boolean isAddSSR() {
		return addSSR;
	}

	public boolean isSeatBlock() {
		return seatBlock;
	}

	public boolean isForceConfirm() {
		return forceConfirm;
	}

	public boolean isLoadProfile() {
		return loadProfile;
	}

	public boolean isTbaBookings() {
		return tbaBookings;
	}

	public boolean isBuffertimeModification() {
		return buffertimeModification;
	}

	public boolean isOnHold() {
		return onHold;
	}

	public boolean isAddInsurance() {
		return addInsurance;
	}

	public boolean isCashPayments() {
		return cashPayments;
	}

	public boolean isBspPayments() {
		return bspPayments;
	}

	public boolean isCreditPayments() {
		return creditPayments;
	}

	public boolean isOnAccountPayments() {
		return onAccountPayments;
	}

	public boolean isOnAccountMultiAgentPayments() {
		return onAccountMultiAgentPayments;
	}

	public boolean isOnAccountPaymentsReporting() {
		return onAccountPaymentsReporting;
	}

	public boolean isNameChange() {
		return nameChange;
	}

	public boolean isPrintItineraryForPartialPayment() {
		return printItineraryForPartialPayment;
	}

	/**
	 * @return the groupChargeAdjustment
	 */
	public boolean isGroupChargeAdjustment() {
		return groupChargeAdjustment;
	}

	/**
	 * @param groupChargeAdjustment
	 *            the groupChargeAdjustment to set
	 */
	public void setGroupChargeAdjustment(boolean groupChargeAdjustment) {
		this.groupChargeAdjustment = groupChargeAdjustment;
	}

	public boolean isRemovePassenger() {
		return removePassenger;
	}

	public boolean isSplitPassenger() {
		return splitPassenger;
	}

	public boolean isAddInfant() {
		return addInfant;
	}

	public void setAddInfant(boolean addInfant) {
		this.addInfant = addInfant;
	}

	public boolean isAddUserNotes() {
		return addUserNotes;
	}

	/**
	 * @return the gdsReservationModAllowed
	 */
	public boolean isGdsReservationModAllowed() {
		return gdsReservationModAllowed;
	}

	/**
	 * @param gdsReservationModAllowed
	 *            the gdsReservationModAllowed to set
	 */
	public void setGdsReservationModAllowed(boolean gdsReservationModAllowed) {
		this.gdsReservationModAllowed = gdsReservationModAllowed;
	}

	public boolean isCancelReservation() {
		return cancelReservation;
	}

	public boolean isCurrencyManupulation() {
		return currencyManupulation;
	}

	public boolean isAccessAnyBooking() {
		return accessAnyBooking;
	}

	public boolean isAccessAPIBookings() {
		return accessAPIBookings;
	}

	public boolean isAccessHolidaysBookings() {
		return accessHolidaysBookings;
	}

	public boolean isAccessXBEBookings() {
		return accessXBEBookings;
	}

	public boolean isAccessIBEBookings() {
		return accessIBEBookings;
	}

	public boolean isPartialPayments() {
		return partialPayments;
	}

	public boolean isOnholdInsurance() {
		return onholdInsurance;
	}

	public boolean isAdjustCharges() {
		return adjustCharges;
	}

	public boolean isAdjustOtherCarrierCharges() {
		return adjustOtherCarrierCharges;
	}

	public boolean isRefundableChargeAdjust() {
		return refundableChargeAdjust;
	}

	public boolean isNonRefundableChargeAdjust() {
		return nonRefundableChargeAdjust;
	}

	public boolean isViewReservationHistoryForOtherCarriers() {
		return viewReservationHistoryForOtherCarriers;
	}

	// public boolean isModifySegment() {
	// return modifySegment;
	// }
	public boolean isCancelSegment() {
		return cancelSegment;
	}

	public boolean isBufferTimeOnHold() {
		return bufferTimeOnHold;
	}

	public boolean isTbaNameChange() {
		return tbaNameChange;
	}

	public boolean isPostDepature() {
		return postDepature;
	}

	public boolean isInBufferTime() {
		return inBufferTime;
	}

	public boolean isBufferTimeNameChange() {
		return BufferTimeNameChange;
	}

	public boolean isViewItinerary() {
		return viewItinerary;
	}

	public boolean isNotTransfered() {
		return notTransfered;
	}

	public void setNotTransfered(boolean notTransfered) {
		this.notTransfered = notTransfered;
	}

	public boolean isClearAlert() {
		return clearAlert;
	}

	public void setClearAlert(boolean clearAlert) {
		this.clearAlert = clearAlert;
	}

	public boolean isTransferAlert() {
		return transferAlert;
	}

	public void setTransferAlert(boolean transferAlert) {
		this.transferAlert = transferAlert;
	}

	public boolean isModifyCnxResAllowed() {
		return modifyCnxResAllowed;
	}

	public boolean isModifyFlown() {
		return modifyFlown;
	}

	public boolean isTriggerExternalSearch() {
		return triggerExternalSearch;
	}

	public void setTriggerExternalSearch(boolean triggerExternalSearch) {
		this.triggerExternalSearch = triggerExternalSearch;
	}

	public boolean isCancelledReservation() {
		return cancelledReservation;
	}

	public boolean isPartialPaymentModify() {
		return partialPaymentModify;
	}

	public boolean isModifyOnholdConfirm() {
		return modifyOnholdConfirm;
	}

	public boolean isAllowRefundNoCredit() {
		return allowRefundNoCredit;
	}

	public boolean isAllowCashRefund() {
		return allowCashRefund;
	}

	public boolean isAllowOnAccountRefund() {
		return allowOnAccountRefund;
	}

	public boolean isAllowCreditCardRefund() {
		return allowCreditCardRefund;
	}

	public boolean isAllowAnyOnAccountRefund() {
		return allowAnyOnAccountRefund;
	}

	public boolean isAllowAnyCreditCardRefund() {
		return allowAnyCreditCardRefund;
	}

	public boolean isAllowBspRefund() {
		return allowBspRefund;
	}

	public boolean isOverrrideOnHoldRestricted() {
		return overrideOnHoldRestricted;
	}

	public boolean isViewAvailableCredit() {
		return viewAvailableCredit;
	}

	public void setViewAvailableCredit(boolean viewAvailableCredit) {
		this.viewAvailableCredit = viewAvailableCredit;
	}

	public boolean isAllowPaxCreditExpiryDateChange() {
		return allowPaxCreditExpiryDateChange;
	}

	public boolean isAllowReinstatePassengerCredits() {
		return allowReinstatePassengerCredits;
	}

	public void setAllowPaxCreditExpiryDateChange(boolean allowPaxCreditExpiryDateChange) {
		this.allowPaxCreditExpiryDateChange = allowPaxCreditExpiryDateChange;
	}

	public void setAllowReinstatePassengerCredits(boolean allowReinstatePassengerCredits) {
		this.allowReinstatePassengerCredits = allowReinstatePassengerCredits;
	}

	public boolean isViewReservationAuditAllowed() {
		return viewReservationAuditAllowed;
	}

	public void setViewReservationAuditAllowed(boolean viewReservationAuditAllowed) {
		this.viewReservationAuditAllowed = viewReservationAuditAllowed;
	}

	public boolean isAllowNamedCreditTransfer() {
		return allowNamedCreditTransfer;
	}

	public void setAllowNamedCreditTransfer(boolean allowNamedCreditTransfer) {
		this.allowNamedCreditTransfer = allowNamedCreditTransfer;
	}

	public boolean isAllowAnyCreditTransfer() {
		return allowAnyCreditTransfer;
	}

	public void setAllowAnyCreditTransfer(boolean allowAnyCreditTransfer) {
		this.allowAnyCreditTransfer = allowAnyCreditTransfer;
	}

	public boolean isViewReservationDetailsAllowed() {
		return viewReservationDetailsAllowed;
	}

	public void setViewReservationDetailsAllowed(boolean viewReservationDetailsAllowed) {
		this.viewReservationDetailsAllowed = viewReservationDetailsAllowed;
	}

	public boolean isViewPassengerAccountAllowed() {
		return viewPassengerAccountAllowed;
	}

	public void setViewPassengerAccountAllowed(boolean viewPassengerAccountAllowed) {
		this.viewPassengerAccountAllowed = viewPassengerAccountAllowed;
	}

	public boolean isBufferTimeModAccountAllowed() {
		return bufferTimeModAccountAllowed;
	}

	public void setBufferTimeModAccountAllowed(boolean bufferTimeModAccountAllowed) {
		this.bufferTimeModAccountAllowed = bufferTimeModAccountAllowed;
	}

	public boolean isOpenReturnEnabled() {
		return openReturnEnabled;
	}

	public boolean isAllowCOSSearch() {
		return allowCOSSearch;
	}

	public void setAllowCOSSearch(boolean allowCOSSearch) {
		this.allowCOSSearch = allowCOSSearch;
	}

	public boolean isAllowLCCAnyOndModify() {
		return allowLCCAnyOndModify;
	}

	public void setAllowLCCAnyOndModify(boolean allowLCCAnyOndModify) {
		this.allowLCCAnyOndModify = allowLCCAnyOndModify;
	}

	public boolean isAllowCapturePayRef() {
		return allowCapturePayRef;
	}

	/**
	 * @return the transferOwnership
	 */
	public boolean isTransferOwnership() {
		return transferOwnership;
	}

	/**
	 * @return the transferOwnershipToAnyCarrier
	 */
	public boolean isTransferOwnershipToAnyCarrier() {
		return transferOwnershipToAnyCarrier;
	}

	/**
	 * @return the allowAnyOpCarrierOnAccountRefund
	 */
	public boolean isAllowAnyOpCarrierOnAccountRefund() {
		return allowAnyOpCarrierOnAccountRefund;
	}

	/**
	 * @return the overrideOnHoldRestricted
	 */
	public boolean isOverrideOnHoldRestricted() {
		return overrideOnHoldRestricted;
	}

	/**
	 * @return the allowOnAccountRptRefund
	 */
	public boolean isAllowOnAccountRptRefund() {
		return allowOnAccountRptRefund;
	}

	public boolean isViewItineraryWithCharges() {
		return viewItineraryWithCharges;
	}

	public boolean isViewItineraryWithPaymentInfo() {
		return viewItineraryWithPaymentInfo;
	}

	public boolean isViewItineraryWithoutPaxCharges() {
		return viewItineraryWithoutPaxCharges;
	}

	/**
	 * @return the groundServiceEnabled
	 */
	public boolean isGroundServiceEnabled() {
		return groundServiceEnabled && isAddSegment();
	}

	/**
	 * @param groundServiceEnabled
	 *            the groundServiceEnabled to set
	 */
	public void setGroundServiceEnabled(boolean groundServiceEnabled) {
		this.groundServiceEnabled = groundServiceEnabled;
	}

	/**
	 * @return the customizedItineraryView
	 */
	public boolean isCustomizedItineraryView() {
		return customizedItineraryView;
	}

	/**
	 * @param customizedItineraryView
	 *            the customizedItineraryView to set
	 */
	public void setCustomizedItineraryView(boolean customizedItineraryView) {
		this.customizedItineraryView = customizedItineraryView;
	}

	public boolean isAllowRecieptPrinting() {
		return allowRecieptPrinting;
	}

	public void setAllowRecieptPrinting(boolean allowRecieptPrinting) {
		this.allowRecieptPrinting = allowRecieptPrinting;
	}

	public boolean getModifySegmentByDate() {
		return modifySegmentByDate;
	}

	public void setModifySegmentByDate(boolean modifySegmentByDate) {
		this.modifySegmentByDate = modifySegmentByDate;
	}

	public boolean getModifySegmentByRoute() {
		return modifySegmentByRoute;
	}

	public void setModifySegmentByRoute(boolean modifySegmentByRoute) {
		this.modifySegmentByRoute = modifySegmentByRoute;
	}

	public boolean isDisplayContDetailsInPNRSummary() {
		return displayContDetailsInPNRSummary;
	}

	public void setDisplayContDetailsInPNRSummary(boolean displayContDetailsInPNRSummary) {
		this.displayContDetailsInPNRSummary = displayContDetailsInPNRSummary;
	}

	public void setExtendOnhold(boolean extendOnhold) {
		this.extendOnhold = extendOnhold;
	}

	public boolean isExtendOnhold() {
		return extendOnhold;
	}

	public boolean isChangeWaitListingPriority() {
		return changeWaitListingPriority;
	}

	public void setChangeWaitListingPriority(boolean changeWaitListingPriority) {
		this.changeWaitListingPriority = changeWaitListingPriority;
	}

	public void setModifyContactDetails(boolean modifyContactDetails) {
		this.modifyContactDetails = modifyContactDetails;
	}

	public boolean isModifyContactDetails() {
		return modifyContactDetails;
	}

	public void setviewExternalPayments(boolean viewExternalPayments) {
		this.viewExternalPayments = viewExternalPayments;
	}

	public boolean isViewExternalPayments() {
		return viewExternalPayments;
	}

	public void setViewCredit(boolean viewCredit) {
		this.viewCredit = viewCredit;
	}

	public boolean isViewCredit() {
		return viewCredit;
	}

	public void setaAllowRefund(boolean allowRefund) {
		this.allowRefund = allowRefund;
	}

	public boolean isAllowRefund() {
		return allowRefund;
	}

	public void setHasOverrideCnxModChargePrivilege(boolean hasOverrideCnxModChargePrivilege) {
		this.hasOverrideCnxModChargePrivilege = hasOverrideCnxModChargePrivilege;
	}

	public boolean isHasOverrideCnxModChargePrivilege() {
		return hasOverrideCnxModChargePrivilege;
	}

	public void setBlnShowChargeTypeOveride(boolean blnShowChargeTypeOveride) {
		this.blnShowChargeTypeOveride = blnShowChargeTypeOveride;
	}

	public boolean isBlnShowChargeTypeOveride() {
		return blnShowChargeTypeOveride;
	}

	public boolean isMultiMealEnabled() {
		return multiMealEnabled;
	}

	public void setMultiMealEnabled(boolean multiMealEnabled) {
		this.multiMealEnabled = multiMealEnabled;
	}

	public boolean isbCSelectionAtAvailabilitySearchEnabled() {
		return bCSelectionAtAvailabilitySearchEnabled;
	}

	public void setbCSelectionAtAvailabilitySearchEnabled(boolean bCSelectionAtAvailabilitySearchEnabled) {
		this.bCSelectionAtAvailabilitySearchEnabled = bCSelectionAtAvailabilitySearchEnabled;
	}

	/**
	 * @return : The charge adjustment privilge container.
	 */
	public ChargeAdjustPrivilegeContainer getChargeAdjustPrivileges() {
		return chargeAdjustPrivileges;
	}

	public boolean isEndorsementsAllowed() {
		return endorsementsAllowed;
	}

	public void setEndorsementsAllowed(boolean endorsementsAllowed) {
		this.endorsementsAllowed = endorsementsAllowed;
	}

	public boolean isCountryPaxConfigEnabled() {
		return countryPaxConfigEnabled;
	}

	public void setCountryPaxConfigEnabled(boolean countryPaxConfigEnabled) {
		this.countryPaxConfigEnabled = countryPaxConfigEnabled;
	}

	public boolean isItineraryFareMaskPrivilege() {
		return itineraryFareMaskPrivilege;
	}

	public void setItineraryFareMaskPrivilege(boolean itineraryFareMaskPrivilege) {
		this.itineraryFareMaskPrivilege = itineraryFareMaskPrivilege;
	}

	public boolean isItineraryFareMaskModifyPrivilege() {
		return itineraryFareMaskModifyPrivilege;
	}

	public void setItineraryFareMaskModifyPrivilege(boolean itineraryFareMaskModifyPrivilege) {
		this.itineraryFareMaskModifyPrivilege = itineraryFareMaskModifyPrivilege;
	}


	public boolean isOriginCountryOfCallPrivilege() {
		return originCountryOfCallPrivilege;
	}

	public void setOriginCountryOfCallPrivilege(boolean originCountryOfCallPrivilege) {
		this.originCountryOfCallPrivilege = originCountryOfCallPrivilege;
	}

	public boolean isOriginCountryModifyPrivilege() {
		return originCountryModifyPrivilege;
	}

	public void setOriginCountryModifyPrivilege(boolean originCountryModifyPrivilege) {
		this.originCountryModifyPrivilege = originCountryModifyPrivilege;
	}

	public void setPrintPNRHistoryAllow(boolean printPNRHistoryAllow) {
		this.printPNRHistoryAllow = printPNRHistoryAllow;
	}

	public boolean isPrintPNRHistoryAllow() {
		return printPNRHistoryAllow;
	}

	public boolean isFltStopOverDurInfoToolTipEnabled() {
		return fltStopOverDurInfoToolTipEnabled;
	}

	public void setFltStopOverDurInfoToolTipEnabled(boolean fltStopOverDurInfoToolTipEnabled) {
		this.fltStopOverDurInfoToolTipEnabled = fltStopOverDurInfoToolTipEnabled;
	}

	public void setAllowOverrideAnyFare(boolean allowOverrideAnyFare) {
		this.allowOverrideAnyFare = allowOverrideAnyFare;
	}

	public boolean isAllowOverrideAnyFare() {
		return allowOverrideAnyFare;
	}

	public void setAllowOverrideVisibleFare(boolean allowOverrideVisibleFare) {
		this.allowOverrideVisibleFare = allowOverrideVisibleFare;
	}

	public boolean isAllowOverrideVisibleFare() {
		return allowOverrideVisibleFare;
	}

	public void setAllowOverrideAgentFareOnly(boolean allowOverrideAgentFareOnly) {
		this.allowOverrideAgentFareOnly = allowOverrideAgentFareOnly;
	}

	public boolean isAllowOverrideAgentFareOnly() {
		return allowOverrideAgentFareOnly;
	}

	public void setVoidReservation(boolean voidReservation) {
		this.voidReservation = voidReservation;
	}

	public boolean isVoidReservation() {
		return voidReservation;
	}

	public boolean isAllowOnholdRollForward() {
		return allowOnholdRollForward;
	}

	public void setAllowOnholdRollForward(boolean allowOnholdRollForward) {
		this.allowOnholdRollForward = allowOnholdRollForward;
	}

	public boolean isDuplicateNameCheckEnabled() {
		return duplicateNameCheckEnabled;
	}

	public boolean isDuplicateNameSkipPrivilege() {
		return duplicateNameSkipPrivilege;
	}

	public boolean isDuplicateNameSkipModifyPrivilege() {
		return duplicateNameSkipModifyPrivilege;
	}

	public boolean isAllCoSChangeFareEnabled() {
		return allCoSChangeFareEnabled;
	}

	public boolean isBaggageMandatory() {
		return baggageMandatory;
	}

	public void setBaggageMandatory(boolean baggageMandatory) {
		this.baggageMandatory = baggageMandatory;
	}

	public boolean isAllowCountryPaxConfigOverride() {
		return allowCountryPaxConfigOverride;
	}

	public void setAllowCountryPaxConfigOverride(boolean allowCountryPaxConfigOverride) {
		this.allowCountryPaxConfigOverride = allowCountryPaxConfigOverride;
	}

	public boolean isRequoteEnabled() {
		return requoteEnabled;
	}

	public void setRequoteEnabled(boolean requoteEnabled) {
		this.requoteEnabled = requoteEnabled;
	}

	public boolean isAllowOverBook() {
		return allowOverBook;
	}

	public void setAllowOverBook(boolean allowOverBook) {
		this.allowOverBook = allowOverBook;
	}

	public boolean isRequoteAutoFQEnabled() {
		return requoteAutoFQEnabled;
	}

	public boolean isRequoteAutoAddOndFQEnabled() {
		return requoteAutoAddOndFQEnabled;
	}

	public boolean isRequoteAutoCnxOndFQEnabled() {
		return requoteAutoCnxOndFQEnabled;
	}

	public boolean isRequoteAutoFltSelectEnabled() {
		return requoteAutoFltSelectEnabled;
	}

	public boolean isRequoteSetBCForExistingSegments() {
		return requoteSetBCForExistingSegments;
	}

	public boolean isAllowDisplayPaxNames() {
		return allowDisplayPaxNames;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public boolean isAllowVoidReservation() {
		return allowVoidReservation;
	}

	public boolean isAllowOverrideSameFare() {
		return allowOverrideSameFare;
	}

	public void setConfirmButtonForOHDModification(boolean confirmButtonForOHDModification) {
		this.confirmButtonForOHDModification = confirmButtonForOHDModification;
	}

	public boolean isConfirmButtonForOHDModification() {
		return confirmButtonForOHDModification;
	}

	public boolean isAllowWaitListBooking() {
		return allowWaitListBooking;
	}

	public void setAllowWaitListBooking(boolean allowWaitListBooking) {
		this.allowWaitListBooking = allowWaitListBooking;
	}

	public boolean isOverrideNameChangeCharge() {
		return overrideNameChangeCharge;
	}

	public boolean isUserNotesMandatory() {
		return userNotesMandatory;
	}

	public boolean isTbaGroupNameUpload() {
		return tbaGroupNameUpload;
	}

	public void setTbaGroupNameUpload(boolean tbaGroupNameUpload) {
		this.tbaGroupNameUpload = tbaGroupNameUpload;
	}

	public boolean isAutoCancellationEnable() {
		return autoCancellationEnable;
	}

	public void setAutoCancellationEnable(boolean autoCancellationEnable) {
		this.autoCancellationEnable = autoCancellationEnable;
	}

	public boolean isAllowExcludeCharges() {
		return allowExcludeCharges;
	}

	public void setAllowExcludeCharges(boolean allowExcludeCharges) {
		this.allowExcludeCharges = allowExcludeCharges;
	}

	public boolean isAllowViewAllBCFares() {
		return allowViewAllBCFares;
	}

	public boolean isAllowChangePassengerCouponStatus() {
		return allowChangePassengerCouponStatus;
	}

	public void setAllowViewAllBCFares(boolean allowViewAllBCFares) {
		this.allowViewAllBCFares = allowViewAllBCFares;
	}

	public boolean isAllowModifyToAnyBCWithinSameCOS() {
		return allowModifyToAnyBCWithinSameCOS;
	}

	public boolean isAllowOverrideSameOrHigherFare() {
		return allowOverrideSameOrHigherFare;
	}

	public boolean isPromoCodeEnabled() {
		return promoCodeEnabled;
	}

	public void setPromoCodeEnabled(boolean promoCodeEnabled) {
		this.promoCodeEnabled = promoCodeEnabled;
	}

	public boolean isReQuoteOnlyRequiredONDs() {
		return reQuoteOnlyRequiredONDs;
	}

	public void setReQuoteOnlyRequiredONDs(boolean reQuoteOnlyRequiredONDs) {
		this.reQuoteOnlyRequiredONDs = reQuoteOnlyRequiredONDs;
	}

	public boolean isAllowKeepCommissionOnRefund() {
		return allowKeepCommissionOnRefund;
	}

	public boolean isFlownSegmentExists() {
		return isFlownSegmentExists;
	}

	public void setFlownSegmentExists(boolean isFlownSegmentExists) {
		this.isFlownSegmentExists = isFlownSegmentExists;
	}

	public boolean isAllowPassengerDetailCSVUpload() {
		return allowPassengerDetailCSVUpload;
	}

	public void setAllowPassengerDetailCSVUpload(boolean allowCSVPassengerDetailUpload) {
		this.allowPassengerDetailCSVUpload = allowCSVPassengerDetailUpload;
	}

	public boolean isAllowExtendedPassengerNameModificationWithinBuffer() {
		return allowExtendedPassengerNameModificationWithinBuffer;
	}

	public void
			setAllowExtendedPassengerNameModificationWithinBuffer(boolean allowExtendedPassengerNameModificationWithinBuffer) {
		this.allowExtendedPassengerNameModificationWithinBuffer = allowExtendedPassengerNameModificationWithinBuffer;
	}

	public boolean isAllowCreditUtilizedPassengerNameModification() {
		return allowCreditUtilizedPassengerNameModification;
	}

	public void setAllowCreditUtilizedPassengerNameModification(boolean allowCreditUtilizedPassengerNameModification) {
		this.allowCreditUtilizedPassengerNameModification = allowCreditUtilizedPassengerNameModification;
	}

	public boolean isAllowPassengerNameModificationHavingDepatedSegments() {
		return allowPassengerNameModificationHavingDepatedSegments;
	}

	/**
	 * @param allowPassengerNameModificationHavingDepatedSegments
	 *            the allowPassengerNameModificationHavingDepatedSegments to set
	 */
	public void
			setAllowPassengerNameModificationHavingDepatedSegments(boolean allowPassengerNameModificationHavingDepatedSegments) {
		this.allowPassengerNameModificationHavingDepatedSegments = allowPassengerNameModificationHavingDepatedSegments;
	}

	public boolean isAllowExtendedPassengerNameModification() {
		return allowExtendedPassengerNameModification;
	}

	public void setAllowExtendedPassengerNameModification(boolean allowPassengerNameModificationWithinNameChangeCutoff) {
		this.allowExtendedPassengerNameModification = allowPassengerNameModificationWithinNameChangeCutoff;
	}

	public boolean isAllowCreateUserAlert() {
		return allowCreateUserAlert;
	}

	public void setAllowCreateUserAlert(boolean allowCreateUserAlert) {
		this.allowCreateUserAlert = allowCreateUserAlert;
	}

	public boolean isGroupBookingSplitPassengers() {
		return groupBookingSplitPassengers;
	}

	public void setGroupBookingSplitPassengers(boolean groupBookingSplitPassengers) {
		this.groupBookingSplitPassengers = groupBookingSplitPassengers;
	}

	public boolean isAllowOverBookAfterCutoffTime() {
		return allowOverBookAfterCutoffTime;
	}

	public void setAllowOverBookAfterCutoffTime(boolean allowOverBookAfterCutoffTime) {
		this.allowOverBookAfterCutoffTime = allowOverBookAfterCutoffTime;
	}

	public boolean isAgentCommissionEnabled() {
		return agentCommissionEnabled;
	}

	public void setAgentCommissionEnabled(boolean agentCommissionEnabled) {
		this.agentCommissionEnabled = agentCommissionEnabled;
	}

	public boolean isAllowVoidOwnReservation() {
		return allowVoidOwnReservation;
	}

	public void setAllowVoidOwnReservation(boolean allowVoidOwnReservation) {
		this.allowVoidOwnReservation = allowVoidOwnReservation;
	}

	public boolean isBspPaymentsForAlterRes() {
		return bspPaymentsForAlterRes;
	}

	public void setBspPaymentsForAlterRes(boolean bspPaymentsForAlterRes) {
		this.bspPaymentsForAlterRes = bspPaymentsForAlterRes;
	}

	public boolean isFareRuleBufferTimeModification() {
		return fareRuleBufferTimeModification;
	}

	public void setFareRuleBufferTimeModification(boolean fareRuleBufferTimeModification) {
		this.fareRuleBufferTimeModification = fareRuleBufferTimeModification;
	}

	public boolean isAllowMultiCitySearch() {
		return allowMultiCitySearch;
	}

	public void setAllowMultiCitySearch(boolean allowMultiCitySearch) {
		this.allowMultiCitySearch = allowMultiCitySearch;
	}

	public boolean isEnforceSameOrHigherFare() {
		return enforceSameOrHigherFare;
	}

	public void setEnforceSameOrHigherFare(boolean enforceSameOrHigherFare) {
		this.enforceSameOrHigherFare = enforceSameOrHigherFare;
	}

	public boolean isAllowCaptureExternalInternationalFlightDetails() {
		return allowCaptureExternalInternationalFlightDetails;
	}

	public void setAllowCaptureExternalInternationalFlightDetails(boolean allowCaptureExternalInternationalFlightDetails) {
		this.allowCaptureExternalInternationalFlightDetails = allowCaptureExternalInternationalFlightDetails;
	}

	public boolean isCreateNewPnrFromOld() {
		return createNewPnrFromOld;
	}

	/**
	 * @return the allowPassengerUserNoteModificationHavingDepartedSegments
	 */
	public boolean isAllowPassengerUserNoteModificationHavingDepartedSegments() {
		return allowPassengerUserNoteModificationHavingDepartedSegments;
	}

	/**
	 * @param allowPassengerUserNoteModificationHavingDepartedSegments
	 *            the allowPassengerUserNoteModificationHavingDepartedSegments to set
	 */
	public void setAllowPassengerUserNoteModificationHavingDepartedSegments(
			boolean allowPassengerUserNoteModificationHavingDepartedSegments) {
		this.allowPassengerUserNoteModificationHavingDepartedSegments = allowPassengerUserNoteModificationHavingDepartedSegments;
	}

	public boolean isRestrictModForCnxFlown() {
		return restrictModForCnxFlown;
	}

	public boolean isEnableFareRuleNCC() {
		return enableFareRuleNCC;
	}

	public void setEnableFareRuleNCC(boolean enableFareRuleNCC) {
		this.enableFareRuleNCC = enableFareRuleNCC;
	}

	public boolean isEnableFareRuleNCCForFlown() {
		return enableFareRuleNCCForFlown;
	}

	public void setEnableFareRuleNCCForFlown(boolean enableFareRuleNCCForFlown) {
		this.enableFareRuleNCCForFlown = enableFareRuleNCCForFlown;
	}

	public int getNameChangeMaxCount() {
		return nameChangeMaxCount;
	}

	public void setNameChangeMaxCount(int nameChangeMaxCount) {
		this.nameChangeMaxCount = nameChangeMaxCount;
	}

	public Collection<String> getAllowedSubOperations() {
		return allowedSubOperations;
	}

	public boolean isAllowAddInfantToPartiallyFlownBookings() {
		return allowAddInfantToPartiallyFlownBookings;
	}

	public void setAllowAddInfantToPartiallyFlownBookings(boolean allowAddInfantToPartiallyFlownBookings) {
		this.allowAddInfantToPartiallyFlownBookings = allowAddInfantToPartiallyFlownBookings;
	}

	public boolean isExtendedNameChangeEnabled() {
		return extendedNameChangeEnabled;
	}

	public void setExtendedNameChangeEnabled(boolean extendedNameChangeEnabled) {
		this.extendedNameChangeEnabled = extendedNameChangeEnabled;
	}

	public void setCancelReservation(boolean cancelReservation) {
		this.cancelReservation = cancelReservation;
	}

	public boolean isAllowCouponControlRequest() {
		return allowCouponControlRequest;
	}

	public void setAllowCouponControlRequest(boolean allowCouponControlRequest) {
		this.allowCouponControlRequest = allowCouponControlRequest;
	}

	public boolean isAllowSkipNic() {
		return allowSkipNic;
	}

	public void setAllowSkipNic(boolean allowSkipNic) {
		this.allowSkipNic = allowSkipNic;
	}

	public boolean isAllowExternalTicketExchange() {
		return allowExternalTicketExchange;
	}

	public void setAllowExternalTicketExchange(boolean allowExternalTicketExchange) {
		this.allowExternalTicketExchange = allowExternalTicketExchange;
	}

	public boolean isAllowOverrideRequoteNameChange() {
		return allowOverrideRequoteNameChange;
	}

	public void setAllowOverrideRequoteNameChange(boolean allowOverrideRequoteNameChange) {
		this.allowOverrideRequoteNameChange = allowOverrideRequoteNameChange;
	}

	public boolean isAllowAddUserNotes() {
		return allowAddUserNotes;
	}

	public boolean isAllowClassifyUserNotes() {
		return allowClassifyUserNotes;
	}
	
	public boolean isCheckBlackListPax() {
		return checkBlackListPax;
	}

	public boolean isVoucherPayments() {
		return voucherPayments;
	}

	public void setVoucherPayments(boolean voucherPayments) {
		this.voucherPayments = voucherPayments;
	}

	public boolean isGoquoValid() {
		return goquoValid;
	}

	public void setGoquoValid(boolean goquoValid) {
		this.goquoValid = goquoValid;
	}

	public boolean isGoquoBooking() {
		return goquoBooking;
	}

	public void setGoquoBooking(boolean goquoBooking) {
		this.goquoBooking = goquoBooking;
	}

	public boolean isAllowExtendedNameChange() {
		return allowExtendedNameChange;
	}

	public boolean isClearAlertAfterTransfer() {
		return clearAlertAfterTransfer;
	}

	public void setClearAlertAfterTransfer(boolean clearAlertAfterTransfer) {
		this.clearAlertAfterTransfer = clearAlertAfterTransfer;
	}

	public boolean isAllowViewMCO() {
		return allowViewMCO;
	}

	public void setAllowViewMCO(boolean allowViewMCO) {
		this.allowViewMCO = allowViewMCO;
	}

	public boolean isAllowRouteSelectionForAgents() {
		return allowRouteSelectionForAgents;
	}

	public void setAllowRouteSelectionForAgents(boolean allowRouteSelectionForAgents) {
		this.allowRouteSelectionForAgents = allowRouteSelectionForAgents;
	}

	public boolean isOfflinePayments() {
		return offlinePayments;
	}

	public void setOfflinePayments(boolean offlinePayments) {
		this.offlinePayments = offlinePayments;
	}

	public String getTaxRegistrationNumberEnabledCountries() {
		return taxRegistrationNumberEnabledCountries;
	}

	public void setTaxRegistrationNumberEnabledCountries(String taxRegistrationNumberEnabledCountries) {
		this.taxRegistrationNumberEnabledCountries = taxRegistrationNumberEnabledCountries;
	}

	public boolean isAllowViewPublicOnewayFaresForReturn() {
		return allowViewPublicOnewayFaresForReturn;
	}

	public boolean isAllowToRemoveReprotectedAnciForOHD() {
		return allowToRemoveReprotectedAnciForOHD;
	}

	public void setAllowToRemoveReprotectedAnciForOHD(boolean allowToRemoveReprotectedAnciForOHD) {
		this.allowToRemoveReprotectedAnciForOHD = allowToRemoveReprotectedAnciForOHD;
	}
}
