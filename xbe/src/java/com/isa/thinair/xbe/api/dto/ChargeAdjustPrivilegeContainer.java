package com.isa.thinair.xbe.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.ChargeAdjustmentPrivilege;

/**
 * The container object holding the carrier wise charge adjsutment privileges. We had to introduce this object as of now
 * the code does no support dynamic handling of privileges. By introducing this object we are enabling to define a new
 * charge adjustment privileges without any code changes being required on privilege handling.
 * 
 * @author sanjaya
 */
public class ChargeAdjustPrivilegeContainer implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The carrierwise charge adjustment privileges map. */
	private Map<String, Collection<ChargeAdjustmentPrivilege>> carrierWiseChargeAdjustPrivileges = new HashMap<String, Collection<ChargeAdjustmentPrivilege>>();

	/**
	 * @return : The carrierwise charge adjustment privileges map.
	 */
	public Map<String, Collection<ChargeAdjustmentPrivilege>> getCarrierWiseChargeAdjustPrivileges() {
		return carrierWiseChargeAdjustPrivileges;
	}
}