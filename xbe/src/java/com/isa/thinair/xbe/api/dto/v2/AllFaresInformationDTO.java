package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;

public class AllFaresInformationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orignNDest;
	private String fareType;
	private String displayCodeForFareType;
	private List<FareRuleDTO> fareRuleInfo;
	private List<InvAllocationDTO> invAllocationInfo;

	public String getOrignNDest() {
		return orignNDest;
	}

	public void setOrignNDest(String orignNDest) {
		this.orignNDest = orignNDest;
	}

	public String getDisplayCodeForFareType() {
		return displayCodeForFareType;
	}

	public void setDisplayCodeForFareType(String displayCodeForFareType) {
		this.displayCodeForFareType = displayCodeForFareType;
	}

	public String getFareType() {
		return fareType;
	}

	public void setFareType(String fareType) {
		this.fareType = fareType;
	}

	public List<FareRuleDTO> getFareRuleInfo() {
		return fareRuleInfo;
	}

	public void setFareRuleInfo(List<FareRuleDTO> fareRuleInfo) {
		this.fareRuleInfo = fareRuleInfo;
	}

	public List<InvAllocationDTO> getInvAllocationInfo() {
		return invAllocationInfo;
	}

	public void setInvAllocationInfo(List<InvAllocationDTO> invAllocationInfo) {
		this.invAllocationInfo = invAllocationInfo;
	}

}
