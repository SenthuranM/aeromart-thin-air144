package com.isa.thinair.xbe.api.dto;

import java.io.Serializable;

public class SystemParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long minimumTransitionTimeInMillis;

	private String defaultLanguage;

	private boolean dynamicOndListPopulationEnabled;

	private boolean userDefineTransitTimeEnabled = false;
	
	private int allowedMaxNoONDMulticitySearch;

	public long getMinimumTransitionTimeInMillis() {
		return minimumTransitionTimeInMillis;
	}

	public void setMinimumTransitionTimeInMillis(long minimumTransitionTimeInMillis) {
		this.minimumTransitionTimeInMillis = minimumTransitionTimeInMillis;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public boolean isDynamicOndListPopulationEnabled() {
		return dynamicOndListPopulationEnabled;
	}

	public void setDynamicOndListPopulationEnabled(boolean dynamicOndListPopulationEnabled) {
		this.dynamicOndListPopulationEnabled = dynamicOndListPopulationEnabled;
	}

	public boolean isUserDefineTransitTimeEnabled() {
		return userDefineTransitTimeEnabled;
	}

	public void setUserDefineTransitTimeEnabled(boolean userDefineTransitTimeEnabled) {
		this.userDefineTransitTimeEnabled = userDefineTransitTimeEnabled;
	}

	public int getAllowedMaxNoONDMulticitySearch() {
		return allowedMaxNoONDMulticitySearch;
	}

	public void setAllowedMaxNoONDMulticitySearch(int allowedMaxNoONDMulticitySearch) {
		this.allowedMaxNoONDMulticitySearch = allowedMaxNoONDMulticitySearch;
	}
}
