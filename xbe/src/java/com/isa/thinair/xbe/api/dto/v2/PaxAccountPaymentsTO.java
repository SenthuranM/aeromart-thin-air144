package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

public class PaxAccountPaymentsTO {
	private String totalPayment;

	private String balance;

	private String currency;

	private Collection<PaymentListTO> paymentList;
	
	private String actualCredit;
	/**
	 * @return the totalPayment
	 */
	public String getTotalPayment() {
		return totalPayment;
	}

	/**
	 * @param totalPayment
	 *            the totalPayment to set
	 */
	public void setTotalPayment(String totalPayment) {
		this.totalPayment = totalPayment;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the paymentList
	 */
	public Collection<PaymentListTO> getPaymentList() {
		return paymentList;
	}

	/**
	 * @param paymentList
	 *            the paymentList to set
	 */
	public void setPaymentList(Collection<PaymentListTO> paymentList) {
		this.paymentList = paymentList;
	}

	public String getActualCredit() {
		return actualCredit;
	}

	public void setActualCredit(String actualCredit) {
		this.actualCredit = actualCredit;
	}
}
