package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;

/**
 * @author Nilindra Fernando
 */
public class FlightListTO {
	private String type;

	private Collection<FlightInfoTO> flights;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the flights
	 */
	public Collection<FlightInfoTO> getFlights() {
		return flights;
	}

	/**
	 * @param flights
	 *            the flights to set
	 */
	public void setFlights(Collection<FlightInfoTO> flights) {
		this.flights = flights;
	}

}
