/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.xbe.api.dto.v2;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AdultPaxTO implements IDisplayPax {

	private String displayAdultType;

	private String displayAdultTitle;

	private String displayAdultFirstName;

	private String displayAdultLastName;

	private String displayAdultNationality;

	private String displayAdultDOB;

	private String displayAdultCharg;

	private String displayAdultSSR;

	private String displayAdultSSRCode;

	private String displayAdultSSRInformation;

	private String displayInfantTravelling;

	private String displayInfantWith;

	private String displayAdultPay;

	private String displayAdultBal;

	private String displayAdultAcco;

	private String displayAdultMCO;

	private String displaySelectPax;

	private String displayPaxTravelReference;

	private String displayAdultInfantCharg;

	private String displayPaxSequence;

	private String displayOrdePaxSequence;

	private String displayPaxCategory;

	private String displayPnrPaxCatFOIDNumber;

	private String displayPnrPaxCatFOIDExpiry;

	private String displayPnrPaxCatFOIDPlace;

	private String displayPnrPaxCatFOIDType;

	private String displayPnrPaxPlaceOfBirth;

	private String displayTravelDocType;

	private String displayVisaDocNumber;

	private String displayVisaDocPlaceOfIssue;

	private String displayVisaDocIssueDate;

	private String displayVisaApplicableCountry;
	private boolean nameEditable;

	private String displayETicketMask;

	private String displayAdultFirstNameOl;

	private String displayAdultLastNameOl;

	private String displayNameTranslationLanguage;

	private String displayAdultTitleOl;
	private String displaypnrPaxArrivalFlightNumber;

	private String displaypnrPaxFltArrivalDate;

	private String displaypnrPaxArrivalTime;

	private String displaypnrPaxDepartureFlightNumber;

	private String displaypnrPaxFltDepartureDate;

	private String displaypnrPaxDepartureTime;

	private String displayPnrPaxGroupId;

	private String displayNationalIDNo;

	private String displayFFID;
	
	private boolean blacklisted;

	public AdultPaxTO() {

	}

	private AdultPaxTO(LCCClientReservationPax lccClientReservationPax, int intPaxCount, boolean viewAccntInfo,
			boolean showPaxEticket) {
		String totalCharges = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalPrice());
		String availableBalance = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalAvailableBalance());
		String totalPaid = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalPaidAmount());

		setDisplayAdultCharg(totalCharges);
		setDisplayAdultPay(totalPaid);
		setDisplayAdultBal(availableBalance);

		if (showPaxEticket) {
			setDisplayETicketMask("<input type='button' id='btndisplayETckMask" + intPaxCount
					+ "' name='btndisplayETckMask' value='E-TCK' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.loadGroupPaxStatusUpdate()'>");
		} else {
			setDisplayETicketMask("<input type='button' id='btndisplayETckMask" + intPaxCount
					+ "' name='btndisplayETckMask' value='E-TCK' class='ui-state-default ui-corner-all' disabled='disabled'>");
		}
	}

	public static AdultPaxTO getChildDisplayInstance(LCCClientReservationPax lccClientReservationPax, int intPaxCount,
			boolean viewAccntInfo, boolean showPaxEticket, boolean allowViewMCO, boolean hasMCO) {
		AdultPaxTO child = new AdultPaxTO(lccClientReservationPax, intPaxCount, viewAccntInfo, showPaxEticket);
		child.setDisplayPaxType(PaxTypeTO.CHILD);

		child.setDisplayPaxSSR("<input type='button' id='btndisplayAdultSSR" + intPaxCount
				+ "' name='btndisplayChildSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnSSROnClick({row:"
				+ intPaxCount + ", type:\"ADT\"})'>");

		if (viewAccntInfo) {
			child.setDisplayAdultAcco("<input type='button' id='btndisplayAdultAcco" + intPaxCount
					+ "' name='btndisplayChildAcco' value='ACC' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnAccoOnClick({row:"
					+ intPaxCount + ", type:\"ADT\"})'>");
		} else {
			child.setDisplayAdultAcco("<input type='button' id='btndisplayAdultAcco" + intPaxCount
					+ "' name='btndisplayChildAcco' value='ACC' class='ui-state-default ui-corner-all' disabled='disabled'>");
		}

		if (allowViewMCO) {
			if (hasMCO) {
				child.setDisplayAdultMCO("<input type='button' id='btndisplayAdultMCO" + intPaxCount
						+ "' name='btndisplayChildMCO' value='MCO' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnMCOOnClick({row:"
						+ intPaxCount + ", type:\"ADT\"})'>");
			} else {
				child.setDisplayAdultMCO("<input type='button' id='btndisplayAdultMCO" + intPaxCount
						+ "' name='btndisplayChildMCO' value='MCO' class='ui-state-disabled ui-corner-all' disabled>");
			}
		}

		return child;
	}

	public static AdultPaxTO getAdultDisplayInstance(LCCClientReservationPax lccClientReservationPax, int intPaxCount,
			boolean viewAccntInfo, boolean showPaxEticket, boolean allowViewMCO, boolean hasMCO) {
		AdultPaxTO adult = new AdultPaxTO(lccClientReservationPax, intPaxCount, viewAccntInfo, showPaxEticket);
		adult.setDisplayPaxType(PaxTypeTO.ADULT);

		adult.setDisplayAdultSSR("<input type='button' id='btndisplayAdultSSR" + intPaxCount
				+ "' name='btndisplayAdultSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnSSROnClick({row:"
				+ intPaxCount + ", type:\"ADT\"})'>");

		if (viewAccntInfo) {
			adult.setDisplayAdultAcco("<input type='button' id='btndisplayAdultAcco" + intPaxCount
					+ "' name='btndisplayChildAcco' value='ACC' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnAccoOnClick({row:"
					+ intPaxCount + ", type:\"ADT\"})'>");

		} else {
			adult.setDisplayAdultAcco("<input type='button' id='btndisplayAdultAcco" + intPaxCount
					+ "' name='btndisplayAdultAcco' value='ACC' class='ui-state-default ui-corner-all' disabled='disabled'>");
		}

		if (allowViewMCO) {
			if (hasMCO) {
				adult.setDisplayAdultMCO("<input type='button' id='btndisplayAdultMCO" + intPaxCount
					+ "' name='btndisplayAdultMCO' value='MCO' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnMCOOnClick({row:"
					+ intPaxCount + ", type:\"ADT\"})'>");
			} else {
				adult.setDisplayAdultMCO("<input type='button' id='btndisplayAdultMCO" + intPaxCount
						+ "' name='btndisplayAdultMCO' value='MCO' class='ui-state-disabled ui-corner-all' disabled>");
			}
		}

		return adult;
	}

	public String getDisplayNationalIDNo() {
		return displayNationalIDNo;
	}

	public void setDisplayNationalIDNo(String displayNationalIDNo) {
		this.displayNationalIDNo = displayNationalIDNo;
	}

	/**
	 * @return the displayAdultType
	 */
	public String getDisplayAdultType() {
		return displayAdultType;
	}

	/**
	 * @param displayAdultType
	 *            the displayAdultType to set
	 */
	public void setDisplayAdultType(String displayAdultType) {
		this.displayAdultType = displayAdultType;
	}

	/**
	 * @return the displayAdultTitle
	 */
	public String getDisplayAdultTitle() {
		return displayAdultTitle;
	}

	/**
	 * @param displayAdultTitle
	 *            the displayAdultTitle to set
	 */
	public void setDisplayAdultTitle(String displayAdultTitle) {
		this.displayAdultTitle = displayAdultTitle;
	}

	/**
	 * @return the displayAdultFirstName
	 */
	public String getDisplayAdultFirstName() {
		return displayAdultFirstName;
	}

	/**
	 * @param displayAdultFirstName
	 *            the displayAdultFirstName to set
	 */
	public void setDisplayAdultFirstName(String displayAdultFirstName) {
		this.displayAdultFirstName = displayAdultFirstName;
	}

	/**
	 * @return the displayAdultLastName
	 */
	public String getDisplayAdultLastName() {
		return displayAdultLastName;
	}

	/**
	 * @param displayAdultLastName
	 *            the displayAdultLastName to set
	 */
	public void setDisplayAdultLastName(String displayAdultLastName) {
		this.displayAdultLastName = displayAdultLastName;
	}

	/**
	 * @return the displayAdultNationality
	 */
	public String getDisplayAdultNationality() {
		return displayAdultNationality;
	}

	/**
	 * @param displayAdultNationality
	 *            the displayAdultNationality to set
	 */
	public void setDisplayAdultNationality(String displayAdultNationality) {
		this.displayAdultNationality = displayAdultNationality;
	}

	/**
	 * @return the displayAdultDOB
	 */
	public String getDisplayAdultDOB() {
		return displayAdultDOB;
	}

	/**
	 * @param displayAdultDOB
	 *            the displayAdultDOB to set
	 */
	public void setDisplayAdultDOB(String displayAdultDOB) {
		try {
			if (displayAdultDOB != null && !"".equals(displayAdultDOB.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayAdultDOB = format.format(format.parse(displayAdultDOB));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayAdultDOB = displayAdultDOB;
	}

	/**
	 * @return the displayAdultCharg
	 */
	public String getDisplayAdultCharg() {
		return displayAdultCharg;
	}

	/**
	 * @param displayAdultCharg
	 *            the displayAdultCharg to set
	 */
	public void setDisplayAdultCharg(String displayAdultCharg) {
		this.displayAdultCharg = displayAdultCharg;
	}

	/**
	 * @return the displayAdultSSR
	 */
	public String getDisplayAdultSSR() {
		return displayAdultSSR;
	}

	/**
	 * @param displayAdultSSR
	 *            the displayAdultSSR to set
	 */
	public void setDisplayAdultSSR(String displayAdultSSR) {
		this.displayAdultSSR = displayAdultSSR;
	}

	/**
	 * @return the displayAdultSSRCode
	 */
	public String getDisplayAdultSSRCode() {
		return displayAdultSSRCode;
	}

	/**
	 * @param displayAdultSSRCode
	 *            the displayAdultSSRCode to set
	 */
	public void setDisplayAdultSSRCode(String displayAdultSSRCode) {
		this.displayAdultSSRCode = displayAdultSSRCode;
	}

	/**
	 * @return the displayAdultSSRInformation
	 */
	public String getDisplayAdultSSRInformation() {
		return displayAdultSSRInformation;
	}

	/**
	 * @param displayAdultSSRInformation
	 *            the displayAdultSSRInformation to set
	 */
	public void setDisplayAdultSSRInformation(String displayAdultSSRInformation) {
		this.displayAdultSSRInformation = displayAdultSSRInformation;
	}

	/**
	 * @return the displayInfantTravelling
	 */
	public String getDisplayInfantTravelling() {
		return displayInfantTravelling;
	}

	/**
	 * @param displayInfantTravelling
	 *            the displayInfantTravelling to set
	 */
	public void setDisplayInfantTravelling(String displayInfantTravelling) {
		this.displayInfantTravelling = displayInfantTravelling;
	}

	/**
	 * @return the displayInfantWith
	 */
	public String getDisplayInfantWith() {
		return displayInfantWith;
	}

	/**
	 * @param displayInfantWith
	 *            the displayInfantWith to set
	 */
	public void setDisplayInfantWith(String displayInfantWith) {
		this.displayInfantWith = displayInfantWith;
	}

	/**
	 * @return the displayAdultPay
	 */
	public String getDisplayAdultPay() {
		return displayAdultPay;
	}

	/**
	 * @param displayAdultPay
	 *            the displayAdultPay to set
	 */
	public void setDisplayAdultPay(String displayAdultPay) {
		this.displayAdultPay = displayAdultPay;
	}

	/**
	 * @return the displayAdultBal
	 */
	public String getDisplayAdultBal() {
		return displayAdultBal;
	}

	/**
	 * @param displayAdultBal
	 *            the displayAdultBal to set
	 */
	public void setDisplayAdultBal(String displayAdultBal) {
		this.displayAdultBal = displayAdultBal;
	}

	/**
	 * @return the displayAdultAcco
	 */
	public String getDisplayAdultAcco() {
		return displayAdultAcco;
	}

	/**
	 * @param displayAdultAcco
	 *            the displayAdultAcco to set
	 */
	public void setDisplayAdultAcco(String displayAdultAcco) {
		this.displayAdultAcco = displayAdultAcco;
	}

	/**
	 * @return the displaySelectPax
	 */
	public String getDisplaySelectPax() {
		return displaySelectPax;
	}

	public String getDisplayAdultMCO() {
		return displayAdultMCO;
	}

	public void setDisplayAdultMCO(String displayAdultMCO) {
		this.displayAdultMCO = displayAdultMCO;
	}

	/**
	 * @param displaySelectPax
	 *            the displaySelectPax to set
	 */
	public void setDisplaySelectPax(String displaySelectPax) {
		this.displaySelectPax = displaySelectPax;
	}

	/**
	 * @return the displayPaxTravelReference
	 */
	public String getDisplayPaxTravelReference() {
		return displayPaxTravelReference;
	}

	/**
	 * @param displayPaxTravelReference
	 *            the displayPaxTravelReference to set
	 */
	public void setDisplayPaxTravelReference(String displayPaxTravelReference) {
		this.displayPaxTravelReference = displayPaxTravelReference;
	}

	/**
	 * @return the displayAdultInfantCharg
	 */
	public String getDisplayAdultInfantCharg() {
		return displayAdultInfantCharg;
	}

	/**
	 * @param displayAdultInfantCharg
	 *            the displayAdultInfantCharg to set
	 */
	public void setDisplayAdultInfantCharg(String displayAdultInfantCharg) {
		this.displayAdultInfantCharg = displayAdultInfantCharg;
	}

	public String getDisplayPaxSequence() {
		return displayPaxSequence;
	}

	public void setDisplayPaxSequence(String displayPaxSequence) {
		this.displayPaxSequence = displayPaxSequence;
	}

	public boolean isNameEditable() {
		return nameEditable;
	}

	public void setNameEditable(boolean nameEditable) {
		this.nameEditable = nameEditable;
	}

	public String getDisplayPaxCategory() {
		return displayPaxCategory;
	}

	public void setDisplayPaxCategory(String displayPaxCategory) {
		this.displayPaxCategory = displayPaxCategory;
	}

	public String getDisplayPnrPaxCatFOIDNumber() {
		return displayPnrPaxCatFOIDNumber;
	}

	public void setDisplayPnrPaxCatFOIDNumber(String displayPnrPaxCatFOIDNumber) {
		this.displayPnrPaxCatFOIDNumber = displayPnrPaxCatFOIDNumber;
	}

	public String getDisplayPnrPaxCatFOIDExpiry() {
		return displayPnrPaxCatFOIDExpiry;
	}

	public void setDisplayPnrPaxCatFOIDExpiry(String displayPnrPaxCatFOIDExpiry) {
		try {
			if (displayPnrPaxCatFOIDExpiry != null && !"".equals(displayPnrPaxCatFOIDExpiry.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayPnrPaxCatFOIDExpiry = format.format(format.parse(displayPnrPaxCatFOIDExpiry));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayPnrPaxCatFOIDExpiry = displayPnrPaxCatFOIDExpiry;
	}

	public String getDisplayPnrPaxCatFOIDPlace() {
		return displayPnrPaxCatFOIDPlace;
	}

	public void setDisplayPnrPaxCatFOIDPlace(String displayPnrPaxCatFOIDPlace) {
		this.displayPnrPaxCatFOIDPlace = displayPnrPaxCatFOIDPlace;
	}

	public String getDisplayOrdePaxSequence() {
		return displayOrdePaxSequence;
	}

	public void setDisplayOrdePaxSequence(String displayOrdePaxSequence) {
		this.displayOrdePaxSequence = displayOrdePaxSequence;
	}

	public String getDisplayPnrPaxCatFOIDType() {
		return displayPnrPaxCatFOIDType;
	}

	public void setDisplayPnrPaxCatFOIDType(String displayPnrPaxCatFOIDType) {
		this.displayPnrPaxCatFOIDType = displayPnrPaxCatFOIDType;
	}

	public String getDisplayETicketMask() {
		return displayETicketMask;
	}

	public void setDisplayETicketMask(String displayETicketMask) {
		this.displayETicketMask = displayETicketMask;
	}

	public String getDisplayAdultFirstNameOl() {
		return displayAdultFirstNameOl;
	}

	public void setDisplayAdultFirstNameOl(String displayAdultFirstNameOl) {
		this.displayAdultFirstNameOl = displayAdultFirstNameOl;
	}

	public String getDisplayAdultLastNameOl() {
		return displayAdultLastNameOl;
	}

	public void setDisplayAdultLastNameOl(String displayAdultLastNameOl) {
		this.displayAdultLastNameOl = displayAdultLastNameOl;
	}

	public String getDisplayNameTranslationLanguage() {
		return displayNameTranslationLanguage;
	}

	public void setDisplayNameTranslationLanguage(String displayNameTranslationLanguage) {
		this.displayNameTranslationLanguage = displayNameTranslationLanguage;
	}

	public String getDisplayAdultTitleOl() {
		return displayAdultTitleOl;
	}

	public void setDisplayAdultTitleOl(String displayAdultTitleOl) {
		this.displayAdultTitleOl = displayAdultTitleOl;
	}

	public String getDisplayPnrPaxPlaceOfBirth() {
		return displayPnrPaxPlaceOfBirth;
	}

	public void setDisplayPnrPaxPlaceOfBirth(String displayPnrPaxPlaceOfBirth) {
		this.displayPnrPaxPlaceOfBirth = displayPnrPaxPlaceOfBirth;
	}

	public String getDisplayTravelDocType() {
		return displayTravelDocType;
	}

	public void setDisplayTravelDocType(String displayTravelDocType) {
		this.displayTravelDocType = displayTravelDocType;
	}

	public String getDisplayVisaDocNumber() {
		return displayVisaDocNumber;
	}

	public void setDisplayVisaDocNumber(String displayVisaDocNumber) {
		this.displayVisaDocNumber = displayVisaDocNumber;
	}

	public String getDisplayVisaDocPlaceOfIssue() {
		return displayVisaDocPlaceOfIssue;
	}

	public void setDisplayVisaDocPlaceOfIssue(String displayVisaDocPlaceOfIssue) {
		this.displayVisaDocPlaceOfIssue = displayVisaDocPlaceOfIssue;
	}

	public String getDisplayVisaDocIssueDate() {
		return displayVisaDocIssueDate;
	}

	public void setDisplayVisaDocIssueDate(String displayVisaDocIssueDate) {
		try {
			if (displayVisaDocIssueDate != null && !"".equals(displayVisaDocIssueDate.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayVisaDocIssueDate = format.format(format.parse(displayVisaDocIssueDate));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayVisaDocIssueDate = displayVisaDocIssueDate;
	}

	public String getDisplayVisaApplicableCountry() {
		return displayVisaApplicableCountry;
	}

	public void setDisplayVisaApplicableCountry(String displayVisaApplicableCountry) {
		this.displayVisaApplicableCountry = displayVisaApplicableCountry;
	}

	public String getDisplaypnrPaxArrivalFlightNumber() {
		return displaypnrPaxArrivalFlightNumber;
	}

	public void setDisplaypnrPaxArrivalFlightNumber(String displaypnrPaxArrivalFlightNumber) {
		this.displaypnrPaxArrivalFlightNumber = displaypnrPaxArrivalFlightNumber;
	}

	public String getDisplaypnrPaxFltArrivalDate() {
		return displaypnrPaxFltArrivalDate;
	}

	public void setDisplaypnrPaxFltArrivalDate(String displaypnrPaxFltArrivalDate) {
		this.displaypnrPaxFltArrivalDate = displaypnrPaxFltArrivalDate;
	}

	public String getDisplaypnrPaxArrivalTime() {
		return displaypnrPaxArrivalTime;
	}

	public void setDisplaypnrPaxArrivalTime(String displaypnrPaxArrivalTime) {
		this.displaypnrPaxArrivalTime = displaypnrPaxArrivalTime;
	}

	public String getDisplaypnrPaxDepartureFlightNumber() {
		return displaypnrPaxDepartureFlightNumber;
	}

	public void setDisplaypnrPaxDepartureFlightNumber(String displaypnrPaxDepartureFlightNumber) {
		this.displaypnrPaxDepartureFlightNumber = displaypnrPaxDepartureFlightNumber;
	}

	public String getDisplaypnrPaxFltDepartureDate() {
		return displaypnrPaxFltDepartureDate;
	}

	public void setDisplaypnrPaxFltDepartureDate(String displaypnrPaxFltDepartureDate) {
		this.displaypnrPaxFltDepartureDate = displaypnrPaxFltDepartureDate;
	}

	public String getDisplaypnrPaxDepartureTime() {
		return displaypnrPaxDepartureTime;
	}

	public void setDisplaypnrPaxDepartureTime(String displaypnrPaxDepartureTime) {
		this.displaypnrPaxDepartureTime = displaypnrPaxDepartureTime;
	}

	public String getDisplayPnrPaxGroupId() {
		return displayPnrPaxGroupId;
	}

	public void setDisplayPnrPaxGroupId(String displayPnrPaxGroupId) {
		this.displayPnrPaxGroupId = displayPnrPaxGroupId;
	}

	public void setDisplayFFID(String ffid) {
		this.displayFFID = ffid;
	}

	public String getDisplayFFID() {
		return this.displayFFID;
	}

	@Override
	public void setDisplayPaxType(String displayPaxType) {
		setDisplayAdultType(displayPaxType);
	}

	@Override
	public void setDisplayPaxFirstName(String displayPaxFirstName) {
		setDisplayAdultFirstName(displayPaxFirstName);
	}

	@Override
	public void setDisplayPaxLastName(String displayPaxLastName) {
		setDisplayAdultLastName(displayPaxLastName);
	}

	@Override
	public void setDisplayPaxNationality(String displayPaxNationality) {
		setDisplayAdultNationality(displayPaxNationality);
	}

	@Override
	public void setDisplayPaxDOB(String displayPaxDOB) {
		setDisplayAdultDOB(displayPaxDOB);
	}

	@Override
	public void setDisplayPaxTravellingWith(String displayPaxTravellingWith) {
		setDisplayInfantTravelling(displayPaxTravellingWith);
	}

	@Override
	public void setDisplayPaxSSR(String displayPaxSSR) {
		setDisplayAdultSSR(displayPaxSSR);
	}

	@Override
	public void setDisplayPaxSSRCode(String displayPaxSSRCode) {
		setDisplayAdultSSRCode(displayPaxSSRCode);
	}

	@Override
	public void setDisplayPaxSSRInformation(String displayPaxSSRInformation) {
		setDisplayAdultSSRInformation(displayPaxSSRInformation);
	}

	@Override
	public void setDisplayPaxSelect(String displayPaxSelect) {
		// TODO
	}

	@Override
	public void setDisplayPaxFirstNameOl(String displayPaxFirstNameOl) {
		setDisplayAdultFirstNameOl(displayPaxFirstNameOl);
	}

	@Override
	public void setDisplayPaxLastNameOl(String displayPaxLastNameOl) {
		setDisplayAdultLastNameOl(displayPaxLastNameOl);
	}

	@Override
	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		// TODO
	}

	@Override
	public void setDisplayPaxTitle(String displayPaxTitle) {
		setDisplayAdultTitle(displayPaxTitle);
	}

	@Override
	public void setDisplayPaxTitleOl(String displayPaxTitleOl) {
		setDisplayAdultTitleOl(displayPaxTitleOl);
	}

	public boolean isBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(boolean isBlacklisted) {
		this.blacklisted = isBlacklisted;
	}
}
