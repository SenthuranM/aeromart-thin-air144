package com.isa.thinair.xbe.api.dto;

import java.io.Serializable;

public class ReservationRowDataDTO implements Serializable {
	private static final long serialVersionUID = 4922015135105843600L;

	private CurrencyInfoTO selectedCurrencyInfo = null;

	/**
	 * @return the selectedCurrencyInfo
	 */
	public CurrencyInfoTO getSelectedCurrencyInfo() {
		return selectedCurrencyInfo;
	}

	/**
	 * @param selectedCurrencyInfo
	 *            the selectedCurrencyInfo to set
	 */
	public void setSelectedCurrencyInfo(CurrencyInfoTO selectedCurrencyInfo) {
		this.selectedCurrencyInfo = selectedCurrencyInfo;
	}
}
