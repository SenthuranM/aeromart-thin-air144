package com.isa.thinair.xbe.api.dto.v2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.v2.reservation.AbstractBookingShoppingCart;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

/**
 * Booking shopping cart to track all items that are getting added till the booking is paid.
 * 
 * @author Nilindra Fernando
 */
public class BookingShoppingCart extends AbstractBookingShoppingCart {

	private BigDecimal usedPaxCredit;

	private BigDecimal paidAmount;

	private ReservationBalanceTO reservationBalance;

	private LCCClientResAlterQueryModesTO reservationAlterModesTO;

	private List<LCCInsuranceQuotationDTO> insuranceQuotes;

	private int externalChargesType = ChargeRateOperationType.ANY;

	private int payablePaxCount;

	private DiscountedFareDetails fareDiscount;
	private BigDecimal fareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private boolean skipPromotionForOnHold = false;

	private String totAgentCommission = "0.00";
	private AgentCommissionDetails agentCommissions;
	private boolean restrictCommissionVisibility;

	private Set<ServiceTaxContainer> applicableServiceTaxes;

	private ReservationDiscountDTO reservationDiscountDTO;

	private PayByVoucherInfo payByVoucherInfo;

	private int invalidVoucherRedeemAttempts;

	private List<VoucherDTO> redeemedVoucherList;

	@SuppressWarnings("unchecked")
	public BookingShoppingCart(int chargesType) throws ModuleException {
		this.externalChargesType = chargesType;
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BSP_FEE);

		this.setAllExternalChargesMap(ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
				this.externalChargesType));
		this.usedPaxCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		this.paidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public BigDecimal getModifyOrCancellationAmount() {
		BigDecimal modifyOrCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			modifyOrCancellationAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalCnxCharge(),
					this.reservationBalance.getTotalModCharge());
		}
		return modifyOrCancellationAmount;
	}

	@Override
	public BigDecimal getTotalPrice() {

		BigDecimal payTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			payTotalAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalPrice(),
					AccelAeroCalculator.getDefaultBigDecimalZero());
		} else {
			payTotalAmount = AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(true),
					getAvaliableBalance());
		}
		return payTotalAmount;
	}

	@Override
	public BigDecimal getTotalPriceIncludingCCCharges() {

		BigDecimal payTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			payTotalAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalPrice(),
					AccelAeroCalculator.getDefaultBigDecimalZero(), this.getTotalSelectedExternalChargesAmount(false));
		} else {
			payTotalAmount = AccelAeroCalculator.add(getSelectedFareAmount(), this.getTotalSelectedExternalChargesAmount(false),
					getAvaliableBalance());
		}
		return payTotalAmount;
	}

	@Override
	public BigDecimal getTotalPayable() {
		BigDecimal paybleAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			// paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalPrice(),
			// this.reservationBalance.getTotalCreditAmount(),
			// this.usedPaxCredit.negate() , this.paidAmount.negate());
			paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalAmountDue(), this.usedPaxCredit.negate());
		} else {
			paybleAmount = AccelAeroCalculator.add(getSelectedFareAmount(), getExternalChargesBalanceToPay(),
					this.usedPaxCredit.negate(), this.paidAmount.negate());
			paybleAmount = deductFareDiscount(paybleAmount);
		}
		return paybleAmount;
	}

	@Override
	public BigDecimal getTotalPayable(EXTERNAL_CHARGES... excludeChargeCodes) {
		BigDecimal paybleAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			// paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalPrice(),
			// this.reservationBalance.getTotalCreditAmount(),
			// this.usedPaxCredit.negate() , this.paidAmount.negate());
			paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalAmountDue(), this.usedPaxCredit.negate());
		} else {
			paybleAmount = AccelAeroCalculator.add(getSelectedFareAmount(), getExternalChargesBalanceToPay(excludeChargeCodes),
					this.usedPaxCredit.negate(), this.paidAmount.negate());
			paybleAmount = deductFareDiscount(paybleAmount);
		}
		return paybleAmount;
	}

	@Override
	public BigDecimal getTotalPayableIncludingCCCharges() {
		BigDecimal paybleAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.reservationBalance != null) {
			// paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalPrice(),
			// this.reservationBalance.getTotalCreditAmount(),
			// this.usedPaxCredit.negate() , this.paidAmount.negate());
			paybleAmount = AccelAeroCalculator.add(this.reservationBalance.getTotalAmountDue(), this.usedPaxCredit.negate(),
					getExternalChargesIncludingCCChargesBalanceToPay());
		} else {
			paybleAmount = AccelAeroCalculator.add(getSelectedFareAmount(), getExternalChargesIncludingCCChargesBalanceToPay(),
					this.usedPaxCredit.negate(), this.paidAmount.negate());
			paybleAmount = deductFareDiscount(paybleAmount);
		}
		return paybleAmount;
	}

	@Override
	public BigDecimal getTotalTax() {
		if (this.reservationBalance != null) {
			return this.reservationBalance.getSegmentSummary().getNewTaxAmount();
		} else {
			return super.getTotalTax();
		}
	}

	@Override
	public BigDecimal getTotalSurcharges() {
		if (this.reservationBalance != null) {
			return this.reservationBalance.getSegmentSummary().getNewSurchargeAmount();
		} else {
			return super.getTotalSurcharges();
		}
	}

	public BigDecimal getServiceCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExternalCharge(externalCharge) != null) {
			serviceCharge = this.getSelectedExternalCharge(externalCharge).getAmount();
		}
		return serviceCharge;
	}

	public BigDecimal getHandlingCharge() {
		BigDecimal handlingCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE) != null) {
			handlingCharge = this.getSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE).getTotalAmount();
		}
		return handlingCharge;
	}

	public void updateExternalCharges(Collection<EXTERNAL_CHARGES> externalCharges) throws ModuleException {
		if (externalCharges != null && !externalCharges.isEmpty()) {
			this.getAllExternalChargesMap().putAll((ModuleServiceLocator.getReservationBD()
					.getQuotedExternalCharges(externalCharges, null, this.externalChargesType)));
		}
	}

	private BigDecimal getAvaliableBalance() {
		if (this.reservationBalance == null) {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			return reservationBalance.getTotalAmountDue();
		}
	}

	public BigDecimal getTotalPaidAmount() {
		return AccelAeroCalculator.add(getPaidAmount(), getUsedPaxCredit());
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public BigDecimal getUsedPaxCredit() {
		return usedPaxCredit;
	}

	public void setUsedPaxCredit(BigDecimal usedPaxCredit) {
		this.usedPaxCredit = usedPaxCredit;
	}

	public ReservationBalanceTO getReservationBalance() {
		return reservationBalance;
	}

	public void setReservationBalance(ReservationBalanceTO reservationBalance) {
		this.reservationBalance = reservationBalance;
	}

	public LCCClientResAlterQueryModesTO getReservationAlterModesTO() {
		return reservationAlterModesTO;
	}

	public void setReservationAlterModesTO(LCCClientResAlterQueryModesTO reservationAlterModesTO) {
		this.reservationAlterModesTO = reservationAlterModesTO;
	}

	public List<LCCInsuranceQuotationDTO> getInsuranceQuotes() {
		return insuranceQuotes;
	}

	public void setInsuranceQuotes(List<LCCInsuranceQuotationDTO> insuranceQuotes) {
		this.insuranceQuotes = insuranceQuotes;
	}

	@Override
	public BigDecimal getAncillaryTotalExternalCharge(boolean withInsurance) {
		if (this.reservationBalance != null) {
			return BigDecimal.ZERO;
		} else {
			return super.getAncillaryTotalExternalCharge(withInsurance);
		}
	}

	public int getPayablePaxCount() {

		if (getPaxList() != null && this.payablePaxCount == 0) {

			for (ReservationPaxTO pax : getPaxList()) {
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					payablePaxCount++;
				}
			}
		}
		return this.payablePaxCount;
	}

	public void setPayablePaxCount(int payablePaxCount) {
		this.payablePaxCount = payablePaxCount;
	}

	/**
	 * @return
	 */
	public int getExternalChargesType() {
		return externalChargesType;
	}

	/**
	 * @param externalChargesType
	 */
	public void setExternalChargesType(int externalChargesType) {
		this.externalChargesType = externalChargesType;
	}

	public void removeFareDiscount() {
		this.fareDiscount = null;
		this.fareDiscountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public void setFareDiscount(Integer fareDiscountPercentage, String fareDiscountNotes, String fareDiscountCode,
			List<PassengerTypeQuantityTO> paxQtyList, boolean isOldFareDiscApplied) {
		fareDiscount = new DiscountedFareDetails();
		fareDiscount.setFarePercentage(fareDiscountPercentage);
		fareDiscount.setNotes(fareDiscountNotes);
		fareDiscount.setCode(fareDiscountCode);
		fareDiscount.setOldFareDiscountApplied(isOldFareDiscApplied);
	}

	public void setFareDiscount(List<PassengerTypeQuantityTO> paxQtyList) {
		if (getPriceInfoTO() != null && getPriceInfoTO().getFareTypeTO() != null) {

			ApplicablePromotionDetailsTO promoDetails = getPriceInfoTO().getFareTypeTO().getPromotionTO();
			setFareDiscountWith(promoDetails, paxQtyList);

		} else {
			removeFareDiscount();
		}
	}

	public void setFareDiscount(List<PassengerTypeQuantityTO> paxQtyList, ApplicablePromotionDetailsTO promoDetails) {
		setFareDiscountWith(promoDetails, paxQtyList);
	}

	private void setFareDiscountWith(ApplicablePromotionDetailsTO promoDetails, List<PassengerTypeQuantityTO> paxQtyList) {

		if (promoDetails != null) {
			if (fareDiscount == null) {
				fareDiscount = new DiscountedFareDetails();
			}

			fareDiscount.setPromotionId(promoDetails.getPromoCriteriaId());
			fareDiscount.setPromotionType(promoDetails.getPromoType());
			fareDiscount.setPromoCode(promoDetails.getPromoCode());
			fareDiscount.setDescription(promoDetails.getDescription());
			fareDiscount.setSystemGenerated(promoDetails.isSystemGenerated());
			fareDiscount.setAllowSamePaxOnly(promoDetails.isAllowSamePaxOnly());
			fareDiscount.setOriginPnr(promoDetails.getOriginPnr());
			fareDiscount.setNotes(promoDetails.getPromoName());
			fareDiscount.setFarePercentage(promoDetails.getDiscountValue());
			fareDiscount.setDiscountType(promoDetails.getDiscountType());
			fareDiscount.setDiscountApplyTo(promoDetails.getApplyTo());
			fareDiscount.setDiscountAs(promoDetails.getDiscountAs());
			fareDiscount.addApplicablePaxCount(PaxTypeTO.ADULT, promoDetails.getApplicableAdultCount());
			fareDiscount.addApplicablePaxCount(PaxTypeTO.CHILD, promoDetails.getApplicableChildCount());
			fareDiscount.addApplicablePaxCount(PaxTypeTO.INFANT, promoDetails.getApplicableInfantCount());
			fareDiscount.setApplicableAncillaries(promoDetails.getApplicableAncillaries());
			fareDiscount.setApplicableBINs(promoDetails.getApplicableBINs());
			fareDiscount.setApplicableOnds(promoDetails.getApplicableOnds());

		} else {
			removeFareDiscount();
		}
	}

	public void setDomesticFareDiscount(List<PassengerTypeQuantityTO> paxQtyList, ArrayList<String> domesticSegmentCodeList)
			throws ModuleException {

		if (fareDiscount == null)
			fareDiscount = new DiscountedFareDetails();

		if (getPriceInfoTO() != null && AppSysParamsUtil.isDomesticFareDiscountEnabled() && domesticSegmentCodeList != null
				&& !domesticSegmentCodeList.isEmpty()) {
			fareDiscount.setDomesticDiscountApplied(true);
			fareDiscount.setNotes("Default Domestic Fare Discount");
			fareDiscount.setDomesticSegmentCodeList(domesticSegmentCodeList);
		} else {
			fareDiscount.setDomesticDiscountApplied(false);
			fareDiscount.setDomFareDiscountPercentage(0);
			fareDiscount.setDomesticSegmentCodeList(null);
		}
	}

	@Override
	public void setPriceInfoTO(PriceInfoTO priceInfoTO) {
		removeFareDiscount();
		resetSelectedAncillary();
		super.setPriceInfoTO(priceInfoTO);
	}

	public float getFareDiscountPercentage() {
		float farePercentage = 0;
		if (fareDiscount != null) {
			farePercentage = fareDiscount.getFarePercentage();
		}
		return farePercentage;
	}

	private BigDecimal deductFareDiscount(BigDecimal amt) {
		return AccelAeroCalculator.subtract(amt, getTotalFareDiscount(true));
	}

	public BigDecimal getTotalFareDiscount(boolean excludeCreditDiscount) {
		BigDecimal discountAmount = fareDiscountAmount;
		if (excludeCreditDiscount && this.fareDiscount != null && this.fareDiscount.getPromotionId() != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(this.fareDiscount.getDiscountAs())) {
			discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		return discountAmount;
	}

	public DiscountedFareDetails getFareDiscount() {
		return fareDiscount;
	}

	public void setFareDiscount(DiscountedFareDetails fareDiscount) {
		this.fareDiscount = fareDiscount;
	}

	public BigDecimal getFareDiscountAmount() {
		return fareDiscountAmount;
	}

	public void setFareDiscountAmount(BigDecimal fareDiscountAmount) {
		this.fareDiscountAmount = fareDiscountAmount;
	}

	public boolean isSkipPromotionForOnHold() {
		return skipPromotionForOnHold;
	}

	public void setSkipPromotionForOnHold(boolean skipPromotionForOnHold) {
		this.skipPromotionForOnHold = skipPromotionForOnHold;
	}

	public String getTotAgentCommission() {
		return totAgentCommission;
	}

	public void setTotAgentCommission(String totAgentCommission) {
		this.totAgentCommission = totAgentCommission;
	}

	public AgentCommissionDetails getAgentCommissions() {
		return agentCommissions;
	}

	public void setAgentCommissions(AgentCommissionDetails agentCommissions) {
		this.agentCommissions = agentCommissions;
	}

	public boolean isRestrictCommissionVisibility() {
		return restrictCommissionVisibility;
	}

	public void setRestrictCommissionVisibility(boolean restrictCommissionVisibility) {
		this.restrictCommissionVisibility = restrictCommissionVisibility;
	}

	public Set<ServiceTaxContainer> getApplicableServiceTaxes() {
		if (this.applicableServiceTaxes == null) {
			this.applicableServiceTaxes = new HashSet<ServiceTaxContainer>();
		}

		return this.applicableServiceTaxes;
	}

	public void addServiceTax(boolean isTaxApplicable, BigDecimal taxRatio, EXTERNAL_CHARGES externalCharge) {
		ServiceTaxContainer serviceTaxContainer = new ServiceTaxContainer(isTaxApplicable, taxRatio, externalCharge);
		getApplicableServiceTaxes().add(serviceTaxContainer);
	}

	public void addNewServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		getApplicableServiceTaxes().clear();
		if (applicableServiceTaxes != null) {
			getApplicableServiceTaxes().addAll(applicableServiceTaxes);
		}
	}

	public void addServiceTaxes(Set<ServiceTaxContainer> applicableServiceTaxes) {
		if (applicableServiceTaxes != null) {
			getApplicableServiceTaxes().addAll(applicableServiceTaxes);
		}
	}

	public ServiceTaxContainer getServiceTax(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer;
			}
		}

		return null;
	}

	public BigDecimal getExtFeeAmount() {
		if (this.reservationBalance != null) {
			return this.reservationBalance.getSegmentSummary().getNewExtraFeeAmount();
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public boolean isTaxApplicable(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer.isTaxApplicable();
			}
		}

		return false;
	}

	public BigDecimal getServiceTaxRatio(EXTERNAL_CHARGES externalCharge) {
		for (ServiceTaxContainer serviceTaxContainer : getApplicableServiceTaxes()) {
			if (serviceTaxContainer != null && serviceTaxContainer.getExternalCharge() == externalCharge) {
				return serviceTaxContainer.getTaxRatio();
			}
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public BigDecimal getServiceChargeTax(EXTERNAL_CHARGES externalCharge) {
		BigDecimal serviceChargeTax = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (this.getSelectedExternalCharge(externalCharge) != null) {
			serviceChargeTax = this.getSelectedExternalCharge(externalCharge).getTotalAmount();
		}
		return serviceChargeTax;
	}

	public ReservationDiscountDTO getReservationDiscountDTO() {
		return reservationDiscountDTO;
	}

	public void setReservationDiscountDTO(ReservationDiscountDTO reservationDiscountDTO) {
		this.reservationDiscountDTO = reservationDiscountDTO;
	}

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	public int getInvalidVoucherRedeemAttempts() {
		return invalidVoucherRedeemAttempts;
	}

	public void setInvalidVoucherRedeemAttempts(int invalidVoucherRedeemAttempts) {
		this.invalidVoucherRedeemAttempts = invalidVoucherRedeemAttempts;
	}

	public int getPayableInfantPaxCount() {
		int paxCount = 0;
		if (getPaxList() != null) {
			for (ReservationPaxTO pax : getPaxList()) {
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					if (pax.getIsParent()) {
						paxCount++;
					}
				}
			}
		}
		return paxCount;
	}

	public List<VoucherDTO> getRedeemedVoucherList() {
		if (redeemedVoucherList == null) {
			redeemedVoucherList = new ArrayList<VoucherDTO>();
		}
		return redeemedVoucherList;
	}

	public void setRedeemedVoucherList(List<VoucherDTO> redeemedVoucherList) {
		this.redeemedVoucherList = redeemedVoucherList;
	}

}
