package com.isa.thinair.xbe.api.dto.v2;

public class ModifySegmentTO {

	private String PNR;

	private String mode;

	private String modifyingSegments;

	private String from;

	private String to;

	private String departureDate;

	private String version;

	private String bookingType;

	private String bookingclass;

	private int noOfAdults;

	private int noOfChildren;

	private int noOfInfants;

	private String selectedCurrency;

	private String groupPNR;

	private String ownerAgentCode;

	private String firstDepature;

	private String lastArrival;

	private String status;

	private boolean isModifyingSurfaceSegment;

	private String foidIds = "";

	private String foidCode = "";

	private String[] segmentList;
	/* segment modification */
	private boolean isAllowModifyByDate;

	private boolean isAllowModifyByRoute;

	/** The carrier which operates the segment being modified */
	private String modifyingSegmentOperatingCarrier;

	// open return confirm before date
	private String confirmBeforeDate;
	/** Interline flag **/
	private boolean isInterline;

	private boolean ticketExpiryEnabled;

	private String ticketExpiryDate;

	private String ticketValidFromDate;
	
	private boolean alertSegmentTransfer;
	
	private String alertSegmentID;

	private boolean requote;

	private String ohdReleaseTime;
	
	private boolean isGdsPnr;

	private boolean taxInvoicesExist;

               
	public void setOhdReleaseTime(String s){              
		this.ohdReleaseTime = s;
		
	}
	/**
         * @return the  ohdReleaseTime
         */

	public String getOhdReleaseTime(){
		return ohdReleaseTime;
	}
	/**
	 * @return the pNR
	 */
	public String getPNR() {
		return PNR;
	}

	/**
	 * @param pNR
	 *            the pNR to set
	 */
	public void setPNR(String pNR) {
		PNR = pNR;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getModifyingSegments() {
		return modifyingSegments;
	}

	public void setModifyingSegments(String modifyingSegments) {
		this.modifyingSegments = modifyingSegments;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getBookingclass() {
		return bookingclass;
	}

	public void setBookingclass(String bookingclass) {
		this.bookingclass = bookingclass;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public String getFirstDepature() {
		return firstDepature;
	}

	public void setFirstDepature(String firstDepature) {
		this.firstDepature = firstDepature;
	}

	public String getLastArrival() {
		return lastArrival;
	}

	public void setLastArrival(String lastArrival) {
		this.lastArrival = lastArrival;
	}

	/**
	 * @return the isModifyingSurfaceSegment
	 */
	public boolean isModifyingSurfaceSegment() {
		return isModifyingSurfaceSegment;
	}

	/**
	 * @param isModifyingSurfaceSegment
	 *            the isModifyingSurfaceSegment to set
	 */
	public void setModifyingSurfaceSegment(boolean isModifyingSurfaceSegment) {
		this.isModifyingSurfaceSegment = isModifyingSurfaceSegment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFoidIds() {
		return foidIds;
	}

	public void setFoidIds(String foidIds) {
		this.foidIds = foidIds;
	}

	public String getFoidCode() {
		return foidCode;
	}

	public void setFoidCode(String foidCode) {
		this.foidCode = foidCode;
	}

	public String[] getSegmentList() {
		return segmentList;
	}

	public void setSegmentList(String[] segmentList) {
		this.segmentList = segmentList;
	}

	/* segment modification */
	public boolean getIsAllowModifyByDate() {
		return isAllowModifyByDate;
	}

	public void setIsAllowModifyByDate(boolean isAllowModifyByDate) {
		this.isAllowModifyByDate = isAllowModifyByDate;
	}

	public boolean getIsAllowModifyByRoute() {
		return isAllowModifyByRoute;
	}

	public void setIsAllowModifyByRoute(boolean isAllowModifyByRoute) {
		this.isAllowModifyByRoute = isAllowModifyByRoute;
	}

	/**
	 * @return the operating carrier of the segment being modified.
	 */
	public String getModifyingSegmentOperatingCarrier() {
		return modifyingSegmentOperatingCarrier;
	}

	/**
	 * @param modifyingSegmentOperatingCarrier
	 *            the operating carrier of the segment being modified
	 */
	public void setModifyingSegmentOperatingCarrier(String modifyingSegmentOperatingCarrier) {
		this.modifyingSegmentOperatingCarrier = modifyingSegmentOperatingCarrier;
	}

	public String getConfirmBeforeDate() {
		return confirmBeforeDate;
	}

	/**
	 * @param confirmBeforeDate
	 */
	public void setConfirmBeforeDate(String confirmBeforeDate) {
		this.confirmBeforeDate = confirmBeforeDate;
	}

	/**
	 * 
	 * @return the isInterline
	 */

	public boolean getIsInterline() {
		return isInterline;
	}

	/**
	 * @param isInterline
	 *            the isInterline to set
	 */
	public void setInterline(boolean isInterline) {
		this.isInterline = isInterline;
	}

	public boolean isTicketExpiryEnabled() {
		return ticketExpiryEnabled;
	}

	public void setTicketExpiryEnabled(boolean ticketExpiryEnabled) {
		this.ticketExpiryEnabled = ticketExpiryEnabled;
	}

	public String getTicketExpiryDate() {
		return ticketExpiryDate;
	}

	public void setTicketExpiryDate(String ticketExpiryDate) {
		this.ticketExpiryDate = ticketExpiryDate;
	}

	public String getTicketValidFromDate() {
		return ticketValidFromDate;
	}

	public void setTicketValidFromDate(String ticketValidFromDate) {
		this.ticketValidFromDate = ticketValidFromDate;
	}

	public boolean isRequote() {
		return requote;
	}

	public void setRequote(boolean requote) {
		this.requote = requote;
	}

	public boolean isAlertSegmentTransfer() {
		return alertSegmentTransfer;
	}

	public void setAlertSegmentTransfer(boolean alertSegmentTransfer) {
		this.alertSegmentTransfer = alertSegmentTransfer;
	}

	public String getAlertSegmentID() {
		return alertSegmentID;
	}

	public void setAlertSegmentID(String alertSegmentID) {
		this.alertSegmentID = alertSegmentID;
	}

	public boolean isGdsPnr() {
		return isGdsPnr;
	}

	public void setGdsPnr(boolean isGdsPnr) {
		this.isGdsPnr = isGdsPnr;
	}

	public boolean isTaxInvoicesExist() {
		return taxInvoicesExist;
	}

	public void setIsTaxInvoicesAvailable(boolean isTaxInvoicesAvailable) {
		this.taxInvoicesExist = isTaxInvoicesAvailable;
	}
}