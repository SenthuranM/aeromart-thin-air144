package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.math.BigDecimal;

public class NoShowPassenger implements Serializable{
	
	private static final long serialVersionUID = -6755559298298260934L;
	
	private BigDecimal refundableTaxTotal;
	
	private BigDecimal refundedTaxTotal;
	
	private boolean isRefundable;
	
	private String travellerRef;
	
	private String passengerName;
	
	private String infantName;
	
	private String PaxType;

	public BigDecimal getRefundableTaxTotal() {
		return refundableTaxTotal;
	}

	public void setRefundableTaxTotal(BigDecimal refundableTaxTotal) {
		this.refundableTaxTotal = refundableTaxTotal;
	}

	public BigDecimal getRefundedTaxTotal() {
		return refundedTaxTotal;
	}

	public void setRefundedTaxTotal(BigDecimal refundedTaxTotal) {
		this.refundedTaxTotal = refundedTaxTotal;
	}

	public boolean isRefundable() {
		return isRefundable;
	}

	public void setRefundable(boolean isRefundable) {
		this.isRefundable = isRefundable;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getPaxType() {
		return PaxType;
	}

	public void setPaxType(String paxType) {
		PaxType = paxType;
	}

	public String getInfantName() {
		return infantName;
	}

	public void setInfantName(String infantName) {
		this.infantName = infantName;
	}

	public String getTravellerRef() {
		return travellerRef;
	}

	public void setTravellerRef(String travellerRef) {
		this.travellerRef = travellerRef;
	}

}
