package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientAlertTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;

/**
 * Used to store all the session data used in the Reservation Not to be loaded to the session. pass the data through
 * request
 * 
 * @author Thushara
 * 
 */
public class XBESessionDTO implements Serializable {

	private static final long serialVersionUID = -1925760396657597567L;

	private String pnr;

	private Collection<LCCClientReservationSegment> pnrSegments;

	private Collection<LCCClientReservationPax> pnrPaxs;

	private Collection<LCCClientCarrierOndGroup> pnrOndGroups;

	private Collection<LCCClientAlertInfoTO> pnrAlerts;

	private CommonReservationContactInfo contactInfo;

	private Integer noOfAdults = 0;

	private Integer noOfClidren = 0;

	private Integer noOfInfants = 0;

	private PaymentSummaryTO paymentSummary;

	private String ownerAgentCode;

	private Collection<LCCClientAlertInfoTO> flexiPnrAlerts;

	private Collection<LCCClientAlertInfoTO> additionalSegInfo;

	private Collection<LCCClientAlertInfoTO> openReturnSegInfo;

	private Collection<LccClientPassengerEticketInfoTO> passengerTktCoupons;

	private LCCClientAlertTO autoCancellationAlert;

	private Collection<LCCClientAlertTO> promotionAlerts;

	private boolean creditPromotion;

	public Collection<LCCClientReservationSegment> getPnrSegments() {
		return pnrSegments;
	}

	public void setPnrSegments(Collection<LCCClientReservationSegment> pnrSegments) {
		this.pnrSegments = pnrSegments;
	}

	public Collection<LCCClientReservationPax> getPnrPaxs() {
		return pnrPaxs;
	}

	public void setPnrPaxs(Collection<LCCClientReservationPax> pnrPaxs) {
		this.pnrPaxs = pnrPaxs;
	}

	public Collection<LCCClientCarrierOndGroup> getPnrOndGroups() {
		return pnrOndGroups;
	}

	public void setPnrOndGroups(Collection<LCCClientCarrierOndGroup> pnrOndGroups) {
		this.pnrOndGroups = pnrOndGroups;
	}

	public Collection<LCCClientAlertInfoTO> getPnrAlerts() {
		return pnrAlerts;
	}

	public void setPnrAlerts(Collection<LCCClientAlertInfoTO> pnrAlerts) {
		this.pnrAlerts = pnrAlerts;
	}

	public Integer getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(Integer noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public Integer getNoOfClidren() {
		return noOfClidren;
	}

	public void setNoOfClidren(Integer noOfClidren) {
		this.noOfClidren = noOfClidren;
	}

	public Integer getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(Integer noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public PaymentSummaryTO getPaymentSummary() {
		return paymentSummary;
	}

	public void setPaymentSummary(PaymentSummaryTO paymentSummary) {
		this.paymentSummary = paymentSummary;
	}

	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Collection<LCCClientAlertInfoTO> getFlexiPnrAlerts() {
		return flexiPnrAlerts;
	}

	public void setFlexiPnrAlerts(Collection<LCCClientAlertInfoTO> flexiPnrAlerts) {
		this.flexiPnrAlerts = flexiPnrAlerts;
	}

	public Collection<LCCClientAlertInfoTO> getAdditionalSegInfo() {
		return additionalSegInfo;
	}

	public void setAdditionalSegInfo(Collection<LCCClientAlertInfoTO> additionalSegInfo) {
		this.additionalSegInfo = additionalSegInfo;
	}

	public Collection<LCCClientAlertInfoTO> getOpenReturnSegInfo() {
		return openReturnSegInfo;
	}

	public void setOpenReturnSegInfo(Collection<LCCClientAlertInfoTO> openReturnSegInfo) {
		this.openReturnSegInfo = openReturnSegInfo;
	}

	public Collection<LccClientPassengerEticketInfoTO> getPassengerTktCoupons() {
		return passengerTktCoupons;
	}

	public void setPassengerTktCoupons(Collection<LccClientPassengerEticketInfoTO> passengerTktCoupons) {
		this.passengerTktCoupons = passengerTktCoupons;
	}

	public LCCClientAlertTO getAutoCancellationAlert() {
		return autoCancellationAlert;
	}

	public void setAutoCancellationAlert(LCCClientAlertTO autoCancellationAlert) {
		this.autoCancellationAlert = autoCancellationAlert;
	}

	public Collection<LCCClientAlertTO> getPromotionAlerts() {
		return promotionAlerts;
	}

	public void setPromotionAlerts(Collection<LCCClientAlertTO> promotionAlerts) {
		this.promotionAlerts = promotionAlerts;
	}

	public boolean isCreditPromotion() {
		return creditPromotion;
	}

	public void setCreditPromotion(boolean creditPromotion) {
		this.creditPromotion = creditPromotion;
	}

}