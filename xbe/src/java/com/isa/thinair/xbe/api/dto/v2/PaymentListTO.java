package com.isa.thinair.xbe.api.dto.v2;

import java.util.Date;

public class PaymentListTO {

	private String displayDate;

	private String displayMethod;

	private String displayAmount;

	private String displayPayAmount;

	private String displayNotes;

	private String displayCarrierCode;

	private String hdnAuthorizationId;

	private String hdnPayAmount;

	private String hdnPaidCurrency;

	private String hdnPaidAirLineCurrency;

	private String hdnPayRef;

	private String hdnActualPaymentMethod;

	private Date hdnPaydate;

	private String recieptNumber;

	private String enableRecieptPrint;

	private String hdnCarrierVisePayments;

	private String hdnLccUniqueTnxId;

	private String paymentMethod;

	private boolean supportOnlyFullRefund = false;
	
	private String paymentBrokerRefNo;
	
	private String displayRefundableAmount;
	
	private String displayPaxName;
	
	private boolean nonRefundable = false;

	/**
	 * @return the displayDate
	 */
	public String getDisplayDate() {
		return displayDate;
	}

	public String getHdnPaidAirLineCurrency() {
		return hdnPaidAirLineCurrency;
	}

	public void setHdnPaidAirLineCurrency(String hdnPaidAirLineCurrency) {
		this.hdnPaidAirLineCurrency = hdnPaidAirLineCurrency;
	}

	/**
	 * @param displayDate
	 *            the displayDate to set
	 */
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}

	/**
	 * @return the displayMethod
	 */
	public String getDisplayMethod() {
		return displayMethod;
	}

	/**
	 * @param displayMethod
	 *            the displayMethod to set
	 */
	public void setDisplayMethod(String displayMethod) {
		this.displayMethod = displayMethod;
	}

	/**
	 * @return the displayAmount
	 */
	public String getDisplayAmount() {
		return displayAmount;
	}

	/**
	 * @param displayAmount
	 *            the displayAmount to set
	 */
	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}

	/**
	 * @return the displayPayAmount
	 */
	public String getDisplayPayAmount() {
		return displayPayAmount;
	}

	/**
	 * @param displayPayAmount
	 *            the displayPayAmount to set
	 */
	public void setDisplayPayAmount(String displayPayAmount) {
		this.displayPayAmount = displayPayAmount;
	}

	/**
	 * @return the displayNotes
	 */
	public String getDisplayNotes() {
		return displayNotes;
	}

	/**
	 * @param displayNotes
	 *            the displayNotes to set
	 */
	public void setDisplayNotes(String displayNotes) {
		this.displayNotes = displayNotes;
	}

	public String getDisplayCarrierCode() {
		return displayCarrierCode;
	}

	public void setDisplayCarrierCode(String displayCarrierCode) {
		this.displayCarrierCode = displayCarrierCode;
	}

	public String getHdnAuthorizationId() {
		return hdnAuthorizationId;
	}

	public void setHdnAuthorizationId(String hdnAuthorizationId) {
		this.hdnAuthorizationId = hdnAuthorizationId;
	}

	public String getHdnPayAmount() {
		return hdnPayAmount;
	}

	public void setHdnPayAmount(String hdnPayAmount) {
		this.hdnPayAmount = hdnPayAmount;
	}

	public String getHdnPaidCurrency() {
		return hdnPaidCurrency;
	}

	public void setHdnPaidCurrency(String hdnPaidCurrency) {
		this.hdnPaidCurrency = hdnPaidCurrency;
	}

	public String getHdnPayRef() {
		return hdnPayRef;
	}

	public void setHdnPayRef(String hdnPayRef) {
		this.hdnPayRef = hdnPayRef;
	}

	public String getHdnActualPaymentMethod() {
		return hdnActualPaymentMethod;
	}

	public void setHdnActualPaymentMethod(String hdnActualPaymentMethod) {
		this.hdnActualPaymentMethod = hdnActualPaymentMethod;
	}

	public Date getHdnPaydate() {
		return hdnPaydate;
	}

	public void setHdnPaydate(Date hdnPaydate) {
		this.hdnPaydate = hdnPaydate;
	}

	public String getRecieptNumber() {
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	public String getEnableRecieptPrint() {
		return enableRecieptPrint;
	}

	public void setEnableRecieptPrint(String enableRecieptPrint) {
		this.enableRecieptPrint = enableRecieptPrint;
	}

	/**
	 * @return the hdnCarrierVisePayments
	 */
	public String getHdnCarrierVisePayments() {
		return hdnCarrierVisePayments;
	}

	/**
	 * @param hdnCarrierVisePayments
	 *            the hdnCarrierVisePayments to set
	 */
	public void setHdnCarrierVisePayments(String hdnCarrierVisePayments) {
		this.hdnCarrierVisePayments = hdnCarrierVisePayments;
	}

	/**
	 * @return the hdnLccUniqueTnxId
	 */
	public String getHdnLccUniqueTnxId() {
		return hdnLccUniqueTnxId;
	}

	/**
	 * @param hdnLccUniqueTnxId
	 *            the hdnLccUniqueTnxId to set
	 */
	public void setHdnLccUniqueTnxId(String hdnLccUniqueTnxId) {
		this.hdnLccUniqueTnxId = hdnLccUniqueTnxId;
	}

	/**
	 * @return
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod
	 */
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the supportOnlyFullRefund
	 */
	public boolean isSupportOnlyFullRefund() {
		return supportOnlyFullRefund;
	}

	/**
	 * @param supportOnlyFullRefund
	 *            the supportOnlyFullRefund to set
	 */
	public void setSupportOnlyFullRefund(boolean supportOnlyFullRefund) {
		this.supportOnlyFullRefund = supportOnlyFullRefund;
	}

	public String getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setPaymentBrokerRefNo(String paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public String getDisplayRefundableAmount() {
		return displayRefundableAmount;
	}

	public void setDisplayRefundableAmount(String displayRefundableAmount) {
		this.displayRefundableAmount = displayRefundableAmount;
	}

	public String getDisplayPaxName() {
		return displayPaxName;
	}

	public void setDisplayPaxName(String displayPaxName) {
		this.displayPaxName = displayPaxName;
	}

	public boolean isNonRefundable() {
		return nonRefundable;
	}

	public void setNonRefundable(boolean nonRefundable) {
		this.nonRefundable = nonRefundable;
	}	
}
