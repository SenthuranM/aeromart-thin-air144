package com.isa.thinair.xbe.api.dto.v2;

public class UserNoteHistoryTO {

	private String displayUserName;
	private String displayUserNote;
	private String displayModifyDate;
	private String displayAction;
	private String displaySystemNote;
	private String displayCarrierCode;

	public String getDisplayUserName() {
		return displayUserName;
	}

	public void setDisplayUserName(String displayUserName) {
		this.displayUserName = displayUserName;
	}

	public String getDisplayUserNote() {
		return displayUserNote;
	}

	public void setDisplayUserNote(String displayUserNote) {
		this.displayUserNote = displayUserNote;
	}

	public String getDisplayModifyDate() {
		return displayModifyDate;
	}

	public void setDisplayModifyDate(String displayModifyDate) {
		this.displayModifyDate = displayModifyDate;
	}

	public String getDisplayAction() {
		return displayAction;
	}

	public void setDisplayAction(String displayAction) {
		this.displayAction = displayAction;
	}

	public String getDisplaySystemNote() {
		return displaySystemNote;
	}

	public void setDisplaySystemNote(String displaySystemNote) {
		this.displaySystemNote = displaySystemNote;
	}

	public String getDisplayCarrierCode() {
		return displayCarrierCode;
	}

	public void setDisplayCarrierCode(String displayCarrierCode) {
		this.displayCarrierCode = displayCarrierCode;
	}

}
