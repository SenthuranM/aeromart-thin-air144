package com.isa.thinair.xbe.api.dto.v2;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;

public class XBEReservationContactInfoDTO {

	private String country;
	private String state;
	private String taxRegNo;
	private String streetAddress1;
	private String streetAddress2;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTaxRegNo() {
		return taxRegNo;
	}

	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public static XBEReservationContactInfoDTO getContactInfoDTO(CommonReservationContactInfo reservationContact) {
		XBEReservationContactInfoDTO contactInfoDTO = new XBEReservationContactInfoDTO();
		if (reservationContact != null) {
			contactInfoDTO.setCountry(reservationContact.getCountryCode());
			contactInfoDTO.setState(reservationContact.getState());
			contactInfoDTO.setTaxRegNo(reservationContact.getTaxRegNo());
			contactInfoDTO.setStreetAddress1(reservationContact.getStreetAddress1());
			contactInfoDTO.setStreetAddress2(reservationContact.getStreetAddress2());
		}
		return contactInfoDTO;
	}
}
