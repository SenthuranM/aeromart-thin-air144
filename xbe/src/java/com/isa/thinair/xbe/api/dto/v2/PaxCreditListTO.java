package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;

public class PaxCreditListTO {
	private String pnrNo;

	private String paxName;

	private String pnrStatus;

	private String paxCount;

	private String carrier;

	private List<FlightInfoTO> flightInfo;

	private Collection<PaxCreditTO> paxCredits;

	/**
	 * @return the pnrNo
	 */
	public String getPnrNo() {
		return pnrNo;
	}

	/**
	 * @param pnrNo
	 *            the pnrNo to set
	 */
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the pnrStatus
	 */
	public String getPnrStatus() {
		return pnrStatus;
	}

	/**
	 * @param pnrStatus
	 *            the pnrStatus to set
	 */
	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	/**
	 * @return the paxCount
	 */
	public String getPaxCount() {
		return paxCount;
	}

	/**
	 * @param paxCount
	 *            the paxCount to set
	 */
	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}

	/**
	 * @return the flightInfo
	 */
	public List<FlightInfoTO> getFlightInfo() {
		return flightInfo;
	}

	/**
	 * @param flightInfo
	 *            the flightInfo to set
	 */
	public void setFlightInfo(List<FlightInfoTO> flightInfo) {
		this.flightInfo = flightInfo;
	}

	/**
	 * @return the paxCredits
	 */
	public Collection<PaxCreditTO> getPaxCredits() {
		return paxCredits;
	}

	/**
	 * @param paxCredits
	 *            the paxCredits to set
	 */
	public void setPaxCredits(Collection<PaxCreditTO> paxCredits) {
		this.paxCredits = paxCredits;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier
	 *            the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
}
