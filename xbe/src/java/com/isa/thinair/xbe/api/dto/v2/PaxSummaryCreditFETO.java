package com.isa.thinair.xbe.api.dto.v2;

import java.math.BigDecimal;

public class PaxSummaryCreditFETO extends PaxCreditFETO {

	private boolean hasPayment = false;
	private Integer displayPaxID = null;
	private BigDecimal displayUsedCredit = null;
	private BigDecimal displayTotal = null;
	private String paxType;

	public boolean isHasPayment() {
		return hasPayment;
	}

	public void setHasPayment(String hasPayment) {
		this.hasPayment = "true".equals(hasPayment);
	}

	public Integer getDisplayPaxID() {
		return displayPaxID;
	}

	public void setDisplayPaxID(Integer displayPaxID) {
		this.displayPaxID = displayPaxID;
	}

	public BigDecimal getDisplayUsedCredit() {
		return displayUsedCredit;
	}

	public void setDisplayUsedCredit(BigDecimal displayUsedCredit) {
		this.displayUsedCredit = displayUsedCredit;
	}

	public BigDecimal getDisplayTotal() {
		return displayTotal;
	}

	public void setDisplayTotal(BigDecimal displayTotal) {
		this.displayTotal = displayTotal;
	}

	public boolean isFullyPaidByCredit() {
		return getDisplayTotal().equals(getDisplayUsedCredit()) && getDisplayToPay().compareTo(BigDecimal.ZERO) == 0;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
