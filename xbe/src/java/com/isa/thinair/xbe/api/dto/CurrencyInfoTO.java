package com.isa.thinair.xbe.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CurrencyInfoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6609547634954522781L;
	private String currencyCode = null;
	private String currencyDesc = null;
	private BigDecimal currencyExchangeRate = null;
	private Date exchangeRateTimestamp = null;
	private BigDecimal boundryValue;
	private BigDecimal breakPointValue;
	private Boolean isApplyRounding = null;
	private Boolean isCardPaymentEnabled = null;
	private Integer defaultIpgId = null;

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the currencyDesc
	 */
	public String getCurrencyDesc() {
		return currencyDesc;
	}

	/**
	 * @param currencyDesc
	 *            the currencyDesc to set
	 */
	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	/**
	 * @return the currencyExchangeRate
	 */
	public BigDecimal getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	/**
	 * @param currencyExchangeRate
	 *            the currencyExchangeRate to set
	 */
	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	/**
	 * @return the exchangeRateTimestamp
	 */
	public Date getExchangeRateTimestamp() {
		return exchangeRateTimestamp;
	}

	/**
	 * @param exchangeRateTimestamp
	 *            the exchangeRateTimestamp to set
	 */
	public void setExchangeRateTimestamp(Date exchangeRateTimestamp) {
		this.exchangeRateTimestamp = exchangeRateTimestamp;
	}

	/**
	 * @return the boundryValue
	 */
	public BigDecimal getBoundryValue() {
		return boundryValue;
	}

	/**
	 * @param boundryValue
	 *            the boundryValue to set
	 */
	public void setBoundryValue(BigDecimal boundryValue) {
		this.boundryValue = boundryValue;
	}

	/**
	 * @return the breakPointValue
	 */
	public BigDecimal getBreakPointValue() {
		return breakPointValue;
	}

	/**
	 * @param breakPointValue
	 *            the breakPointValue to set
	 */
	public void setBreakPointValue(BigDecimal breakPointValue) {
		this.breakPointValue = breakPointValue;
	}

	/**
	 * @return the isApplyRounding
	 */
	public Boolean getIsApplyRounding() {
		return isApplyRounding;
	}

	/**
	 * @param isApplyRounding
	 *            the isApplyRounding to set
	 */
	public void setIsApplyRounding(Boolean isApplyRounding) {
		this.isApplyRounding = isApplyRounding;
	}

	/**
	 * @return the isCardPaymentEnabled
	 */
	public Boolean getIsCardPaymentEnabled() {
		return isCardPaymentEnabled;
	}

	/**
	 * @param isCardPaymentEnabled
	 *            the isCardPaymentEnabled to set
	 */
	public void setIsCardPaymentEnabled(Boolean isCardPaymentEnabled) {
		this.isCardPaymentEnabled = isCardPaymentEnabled;
	}

	public Integer getDefaultIpgId() {
		return defaultIpgId;
	}

	public void setDefaultIpgId(Integer defaultIpgId) {
		this.defaultIpgId = defaultIpgId;
	}
}
