package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;

public class BookingTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6894632047863283649L;

	private String PNR;

	private boolean groupPNR;

	private String status;

	private String displayStatus;

	private String bookingDate;

	private String agent;

	private String totalPaxAdultCount;

	private String totalPaxChildCount;

	private String totalPaxInfantCount;

	private String version;

	private String releaseTime;

	private String loadedByMarketing;

	private String bookingCategory;

	private String itineraryFareMask;

	private boolean ticketExpiryEnabled;

	private String ticketExpiryDate;

	private String ticketValidFromDate;
	
	private boolean useEtsForReservation;

	private String originCountryOfCall;



	/**
	 * @return the pNR
	 */
	public String getPNR() {
		return PNR;
	}

	/**
	 * @param pNR
	 *            the pNR to set
	 */
	public void setPNR(String pNR) {
		PNR = pNR;
	}

	/**
	 * @return the isGroupPNR
	 */
	public boolean isGroupPNR() {
		return groupPNR;
	}

	/**
	 * @param isGroupPNR
	 *            the isGroupPnr to set
	 */
	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the bookingDate
	 */
	public String getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent
	 *            the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the totalPaxAdultCount
	 */
	public String getTotalPaxAdultCount() {
		return totalPaxAdultCount;
	}

	/**
	 * @param totalPaxAdultCount
	 *            the totalPaxAdultCount to set
	 */
	public void setTotalPaxAdultCount(String totalPaxAdultCount) {
		this.totalPaxAdultCount = totalPaxAdultCount;
	}

	/**
	 * @return the totalPaxChildCount
	 */
	public String getTotalPaxChildCount() {
		return totalPaxChildCount;
	}

	/**
	 * @param totalPaxChildCount
	 *            the totalPaxChildCount to set
	 */
	public void setTotalPaxChildCount(String totalPaxChildCount) {
		this.totalPaxChildCount = totalPaxChildCount;
	}

	/**
	 * @return the totalPaxInfantCount
	 */
	public String getTotalPaxInfantCount() {
		return totalPaxInfantCount;
	}

	/**
	 * @param totalPaxInfantCount
	 *            the totalPaxInfantCount to set
	 */
	public void setTotalPaxInfantCount(String totalPaxInfantCount) {
		this.totalPaxInfantCount = totalPaxInfantCount;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}

	/**
	 * @return the loadedByMarketing
	 */
	public String getLoadedByMarketing() {
		return loadedByMarketing;
	}

	/**
	 * @param loadedByMarketing
	 *            the loadedByMarketing to set
	 */
	public void setLoadedByMarketing(String loadedByMarketing) {
		this.loadedByMarketing = loadedByMarketing;
	}

	/**
	 * @return
	 */
	public String getBookingCategory() {
		return bookingCategory;
	}

	/**
	 * @param bookingCategory
	 */
	public void setBookingCategory(String bookingCategory) {
		this.bookingCategory = bookingCategory;
	}

	public String getItineraryFareMask() {
		return itineraryFareMask;
	}

	public void setItineraryFareMask(String itineraryFareMask) {
		this.itineraryFareMask = itineraryFareMask;
	}

	public boolean isTicketExpiryEnabled() {
		return ticketExpiryEnabled;
	}

	public void setTicketExpiryEnabled(boolean ticketExpiryEnabled) {
		this.ticketExpiryEnabled = ticketExpiryEnabled;
	}

	public String getTicketExpiryDate() {
		return ticketExpiryDate;
	}

	public void setTicketExpiryDate(String ticketExpiryDate) {
		this.ticketExpiryDate = ticketExpiryDate;
	}

	public String getTicketValidFromDate() {
		return ticketValidFromDate;
	}

	public void setTicketValidFromDate(String ticketValidFromDate) {
		this.ticketValidFromDate = ticketValidFromDate;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}
	
	public boolean isUseEtsForReservation() {
		return useEtsForReservation;
	}
	

	public void setUseEtsForReservation(boolean useEtsForReservation) {
		this.useEtsForReservation = useEtsForReservation;
	}

	public String getOriginCountryOfCall() {
		return originCountryOfCall;
	}

	public void setOriginCountryOfCall(String originCountryOfCall) {
		this.originCountryOfCall = originCountryOfCall;
	}
	

}