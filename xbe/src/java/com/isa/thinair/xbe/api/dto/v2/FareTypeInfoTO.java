package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareDiscountTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRulesInformationDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiFareRulesInformationDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.webplatform.api.v2.reservation.SurchargeFETO;
import com.isa.thinair.webplatform.api.v2.reservation.TaxFETO;

public class FareTypeInfoTO {

	private String logicalCCtype;

	private boolean showFareRules = false;

	private FareRulesInformationDTO fareRulesInfo;

	private FlexiFareRulesInformationDTO flexiFareRulesInfo;

	private Collection<PaxFareTO> paxFareTOs;

	private Collection<PaxPriceTO> paxPriceTOs;

	private Collection<FareQuoteTO> fareQuoteTO;

	private Collection<AdultPaxTO> paxAdults;

	private Collection<InfantPaxTO> paxInfants;

	private FareQuoteSummaryTO fareQuoteSummaryTO;

	private Collection<SurchargeFETO> surchargeTOs;

	private Collection<TaxFETO> taxTOs;

	private boolean overriddenFare = false;

	private FareDiscountTO fareDiscountInfo;

	private String comment;

	private String totAgentCommission = "0.00";

	private AgentCommissionDetails applicableAgentCommissions;

	/**
	 * App parameter to be passed to the view to indicate whether FareRule information should be displayed in the
	 * FareQuote table summary. Not returned just here for readability.
	 */
	private boolean showFareRuleInfoInSummary;
	
	/**
	 * Default value set to be true as scenarios where all onds doesn't have fare will be explicitly set.
	 */
	private boolean allRequestedOndsHasFares = true;

	public String getLogicalCCtype() {
		return logicalCCtype;
	}

	public void setLogicalCCtype(String logicalCCtype) {
		this.logicalCCtype = logicalCCtype;
	}

	public void setShowFareRules(boolean showFareRules) {
		this.showFareRules = showFareRules;
	}

	public void setPaxFareTOs(Collection<PaxFareTO> paxFareTOs) {
		this.paxFareTOs = paxFareTOs;
	}

	public void setPaxPriceTOs(Collection<PaxPriceTO> paxPriceTOs) {
		this.paxPriceTOs = paxPriceTOs;
	}

	public void setFareQuoteTO(Collection<FareQuoteTO> fareQuoteTO) {
		this.fareQuoteTO = fareQuoteTO;
	}

	public void setPaxAdults(Collection<AdultPaxTO> paxAdults) {
		this.paxAdults = paxAdults;
	}

	public void setPaxInfants(Collection<InfantPaxTO> paxInfants) {
		this.paxInfants = paxInfants;
	}

	public void setFareQuoteSummaryTO(FareQuoteSummaryTO fareQuoteSummaryTO) {
		this.fareQuoteSummaryTO = fareQuoteSummaryTO;
	}

	public void setSurchargeTOs(Collection<SurchargeFETO> surchargeTOs) {
		this.surchargeTOs = surchargeTOs;
	}

	public void setTaxTOs(Collection<TaxFETO> taxTOs) {
		this.taxTOs = taxTOs;
	}

	/**
	 * @return the paxAdults
	 */
	public Collection<AdultPaxTO> getPaxAdults() {
		return paxAdults;
	}

	/**
	 * @return the paxInfants
	 */
	public Collection<InfantPaxTO> getPaxInfants() {
		return paxInfants;
	}

	/**
	 * @return the paxFareDTO
	 */
	public Collection<PaxFareTO> getPaxFareTOs() {
		return paxFareTOs;
	}

	/**
	 * @return the FareQuoteTO
	 */
	public Collection<FareQuoteTO> getFareQuoteTO() {
		return fareQuoteTO;
	}

	/**
	 * @return the FareQuoteSummaryTO
	 */
	public FareQuoteSummaryTO getFareQuoteSummaryTO() {
		return fareQuoteSummaryTO;
	}

	/**
	 * @return the surchargeTOs
	 */
	public Collection<SurchargeFETO> getSurchargeTOs() {
		return surchargeTOs;
	}

	/**
	 * @return the taxTOs
	 */
	public Collection<TaxFETO> getTaxTOs() {
		return taxTOs;
	}

	/**
	 * @return the paxPriceTOs
	 */
	public Collection<PaxPriceTO> getPaxPriceTOs() {
		return paxPriceTOs;
	}

	public boolean isShowFareRules() {
		return showFareRules;
	}

	public FareRulesInformationDTO getFareRulesInfo() {
		return fareRulesInfo;
	}

	public void setFareRulesInfo(FareRulesInformationDTO fareRulesInfo) {
		this.fareRulesInfo = fareRulesInfo;
	}

	public FlexiFareRulesInformationDTO getFlexiFareRulesInfo() {
		return flexiFareRulesInfo;
	}

	public void setFlexiFareRulesInfo(FlexiFareRulesInformationDTO flexiFareRulesInfo) {
		this.flexiFareRulesInfo = flexiFareRulesInfo;
	}

	public boolean isOverriddenFare() {
		return overriddenFare;
	}

	public void setOverriddenFare(boolean overriddenFare) {
		this.overriddenFare = overriddenFare;
	}

	public void setFareDiscountInfo(FareDiscountTO fareDiscountInfo) {
		this.fareDiscountInfo = fareDiscountInfo;
	}

	public FareDiscountTO getFareDiscountInfo() {
		return fareDiscountInfo;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setShowFareRuleInfoInSummary(boolean showFareRuleInfoInSummary) {
		this.showFareRuleInfoInSummary = showFareRuleInfoInSummary;
	}

	public boolean isShowFareRuleInfoInSummary() {
		return AppSysParamsUtil.isShowFareRuleInfoInSummary();
	}

	public String getTotAgentCommission() {
		return totAgentCommission;
	}

	public void setTotAgentCommission(String totAgentCommission) {
		this.totAgentCommission = totAgentCommission;
	}

	public AgentCommissionDetails getApplicableAgentCommissions() {
		return applicableAgentCommissions;
	}

	public void setApplicableAgentCommissions(AgentCommissionDetails applicableAgentCommissions) {
		this.applicableAgentCommissions = applicableAgentCommissions;
	}

	public boolean isAllRequestedOndsHasFares() {
		return allRequestedOndsHasFares;
	}

	public void setAllRequestedOndsHasFares(boolean allRequestedOndsHasFares) {
		this.allRequestedOndsHasFares = allRequestedOndsHasFares;
	}
	
}
