package com.isa.thinair.xbe.api.dto.v2;

import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;

public class PaxTO {

	private String itnSeqNo;

	private String itnPaxName;

	private String itnSeat;

	private String itnMeal;

	private List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries;

	/**
	 * @return the itnSeqNo
	 */
	public String getItnSeqNo() {
		return itnSeqNo;
	}

	/**
	 * @param itnSeqNo
	 *            the itnSeqNo to set
	 */
	public void setItnSeqNo(String itnSeqNo) {
		this.itnSeqNo = itnSeqNo;
	}

	/**
	 * @return the itnPaxName
	 */
	public String getItnPaxName() {
		return itnPaxName;
	}

	/**
	 * @param itnPaxName
	 *            the itnPaxName to set
	 */
	public void setItnPaxName(String itnPaxName) {
		this.itnPaxName = itnPaxName;
	}

	/**
	 * @return the itnSeat
	 */
	public String getItnSeat() {
		return itnSeat;
	}

	/**
	 * @param itnSeat
	 *            the itnSeat to set
	 */
	public void setItnSeat(String itnSeat) {
		this.itnSeat = itnSeat;
	}

	/**
	 * @return the itnMeal
	 */
	public String getItnMeal() {
		return itnMeal;
	}

	/**
	 * @param itnMeal
	 *            the itnMeal to set
	 */
	public void setItnMeal(String itnMeal) {
		this.itnMeal = itnMeal;
	}

	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		return selectedAncillaries;
	}

	public void setSelectedAncillaries(List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}

}
