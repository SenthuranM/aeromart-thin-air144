/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.xbe.core.web.handler.pnladl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.core.config.MailServerConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.pnladl.SendPNLADLHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author srikantha
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */

public final class SendPNLADLRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(SendPNLADLRequestHandler.class);

	private static XBEConfig XBEConfig = new XBEConfig();

	private static final String PARAM_PNL_TIMESTAMP = "hdnPNLTimestamp";

	private static final String PARAM_ADL_TIMESTAMP = "hdnADLTimestamp";

	private static final String PARAM_PNLADL_STATUS = "hdnTransmitStatus";

	private static final String PARAM_ASSIGN_SITA_ADDRESSESS = "hdnSITAValues";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_AIRPORT_CODE = "selAirport";

	private static final String PARAM_SEARCH_FLIGHTNO = "txtFlightNo";

	private static final String PARAM_SEARCH_FLIGHTDATE = "txtDate";

	private static final String PARAM_MAIL_SERVER = "selMailServer";
	private static final String PARAM_SELECTED_SITA = "hdnSITAValues";

	private static final String PARAM_FLIGHT_ID = "hdnFlightID";

	private static final String PARAM_TS_SELECTED = "hdnTS";

	private static final String PARAM_SITA = "hdnRowSITA";

	private static final String PARAM_MSG_TYPE = "hdnmsgType";

	// private static int intSuccess = 0; //0-Not Applicable, 1-SendNewPNLSuccess, 2-SendNewADLSuccess
	// 3-ResendPNLSuccess, 4-ResendADLSuccess 5-ReprintPNLSuccess, 6-ReprintADLSuccess, 7-Fail

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setPNLADLText(HttpServletRequest request, String text) {
		setAttribInRequest(request, "pnlAdlText", new String(text));
	}

	private static String getPNLADLText(HttpServletRequest request) {
		String returnValue = (String) getAttribInRequest(request, "pnlAdlText");
		if (returnValue != null)
			return returnValue;
		else
			return null;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * @param request
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = S2Constants.Result.SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var pnlAdlText =''";
		setExceptionOccured(request, false);
		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable,
																					// 1-SendNewPNLSuccess,
																					// 2-SendNewADLSuccess
																					// 3-ResendPNLSuccess,
																					// 4-ResendADLSuccess
																					// 5-ReprintPNLSuccess,
																					// 6-ReprintADLSuccess, 7-Fail
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if (strHdnMode != null
					&& (strHdnMode.equals(WebConstants.ACTION_NEWPNL) || strHdnMode.equals(WebConstants.ACTION_NEWADL)
							|| strHdnMode.equals(WebConstants.ACTION_RESENDPNL)
							|| strHdnMode.equals(WebConstants.ACTION_RESENDADL)
							|| strHdnMode.equals(WebConstants.ACTION_REPRINT_ADL)
							|| strHdnMode.equals(WebConstants.ACTION_REPRINT_PNL)
							|| strHdnMode.equals(WebConstants.ACTION_PRINT_NEW_ADL)
							|| strHdnMode.equals(WebConstants.ACTION_PRINT_NEW_PNL) || strHdnMode
							.equals(WebConstants.ACTION_PRINT))) {
				if (sendPNLADLData(request)) {
					// if (!isExceptionOccured) {
					if (!isExceptionOccured(request)) {

						// intSuccess=1;
						// strFormData = " var saveSuccess = " + intSuccess + ";"; //0-Not Applicable,
						// 1-SendNewPNLSuccess, 2-SendNewADLSuccess 3-ResendPNLSuccess, 4-ResendADLSuccess
						// 5-ReprintPNLSuccess, 6-ReprintADLSuccess, 7-Fail
						strFormData = " var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable,
																							// 1-SendNewPNLSuccess,
																							// 2-SendNewADLSuccess
																							// 3-ResendPNLSuccess,
																							// 4-ResendADLSuccess
																							// 5-ReprintPNLSuccess,
																							// 6-ReprintADLSuccess,
																							// 7-Fail
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}
			}

			setDisplayData(request);

		} catch (Exception exception) {

			if (exception instanceof RuntimeException) {

				forward = S2Constants.Result.ERROR;
				JavaScriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static boolean sendPNLADLData(HttpServletRequest request) throws Exception {
		String strFlightNo = null;
		String strAirport = null;
		String strPNLTimestamp = null;
		String strADLTimestamp = null;
		String strDateOfFlight = null;
		String strSITAAddress = null;
		String strMailServer = null;
		String strPNLADLStatus = null;
		Date timeStampPNLDate = null;
		Date timeStampADLDate = null;
		Date timeStampSel = null;
		Date dateOfFlight = null;
		String strSelectedSita = null;

		String strPrintText = null;
		String flightId = null;
		String selectedTS = null;
		String selectedSITA = null;
		String messageType = null;
		String strHdnMode = "";
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			strHdnMode = request.getParameter(PARAM_MODE);
			strFlightNo = request.getParameter(PARAM_SEARCH_FLIGHTNO) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTNO);
			strAirport = request.getParameter(PARAM_AIRPORT_CODE) == null ? "" : request.getParameter(PARAM_AIRPORT_CODE);
			strPNLTimestamp = request.getParameter(PARAM_PNL_TIMESTAMP) == null ? "" : request.getParameter(PARAM_PNL_TIMESTAMP);
			strADLTimestamp = request.getParameter(PARAM_ADL_TIMESTAMP) == null ? "" : request.getParameter(PARAM_ADL_TIMESTAMP);
			strDateOfFlight = request.getParameter(PARAM_SEARCH_FLIGHTDATE) == null ? "" : request
					.getParameter(PARAM_SEARCH_FLIGHTDATE);
			strSITAAddress = request.getParameter(PARAM_ASSIGN_SITA_ADDRESSESS) == null ? "" : request
					.getParameter(PARAM_ASSIGN_SITA_ADDRESSESS);
			strMailServer = request.getParameter(PARAM_MAIL_SERVER) == null ? "" : request.getParameter(PARAM_MAIL_SERVER);
			strPNLADLStatus = request.getParameter(PARAM_PNLADL_STATUS) == null ? "" : request.getParameter(PARAM_PNLADL_STATUS);
			strSelectedSita = request.getParameter(PARAM_SELECTED_SITA) == null ? "" : request.getParameter(PARAM_SELECTED_SITA);
			flightId = request.getParameter(PARAM_FLIGHT_ID) == null ? "" : request.getParameter(PARAM_FLIGHT_ID);
			selectedTS = request.getParameter(PARAM_TS_SELECTED) == null ? "" : request.getParameter(PARAM_TS_SELECTED);
			selectedSITA = request.getParameter(PARAM_SITA) == null ? "" : request.getParameter(PARAM_SITA);
			messageType = request.getParameter(PARAM_MSG_TYPE) == null ? "" : request.getParameter(PARAM_MSG_TYPE);

			boolean allowManuallySendPnl= BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MANUALLY_SEND_PNL_ADL);
			SimpleDateFormat dateFormat = null;

			if (!strPNLTimestamp.equals("")) {
				if (strPNLTimestamp.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strPNLTimestamp.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampPNLDate = dateFormat.parse(strPNLTimestamp);
			}

			if (!strADLTimestamp.equals("")) {
				if (strADLTimestamp.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strADLTimestamp.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampADLDate = dateFormat.parse(strADLTimestamp);
			}

			if (!selectedTS.equals("")) {
				if (selectedTS.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (selectedTS.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}

				timeStampSel = dateFormat.parse(selectedTS);
			}

			if (!strDateOfFlight.equals("")) {
				if (strDateOfFlight.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strDateOfFlight.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strDateOfFlight.indexOf(' ') != -1) {
					strDateOfFlight = strDateOfFlight.substring(0, strDateOfFlight.indexOf(' '));
				}

				dateOfFlight = dateFormat.parse(strDateOfFlight);
			}

			Date currentDate = new Date();
			Timestamp timeStampPNL = null;
			Timestamp timeStampADL = null;
			Timestamp selectedTimeStamp = null;
			Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
			if (timeStampPNLDate != null)
				timeStampPNL = new Timestamp(timeStampPNLDate.getTime());
			if (timeStampADLDate != null)
				timeStampADL = new Timestamp(timeStampADLDate.getTime());
			if (selectedTS != null && !selectedTS.equals("")) {
				selectedTimeStamp = new Timestamp(timeStampSel.getTime());
			}

			String arrSITAAddress[] = strSITAAddress.split(",");

			if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_NEWPNL) && allowManuallySendPnl ) {

				ModuleServiceLocator.getReservationAuxilliaryBD().sendPNL(strFlightNo, strAirport, dateOfFlight,
						arrSITAAddress, PNLConstants.SendingMethod.MANUALLY);

				// to set the success message to client
				// intSuccess=1;
				setIntSuccess(request, 1);

				saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_SENDNEW_PNL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
			} else if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_NEWADL) && allowManuallySendPnl) {

				ModuleServiceLocator.getReservationAuxilliaryBD().sendADLAuxiliary(strFlightNo, strAirport, dateOfFlight,
						arrSITAAddress, PNLConstants.SendingMethod.MANUALLY);

				// to set the success message to client
				// intSuccess=2;
				setIntSuccess(request, 2);

				saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_SENDNEW_ADL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
			} else if (!strHdnMode.equals("")
					&& (strHdnMode.equals(WebConstants.ACTION_RESENDPNL) || strHdnMode.equals(WebConstants.ACTION_RESENDADL))) {

				// ModuleServiceLocator.getReservationAuxilliaryBD().resendManualPNL(strFlightNo,strAirport,dateOfFlight,arrSITAAddress,strMailServer,timeStampPNL,"P");
				ModuleServiceLocator.getReservationAuxilliaryBD().resendExistingManualPNLADL(Integer.parseInt(flightId),
						selectedTimeStamp, selectedSITA, strAirport, messageType, strPNLADLStatus, arrSITAAddress, strMailServer,
						strFlightNo, dateOfFlight);

				// intSuccess=3;
				if (messageType.equals("PNL")) {
					setIntSuccess(request, 3);
					saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_RESEND_PNL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
				} else {
					setIntSuccess(request, 4);
					saveMessage(request, XBEConfig.getMessage(WebConstants.KEY_RESEND_ADL_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);
				}

			} else if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				// ModuleServiceLocator.getReservationAuxilliaryBD().getSavedPnlAdl(
				strPrintText = ModuleServiceLocator.getReservationAuxilliaryBD().getSavedPnlAdl(Integer.parseInt(flightId),
						selectedTimeStamp, selectedSITA, strAirport, messageType, strPNLADLStatus);

				if (strPrintText != null) {

					setPNLADLText(request, strPrintText);
				} else {
					setPNLADLText(request, "");
				}
			} else if (!strHdnMode.equals("") && (strHdnMode.equals(WebConstants.ACTION_NEWPNL) || strHdnMode.equals(WebConstants.ACTION_NEWADL)) 
					&& !allowManuallySendPnl){
				setExceptionOccured(request, true);
				setIntSuccess(request, 7);
				saveMessage(request, "Insufficient privilege to send PNL/ADL", WebConstants.MSG_ERROR);
			}

			setExceptionOccured(request, false);
			SimpleDateFormat dateFormataudit = new SimpleDateFormat("yyyy-MM-dd");

			if (!strHdnMode.equals("") && !strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
				contents.put("Action", strHdnMode);
				contents.put("flight id", flightId);
				contents.put("flight No", strFlightNo);
				contents.put("dep date", dateFormataudit.format(dateOfFlight));
				contents.put("Airport", strAirport);
				contents.put("Sita Address", strSITAAddress);
				audit(request, TasksUtil.SITA_TRANSFER_PNL_ADL, contents);
			}

			if (getPNLADLText(request) != null)
				request.setAttribute(WebConstants.REQ_HTML_PNLADL_TEXT_DATA, getPNLADLText(request));
			else if (getPNLADLText(request) != null)
				request.setAttribute(WebConstants.REQ_HTML_PNLADL_TEXT_DATA, getPNLADLText(request));

		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.SendPNLADL() method is failed :" + moduleException.getMessageString(),
					moduleException);
			setExceptionOccured(request, true);
			setIntSuccess(request, 7);
			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strSelectedSita + "';";
			strFormData += "arrFormData[0][2] = '" + strMailServer + "';";

			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable,
																					// 1-SendNewPNLSuccess,
																					// 2-SendNewADLSuccess
																					// 3-ResendPNLSuccess,
																					// 4-ResendADLSuccess
																					// 5-ReprintPNLSuccess,
																					// 6-ReprintADLSuccess, 7-Fail

			strPrintText = "";

			setPNLADLText(request, strPrintText);
			if (!strHdnMode.equals("") && !strHdnMode.equals(WebConstants.ACTION_PRINT)) {
				SimpleDateFormat dateFormataudit = new SimpleDateFormat("yyyy-MM-dd");
				LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
				contents.put("Action", strHdnMode);
				contents.put("flight id", flightId);
				contents.put("flight No", strFlightNo);
				contents.put("dep date", dateFormataudit.format(dateOfFlight));
				contents.put("Airport", strAirport);
				contents.put("Sita Address", strSITAAddress);
				contents.put("Status", moduleException.getMessageString());
				audit(request, TasksUtil.SITA_TRANSFER_PNL_ADL, contents);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			setIntSuccess(request, 7);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}

		return true;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setSendPNLADLRowHtml(request);
		}
		setReqParams(request);
		setAirportList(request);
		if (request.getParameter(PARAM_AIRPORT_CODE) != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setSITAHtml(request);

			setMailServerConfigurations(request);
		}
		setClientErrors(request);
		request.setAttribute(WebConstants.REQ_IS_EXCEPTION, "var isException='" + isExceptionOccured(request) + "';");
		request.setAttribute(WebConstants.REQ_HAS_MANUALLY_SEND_PNL, BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MANUALLY_SEND_PNL_ADL));

		// if (!isExceptionOccured) {
		if (!isExceptionOccured(request)) {

			setPrintData(request);

			String strFormData = "var arrFormData = new Array();";
			// strFormData += " var saveSuccess = " + intSuccess + ";"; //0-Not Applicable, 1-SendNewPNLSuccess,
			// 2-SendNewADLSuccess 3-ResendPNLSuccess, 4-ResendADLSuccess 5-ReprintPNLSuccess, 6-ReprintADLSuccess,
			// 7-Fail
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

	}
	
	

	private static void setPrintData(HttpServletRequest request) throws ModuleException {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String strPNLADLText = "";
		// if( strHdnMode != null &&
		// (strHdnMode.equals(Constants.ACTION_REPRINT_PNL)||strHdnMode.equals(Constants.ACTION_REPRINT_ADL)||strHdnMode.equals(Constants.ACTION_PRINT_NEW_PNL)||strHdnMode.equals(Constants.ACTION_PRINT_NEW_ADL))){
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_PRINT)) {
			// Windows encodes returns as \r\n hex
			if (getPNLADLText(request) != null)
				strPNLADLText = getPNLADLText(request).replaceAll("\n", "<br>");
			// if(getPNLADLText(request)!= null)
			// strPNLADLText = getPNLADLText(request).replaceAll("\n","<br>");
		}
		// String regVar=" var pnlAdlText = '" +strPNLADLText+ "';";
		// strPNLADLText="...AKjhddaskgakjd...";
		String regVar = " var pnlAdlText = '" + strPNLADLText + "';";

		request.setAttribute(WebConstants.REQ_HTML_PNLADL_TEXT_DATA, regVar);

	}

	private static void setSITAHtml(HttpServletRequest request) {
		try {
			String carrierCode = AppSysParamsUtil.extractCarrierCode(request.getParameter(PARAM_SEARCH_FLIGHTNO));
			String flightNumber = (request.getParameter(PARAM_SEARCH_FLIGHTNO) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTNO));
			String strDateOfFlight = request.getParameter(PARAM_SEARCH_FLIGHTDATE) == null ? "" : request.getParameter(PARAM_SEARCH_FLIGHTDATE);
			String strSITAList = JavaScriptGenerator.createActiveSITAHtml(request.getParameter(PARAM_AIRPORT_CODE), flightNumber, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL, strDateOfFlight);
			request.setAttribute(WebConstants.REQ_HTML_SITA_DATA, strSITAList);
		} catch (ModuleException e) {
			log.error("SendPNLADLRequestHandler.setSITAHtml() method is failed :" + e.getMessageString(), e);
		}

	}

	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAirportsList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			// saveMessage(request, moduleException.getExceptionCode() + " ~ " + moduleException.getMessageString(),
			// Constants.MSG_ERROR);
		}
	}

	/**
	 * Set Mail Server Configurations
	 * 
	 * @param request
	 */
	private static void setMailServerConfigurations(HttpServletRequest request) {
		try {
			MailServerConfig defaultMailServerConfig = ModuleServiceLocator.getMessagingBD().getDefaultMailServer();
			Collection colMailServerConfig = ModuleServiceLocator.getMessagingBD().getMailServerConfigs(
					MessagingCustomConstants.MAIL_SERVER_CONFIGS_CODE_PNL_ADL_TRANSMISSION);

			if (defaultMailServerConfig == null) {
				String defaultMailServerJS = "var defaultMailServer = '';";
				request.setAttribute(WebConstants.REQ_HTML_DEFAULT_MAIL_SERVER, defaultMailServerJS);
			} else {
				String defaultMailServerJS = "var defaultMailServer = '" + defaultMailServerConfig.getHostAddress() + "';";
				request.setAttribute(WebConstants.REQ_HTML_DEFAULT_MAIL_SERVER, defaultMailServerJS);
				colMailServerConfig.add(defaultMailServerConfig);
			}

			String strHtml = SelectListGenerator.createMailServerList(colMailServerConfig);
			request.setAttribute(WebConstants.REQ_HTML_MAILSERVER_LIST, strHtml);
		} catch (ModuleException moduleException) {

			log.error("SendPNLADLRequestHandler.setMailServerList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = SendPNLADLHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			// saveMessage(request, moduleException.getExceptionCode() + " ~ " + moduleException.getMessageString(),
			// Constants.MSG_ERROR);
		}
	}
	
	private static void setReqParams(HttpServletRequest request) {
		// TODO decide a way to make this configuration i.e airport wise configuration for pnl sending
		String jsStr = " isDCSWS = false; ";
		if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
			jsStr = " isDCSWS = false; ";
		}
		request.setAttribute(WebConstants.REQ_PARAMS, jsStr);
	}

	private static void setSendPNLADLRowHtml(HttpServletRequest request) throws Exception {

		try {
			SendPNLADLHTMLGenerator htmlGen = new SendPNLADLHTMLGenerator();
			String strHtml = htmlGen.getSendPNLADLRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLHandler.setSendPNLADLRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			// saveMessage(request, moduleException.getExceptionCode() + " ~ " + moduleException.getMessageString(),
			// Constants.MSG_ERROR);
		}
	}
}
