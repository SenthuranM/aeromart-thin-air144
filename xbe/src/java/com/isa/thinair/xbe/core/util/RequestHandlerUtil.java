package com.isa.thinair.xbe.core.util;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class RequestHandlerUtil {
	/**
	 * extracts agent currency from the request.
	 * 
	 * @param request
	 * @return currency
	 * @throws ModuleException
	 */
	public static Currency getAgentCurrency(HttpServletRequest request) throws ModuleException {
		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
		String currencyCode = up.getAgentCurrencyCode();
		Currency currency = null;

		if (currencyCode != null) {
			currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(currencyCode);
		}

		return currency;
	}

	/**
	 * extracts agent currency code from the request.
	 * 
	 * @param request
	 * @return currency code
	 * @throws ModuleException
	 */
	public static String getAgentCurrencyCode(HttpServletRequest request) throws ModuleException {
		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
		return up.getAgentCurrencyCode();
	}

	/**
	 * extracts the current agent's code
	 * 
	 * @param request
	 * @return
	 */
	public static String getAgentCode(HttpServletRequest request) {
		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
		return up.getAgentCode();
	}

	public static String getAgentStationCode(HttpServletRequest request) {
		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
		return up.getAgentStation();
	}

	/**
	 * 
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isPasswordReset(String userId) throws ModuleException {
		return DatabaseUtil.getIsUserPWDReseted(userId);
	}
	
	public static String getCOAgentTaxRegNo(String agentId) throws ModuleException{
		String agentTaxRegNo = "";
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(agentId);
		if(agent.getAgentTypeCode().equals("CO")){
			agentTaxRegNo = PlatformUtiltiies.nullHandler(agent.getTaxRegNo());
		}
		return agentTaxRegNo;
	}

}
