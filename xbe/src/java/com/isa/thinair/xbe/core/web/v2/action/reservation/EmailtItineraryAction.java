package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.CustomizedItineraryUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class EmailtItineraryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(EmailtItineraryAction.class);

	private static String ACTION_EMAIL = "EMAIL";
	private static String ACTION_PRINT = "PRINT";

	private boolean success = true;
	private String messageTxt;

	private String hdnPNRNo;
	private String itineraryLanguage;
	private boolean includePaxFinancials;
	private boolean includePaymentDetails;
	private boolean includeTicketCharges;
	private boolean includeTermsAndConditions = true;
	private boolean groupPNR;
	private String selectedPax;

	private String operationType;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			emailItineraryInfo(false);
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return forward;
	}

	public String printIndividualPaxItinerary() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			emailItineraryInfo(true);
		} catch (Exception e) {
			String msg = BasicRH.getErrorMessage(e,userLanguage);

			forward = S2Constants.Result.ERROR;

			JavaScriptGenerator.setServerError(request, msg, "top", "");
			log.error(msg, e);
		}

		return forward;
	}

	private void emailItineraryInfo(boolean individualPax) throws ModuleException {

		String pnr = hdnPNRNo;
		boolean includeBaggageAllowance = false;
		boolean isbaggageEnable = false;
		boolean isbaggageAllowanceRemarksEnable = false;
		
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		// ReservationUtil.getPnrModesDTO(pnr, isGroupPNR, false);
		ReservationProcessParams resPro = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, true, false, null, null, false, false);
		
		String agentStation = ((UserPrincipal) request.getUserPrincipal()).getAgentStation();

		TrackInfoDTO trackInfoDTO = getTrackInfo();

		LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, groupPNR, itineraryLanguage, request, false, null,
				null, null, false);

		/*
		 * Need the origin country for itinerary. It is necessary to determine whether additional charge details will be
		 * printed based on the country's configuration.
		 */
		pnrModesDTO.setLoadOriginCountry(true);

		String strBaggageAllowanceRemarks = ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);
		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggageAllowanceRemarks != null && strBaggageAllowanceRemarks.trim().equals("Y")) {
			isbaggageAllowanceRemarksEnable = true;
		}
		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			isbaggageEnable = true;
		}
		if (isbaggageAllowanceRemarksEnable && !isbaggageEnable) {
			includeBaggageAllowance = true;
		}
		
		if (AppSysParamsUtil.showChargesPaymentsInItinerary()
				&& (!resPro.isViewItineraryWithPaymentInfo() || operationType.equals("CREATE_BOOKING"))) {
			includePaymentDetails = true;
		} else if (resPro.isViewItineraryWithPaymentInfo() && includePaymentDetails) {
			includePaymentDetails = true;
		}

		if (AppSysParamsUtil.showChargesPaymentsInItinerary()
				&& (!resPro.isViewItineraryWithCharges() || operationType.equals("CREATE_BOOKING"))) {
			includeTicketCharges = true;
		} else if (resPro.isViewItineraryWithCharges() && includeTicketCharges) {
			includeTicketCharges = true;
		}
		CommonItineraryParamDTO commonItineraryParam = new CommonItineraryParamDTO();
		commonItineraryParam.setItineraryLanguage(itineraryLanguage);
		commonItineraryParam.setIncludePaxFinancials(includePaxFinancials);
		commonItineraryParam.setIncludePaymentDetails(includePaymentDetails);
		commonItineraryParam.setIncludeTicketCharges(includeTicketCharges);
		commonItineraryParam.setIncludeTermsAndConditions(includeTermsAndConditions);
		commonItineraryParam.setIncludeBaggageAllowance(includeBaggageAllowance);
		commonItineraryParam.setStation(agentStation);
		commonItineraryParam.setAppIndicator(ApplicationEngine.XBE);
		commonItineraryParam.setAirportMap(SelectListGenerator.getAirportsList(groupPNR));
		commonItineraryParam.setOperationType(operationType);
		commonItineraryParam.setIncludePaxContactDetails(AppSysParamsUtil.showPassenegerContactDetailsInItinerary());
		commonItineraryParam.setIncludeStationContactDetails(AppSysParamsUtil.showStationContactDetailsInItinerary());

		CommonItineraryParamDTO commonItineraryParamDTO = null;
		List<CommonItineraryParamDTO> itineraryDtoList = null;
		if (isCustomizedItinerary(request)) {
			itineraryDtoList = CustomizedItineraryUtil.itineraryCompose(request, ACTION_EMAIL, commonItineraryParam,
					trackInfoDTO, pnrModesDTO);
		} else {
			commonItineraryParamDTO = commonItineraryParam;
		}

		ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();

		boolean isWebOrMobileVisible = JavaScriptGenerator.checkWebOrMobileAccess(request);
		
		if (!JavaScriptGenerator.checkAccess(request, PriviledgeConstants.ANY_PNR_SEARCH)) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			String strAgentCode = userPrincipal.getAgentCode();
			Collection<String> carrierCodes = userPrincipal.getCarrierCodes();

			reservationSearchDTO.setSearchGSABookings(JavaScriptGenerator.checkAccess(request,
					PriviledgeConstants.ANY_GSA_PNR_SEARCH));
			reservationSearchDTO.setSearchIBEBookings(isWebOrMobileVisible);
			reservationSearchDTO.setOwnerAgentCode(strAgentCode);
			reservationSearchDTO.setBookingCategories(this.getBookingCategories());
			reservationSearchDTO.setPnr(pnr);
			reservationSearchDTO.setSearchableCarriers(carrierCodes);
		}

		if (itineraryDtoList != null) {
			for (CommonItineraryParamDTO itineraryParamDTO : itineraryDtoList) {
				getItinerary(itineraryParamDTO, pnrModesDTO, getTrackInfo(), individualPax, selectedPax, ACTION_EMAIL,
						reservationSearchDTO);
			}
		} else {
			getItinerary(commonItineraryParamDTO, pnrModesDTO, getTrackInfo(), individualPax, selectedPax, ACTION_EMAIL,
					reservationSearchDTO);
		}

	}

	/**
	 * @return the hdnPNRNo
	 */
	public String getHdnPNRNo() {
		return hdnPNRNo;
	}

	/**
	 * @param hdnPNRNo
	 *            the hdnPNRNo to set
	 */
	public void setHdnPNRNo(String hdnPNRNo) {
		this.hdnPNRNo = hdnPNRNo;
	}

	/**
	 * @return the itineraryLanguage
	 */
	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	/**
	 * @param itineraryLanguage
	 *            the itineraryLanguage to set
	 */
	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}

	/**
	 * @return the includePaxFinancials
	 */
	public boolean isIncludePaxFinancials() {
		return includePaxFinancials;
	}

	/**
	 * @param includePaxFinancials
	 *            the includePaxFinancials to set
	 */
	public void setIncludePaxFinancials(boolean includePaxFinancials) {
		this.includePaxFinancials = includePaxFinancials;
	}

	/**
	 * @return the includePaymentDetails
	 */
	public boolean isIncludePaymentDetails() {
		return includePaymentDetails;
	}

	/**
	 * @param includePaymentDetails
	 *            the includePaymentDetails to set
	 */
	public void setIncludePaymentDetails(boolean includePaymentDetails) {
		this.includePaymentDetails = includePaymentDetails;
	}

	/**
	 * @return the includeTicketCharges
	 */
	public boolean isIncludeTicketCharges() {
		return includeTicketCharges;
	}

	/**
	 * @param includeTicketCharges
	 *            the includeTicketCharges to set
	 */
	public void setIncludeTicketCharges(boolean includeTicketCharges) {
		this.includeTicketCharges = includeTicketCharges;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @return the selectedPax
	 */
	public String getSelectedPax() {
		return selectedPax;
	}

	/**
	 * @param selectedPax
	 *            the selectedPax to set
	 */
	public void setSelectedPax(String selectedPax) {
		this.selectedPax = selectedPax;
	}

	/**
	 * @return the includeTermsAndConditions
	 */
	public boolean isIncludeTermsAndConditions() {
		return includeTermsAndConditions;
	}

	/**
	 * @param includeTermsAndConditions
	 *            the includeTermsAndConditions to set
	 */
	public void setIncludeTermsAndConditions(boolean includeTermsAndConditions) {
		this.includeTermsAndConditions = includeTermsAndConditions;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType
	 *            the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public static boolean isCustomizedItinerary(HttpServletRequest request) {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams processParams = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null, true, false, null, null, false, false);
		return processParams.isCustomizedItineraryView();
	}

	public String getItinerary(CommonItineraryParamDTO commonItineraryParam, LCCClientPnrModesDTO pnrModesDTO,
			TrackInfoDTO trackInfoDTO, boolean individualPax, String selectedPax, String action,
			ReservationSearchDTO reservationSearchDTO) throws ModuleException {
		String itinerary = "";
		commonItineraryParam.setAmountMaskingForcePriviledge(JavaScriptGenerator.checkAccess(request,
				PriviledgeConstants.ALLOW_AEROMART_PAY_OPERATIONS));
		if (individualPax) {
			String paxs[] = selectedPax.split(",");
			for (String pax : paxs) {
				if (pax != null || !pax.equals("")) {
					commonItineraryParam.setSelectedPaxDetails(pax);
					if (action.equals(ACTION_EMAIL)) {
						ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
								trackInfoDTO);
					} else if (action.equals(ACTION_PRINT)) {
						itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
								commonItineraryParam, reservationSearchDTO, trackInfoDTO);
					}
				}
			}
		} else {
			commonItineraryParam.setSelectedPaxDetails(selectedPax);
			if (action.equals(ACTION_EMAIL)) {
				ModuleServiceLocator.getAirproxyReservationBD().sendEmailItinerary(pnrModesDTO, commonItineraryParam,
						trackInfoDTO);
			} else if (action.equals(ACTION_PRINT)) {
				itinerary += ModuleServiceLocator.getAirproxyReservationBD().getItineraryForPrint(pnrModesDTO,
						commonItineraryParam, reservationSearchDTO, trackInfoDTO);
			}
		}

		return itinerary;
	}

	private Collection<String> getBookingCategories() throws ModuleException {
		Collection<String> privilegeBookingCategories = new ArrayList<String>();
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_STANDARD_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.STANDARD.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_CHARTER_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.CHARTER.getCode());
		}
		if (JavaScriptGenerator.checkAccess(request, PriviledgeConstants.PRIVI_SEARCH_ANY_HR_BOOKING)) {
			privilegeBookingCategories.add(BookingCategory.HR.getCode());
		}
		return privilegeBookingCategories;
	}

}