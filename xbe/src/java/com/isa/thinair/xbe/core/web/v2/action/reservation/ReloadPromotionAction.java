package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentPassengerTO;
import com.isa.thinair.xbe.api.dto.v2.PaymentSummaryTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.RequestHandlerUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.PaymentUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ReloadPromotionAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ReloadPromotionAction.class);

	private boolean success = true;

	private boolean checkPromotionsWithCode = false;

	private boolean overrideExistingPromotion = false;

	private FlightSearchDTO searchParams;

	private String selectedFlightList;

	private ContactInfoTO contactInfo;

	// Bank Identification Number
	private String bin;

	private String promoCode;

	private Collection<PaymentPassengerTO> passengerPayment;

	private PaymentSummaryTO paymentSummaryTO;

	private BigDecimal existingDiscountAmount;

	private BigDecimal binPromotionDiscountAmount;

	private boolean hasExistingPromotion;

	private boolean hasBinPromotion;

	public String promotionsForBankIdentification() {

		HttpSession session = request.getSession();
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) session
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		List<FlightSegmentTO> flightSegmentTOs = constructFlighSegmentTO();
		ApplicablePromotionDetailsTO applicablePromotions = getApplicableDiscountPromotionsForBIN(flightSegmentTOs,
				bookingShoppingCart);

		FlightAvailRQ flightAvailRQ = null;
		List<PassengerTypeQuantityTO> passengerTypeQuantityList = null;
		try {
			flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			passengerTypeQuantityList = flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList();

			if (applicablePromotions != null && bookingShoppingCart.getFareDiscount() != null) {
				hasExistingPromotion = true;
				// check the picked promotion is same as the promotion which is already applied
				if (!applicablePromotions.getPromoCriteriaId().equals(bookingShoppingCart.getFareDiscount().getPromotionId())) {
					binPromotionDiscountAmount = getDiscountAmount(applicablePromotions, passengerTypeQuantityList);
					existingDiscountAmount = bookingShoppingCart.getFareDiscountAmount();
					hasBinPromotion = true;
				}
			}

			if (applicablePromotions != null && (overrideExistingPromotion || !hasExistingPromotion)) {
				updatePaymentInfomation(bookingShoppingCart, applicablePromotions, passengerTypeQuantityList, flightAvailRQ);
				hasExistingPromotion = false;
				hasBinPromotion = true;
			}
		} catch (ParseException e) {
			this.success = false;
			log.error(e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private void updatePaymentInfomation(BookingShoppingCart bookingShoppingCart,
			ApplicablePromotionDetailsTO applicablePromotions, List<PassengerTypeQuantityTO> passengerTypeQuantityList, FlightAvailRQ flightAvailRQ) {
		try {
			bookingShoppingCart.setFareDiscount(passengerTypeQuantityList, applicablePromotions);
			ReservationDiscountDTO resDiscountDTO = null;
			if (bookingShoppingCart.getPriceInfoTO() != null) {
				XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
						S2Constants.Session_Data.XBE_SES_RESDATA);

				resDiscountDTO = ReservationUtil.calculateDiscountForReservation(bookingShoppingCart,
						bookingShoppingCart.getPaxList(), flightAvailRQ, getTrackInfo(), false,
						bookingShoppingCart.getFareDiscount(), bookingShoppingCart.getPriceInfoTO(), resInfo.getTransactionId());
				bookingShoppingCart.setReservationDiscountDTO(resDiscountDTO);
			}
			

			passengerPayment = PaymentUtil.createPaymentPaxList(bookingShoppingCart, resDiscountDTO, RequestHandlerUtil.getAgentCode(request));
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShoppingCart, searchParams.getSelectedCurrency(),
					exchangeRateProxy, "0.00", RequestHandlerUtil.getAgentCode(request));
		} catch (ModuleException e) {
			this.success = false;
			log.error(e);
		} catch (org.json.simple.parser.ParseException e) {
			this.success = false;
			log.error(e);
		}
	}

	public String removeBINPromotion() {

		HttpSession session = request.getSession();
		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) session
				.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		FlightAvailRQ flightAvailRQ;
		try {
			flightAvailRQ = ReservationBeanUtil.createFlightAvailRQ(searchParams);
			List<PassengerTypeQuantityTO> passengerTypeQuantityList = flightAvailRQ.getTravelerInfoSummary()
					.getPassengerTypeQuantityList();

			bookingShoppingCart.setFareDiscount(passengerTypeQuantityList, null);

			passengerPayment = PaymentUtil.createPaymentPaxList(bookingShoppingCart, null, RequestHandlerUtil.getAgentCode(request));
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

			paymentSummaryTO = PaymentUtil.createPaymentSummary(bookingShoppingCart, searchParams.getSelectedCurrency(),
					exchangeRateProxy, "0.00", RequestHandlerUtil.getAgentCode(request));

		} catch (ModuleException e) {
			this.success = false;
			log.error(e);
		} catch (ParseException e) {
			this.success = false;
			log.error(e);
		} catch (org.json.simple.parser.ParseException e) {
			this.success = false;
			log.error(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	private List<FlightSegmentTO> constructFlighSegmentTO() {
		List<FlightSegmentTO> flightSegmentTOs = null;
		try {
			flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
			SortUtil.sortFlightSegByDepDate(flightSegmentTOs);
		} catch (ParseException e) {
			log.error(e);
		} catch (org.json.simple.parser.ParseException e) {
			log.error(e);
		}
		return flightSegmentTOs;
	}

	private ApplicablePromotionDetailsTO getApplicableDiscountPromotionsForBIN(List<FlightSegmentTO> flightSegmentTOs,
			BookingShoppingCart bookingShoppingCart) {
		try {
			AirproxyReservationQueryBD airproxySearchAndQuoteBD = ModuleServiceLocator.getAirproxySearchAndQuoteBD();

			ApplicablePromotionDetailsTO promotion = airproxySearchAndQuoteBD.pickApplicablePromotions(
					buildPromotionCriteria(flightSegmentTOs, bookingShoppingCart),
					PromotionsUtils.getPreferedSystem(searchParams.getSearchSystem()), getTrackInfo());

			if (promotion != null && promotion.getPromoType().equals(PromotionCriteriaConstants.PromotionCriteriaTypes.DISCOUNT)) {
				return promotion;
			}
			return null;
		} catch (ModuleException e) {
			log.error(e);
		}
		return null;
	}

	private PromoSelectionCriteria buildPromotionCriteria(List<FlightSegmentTO> flightSegmentTOs,
			BookingShoppingCart bookingShoppingCart) {

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		List<OndClassOfServiceSummeryTO> availableLogicalCCList = bookingShoppingCart.getPriceInfoTO()
				.getAvailableLogicalCCList();

		List<FareRuleDTO> fareRules = bookingShoppingCart.getPriceInfoTO().getFareTypeTO().getApplicableFareRules();

		PromoSelectionCriteria promoSelectionCriteria = new PromoSelectionCriteria();
		promoSelectionCriteria.setReservationDate(CalendarUtil.getCurrentSystemTimeInZulu());
		promoSelectionCriteria.setAdultCount(searchParams.getAdultCount());
		promoSelectionCriteria.setChildCount(searchParams.getChildCount());
		promoSelectionCriteria.setInfantCount(searchParams.getInfantCount());
		if (promoCode != null && !promoCode.isEmpty()) {
			promoSelectionCriteria.setPromoCode(promoCode);
			this.checkPromotionsWithCode = true;
		}

		if (bin != null && !bin.isEmpty()) {
			promoSelectionCriteria.setBankIdNo(Integer.parseInt(bin.trim()));
		}

		promoSelectionCriteria.setSalesChannel(userPrincipal.getSalesChannel());
		promoSelectionCriteria.setAgent(userPrincipal.getAgentCode());
		promoSelectionCriteria.setPreferredLanguage(contactInfo.getPreferredLang());

		if (searchParams.getReturnDate() != null && !searchParams.getReturnDate().isEmpty()) {
			promoSelectionCriteria.setJourneyType(JourneyType.ROUNDTRIP);
		} else {
			promoSelectionCriteria.setJourneyType(JourneyType.SINGLE_SECTOR);
		}

		if (flightSegmentTOs != null) {
			for (FlightSegmentTO flightSegment : flightSegmentTOs) {
				promoSelectionCriteria.getFlights().add(flightSegment.getFlightNumber());
				promoSelectionCriteria.getFlightSegIds().add(flightSegment.getFlightSegId());
				promoSelectionCriteria.getFlightSegWiseLogicalCabinClass().put(flightSegment.getFlightSegId(), flightSegment.getLogicalCabinClassCode());
				promoSelectionCriteria.getCabinClasses().add(flightSegment.getCabinClassCode());
			}

			promoSelectionCriteria.setOndFlightDates(PromotionsUtils.getOndFlightDates(flightSegmentTOs));
		}

		for (FareRuleDTO fareRule : fareRules) {
			promoSelectionCriteria.getBookingClasses().add(fareRule.getBookingClassCode());
		}

		for (OndClassOfServiceSummeryTO ondClassSummery : availableLogicalCCList) {
			promoSelectionCriteria.getOndList().add(ondClassSummery.getOndCode());
			for (LogicalCabinClassInfoTO cabinClassInfoTO : ondClassSummery.getAvailableLogicalCCList()) {
				promoSelectionCriteria.getLogicalCabinClasses().add(cabinClassInfoTO.getLogicalCCCode());
			}
		}

		promoSelectionCriteria.setDryOperatingAirline(PromotionsUtils.getDryOperatingAirline(flightSegmentTOs,
				searchParams.getSearchSystem()));
		return promoSelectionCriteria;
	}

	private BigDecimal getDiscountAmount(ApplicablePromotionDetailsTO promoDetails, List<PassengerTypeQuantityTO> paxQtyList) {

		BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
				S2Constants.Session_Data.LCC_SES_BOOKING_CART);

		int totalPaxCount = 0;
		for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {
			totalPaxCount += paxTypeQty.getQuantity();
		}
		BigDecimal value = BigDecimal.ZERO;
		float effectiveDiscountValue = 0;
		BigDecimal[] effectiveDiscAmountArr = PromotionsUtils.getEffectivePaxDiscountValueWithoutLoss(
				promoDetails.getDiscountValue(), promoDetails.getApplyTo(), promoDetails.getDiscountType(), totalPaxCount, 0);

		int count = 0;

		for (PassengerTypeQuantityTO paxTypeQty : paxQtyList) {
			Integer paxCount = paxTypeQty.getQuantity();
			if (PromotionCriteriaConstants.PromotionCriteriaTypes.BUYNGET.equals(promoDetails.getPromoType())) {
				paxCount = bookingShoppingCart.getFareDiscount().getPaxCount(paxTypeQty.getPassengerType());
			}
			if (paxCount != null && paxCount > 0) {
				for (int i = 0; i < paxCount; i++) {

					if (PromotionCriteriaConstants.DiscountTypes.PERCENTAGE.equals(promoDetails.getDiscountType())) {
						effectiveDiscountValue = effectiveDiscAmountArr[0].floatValue();
					} else {
						effectiveDiscountValue = effectiveDiscAmountArr[count].floatValue();
					}

					BigDecimal discountAmount = bookingShoppingCart.getPriceInfoTO().getPerPaxDiscountAmount(
							paxTypeQty.getPassengerType(), effectiveDiscountValue, promoDetails.getDiscountType(),
							promoDetails.getApplyTo(), promoDetails.getApplicableOnds());

					value = AccelAeroCalculator.add(value, discountAmount);
					count++;
				}
			}
		}
		return AccelAeroCalculator.scaleValueDefault(value);

	}

	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public Collection<PaymentPassengerTO> getPassengerPayment() {
		return passengerPayment;
	}

	public void setPassengerPayment(Collection<PaymentPassengerTO> passengerPayment) {
		this.passengerPayment = passengerPayment;
	}

	public PaymentSummaryTO getPaymentSummaryTO() {
		return paymentSummaryTO;
	}

	public void setPaymentSummaryTO(PaymentSummaryTO paymentSummaryTO) {
		this.paymentSummaryTO = paymentSummaryTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public boolean isCheckPromotionsWithCode() {
		return checkPromotionsWithCode;
	}

	public void setCheckPromotionsWithCode(boolean checkPromotionsWithCode) {
		this.checkPromotionsWithCode = checkPromotionsWithCode;
	}

	public boolean isOverrideExistingPromotion() {
		return overrideExistingPromotion;
	}

	public void setOverrideExistingPromotion(boolean overrideExistingPromotion) {
		this.overrideExistingPromotion = overrideExistingPromotion;
	}

	public BigDecimal getExistingDiscountAmount() {
		return existingDiscountAmount;
	}

	public void setExistingDiscountAmount(BigDecimal existingDiscountAmount) {
		this.existingDiscountAmount = existingDiscountAmount;
	}

	public boolean isHasExistingPromotion() {
		return hasExistingPromotion;
	}

	public void setHasExistingPromotion(boolean hasExistingPromotion) {
		this.hasExistingPromotion = hasExistingPromotion;
	}

	public BigDecimal getBinPromotionDiscountAmount() {
		return binPromotionDiscountAmount;
	}

	public void setBinPromotionDiscountAmount(BigDecimal binPromotionDiscountAmount) {
		this.binPromotionDiscountAmount = binPromotionDiscountAmount;
	}

	public boolean isHasBinPromotion() {
		return hasBinPromotion;
	}

	public void setHasBinPromotion(boolean hasBinPromotion) {
		this.hasBinPromotion = hasBinPromotion;
	}
}
