package com.isa.thinair.xbe.core.web.handler.travagent;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.MessageCodes;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.util.StringUtil;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.travagent.AgentCollectionHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.travagent.PostPaymentHTMLGenerator;
import com.isa.thinair.xbe.core.web.generator.travagent.TravelAgentHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class AgentCollectionRequestHandler extends BasicRH {

	private static Log log = LogFactory.getLog(AgentCollectionRequestHandler.class);

	private static XBEConfig xbeConfig = new XBEConfig();

	private static final String PARAM_HDNDATA = "hdnData";
	private static final String PARAM_HDNAGENTCODE = "hdnAgentCode";
	private static final String PARAM_HDNAGENTNAME = "hdnAgentName";
	private static final String PARAM_AGENTCODE = "hdnCode";
	private static final String PARAM_ADJTYPE = "selType";
	private static final String PARAM_CARRIER = "selCarrier";
	private static final String PARAM_ADJAMOUNT = "txtAmount";
	private static final String PARAM_ADJREMARKS = "txtRemarks";
	private static final String PARAM_CURENCY = "selCurrIn";
	private static final String PARAM_RECEIPT_NO = "txtReciptNo";
	private static final String PARAM_DEBIT = "DBT";
	private static final String PARAM_HDNISACCEPTRECORD = "hdnIsAcceptRecord";
	private static final String PARAM_HDNFRESHRECORD = "hdnIsFreshRecord";
	private static final String PARAM_HDN_ORG_TNX_ID = "hdnOriginTnxId";
	

	private static String isSuccessfulSaveMessageDisplayEnabledJS = "isSuccessfulSaveMessageDisplayEnabled = "
			+ isSuccessfulSaveMessageDisplayEnabled() + ";";

	public static String execute(HttpServletRequest request) throws ModuleException {

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		// TODO (Chethiya) : Add proper validations for this
		if ((!AppSysParamsUtil.isGSAStructureVersion2Enabled() && AgentType.TA.equals(userPrincipal.getAgentTypeCode()))
				|| (AppSysParamsUtil.isGSAStructureVersion2Enabled() && AgentType.STA.equals(userPrincipal.getAgentTypeCode()))) {
			log.debug("User does not have sufficient privilages");
			throw new ModuleException(MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN);
		}

		boolean isSaveTransactionSuccessful = false;
		String saveSuccess = "isSaveTransactionSuccessful = false;";
		String forward = S2Constants.Result.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");
		String strAction = request.getParameter("hdnAction");
		String strCreditMode = request.getParameter("hdnCMode");

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		// Getting the values to the adjust payment
		try {

			if (strHdnMode != null && strHdnMode.equals("showAdjustPayment")) {

				String strFormData = "var arrFormData = new Array();";//
				strFormData += getADJFormData(request);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				setAdjPaymentHtml(request);
				setClientErrors(request);
				setPaymentTypeList(request);
				setPayCarrierList(request);
				setApplicationBaseCurr(request);
				setGSASearch(request);
				setBSPCreditLimitAppParam(request);
				setViewPaymentOnlyPrivilegeHtml(request);
				setIsPreventAddingDupliatesInRecordNSettleScreen(request);
				TravelAgentHTMLGenerator.setTravelAgentListHtml(request);
				setGSAVersion2AppParam(request);
				return "adjustedpayment";
			}

		} catch (ModuleException e) {
			isSaveTransactionSuccessful = false;
			log.error("EXCEPTION IN showAdjustpayment " + e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		// save adjusted payment
		if (strCreditMode != null && strCreditMode.equals("SAVEADJPAAYMENT")) {

			try {

				checkAgentSettlementAllowed(request, PARAM_AGENTCODE);

				if (isGSA(request)) {
					checkForGSAPerformSave(request, PARAM_AGENTCODE); // (AARESAA-1830)
				}

				isSaveTransactionSuccessful = true;
				saveAdjustedPayment(request);
				setPaymentTypeList(request);
				setApplicationBaseCurr(request);
				String strFormData = "var arrFormData = new Array();";
				strFormData += getADJFormData(request);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", userLanguage), WebConstants.MSG_SUCCESS);

			} catch (ModuleException e) {
				isSaveTransactionSuccessful = false;
				log.error("EXCEPTION IN saveAdjustpayment " + e);
				String strFormData = "var arrFormData = new Array();";
				strFormData += getADJFormData(request);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);				
				String errorMsg = retrieveErrorMessage(e.getExceptionCode(),  userLanguage);
				saveMessage(request, errorMsg, WebConstants.MSG_ERROR);

			} finally {
				try {
					setAdjPaymentHtml(request);
				} catch (ModuleException e) {
					log.error("EXCEPTION IN FINALLY saveAdjustpayment " + e.getExceptionCode());
				}
			}
		}

		try {
			if (strHdnMode != null && strHdnMode.equalsIgnoreCase(WebConstants.ACTION_SEARCH)) {
				setAgentCollectionRowHtml(request);
				log.debug("\nAgentCollectionRequestHandler setAgentCollectioRowHtml() SUCCESS");
			} else {
				String strEmptyArray = "var arrData = new Array();";
				request.setAttribute(WebConstants.REQ_HTML_ROWS, strEmptyArray);
				String strJavascriptTotalNoOfRecs = "var totalRecords = " + 0 + ";";
				request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
				log.debug("\nAgentCollectionRequestHandler setAgentCollectioRowHtml() NOT ENTERED");
			}
		} catch (ModuleException e) {
			log.error("\nAgentCollectionRequestHandler setAgentCollectioRowHtml() FAILED IN TRY CATCH BLOCK:" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
				isSaveTransactionSuccessful = true;

				checkAgentSettlementAllowed(request, PARAM_HDNAGENTCODE);

				if (isGSA(request)) {
					checkForGSAPerformSave(request, PARAM_HDNAGENTCODE); // (AARESAA-1830)
				}
				if (strAction.equals("INVSET")) {
					checkPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_INVOICE);
					saveInvoiceSettlement(request);
					setInvoiceSettlementData(request);
				}
				setAgentCollectionRowHtml(request);
				saveMessage(request, xbeConfig.getMessage("cc.airadmin.add.success", userLanguage), WebConstants.MSG_SUCCESS);

			}
		} catch (ParseException p) {

		} catch (ModuleException e) {
			String errorMsg = retrieveErrorMessage(e.getExceptionCode(),  userLanguage);		
			log.error("TRAVEL AGENT REQUEST HANDLER SAVEDATA() FAILED: " + e.getMessageString());
			saveMessage(request, errorMsg, WebConstants.MSG_ERROR);
			isSaveTransactionSuccessful = false;

		}

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			if (isSaveTransactionSuccessful) {
				saveSuccess = "isSaveTransactionSuccessful = true;";
			} else {
				saveSuccess = "isSaveTransactionSuccessful = false;";
			}
			request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		}

		try {
			setDisplayData(request);
			log.debug("\nAgentCollectionRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("\nAgentCollectionRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}
		return forward;
	}

	/**
	 * Method will reassure that Invoice Settlement for Agent Save not initiated by GSA for GSA (AARESAA-1830)
	 * 
	 * @param request
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	private static void checkForGSAPerformSave(HttpServletRequest request, String agentCode) throws ModuleException {

		String strAgentCode = StringUtil.getNotNullString(request.getParameter(agentCode));
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (userPrincipal.getAgentCode().equals(strAgentCode.trim())) {
			log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":" + "GSA perform illegal Settlement");
			throw new ModuleException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
		}

	}

	private static void checkAgentSettlementAllowed(HttpServletRequest request, String agentCode) throws ModuleException {
		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {

			String strAgentCode = StringUtil.getNotNullString(request.getParameter(agentCode));
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			String loggedAgent = userPrincipal.getAgentCode();

			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strAgentCode);

			String parentAgentCode = agent.getGsaCode();
			if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(parentAgentCode)) {
				if (!loggedAgent.equals(parentAgentCode)) {
					log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":"
							+ "User perform illegal Settlement");
					throw new ModuleException("cc.airtravelagent.logic.invalid.reporting.agent");
				}
			} else {

				if (!AppSysParamsUtil.getCarrierAgent().equals(userPrincipal.getAgentTypeCode())
						|| !hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT_ANY)) {
					log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":"
							+ "in-sufficeint privillege or agent type invalid");
					throw new ModuleException("cc.airtravelagent.logic.invalid.reporting.agent");
				}
			}

			Agent loggedUserAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(loggedAgent);
			if (Agent.HAS_CREDIT_LIMIT_YES.equals(agent.getHasCreditLimit())
					&& !Agent.HAS_CREDIT_LIMIT_YES.equals(loggedUserAgent.getHasCreditLimit())
					&& !hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT_ANY)) {
				throw new ModuleException("cc.user.form.settle.payment.fixed.agent.limit.priv");
			}

		}
	}

	/**
	 * Check for weather the current log user is GSA , if then allow to search his profile at back end.
	 * 
	 * @param request
	 */
	private static void setGSASearch(HttpServletRequest request) {

		String searchGsaString = "";
		String gsaNameNCode = "";

		if (isGSA(request)) {
			searchGsaString = "var searchGSA = true;";
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			gsaNameNCode = "var gNameNCode = " + '"' + userPrincipal.getName().trim() + "^" + userPrincipal.getAgentCode().trim()
					+ '"' + ";";
		} else {
			searchGsaString = "var searchGSA = false;";
			gsaNameNCode = "var gNameNCode = " + '"' + '"' + ";";

		}
		request.setAttribute(WebConstants.REQ_SEARCH_IF_GSA_USER, searchGsaString);
		request.setAttribute(WebConstants.REQ_IF_GSA_NAME_CODE, gsaNameNCode);

	}

	/**
	 * Check Weather the current logged user is GSA.
	 * 
	 * @param request
	 * @return
	 */
	private static boolean isGSA(HttpServletRequest request) {

		boolean isGsa = false;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		String agentType = userPrincipal.getAgentTypeCode();
		if (agentType != null && AgentType.GSA.equals(agentType)) {
			isGsa = true;
		}
		return isGsa;

	}

	private static void setGSAPriviHtml(HttpServletRequest request) {

		String accessAnyPrviString = "";

		if (BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_ANY)) {

			accessAnyPrviString = "var hasPriviGSA = true;";
		} else {
			accessAnyPrviString = "var hasPriviGSA = false;";
		}
		request.setAttribute(WebConstants.REQ_PRIV_TA_PAY_SETTLE_ANY, accessAnyPrviString);

	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setPaymentPurposeList(request);
		setPaymentTypeList(request);
		setPayCarrierList(request);
		setApplicationBaseCurr(request);
		setGSAPriviHtml(request);
		setGSASearch(request);
		setBSPCreditLimitAppParam(request);
		setIsPreventAddingDupliatesInRecordNSettleScreen(request);
		TravelAgentHTMLGenerator.setTravelAgentListHtml(request);
		setGSAVersion2AppParam(request);
		setViewPaymentOnlyPrivilegeHtml(request);
		request.setAttribute(WebConstants.REQ_DEFAULT_AIRLINE_CODE, AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode());
	}

	private static void saveInvoiceSettlement(HttpServletRequest request) throws ModuleException, ParseException {

		String strData = StringUtil.getNotNullString(request.getParameter(PARAM_HDNDATA));
		String strAgentCode = StringUtil.getNotNullString(request.getParameter(PARAM_HDNAGENTCODE));
		String strAgentName = StringUtil.getNotNullString(request.getParameter(PARAM_HDNAGENTNAME));

		String userId = request.getUserPrincipal().getName();

		request.setAttribute(WebConstants.REQ_AGENT_CODE, strAgentCode);
		request.setAttribute(WebConstants.REQ_AGENT_NAME, strAgentName);

		StringTokenizer tokens = new StringTokenizer(strData, "||");
		String str = "";
		Collection<InvoiceSettlement> invSettle = new ArrayList<InvoiceSettlement>();

		while (tokens.hasMoreTokens()) {
			str = (String) tokens.nextElement();

			String[] strChildArr = str.split(",");
			InvoiceSettlement invoiceSettlement = new InvoiceSettlement();
			GregorianCalendar date = new GregorianCalendar();

			invoiceSettlement.setAmountPaid(AccelAeroCalculator.getTwoScaledBigDecimalFromString(strChildArr[5]));
			invoiceSettlement.setCpCode("INV");
			invoiceSettlement.setInvoiceNo(strChildArr[1]);
			invoiceSettlement.setSettlementDate(date.getTime());
			invoiceSettlement.setUserId(userId);
			invSettle.add(invoiceSettlement);

			invoiceSettlement = null;
		}
		ModuleServiceLocator.getInvoicingBD().settleInvoice(strAgentCode, invSettle);

	}

	private static void saveAdjustedPayment(HttpServletRequest request) throws ModuleException {

		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		String strAgentCode = StringUtil.getNotNullString(request.getParameter(PARAM_AGENTCODE));
		String strAdjType = StringUtil.getNotNullString(request.getParameter(PARAM_ADJTYPE));
		String strAdjAmount = StringUtil.getNotNullString(request.getParameter(PARAM_ADJAMOUNT));
		String strRemarks = StringUtil.getNotNullString(request.getParameter(PARAM_ADJREMARKS));
		String strCurrency = StringUtil.getNotNullString(request.getParameter(PARAM_CURENCY));
		String strReciptNo = StringUtil.getNotNullString(request.getParameter(PARAM_RECEIPT_NO));
		Boolean isAcceptDuplicateRecord = new Boolean(StringUtil.getNotNullString(request.getParameter(PARAM_HDNISACCEPTRECORD)));
		Boolean isFreshRecord = new Boolean(StringUtil.getNotNullString(request.getParameter(PARAM_HDNFRESHRECORD)));
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (XBEModuleUtils.getConfig().isUniqueReceiptNumber()) {
			AgentTransaction agentTransaction = ModuleServiceLocator.getTravelAgentFinanceBD().getAgentTransaction(strReciptNo);
			if (agentTransaction != null) {
				throw new ModuleException(WebConstants.KEY_RECEIPT_NUMBER_EXISTS);
			}
		}

		String hdnOriginTnxId = request.getParameter(PARAM_HDN_ORG_TNX_ID);
		
		
		String strCarrier = null;
		if (AppSysParamsUtil.getAdjustmentCarrierList() != null) {
			strCarrier = StringUtil.getNotNullString(request.getParameter(PARAM_CARRIER));
		} else {
			strCarrier = AppSysParamsUtil.getDefaultCarrierCode(); // set default carrier as adjustment carrier if no
																	// other carriers.
		}
		if (!strAdjAmount.equals("")) {
			amount = AccelAeroCalculator.getTwoScaledBigDecimalFromString(strAdjAmount);
		}

		if (AppSysParamsUtil.isPreventAddingDuplicatesInRecordNSettleScreen() && !isAcceptDuplicateRecord) {
			int nominalCode = -1;
			if (strAdjType.equals(CollectionTypeEnum.CHEQUE.toString())) {
				nominalCode = AgentTransactionNominalCode.PAY_INV_CHQ.getCode();
			} else if (strAdjType.equals(CollectionTypeEnum.CASH.toString())) {
				nominalCode = AgentTransactionNominalCode.PAY_INV_CASH.getCode();
			}
			List<AgentTransaction> agentTransactionList = ModuleServiceLocator.getTravelAgentFinanceBD()
					.isDuplicateRecordExistForAgent(strAgentCode, amount, nominalCode);

			if (agentTransactionList != null && agentTransactionList.size() > 0) {
				for (AgentTransaction transaction : agentTransactionList) {
					if (transaction.getReceiptNumber() != null && transaction.getReceiptNumber().equals(strReciptNo)) {
						request.setAttribute(WebConstants.REQ_IS_EXACT_MATCH_EXISTS, true);
						request.setAttribute(WebConstants.REQ_IS_DUPLICATE_EXISTS, false);
						return;
					}
				}
				request.setAttribute(WebConstants.REQ_IS_DUPLICATE_EXISTS, true);
				request.setAttribute(WebConstants.REQ_IS_EXACT_MATCH_EXISTS, false);
				return;
			}
		}

		if (isAcceptDuplicateRecord || isFreshRecord || !AppSysParamsUtil.isPreventAddingDuplicatesInRecordNSettleScreen()) {
			if (strAdjType.equals(CollectionTypeEnum.CASH.toString()))
				ModuleServiceLocator.getTravelAgentFinanceBD().recordPayment(strAgentCode, amount, strRemarks,
						CollectionTypeEnum.CASH, strCurrency, strCarrier, strReciptNo);

			if (strAdjType.equals(CollectionTypeEnum.CHEQUE.toString()))
				ModuleServiceLocator.getTravelAgentFinanceBD().recordPayment(strAgentCode, amount, strRemarks,
						CollectionTypeEnum.CHEQUE, strCurrency, strCarrier, strReciptNo);

			if (strAdjType.equals(CollectionTypeEnum.CREDIT.toString()))
				ModuleServiceLocator.getTravelAgentFinanceBD().recordCredit(strAgentCode, amount, strRemarks, "", strCurrency,
						strCarrier, strReciptNo);

			if (strAdjType.equals(CollectionTypeEnum.DEBIT.toString())) {
				ModuleServiceLocator.getTravelAgentFinanceBD().recordDebit(strAgentCode, amount, strRemarks, "", strCurrency,
						strCarrier, strReciptNo);
			}
			
			if (strAdjType.equals(CollectionTypeEnum.REVERSE_CREDIT.toString())) {
				ModuleServiceLocator.getTravelAgentFinanceBD().reverseCredit(strAgentCode, amount, strRemarks, "", strCurrency,
						strCarrier, strReciptNo,hdnOriginTnxId);
			}
				

			if (strAdjType.equals(CollectionTypeEnum.REVERSE.toString()))
				ModuleServiceLocator.getTravelAgentFinanceBD().recordPaymentReverse(strAgentCode, amount, strRemarks,
						strCurrency, strCarrier, strReciptNo);

			if (strAdjType.equals(CollectionTypeEnum.BSP.toString())) {

				if (!AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
					log.error("Unauthorized operation: This feature not enabled");
					throw new ModuleException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
				}

				ModuleServiceLocator.getTravelAgentFinanceBD().recordBSPCredit(strAgentCode, amount, strRemarks, "", strCurrency,
						strCarrier, strReciptNo);
			}	
		}
		request.setAttribute(WebConstants.REQ_POPUPMESSAGE, xbeConfig.getMessage(WebConstants.KEY_ADD_SUCCESS, userLanguage));
	}

	private static void setPaymentTypeList(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createCollectionTypeList();
		if (hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT_ANY)) {
			strList = SelectListGenerator.createCollectionTypeList();
		} else if (hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT_GSA)) {
			strList = "<option value='CSH'>Cash</option>";
		}

		if (hasPrivilege(request, WebConstants.PRIV_TA_PAY_REVERSE_CREDIT)) {
			strList += "<option value='" + CollectionTypeEnum.REVERSE_CREDIT.toString() + "'>Reverse Credit</option>";
			request.setAttribute(WebConstants.REQ_PREV_TA_CREDIT_REVERSE, true);
		} else {
			request.setAttribute(WebConstants.REQ_PREV_TA_CREDIT_REVERSE, false);
		}

		request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST, strList);
	}

	private static void setPayCarrierList(HttpServletRequest request) {
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		if (AppSysParamsUtil.getAdjustmentCarrierList() != null) {
			request.setAttribute(WebConstants.REQ_HTML_PAYMENTCARRIER_ENABLE, true);
			request.setAttribute(WebConstants.REQ_HTML_PAYMENTCARRIER_LIST,
					SelectListGenerator.SelectAdjustmentCarrierList(user.getDefaultCarrierCode()));
		} else {
			request.setAttribute(WebConstants.REQ_HTML_PAYMENTCARRIER_ENABLE, false);
		}
	}

	private static void setPaymentPurposeList(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createPaymentPurposeList();
		request.setAttribute(WebConstants.REQ_HTML_PAYMENTPURPOSE_LIST, strList);
	}

	private static void setApplicationBaseCurr(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_BASE_CURRENCY, AppSysParamsUtil.getBaseCurrency());
	}

	private static void setAgentCollectionRowHtml(HttpServletRequest request) throws ModuleException {
		AgentCollectionHTMLGenerator htmlGen = new AgentCollectionHTMLGenerator();
		String strHtml = "";
		String terrotitryCode = "";
		if (hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_GSA)) {
			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
			String agentCode = user.getAgentCode();
			if (agentCode != null) {
				terrotitryCode = SelectListGenerator.getAgentTerrotiry(agentCode);
			}
		}
		strHtml = htmlGen.getAgentCollectionRowHtml(request, terrotitryCode, AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
	}

	private static void setInvoiceSettlementData(HttpServletRequest request) throws ModuleException {
		PostPaymentHTMLGenerator htmlGen = new PostPaymentHTMLGenerator();
		String strHtml = htmlGen.getInvoiceDetailsRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_INVOICE_ARRAY, strHtml);
	}

	private static void setAdjPaymentHtml(HttpServletRequest request) throws ModuleException {
		AgentCollectionHTMLGenerator htmlGen = new AgentCollectionHTMLGenerator();
		String strHtml = htmlGen.getAdjustpaymentRowHtml(request);
		request.setAttribute(WebConstants.REQ_ADJUST_PAYROWS, strHtml);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = AgentCollectionHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (xbeConfig.getMessage("cc.airadmin.savemessage.display", null).equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static boolean isNotNull(String str) {
		return (str != null) ? true : false;
	}

	/**
	 * Creates Adjusted page's Form Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Form Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String getADJFormData(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		String strDate = request.getParameter("txtDStart");
		String strStopDate = request.getParameter("txtDStop");
		String strAgentId = request.getParameter("hdnAgentID");
		String strAgentName = request.getParameter("hdnAgentName");
		String strTypeR = request.getParameter("chkTypeR");
		String strTypeC = request.getParameter("chkTypeC");
		String strTypeD = request.getParameter("chkTypeD");
		String strTypeRR = request.getParameter("chkTypeRR");
		String strTypeA = request.getParameter("chkTypeA");
		String strTypeRV = request.getParameter("chkTypeRV");
		String strTypeQ = request.getParameter("chkTypeQ");
		String chkPayTypeBSP = request.getParameter("chkTypeBSP");
		String strReciptNo = request.getParameter("txtReciptNo");
		String strChkTypeCG = request.getParameter("chkTypeCG");
		String strChkTypeCR = request.getParameter("chkTypeCR");
		String strChkTypeAO = request.getParameter("chkTypeAO");
		String strChkTypeCCD = request.getParameter("chkTypeCCD");
		String strAdjAmount = StringUtil.getNotNullString(request.getParameter(PARAM_ADJAMOUNT));
		String strRemarks = StringUtil.getNotNullString(request.getParameter(PARAM_ADJREMARKS));
		String selPayType = StringUtil.getNotNullString(request.getParameter(PARAM_ADJTYPE));

		String strFromPage = request.getParameter("hdnFromPage");
		String selectedCarriers = request.getParameter("hdnSelectedCarriers");
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");

		sb.append("arrFormData[0] = '" + strAgentId + "';");
		sb.append("arrFormData[2] = '" + strDate + "';");
		sb.append("arrFormData[3] = '" + strStopDate + "';");

		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(strAgentId);

		BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal amountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal crdLimit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal avlBalanceForsettlement = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal avlBspBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (agent != null) {

			if (agent.getAgentSummary() != null) {
				balance = agent.getAgentSummary().getAvailableCredit();
				crdLimit = agent.getCreditLimit();
				avlBspBalance = agent.getAgentSummary().getAvailableBSPCredit();
				avlBalanceForsettlement = agent.getAgentSummary().getAvailableFundsForInv();
				if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
					amountDue = agent.getAgentSummary().getOwnDueAmount();
				} else {
					amountDue = AccelAeroCalculator.subtract(crdLimit, balance);
				}
			}

		}

		sb.append("arrFormData[5] = '" + (strAgentName == null ? "" : strAgentName) + "';");
		sb.append("arrFormData[1] = '" + decimalFormat.format(balance) + "';");
		sb.append("arrFormData[6] = '" + decimalFormat.format(amountDue) + "';");
		sb.append("arrFormData[7] = '" + decimalFormat.format(avlBalanceForsettlement) + "';");

		if (isNotNull(strTypeR)) {
			sb.append("arrFormData[8] = 'true';");
		} else {
			sb.append("arrFormData[8] = 'false';");
		}
		if (isNotNull(strTypeC)) {
			sb.append("arrFormData[9] = 'true';");
		} else {
			sb.append("arrFormData[9] = 'false';");
		}
		if (isNotNull(strTypeD)) {
			sb.append("arrFormData[10] = 'true';");
		} else {
			sb.append("arrFormData[10] = 'false';");
		}
		if (isNotNull(strTypeRR)) {
			sb.append("arrFormData[11] = 'true';");
		} else {
			sb.append("arrFormData[11] = 'false';");
		}
		if (isNotNull(strTypeA)) {
			sb.append("arrFormData[12] = 'true';");
		} else {
			sb.append("arrFormData[12] = 'false';");
		}
		if (isNotNull(strTypeRV)) {
			sb.append("arrFormData[13] = 'true';");
		} else {
			sb.append("arrFormData[13] = 'false';");
		}
		if (isNotNull(strTypeQ)) {
			sb.append("arrFormData[15] = 'true';");
		} else {
			sb.append("arrFormData[15] = 'false';");
		}
		sb.append("arrFormData[14] = '" + strFromPage + "';");

		sb.append("arrFormData[16] = '" + agent.getCurrencySpecifiedIn() + "';");

		sb.append("arrFormData[17] = '" + agent.getCurrencyCode() + "';");

		sb.append("arrFormData[18] = '" + selectedCarriers + "';");

		sb.append("arrFormData[19] = '" + avlBspBalance + "';");

		if (isNotNull(chkPayTypeBSP)) {
			sb.append("arrFormData[20] = 'true';");
		} else {
			sb.append("arrFormData[20] = 'false';");
		}

		sb.append("arrFormData[21] = '" + strReciptNo + "';");

		if (isNotNull(strChkTypeCG)) {
			sb.append("arrFormData[22] = 'true';");
		} else {
			sb.append("arrFormData[22] = 'false';");
		}
		if (isNotNull(strChkTypeCR)) {
			sb.append("arrFormData[23] = 'true';");
		} else {
			sb.append("arrFormData[23] = 'false';");
		}

		sb.append("arrFormData[24] = '" + strAdjAmount + "';");
		sb.append("arrFormData[25] = '" + strRemarks + "';");
		sb.append("arrFormData[26] = '" + strReciptNo + "';");
		sb.append("arrFormData[27] = '" + selPayType + "';");
		
		if (isNotNull(strChkTypeAO)) {
			sb.append("arrFormData[28] = 'true';");
		} else {
			sb.append("arrFormData[28] = 'false';");
		}

		if(isNotNull(strChkTypeCCD)){
			sb.append("arrFormData[29] = 'true';");
		} else {
			sb.append("arrFormData[29] = 'false';");
		}
		
		return sb.toString();
	}

	private static void setBSPCreditLimitAppParam(HttpServletRequest request) {
		String maintainAgentBspCreditAppParamStr = "";

		if (AppSysParamsUtil.isMaintainAgentBspCreditLimit()) {
			maintainAgentBspCreditAppParamStr = "var maintainAgentBspCredLmt = true;";
		} else {
			maintainAgentBspCreditAppParamStr = "var maintainAgentBspCredLmt = false;";
		}
		request.setAttribute(WebConstants.REQ_AGENT_MAINTAIN_BSP_CRED_LIM_ENABLED, maintainAgentBspCreditAppParamStr);

	}

	private static void setIsPreventAddingDupliatesInRecordNSettleScreen(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_IS_PREVENT_ADDING_DUPLICATE_RECORDS,
				AppSysParamsUtil.isPreventAddingDuplicatesInRecordNSettleScreen());
	}
	
	private static void setGSAVersion2AppParam(HttpServletRequest request) {
		String gsaVersion2Enabled = "";
		if (AppSysParamsUtil.isGSAStructureVersion2Enabled()) {
			gsaVersion2Enabled = "var gsaStructureV2Enabled = true";
		} else {
			gsaVersion2Enabled = "var gsaStructureV2Enabled = false";
		}

		request.setAttribute(WebConstants.REQ_GSA_V2, gsaVersion2Enabled);
	}
	
	private static String retrieveErrorMessage(String exceptionCode, String userLanguage) {
		String errorMsg = xbeConfig.getMessage(exceptionCode, userLanguage);
		if (!com.isa.thinair.commons.core.util.StringUtil.isNullOrEmpty(errorMsg)
				&& errorMsg.indexOf("Message code set is null or does not exists") > -1) {
			errorMsg = XBEConfig.getClientMessage(exceptionCode, userLanguage);
		}
		return errorMsg;
	}

	private static void setViewPaymentOnlyPrivilegeHtml(HttpServletRequest request) {

		String viewAgentPaymentsOnly = "";

		if (!BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_PAY_SETTLE_PAYMENT)
				&& BasicRH.hasPrivilege(request, WebConstants.PRIV_TA_PAY_VIEW_PAYMENT)) {

			viewAgentPaymentsOnly = "var hasPriviForViewPayOnly = true;";
		} else {
			viewAgentPaymentsOnly = "var hasPriviForViewPayOnly = false;";
		}
		request.setAttribute(WebConstants.REQ_PRIV_TA_PAY_VIEW_ONLY, viewAgentPaymentsOnly);

	}


}
