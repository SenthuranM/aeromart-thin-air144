package com.isa.thinair.xbe.core.web.v2.action.reservation;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airreservation.api.dto.ResContactInfoSearchDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.api.dto.v2.ContactInfoTO;
import com.isa.thinair.xbe.api.dto.v2.ProfileInfoTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.PassengerUtil;

/**
 * POJO to handle the Load Profile
 * 
 * @author Rilwan A. Latiff
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadProfileAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(LoadProfileAction.class);

	private String profileID;

	private ContactInfoTO contactInfo;

	private ProfileInfoTO profileInfoTO;

	private boolean success = true;

	private String messageTxt;

	public String execute() throws ModuleException {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			ResContactInfoSearchDTO resCntInfDto = new ResContactInfoSearchDTO();
			resCntInfDto.setExactMatchOnly(true);

			if (profileID != null && !"".equals(profileID)) {
				if (NumberUtils.isNumber(profileID.trim())) {
					resCntInfDto.setPnr(profileID.trim());
					ReservationContactInfo rescontinf = ModuleServiceLocator.getReservationQueryBD()
							.getLatestReservationContactInfo(resCntInfDto);
					if (rescontinf != null) {
						profileInfoTO = PassengerUtil.createProfileInfo(rescontinf);
					}
				} else {
					Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(profileID.trim());
					if (customer != null) {
						profileInfoTO = PassengerUtil.createProfileInfo(customer);
					} else {
						resCntInfDto.setEmail(profileID.trim());
						ReservationContactInfo rescontinf = ModuleServiceLocator.getReservationQueryBD()
								.getLatestReservationContactInfo(resCntInfDto);
						if (rescontinf != null) {
							profileInfoTO = PassengerUtil.createProfileInfo(rescontinf);
						}
					}
				}
			} else {

				if ((contactInfo.getMobileArea() != null && contactInfo.getMobileCountry() != null && contactInfo.getMobileNo() != null)
						&& (!"".equals(contactInfo.getMobileArea().trim()) && !"".equals(contactInfo.getMobileCountry().trim()) && !""
								.equals(contactInfo.getMobileNo().trim()))) {
					resCntInfDto.setTelephoneNo(contactInfo.getMobileCountry().trim(), contactInfo.getMobileArea().trim(),
							contactInfo.getMobileNo().trim());
				} else if ((contactInfo.getPhoneArea() != null && contactInfo.getPhoneCountry() != null && contactInfo
						.getPhoneNo() != null)
						&& (!"".equals(contactInfo.getPhoneArea().trim()) && !"".equals(contactInfo.getPhoneCountry().trim()) && !""
								.equals(contactInfo.getPhoneNo().trim()))) {
					resCntInfDto.setTelephoneNo(contactInfo.getPhoneCountry().trim(), contactInfo.getPhoneArea().trim(),
							contactInfo.getPhoneNo().trim());
				}
				ReservationContactInfo customer = ModuleServiceLocator.getReservationQueryBD().getLatestReservationContactInfo(
						resCntInfDto);
				if (customer != null) {
					profileInfoTO = PassengerUtil.createProfileInfo(customer);
				}
			}
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @return the profileInfoDTO
	 */
	public ProfileInfoTO getProfileInfoTO() {
		return profileInfoTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	@JSON(serialize = false)
	public ContactInfoTO getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

}
