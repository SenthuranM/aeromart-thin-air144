package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.BookedHalaServicesReportRH;

/**
 * Action class for Booked HalaServices report
 * 
 * @author lalanthi
 * @since 01/09/2009
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.HALA_SERVICES_REPORT)
public class ShowBookedHalaServicesReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		return BookedHalaServicesReportRH.execute(request, response);
	}

}
