/**
 * 
 */
package com.isa.thinair.xbe.core.web.action.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.dto.external.AgentFrequentCustomerDTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.StringUtil;

/**
 * @author suneth
 *
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = S2Constants.Result.ERROR, type = JSONResult.class, value = "") })
public class AgentFrequentCustomerAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(AgentFrequentCustomerAction.class);

	private ArrayList<Map<String, Object>> agentFrequentCustomerList;

	private int page;

	private int records;

	private int total;

	private AgentFrequentCustomer agentFrequentCustomer = null;

	private AgentFrequentCustomerDTO agentFrequentCustomerDTO;

	private String message;

	private String dateOfBirth;

	private String passportExpiryDate;

	private boolean success = true;

	private String operation;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String searchFrequentCustomer() {
		String success = S2Constants.Result.SUCCESS;

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

		String firstName = agentFrequentCustomer.getFirstName().toLowerCase();
		String lastName = agentFrequentCustomer.getLastName().toLowerCase();
		String email = agentFrequentCustomer.getEmail().toUpperCase();
		String agentCode = userPrincipal.getAgentCode();

		try {

			int start = (page - 1) * 20;
			Page agentFrequentCustomerPage = ModuleServiceLocator.getCustomerBD().searchAgentFrequentCustomers(firstName,
					lastName, email, agentCode);
			setAgentFrequentCustomerList(getAgentFrequentCustomerDTOList(
					(ArrayList<AgentFrequentCustomerDTO>) agentFrequentCustomerPage.getPageData(), start, 20));
			this.records = agentFrequentCustomerPage.getTotalNoOfRecords();
			this.total = agentFrequentCustomerPage.getTotalNoOfRecords() / 20;
			int mod = agentFrequentCustomerPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}

		} catch (Exception e) {
			log.error("Search agent frequent customers failed ==> ", e);
		}

		return success;
	}

	public String saveOrUpdateFrequentCustomer() {
		String success = S2Constants.Result.SUCCESS;

		if (!setFrequentCustomerDetails(operation)) {
			return S2Constants.Result.ERROR;
		}

		if (agentFrequentCustomer.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
			agentFrequentCustomer.setTitle("BABY");
			agentFrequentCustomer.setEmail("NA");
		}

		try {
			ModuleServiceLocator.getCustomerBD().saveUpdateAgentFrequentCustomer(agentFrequentCustomer,
					((UserPrincipal) request.getUserPrincipal()).getUserId(), operation);
			if (operation.equals("save")) {
				message = "New Frequent Customer Added Successfully";
			} else if (operation.equals("update")) {
				message = "Frequent Customer Details Modified Successfully";
			}
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(operation + " Frequent Customer Faild.");
			}
			e.printStackTrace();
			this.success = false;
			message = operation + " Frequent Customer Faild.";

			success = S2Constants.Result.ERROR;
		}

		return success;
	}

	private static ArrayList<Map<String, Object>> getAgentFrequentCustomerDTOList(
			List<AgentFrequentCustomerDTO> agentFrequentCustomerList, int start, int pageSize) {

		int index = 0;
		ArrayList<Map<String, Object>> agentFrequentCustomerDTOList = new ArrayList<Map<String, Object>>();
		AgentFrequentCustomerDTO agentFrequentCustomerDTO = null;
		int i = 0;
		Iterator<AgentFrequentCustomerDTO> iterator = agentFrequentCustomerList.iterator();
		while (iterator.hasNext()) {
			agentFrequentCustomerDTO = iterator.next();

			if (index >= start && index < (pageSize + start)) {

				Map<String, Object> row = new HashMap<String, Object>();
				row.put("agentFrequentCustomerDTO", agentFrequentCustomerDTO);
				row.put("id", i++);
				agentFrequentCustomerDTOList.add(row);

			}

			index++;

		}

		return agentFrequentCustomerDTOList;

	}

	public String deleteFrequentCustomer() {
		String success = S2Constants.Result.SUCCESS;

		if (!setFrequentCustomerDetails("Delete")) {
			return S2Constants.Result.ERROR;
		}

		try {
			ModuleServiceLocator.getCustomerBD().deleteAgentFrequentCustomer(agentFrequentCustomer,
					((UserPrincipal) request.getUserPrincipal()).getUserId());
			message = "Frequent Customer Deleted Successfully";
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Deleting Frequent Customer Faild.");
			}
			e.printStackTrace();
			this.success = false;
			message = "Deleting Frequent Customer Faild.";

			success = S2Constants.Result.ERROR;
		}

		return success;
	}

	private boolean setFrequentCustomerDetails(String operation) {

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date;

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		agentFrequentCustomer.setAgentCode(userPrincipal.getAgentCode());

		if (dateOfBirth == null || dateOfBirth.equals("")) {
			agentFrequentCustomer.setDateOfBirth(null);
		} else {

			try {
				date = format.parse(dateOfBirth);
				agentFrequentCustomer.setDateOfBirth(date);
			} catch (ParseException e) {

				if (log.isDebugEnabled()) {
					log.debug(operation + ": Date of Birth parse failed.");
				}
				e.printStackTrace();
				this.success = false;
				message = operation + " Frequent Customer Faild.";

				return false;

			}

		}

		if (passportExpiryDate == null || passportExpiryDate.equals("")) {
			agentFrequentCustomer.setPassportExpiryDate(null);
		} else {

			try {
				date = format.parse(passportExpiryDate);
				agentFrequentCustomer.setPassportExpiryDate(date);
			} catch (ParseException e) {

				if (log.isDebugEnabled()) {
					log.debug(operation + ": Passprot Expiry Date parse failed.");
				}
				e.printStackTrace();
				this.success = false;
				message = operation + " Frequent Customer Faild.";

				return false;

			}

		}

		agentFrequentCustomer.setEmail(agentFrequentCustomer.getEmail().toUpperCase());
		agentFrequentCustomer.setFirstName(StringUtil.toInitCap(agentFrequentCustomer.getFirstName()));
		agentFrequentCustomer.setLastName(StringUtil.toInitCap(agentFrequentCustomer.getLastName()));

		return true;

	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the agentFrequentCustomer
	 */
	public AgentFrequentCustomer getAgentFrequentCustomer() {
		return agentFrequentCustomer;
	}

	/**
	 * @param agentFrequentCustomer
	 *            the agentFrequentCustomer to set
	 */
	public void setAgentFrequentCustomer(AgentFrequentCustomer agentFrequentCustomer) {
		this.agentFrequentCustomer = agentFrequentCustomer;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the passportExpiryDate
	 */
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate
	 *            the passportExpiryDate to set
	 */
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return the agentFrequentCustomerList
	 */
	public ArrayList<Map<String, Object>> getAgentFrequentCustomerList() {
		return agentFrequentCustomerList;
	}

	/**
	 * @param agentFrequentCustomerList
	 *            the agentFrequentCustomerList to set
	 */
	public void setAgentFrequentCustomerList(ArrayList<Map<String, Object>> agentFrequentCustomerList) {
		this.agentFrequentCustomerList = agentFrequentCustomerList;
	}

	/**
	 * @return the agentFrequentCustomerDTO
	 */
	public AgentFrequentCustomerDTO getAgentFrequentCustomerDTO() {
		return agentFrequentCustomerDTO;
	}

	/**
	 * @param agentFrequentCustomerDTO
	 *            the agentFrequentCustomerDTO to set
	 */
	public void setAgentFrequentCustomerDTO(AgentFrequentCustomerDTO agentFrequentCustomerDTO) {
		this.agentFrequentCustomerDTO = agentFrequentCustomerDTO;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * @param operation
	 *            the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

}
