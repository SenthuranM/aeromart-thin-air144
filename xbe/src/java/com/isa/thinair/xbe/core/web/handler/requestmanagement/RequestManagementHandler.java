/**
 * 
 */
package com.isa.thinair.xbe.core.web.handler.requestmanagement;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author suneth
 *
 */
public class RequestManagementHandler extends BasicRH{

	/**
	 * Main Execute Method for RequestManagement Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.SUCCESS;
		
		try {
			
			setDisplayData(request);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			forward = "error";

		}
		
		return forward;
	}
	
	private static void setDisplayData(HttpServletRequest request) throws Exception{
		
		setQueuePages(request);
		setQueues(request);
		setRequestStatus(request);
		
	}
	
	private static void setQueuePages(HttpServletRequest request) throws ModuleException{
		
		
		String queuePages = SelectListGenerator.createQueuePagesList();
		request.setAttribute(WebConstants.REQ_HTML_QUEUE_PAGES, queuePages);
		
		
		
	}
	
	private static void setQueues(HttpServletRequest request) throws Exception{
		
		String queues = SelectListGenerator.createQueuesList();
		request.setAttribute(WebConstants.REQ_HTML_QUEUES, queues);

	}
	
	private static void setRequestStatus(HttpServletRequest request) throws Exception{
		
		String requestStatus = SelectListGenerator.createRequestStatusList();
		request.setAttribute(WebConstants.REQ_HTML_REQUEST_STATUS, requestStatus);

	}
	
}
