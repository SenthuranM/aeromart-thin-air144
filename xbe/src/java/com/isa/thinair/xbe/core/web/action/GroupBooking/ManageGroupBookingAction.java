package com.isa.thinair.xbe.core.web.action.GroupBooking;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingReqRoutesTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.GroupBooking.GroupBookingRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ManageGroupBookingAction extends BaseRequestAwareAction {

	private GroupBookingRequestTO grpBookingReq;
	private List<GroupBookingReqRoutesTO> groupBookingRoutes;

	private String subRequestJSON;

	// For the search grid population
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private final String DATE_FORMAT = "dd/MM/yyyy";

	private String msgType;
	private String rollForwardStartDateStr;
	private String rollForwardEndDateStr;
	private Date rollForwardStartDate;
	private Date rollForwardEndDate;
	private String rollForwardDay;
	// private int[] appDay;

	private long requestID;
	private boolean mainReqestOnly;
	private Boolean sharedRequest = Boolean.FALSE;
	private GroupBookingRequestTO gbRequest;

	private String appDayStr;

	private Log log = LogFactory.getLog(ManageGroupBookingAction.class);

	public String execute() {
		return GroupBookingRH.execute(request);
	}

	public String save() {

		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			grpBookingReq.setRequestDate(new Date());
			grpBookingReq.setRequestedDate(new Date());
			long reqId = getGrpBookingReq().getRequestID();
			grpBookingReq.setGroupBookingRoutes(new HashSet<GroupBookingReqRoutesTO>(groupBookingRoutes));
			grpBookingReq.setCraeteUserCode(user.getUserId());
			grpBookingReq.setStationCode(user.getAgentStation());
			grpBookingReq.setUserId(user.getUserId());
			ServiceResponce response;
			if (reqId == 0) {
				response = ModuleServiceLocator.getGroupBookingBD().addGroupBookingRequestDetails(getGrpBookingReq());
				setGbRequestTO((GroupBookingRequestTO) response.getResponseParam(CommandParamNames.RESPONSE_KEY));

			} else {
				response = ModuleServiceLocator.getGroupBookingBD().updateGroupBookingRequest(getGrpBookingReq());
				setGbRequestTO((GroupBookingRequestTO) response.getResponseParam(CommandParamNames.RESPONSE_KEY));
			}
			
		} catch (Exception e) {
			log.error(e);
		}
		return S2Constants.Result.SUCCESS;

	}

	public GroupBookingRequestTO getGbRequest() {
		return gbRequest;
	}

	public void setGbRequestTO(GroupBookingRequestTO gbRequest) {
		this.gbRequest = gbRequest;
	}

	public String search() {
		Page<GroupBookingRequestTO> criteriaSearchPage = null;
		int start = (page - 1) * 20;
		Map<String, Object> row;
		
		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			String stationsExist = ModuleServiceLocator.getGroupBookingBD().getUserStation(user.getUserId());
			grpBookingReq.setGroupBookingRoutes(new HashSet<GroupBookingReqRoutesTO>(groupBookingRoutes));
		
			
				if (!sharedRequest) {
					criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD().getGroupBookingRequestForStations(
							getGrpBookingReq(), user.getAgentStation(), stationsExist, start, mainReqestOnly);
				} else {
					criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD().getGroupBookingRequestForStations(
							getGrpBookingReq(), null, stationsExist, start, mainReqestOnly);
				}
			
			
		
			this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
			this.total = criteriaSearchPage.getTotalNoOfRecords() / 20;
			int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}

			Collection<GroupBookingRequestTO> groupRequests = criteriaSearchPage.getPageData();
			List<GroupBookingRequestTO> groupReqList = new ArrayList<GroupBookingRequestTO>(groupRequests);

			Collections.sort(groupReqList, new Comparator<GroupBookingRequestTO>() {
				public int compare(GroupBookingRequestTO p1, GroupBookingRequestTO p2) {
					return p2.getRequestID().compareTo(p1.getRequestID());
				}
			});

			setRows(new ArrayList<Map<String, Object>>());
			int i = 1;
			for (GroupBookingRequestTO groupRequest : groupReqList) {
				row = new HashMap<String, Object>();
				row.put("id", ((page - 1) * 20) + i++);
				row.put("groupRequest", groupRequest);
				getRows().add(row);

			}
			msgType = S2Constants.Result.SUCCESS;
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			msgType = S2Constants.Result.ERROR;
			e.printStackTrace();
		}

		return msgType;

	}

	public String executeRollForward() {
		try {
			Set<GroupBookingRequestTO> subRequests = fromJSONToGroupBooking(subRequestJSON);

			ModuleServiceLocator.getGroupBookingBD().executeRollforward(requestID, subRequests);
			msgType = S2Constants.Result.SUCCESS;
		} catch (Exception e) {
			msgType = S2Constants.Result.ERROR;
			e.printStackTrace();
		}
		return msgType;

	}

	public String agentReqUpdate() {

		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			grpBookingReq.setCraeteUserCode(user.getAgentCode());
			ModuleServiceLocator.getGroupBookingBD().updateGroupBookingAgentStatus(getGrpBookingReq());
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();
	}

	public String agentRequote() {

		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			grpBookingReq.setCraeteUserCode(user.getAgentCode());
			ModuleServiceLocator.getGroupBookingBD().updateGroupBookingRequote(getGrpBookingReq());
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {
			// TODO Auto-generated catch block
			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();
	}

	/**
	 * Parses the JSON string to the necessary java object.
	 * 
	 * @param jsonString
	 *            JSON String with the group booking sub requests data.
	 * @return The parsed {@link GroupBookingRequestTO} instance.
	 */
	private Set<GroupBookingRequestTO> fromJSONToGroupBooking(String jsonString) throws JSONException, ParseException {
		Set<GroupBookingRequestTO> requests = new HashSet<GroupBookingRequestTO>();

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> parsedDataMap = (List<Map<String, Object>>) JSONUtil.deserialize(jsonString);

		for (Map<String, Object> dataMap : parsedDataMap) {
			GroupBookingRequestTO groupBooking = new GroupBookingRequestTO();
			groupBooking.setAdultAmount(Integer.parseInt((String) dataMap.get("adultAmount")));
			groupBooking.setChildAmount(Integer.parseInt((String) dataMap.get("childAmount")));
			groupBooking.setInfantAmount(Integer.parseInt((String) dataMap.get("infantAmount")));

			@SuppressWarnings("unchecked")
			List<Map<String, Object>> routeData = (List<Map<String, Object>>) dataMap.get("groupBookingRoutes");
			Set<GroupBookingReqRoutesTO> routeSet = new HashSet<GroupBookingReqRoutesTO>();

			for (Map<String, Object> route : routeData) {
				GroupBookingReqRoutesTO routeTO = new GroupBookingReqRoutesTO();
				routeTO.setFlightNumber((String) route.get("flightNumber"));
				routeTO.setArrival((String) route.get("arrival"));
				routeTO.setDepartureDateStr((String) route.get("departureDateStr"));
				routeTO.setReturningDateStr((String) route.get("returningDateStr"));
				routeTO.setDeparture((String) route.get("departure"));
				routeTO.setVia((String) route.get("via"));
				routeSet.add(routeTO);
			}

			groupBooking.setGroupBookingRoutes(routeSet);
			groupBooking.setOalFare((String) dataMap.get("oalFare"));
			groupBooking.setAgentComments((String) dataMap.get("agentComments"));

			requests.add(groupBooking);
		}

		return requests;
	}

	public GroupBookingRequestTO getGrpBookingReq() {
		return grpBookingReq;
	}

	public void setGrpBookingReq(GroupBookingRequestTO grpBookingReq) {
		this.grpBookingReq = grpBookingReq;
	}

	public List<GroupBookingReqRoutesTO> getGroupBookingRoutes() {
		return groupBookingRoutes;
	}

	public void setGroupBookingRoutes(List<GroupBookingReqRoutesTO> groupBookingRoutes) {
		this.groupBookingRoutes = groupBookingRoutes;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Date getRollForwardStartDate() {
		return rollForwardStartDate;
	}

	public void setRollForwardStartDate(Date rollForwardStartDate) {
		this.rollForwardStartDate = rollForwardStartDate;
	}

	public Date getRollForwardEndDate() {
		return rollForwardEndDate;
	}

	public void setRollForwardEndDate(Date rollForwardEndDate) {
		this.rollForwardEndDate = rollForwardEndDate;
	}

	public long getRequestID() {
		return requestID;
	}

	public void setRequestID(long requestID) {
		this.requestID = requestID;
	}

	public String getRollForwardStartDateStr() {
		return rollForwardStartDateStr;
	}

	public void setRollForwardStartDateStr(String rollForwardStartDateStr) throws ParseException {
		this.rollForwardStartDateStr = rollForwardStartDateStr;
		this.rollForwardStartDate = new SimpleDateFormat(DATE_FORMAT).parse(rollForwardStartDateStr);
	}

	public String getRollForwardEndDateStr() {
		return rollForwardEndDateStr;
	}

	public void setRollForwardEndDateStr(String rollForwardEndDateStr) throws ParseException {
		this.rollForwardEndDateStr = rollForwardEndDateStr;
		this.rollForwardEndDate = new SimpleDateFormat(DATE_FORMAT).parse(rollForwardEndDateStr);
	}

	public String getRollForwardDay() {
		return rollForwardDay;
	}

	public void setRollForwardDay(String rollForwardDay) {
		this.rollForwardDay = rollForwardDay;
	}

	public String getAppDayStr() {
		return appDayStr;
	}

	public void setAppDayStr(String appDayStr) {
		this.appDayStr = appDayStr;
	}

	public boolean isMainReqestOnly() {
		return mainReqestOnly;
	}

	public void setMainReqestOnly(boolean mainReqestOnly) {
		this.mainReqestOnly = mainReqestOnly;
	}

	/**
	 * @return the subRequestJSON
	 */
	public String getSubRequestJSON() {
		return subRequestJSON;
	}

	/**
	 * @param subRequestJSON
	 *            the subRequestJSON to set
	 */
	public void setSubRequestJSON(String subRequestJSON) {
		this.subRequestJSON = subRequestJSON;
	}

	public Boolean getSharedRequest() {
		return sharedRequest;
	}

	public void setSharedRequest(Boolean sharedRequest) {
		this.sharedRequest = sharedRequest;
	}

	

}
