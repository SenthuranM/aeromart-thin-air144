/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.ModifyReservationUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.HandlingFeeCalculationUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * POJO to handle Remove Passenger
 * 
 * @author Abheek Das Gupta
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class RemovePaxAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(RemovePaxAction.class);

	private boolean success = true;

	private String messageTxt;

	private String groupPNR;

	private String pnr;

	private String paxAdults;

	private String paxInfants;

	private String adultCnxCharge;

	private String childCnxCharge;

	private String infantCnxCharge;

	private String defaultAdultCnxCharge;

	private String defaultChildCnxCharge;

	private String defaultInfantCnxCharge;

	private String version;

	private String newPnrNo;

	private String routeType;

	private String userNotes;

	private ConfirmUpdateOverrideChargesTO adultOverideCharges;

	private ConfirmUpdateOverrideChargesTO childOverideCharges;

	private ConfirmUpdateOverrideChargesTO infantOverideCharges;

	public String execute() {
		// FIXME
		boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
		if (!isGroupPNR) {
			groupPNR = pnr;
		}
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));
			Collection<LCCClientReservationPax> colPaxes = ReservationUtil.transformJsonPassengers(request
					.getParameter("resPaxs"));

			Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
			List<String> segmentsToDepart = new ArrayList<String>();
			for (LCCClientReservationSegment reservationSegment : colsegs) {
				if (currentDate.compareTo(reservationSegment.getDepartureDateZulu()) < 0) {
					segmentsToDepart.add(reservationSegment.getFlightSegmentRefNumber());
				}
			}

			List<String> paxInfantList = getPnrPaxIds(paxInfants);
			List<String> paxAdultsList = getPnrPaxIds(paxAdults);
			
			/*
			 * TODO currently the pax status and e ticket status is validating from the front end (remove pax button) it
			 * is better to have the validation here also
			 */
			boolean isInfantRemovable = false;
			boolean onlyInfantAvailable = (paxInfantList.size() > 0 && paxAdultsList.isEmpty());
			isInfantRemovable = (onlyInfantAvailable && AppSysParamsUtil.allowRemoveInfantWhenFutureSegmentsNotThere());

			if (isInfantRemovable || segmentsToDepart.size() > 0) {

				List<LCCClientReservationPax> filteredLCCClientReservationPaxs = new ArrayList<LCCClientReservationPax>();

				for (LCCClientReservationPax reservationPax : colPaxes) {

					if (ModifyReservationUtil.isSelectedTraveler(reservationPax.getTravelerRefNumber(), paxAdultsList)) {

						if (reservationPax.getPaxType().equals(PaxTypeTO.ADULT)) {
							String trmAdultCnxCharge = BeanUtils.nullHandler(adultCnxCharge);

							if (trmAdultCnxCharge.length() > 0 && !trmAdultCnxCharge.equals("undefined")) {
								reservationPax.setTotalCancelCharge(new BigDecimal(trmAdultCnxCharge));
							} else {
								reservationPax.setTotalCancelCharge(null);
							}

						} else if (reservationPax.getPaxType().equals(PaxTypeTO.CHILD)) {
							String trmChildCnxCharge = BeanUtils.nullHandler(childCnxCharge);

							if (trmChildCnxCharge.length() > 0 && !trmChildCnxCharge.equals("undefined")) {
								reservationPax.setTotalCancelCharge(new BigDecimal(trmChildCnxCharge));
							} else {
								reservationPax.setTotalCancelCharge(null);
							}
						}

						filteredLCCClientReservationPaxs.add(reservationPax);

						if (reservationPax.getInfants().size() > 0) {
							Set<LCCClientReservationPax> infantPaxs = reservationPax.getInfants();
							for (LCCClientReservationPax infantPax : infantPaxs) {
								if (!ModifyReservationUtil.isSelectedTraveler(infantPax.getTravelerRefNumber(), paxInfantList)) {
									infantPax.setTotalCancelCharge(null);
									filteredLCCClientReservationPaxs.add(infantPax);
								}
							}
						}
					} else if (ModifyReservationUtil.isSelectedTraveler(reservationPax.getTravelerRefNumber(), paxInfantList)) {
						if (reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
							String trmInfantCnxCharge = BeanUtils.nullHandler(infantCnxCharge);

							if (trmInfantCnxCharge.length() > 0 && !trmInfantCnxCharge.equals("undefined")) {
								reservationPax.setTotalCancelCharge(new BigDecimal(trmInfantCnxCharge));
							} else {
								reservationPax.setTotalCancelCharge(null);
							}

						}

						filteredLCCClientReservationPaxs.add(reservationPax);
					}
				}

				BigDecimal customAdultCharge = null;
				BigDecimal customChildCharge = null;
				BigDecimal customInfantCharge = null;
				PnrChargeDetailTO adultChargeDetailTO = null;
				PnrChargeDetailTO childChargeDetailTO = null;
				PnrChargeDetailTO infantChargeDetailTO = null;
				boolean setSendingAbosoluteCharge = false;

				// Default cnx charge state no route value will be set
				if (this.routeType != null && !this.routeType.equals("")) {
					if (adultCnxCharge != null && !adultCnxCharge.equals("") && !adultCnxCharge.equals("undefined")) {
						customAdultCharge = new BigDecimal(adultCnxCharge);
						if (this.adultOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.adultOverideCharges)) {
							setSendingAbosoluteCharge = true;
						} // if not modifying using percentage
					}

					if (childCnxCharge != null && !childCnxCharge.equals("") && !childCnxCharge.equals("undefined")) {
						customChildCharge = new BigDecimal(childCnxCharge);
						if (this.childOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.childOverideCharges)) {
							setSendingAbosoluteCharge = true;
						}
					}

					if (infantCnxCharge != null && !infantCnxCharge.equals("") && !infantCnxCharge.equals("undefined")) {
						customInfantCharge = new BigDecimal(infantCnxCharge);
						if (this.infantOverideCharges == null
								|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
										this.infantOverideCharges)) {
							setSendingAbosoluteCharge = true;
						}
					}
				}

				if (this.adultOverideCharges != null) {
					adultChargeDetailTO = this.createPNRDetailTO(adultOverideCharges);
				}
				if (this.infantOverideCharges != null) {
					childChargeDetailTO = this.createPNRDetailTO(infantOverideCharges);
				}
				if (this.childOverideCharges != null) {
					infantChargeDetailTO = this.createPNRDetailTO(childOverideCharges);
				}

				// Creates the custom charges TO
				CustomChargesTO customChargesTO = new CustomChargesTO(customAdultCharge, customChildCharge, customInfantCharge,
						customAdultCharge, customChildCharge, customInfantCharge, adultChargeDetailTO, childChargeDetailTO,
						infantChargeDetailTO, setSendingAbosoluteCharge);
				if (this.defaultAdultCnxCharge != null && !this.defaultAdultCnxCharge.isEmpty()) {
					customChargesTO.setDefaultCustomAdultCharge(new BigDecimal(defaultAdultCnxCharge));
				}
				if (this.defaultChildCnxCharge != null && !this.defaultChildCnxCharge.isEmpty()) {
					customChargesTO.setDefaultCustomChildCharge(new BigDecimal(defaultChildCnxCharge));
				}
				if (this.defaultInfantCnxCharge != null) {
					customChargesTO.setDefaultCustomInfantCharge(new BigDecimal(defaultInfantCnxCharge));
				}
				
				List<Integer> paxExclusionSeq = ModifyReservationUtil.getExclusionPaxSequenceFromHandlingFee(colPaxes,
						paxInfantList, paxAdultsList);
				Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges = HandlingFeeCalculationUtil
						.composeExternalChargesByPaxSequence(pnr, colsegs, paxExclusionSeq,
								CommonsConstants.ModifyOperation.REMOVE_PAX.getOperation());

				LCCClientReservation newReservation = ModuleServiceLocator.getAirproxyPassengerBD().removePassenger(groupPNR,
						version, filteredLCCClientReservationPaxs, isGroupPNR, customChargesTO, getTrackInfo(), userNotes,
						paxExternalCharges);
				success = (newReservation.getPNR() != null) ? true : false;
				this.newPnrNo = newReservation.getPNR();

				if (!success) {
					messageTxt = "Remove passenger failed.";
				}
			} else {
				success = false;
				messageTxt = "Flight already departed.";
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private PnrChargeDetailTO createPNRDetailTO(ConfirmUpdateOverrideChargesTO chargesTO) {
		PnrChargeDetailTO chargeDetailTO = new PnrChargeDetailTO();
		chargeDetailTO.setCancellationChargeType(chargesTO.getChargeType());
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
			chargeDetailTO.setMinCancellationAmount(new BigDecimal(chargesTO.getMin()));
		}
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
			chargeDetailTO.setMaximumCancellationAmount(new BigDecimal(chargesTO.getMax()));
		}

		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getPrecentageVal())) {
			chargeDetailTO.setChargePercentage(new BigDecimal(chargesTO.getPrecentageVal()));
		}
		return chargeDetailTO;
	}

	private List<String> getPnrPaxIds(String jSonString) throws ParseException {
		List<String> jsonList = new ArrayList<String>();
		if (jSonString != null && !jSonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jSonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();
				String paxId = BeanUtils.nullHandler(jObject.get("paxID"));
				jsonList.add(paxId);
			}
		}
		return jsonList;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	/**
	 * @param paxAdults
	 *            the paxAdults to set
	 */
	public void setPaxAdults(String paxAdults) {
		this.paxAdults = paxAdults;
	}

	/**
	 * @param paxInfants
	 *            the paxInfants to set
	 */
	public void setPaxInfants(String paxInfants) {
		this.paxInfants = paxInfants;
	}

	public void setAdultCnxCharge(String adultCnxCharge) {
		this.adultCnxCharge = adultCnxCharge;
	}

	public void setChildCnxCharge(String childCnxCharge) {
		this.childCnxCharge = childCnxCharge;
	}

	public void setInfantCnxCharge(String infantCnxCharge) {
		this.infantCnxCharge = infantCnxCharge;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the newPnrNo
	 */
	public String getNewPnrNo() {
		return newPnrNo;
	}

	/**
	 * @param newPnrNo
	 *            the newPnrNo to set
	 */
	public void setNewPnrNo(String newPnrNo) {
		this.newPnrNo = newPnrNo;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public ConfirmUpdateOverrideChargesTO getAdultOverideCharges() {
		return adultOverideCharges;
	}

	public void setAdultOverideCharges(ConfirmUpdateOverrideChargesTO adultOverideCharges) {
		this.adultOverideCharges = adultOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getChildOverideCharges() {
		return childOverideCharges;
	}

	public void setChildOverideCharges(ConfirmUpdateOverrideChargesTO childOverideCharges) {
		this.childOverideCharges = childOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getInfantOverideCharges() {
		return infantOverideCharges;
	}

	public void setInfantOverideCharges(ConfirmUpdateOverrideChargesTO infantOverideCharges) {
		this.infantOverideCharges = infantOverideCharges;
	}

	public String getAdultCnxCharge() {
		return adultCnxCharge;
	}

	public String getChildCnxCharge() {
		return childCnxCharge;
	}

	public String getInfantCnxCharge() {
		return infantCnxCharge;
	}

	public String getUserNotes() {
		return userNotes;
	}

	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public String getDefaultAdultCnxCharge() {
		return defaultAdultCnxCharge;
	}

	public void setDefaultAdultCnxCharge(String defaultAdultCnxCharge) {
		this.defaultAdultCnxCharge = defaultAdultCnxCharge;
	}

	public String getDefaultChildCnxCharge() {
		return defaultChildCnxCharge;
	}

	public void setDefaultChildCnxCharge(String defaultChildCnxCharge) {
		this.defaultChildCnxCharge = defaultChildCnxCharge;
	}

	public String getDefaultInfantCnxCharge() {
		return defaultInfantCnxCharge;
	}

	public void setDefaultInfantCnxCharge(String defaultInfantCnxCharge) {
		this.defaultInfantCnxCharge = defaultInfantCnxCharge;
	}

}
