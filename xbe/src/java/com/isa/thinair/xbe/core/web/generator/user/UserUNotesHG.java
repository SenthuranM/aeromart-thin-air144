package com.isa.thinair.xbe.core.web.generator.user;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class UserUNotesHG {

	private static Log log = LogFactory.getLog(UserUNotesHG.class);

	/**
	 * Gets the collection of Notes
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String getUserNotesDetails(HttpServletRequest request) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Geting the User Notes for User");
		}
		String strUserId = request.getParameter("hdnUserId");
		Collection<UserNotes> colUserNotes = null;
		if (strUserId != null && !strUserId.equals("")) {
			colUserNotes = ModuleServiceLocator.getSecurityBD().getUserNotes(WebConstants.USER_NOTES_FOR_USER_MAINTENANCE,
					strUserId);
		}

		return createUserNotes(colUserNotes);
	}

	/**
	 * Creates the Grid Array
	 * 
	 * @param colNotes
	 * @return
	 * @throws ModuleException
	 */
	private static String createUserNotes(Collection<UserNotes> colNotes) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Iterator<UserNotes> iter = null;
		UserNotes userNotes = null;
		SimpleDateFormat smft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		int i = 0;
		if (colNotes != null) {
			iter = colNotes.iterator();
			while (iter.hasNext()) {
				userNotes = (UserNotes) iter.next();
				sb.append("arrUsernotes [" + i + "] = new Array();");
				sb.append("arrUsernotes [" + i + "][1] = '" + smft.format(userNotes.getCreatedDate()) + "';");
				sb.append("arrUsernotes [" + i + "][2] = '" + userNotes.getContent() + "';");
				sb.append("arrUsernotes [" + i + "][3] = '" + userNotes.getUserId() + "';");

				i++;
			}

		}
		return sb.toString();
	}

}
