package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.AncillaryUtil;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Action class for SSRs
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciSSRAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private List<LCCFlightSegmentSSRDTO> flightSegmentSSRs;

	private FlightSearchDTO searchParams;

	private String selectedFlightList;

	private boolean modifyAncillary;

	private String resPaxInfo;

	private boolean isRequote = false;
	
	private boolean gdsPnr = false;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			String strTxnIdntifier = null;
			SYSTEM system = null;
			List<FlightSegmentTO> flightSegmentTOs = null;
			Map<String, Set<String>> fltRefWiseSelectedSSR = null;
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				system = fltSegBuilder.getSystem();
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

				if (resPaxInfo != null && !resPaxInfo.equals("")) {
					Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
					fltRefWiseSelectedSSR = AncillaryUtil.getFlightReferenceWiseSelectedSSRs(colpaxs);
				}
			} else {
				strTxnIdntifier = resInfo.getTransactionId();
				system = resInfo.getSelectedSystem();
				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}
			}

			// Set the querying system
			flightSegmentSSRs = ModuleServiceLocator.getAirproxyAncillaryBD().getSpecialServiceRequests(flightSegmentTOs,
					strTxnIdntifier, system, null, TrackInfoUtil.getBasicTrackInfo(request), fltRefWiseSelectedSSR,
					modifyAncillary, resInfo.isGdsReservationModAllowed(), BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_MODIFY_SSR_WITHIN_BUFFER), false);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("SSR REQ ERROR", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * getters and setters
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @return the gdsPnr
	 */
	public boolean isGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr the gdsPnr to set
	 */
	public void setGdsPnr(boolean gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List<LCCFlightSegmentSSRDTO> getFlightSegmentSSRs() {
		return flightSegmentSSRs;
	}

	@JSON(serialize = false)
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	@JSON(serialize = false)
	public String getResPaxInfo() {
		return resPaxInfo;
	}

	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}
}
