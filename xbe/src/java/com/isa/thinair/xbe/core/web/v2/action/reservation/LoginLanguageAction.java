package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.HashMap;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.v2.util.I18nUtil;

@Namespace(S2Constants.Namespace.PUBLIC)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = org.apache.struts2.json.JSONResult.class, value = ""),
	@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class LoginLanguageAction extends BaseRequestAwareAction {
	
	
	private HashMap<String, HashMap<String, String>>  languageData;
	
	public String execute(){
	
		setLanguageData(I18nUtil.getLoginData());
		
		return S2Constants.Result.SUCCESS;
		
	}

	public HashMap<String, HashMap<String, String>> getLanguageData() {
		return languageData;
	}

	public void setLanguageData(HashMap<String, HashMap<String, String>> languageData) {
		this.languageData = languageData;
	}

	
	
}