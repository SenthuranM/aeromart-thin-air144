package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class UserNotesAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(UserNotesAction.class);
	
	private String pnr;

	private String userNotes;
	
	private String userNoteType;

	private boolean success = true;

	private String messageTxt;
	
	public String execute() {

		boolean isGroupPNR = false; 
		String chkGroupPNR = request.getParameter("groupPNR");
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		
		try{
	    	ArrayList<String> lastUserNote = new ArrayList<String>();
	    	lastUserNote.add(userNotes);
			UserNoteTO userNoteTO = new UserNoteTO();

	    	userNoteTO.setUserNotes(lastUserNote);
	    	userNoteTO.setUserNoteType(userNoteType);
	    	userNoteTO.setPnr(pnr);	

			if ((chkGroupPNR != null) && (!chkGroupPNR.isEmpty())) {
				isGroupPNR = true;
			}

			userNoteTO.setGroupPNR(isGroupPNR);

			ModuleServiceLocator.getAirproxyReservationBD().addUserNote(userNoteTO, getTrackInfo());
	    } catch (Exception e) {
	    	success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			
			log.error(e);
			setSuccess(false);
	    }
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the userNotes
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes the userNotes to set
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	/**
	 * @return the userNoteType
	 */
	public String getUserNoteType() {
		return userNoteType;
	}

	/**
	 * @param userNoteType the userNoteType to set
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}
    
}
